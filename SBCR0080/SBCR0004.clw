

   MEMBER('sbcr0080.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBCR0004.INC'),ONCE        !Local module procedure declarations
                     END


ReplacedPartsDataReport PROCEDURE                     !Generated from procedure template - Window

StartDate            DATE
EndDate              DATE
TempDate             DATE
SavePath             STRING(255)
RecordCount          LONG
LOC_GROUP            GROUP,PRE(loc)
ApplicationName      STRING(30)
ProgramName          STRING(30)
UserName             STRING(61)
Filename             STRING(255)
                     END
FoundCount           LONG
CSVFILE     FILE,DRIVER('BASIC','/ALWAYSQUOTE=ON'),PRE(csv),NAME(Out_Filename),CREATE,BINDABLE,THREAD
Record          RECORD
rec_type                STRING(8)        ! STRING(2)     ! Record Type always "PA"
tpm_rcv_no              STRING(10)       ! STRING(7)     ! TPM Reference (Work Order) number
acc_no                  STRING(6)        ! STRING(3)     ! TPM ID (Fix ID by each TPM)
IMEI_no                 STRING(15)       ! STRING(15)    ! IMEI number
tpm_rcv_datetime        STRING(16)       ! STRING(10)    ! TPM Receive yyyymmdd
parts_code              STRING(17)       ! STRING(17)    ! Replaced Parts code
qty                     STRING(3)        ! STRING(2)     ! Qty
parts_description       STRING(30)       ! STRING(30)    ! Parts Description
main_cause              STRING(10)       ! STRING(1)     ! Main Parts Cause Flag
                END
              END
window               WINDOW('Replaced Parts Data Export'),AT(,,216,103),FONT('Tahoma',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       PROMPT('Completed Date From'),AT(8,16),USE(?Prompt1),TRN,LEFT
                       ENTRY(@D08B),AT(100,16,64,10),USE(StartDate)
                       BUTTON,AT(168,16,10,10),USE(?PopCalendar),IMM,FLAT,LEFT,ICON('calenda2.ico')
                       PROMPT('Completed Date To'),AT(8,32),USE(?Prompt2),TRN,LEFT
                       ENTRY(@D08B),AT(100,32,64,10),USE(EndDate)
                       BUTTON,AT(168,32,10,10),USE(?PopCalendar:2),FLAT,LEFT,ICON('calenda2.ico')
                       SHEET,AT(4,2,208,70),USE(?Sheet1),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Record No.'),AT(20,52),USE(?RecordCountPrompt),TRN,HIDE
                           STRING(@n8),AT(64,52,32,),USE(RecordCount),TRN,HIDE,RIGHT
                           PROMPT('Found'),AT(112,52),USE(?FoundCountPrompt),TRN,HIDE,LEFT
                           STRING(@n8),AT(140,52),USE(FoundCount),TRN,HIDE
                         END
                       END
                       PANEL,AT(4,74,208,26),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('E&xport'),AT(88,78,56,16),USE(?OKButton),FLAT,LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('&Cancel'),AT(148,78,56,16),USE(?CancelButton),FLAT,LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Open                   PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?StartDate{prop:ReadOnly} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 15066597
    Elsif ?StartDate{prop:Req} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 8454143
    Else ! If ?StartDate{prop:Req} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 16777215
    End ! If ?StartDate{prop:Req} = True
    ?StartDate{prop:Trn} = 0
    ?StartDate{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?EndDate{prop:ReadOnly} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 15066597
    Elsif ?EndDate{prop:Req} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 8454143
    Else ! If ?EndDate{prop:Req} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 16777215
    End ! If ?EndDate{prop:Req} = True
    ?EndDate{prop:Trn} = 0
    ?EndDate{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?RecordCountPrompt{prop:FontColor} = -1
    ?RecordCountPrompt{prop:Color} = 15066597
    ?RecordCount{prop:FontColor} = -1
    ?RecordCount{prop:Color} = 15066597
    ?FoundCountPrompt{prop:FontColor} = -1
    ?FoundCountPrompt{prop:Color} = 15066597
    ?FoundCount{prop:FontColor} = -1
    ?FoundCount{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE

    CommandLine = CLIP(COMMAND(''))

    tmpPos = INSTRING('%', CommandLine)
    IF (NOT tmpPos) THEN
        MessageEx('Attempting to use ' & CLIP(LOC:ProgramName) & '<10,13>'           & |
            '   without using ' & CLIP(LOC:ApplicationName) & '.<10,13>'                        & |
            '   Start ' & CLIP(LOC:ApplicationName) & ' and run the report from there.<10,13>',   |
            CLIP(LOC:ApplicationName),                                                            |
            'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
       POST(Event:CloseWindow)
       EXIT
    END

    SET(USERS)
    Access:USERS.Clearkey(use:Password_Key)
    glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
    Access:USERS.Clearkey(use:Password_Key)
    use:Password = glo:Password
    IF (Access:USERS.Tryfetch(use:Password_Key) <> Level:benign) THEN
        MessageEx('Unable to find your logged in user details.', |
                CLIP(LOC:ApplicationName),                                  |
                'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
        POST(Event:CloseWindow)
        EXIT
    END

    IF (CLIP(use:Forename) = '') THEN
        LOC:UserName = use:Surname
    ELSE
        LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname
    END

    IF (CLIP(LOC:UserName) = '') THEN
        LOC:UserName = '<' & use:User_Code & '>'
    END

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReplacedPartsDataReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:JOBNOTES.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:WARPARTS.UseFile
  Access:PARTS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?StartDate{Prop:Alrt,255} = MouseLeft2
  ?EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBNOTES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  StartDate = TODAY()
  EndDate   = StartDate
  
  LOC:ApplicationName = 'ServiceBase 2000'
  LOC:ProgramName = 'Replaced Parts Data Export'
  LOC:UserName = ''
  
  DO GetUserName
    
  DISPLAY
  PARENT.Open


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          StartDate = TINCALENDARStyle1(StartDate)
          Display(?StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          EndDate = TINCALENDARStyle1(EndDate)
          Display(?EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OKButton
      ThisWindow.Update
      savepath = PATH()
      
      SET(defaults)
      access:defaults.next()
      
      fname" = '\PA002-' & FORMAT(TODAY(), @D012) & '.csv'
      
      IF (def:exportpath <> '') THEN
          Out_Filename = CLIP(def:exportpath) & fname"
      ELSE
          Out_Filename = 'C:' & fname"
      END
      
      IF (NOT FILEDIALOG('Choose File',Out_Filename,'*.*', file:save + file:keepdir + file:longname)) THEN
          SETPATH(savepath)
      ELSE
      
          IF (StartDate > EndDate) THEN
              TempDate = EndDate
              EndDate = StartDate
              StartDate = TempDate
          END
      
          REMOVE(CSVFile)
          CREATE(CSVFile)
          OPEN(CSVFile)
          IF (ERROR() <> Level:benign) THEN
              MessageEx('Cannot create export file.', LOC:ApplicationName,|
                          'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                          beep:systemhand,msgex:samewidths,84,26,0)
          ELSE
              SETCURSOR(cursor:wait)
      
              RecordCount = 0
              FoundCount = 0
              TempCount# = 0
              TempCount2# = 0
              UNHIDE(?RecordCountPrompt)
              UNHIDE(?RecordCount)
              UNHIDE(?FoundCountPrompt)
              UNHIDE(?FoundCount)
              DISPLAY()
      
              CLEAR(CSVFile)
              csv:rec_type                = 'rec_type'
              csv:tpm_rcv_no              = 'tpm_rcv_no'
              csv:acc_no                  = 'acc_no'
              csv:IMEI_no                 = 'IMEI_no'
              csv:tpm_rcv_datetime        = 'tpm_rcv_datetime'
              csv:parts_code              = 'Parts code'
              csv:qty                     = 'Qty'
              csv:parts_description       = 'Parts_Description'
              csv:main_cause              = 'main_cause'
              ADD(CSVFile)
      
              access:JOBS.clearkey(job:datecompletedkey)
              job:date_completed = StartDate
              SET(job:datecompletedkey,job:datecompletedkey)
              LOOP
                  IF ((access:JOBS.next() <> Level:benign) OR (job:date_completed > EndDate)) THEN
                      BREAK
                  END
      
                  TempCount# += 1
                  TempCount2# += 1
                  IF (TempCount2# >= 61) THEN
                      TempCount2# = 0
                      RecordCount = TempCount#
                      DISPLAY(?RecordCount)
                  END
      
                  IF ((job:manufacturer <> 'SHARP') OR (job:Completed <> 'YES')) THEN
                      CYCLE
                  END
      
                  !FoundCount += 1
                  !DISPLAY(?FoundCount)
      
                  access:jobse.clearkey(jobe:RefNumberKey)
                  jobe:refnumber = job:ref_number
                  IF (access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign) THEN
                      workshop"        = FORMAT(jobe:InWorkshopDate, @D012)
                  ELSE
                      workshop"        = ''
                  END
      
                  IF (job:warranty_job = 'YES') THEN
                      access:warparts.clearkey(wpr:part_number_key)
                      wpr:ref_number = job:ref_number
                      SET(wpr:part_number_key, wpr:part_number_key)
                      LOOP
                          IF ((access:WARPARTS.next() <> Level:Benign) OR (wpr:ref_number <> job:ref_number)) THEN
                              BREAK
                          END
                          IF (wpr:part_number = 'ADJUSTMENT') THEN
                              CYCLE
                          END
      
                          FoundCount += 1
                          DISPLAY(?FoundCount)
      
                          CLEAR(CSVFile)
                          csv:rec_type                = 'PA'
                          csv:tpm_rcv_no              = SUB(CLIP(LEFT(job:ref_number)), 1, 7)
                          csv:acc_no                  = '002'
                          csv:IMEI_no                 = SUB(CLIP(LEFT(job:ESN)), 1, 15)
                          csv:tpm_rcv_datetime        = CLIP(LEFT(workshop"))
                          csv:parts_code              = SUB(CLIP(LEFT(wpr:part_number)), 1, 17)
                          csv:qty                     = LEFT(FORMAT(wpr:quantity, @n2))
                          csv:parts_description       = SUB(CLIP(LEFT(wpr:description)), 1, 30)
                          csv:main_cause              = SUB(CLIP(LEFT(wpr:Fault_Code5)), 1, 1)
                          ADD(CSVFile)
                      END
                  END
      
                  IF (job:chargeable_job = 'YES') THEN
                      access:parts.clearkey(par:part_number_key)
                      par:ref_number = job:ref_number
                      SET(par:part_number_key, par:part_number_key)
                      LOOP
                          IF ((access:PARTS.next() <> Level:Benign) OR (par:ref_number <> job:ref_number)) THEN
                              BREAK
                          END
                          IF (par:part_number = 'ADJUSTMENT') THEN
                              CYCLE
                          END
      
                          FoundCount += 1
                          DISPLAY(?FoundCount)
      
                          CLEAR(CSVFile)
                          csv:rec_type                = 'PA'
                          csv:tpm_rcv_no              = SUB(CLIP(LEFT(job:ref_number)), 1, 7)
                          csv:acc_no                  = '002'
                          csv:IMEI_no                 = SUB(CLIP(LEFT(job:ESN)), 1, 15)
                          csv:tpm_rcv_datetime        = CLIP(LEFT(workshop"))
                          csv:parts_code              = SUB(CLIP(LEFT(par:part_number)), 1, 17)
                          csv:qty                     = LEFT(FORMAT(par:quantity, @n2))
                          csv:parts_description       = SUB(CLIP(LEFT(par:description)), 1, 30)
                          csv:main_cause              = SUB(CLIP(LEFT(par:Fault_Code5)), 1, 1)
                          ADD(CSVFile)
                      END
                  END
              END
      
              RecordCount = TempCount#
              DISPLAY(?RecordCount)
              CLOSE(CSVFile)
              SETCURSOR()
              MESSAGE('Replaced Parts Data Export Complete')
              POST(Event:CloseWindow)
          END
      
      END
      
    OF ?CancelButton
      ThisWindow.Update
      POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

