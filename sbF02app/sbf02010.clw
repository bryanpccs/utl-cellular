

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF02010.INC'),ONCE        !Local module procedure declarations
                     END


SonyExport PROCEDURE                                  !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
Export_type          STRING(1)
local:File_Name      STRING(255),STATIC
savepath             STRING(255)
save_job_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Sony Export'),AT(,,136,96),FONT('Tahoma',8,,,CHARSET:ANSI),GRAY,DOUBLE
                       SHEET,AT(4,4,128,60),USE(?Sheet1),SPREAD
                         TAB('Sony Export'),USE(?Tab1)
                           OPTION('Export'),AT(8,20,120,40),USE(Export_type),BOXED
                             RADIO(' All Incomplete Jobs'),AT(12,32),USE(?Option1:Radio1),VALUE('A')
                             RADIO(' Jobs Completed Yesterday'),AT(12,48),USE(?Option1:Radio2),VALUE('J')
                           END
                         END
                       END
                       BUTTON('OK'),AT(16,72,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(72,72,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,68,128,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?cancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
SonyExport     File,Driver('BASIC'),Pre(sonexp),Name(local:File_Name),Create,Bindable,Thread
Record                  Record
OrderNumber                 String(30)
JobNumber                   Long
Status                      String(30)
Date_Booked                 String(30)
Warranty                    String(30)
InvoiceText                 String(255)
CompletedDate               String(15)
Consignment_Number          String(30)
                        End
                    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Export_type{prop:Font,3} = -1
    ?Export_type{prop:Color} = 15066597
    ?Export_type{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'SonyExport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Export_type',Export_type,'SonyExport',1)
    SolaceViewVars('local:File_Name',local:File_Name,'SonyExport',1)
    SolaceViewVars('savepath',savepath,'SonyExport',1)
    SolaceViewVars('save_job_id',save_job_id,'SonyExport',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Export_type;  SolaceCtrlName = '?Export_type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio1;  SolaceCtrlName = '?Option1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio2;  SolaceCtrlName = '?Option1:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SonyExport')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'SonyExport')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Option1:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'SonyExport',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      savepath = path()
      local:file_name = 'SONYEXP.CSV'
      If not filedialog ('Choose File',local:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !Failed
          setpath(savepath)
      else!If not filedialog
          !Found
          setpath(savepath)
      
          Remove(sonyexport)
      
          Open(sonyexport)
          Error# = 0
          If Error()
              Create(sonyexport)
              Open(sonyexport)
              If Error()
                  Case MessageEx('Cannot create export file.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Error# = 1
              End!If Error()
          End!If Error()
          If Error# = 0
      
              recordspercycle         = 25
              recordsprocessed        = 0
              percentprogress         = 0
              progress:thermometer    = 0
              open(progresswindow)
      
              ?progress:userstring{prop:text} = 'Exporting Incomplete Jobs...'
              ?progress:pcttext{prop:text} = '0% Completed'
      
      
              recordstoprocess    = Records(Courier)
      
      !---Before Routine
      
              IF Export_type= 'A'
      
                Setcursor(Cursor:Wait)
                recordstoprocess    = Records(Jobs)
                Save_job_ID = Access:JOBS.SaveFile()
                Access:JOBS.ClearKey(job:DateCompletedKey)
                job:Date_Completed = ''
                Set(job:DateCompletedKey,job:DateCompletedKey)
                Loop
                    If Access:JOBS.NEXT()
                       Break
                    End !If
                    If job:Date_Completed <> ''      |
                        Then Break.  ! End If
            !---Insert Routine
                    Do GetNextRecord2
                    cancelcheck# += 1
                    If cancelcheck# > (RecordsToProcess/100)
                        Do cancelcheck
                        If tmp:cancel = 1
                            Break
                        End!If tmp:cancel = 1
                        cancelcheck# = 0
                    End!If cancelcheck# > 50
      
      
                    If job:manufacturer <> 'SONY'
                        Cycle
                    End!If job:manufacturer <> 'PANASONIC'
      
                    Clear(sonexp:Record)
                    sonexp:OrderNumber  = job:Order_Number
                    sonexp:JobNumber    = job:Ref_Number
                    sonexp:Status       = job:Current_Status
                    sonexp:Date_Booked  = Format(job:Date_Booked,@d6)
                    If job:Warranty_Job = 'YES'
                        sonexp:Warranty           = 'YES'
                    Else!If job:Warranty_Job = 'YES'
                        sonexp:Warranty           = 'NO'
                    End!If job:Warranty_Job = 'YES'
                    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                    jbn:RefNumber   = job:Ref_Number
                    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        !Found
                        sonexp:InvoiceText = CLIP(job:Repair_Type)&' '&CLIP(job:Repair_Type_Warranty)&' '&jbn:Invoice_Text
                    Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        sonexp:InvoiceText  =  CLIP(job:Repair_Type)&' '&CLIP(job:Repair_Type_Warranty)
                    End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                    sonexp:CompletedDate = Format(job:Date_Completed,@d6)
                    sonexp:Consignment_Number = job:Incoming_Consignment_Number
                    Add(sonyexport)
                End !Loop
                Access:JOBS.RestoreFile(Save_job_ID)
              ELSE
                Setcursor(Cursor:Wait)
                recordstoprocess    = Records(Jobs)
                Save_job_ID = Access:JOBS.SaveFile()
                Access:JOBS.ClearKey(job:DateCompletedKey)
                job:Date_Completed = TODAY()-1
                Set(job:DateCompletedKey,job:DateCompletedKey)
                Loop
                    If Access:JOBS.NEXT()
                       Break
                    End !If
                    If job:Date_Completed <> TODAY()-1      |
                        Then Break.  ! End If
            !---Insert Routine
                    Do GetNextRecord2
                    cancelcheck# += 1
                    If cancelcheck# > (RecordsToProcess/100)
                        Do cancelcheck
                        If tmp:cancel = 1
                            Break
                        End!If tmp:cancel = 1
                        cancelcheck# = 0
                    End!If cancelcheck# > 50
      
      
                    If job:manufacturer <> 'SONY'
                        Cycle
                    End!If job:manufacturer <> 'PANASONIC'
      
                    Clear(sonexp:Record)
                    sonexp:OrderNumber  = job:Order_Number
                    sonexp:JobNumber    = job:Ref_Number
                    sonexp:Status       = job:Current_Status
                    sonexp:Date_Booked  = Format(job:Date_Booked,@d6)
                    If job:Warranty_Job = 'YES'
                        sonexp:Warranty           = 'YES'
                    Else!If job:Warranty_Job = 'YES'
                        sonexp:Warranty           = 'NO'
                    End!If job:Warranty_Job = 'YES'
                    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                    jbn:RefNumber   = job:Ref_Number
                    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        !Found
                        sonexp:InvoiceText = CLIP(job:Repair_Type)&' '&CLIP(job:Repair_Type_Warranty)&' '&jbn:Invoice_Text
                    Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        sonexp:InvoiceText  =  CLIP(job:Repair_Type)&' '&CLIP(job:Repair_Type_Warranty)
                    End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                    sonexp:CompletedDate = Format(job:Date_Completed,@d6)
                    sonexp:Consignment_Number = job:Incoming_Consignment_Number
                    Add(sonyexport)
                End !Loop
                Access:JOBS.RestoreFile(Save_job_ID)
              END
          !---After Routine
              Setcursor()
              Do EndPrintRun
              close(progresswindow)
              Close(sonyexport)
              Case MessageEx('Export Complete.','ServiceBase 2000',|
                             'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End!If Error# = 0
      
      End!If not filedialog
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'SonyExport')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

