

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBF02001.INC'),ONCE        !Local module procedure declarations
                     END





ConsignmentSearch PROCEDURE                           !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
save_job_id          USHORT,AUTO
save_cou_id          USHORT,AUTO
tmp:ConsignmentNumber STRING(30)
DisplayString        STRING(255)
tmp:startdate        DATE
tmp:enddate          DATE
tmp:imei             STRING(30)
tmp:courier          STRING(30)
save_jobser_id       USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBSEARC)
                       PROJECT(jobser:RefNumber)
                       PROJECT(jobser:JobType)
                       PROJECT(jobser:AccountNumber)
                       PROJECT(jobser:IMEI)
                       PROJECT(jobser:Courier)
                       PROJECT(jobser:RecordNumber)
                       PROJECT(jobser:ConsignmentNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
jobser:RefNumber       LIKE(jobser:RefNumber)         !List box control field - type derived from field
jobser:JobType         LIKE(jobser:JobType)           !List box control field - type derived from field
jobser:AccountNumber   LIKE(jobser:AccountNumber)     !List box control field - type derived from field
jobser:IMEI            LIKE(jobser:IMEI)              !List box control field - type derived from field
jobser:Courier         LIKE(jobser:Courier)           !List box control field - type derived from field
jobser:RecordNumber    LIKE(jobser:RecordNumber)      !Primary key field - type derived from field
jobser:ConsignmentNumber LIKE(jobser:ConsignmentNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
window               WINDOW('Consignment Number Search'),AT(,,440,323),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,432,288),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Consignment Note Number Search Facility'),AT(8,8),USE(?Prompt4),FONT(,14,COLOR:Navy,FONT:bold)
                           PROMPT('Select the date range of when the jobs were despatched.'),AT(8,28),USE(?Prompt5)
                           PROMPT('Start Date'),AT(8,44),USE(?tmp:startdate:Prompt)
                           ENTRY(@d6),AT(84,44,64,10),USE(tmp:startdate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(152,44,10,10),USE(?LookupStartDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(8,60),USE(?tmp:enddate:Prompt)
                           ENTRY(@d6),AT(84,60,64,10),USE(tmp:enddate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(152,60,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('Select the Consignment Number to search for'),AT(8,80),USE(?Prompt5:2)
                           PROMPT('Search Results'),AT(8,136),USE(?SearchResults)
                           PROMPT('Consignment Number'),AT(8,96),USE(?tmp:ConsignmentNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,96,124,10),USE(tmp:ConsignmentNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Consignment Number'),TIP('Consignment Number'),UPR
                           BUTTON('Begin Search'),AT(84,112,56,16),USE(?BeginSearch),LEFT,ICON('Spy.gif')
                           LIST,AT(8,148,360,140),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('51L(2)|M~Job Number~@s8@40L(2)|M~Job Type~@s3@120L(2)|M~Account Number~@s30@71L(' &|
   '2)|M~I.M.E.I. Number~@s16@120L(2)|M~Courier~@s30@'),FROM(Queue:Browse)
                           BUTTON('Open'),AT(376,208,56,16),USE(?Button3),LEFT,ICON('edit.gif')
                         END
                       END
                       PANEL,AT(4,296,432,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(376,300,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,imm,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
       button('Cancel'),at(54,44,56,16),use(?cancel),left,icon('cancel.gif')
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse                  !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepLongClass   !LONG            !Xplore: Column displaying jobser:RefNumber
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying jobser:RefNumber
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying jobser:JobType
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying jobser:JobType
Xplore1Step3         StepStringClass !STRING          !Xplore: Column displaying jobser:AccountNumber
Xplore1Locator3      IncrementalLocatorClass          !Xplore: Column displaying jobser:AccountNumber
Xplore1Step4         StepStringClass !STRING          !Xplore: Column displaying jobser:IMEI
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying jobser:IMEI
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying jobser:Courier
Xplore1Locator5      IncrementalLocatorClass          !Xplore: Column displaying jobser:Courier
Xplore1Step6         StepLongClass   !LONG            !Xplore: Column displaying jobser:RecordNumber
Xplore1Locator6      IncrementalLocatorClass          !Xplore: Column displaying jobser:RecordNumber
Xplore1Step7         StepStringClass !STRING          !Xplore: Column displaying jobser:ConsignmentNumber
Xplore1Locator7      IncrementalLocatorClass          !Xplore: Column displaying jobser:ConsignmentNumber

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?tmp:startdate:Prompt{prop:FontColor} = -1
    ?tmp:startdate:Prompt{prop:Color} = 15066597
    If ?tmp:startdate{prop:ReadOnly} = True
        ?tmp:startdate{prop:FontColor} = 65793
        ?tmp:startdate{prop:Color} = 15066597
    Elsif ?tmp:startdate{prop:Req} = True
        ?tmp:startdate{prop:FontColor} = 65793
        ?tmp:startdate{prop:Color} = 8454143
    Else ! If ?tmp:startdate{prop:Req} = True
        ?tmp:startdate{prop:FontColor} = 65793
        ?tmp:startdate{prop:Color} = 16777215
    End ! If ?tmp:startdate{prop:Req} = True
    ?tmp:startdate{prop:Trn} = 0
    ?tmp:startdate{prop:FontStyle} = font:Bold
    ?tmp:enddate:Prompt{prop:FontColor} = -1
    ?tmp:enddate:Prompt{prop:Color} = 15066597
    If ?tmp:enddate{prop:ReadOnly} = True
        ?tmp:enddate{prop:FontColor} = 65793
        ?tmp:enddate{prop:Color} = 15066597
    Elsif ?tmp:enddate{prop:Req} = True
        ?tmp:enddate{prop:FontColor} = 65793
        ?tmp:enddate{prop:Color} = 8454143
    Else ! If ?tmp:enddate{prop:Req} = True
        ?tmp:enddate{prop:FontColor} = 65793
        ?tmp:enddate{prop:Color} = 16777215
    End ! If ?tmp:enddate{prop:Req} = True
    ?tmp:enddate{prop:Trn} = 0
    ?tmp:enddate{prop:FontStyle} = font:Bold
    ?Prompt5:2{prop:FontColor} = -1
    ?Prompt5:2{prop:Color} = 15066597
    ?SearchResults{prop:FontColor} = -1
    ?SearchResults{prop:Color} = 15066597
    ?tmp:ConsignmentNumber:Prompt{prop:FontColor} = -1
    ?tmp:ConsignmentNumber:Prompt{prop:Color} = 15066597
    If ?tmp:ConsignmentNumber{prop:ReadOnly} = True
        ?tmp:ConsignmentNumber{prop:FontColor} = 65793
        ?tmp:ConsignmentNumber{prop:Color} = 15066597
    Elsif ?tmp:ConsignmentNumber{prop:Req} = True
        ?tmp:ConsignmentNumber{prop:FontColor} = 65793
        ?tmp:ConsignmentNumber{prop:Color} = 8454143
    Else ! If ?tmp:ConsignmentNumber{prop:Req} = True
        ?tmp:ConsignmentNumber{prop:FontColor} = 65793
        ?tmp:ConsignmentNumber{prop:Color} = 16777215
    End ! If ?tmp:ConsignmentNumber{prop:Req} = True
    ?tmp:ConsignmentNumber{prop:Trn} = 0
    ?tmp:ConsignmentNumber{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        case event()
            of event:timer
                break
            of event:closewindow
                cancel# = 1
                break
            of event:accepted
                if field() = ?cancel
                    cancel# = 1
                    break
                end!if field() = ?button1
        end!case event()
    end!accept
    if cancel# = 1
        case messageex('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,charset:ansi,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            of 1 ! &yes button
                tmp:cancel = 1
            of 2 ! &no button
        end!case messageex
    end!if cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ConsignmentSearch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_id',save_job_id,'ConsignmentSearch',1)
    SolaceViewVars('save_cou_id',save_cou_id,'ConsignmentSearch',1)
    SolaceViewVars('tmp:ConsignmentNumber',tmp:ConsignmentNumber,'ConsignmentSearch',1)
    SolaceViewVars('DisplayString',DisplayString,'ConsignmentSearch',1)
    SolaceViewVars('tmp:startdate',tmp:startdate,'ConsignmentSearch',1)
    SolaceViewVars('tmp:enddate',tmp:enddate,'ConsignmentSearch',1)
    SolaceViewVars('tmp:imei',tmp:imei,'ConsignmentSearch',1)
    SolaceViewVars('tmp:courier',tmp:courier,'ConsignmentSearch',1)
    SolaceViewVars('save_jobser_id',save_jobser_id,'ConsignmentSearch',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:startdate:Prompt;  SolaceCtrlName = '?tmp:startdate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:startdate;  SolaceCtrlName = '?tmp:startdate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStartDate;  SolaceCtrlName = '?LookupStartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:enddate:Prompt;  SolaceCtrlName = '?tmp:enddate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:enddate;  SolaceCtrlName = '?tmp:enddate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEndDate;  SolaceCtrlName = '?LookupEndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:2;  SolaceCtrlName = '?Prompt5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SearchResults;  SolaceCtrlName = '?SearchResults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ConsignmentNumber:Prompt;  SolaceCtrlName = '?tmp:ConsignmentNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ConsignmentNumber;  SolaceCtrlName = '?tmp:ConsignmentNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BeginSearch;  SolaceCtrlName = '?BeginSearch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('ConsignmentSearch','?List')      !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ConsignmentSearch')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ConsignmentSearch')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt4
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:COURIER.Open
  Relate:EXCHANGE.Open
  Relate:JOBSEARC.Open
  Access:JOBS.UseFile
  Access:LOAN.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:JOBSEARC,SELF)
  tmp:startdate = Deformat('1/1/1990',@d6)
  tmp:enddate = Today()
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse,window,?List,'sbf02app.INI','>Header',1,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  ?tmp:startdate{Prop:Alrt,255} = MouseLeft2
  ?tmp:enddate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,jobser:ConsignmentNoKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,jobser:ConsignmentNumber,1,BRW1)
  BRW1.AddField(jobser:RefNumber,BRW1.Q.jobser:RefNumber)
  BRW1.AddField(jobser:JobType,BRW1.Q.jobser:JobType)
  BRW1.AddField(jobser:AccountNumber,BRW1.Q.jobser:AccountNumber)
  BRW1.AddField(jobser:IMEI,BRW1.Q.jobser:IMEI)
  BRW1.AddField(jobser:Courier,BRW1.Q.jobser:Courier)
  BRW1.AddField(jobser:RecordNumber,BRW1.Q.jobser:RecordNumber)
  BRW1.AddField(jobser:ConsignmentNumber,BRW1.Q.jobser:ConsignmentNumber)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:EXCHANGE.Close
    Relate:JOBSEARC.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('ConsignmentSearch','?List',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  Remove(jobsearc)
  If Error()
      access:jobsearc.close()
      Remove(jobsearc)
  End!If Error()
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ConsignmentSearch',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(jobser:RefNumber,BRW1.Q.jobser:RefNumber)
  Xplore1.AddField(jobser:JobType,BRW1.Q.jobser:JobType)
  Xplore1.AddField(jobser:AccountNumber,BRW1.Q.jobser:AccountNumber)
  Xplore1.AddField(jobser:IMEI,BRW1.Q.jobser:IMEI)
  Xplore1.AddField(jobser:Courier,BRW1.Q.jobser:Courier)
  Xplore1.AddField(jobser:RecordNumber,BRW1.Q.jobser:RecordNumber)
  Xplore1.AddField(jobser:ConsignmentNumber,BRW1.Q.jobser:ConsignmentNumber)
  BRW1.FileOrderNbr = BRW1.AddSortOrder()             !Xplore Sort Order for File Sequence
  BRW1.SetOrder('')                                   !Xplore
  Xplore1.AddAllColumnSortOrders(0)                   !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:startdate = TINCALENDARStyle1(tmp:startdate)
          Display(?tmp:startdate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:enddate = TINCALENDARStyle1(tmp:enddate)
          Display(?tmp:enddate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?BeginSearch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BeginSearch, Accepted)
      recordspercycle         = 25
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
      thiswindow.reset(1)
      open(progresswindow)
      
      ?progress:userstring{prop:text} = 'Running...'
      ?progress:pcttext{prop:text} = '0% Completed'
      
      recordspercycle         = 25
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
      recordstoprocess    = Records(Jobs)
      
      
      !---before routine
      
      
      setcursor(cursor:wait)
      save_jobser_id = access:jobsearc.savefile()
      access:jobsearc.clearkey(jobser:refnumberkey)
      set(jobser:refnumberkey,jobser:refnumberkey)
      loop
          if access:jobsearc.next()
             break
          end !if
          Delete(jobsearc)
      end !loop
      access:jobsearc.restorefile(save_jobser_id)
      
      save_job_id = access:jobs.savefile()
      access:jobs.clearkey(job:consignmentnokey)
      job:consignment_number = tmp:consignmentnumber
      set(job:consignmentnokey,job:consignmentnokey)
      loop
          if access:jobs.next()
             break
          end !if
          if job:consignment_number <> tmp:consignmentnumber      |
              then break.  ! end if
          ?progress:userstring{prop:text} = 'Checking Jobs..'
          do getnextrecord2
          
      
          cancelcheck# += 1
          if cancelcheck# > (recordstoprocess/100)
              do cancelcheck
              if tmp:cancel = 1
                  break
              end!if tmp:cancel = 1
              cancelcheck# = 0
          end!if cancelcheck# > 50
      
          If access:jobsearc.primerecord() = Level:Benign
              jobser:refnumber    = job:ref_number
              jobser:consignmentnumber = job:consignment_number
              jobser:jobtype      = 'JOB'
              jobser:courier      = job:courier
              jobser:imei         = job:esn
              jobser:accountnumber    = job:account_number
              If access:jobsearc.tryinsert()
                  access:jobsearc.cancelautoinc()
              End!If access:jobsearc.tryinsert()
          End!If access:jobsearc.primerecord() = Level:Benign
      end !loop
      access:jobs.restorefile(save_job_id)
      
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
      recordstoprocess    = Records(Courier)
      
      If tmp:cancel <> 1
      
      
          save_cou_id = access:courier.savefile()
          access:courier.clearkey(cou:courier_key)
          set(cou:courier_key)
          loop
              if access:courier.next()
                 break
              end !if
              ?progress:userstring{prop:text} = 'Checking Exchanges/Loans..'
              do getnextrecord2
              cancelcheck# += 1
              if cancelcheck# > (recordstoprocess/100)
                  do cancelcheck
                  if tmp:cancel = 1
                      break
                  end!if tmp:cancel = 1
                  cancelcheck# = 0
              end!if cancelcheck# > 50
      
              save_job_id = access:jobs.savefile()
              access:jobs.clearkey(job:datedespexckey)
              job:exchange_courier    = cou:courier
              job:exchange_despatched = tmp:startdate
              set(job:datedespexckey,job:datedespexckey)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  if job:exchange_courier    <> cou:courier      |
                      or job:exchange_despatched > tmp:enddate |
                      then break.  ! end if
      
      
                  if job:exchange_consignment_number = tmp:consignmentnumber
                      access:exchange.clearkey(xch:ref_number_key)
                      xch:ref_number  = job:exchange_unit_number
                      If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                          !Found
                      Else! If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                          !Error
                      End! If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
      
                      If access:jobsearc.primerecord() = Level:Benign
                          jobser:refnumber    = job:ref_number
                          jobser:consignmentnumber = job:exchange_consignment_number
                          jobser:jobtype      = 'EXC'
                          jobser:courier      = job:exchange_courier
                          jobser:imei         = xch:esn
                          jobser:accountnumber    = job:account_number
                          If access:jobsearc.tryinsert()
                              access:jobsearc.cancelautoinc()
                          End!If access:jobsearc.tryinsert()
                      End!If access:jobsearc.primerecord() = Level:Benign
                  End!if job:exchange_consignment_number = tmp:consignmentnumber
              end !loop
              access:jobs.restorefile(save_job_id)
      
              save_job_id = access:jobs.savefile()
              access:jobs.clearkey(job:datedesploakey)
              job:loan_courier    = cou:courier
              job:loan_despatched = tmp:startdate
              set(job:datedesploakey,job:datedesploakey)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  if job:loan_courier    <> cou:courier      |
                      Or job:loan_despatched > tmp:enddate   |
                      then break.  ! end if
                  if job:exchange_consignment_number = tmp:consignmentnumber
                      access:loan.clearkey(loa:ref_number_key)
                      loa:ref_number  = job:loan_unit_number
                      If access:loan.tryfetch(loa:ref_number_key) = Level:Benign
                          !Found
                      Else! If access:loan.tryfetch(loa:ref_number_key) = Level:Benign
                          !Error
                      End! If access:loan.tryfetch(loa:ref_number_key) = Level:Benign
      
                      If access:jobsearc.primerecord() = Level:Benign
                          jobser:refnumber    = job:ref_number
                          jobser:consignmentnumber = job:loan_consignment_number
                          jobser:jobtype      = 'LOA'
                          jobser:accountnumber   = job:account_number
                          jobser:courier      = job:loan_courier
                          jobser:imei         = loa:esn
                          If access:jobsearc.tryinsert()
                              access:jobsearc.cancelautoinc()
                          End!If access:jobsearc.tryinsert()
                      End!If access:jobsearc.primerecord() = Level:Benign
                  End!if job:exchange_consignment_number = tmp:consignmentnu
              end !loop
              access:jobs.restorefile(save_job_id)
              end !loop
          access:courier.restorefile(save_cou_id)
      End!If tmp:cancel <> 1
      setcursor()
      
      !---after routine
      do endprintrun
      close(progresswindow)
      BRW1.ResetSort(1)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BeginSearch, Accepted)
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = brw1.q.jobser:refnumber
      If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
          !Found
          SaveRequest# = Globalrequest
          Globalrequest = ChangeRecord
          UpdateJobs
          Globalrequest   = SaveRequest#
      Else! If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
          !Error
      End! If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ConsignmentSearch')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:startdate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:enddate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
    OF EVENT:GainFocus
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?List                                  !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List                             !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !jobser:RefNumber
  OF 2 !jobser:JobType
  OF 3 !jobser:AccountNumber
  OF 4 !jobser:IMEI
  OF 5 !jobser:Courier
  OF 6 !jobser:RecordNumber
  OF 7 !jobser:ConsignmentNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step7)
  SELF.FQ.SortField = SELF.BC.AddSortOrder()
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(,jobser:RefNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,jobser:JobType,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,jobser:AccountNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,jobser:IMEI,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(,jobser:Courier,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,jobser:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(,jobser:ConsignmentNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(JOBSEARC)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('jobser:RefNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job Number')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Job Number')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jobser:JobType')             !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Job Type')                   !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Job Type')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jobser:AccountNumber')       !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jobser:IMEI')                !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('I.M.E.I. Number')            !Header
                PSTRING('@s16')                       !Picture
                PSTRING('I.M.E.I. Number')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jobser:Courier')             !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Courier')                    !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Courier')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jobser:RecordNumber')        !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Record Number')              !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jobser:ConsignmentNumber')   !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Consignment Number')         !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Consignment Number')         !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(7)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)
