

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF02004.INC'),ONCE        !Local module procedure declarations
                     END


CRCImportSwift PROCEDURE (func:PrintLabel,func:PrintJobCard) !Generated from procedure template - Window

tmp:FileName         STRING(255)
BouncerList          QUEUE,PRE(bounce)
RefNumber            LONG
                     END
tmp:LetterName       STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('C.R.C. Jobs Import (Swift)'),AT(,,247,92),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,240,56),USE(?Sheet1),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Select the file you wish to import'),AT(8,8,140,12),USE(?Prompt1),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(228,24,10,10),USE(?LookupFileName),SKIP,ICON('List3.ico')
                           PROMPT('Import File Name'),AT(9,24),USE(?tmp:FileName:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,24,140,10),USE(filename3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Import File Name'),TIP('Import File Name'),REQ,UPR
                           PROMPT('Letter Name'),AT(8,40),USE(?tmp:LetterName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,40,140,10),USE(tmp:LetterName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Letter Name'),TIP('Letter Name'),UPR
                           BUTTON,AT(228,40,10,10),USE(?LookupLetters),SKIP,ICON('List3.ico')
                         END
                       END
                       PANEL,AT(4,64,240,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Import Jobs'),AT(128,68,56,16),USE(?Button3),LEFT,ICON('DISK.GIF')
                       BUTTON('Cancel'),AT(184,68,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup4          SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ImportFile      FILE,DRIVER('BASIC'),PRE(imp),NAME(filename3),CREATE,BINDABLE,THREAD
Record              Record
RaisedBy                String(30)
ModelNumber             String(30)
UnitType1               String(30) !Handset
UnitType2               String(30) !Battery
UnitType3               String(30) !Charger
CustomerName            String(30)
MobileNumber            String(30)
IMEI                    String(30)
MSN                     String(30)
JobType                 String(30)
CollectionDate          String(30)
PostCode                String(30)
OrderNumber             String(30)
DOP                     String(30)
FaultDescription        String(255)
                    End
                End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:FileName:Prompt{prop:FontColor} = -1
    ?tmp:FileName:Prompt{prop:Color} = 15066597
    If ?filename3{prop:ReadOnly} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 15066597
    Elsif ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 8454143
    Else ! If ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 16777215
    End ! If ?filename3{prop:Req} = True
    ?filename3{prop:Trn} = 0
    ?filename3{prop:FontStyle} = font:Bold
    ?tmp:LetterName:Prompt{prop:FontColor} = -1
    ?tmp:LetterName:Prompt{prop:Color} = 15066597
    If ?tmp:LetterName{prop:ReadOnly} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 15066597
    Elsif ?tmp:LetterName{prop:Req} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 8454143
    Else ! If ?tmp:LetterName{prop:Req} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 16777215
    End ! If ?tmp:LetterName{prop:Req} = True
    ?tmp:LetterName{prop:Trn} = 0
    ?tmp:LetterName{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CRCImportSwift',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:FileName',tmp:FileName,'CRCImportSwift',1)
    SolaceViewVars('BouncerList:RefNumber',BouncerList:RefNumber,'CRCImportSwift',1)
    SolaceViewVars('tmp:LetterName',tmp:LetterName,'CRCImportSwift',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFileName;  SolaceCtrlName = '?LookupFileName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:FileName:Prompt;  SolaceCtrlName = '?tmp:FileName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?filename3;  SolaceCtrlName = '?filename3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LetterName:Prompt;  SolaceCtrlName = '?tmp:LetterName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LetterName;  SolaceCtrlName = '?tmp:LetterName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLetters;  SolaceCtrlName = '?LookupLetters';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CRCImportSwift')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'CRCImportSwift')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFCRC.Open
  Relate:JOBS2_ALIAS.Open
  Relate:LETTERS.Open
  Relate:MERGELET.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  FileLookup4.Init
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:LongName)
  FileLookup4.SetMask('CSV File','*.CSV')
  FileLookup4.WindowTitle='C.R.C. Jobs Import File'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFCRC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:LETTERS.Close
    Relate:MERGELET.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'CRCImportSwift',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFileName
      ThisWindow.Update
      filename3 = Upper(FileLookup4.Ask(1)  )
      DISPLAY
    OF ?LookupLetters
      ThisWindow.Update
      tmp:LetterName = BrowseAllLetters()
      ThisWindow.Reset
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      Case MessageEx('Are you sure you want to import the selected file?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Clear(BouncerList)
              Free(BouncerList)
              error# = 0
              Set(DEFCRC)
              If Access:DEFCRC.Next()
                  Case MessageEx('You have not setup any import defaults.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If Access:DEFCRC.Next()
      
              If error# = 0
                  If dfc:AccountNumber = ''
                      Case MessageEx('You have not setup any import defaults.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End!If dfc:AccountNumber = ''
              End!If error# = 0
      
              If error# = 0
      
                  Open(IMPORTFILE)
                  If Error()
                      Case MessageEx('Cannot open the import file.'&|
                        '<13,10>'&|
                        '<13,10>(' & Error() & ')','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If Error()
                      Setcursor(cursor:wait)
                      Set(IMPORTFILE,0)
                      Loop
                          Next(IMPORTFILE)
                          If Error()
                              Break
                          End!If Error()
                          If imp:RaisedBy <> 'IMPORT'
                              Cycle
                          End!If imp:RaisedBy <> 'IMPORT'
                          if access:jobs.primerecord() = level:benign
                              !call the status routine
                              GetStatus(0,1,'JOB')
                              job:location                      = dfc:location
                  !            If internal_location_temp <> ''
                  !                access:locinter.clearkey(loi:location_key)
                  !                loi:location    = internal_location_temp
                  !                If access:locinter.fetch(loi:location_key) = Level:Benign
                  !                    If loi:allocate_spaces = 'YES'
                  !                        If loi:current_spaces <= 0
                  !                            beep(beep:systemhand)  ;  yield()
                  !                            message('There are no more available spaces at this location.', |
                  !                                    'ServiceBase 2000', icon:hand)
                  !                            access:jobs.cancelautoinc()
                  !                            Select(?internal_location_temp:2)
                  !                            Exit
                  !                        Else   !If loi:current_spaces <= 0
                  !                            loi:current_spaces -= 1
                  !                            If loi:current_spaces <= 0
                  !                                loi:current_spaces = 0
                  !                                loi:location_available = 'NO'
                  !                            End
                  !                            access:locinter.update()
                  !                        End   !If loi:current_spaces <= 0
                  !                    End
                  !                    !call the status routine
                  !                    job:workshop                      = 'YES'
                  !                End
                  !            Else
                  !                job:workshop = ''
                  !            End
                              space# = Instring(' ',imp:ModelNumber,1,1)
                              job:Manufacturer    = Clip(Upper(Sub(imp:ModelNumber,1,space#-1)))
                              job:Model_Number    = Clip(Upper(Sub(imp:ModelNumber,space#+1,Len(imp:ModelNumber))))
      
                              If Upper(imp:UnitType1)    = 'YES'
                                  job:Unit_Type   = 'HANDSET'
                              Elsif Upper(imp:UnitType2) = 'YES'
                                  job:Unit_Type   = 'BATTERY'
                              Elsif Upper(imp:UnitType3) = 'YES'
                                  job:UNit_Type   = 'CHARGER'
                              End!If imp:UnitType1    = 'YES'
      
                              space# = Instring(' ',Clip(imp:CustomerName),1,1)
      
                              If space# = 0
                                  job:Title           = ''
                                  job:Surname         = Clip(Upper(imp:CustomerName))
                              Else!If space# = 0
                                  job:Title           = Clip(Upper(Sub(imp:CustomerName,1,space#-1)))
                                  job:Surname         = Clip(Upper(Sub(imp:CustomerName,space#+1,Len(imp:CustomerName))))
      
                              End!If space# = 0
      
                              job:Mobile_Number   = Upper(imp:MobileNumber)
                              job:ESN             = Upper(imp:IMEI)
                              job:MSN             = Upper(imp:MSN)
                              Case imp:JobType
                                  Of 'Yes'
                                      job:Warranty_Job = 'YES'
                                      job:Chargeable_Job = 'NO'
                                  Else
                                      job:Warranty_Job = 'NO'
                                      job:Chargeable_Job = 'YES'
                              End!Case imp:JobType
      
                              job:Postcode        = Upper(imp:Postcode)
                              Postcode_Routine(job:Postcode,job:Address_Line1,job:Address_Line2,job:Address_Line3)
      
                              job:Order_Number    = Upper(imp:OrderNumber)
                              !job:DOP            = Upper(Deformat(imp:DOP,@d6)) original
                              job:DOP             = Upper(Deformat(imp:DOP,@d7-)) ! 'dd-Mmm-yy'
                              If dfc:Location = ''
                                  job:Workshop = 'NO'
                              Else!If dfc:Location = ''
                                  job:Workshop    = 'YES'
                              End!If dfc:Location = ''
      
                              access:modelnum.clearkey(mod:model_number_key)
                              mod:model_number = job:model_number
                              if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                                  If mod:product_Type <> ''
                                      job:fault_code1 = mod:product_type
                                  End!If job:product_Type <> ''
                              end
                              access:jobnotes.primerecord()
                              jbn:refnumber  = job:ref_number
                              jbn:Engineers_Notes   = Upper(imp:FaultDescription)
                              access:jobnotes.insert()
                              job:account_number      = dfc:AccountNumber
                              despatch# = 0
                              access:subtracc.clearkey(sub:account_number_key)
                              sub:account_number = job:account_number
                              if access:subtracc.fetch(sub:account_number_key) = level:benign
                                  access:tradeacc.clearkey(tra:account_number_key) 
                                  tra:account_number = sub:main_account_number
                                  if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                      IF tra:invoice_sub_accounts = 'YES'
                                          job:courier_cost        = sub:courier_cost
                                          job:courier_cost_warranty = sub:courier_cost
                                      Else!IF tra:invoice_sub_accounts = 'YES'
                                          job:courier_cost        = tra:courier_cost
                                          job:courier_cost_warranty = sub:courier_cost
                                      End!IF tra:invoice_sub_accounts = 'YES'
                                      if tra:use_sub_accounts = 'YES'
                                          job:courier = sub:courier_outgoing
                                          job:incoming_courier = sub:courier_incoming
                                          job:exchange_courier = job:courier
                                          job:loan_courier = job:courier
                                          If sub:despatch_paid_jobs <> 'YES' and sub:despatch_invoiced_jobs <> 'YES'
                                              despatch# = 1
                                          End!If sub:despatch_paid_jobs <> 'YES' and sub:despatch_invoiced_jobs <> 'YES'
                                      else!if tra:use_sub_accounts = 'YES'
                                          job:courier = tra:courier_outgoing
                                          job:incoming_courier = tra:courier_incoming
                                          job:exchange_courier = job:courier
                                          job:loan_courier = job:courier
                                          If tra:despatch_paid_jobs <> 'YES' and tra:despatch_invoiced_jobs <> 'YES'
                                              despatch# = 1
                                          End!If tra:despatch_paid_jobs <> 'YES' and tra:despatch_invoiced_jobs <> 'YES'
                                      end!if tra:use_sub_accounts = 'YES'
                                  end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                              end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                              job:transit_type    = dfc:TransitType
      
                              access:trantype.clearkey(trt:transit_type_key)
                              trt:transit_type = job:transit_type
                              if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                                  IF trt:InWorkshop = 'YES'
                                      job:workshop = 'YES'
                                  End!IF trt:InWorkshop = 'YES'
                                  GetStatus(Sub(trt:initial_status,1,3),InsertRecord,'JOB')
                                  GetStatus(Sub(trt:Exchangestatus,1,3),InsertRecord,'EXC')
                                  GetStatus(Sub(trt:Loanstatus,1,3),InsertRecord,'LOA')
                              end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
      
                              access:users.clearkey(use:password_key)
                              use:password    =glo:password
                              If access:users.fetch(use:password_key) = Level:Benign
                                  job:who_booked                    = use:user_code
                              End
                              If job:warranty_job = 'YES'
                                  job:edi = Set_Edi(job:manufacturer)
                              end
      
                              access:users.clearkey(use:password_key)
                              use:password    =glo:password
                              access:users.fetch(use:password_key)
                              job:who_booked  = use:user_code
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                                  aud:notes         = 'UNIT DETAILS: ' & Clip(job:Manufacturer) & ' - ' & Clip(job:Model_Number) &|
                                                      '<13,10>I.M.E.I. NO: ' & Clip(job:ESN) & |
                                                      '<13,10>M.S.N.: ' & Clip(job:MSN) & |
                                                      '<13,10,13,10>CHARGEABLE JOB: ' & Clip(job:Chargeable_Job) &|
                                                      '<13,10>WARRANTY JOB: ' & Clip(job:Warranty_Job)
                                  aud:ref_number    = job:ref_number
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  aud:action        = 'CRC IMPORT'
                                  if access:audit.insert()
                                      access:audit.cancelautoinc()
                                  end
                              end!if access:audit.primerecord() = level:benign
      
                              if access:jobs.tryinsert()
                                  access:jobs.cancelautoinc()
                              Else
                                  If Access:JOBSE.PrimeRecord() = Level:Benign
                                      jobe:RefNumber  = job:Ref_Number
                                      If job:Workshop = 'YES'
                                          jobe:InWorkshopDate = Today()
                                          jobe:InWorkshopTime = Clock()
                                      End !If job:Workshop = 'YES'
                                      If Access:JOBSE.TryInsert() = Level:Benign
                                          !Insert Successful
                                      Else !If Access:JOBSE.TryInsert() = Level:Benign
                                          !Insert Failed
                                      End !If Access:JOBSE.TryInsert() = Level:Benign
                                  End !If Access:JOBSE.PrimeRecord() = Level:Benign
      
                                  PrintLetter(job:ref_Number,tmp:LetterName)
      
                                  If func:PrintLabel
                                      glo:Select1 = job:Ref_Number
                                      Thermal_Labels('')
                                      glo:Select1 = ''
                                  End !If func:PrintLabel
                                  If func:PrintJobCard
                                      glo:Select1 = job:Ref_Number
                                      Job_Card()
                                      glo:Select1 = ''
                                  End !If func:PrintLabel
                                  If CountBouncer(job:Ref_Number,job:Date_Booked,job:Esn)
                                      bounce:RefNumber = job:Ref_Number
                                      Add(BouncerList)
                                  End!If CountBouncer(job:Ref_Number,job:Date_Booked,job:Esn)
                              End!if access:jobs.tryinsert()
                          end!if access:jobs.primerecord() = level:benign
                      End!Loop
                  End!If Error()
                  Close(IMPORTFILE)
                  Setcursor()
                  Case MessageEx('Import Completed.','ServiceBase 2000',|
                                 'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  If Records(BouncerList)
                      Case MessageEx('One or more of the imported Jobs are Bouncers.'&|
                        '<13,10>'&|
                        '<13,10>Do you wish to print the Bouncer History Reports for these jobs?','ServiceBase 2000',|
                                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              Sort(BouncerList,bounce:RefNumber)
                              Loop x# = 1 To Records(BouncerList)
                                  Get(BouncerList,x#)
                                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                                  job:Ref_Number  = bounce:RefNUmber
                                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                      !Found
                                      glo:Select2 = job:esn
                                      Bouncer_History
                                      glo:Select2 = ''
                                  Else! If Access:.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                  
                              End!Loop x# = 1 To Records(BouncerList)
                          Of 2 ! &No Button
                      End!Case MessageEx
                  End!If Records(BouncerList)
              End!If error# = 0
      
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'CRCImportSwift')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

