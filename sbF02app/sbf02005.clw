

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF02005.INC'),ONCE        !Local module procedure declarations
                     END


CRCImportPanasonic PROCEDURE (func:PrintLabel,func:PrintJobCard) !Generated from procedure template - Window

BouncerList          QUEUE,PRE(bounce)
RefNumber            LONG
                     END
tmp:LetterName       STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('C.R.C. Jobs Import (Panasonic)'),AT(,,247,90),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,240,56),USE(?Sheet1),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Select the file you wish to import'),AT(8,8,140,12),USE(?Prompt1),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Import File Name'),AT(9,24),USE(?tmp:FileName:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,24,140,10),USE(filename3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Import File Name'),TIP('Import File Name'),REQ,UPR
                           BUTTON,AT(228,24,10,10),USE(?LookupFileName),SKIP,ICON('List3.ico')
                           BUTTON,AT(228,40,10,10),USE(?LookupLetter),SKIP,ICON('List3.ico')
                           PROMPT('Letter Name'),AT(8,40),USE(?tmp:LetterName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,40,140,10),USE(tmp:LetterName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Letter Name'),TIP('Letter Name'),REQ,UPR
                         END
                       END
                       PANEL,AT(4,64,240,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Import Jobs'),AT(128,68,56,16),USE(?Button3),LEFT,ICON('DISK.GIF')
                       BUTTON('Cancel'),AT(184,68,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup4          SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ImportFile      FILE,DRIVER('BASIC','/COMMA = 124'),PRE(imp),NAME(filename3),CREATE,BINDABLE,THREAD
Record              Record
ID                      String(30)
MobileNumber            String(30)
SerialNumber            String(30)
WarrantyStatus          String(30)
JobNotes                String(255)
DReceiver               String(30)
LockCode                String(30)
DCompany                String(30)
DAddressLine1           String(30)
DAddressLine2           String(30)
DAddressLine3           String(30)
DAddressLine4           String(30)
DPostcode               String(30)
DTelephoneNumber        String(30)
ModelNumber             String(30)
ServiceP                String(30)
CReceiver               String(30)
CCompany                String(30)
CAddressLine1           String(30)
CAddressLine2           String(30)
CAddressLine3           String(30)
CAddressLine4           String(30)
CPostcode               String(30)
CTelephoneNumber        String(30)
DOP                     String(30)

                    End
                End

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:FileName:Prompt{prop:FontColor} = -1
    ?tmp:FileName:Prompt{prop:Color} = 15066597
    If ?filename3{prop:ReadOnly} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 15066597
    Elsif ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 8454143
    Else ! If ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 16777215
    End ! If ?filename3{prop:Req} = True
    ?filename3{prop:Trn} = 0
    ?filename3{prop:FontStyle} = font:Bold
    ?tmp:LetterName:Prompt{prop:FontColor} = -1
    ?tmp:LetterName:Prompt{prop:Color} = 15066597
    If ?tmp:LetterName{prop:ReadOnly} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 15066597
    Elsif ?tmp:LetterName{prop:Req} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 8454143
    Else ! If ?tmp:LetterName{prop:Req} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 16777215
    End ! If ?tmp:LetterName{prop:Req} = True
    ?tmp:LetterName{prop:Trn} = 0
    ?tmp:LetterName{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CRCImportPanasonic',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('BouncerList:RefNumber',BouncerList:RefNumber,'CRCImportPanasonic',1)
    SolaceViewVars('tmp:LetterName',tmp:LetterName,'CRCImportPanasonic',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:FileName:Prompt;  SolaceCtrlName = '?tmp:FileName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?filename3;  SolaceCtrlName = '?filename3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFileName;  SolaceCtrlName = '?LookupFileName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLetter;  SolaceCtrlName = '?LookupLetter';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LetterName:Prompt;  SolaceCtrlName = '?tmp:LetterName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LetterName;  SolaceCtrlName = '?tmp:LetterName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CRCImportPanasonic')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'CRCImportPanasonic')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFCRC.Open
  Relate:JOBS2_ALIAS.Open
  Relate:LETTERS.Open
  Relate:MERGELET.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:MODELNUM.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FileLookup4.Init
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:LongName)
  FileLookup4.SetMask('CRC File','*.CRC')
  FileLookup4.WindowTitle='C.R.C. Jobs Import File'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFCRC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:LETTERS.Close
    Relate:MERGELET.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'CRCImportPanasonic',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFileName
      ThisWindow.Update
      filename3 = Upper(FileLookup4.Ask(1)  )
      DISPLAY
    OF ?LookupLetter
      ThisWindow.Update
      tmp:LetterName = BrowseAllLetters()
      ThisWindow.Reset
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      Case MessageEx('Are you sure you want to import the selected file?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Clear(BouncerList)
              Free(BouncerList)
              error# = 0
              Set(DEFCRC)
              If Access:DEFCRC.Next()
                  Case MessageEx('You have not setup any import defaults.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If Access:DEFCRC.Next()
      
              If error# = 0
                  If dfc:AccountNumber = ''
                      Case MessageEx('You have not setup any import defaults.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End!If dfc:AccountNumber = ''
              End!If error# = 0
      
              If error# = 0
      
                  Open(IMPORTFILE)
                  If Error()
                      Case MessageEx('Cannot open the import file.'&|
                        '<13,10>'&|
                        '<13,10>(' & Error() & ')','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If Error()
                      Setcursor(cursor:wait)
                      Set(IMPORTFILE,0)
                      Loop
                          Next(IMPORTFILE)
                          If Error()
                              Break
                          End!If Error()
                          If imp:ID = 'ID'
                              Cycle
                          End!If imp:RaisedBy <> 'IMPORT'
                          if access:jobs.primerecord() = level:benign
                              !call the status routine
                              GetStatus(0,1,'JOB')
                              job:location                      = dfc:PLocation
      
                              job:Order_Number    = Clip(Upper(imp:ID))
                              job:Mobile_Number   = Clip(Upper(imp:MobileNumber))
                              job:esn             = Clip(Upper(imp:SerialNumber))
      
                              EndNotes# = Instring(' ',Upper(imp:JobNotes),1,1)
                              If EndNotes# = 0
                                  EndNotes# = Len(Clip(imp:JobNotes))
                              End!If EndNotes# = 0
                              access:jobnotes.primerecord()
                              jbn:refnumber  = job:ref_number
                              jbn:Engineers_Notes   = Clip(Upper(imp:JobNotes))
                              access:jobnotes.insert()
      
                              job:Surname                 = Clip(Upper(imp:DReceiver))
      
                              job:Company_Name_Delivery   = Clip(Upper(imp:DCompany))
                              job:Address_Line1_Delivery  = Clip(Upper(imp:DAddressLine1))
                              job:Address_Line2_Delivery  = Clip(Upper(imp:DAddressLine2))
                              job:Address_Line3_Delivery  = Clip(Upper(imp:DAddressLine3))
                              job:Postcode_Delivery       = Clip(Upper(imp:DPostcode))
                              job:Telephone_Delivery      = Clip(Upper(imp:DTelephoneNumber))
      
                              job:Company_Name   = Clip(Upper(imp:DCompany))
                              job:Address_Line1  = Clip(Upper(imp:DAddressLine1))
                              job:Address_Line2  = Clip(Upper(imp:DAddressLine2))
                              job:Address_Line3  = Clip(Upper(imp:DAddressLine3))
                              job:Postcode       = Clip(Upper(imp:DPostcode))
                              job:Telephone_Number      = Clip(Upper(imp:DTelephoneNumber))
      
      
                              job:Model_Number            = Clip(Upper(imp:ModelNumber))
      
                              job:Company_Name_Collection   = Clip(Upper(imp:CCompany))
                              job:Address_Line1_Collection  = Clip(Upper(imp:CAddressLine1))
                              job:Address_Line2_Collection  = Clip(Upper(imp:CAddressLine2))
                              job:Address_Line3_Collection  = Clip(Upper(imp:CAddressLine3))
                              job:Postcode_Collection       = Clip(Upper(imp:CPostcode))
                              job:Telephone_Collection      = Clip(Upper(imp:CTelephoneNumber))
      
                              job:DOP                     = Clip(Upper(Deformat(imp:DOP,@d6)))
      
                              Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                              mod:Model_Number = job:Model_Number
                              If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                  job:Manufacturer = mod:Manufacturer
                                  If mod:product_Type <> ''
                                      job:fault_code1 = mod:product_type
                                  End!If job:product_Type <> ''
      
                              End!If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      
                              If Upper(imp:WarrantyStatus) = 'IN WARRANTY REPAIR'
                                  job:Warranty_Job = 'YES'
                                  job:Chargeable_Job = 'NO'
                              Else!If Upper(imp:WarrantyStatus) = 'IN WARRANTY REPAIR'
                                  job:Chargeable_Job = 'YES'
                                  job:Warranty_Job = 'NO'
                              End!If Upper(imp:WarrantyStatus) = 'IN WARRANTY REPAIR'
      
                              If dfc:PLocation = ''
                                  job:Workshop = 'NO'
                              Else !If dfc:Location = ''
                                  job:Workshop    = 'YES'
                              End !If dfc:Location = ''
                              
      
                              job:account_number      = dfc:PAccountNumber
                              despatch# = 0
                              access:subtracc.clearkey(sub:account_number_key)
                              sub:account_number = job:account_number
                              if access:subtracc.fetch(sub:account_number_key) = level:benign
                                  access:tradeacc.clearkey(tra:account_number_key) 
                                  tra:account_number = sub:main_account_number
                                  if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                      IF tra:invoice_sub_accounts = 'YES'
                                          job:courier_cost        = sub:courier_cost
                                          job:courier_cost_warranty = sub:courier_cost
                                      Else!IF tra:invoice_sub_accounts = 'YES'
                                          job:courier_cost        = tra:courier_cost
                                          job:courier_cost_warranty = sub:courier_cost
                                      End!IF tra:invoice_sub_accounts = 'YES'
                                      if tra:use_sub_accounts = 'YES'
                                          job:courier = sub:courier_outgoing
                                          job:incoming_courier = sub:courier_incoming
                                          job:exchange_courier = job:courier
                                          job:loan_courier = job:courier
                                          If sub:despatch_paid_jobs <> 'YES' and sub:despatch_invoiced_jobs <> 'YES'
                                              despatch# = 1
                                          End!If sub:despatch_paid_jobs <> 'YES' and sub:despatch_invoiced_jobs <> 'YES'
                                      else!if tra:use_sub_accounts = 'YES'
                                          job:courier = tra:courier_outgoing
                                          job:incoming_courier = tra:courier_incoming
                                          job:exchange_courier = job:courier
                                          job:loan_courier = job:courier
                                          If tra:despatch_paid_jobs <> 'YES' and tra:despatch_invoiced_jobs <> 'YES'
                                              despatch# = 1
                                          End!If tra:despatch_paid_jobs <> 'YES' and tra:despatch_invoiced_jobs <> 'YES'
                                      end!if tra:use_sub_accounts = 'YES'
                                      If tra:invoice_sub_accounts  <> 'YES'
                                          If tra:use_customer_address <> 'YES'
                                              JOB:Postcode            = tra:Postcode
                                              JOB:Company_Name        = tra:Company_Name
                                              JOB:Address_Line1       = tra:Address_Line1
                                              JOB:Address_Line2       = tra:Address_Line2
                                              JOB:Address_Line3       = tra:Address_Line3
                                              JOB:Telephone_Number    = tra:Telephone_Number
                                              JOB:Fax_Number          = tra:Fax_Number
                                          End!If tra:use_customer_address <> 'YES'
                                      Else!If tra:invoice_sub_accounts  <> 'YES'
                                          If sub:use_customer_address <> 'YES'
                                              JOB:Postcode            = sub:Postcode
                                              JOB:Company_Name        = sub:Company_Name
                                              JOB:Address_Line1       = sub:Address_Line1
                                              JOB:Address_Line2       = sub:Address_Line2
                                              JOB:Address_Line3       = sub:Address_Line3
                                              JOB:Telephone_Number    = sub:Telephone_Number
                                              JOB:Fax_Number          = sub:Fax_Number
                                          End!If sub:use_customer_address <> 'YES'
                                      End!If tra:invoice_sub_accounts  <> 'YES'
                                      If tra:use_sub_accounts <> 'YES'
                                          If tra:use_delivery_address = 'YES'
                                              job:postcode_delivery   = tra:postcode
                                              job:company_name_delivery   = tra:company_name
                                              job:address_line1_delivery  = tra:address_line1
                                              job:address_line2_delivery  = tra:address_line2
                                              job:address_line3_delivery  = tra:address_line3
                                              job:telephone_delivery      = tra:telephone_number
                                          End !If tra:use_delivery_address = 'YES'
                                          If tra:use_collection_address = 'YES'
                                              job:postcode_collection   = tra:postcode
                                              job:company_name_collection   = tra:company_name
                                              job:address_line1_collection  = tra:address_line1
                                              job:address_line2_collection  = tra:address_line2
                                              job:address_line3_collection  = tra:address_line3
                                              job:telephone_collection      = tra:telephone_number
                                          End !If tra:use_delivery_address = 'YES'
                                      Else
                                          If sub:use_delivery_address = 'YES'
                                              job:postcode_delivery      = sub:postcode
                                              job:company_name_delivery  = sub:company_name
                                              job:address_line1_delivery = sub:address_line1
                                              job:address_line2_delivery = sub:address_line2
                                              job:address_line3_delivery = sub:address_line3
                                              job:telephone_delivery     = sub:telephone_number
                                          End
                                          If sub:use_collection_address = 'YES'
                                              job:postcode_collection      = sub:postcode
                                              job:company_name_collection  = sub:company_name
                                              job:address_line1_collection = sub:address_line1
                                              job:address_line2_collection = sub:address_line2
                                              job:address_line3_collection = sub:address_line3
                                              job:telephone_collection     = sub:telephone_number
                                          End
                                      End
      
                                  end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                              end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                              job:transit_type    = dfc:PTransitType
      
                              access:trantype.clearkey(trt:transit_type_key)
                              trt:transit_type = job:transit_type
                              if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                                  IF trt:InWorkshop = 'YES'
                                      job:workshop = 'YES'
                                  End!IF trt:InWorkshop = 'YES'
                                  GetStatus(Sub(trt:initial_status,1,3),InsertRecord,'JOB')
                                  GetStatus(Sub(trt:Exchangestatus,1,3),InsertRecord,'EXC')
                                  GetStatus(Sub(trt:Loanstatus,1,3),InsertRecord,'LOA')
                              end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
      
                              access:users.clearkey(use:password_key)
                              use:password    =glo:password
                              If access:users.fetch(use:password_key) = Level:Benign
                                  job:who_booked                    = use:user_code
                              End
                              If job:warranty_job = 'YES'
                                  ! Start Change BE028 BE(26/03/04)
                                  !job:edi = Set_Edi(job:manufacturer)
                                  job:edi = PendingJob(job:manufacturer)
                                  ! End Change BE028 BE(26/03/04)
                              end
      
                              access:users.clearkey(use:password_key)
                              use:password    =glo:password
                              access:users.fetch(use:password_key)
                              job:who_booked  = use:user_code
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                                  aud:notes         = 'UNIT DETAILS: ' & Clip(job:Manufacturer) & ' - ' & Clip(job:Model_Number) &|
                                                      '<13,10>I.M.E.I. NO: ' & Clip(job:ESN) & |
                                                      '<13,10,13,10>CHARGEABLE JOB: ' & Clip(job:Chargeable_Job) &|
                                                      '<13,10>WARRANTY JOB: ' & Clip(job:Warranty_Job)
                                  aud:ref_number    = job:ref_number
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  aud:action        = 'CRC IMPORT (PANASONIC)'
                                  if access:audit.insert()
                                      access:audit.cancelautoinc()
                                  end
                              end!if access:audit.primerecord() = level:benign
      
      
                              if access:jobs.tryinsert()
                                  access:jobs.cancelautoinc()
                              Else
                                  If Access:JOBSE.PrimeRecord() = Level:Benign
                                      jobe:RefNumber  = job:Ref_Number
                                      If job:Workshop = 'YES'
                                          jobe:InWorkshopDate = Today()
                                          jobe:InWorkshopTime = Clock()
                                      End !If job:Workshop = 'YES'
                                      If Access:JOBSE.TryInsert() = Level:Benign
                                          !Insert Successful
                                      Else !If Access:JOBSE.TryInsert() = Level:Benign
                                          !Insert Failed
                                      End !If Access:JOBSE.TryInsert() = Level:Benign
                                  End !If Access:JOBSE.PrimeRecord() = Level:Benign
      
                                  If func:PrintLabel
                                      glo:Select1 = job:Ref_Number
                                      Thermal_Labels('')
                                      glo:Select1 = ''
                                  End !If func:PrintLabel
                                   If func:PrintJobCard
                                      glo:Select1 = job:Ref_Number
                                      Job_Card()
                                      glo:Select1 = ''
                                  End !If func:PrintLabel
                                  If CountBouncer(job:Ref_Number,job:Date_Booked,job:Esn)
                                      bounce:RefNumber = job:Ref_Number
                                      Add(BouncerList)
                                  End!If CountBouncer(job:Ref_Number,job:Date_Booked,job:Esn)
      
                                  PrintLetter(job:Ref_Number,tmp:LetterName)
      
                              End!if access:jobs.tryinsert()
                          end!if access:jobs.primerecord() = level:benign
                      End!Loop
                  End!If Error()
                  Setcursor()
                  Close(IMPORTFILE)
      
                  Case MessageEx('Import Completed.','ServiceBase 2000',|
                                 'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  If Records(BouncerList)
                      Case MessageEx('One or more of the imported Jobs are Bouncers.'&|
                        '<13,10>'&|
                        '<13,10>Do you wish to print the Bouncer History Reports for these jobs?','ServiceBase 2000',|
                                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              Sort(BouncerList,bounce:RefNumber)
                              Loop x# = 1 To Records(BouncerList)
                                  Get(BouncerList,x#)
                                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                                  job:Ref_Number  = bounce:RefNUmber
                                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                      !Found
                                      glo:Select2 = job:esn
                                      Bouncer_History
                                      glo:Select2 = ''
                                  Else! If Access:.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                  
                              End!Loop x# = 1 To Records(BouncerList)
                          Of 2 ! &No Button
                      End!Case MessageEx
                  End
              End!If error# = 0
      
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'CRCImportPanasonic')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

