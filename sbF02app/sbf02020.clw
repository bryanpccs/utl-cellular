

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF02020.INC'),ONCE        !Local module procedure declarations
                     END


CRCImportGeneric PROCEDURE (func:PrintLabel,func:PrintJobCard) !Generated from procedure template - Window

tmp:FileName         STRING(255)
tmp:LetterName       STRING(30)
tmp:AccountNumber    STRING(30)
tmp:TransitType      STRING(30)
ModelQueue           QUEUE,PRE(MDQ)
Code                 STRING(30)
Model                STRING(30)
                     END
BouncerList          QUEUE,PRE(bounce)
RefNumber            LONG
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Jobs Import (Generic)'),AT(,,247,124),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,240,88),USE(?Sheet1),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Select the file you wish to import'),AT(8,8,140,12),USE(?Prompt1),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Import File Name'),AT(9,24),USE(?tmp:FileName:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,24,140,10),USE(filename3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Import File Name'),TIP('Import File Name'),UPR
                           BUTTON,AT(228,24,10,10),USE(?LookupFileName),SKIP,ICON('List3.ico')
                           PROMPT('Letter Name'),AT(9,41),USE(?tmp:LetterName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,40,140,10),USE(tmp:LetterName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Letter Name'),TIP('Letter Name'),UPR
                           BUTTON,AT(228,41,10,10),USE(?LookupLetter),SKIP,ICON('List3.ico')
                           PROMPT('Account Number'),AT(8,56),USE(?Prompt4)
                           PROMPT('Transit Type'),AT(8,72),USE(?Prompt5)
                           ENTRY(@s30),AT(84,72,140,10),USE(tmp:TransitType)
                           BUTTON('...'),AT(228,72,10,10),USE(?CallLookup:2),LEFT,ICON('list3.ico')
                           PROMPT(''),AT(85,81),USE(?Prompt6)
                           ENTRY(@s30),AT(84,56,140,10),USE(tmp:AccountNumber)
                           BUTTON('...'),AT(228,56,10,10),USE(?CallLookup),LEFT,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,96,240,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Import Jobs'),AT(128,100,56,16),USE(?ImportButton),LEFT,ICON('DISK.GIF')
                       BUTTON('Cancel'),AT(184,100,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup4          SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ImportFile      FILE,DRIVER('BASIC'),PRE(imp),NAME(filename3),CREATE,BINDABLE,THREAD
Record              Record
OrderNumber             String(30)
ModelNumber             String(30)
ESN                     String(30)
FaultDescription        String(255)
WarrantyStatus          String(30)
                    End
                End

LupFile      FILE,DRIVER('BASIC'),PRE(lup),NAME(filename9),CREATE,BINDABLE,THREAD
Record              Record
Ext_Code                String(30)
SB_Code                 String(30)
                    END
                  END

!Save Entry Fields Incase Of Lookup
look:tmp:TransitType                Like(tmp:TransitType)
look:tmp:AccountNumber                Like(tmp:AccountNumber)
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:FileName:Prompt{prop:FontColor} = -1
    ?tmp:FileName:Prompt{prop:Color} = 15066597
    If ?filename3{prop:ReadOnly} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 15066597
    Elsif ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 8454143
    Else ! If ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 16777215
    End ! If ?filename3{prop:Req} = True
    ?filename3{prop:Trn} = 0
    ?filename3{prop:FontStyle} = font:Bold
    ?tmp:LetterName:Prompt{prop:FontColor} = -1
    ?tmp:LetterName:Prompt{prop:Color} = 15066597
    If ?tmp:LetterName{prop:ReadOnly} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 15066597
    Elsif ?tmp:LetterName{prop:Req} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 8454143
    Else ! If ?tmp:LetterName{prop:Req} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 16777215
    End ! If ?tmp:LetterName{prop:Req} = True
    ?tmp:LetterName{prop:Trn} = 0
    ?tmp:LetterName{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?tmp:TransitType{prop:ReadOnly} = True
        ?tmp:TransitType{prop:FontColor} = 65793
        ?tmp:TransitType{prop:Color} = 15066597
    Elsif ?tmp:TransitType{prop:Req} = True
        ?tmp:TransitType{prop:FontColor} = 65793
        ?tmp:TransitType{prop:Color} = 8454143
    Else ! If ?tmp:TransitType{prop:Req} = True
        ?tmp:TransitType{prop:FontColor} = 65793
        ?tmp:TransitType{prop:Color} = 16777215
    End ! If ?tmp:TransitType{prop:Req} = True
    ?tmp:TransitType{prop:Trn} = 0
    ?tmp:TransitType{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    If ?tmp:AccountNumber{prop:ReadOnly} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 15066597
    Elsif ?tmp:AccountNumber{prop:Req} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 8454143
    Else ! If ?tmp:AccountNumber{prop:Req} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 16777215
    End ! If ?tmp:AccountNumber{prop:Req} = True
    ?tmp:AccountNumber{prop:Trn} = 0
    ?tmp:AccountNumber{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CRCImportGeneric',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:FileName',tmp:FileName,'CRCImportGeneric',1)
    SolaceViewVars('tmp:LetterName',tmp:LetterName,'CRCImportGeneric',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'CRCImportGeneric',1)
    SolaceViewVars('tmp:TransitType',tmp:TransitType,'CRCImportGeneric',1)
    SolaceViewVars('ModelQueue:Code',ModelQueue:Code,'CRCImportGeneric',1)
    SolaceViewVars('ModelQueue:Model',ModelQueue:Model,'CRCImportGeneric',1)
    SolaceViewVars('BouncerList:RefNumber',BouncerList:RefNumber,'CRCImportGeneric',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:FileName:Prompt;  SolaceCtrlName = '?tmp:FileName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?filename3;  SolaceCtrlName = '?filename3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFileName;  SolaceCtrlName = '?LookupFileName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LetterName:Prompt;  SolaceCtrlName = '?tmp:LetterName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LetterName;  SolaceCtrlName = '?tmp:LetterName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLetter;  SolaceCtrlName = '?LookupLetter';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitType;  SolaceCtrlName = '?tmp:TransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup:2;  SolaceCtrlName = '?CallLookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountNumber;  SolaceCtrlName = '?tmp:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup;  SolaceCtrlName = '?CallLookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ImportButton;  SolaceCtrlName = '?ImportButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CRCImportGeneric')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'CRCImportGeneric')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:JOBS2_ALIAS.Open
  Relate:LETTERS.Open
  Relate:MERGELET.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:MODELNUM.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  ! Initialise Default Import Fields
  tmp:AccountNumber = GETINI('GENERIC','AccountNumber','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  tmp:TransitType = GETINI('GENERIC','TransitType','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  
  filename3 = ''
  
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FileLookup4.Init
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:LongName)
  FileLookup4.SetMask('CRC File','*.CSV')
  FileLookup4.WindowTitle='C.R.C. Jobs Import File'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:JOBS2_ALIAS.Close
    Relate:LETTERS.Close
    Relate:MERGELET.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'CRCImportGeneric',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Transit_Types
      Browse_Sub_Accounts
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFileName
      ThisWindow.Update
      filename3 = Upper(FileLookup4.Ask(1)  )
      DISPLAY
    OF ?LookupLetter
      ThisWindow.Update
      tmp:LetterName = BrowseAllLetters()
      ThisWindow.Reset
    OF ?tmp:TransitType
      IF tmp:TransitType OR ?tmp:TransitType{Prop:Req}
        trt:Transit_Type = tmp:TransitType
        !Save Lookup Field Incase Of error
        look:tmp:TransitType        = tmp:TransitType
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:TransitType = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            tmp:TransitType = look:tmp:TransitType
            SELECT(?tmp:TransitType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update
      trt:Transit_Type = tmp:TransitType
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        tmp:TransitType = trt:Transit_Type
      END
      ThisWindow.Reset(1)
    OF ?tmp:AccountNumber
      IF tmp:AccountNumber OR ?tmp:AccountNumber{Prop:Req}
        sub:Account_Number = tmp:AccountNumber
        !Save Lookup Field Incase Of error
        look:tmp:AccountNumber        = tmp:AccountNumber
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:AccountNumber = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            tmp:AccountNumber = look:tmp:AccountNumber
            SELECT(?tmp:AccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      sub:Account_Number = tmp:AccountNumber
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        tmp:AccountNumber = sub:Account_Number
      END
      ThisWindow.Reset(1)
    OF ?ImportButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
      CASE MessageEx('Are you sure you want to import the selected file?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,|
                     12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
          OF 1 ! &Yes Button
              CLEAR(BouncerList)
              FREE(BouncerList)
              error# = 0
      
              IF (error# = 0) THEN
                  IF (tmp:AccountNumber = ''  OR tmp:TransitType = '') THEN
                      MessageEx('You have not setup any import defaults.','ServiceBase 2000',|
                                     'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,|
                                     12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                      error# = 1
                  END
              END
      
              IF (error# = 0) THEN
                  filename9 = CLIP(Path())&'\Vodaxref.csv'
                  OPEN(lupfile)
                  IF (ERROR()) THEN
                      MessageEx('Cannot open the Lookup file.','ServiceBase 2000',|
                              'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,|
                              CHARSET:ANSI,12632256,'',0,|
                              beep:systemhand,msgex:samewidths,84,26,0)
                      error# = 1
                  ELSE
                      ! Intialise Model lookup Table
                      FREE(ModelQueue)
                      SET(lupfile)
                      SETCURSOR(CURSOR:WAIT)
                      LOOP
                          NEXT(lupFile)
                          IF (ERRORCODE()) THEN
                              BREAK
                          END
                          CLEAR(ModelQueue)
                          MDQ:Code = UPPER(lup:ext_code)
                          MDQ:Model = UPPER(lup:sb_code)
                          ADD(ModelQueue)
                      END
                      SORT(ModelQueue, MDQ:Code)
                      SETCURSOR()
                  END
                  CLOSE(lupfile)
              END
      
              IF (error# = 0) THEN
      
                  OPEN(IMPORTFILE)
                  IF (Error()) THEN
                      MessageEx('Cannot open the import file.'&|
                                  '<13,10>'&|
                                  '<13,10>(' & Error() & ')','ServiceBase 2000',|
                                  'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,|
                                  12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                  ELSE
                      access:users.clearkey(use:password_key)
                      use:password = glo:password
                      access:users.fetch(use:password_key)
                      usercode" = use:user_code
      
                      SETCURSOR(cursor:wait)
                      SET(IMPORTFILE,0)
                      LOOP
                          NEXT(IMPORTFILE)
      
                          IF (Error()) THEN
                              BREAK
                          END
      
                          IF (access:jobs.primerecord() = level:benign) THEN
                              !call the status routine
                              GetStatus(0,1,'JOB')
      
                              job:Order_Number    = Clip(Upper(imp:OrderNumber))
                              job:esn             = Clip(Upper(imp:ESN))
                              !job:Model_Number    = Clip(Upper(imp:ModelNumber))
      
                              
                              MDQ:Code = Clip(Upper(imp:ModelNumber))
                              GET(ModelQueue, MDQ:Code)
                              IF (ERRORCODE()) THEN
                                  job:Model_Number = ''
                              ELSE
                                  job:Model_Number = MDQ:Model
                              END
      
                              Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                              mod:Model_Number = job:Model_Number
                              IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign) THEN
                                 job:Manufacturer = mod:Manufacturer
                                 IF (mod:product_Type <> '') THEN
                                    job:fault_code1 = mod:product_type
                                 END
                                 IF (mod:Unit_Type <> '') THEN
                                    job:Unit_Type = mod:Unit_Type
                                 END
                              ELSE
                                 QueryModelNumber(job:Model_Number)
                                 job:Manufacturer = mod:Manufacturer
                                 IF (mod:product_Type <> '') THEN
                                    job:fault_code1 = mod:product_type
                                 END
                                 IF (mod:Unit_Type <> '') THEN
                                    job:Unit_Type = mod:Unit_Type
                                 END
                              END
      
                              IF (CLIP(UPPER(imp:WarrantyStatus)) = 'PT STOCK WARRANTY') THEN
                                  job:Warranty_Job = 'YES'
                                  job:Chargeable_Job = 'NO'
                              ELSIF (CLIP(UPPER(imp:WarrantyStatus)) = 'PT STOCK NON-WARRANTY REPAIR') THEN
                                  job:Warranty_Job = 'NO'
                                  job:Chargeable_Job = 'YES'
                              ELSE
                                  job:Warranty_Job = 'NO'
                                  job:Chargeable_Job = 'NO'
                              END
      
                              !job:Warranty_Charge_Type = CLIP(UPPER(imp:WarrantyStatus))
      
                              job:account_number      = tmp:AccountNumber
                              despatch# = 0
                              access:subtracc.clearkey(sub:account_number_key)
                              sub:account_number = job:account_number
                              IF (access:subtracc.fetch(sub:account_number_key) = level:benign) THEN
                                  access:tradeacc.clearkey(tra:account_number_key) 
                                  tra:account_number = sub:main_account_number
                                  IF (access:tradeacc.fetch(tra:account_number_key) = level:benign) THEN
                                      IF (tra:invoice_sub_accounts = 'YES') THEN
                                          job:courier_cost        = sub:courier_cost
                                          job:courier_cost_warranty = sub:courier_cost
                                      ELSE
                                          job:courier_cost        = tra:courier_cost
                                          job:courier_cost_warranty = sub:courier_cost
                                      END
                                      IF (tra:use_sub_accounts = 'YES') THEN
                                          job:courier = sub:courier_outgoing
                                          job:incoming_courier = sub:courier_incoming
                                          job:exchange_courier = job:courier
                                          job:loan_courier = job:courier
                                          IF (sub:despatch_paid_jobs <> 'YES' AND sub:despatch_invoiced_jobs <> 'YES') THEN
                                              despatch# = 1
                                          END
                                      ELSE
                                          job:courier = tra:courier_outgoing
                                          job:incoming_courier = tra:courier_incoming
                                          job:exchange_courier = job:courier
                                          job:loan_courier = job:courier
                                          IF (tra:despatch_paid_jobs <> 'YES' AND tra:despatch_invoiced_jobs <> 'YES') THEN
                                              despatch# = 1
                                          END
                                      END
                                      IF (tra:invoice_sub_accounts  <> 'YES') THEN
                                          IF (tra:use_customer_address <> 'YES') THEN
                                              JOB:Postcode            = tra:Postcode
                                              JOB:Company_Name        = tra:Company_Name
                                              JOB:Address_Line1       = tra:Address_Line1
                                              JOB:Address_Line2       = tra:Address_Line2
                                              JOB:Address_Line3       = tra:Address_Line3
                                              JOB:Telephone_Number    = tra:Telephone_Number
                                              JOB:Fax_Number          = tra:Fax_Number
                                          END
                                      ELSE
                                          IF (sub:use_customer_address <> 'YES') THEN
                                              JOB:Postcode            = sub:Postcode
                                              JOB:Company_Name        = sub:Company_Name
                                              JOB:Address_Line1       = sub:Address_Line1
                                              JOB:Address_Line2       = sub:Address_Line2
                                              JOB:Address_Line3       = sub:Address_Line3
                                              JOB:Telephone_Number    = sub:Telephone_Number
                                              JOB:Fax_Number          = sub:Fax_Number
                                          END
                                      END
                                      IF (tra:use_sub_accounts <> 'YES') THEN
                                          IF (tra:use_delivery_address = 'YES') THEN
                                              job:postcode_delivery   = tra:postcode
                                              job:company_name_delivery   = tra:company_name
                                              job:address_line1_delivery  = tra:address_line1
                                              job:address_line2_delivery  = tra:address_line2
                                              job:address_line3_delivery  = tra:address_line3
                                              job:telephone_delivery      = tra:telephone_number
                                          END
                                          IF (tra:use_collection_address = 'YES') THEN
                                              job:postcode_collection   = tra:postcode
                                              job:company_name_collection   = tra:company_name
                                              job:address_line1_collection  = tra:address_line1
                                              job:address_line2_collection  = tra:address_line2
                                              job:address_line3_collection  = tra:address_line3
                                              job:telephone_collection      = tra:telephone_number
                                          END
                                      ELSE
                                          IF (sub:use_delivery_address = 'YES') THEN
                                              job:postcode_delivery      = sub:postcode
                                              job:company_name_delivery  = sub:company_name
                                              job:address_line1_delivery = sub:address_line1
                                              job:address_line2_delivery = sub:address_line2
                                              job:address_line3_delivery = sub:address_line3
                                              job:telephone_delivery     = sub:telephone_number
                                          END
                                          IF (sub:use_collection_address = 'YES') THEN
                                              job:postcode_collection      = sub:postcode
                                              job:company_name_collection  = sub:company_name
                                              job:address_line1_collection = sub:address_line1
                                              job:address_line2_collection = sub:address_line2
                                              job:address_line3_collection = sub:address_line3
                                              job:telephone_collection     = sub:telephone_number
                                          END
                                      END
                                  END
                              END
      
                              job:Transit_Type = tmp:TransitType
      
                              access:trantype.clearkey(trt:transit_type_key)
                              trt:transit_type = job:transit_type
                              IF (access:trantype.tryfetch(trt:transit_type_key) = Level:Benign) THEN
                                  !IF trt:InWorkshop = 'YES'
                                  !    job:workshop = 'YES'
                                  !End!IF trt:InWorkshop = 'YES'
                                  job:workshop = trt:InWorkshop
                                  job:location = trt:internallocation
                                  job:turnaround_time = trt:initial_priority
                                  GetStatus(Sub(trt:initial_status,1,3),InsertRecord,'JOB')
                                  GetStatus(Sub(trt:Exchangestatus,1,3),InsertRecord,'EXC')
                                  GetStatus(Sub(trt:Loanstatus,1,3),InsertRecord,'LOA')
                              END
      
                              job:who_booked = usercode"
                              IF (job:Warranty_Job = 'YES') THEN
                                  job:edi = PendingJob(job:manufacturer)
                              END
      
                              IF (Access:JOBNOTES.PrimeRecord() = Level:Benign) THEN
                                  jbn:RefNumber  = job:Ref_Number
                                  ! Start Change 3414 BE(28/01/04)
                                  !jbn:Engineers_Notes   = CLIP(UPPER(imp:FaultDescription))
                                  jbn:Fault_Description = CLIP(UPPER(imp:FaultDescription))
                                  ! End Change 3414 BE(28/01/04)
                                  IF (Access:JOBNOTES.Insert() <> Level:Benign) THEN
                                      access:JOBNOTES.cancelautoinc()
                                  END
                              END
      
                              IF (Access:JOBSE.PrimeRecord() = Level:Benign) THEN
                                  jobe:RefNumber  = job:Ref_Number
      
                                  IF (job:Workshop = 'YES') THEN
                                      jobe:InWorkshopDate = TODAY()
                                      jobe:InWorkshopTime = CLOCK()
                                  END
                                 IF (Access:JOBSE.Insert() <> Level:Benign) THEN
                                     access:JOBSE.cancelautoinc()
                                  END
                              END
      
                              GET(audit,0)
                              IF (access:audit.primerecord() = level:benign) THEN
                                  aud:notes         = 'UNIT DETAILS: ' & Clip(job:Manufacturer) & ' - ' & Clip(job:Model_Number) &|
                                                      '<13,10>I.M.E.I. NO: ' & Clip(job:ESN) & |
                                                      '<13,10,13,10>CHARGEABLE JOB: ' & Clip(job:Chargeable_Job) &|
                                                      '<13,10>WARRANTY JOB: ' & Clip(job:Warranty_Job)
                                  aud:ref_number    = job:ref_number
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  aud:user = usercode"
                                  aud:action        = 'CRC IMPORT (GENERIC)'
                                  IF (access:audit.insert() <> Level:benign) THEN
                                     access:audit.cancelautoinc()
                                  END
                              END
      
                              IF (access:jobs.tryinsert() <> Level:Benign) THEN
                                  access:jobs.cancelautoinc()
                              ELSE
      
                                  PrintLetter(job:Ref_Number,tmp:LetterName)
      
                                  IF (func:PrintLabel) THEN
                                      glo:Select1 = job:Ref_Number
                                      Thermal_Labels('')
                                      glo:Select1 = ''
                                  END
      
                                  IF (func:PrintJobCard) THEN
                                      glo:Select1 = job:Ref_Number
                                      Job_Card()
                                      glo:Select1 = ''
                                  END
      
                                  IF (CountBouncer(job:Ref_Number,job:Date_Booked,job:Esn))
                                      bounce:RefNumber = job:Ref_Number
                                      Add(BouncerList)
                                  END
      
                              END
                          END
                      END !Loop
                  END !If Error()
                  SETCURSOR()
                  CLOSE(IMPORTFILE)
      
                  MessageEx('Import Completed.','ServiceBase 2000',|
                                 'Styles\idea.ico','&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,|
                                 12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
      
                  IF (Records(BouncerList)) THEN
      
                      CASE MessageEx('One or more of the imported jobs are Bouncers.'&|
                                      '<13,10>'&|
                                      '<13,10>Do you wish to print a Bouncer History Report for these jobs?','ServiceBase 2000',|
                                      'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,|
                                      12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                          OF 1 ! &Yes Button
                              SORT(BouncerList,bounce:RefNumber)
                              LOOP x# = 1 TO RECORDS(BouncerList)
                                  GET(BouncerList,x#)
                                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                                  job:Ref_Number  = bounce:RefNUmber
                                  IF (Access:JOBS.fetch(job:Ref_Number_Key) = Level:Benign) THEN
                                      glo:Select2 = job:esn
                                      Bouncer_History
                                      glo:Select2 = ''
                                  END
                              END
                          OF 2 ! &No Button
                      END
                  END
      
                  ! Save Default Import Fields
                  PUTINI('GENERIC','AccountNumber',tmp:AccountNumber,CLIP(PATH()) & '\CRCIMPEXTRA.INI')
                  PUTINI('GENERIC','TransitType',tmp:TransitType,CLIP(PATH()) & '\CRCIMPEXTRA.INI')
      
              END !If error# = 0
      
          OF 2 ! &No Button
      END !Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'CRCImportGeneric')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

