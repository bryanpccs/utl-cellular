

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBF02016.INC'),ONCE        !Local module procedure declarations
                     END


PrintLetter          PROCEDURE  (func:JobNumber,func:LetterName) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'PrintLetter')      !Add Procedure to Log
  end


    Access:LETTERS.ClearKey(let:DescriptionKey)
    let:Description = func:LetterName
    If Access:LETTERS.TryFetch(let:DescriptionKey) = Level:Benign
        !Found
        glo:Select1 = func:JobNumber
        glo:Select2 = func:LetterName
        glo:Preview = False
        Standard_Letter
    Else!If Access:LETTERS.TryFetch(let:DescriptionKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
        Access:MERGELET.ClearKey(mrg:LetterNameKey)
        mrg:LetterName = func:Lettername
        If Access:MERGELET.TryFetch(mrg:LetterNameKey) = Level:Benign
            !Found
            glo:Select1 = func:JobNumber
            glo:Select2 = func:LetterName
            glo:Preview = False
            ViewLetter
        Else!If Access:MERGELET.TryFetch(mrg:LetterNameKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MERGELET.TryFetch(mrg:LetterNameKey) = Level:Benign
    End!If Access:LETTERS.TryFetch(let:DescriptionKey) = Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PrintLetter',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
