

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF02019.INC'),ONCE        !Local module procedure declarations
                     END


CRCImportVodafoneCorporate PROCEDURE (func:PrintLabel,func:PrintJobCard) !Generated from procedure template - Window

tmp:FileName         STRING(255)
BouncerList          QUEUE,PRE(bounce)
RefNumber            LONG
                     END
tmp:ModelNumberError BYTE
tmp:LetterName       STRING(30)
AccountNumberValue   STRING(30)
LocationValue        STRING(30)
TransitTypeValue     STRING(30)
ModelQueue           QUEUE,PRE(MDQ)
Code                 STRING(30)
Model                STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Jobs Import (Vodafone Corporate)'),AT(,,247,108),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,240,72),USE(?Sheet1),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Select the file you wish to import'),AT(8,8,140,12),USE(?Prompt1),TRN,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Import File Name'),AT(9,24),USE(?tmp:FileName:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,24,140,10),USE(filename3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Import File Name'),TIP('Import File Name'),UPR
                           BUTTON,AT(228,24,10,10),USE(?LookupFileName),SKIP,ICON('List3.ico')
                           PROMPT('Letter Name'),AT(8,40),USE(?tmp:LetterName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,40,140,10),USE(tmp:LetterName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Letter Name'),TIP('Letter Name'),UPR
                           BUTTON,AT(228,40,10,10),USE(?LookupLetter),SKIP,ICON('List3.ico')
                           STRING('Account Number'),AT(8,56),USE(?String1),TRN
                           ENTRY(@s30),AT(84,56,140,10),USE(AccountNumberValue),FONT(,,,FONT:bold)
                           BUTTON,AT(228,56,10,10),USE(?CallLookup),ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,80,240,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Import Jobs'),AT(128,84,56,16),USE(?ImportJobs),LEFT,ICON('DISK.GIF')
                       BUTTON('Cancel'),AT(184,84,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup4          SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ImportFile      FILE,DRIVER('BASIC'),PRE(imp),NAME(filename3),CREATE,BINDABLE,THREAD
Record              Record
OrderNumber         String(30)
SeqCode             String(30)
ServiceType         String(30)
CallCentreAgent     String(30)
TelephoneNumber     String(30)
ESN                 String(30)
ModelNumber         String(30)
FaultDescription    String(30)
PaymentFlag         String(30)
CorporateID         String(30)
Surname             String(30)
CompanyNameCollect  String(30)
AddressLine1Collect String(30)
AddressLine2Collect String(30)
AddressLine3Collect String(30)
AddressLine4Collect String(30)
PostCodeCollect     String(30)
CountryCode         String(30)
TelephoneCollect    String(30)
SurnameDeliver      String(30)
CompanyNameDeliver  String(30)
AddressLine1Deliver String(30)
AddressLine2Deliver String(30)
AddressLine3Deliver String(30)
AddressLine4Deliver String(30)
PostCodeDeliver     String(30)
CountryCode2        String(30)
TelephoneDeliver    String(30)
PurchaseDate        String(30)
! Start Change 3652 BE(09/12/03)
DateRequired        String(30)
TimeRequired        String(30)
Filler1             String(30)
! End Change 3652 BE(09/12/03)
                    End
                End

ExportFile      FILE,DRIVER('BASIC'),PRE(exp),NAME(filename4),CREATE,BINDABLE,THREAD
Record              Record
refNumber         String(30)
CDelivery_Name    String(30)
CAddress1         String(255)
CAddress2         String(255)
CAddress3         String(255)
CPostcode         String(30)
CTelephone        String(30)
COrder            String(30)
! Start Change 3156 BE(11/11/03)
CSurname            String(30)
! End Change 3156 BE(11/11/03)
                    END
                End

LupFile      FILE,DRIVER('BASIC'),PRE(lup),NAME(filename9),CREATE,BINDABLE,THREAD
Record              Record
Odd_Code             String(30)
Real_Code            String(30)
                    END
                  END

    MAP
GetTradeFault       PROCEDURE(LONG, STRING, STRING), STRING
    END
!Save Entry Fields Incase Of Lookup
look:AccountNumberValue                Like(AccountNumberValue)
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:FileName:Prompt{prop:FontColor} = -1
    ?tmp:FileName:Prompt{prop:Color} = 15066597
    If ?filename3{prop:ReadOnly} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 15066597
    Elsif ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 8454143
    Else ! If ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 16777215
    End ! If ?filename3{prop:Req} = True
    ?filename3{prop:Trn} = 0
    ?filename3{prop:FontStyle} = font:Bold
    ?tmp:LetterName:Prompt{prop:FontColor} = -1
    ?tmp:LetterName:Prompt{prop:Color} = 15066597
    If ?tmp:LetterName{prop:ReadOnly} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 15066597
    Elsif ?tmp:LetterName{prop:Req} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 8454143
    Else ! If ?tmp:LetterName{prop:Req} = True
        ?tmp:LetterName{prop:FontColor} = 65793
        ?tmp:LetterName{prop:Color} = 16777215
    End ! If ?tmp:LetterName{prop:Req} = True
    ?tmp:LetterName{prop:Trn} = 0
    ?tmp:LetterName{prop:FontStyle} = font:Bold
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    If ?AccountNumberValue{prop:ReadOnly} = True
        ?AccountNumberValue{prop:FontColor} = 65793
        ?AccountNumberValue{prop:Color} = 15066597
    Elsif ?AccountNumberValue{prop:Req} = True
        ?AccountNumberValue{prop:FontColor} = 65793
        ?AccountNumberValue{prop:Color} = 8454143
    Else ! If ?AccountNumberValue{prop:Req} = True
        ?AccountNumberValue{prop:FontColor} = 65793
        ?AccountNumberValue{prop:Color} = 16777215
    End ! If ?AccountNumberValue{prop:Req} = True
    ?AccountNumberValue{prop:Trn} = 0
    ?AccountNumberValue{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CRCImportVodafoneCorporate',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:FileName',tmp:FileName,'CRCImportVodafoneCorporate',1)
    SolaceViewVars('BouncerList:RefNumber',BouncerList:RefNumber,'CRCImportVodafoneCorporate',1)
    SolaceViewVars('tmp:ModelNumberError',tmp:ModelNumberError,'CRCImportVodafoneCorporate',1)
    SolaceViewVars('tmp:LetterName',tmp:LetterName,'CRCImportVodafoneCorporate',1)
    SolaceViewVars('AccountNumberValue',AccountNumberValue,'CRCImportVodafoneCorporate',1)
    SolaceViewVars('LocationValue',LocationValue,'CRCImportVodafoneCorporate',1)
    SolaceViewVars('TransitTypeValue',TransitTypeValue,'CRCImportVodafoneCorporate',1)
    SolaceViewVars('ModelQueue:Code',ModelQueue:Code,'CRCImportVodafoneCorporate',1)
    SolaceViewVars('ModelQueue:Model',ModelQueue:Model,'CRCImportVodafoneCorporate',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:FileName:Prompt;  SolaceCtrlName = '?tmp:FileName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?filename3;  SolaceCtrlName = '?filename3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFileName;  SolaceCtrlName = '?LookupFileName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LetterName:Prompt;  SolaceCtrlName = '?tmp:LetterName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LetterName;  SolaceCtrlName = '?tmp:LetterName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLetter;  SolaceCtrlName = '?LookupLetter';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AccountNumberValue;  SolaceCtrlName = '?AccountNumberValue';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup;  SolaceCtrlName = '?CallLookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ImportJobs;  SolaceCtrlName = '?ImportJobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CRCImportVodafoneCorporate')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'CRCImportVodafoneCorporate')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFCRC.Open
  Relate:JOBBATCH.Open
  Relate:JOBS2_ALIAS.Open
  Relate:LETTERS.Open
  Relate:MERGELET.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:MODELNUM.UseFile
  Access:JOBSE.UseFile
  Access:TRAFAULT.UseFile
  Access:TRAFAULO.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ! Initialise Default Import Fields
  LocationValue = GETINI('IMPORT_VC','VLocation','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  AccountNumberValue = GETINI('IMPORT_VC','VAccountNumber','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  TransitTypeValue = GETINI('IMPORT_VC','VTransitType','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  Do RecolourWindow
  ! support for CPCS
  FileLookup4.Init
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:LongName)
  FileLookup4.SetMask('CRC File','*.CSV')
  FileLookup4.WindowTitle='C.R.C. Jobs Import File'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFCRC.Close
    Relate:JOBBATCH.Close
    Relate:JOBS2_ALIAS.Close
    Relate:LETTERS.Close
    Relate:MERGELET.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'CRCImportVodafoneCorporate',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Sub_Accounts
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFileName
      ThisWindow.Update
      filename3 = Upper(FileLookup4.Ask(1)  )
      DISPLAY
    OF ?LookupLetter
      ThisWindow.Update
      tmp:LetterName = BrowseAllLetters()
      ThisWindow.Reset
    OF ?AccountNumberValue
      IF AccountNumberValue OR ?AccountNumberValue{Prop:Req}
        sub:Account_Number = AccountNumberValue
        !Save Lookup Field Incase Of error
        look:AccountNumberValue        = AccountNumberValue
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            AccountNumberValue = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            AccountNumberValue = look:AccountNumberValue
            SELECT(?AccountNumberValue)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      sub:Account_Number = AccountNumberValue
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        AccountNumberValue = sub:Account_Number
      END
      ThisWindow.Reset(1)
    OF ?ImportJobs
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportJobs, Accepted)
! Start Change 2649 BE(10/06/03)
! Code copied from CRCImportVodafone  (known as Vodafone ONR)
!

      Case MessageEx('Are you sure you want to import the selected file?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                     beep:systemquestion,msgex:samewidths,84,26,0)
          Of 1 ! &Yes Button
              Clear(BouncerList)
              Free(BouncerList)
              error# = 0

              IF (error# = 0) THEN
                  IF (GETINI('IMPORT_VC','VAccountNumber','',CLIP(PATH()) & '\CRCIMPEXTRA.INI') = '') THEN
                      MessageEx('You have not setup any import defaults.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                     beep:systemhand,msgex:samewidths,84,26,0)
                      error# = 1
                  END
              END

              ! Start Change 3414 BE(05/12/03)
              !IF (error# = 0) THEN
              !    ! Start Change 2649 BE(10/06/03)
              !    !filename9 = CLIP(Path())&'\IBMtoSB_Models.csv'
              !    filename9 = CLIP(Path())&'\Vodaxref.csv'
              !    ! End Change 2649 BE(10/06/03)
              !    open(lupfile)
              !    IF (ERROR()) THEN
              !        MessageEx('Cannot open the Lookup file.','ServiceBase 2000',|
              !                  'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
              !                  beep:systemhand,msgex:samewidths,84,26,0)
              !        error# = 1
              !   END
              !END
              ! End Change 3414 BE(05/12/03)

              ! Start Change 3414 BE(05/12/03)
              IF (error# = 0) THEN
                filename9 = CLIP(Path())&'\Vodaxref.csv'
                OPEN(lupfile)
                IF (ERROR()) THEN
                    MessageEx('Cannot open the Lookup file.','ServiceBase 2000',|
                            'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,|
                            CHARSET:ANSI,12632256,'',0,|
                            beep:systemhand,msgex:samewidths,84,26,0)
                    error# = 1
                ELSE
                    ! Intialise Model lookup Table
                    FREE(ModelQueue)
                    SET(lupfile)
                    SETCURSOR(CURSOR:Wait)
                    LOOP
                        NEXT(lupFile)
                        IF (ERRORCODE()) THEN
                            BREAK
                        END
                        CLEAR(ModelQueue)
                        MDQ:Code = UPPER(lup:odd_code)
                        MDQ:Model = UPPER(lup:real_code)
                        ADD(ModelQueue)
                    END
                    SORT(ModelQueue, MDQ:Code)
                    SETCURSOR()
                END
                CLOSE(lupfile)
              END
              ! End Change 3414 BE(05/12/03)

              IF (error# = 0) THEN
                OPEN(ImportFile)
                IF (Error()) THEN
                    MessageEx('Cannot open the import file.'&|
                                '<13,10>'&'<13,10>(' & Error() & ')','ServiceBase 2000',|
                                'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                beep:systemhand,msgex:samewidths,84,26,0)
                    error# = 1
                END
              END

              ! Start Change 2472 BE(10/04/03)
              batch_number# = 0
              ! End Change 2472 BE(10/04/03)

              IF (error# = 0) THEN
                    ! Filename Format VCddhhnn.csv
                    ! Start BE(16/06/03)
                    ! Filename Format VCddhhmm.csv
                    !nn" = FORMAT(SUB(FORMAT(CLOCK(),@n07), 6, 2), @n02)
                    !filename4 = CLIP(GETINI('IMPORT_VC','VDirectory','',CLIP(PATH()) & '\CRCIMPEXTRA.INI'))& |
                    !             '\VC'&CLIP(FORMAT(DAY(TODAY()),@n2))& |
                    !             CLIP(FORMAT(SUB(FORMAT(CLOCK(),@t1),1,2),@n02))&CLIP(nn")&'.CSV'
                    nn" = FORMAT(CLOCK(),@t01)
                    filename4 = CLIP(GETINI('IMPORT_VC','VDirectory','',CLIP(PATH()) & '\CRCIMPEXTRA.INI'))& |
                                 '\VC'&CLIP(FORMAT(DAY(TODAY()),@n02))& |
                                 CLIP(SUB(nn",1,2))&CLIP(SUB(nn",4,2))&'.CSV'
                    ! End BE(16/06/03)
                    OPEN(ExportFile)
                    IF ERROR()
                        CREATE(ExportFile)
                        OPEN(ExportFile)
                    END

                    !Start Change 2472 BE(10/04/03)
                    GET(jobbatch,0)
                    IF (access:jobbatch.primerecord() = level:benign) THEN
                        IF (access:jobbatch.insert()) THEN
                            access:jobbatch.cancelautoinc()
                        ELSE
                            batch_number# = jbt:batch_number
                        END
                    END
                    !Start Change 2472 BE(10/04/03)

                   ! LocationValue = GETINI('IMPORT_VC','VLocation','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
                    !AccountNumberValue = GETINI('IMPORT_VC','VAccountNumber','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
                    !TransitTypeValue = GETINI('IMPORT_VC','VTransitType','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')

                    Setcursor(cursor:wait)
                    Set(IMPORTFILE,0)
                    Loop
                        Next(IMPORTFILE)
                        If Error()
                            Break
                        End!If Error()
                        if access:jobs.primerecord() = level:benign
                            !call the status routine
                            GetStatus(0,1,'JOB')
                            !Need to add defaults here!
                            job:location = LocationValue

                            ! Start Change 2472 BE(10/04/03)
                            job:Batch_Number = batch_number#
                            ! End  Change 2472 BE(10/04/03)

                            job:Order_Number    = Clip(Upper(imp:OrderNumber))
                            job:esn             = Clip(Upper(imp:ESN))
                            job:Surname                 = Clip(Upper(imp:Surname))
                            !job:Title                   = Clip(Upper(imp:Title))
                            !job:Initial                 = Clip(Upper(imp:Initial))
      
                            job:Company_Name_Delivery   = Clip(Upper(imp:CompanyNameDeliver))
                            job:Address_Line1_Delivery  = Clip(Upper(imp:AddressLine1Deliver))
                            job:Address_Line2_Delivery  = Clip(Upper(imp:AddressLine2Deliver))
                            job:Address_Line3_Delivery  = Clip(Upper(imp:AddressLine3Deliver)) & ' ' & CLIP(Upper(imp:AddressLine4Deliver))
                            job:Postcode_Delivery       = Clip(Upper(imp:PostcodeDeliver))
                            job:Telephone_Delivery      = Clip(Upper(imp:TelephoneDeliver))
                            !job:Company_Name   = Clip(Upper(imp:CCompany))
                            !job:Address_Line1  = Clip(Upper(imp:CAddressLine1))
                            !job:Address_Line2  = Clip(Upper(imp:CAddressLine2))
                            !job:Address_Line3  = Clip(Upper(imp:CAddressLine3))
                            !job:Postcode       = Clip(Upper(imp:CPostcode))
                            !job:Telephone_Number      = Clip(Upper(imp:CTelephoneNumber))

                            ! Start Change BE015 BE(20/08/03)
                            job:Mobile_Number = Clip(Upper(imp:TelephoneNumber))
                            ! End Change BE015 BE(20/08/03)

                            ! Start Change 3414 BE(05/12/03)
                            !Lookup model number!
                            !SET(lupfile,0)
                            !LOOP
                            !  NEXT(lupFile)
                            !  ! Start Change 2472 BE(10/04/03)
                            !  IF Error() THEN
                            !      Break
                            !  END
                            !  ! End Change 2472 BE(10/04/03)
                            !  IF CLIP(UPPER(lup:odd_code)) = CLIP(UPPER(imp:ModelNumber))
                            !    imp:ModelNumber = lup:real_code
                            !    BREAK
                            !  END
                            !END
                            !
                            !job:Model_Number            = Clip(Upper(imp:ModelNumber))
                            ! End Change 3414 BE(05/12/03)

                            job:Company_Name_Collection   = Clip(Upper(imp:CompanyNameCollect))
                            job:Address_Line1_Collection  = Clip(Upper(imp:AddressLine1Collect))
                            job:Address_Line2_Collection  = Clip(Upper(imp:AddressLine2Collect))
                            job:Address_Line3_Collection  = Clip(Upper(imp:AddressLine3Collect)) & ' ' & Clip(Upper(imp:AddressLine4Collect))
                            job:Postcode_Collection       = Clip(Upper(imp:PostcodeCollect))
                            job:Telephone_Collection      = Clip(Upper(imp:TelephoneCollect))

                            ! Start Change 2649 BE(10/06/03)
                            !job:DOP                       = Clip(Upper(Deformat(imp:PurchaseDate,@d6)))
                            ! Start Change BE015 BE(20/08/03)
                            !job:DOP                       = Clip(Upper(Deformat(imp:PurchaseDate,@d12)))
                            IF (imp:PurchaseDate = '00000000') OR (imp:PurchaseDate = '0') OR (imp:PurchaseDate = '') THEN
                                job:DOP = ''
                            ELSE
                                job:DOP = Clip(Upper(Deformat(imp:PurchaseDate,@d12)))
                            END
                            ! End Change BE015 BE(20/08/03)
                            ! End Change 2649 BE(10/06/03)

                            ! Start Change 3414 BE(05/12/03)
                            !tmp:ModelNumberError = 1
                            !LOOP UNTIL tmp:ModelNumberError = 0
                            !  Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                            !  mod:Model_Number = job:Model_Number
                            !  If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                            !     job:Manufacturer = mod:Manufacturer
                            !     If mod:product_Type <> ''
                            !        job:fault_code1 = mod:product_type
                            !     End!If mod:product_Type <> ''
                            !     If mod:Unit_Type <> ''
                            !        job:Unit_Type = mod:Unit_Type
                            !     End !if mod:unit_type <> ''
                            !     tmp:ModelNumberError = 0
                            !  Else
                            !     QueryModelNumber(job:Model_Number)
                            !     job:Manufacturer = mod:Manufacturer
                            !     If mod:product_Type <> ''
                            !        job:fault_code1 = mod:product_type
                            !     End!If mod:product_Type <> ''
                            !     If mod:Unit_Type <> ''
                            !        job:Unit_Type = mod:Unit_Type
                            !     End !if mod:unit_type <> ''
                            !     tmp:ModelNumberError = 0
                            !  End !If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                            !END !LOOP

                            MDQ:Code = Clip(Upper(imp:ModelNumber))
                            GET(ModelQueue, MDQ:Code)
                            IF (ERRORCODE()) THEN
                                job:Model_Number = ''
                            ELSE
                                job:Model_Number = MDQ:Model
                            END

                            Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                            mod:Model_Number = job:Model_Number
                            IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign) THEN
                                job:Manufacturer = mod:Manufacturer
                                IF (mod:product_Type <> '') THEN
                                    job:fault_code1 = mod:product_type
                                END
                                IF (mod:Unit_Type <> '') THEN
                                    job:Unit_Type = mod:Unit_Type
                                END
                            ELSE
                                QueryModelNumber(job:Model_Number)
                                job:Manufacturer = mod:Manufacturer
                                IF (mod:product_Type <> '') THEN
                                    job:fault_code1 = mod:product_type
                                END
                                IF (mod:Unit_Type <> '') THEN
                                    job:Unit_Type = mod:Unit_Type
                                END
                            END
                            ! End Change 3414 BE(05/12/03)

    !                       If Upper(imp:WarrantyStatus) = 'IN WARRANTY REPAIR'
                            job:Warranty_Job = 'YES'
                            job:Chargeable_Job = 'NO'
    !                       Else!If Upper(imp:WarrantyStatus) = 'IN WARRANTY REPAIR'
     !                          job:Chargeable_Job = 'YES'
    !                           job:Warranty_Job = 'NO'
    !                       End!If Upper(imp:WarrantyStatus) = 'IN WARRANTY REPAIR'
      
                            If LocationValue = ''
                                job:Workshop = 'NO'
                            Else !If dfc:Location = ''
                                job:Workshop    = 'YES'
                            End !If dfc:Location = ''
                              
      
                            job:account_number = AccountNumberValue
                            despatch# = 0
                            access:subtracc.clearkey(sub:account_number_key)
                            sub:account_number = job:account_number
                            if access:subtracc.fetch(sub:account_number_key) = level:benign
                                access:tradeacc.clearkey(tra:account_number_key)
                                tra:account_number = sub:main_account_number
                                if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                    IF tra:invoice_sub_accounts = 'YES'
                                        job:courier_cost        = sub:courier_cost
                                        job:courier_cost_warranty = sub:courier_cost
                                    Else!IF tra:invoice_sub_accounts = 'YES'
                                        job:courier_cost        = tra:courier_cost
                                        job:courier_cost_warranty = sub:courier_cost
                                    End!IF tra:invoice_sub_accounts = 'YES'
                                    if tra:use_sub_accounts = 'YES'
                                        job:courier = sub:courier_outgoing
                                        job:incoming_courier = sub:courier_incoming
                                        job:exchange_courier = job:courier
                                        job:loan_courier = job:courier
                                        If sub:despatch_paid_jobs <> 'YES' and sub:despatch_invoiced_jobs <> 'YES'
                                            despatch# = 1
                                        End!If sub:despatch_paid_jobs <> 'YES' and sub:despatch_invoiced_jobs <> 'YES'
                                    else!if tra:use_sub_accounts = 'YES'
                                        job:courier = tra:courier_outgoing
                                        job:incoming_courier = tra:courier_incoming
                                        job:exchange_courier = job:courier
                                        job:loan_courier = job:courier
                                        If tra:despatch_paid_jobs <> 'YES' and tra:despatch_invoiced_jobs <> 'YES'
                                            despatch# = 1
                                        End!If tra:despatch_paid_jobs <> 'YES' and tra:despatch_invoiced_jobs <> 'YES'
                                    end!if tra:use_sub_accounts = 'YES'
                                    If tra:invoice_sub_accounts  <> 'YES'
                                        If tra:use_customer_address <> 'YES'
                                            JOB:Postcode            = tra:Postcode
                                            JOB:Company_Name        = tra:Company_Name
                                            JOB:Address_Line1       = tra:Address_Line1
                                            JOB:Address_Line2       = tra:Address_Line2
                                            JOB:Address_Line3       = tra:Address_Line3
                                            JOB:Telephone_Number    = tra:Telephone_Number
                                            JOB:Fax_Number          = tra:Fax_Number
                                        End!If tra:use_customer_address <> 'YES'
                                    Else!If tra:invoice_sub_accounts  <> 'YES'
                                        If sub:use_customer_address <> 'YES'
                                            JOB:Postcode            = sub:Postcode
                                            JOB:Company_Name        = sub:Company_Name
                                            JOB:Address_Line1       = sub:Address_Line1
                                            JOB:Address_Line2       = sub:Address_Line2
                                            JOB:Address_Line3       = sub:Address_Line3
                                            JOB:Telephone_Number    = sub:Telephone_Number
                                            JOB:Fax_Number          = sub:Fax_Number
                                        End!If sub:use_customer_address <> 'YES'
                                    End!If tra:invoice_sub_accounts  <> 'YES'
                                    If tra:use_sub_accounts <> 'YES'
                                        If tra:use_delivery_address = 'YES'
                                            job:postcode_delivery   = tra:postcode
                                            job:company_name_delivery   = tra:company_name
                                            job:address_line1_delivery  = tra:address_line1
                                            job:address_line2_delivery  = tra:address_line2
                                            job:address_line3_delivery  = tra:address_line3
                                            job:telephone_delivery      = tra:telephone_number
                                        End !If tra:use_delivery_address = 'YES'
                                        If tra:use_collection_address = 'YES'
                                            job:postcode_collection   = tra:postcode
                                            job:company_name_collection   = tra:company_name
                                            job:address_line1_collection  = tra:address_line1
                                            job:address_line2_collection  = tra:address_line2
                                            job:address_line3_collection  = tra:address_line3
                                            job:telephone_collection      = tra:telephone_number
                                        End !If tra:use_delivery_address = 'YES'
                                    Else
                                        If sub:use_delivery_address = 'YES'
                                            job:postcode_delivery      = sub:postcode
                                            job:company_name_delivery  = sub:company_name
                                            job:address_line1_delivery = sub:address_line1
                                            job:address_line2_delivery = sub:address_line2
                                            job:address_line3_delivery = sub:address_line3
                                            job:telephone_delivery     = sub:telephone_number
                                        End
                                        If sub:use_collection_address = 'YES'
                                            job:postcode_collection      = sub:postcode
                                            job:company_name_collection  = sub:company_name
                                            job:address_line1_collection = sub:address_line1
                                            job:address_line2_collection = sub:address_line2
                                            job:address_line3_collection = sub:address_line3
                                            job:telephone_collection     = sub:telephone_number
                                        End
                                    End
      
                                end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                            end!if access:subtracc.fetch(sub:account_number_key) = level:benign
                            job:transit_type = TransitTypeValue
                            access:trantype.clearkey(trt:transit_type_key)
                            trt:transit_type = job:transit_type
                            if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                                IF trt:InWorkshop = 'YES'
                                    job:workshop = 'YES'
                                End!IF trt:InWorkshop = 'YES'
                                GetStatus(Sub(trt:initial_status,1,3),InsertRecord,'JOB')
                                GetStatus(Sub(trt:Exchangestatus,1,3),InsertRecord,'EXC')
                                GetStatus(Sub(trt:Loanstatus,1,3),InsertRecord,'LOA')
                            end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign

                            access:users.clearkey(use:password_key)
                            use:password    =glo:password
                            If access:users.fetch(use:password_key) = Level:Benign
                                job:who_booked                    = use:user_code
                            End
                            If job:warranty_job = 'YES'
                                ! Start Change 3652 BE(09/12/03)
                                !job:edi = Set_Edi(job:manufacturer)
                                job:edi = PendingJob(job:manufacturer)
                                ! End Change 3652 BE(09/12/03)
                            end

                            If Access:JOBNOTES.PrimeRecord() = Level:Benign
                                jbn:RefNumber  = job:Ref_Number
                                ! Start Change 2472 BE(10/04/03)
                                !jbn:Engineers_Notes   = imp:FaultDescription
                                ! Start Change 2472 BE(10/04/03)

                                ! Start Change 3652 BE(09/12/03)
                                DateReq" = CLIP(LEFT(imp:DateRequired))
                                TimeReq" = CLIP(LEFT(imp:TimeRequired))
                                IF (DateReq" <> '') THEN
                                  IF (LEN(CLIP(DateReq")) = 8) THEN
                                      Jbn:Engineers_Notes = 'Date Required: ' & DateReq"[7 : 8] & '/' & |
                                                             DateReq"[5 : 6] & '/' & DateReq"[1 : 4]
                                  ELSE
                                      Jbn:Engineers_Notes = 'Date Required: ' & CLIP(DateReq")
                                  END
                                  IF (TimeReq" <> '') THEN
                                      IF (LEN(CLIP(TimeReq")) = 4) THEN
                                          Jbn:Engineers_Notes = CLIP(Jbn:Engineers_Notes) & |
                                                  '<13><10>Time Required: ' & TimeReq"[1 : 2] & ':' & TimeReq"[3 : 4]
                                      ELSE
                                          Jbn:Engineers_Notes = CLIP(Jbn:Engineers_Notes) & |
                                                  '<13><10>Time Required: ' & CLIP(TimeReq")
                                      END
                                  END
                                END
                                ! End Change 3652 BE(09/12/03)

                                ! Start Change 3299 BE(03/11/03)
                                !If Access:JOBNOTES.TryInsert() = Level:Benign
                                !    !Insert Successful
                                !Else !If Access:JOBNOTES.TryInsert() = Level:Benign
                                !    !Insert Failed
                                !End !If Access:JOBNOTES.TryInsert() = Level:Benign

                                IF (Access:JOBSE.PrimeRecord() = Level:Benign) THEN
                                    jobe:RefNumber = job:Ref_Number

                                    jobe:TraFaultCode1 = GetTradeFault(1, Clip(imp:FaultDescription), tra:account_number )
                                    jobe:TraFaultCode5 = GetTradeFault(5, Clip(Upper(imp:ServiceType)), tra:account_number )
                                    jobe:TraFaultCode4 = GetTradeFault(4, Clip(Upper(imp:CallCentreAgent)), tra:account_number )
                                    jobe:TraFaultCode6 = GetTradeFault(6, Clip(Upper(imp:PaymentFlag)), tra:account_number )
                                    jobe:TraFaultCode3 = GetTradeFault(3, Clip(Upper(imp:CorporateID)), tra:account_number )
                                    IF (job:Workshop = 'YES') THEN
                                        jobe:InWorkshopDate = Today()
                                        jobe:InWorkshopTime = Clock()
                                    END
                                    IF (Access:JOBSE.TryInsert() <> Level:Benign) THEN
                                        Access:JOBSE.cancelautoinc()
                                    END
                                END
                                IF (Access:JOBNOTES.TryInsert() <> Level:Benign) THEN
                                    Access:JOBNOTES.cancelautoinc()
                                END
                                ! End Change 3299 BE(03/11/03)

                            End !If Access:JOBNOTES.PrimeRecord() = Level:Benign

                            ! Start Change 3299 BE(03/11/03)
                            !If Access:JOBSE.PrimeRecord() = Level:Benign
                            !   jobe:RefNumber = job:Ref_Number
                            !
                            !   ! Start Change 2472 BE(10/04/03)
                            !   jobe:TraFaultCode1 = Clip(imp:FaultDescription)
                            !   ! Start Change 2472 BE(10/04/03)
                            !
                            !   ! Start Change BE015 BE(20/08/03)
                            !   !jobe:TraFaultCode12 = Clip(Upper(imp:ServiceType))
                            !   jobe:TraFaultCode5 = Clip(Upper(imp:ServiceType))
                            !   ! End Change BE015 BE(20/08/03)
                            !
                            !   jobe:TraFaultCode4 = Clip(Upper(imp:CallCentreAgent))
                            !
                            !   ! Start Change BE015 BE(20/08/03)
                            !   !jobe:TraFaultCode11 = Clip(Upper(imp:PaymentFlag))
                            !   jobe:TraFaultCode6 = Clip(Upper(imp:PaymentFlag))
                            !   ! End Change BE015 BE(20/08/03)
                            !
                            !   jobe:TraFaultCode3 = Clip(Upper(imp:CorporateID))
                            !   If job:Workshop = 'YES'
                            !     jobe:InWorkshopDate = Today()
                            !     jobe:InWorkshopTime = Clock()
                            !   End !If job:Workshop = 'YES'
                            !   If Access:JOBSE.TryInsert() = Level:Benign
                            !      !insert successful
                            !   Else
                            !       !insert failed
                            !  End !If Acess:JOBSE.TryInsert
                            !End !if
                            ! End Change 3299 BE(03/11/03)
      
                            access:users.clearkey(use:password_key)
                            use:password    =glo:password
                            access:users.fetch(use:password_key)
                            job:who_booked  = use:user_code
                            get(audit,0)
                            if access:audit.primerecord() = level:benign
                                aud:notes         = 'UNIT DETAILS: ' & Clip(job:Manufacturer) & ' - ' & Clip(job:Model_Number) &|
                                                    '<13,10>I.M.E.I. NO: ' & Clip(job:ESN) & |
                                                    '<13,10,13,10>CHARGEABLE JOB: ' & Clip(job:Chargeable_Job) &|
                                                    '<13,10>WARRANTY JOB: ' & Clip(job:Warranty_Job)
                                aud:ref_number    = job:ref_number
                                aud:date          = Today()
                                aud:time          = Clock()
                                access:users.clearkey(use:password_key)
                                use:password =glo:password
                                access:users.fetch(use:password_key)
                                aud:user = use:user_code
                                aud:action        = 'CRC IMPORT (VODAFONE CORPORATE)'
                                if access:audit.insert()
                                    access:audit.cancelautoinc()
                                end
                            end!if access:audit.primerecord() = level:benign

                            if access:jobs.tryinsert()
                                access:jobs.cancelautoinc()
                            Else
!                               If Access:JOBSE.PrimeRecord() = Level:Benign
!                                    jobe:RefNumber  = job:Ref_Number
!                                    If job:Workshop = 'YES'
!                                          jobe:InWorkshopDate = Today()
!                                          jobe:InWorkshopTime = Clock()
!                                      End !If job:Workshop = 'YES'
!                                      If Access:JOBSE.TryInsert() = Level:Benign
!                                          !Insert Successful
!                                      Else !If Access:JOBSE.TryInsert() = Level:Benign
!                                          !Insert Failed
!                                      End !If Access:JOBSE.TryInsert() = Level:Benign
!                                End !If Access:JOBSE.PrimeRecord() = Level:Benign

                                 PrintLetter(job:Ref_Number,tmp:LetterName)

                                 exp:RefNumber = job:ref_number
                                 exp:CPostCode = job:postcode_delivery
                                 exp:CDelivery_Name = job:company_name_delivery
                                 exp:Caddress1 = job:address_line1_delivery
                                 exp:Caddress2 = job:address_line2_delivery
                                 exp:Caddress3 = job:address_line3_delivery
                                 exp:CTelephone = job:telephone_delivery
                                 exp:Corder = job:Order_Number
                                 ! Start Change 3156 BE(11/11/03)
                                 exp:CSurname = job:Surname
                                 ! End Change 3156 BE(11/11/03)
                                 ADD(ExportFile)

                                 If func:PrintLabel
                                     glo:Select1 = job:Ref_Number
                                     Thermal_Labels('')
                                     glo:Select1 = ''
                                 End !If func:PrintLabel
                                 If func:PrintJobCard
                                     glo:Select1 = job:Ref_Number
                                     Job_Card()
                                     glo:Select1 = ''
                                 End !If func:PrintLabel

                                 !Export the file!

                                 If CountBouncer(job:Ref_Number,job:Date_Booked,job:Esn)
                                     bounce:RefNumber = job:Ref_Number
                                     Add(BouncerList)
                                 End!If CountBouncer(job:Ref_Number,job:Date_Booked,job:Esn)
                            End!if access:jobs.tryinsert()
                        end!if access:jobs.primerecord() = level:benign
                    End!Loop

                    Setcursor()
                    Close(IMPORTFILE)
                    CLOSE(ExportFile)
                    Close(LupFile)

                    ! Start Change 2472 BE(10/04/03)
                    MessageEx('Import Completed<13><10>Batch No. ' & CLIP(FORMAT(batch_number#, @n6)) |
                                ,'ServiceBase 2000',|
                               'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0, |
                               beep:systemasterisk,msgex:samewidths,84,26,0)
                    ! End  Change 2472 BE(10/04/03)

                    If Records(BouncerList)
                        Case MessageEx('One or more of the imported Jobs are Bouncers.'&|
                                        '<13,10>'&|
                                        '<13,10>Do you wish to print the Bouncer History Reports for these jobs?','ServiceBase 2000',|
                                        'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                        beep:systemexclamation,msgex:samewidths,84,26,0)
                            Of 1 ! &Yes Button
                                Sort(BouncerList,bounce:RefNumber)
                                Loop x# = 1 To Records(BouncerList)
                                    Get(BouncerList,x#)

                                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                                    job:Ref_Number  = bounce:RefNUmber
                                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                        !Found
                                        glo:Select2 = job:esn
                                        Bouncer_History
                                        glo:Select2 = ''
                                    Else! If Access:.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                    End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

                                End!Loop x# = 1 To Records(BouncerList)
                            Of 2 ! &No Button
                        End!Case MessageEx
                    End

            End!If error# = 0
      
        Of 2 ! &No Button
    End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportJobs, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'CRCImportVodafoneCorporate')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
GetTradeFault   PROCEDURE(IN:CodeNumber, IN:CodeValue, IN:TradeAccount)
    CODE

    Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
    taf:AccountNumber = IN:TradeAccount
    taf:Field_Number  = IN:CodeNumber
    IF (Access:TRAFAULT.TryFetch(taf:Field_Number_Key) = Level:Benign) THEN
        IF (taf:lookup = 'YES') THEN
            Access:TRAFAULO.ClearKey(tfo:Field_Key)
            tfo:AccountNumber = IN:TradeAccount
            tfo:Field_Number  = IN:CodeNumber
            tfo:Field         = IN:CodeValue
            SET(tfo:Field_Key, tfo:Field_Key)
            IF ((Access:TRAFAULO.next() = Level:Benign) AND |
                (tfo:AccountNumber = IN:TradeAccount) AND |
                (tfo:Field_Number  = IN:CodeNumber) AND |
                (tfo:Field  = IN:CodeValue)) THEN
                IF (taf:ReplicateFault = 'YES') THEN
                    IF (jbn:fault_description = '') THEN
                        jbn:fault_description = Clip(tfo:description)
                    ELSE
                        jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(tfo:description)
                    END
                END
                IF (taf:ReplicateInvoice = 'YES') THEN
                    IF (jbn:invoice_text = '') THEN
                        jbn:invoice_text = Clip(tfo:description)
                    ELSE
                        jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(tfo:description)
                    END
                END
            END
        END
    END

    RETURN(IN:CodeValue)
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
