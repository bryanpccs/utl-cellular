

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF02003.INC'),ONCE        !Local module procedure declarations
                     END


CRCDefaults PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
VodafoneAcNo         STRING(30)
VodafoneTransitType  STRING(30)
VodafoneLocation     STRING(30)
VodaFone_Directory   CSTRING(255)
VcAcNo               STRING(30)
VcTransitType        STRING(30)
VcLocation           STRING(30)
Vc_Directory         CSTRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::dfc:Record  LIKE(dfc:RECORD),STATIC
QuickWindow          WINDOW('Import Defaults'),AT(,,233,131),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('CRCDefaults'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,224,96),USE(?CurrentTab),SPREAD
                         TAB('Swift Dealer'),USE(?Tab1)
                           PROMPT('Account Number'),AT(8,36),USE(?DFC:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(dfc:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Account Number'),TIP('Account Number'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(212,36,10,10),USE(?LookupAccountNumber),SKIP,ICON('List3.ico')
                           PROMPT('Transit Type'),AT(8,52),USE(?DFC:TransitType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(dfc:TransitType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Transit Type'),TIP('Transit Type'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupTransitType),SKIP,ICON('List3.ico')
                           PROMPT('Location'),AT(8,68),USE(?DFC:Location:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(dfc:Location),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,68,10,10),USE(?LookupLocation),SKIP,ICON('List3.ico')
                         END
                         TAB('Swift End User'),USE(?Tab2)
                           PROMPT('Account Number'),AT(8,36),USE(?dfc:EAccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(dfc:EAccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Acc'),TIP('Acc'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(212,36,10,10),USE(?LookupEAccountNumber),SKIP,ICON('List3.ico')
                           PROMPT('Transit Type'),AT(8,52),USE(?dfc:ETransitType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(dfc:ETransitType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Transit Type (End User)'),TIP('Transit Type (End User)'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupETransitType),SKIP,ICON('List3.ico')
                           PROMPT('Location'),AT(8,68),USE(?dfc:ELocation:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(dfc:ELocation),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location (End User)'),TIP('Location (End User)'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,68,10,10),USE(?LookupELocation),SKIP,ICON('List3.ico')
                         END
                         TAB('Panasonic'),USE(?Tab3)
                           PROMPT('Account Number'),AT(8,36),USE(?dfc:PAccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(dfc:PAccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Account Number'),TIP('Account Number'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(212,36,10,10),USE(?LookupPanasonicAccountNumber),SKIP,ICON('List3.ico')
                           PROMPT('Transit Type'),AT(8,52),USE(?dfc:PTransitType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(dfc:PTransitType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Transit Type'),TIP('Transit Type'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupPanasonicTransitType),SKIP,ICON('List3.ico')
                           PROMPT('Location'),AT(8,68),USE(?dfc:PLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(dfc:PLocation),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),UPR
                           BUTTON,AT(212,68,10,10),USE(?LookupPanasonicLocation),SKIP,ICON('List3.ico')
                         END
                         TAB('Sony'),USE(?Tab4)
                           PROMPT('Account Number'),AT(8,36),USE(?dfc:SAccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(dfc:SAccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Account Number'),TIP('Account Number'),UPR
                           BUTTON,AT(212,36,10,10),USE(?CallLookupSonyAccount),LEFT,ICON('list3.ico')
                           PROMPT('Transit Type'),AT(8,52),USE(?dfc:STransitType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(dfc:STransitType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Transit Type'),TIP('Transit Type'),UPR
                           BUTTON,AT(212,52,10,10),USE(?CallLookupSonyTransitType),LEFT,ICON('list3.ico')
                           PROMPT('Transit Type'),AT(8,68),USE(?dfc:STransitType2:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(dfc:STransitType2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Transit Type'),TIP('Transit Type'),UPR
                           BUTTON,AT(212,68,10,10),USE(?CallLookupSonyTransitType2),LEFT,ICON('list3.ico')
                           PROMPT('Location'),AT(9,84),USE(?dfc:SLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,124,10),USE(dfc:SLocation),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),UPR
                           BUTTON,AT(212,84,10,10),USE(?CallLookupSonyLocation),LEFT,ICON('list3.ico')
                         END
                         TAB('Vodafone ONR'),USE(?Tab5)
                           STRING('Account Number'),AT(8,36),USE(?StrVodafoneAcNo),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(VodafoneAcNo),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(212,36,10,10),USE(?CallLookup),ICON('list3.ico')
                           STRING('Transit Type'),AT(8,52),USE(?StrVodafoneTranType),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(VodafoneTransitType),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(212,52,10,10),USE(?CallLookup:2),ICON('list3.ico')
                           STRING('Location'),AT(8,68),USE(?StrVodafoneLocation),TRN
                           ENTRY(@s30),AT(84,68,124,10),USE(VodafoneLocation),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(212,68,10,10),USE(?CallLookup:3),ICON('list3.ico')
                           PROMPT('Parceline Directory'),AT(8,84),USE(?VodaFone_Directory:Prompt)
                           ENTRY(@s254),AT(84,84,124,10),USE(VodaFone_Directory),FONT(,,,FONT:bold)
                           BUTTON,AT(212,84,10,10),USE(?LookupFile),ICON('list3.ico')
                         END
                         TAB('Vodafone Corporate'),USE(?Tab6)
                           STRING('Account Number'),AT(8,36),USE(?StrVodafoneAcNo:2),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(VcAcNo),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(212,36,10,10),USE(?CallLookup:4),ICON('list3.ico')
                           STRING('Transit Type'),AT(8,52),USE(?StrVodafoneTranType:2),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(VcTransitType),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(212,52,10,10),USE(?CallLookup:5),ICON('list3.ico')
                           STRING('Location'),AT(8,68),USE(?StrVodafoneLocation:2),TRN
                           ENTRY(@s30),AT(84,68,124,10),USE(VcLocation),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(212,68,10,10),USE(?CallLookup:6),ICON('list3.ico')
                           PROMPT('Parceline Directory'),AT(8,84),USE(?VodaFone_Directory:Prompt:2)
                           ENTRY(@s254),AT(84,84,124,10),USE(Vc_Directory),FONT(,,,FONT:bold)
                           BUTTON,AT(212,84,10,10),USE(?LookupFile:2),ICON('list3.ico')
                         END
                       END
                       BUTTON('&OK'),AT(112,108,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(168,108,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,104,224,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FileLookup37         SelectFileClass
FileLookup38         SelectFileClass
!Save Entry Fields Incase Of Lookup
look:dfc:AccountNumber                Like(dfc:AccountNumber)
look:dfc:TransitType                Like(dfc:TransitType)
look:dfc:Location                Like(dfc:Location)
look:dfc:EAccountNumber                Like(dfc:EAccountNumber)
look:dfc:ETransitType                Like(dfc:ETransitType)
look:dfc:ELocation                Like(dfc:ELocation)
look:dfc:PAccountNumber                Like(dfc:PAccountNumber)
look:dfc:PTransitType                Like(dfc:PTransitType)
look:dfc:PLocation                Like(dfc:PLocation)
look:dfc:SAccountNumber                Like(dfc:SAccountNumber)
look:dfc:STransitType                Like(dfc:STransitType)
look:dfc:STransitType2                Like(dfc:STransitType2)
look:dfc:SLocation                Like(dfc:SLocation)
look:VodafoneAcNo                Like(VodafoneAcNo)
look:VodafoneTransitType                Like(VodafoneTransitType)
look:VodafoneLocation                Like(VodafoneLocation)
look:VcAcNo                Like(VcAcNo)
look:VcTransitType                Like(VcTransitType)
look:VcLocation                Like(VcLocation)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?DFC:AccountNumber:Prompt{prop:FontColor} = -1
    ?DFC:AccountNumber:Prompt{prop:Color} = 15066597
    If ?dfc:AccountNumber{prop:ReadOnly} = True
        ?dfc:AccountNumber{prop:FontColor} = 65793
        ?dfc:AccountNumber{prop:Color} = 15066597
    Elsif ?dfc:AccountNumber{prop:Req} = True
        ?dfc:AccountNumber{prop:FontColor} = 65793
        ?dfc:AccountNumber{prop:Color} = 8454143
    Else ! If ?dfc:AccountNumber{prop:Req} = True
        ?dfc:AccountNumber{prop:FontColor} = 65793
        ?dfc:AccountNumber{prop:Color} = 16777215
    End ! If ?dfc:AccountNumber{prop:Req} = True
    ?dfc:AccountNumber{prop:Trn} = 0
    ?dfc:AccountNumber{prop:FontStyle} = font:Bold
    ?DFC:TransitType:Prompt{prop:FontColor} = -1
    ?DFC:TransitType:Prompt{prop:Color} = 15066597
    If ?dfc:TransitType{prop:ReadOnly} = True
        ?dfc:TransitType{prop:FontColor} = 65793
        ?dfc:TransitType{prop:Color} = 15066597
    Elsif ?dfc:TransitType{prop:Req} = True
        ?dfc:TransitType{prop:FontColor} = 65793
        ?dfc:TransitType{prop:Color} = 8454143
    Else ! If ?dfc:TransitType{prop:Req} = True
        ?dfc:TransitType{prop:FontColor} = 65793
        ?dfc:TransitType{prop:Color} = 16777215
    End ! If ?dfc:TransitType{prop:Req} = True
    ?dfc:TransitType{prop:Trn} = 0
    ?dfc:TransitType{prop:FontStyle} = font:Bold
    ?DFC:Location:Prompt{prop:FontColor} = -1
    ?DFC:Location:Prompt{prop:Color} = 15066597
    If ?dfc:Location{prop:ReadOnly} = True
        ?dfc:Location{prop:FontColor} = 65793
        ?dfc:Location{prop:Color} = 15066597
    Elsif ?dfc:Location{prop:Req} = True
        ?dfc:Location{prop:FontColor} = 65793
        ?dfc:Location{prop:Color} = 8454143
    Else ! If ?dfc:Location{prop:Req} = True
        ?dfc:Location{prop:FontColor} = 65793
        ?dfc:Location{prop:Color} = 16777215
    End ! If ?dfc:Location{prop:Req} = True
    ?dfc:Location{prop:Trn} = 0
    ?dfc:Location{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?dfc:EAccountNumber:Prompt{prop:FontColor} = -1
    ?dfc:EAccountNumber:Prompt{prop:Color} = 15066597
    If ?dfc:EAccountNumber{prop:ReadOnly} = True
        ?dfc:EAccountNumber{prop:FontColor} = 65793
        ?dfc:EAccountNumber{prop:Color} = 15066597
    Elsif ?dfc:EAccountNumber{prop:Req} = True
        ?dfc:EAccountNumber{prop:FontColor} = 65793
        ?dfc:EAccountNumber{prop:Color} = 8454143
    Else ! If ?dfc:EAccountNumber{prop:Req} = True
        ?dfc:EAccountNumber{prop:FontColor} = 65793
        ?dfc:EAccountNumber{prop:Color} = 16777215
    End ! If ?dfc:EAccountNumber{prop:Req} = True
    ?dfc:EAccountNumber{prop:Trn} = 0
    ?dfc:EAccountNumber{prop:FontStyle} = font:Bold
    ?dfc:ETransitType:Prompt{prop:FontColor} = -1
    ?dfc:ETransitType:Prompt{prop:Color} = 15066597
    If ?dfc:ETransitType{prop:ReadOnly} = True
        ?dfc:ETransitType{prop:FontColor} = 65793
        ?dfc:ETransitType{prop:Color} = 15066597
    Elsif ?dfc:ETransitType{prop:Req} = True
        ?dfc:ETransitType{prop:FontColor} = 65793
        ?dfc:ETransitType{prop:Color} = 8454143
    Else ! If ?dfc:ETransitType{prop:Req} = True
        ?dfc:ETransitType{prop:FontColor} = 65793
        ?dfc:ETransitType{prop:Color} = 16777215
    End ! If ?dfc:ETransitType{prop:Req} = True
    ?dfc:ETransitType{prop:Trn} = 0
    ?dfc:ETransitType{prop:FontStyle} = font:Bold
    ?dfc:ELocation:Prompt{prop:FontColor} = -1
    ?dfc:ELocation:Prompt{prop:Color} = 15066597
    If ?dfc:ELocation{prop:ReadOnly} = True
        ?dfc:ELocation{prop:FontColor} = 65793
        ?dfc:ELocation{prop:Color} = 15066597
    Elsif ?dfc:ELocation{prop:Req} = True
        ?dfc:ELocation{prop:FontColor} = 65793
        ?dfc:ELocation{prop:Color} = 8454143
    Else ! If ?dfc:ELocation{prop:Req} = True
        ?dfc:ELocation{prop:FontColor} = 65793
        ?dfc:ELocation{prop:Color} = 16777215
    End ! If ?dfc:ELocation{prop:Req} = True
    ?dfc:ELocation{prop:Trn} = 0
    ?dfc:ELocation{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    ?dfc:PAccountNumber:Prompt{prop:FontColor} = -1
    ?dfc:PAccountNumber:Prompt{prop:Color} = 15066597
    If ?dfc:PAccountNumber{prop:ReadOnly} = True
        ?dfc:PAccountNumber{prop:FontColor} = 65793
        ?dfc:PAccountNumber{prop:Color} = 15066597
    Elsif ?dfc:PAccountNumber{prop:Req} = True
        ?dfc:PAccountNumber{prop:FontColor} = 65793
        ?dfc:PAccountNumber{prop:Color} = 8454143
    Else ! If ?dfc:PAccountNumber{prop:Req} = True
        ?dfc:PAccountNumber{prop:FontColor} = 65793
        ?dfc:PAccountNumber{prop:Color} = 16777215
    End ! If ?dfc:PAccountNumber{prop:Req} = True
    ?dfc:PAccountNumber{prop:Trn} = 0
    ?dfc:PAccountNumber{prop:FontStyle} = font:Bold
    ?dfc:PTransitType:Prompt{prop:FontColor} = -1
    ?dfc:PTransitType:Prompt{prop:Color} = 15066597
    If ?dfc:PTransitType{prop:ReadOnly} = True
        ?dfc:PTransitType{prop:FontColor} = 65793
        ?dfc:PTransitType{prop:Color} = 15066597
    Elsif ?dfc:PTransitType{prop:Req} = True
        ?dfc:PTransitType{prop:FontColor} = 65793
        ?dfc:PTransitType{prop:Color} = 8454143
    Else ! If ?dfc:PTransitType{prop:Req} = True
        ?dfc:PTransitType{prop:FontColor} = 65793
        ?dfc:PTransitType{prop:Color} = 16777215
    End ! If ?dfc:PTransitType{prop:Req} = True
    ?dfc:PTransitType{prop:Trn} = 0
    ?dfc:PTransitType{prop:FontStyle} = font:Bold
    ?dfc:PLocation:Prompt{prop:FontColor} = -1
    ?dfc:PLocation:Prompt{prop:Color} = 15066597
    If ?dfc:PLocation{prop:ReadOnly} = True
        ?dfc:PLocation{prop:FontColor} = 65793
        ?dfc:PLocation{prop:Color} = 15066597
    Elsif ?dfc:PLocation{prop:Req} = True
        ?dfc:PLocation{prop:FontColor} = 65793
        ?dfc:PLocation{prop:Color} = 8454143
    Else ! If ?dfc:PLocation{prop:Req} = True
        ?dfc:PLocation{prop:FontColor} = 65793
        ?dfc:PLocation{prop:Color} = 16777215
    End ! If ?dfc:PLocation{prop:Req} = True
    ?dfc:PLocation{prop:Trn} = 0
    ?dfc:PLocation{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    ?dfc:SAccountNumber:Prompt{prop:FontColor} = -1
    ?dfc:SAccountNumber:Prompt{prop:Color} = 15066597
    If ?dfc:SAccountNumber{prop:ReadOnly} = True
        ?dfc:SAccountNumber{prop:FontColor} = 65793
        ?dfc:SAccountNumber{prop:Color} = 15066597
    Elsif ?dfc:SAccountNumber{prop:Req} = True
        ?dfc:SAccountNumber{prop:FontColor} = 65793
        ?dfc:SAccountNumber{prop:Color} = 8454143
    Else ! If ?dfc:SAccountNumber{prop:Req} = True
        ?dfc:SAccountNumber{prop:FontColor} = 65793
        ?dfc:SAccountNumber{prop:Color} = 16777215
    End ! If ?dfc:SAccountNumber{prop:Req} = True
    ?dfc:SAccountNumber{prop:Trn} = 0
    ?dfc:SAccountNumber{prop:FontStyle} = font:Bold
    ?dfc:STransitType:Prompt{prop:FontColor} = -1
    ?dfc:STransitType:Prompt{prop:Color} = 15066597
    If ?dfc:STransitType{prop:ReadOnly} = True
        ?dfc:STransitType{prop:FontColor} = 65793
        ?dfc:STransitType{prop:Color} = 15066597
    Elsif ?dfc:STransitType{prop:Req} = True
        ?dfc:STransitType{prop:FontColor} = 65793
        ?dfc:STransitType{prop:Color} = 8454143
    Else ! If ?dfc:STransitType{prop:Req} = True
        ?dfc:STransitType{prop:FontColor} = 65793
        ?dfc:STransitType{prop:Color} = 16777215
    End ! If ?dfc:STransitType{prop:Req} = True
    ?dfc:STransitType{prop:Trn} = 0
    ?dfc:STransitType{prop:FontStyle} = font:Bold
    ?dfc:STransitType2:Prompt{prop:FontColor} = -1
    ?dfc:STransitType2:Prompt{prop:Color} = 15066597
    If ?dfc:STransitType2{prop:ReadOnly} = True
        ?dfc:STransitType2{prop:FontColor} = 65793
        ?dfc:STransitType2{prop:Color} = 15066597
    Elsif ?dfc:STransitType2{prop:Req} = True
        ?dfc:STransitType2{prop:FontColor} = 65793
        ?dfc:STransitType2{prop:Color} = 8454143
    Else ! If ?dfc:STransitType2{prop:Req} = True
        ?dfc:STransitType2{prop:FontColor} = 65793
        ?dfc:STransitType2{prop:Color} = 16777215
    End ! If ?dfc:STransitType2{prop:Req} = True
    ?dfc:STransitType2{prop:Trn} = 0
    ?dfc:STransitType2{prop:FontStyle} = font:Bold
    ?dfc:SLocation:Prompt{prop:FontColor} = -1
    ?dfc:SLocation:Prompt{prop:Color} = 15066597
    If ?dfc:SLocation{prop:ReadOnly} = True
        ?dfc:SLocation{prop:FontColor} = 65793
        ?dfc:SLocation{prop:Color} = 15066597
    Elsif ?dfc:SLocation{prop:Req} = True
        ?dfc:SLocation{prop:FontColor} = 65793
        ?dfc:SLocation{prop:Color} = 8454143
    Else ! If ?dfc:SLocation{prop:Req} = True
        ?dfc:SLocation{prop:FontColor} = 65793
        ?dfc:SLocation{prop:Color} = 16777215
    End ! If ?dfc:SLocation{prop:Req} = True
    ?dfc:SLocation{prop:Trn} = 0
    ?dfc:SLocation{prop:FontStyle} = font:Bold
    ?Tab5{prop:Color} = 15066597
    ?StrVodafoneAcNo{prop:FontColor} = -1
    ?StrVodafoneAcNo{prop:Color} = 15066597
    If ?VodafoneAcNo{prop:ReadOnly} = True
        ?VodafoneAcNo{prop:FontColor} = 65793
        ?VodafoneAcNo{prop:Color} = 15066597
    Elsif ?VodafoneAcNo{prop:Req} = True
        ?VodafoneAcNo{prop:FontColor} = 65793
        ?VodafoneAcNo{prop:Color} = 8454143
    Else ! If ?VodafoneAcNo{prop:Req} = True
        ?VodafoneAcNo{prop:FontColor} = 65793
        ?VodafoneAcNo{prop:Color} = 16777215
    End ! If ?VodafoneAcNo{prop:Req} = True
    ?VodafoneAcNo{prop:Trn} = 0
    ?VodafoneAcNo{prop:FontStyle} = font:Bold
    ?StrVodafoneTranType{prop:FontColor} = -1
    ?StrVodafoneTranType{prop:Color} = 15066597
    If ?VodafoneTransitType{prop:ReadOnly} = True
        ?VodafoneTransitType{prop:FontColor} = 65793
        ?VodafoneTransitType{prop:Color} = 15066597
    Elsif ?VodafoneTransitType{prop:Req} = True
        ?VodafoneTransitType{prop:FontColor} = 65793
        ?VodafoneTransitType{prop:Color} = 8454143
    Else ! If ?VodafoneTransitType{prop:Req} = True
        ?VodafoneTransitType{prop:FontColor} = 65793
        ?VodafoneTransitType{prop:Color} = 16777215
    End ! If ?VodafoneTransitType{prop:Req} = True
    ?VodafoneTransitType{prop:Trn} = 0
    ?VodafoneTransitType{prop:FontStyle} = font:Bold
    ?StrVodafoneLocation{prop:FontColor} = -1
    ?StrVodafoneLocation{prop:Color} = 15066597
    If ?VodafoneLocation{prop:ReadOnly} = True
        ?VodafoneLocation{prop:FontColor} = 65793
        ?VodafoneLocation{prop:Color} = 15066597
    Elsif ?VodafoneLocation{prop:Req} = True
        ?VodafoneLocation{prop:FontColor} = 65793
        ?VodafoneLocation{prop:Color} = 8454143
    Else ! If ?VodafoneLocation{prop:Req} = True
        ?VodafoneLocation{prop:FontColor} = 65793
        ?VodafoneLocation{prop:Color} = 16777215
    End ! If ?VodafoneLocation{prop:Req} = True
    ?VodafoneLocation{prop:Trn} = 0
    ?VodafoneLocation{prop:FontStyle} = font:Bold
    ?VodaFone_Directory:Prompt{prop:FontColor} = -1
    ?VodaFone_Directory:Prompt{prop:Color} = 15066597
    If ?VodaFone_Directory{prop:ReadOnly} = True
        ?VodaFone_Directory{prop:FontColor} = 65793
        ?VodaFone_Directory{prop:Color} = 15066597
    Elsif ?VodaFone_Directory{prop:Req} = True
        ?VodaFone_Directory{prop:FontColor} = 65793
        ?VodaFone_Directory{prop:Color} = 8454143
    Else ! If ?VodaFone_Directory{prop:Req} = True
        ?VodaFone_Directory{prop:FontColor} = 65793
        ?VodaFone_Directory{prop:Color} = 16777215
    End ! If ?VodaFone_Directory{prop:Req} = True
    ?VodaFone_Directory{prop:Trn} = 0
    ?VodaFone_Directory{prop:FontStyle} = font:Bold
    ?Tab6{prop:Color} = 15066597
    ?StrVodafoneAcNo:2{prop:FontColor} = -1
    ?StrVodafoneAcNo:2{prop:Color} = 15066597
    If ?VcAcNo{prop:ReadOnly} = True
        ?VcAcNo{prop:FontColor} = 65793
        ?VcAcNo{prop:Color} = 15066597
    Elsif ?VcAcNo{prop:Req} = True
        ?VcAcNo{prop:FontColor} = 65793
        ?VcAcNo{prop:Color} = 8454143
    Else ! If ?VcAcNo{prop:Req} = True
        ?VcAcNo{prop:FontColor} = 65793
        ?VcAcNo{prop:Color} = 16777215
    End ! If ?VcAcNo{prop:Req} = True
    ?VcAcNo{prop:Trn} = 0
    ?VcAcNo{prop:FontStyle} = font:Bold
    ?StrVodafoneTranType:2{prop:FontColor} = -1
    ?StrVodafoneTranType:2{prop:Color} = 15066597
    If ?VcTransitType{prop:ReadOnly} = True
        ?VcTransitType{prop:FontColor} = 65793
        ?VcTransitType{prop:Color} = 15066597
    Elsif ?VcTransitType{prop:Req} = True
        ?VcTransitType{prop:FontColor} = 65793
        ?VcTransitType{prop:Color} = 8454143
    Else ! If ?VcTransitType{prop:Req} = True
        ?VcTransitType{prop:FontColor} = 65793
        ?VcTransitType{prop:Color} = 16777215
    End ! If ?VcTransitType{prop:Req} = True
    ?VcTransitType{prop:Trn} = 0
    ?VcTransitType{prop:FontStyle} = font:Bold
    ?StrVodafoneLocation:2{prop:FontColor} = -1
    ?StrVodafoneLocation:2{prop:Color} = 15066597
    If ?VcLocation{prop:ReadOnly} = True
        ?VcLocation{prop:FontColor} = 65793
        ?VcLocation{prop:Color} = 15066597
    Elsif ?VcLocation{prop:Req} = True
        ?VcLocation{prop:FontColor} = 65793
        ?VcLocation{prop:Color} = 8454143
    Else ! If ?VcLocation{prop:Req} = True
        ?VcLocation{prop:FontColor} = 65793
        ?VcLocation{prop:Color} = 16777215
    End ! If ?VcLocation{prop:Req} = True
    ?VcLocation{prop:Trn} = 0
    ?VcLocation{prop:FontStyle} = font:Bold
    ?VodaFone_Directory:Prompt:2{prop:FontColor} = -1
    ?VodaFone_Directory:Prompt:2{prop:Color} = 15066597
    If ?Vc_Directory{prop:ReadOnly} = True
        ?Vc_Directory{prop:FontColor} = 65793
        ?Vc_Directory{prop:Color} = 15066597
    Elsif ?Vc_Directory{prop:Req} = True
        ?Vc_Directory{prop:FontColor} = 65793
        ?Vc_Directory{prop:Color} = 8454143
    Else ! If ?Vc_Directory{prop:Req} = True
        ?Vc_Directory{prop:FontColor} = 65793
        ?Vc_Directory{prop:Color} = 16777215
    End ! If ?Vc_Directory{prop:Req} = True
    ?Vc_Directory{prop:Trn} = 0
    ?Vc_Directory{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CRCDefaults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'CRCDefaults',1)
    SolaceViewVars('ActionMessage',ActionMessage,'CRCDefaults',1)
    SolaceViewVars('VodafoneAcNo',VodafoneAcNo,'CRCDefaults',1)
    SolaceViewVars('VodafoneTransitType',VodafoneTransitType,'CRCDefaults',1)
    SolaceViewVars('VodafoneLocation',VodafoneLocation,'CRCDefaults',1)
    SolaceViewVars('VodaFone_Directory',VodaFone_Directory,'CRCDefaults',1)
    SolaceViewVars('VcAcNo',VcAcNo,'CRCDefaults',1)
    SolaceViewVars('VcTransitType',VcTransitType,'CRCDefaults',1)
    SolaceViewVars('VcLocation',VcLocation,'CRCDefaults',1)
    SolaceViewVars('Vc_Directory',Vc_Directory,'CRCDefaults',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DFC:AccountNumber:Prompt;  SolaceCtrlName = '?DFC:AccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:AccountNumber;  SolaceCtrlName = '?dfc:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupAccountNumber;  SolaceCtrlName = '?LookupAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DFC:TransitType:Prompt;  SolaceCtrlName = '?DFC:TransitType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:TransitType;  SolaceCtrlName = '?dfc:TransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupTransitType;  SolaceCtrlName = '?LookupTransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DFC:Location:Prompt;  SolaceCtrlName = '?DFC:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:Location;  SolaceCtrlName = '?dfc:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:EAccountNumber:Prompt;  SolaceCtrlName = '?dfc:EAccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:EAccountNumber;  SolaceCtrlName = '?dfc:EAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEAccountNumber;  SolaceCtrlName = '?LookupEAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:ETransitType:Prompt;  SolaceCtrlName = '?dfc:ETransitType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:ETransitType;  SolaceCtrlName = '?dfc:ETransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupETransitType;  SolaceCtrlName = '?LookupETransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:ELocation:Prompt;  SolaceCtrlName = '?dfc:ELocation:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:ELocation;  SolaceCtrlName = '?dfc:ELocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupELocation;  SolaceCtrlName = '?LookupELocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:PAccountNumber:Prompt;  SolaceCtrlName = '?dfc:PAccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:PAccountNumber;  SolaceCtrlName = '?dfc:PAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupPanasonicAccountNumber;  SolaceCtrlName = '?LookupPanasonicAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:PTransitType:Prompt;  SolaceCtrlName = '?dfc:PTransitType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:PTransitType;  SolaceCtrlName = '?dfc:PTransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupPanasonicTransitType;  SolaceCtrlName = '?LookupPanasonicTransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:PLocation:Prompt;  SolaceCtrlName = '?dfc:PLocation:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:PLocation;  SolaceCtrlName = '?dfc:PLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupPanasonicLocation;  SolaceCtrlName = '?LookupPanasonicLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:SAccountNumber:Prompt;  SolaceCtrlName = '?dfc:SAccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:SAccountNumber;  SolaceCtrlName = '?dfc:SAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookupSonyAccount;  SolaceCtrlName = '?CallLookupSonyAccount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:STransitType:Prompt;  SolaceCtrlName = '?dfc:STransitType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:STransitType;  SolaceCtrlName = '?dfc:STransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookupSonyTransitType;  SolaceCtrlName = '?CallLookupSonyTransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:STransitType2:Prompt;  SolaceCtrlName = '?dfc:STransitType2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:STransitType2;  SolaceCtrlName = '?dfc:STransitType2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookupSonyTransitType2;  SolaceCtrlName = '?CallLookupSonyTransitType2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:SLocation:Prompt;  SolaceCtrlName = '?dfc:SLocation:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dfc:SLocation;  SolaceCtrlName = '?dfc:SLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookupSonyLocation;  SolaceCtrlName = '?CallLookupSonyLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StrVodafoneAcNo;  SolaceCtrlName = '?StrVodafoneAcNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VodafoneAcNo;  SolaceCtrlName = '?VodafoneAcNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup;  SolaceCtrlName = '?CallLookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StrVodafoneTranType;  SolaceCtrlName = '?StrVodafoneTranType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VodafoneTransitType;  SolaceCtrlName = '?VodafoneTransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup:2;  SolaceCtrlName = '?CallLookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StrVodafoneLocation;  SolaceCtrlName = '?StrVodafoneLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VodafoneLocation;  SolaceCtrlName = '?VodafoneLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup:3;  SolaceCtrlName = '?CallLookup:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VodaFone_Directory:Prompt;  SolaceCtrlName = '?VodaFone_Directory:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VodaFone_Directory;  SolaceCtrlName = '?VodaFone_Directory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFile;  SolaceCtrlName = '?LookupFile';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StrVodafoneAcNo:2;  SolaceCtrlName = '?StrVodafoneAcNo:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VcAcNo;  SolaceCtrlName = '?VcAcNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup:4;  SolaceCtrlName = '?CallLookup:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StrVodafoneTranType:2;  SolaceCtrlName = '?StrVodafoneTranType:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VcTransitType;  SolaceCtrlName = '?VcTransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup:5;  SolaceCtrlName = '?CallLookup:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StrVodafoneLocation:2;  SolaceCtrlName = '?StrVodafoneLocation:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VcLocation;  SolaceCtrlName = '?VcLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup:6;  SolaceCtrlName = '?CallLookup:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VodaFone_Directory:Prompt:2;  SolaceCtrlName = '?VodaFone_Directory:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Vc_Directory;  SolaceCtrlName = '?Vc_Directory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFile:2;  SolaceCtrlName = '?LookupFile:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)








ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:DEFCRC.Open
    SET(DEFCRC)
    CASE Access:DEFCRC.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:DEFCRC.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('CRCDefaults')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'CRCDefaults')      !Add Procedure to Log
      end
    
    
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DFC:AccountNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(dfc:Record,History::dfc:Record)
  SELF.AddHistoryField(?dfc:AccountNumber,2)
  SELF.AddHistoryField(?dfc:TransitType,3)
  SELF.AddHistoryField(?dfc:Location,4)
  SELF.AddHistoryField(?dfc:EAccountNumber,8)
  SELF.AddHistoryField(?dfc:ETransitType,9)
  SELF.AddHistoryField(?dfc:ELocation,10)
  SELF.AddHistoryField(?dfc:PAccountNumber,5)
  SELF.AddHistoryField(?dfc:PTransitType,6)
  SELF.AddHistoryField(?dfc:PLocation,7)
  SELF.AddHistoryField(?dfc:SAccountNumber,11)
  SELF.AddHistoryField(?dfc:STransitType,12)
  SELF.AddHistoryField(?dfc:STransitType2,13)
  SELF.AddHistoryField(?dfc:SLocation,14)
  SELF.AddUpdateFile(Access:DEFCRC)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:DEFCRC.Open
  Relate:LOCINTER.Open
  Relate:TRANTYPE.Open
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFCRC
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  ! Start Change 4628 BE(24/09/2004)
  !If Instring('HAYS',def:User_Name,1,1)
  If Instring('ACR',def:User_Name,1,1)
  ! End Change 4628 BE(24/09/2004)
      ?Tab1{prop:Hide} = 1
      ?Tab2{prop:Hide} = 1
      ?Tab3{prop:Hide} = 1
      ?Tab5{prop:Hide} = 1
  End !Instring('HAYS',def:User_Name,1,1)
  
  VodafoneAcNo = GETINI('IMPORT','VAccountNumber','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  VodafoneTransitType = GETINI('IMPORT','VTransitType','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  VodafoneLocation = GETINI('IMPORT','VLocation','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  Vodafone_Directory = GETINI('IMPORT','VDirectory','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  
  ! Start Change 2649 BE(10/06/03)
  VcAcNo = GETINI('IMPORT_VC','VAccountNumber','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  VcTransitType = GETINI('IMPORT_VC','VTransitType','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  VcLocation = GETINI('IMPORT_VC','VLocation','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  Vc_Directory = GETINI('IMPORT_VC','VDirectory','',CLIP(PATH()) & '\CRCIMPEXTRA.INI')
  ! End Change 2649 BE(10/06/03)
  Do RecolourWindow
  ! support for CPCS
  IF ?dfc:AccountNumber{Prop:Tip} AND ~?LookupAccountNumber{Prop:Tip}
     ?LookupAccountNumber{Prop:Tip} = 'Select ' & ?dfc:AccountNumber{Prop:Tip}
  END
  IF ?dfc:AccountNumber{Prop:Msg} AND ~?LookupAccountNumber{Prop:Msg}
     ?LookupAccountNumber{Prop:Msg} = 'Select ' & ?dfc:AccountNumber{Prop:Msg}
  END
  IF ?dfc:TransitType{Prop:Tip} AND ~?LookupTransitType{Prop:Tip}
     ?LookupTransitType{Prop:Tip} = 'Select ' & ?dfc:TransitType{Prop:Tip}
  END
  IF ?dfc:TransitType{Prop:Msg} AND ~?LookupTransitType{Prop:Msg}
     ?LookupTransitType{Prop:Msg} = 'Select ' & ?dfc:TransitType{Prop:Msg}
  END
  IF ?dfc:Location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?dfc:Location{Prop:Tip}
  END
  IF ?dfc:Location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?dfc:Location{Prop:Msg}
  END
  IF ?dfc:PAccountNumber{Prop:Tip} AND ~?LookupPanasonicAccountNumber{Prop:Tip}
     ?LookupPanasonicAccountNumber{Prop:Tip} = 'Select ' & ?dfc:PAccountNumber{Prop:Tip}
  END
  IF ?dfc:PAccountNumber{Prop:Msg} AND ~?LookupPanasonicAccountNumber{Prop:Msg}
     ?LookupPanasonicAccountNumber{Prop:Msg} = 'Select ' & ?dfc:PAccountNumber{Prop:Msg}
  END
  IF ?dfc:PTransitType{Prop:Tip} AND ~?LookupPanasonicTransitType{Prop:Tip}
     ?LookupPanasonicTransitType{Prop:Tip} = 'Select ' & ?dfc:PTransitType{Prop:Tip}
  END
  IF ?dfc:PTransitType{Prop:Msg} AND ~?LookupPanasonicTransitType{Prop:Msg}
     ?LookupPanasonicTransitType{Prop:Msg} = 'Select ' & ?dfc:PTransitType{Prop:Msg}
  END
  IF ?dfc:PLocation{Prop:Tip} AND ~?LookupPanasonicLocation{Prop:Tip}
     ?LookupPanasonicLocation{Prop:Tip} = 'Select ' & ?dfc:PLocation{Prop:Tip}
  END
  IF ?dfc:PLocation{Prop:Msg} AND ~?LookupPanasonicLocation{Prop:Msg}
     ?LookupPanasonicLocation{Prop:Msg} = 'Select ' & ?dfc:PLocation{Prop:Msg}
  END
  IF ?dfc:EAccountNumber{Prop:Tip} AND ~?LookupEAccountNumber{Prop:Tip}
     ?LookupEAccountNumber{Prop:Tip} = 'Select ' & ?dfc:EAccountNumber{Prop:Tip}
  END
  IF ?dfc:EAccountNumber{Prop:Msg} AND ~?LookupEAccountNumber{Prop:Msg}
     ?LookupEAccountNumber{Prop:Msg} = 'Select ' & ?dfc:EAccountNumber{Prop:Msg}
  END
  IF ?dfc:ETransitType{Prop:Tip} AND ~?LookupETransitType{Prop:Tip}
     ?LookupETransitType{Prop:Tip} = 'Select ' & ?dfc:ETransitType{Prop:Tip}
  END
  IF ?dfc:ETransitType{Prop:Msg} AND ~?LookupETransitType{Prop:Msg}
     ?LookupETransitType{Prop:Msg} = 'Select ' & ?dfc:ETransitType{Prop:Msg}
  END
  IF ?dfc:ELocation{Prop:Tip} AND ~?LookupELocation{Prop:Tip}
     ?LookupELocation{Prop:Tip} = 'Select ' & ?dfc:ELocation{Prop:Tip}
  END
  IF ?dfc:ELocation{Prop:Msg} AND ~?LookupELocation{Prop:Msg}
     ?LookupELocation{Prop:Msg} = 'Select ' & ?dfc:ELocation{Prop:Msg}
  END
  IF ?dfc:SAccountNumber{Prop:Tip} AND ~?CallLookupSonyAccount{Prop:Tip}
     ?CallLookupSonyAccount{Prop:Tip} = 'Select ' & ?dfc:SAccountNumber{Prop:Tip}
  END
  IF ?dfc:SAccountNumber{Prop:Msg} AND ~?CallLookupSonyAccount{Prop:Msg}
     ?CallLookupSonyAccount{Prop:Msg} = 'Select ' & ?dfc:SAccountNumber{Prop:Msg}
  END
  IF ?dfc:STransitType{Prop:Tip} AND ~?CallLookupSonyTransitType{Prop:Tip}
     ?CallLookupSonyTransitType{Prop:Tip} = 'Select ' & ?dfc:STransitType{Prop:Tip}
  END
  IF ?dfc:STransitType{Prop:Msg} AND ~?CallLookupSonyTransitType{Prop:Msg}
     ?CallLookupSonyTransitType{Prop:Msg} = 'Select ' & ?dfc:STransitType{Prop:Msg}
  END
  IF ?dfc:STransitType2{Prop:Tip} AND ~?CallLookupSonyTransitType2{Prop:Tip}
     ?CallLookupSonyTransitType2{Prop:Tip} = 'Select ' & ?dfc:STransitType2{Prop:Tip}
  END
  IF ?dfc:STransitType2{Prop:Msg} AND ~?CallLookupSonyTransitType2{Prop:Msg}
     ?CallLookupSonyTransitType2{Prop:Msg} = 'Select ' & ?dfc:STransitType2{Prop:Msg}
  END
  IF ?dfc:SLocation{Prop:Tip} AND ~?CallLookupSonyLocation{Prop:Tip}
     ?CallLookupSonyLocation{Prop:Tip} = 'Select ' & ?dfc:SLocation{Prop:Tip}
  END
  IF ?dfc:SLocation{Prop:Msg} AND ~?CallLookupSonyLocation{Prop:Msg}
     ?CallLookupSonyLocation{Prop:Msg} = 'Select ' & ?dfc:SLocation{Prop:Msg}
  END
  IF ?VodafoneAcNo{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?VodafoneAcNo{Prop:Tip}
  END
  IF ?VodafoneAcNo{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?VodafoneAcNo{Prop:Msg}
  END
  IF ?VodafoneTransitType{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?VodafoneTransitType{Prop:Tip}
  END
  IF ?VodafoneTransitType{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?VodafoneTransitType{Prop:Msg}
  END
  IF ?VodafoneLocation{Prop:Tip} AND ~?CallLookup:3{Prop:Tip}
     ?CallLookup:3{Prop:Tip} = 'Select ' & ?VodafoneLocation{Prop:Tip}
  END
  IF ?VodafoneLocation{Prop:Msg} AND ~?CallLookup:3{Prop:Msg}
     ?CallLookup:3{Prop:Msg} = 'Select ' & ?VodafoneLocation{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.AddItem(ToolbarForm)
  FileLookup37.Init
  FileLookup37.Flags=BOR(FileLookup37.Flags,FILE:LongName)
  FileLookup37.Flags=BOR(FileLookup37.Flags,FILE:Directory)
  FileLookup37.SetMask('All Files','*.*')
  FileLookup37.WindowTitle='Parceline Export Directory'
  FileLookup38.Init
  FileLookup38.Flags=BOR(FileLookup38.Flags,FILE:LongName)
  FileLookup38.Flags=BOR(FileLookup38.Flags,FILE:Directory)
  FileLookup38.SetMask('All Files','*.*')
  FileLookup38.WindowTitle='Parceline Directory'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
    
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(DEFCRC)
      Access:DEFCRC.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFCRC.Close
    Relate:LOCINTER.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'CRCDefaults',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Sub_Accounts
      Browse_Transit_Types
      Browse_Available_Locations
      Browse_Sub_Accounts
      Browse_Transit_Types
      Browse_Available_Locations
      Browse_Sub_Accounts
      Browse_Transit_Types
      Browse_Available_Locations
      Browse_Sub_Accounts
      Browse_Transit_Types
      Browse_Transit_Types
      Browse_Available_Locations
      Browse_Sub_Accounts
      Browse_Transit_Types
      Browse_Available_Locations
      Browse_Sub_Accounts
      Browse_Transit_Types
      Browse_Available_Locations
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    
    
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      PUTINI('IMPORT','VAccountNumber',VodafoneAcNo,CLIP(PATH()) & '\CRCIMPEXTRA.INI')
      PUTINI('IMPORT','VTransitType',VodafoneTransitType,CLIP(PATH()) & '\CRCIMPEXTRA.INI')
      PUTINI('IMPORT','VLocation',VodafoneLocation,CLIP(PATH()) & '\CRCIMPEXTRA.INI')
      PUTINI('IMPORT','VDirectory',Vodafone_Directory,CLIP(PATH()) & '\CRCIMPEXTRA.INI')
      
      ! Start Change 2649 BE(10/06/03)
      PUTINI('IMPORT_VC','VAccountNumber',VcAcNo,CLIP(PATH()) & '\CRCIMPEXTRA.INI')
      PUTINI('IMPORT_VC','VTransitType',VcTransitType,CLIP(PATH()) & '\CRCIMPEXTRA.INI')
      PUTINI('IMPORT_VC','VLocation',VcLocation,CLIP(PATH()) & '\CRCIMPEXTRA.INI')
      PUTINI('IMPORT_VC','VDirectory',Vc_Directory,CLIP(PATH()) & '\CRCIMPEXTRA.INI')
      ! End Change 2649 BE(10/06/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?dfc:AccountNumber
      IF dfc:AccountNumber OR ?dfc:AccountNumber{Prop:Req}
        sub:Account_Number = dfc:AccountNumber
        !Save Lookup Field Incase Of error
        look:dfc:AccountNumber        = dfc:AccountNumber
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            dfc:AccountNumber = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            dfc:AccountNumber = look:dfc:AccountNumber
            SELECT(?dfc:AccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupAccountNumber
      ThisWindow.Update
      sub:Account_Number = dfc:AccountNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          dfc:AccountNumber = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?dfc:AccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:AccountNumber)
    OF ?dfc:TransitType
      IF dfc:TransitType OR ?dfc:TransitType{Prop:Req}
        trt:Transit_Type = dfc:TransitType
        !Save Lookup Field Incase Of error
        look:dfc:TransitType        = dfc:TransitType
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            dfc:TransitType = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            dfc:TransitType = look:dfc:TransitType
            SELECT(?dfc:TransitType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupTransitType
      ThisWindow.Update
      trt:Transit_Type = dfc:TransitType
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          dfc:TransitType = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?dfc:TransitType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:TransitType)
    OF ?dfc:Location
      IF dfc:Location OR ?dfc:Location{Prop:Req}
        loi:Location = dfc:Location
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:dfc:Location        = dfc:Location
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            dfc:Location = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            dfc:Location = look:dfc:Location
            SELECT(?dfc:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = dfc:Location
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          dfc:Location = loi:Location
          Select(?+1)
      ELSE
          Select(?dfc:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:Location)
    OF ?dfc:EAccountNumber
      IF dfc:EAccountNumber OR ?dfc:EAccountNumber{Prop:Req}
        sub:Account_Number = dfc:EAccountNumber
        !Save Lookup Field Incase Of error
        look:dfc:EAccountNumber        = dfc:EAccountNumber
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            dfc:EAccountNumber = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            dfc:EAccountNumber = look:dfc:EAccountNumber
            SELECT(?dfc:EAccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupEAccountNumber
      ThisWindow.Update
      sub:Account_Number = dfc:EAccountNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          dfc:EAccountNumber = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?dfc:EAccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:EAccountNumber)
    OF ?dfc:ETransitType
      IF dfc:ETransitType OR ?dfc:ETransitType{Prop:Req}
        trt:Transit_Type = dfc:ETransitType
        !Save Lookup Field Incase Of error
        look:dfc:ETransitType        = dfc:ETransitType
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            dfc:ETransitType = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            dfc:ETransitType = look:dfc:ETransitType
            SELECT(?dfc:ETransitType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupETransitType
      ThisWindow.Update
      trt:Transit_Type = dfc:ETransitType
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          dfc:ETransitType = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?dfc:ETransitType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:ETransitType)
    OF ?dfc:ELocation
      IF dfc:ELocation OR ?dfc:ELocation{Prop:Req}
        loi:Location = dfc:ELocation
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:dfc:ELocation        = dfc:ELocation
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            dfc:ELocation = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            dfc:ELocation = look:dfc:ELocation
            SELECT(?dfc:ELocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupELocation
      ThisWindow.Update
      loi:Location = dfc:ELocation
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          dfc:ELocation = loi:Location
          Select(?+1)
      ELSE
          Select(?dfc:ELocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:ELocation)
    OF ?dfc:PAccountNumber
      IF dfc:PAccountNumber OR ?dfc:PAccountNumber{Prop:Req}
        sub:Account_Number = dfc:PAccountNumber
        !Save Lookup Field Incase Of error
        look:dfc:PAccountNumber        = dfc:PAccountNumber
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            dfc:PAccountNumber = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            dfc:PAccountNumber = look:dfc:PAccountNumber
            SELECT(?dfc:PAccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupPanasonicAccountNumber
      ThisWindow.Update
      sub:Account_Number = dfc:PAccountNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          dfc:PAccountNumber = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?dfc:PAccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:PAccountNumber)
    OF ?dfc:PTransitType
      IF dfc:PTransitType OR ?dfc:PTransitType{Prop:Req}
        trt:Transit_Type = dfc:PTransitType
        !Save Lookup Field Incase Of error
        look:dfc:PTransitType        = dfc:PTransitType
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            dfc:PTransitType = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            dfc:PTransitType = look:dfc:PTransitType
            SELECT(?dfc:PTransitType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupPanasonicTransitType
      ThisWindow.Update
      trt:Transit_Type = dfc:PTransitType
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          dfc:PTransitType = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?dfc:PTransitType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:PTransitType)
    OF ?dfc:PLocation
      IF dfc:PLocation OR ?dfc:PLocation{Prop:Req}
        loi:Location = dfc:PLocation
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:dfc:PLocation        = dfc:PLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            dfc:PLocation = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            dfc:PLocation = look:dfc:PLocation
            SELECT(?dfc:PLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupPanasonicLocation
      ThisWindow.Update
      loi:Location = dfc:PLocation
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          dfc:PLocation = loi:Location
          Select(?+1)
      ELSE
          Select(?dfc:PLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:PLocation)
    OF ?dfc:SAccountNumber
      IF dfc:SAccountNumber OR ?dfc:SAccountNumber{Prop:Req}
        sub:Account_Number = dfc:SAccountNumber
        !Save Lookup Field Incase Of error
        look:dfc:SAccountNumber        = dfc:SAccountNumber
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            dfc:SAccountNumber = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            dfc:SAccountNumber = look:dfc:SAccountNumber
            SELECT(?dfc:SAccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookupSonyAccount
      ThisWindow.Update
      sub:Account_Number = dfc:SAccountNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          dfc:SAccountNumber = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?dfc:SAccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:SAccountNumber)
    OF ?dfc:STransitType
      IF dfc:STransitType OR ?dfc:STransitType{Prop:Req}
        trt:Transit_Type = dfc:STransitType
        !Save Lookup Field Incase Of error
        look:dfc:STransitType        = dfc:STransitType
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            dfc:STransitType = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            dfc:STransitType = look:dfc:STransitType
            SELECT(?dfc:STransitType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookupSonyTransitType
      ThisWindow.Update
      trt:Transit_Type = dfc:STransitType
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          dfc:STransitType = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?dfc:STransitType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:STransitType)
    OF ?dfc:STransitType2
      IF dfc:STransitType2 OR ?dfc:STransitType2{Prop:Req}
        trt:Transit_Type = dfc:STransitType2
        !Save Lookup Field Incase Of error
        look:dfc:STransitType2        = dfc:STransitType2
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            dfc:STransitType2 = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            dfc:STransitType2 = look:dfc:STransitType2
            SELECT(?dfc:STransitType2)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookupSonyTransitType2
      ThisWindow.Update
      trt:Transit_Type = dfc:STransitType2
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          dfc:STransitType2 = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?dfc:STransitType2)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:STransitType2)
    OF ?dfc:SLocation
      IF dfc:SLocation OR ?dfc:SLocation{Prop:Req}
        loi:Location = dfc:SLocation
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:dfc:SLocation        = dfc:SLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            dfc:SLocation = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            dfc:SLocation = look:dfc:SLocation
            SELECT(?dfc:SLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookupSonyLocation
      ThisWindow.Update
      loi:Location = dfc:SLocation
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          dfc:SLocation = loi:Location
          Select(?+1)
      ELSE
          Select(?dfc:SLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dfc:SLocation)
    OF ?VodafoneAcNo
      IF VodafoneAcNo OR ?VodafoneAcNo{Prop:Req}
        sub:Account_Number = VodafoneAcNo
        !Save Lookup Field Incase Of error
        look:VodafoneAcNo        = VodafoneAcNo
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            VodafoneAcNo = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            VodafoneAcNo = look:VodafoneAcNo
            SELECT(?VodafoneAcNo)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      sub:Account_Number = VodafoneAcNo
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          VodafoneAcNo = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?VodafoneAcNo)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?VodafoneAcNo)
    OF ?VodafoneTransitType
      IF VodafoneTransitType OR ?VodafoneTransitType{Prop:Req}
        trt:Transit_Type = VodafoneTransitType
        !Save Lookup Field Incase Of error
        look:VodafoneTransitType        = VodafoneTransitType
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            VodafoneTransitType = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            VodafoneTransitType = look:VodafoneTransitType
            SELECT(?VodafoneTransitType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update
      trt:Transit_Type = VodafoneTransitType
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          VodafoneTransitType = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?VodafoneTransitType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?VodafoneTransitType)
    OF ?VodafoneLocation
      IF VodafoneLocation OR ?VodafoneLocation{Prop:Req}
        loi:Location = VodafoneLocation
        !Save Lookup Field Incase Of error
        look:VodafoneLocation        = VodafoneLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            VodafoneLocation = loi:Location
          ELSE
            !Restore Lookup On Error
            VodafoneLocation = look:VodafoneLocation
            SELECT(?VodafoneLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:3
      ThisWindow.Update
      loi:Location = VodafoneLocation
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          VodafoneLocation = loi:Location
          Select(?+1)
      ELSE
          Select(?VodafoneLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?VodafoneLocation)
    OF ?LookupFile
      ThisWindow.Update
      VodaFone_Directory = Upper(FileLookup37.Ask(1)  )
      DISPLAY
    OF ?VcAcNo
      IF VcAcNo OR ?VcAcNo{Prop:Req}
        sub:Account_Number = VcAcNo
        !Save Lookup Field Incase Of error
        look:VcAcNo        = VcAcNo
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            VcAcNo = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            VcAcNo = look:VcAcNo
            SELECT(?VcAcNo)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:4
      ThisWindow.Update
      sub:Account_Number = VcAcNo
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        VcAcNo = sub:Account_Number
      END
      ThisWindow.Reset(1)
    OF ?VcTransitType
      IF VcTransitType OR ?VcTransitType{Prop:Req}
        trt:Transit_Type = VcTransitType
        !Save Lookup Field Incase Of error
        look:VcTransitType        = VcTransitType
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            VcTransitType = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            VcTransitType = look:VcTransitType
            SELECT(?VcTransitType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:5
      ThisWindow.Update
      trt:Transit_Type = VcTransitType
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        VcTransitType = trt:Transit_Type
      END
      ThisWindow.Reset(1)
    OF ?VcLocation
      IF VcLocation OR ?VcLocation{Prop:Req}
        loi:Location = VcLocation
        !Save Lookup Field Incase Of error
        look:VcLocation        = VcLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            VcLocation = loi:Location
          ELSE
            !Restore Lookup On Error
            VcLocation = look:VcLocation
            SELECT(?VcLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:6
      ThisWindow.Update
      loi:Location = VcLocation
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        VcLocation = loi:Location
      END
      ThisWindow.Reset(1)
    OF ?LookupFile:2
      ThisWindow.Update
      Vc_Directory = Upper(FileLookup38.Ask(1)  )
      DISPLAY
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'CRCDefaults')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?dfc:AccountNumber
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:AccountNumber, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Account Number')
             Post(Event:Accepted,?LookupAccountNumber)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupAccountNumber)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:AccountNumber, AlertKey)
    END
  OF ?dfc:TransitType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:TransitType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Transit Type')
             Post(Event:Accepted,?LookupTransitType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupTransitType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:TransitType, AlertKey)
    END
  OF ?dfc:Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:Location, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Location')
             Post(Event:Accepted,?LookupLocation)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupLocation)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:Location, AlertKey)
    END
  OF ?dfc:EAccountNumber
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:EAccountNumber, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Account Number')
             Post(Event:Accepted,?LookupEAccountNumber)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupEAccountNumber)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:EAccountNumber, AlertKey)
    END
  OF ?dfc:ETransitType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:ETransitType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Transit Type')
             Post(Event:Accepted,?LookupETransitType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupETransitType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:ETransitType, AlertKey)
    END
  OF ?dfc:ELocation
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:ELocation, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Locatino')
             Post(Event:Accepted,?LookupELocation)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupELocation)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:ELocation, AlertKey)
    END
  OF ?dfc:PAccountNumber
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:PAccountNumber, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Account Number')
             Post(Event:Accepted,?LookupPanasonicAccountNumber)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupPanasonicAccountNumber)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:PAccountNumber, AlertKey)
    END
  OF ?dfc:PTransitType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:PTransitType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Transit Type')
             Post(Event:Accepted,?LookupPanasonicTransitType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupPanasonicTransitType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:PTransitType, AlertKey)
    END
  OF ?dfc:PLocation
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:PLocation, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Location')
             Post(Event:Accepted,?LookupPanasonicLocation)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupPanasonicLocation)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?dfc:PLocation, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

