

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF02017.INC'),ONCE        !Local module procedure declarations
                     END


ImportSamsung PROCEDURE                               !Generated from procedure template - Window

IniFilename          STRING(100)
SamDefaults          GROUP,PRE(sam)
Account_Number       STRING(15)
Warranty_Charge_Type STRING(30)
Chargeable_Charge_Type STRING(30)
Transit_Type         STRING(30)
Job_Priority         STRING(30)
Unit_Type            STRING(30)
Status_Type          STRING(30)
Delivery_text        STRING(1000)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Jobs Import (Samsung)'),AT(,,247,91),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,240,56),USE(?Sheet1),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Select the file you wish to import'),AT(8,8,140,12),USE(?Prompt1),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Import File Name'),AT(9,24),USE(?tmp:FileName:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,24,140,10),USE(filename3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Import File Name'),TIP('Import File Name'),UPR
                           BUTTON,AT(228,24,10,10),USE(?LookupFileName),SKIP,ICON('List3.ico')
                           PROMPT('0 Records Imported'),AT(84,44),USE(?CountPrompt),HIDE
                         END
                       END
                       PANEL,AT(4,64,240,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Import Jobs'),AT(128,68,56,16),USE(?ImportButton),LEFT,ICON('DISK.GIF')
                       BUTTON('Cancel'),AT(184,68,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup4          SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ImportFile      FILE,DRIVER('BASIC'),PRE(imp),NAME(filename3),CREATE,BINDABLE,THREAD
Record              Record
ModelNumber             String(30)  ! MODEL
CustomerTitle           String(4)   ! Title
CustomerInitial         String(1)   ! Initial
CustomerSurname         String(30)  ! Surname
CustomerContactNo       String(15)  ! Telephone_Number
IMEI                    String(20)  ! ESN
MSN                     String(20)  ! MSN
DOP                     String(10)  ! DOP
CustomerAddressLine1    String(30)  ! Address_Line1
CustomerAddressLine2    String(30)  ! Address_Line2
CustomerAddressLine3    String(30)  ! Address_Line3
CustomerPostcode        String(10)  ! Postcode
SpecialInstructions     String(255) ! Special Instructions
ClientReference         String(30)  !
Retailer                String(30)  ! Retailer
SerialNumber            String(30)  ! S/N Number
IsWarranty              String(1)   ! Charge
                    End
                End


!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:FileName:Prompt{prop:FontColor} = -1
    ?tmp:FileName:Prompt{prop:Color} = 15066597
    If ?filename3{prop:ReadOnly} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 15066597
    Elsif ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 8454143
    Else ! If ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 16777215
    End ! If ?filename3{prop:Req} = True
    ?filename3{prop:Trn} = 0
    ?filename3{prop:FontStyle} = font:Bold
    ?CountPrompt{prop:FontColor} = -1
    ?CountPrompt{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ImportSamsung',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('IniFilename',IniFilename,'ImportSamsung',1)
    SolaceViewVars('SamDefaults:Account_Number',SamDefaults:Account_Number,'ImportSamsung',1)
    SolaceViewVars('SamDefaults:Warranty_Charge_Type',SamDefaults:Warranty_Charge_Type,'ImportSamsung',1)
    SolaceViewVars('SamDefaults:Chargeable_Charge_Type',SamDefaults:Chargeable_Charge_Type,'ImportSamsung',1)
    SolaceViewVars('SamDefaults:Transit_Type',SamDefaults:Transit_Type,'ImportSamsung',1)
    SolaceViewVars('SamDefaults:Job_Priority',SamDefaults:Job_Priority,'ImportSamsung',1)
    SolaceViewVars('SamDefaults:Unit_Type',SamDefaults:Unit_Type,'ImportSamsung',1)
    SolaceViewVars('SamDefaults:Status_Type',SamDefaults:Status_Type,'ImportSamsung',1)
    SolaceViewVars('SamDefaults:Delivery_text',SamDefaults:Delivery_text,'ImportSamsung',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:FileName:Prompt;  SolaceCtrlName = '?tmp:FileName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?filename3;  SolaceCtrlName = '?filename3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFileName;  SolaceCtrlName = '?LookupFileName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CountPrompt;  SolaceCtrlName = '?CountPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ImportButton;  SolaceCtrlName = '?ImportButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ImportSamsung')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ImportSamsung')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFCRC.Open
  Relate:JOBS2_ALIAS.Open
  Relate:LETTERS.Open
  Relate:MERGELET.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:MODELNUM.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FileLookup4.Init
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:LongName)
  FileLookup4.SetMask('Samsung Jobs File','*.CSV')
  FileLookup4.WindowTitle='Samsung Jobs Import File'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFCRC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:LETTERS.Close
    Relate:MERGELET.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ImportSamsung',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFileName
      ThisWindow.Update
      filename3 = Upper(FileLookup4.Ask(1)  )
      DISPLAY
    OF ?ImportButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
      CASE MessageEx('Are you sure you want to import the selected file?','ServiceBase 2000', |
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0, |
                     beep:systemquestion,msgex:samewidths,84,26,0)
          OF 1 ! Yes Button
              ! get defaults from ini file
              IniFilename = PATH() & '\SAMSUNGIMP.INI'
              sam:Account_Number = GETINI('Defaults', 'Account_Number','',IniFilename)
              sam:Warranty_Charge_Type = GETINI('Defaults', 'Warranty_Charge_Type','',IniFilename)
              sam:Chargeable_Charge_Type = GETINI('Defaults', 'Chargeable_Charge_Type','',IniFilename)
              sam:Transit_Type = GETINI('Defaults', 'Transit_Type','',IniFilename)
              sam:Job_Priority = GETINI('Defaults', 'Job_Priority','',IniFilename)
              sam:Unit_Type = GETINI('Defaults', 'Unit_Type','',IniFilename)
              sam:Status_Type = GETINI('Defaults', 'Status_Type','',IniFilename)
              sam:Delivery_text = GETINI('Defaults', 'Delivery_text','',IniFilename)
      
              IF (CLIP(sam:Account_Number) = '') THEN
                  MessageEx('You have not setup any import defaults.','ServiceBase 2000',|
                            'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0, |
                             beep:systemhand,msgex:samewidths,84,26,0)
              ELSE
                  Open(IMPORTFILE)
                  IF (Error()) THEN
                      MessageEx('Cannot open the import file.' & |
                                '<13,10>' & |
                                '<13,10>(' & Error() & ')','ServiceBase 2000', |
                                'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0, |
                                beep:systemhand,msgex:samewidths,84,26,0)
                  ELSE
                     SETCURSOR(cursor:wait)
                      SET(IMPORTFILE,0)
                      RecordNo# = 0
                      UNHIDE(?CountPrompt)
                      LOOP
                          Next(IMPORTFILE)
                          IF (ERROR()) THEN
                              BREAK
                          END
      
                          IF (RecordNo# = 0) THEN
                              RecordNo# = 1
                              ! Start 2458 Reprise BE(07/05/03)
                              !IF (UPPER(CLIP(imp:ModelNumber)) = 'MODEL') THEN
                              !    ! ignore first record if it consists of column headings
                              !    CYCLE
                              !END
                              CYCLE
                              ! End 2458 Reprise BE(07/05/03)
                          END
      
                          ?CountPrompt{PROP:Text} = FORMAT(RecordNo#, @n8) & ' Records Imported'
                          DISPLAY(?CountPrompt)
                          RecordNo# += 1
      
                          IF (access:jobs.primerecord() = level:benign) THEN
      
                              access:USERS.clearkey(use:password_key)
                              use:password = glo:password
                              IF (access:USERS.fetch(use:password_key) = Level:Benign) THEN
                                  usercode" = use:user_code
                                  job:who_booked = use:user_code
                              END
      
                              job:date_booked                   = Today()
                              job:time_booked                   = Clock()
      
                              access:MODELNUM.clearkey(mod:model_number_key)
                              mod:model_number = CLIP(imp:ModelNumber)
                              IF (access:MODELNUM.fetch(mod:model_number_key) = Level:Benign) THEN
                                  job:manufacturer = mod:manufacturer
                                  job:model_number = mod:model_number
                              ELSE
                                  job:manufacturer = 'SAMSUNG'
                              END
      
                              IF (UPPER(imp:IsWarranty) = 'W') THEN
                                  job:warranty_job         = 'YES'
                                  job:chargeable_job       = 'NO'
                                  job:warranty_charge_type = CLIP(sam:warranty_charge_type)
                              ELSIF (UPPER(imp:IsWarranty) = 'C') THEN
                                  job:chargeable_job       = 'YES'
                                  job:warranty_job         = 'NO'
                                  job:charge_type          = CLIP(sam:chargeable_charge_type)
                                  access:CHARTYPE.clearkey(cha:charge_type_key)
                                  cha:charge_type = job:charge_type
                                  IF (access:CHARTYPE.fetch(cha:charge_type_key) = Level:Benign) THEN
                                      IF ((cha:allow_estimate = 'YES') AND (cha:force_estimate = 'YES')) THEN
                                              job:estimate = 'YES'
                                      END
                                 END
                              END
      
                              job:dop                           = (deformat(imp:Dop,@d6))
                              job:esn                           = CLIP(imp:IMEI)
                              job:msn                           = CLIP(imp:MSN)
                              job:order_number                  = CLIP(imp:ClientReference)
                              job:title                         = CLIP(imp:CustomerTitle)
                              job:initial                       = CLIP(imp:CustomerInitial)
                              job:surname                       = CLIP(imp:CustomerSurname)
                              job:fault_code3                   = CLIP(imp:SerialNumber)
      
                              ! Start Code 2458/2 BE(07/05/03)
                              !job:fault_code4                   = CLIP(imp:Retailer)
                              job:fault_code8                   = CLIP(imp:Retailer)
                              ! End Code 2458/2 BE(07/05/03)
      
                              !job:edi                           = 'EDI'
      
                              job:Address_Line1                 = CLIP(imp:CustomerAddressLine1)
                              job:Address_Line2                 = CLIP(imp:CustomerAddressLine2)
                              job:Address_Line3                 = CLIP(imp:CustomerAddressLine3)
                              job:Postcode                      = CLIP(imp:CustomerPostcode)
                              job:Telephone_Number              = CLIP(imp:CustomerContactNo)
      
                              job:Address_Line1_Delivery        = CLIP(imp:CustomerAddressLine1)
                              job:Address_Line2_Delivery        = CLIP(imp:CustomerAddressLine2)
                              job:Address_Line3_Delivery        = CLIP(imp:CustomerAddressLine3)
                              job:Postcode_Delivery             = CLIP(imp:CustomerPostcode)
                              job:Telephone_Delivery            = CLIP(imp:CustomerContactNo)
      
                              job:account_number                = CLIP(sam:account_number)
                              job:transit_type                  = CLIP(sam:transit_type)
                              job:turnaround_time               = CLIP(sam:job_priority)
                              job:unit_type                     = CLIP(sam:unit_type)
      
                              IF (access:JOBNOTES.primerecord() = Level:Benign) THEN
                                  jbn:RefNumber = job:ref_number
                                  jbn:fault_description = imp:SpecialInstructions
                                  jbn:delivery_text = sam:delivery_text
                                  IF (access:JOBNOTES.tryinsert() <> Level:benign) THEN
                                      access:JOBNOTES.cancelautoinc()
                                  END
                              END
      
                              IF (CLIP(sam:status_Type) <> '') THEN
                                  access:status.clearkey(sts:status_key)
                                  sts:status = CLIP(sam:status_Type)
                                  IF (access:status.fetch(sts:status_key) = Level:Benign) THEN
                                      GetStatus(sts:ref_number,1,'JOB')
                                  END
                              ELSE
                                  GetStatus(0,1,'JOB')
                              END
              
                              GET(audit,0)
                              IF (access:audit.primerecord() = level:benign) THEN
                                  aud:notes         = 'JOB AUTOMATICALLY CREATED THROUGH THE SAMSUNG IMPORT ROUTINE'
                                  aud:ref_number    = job:ref_number
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  aud:user          = CLIP(usercode")
                                  aud:action        = 'IMPORT ROUTINE'
                                  aud:type          = 'JOB'
                                  IF (access:audit.insert() <> Level:Benign) THEN
                                      access:audit.cancelautoinc()
                                  END
                              END
      
                              IF (access:jobs.insert() <> Level:Benign) THEN
                                  access:jobs.cancelautoinc()
                              END
                          END
                      END
                  END
                  SETCURSOR()
                  CLOSE(IMPORTFILE)
                  MessageEx('Import Completed.','ServiceBase 2000',|
                                 'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ImportSamsung')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

