

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF02013.INC'),ONCE        !Local module procedure declarations
                     END


IntecImport PROCEDURE                                 !Generated from procedure template - Window

tmp:FileName         STRING(255)
BouncerList          QUEUE,PRE(bounce)
RefNumber            LONG
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Jobs Import'),AT(,,247,75),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,240,40),USE(?Sheet1),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Select the file you wish to import'),AT(8,8,140,12),USE(?Prompt1),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(228,24,10,10),USE(?LookupFileName),SKIP,ICON('List3.ico')
                           PROMPT('Import File Name'),AT(9,24),USE(?tmp:FileName:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,24,140,10),USE(filename3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Import File Name'),TIP('Import File Name'),UPR
                         END
                       END
                       PANEL,AT(4,48,240,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Import Jobs'),AT(128,52,56,16),USE(?Button3),LEFT,ICON('DISK.GIF')
                       BUTTON('Cancel'),AT(184,52,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup4          SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ImportFile      FILE,DRIVER('BASIC'),PRE(imp),NAME(filename3),CREATE,BINDABLE,THREAD
Record              Record
OrderNumber             String(30)
AuthorityNumber         String(30)
InsuranceRefNo          String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
AccountNumber           String(30)
Title                   String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
Initial                 String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
Surname                 String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
AddressLine1            String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
AddressLine2            String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
AddressLine3            String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
Postcode                String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
TelephoneNumber         String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
MobileNumber            String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
Manufacturer            String(30)
ModelNumber             String(30)
Colour                  String(30)
IMEINumber              String(30)
MSN                     String(30)
UnitType                String(30) !New Fields - TrkBs: 2264 (DBH: 08-12-2004)
DOP                     String(30)
FaultDescription        String(255)
TransitType             String(30)
CChargeType             String(30)
WChargeType             String(30)
                    End
                End

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:FileName:Prompt{prop:FontColor} = -1
    ?tmp:FileName:Prompt{prop:Color} = 15066597
    If ?filename3{prop:ReadOnly} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 15066597
    Elsif ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 8454143
    Else ! If ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 16777215
    End ! If ?filename3{prop:Req} = True
    ?filename3{prop:Trn} = 0
    ?filename3{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'IntecImport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:FileName',tmp:FileName,'IntecImport',1)
    SolaceViewVars('BouncerList:RefNumber',BouncerList:RefNumber,'IntecImport',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFileName;  SolaceCtrlName = '?LookupFileName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:FileName:Prompt;  SolaceCtrlName = '?tmp:FileName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?filename3;  SolaceCtrlName = '?filename3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('IntecImport')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'IntecImport')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:JOBS2_ALIAS.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:MODELNUM.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FileLookup4.Init
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:LongName)
  FileLookup4.SetMask('CSV File','*.CSV')
  FileLookup4.WindowTitle='Jobs Import File'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:JOBS2_ALIAS.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'IntecImport',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFileName
      ThisWindow.Update
      filename3 = Upper(FileLookup4.Ask(1)  )
      DISPLAY
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      Case MessageEx('Are you sure you want to import the selected file?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Clear(BouncerList)
              Free(BouncerList)
              error# = 0
      
              If error# = 0
      
                  Open(IMPORTFILE)
                  If Error()
                      Case MessageEx('Cannot open the import file.'&|
                        '<13,10>'&|
                        '<13,10>(' & Error() & ')','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If Error()
                      Setcursor(cursor:wait)
                      Set(IMPORTFILE,0)
                      Loop
                          Next(IMPORTFILE)
                          If Error()
                              Break
                          End!If Error()
                          If CLIP(UPPER(imp:modelnumber)) = 'MODEL_NUMBER'
                              Cycle
                          End!If imp:RaisedBy <> 'IMPORT'
                          if access:jobs.primerecord() = level:benign
                              !call the status routine
                              GetStatus(0,1,'JOB')
      
                              job:Order_Number              = Clip(Upper(imp:OrderNumber))
                              job:esn                       = Clip(Upper(imp:IMEINumber))
                              job:Model_Number              = Clip(Upper(imp:ModelNumber))
                              job:Colour                    = Clip(Upper(imp:Colour))
                              job:DOP                       = Deformat(imp:DOP,@d6)
                              job:Authority_Number          = Clip(Upper(imp:AuthorityNumber))
                              job:MSN                       = Clip(Upper(imp:MSN))

                              !Start - New Fields - TrkBs: 2265 (DBH: 08-12-2004)
                              !I presume the below "use trade as customer address" rules would still apply - TrkBs: 2265 (DBH: 08-12-2004)
                              job:Insurance_Reference_Number    = Clip(Upper(imp:InsuranceRefNo))
                              job:Title                     = Clip(Upper(imp:Title))
                              job:Initial                   = Clip(Upper(imp:Initial))
                              job:Surname                   = Clip(Upper(imp:Surname))
                              job:Postcode                  = Clip(Upper(imp:Postcode))
                              job:Address_Line1             = Clip(Upper(imp:AddressLine1))
                              job:Address_Line2             = Clip(Upper(imp:AddressLine2))
                              job:Address_Line3             = Clip(Upper(imp:AddressLine3))
                              job:Telephone_Number          = Clip(Upper(imp:TelephoneNumber))
                              job:Mobile_Number             = Clip(Upper(imp:MobileNumber))
                              job:Unit_Type                 = Clip(Upper(imp:UnitType))
                              !Collection & Delivery addresses should be the same - TrkBs: 2264 (DBH: 08-12-2004)
                              job:Postcode_Collection       = job:Postcode
                              job:Address_Line1_Collection  = job:Address_Line1
                              job:Address_Line2_Collection  = job:Address_Line2
                              job:Address_Line3_Collection  = job:Address_Line3
                              job:Telephone_Collection      = job:Telephone_Number
                              job:Postcode_Delivery         = job:Postcode
                              job:Address_Line1_Delivery    = job:Address_Line1
                              job:Address_Line2_Delivery    = job:Address_Line2
                              job:Address_Line3_Delivery    = job:Address_Line3
                              job:Telephone_Delivery        = job:Telephone_Number
                              !End   - New Fields - TrkBs: 2265 (DBH: 08-12-2004)

                              Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                              mod:Model_Number = job:Model_Number
                              If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                  job:Manufacturer = mod:Manufacturer
                                  If mod:product_Type <> ''
                                      job:fault_code1 = mod:product_type
                                  End!If job:product_Type <> ''
      
                              End!If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      
                              If imp:CChargeType <> ''
                                  job:Chargeable_Job = 'YES'
                                  job:Charge_Type    = Clip(Upper(imp:CChargeType))
                              Else !If imp:CChargeType <> ''
                                  job:Chargeable_job = 'NO'
                              End !If imp:CChargeType <> ''
                              If imp:WChargeType <> ''
                                  job:Warranty_job = 'YES'
                                  job:Warranty_Charge_Type = Clip(Upper(imp:WChargeType))
                              Else !If imp:WChargeType <> ''
                                  job:Warranty_Job = 'NO'
                              End !If imp:WChargeType <> ''
      
                              job:account_number      = Clip(Upper(imp:AccountNumber))
                              despatch# = 0
                              access:subtracc.clearkey(sub:account_number_key)
                              sub:account_number = job:account_number
                              if access:subtracc.fetch(sub:account_number_key) = level:benign
                                  access:tradeacc.clearkey(tra:account_number_key) 
                                  tra:account_number = sub:main_account_number
                                  if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                      IF tra:invoice_sub_accounts = 'YES'
                                          job:courier_cost        = sub:courier_cost
                                          job:courier_cost_warranty = sub:courier_cost
                                      Else!IF tra:invoice_sub_accounts = 'YES'
                                          job:courier_cost        = tra:courier_cost
                                          job:courier_cost_warranty = sub:courier_cost
                                      End!IF tra:invoice_sub_accounts = 'YES'
                                      if tra:use_sub_accounts = 'YES'
                                          job:courier = sub:courier_outgoing
                                          job:incoming_courier = sub:courier_incoming
                                          job:exchange_courier = job:courier
                                          job:loan_courier = job:courier
                                          If sub:despatch_paid_jobs <> 'YES' and sub:despatch_invoiced_jobs <> 'YES'
                                              despatch# = 1
                                          End!If sub:despatch_paid_jobs <> 'YES' and sub:despatch_invoiced_jobs <> 'YES'
                                      else!if tra:use_sub_accounts = 'YES'
                                          job:courier = tra:courier_outgoing
                                          job:incoming_courier = tra:courier_incoming
                                          job:exchange_courier = job:courier
                                          job:loan_courier = job:courier
                                          If tra:despatch_paid_jobs <> 'YES' and tra:despatch_invoiced_jobs <> 'YES'
                                              despatch# = 1
                                          End!If tra:despatch_paid_jobs <> 'YES' and tra:despatch_invoiced_jobs <> 'YES'
                                      end!if tra:use_sub_accounts = 'YES'
                                      If tra:invoice_sub_accounts  <> 'YES'
                                          If tra:use_customer_address <> 'YES'
                                              JOB:Postcode            = tra:Postcode
                                              JOB:Company_Name        = tra:Company_Name
                                              JOB:Address_Line1       = tra:Address_Line1
                                              JOB:Address_Line2       = tra:Address_Line2
                                              JOB:Address_Line3       = tra:Address_Line3
                                              JOB:Telephone_Number    = tra:Telephone_Number
                                              JOB:Fax_Number          = tra:Fax_Number
                                          End!If tra:use_customer_address <> 'YES'
                                      Else!If tra:invoice_sub_accounts  <> 'YES'
                                          If sub:use_customer_address <> 'YES'
                                              JOB:Postcode            = sub:Postcode
                                              JOB:Company_Name        = sub:Company_Name
                                              JOB:Address_Line1       = sub:Address_Line1
                                              JOB:Address_Line2       = sub:Address_Line2
                                              JOB:Address_Line3       = sub:Address_Line3
                                              JOB:Telephone_Number    = sub:Telephone_Number
                                              JOB:Fax_Number          = sub:Fax_Number
                                          End!If sub:use_customer_address <> 'YES'
                                      End!If tra:invoice_sub_accounts  <> 'YES'
                                      If tra:use_sub_accounts <> 'YES'
                                          If tra:use_delivery_address = 'YES'
                                              job:postcode_delivery   = tra:postcode
                                              job:company_name_delivery   = tra:company_name
                                              job:address_line1_delivery  = tra:address_line1
                                              job:address_line2_delivery  = tra:address_line2
                                              job:address_line3_delivery  = tra:address_line3
                                              job:telephone_delivery      = tra:telephone_number
                                          End !If tra:use_delivery_address = 'YES'
                                          If tra:use_collection_address = 'YES'
                                              job:postcode_collection   = tra:postcode
                                              job:company_name_collection   = tra:company_name
                                              job:address_line1_collection  = tra:address_line1
                                              job:address_line2_collection  = tra:address_line2
                                              job:address_line3_collection  = tra:address_line3
                                              job:telephone_collection      = tra:telephone_number
                                          End !If tra:use_delivery_address = 'YES'
                                      Else
                                          If sub:use_delivery_address = 'YES'
                                              job:postcode_delivery      = sub:postcode
                                              job:company_name_delivery  = sub:company_name
                                              job:address_line1_delivery = sub:address_line1
                                              job:address_line2_delivery = sub:address_line2
                                              job:address_line3_delivery = sub:address_line3
                                              job:telephone_delivery     = sub:telephone_number
                                          End
                                          If sub:use_collection_address = 'YES'
                                              job:postcode_collection      = sub:postcode
                                              job:company_name_collection  = sub:company_name
                                              job:address_line1_collection = sub:address_line1
                                              job:address_line2_collection = sub:address_line2
                                              job:address_line3_collection = sub:address_line3
                                              job:telephone_collection     = sub:telephone_number
                                          End
                                      End
      
                                  end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                              end!if access:subtracc.fetch(sub:account_number_key) = level:benign
                              job:Transit_Type        = Clip(Upper(imp:TransitType))
                              access:trantype.clearkey(trt:transit_type_key)
                              trt:transit_type = job:transit_type
                              if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                                  IF trt:InWorkshop = 'YES'
                                      job:workshop = 'YES'
                                  End!IF trt:InWorkshop = 'YES'
                                  GetStatus(Sub(trt:initial_status,1,3),InsertRecord,'JOB')
                                  GetStatus(Sub(trt:Exchangestatus,1,3),InsertRecord,'EXC')
                                  GetStatus(Sub(trt:Loanstatus,1,3),InsertRecord,'LOA')
                              end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
      
                              access:users.clearkey(use:password_key)
                              use:password    =glo:password
                              If access:users.fetch(use:password_key) = Level:Benign
                                  job:who_booked                    = use:user_code
                              End
                              If job:warranty_job = 'YES'
                                  job:edi = Set_Edi(job:manufacturer)
                              end
      
                              If Access:JOBNOTES.PrimeRecord() = Level:Benign
                                  jbn:RefNumber  = job:Ref_Number
                                  jbn:Fault_Description   = Clip(Upper(imp:FaultDescription))
                                  If Access:JOBNOTES.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:JOBNOTES.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:JOBNOTES.TryInsert() = Level:Benign
                              End !If Access:JOBNOTES.PrimeRecord() = Level:Benign
      
                              access:users.clearkey(use:password_key)
                              use:password    =glo:password
                              access:users.fetch(use:password_key)
                              job:who_booked  = use:user_code
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                                  aud:notes         = 'UNIT DETAILS: ' & Clip(job:Manufacturer) & ' - ' & Clip(job:Model_Number) &|
                                                      '<13,10>I.M.E.I. NO: ' & Clip(job:ESN) & |
                                                      '<13,10,13,10>CHARGEABLE JOB: ' & Clip(job:Chargeable_Job) &|
                                                      '<13,10>WARRANTY JOB: ' & Clip(job:Warranty_Job)
                                  aud:ref_number    = job:ref_number
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  aud:action        = 'JOBS IMPORT'
                                  if access:audit.insert()
                                      access:audit.cancelautoinc()
                                  end
                              end!if access:audit.primerecord() = level:benign
      
                              if access:jobs.tryinsert()
                                  access:jobs.cancelautoinc()
                              Else
                                If Access:JOBSE.PrimeRecord() = Level:Benign
                                    jobe:RefNumber  = job:Ref_Number
                                    If job:Workshop = 'YES'
                                        jobe:InWorkshopDate = Today()
                                        jobe:InWorkshopTime = Clock()
                                    End !If job:Workshop = 'YES'
                                    If Access:JOBSE.TryInsert() = Level:Benign
                                        !Insert Successful
                                    Else !If Access:JOBSE.TryInsert() = Level:Benign
                                        !Insert Failed
                                    End !If Access:JOBSE.TryInsert() = Level:Benign
                                End !If Access:JOBSE.PrimeRecord() = Level:Benign

!                                  If func:PrintLabel
!                                      glo:Select1 = job:Ref_Number
!                                      Thermal_Labels('')
!                                      glo:Select1 = ''
!                                  End !If func:PrintLabel
                                  If CountBouncer(job:Ref_Number,job:Date_Booked,job:Esn)
                                      bounce:RefNumber = job:Ref_Number
                                      Add(BouncerList)
                                  End!If CountBouncer(job:Ref_Number,job:Date_Booked,job:Esn)
                              End!if access:jobs.tryinsert()
                          end!if access:jobs.primerecord() = level:benign
                      End!Loop
                  End!If Error()
                  Setcursor()
                  Close(IMPORTFILE)
                  Case MessageEx('Import Completed.','ServiceBase 2000',|
                                 'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  If Records(BouncerList)
                      Case MessageEx('One or more of the imported Jobs are Bouncers.'&|
                        '<13,10>'&|
                        '<13,10>Do you wish to print the Bouncer History Reports for these jobs?','ServiceBase 2000',|
                                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              Sort(BouncerList,bounce:RefNumber)
                              Loop x# = 1 To Records(BouncerList)
                                  Get(BouncerList,x#)
                                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                                  job:Ref_Number  = bounce:RefNUmber
                                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                      !Found
                                      glo:Select2 = job:esn
                                      Bouncer_History
                                      glo:Select2 = ''
                                  Else! If Access:.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                  
                              End!Loop x# = 1 To Records(BouncerList)
                          Of 2 ! &No Button
                      End!Case MessageEx
                  End
              End!If error# = 0
      
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'IntecImport')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

