

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF02002.INC'),ONCE        !Local module procedure declarations
                     END


CRCImports PROCEDURE                                  !Generated from procedure template - Window

tmp:ImportType       BYTE(0)
tmp:PrintLabel       BYTE(0)
tmp:Print_Job_Card   BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('CRC Import Facility'),AT(,,220,220),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,184),USE(?Sheet1),SPREAD
                         TAB('Select Import Type'),USE(?Tab1)
                           OPTION('Import Types'),AT(12,20,196,128),USE(tmp:ImportType),BOXED
                             RADIO('Swift Dealer Import'),AT(76,36,88,10),USE(?Option1:Radio1),TRN,VALUE('0')
                             RADIO('Swift End User Import'),AT(76,52),USE(?Option1:Radio3),TRN,VALUE('3')
                             RADIO('Panasonic Import'),AT(76,68,88,10),USE(?Option1:Radio2),TRN,VALUE('1')
                             RADIO('Sony Import'),AT(76,84),USE(?tmp:ImportType:Radio4),TRN,VALUE('4')
                             RADIO('Vodafone ONR Import'),AT(76,100),USE(?tmp:ImportType:Radio5),TRN,VALUE('5')
                             RADIO('Vodafone Corporate Import'),AT(76,116),USE(?tmp:ImportType:Radio6),TRN
                             RADIO('Generic Import'),AT(76,132),USE(?tmp:ImportType:Radio7),TRN
                           END
                           CHECK('Print Job Labels'),AT(76,156),USE(tmp:PrintLabel),TRN,MSG('Print Job Label'),TIP('Print Job Label'),VALUE('1','0')
                           CHECK('Print Job Card'),AT(76,168),USE(tmp:Print_Job_Card),TRN,VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,192,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(156,196,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       BUTTON('&OK'),AT(96,196,56,16),USE(?OK),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:ImportType{prop:Font,3} = -1
    ?tmp:ImportType{prop:Color} = 15066597
    ?tmp:ImportType{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio3{prop:Font,3} = -1
    ?Option1:Radio3{prop:Color} = 15066597
    ?Option1:Radio3{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    ?tmp:ImportType:Radio4{prop:Font,3} = -1
    ?tmp:ImportType:Radio4{prop:Color} = 15066597
    ?tmp:ImportType:Radio4{prop:Trn} = 0
    ?tmp:ImportType:Radio5{prop:Font,3} = -1
    ?tmp:ImportType:Radio5{prop:Color} = 15066597
    ?tmp:ImportType:Radio5{prop:Trn} = 0
    ?tmp:ImportType:Radio6{prop:Font,3} = -1
    ?tmp:ImportType:Radio6{prop:Color} = 15066597
    ?tmp:ImportType:Radio6{prop:Trn} = 0
    ?tmp:ImportType:Radio7{prop:Font,3} = -1
    ?tmp:ImportType:Radio7{prop:Color} = 15066597
    ?tmp:ImportType:Radio7{prop:Trn} = 0
    ?tmp:PrintLabel{prop:Font,3} = -1
    ?tmp:PrintLabel{prop:Color} = 15066597
    ?tmp:PrintLabel{prop:Trn} = 0
    ?tmp:Print_Job_Card{prop:Font,3} = -1
    ?tmp:Print_Job_Card{prop:Color} = 15066597
    ?tmp:Print_Job_Card{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CRCImports',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:ImportType',tmp:ImportType,'CRCImports',1)
    SolaceViewVars('tmp:PrintLabel',tmp:PrintLabel,'CRCImports',1)
    SolaceViewVars('tmp:Print_Job_Card',tmp:Print_Job_Card,'CRCImports',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ImportType;  SolaceCtrlName = '?tmp:ImportType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio1;  SolaceCtrlName = '?Option1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio3;  SolaceCtrlName = '?Option1:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio2;  SolaceCtrlName = '?Option1:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ImportType:Radio4;  SolaceCtrlName = '?tmp:ImportType:Radio4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ImportType:Radio5;  SolaceCtrlName = '?tmp:ImportType:Radio5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ImportType:Radio6;  SolaceCtrlName = '?tmp:ImportType:Radio6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ImportType:Radio7;  SolaceCtrlName = '?tmp:ImportType:Radio7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PrintLabel;  SolaceCtrlName = '?tmp:PrintLabel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Print_Job_Card;  SolaceCtrlName = '?tmp:Print_Job_Card';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CRCImports')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'CRCImports')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Option1:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'CRCImports',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      Case tmp:ImportType
          Of 0
              CRCImportSwift(tmp:PrintLabel,tmp:Print_Job_Card)
          Of 1
              CRCImportPanasonic(tmp:PrintLabel,tmp:Print_Job_Card)
          Of 3
              CRCImportSwiftEndUser(tmp:PrintLabel,tmp:Print_Job_Card)
          Of 4
              CRCImportSony(tmp:PrintLabel,tmp:Print_Job_Card)
          Of 5
             CRCImportVodafone(tmp:PrintLabel,tmp:Print_Job_Card)
          ! Start Change 2649 BE(10/06/03)
          Of 6
             CRCImportVodafoneCorporate(tmp:PrintLabel,tmp:Print_Job_Card)
          ! End Change 2649 BE(10/06/03)
          ! Start Change 3414 BE(04/12/03)
          Of 7
             CRCImportGeneric(tmp:PrintLabel,tmp:Print_Job_Card)
          ! End Change 3414 BE(04/12/03)
          Else
              ! error
      End!Case tmp:ImportType
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'CRCImports')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

