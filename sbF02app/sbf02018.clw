

   MEMBER('sbf02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF02018.INC'),ONCE        !Local module procedure declarations
                     END


CRCExportPanasonicCritiera2 PROCEDURE                 !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
FilesOpened          BYTE
save_job_id          USHORT,AUTO
tmp:StartDate        DATE
tmp:EndDate          DATE
local:File_Name      STRING(255),STATIC
savepath             STRING(255)
save_cou_id          USHORT,AUTO
Trade_Main           STRING(15)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Trade_Main
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB8::View:FileDropCombo VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
window               WINDOW('Panasonic Export'),AT(,,232,108),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,72),USE(?Sheet1),SPREAD
                         TAB('Date Range'),USE(?Tab1)
                           PROMPT('Start Date'),AT(8,20),USE(?glo:select1:Prompt)
                           ENTRY(@d6),AT(84,20,64,10),USE(tmp:StartDate),FONT(,8,,FONT:bold),CAP
                           BUTTON,AT(152,20,10,10),USE(?PopCalendar),ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(8,36),USE(?glo:select2:Prompt)
                           ENTRY(@d6),AT(84,36,64,10),USE(tmp:EndDate),FONT(,8,,FONT:bold),UPR
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar:2),ICON('Calenda2.ico')
                           STRING('Trade Account '),AT(9,52),USE(?String1)
                           COMBO(@s20),AT(84,52,124,10),USE(Trade_Main),IMM,FONT(,,,FONT:bold),FORMAT('65L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),DROP(5,185),FROM(Queue:FileDropCombo)
                           STRING('(leave blank to select ALL accounts)'),AT(84,64),USE(?String2)
                         END
                       END
                       PANEL,AT(4,80,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(112,84,56,16),USE(?OkButton),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(168,84,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?cancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
PanasonicExport     File,Driver('BASIC'),Pre(panexp),Name(local:File_Name),Create,Bindable,Thread
Record                  Record
OrderNumber                 String(30)
JobNumber                   Long
Status                      String(30)
Warranty                    String(30)
InvoiceText                 String(255)
CompletedDate               String(15)
ConsignmentNumber           String(30)
Blank                       String(30)
InConsignmentNumber         String(30)
                        End
                    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?glo:select1:Prompt{prop:FontColor} = -1
    ?glo:select1:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    If ?Trade_Main{prop:ReadOnly} = True
        ?Trade_Main{prop:FontColor} = 65793
        ?Trade_Main{prop:Color} = 15066597
    Elsif ?Trade_Main{prop:Req} = True
        ?Trade_Main{prop:FontColor} = 65793
        ?Trade_Main{prop:Color} = 8454143
    Else ! If ?Trade_Main{prop:Req} = True
        ?Trade_Main{prop:FontColor} = 65793
        ?Trade_Main{prop:Color} = 16777215
    End ! If ?Trade_Main{prop:Req} = True
    ?Trade_Main{prop:Trn} = 0
    ?Trade_Main{prop:FontStyle} = font:Bold
    ?String2{prop:FontColor} = -1
    ?String2{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CRCExportPanasonicCritiera2',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'CRCExportPanasonicCritiera2',1)
    SolaceViewVars('save_job_id',save_job_id,'CRCExportPanasonicCritiera2',1)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'CRCExportPanasonicCritiera2',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'CRCExportPanasonicCritiera2',1)
    SolaceViewVars('local:File_Name',local:File_Name,'CRCExportPanasonicCritiera2',1)
    SolaceViewVars('savepath',savepath,'CRCExportPanasonicCritiera2',1)
    SolaceViewVars('save_cou_id',save_cou_id,'CRCExportPanasonicCritiera2',1)
    SolaceViewVars('Trade_Main',Trade_Main,'CRCExportPanasonicCritiera2',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select1:Prompt;  SolaceCtrlName = '?glo:select1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Trade_Main;  SolaceCtrlName = '?Trade_Main';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CRCExportPanasonicCritiera2')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'CRCExportPanasonicCritiera2')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?glo:select1:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:StartDate = Deformat('1/1/1990',@d6)
  tmp:EndDate   = Today()
  Relate:COURIER.Open
  Relate:DEFAULTS.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  FDCB8.Init(Trade_Main,?Trade_Main,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:TRADEACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder(tra:Account_Number_Key)
  FDCB8.AddField(tra:Account_Number,FDCB8.Q.tra:Account_Number)
  FDCB8.AddField(tra:Company_Name,FDCB8.Q.tra:Company_Name)
  FDCB8.AddField(tra:RecordNumber,FDCB8.Q.tra:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'CRCExportPanasonicCritiera2',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      savepath = path()
      local:file_name = 'PANEXP.CSV'
      If not filedialog ('Choose File',local:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !Failed
          setpath(savepath)
      else!If not filedialog
          !Found
          setpath(savepath)
      
          Remove(PanasonicExport)
      
          Open(PanasonicExport)
          Error# = 0
          If Error()
              Create(PanasonicExport)
              Open(PanasonicExport)
              If Error()
                  Case MessageEx('Cannot create export file.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Error# = 1
              End!If Error()
          End!If Error()
          If Error# = 0
      
              recordspercycle         = 25
              recordsprocessed        = 0
              percentprogress         = 0
              progress:thermometer    = 0
              thiswindow.reset(1)
              open(progresswindow)
      
              ?progress:userstring{prop:text} = 'Exporting Despatched Jobs...'
              ?progress:pcttext{prop:text} = '0% Completed'
      
      
              recordstoprocess    = Records(Courier)
      
      !---Before Routine
              Count# = 0
      
              Save_cou_ID = Access:COURIER.SaveFile()
              Set(cou:Courier_Key)
              Loop
                  If Access:COURIER.NEXT()
                     Break
                  End !If
      !---Insert Routine
                  Do GetNextRecord2
                      Do cancelcheck
                      If tmp:cancel = 1
                          Break
                      End!If tmp:cancel = 1
      
      
                  Save_job_ID = Access:JOBS.SaveFile()
                  Access:JOBS.ClearKey(job:DateDespatchKey)
                  job:Courier         = cou:Courier
                  job:Date_Despatched = tmp:StartDate
                  Set(job:DateDespatchKey,job:DateDespatchKey)
                  Loop
                      If Access:JOBS.NEXT()
                         Break
                      End !If
                      If job:Courier         <> cou:Courier      |
                      Or job:Date_Despatched > tmp:EndDate      |
                          Then Break.  ! End If
                      If job:manufacturer <> 'PANASONIC'
                          Cycle
                      End!If job:manufacturer <> 'PANASONIC'
                      IF Trade_Main <> ''
                        Access:SubTracc.ClearKey(sub:Account_Number_Key)
                        sub:Account_Number = job:Account_Number
                        IF Access:SubTracc.Fetch(sub:Account_Number_Key)
                          !Error!
                          CYCLE
                        ELSE
                          IF sub:Main_Account_Number <> Trade_Main
                            CYCLE
                          END
                        END
                      END
                      Clear(panexp:Record)
                      panexp:OrderNumber          = job:Order_Number
                      panexp:JobNumber            = job:Ref_Number
                      panexp:Status               = job:Current_Status
                      If job:Warranty_Job = 'YES'
                          panexp:Warranty           = 'IN WARRANTY REPAIR'
                      Else!If job:Warranty_Job = 'YES'
                          panexp:Warranty           = 'OUT OF WARRANTY'
                      End!If job:Warranty_Job = 'YES'
      
                      Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                      jbn:RefNumber   = job:Ref_Number
                      If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                          !Found
                          panexp:InvoiceText = jbn:Invoice_Text
                      Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          panexp:InvoiceText  = ''
                      End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                      
                      panexp:CompletedDate        = Format(job:Date_Completed,@d6)
                      panexp:ConsignmentNumber    = job:Consignment_Number
                      panexp:Blank                = ''
                      panexp:InConsignmentNumber  = job:Incoming_Consignment_Number
                      Add(PanasonicExport)
                      Count# += 1
                      ?progress:userstring{prop:Text} = 'Despatched Jobs Found: ' & Count#
                      Display()
                  End !Loop
                  Access:JOBS.RestoreFile(Save_job_ID)
      
                  Save_job_ID = Access:JOBS.SaveFile()
                  Access:JOBS.ClearKey(job:DateDespLoaKey)
                  job:Loan_Courier    = cou:Courier
                  job:Loan_Despatched = tmp:StartDate
                  Set(job:DateDespLoaKey,job:DateDespLoaKey)
                  Loop
                      If Access:JOBS.NEXT()
                         Break
                      End !If
                      If job:Loan_Courier    <> cou:Courier      |
                      Or job:Loan_Despatched > tmp:EndDate      |
                          Then Break.  ! End If
                      If job:manufacturer <> 'PANASONIC'
                          Cycle
                      End!If job:manufacturer <> 'PANASONIC'
                      IF Trade_Main <> ''
                        Access:SubTracc.ClearKey(sub:Account_Number_Key)
                        sub:Account_Number = job:Account_Number
                        IF Access:SubTracc.Fetch(sub:Account_Number_Key)
                          !Error!
                          CYCLE
                        ELSE
                          IF sub:Main_Account_Number <> Trade_Main
                            CYCLE
                          END
                        END
                      END
                      Clear(panexp:Record)
                      panexp:OrderNumber          = job:Order_Number
                      panexp:JobNumber            = job:Ref_Number
                      panexp:Status               = job:Current_Status
                      If job:Warranty_Job = 'YES'
                          panexp:Warranty           = 'IN WARRANTY REPAIR'
                      Else!If job:Warranty_Job = 'YES'
                          panexp:Warranty           = 'OUT OF WARRANTY'
                      End!If job:Warranty_Job = 'YES'
      
                      Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                      jbn:RefNumber   = job:Ref_Number
                      If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                          !Found
                          panexp:InvoiceText = jbn:Invoice_Text
                      Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          panexp:InvoiceText  = ''
                      End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                      
                      panexp:CompletedDate        = Format(job:Date_Completed,@d6)
                      panexp:ConsignmentNumber    = job:Loan_Consignment_Number
                      panexp:Blank                = ''
                      panexp:InConsignmentNumber  = job:Incoming_Consignment_Number
                      Add(PanasonicExport)
                      Count# += 1
                      ?progress:userstring{prop:Text} = 'Despatched Jobs Found: ' & Count#
                      Display()
                  End !Loop
                  Access:JOBS.RestoreFile(Save_job_ID)
      
                  Save_job_ID = Access:JOBS.SaveFile()
                  Access:JOBS.ClearKey(job:DateDespExcKey)
                  job:Exchange_Courier    = cou:Courier
                  job:Exchange_Despatched = tmp:StartDate
                  Set(job:DateDespExcKey,job:DateDespExcKey)
                  Loop
                      If Access:JOBS.NEXT()
                         Break
                      End !If
                      If job:Exchange_Courier    <> cou:Courier      |
                      Or job:Exchange_Despatched > tmp:EndDate      |
                          Then Break.  ! End If
                      If job:manufacturer <> 'PANASONIC'
                          Cycle
                      End!If job:manufacturer <> 'PANASONIC'
                      IF Trade_Main <> ''
                        Access:SubTracc.ClearKey(sub:Account_Number_Key)
                        sub:Account_Number = job:Account_Number
                        IF Access:SubTracc.Fetch(sub:Account_Number_Key)
                          !Error!
                          CYCLE
                        ELSE
                          IF sub:Main_Account_Number <> Trade_Main
                            CYCLE
                          END
                        END
                      END
                      Clear(panexp:Record)
                      panexp:OrderNumber          = job:Order_Number
                      panexp:JobNumber            = job:Ref_Number
                      panexp:Status               = job:Current_Status
                      If job:Warranty_Job = 'YES'
                          panexp:Warranty           = 'IN WARRANTY REPAIR'
                      Else!If job:Warranty_Job = 'YES'
                          panexp:Warranty           = 'OUT OF WARRANTY'
                      End!If job:Warranty_Job = 'YES'
      
                      Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                      jbn:RefNumber   = job:Ref_Number
                      If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                          !Found
                          panexp:InvoiceText = jbn:Invoice_Text
                      Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          panexp:InvoiceText  = ''
                      End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                      
                      panexp:CompletedDate        = Format(job:Date_Completed,@d6)
                      panexp:ConsignmentNumber    = job:Exchange_Consignment_Number
                      panexp:Blank                = ''
                      panexp:InConsignmentNumber  = job:Incoming_Consignment_Number
                      Add(PanasonicExport)
                      Count# += 1
                      ?progress:userstring{prop:Text} = 'Despatched Jobs Found: ' & Count#
                      Display()
                  End !Loop
                  Access:JOBS.RestoreFile(Save_job_ID)
              End !Loop
              Access:COURIER.RestoreFile(Save_cou_ID)
      
              recordspercycle         = 25
              recordsprocessed        = 0
              percentprogress         = 0
              progress:thermometer    = 0
      
              ?progress:userstring{prop:text} = 'Exporting Incomplete Jobs....'
              ?progress:pcttext{prop:text} = '0% Completed'
      
      
              recordstoprocess    = Records(Jobs)
      
      
          !---Before Routine
      
              CompCount# = 0
      
              If tmp:Cancel <> 1
      
                  Save_job_ID = Access:JOBS.SaveFile()
                  Access:JOBS.ClearKey(job:DateCompletedKey)
                  job:Date_Completed = ''
                  Set(job:DateCompletedKey,job:DateCompletedKey)
                  Loop
                      If Access:JOBS.NEXT()
                         Break
                      End !If
                      If job:Date_Completed <> ''      |
                          Then Break.  ! End If
              !---Insert Routine
                      Do GetNextRecord2
                      cancelcheck# += 1
          !            If cancelcheck# > (RecordsToProcess/100)
                          Do cancelcheck
                          If tmp:cancel = 1
                              Break
                          End!If tmp:cancel = 1
                          cancelcheck# = 0
          !            End!If cancelcheck# > 50
      
                      If job:manufacturer <> 'PANASONIC'
                          Cycle
                      End!If job:manufacturer <> 'PANASONIC'
      
                      IF Trade_Main <> ''
                            Access:SubTracc.ClearKey(sub:Account_Number_Key)
                            sub:Account_Number = job:Account_Number
                            IF Access:SubTracc.Fetch(sub:Account_Number_Key)
                              !Error!
                              CYCLE
                            ELSE
                              IF sub:Main_Account_Number <> Trade_Main
                                CYCLE
                              END
                            END
                          END
                      Clear(panexp:Record)
                      panexp:OrderNumber          = job:Order_Number
                      panexp:JobNumber            = job:Ref_Number
                      panexp:Status               = job:Current_Status
                      If job:Warranty_Job = 'YES'
                          panexp:Warranty           = 'IN WARRANTY REPAIR'
                      Else!If job:Warranty_Job = 'YES'
                          panexp:Warranty           = 'OUT OF WARRANTY'
                      End!If job:Warranty_Job = 'YES'
      
                      Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                      jbn:RefNumber   = job:Ref_Number
                      If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                          !Found
                          panexp:InvoiceText = jbn:Invoice_Text
                      Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          panexp:InvoiceText  = ''
                      End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                      
                      panexp:CompletedDate        = Format(job:Date_Completed,@d6)
                      panexp:ConsignmentNumber    = job:Consignment_Number
                      panexp:Blank                = ''
                      panexp:InConsignmentNumber  = job:Incoming_Consignment_Number
                      Add(PanasonicExport)
                      Count# += 1
                      CompCount# += 1
                      ?progress:userstring{prop:Text} = 'Incomplete Jobs Found: ' & CompCount# & '  Total Found: ' & Count#
                      Display()
                  End !Loop
                  Access:JOBS.RestoreFile(Save_job_ID)
              End !If tmp:Cancel <> 1
          !---After Routine
              Setcursor()
              Do EndPrintRun
              close(progresswindow)
              Close(PanasonicExport)
              Case MessageEx('Export Complete.','ServiceBase 2000',|
                             'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End!If Error# = 0
      
      End!If not filedialog
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = ''
      glo:select2 = ''
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'CRCExportPanasonicCritiera2')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

