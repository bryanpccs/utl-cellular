

   MEMBER('celdatex.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELDA008.INC'),ONCE        !Local module procedure declarations
                     END



Cellext PROCEDURE                                     !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
PARS                 GROUP,PRE(par)
StartDate            DATE
EndDate              DATE
                     END
SavePath             STRING(255)
ExtractMode          LONG
RecordCount          LONG
JobsExtracted        LONG
LastJobNumber        LONG
LOC_GROUP            GROUP,PRE(loc)
ApplicationName      STRING(30)
ProgramName          STRING(30)
UserName             STRING(61)
Filename             STRING(255)
                     END
ExtractStartDate     DATE
ExtractStartTime     TIME
ExtractEndDate       DATE
ExtractEndTime       TIME
ExtractDirectory     STRING(255)
EndBuildIndexTime    TIME
JOBS_XTR               FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(JXTF),NAME(JobsFilename),CREATE,THREAD
PK_Ref_Number            KEY(JXT:Ref_Number),PRIMARY
JXT_Delete_Flag          KEY(JXT:Delete_Flag,JXT:Date_Completed),DUP
Record                   RECORD,PRE(JXT)
Ref_Number                  LONG
Delete_Flag                 BYTE
Batch_Number                LONG         ! ***
Internal_Status             STRING(10)    ! ***
Auto_Search                 STRING(30)    ! ***
Who_Booked                  STRING(3)
Date_Booked                 DATE
Time_Booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)     ! ***
Web_Type                    STRING(3)      ! ***
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)    ! ***
Location_Type               STRING(10)    ! ***
Phone_Lock                  STRING(30)    ! ***
Workshop                    STRING(3)      ! *** 
Location                    STRING(30)
Authority_Number            STRING(30)     ! ***
Insurance_Ref_Number        STRING(30)     ! ***
DOP                         DATE
Insurance                   STRING(3)       ! ***
Insurance_Type              STRING(30)     ! ****
Transit_Type                STRING(30)     ! ***
Physical_Damage             STRING(3)      ! ***
Intermittent_Fault          STRING(3)      ! ***
Loan_Status                 STRING(30)     ! *** 
Exchange_Status             STRING(30)
Job_Priority                STRING(30)     ! *** 
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)     ! ***
Department_Name             STRING(30)     ! ***
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)       ! ***
Date_In_Repair              DATE
Time_In_Repair              TIME
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)      ! ***
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Pass         DATE
Time_QA_Second_Pass         TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(10)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)   ! ***
Mobile_Number               STRING(15)
Company_Name_Col            STRING(30)
Address_Line1_Col           STRING(30)
Address_Line2_Col           STRING(30)
Address_Line3_Col           STRING(30)
Postcode_Col                STRING(10)
Telephone_Col               STRING(15)
Company_Name_Del            STRING(30)
Address_Line1_Del           STRING(30)
Address_Line2_Del           STRING(30)
Address_Line3_Del           STRING(30)
Postcode_Del                STRING(10)
Telephone_Del               STRING(15)
Completed                   STRING(3)
Date_Completed              DATE
Time_Completed              TIME
Paid                        STRING(3)      ! ***
Paid_Warranty               STRING(3)      ! ***
Date_Paid                   DATE           ! *** 
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Cha_Charges          STRING(3)       ! ***Ignore_Chargeable_Charges
Ignore_War_Charges          STRING(3)       ! ***Ignore_Warranty_Charges
Ignore_Est_Charges          STRING(3)       ! **Ignore_Estimate_Charges*
Advance_Payment             REAL            ! ***
Labour_Cost                 REAL
Labour_Cost_Estimate        REAL
Labour_Cost_Warranty        REAL
Parts_Cost                  REAL            ! ***
Parts_Cost_Estimate         REAL            ! ***
Parts_Cost_Warranty         REAL            ! ***
Courier_Cost                REAL            ! ***
Courier_Cost_Est            REAL            ! ***Courier_Cost_Estimate
Courier_Cost_War            REAL            ! ***Courier_Cost_Warranty
Sub_Total                   REAL            ! ***
Sub_Total_Estimate          REAL            ! ***
Sub_Total_Warranty          REAL            ! ***
Loan_Issued_Date            DATE            ! ***
Loan_Unit_Number            LONG            ! ***
Loan_Accessory              STRING(3)       ! ***
Loan_User                   STRING(3)       ! ***
Loan_Courier                STRING(30)      ! ***
Loan_Consignment_Num        STRING(30)      ! ***Loan_Consignment_Number
Loan_Despatched             DATE            ! ***
Loan_Despatched_User        STRING(3)       ! ***
Loan_Despatch_Number        LONG            ! ***
LoanService                 STRING(1)       ! *** LoaService
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)        ! ***
Loan_Authorised             STRING(3)        ! ***
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consign_Num        STRING(30)       ! ***Exchange_Consignment_Number
Exchange_Despatched         DATE
Exchange_Desp_User          STRING(3)
Exchange_Desp_Number        LONG
Exchange_Service            STRING(1)          ! ***ExcService
Date_Despatched             DATE
Despatch_Number             LONG               ! ***
Despatch_User               STRING(3)          ! ***
Courier                     STRING(30)         ! ***
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)         ! ***
In_Consignment_Num          STRING(30)
Incoming_Date               DATE               ! ***
Despatched                  STRING(3)          ! ***
Despatch_Type               STRING(3)          ! ***
Current_Courier             STRING(30)         ! ***
JobService                  STRING(1)          ! ***
Last_Repair_Days            LONG               ! *** 
Third_Party_Site            STRING(30)
Estimate                    STRING(3)          ! ***
Estimate_If_Over            REAL               ! ***
Estimate_Accepted           STRING(3)          ! ***
Estimate_Rejected           STRING(3)          ! ***
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
PreviousStatus              STRING(30)   ! ***
StatusUser                  STRING(3)    ! ***
Status_End_Date             DATE         ! ***
Status_End_Time             TIME         ! ***
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)   ! ***
InvoiceAccount              STRING(15)   ! ***
InvoiceStatus               STRING(3)    ! ***
InvoiceBatch                LONG         ! ***
InvoiceQuery                STRING(255)  ! ***
EDI                         STRING(3)    ! ***
EDI_Batch_Number            REAL
Inv_Exception               STRING(3)    ! ***InvoiceException
Inv_Failure_Reason          STRING(80)   ! ***Invoice_Failure_Reason
Inv_Date                    DATE         ! ***Invoice_date
Inv_Date_Warranty           DATE         ! ***Invoice_Date_warranty
Inv_Courier_Cost            REAL         ! ***Invoice_Courier_Cost
Inv_Labour_Cost             REAL         ! ***Invoice_Labour_cost
Inv_Parts_Cost              REAL         ! ***Invoice_Parts_Cost
Inv_Sub_Total               REAL         ! ***Invoice_Sub_Total
Inv_Number_Warranty         LONG         ! ***Invoice_Number_Warranty
Winv_Courier_Cost           REAL         ! ***WInvoice_Courier_Cost
Winv_Labour_Cost            REAL         ! ***WInvoice_Labour-Cost
Winv_Parts_Cost             REAL         ! ***WInvoice_Parts_Cost
Winv_Sub_Total              REAL         ! ***WInvoice_Sub_Total
! JOBSE
JobMark                     BYTE         ! ***
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SimNumber                   STRING(30)  ! ***
JobReceived                 BYTE        ! ***
SkillLevel                  LONG        ! ***
UPSFlagCode                 STRING(1)   ! ***
FailedDelivery              BYTE        ! ***
CConfirmSecondEntry         STRING(3)   ! ***
WConfirmSecondEntry         STRING(3)   ! ***
EndUserEmailAddress         STRING(255) ! ***
Network                     STRING(30)  ! ***
ExchangeReason              STRING(255) ! ***
LoanReason                  STRING(255) ! ***
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
! JOBNOTES
Fault_description           STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Collection_Text             STRING(255)   ! ***
Delivery_Text               STRING(255)   ! ***
ColContactName              STRING(30)    ! ***
ColDepartment               STRING(30)    ! ***
DelContactName              STRING(30)    ! ***
DelDepartment               STRING(30)    ! ***
                         END
                     END
PARTS_XTR          FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(PXTF),NAME(PartsFilename),CREATE,THREAD
PK_Record_Number         KEY(PXT:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
PXT_Ref_Number           KEY(PXT:Ref_Number),DUP
Record                   RECORD,PRE(PXT)
Record_Number               LONG
Ref_Number                  LONG
Adjustment                  STRING(3)   ! ***
Part_Ref_Number             LONG        ! ***
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL       ! ***
Retail_Cost                 REAL       ! ***
Quantity                    LONG
Warranty_Part               STRING(3)  ! ***
Exclude_From_Order          STRING(3)  ! ***
Despatch_Note_Number        STRING(30) ! ***
Date_Ordered                DATE       ! ***
Pending_Ref_Number          LONG       ! ***
Order_Number                LONG       ! ***
Date_Received               DATE       ! ***
Order_Part_Number           LONG       ! ***
Status_Date                 DATE       ! ***
Fault_Codes_Checked         STRING(3)  ! ***
InvoicePart                 LONG       ! ***
Credit                      STRING(3)  ! ***
Requested                   BYTE       ! ***
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
                         END
                     END
WARPARTS_XTR         FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(WXTF),NAME(WarpartsFilename),CREATE,THREAD
PK_Record_Number         KEY(WXT:Record_number),NAME('AUTOINCREMENT'),PRIMARY
WXT_Ref_Number           KEY(WXT:Ref_Number),DUP
Record                   RECORD,PRE(WXT)
Record_Number               LONG
Ref_Number                  LONG
Adjustment                  STRING(3)   ! ***
Part_Ref_Number             LONG        ! ***
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL       ! ***
Retail_Cost                 REAL       ! ***
Quantity                    LONG
Warranty_Part               STRING(3)  ! ***
Exclude_From_Order          STRING(3)  ! ***
Despatch_Note_Number        STRING(30) ! ***
Date_Ordered                DATE       ! ***
Pending_Ref_Number          LONG       ! ***
Order_Number                LONG       ! ***
Date_Received               DATE       ! ***
Order_Part_Number           LONG       ! ***
Status_Date                 DATE       ! ***
Main_Part                   STRING(3)  ! ***
Fault_Codes_Checked         STRING(3)  ! ***
InvoicePart                 LONG       ! ***
Credit                      STRING(3)  ! ***
Requested                   BYTE       ! ***
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
                         END
                     END
ESTPARTS_XTR         FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(EPXF),NAME(EstpartsFilename),CREATE,THREAD
PK_Record_Number         KEY(EPX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
EPX_Ref_Number           KEY(EPX:Ref_Number),DUP
Record                   RECORD,PRE(EPX)
Record_Number               LONG
Ref_Number                  LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    LONG
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Date_Received               DATE
Order_Part_Number           LONG
Status_Date                 DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
                         END
                     END
AUDIT_XTR            FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(AUXF),NAME(AuditFilename),CREATE,THREAD
PK_Record_Number         KEY(AUX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
AUX_Ref_Number           KEY(AUX:Ref_Number),DUP
Record                   RECORD,PRE(AUX)
Record_Number               LONG
Ref_Number                  LONG
Date_Stamp                  DATE
Time_Stamp                  TIME
User_Code                   STRING(3)
ActionField                 STRING(80)
DummyField                  STRING(2)
Notes                       STRING(255)
Type                        STRING(3)
                         END
                     END
AUDSTATS_XTR          FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(ASXF),NAME(AudstatsFilename),CREATE,THREAD
PK_Record_Number         KEY(ASX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
ASX_Ref_Number           KEY(ASX:Ref_Number),DUP
Record                   RECORD,PRE(ASX)
Record_Number               LONG
Ref_Number                  LONG
Type                        STRING(3)
DateChanged                 DATE
TimeChanged                 TIME
OldStatus                   STRING(30)
NewStatus                   STRING(30)
UserCode                    STRING(3)
                         END
                     END


CONTHIST_XTR         FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(CHXF),NAME(ConthistFilename),CREATE,THREAD
PK_Record_Number         KEY(CHX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
CHX_Ref_Number           KEY(CHX:Ref_Number),DUP
Record                   RECORD,PRE(CHX)
Record_Number               LONG
Ref_Number                  LONG
Date_Stamp                  DATE
Time_Stamp                  TIME
User_Code                   STRING(3)
ActionField                 STRING(80)
Notes_1                     STRING(250)
Notes_2                     STRING(250)
Notes_3                     STRING(250)
Notes_4                     STRING(250)
                         END
                     END
JOBSTAGE_XTR         FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(JSXF),NAME(JobstageFilename),CREATE,THREAD
PK_Record_Number         KEY(JSX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
JSX_Ref_Number           KEY(JSX:Ref_Number),DUP
Record                   RECORD,PRE(JSX)
Record_Number               LONG
Ref_Number                  LONG
Job_Stage                   STRING(30)
Date_Stamp                  DATE
Time_Stamp                  TIME
User_Code                   STRING(3)
                         END
                     END



JOBTHIRD_XTR         FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(JOXF),NAME(JobthirdFilename),CREATE,THREAD
PK_Record_Number         KEY(JOX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
JOX_Ref_Number           KEY(JOX:Ref_Number),DUP
JOX_ThirdPartyNumber     KEY(JOX:ThirdPartyNumber),DUP
Record                   RECORD,PRE(JOX)
Record_Number               LONG
Ref_Number                  LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
outmsn                      STRING(30)
InMSN                       STRING(30)
                         END
                     END
JOBPAYMT_XTR         FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(JPXF),NAME(JobpaymtFilename),CREATE,THREAD
PK_Record_Number         KEY(JPX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
JPX_Ref_Number           KEY(JPX:Ref_Number),DUP
Record                   RECORD,PRE(JPX)
Record_Number               LONG
Ref_Number                  LONG
Date_Stamp                  DATE
Payment_Type                STRING(30)
Credit_Card_Number          STRING(20)
Expiry_Date                 STRING(5)
Issue_Number                STRING(5)
Amount                      REAL
User_Code                   STRING(3)
                         END
                     END
JOBSENG_XTR          FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(JEXF),NAME(JobsengFilename),CREATE,THREAD
PK_Record_Number         KEY(JEX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
JEX_Ref_Number           KEY(JEX:Ref_Number),DUP
Record                   RECORD,PRE(JEX)
Record_Number               LONG
Ref_Number                  LONG
UserCode                    STRING(3)
DateAllocated               DATE
TimeAllocated               TIME
AllocatedBY                 STRING(3)
EngSkillLevel               LONG
JobSkillLevel               STRING(30)
                         END
                     END
JOBACC_XTR          FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(JAXF),NAME(JobaccFilename),CREATE,THREAD
PK_Record_Number         KEY(JAX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
JAX_Ref_Number           KEY(JAX:Ref_Number),DUP
Record                   RECORD,PRE(JAX)
Record_Number               LONG
Ref_Number                  LONG
Accessory                   STRING(30)
                         END
                     END
JOBEXHIS_XTR          FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(JHXF),NAME(JobexhisFilename),CREATE,THREAD
PK_Record_Number         KEY(JHX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
JHX_Ref_Number           KEY(JHX:Ref_Number),DUP
Record                   RECORD,PRE(JHX)
Record_Number               LONG
Ref_Number                  LONG
Loan_Unit_Number            REAL
Date_Stamp                  DATE
Time_Stamp                  TIME
User_Code                   STRING(3)
Status                      STRING(60)
                         END
                     END
JOBLOHIS_XTR          FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(JLXF),NAME(JoblohisFilename),CREATE,THREAD
PK_Record_Number         KEY(JLX:Record_Number),NAME('AUTOINCREMENT'),PRIMARY
JLX_Ref_Number           KEY(JLX:Ref_Number),DUP
Record                   RECORD,PRE(JLX)
Record_Number               LONG
Ref_Number                  LONG
Loan_Unit_Number            REAL
Date_Stamp                  DATE
Time_Stamp                  TIME
User_Code                   STRING(3)
Status                      STRING(60)
                         END
                     END
TRDBATCH_XTR          FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(TRXF),NAME(TrdbatchFilename),CREATE,THREAD
PK_Record_Number         KEY(TRX:Record_Number),PRIMARY
TRX_Ref_Number           KEY(TRX:Ref_Number),DUP
Record                   RECORD,PRE(TRX)
Record_Number               LONG
Ref_Number                  LONG
Batch_Number                LONG
Company_Name                STRING(30)
ESN                         STRING(20)
Status                      STRING(3)
Date_Stamp                  DATE
Time_Stamp                  TIME
AuthorisationNo             STRING(30)
Exchanged                   STRING(3)
DateReturn                  DATE
TimeReturn                  TIME
TurnTime                    LONG
MSN                         STRING(30)
                         END
                     END
    MAP
ExtractRecords  PROCEDURE(Date, Date),Long
AddParts        PROCEDURE(Long)
AddWarparts     PROCEDURE(Long)
AddEstparts     PROCEDURE(Long)
AddConthist     PROCEDURE(Long)
AddAudit        PROCEDURE(Long)
AddAudstats     PROCEDURE(Long)
AddJobthird     PROCEDURE(Long)
AddJobpaymt     PROCEDURE(Long)
AddJobstage     PROCEDURE(Long)
AddJobseng      PROCEDURE(Long)
AddTrdbatch     PROCEDURE(Long)
AddJobexhis     PROCEDURE(Long)
AddJoblohis     PROCEDURE(Long)
AddJobacc       PROCEDURE(Long)
CancelCheck     PROCEDURE(),Long
OpenFiles       PROCEDURE(),Long
CloseFiles      PROCEDURE()
ConvertDate     PROCEDURE(Date),Date
    END
mo:SelectedTab  Long  ! makeover template ! LocalTreat = Default ; PT = Window  WT = Window
window               WINDOW('Service Base Extract'),AT(,,216,169),FONT('Tahoma',8,,),CENTER,ICON('pc.ico'),TIMER(1),GRAY,DOUBLE,IMM
                       SHEET,AT(4,2,208,134),USE(?Sheet1),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Extract Folder'),AT(8,16),USE(?Prompt11),TRN
                           ENTRY(@s255),AT(72,16,95,10),USE(ExtractDirectory)
                           BUTTON('...'),AT(172,16,10,10),USE(?DirectoryButton),LEFT,ICON('list3.ico')
                           PROMPT('Select Completed Jobs'),AT(8,36),USE(?Prompt12)
                           PROMPT('From:'),AT(29,48),USE(?Prompt2),TRN
                           ENTRY(@D08B),AT(57,48,95,10),USE(par:StartDate)
                           BUTTON('...'),AT(160,48,10,10),USE(?PopCalendar),FLAT,LEFT,ICON('calenda2.ico')
                           PROMPT('To:'),AT(36,64),USE(?Prompt3),TRN
                           ENTRY(@D08B),AT(57,64,95,10),USE(par:EndDate)
                           BUTTON('...'),AT(160,64,10,10),USE(?PopCalendar:2),FLAT,LEFT,ICON('calenda2.ico')
                           PROMPT('Started At:'),AT(16,80),USE(?Prompt9),TRN
                           ENTRY(@d06B),AT(56,80,68,10),USE(ExtractStartDate),COLOR(COLOR:Silver),READONLY
                           ENTRY(@T04B),AT(132,80,52,10),USE(ExtractStartTime),COLOR(COLOR:Silver),READONLY
                           PROMPT('Finished At:'),AT(12,96),USE(?Prompt10),TRN
                           ENTRY(@d06B),AT(56,96,68,10),USE(ExtractEndDate),COLOR(COLOR:Silver),READONLY
                           ENTRY(@T04B),AT(132,96,52,10),USE(ExtractEndTime),COLOR(COLOR:Silver),READONLY
                           PROMPT('Record No.'),AT(20,112),USE(?RecordCountPrompt),TRN,HIDE
                           STRING(@n8),AT(64,112,32,),USE(RecordCount),TRN,HIDE,RIGHT
                           PROMPT('Jobs Extracted'),AT(108,112),USE(?JobsExtractedPrompt),TRN,HIDE
                           STRING(@n8),AT(160,112),USE(JobsExtracted),TRN,HIDE
                           PROMPT('Building Indexes...'),AT(20,124,104,12),USE(?BuildIndexPrompt),TRN,HIDE
                           STRING(@t04B),AT(132,124),USE(EndBuildIndexTime),TRN,HIDE
                         END
                       END
                       PANEL,AT(4,140,208,26),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Extract'),AT(88,144,56,16),USE(?ExtractButton),FLAT,LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('&Cancel'),AT(148,144,56,16),USE(?CancelButton),FLAT,LEFT,ICON('cancel.ico')
                     END

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Open                   PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
GetUserName  ROUTINE

    Access:USERS.Clearkey(use:Password_Key)
    use:Password = glo:Password
    IF (Access:USERS.Tryfetch(use:Password_Key) = Level:benign) THEN
        IF (CLIP(use:Forename) = '') THEN
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname
        END

        IF (CLIP(LOC:UserName) = '') THEN
            LOC:UserName = '<' & use:User_Code & '>'
        END
    END

    EXIT
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Cellext')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt11
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:JOBPAYMT.Open
  Access:JOBS.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:USERS.UseFile
  Access:AUDSTATS.UseFile
  Access:CONTHIST.UseFile
  Access:ESTPARTS.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBTHIRD.UseFile
  Access:JOBSENG.UseFile
  Access:TRDBATCH.UseFile
  Access:INVOICE.UseFile
  Access:JOBEXHIS.UseFile
  Access:JOBLOHIS.UseFile
  Access:JOBACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
    Loop DBHControl# = Firstfield() To LastField()
        If DBHControl#{prop:type} = Create:OPTION Or |
            DBHControl#{prop:type} = Create:RADIO Or |
            DBhControl#{prop:type} = Create:GROUP
            DBHControl#{prop:trn} = 1      
        End!DBhControl#{prop:type} = 'GROUP'
    End!Loop DBHControl# = Firstfield() To LastField()
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,?Sheet1)
      
  
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:JOBPAYMT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Open, ())
  SET(defaults)
  access:defaults.next()
  
  par:EndDate = TODAY()
  par:StartDate = DEFORMAT('1/1/1990', @d6)
  !par:StartJobNumber = GETINI('EXTRACT','JobNumber',0,CLIP(PATH())&'\EXTRACT.INI')
  !par:EndJobNumber = par:StartJobNumber
  
  LOC:ApplicationName = 'ServiceBase 2000'
  LOC:ProgramName = 'ServiceBase Extract'
  LOC:UserName = ''
  
  ExtractDirectory = CLIP(GETINI('EXTRACT','Path',CLIP(PATH()) & '\EXTRACT',CLIP(PATH())&'\EXTRACT.INI'))
  
  DO GetUserName
    
  DISPLAY
  PARENT.Open
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Open, ())


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DirectoryButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DirectoryButton, Accepted)
      filedialog ('Choose Directory',ExtractDirectory,'All Directories|*.*', |
                        file:save + file:keepdir + file:noerror + file:longname + file:directory)
      DISPLAY(?ExtractDirectory)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DirectoryButton, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:StartDate = TINCALENDARStyle1(par:StartDate)
          Display(?par:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:EndDate = TINCALENDARStyle1(par:EndDate)
          Display(?par:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ExtractButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ExtractButton, Accepted)
      savepath = PATH()
      
      CASE MessageEx('You have chosen to extract completed jobs between<13,10>' &|
                       FORMAT(par:StartDate, @d08) & ' and ' & FORMAT(par:EndDate, @d08) & '.'&|
                      '<13,10>Do you want to continue?','ServiceBase 2000',|
                      'Styles\question.ico','&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                      beep:systemquestion,msgex:samewidths,84,26,0)
      Of 1 ! &Yes Button
          IF (OpenFiles() = 0) THEN
              IF (ExtractRecords(par:StartDate, par:EndDate) = 0) THEN
                  CloseFiles()
                  MessageEx('The Data Extract has completed.', |
                            'ServiceBase 2000',|
                            'Styles\idea.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
              ELSE
                  CloseFiles()
              END
          END
      OF 2 ! &No Button
      END
      
      
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ExtractButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
ExtractRecords   PROCEDURE(IN:StartDate, IN:EndDate)
    CODE

        JobsExtracted = 0
        RecordCount = 0
        UNHIDE(?RecordCountPrompt)
        UNHIDE(?RecordCount)
        UNHIDE(?JobsExtractedPrompt)
        UNHIDE(?JobsExtracted)

        HIDE(?BuildIndexPrompt)
        HIDE(?EndBuildIndexTime)

        ExtractStartDate = TODAY()
        ExtractStartTime = CLOCK()

        SETCURSOR(CURSOR:Wait)

        Access:JOBS.ClearKey(JOB:DateCompletedKey)
        JOB:Date_Completed = IN:StartDate
        SET(JOB:DateCompletedKey,JOB:DateCompletedKey)
        LOOP
            IF ((Access:JOBS.NEXT() <> Level:Benign) OR (JOB:Date_Completed > IN:EndDate)) THEN
                BREAK
            END

            RecordCount += 1
            DISPLAY(?RecordCount)
            IF (CancelCheck() = 1) THEN
                HIDE(?RecordCountPrompt)
                HIDE(?RecordCount)
                HIDE(?JobsExtractedPrompt)
                HIDE(?JobsExtracted)
                SETCURSOR()
                RETURN(1)
            END

            IF (job:Completed <> 'YES') THEN
                CYCLE
            END

            IF (ExtractMode <> 0) THEN
                CLEAR(JOBS_XTR)
                jxt:Ref_Number = job:Ref_Number
                GET(JOBS_XTR, jxtf:pk_ref_number)
                ! If this record already exists in Extract File
                ! then ignore insert record.
                IF (ERRORCODE() = 0) THEN
                    CYCLE
                END
            END

            CLEAR(JOBS_XTR)

            jxt:Ref_Number                  = job:Ref_Number
            jxt:Delete_Flag                 = 0
            jxt:Batch_Number                = job:Batch_Number        ! ***
            jxt:Internal_Status             = job:Internal_Status   ! ***
            jxt:Auto_Search                 = job:Auto_Search   ! ***
            jxt:Who_Booked                  = job:Who_Booked
            jxt:Date_Booked                 = ConvertDate(job:Date_Booked)
            jxt:Time_Booked                 = job:Time_Booked
            jxt:Cancelled                   = job:Cancelled
            jxt:Bouncer                     = job:Bouncer
            jxt:Bouncer_Type                = job:Bouncer_Type    ! ***
            jxt:Web_Type                    = job:Web_Type     ! ***
            jxt:Warranty_Job                = job:Warranty_Job
            jxt:Chargeable_Job              = job:Chargeable_Job
            jxt:Model_Number                = job:Model_Number
            jxt:Manufacturer                = job:Manufacturer
            jxt:ESN                         = job:ESN
            jxt:MSN                         = job:MSN
            jxt:ProductCode                 = job:ProductCode
            jxt:Unit_Type                   = job:Unit_Type
            jxt:Colour                      = job:Colour   ! ***
            jxt:Location_Type               = job:Location_Type   ! ***
            jxt:Phone_Lock                  = job:Phone_Lock   ! ***
            jxt:Workshop                    = job:Workshop     ! ***
            jxt:Location                    = job:Location
            jxt:Authority_Number            = job:Authority_Number    ! ***
            jxt:Insurance_Ref_Number        = job:Insurance_Reference_Number    ! ***
            jxt:DOP                         = ConvertDate(job:DOP)
            jxt:Insurance                   = job:Insurance      ! ***
            jxt:Insurance_Type              = job:Insurance_Type    ! ****
            jxt:Transit_Type               = job:Transit_Type    ! ***
            jxt:Physical_Damage             = job:Physical_Damage     ! ***
            jxt:Intermittent_Fault          = job:Intermittent_Fault     ! ***
            jxt:Loan_Status                 = job:Loan_Status    ! ***
            jxt:Exchange_Status             = job:Exchange_Status
            jxt:Job_Priority                = job:Job_Priority     ! ***
            jxt:Charge_Type                 = job:Charge_Type
            jxt:Warranty_Charge_Type        = job:Warranty_Charge_Type
            jxt:Current_Status              = job:Current_Status
            jxt:Account_Number              = job:Account_Number
            jxt:Trade_Account_Name          = job:Trade_Account_Name    ! ***
            jxt:Department_Name             = job:Department_Name    ! ***
            jxt:Order_Number                = job:Order_Number
            jxt:POP                         = job:POP
            jxt:In_Repair                   = job:In_Repair      ! ***
            jxt:Date_In_Repair              = ConvertDate(job:Date_In_Repair)
            jxt:Time_In_Repair              = job:Time_In_Repair
            jxt:Date_On_Test                = ConvertDate(job:Date_On_Test)
            jxt:Time_On_Test                = job:Time_On_Test
            jxt:Estimate_Ready              = job:Estimate_Ready     ! ***
            jxt:QA_Passed                   = job:QA_Passed
            jxt:Date_QA_Passed              = ConvertDate(job:Date_QA_Passed)
            jxt:Time_QA_Passed              = job:Time_QA_Passed
            jxt:QA_Rejected                 = job:QA_Rejected
            jxt:Date_QA_Rejected            = ConvertDate(job:Date_QA_Rejected)
            jxt:Time_QA_Rejected            = job:Time_QA_Rejected
            jxt:QA_Second_Passed            = job:QA_Second_Passed
            jxt:Date_QA_Second_Pass         = ConvertDate(job:Date_QA_Second_Passed)
            jxt:Time_QA_Second_Pass         = job:Time_QA_Second_Passed
            jxt:Title                       = job:Title
            jxt:Initial                     = job:Initial
            jxt:Surname                     = job:Surname
            jxt:Postcode                    = job:Postcode
            jxt:Company_name                = job:Company_name
            jxt:Address_Line1               = job:Address_Line1
            jxt:Address_Line2               = job:Address_Line2
            jxt:Address_Line3               = job:Address_Line3
            jxt:Telephone_Number            = job:Telephone_Number
            jxt:Fax_Number                  = job:Fax_Number  ! ***
            jxt:Mobile_Number               = job:Mobile_Number
            jxt:Postcode_Col                = job:Postcode_Collection
            jxt:Company_Name_Col            = job:Company_Name_Collection
            jxt:Address_Line1_Col           = job:Address_Line1_Collection
            jxt:Address_Line2_Col           = job:Address_Line2_Collection
            jxt:Address_Line3_Col           = job:Address_Line3_Collection
            jxt:Telephone_Col               = job:Telephone_Collection
            jxt:Postcode_Del                = job:Postcode_Delivery
            jxt:Company_Name_Del            = job:Company_Name_Delivery
            jxt:Address_line1_Del           = job:Address_line1_Delivery
            jxt:Address_Line2_Del           = job:Address_Line2_Delivery
            jxt:Address_Line3_Del           = job:Address_Line3_Delivery
            jxt:Telephone_Del               = job:Telephone_Delivery
            jxt:Completed                   = job:Completed
            jxt:Date_Completed              = ConvertDate(job:Date_Completed)
            jxt:Time_Completed              = job:Time_Completed
            jxt:Paid                        = job:Paid     ! ***
            jxt:Paid_Warranty               = job:Paid_Warranty     ! ***
            jxt:Date_Paid                   = ConvertDate(job:Date_Paid)          ! ***
            jxt:Repair_Type                 = job:Repair_Type
            jxt:Repair_Type_Warranty        = job:Repair_Type_Warranty
            jxt:Engineer                    = job:Engineer
            jxt:Ignore_Cha_Charges          = job:Ignore_Chargeable_Charges      ! ***
            jxt:Ignore_War_Charges          = job:Ignore_Warranty_Charges      ! ***
            jxt:Ignore_Est_Charges          = job:Ignore_Estimate_Charges      ! ***
            jxt:Advance_Payment             = job:Advance_Payment           ! ***
            jxt:Labour_Cost                 = job:Labour_Cost
            jxt:Labour_Cost_Estimate        = job:Labour_Cost_Estimate
            jxt:Labour_Cost_Warranty        = job:Labour_Cost_Warranty
            jxt:Parts_Cost                  = job:Parts_Cost           ! ***
            jxt:Parts_Cost_Estimate         = job:Parts_Cost_Estimate          ! ***
            jxt:Parts_Cost_Warranty         = job:Parts_Cost_Warranty           ! ***
            jxt:Courier_Cost                = job:Courier_Cost           ! ***
            jxt:Courier_Cost_Est            = job:Courier_Cost_Estimate           ! ***
            jxt:Courier_Cost_War            = job:Courier_Cost_Warranty           ! ***
            jxt:Sub_Total                   = job:Sub_Total           ! ***
            jxt:Sub_Total_Estimate          = job:Sub_Total_Estimate           ! ***
            jxt:Sub_Total_Warranty          = job:Sub_Total_Warranty           ! ***
            jxt:Loan_Issued_Date            = ConvertDate(job:Loan_Issued_Date)           ! ***
            jxt:Loan_Unit_Number            = job:Loan_Unit_Number           ! ***
            jxt:Loan_Accessory              = job:Loan_Accessory       ! ***
            jxt:Loan_User                   = job:Loan_User      ! ***
            jxt:Loan_Courier                = job:Loan_Courier     ! ***
            jxt:Loan_Consignment_Num        = job:Loan_Consignment_Number     ! ***
            jxt:Loan_Despatched             = ConvertDate(job:Loan_Despatched)           ! ***
            jxt:Loan_Despatched_User        = job:Loan_Despatched_User      ! ***
            jxt:Loan_Despatch_Number        = job:Loan_Despatch_Number           ! ***
            jxt:LoanService                 = job:LoaService      ! ***
            jxt:Exchange_Unit_Number        = job:Exchange_Unit_Number
            jxt:Exchange_Authorised         = job:Exchange_Authorised       ! ***
            jxt:Loan_Authorised             = job:Loan_Authorised       ! ***
            jxt:Exchange_Accessory          = job:Exchange_Accessory
            jxt:Exchange_Issued_Date        = ConvertDate(job:Exchange_Issued_Date)
            jxt:Exchange_User               = job:Exchange_User
            jxt:Exchange_Courier            = job:Exchange_Courier
            jxt:Exchange_Consign_Num        = job:Exchange_Consignment_Number      ! ***
            jxt:Exchange_Despatched         = ConvertDate(job:Exchange_Despatched)
            jxt:Exchange_Desp_User          = job:Exchange_Despatched_User
            jxt:Exchange_Desp_Number        = job:Exchange_Despatch_Number
            jxt:Exchange_Service            = job:ExcService         ! ***
            jxt:Date_Despatched             = ConvertDate(job:Date_Despatched)
            jxt:Despatch_Number             = job:Despatch_Number              ! ***
            jxt:Despatch_User               = job:Despatch_User         ! ***
            jxt:Courier                     = job:Courier        ! ***
            jxt:Consignment_Number          = job:Consignment_Number
            jxt:Incoming_Courier            = job:Incoming_Courier        ! ***
            jxt:In_Consignment_Num          = job:Incoming_Consignment_Number
            jxt:Incoming_Date               = ConvertDate(job:Incoming_Date)              ! ***
            jxt:Despatched                  = job:Despatched         ! ***
            jxt:Despatch_Type               = job:Despatch_Type         ! ***
            jxt:Current_Courier             = job:Current_Courier        ! ***
            jxt:JobService                  = job:JobService         ! ***
            jxt:Last_Repair_Days            = job:Last_Repair_Days              ! ***
            jxt:Third_Party_Site            = job:Third_Party_Site
            jxt:Estimate                    = job:Estimate        ! ***
            jxt:Estimate_If_Over            = job:Estimate_If_Over              ! ***
            jxt:Estimate_Accepted           = job:Estimate_Accepted         ! ***
            jxt:Estimate_Rejected           = job:Estimate_Rejected         ! ***
            jxt:Third_Party_Printed         = job:Third_Party_Printed
            jxt:ThirdPartyDateDesp          = ConvertDate(job:ThirdPartyDateDesp)
            jxt:Fault_Code1                 = job:Fault_Code1
            jxt:Fault_Code2                 = job:Fault_Code2
            jxt:Fault_Code3                 = job:Fault_Code3
            jxt:Fault_Code4                 = job:Fault_Code4
            jxt:Fault_Code5                 = job:Fault_Code5
            jxt:Fault_Code6                 = job:Fault_Code6
            jxt:Fault_Code7                 = job:Fault_Code7
            jxt:Fault_Code8                 = job:Fault_Code8
            jxt:Fault_Code9                 = job:Fault_Code9
            jxt:Fault_Code10                = job:Fault_Code10
            jxt:Fault_Code11                = job:Fault_Code11
            jxt:Fault_Code12                = job:Fault_Code12
            jxt:PreviousStatus              = job:PreviousStatus  ! ***
            jxt:StatusUser                  = job:StatusUser   ! ***
            jxt:Status_End_Date             = ConvertDate(job:Status_End_Date)        ! ***
            jxt:Status_End_Time             = job:Status_End_Time        ! ***
            jxt:Turnaround_Time             = job:Turnaround_Time
            jxt:Special_Instructions        = job:Special_Instructions  ! ***
            jxt:InvoiceAccount              = job:InvoiceAccount  ! ***
            jxt:InvoiceStatus               = job:InvoiceStatus   ! ***
            jxt:InvoiceBatch                = job:InvoiceBatch        ! ***
            jxt:InvoiceQuery                = job:InvoiceQuery ! ***
            jxt:EDI                         = job:EDI   ! ***
            jxt:EDI_Batch_Number            = job:EDI_Batch_Number
            jxt:Inv_Exception               = job:Invoice_Exception   ! ***
            jxt:Inv_Failure_Reason          = job:Invoice_Failure_Reason  ! ***
            jxt:Inv_Date                    = ConvertDate(job:Invoice_date)        ! ***
            jxt:Inv_Date_Warranty           = ConvertDate(job:Invoice_Date_warranty)        ! ***
            jxt:Inv_Courier_Cost            = job:Invoice_Courier_Cost        ! ***
            jxt:Inv_Labour_Cost             = job:Invoice_Labour_cost        ! ***
            jxt:Inv_Parts_Cost              = job:Invoice_Parts_Cost        ! ***
            jxt:Inv_Sub_Total               = job:Invoice_Sub_Total        ! ***
            jxt:Inv_Number_Warranty         = job:Invoice_Number_Warranty        ! ***
            jxt:Winv_Courier_Cost           = job:WInvoice_Courier_Cost        ! ***
            jxt:Winv_Labour_Cost            = job:WInvoice_Labour_Cost        ! ***
            jxt:Winv_Parts_Cost             = job:WInvoice_Parts_Cost        ! ***
            jxt:Winv_Sub_Total              = job:WInvoice_Sub_Total        ! ***

            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = job:Ref_Number
            IF (access:JOBNOTES.fetch(jbn:RefNumberKey) = Level:Benign) THEN
                jxt:Fault_Description = jbn:Fault_description
                jxt:Engineers_Notes   = jbn:Engineers_Notes
                jxt:Invoice_Text      = jbn:Invoice_Text
                jxt:Collection_Text   = jbn:Collection_Text
                jxt:Delivery_Text     = jbn:Delivery_Text
                jxt:ColContactName    = jbn:ColContatName
                jxt:ColDepartment     = jbn:ColDepartment
                jxt:DelContactName    = jbn:DelContactName
                jxt:DelDepartment     = jbn:DelDepartment
            END

            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign) THEN
                jxt:JobMark                = jobe:JobMark
                jxt:TraFaultCode1          = jobe:TraFaultCode1
                jxt:TraFaultCode2          = jobe:TraFaultCode2
                jxt:TraFaultCode3          = jobe:TraFaultCode3
                jxt:TraFaultCode4          = jobe:TraFaultCode4
                jxt:TraFaultCode5          = jobe:TraFaultCode5
                jxt:TraFaultCode6          = jobe:TraFaultCode6
                jxt:TraFaultCode7          = jobe:TraFaultCode7
                jxt:TraFaultCode8          = jobe:TraFaultCode8
                jxt:TraFaultCode9          = jobe:TraFaultCode9
                jxt:TraFaultCode10         = jobe:TraFaultCode10
                jxt:TraFaultCode11         = jobe:TraFaultCode11
                jxt:TraFaultCode12         = jobe:TraFaultCode12
                jxt:SimNumber              = jobe:SimNumber
                jxt:JobReceived            = jobe:JobReceived
                jxt:SkillLevel             = jobe:SkillLevel
                jxt:UPSFlagCode            = jobe:UPSFlagCode
                jxt:FailedDelivery         = jobe:FailedDelivery
                jxt:CConfirmSecondEntry    = jobe:CConfirmSecondEntry
                jxt:WConfirmSecondEntry    = jobe:WConfirmSecondEntry
                jxt:EndUserEmailAddress    = jobe:EndUserEmailAddress
                jxt:Network                = jobe:Network
                jxt:ExchangeReason         = jobe:ExchangeReason
                jxt:LoanReason             = jobe:LoanReason
                jxt:InWorkshopDate         = ConvertDate(jobe:InWorkshopDate)
                jxt:InWorkshopTime         = jobe:InworkshopTime
                jxt:Pre_RF_Board_IMEI      = jobe:Pre_RF_Board_IMEI

            END

            IF (ExtractMode = 0) THEN
                APPEND(JOBS_XTR)
            ELSE
                ADD(JOBS_XTR)
            END

            AddParts(job:Ref_Number)
            AddWarparts(job:Ref_Number)
            AddEstparts(job:Ref_Number)
            AddJobstage(job:Ref_Number)
            AddJobthird(job:Ref_Number)
            AddJobpaymt(job:Ref_Number)
            AddAudit(job:Ref_Number)
            AddAudstats(job:Ref_Number)
            AddConthist(job:Ref_Number)
            AddJobseng(job:Ref_Number)
            AddJobexhis(job:Ref_Number)
            AddJoblohis(job:Ref_Number)
            AddJobacc(job:Ref_Number)
            AddTrdbatch(job:Ref_Number)

            JobsExtracted += 1
            DISPLAY(?JobsExtracted)

        END ! Main Processing Loop

        ExtractEndDate = TODAY()
        ExtractEndTime = CLOCK()
        DISPLAY()
        SETCURSOR()

        RETURN(0)
AddParts        PROCEDURE(JobNumber)
    CODE

    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = JobNumber
    SET(par:Part_Number_Key,par:Part_Number_Key)
    LOOP
        IF ((Access:PARTS.NEXT() <> Level:Benign) OR (par:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(PARTS_XTR)

        pxt:Ref_Number = JobNumber

        pxt:Adjustment           = par:Adjustment
        pxt:Part_Ref_Number      = par:Part_Ref_Number
        pxt:Part_Number          = par:Part_Number
        pxt:Description          = par:Description
        pxt:Supplier             = par:Supplier
        pxt:Purchase_Cost        = par:Purchase_Cost
        pxt:Sale_Cost            = par:Sale_Cost
        pxt:Retail_Cost          = par:Retail_Cost
        pxt:Quantity             = par:Quantity
        pxt:Warranty_Part        = par:Warranty_Part
        pxt:Exclude_From_Order   = par:Exclude_From_Order
        pxt:Despatch_Note_Number = par:Despatch_Note_Number
        pxt:Date_Ordered         = ConvertDate(par:Date_Ordered)
        pxt:Pending_Ref_Number   = par:Pending_Ref_Number
        pxt:Order_Number         = par:Order_Number
        pxt:Date_Received        = ConvertDate(par:Date_Received)
        pxt:Order_Part_Number    = par:Order_Part_Number
        pxt:Status_Date          = ConvertDate(par:Status_Date)
        pxt:Fault_Codes_Checked  = par:Fault_Codes_Checked
        pxt:InvoicePart          = par:InvoicePart
        pxt:Credit               = par:Credit
        pxt:Requested            = par:Requested
        pxt:Fault_Code1          = par:Fault_Code1
        pxt:Fault_Code2          = par:Fault_Code2
        pxt:Fault_Code3          = par:Fault_Code3
        pxt:Fault_Code4          = par:Fault_Code4
        pxt:Fault_Code5          = par:Fault_Code5
        pxt:Fault_Code6          = par:Fault_Code6
        pxt:Fault_Code7          = par:Fault_Code7
        pxt:Fault_Code8          = par:Fault_Code8
        pxt:Fault_Code9          = par:Fault_Code9
        pxt:Fault_Code10         = par:Fault_Code10
        pxt:Fault_Code11         = par:Fault_Code11
        pxt:Fault_Code12         = par:Fault_Code12

        IF (ExtractMode = 0) THEN
            APPEND(PARTS_XTR)
        ELSE
            ADD(PARTS_XTR)
        END
    END

    RETURN
AddWarparts        PROCEDURE(JobNumber)
    CODE

    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = JobNumber
    SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
    LOOP
        IF ((Access:WARPARTS.NEXT() <> Level:benign) OR (wpr:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(WARPARTS_XTR)

        wxt:Ref_Number = JobNumber

        wxt:Adjustment           = wpr:Adjustment
        wxt:Part_Ref_Number      = wpr:Part_Ref_Number
        wxt:Part_Number          = wpr:Part_Number
        wxt:Description          = wpr:Description
        wxt:Supplier             = wpr:Supplier
        wxt:Purchase_Cost        = wpr:Purchase_Cost
        wxt:Sale_Cost            = wpr:Sale_Cost
        wxt:Retail_Cost          = wpr:Retail_Cost
        wxt:Quantity             = wpr:Quantity
        wxt:Warranty_Part        = wpr:Warranty_Part
        wxt:Exclude_From_Order   = wpr:Exclude_From_Order
        wxt:Despatch_Note_Number = wpr:Despatch_Note_Number
        wxt:Date_Ordered         = ConvertDate(wpr:Date_Ordered)
        wxt:Pending_Ref_Number   = wpr:Pending_Ref_Number
        wxt:Order_Number         = wpr:Order_Number
        wxt:Date_Received        = ConvertDate(wpr:Date_Received)
        wxt:Order_Part_Number    = wpr:Order_Part_Number
        wxt:Status_Date          = ConvertDate(wpr:Status_Date)
        wxt:Main_Part            = wpr:Main_Part
        wxt:Fault_Codes_Checked  = wpr:Fault_Codes_Checked
        wxt:InvoicePart          = wpr:InvoicePart
        wxt:Credit               = wpr:Credit
        wxt:Requested            = wpr:Requested
        wxt:Fault_Code1          = wpr:Fault_Code1
        wxt:Fault_Code2          = wpr:Fault_Code2
        wxt:Fault_Code3          = wpr:Fault_Code3
        wxt:Fault_Code4          = wpr:Fault_Code4
        wxt:Fault_Code5          = wpr:Fault_Code5
        wxt:Fault_Code6          = wpr:Fault_Code6
        wxt:Fault_Code7          = wpr:Fault_Code7
        wxt:Fault_Code8          = wpr:Fault_Code8
        wxt:Fault_Code9          = wpr:Fault_Code9
        wxt:Fault_Code10         = wpr:Fault_Code10
        wxt:Fault_Code11         = wpr:Fault_Code11
        wxt:Fault_Code12         = wpr:Fault_Code12

        IF (ExtractMode = 0) THEN
            APPEND(WARPARTS_XTR)
        ELSE
            ADD(WARPARTS_XTR)
        END
    END
    RETURN
AddEstparts        PROCEDURE(JobNumber)
    CODE

    Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
    epr:Ref_Number  = JobNumber
    SET(epr:Part_Number_Key,epr:Part_Number_Key)
    LOOP
        IF ((Access:ESTPARTS.NEXT() <> Level:benign) OR (epr:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(ESTPARTS_XTR)

        epx:Ref_Number = JobNumber

        epx:Adjustment           = epr:Adjustment
        epx:Part_Ref_number      = epr:Part_Ref_Number
        epx:Part_Number          = epr:Part_Number
        epx:Description          = epr:Description
        epx:Supplier             = epr:Supplier
        epx:Purchase_Cost        = epr:Purchase_Cost
        epx:Sale_Cost            = epr:Sale_cost
        epx:Retail_Cost          = epr:Retail_Cost
        epx:Quantity             = epr:Quantity
        epx:Warranty_Part        = epr:Warranty_Part
        epx:Exclude_From_Order   = epr:Exclude_From_Order
        epx:Despatch_Note_Number = epr:Despatch_Note_Number
        epx:Date_Ordered         = ConvertDate(epr:Date_Ordered)
        epx:Pending_Ref_Number   = epr:Pending_Ref_Number
        epx:Order_Number         = epr:Order_Number
        epx:Date_Received        = ConvertDate(epr:Date_Received)
        epx:Order_Part_Number    = epr:Order_Part_Number
        epx:Status_Date          = ConvertDate(epr:Status_Date)
        epx:Fault_Code1          = epr:Fault_Code1
        epx:Fault_Code2          = epr:Fault_Code2
        epx:Fault_Code3          = epr:Fault_Code3
        epx:Fault_Code4          = epr:Fault_Code4
        epx:Fault_Code5          = epr:Fault_Code5
        epx:Fault_Code6          = epr:Fault_Code6
        epx:Fault_Code7          = epr:Fault_Code7
        epx:Fault_Code8          = epr:Fault_Code8
        epx:Fault_Code9          = epr:Fault_Code9
        epx:Fault_Code10         = epr:Fault_Code10
        epx:Fault_Code11         = epr:Fault_Code11
        epx:Fault_Code12         = epr:Fault_Code12

        IF (ExtractMode = 0) THEN
            APPEND(ESTPARTS_XTR)
        ELSE
            ADD(ESTPARTS_XTR)
        END
    END
    RETURN
AddAudit        PROCEDURE(JobNumber)
    CODE

    Access:AUDIT.ClearKey(aud:Ref_Number_Key)
    aud:Ref_Number  = JobNumber
    SET(aud:Ref_Number_Key,aud:Ref_Number_Key)
    LOOP
        IF ((Access:AUDIT.NEXT() <> Level:Benign) OR (aud:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(AUDIT_XTR)

        aux:Ref_Number  = JobNumber

        aux:Date_Stamp  = ConvertDate(aud:Date)
        aux:Time_Stamp  = aud:Time
        aux:User_Code   = aud:User
        aux:ActionField = aud:Action
        aux:DummyField  = aud:DummyField
        aux:Notes       = aud:Notes
        aux:Type        = aud:Type

        IF (ExtractMode = 0) THEN
            APPEND(AUDIT_XTR)
        ELSE
            ADD(AUDIT_XTR)
        END
    END
    RETURN
AddAudstats        PROCEDURE(JobNumber)
    CODE

    Access:AUDSTATS.ClearKey(aus:DateChangedKey)
    aus:RefNumber  = JobNumber
    SET(aus:DateChangedKey,aus:DateChangedKey)
    LOOP
        IF ((Access:AUDSTATS.NEXT() <> Level:Benign) OR (aus:RefNumber  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(AUDSTATS_XTR)

        asx:Ref_Number  = JobNumber

        asx:Type        = aus:Type
        asx:DateChanged = ConvertDate(aus:DateChanged)
        asx:TimeChanged = aus:TimeChanged
        asx:OldStatus   = aus:OldStatus
        asx:NewStatus   = aus:NewStatus
        asx:UserCode    = aus:UserCode

        IF (ExtractMode = 0) THEN
            APPEND(AUDSTATS_XTR)
        ELSE
            ADD(AUDSTATS_XTR)
        END
    END
    RETURN
AddConthist        PROCEDURE(JobNumber)
    CODE
    Access:CONTHIST.ClearKey(cht:Ref_Number_Key)
    cht:Ref_Number  = JobNumber
    SET(cht:Ref_Number_Key,cht:Ref_Number_Key)
    LOOP
        IF ((Access:CONTHIST.NEXT() <> Level:Benign) OR (cht:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(CONTHIST_XTR)

        chx:Ref_Number  = JobNumber

        chx:Date_Stamp  = ConvertDate(cht:Date)
        chx:Time_Stamp  = cht:Time
        chx:User_Code   = cht:User
        chx:ActionField = cht:Action

        NoteLen# = LEN(CLIP(cht:Notes))
        IF (NoteLen# < 251) THEN
            chx:Notes_1 = cht:Notes[1 : NoteLen#]
        ELSIF (NoteLen# < 501) THEN
            chx:Notes_1 = cht:Notes[1 : 250]
            chx:Notes_2 = cht:Notes[251 : NoteLen#]
        ELSIF ((NoteLen# < 751)) THEN
            chx:Notes_1 = cht:Notes[1 : 250]
            chx:Notes_2 = cht:Notes[251 : 500]
            chx:Notes_3 = cht:Notes[501 : NoteLen#]
        ELSE
            chx:Notes_1 = cht:Notes[1 : 250]
            chx:Notes_2 = cht:Notes[251 : 500]
            chx:Notes_3 = cht:Notes[501 : 750]
            chx:Notes_4 = cht:Notes[751 : NoteLen#]
        END

        IF (ExtractMode = 0) THEN
            APPEND(CONTHIST_XTR)
        ELSE
            ADD(CONTHIST_XTR)
        END
    END
    RETURN
AddJobstage        PROCEDURE(JobNumber)
    CODE
    Access:JOBSTAGE.ClearKey(jst:Ref_Number_Key)
    jst:Ref_Number  = JobNumber
    SET(jst:Ref_Number_Key,jst:Ref_Number_Key)
    LOOP
        IF ((Access:JOBSTAGE.NEXT() <> Level:Benign) OR (jst:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(JOBSTAGE_XTR)

        jsx:Ref_Number  = JobNumber

        jsx:Job_Stage   =  jst:Job_Stage
        jsx:Date_Stamp  =  ConvertDate(jst:Date)
        jsx:Time_Stamp  =  jst:Time
        jsx:User_Code   =  jst:User

        IF (ExtractMode = 0) THEN
            APPEND(JOBSTAGE_XTR)
        ELSE
            ADD(JOBSTAGE_XTR)
        END
    END
    RETURN
AddJobthird        PROCEDURE(JobNumber)
    CODE
    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
    jot:RefNumber  = JobNumber
    SET(jot:RefNumberKey,jot:RefNumberKey)
    LOOP
        IF ((Access:JOBTHIRD.NEXT() <> Level:Benign) OR (jot:RefNumber  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(JOBTHIRD_XTR)

        jox:Ref_Number         = JobNumber

        jox:OriginalIMEI       = jot:OriginalIMEI
        jox:OutIMEI            = jot:OutIMEI
        jox:InIMEI             = jot:InIMEI
        jox:DateOut            = ConvertDate(jot:DateOut)
        jox:DateDespatched     = ConvertDate(jot:DateDespatched)
        jox:DateIn             = ConvertDate(jot:DateIn)
        jox:ThirdPartyNumber   = jot:ThirdPartyNumber
        jox:OriginalMSN        = jot:OriginalMSN
        jox:outmsn             = jot:outmsn
        jox:InMSN              = jot:InMSN

        IF (ExtractMode = 0) THEN
            APPEND(JOBTHIRD_XTR)
        ELSE
            ADD(JOBTHIRD_XTR)
        END
    END
    RETURN
AddJobpaymt        PROCEDURE(JobNumber)
    CODE
    Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
    jpt:Ref_Number  = JobNumber
    SET(jpt:All_Date_Key,jpt:All_Date_Key)
    LOOP
        IF ((Access:JOBPAYMT.NEXT() <> Level:Benign) OR (jpt:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(JOBPAYMT_XTR)

        jpx:Ref_Number           = JobNumber

        jpx:Date_Stamp           = ConvertDate(jpt:Date)
        jpx:Payment_Type         = jpt:Payment_Type
        jpx:Credit_Card_Number   = jpt:Credit_Card_Number
        jpx:Expiry_Date          = jpt:Expiry_Date
        jpx:Issue_Number         = jpt:Issue_Number
        jpx:Amount               = jpt:Amount
        jpx:User_Code            = jpt:User_Code

        IF (ExtractMode = 0) THEN
            APPEND(JOBPAYMT_XTR)
        ELSE
            ADD(JOBPAYMT_XTR)
        END
    END
    RETURN
AddJobseng        PROCEDURE(JobNumber)
    CODE
    Access:JOBSENG.ClearKey(joe:JobNumberKey)
    joe:JobNumber  = JobNumber
    SET(joe:JobNumberKey,joe:JobNumberKey)
    LOOP
        IF ((Access:JOBSENG.NEXT() <> Level:Benign) OR (joe:JobNumber  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(JOBSENG_XTR)

        jex:Ref_Number           = JobNumber

        jex:UserCode             = joe:UserCode
        jex:DateAllocated        = ConvertDate(joe:DateAllocated)
        jex:TimeAllocated        = joe:TimeAllocated
        jex:AllocatedBy          = joe:AllocatedBy
        jex:EngSkillLevel        = joe:EngSkillLevel
        jex:JobSkillLevel        = joe:JobSkillLevel

        IF (ExtractMode = 0) THEN
            APPEND(JOBSENG_XTR)
        ELSE
            ADD(JOBSENG_XTR)
        END
    END
    RETURN
AddJobacc        PROCEDURE(JobNumber)
    CODE
    Access:JOBACC.ClearKey(jac:Ref_Number_Key)
    jac:Ref_Number  = JobNumber
    SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
    LOOP
        IF ((Access:JOBACC.NEXT() <> Level:Benign) OR (jac:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(JOBACC_XTR)

        jax:Ref_Number           = JobNumber

        jax:Accessory            = jac:Accessory

        IF (ExtractMode = 0) THEN
            APPEND(JOBACC_XTR)
        ELSE
            ADD(JOBACC_XTR)
        END
    END
    RETURN
AddJobexhis        PROCEDURE(JobNumber)
    CODE
    Access:JOBEXHIS.ClearKey(jxh:Ref_Number_Key)
    jxh:Ref_Number  = JobNumber
    SET(jxh:Ref_Number_Key,jxh:Ref_Number_Key)
    LOOP
        IF ((Access:JOBEXHIS.NEXT() <> Level:Benign) OR (jxh:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(JOBEXHIS_XTR)

        jhx:Ref_Number           = JobNumber

        jhx:Loan_Unit_Number     = jxh:Loan_Unit_Number
        jhx:Date_Stamp           = ConvertDate(jxh:Date)
        jhx:Time_Stamp           = jxh:Time
        jhx:User_Code            = jxh:User
        jhx:Status               = jxh:Status

        IF (ExtractMode = 0) THEN
            APPEND(JOBEXHIS_XTR)
        ELSE
            ADD(JOBEXHIS_XTR)
        END
    END
    RETURN
AddJoblohis        PROCEDURE(JobNumber)
    CODE
    Access:JOBLOHIS.ClearKey(jlh:Ref_Number_Key)
    jlh:Ref_Number  = JobNumber
    SET(jlh:Ref_Number_Key,jlh:Ref_Number_Key)
    LOOP
        IF ((Access:JOBLOHIS.NEXT() <> Level:Benign) OR (jlh:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(JOBLOHIS_XTR)

        jlx:Ref_Number           = JobNumber

        jlx:Loan_Unit_Number     = jlh:Loan_Unit_Number
        jlx:Date_Stamp           = ConvertDate(jlh:Date)
        jlx:Time_Stamp           = jlh:Time
        jlx:User_Code            = jlh:User
        jlx:Status               = jlh:Status

        IF (ExtractMode = 0) THEN
            APPEND(JOBLOHIS_XTR)
        ELSE
            ADD(JOBLOHIS_XTR)
        END
    END
    RETURN
AddTrdbatch        PROCEDURE(JobNumber)
    CODE
    Access:TRDBATCH.ClearKey(trb:JobNumberKey)
    trb:Ref_Number  = JobNumber
    SET(trb:JobNumberKey,trb:JobNumberKey)
    LOOP
        IF ((Access:TRDBATCH.NEXT() <> Level:Benign) OR (trb:Ref_Number  <> JobNumber)) THEN
           BREAK
        END

        CLEAR(TRDBATCH_XTR)

        trx:Record_Number        =  trb:RecordNumber
        trx:Ref_Number           =  JobNumber

        trx:Batch_Number         =  trb:Batch_Number
        trx:Company_Name         =  trb:Company_Name
        trx:ESN                  =  trb:ESN
        trx:Status               =  trb:Status
        trx:Date_Stamp           =  ConvertDate(trb:Date)
        trx:Time_Stamp           =  trb:Time
        trx:AuthorisationNo      =  trb:AuthorisationNo
        trx:Exchanged            =  trb:Exchanged
        trx:DateReturn           =  ConvertDate(trb:DateReturn)
        trx:TimeReturn           =  trb:TimeReturn
        trx:TurnTime             =  trb:TurnTime
        trx:MSN                  =  trb:MSN

        IF (ExtractMode = 0) THEN
            APPEND(TRDBATCH_XTR)
        ELSE
            ADD(TRDBATCH_XTR)
        END
    END
    RETURN
OpenFiles   PROCEDURE
CSTR_DIRECTORY  CSTRING(255)
    CODE

    PUTINI('EXTRACT','Path',CLIP(ExtractDirectory),CLIP(PATH())&'\EXTRACT.INI')
    CSTR_Directory = CLIP(ExtractDirectory)
    IF (~EXISTS(CSTR_Directory)) THEN
        res# = MkDir( CSTR_Directory )
    END

    JobsFilename      = CLIP(ExtractDirectory) & '\JOBS.XTR'
    PartsFilename     = CLIP(ExtractDirectory) & '\PARTS.XTR'
    WarpartsFilename  = CLIP(ExtractDirectory) & '\WARPARTS.XTR'
    EstpartsFilename  = CLIP(ExtractDirectory) & '\ESTPARTS.XTR'
    AuditFilename     = CLIP(ExtractDirectory) & '\AUDIT.XTR'
    AudstatsFilename  = CLIP(ExtractDirectory) & '\AUDSTATS.XTR'
    ConthistFilename  = CLIP(ExtractDirectory) & '\CONTHIST.XTR'
    JobstageFilename  = CLIP(ExtractDirectory) & '\JOBSTAGE.XTR'
    JobthirdFilename  = CLIP(ExtractDirectory) & '\JOBTHIRD.XTR'
    JobpaymtFilename  = CLIP(ExtractDirectory) & '\JOBPAYMT.XTR'
    JobsengFilename   = CLIP(ExtractDirectory) & '\JOBSENG.XTR'
    TrdbatchFilename  = CLIP(ExtractDirectory) & '\TRDBATCH.XTR'
    JobexhisFilename = CLIP(ExtractDirectory) & '\JOBEXHIS.XTR'
    JoblohisFilename  = CLIP(ExtractDirectory) & '\JOBLOHIS.XTR'
    JobaccFilename    = CLIP(ExtractDirectory) & '\JOBACC.XTR'

    IF (~EXISTS(JobsFilename)) THEN
        CREATE(JOBS_XTR)
    END

    IF (~EXISTS(PartsFilename)) THEN
        CREATE(PARTS_XTR)
    END

    IF (~EXISTS(WarpartsFilename)) THEN
        CREATE(WARPARTS_XTR)
    END

    IF (~EXISTS(EstpartsFilename)) THEN
        CREATE(ESTPARTS_XTR)
    END

    IF (~EXISTS(AuditFilename)) THEN
        CREATE(AUDIT_XTR)
    END

    IF (~EXISTS(AudstatsFilename)) THEN
        CREATE(AUDSTATS_XTR)
    END

    IF (~EXISTS(ConthistFilename)) THEN
        CREATE(CONTHIST_XTR)
    END

    IF (~EXISTS(JobstageFilename)) THEN
        CREATE(JOBSTAGE_XTR)
    END

    IF (~EXISTS(JobthirdFilename)) THEN
        CREATE(JOBTHIRD_XTR)
    END

    IF (~EXISTS(JobpaymtFilename)) THEN
        CREATE(JOBPAYMT_XTR)
    END

    IF (~EXISTS(JobsengFilename)) THEN
        CREATE(JOBSENG_XTR)
    END

    IF (~EXISTS(TrdbatchFilename)) THEN
        CREATE(TRDBATCH_XTR)
    END

    IF (~EXISTS(JobexhisFilename)) THEN
        CREATE(JOBEXHIS_XTR)
    END

    IF (~EXISTS(JoblohisFilename)) THEN
        CREATE(JOBLOHIS_XTR)
    END

    IF (~EXISTS(JobaccFilename)) THEN
        CREATE(JOBACC_XTR)
    END

    OPEN(JOBS_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(PARTS_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(WARPARTS_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(ESTPARTS_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(AUDIT_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(AUDSTATS_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(CONTHIST_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(JOBTHIRD_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(JOBPAYMT_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(JOBSTAGE_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(JOBSENG_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(TRDBATCH_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(JOBEXHIS_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(JOBLOHIS_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    OPEN(JOBACC_XTR, 22h)
    IF (ERRORCODE()) THEN
        MESSAGE(ERRORFILE() & ': ' & ERROR())
        RETURN(1)
    END

    IF (RECORDS(JOBS_XTR) = 0) THEN
        ExtractMode = 0
    ELSE
        ExtractMode = 1
    END

    RETURN(0)

CloseFiles      PROCEDURE
        CODE
        IF (ExtractMode = 0) THEN
            SETCURSOR(CURSOR:Wait)
            UNHIDE(?BuildIndexPrompt)
            ?BuildIndexPrompt{PROP:Text} = 'Building Index (JOBS)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(JOBS_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(JOBS_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (PARTS)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(PARTS_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(PARTS_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (WARPARTS)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(WARPARTS_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(WARPARTS_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (ESTPARTS)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(ESTPARTS_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(ESTPARTS_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (AUDIT)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(AUDIT_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(AUDIT_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (AUDSTATS)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(AUDSTATS_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(AUDSTATS_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (CONTHIST)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(CONTHIST_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(CONTHIST_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (JOBTHIRD)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(JOBTHIRD_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(JOBTHIRD_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (JOBSTAGE)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(JOBSTAGE_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(JOBSTAGE_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (JOBPAYMT)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(JOBPAYMT_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(JOBPAYMT_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (JOBSENG)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(JOBSENG_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(JOBSENG_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (TRDBATCH)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(TRDBATCH_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(TRDBATCH_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (JOBEXHIS)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(JOBEXHIS_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(JOBEXHIS_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (JOBLOHIS)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(JOBLOHIS_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(JOBLOHIS_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Building Index (JOBACC)...'
            DISPLAY(?BuildIndexPrompt)
            BUILD(JOBACC_XTR)
            IF (ERRORCODE()) THEN
                MESSAGE(ERRORFILE() & ': ' & ERROR())
            END
            CLOSE(JOBACC_XTR)

            ?BuildIndexPrompt{PROP:Text} = 'Build Index Finished'
            DISPLAY(?BuildIndexPrompt)

            EndBuildIndexTime = CLOCK()
            UNHIDE(?EndBuildIndexTime)
            DISPLAY(?EndBuildIndexTime)
            SETCURSOR()
        ELSE
            CLOSE(JOBS_XTR)
            CLOSE(PARTS_XTR)
            CLOSE(WARPARTS_XTR)
            CLOSE(ESTPARTS_XTR)
            CLOSE(AUDIT_XTR)
            CLOSE(AUDSTATS_XTR)
            CLOSE(CONTHIST_XTR)
            CLOSE(JOBTHIRD_XTR)
            CLOSE(JOBSTAGE_XTR)
            CLOSE(JOBPAYMT_XTR)
            CLOSE(JOBSENG_XTR)
            CLOSE(TRDBATCH_XTR)
            CLOSE(JOBEXHIS_XTR)
            CLOSE(JOBLOHIS_XTR)
            CLOSE(JOBACC_XTR)
        END
        RETURN
ConvertDate       PROCEDURE(theDate)
    CODE
    IF (theDate = '') THEN
        RETURN(4)  ! 1/1/1801
    ELSE
        RETURN(theDate)
    END
CancelCheck                    PROCEDURE
    CODE
    cancel# = 0
    ACCEPT
        CASE Event()
            OF Event:Timer
                Break
            OF Event:CloseWindow
                cancel# = 1
                Break
            OF Event:accepted
                IF (Field() = ?CancelButton) THEN
                    cancel# = 1
                    BREAK
                END
        END
    END

    IF (cancel# = 1) THEN
        SETCURSOR()
        CASE MessageEx('Are you sure you want to cancel?',LOC:ApplicationName,|
                       'Styles\question.ico','&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                       beep:systemquestion,msgex:samewidths,84,26,0)
            OF 1 ! &Yes Button
                RETURN(1)
            OF 2 ! &No Button
                SETCURSOR(CURSOR:Wait)
        END
    END

    RETURN(0)
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
