

   MEMBER('celdatex.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELDA007.INC'),ONCE        !Local module procedure declarations
                     END



BatchDelete PROCEDURE                                 !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
PARS                 GROUP,PRE(par)
StartDate            DATE
EndDate              DATE
                     END
SavePath             STRING(255)
RecordCount          LONG
JobsDeleted          LONG
LOC_GROUP            GROUP,PRE(loc)
ApplicationName      STRING(30)
ProgramName          STRING(30)
UserName             STRING(61)
Filename             STRING(255)
                     END
DeleteStartDate      DATE
DeleteStartTime      TIME
DeleteEndDate        DATE
DeleteEndTime        TIME
ExtractDirectory     STRING(255)
JOBS_XTR               FILE,DRIVER('Btrieve','/COMPRESS=ON'),OEM,PRE(JXTF),NAME(JobsFilename),CREATE,THREAD
PK_Ref_Number            KEY(JXT:Ref_Number),PRIMARY
JXT_Delete_Flag          KEY(JXT:Delete_Flag,JXT:Date_Completed),DUP
Record                   RECORD,PRE(JXT)
Ref_Number                  LONG
Delete_Flag                 BYTE
Batch_Number                LONG         ! ***
Internal_Status             STRING(10)    ! ***
Auto_Search                 STRING(30)    ! ***
Who_Booked                  STRING(3)
Date_Booked                 DATE
Time_Booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)     ! ***
Web_Type                    STRING(3)      ! ***
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)    ! ***
Location_Type               STRING(10)    ! ***
Phone_Lock                  STRING(30)    ! ***
Workshop                    STRING(3)      ! *** 
Location                    STRING(30)
Authority_Number            STRING(30)     ! ***
Insurance_Ref_Number        STRING(30)     ! ***
DOP                         DATE
Insurance                   STRING(3)       ! ***
Insurance_Type              STRING(30)     ! ****
Transit_Type                STRING(30)     ! ***
Physical_Damage             STRING(3)      ! ***
Intermittent_Fault          STRING(3)      ! ***
Loan_Status                 STRING(30)     ! *** 
Exchange_Status             STRING(30)
Job_Priority                STRING(30)     ! *** 
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)     ! ***
Department_Name             STRING(30)     ! ***
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)       ! ***
Date_In_Repair              DATE
Time_In_Repair              TIME
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)      ! ***
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Pass         DATE
Time_QA_Second_Pass         TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(10)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)   ! ***
Mobile_Number               STRING(15)
Company_Name_Col            STRING(30)
Address_Line1_Col           STRING(30)
Address_Line2_Col           STRING(30)
Address_Line3_Col           STRING(30)
Postcode_Col                STRING(10)
Telephone_Col               STRING(15)
Company_Name_Del            STRING(30)
Address_Line1_Del           STRING(30)
Address_Line2_Del           STRING(30)
Address_Line3_Del           STRING(30)
Postcode_Del                STRING(10)
Telephone_Del               STRING(15)
Completed                   STRING(3)
Date_Completed              DATE
Time_Completed              TIME
Paid                        STRING(3)      ! ***
Paid_Warranty               STRING(3)      ! ***
Date_Paid                   DATE           ! *** 
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Cha_Charges          STRING(3)       ! ***Ignore_Chargeable_Charges
Ignore_War_Charges          STRING(3)       ! ***Ignore_Warranty_Charges
Ignore_Est_Charges          STRING(3)       ! **Ignore_Estimate_Charges*
Advance_Payment             REAL            ! ***
Labour_Cost                 REAL
Labour_Cost_Estimate        REAL
Labour_Cost_Warranty        REAL
Parts_Cost                  REAL            ! ***
Parts_Cost_Estimate         REAL            ! ***
Parts_Cost_Warranty         REAL            ! ***
Courier_Cost                REAL            ! ***
Courier_Cost_Est            REAL            ! ***Courier_Cost_Estimate
Courier_Cost_War            REAL            ! ***Courier_Cost_Warranty
Sub_Total                   REAL            ! ***
Sub_Total_Estimate          REAL            ! ***
Sub_Total_Warranty          REAL            ! ***
Loan_Issued_Date            DATE            ! ***
Loan_Unit_Number            LONG            ! ***
Loan_Accessory              STRING(3)       ! ***
Loan_User                   STRING(3)       ! ***
Loan_Courier                STRING(30)      ! ***
Loan_Consignment_Num        STRING(30)      ! ***Loan_Consignment_Number
Loan_Despatched             DATE            ! ***
Loan_Despatched_User        STRING(3)       ! ***
Loan_Despatch_Number        LONG            ! ***
LoanService                 STRING(1)       ! *** LoaService
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)        ! ***
Loan_Authorised             STRING(3)        ! ***
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consign_Num         STRING(30)       ! ***Exchange_Consignment_Number
Exchange_Despatched         DATE
Exchange_Desp_User          STRING(3)
Exchange_Desp_Number        LONG
Exchange_Service            STRING(1)          ! ***ExcService
Date_Despatched             DATE
Despatch_Number             LONG               ! ***
Despatch_User               STRING(3)          ! ***
Courier                     STRING(30)         ! ***
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)         ! ***
In_Consignment_Num          STRING(30)
Incoming_Date               DATE               ! ***
Despatched                  STRING(3)          ! ***
Despatch_Type               STRING(3)          ! ***
Current_Courier             STRING(30)         ! ***
JobService                  STRING(1)          ! ***
Last_Repair_Days            LONG               ! *** 
Third_Party_Site            STRING(30)
Estimate                    STRING(3)          ! ***
Estimate_If_Over            REAL               ! ***
Estimate_Accepted           STRING(3)          ! ***
Estimate_Rejected           STRING(3)          ! ***
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
PreviousStatus              STRING(30)   ! ***
StatusUser                  STRING(3)    ! ***
Status_End_Date             DATE         ! ***
Status_End_Time             TIME         ! ***
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)   ! ***
InvoiceAccount              STRING(15)   ! ***
InvoiceStatus               STRING(3)    ! ***
InvoiceBatch                LONG         ! ***
InvoiceQuery                STRING(255)  ! ***
EDI                         STRING(3)    ! ***
EDI_Batch_Number            REAL
Inv_Exception               STRING(3)    ! ***InvoiceException
Inv_Failure_Reason          STRING(80)   ! ***Invoice_Failure_Reason
Inv_Date                    DATE         ! ***Invoice_date
Inv_Date_Warranty           DATE         ! ***Invoice_Date_warranty
Inv_Courier_Cost            REAL         ! ***Invoice_Courier_Cost
Inv_Labour_Cost             REAL         ! ***Invoice_Labour_cost
Inv_Parts_Cost              REAL         ! ***Invoice_Parts_Cost
Inv_Sub_Total               REAL         ! ***Invoice_Sub_Total
Inv_Number_Warranty         LONG         ! ***Invoice_Number_Warranty
Winv_Courier_Cost           REAL         ! ***WInvoice_Courier_Cost
Winv_Labour_Cost            REAL         ! ***WInvoice_Labour-Cost
Winv_Parts_Cost             REAL         ! ***WInvoice_Parts_Cost
Winv_Sub_Total              REAL         ! ***WInvoice_Sub_Total
! JOBSE
JobMark                     BYTE         ! ***
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SimNumber                   STRING(30)  ! ***
JobReceived                 BYTE        ! ***
SkillLevel                  LONG        ! ***
UPSFlagCode                 STRING(1)   ! ***
FailedDelivery              BYTE        ! ***
CConfirmSecondEntry         STRING(3)   ! ***
WConfirmSecondEntry         STRING(3)   ! ***
EndUserEmailAddress         STRING(255) ! ***
Network                     STRING(30)  ! ***
ExchangeReason              STRING(255) ! ***
LoanReason                  STRING(255) ! ***
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
! JOBNOTES
Fault_description           STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Collection_Text             STRING(255)   ! ***
Delivery_Text               STRING(255)   ! ***
ColContactName              STRING(30)    ! ***
ColDepartment               STRING(30)    ! ***
DelContactName              STRING(30)    ! ***
DelDepartment               STRING(30)    ! ***
                         END
                     END
    MAP
DeleteRecords   PROCEDURE(Date, Date),Long
CancelCheck     PROCEDURE(),Long
    END
mo:SelectedTab  Long  ! makeover template ! LocalTreat = Default ; PT = Window  WT = Window
window               WINDOW('Batch Delete'),AT(,,216,200),FONT('Tahoma',8,,),CENTER,ICON('pc.ico'),TIMER(1),GRAY,DOUBLE,IMM
                       SHEET,AT(4,2,208,166),USE(?Sheet1),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Delete Extracted Records'),AT(24,12),USE(?Prompt11),TRN,FONT(,14,,FONT:bold)
                           PROMPT('All Records Deleted will be'),AT(32,32),USE(?Prompt7),TRN,FONT(,12,,FONT:bold)
                           PROMPT('lost from ServiceBase.'),AT(44,44),USE(?Prompt8),TRN,FONT(,12,,FONT:bold)
                           PROMPT('Extract Folder'),AT(8,68),USE(?Prompt12)
                           ENTRY(@s255),AT(60,68,124,10),USE(ExtractDirectory)
                           BUTTON,AT(192,68,10,10),USE(?ExtractDirectoryButton),LEFT,ICON('list3.ico')
                           PROMPT('From:'),AT(32,88),USE(?Prompt2),TRN
                           ENTRY(@D08B),AT(60,88,95,10),USE(par:StartDate)
                           BUTTON('...'),AT(160,88,10,10),USE(?PopCalendar),FLAT,LEFT,ICON('calenda2.ico')
                           PROMPT('To:'),AT(36,104),USE(?Prompt3),TRN
                           ENTRY(@D08B),AT(60,104,95,10),USE(par:EndDate)
                           BUTTON('...'),AT(160,104,10,10),USE(?PopCalendar:2),FLAT,LEFT,ICON('calenda2.ico')
                           PROMPT('Started At:'),AT(20,124),USE(?Prompt9),TRN
                           ENTRY(@d06B),AT(60,124,68,10),USE(DeleteStartDate),COLOR(COLOR:Silver),READONLY
                           ENTRY(@T04_B),AT(136,124,52,10),USE(DeleteStartTime),COLOR(COLOR:Silver),READONLY
                           PROMPT('Ended At:'),AT(20,140),USE(?Prompt10),TRN
                           ENTRY(@d06B),AT(60,140,68,10),USE(DeleteEndDate),COLOR(COLOR:Silver),READONLY
                           ENTRY(@T04_B),AT(136,140,52,10),USE(DeleteEndTime),COLOR(COLOR:Silver),READONLY
                           PROMPT('Record No.'),AT(20,156),USE(?RecordCountPrompt),TRN,HIDE
                           STRING(@n8),AT(64,156,32,),USE(RecordCount),TRN,HIDE,RIGHT
                           PROMPT('Jobs Deleted'),AT(108,156),USE(?JobsDeletedPrompt),TRN,HIDE
                           STRING(@n8),AT(156,156),USE(JobsDeleted),TRN,HIDE
                         END
                       END
                       PANEL,AT(4,172,208,26),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Delete'),AT(88,176,56,16),USE(?DeleteButton),FLAT,LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('&Cancel'),AT(148,176,56,16),USE(?CancelButton),FLAT,LEFT,ICON('cancel.ico')
                     END

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Open                   PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
GetUserName   ROUTINE

    Access:USERS.Clearkey(use:Password_Key)
    use:Password = glo:Password
    IF (Access:USERS.Tryfetch(use:Password_Key) = Level:benign) THEN
        IF (CLIP(use:Forename) = '') THEN
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname
        END

        IF (CLIP(LOC:UserName) = '') THEN
            LOC:UserName = '<' & use:User_Code & '>'
        END
    END

    EXIT
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BatchDelete')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt11
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:JOBPAYMT.Open
  Access:JOBS.UseFile
  Access:AUDSTATS.UseFile
  Access:CONTHIST.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBTHIRD.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:ESTPARTS.UseFile
  Access:USERS.UseFile
  Access:JOBSENG.UseFile
  Access:JOBEXHIS.UseFile
  Access:JOBLOHIS.UseFile
  Access:JOBACC.UseFile
  Access:TRDBATCH.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
    Loop DBHControl# = Firstfield() To LastField()
        If DBHControl#{prop:type} = Create:OPTION Or |
            DBHControl#{prop:type} = Create:RADIO Or |
            DBhControl#{prop:type} = Create:GROUP
            DBHControl#{prop:trn} = 1      
        End!DBhControl#{prop:type} = 'GROUP'
    End!Loop DBHControl# = Firstfield() To LastField()
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,?Sheet1)
      
  
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:JOBPAYMT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Open, ())
  SET(defaults)
  access:defaults.next()
  
  par:EndDate = TODAY()
  par:StartDate = DEFORMAT('1/1/1990', @d6)
  
  LOC:ApplicationName = 'ServiceBase 2000'
  LOC:ProgramName = 'Batch Delete'
  LOC:UserName = ''
  
  ExtractDirectory = CLIP(GETINI('EXTRACT','Path',CLIP(PATH()) & '\EXTRACT',CLIP(PATH())&'\EXTRACT.INI'))
  
  DO GetUserName
    
  DISPLAY
  PARENT.Open
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Open, ())


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ExtractDirectoryButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ExtractDirectoryButton, Accepted)
      filedialog ('Choose Directory',ExtractDirectory,'All Directories|*.*', |
                        file:save + file:keepdir + file:noerror + file:longname + file:directory)
      DISPLAY(?ExtractDirectory)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ExtractDirectoryButton, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:StartDate = TINCALENDARStyle1(par:StartDate)
          Display(?par:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:EndDate = TINCALENDARStyle1(par:EndDate)
          Display(?par:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DeleteButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DeleteButton, Accepted)
      savepath = PATH()
      
      CASE MessageEx('You have chosen to Delete all successfully extracted jobs between<13><10>' &|
                   FORMAT(par:StartDate, @d08) & ' and ' & FORMAT(par:EndDate, @d08) & '.'&|
                  '<13,10>'&|
                  '<13,10>Warning! You will no longer be able to access these jobs from ServiceBase.'&|
                  '<13,10>'&|
                  '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                  'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                  beep:systemquestion,msgex:samewidths,84,26,0)
      Of 1 ! &Yes Button
          JobsFilename      = CLIP(ExtractDirectory) & '\JOBS.XTR'
          OPEN(JOBS_XTR, 22h)
          IF (ERRORCODE()) THEN
              MessageEx(ERRORFILE() & ': ' & ERROR(), |
                        'ServiceBase 2000',|
                        'Styles\idea.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                        beep:systemasterisk,msgex:samewidths,84,26,0)
          ELSE
              IF (DeleteRecords(par:StartDate, par:EndDate) = 0) THEN
                  MessageEx('The Batch Delete has completed.', |
                            'ServiceBase 2000',|
                            'Styles\idea.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
              END
              CLOSE(JOBS_XTR)
          END
      OF 2 ! &No Button
      END
      
      
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DeleteButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
DeleteRecords   PROCEDURE(IN:StartDate, IN:EndDate)
    CODE

        JobsDeleted = 0
        RecordCount = 0
        UNHIDE(?RecordCountPrompt)
        UNHIDE(?RecordCount)
        UNHIDE(?JobsDeletedPrompt)
        UNHIDE(?JobsDeleted)

        DeleteStartDate = TODAY()
        DeleteStartTime = CLOCK()
        DeleteEndDate = 0
        DeleteEndTime = 0
        DISPLAY()

        SETCURSOR(CURSOR:Wait)

        jxt:Delete_Flag = 0
        jxt:Date_Completed = IN:StartDate
        SET(jxtf:jxt_Delete_Flag,jxtf:jxt_Delete_Flag)
        LOOP
            NEXT(JOBS_XTR)

            IF (ERRORCODE()) THEN
                BREAK
            END

            IF ((jxt:Date_Completed > IN:EndDate) OR (jxt:Delete_Flag <> 0)) THEN
                BREAK
            END

            RecordCount += 1
            DISPLAY(?RecordCount)
            DISPLAY(?JobsDeleted)
            IF (CancelCheck() = 1) THEN
                HIDE(?RecordCountPrompt)
                HIDE(?RecordCount)
                HIDE(?JobsDeletedPrompt)
                HIDE(?JobsDeleted)
                SETCURSOR()
                RETURN(1)
            END

            Access:JOBS.ClearKey(JOB:Ref_Number_key)
            job:Ref_Number = jxt:Ref_Number
            IF ((Access:JOBS.Fetch(JOB:Ref_Number_key) <> Level:Benign) OR (job:Completed <> 'YES')) THEN
                CYCLE
            END

            Access:AUDIT.ClearKey(aud:Ref_Number_Key)
            aud:Ref_Number = job:Ref_Number
            SET(aud:Ref_Number_Key,aud:Ref_Number_Key)
            LOOP
                IF ((Access:AUDIT.NEXT() <> Level:Benign) OR (aud:Ref_Number <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(AUDIT)
                !JobsDeleted += 1  ! TEMP
            END

            Access:AUDSTATS.ClearKey(aus:DateChangedKey)
            aus:RefNumber   = job:Ref_Number
            SET(aus:DateChangedKey,aus:DateChangedKey)
            LOOP
                IF ((Access:AUDSTATS.NEXT() <> Level:Benign) OR (aus:RefNumber   <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(AUDSTATS)
                !JobsDeleted += 1  ! TEMP
            END

            Access:CONTHIST.ClearKey(cht:Ref_Number_Key)
            cht:Ref_Number = job:Ref_Number
            SET(cht:Ref_Number_Key,cht:Ref_Number_Key)
            LOOP
                IF ((Access:CONTHIST.NEXT() <> Level:Benign) OR (cht:Ref_Number <> job:Ref_Number )) THEN
                   BREAK
                END
                DELETE(CONTHIST)
                !JobsDeleted += 1  ! TEMP
            END

            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = job:Ref_Number
            SET(jbn:RefNumberKey,jbn:RefNumberKey)
            LOOP
                IF ((Access:JOBNOTES.NEXT() <> Level:benign) OR (jbn:RefNumber <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(JOBNOTES)
                !JobsDeleted += 1  ! TEMP
            END

            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            SET(jobe:RefNumberKey,jobe:RefNumberKey)
            LOOP
                IF ((Access:JOBSE.NEXT() <> Level:Benign) OR (jobe:RefNumber <> job:Ref_Number)) THEN
                   Break
                END
                DELETE(JOBSE)
                !JobsDeleted += 1  ! TEMP
            END

            Access:JOBSTAGE.ClearKey(jst:Ref_Number_Key)
            jst:Ref_Number = job:Ref_Number
            SET(jst:Ref_Number_Key,jst:Ref_Number_Key)
            LOOP
                IF ((Access:JOBSTAGE.NEXT() <> Level:Benign) OR (jst:Ref_Number <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(JOBSTAGE)
                !JobsDeleted += 1  ! TEMP
            END

            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            SET(jot:RefNumberKey,jot:RefNumberKey)
            LOOP
                IF ((Access:JOBTHIRD.NEXT() <> Level:Benign) OR (jot:RefNumber <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(JOBTHIRD)
                !JobsDeleted += 1  ! TEMP
            END

            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            SET(par:Part_Number_Key,par:Part_Number_Key)
            LOOP
                IF ((Access:PARTS.NEXT() <> Level:Benign) OR (par:Ref_Number  <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(PARTS)
                !JobsDeleted += 1  ! TEMP
            END

            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
            LOOP
                IF ((Access:WARPARTS.NEXT() <> Level:benign) OR (wpr:Ref_Number  <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(WARPARTS)
                !JobsDeleted += 1  ! TEMP
            END

            Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
            epr:Ref_Number  = job:Ref_Number
            SET(epr:Part_Number_Key,epr:Part_Number_Key)
            LOOP
                IF ((Access:ESTPARTS.NEXT() <> Level:Benign) OR (epr:Ref_Number  <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(ESTPARTS)
                !JobsDeleted += 1  ! TEMP
            END

            Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
            jpt:Ref_Number = job:Ref_Number
            SET(jpt:All_Date_Key,jpt:All_Date_Key)
            LOOP
                IF ((Access:JOBPAYMT.NEXT() <> Level:benign) OR (jpt:Ref_Number <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(JOBPAYMT)
                !JobsDeleted += 1  ! TEMP
            END

            Access:JOBSENG.ClearKey(joe:JobNumberKey)
            joe:JobNumber = job:Ref_Number
            SET(joe:JobNumberKey,joe:JobNumberKey)
            LOOP
                IF ((Access:JOBSENG.NEXT() <> Level:benign) OR (joe:JobNumber <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(JOBSENG)
                !JobsDeleted += 1  ! TEMP
            END

            Access:JOBEXHIS.ClearKey(jxh:Ref_Number_Key)
            jxh:Ref_Number = job:Ref_Number
            SET(jxh:Ref_Number_Key,jxh:Ref_Number_Key)
            LOOP
                IF ((Access:JOBEXHIS.NEXT() <> Level:benign) OR (jxh:Ref_Number <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(JOBEXHIS)
                !JobsDeleted += 1  ! TEMP
            END

            Access:JOBLOHIS.ClearKey(jlh:Ref_Number_Key)
            jlh:Ref_Number = job:Ref_Number
            SET(jlh:Ref_Number_Key,jlh:Ref_Number_Key)
            LOOP
                IF ((Access:JOBLOHIS.NEXT() <> Level:benign) OR (jlh:Ref_Number <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(JOBLOHIS)
                !JobsDeleted += 1  ! TEMP
            END

            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = job:Ref_Number
            SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
            LOOP
                IF ((Access:JOBACC.NEXT() <> Level:benign) OR (jac:Ref_Number <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(JOBACC)
                !JobsDeleted += 1  ! TEMP
            END

            Access:TRDBATCH.ClearKey(trb:JobNumberKey)
            trb:Ref_Number = job:Ref_Number
            SET(trb:JobNumberKey,trb:JobNumberKey)
            LOOP
                IF ((Access:TRDBATCH.NEXT() <> Level:benign) OR (trb:Ref_Number <> job:Ref_Number)) THEN
                   BREAK
                END
                DELETE(TRDBATCH)
                !JobsDeleted += 1  ! TEMP
            END

            Delete(JOBS)

            jxt:Delete_Flag = 1
            PUT(JOBS_XTR)

            JobsDeleted += 1
            !DISPLAY(?JobsDeleted)

            !IF (JobsDeleted > 1000) THEN  ! TEMP
            !    BREAK
            !END

        END ! Main Processing Loop

        DeleteEndDate = TODAY()
        DeleteEndTime = CLOCK()
        DISPLAY()
        SETCURSOR()
        RETURN(0)
CancelCheck                    PROCEDURE
    CODE
    cancel# = 0
    ACCEPT
        CASE Event()
            OF Event:Timer
                Break
            OF Event:CloseWindow
                cancel# = 1
                Break
            OF Event:accepted
                IF (Field() = ?CancelButton) THEN
                    cancel# = 1
                    BREAK
                END
        END
    END

    IF (cancel# = 1) THEN
        SETCURSOR()
        CASE MessageEx('Are you sure you want to cancel?',LOC:ApplicationName,|
                       'Styles\question.ico','&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                       beep:systemquestion,msgex:samewidths,84,26,0)
            OF 1 ! &Yes Button
                RETURN(1)
            OF 2 ! &No Button
                SETCURSOR(CURSOR:Wait)
        END
    END

    RETURN(0)
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
