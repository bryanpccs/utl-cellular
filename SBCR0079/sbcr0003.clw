

   MEMBER('sbcr0079.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBCR0003.INC'),ONCE        !Local module procedure declarations
                     END


ProcessedDataReport PROCEDURE                         !Generated from procedure template - Window

StartDate            DATE
EndDate              DATE
TempDate             DATE
SavePath             STRING(255)
RecordCount          LONG
LOC_GROUP            GROUP,PRE(loc)
ApplicationName      STRING(30)
ProgramName          STRING(30)
UserName             STRING(61)
Filename             STRING(255)
                     END
FoundCount           LONG
CSVFile     FILE,DRIVER('BASIC','/ALWAYSQUOTE=ON'),PRE(csv),NAME(Out_Filename),CREATE,BINDABLE,THREAD
Record        RECORD
rec_type                STRING(8)        ! STRING(2)     ! Record Type always "CM"
tpm_rcv_no              STRING(10)       ! STRING(7)     ! TPM Reference (Work Order) number
acc_no                  STRING(6)        ! STRING(3)     ! TPM ID (Fix ID by each TPM)
product_code            STRING(12)       ! STRING(8)     ! Product Model Name
IMEI_no                 STRING(15)       ! STRING(15)    ! IMEI number
tpm_rcv_datetime        STRING(16)       ! STRING(10)    ! TPM Receive yyyymmdd
phenomenon_code_1       STRING(17)       ! STRING(7)     ! Symptom confirmed by TPM 1
phenomenon_code_2       STRING(17)       ! STRING(7)     ! Symptom confirmed by TPM 2(op)
cause_code_1            STRING(12)       ! STRING(6)     ! Cause Code by TPM 1
cause_code_2            STRING(12)       ! STRING(6)     ! Cause Code by TPM 2(op)
repair_process_1        STRING(16)       ! STRING(4)     ! Repair Code 1
repair_process_2        STRING(16)       ! STRING(4)     ! Repair Code 2(op)
repair_process_3        STRING(16)       ! STRING(4)     ! Repair Code 3(op)
repair_process_4        STRING(16)       ! STRING(4)     ! Repair Code 4(op)
repair_process_5        STRING(16)       ! STRING(4)     ! Repair Code 5(op)
repair_process_6        STRING(16)       ! STRING(4)     ! Repair Code 6(op)
tech_text               STRING(200)      ! STRING(200)   ! Technician Text
claim_type              STRING(10)       ! STRING(1)     ! Claim Type
contract_type1          STRING(14)       ! STRING(5)     ! Service Type 1
contract_type2          STRING(14)       ! STRING(5)     ! Service Type 2
contract_type3          STRING(14)       ! STRING(5)     ! Service Type 3
complete_input_date     STRING(19)       ! STRING(10)    ! Complete Entry Date
delete_type             STRING(11)       ! STRING(1)     ! Delete Flag
create_datetime         STRING(15)       ! STRING(14)    ! Data create datetime
                        END
              END
window               WINDOW('Processed Data Export'),AT(,,216,103),FONT('Tahoma',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       PROMPT('Completed Date From'),AT(8,16),USE(?Prompt1),TRN,LEFT
                       ENTRY(@D08B),AT(100,16,64,10),USE(StartDate)
                       BUTTON,AT(168,16,10,10),USE(?PopCalendar),IMM,FLAT,LEFT,ICON('calenda2.ico')
                       PROMPT('Completed Date To'),AT(8,32),USE(?Prompt2),TRN,LEFT
                       ENTRY(@D08B),AT(100,32,64,10),USE(EndDate)
                       BUTTON,AT(168,32,10,10),USE(?PopCalendar:2),FLAT,LEFT,ICON('calenda2.ico')
                       SHEET,AT(4,2,208,70),USE(?Sheet1),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Record No.'),AT(20,52),USE(?RecordCountPrompt),TRN,HIDE
                           STRING(@n8),AT(64,52,32,),USE(RecordCount),TRN,HIDE,RIGHT
                           PROMPT('Found'),AT(112,52),USE(?FoundCountPrompt),TRN,HIDE,LEFT
                           STRING(@n8),AT(140,52),USE(FoundCount),TRN,HIDE
                         END
                       END
                       PANEL,AT(4,74,208,26),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('E&xport'),AT(88,78,56,16),USE(?OKButton),FLAT,LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('&Cancel'),AT(148,78,56,16),USE(?CancelButton),FLAT,LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Open                   PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?StartDate{prop:ReadOnly} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 15066597
    Elsif ?StartDate{prop:Req} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 8454143
    Else ! If ?StartDate{prop:Req} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 16777215
    End ! If ?StartDate{prop:Req} = True
    ?StartDate{prop:Trn} = 0
    ?StartDate{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?EndDate{prop:ReadOnly} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 15066597
    Elsif ?EndDate{prop:Req} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 8454143
    Else ! If ?EndDate{prop:Req} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 16777215
    End ! If ?EndDate{prop:Req} = True
    ?EndDate{prop:Trn} = 0
    ?EndDate{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?RecordCountPrompt{prop:FontColor} = -1
    ?RecordCountPrompt{prop:Color} = 15066597
    ?RecordCount{prop:FontColor} = -1
    ?RecordCount{prop:Color} = 15066597
    ?FoundCountPrompt{prop:FontColor} = -1
    ?FoundCountPrompt{prop:Color} = 15066597
    ?FoundCount{prop:FontColor} = -1
    ?FoundCount{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE

    CommandLine = CLIP(COMMAND(''))

    tmpPos = INSTRING('%', CommandLine)
    IF (NOT tmpPos) THEN
        MessageEx('Attempting to use ' & CLIP(LOC:ProgramName) & '<10,13>'           & |
            '   without using ' & CLIP(LOC:ApplicationName) & '.<10,13>'                        & |
            '   Start ' & CLIP(LOC:ApplicationName) & ' and run the report from there.<10,13>',   |
            CLIP(LOC:ApplicationName),                                                            |
            'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
       POST(Event:CloseWindow)
       EXIT
    END

    SET(USERS)
    Access:USERS.Clearkey(use:Password_Key)
    glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
    Access:USERS.Clearkey(use:Password_Key)
    use:Password = glo:Password
    IF (Access:USERS.Tryfetch(use:Password_Key) <> Level:benign) THEN
        MessageEx('Unable to find your logged in user details.', |
                CLIP(LOC:ApplicationName),                                  |
                'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
        POST(Event:CloseWindow)
        EXIT
    END

    IF (CLIP(use:Forename) = '') THEN
        LOC:UserName = use:Surname
    ELSE
        LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname
    END

    IF (CLIP(LOC:UserName) = '') THEN
        LOC:UserName = '<' & use:User_Code & '>'
    END

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ProcessedDataReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:JOBNOTES.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:WARPARTS.UseFile
  Access:PARTS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?StartDate{Prop:Alrt,255} = MouseLeft2
  ?EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBNOTES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  StartDate = TODAY()
  EndDate   = StartDate
  
  LOC:ApplicationName = 'ServiceBase 2000'
  LOC:ProgramName = 'Processed Data Export'
  LOC:UserName = ''
  
  DO GetUserName
    
  DISPLAY
  PARENT.Open


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          StartDate = TINCALENDARStyle1(StartDate)
          Display(?StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          EndDate = TINCALENDARStyle1(EndDate)
          Display(?EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OKButton
      ThisWindow.Update
      savepath = PATH()
      
      SET(defaults)
      access:defaults.next()
      
      fname" = '\CM002-' & FORMAT(TODAY(), @D012) & '.csv'
      
      IF (def:exportpath <> '') THEN
          Out_Filename = CLIP(def:exportpath) & fname"
      ELSE
          Out_Filename = 'C:' & fname"
      END
      
      IF (NOT FILEDIALOG('Choose File',Out_Filename,'*.*', file:save + file:keepdir + file:longname)) THEN
          SETPATH(savepath)
      ELSE
      
          IF (StartDate > EndDate) THEN
              TempDate = EndDate
              EndDate = StartDate
              StartDate = TempDate
          END
      
          REMOVE(CSVFile)
          CREATE(CSVFile)
          OPEN(CSVFile)
          IF (ERROR() <> Level:benign) THEN
              MessageEx('Cannot create export file.', LOC:ApplicationName,|
                          'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                          beep:systemhand,msgex:samewidths,84,26,0)
          ELSE
              SETCURSOR(cursor:wait)
      
              RecordCount = 0
              FoundCount = 0
              TempCount# = 0
              TempCount2# = 0
              UNHIDE(?RecordCountPrompt)
              UNHIDE(?RecordCount)
              UNHIDE(?FoundCountPrompt)
              UNHIDE(?FoundCount)
              DISPLAY()
      
              CLEAR(CSVFile)
              csv:rec_type                = 'rec_type'
              csv:tpm_rcv_no              = 'tpm_rcv_no'
              csv:acc_no                  = 'acc_no'
              csv:product_code            = 'product_code'
              csv:IMEI_no                 = 'IMEI_no'
              csv:tpm_rcv_datetime        = 'tpm_rcv_datetime'
              csv:phenomenon_code_1       = 'phenomenon_code_1'
              csv:phenomenon_code_2       = 'phenomenon_code_2'
              csv:cause_code_1            = 'cause_code_1'
              csv:cause_code_2            = 'cause_code_2'
              csv:repair_process_1        = 'repair_process_1'
              csv:repair_process_2        = 'repair_process_2'
              csv:repair_process_3        = 'repair_process_3'
              csv:repair_process_4        = 'repair_process_4'
              csv:repair_process_5        = 'repair_process_5'
              csv:repair_process_6        = 'repair_process_6'
              csv:tech_text               = 'tech_text'
              csv:claim_type              = 'claim_type'
              csv:contract_type1          = 'contract_type1'
              csv:contract_type2          = 'contract_type2'
              csv:contract_type3          = 'contract_type3'
              csv:complete_input_date     = 'complete_input_date'
              csv:delete_type             = 'delete_type'
              csv:create_datetime         = 'create_datetime'
              ADD(CSVFile)
      
              thedate" = FORMAT(TODAY(), @d012) & ' ' & FORMAT(CLOCK(), @T01)
      
              access:JOBS.clearkey(job:datecompletedkey)
              job:date_completed = StartDate
              SET(job:datecompletedkey,job:datecompletedkey)
              LOOP
                  IF ((access:JOBS.next() <> Level:benign) OR (job:date_completed > EndDate)) THEN
                      BREAK
                  END
      
                  TempCount# += 1
                  TempCount2# += 1
                  IF (TempCount2# >= 61) THEN
                      TempCount2# = 0
                      RecordCount = TempCount#
                      DISPLAY(?RecordCount)
                  END
      
                  IF ((job:manufacturer <> 'SHARP') OR (job:Completed <> 'YES')) THEN
                      CYCLE
                  END
      
                  FoundCount += 1
                  DISPLAY(?FoundCount)
      
                  CLEAR(CSVFile)
                  csv:rec_type                = 'CM'
                  csv:tpm_rcv_no              = SUB(CLIP(LEFT(job:ref_number)), 1, 7)
                  csv:acc_no                  = '002'
                  csv:product_code            = SUB(CLIP(LEFT(job:model_number)), 1, 8)
                  csv:IMEI_no                 = SUB(CLIP(LEFT(job:ESN)), 1, 15)
                  access:jobse.clearkey(jobe:RefNumberKey)
                  jobe:refnumber = job:ref_number
                  IF (access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign) THEN
                      csv:tpm_rcv_datetime        = FORMAT(jobe:InWorkshopDate, @D012)
                  ELSE
                      csv:tpm_rcv_datetime        = ''
                  END
                  csv:phenomenon_code_1       = SUB(CLIP(LEFT(job:Fault_Code5)), 1, 7)
                  csv:phenomenon_code_2       = ''
                  csv:cause_code_1            = SUB(CLIP(LEFT(job:Fault_Code6)), 1, 6)
                  csv:cause_code_2            = ''
                  csv:repair_process_1        = ''
                  csv:repair_process_2        = ''
                  csv:repair_process_3        = ''
                  csv:repair_process_4        = ''
                  csv:repair_process_5        = ''
                  csv:repair_process_6        = ''
      
                  count# = 0
      
                  IF (job:warranty_job = 'YES') THEN
                      access:warparts.clearkey(wpr:part_number_key)
                      wpr:ref_number = job:ref_number
                      SET(wpr:part_number_key, wpr:part_number_key)
                      LOOP
                          IF ((access:WARPARTS.next() <> Level:Benign) OR (wpr:ref_number <> job:ref_number)) THEN
                              BREAK
                          END
                          IF (wpr:part_number = 'ADJUSTMENT') THEN
                              CYCLE
                          END
      
                          count# += 1
                          CASE count#
                          OF 1
                              csv:repair_process_1  = SUB(CLIP(LEFT(wpr:Fault_Code4)), 1, 4)
                          OF 2
                              csv:repair_process_2  = SUB(CLIP(LEFT(wpr:Fault_Code4)), 1, 4)
                          OF 3
                              csv:repair_process_3  = SUB(CLIP(LEFT(wpr:Fault_Code4)), 1, 4)
                          OF 4
                              csv:repair_process_4  = SUB(CLIP(LEFT(wpr:Fault_Code4)), 1, 4)
                          OF 5
                              csv:repair_process_5  = SUB(CLIP(LEFT(wpr:Fault_Code4)), 1, 4)
                          ELSE
                              BREAK
                          END
                      END
                  END
      
                  IF ((job:chargeable_job = 'YES') AND (count# < 5)) THEN
                      access:parts.clearkey(par:part_number_key)
                      par:ref_number = job:ref_number
                      SET(par:part_number_key, par:part_number_key)
                      LOOP
                          IF ((access:PARTS.next() <> Level:Benign) OR (par:ref_number <> job:ref_number)) THEN
                              BREAK
                          END
                          IF (par:part_number = 'ADJUSTMENT') THEN
                              CYCLE
                          END
      
                          count# += 1
                          CASE count#
                          OF 1
                              csv:repair_process_1  = SUB(CLIP(LEFT(par:Fault_Code4)), 1, 4)
                          OF 2
                              csv:repair_process_2  = SUB(CLIP(LEFT(par:Fault_Code4)), 1, 4)
                          OF 3
                              csv:repair_process_3  = SUB(CLIP(LEFT(par:Fault_Code4)), 1, 4)
                          OF 4
                              csv:repair_process_4  = SUB(CLIP(LEFT(par:Fault_Code4)), 1, 4)
                          OF 5
                              csv:repair_process_5  = SUB(CLIP(LEFT(par:Fault_Code4)), 1, 4)
                          ELSE
                              BREAK
                          END
                      END
                  END
      
                  access:JOBNOTES.clearkey(jbn:RefNumberKey)
                  jbn:refnumber = job:ref_number
                  IF (access:JOBNOTES.fetch(jbn:RefNumberKey) = Level:Benign) THEN
                      ! Start Change 3133 BE(24/09/03)
                      !csv:tech_text           = SUB(CLIP(LEFT(jbn:Engineers_notes)), 1, 200)
                      csv:tech_text           = SUB(CLIP(LEFT(jbn:Invoice_Text)), 1, 200)
                      ! End Change 3133 BE(24/09/03)
                  ELSE
                      csv:tech_text           = ''
                  END
      
                  IF (job:warranty_job = 'YES') THEN
                      csv:claim_type          = '0'
                  ELSIF (job:chargeable_job = 'YES') THEN
                      csv:claim_type          = '1'
                  ELSE
                      csv:claim_type          = ''
                  END
      
                  csv:contract_type1          = SUB(CLIP(LEFT(job:Fault_Code7)), 1, 5)
                  csv:contract_type2          = ''
                  csv:contract_type3          = ''
                  csv:complete_input_date     = FORMAT(job:date_completed, @D012)
      
                  IF (job:cancelled = 'YES') THEN
                      csv:delete_type         = '1'
                  ELSE
                      csv:delete_type         = '0'
                  END
      
                  csv:create_datetime = thedate"
                  ADD(CSVFile)
              END
      
              RecordCount = TempCount#
              DISPLAY(?RecordCount)
              CLOSE(CSVFile)
              SETCURSOR()
              MESSAGE('Processed Data Export Complete')
              POST(Event:CloseWindow)
          END
      
      END
      
    OF ?CancelButton
      ThisWindow.Update
      POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

