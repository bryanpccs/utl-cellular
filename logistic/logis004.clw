

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('LOGIS006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS014.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS026.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS027.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS036.INC'),ONCE        !Req'd for module callout resolution
                     END








UpdateLOGSTOCK PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ClubNokia            STRING(30)
SMPFNumber           STRING(30)
DespatchNoteNo       STRING(30)
Sav:RefNumber        LONG
sav:IMEI             STRING(30)
sav:Location         STRING(30)
save_logser_id       USHORT,AUTO
save_lstl_id         USHORT,AUTO
ActionMessage        CSTRING(40)
tmp:instock          LONG
tmp:despatched       LONG
tmp:status           STRING(10)
tmp:locator          STRING(30)
tmp:type             STRING('AVL')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW14::View:Browse   VIEW(LOGASSST)
                       PROJECT(logast:PartNumber)
                       PROJECT(logast:Description)
                       PROJECT(logast:RecordNumber)
                       PROJECT(logast:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
logast:PartNumber      LIKE(logast:PartNumber)        !List box control field - type derived from field
logast:Description     LIKE(logast:Description)       !List box control field - type derived from field
logast:RecordNumber    LIKE(logast:RecordNumber)      !Primary key field - type derived from field
logast:RefNumber       LIKE(logast:RefNumber)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW16::View:Browse   VIEW(LOGSERST)
                       PROJECT(logser:ESN)
                       PROJECT(logser:ClubNokia)
                       PROJECT(logser:Status)
                       PROJECT(logser:RecordNumber)
                       PROJECT(logser:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
logser:ESN             LIKE(logser:ESN)               !List box control field - type derived from field
logser:ClubNokia       LIKE(logser:ClubNokia)         !List box control field - type derived from field
logser:Status          LIKE(logser:Status)            !List box control field - type derived from field
logser:RecordNumber    LIKE(logser:RecordNumber)      !Primary key field - type derived from field
logser:RefNumber       LIKE(logser:RefNumber)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(LOGSTLOC)
                       PROJECT(lstl:Location)
                       PROJECT(lstl:allocquantity)
                       PROJECT(lstl:AvlQuantity)
                       PROJECT(lstl:DesQuantity)
                       PROJECT(lstl:RecordNumber)
                       PROJECT(lstl:RefNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
lstl:Location          LIKE(lstl:Location)            !List box control field - type derived from field
lstl:allocquantity     LIKE(lstl:allocquantity)       !List box control field - type derived from field
lstl:AvlQuantity       LIKE(lstl:AvlQuantity)         !List box control field - type derived from field
lstl:DesQuantity       LIKE(lstl:DesQuantity)         !List box control field - type derived from field
lstl:RecordNumber      LIKE(lstl:RecordNumber)        !Primary key field - type derived from field
lstl:RefNumber         LIKE(lstl:RefNumber)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::logsto:Record LIKE(logsto:RECORD),STATIC
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5504                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask16         DECIMAL(10,0,538770663)          !Xplore
XploreMask116         DECIMAL(10,0,0)                 !Xplore
XploreTitle16        STRING(' ')                      !Xplore
xpInitialTab16       SHORT                            !Xplore
XploreMask14         DECIMAL(10,0,538770663)          !Xplore
XploreMask114         DECIMAL(10,0,0)                 !Xplore
XploreTitle14        STRING(' ')                      !Xplore
xpInitialTab14       SHORT                            !Xplore
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
QuickWindow          WINDOW('Inserting Stock'),AT(,,678,259),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('UpdateLOGSTOCK'),SYSTEM,GRAY,DOUBLE,MDI,IMM
                       SHEET,AT(4,4,224,96),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Sales Code'),AT(8,20),USE(?LGS:SalesCode:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(logsto:SalesCode),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),REQ,UPR
                           PROMPT('Description'),AT(8,36),USE(?logsto:Description:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(logsto:Description),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Model Number'),AT(8,52),USE(?logsto:ModelNumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(logsto:ModelNumber),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupModel),SKIP,ICON('list3.ico')
                           PROMPT('In Stock'),AT(84,76),USE(?tmp:instock:Prompt),TRN
                           ENTRY(@s8),AT(84,84,56,10),USE(tmp:instock),SKIP,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Despatched'),AT(152,76),USE(?tmp:despatched:Prompt),TRN
                           PROMPT('Quantity'),AT(8,84),USE(?logsto:Description:Prompt:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s8),AT(152,84,56,10),USE(tmp:despatched),SKIP,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                         END
                       END
                       SHEET,AT(448,4,228,224),USE(?Sheet3),SPREAD
                         TAB('By E.S.N.'),USE(?Tab3)
                           ENTRY(@s30),AT(456,24,124,10),USE(logser:ESN),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           LIST,AT(456,40,216,164),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('70L(2)|M~E.S.N. / I.M.E.I.~@s16@96L(2)|M~Club Nokia Site~@s30@40L(2)|M~Status~@s' &|
   '10@'),FROM(Queue:Browse:1)
                           BUTTON('&Insert'),AT(496,208,56,16),USE(?Insert:2),LEFT,ICON('insert.ico')
                           BUTTON('&Change'),AT(556,208,56,16),USE(?Change:2),LEFT,ICON('edit.ico')
                           BUTTON('&Delete'),AT(616,208,56,16),USE(?Delete:2),LEFT,ICON('delete.ico')
                         END
                       END
                       SHEET,AT(232,4,212,224),USE(?Sheet4),SPREAD
                         TAB('By Stock Location'),USE(?StockLocation)
                           ENTRY(@s30),AT(236,28,124,10),USE(lstl:Location),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           LIST,AT(236,44,204,160),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('83L(2)|M~Stock Location~@s30@35L(2)|M~Allocated~@s8@37L(2)|M~Qty Avail~@s8@32L(2' &|
   ')|M~Qty Desp~@s8@'),FROM(Queue:Browse:2)
                           BUTTON('&Insert'),AT(264,208,56,16),USE(?Insert:3),LEFT,ICON('insert.ico')
                           BUTTON('&Change'),AT(324,208,56,16),USE(?Change),LEFT,ICON('edit.ico')
                           BUTTON('&Delete'),AT(384,208,56,16),USE(?Delete:3),LEFT,ICON('delete.ico')
                         END
                       END
                       SHEET,AT(4,104,224,124),USE(?Sheet2),SPREAD
                         TAB('Associated Accessories'),USE(?AssociatedStockTab)
                           LIST,AT(8,120,216,84),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('108L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(8,208,56,16),USE(?Insert),LEFT,ICON('insert.ico')
                           BUTTON('&Delete'),AT(168,208,56,16),USE(?Delete),LEFT,ICON('delete.ico')
                         END
                       END
                       BUTTON('&OK'),AT(560,236,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(616,236,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,232,672,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

BRW14                CLASS(BrowseClass)               !Browse using ?List
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse                  !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
BRW16                CLASS(BrowseClass)               !Browse using ?List:2
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW16::Sort0:Locator IncrementalLocatorClass          !Default Locator
BRW5                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  IncrementalLocatorClass          !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

!Save Entry Fields Incase Of Lookup
look:logsto:ModelNumber                Like(logsto:ModelNumber)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore16             CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore16Step1        StepStringClass !STRING          !Xplore: Column displaying logser:ESN
Xplore16Locator1     IncrementalLocatorClass          !Xplore: Column displaying logser:ESN
Xplore16Step2        StepStringClass !STRING          !Xplore: Column displaying logser:ClubNokia
Xplore16Locator2     IncrementalLocatorClass          !Xplore: Column displaying logser:ClubNokia
Xplore16Step3        StepStringClass !STRING          !Xplore: Column displaying logser:Status
Xplore16Locator3     IncrementalLocatorClass          !Xplore: Column displaying logser:Status
Xplore16Step4        StepLongClass   !LONG            !Xplore: Column displaying logser:RecordNumber
Xplore16Locator4     IncrementalLocatorClass          !Xplore: Column displaying logser:RecordNumber
Xplore16Step5        StepLongClass   !LONG            !Xplore: Column displaying logser:RefNumber
Xplore16Locator5     IncrementalLocatorClass          !Xplore: Column displaying logser:RefNumber
Xplore14             CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore14Step1        StepStringClass !STRING          !Xplore: Column displaying logast:PartNumber
Xplore14Locator1     StepLocatorClass                 !Xplore: Column displaying logast:PartNumber
Xplore14Step2        StepStringClass !STRING          !Xplore: Column displaying logast:Description
Xplore14Locator2     StepLocatorClass                 !Xplore: Column displaying logast:Description
Xplore14Step3        StepLongClass   !LONG            !Xplore: Column displaying logast:RecordNumber
Xplore14Locator3     StepLocatorClass                 !Xplore: Column displaying logast:RecordNumber
Xplore14Step4        StepLongClass   !LONG            !Xplore: Column displaying logast:RefNumber
Xplore14Locator4     StepLocatorClass                 !Xplore: Column displaying logast:RefNumber

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateLOGSTOCK',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateLOGSTOCK',1)
    SolaceViewVars('ClubNokia',ClubNokia,'UpdateLOGSTOCK',1)
    SolaceViewVars('SMPFNumber',SMPFNumber,'UpdateLOGSTOCK',1)
    SolaceViewVars('DespatchNoteNo',DespatchNoteNo,'UpdateLOGSTOCK',1)
    SolaceViewVars('Sav:RefNumber',Sav:RefNumber,'UpdateLOGSTOCK',1)
    SolaceViewVars('sav:IMEI',sav:IMEI,'UpdateLOGSTOCK',1)
    SolaceViewVars('sav:Location',sav:Location,'UpdateLOGSTOCK',1)
    SolaceViewVars('save_logser_id',save_logser_id,'UpdateLOGSTOCK',1)
    SolaceViewVars('save_lstl_id',save_lstl_id,'UpdateLOGSTOCK',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateLOGSTOCK',1)
    SolaceViewVars('tmp:instock',tmp:instock,'UpdateLOGSTOCK',1)
    SolaceViewVars('tmp:despatched',tmp:despatched,'UpdateLOGSTOCK',1)
    SolaceViewVars('tmp:status',tmp:status,'UpdateLOGSTOCK',1)
    SolaceViewVars('tmp:locator',tmp:locator,'UpdateLOGSTOCK',1)
    SolaceViewVars('tmp:type',tmp:type,'UpdateLOGSTOCK',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LGS:SalesCode:Prompt;  SolaceCtrlName = '?LGS:SalesCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsto:SalesCode;  SolaceCtrlName = '?logsto:SalesCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsto:Description:Prompt;  SolaceCtrlName = '?logsto:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsto:Description;  SolaceCtrlName = '?logsto:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsto:ModelNumber:Prompt;  SolaceCtrlName = '?logsto:ModelNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsto:ModelNumber;  SolaceCtrlName = '?logsto:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupModel;  SolaceCtrlName = '?LookupModel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:instock:Prompt;  SolaceCtrlName = '?tmp:instock:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:instock;  SolaceCtrlName = '?tmp:instock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:despatched:Prompt;  SolaceCtrlName = '?tmp:despatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsto:Description:Prompt:2;  SolaceCtrlName = '?logsto:Description:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:despatched;  SolaceCtrlName = '?tmp:despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logser:ESN;  SolaceCtrlName = '?logser:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockLocation;  SolaceCtrlName = '?StockLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lstl:Location;  SolaceCtrlName = '?lstl:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AssociatedStockTab;  SolaceCtrlName = '?AssociatedStockTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore16.GetBbSize('UpdateLOGSTOCK','?List:2')      !Xplore
  BRW16.SequenceNbr = 0                               !Xplore
  Xplore14.GetBbSize('UpdateLOGSTOCK','?List')        !Xplore
  BRW14.SequenceNbr = 0                               !Xplore
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Stock Item'
  OF ChangeRecord
    ActionMessage = 'Changing A Stock Item'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateLOGSTOCK')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateLOGSTOCK')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LGS:SalesCode:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(logsto:Record,History::logsto:Record)
  SELF.AddHistoryField(?logsto:SalesCode,2)
  SELF.AddHistoryField(?logsto:Description,3)
  SELF.AddHistoryField(?logsto:ModelNumber,4)
  SELF.AddUpdateFile(Access:LOGSTOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOGASSST.Open
  Relate:LOGSALCD.Open
  Relate:LOGSTOLC.Open
  Relate:MODELNUM.Open
  Access:LOGSTOCK.UseFile
  Access:STOCK.UseFile
  Access:USERS.UseFile
  Access:LOGSTHII.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOGSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW14.Init(?List,Queue:Browse.ViewPosition,BRW14::View:Browse,Queue:Browse,Relate:LOGASSST,SELF)
  BRW16.Init(?List:2,Queue:Browse:1.ViewPosition,BRW16::View:Browse,Queue:Browse:1,Relate:LOGSERST,SELF)
  BRW5.Init(?List:3,Queue:Browse:2.ViewPosition,BRW5::View:Browse,Queue:Browse:2,Relate:LOGSTLOC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore16.Init(ThisWindow,BRW16,Queue:Browse:1,QuickWindow,?List:2,'logistic.INI','>Header',1,BRW16.ViewOrder,Xplore16.RestoreHeader,BRW16.SequenceNbr,XploreMask16,XploreMask116,XploreTitle16,BRW16.FileSeqOn)
  Xplore14.Init(ThisWindow,BRW14,Queue:Browse,QuickWindow,?List,'logistic.INI','>Header',1,BRW14.ViewOrder,Xplore14.RestoreHeader,BRW14.SequenceNbr,XploreMask14,XploreMask114,XploreTitle14,BRW14.FileSeqOn)
  IF ?logsto:ModelNumber{Prop:Tip} AND ~?LookupModel{Prop:Tip}
     ?LookupModel{Prop:Tip} = 'Select ' & ?logsto:ModelNumber{Prop:Tip}
  END
  IF ?logsto:ModelNumber{Prop:Msg} AND ~?LookupModel{Prop:Msg}
     ?LookupModel{Prop:Msg} = 'Select ' & ?logsto:ModelNumber{Prop:Msg}
  END
  BRW14.Q &= Queue:Browse
  BRW14.AddSortOrder(,logast:RefNumberKey)
  BRW14.AddRange(logast:RefNumber,logsto:RefNumber)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,logast:PartNumber,1,BRW14)
  BRW14.AddField(logast:PartNumber,BRW14.Q.logast:PartNumber)
  BRW14.AddField(logast:Description,BRW14.Q.logast:Description)
  BRW14.AddField(logast:RecordNumber,BRW14.Q.logast:RecordNumber)
  BRW14.AddField(logast:RefNumber,BRW14.Q.logast:RefNumber)
  BRW16.Q &= Queue:Browse:1
  BRW16.AddSortOrder(,logser:RefNumberKey)
  BRW16.AddRange(logser:RefNumber,lstl:RecordNumber)
  BRW16.AddLocator(BRW16::Sort0:Locator)
  BRW16::Sort0:Locator.Init(?logser:esn,logser:ESN,1,BRW16)
  BRW16.AddField(logser:ESN,BRW16.Q.logser:ESN)
  BRW16.AddField(logser:ClubNokia,BRW16.Q.logser:ClubNokia)
  BRW16.AddField(logser:Status,BRW16.Q.logser:Status)
  BRW16.AddField(logser:RecordNumber,BRW16.Q.logser:RecordNumber)
  BRW16.AddField(logser:RefNumber,BRW16.Q.logser:RefNumber)
  BRW5.Q &= Queue:Browse:2
  BRW5.AddSortOrder(,lstl:RefLocationKey)
  BRW5.AddRange(lstl:RefNumber,Relate:LOGSTLOC,Relate:LOGSTOCK)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(?lstl:Location,lstl:Location,1,BRW5)
  BRW5.AddField(lstl:Location,BRW5.Q.lstl:Location)
  BRW5.AddField(lstl:allocquantity,BRW5.Q.lstl:allocquantity)
  BRW5.AddField(lstl:AvlQuantity,BRW5.Q.lstl:AvlQuantity)
  BRW5.AddField(lstl:DesQuantity,BRW5.Q.lstl:DesQuantity)
  BRW5.AddField(lstl:RecordNumber,BRW5.Q.lstl:RecordNumber)
  BRW5.AddField(lstl:RefNumber,BRW5.Q.lstl:RefNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?CurrentTab)
  ?Insert:2{prop:trn} = 0
  ?Change:2{prop:trn} = 0
  ?Delete:2{prop:trn} = 0
  ?Insert:3{prop:trn} = 0
  ?Change{prop:trn} = 0
  ?Delete:3{prop:trn} = 0
  ?Insert{prop:trn} = 0
  ?Delete{prop:trn} = 0
  QuickWindow{prop:buffer} = 1
  BRW14.AskProcedure = 2
  BRW5.AskProcedure = 3
  BRW16.AskProcedure = 4
  BRW14.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGASSST.Close
    Relate:LOGSALCD.Close
    Relate:LOGSTOLC.Close
    Relate:MODELNUM.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore16.EraseVisual()                            !Xplore
    Xplore14.EraseVisual()                            !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore16.sq.Col = Xplore16.CurrentCol
    GET(Xplore16.sq,Xplore16.sq.Col)
    IF Xplore16.ListType = 1 AND BRW16.ViewOrder = False !Xplore
      BRW16.SequenceNbr = 0                           !Xplore
    END                                               !Xplore
    Xplore16.PutInix('UpdateLOGSTOCK','?List:2',BRW16.SequenceNbr,Xplore16.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisWindow.Opened                                !Xplore
    Xplore14.sq.Col = Xplore14.CurrentCol
    GET(Xplore14.sq,Xplore14.sq.Col)
    IF Xplore14.ListType = 1 AND BRW14.ViewOrder = False !Xplore
      BRW14.SequenceNbr = 0                           !Xplore
    END                                               !Xplore
    Xplore14.PutInix('UpdateLOGSTOCK','?List',BRW14.SequenceNbr,Xplore14.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore16.Kill()                                     !Xplore
  Xplore14.Kill()                                     !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateLOGSTOCK',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If request = Insertrecord And Field() = ?Insert
      access:logassst.cancelautoinc()
      do_update# = 0
      Globalrequest = Selectrecord
      Browse_Stock
      If Globalresponse = RequestCompleted
          Get(logassst,0)
          If access:logassst.primerecord() = Level:Benign
              logast:RefNumber    = logsto:RefNumber
              logast:PartNumber   = sto:part_number
              logast:Description  = sto:description
              logast:PartRefNumber    = sto:ref_number
              logast:location     = sto:location
              If access:logassst.tryinsert()
                  access:logassst.cancelautoinc()
              End!If access:logassst.tryinsert()
          End!If access:logassst.primerecord() = Level:Benign
      End!If Globalresponse = RequestCompleted
  
  End!If request = Insert And Field() = ?Insert
  
  If request = Deleterecord and Field() = ?Delete:2
      BRW16.UpdateBuffer()
      BRW5.UpdateBuffer()
      sav:IMEI     = brw16.q.logser:esn
      sav:Location = brw5.q.lstl:location
      sav:RefNumber = brw16.q.logser:refnumber
  End!If request = Deleterecord and Field() = ?Delete:2
  If do_update# = 1
  
  End!If do_update# = 1
  If Request = Deleterecord And Field() = ?Delete:2 ! And GlobalResponse = RequestCompleted
      !Add to history
      !Get History!
      Deletion_Reason = ''
  
  
  DeletionReason()
      IF Deletion_Reason = ''
        Access:USERS.Clearkey(use:Password_Key)
        use:Password          = glo:Password
        Access:USERS.Tryfetch(use:Password_Key)
        Deletion_Reason = 'USER REFUSED TO GIVE REASON - '&use:User_Code
      END
      Access:LogsThis.ClearKey(logsth:RecordNumberKey)
      logsth:RecordNumber = SAV:RefNumber
      IF Access:LogsThis.Fetch(logsth:RecordNumberKey)
        !Error!
        SMPFNumber = 'UNKNOWN'
        DespatchNoteNo = 'UNKNOWN'
        ClubNokia = 'UNKNOWN'
      ELSE
        SMPFNumber = logsth:SMPFNumber
        DespatchNoteNo = logsth:DespatchNoteNo
        ClubNokia = logsth:ClubNokia
      END
      If Access:LOGSTHIS.PrimeRecord() = Level:Benign
          logsth:RefNumber      = logsto:RefNumber
          logsth:Date           = Today()
          Access:USERS.Clearkey(use:Password_Key)
          use:Password          = glo:Password
          Access:USERS.Tryfetch(use:Password_Key)
          logsth:UserCode       = use:User_Code
          logsth:StockOut       = 1
          logsth:Status         = 'DELETED'
          logsth:ModelNumber    = logsto:ModelNumber
          logsth:Location       = sav:Location
          logsth:SMPFNumber     = SMPFNumber
          logsth:DespatchNoteNo = DespatchNoteNo
          logsth:ClubNokia      = ClubNokia
          logsth:Deletion_Reason = Deletion_Reason
          If Access:LOGSTHIS.Insert()
              Access:LOGSTHIS.CancelAutoInc()
          End !If Access:LOGSTHIS.Insert()
          If Access:LOGSTHII.PrimeRecord() = Level:Benign
              logsti:RefNumber    = logsth:RecordNumber
              logsti:IMEI         = sav:IMEI
              If Access:LOGSTHII.Insert()
                  Access:LOGSTHII.CancelAutoInc()
              End
          End
      ELSE
        MESSAGE(Errorcode())
      End !If Access:LOGSTHIS.PrimeRecord() = Level:Benign
  End !If Request = Deleterecord And Field() = ?Delete:2 And GlobalResponse = RequestCompleted
  
      WorkOutQty(logsto:refnumber)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      BrowseModelNum
      UpdateLOGASSST
      UpdateStockLocation
      UpdateLOGSERST
    END
    ReturnValue = GlobalResponse
  END
  BRW5.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore16.Upper = True                               !Xplore
  Xplore14.Upper = True                               !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore16.AddField(logser:ESN,BRW16.Q.logser:ESN)
  Xplore16.AddField(logser:ClubNokia,BRW16.Q.logser:ClubNokia)
  Xplore16.AddField(logser:Status,BRW16.Q.logser:Status)
  Xplore16.AddField(logser:RecordNumber,BRW16.Q.logser:RecordNumber)
  Xplore16.AddField(logser:RefNumber,BRW16.Q.logser:RefNumber)
  BRW16.FileOrderNbr = BRW16.AddSortOrder(,logser:RefNumberKey) !Xplore Sort Order for File Sequence
  BRW16.AddRange(logser:RefNumber,lstl:RecordNumber)  !Xplore
  BRW16.SetOrder('')                                  !Xplore
  BRW16.ViewOrder = True                              !Xplore
  Xplore16.AddAllColumnSortOrders(1)                  !Xplore
  BRW16.ViewOrder = False                             !Xplore
  Xplore14.AddField(logast:PartNumber,BRW14.Q.logast:PartNumber)
  Xplore14.AddField(logast:Description,BRW14.Q.logast:Description)
  Xplore14.AddField(logast:RecordNumber,BRW14.Q.logast:RecordNumber)
  Xplore14.AddField(logast:RefNumber,BRW14.Q.logast:RefNumber)
  BRW14.FileOrderNbr = BRW14.AddSortOrder(,logast:RefNumberKey) !Xplore Sort Order for File Sequence
  BRW14.AddRange(logast:RefNumber,logsto:RefNumber)   !Xplore
  BRW14.SetOrder('')                                  !Xplore
  BRW14.ViewOrder = True                              !Xplore
  Xplore14.AddAllColumnSortOrders(1)                  !Xplore
  BRW14.ViewOrder = False                             !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?logsto:ModelNumber
      IF logsto:ModelNumber OR ?logsto:ModelNumber{Prop:Req}
        mod:Model_Number = logsto:ModelNumber
        !Save Lookup Field Incase Of error
        look:logsto:ModelNumber        = logsto:ModelNumber
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            logsto:ModelNumber = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            logsto:ModelNumber = look:logsto:ModelNumber
            SELECT(?logsto:ModelNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupModel
      ThisWindow.Update
      mod:Model_Number = logsto:ModelNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          logsto:ModelNumber = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?logsto:ModelNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?logsto:ModelNumber)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateLOGSTOCK')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore16.IgnoreEvent = True                      !Xplore
     Xplore16.IgnoreEvent = False                     !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore14.IgnoreEvent = True                      !Xplore
     Xplore14.IgnoreEvent = False                     !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?logsto:ModelNumber
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logsto:ModelNumber, AlertKey)
      Post(Event:Accepted,?lookupmodel)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logsto:ModelNumber, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    OF ?Sheet3
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    OF ?Sheet4
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    OF ?Sheet2
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?logser:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logser:ESN, Selected)
      Select(?List:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logser:ESN, Selected)
    OF ?lstl:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lstl:Location, Selected)
      Select(?List:3)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lstl:Location, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 16)                       !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      POST(xpEVENT:Xplore + 14)                       !Xplore
    OF EVENT:GainFocus
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore16.GetColumnInfo()                        !Xplore
      Xplore14.GetColumnInfo()                        !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW14.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,14,'GetFreeElementName'
  IF BRW14.ViewOrder = True                           !Xplore
     RETURN('UPPER(' & CLIP(BRW14.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW14.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,14,'GetFreeElementPosition'
  IF BRW14.ViewOrder = True                           !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW14.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.DeleteControl=?Delete
  END


BRW14.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,14
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore14.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore14.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore14.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW14.ViewOrder = True                           !Xplore
    SavePtr# = POINTER(Xplore14.sq)                   !Xplore
    Xplore14.SetupOrder(BRW14.SortOrderNbr)           !Xplore
    GET(Xplore14.sq,SavePtr#)                         !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW14.SortOrderNbr,Force)       !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW14.FileSeqOn = True                        !Xplore
    RETURN SELF.SetSort(BRW14.FileOrderNbr,Force)     !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW14.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,14,'TakeEvent'
  Xplore14.LastEvent = EVENT()                        !Xplore
  IF FOCUS() = ?List                                  !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore14.AdjustAllColumns()                  !Xplore
      OF AltF12                                       !Xplore
         Xplore14.ResetToDefault()                    !Xplore
      OF AltR                                         !Xplore
         Xplore14.ToggleBar()                         !Xplore
      OF AltM                                         !Xplore
         Xplore14.InvokePopup()                       !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore14.RightButtonUp                         !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW14.FileSeqOn = False                        !Xplore
       Xplore14.LeftButtonUp(1)                       !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW14.SavePosition()                          !Xplore
       Xplore14.HandleMyEvents()                      !Xplore
       !BRW14.RestorePosition()                       !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List                             !Xplore
  PARENT.TakeEvent


BRW14.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore14.LeftButton2()                       !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW16.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,16,'GetFreeElementName'
  IF BRW16.ViewOrder = True                           !Xplore
     RETURN('UPPER(' & CLIP(BRW16.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW16.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,16,'GetFreeElementPosition'
  IF BRW16.ViewOrder = True                           !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW16.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW16.ResetFromView PROCEDURE

tmp:instock:Cnt      LONG
tmp:despatched:Cnt   LONG
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:LOGSERST.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    IF (logser:Status = 'AVAILABLE')
      tmp:instock:Cnt += 1
    END
    IF (logser:Status = 'DESPATCHED')
      tmp:despatched:Cnt += 1
    END
  END
  tmp:instock = tmp:instock:Cnt
  tmp:despatched = tmp:despatched:Cnt
  PARENT.ResetFromView
  Relate:LOGSERST.SetQuickScan(0)
  SETCURSOR()


BRW16.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,16
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore16.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore16.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore16.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW16.ViewOrder = True                           !Xplore
    SavePtr# = POINTER(Xplore16.sq)                   !Xplore
    Xplore16.SetupOrder(BRW16.SortOrderNbr)           !Xplore
    GET(Xplore16.sq,SavePtr#)                         !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW16.SortOrderNbr,Force)       !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW16.FileSeqOn = True                        !Xplore
    RETURN SELF.SetSort(BRW16.FileOrderNbr,Force)     !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW16.SetQueueRecord PROCEDURE

  CODE
  IF (logser:Status = 'AVL')
    tmp:status = 'Available'
  ELSE
    tmp:status = 'Despatched'
  END
  PARENT.SetQueueRecord


BRW16.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,16,'TakeEvent'
  Xplore16.LastEvent = EVENT()                        !Xplore
  IF FOCUS() = ?List:2                                !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore16.AdjustAllColumns()                  !Xplore
      OF AltF12                                       !Xplore
         Xplore16.ResetToDefault()                    !Xplore
      OF AltR                                         !Xplore
         Xplore16.ToggleBar()                         !Xplore
      OF AltM                                         !Xplore
         Xplore16.InvokePopup()                       !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore16.RightButtonUp                         !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW16.FileSeqOn = False                        !Xplore
       Xplore16.LeftButtonUp(1)                       !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW16.SavePosition()                          !Xplore
       Xplore16.HandleMyEvents()                      !Xplore
       !BRW16.RestorePosition()                       !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List:2                           !Xplore
  PARENT.TakeEvent


BRW16.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore16.LeftButton2()                       !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW16.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW16.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete:3
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW16.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW16.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW16.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW16.ResetPairsQ PROCEDURE()
  CODE
Xplore16.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore16.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore16.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW16.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !logser:ESN
  OF 2 !logser:ClubNokia
  OF 3 !logser:Status
  OF 4 !logser:RecordNumber
  OF 5 !logser:RefNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore16.SetNewOrderFields PROCEDURE()
  CODE
  BRW16.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore16.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore16Step5,logser:RefNumberKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,logser:RefNumberKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore16Step1.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore16Locator1)
      Xplore16Locator1.Init(?logser:ESN,logser:ESN,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore16Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore16Locator2)
      Xplore16Locator2.Init(?logser:ESN,logser:ClubNokia,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore16Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore16Locator3)
      Xplore16Locator3.Init(?logser:ESN,logser:Status,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore16Step4.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore16Locator4)
      Xplore16Locator4.Init(?logser:ESN,logser:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore16Step5.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore16Locator5)
      Xplore16Locator5.Init(?logser:ESN,logser:RefNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(logser:RefNumber,lstl:RecordNumber)
  RETURN
!================================================================================
Xplore16.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore16.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW16.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(LOGSERST)
  END
  RETURN TotalRecords
!================================================================================
Xplore16.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('logser:ESN')                 !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('E.S.N. / I.M.E.I.')          !Header
                PSTRING('@s16')                       !Picture
                PSTRING('E.S.N. / I.M.E.I.')          !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('logser:ClubNokia')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Club Nokia Site')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Club Nokia Site')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('logser:Status')              !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Status')                     !Header
                PSTRING('@s10')                       !Picture
                PSTRING('Status')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('logser:RecordNumber')        !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Entry Long')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Entry Long')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('logser:RefNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Entry Long')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Entry Long')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(5)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)
BRW14.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW14.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW14.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW14.ResetPairsQ PROCEDURE()
  CODE
Xplore14.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore14.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore14.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW14.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !logast:PartNumber
  OF 2 !logast:Description
  OF 3 !logast:RecordNumber
  OF 4 !logast:RefNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore14.SetNewOrderFields PROCEDURE()
  CODE
  BRW14.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore14.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore14Step4,logast:RefNumberKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,logast:RefNumberKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore14Step1.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore14Locator1)
      Xplore14Locator1.Init(,logast:PartNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore14Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore14Locator2)
      Xplore14Locator2.Init(,logast:Description,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore14Step3.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore14Locator3)
      Xplore14Locator3.Init(,logast:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore14Step4.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore14Locator4)
      Xplore14Locator4.Init(,logast:RefNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(logast:RefNumber,logsto:RefNumber)
  RETURN
!================================================================================
Xplore14.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore14.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW14.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(LOGASSST)
  END
  RETURN TotalRecords
!================================================================================
Xplore14.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('logast:PartNumber')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Part Number')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Part Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logast:Description')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Description')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Description')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logast:RecordNumber')        !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Entry Long')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Entry Long')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logast:RefNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Entry Long')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Entry Long')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(4)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue

