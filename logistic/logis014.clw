

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS014.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Stock PROCEDURE                                !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisThreadActive BYTE
CurrentTab           STRING(80)
tmp:LastOrdered      DATE
save_orp_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
tmp:partnumber       STRING(30)
tmp:description      STRING(30)
tmp:location         STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
Location_Temp        STRING(30)
Manufacturer_Temp    STRING(30)
CPCSExecutePopUp     BYTE
accessory_temp       STRING('ALL')
shelf_location_temp  STRING(60)
no_temp              STRING('NO')
yes_temp             STRING('YES')
BrFilter1            STRING(300)
BrLocator1           STRING(30)
BrFilter2            STRING(300)
BrLocator2           STRING(30)
tmp:QtyOnOrder       LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Location_Temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Location)
                       PROJECT(sto:Accessory)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !List box control field - type derived from field
no_temp                LIKE(no_temp)                  !Browse hot field - type derived from local data
yes_temp               LIKE(yes_temp)                 !Browse hot field - type derived from local data
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
sto:Accessory          LIKE(sto:Accessory)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Browse
mo:SelectedField  Long
QuickWindow          WINDOW('Browse the Stock File'),AT(,,470,262),FONT('Arial',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Stock'),SYSTEM,GRAY,RESIZE,MDI
                       PROMPT('Site Location'),AT(8,12),USE(?Prompt1),FONT(,,COLOR:White,)
                       COMBO(@s30),AT(84,12,124,10),USE(Location_Temp),VSCROLL,FONT('Arial',8,,FONT:bold),UPR,FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       OPTION('Part Type'),AT(220,4,156,20),USE(accessory_temp),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                         RADIO('Non-Accessory'),AT(228,11),USE(?accessory_temp:Radio1),TRN,VALUE('NO')
                         RADIO('Accessory'),AT(296,11),USE(?accessory_temp:Radio2),VALUE('YES')
                         RADIO('All'),AT(348,11),USE(?accessory_temp:Radio3),VALUE('ALL')
                       END
                       LIST,AT(8,64,368,192),USE(?Browse:1),IMM,MSG('Browsing Records'),ALRT(HomeKey),FORMAT('125L(2)|M~Part Number~@s30@124L(2)|M~Description~@s30@48L(2)|M~Reference Number~' &|
   '@n012@/'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(392,32,76,20),USE(?Select),LEFT,ICON('select.ico')
                       SHEET,AT(4,32,380,228),USE(?CurrentTab),SPREAD
                         TAB('By &Part Number'),USE(?Tab:3)
                           ENTRY(@s30),AT(8,48,124,10),USE(sto:Part_Number),FONT('Arial',8,,FONT:bold),UPR
                         END
                         TAB('By Desc&ription'),USE(?Tab:4)
                           ENTRY(@s30),AT(8,48,124,10),USE(sto:Description),FONT('Arial',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('Close'),AT(392,240,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                       PANEL,AT(4,4,376,24),USE(?Panel1),FILL(COLOR:Gray)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
BRW1::Sort6:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
BRW1::Sort7:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
BRW1::Sort8:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
BRW1::Sort9:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort5:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
BRW1::Sort6:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
BRW1::Sort7:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
BRW1::Sort8:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
BRW1::Sort9:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_stm_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
Refresh_Browse      Routine
!    thiswindow.reset(1)
!    Case Choice(?CurrentTab)
!        Of 5
!            If accessory_temp = 'ALL'
!                sto:location    = Upper(location_temp)
!                sto:accessory   = Upper(accessory_temp)
!                brw1.addrange(sto:manufacturer,manufacturer_temp)
!                brw1.applyrange 
!            Else!If accessory_temp = 'ALL'
!                sto:location    = Upper(location_temp)
!                sto:accessory   = Upper(accessory_temp)
!                brw1.addrange(sto:manufacturer,manufacturer_temp)
!                brw1.applyrange 
!            End!If accessory_temp <> 'ALL'
!        Else
!            If accessory_temp = 'ALL'
!                brw1.addrange(sto:location,location_temp)
!                brw1.applyrange 
!            Else!If accessory_temp = 'ALL'
!                sto:location    = Upper(location_temp)
!                brw1.addrange(sto:accessory,accessory_temp)
!                brw1.applyrange 
!            End!If accessory_temp <> 'ALL'
!    End!Case Choice(?CurrentTab)
!    thiswindow.reset(1)
!    Brw1.resetsort(1)



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Stock',1)
    SolaceViewVars('tmp:LastOrdered',tmp:LastOrdered,'Browse_Stock',1)
    SolaceViewVars('save_orp_id',save_orp_id,'Browse_Stock',1)
    SolaceViewVars('save_loc_id',save_loc_id,'Browse_Stock',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Browse_Stock',1)
    SolaceViewVars('tmp:partnumber',tmp:partnumber,'Browse_Stock',1)
    SolaceViewVars('tmp:description',tmp:description,'Browse_Stock',1)
    SolaceViewVars('tmp:location',tmp:location,'Browse_Stock',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Stock',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Stock',1)
    SolaceViewVars('Location_Temp',Location_Temp,'Browse_Stock',1)
    SolaceViewVars('Manufacturer_Temp',Manufacturer_Temp,'Browse_Stock',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'Browse_Stock',1)
    SolaceViewVars('accessory_temp',accessory_temp,'Browse_Stock',1)
    SolaceViewVars('shelf_location_temp',shelf_location_temp,'Browse_Stock',1)
    SolaceViewVars('no_temp',no_temp,'Browse_Stock',1)
    SolaceViewVars('yes_temp',yes_temp,'Browse_Stock',1)
    SolaceViewVars('BrFilter1',BrFilter1,'Browse_Stock',1)
    SolaceViewVars('BrLocator1',BrLocator1,'Browse_Stock',1)
    SolaceViewVars('BrFilter2',BrFilter2,'Browse_Stock',1)
    SolaceViewVars('BrLocator2',BrLocator2,'Browse_Stock',1)
    SolaceViewVars('tmp:QtyOnOrder',tmp:QtyOnOrder,'Browse_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_Temp;  SolaceCtrlName = '?Location_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp;  SolaceCtrlName = '?accessory_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp:Radio1;  SolaceCtrlName = '?accessory_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp:Radio2;  SolaceCtrlName = '?accessory_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp:Radio3;  SolaceCtrlName = '?accessory_temp:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select;  SolaceCtrlName = '?Select';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Part_Number;  SolaceCtrlName = '?sto:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Description;  SolaceCtrlName = '?sto:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Stock'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Relate:LOGDEFLT.Open
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ?Browse:1{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sto:Location_Key)
  BRW1.AddRange(sto:Location,Location_Temp)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Part_Number_Accessory_Key)
  BRW1.AddRange(sto:Accessory,accessory_temp)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Part_Number_Accessory_Key)
  BRW1.AddRange(sto:Accessory,accessory_temp)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Key)
  BRW1.AddRange(sto:Location,Location_Temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Accessory_Key)
  BRW1.AddRange(sto:Accessory,accessory_temp)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Accessory_Key)
  BRW1.AddRange(sto:Accessory,accessory_temp)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Location_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,sto:Location,1,BRW1)
  BIND('no_temp',no_temp)
  BIND('yes_temp',yes_temp)
  BIND('Location_Temp',Location_Temp)
  BIND('Manufacturer_Temp',Manufacturer_Temp)
  BIND('accessory_temp',accessory_temp)
  BIND('tmp:QtyOnOrder',tmp:QtyOnOrder)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(no_temp,BRW1.Q.no_temp)
  BRW1.AddField(yes_temp,BRW1.Q.yes_temp)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Accessory,BRW1.Q.sto:Accessory)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Browse)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Browse,mo:SelectedTab,?CurrentTab)
  QuickWindow{prop:buffer} = 1
  FDCB6.Init(Location_Temp,?Location_Temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(loc:Location_Key)
  FDCB6.AddField(loc:Location,FDCB6.Q.loc:Location)
  FDCB6.AddField(loc:RecordNumber,FDCB6.Q.loc:RecordNumber)
  FDCB6.AddUpdateField(loc:Location,Location_Temp)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:3{PROP:TEXT} = 'By &Part Number'
    ?Tab:4{PROP:TEXT} = 'By Desc&ription'
    ?Browse:1{PROP:FORMAT} ='125L(2)|M~Part Number~@s30@#1#124L(2)|M~Description~@s30@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
    Relate:LOGDEFLT.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Stock'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Location_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, Accepted)
      BRW1.ApplyRange()
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, Accepted)
    OF ?accessory_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessory_temp, Accepted)
      BRW1.ApplyRange()
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessory_temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Browse,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      If keycode() = HomeKey
          Presskey(CtrlHome)
      End!If keycode() = HomeKey
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      Do refresh_browse
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Part Number~@s30@#1#124L(2)|M~Description~@s30@#2#'
          ?Tab:3{PROP:TEXT} = 'By &Part Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Description~@s30@#2#125L(2)|M~Part Number~@s30@#1#'
          ?Tab:4{PROP:TEXT} = 'By Desc&ription'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Browse,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sto:Part_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Part_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Part_Number, Selected)
    OF ?sto:Description
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Description, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Description, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ! Site Location Defaults
      Set(logdeflt)
      access:logdeflt.next()
      If ldef:location <> ''
          location_temp   = ldef:location
      Else!If ldef:location <> ''
          Set(defstock)
          access:defstock.next()
          If dst:use_site_location = 'YES'
              location_temp = dst:site_location
          Else
              location_temp = ''
          End
      End!If ldef:location <> ''
      brw1.resetsort(1)
      Display()
      Select(?browse:1)
      
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      FDCB6.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ApplyRange, (),BYTE)
    IF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
       GET(SELF.Order.RangeList.List,1)
       Self.Order.RangeList.List.Right = Location_Temp
    ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) <> 'ALL'
       GET(SELF.Order.RangeList.List,1)
       Self.Order.RangeList.List.Right = Location_Temp
       GET(SELF.Order.RangeList.List,2)
       Self.Order.RangeList.List.Right = accessory_temp
    ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
       GET(SELF.Order.RangeList.List,1)
       Self.Order.RangeList.List.Right = Location_Temp
    ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) <> 'ALL'
       GET(SELF.Order.RangeList.List,1)
       Self.Order.RangeList.List.Right = Location_Temp
       GET(SELF.Order.RangeList.List,2)
       Self.Order.RangeList.List.Right = accessory_temp
    ELSE
    END
  ReturnValue = PARENT.ApplyRange()
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ApplyRange, (),BYTE)
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(6,Force)
  ELSE
    RETURN SELF.SetSort(7,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  shelf_location_temp = CLIP(sto:Shelf_Location) & ' / ' & CLIP(sto:Second_Location)
  PARENT.SetQueueRecord


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue


FDCB6.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

