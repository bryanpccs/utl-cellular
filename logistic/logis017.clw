

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS017.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('LOGIS032.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS033.INC'),ONCE        !Req'd for module callout resolution
                     END





BroweStockHistory PROCEDURE                           !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
tmp:status           STRING('ALL {7}')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOGSTHIS)
                       PROJECT(logsth:Date)
                       PROJECT(logsth:UserCode)
                       PROJECT(logsth:StockIn)
                       PROJECT(logsth:StockOut)
                       PROJECT(logsth:Status)
                       PROJECT(logsth:Location)
                       PROJECT(logsth:ClubNokia)
                       PROJECT(logsth:SMPFNumber)
                       PROJECT(logsth:DespatchNoteNo)
                       PROJECT(logsth:RecordNumber)
                       PROJECT(logsth:ModelNumber)
                       PROJECT(logsth:Deletion_Reason)
                       PROJECT(logsth:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
logsth:Date            LIKE(logsth:Date)              !List box control field - type derived from field
logsth:UserCode        LIKE(logsth:UserCode)          !List box control field - type derived from field
logsth:StockIn         LIKE(logsth:StockIn)           !List box control field - type derived from field
logsth:StockOut        LIKE(logsth:StockOut)          !List box control field - type derived from field
logsth:Status          LIKE(logsth:Status)            !List box control field - type derived from field
logsth:Location        LIKE(logsth:Location)          !List box control field - type derived from field
logsth:ClubNokia       LIKE(logsth:ClubNokia)         !List box control field - type derived from field
logsth:SMPFNumber      LIKE(logsth:SMPFNumber)        !List box control field - type derived from field
logsth:DespatchNoteNo  LIKE(logsth:DespatchNoteNo)    !List box control field - type derived from field
logsth:RecordNumber    LIKE(logsth:RecordNumber)      !List box control field - type derived from field
logsth:ModelNumber     LIKE(logsth:ModelNumber)       !List box control field - type derived from field
logsth:Deletion_Reason LIKE(logsth:Deletion_Reason)   !Browse hot field - type derived from field
logsth:RefNumber       LIKE(logsth:RefNumber)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(LOGSTHII)
                       PROJECT(logsti:IMEI)
                       PROJECT(logsti:RecordNumber)
                       PROJECT(logsti:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
logsti:IMEI            LIKE(logsti:IMEI)              !List box control field - type derived from field
logsti:RecordNumber    LIKE(logsti:RecordNumber)      !Primary key field - type derived from field
logsti:RefNumber       LIKE(logsti:RefNumber)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5504                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,538770663)          !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Browse
mo:SelectedField  Long
QuickWindow          WINDOW('Browse Stock History'),AT(,,679,268),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('BroweStockHistory'),SYSTEM,GRAY,DOUBLE,MDI,IMM
                       LIST,AT(8,28,468,216),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('41R(2)|M~Date~@d6@22R(2)|M~User~@s3@38L(2)|M~Stock In~@s8@39L(2)|M~Stock Out~@s8' &|
   '@43L(2)|M~Status~@s10@101L(2)|M~Stock Location~@s30@97L(2)|M~Club Nokia Site~@s3' &|
   '0@69L(2)|M~SMPF Number~@s30@85L(2)|M~Delivery Note Number~@s30@32L(2)|M~Entry Lo' &|
   'ng~@s8@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('Amend IMEI'),AT(600,28,76,20),USE(?Change),LEFT,ICON('phone.ico')
                       BUTTON('Amend History'),AT(600,52,76,20),USE(?Change:2),LEFT,ICON('GEARWORK.ICO')
                       SHEET,AT(4,4,592,260),USE(?CurrentTab),WIZARD,SPREAD
                         TAB('By Date'),USE(?Tab:2)
                           PROMPT('By Date'),AT(8,8),USE(?Prompt2)
                           PROMPT('Serialized Stock'),AT(484,12),USE(?Prompt1)
                           ENTRY(@s30),AT(484,28,64,10),USE(logsti:IMEI),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           LIST,AT(484,44,104,212),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~I.M.E.I.~@s30@'),FROM(Queue:Browse)
                           PROMPT('Deletion Reason:'),AT(8,248),USE(?logsth:Deletion_Reason:Prompt)
                           ENTRY(@s40),AT(64,248,412,12),USE(logsth:Deletion_Reason),FONT(,,,FONT:bold)
                         END
                       END
                       BUTTON('Close'),AT(600,244,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  IncrementalLocatorClass          !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepCustomClass !DATE            !Xplore: Column displaying logsth:Date
Xplore1Locator1      StepLocatorClass                 !Xplore: Column displaying logsth:Date
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying logsth:UserCode
Xplore1Locator2      StepLocatorClass                 !Xplore: Column displaying logsth:UserCode
Xplore1Step3         StepLongClass   !LONG            !Xplore: Column displaying logsth:StockIn
Xplore1Locator3      StepLocatorClass                 !Xplore: Column displaying logsth:StockIn
Xplore1Step4         StepLongClass   !LONG            !Xplore: Column displaying logsth:StockOut
Xplore1Locator4      StepLocatorClass                 !Xplore: Column displaying logsth:StockOut
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying logsth:Status
Xplore1Locator5      StepLocatorClass                 !Xplore: Column displaying logsth:Status
Xplore1Step6         StepStringClass !STRING          !Xplore: Column displaying logsth:Location
Xplore1Locator6      StepLocatorClass                 !Xplore: Column displaying logsth:Location
Xplore1Step7         StepStringClass !STRING          !Xplore: Column displaying logsth:ClubNokia
Xplore1Locator7      StepLocatorClass                 !Xplore: Column displaying logsth:ClubNokia
Xplore1Step8         StepStringClass !STRING          !Xplore: Column displaying logsth:SMPFNumber
Xplore1Locator8      StepLocatorClass                 !Xplore: Column displaying logsth:SMPFNumber
Xplore1Step9         StepStringClass !STRING          !Xplore: Column displaying logsth:DespatchNoteNo
Xplore1Locator9      StepLocatorClass                 !Xplore: Column displaying logsth:DespatchNoteNo
Xplore1Step10        StepLongClass   !LONG            !Xplore: Column displaying logsth:RecordNumber
Xplore1Locator10     StepLocatorClass                 !Xplore: Column displaying logsth:RecordNumber
Xplore1Step11        StepStringClass !STRING          !Xplore: Column displaying logsth:ModelNumber
Xplore1Locator11     StepLocatorClass                 !Xplore: Column displaying logsth:ModelNumber
Xplore1Step12        StepStringClass !STRING          !Xplore: Column displaying logsth:Deletion_Reason
Xplore1Locator12     StepLocatorClass                 !Xplore: Column displaying logsth:Deletion_Reason
Xplore1Step13        StepLongClass   !LONG            !Xplore: Column displaying logsth:RefNumber
Xplore1Locator13     StepLocatorClass                 !Xplore: Column displaying logsth:RefNumber

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BroweStockHistory',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'BroweStockHistory',1)
    SolaceViewVars('tmp:status',tmp:status,'BroweStockHistory',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsti:IMEI;  SolaceCtrlName = '?logsti:IMEI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:Deletion_Reason:Prompt;  SolaceCtrlName = '?logsth:Deletion_Reason:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:Deletion_Reason;  SolaceCtrlName = '?logsth:Deletion_Reason';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('BroweStockHistory','?Browse:1')  !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BroweStockHistory')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BroweStockHistory')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='BroweStockHistory'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOGSTHII.Open
  Access:LOGSTOCK.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOGSTHIS,SELF)
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:LOGSTHII,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ?Browse:1{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'logistic.INI','>Header',1,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,logsth:RefNumberKey)
  BRW1.AddRange(logsth:RefNumber,GLO:Select1)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,logsth:RefNumber,1,BRW1)
  BIND('tmp:status',tmp:status)
  BRW1.AddField(logsth:Date,BRW1.Q.logsth:Date)
  BRW1.AddField(logsth:UserCode,BRW1.Q.logsth:UserCode)
  BRW1.AddField(logsth:StockIn,BRW1.Q.logsth:StockIn)
  BRW1.AddField(logsth:StockOut,BRW1.Q.logsth:StockOut)
  BRW1.AddField(logsth:Status,BRW1.Q.logsth:Status)
  BRW1.AddField(logsth:Location,BRW1.Q.logsth:Location)
  BRW1.AddField(logsth:ClubNokia,BRW1.Q.logsth:ClubNokia)
  BRW1.AddField(logsth:SMPFNumber,BRW1.Q.logsth:SMPFNumber)
  BRW1.AddField(logsth:DespatchNoteNo,BRW1.Q.logsth:DespatchNoteNo)
  BRW1.AddField(logsth:RecordNumber,BRW1.Q.logsth:RecordNumber)
  BRW1.AddField(logsth:ModelNumber,BRW1.Q.logsth:ModelNumber)
  BRW1.AddField(logsth:Deletion_Reason,BRW1.Q.logsth:Deletion_Reason)
  BRW1.AddField(logsth:RefNumber,BRW1.Q.logsth:RefNumber)
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,logsti:RefNumberKey)
  BRW2.AddRange(logsti:RefNumber,Relate:LOGSTHII,Relate:LOGSTHIS)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(?LOGSTI:IMEI,logsti:IMEI,1,BRW2)
  BRW2.AddField(logsti:IMEI,BRW2.Q.logsti:IMEI)
  BRW2.AddField(logsti:RecordNumber,BRW2.Q.logsti:RecordNumber)
  BRW2.AddField(logsti:RefNumber,BRW2.Q.logsti:RefNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Browse)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Browse,mo:SelectedTab,?CurrentTab)
  QuickWindow{prop:buffer} = 1
  BRW1.AskProcedure = 1
  BRW2.AskProcedure = 2
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGSTHII.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('BroweStockHistory','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisThreadActive
    ThreadQ.Proc='BroweStockHistory'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BroweStockHistory',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_StockHIS
      Update_IMEIHIS
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(logsth:Date,BRW1.Q.logsth:Date)
  Xplore1.AddField(logsth:UserCode,BRW1.Q.logsth:UserCode)
  Xplore1.AddField(logsth:StockIn,BRW1.Q.logsth:StockIn)
  Xplore1.AddField(logsth:StockOut,BRW1.Q.logsth:StockOut)
  Xplore1.AddField(logsth:Status,BRW1.Q.logsth:Status)
  Xplore1.AddField(logsth:Location,BRW1.Q.logsth:Location)
  Xplore1.AddField(logsth:ClubNokia,BRW1.Q.logsth:ClubNokia)
  Xplore1.AddField(logsth:SMPFNumber,BRW1.Q.logsth:SMPFNumber)
  Xplore1.AddField(logsth:DespatchNoteNo,BRW1.Q.logsth:DespatchNoteNo)
  Xplore1.AddField(logsth:RecordNumber,BRW1.Q.logsth:RecordNumber)
  Xplore1.AddField(logsth:ModelNumber,BRW1.Q.logsth:ModelNumber)
  Xplore1.AddField(logsth:Deletion_Reason,BRW1.Q.logsth:Deletion_Reason)
  Xplore1.AddField(logsth:RefNumber,BRW1.Q.logsth:RefNumber)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,logsth:RefNumberKey) !Xplore Sort Order for File Sequence
  BRW1.AddRange(logsth:RefNumber,GLO:Select1)         !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BroweStockHistory')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
    ThisMakeover.TakeEvent(Win:Browse,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Browse,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?logsti:IMEI
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logsti:IMEI, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logsti:IMEI, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
    OF EVENT:GainFocus
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change:2
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(1)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW2.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !logsth:Date
  OF 2 !logsth:UserCode
  OF 3 !logsth:StockIn
  OF 4 !logsth:StockOut
  OF 5 !logsth:Status
  OF 6 !logsth:Location
  OF 7 !logsth:ClubNokia
  OF 8 !logsth:SMPFNumber
  OF 9 !logsth:DespatchNoteNo
  OF 10 !logsth:RecordNumber
  OF 11 !logsth:ModelNumber
  OF 12 !logsth:Deletion_Reason
  OF 13 !logsth:RefNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step13,logsth:RefNumberKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,logsth:RefNumberKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(,logsth:Date,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,logsth:UserCode,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,logsth:StockIn,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,logsth:StockOut,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(,logsth:Status,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,logsth:Location,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(,logsth:ClubNokia,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(,logsth:SMPFNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(,logsth:DespatchNoteNo,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(,logsth:RecordNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step11.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator11)
      Xplore1Locator11.Init(,logsth:ModelNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step12.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator12)
      Xplore1Locator12.Init(?logsth:Deletion_Reason,logsth:Deletion_Reason,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step13.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator13)
      Xplore1Locator13.Init(,logsth:RefNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(logsth:RefNumber,GLO:Select1)
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(LOGSTHIS)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('logsth:Date')                !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Date')                       !Header
                PSTRING('@d6')                        !Picture
                PSTRING('Date')                       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:UserCode')            !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('User')                       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('User')                       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:StockIn')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Stock In')                   !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Stock In')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:StockOut')            !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Stock Out')                  !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Stock Out')                  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:Status')              !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Status')                     !Header
                PSTRING('@s10')                       !Picture
                PSTRING('Status')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:Location')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Stock Location')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Stock Location')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:ClubNokia')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Club Nokia Site')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Club Nokia Site')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:SMPFNumber')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('SMPF Number')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('SMPF Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:DespatchNoteNo')      !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Delivery Note Number')       !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Delivery Note Number')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:RecordNumber')        !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Entry Long')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Entry Long')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:ModelNumber')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:Deletion_Reason')     !Field Name
                SHORT(160)                            !Default Column Width
                PSTRING('Deletion Reason')            !Header
                PSTRING('@s40')                       !Picture
                PSTRING('Deletion Reason')            !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('logsth:RefNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Entry Long')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Entry Long')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(13)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue

