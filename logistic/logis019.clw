

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS019.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('LOGIS024.INC'),ONCE        !Req'd for module callout resolution
                     END


StockReturns PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
tmp:imei             STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?lrtn:ClubNokiaSite
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?lrtn:ModelNumber
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(LOGRTHIS)
                       PROJECT(lsrh:IMEI)
                       PROJECT(lsrh:RefNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
lsrh:IMEI              LIKE(lsrh:IMEI)                !List box control field - type derived from field
lsrh:RefNumber         LIKE(lsrh:RefNumber)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB9::View:FileDropCombo VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
FDCB10::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
History::lrtn:Record LIKE(lrtn:RECORD),STATIC
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
QuickWindow          WINDOW('Stock Returns'),AT(,,379,215),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('StockReturns'),SYSTEM,GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,212,180),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           COMBO(@s30),AT(84,20,124,10),USE(lrtn:ClubNokiaSite),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Model Number'),AT(8,36),USE(?Prompt3:2)
                           COMBO(@s30),AT(84,36,124,10),USE(lrtn:ModelNumber),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           OPTION('Return Type'),AT(84,68,124,60),USE(lrtn:ReturnType),BOXED,MSG('QTY/SCN')
                             RADIO('Scan I.M.E.I. Numbers'),AT(92,80),USE(?LRTN:ReturnType:Radio1),VALUE('SCN')
                             RADIO('Enter Quantity'),AT(92,92),USE(?LRTN:ReturnType:Radio2),VALUE('QTY')
                           END
                           PROMPT('Club Nokia Site'),AT(8,20),USE(?Prompt3)
                           PROMPT('Quantity'),AT(92,108),USE(?LRTN:Quantity:Prompt),TRN,HIDE
                           ENTRY(@s8),AT(156,108,40,10),USE(lrtn:Quantity),HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('I.M.E.I. Number'),AT(8,148),USE(?tmp:imei:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,148,124,10),USE(tmp:imei),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Returns Number'),AT(8,52),USE(?LRTN:ReturnsNumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(lrtn:ReturnsNumber),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                         END
                       END
                       SHEET,AT(220,4,156,180),USE(?Sheet2),SPREAD
                         TAB('Scanned I.M.E.I. Numbers'),USE(?Tab3)
                           LIST,AT(224,20,148,144),USE(?Browse:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('80L(2)|M~I.M.E.I. Number~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Insert'),AT(224,168,45,14),USE(?Insert:3)
                           BUTTON('&Change'),AT(272,168,45,14),USE(?Change:3)
                           BUTTON('&Delete'),AT(324,168,45,14),USE(?Delete:3)
                         END
                       END
                       PANEL,AT(4,188,372,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(260,192,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(316,192,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

BRW2                 CLASS(BrowseClass)               !Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW2::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

FDCB9                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'StockReturns',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'StockReturns',1)
    SolaceViewVars('ActionMessage',ActionMessage,'StockReturns',1)
    SolaceViewVars('tmp:imei',tmp:imei,'StockReturns',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lrtn:ClubNokiaSite;  SolaceCtrlName = '?lrtn:ClubNokiaSite';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:2;  SolaceCtrlName = '?Prompt3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lrtn:ModelNumber;  SolaceCtrlName = '?lrtn:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lrtn:ReturnType;  SolaceCtrlName = '?lrtn:ReturnType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LRTN:ReturnType:Radio1;  SolaceCtrlName = '?LRTN:ReturnType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LRTN:ReturnType:Radio2;  SolaceCtrlName = '?LRTN:ReturnType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LRTN:Quantity:Prompt;  SolaceCtrlName = '?LRTN:Quantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lrtn:Quantity;  SolaceCtrlName = '?lrtn:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:imei:Prompt;  SolaceCtrlName = '?tmp:imei:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:imei;  SolaceCtrlName = '?tmp:imei';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LRTN:ReturnsNumber:Prompt;  SolaceCtrlName = '?LRTN:ReturnsNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lrtn:ReturnsNumber;  SolaceCtrlName = '?lrtn:ReturnsNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:2;  SolaceCtrlName = '?Browse:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('StockReturns')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'StockReturns')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?lrtn:ClubNokiaSite
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(lrtn:Record,History::lrtn:Record)
  SELF.AddHistoryField(?lrtn:ClubNokiaSite,2)
  SELF.AddHistoryField(?lrtn:ModelNumber,3)
  SELF.AddHistoryField(?lrtn:ReturnType,7)
  SELF.AddHistoryField(?lrtn:Quantity,4)
  SELF.AddHistoryField(?lrtn:ReturnsNumber,5)
  SELF.AddUpdateFile(Access:LOGRETRN)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOGCLSTE.Open
  Relate:LOGRETRN.Open
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOGRETRN
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:LOGRTHIS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Post(Event:Accepted,?lrtn:returntype)
  ?Browse:2{prop:vcr} = TRUE
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,lsrh:RefNumberKey)
  BRW2.AddRange(lsrh:RefNumber,Relate:LOGRTHIS,Relate:LOGRETRN)
  BRW2.AddField(lsrh:IMEI,BRW2.Q.lsrh:IMEI)
  BRW2.AddField(lsrh:RefNumber,BRW2.Q.lsrh:RefNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?CurrentTab)
  QuickWindow{prop:buffer} = 1
  BRW2.AskProcedure = 1
  FDCB9.Init(lrtn:ClubNokiaSite,?lrtn:ClubNokiaSite,Queue:FileDropCombo.ViewPosition,FDCB9::View:FileDropCombo,Queue:FileDropCombo,Relate:LOGCLSTE,ThisWindow,GlobalErrors,0,1,0)
  FDCB9.Q &= Queue:FileDropCombo
  FDCB9.AddSortOrder(logclu:ClubNokiaKey)
  FDCB9.AddField(logclu:ClubNokia,FDCB9.Q.logclu:ClubNokia)
  FDCB9.AddField(logclu:RecordNumber,FDCB9.Q.logclu:RecordNumber)
  ThisWindow.AddItem(FDCB9.WindowComponent)
  FDCB9.DefaultFill = 0
  FDCB10.Init(lrtn:ModelNumber,?lrtn:ModelNumber,Queue:FileDropCombo:1.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:1
  FDCB10.AddSortOrder(mod:Model_Number_Key)
  FDCB10.AddField(mod:Model_Number,FDCB10.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGCLSTE.Close
    Relate:LOGRETRN.Close
    Relate:MODELNUM.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'StockReturns',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateLOGRTHIS
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?lrtn:ReturnType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lrtn:ReturnType, Accepted)
      Case LRTN:ReturnType
          Of 'SCN'
              Unhide(?tmp:imei)
              Unhide(?tmp:imei:prompt)
              Hide(?LRTN:Quantity)
              Hide(?LRTN:Quantity:Prompt)
          Of 'QTY'
              hide(?tmp:imei)
              hide(?tmp:imei:prompt)
              UnHide(?LRTN:Quantity)
              UnHide(?LRTN:Quantity:Prompt)
      End!Case LRTN:ReturnType
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lrtn:ReturnType, Accepted)
    OF ?tmp:imei
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:imei, Accepted)
      If ~0{prop:acceptall}
          get(logrthis,0)
          if access:logrthis.primerecord() = LeveL:Benign
              lsrh:refnumber   = lrtn:RefNumber
              lsrh:imei        = tmp:Imei
              lsrh:modelnumber = lrtn:ModelNumber
              if access:logrthis.insert()
                 access:logrthis.cancelautoinc()
              end
          End!if access:logrthis.primerecord() = LeveL:Benign
          Select(?tmp:imei)
      End!If ~0{prop:acceptall}
      BRW2.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:imei, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'StockReturns')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    OF ?Sheet2
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW2.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue

