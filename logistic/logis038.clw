

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS038.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('LOGIS037.INC'),ONCE        !Req'd for module callout resolution
                     END


Global_Data_Export PROCEDURE                          !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Ex_ModelNumber
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Ex_SalesCode
logsto:SalesCode       LIKE(logsto:SalesCode)         !List box control field - type derived from field
logsto:RefNumber       LIKE(logsto:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?Ex_Location
logstl:Location        LIKE(logstl:Location)          !List box control field - type derived from field
logstl:RefNumber       LIKE(logstl:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?Ex_ClubNokia
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
FDCB6::View:FileDropCombo VIEW(LOGSTOCK)
                       PROJECT(logsto:SalesCode)
                       PROJECT(logsto:RefNumber)
                     END
FDCB4::View:FileDropCombo VIEW(LOGSTOLC)
                       PROJECT(logstl:Location)
                       PROJECT(logstl:RefNumber)
                     END
FDCB7::View:FileDropCombo VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Window
mo:SelectedField  Long
window               WINDOW('Global Data Export'),AT(,,204,216),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,196,180),USE(?Sheet1),SPREAD
                         TAB('Global Data Export'),USE(?Tab1)
                           PROMPT('E.S.N. / I.M.E.I.'),AT(8,20),USE(?Ex_ESN:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,20,124,10),USE(Ex_ESN),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           STRING('Model Number'),AT(8,36),USE(?String1),TRN
                           COMBO(@s30),AT(72,36,124,10),USE(Ex_ModelNumber),IMM,LEFT,FONT(,,,FONT:bold),FORMAT('120L(2)|M@s30@'),DROP(5),FROM(Queue:FileDropCombo)
                           STRING('Sales Code'),AT(8,52),USE(?String2),TRN
                           COMBO(@s30),AT(72,52,124,10),USE(Ex_SalesCode),IMM,FONT(,,,FONT:bold),FORMAT('120L(2)|M@s30@'),DROP(5),FROM(Queue:FileDropCombo:1)
                           PROMPT('SMPF Number'),AT(8,68),USE(?Ex_SMPFNumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,68,124,10),USE(Ex_SMPFNumber),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Despatch Note No.'),AT(8,84),USE(?Ex_DespatchNoteNo:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,84,124,10),USE(Ex_DespatchNoteNo),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           STRING('Stock Location'),AT(8,100),USE(?String3),TRN
                           COMBO(@s30),AT(72,100,124,10),USE(Ex_Location),IMM,FONT(,,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(5),FROM(Queue:FileDropCombo:2)
                           STRING('Club Nokai Site'),AT(8,116),USE(?String4),TRN
                           COMBO(@s30),AT(72,116,124,10),USE(Ex_ClubNokia),IMM,FONT(,,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(5),FROM(Queue:FileDropCombo:3)
                           BUTTON,AT(184,132,10,10),USE(?PopCalendar:2),FLAT,ICON('CALENDA2.ICO')
                           STRING('Date Range'),AT(8,132),USE(?String6),TRN
                           ENTRY(@d6),AT(72,132,,10),USE(Ex_Start_Date),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(120,132,10,10),USE(?PopCalendar),FLAT,ICON('CALENDA2.ICO')
                           ENTRY(@d6),AT(136,132,43,10),USE(Ex_End_Date),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           CHECK(' Available'),AT(71,148),USE(Ex_Status),TRN,VALUE('Y','N')
                           STRING('Status'),AT(8,148),USE(?String7),TRN
                           CHECK(' Transferred'),AT(71,160),USE(Ex_Transferred),TRN,VALUE('Y','N')
                           CHECK(' Despatched'),AT(71,172),USE(Ex_Despatched),TRN,VALUE('Y','N')
                         END
                       END
                       PANEL,AT(4,188,196,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(80,192,56,16),USE(?OkButton),FLAT,LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(140,192,56,16),USE(?CancelButton),FLAT,LEFT,ICON('cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Global_Data_Export',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_ESN:Prompt;  SolaceCtrlName = '?Ex_ESN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_ESN;  SolaceCtrlName = '?Ex_ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_ModelNumber;  SolaceCtrlName = '?Ex_ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_SalesCode;  SolaceCtrlName = '?Ex_SalesCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_SMPFNumber:Prompt;  SolaceCtrlName = '?Ex_SMPFNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_SMPFNumber;  SolaceCtrlName = '?Ex_SMPFNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_DespatchNoteNo:Prompt;  SolaceCtrlName = '?Ex_DespatchNoteNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_DespatchNoteNo;  SolaceCtrlName = '?Ex_DespatchNoteNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String3;  SolaceCtrlName = '?String3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_Location;  SolaceCtrlName = '?Ex_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String4;  SolaceCtrlName = '?String4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_ClubNokia;  SolaceCtrlName = '?Ex_ClubNokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String6;  SolaceCtrlName = '?String6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_Start_Date;  SolaceCtrlName = '?Ex_Start_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_End_Date;  SolaceCtrlName = '?Ex_End_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_Status;  SolaceCtrlName = '?Ex_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String7;  SolaceCtrlName = '?String7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_Transferred;  SolaceCtrlName = '?Ex_Transferred';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ex_Despatched;  SolaceCtrlName = '?Ex_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Global_Data_Export')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Global_Data_Export')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ex_ESN:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOGCLSTE.Open
  Relate:LOGSTOCK.Open
  Relate:LOGSTOLC.Open
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  Ex_ESN                = ''
  Ex_SalesCode          = ''
  Ex_ModelNumber        = ''
  Ex_SMPFNumber         = ''
  Ex_DespatchNoteNo     = ''
  Ex_Location           = ''
  Ex_ClubNokia          = ''
  Ex_Start_Date         = ''
  Ex_End_Date           = ''
  Ex_Date_Type          = ''
  Ex_Status             = ''
  Ex_Transferred        = ''
  Ex_Despatched         = ''
  OPEN(window)
  SELF.Opened=True
  ?Ex_Start_Date{Prop:Alrt,255} = MouseLeft2
  ?Ex_End_Date{Prop:Alrt,255} = MouseLeft2
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,?Sheet1)
  window{prop:buffer} = 1
  FDCB5.Init(Ex_ModelNumber,?Ex_ModelNumber,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(mod:Model_Number_Key)
  FDCB5.AddField(mod:Model_Number,FDCB5.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB6.Init(Ex_SalesCode,?Ex_SalesCode,Queue:FileDropCombo:1.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo:1,Relate:LOGSTOCK,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo:1
  FDCB6.AddSortOrder(logsto:SalesKey)
  FDCB6.AddField(logsto:SalesCode,FDCB6.Q.logsto:SalesCode)
  FDCB6.AddField(logsto:RefNumber,FDCB6.Q.logsto:RefNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB4.Init(Ex_Location,?Ex_Location,Queue:FileDropCombo:2.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo:2,Relate:LOGSTOLC,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo:2
  FDCB4.AddSortOrder(logstl:LocationKey)
  FDCB4.AddField(logstl:Location,FDCB4.Q.logstl:Location)
  FDCB4.AddField(logstl:RefNumber,FDCB4.Q.logstl:RefNumber)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB7.Init(Ex_ClubNokia,?Ex_ClubNokia,Queue:FileDropCombo:3.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:3,Relate:LOGCLSTE,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:3
  FDCB7.AddSortOrder(logclu:ClubNokiaKey)
  FDCB7.AddField(logclu:ClubNokia,FDCB7.Q.logclu:ClubNokia)
  FDCB7.AddField(logclu:RecordNumber,FDCB7.Q.logclu:RecordNumber)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGCLSTE.Close
    Relate:LOGSTOCK.Close
    Relate:LOGSTOLC.Close
    Relate:MODELNUM.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Global_Data_Export',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      IF Ex_Start_Date = ''
        Ex_Start_Date = TODAY() - (365*4)
      END
      IF Ex_End_Date = ''
        Ex_End_Date = TODAY()
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Ex_End_Date = TINCALENDARStyle1(Ex_End_Date)
          Display(?Ex_End_Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Ex_Start_Date = TINCALENDARStyle1(Ex_Start_Date)
          Display(?Ex_Start_Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      START(Process_Export, 25000)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Global_Data_Export')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Window,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Ex_Start_Date
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?Ex_End_Date
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

