

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('LOGIS008.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateLOGSERST PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
SMPFNumber           STRING(30)
DespatchNoteNo       STRING(30)
History::logser:Record LIKE(logser:RECORD),STATIC
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
QuickWindow          WINDOW('Insert Serialized Stock'),AT(,,232,152),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('UpdateLOGSERST'),SYSTEM,GRAY,DOUBLE,MDI,IMM
                       SHEET,AT(4,4,224,116),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('E.S.N. / I.M.E.I.'),AT(8,20),USE(?logser:esn:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(logser:ESN),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           OPTION('Status'),AT(84,36,128,28),USE(logser:Status),BOXED
                             RADIO('Available'),AT(92,48),USE(?Option1:Radio1),VALUE('AVAILABLE')
                             RADIO('Despatched'),AT(152,48),USE(?Option1:Radio2),VALUE('DESPATCHED')
                           END
                           PROMPT('Club Nokia Site'),AT(8,72),USE(?logser:ClubNokia:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,72,124,10),USE(logser:ClubNokia),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('Club Nokia Site'),TIP('Club Nokia Site'),ALRT(MouseLeft2),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(212,72,10,10),USE(?LookupClubNokia),SKIP,ICON('list3.ico')
                           PROMPT('SMPF Number'),AT(8,88),USE(?SMPFNumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,88,124,10),USE(SMPFNumber),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Despatch Note Number'),AT(8,104),USE(?DespatchNoteNo:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,104,124,10),USE(DespatchNoteNo),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                         END
                       END
                       BUTTON('&OK'),AT(112,128,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(168,128,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,124,224,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

!Save Entry Fields Incase Of Lookup
look:logser:ClubNokia                Like(logser:ClubNokia)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateLOGSERST',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateLOGSERST',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateLOGSERST',1)
    SolaceViewVars('SMPFNumber',SMPFNumber,'UpdateLOGSERST',1)
    SolaceViewVars('DespatchNoteNo',DespatchNoteNo,'UpdateLOGSERST',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logser:esn:Prompt;  SolaceCtrlName = '?logser:esn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logser:ESN;  SolaceCtrlName = '?logser:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logser:Status;  SolaceCtrlName = '?logser:Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio1;  SolaceCtrlName = '?Option1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio2;  SolaceCtrlName = '?Option1:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logser:ClubNokia:Prompt;  SolaceCtrlName = '?logser:ClubNokia:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logser:ClubNokia;  SolaceCtrlName = '?logser:ClubNokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupClubNokia;  SolaceCtrlName = '?LookupClubNokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SMPFNumber:Prompt;  SolaceCtrlName = '?SMPFNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SMPFNumber;  SolaceCtrlName = '?SMPFNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DespatchNoteNo:Prompt;  SolaceCtrlName = '?DespatchNoteNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DespatchNoteNo;  SolaceCtrlName = '?DespatchNoteNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Serial Number'
  OF ChangeRecord
    ActionMessage = 'Changing A Serial Number'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateLOGSERST')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateLOGSERST')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?logser:esn:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(logser:Record,History::logser:Record)
  SELF.AddHistoryField(?logser:ESN,3)
  SELF.AddHistoryField(?logser:Status,4)
  SELF.AddHistoryField(?logser:ClubNokia,5)
  SELF.AddUpdateFile(Access:LOGSERST)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOGCLSTE.Open
  Relate:LOGSERST.Open
  Access:LOGSTHIS.UseFile
  Access:LOGSTOCK.UseFile
  Access:LOGSTHII.UseFile
  Access:LOGSTLOC.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOGSERST
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.DeleteAction = Delete:Auto
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
      !Check we have a History for this IMEI?!
      foundx# = 0
      IF logser:ESN <> ''
        Access:LogSthII.ClearKey(logsti:IMEIRefNoKey)
        logsti:IMEI = logser:ESN
        SET(logsti:IMEIRefNoKey,logsti:IMEIRefNoKey)
        LOOP
          IF Access:LogSthII.Next()
            BREAK
          END
          IF logsti:IMEI <> logser:ESN
            BREAK
          END
          foundx# = 1
          BREAK
        END
        IF Foundx# = 1
          Access:LogsThis.ClearKey(logsth:RecordNumberKey)
          logsth:RecordNumber = logsti:RefNumber
          IF Access:LogsThis.Fetch(logsth:RecordNumberKey)
            !Error!
            SMPFNumber = 'UNKNOWN'
            DespatchNoteNo = 'UNKNOWN'
          ELSE
            SMPFNumber = logsth:SMPFNumber
            DespatchNoteNo = logsth:DespatchNoteNo
          END
        END
      END
  OPEN(QuickWindow)
  SELF.Opened=True
  IF ?logser:ClubNokia{Prop:Tip} AND ~?LookupClubNokia{Prop:Tip}
     ?LookupClubNokia{Prop:Tip} = 'Select ' & ?logser:ClubNokia{Prop:Tip}
  END
  IF ?logser:ClubNokia{Prop:Msg} AND ~?LookupClubNokia{Prop:Msg}
     ?LookupClubNokia{Prop:Msg} = 'Select ' & ?logser:ClubNokia{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?CurrentTab)
  QuickWindow{prop:buffer} = 1
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGCLSTE.Close
    Relate:LOGSERST.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateLOGSERST',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseClubNokiaSite
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      IF ThisWindow.Request = InsertRecord
         Access:Logsthis.PrimeRecord()
      
         If access:logsthii.primerecord() = Level:Benign
           logsti:RefNumber    = logsth:RecordNumber
           logsti:IMEI         = logser:ESN
           If access:logsthii.tryinsert()
             access:logsthii.cancelautoinc()
           End!If access:logsthii.tryinsert()
         End!If access:logsthii.primerecord() = Level:Benign
      
         logsth:refnumber      = logsto:RefNumber
         logsth:stockin        = 1
         logsth:status         = logser:Status
         logsth:clubnokia      = logser:ClubNokia
         logsth:smpfnumber     = smpfnumber
         logsth:despatchnoteno = despatchnoteno
         logsth:modelnumber    = logsto:ModelNumber
      
         !Find the location!
         Access:Logstloc.ClearKey(lstl:RecordNumberKey)
         lstl:RecordNumber = logser:RefNumber
         IF Access:Logstloc.Fetch(lstl:RecordNumberKey)
           logsth:location       = 'UNKNOWN'
         ELSE
           logsth:location       = lstl:Location
         END
         logsth:date           = Today()
         access:users.clearkey(use:password_key)
         use:password    = glo:password
         access:users.tryfetch(use:password_key)
         logsth:usercode     = use:user_code
         if access:logsthis.update()
             access:logsthis.cancelautoinc()
         end
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?logser:ClubNokia
      IF logser:ClubNokia OR ?logser:ClubNokia{Prop:Req}
        logclu:ClubNokia = logser:ClubNokia
        !Save Lookup Field Incase Of error
        look:logser:ClubNokia        = logser:ClubNokia
        IF Access:LOGCLSTE.TryFetch(logclu:ClubNokiaKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            logser:ClubNokia = logclu:ClubNokia
          ELSE
            !Restore Lookup On Error
            logser:ClubNokia = look:logser:ClubNokia
            SELECT(?logser:ClubNokia)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupClubNokia
      ThisWindow.Update
      logclu:ClubNokia = logser:ClubNokia
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          logser:ClubNokia = logclu:ClubNokia
          Select(?+1)
      ELSE
          Select(?logser:ClubNokia)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?logser:ClubNokia)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateLOGSERST')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?logser:ClubNokia
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logser:ClubNokia, AlertKey)
      Post(Event:Accepted,?LookupClubNokia)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logser:ClubNokia, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue

