

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS029.INC'),ONCE        !Local module procedure declarations
                     END


PrintRoutines PROCEDURE                               !Generated from procedure template - Window

tmp:refnumber        LONG
tmp:ReportType       BYTE
tmp:ThePath          STRING(255)
tmp:ExportFile       STRING(255),STATIC
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
FDB3::View:FileDrop  VIEW(LOGDESNO)
                       PROJECT(ldes:DespatchNo)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?tmp:refnumber
ldes:DespatchNo        LIKE(ldes:DespatchNo)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Window
mo:SelectedField  Long
window               WINDOW('Print Routines'),AT(,,187,98),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,VSCROLL,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,180,64),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           OPTION('Select Report Type'),AT(8,8,172,56),USE(tmp:ReportType),BOXED
                             RADIO('Despatch Note'),AT(24,26),USE(?Option1:Radio1),VALUE('1')
                           END
                           LIST,AT(104,26,64,10),USE(tmp:refnumber),HIDE,VSCROLL,FONT(,,,FONT:bold),FORMAT('32L(2)|M@s8@'),DROP(10),FROM(Queue:FileDrop)
                           PROMPT('Despatch Note Number'),AT(104,18),USE(?tmp:refnumber:prompt),HIDE,FONT(,7,,)
                         END
                       END
                       PANEL,AT(4,72,180,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(124,76,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       BUTTON('Create CSV'),AT(8,76,56,16),USE(?CreateCSV),LEFT,ICON(ICON:Save)
                       BUTTON('&Print'),AT(68,76,56,16),USE(?Print),LEFT,ICON(ICON:Print1)
                     END

Prog        Class
ProgressSetup      Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(),Byte
ProgressText       Procedure(String func:String)
ProgressFinish     Procedure()
            End
! moving bar window
prog:RecordsToProcess   Long,Auto
prog:RecordsPerCycle    Long,Auto
prog:RecordsProcessed   Long,Auto
prog:RecordsThisCycle   Long,Auto
prog:PercentProgress    Byte
prog:Cancel             Byte
prog:ProgressThermometer    Byte


prog:thermometer byte
prog:SkipRecords        Long
omit('***',ClarionetUsed=0)
CNprog:thermometer byte
CNprog:Text         String(60)
CNprog:pctText      String(60)
***z
progwindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(prog:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?prog:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?prog:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('cancel.gif')
     END

omit('***',ClarionetUsed=0)

!static webjob window
CN:progwindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(cnprog:Text),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?cnprog:Prompt),FONT(,14,,FONT:bold)
     END
***
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

FDB3                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ExportFile    File,Driver('BASIC'),Pre(exp),Name(tmp:ExportFile),Create,Bindable,Thread
Record                  Record
SalesCode                String(30)
Description             String(30)
ModelNumber             String(30)
IMEINumber              String(30)
                        End
                    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

Prog:nextrecord      routine
    Yield()
    prog:recordsprocessed += 1
    prog:recordsthiscycle += 1
    If prog:percentprogress < 100
        prog:percentprogress = (prog:recordsprocessed / prog:recordstoprocess)*100
        If prog:percentprogress > 100
            prog:percentprogress = 100
        End
        If prog:percentprogress <> prog:thermometer then
            prog:thermometer = prog:percentprogress

Omit('***',ClarionetUsed=0)
            If ClarionetServer:Active()
                cnprog:thermometer = prog:thermometer
            Else ! If ClarionetServer:Active()
***
                ?prog:pcttext{prop:text} = format(prog:percentprogress,@n3) & '% Completed'
Omit('***',ClarionetUsed=0)
            End ! If ClarionetServer:Active()
***
        End
    End
    Display()

prog:cancelcheck         routine
    cancel# = 0
    prog:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','Cancel Pressed',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
            Of 1 ! &Yes Button
                prog:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


prog:Finish         routine
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        CNprog:thermometer = 100
    Else ! If ClarionetServer:Active()
***
        prog:thermometer = 100
        ?prog:pcttext{prop:text} = '100% Completed'
        display()
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PrintRoutines',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:refnumber',tmp:refnumber,'PrintRoutines',1)
    SolaceViewVars('tmp:ReportType',tmp:ReportType,'PrintRoutines',1)
    SolaceViewVars('tmp:ThePath',tmp:ThePath,'PrintRoutines',1)
    SolaceViewVars('tmp:ExportFile',tmp:ExportFile,'PrintRoutines',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportType;  SolaceCtrlName = '?tmp:ReportType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio1;  SolaceCtrlName = '?Option1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:refnumber;  SolaceCtrlName = '?tmp:refnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:refnumber:prompt;  SolaceCtrlName = '?tmp:refnumber:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CreateCSV;  SolaceCtrlName = '?CreateCSV';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print;  SolaceCtrlName = '?Print';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PrintRoutines')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'PrintRoutines')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Option1:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOGDESNO.Open
  Relate:LOGRTHIS.Open
  Access:LOGSTOCK.UseFile
  Access:LOGSTHII.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ?tmp:refnumber{prop:vcr} = TRUE
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,?Sheet1)
  window{prop:buffer} = 1
  FDB3.Init(?tmp:refnumber,Queue:FileDrop.ViewPosition,FDB3::View:FileDrop,Queue:FileDrop,Relate:LOGDESNO,ThisWindow)
  FDB3.Q &= Queue:FileDrop
  FDB3.AddSortOrder(ldes:DespatchNoKey)
  FDB3.AddField(ldes:DespatchNo,FDB3.Q.ldes:DespatchNo)
  ThisWindow.AddItem(FDB3.WindowComponent)
  FDB3.DefaultFill = 0
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGDESNO.Close
    Relate:LOGRTHIS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'PrintRoutines',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:ReportType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ReportType, Accepted)
      Hide(?tmp:refnumber)
      Hide(?tmp:refnumber:prompt)
      Case tmp:ReportType
          Of 1
              Unhide(?tmp:refnumber)
              Unhide(?tmp:refnumber:prompt)
      End!Case tmp:ReportType
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ReportType, Accepted)
    OF ?CreateCSV
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateCSV, Accepted)
      If tmp:RefNumber <> ''
          If filedialog ('Choose Directory To Save The Export File',tmp:ThePath,'All Directories|*.*', |
                      file:save+file:keepdir + file:noerror + file:longname + file:directory)
              tmp:ExportFile = 'Despatch Note ' & Clip(tmp:RefNumber) & '.csv'
              If Sub(Clip(tmp:ThePath),-1,1) = '\'
                  tmp:ExportFile = Clip(tmp:ThePath) & Clip(tmp:ExportFile)
              Else ! If Sub(Clip(tmp:ThePath),-1,1) = '\'
                  tmp:ExportFile = Clip(tmp:ThePath) & '\' & Clip(tmp:ExportFile)
              End ! If Sub(Clip(tmp:ThePath),-1,1) = '\'
      
              Remove(tmp:ExportFile)
              Create(ExportFile)
              Open(ExportFile)
      
              Clear(exp:Record)
              exp:SalesCode = 'Sales Code'
              exp:Description = 'Description'
              exp:ModelNumber = 'Model Number'
              exp:IMEINumber = 'IMEI Number'
              Add(ExportFile)
      
              Prog.ProgressSetup(Records(LOGSTHII))
      
              Access:LOGSTHIS.Clearkey(logsth:DespatchNoKey)
              logsth:DespatchNo   = tmp:RefNumber
              If Access:LOGSTHIS.Tryfetch(logsth:DespatchNoKey) = Level:Benign
                  !Found
                  Access:LOGSTOCK.Clearkey(logsto:RefNumberKey)
                  logsto:RefNumber    = logsth:RefNumber
                  If Access:LOGSTOCK.Tryfetch(logsto:RefNumberKey) = Level:Benign
                      !Found
                      Access:LOGSTHII.Clearkey(logsti:RefNumberKey)
                      logsti:RefNumber = logsth:RecordNumber
                      Set(logsti:RefNumberKey,logsti:RefNumberKey)
                      Loop ! Begin LOGSTHII Loop
                          If Access:LOGSTHII.Next()
                              Break
                          End ! If !Access
                          If logsti:RefNumber <> logsth:RecordNumber
                              Break
                          End ! If
      
                          If Prog.InsideLoop()
                              Break
                          End ! If Prog.InsideLoop
      
                          Clear(exp:record)
                          exp:SalesCode   = logsto:SalesCode
                          exp:Description = logsto:Description
                          exp:ModelNumber = logsto:ModelNumber
                          exp:IMEINumber  = logsti:IMEI
                          Add(ExportFile)
                      End ! End LOGSTHII Loop
      
                  Else ! If Access:LOGSTOCK.Tryfetch(logsto:RefNumberKey) = Level:Benign
                      !Error
                  End ! If Access:LOGSTOCK.Tryfetch(logsto:RefNumberKey) = Level:Benign
              Else ! If Access:LOGSTHIS.Tryfetch(logsth:DespatchNoKey) = Level:Benign
                  !Error
              End ! If Access:LOGSTHIS.Tryfetch(logsth:DespatchNoKey) = Level:Benign
      
              Prog.ProgressFinish()
      
              Close(ExportFile)
      
              Case Message('Export Completed.','ServiceBase 2000',|
                             'Styles\idea.ico','&OK',1,1) 
                  Of 1 ! &OK Button
              End!Case Message
              
      
          End ! file:save+file:keepdir + file:noerror + file:longname + file:directory)
      
      End ! tmp:RefNumber <> ''
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateCSV, Accepted)
    OF ?Print
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
      Case tmp:ReportType
          Of 1 !Despatch Note
              LogDespatchNote(tmp:refnumber)
      End!Case tmp:ReportType
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'PrintRoutines')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Window,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

Prog.ProgressSetup        Procedure(Long func:Records)
Code
    prog:SkipRecords = 0

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(CN:ProgWindow)
    Else ! If ClarionetServer:Active()
***
        Open(ProgWindow)
        Prog.ResetProgress(func:Records)
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
Code
    prog:recordspercycle         = 25
    prog:recordsprocessed        = 0
    prog:percentprogress         = 0
    prog:thermometer    = 0
    prog:recordstoprocess    = func:Records

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        ?prog:pcttext{prop:text} = '0% Completed'
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.InsideLoop         Procedure()
Code

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Do Prog:NextRecord
        Do Prog:CancelCheck
        If Prog:Cancel = 1
            Return 1
        End !If Prog:Cancel = 1
        Return 0
Omit('***',ClarionetUsed=0)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
Code
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        ?prog:UserString{prop:Text} = Clip(func:String)
        Display()
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ProgressFinish     Procedure()
Code
    Do Prog:Finish
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(CN:progwindow)
    Else ! If ClarionetServer:Active()
***
        close(progwindow)
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

