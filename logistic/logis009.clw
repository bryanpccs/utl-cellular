

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS009.INC'),ONCE        !Local module procedure declarations
                     END


UpdateLOGCLSTE PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::logclu:Record LIKE(logclu:RECORD),STATIC
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
QuickWindow          WINDOW('Insert Club Nokia Site'),AT(,,220,143),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('UpdateLOGCLSTE'),SYSTEM,GRAY,DOUBLE,MDI,IMM
                       SHEET,AT(4,4,212,108),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Club Nokia Site'),AT(8,20),USE(?logclu:ClubNokia:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(logclu:ClubNokia),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Postcode'),AT(8,36),USE(?LOGCLU:Postcode:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s15),AT(84,36,64,10),USE(logclu:Postcode),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Address'),AT(8,48),USE(?LOGCLU:AddressLine1:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,48,124,10),USE(logclu:AddressLine1),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           ENTRY(@s30),AT(84,60,124,10),USE(logclu:AddressLine2),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           ENTRY(@s30),AT(84,72,124,10),USE(logclu:AddressLine3),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Telephone Number'),AT(8,84),USE(?LOGCLU:TelephoneNumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,64,10),USE(logclu:TelephoneNumber),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Fax Number'),AT(9,96),USE(?LOGCLU:FaxNumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,96,64,10),USE(logclu:FaxNumber),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(4,116,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,120,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,120,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
szpostcode      cstring(15)
szpath          cstring(255)
szaddress       cstring(255)
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateLOGCLSTE',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateLOGCLSTE',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateLOGCLSTE',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logclu:ClubNokia:Prompt;  SolaceCtrlName = '?logclu:ClubNokia:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logclu:ClubNokia;  SolaceCtrlName = '?logclu:ClubNokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOGCLU:Postcode:Prompt;  SolaceCtrlName = '?LOGCLU:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logclu:Postcode;  SolaceCtrlName = '?logclu:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOGCLU:AddressLine1:Prompt;  SolaceCtrlName = '?LOGCLU:AddressLine1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logclu:AddressLine1;  SolaceCtrlName = '?logclu:AddressLine1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logclu:AddressLine2;  SolaceCtrlName = '?logclu:AddressLine2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logclu:AddressLine3;  SolaceCtrlName = '?logclu:AddressLine3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOGCLU:TelephoneNumber:Prompt;  SolaceCtrlName = '?LOGCLU:TelephoneNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logclu:TelephoneNumber;  SolaceCtrlName = '?logclu:TelephoneNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOGCLU:FaxNumber:Prompt;  SolaceCtrlName = '?LOGCLU:FaxNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logclu:FaxNumber;  SolaceCtrlName = '?logclu:FaxNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Club Nokia Site'
  OF ChangeRecord
    ActionMessage = 'Changing A Club Nokia Site'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateLOGCLSTE')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateLOGCLSTE')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?logclu:ClubNokia:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(logclu:Record,History::logclu:Record)
  SELF.AddHistoryField(?logclu:ClubNokia,2)
  SELF.AddHistoryField(?logclu:Postcode,3)
  SELF.AddHistoryField(?logclu:AddressLine1,4)
  SELF.AddHistoryField(?logclu:AddressLine2,5)
  SELF.AddHistoryField(?logclu:AddressLine3,6)
  SELF.AddHistoryField(?logclu:TelephoneNumber,7)
  SELF.AddHistoryField(?logclu:FaxNumber,8)
  SELF.AddUpdateFile(Access:LOGCLSTE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:LOGCLSTE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOGCLSTE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?CurrentTab)
  QuickWindow{prop:buffer} = 1
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:LOGCLSTE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateLOGCLSTE',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?logclu:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logclu:Postcode, Accepted)
      Postcode_Routine(LOGCLU:Postcode,LOGCLU:AddressLine1,LOGCLU:AddressLine2,LOGCLU:AddressLine3)
      Select(?logclu:addressline1,1)
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logclu:Postcode, Accepted)
    OF ?logclu:TelephoneNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logclu:TelephoneNumber, Accepted)
      
          temp_string = Clip(left(logclu:TelephoneNumber))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          logclu:TelephoneNumber    = temp_string
          Display(?logclu:TelephoneNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logclu:TelephoneNumber, Accepted)
    OF ?logclu:FaxNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logclu:FaxNumber, Accepted)
      
          temp_string = Clip(left(logclu:FaxNumber))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          logclu:FaxNumber    = temp_string
          Display(?logclu:FaxNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?logclu:FaxNumber, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateLOGCLSTE')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue

