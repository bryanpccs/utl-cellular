

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS005.INC'),ONCE        !Local module procedure declarations
                     END


UpdateLOGSTHIS PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::logsth:Record LIKE(logsth:RECORD),STATIC
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
QuickWindow          WINDOW('Update the LOGSTHIS File'),AT(,,204,140),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateLOGSTHIS'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,196,114),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?logsth:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(72,20,40,10),USE(logsth:RecordNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Ref Number'),AT(8,34),USE(?logsth:RefNumber:Prompt),TRN
                           ENTRY(@s8),AT(72,34,40,10),USE(logsth:RefNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Date'),AT(8,48),USE(?logsth:Date:Prompt)
                           ENTRY(@d6),AT(72,48,104,10),USE(logsth:Date),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Stock In'),AT(8,62),USE(?logsth:StockIn:Prompt),TRN
                           ENTRY(@s8),AT(72,62,40,10),USE(logsth:StockIn),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('StockOut'),AT(8,76),USE(?logsth:StockOut:Prompt),TRN
                           ENTRY(@s8),AT(72,76,40,10),USE(logsth:StockOut),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Status'),AT(8,90),USE(?logsth:Status:Prompt)
                           ENTRY(@s3),AT(72,90,40,10),USE(logsth:Status)
                           PROMPT('Club Nokia Site'),AT(8,104),USE(?logsth:ClubNokia:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,104,124,10),USE(logsth:ClubNokia),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       BUTTON('OK'),AT(57,122,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(106,122,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(155,122,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateLOGSTHIS',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateLOGSTHIS',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateLOGSTHIS',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:RecordNumber:Prompt;  SolaceCtrlName = '?logsth:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:RecordNumber;  SolaceCtrlName = '?logsth:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:RefNumber:Prompt;  SolaceCtrlName = '?logsth:RefNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:RefNumber;  SolaceCtrlName = '?logsth:RefNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:Date:Prompt;  SolaceCtrlName = '?logsth:Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:Date;  SolaceCtrlName = '?logsth:Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:StockIn:Prompt;  SolaceCtrlName = '?logsth:StockIn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:StockIn;  SolaceCtrlName = '?logsth:StockIn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:StockOut:Prompt;  SolaceCtrlName = '?logsth:StockOut:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:StockOut;  SolaceCtrlName = '?logsth:StockOut';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:Status:Prompt;  SolaceCtrlName = '?logsth:Status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:Status;  SolaceCtrlName = '?logsth:Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:ClubNokia:Prompt;  SolaceCtrlName = '?logsth:ClubNokia:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?logsth:ClubNokia;  SolaceCtrlName = '?logsth:ClubNokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a LOGSTHIS Record'
  OF ChangeRecord
    ActionMessage = 'Changing a LOGSTHIS Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateLOGSTHIS')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateLOGSTHIS')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?logsth:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(logsth:Record,History::logsth:Record)
  SELF.AddHistoryField(?logsth:RecordNumber,1)
  SELF.AddHistoryField(?logsth:RefNumber,2)
  SELF.AddHistoryField(?logsth:Date,3)
  SELF.AddHistoryField(?logsth:StockIn,5)
  SELF.AddHistoryField(?logsth:StockOut,6)
  SELF.AddHistoryField(?logsth:Status,7)
  SELF.AddHistoryField(?logsth:ClubNokia,8)
  SELF.AddUpdateFile(Access:LOGSTHIS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOGSTHIS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOGSTHIS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?CurrentTab)
  QuickWindow{prop:buffer} = 1
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGSTHIS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateLOGSTHIS',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateLOGSTHIS')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue

