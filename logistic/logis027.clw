

   MEMBER('logistic.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('LOGIS027.INC'),ONCE        !Local module procedure declarations
                     END


WorkOutQty           PROCEDURE  (f_RefNumber)         ! Declare Procedure
save_lstl_id         USHORT,AUTO
save_logser_id       USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'WorkOutQty')      !Add Procedure to Log
  end


    save_lstl_id = access:logstloc.savefile()
    access:logstloc.clearkey(lstl:reflocationkey)
    lstl:refnumber = f_RefNumber
    set(lstl:reflocationkey,lstl:reflocationkey)
    loop
        if access:logstloc.next()
           break
        end !if
        if lstl:refnumber <> f_RefNumber      |
            then break.  ! end if
    Compile('***',Debug=1)
        Message('Looping Through Locations','Debug Message',icon:exclamation)
    ***
        lstl:avlquantity    = 0
        lstl:desquantity    = 0
        lstl:allocquantity  = 0
        save_logser_id = access:logserst.savefile()
        access:logserst.clearkey(logser:refnumberkey)
        logser:refnumber = lstl:recordnumber
        set(logser:refnumberkey,logser:refnumberkey)
        loop
            if access:logserst.next()
               break
            end !if
            if logser:refnumber <> lstl:recordnumber      |
                then break.  ! end if
            If logser:status    = 'AVAILABLE'
                lstl:avlquantity    += 1
            Compile('***',Debug=1)
                Message('Found Available','Debug Message',icon:exclamation)
            ***
            End!If logser:status    = 'AVAILABLE'
            If logser:status    = 'DESPATCHED'
                lstl:desquantity    += 1
            End!If logser:status    = 'DESPATCHED'
            If logser:status    = 'ALLOCATED'
                lstl:allocquantity    += 1
            End!If logser:status    = 'ALLOCATED'
        end !loop
        access:logserst.restorefile(save_logser_id)

        access:logstloc.update()
    end !loop
    access:logstloc.restorefile(save_lstl_id)



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'WorkOutQty',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_lstl_id',save_lstl_id,'WorkOutQty',1)
    SolaceViewVars('save_logser_id',save_logser_id,'WorkOutQty',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
