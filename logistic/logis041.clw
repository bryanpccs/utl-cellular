

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS041.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('LOGIS027.INC'),ONCE        !Req'd for module callout resolution
                     END


StockReturns_New PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
tmp:imei             STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Location             STRING(30)
Loc_flag             BYTE
tmp:RefNumber        LONG
tmp:Description      STRING(30)
tmp:ModelNumber      STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?lrtn:ClubNokiaSite
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?lrtn:To_Site
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?Location
logstl:Location        LIKE(logstl:Location)          !List box control field - type derived from field
logstl:RefNumber       LIKE(logstl:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?lrtn:SalesCode
logsto:SalesCode       LIKE(logsto:SalesCode)         !List box control field - type derived from field
logsto:Description     LIKE(logsto:Description)       !List box control field - type derived from field
logsto:RefNumber       LIKE(logsto:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(LOGRTHIS)
                       PROJECT(lsrh:IMEI)
                       PROJECT(lsrh:RefNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
lsrh:IMEI              LIKE(lsrh:IMEI)                !List box control field - type derived from field
lsrh:RefNumber         LIKE(lsrh:RefNumber)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB9::View:FileDropCombo VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
FDCB10::View:FileDropCombo VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
FDCB14::View:FileDropCombo VIEW(LOGSTOLC)
                       PROJECT(logstl:Location)
                       PROJECT(logstl:RefNumber)
                     END
FDCB15::View:FileDropCombo VIEW(LOGSTOCK)
                       PROJECT(logsto:SalesCode)
                       PROJECT(logsto:Description)
                       PROJECT(logsto:RefNumber)
                     END
History::lrtn:Record LIKE(lrtn:RECORD),STATIC
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
QuickWindow          WINDOW('Stock Returns'),AT(,,379,215),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('StockReturns'),SYSTEM,GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,212,180),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           COMBO(@s30),AT(84,20,124,10),USE(lrtn:ClubNokiaSite),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           STRING('To Site'),AT(8,36),USE(?String1)
                           COMBO(@s30),AT(84,36,124,10),USE(lrtn:To_Site),IMM,FONT(,,,FONT:bold),REQ,UPR,FORMAT('120L|M~Entry 30~L(2)@s30@'),DROP(5),FROM(Queue:FileDropCombo:1)
                           PROMPT('From Site'),AT(8,20),USE(?Prompt3)
                           PROMPT('Returns Number'),AT(8,52),USE(?LRTN:ReturnsNumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(lrtn:ReturnsNumber),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           STRING('Sales Code'),AT(8,68),USE(?String2)
                           COMBO(@s20),AT(84,68,124,10),USE(lrtn:SalesCode),IMM,FONT(,,,FONT:bold),REQ,UPR,FORMAT('120L(2)|M@s30@120L(2)|M@s30@'),DROP(5,240),FROM(Queue:FileDropCombo:2),MSG('Sales Code')
                           PROMPT('Model Number'),AT(8,84),USE(?tmp:ModelNumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,124,10),USE(tmp:ModelNumber),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Description'),AT(8,100),USE(?tmp:Description:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,100,124,10),USE(tmp:Description),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           STRING('Location'),AT(8,116),USE(?String3)
                           COMBO(@s20),AT(84,116,124,10),USE(Location),IMM,FONT(,,,FONT:bold),REQ,UPR,FORMAT('120L|M~Stock Location~L(2)@s30@'),DROP(5),FROM(Queue:FileDropCombo:3)
                           PROMPT('I.M.E.I. Number'),AT(8,148),USE(?tmp:imei:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,148,124,10),USE(tmp:imei),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       SHEET,AT(220,4,156,180),USE(?Sheet2),SPREAD
                         TAB('Scanned I.M.E.I. Numbers'),USE(?Tab3)
                           LIST,AT(224,20,148,144),USE(?Browse:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('80L(2)|M~I.M.E.I. Number~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Insert'),AT(224,168,45,14),USE(?Insert:3),HIDE
                           BUTTON('&Change'),AT(272,168,45,14),USE(?Change:3),HIDE
                           BUTTON('&Delete'),AT(324,168,45,14),USE(?Delete:3),HIDE
                         END
                       END
                       PANEL,AT(4,188,372,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(260,192,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(316,192,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

BRW2                 CLASS(BrowseClass)               !Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW2::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

FDCB9                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB14               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB15               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'StockReturns_New',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'StockReturns_New',1)
    SolaceViewVars('ActionMessage',ActionMessage,'StockReturns_New',1)
    SolaceViewVars('tmp:imei',tmp:imei,'StockReturns_New',1)
    SolaceViewVars('Location',Location,'StockReturns_New',1)
    SolaceViewVars('Loc_flag',Loc_flag,'StockReturns_New',1)
    SolaceViewVars('tmp:RefNumber',tmp:RefNumber,'StockReturns_New',1)
    SolaceViewVars('tmp:Description',tmp:Description,'StockReturns_New',1)
    SolaceViewVars('tmp:ModelNumber',tmp:ModelNumber,'StockReturns_New',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lrtn:ClubNokiaSite;  SolaceCtrlName = '?lrtn:ClubNokiaSite';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lrtn:To_Site;  SolaceCtrlName = '?lrtn:To_Site';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LRTN:ReturnsNumber:Prompt;  SolaceCtrlName = '?LRTN:ReturnsNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lrtn:ReturnsNumber;  SolaceCtrlName = '?lrtn:ReturnsNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lrtn:SalesCode;  SolaceCtrlName = '?lrtn:SalesCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber:Prompt;  SolaceCtrlName = '?tmp:ModelNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber;  SolaceCtrlName = '?tmp:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Description:Prompt;  SolaceCtrlName = '?tmp:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Description;  SolaceCtrlName = '?tmp:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String3;  SolaceCtrlName = '?String3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location;  SolaceCtrlName = '?Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:imei:Prompt;  SolaceCtrlName = '?tmp:imei:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:imei;  SolaceCtrlName = '?tmp:imei';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:2;  SolaceCtrlName = '?Browse:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('StockReturns_New')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'StockReturns_New')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?lrtn:ClubNokiaSite
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(lrtn:Record,History::lrtn:Record)
  SELF.AddHistoryField(?lrtn:ClubNokiaSite,2)
  SELF.AddHistoryField(?lrtn:To_Site,9)
  SELF.AddHistoryField(?lrtn:ReturnsNumber,5)
  SELF.AddHistoryField(?lrtn:SalesCode,10)
  SELF.AddUpdateFile(Access:LOGRETRN)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOGCLSTE.Open
  Relate:LOGRETRN.Open
  Relate:LOGSERST.Open
  Relate:LOGSTOLC.Open
  Access:LOGSTHII.UseFile
  Access:LOGSTHIS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOGRETRN
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:LOGRTHIS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  !Post(Event:Accepted,?lrtn:returntype)
  ?Browse:2{prop:vcr} = TRUE
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,lsrh:RefNumberKey)
  BRW2.AddRange(lsrh:RefNumber,Relate:LOGRTHIS,Relate:LOGRETRN)
  BRW2.AddField(lsrh:IMEI,BRW2.Q.lsrh:IMEI)
  BRW2.AddField(lsrh:RefNumber,BRW2.Q.lsrh:RefNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?CurrentTab)
  QuickWindow{prop:buffer} = 1
  FDCB9.Init(lrtn:ClubNokiaSite,?lrtn:ClubNokiaSite,Queue:FileDropCombo.ViewPosition,FDCB9::View:FileDropCombo,Queue:FileDropCombo,Relate:LOGCLSTE,ThisWindow,GlobalErrors,0,1,0)
  FDCB9.Q &= Queue:FileDropCombo
  FDCB9.AddSortOrder(logclu:ClubNokiaKey)
  FDCB9.AddField(logclu:ClubNokia,FDCB9.Q.logclu:ClubNokia)
  FDCB9.AddField(logclu:RecordNumber,FDCB9.Q.logclu:RecordNumber)
  ThisWindow.AddItem(FDCB9.WindowComponent)
  FDCB9.DefaultFill = 0
  FDCB10.Init(lrtn:To_Site,?lrtn:To_Site,Queue:FileDropCombo:1.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:1,Relate:LOGCLSTE,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:1
  FDCB10.AddSortOrder(logclu:ClubNokiaKey)
  FDCB10.AddField(logclu:ClubNokia,FDCB10.Q.logclu:ClubNokia)
  FDCB10.AddField(logclu:RecordNumber,FDCB10.Q.logclu:RecordNumber)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  FDCB14.Init(Location,?Location,Queue:FileDropCombo:3.ViewPosition,FDCB14::View:FileDropCombo,Queue:FileDropCombo:3,Relate:LOGSTOLC,ThisWindow,GlobalErrors,0,1,0)
  FDCB14.Q &= Queue:FileDropCombo:3
  FDCB14.AddSortOrder(logstl:LocationKey)
  FDCB14.AddField(logstl:Location,FDCB14.Q.logstl:Location)
  FDCB14.AddField(logstl:RefNumber,FDCB14.Q.logstl:RefNumber)
  FDCB14.AddUpdateField(logstl:Location,Location)
  FDCB14.AddUpdateField(logstl:Mark_Returns_Available,logstl:Mark_Returns_Available)
  ThisWindow.AddItem(FDCB14.WindowComponent)
  FDCB14.DefaultFill = 0
  FDCB15.Init(lrtn:SalesCode,?lrtn:SalesCode,Queue:FileDropCombo:2.ViewPosition,FDCB15::View:FileDropCombo,Queue:FileDropCombo:2,Relate:LOGSTOCK,ThisWindow,GlobalErrors,0,1,0)
  FDCB15.Q &= Queue:FileDropCombo:2
  FDCB15.AddSortOrder(logsto:SalesKey)
  FDCB15.AddField(logsto:SalesCode,FDCB15.Q.logsto:SalesCode)
  FDCB15.AddField(logsto:Description,FDCB15.Q.logsto:Description)
  FDCB15.AddField(logsto:RefNumber,FDCB15.Q.logsto:RefNumber)
  FDCB15.AddUpdateField(logsto:SalesCode,lrtn:SalesCode)
  FDCB15.AddUpdateField(logsto:Description,tmp:Description)
  FDCB15.AddUpdateField(logsto:ModelNumber,tmp:ModelNumber)
  FDCB15.AddUpdateField(logsto:RefNumber,tmp:RefNumber)
  ThisWindow.AddItem(FDCB15.WindowComponent)
  FDCB15.DefaultFill = 0
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGCLSTE.Close
    Relate:LOGRETRN.Close
    Relate:LOGSERST.Close
    Relate:LOGSTOLC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'StockReturns_New',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?lrtn:SalesCode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lrtn:SalesCode, Accepted)
      !case globalresponse
      !    of requestcompleted
      !        !tmp:salescode = logsto:salescode
      !        tmp:modelnumber    = logsto:modelnumber
      !        tmp:description = logsto:description
      !        tmp:refnumber   = logsto:refnumber
      !    of requestcancelled
      !!        tmp:salescode = ''
      !        select(?-1)
      !end!case globalreponse
      display(?)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lrtn:SalesCode, Accepted)
    OF ?tmp:imei
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:imei, Accepted)
      IF lrtn:ClubNokiaSite = ''
        SELECT(?lrtn:ClubNokiaSite)
        CYCLE
      END
      IF lrtn:ReturnsNumber = ''
        SELECT(?lrtn:ReturnsNumber)
        CYCLE
      END
      IF lrtn:To_Site = ''
        SELECT(?lrtn:To_Site)
        CYCLE
      END
      IF lrtn:SalesCode = ''
        SELECT(?lrtn:SalesCode)
        CYCLE
      END
      If ~0{prop:acceptall}
          get(logrthis,0)
          if access:logrthis.primerecord() = LeveL:Benign
              lsrh:refnumber   = lrtn:RefNumber
              lsrh:imei        = tmp:Imei
              lsrh:modelnumber = lrtn:ModelNumber
              if access:logrthis.insert()
                 access:logrthis.cancelautoinc()
              end
          End!if access:logrthis.primerecord() = LeveL:Benign
          Select(?tmp:imei)
      End!If ~0{prop:acceptall}
      
      access:logstloc.clearkey(lstl:reflocationkey)
      lstl:refnumber = tmp:RefNumber
      lstl:location  = Location
      if access:logstloc.tryfetch(lstl:reflocationkey) = Level:Benign
          get(logsthis,0)
          if access:logsthis.primerecord() = Level:Benign
             get(logserst,0)
             Access:Logserst.ClearKey(logser:RefNumberKey)
             logser:RefNumber = lstl:recordnumber
             logser:ESN       = tmp:Imei
             IF Access:Logserst.Fetch(logser:RefNumberKey)
               !Not here - insert it!
               if access:logserst.primerecord() = Level:Benign
                  logser:refnumber    = lstl:recordnumber
                  logser:esn          = tmp:IMEI
                  IF Loc_flag = TRUE
                    logser:Status = 'AVAILABLE'
                  ELSE
                    logser:Status = 'RETURNED'
                  END
                  logser:clubnokia    = lrtn:To_Site
      
                  if access:logserst.insert()
                    access:logserst.cancelautoinc()
                  end
               ELSE
                  logser:refnumber    = lstl:recordnumber
                  logser:esn          = tmp:IMEI
                  IF Loc_flag = TRUE
                    logser:Status = 'AVAILABLE'
                  ELSE
                    logser:Status = 'RETURNED'
                  END
                  logser:clubnokia    = lrtn:To_Site
                  access:logserst.Update()
               End!if access:logserst.primerecord() = Level:Benign
             END
             Get(logsthii,0)
             If access:logsthii.primerecord() = Level:Benign
               logsti:RefNumber    = logsth:RecordNumber
               logsti:IMEI         = tmp:IMEI
               If access:logsthii.tryinsert()
                 access:logsthii.cancelautoinc()
               End!If access:logsthii.tryinsert()
              End!If access:logsthii.primerecord() = Level:Benign
      
              logsth:refnumber      = tmp:refnumber
              logsth:stockin        = 1
              IF Loc_flag = TRUE
                logser:Status = 'AVAILABLE'
              ELSE
                logser:Status = 'RETURNED'
              END
              logsth:clubnokia      = lrtn:To_Site
              !logsth:smpfnumber     = tmp:smpfnumber
              !logsth:despatchnoteno = tmp:deliverynote
              logsth:modelnumber    = tmp:ModelNumber
              logsth:location       = location
              logsth:date           = Today()
              access:users.clearkey(use:password_key)
              use:password    = glo:password
              access:users.tryfetch(use:password_key)
              logsth:usercode     = use:user_code
              if access:logsthis.insert()
                  access:logsthis.cancelautoinc()
              end
          end
      end
      WorkOutQty(tmp:refnumber)
      !Select(?tmp:imei)
      
      
      
                          
      BRW2.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:imei, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'StockReturns_New')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    OF ?Sheet2
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW2.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue

