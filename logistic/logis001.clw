

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetTalk.inc'),ONCE

                     MAP
                       INCLUDE('LOGIS001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('LOGIS003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS016.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS022.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS028.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS038.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS039.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS040.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('PASS_SCR.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Frame

LocalRequest         LONG
start_help_temp      STRING(255)
pos                  STRING(255)
loc:lastcount        LONG
loc:lasttime         LONG
FilesOpened          BYTE
CurrentTab           STRING(80)
FP::PopupString      STRING(4096)
FP::ReturnValues     QUEUE,PRE(FP:)
RVPtr                LONG
                     END
comp_name_temp       STRING(255)
AllowUserToExit      BYTE
Date_Temp            STRING(60)
Time_Temp            STRING(60)
SRCF::Version        STRING(512)
SRCF::Format         STRING(512)
CPCSExecutePopUp     BYTE
BackerDDEName        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Frame  WinType = Frame
mo:SelectedField  Long
AppFrame             APPLICATION('ServiceBase 2000 Cellular Logistics'),AT(,,647,319),FONT('Arial',8,,),CENTER,IMM,HVSCROLL,ICON('Pc.ico'),ALRT(F12Key),STATUS(-1,-1,0),SYSTEM,MAX,MAXIMIZE,RESIZE
                       MENUBAR
                         MENU('File'),USE(?File)
                           ITEM('&Print Setup ...'),USE(?PrintSetup),MSG('Setup printer'),STD(STD:PrintSetup)
                           ITEM,SEPARATOR
                           ITEM('&Re-Login'),USE(?ReLogin)
                           ITEM,SEPARATOR
                           ITEM('E&xit'),USE(?Exit),MSG('Exit this application'),STD(STD:Close)
                         END
                         MENU('&Edit')
                           ITEM('Cu&t'),USE(?Cut),MSG('Remove item to Windows Clipboard'),STD(STD:Cut)
                           ITEM('&Copy'),USE(?Copy),MSG('Copy item to Windows Clipboard'),STD(STD:Copy)
                           ITEM('&Paste'),USE(?Paste),MSG('Paste contents of Windows Clipboard'),STD(STD:Paste)
                         END
                         MENU('System Administration'),USE(?SystemAdministration)
                           ITEM('Defaults'),USE(?Defaults)
                           ITEM('Club Nokia Sites'),USE(?ClubNokiaSites)
                           ITEM('Stock Locations'),USE(?StockLocations)
                           ITEM('ESN Model Correlation'),USE(?SystemAdministrationESNModelCorrelation)
                         END
                         MENU('Export Routines'),USE(?ExportRoutines)
                           ITEM('Global Data Export'),USE(?ExportRoutinesGlobalDataExport)
                           ITEM('Stock Movement Export'),USE(?StockMovementExport)
                         END
                         MENU('Reports'),USE(?Reports)
                           ITEM('I.M.E.I. Number History'),USE(?IMEINumberHistory)
                           ITEM('Stock Level Report'),USE(?StockLevelReport)
                         END
                         MENU('&Window'),MSG('Create and Arrange windows'),STD(STD:WindowList)
                           ITEM('T&ile'),USE(?Tile),MSG('Make all open windows visible'),STD(STD:TileWindow)
                           ITEM('&Cascade'),USE(?Cascade),MSG('Stack all open windows'),STD(STD:CascadeWindow)
                           ITEM('&Arrange Icons'),USE(?Arrange),MSG('Align all window icons'),STD(STD:ArrangeIcons)
                         END
                         MENU('&Help'),USE(?Help),RIGHT
                         END
                       END
                       TOOLBAR,AT(0,0,,26),COLOR(COLOR:Silver),TILED
                         BUTTON('Exit'),AT(568,1,76,24),USE(?Button6),LEFT,ICON('exitcomp.gif'),STD(STD:Close)
                         BUTTON('Despatch Allocated Stock'),AT(404,1,76,24),USE(?Button8),LEFT,ICON('proc_out.gif')
                         BUTTON('Allocate Stock'),AT(244,1,76,24),USE(?Allocate_Stock),LEFT,ICON('proc_out.gif')
                         BUTTON('Stock Returns'),AT(484,1,76,24),USE(?StockReturns),LEFT,ICON('proc_out.gif')
                         BUTTON('Transfer Stock'),AT(164,1,76,24),USE(?TransferStock),LEFT,ICON('proc_out.gif')
                         BUTTON('Despatch Stock'),AT(324,1,76,24),USE(?DespatchStock),LEFT,ICON('proc_out.gif')
                         BUTTON('Receive Stock'),AT(84,1,76,24),USE(?ReceiveStock),LEFT,ICON('proc_in.gif')
                         BUTTON('Browse Stock'),AT(4,1,76,24),USE(?BrowseStock),LEFT,ICON('stock_br.gif')
                         PANEL,AT(564,1,1,21),USE(?Panel2),BEVEL(0,0,02010H)
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

!Local Data Classes
ThisNetClient        CLASS(NetAutoCloseServer)        !Generated by NetTalk Extension (Class Definition)

                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Login      Routine
    glo:select1 = 'N'
    Insert_Password
    If glo:select1 = 'Y'
        Halt
    Else
        access:users.open
        access:users.usefile
        access:users.clearkey(use:password_key)
        use:password =glo:password
        access:users.fetch(use:password_key)
        set(defaults)
        access:defaults.next()
        0{prop:statustext,2} = 'Current User: ' & Capitalize(Clip(use:forename) & ' ' & Clip(use:surname))
        If glo:password <> 'JOB:ENTER'
            Do Security_Check
        End
        access:users.close
    End
    glo:select1= ''
Log_out      Routine
    access:users.open
    access:users.usefile
    access:logged.open
    access:logged.usefile
    access:users.clearkey(use:password_key)
    use:password =glo:password
    if access:users.fetch(use:password_key) = Level:Benign
        access:logged.clearkey(log:user_code_key)
        log:user_code = use:user_code
        log:time      = glo:timelogged
        if access:logged.fetch(log:user_code_key) = Level:Benign
            delete(logged)
        end !if
    end !if
    access:users.close
    access:logged.close
Security_Check      Routine
    If SecurityCheck('MENU - FILE') = Level:Benign
        enable(?file)
    Else!If SecurityCheck('MENU - FILE') = Level:Benign
        disable(?file)
    End!If SecurityCheck('MENU - FILE') = Level:Benign

    If SecurityCheck('PRINT SETUP') = Level:Benign
        enable(?printsetup)
    Else!If SecurityCheck('PRINT SETUP') = Level:Benign
        disable(?printsetup)
    End!If SecurityCheck('PRINT SETUP') = Level:Benign

    If SecurityCheck('RE-LOGIN') = Level:Benign
        enable(?relogin)
    Else!If SecurityCheck('RE-LOGIN') = Level:Benign
        disable(?relogin)
    End!If SecurityCheck('RE-LOGIN') = Level:Benign
Menu::File ROUTINE                                    !Code for menu items on ?File
  CASE ACCEPTED()
  OF ?ReLogin
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReLogin, Accepted)
    Do Log_out
    Do Login
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReLogin, Accepted)
  END
Menu::SystemAdministration ROUTINE                    !Code for menu items on ?SystemAdministration
  CASE ACCEPTED()
  OF ?Defaults
    LogisticsDefaults
  OF ?ClubNokiaSites
    START(BrowseClubNokiaSite, 25000)
  OF ?StockLocations
    START(BrowseStockLocation, 25000)
  OF ?SystemAdministrationESNModelCorrelation
    START(Browse_ESN_Model, 25000)
  END
Menu::ExportRoutines ROUTINE                          !Code for menu items on ?ExportRoutines
  CASE ACCEPTED()
  OF ?ExportRoutinesGlobalDataExport
    Global_Data_Export
  OF ?StockMovementExport
    StockMovementExport
  END
Menu::Reports ROUTINE                                 !Code for menu items on ?Reports
  CASE ACCEPTED()
  OF ?IMEINumberHistory
    GlobalIMEICriteria
  OF ?StockLevelReport
    StockLevelCriteria
  END
Menu::Help ROUTINE                                    !Code for menu items on ?Help


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Main',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Main',1)
    SolaceViewVars('start_help_temp',start_help_temp,'Main',1)
    SolaceViewVars('pos',pos,'Main',1)
    SolaceViewVars('loc:lastcount',loc:lastcount,'Main',1)
    SolaceViewVars('loc:lasttime',loc:lasttime,'Main',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Main',1)
    SolaceViewVars('CurrentTab',CurrentTab,'Main',1)
    SolaceViewVars('FP::PopupString',FP::PopupString,'Main',1)
    SolaceViewVars('FP::ReturnValues:RVPtr',FP::ReturnValues:RVPtr,'Main',1)
    SolaceViewVars('comp_name_temp',comp_name_temp,'Main',1)
    SolaceViewVars('AllowUserToExit',AllowUserToExit,'Main',1)
    SolaceViewVars('Date_Temp',Date_Temp,'Main',1)
    SolaceViewVars('Time_Temp',Time_Temp,'Main',1)
    SolaceViewVars('SRCF::Version',SRCF::Version,'Main',1)
    SolaceViewVars('SRCF::Format',SRCF::Format,'Main',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'Main',1)
    SolaceViewVars('BackerDDEName',BackerDDEName,'Main',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?File;  SolaceCtrlName = '?File';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PrintSetup;  SolaceCtrlName = '?PrintSetup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReLogin;  SolaceCtrlName = '?ReLogin';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exit;  SolaceCtrlName = '?Exit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cut;  SolaceCtrlName = '?Cut';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Copy;  SolaceCtrlName = '?Copy';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Paste;  SolaceCtrlName = '?Paste';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdministration;  SolaceCtrlName = '?SystemAdministration';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Defaults;  SolaceCtrlName = '?Defaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ClubNokiaSites;  SolaceCtrlName = '?ClubNokiaSites';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockLocations;  SolaceCtrlName = '?StockLocations';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdministrationESNModelCorrelation;  SolaceCtrlName = '?SystemAdministrationESNModelCorrelation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExportRoutines;  SolaceCtrlName = '?ExportRoutines';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExportRoutinesGlobalDataExport;  SolaceCtrlName = '?ExportRoutinesGlobalDataExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockMovementExport;  SolaceCtrlName = '?StockMovementExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reports;  SolaceCtrlName = '?Reports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IMEINumberHistory;  SolaceCtrlName = '?IMEINumberHistory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockLevelReport;  SolaceCtrlName = '?StockLevelReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tile;  SolaceCtrlName = '?Tile';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cascade;  SolaceCtrlName = '?Cascade';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Arrange;  SolaceCtrlName = '?Arrange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button6;  SolaceCtrlName = '?Button6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button8;  SolaceCtrlName = '?Button8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Allocate_Stock;  SolaceCtrlName = '?Allocate_Stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockReturns;  SolaceCtrlName = '?StockReturns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TransferStock;  SolaceCtrlName = '?TransferStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DespatchStock;  SolaceCtrlName = '?DespatchStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReceiveStock;  SolaceCtrlName = '?ReceiveStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BrowseStock;  SolaceCtrlName = '?BrowseStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  AppFrame{Prop:StatusText,1}=CLIP('PC Control Systems Limited �2000')
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Main')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Main')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:LOGGED.Open
  Relate:USERS.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(AppFrame)
  SELF.Opened=True
                                               ! Generated by NetTalk Extension (Start)
  ThisNetClient.DefaultWarningMessage = 'For maintainence reasons this program needs to be closed down. Please immediately save what you are doing and exit the program.'
  ThisNetClient.DefaultCloseDownMessage = 'This application is being closed down by an administrator.'
  ThisNetClient.WarningTimer = 4500
  ThisNetClient.CloseDownTimer = 1000
  ThisNetClient.init('NetCloseApp' & 'cellular', NET:StartService+NET:DontAnnounceNow)
  if ThisNetClient.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  ThisNetClient.Announce(NET:OnlyOncePerThread)     ! Generated by NetTalk Extension
  Start_Screen
  If glo:select1  <> 'OK'
      Halt
  End!If glo:select1  <> 'OK'
  glo:Select1 = ''
  Do Login
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Frame)
  AppFrame{prop:buffer} = 1
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ThisNetClient.Kill(Net:StopService+NET:DontAnnounceNow) ! Generated by NetTalk Extension
  ThisNetClient.Announce(NET:OnlyOncePerThread)     ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Do log_out
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:LOGGED.Close
    Relate:USERS.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Main',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?TransferStock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TransferStock, Accepted)
      glo:file_name2  = 'L' & Sub(Clock(),Len(Clock())-7,7) & '.DAT'
      Remove(glo:file_name2)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TransferStock, Accepted)
    OF ?DespatchStock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DespatchStock, Accepted)
      glo:file_name2  = 'L' & Sub(Clock(),Len(Clock())-7,7) & '.DAT'
      Remove(glo:file_name2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DespatchStock, Accepted)
    OF ?ReceiveStock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReceiveStock, Accepted)
      glo:file_name2  = 'L' & Sub(Clock(),Len(Clock())-7,7) & '.DAT'
      glo:file_name3  = 'LX' & Sub(Clock(),Len(Clock())-6,6) & '.DAT'
      Remove(glo:file_name2)
      Remove(glo:file_name3)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReceiveStock, Accepted)
    ELSE
      DO Menu::File                                   !Process menu items on ?File menu
      DO Menu::SystemAdministration                   !Process menu items on ?SystemAdministration menu
      DO Menu::ExportRoutines                         !Process menu items on ?ExportRoutines menu
      DO Menu::Reports                                !Process menu items on ?Reports menu
      DO Menu::Help                                   !Process menu items on ?Help menu
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button8
      Browse_Allocated_Stock
    OF ?Allocate_Stock
      START(AllocateStock, 25000)
    OF ?StockReturns
      START(BrowseStockReturns, 25000)
    OF ?TransferStock
      DespatchStock('TRANSFER','0')
    OF ?DespatchStock
      DespatchStock('DESPATCH','0')
    OF ?ReceiveStock
      ReceiveStock
    OF ?BrowseStock
      START(BrowseLogStock, 25000)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisNetClient.TakeEvent()                 ! Generated by NetTalk Extension
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Main')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Frame,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Do Log_out
      Do Login
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
         AppFrame{Prop:Active}=1
    OF Event:Timer
       AppFrame{Prop:StatusText,1}=CLIP('PC Control Systems Limited �2000')
    ELSE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue

