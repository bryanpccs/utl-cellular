

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS037.INC'),ONCE        !Local module procedure declarations
                     END


Process_Export PROCEDURE                              !Generated from procedure template - Process

Progress:Thermometer BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
SheetDesc            CSTRING(41)
Process:View         VIEW(LOGSTHIS)
                       PROJECT(logsth:RefNumber)
                       PROJECT(logsth:ModelNumber)
                       PROJECT(logsth:RecordNumber)
                       JOIN(logsto:RefModelNoKey,logsth:RefNumber,logsth:ModelNumber)
                       END
                       JOIN(logsti:RefNumberKey,logsth:RecordNumber)
                       END
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Process  WinType = Process
mo:SelectedField  Long
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
Update                 PROCEDURE(),DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
?F1SS                EQUATE(1000)
!# SW2 Extension            STRING(3)
f1Action             BYTE
f1FileName           CSTRING(256)
RowNumber            USHORT
StartRow             LONG
StartCol             LONG
Template             CSTRING(129)
f1Disabled           BYTE(0)
ssFieldQ              QUEUE(tqField).
CQ                    QUEUE(tqField).
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

!---------------------
ssInit         ROUTINE
!---------------------
  DO ssBuildQueue                                     ! Initialize f1 field queue
  IF NOT UsBrowse(SsFieldQ,'Process_Export',,,,CQ,SheetDesc,StartRow,StartCol,Template)
    ThisWindow.Kill
    RETURN
  END

!---------------------
ssBuildQueue   ROUTINE
!---------------------
    ssFieldQ.Name = 'CHR(39)&CLIP(logsti:IMEI)'
    ssFieldQ.Desc = 'IMEI Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'logsth:Status'
    ssFieldQ.Desc = 'Status'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'logsth:ModelNumber'
    ssFieldQ.Desc = 'Model Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'logsth:SMPFNumber'
    ssFieldQ.Desc = 'SMPF Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'logsth:DespatchNoteNo'
    ssFieldQ.Desc = 'Despatch Note No.'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'logsth:Location'
    ssFieldQ.Desc = 'Location'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'logsth:ClubNokia'
    ssFieldQ.Desc = 'Club Nokia Site'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'FORMAT(logsth:Date,@d6)'
    ssFieldQ.Desc = 'Date'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'logsto:SalesCode'
    ssFieldQ.Desc = 'Sales Code'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'logsto:Description'
    ssFieldQ.Desc = 'Description'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
  SORT(ssFieldQ,+ssFieldQ.Desc)



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Process_Export',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Process_Export',1)
    SolaceViewVars('SheetDesc',SheetDesc,'Process_Export',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Export')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Process_Export')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  DO ssInit
  Relate:LOGSTHIS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  IF ThisWindow.Response <> RequestCancelled
    UsInitDoc(CQ,?F1ss,StartRow,StartCol,Template)
    ?F1SS{'Sheets("Sheet1").Name'} = SheetDesc
    IF Template = ''
      RowNumber = StartRow + 1
    ELSE
      RowNumber = StartRow
    END
  END
  ThisMakeover.SetWindow(Win:Process)
  ProgressWindow{prop:buffer} = 1
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:LOGSTHIS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, logsth:Date)
  ThisProcess.AddSortOrder(logsth:DateStaClubKey)
  ThisProcess.AddRange(logsth:Date,Ex_Start_Date,Ex_End_Date)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(LOGSTHIS,'QUICKSCAN=on')
  SEND(LOGSTOCK,'QUICKSCAN=on')
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGSTHIS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Process_Export',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF ProgressWindow{Prop:AcceptAll} THEN RETURN.
  logsto:RefNumber = logsth:RefNumber                 ! Assign linking field value
  logsto:ModelNumber = logsth:ModelNumber             ! Assign linking field value
  Access:LOGSTOCK.Fetch(logsto:RefModelNoKey)
  PARENT.Reset(Force)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Progress:Cancel
                                                      ! SW2 This section changed
      IF Message('Would you like to view the partially created spreadsheet?','Process Cancelled',ICON:Question,Button:Yes+Button:No,Button:No)|
      = Button:Yes
        UsDeInit(f1FileName, ?F1SS, 1)
      END
      ReturnValue = Level:Fatal
      RETURN ReturnValue
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ThisWindow.Response = RequestCompleted
    f1FileName = 'logexport'
    f1Action = 2
  
    IF UsGetFileName( f1FileName, 1, f1Action )
      UsDeInit( f1FileName, ?F1SS, f1Action )
    END
    ?F1SS{'quit()'}
  ELSE
    IF RowNumber THEN ?F1SS{'quit()'}.
  END
  ReturnValue = PARENT.TakeCloseEvent()
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Process_Export')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Process,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.Update PROCEDURE

  CODE
  PARENT.Update
  logsto:RefNumber = logsth:RefNumber                 ! Assign linking field value
  logsto:ModelNumber = logsth:ModelNumber             ! Assign linking field value
  Access:LOGSTOCK.Fetch(logsto:RefModelNoKey)


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
  UsFillRow(CQ,?F1SS,RowNumber,StartCol)
  RETURN ReturnValue


ThisProcess.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  IF Ex_ESN <> ''
    IF logsti:IMEI <> Ex_ESN
      ReturnValue = Record:Filtered
    END
  END
  
  IF Ex_SalesCode <> ''
    IF logsto:SalesCode <> Ex_SalesCode
      ReturnValue = Record:Filtered
    END
  END
  
  IF Ex_ModelNumber <> ''
    IF logsth:ModelNumber <> Ex_ModelNumber
      ReturnValue = Record:Filtered
    END
  END
  
  IF Ex_SMPFNumber <> ''
    IF logsth:SMPFNumber <> Ex_SMPFNumber
      ReturnValue = Record:Filtered
    END
  END
  
  IF Ex_DespatchNoteNo <> ''
    IF logsth:DespatchNoteNo <> Ex_DespatchNoteNo
      ReturnValue = Record:Filtered
    END
  END
  
  IF Ex_Location <> ''
    IF logsth:Location <> Ex_Location
      ReturnValue = Record:Filtered
    END
  END
  
  IF Ex_ClubNokia <> ''
    IF logsth:ClubNokia <> Ex_ClubNokia
      ReturnValue = Record:Filtered
    END
  END
  
  IF Ex_End_Date <> ''
    IF logsth:Date > Ex_End_Date
      ReturnValue = Record:Filtered
    END
  END
  
  IF Ex_Status <> 'Y'
    IF logsth:Status = 'AVAILABLE'
      ReturnValue = Record:Filtered
    END
  END
  
  IF Ex_Transferred <> 'Y'
    IF logsth:Status = 'TRANSFERRED'
      ReturnValue = Record:Filtered
    END
  END
  
  IF Ex_Despatched <> 'Y'
    IF logsth:Status = 'DESPATCHED'
      ReturnValue = Record:Filtered
    END
  END
  
  
  
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(ValidateRecord, (),BYTE)
  RETURN ReturnValue

