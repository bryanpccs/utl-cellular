

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS039.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('LOGIS020.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS027.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS030.INC'),ONCE        !Req'd for module callout resolution
                     END


AllocateStock PROCEDURE                               !Generated from procedure template - Window

ThisThreadActive BYTE
tmp:insertype        BYTE,DIM(2)
tmp:recordnumber     LONG
tmp:recordnumber2    LONG
save_logasttmp_id    USHORT,AUTO
save_logast_id       USHORT,AUTO
save_logtmp_id       USHORT,AUTO
tmp:tolocation       STRING(30)
tmp:tosalescode      STRING(30)
tmp:todescription    STRING(30)
tmp:toclubnokia      STRING(30)
tmp:q_imported       QUEUE,PRE(tmpimp)
tmp:importedesn      STRING(30)
                     END
tmp:q_scanned        QUEUE,PRE(tmpscn)
tmp:scannedesn       STRING(30)
                     END
tmp:location         STRING(30)
tmp:salescode        STRING(30)
tmp:description      STRING(30)
tmp:RefNumber        LONG
tmp:esn              STRING(30)
tmp:importpath       STRING(255)
tmp:clubnokia        STRING(30)
tmp:desclubnokia     STRING(30)
tmp:totalstock       LONG
tmp:totalDespatch    LONG
tmp:refnumber2       LONG
tmp:smpfnumber       STRING(30)
tmp:DeliveryNote     STRING(30)
tmp:OrderRefNo       STRING(30)
tmp:modelnumber      STRING(30)
tmp:tomodelnumber    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Export_To_ServiceBase STRING(1)
tmp:AllocQty         REAL
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:location
lstl:Location          LIKE(lstl:Location)            !List box control field - type derived from field
lstl:RecordNumber      LIKE(lstl:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?tmp:clubnokia
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?tmp:desclubnokia
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(LOGSTLOC)
                       PROJECT(lstl:Location)
                       PROJECT(lstl:RecordNumber)
                     END
FDCB11::View:FileDropCombo VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
FDCB9::View:FileDropCombo VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
BRW5::View:Browse    VIEW(LOGASSST)
                       PROJECT(logast:Description)
                       PROJECT(logast:RecordNumber)
                       PROJECT(logast:RefNumber)
                       PROJECT(logast:PartNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
logast:Description     LIKE(logast:Description)       !List box control field - type derived from field
logast:RecordNumber    LIKE(logast:RecordNumber)      !Primary key field - type derived from field
logast:RefNumber       LIKE(logast:RefNumber)         !Browse key field - type derived from field
logast:PartNumber      LIKE(logast:PartNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(LOGSERST)
                       PROJECT(logser:ESN)
                       PROJECT(logser:RecordNumber)
                       PROJECT(logser:RefNumber)
                       PROJECT(logser:Status)
                       PROJECT(logser:ClubNokia)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
logser:ESN             LIKE(logser:ESN)               !List box control field - type derived from field
logser:RecordNumber    LIKE(logser:RecordNumber)      !Primary key field - type derived from field
logser:RefNumber       LIKE(logser:RefNumber)         !Browse key field - type derived from field
logser:Status          LIKE(logser:Status)            !Browse key field - type derived from field
logser:ClubNokia       LIKE(logser:ClubNokia)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW20::View:Browse   VIEW(LOGTEMP)
                       PROJECT(logtmp:IMEI)
                       PROJECT(logtmp:RefNumber)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
logtmp:IMEI            LIKE(logtmp:IMEI)              !List box control field - type derived from field
logtmp:RefNumber       LIKE(logtmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Wizard
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Wizard
mo:SelectedField  Long
window               WINDOW('Allocate Stock'),AT(,,456,280),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,220,244),USE(?Sheet2),SPREAD
                         TAB('From Location'),USE(?Tab2)
                           PROMPT('Sales Code'),AT(8,20),USE(?Prompt2:2)
                           ENTRY(@s30),AT(84,20,124,10),USE(tmp:salescode),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           BUTTON,AT(212,20,10,10),USE(?LookupSalesCode),SKIP,ICON('list3.ico')
                           COMBO(@s30),AT(84,68,124,10),USE(tmp:location),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Description'),AT(8,36),USE(?tmp:description:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           PROMPT('Associated Accessories'),AT(8,148,68,16),USE(?tmp:description:Prompt:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(tmp:description),SKIP,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Model Number'),AT(8,52),USE(?tmp:modelnumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(tmp:modelnumber),SKIP,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('FROM Club Nokia Site'),AT(8,84),USE(?tmp:description:Prompt:3),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           COMBO(@s30),AT(84,84,124,10),USE(tmp:clubnokia),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           PROMPT('TO Club Nokia Site'),AT(8,100,68,10),USE(?tmp:desclubnokia:prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           COMBO(@s30),AT(84,100,124,10),USE(tmp:desclubnokia),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                           PROMPT('SMPF Number'),AT(8,116),USE(?tmp:smpfnumber:Prompt),TRN,HIDE,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,116,124,10),USE(tmp:smpfnumber),HIDE,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Delivery Note No'),AT(8,132),USE(?tmp:DeliveryNote:Prompt),TRN,HIDE,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,132,124,10),USE(tmp:DeliveryNote),HIDE,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           LIST,AT(84,148,124,64),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse)
                           STRING('Allocate Quantity'),AT(9,220),USE(?String3)
                           SPIN(@n10),AT(84,220,44,12),USE(tmp:AllocQty),FONT(,,,FONT:bold)
                           PROMPT('Stock Location'),AT(8,68),USE(?Prompt2)
                         END
                       END
                       SHEET,AT(228,4,224,244),USE(?Sheet2:2),WIZARD,SPREAD
                         TAB('Tab 3'),USE(?Tab3)
                           PROMPT('Stock Items'),AT(232,8),USE(?Prompt6)
                           PROMPT('Allocated Items'),AT(344,8,51,10),USE(?despatchtitle)
                           STRING(@s8),AT(259,236),USE(tmp:totalstock),FONT(,,,FONT:bold)
                           PROMPT('Total: '),AT(344,236),USE(?Prompt10:2)
                           STRING(@s8),AT(376,236),USE(tmp:totalDespatch),FONT(,,,FONT:bold)
                           PROMPT('Total: '),AT(232,236),USE(?Prompt10)
                           LIST,AT(232,20,104,212),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~E.S.N. / I.M.E.I.~@s30@'),FROM(Queue:Browse:1)
                           LIST,AT(344,20,104,212),USE(?List:4),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~I.M.E.I.~@s30@'),FROM(Queue:Browse:3)
                         END
                       END
                       PANEL,AT(4,252,448,24),USE(?Panel1),FILL(COLOR:Silver)
                       STRING('ALLOCATION ONLY'),AT(9,257),USE(?String4),TRN,FONT(,14,COLOR:Red,FONT:bold)
                       BUTTON('&Finish'),AT(336,256,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                       BUTTON('Cancel'),AT(392,256,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB9                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW12                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ResetFromView          PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
BRW20                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
ResetFromView          PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW20::Sort0:Locator StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
IMEI        Routine
!    access:logtemp.clearkey(logtmp:imeikey)
!    logtmp:imei = tmp:esn
!    if access:logtemp.tryfetch(logtmp:imeikey) = Level:benign
!        Case MessageEx('The selected I.M.E.I. Number has already been entered.','ServiceBase 2000',|
!                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!        End!Case MessageEx
!        Case f_type
!            Of 'DESPATCH'
!                Select(?tmp:esn)
!            Else
!                Select(?tmp:esn:2)
!        End!Case f_type
!    Else!if access:logtemp.tryfetch(logtmp:imeikey) = Level:benign
!
!        access:logserst_alias.clearkey(logser_ali:nokiastatuskey)
!        logser_ali:refnumber = tmp:recordnumber
!        logser_ali:status    = 'AVAILABLE'
!        logser_ali:clubnokia = tmp:clubnokia
!        logser_ali:esn          = tmp:esn
!        if access:logserst_alias.tryfetch(logser_ali:nokiastatuskey)
!            If f_type = 'DESPATCH'
!                Case MessageEx('The selected I.M.E.I. is not available to be despatched.','ServiceBase 2000',|
!                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                    Of 1 ! &OK Button
!                End!Case MessageEx
!            Else!If f_type = 'DESPATCH'
!                Case MessageEx('The selected I.M.E.I. is not available to be transferred.','ServiceBase 2000',|
!                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                    Of 1 ! &OK Button
!                End!Case MessageEx
!            End!If f_type = 'DESPATCH'
!        Else!if access:logserst_alias.tryfetch(logser_ali:nokiastatuskey)
!            get(logtemp,0)
!            if access:logtemp.primerecord() = Level:Benign
!                logtmp:imei      = tmp:esn
!                if access:logtemp.insert()
!                    access:logtemp.cancelautoinc()
!                end
!            End!if access:logtemp.primerecord() = Level:Benign
!            tmp:esn = ''
!        End!if access:logserst_alias.tryfetch(logser_ali:nokiastatuskey)
!        Case f_type
!            Of 'DESPATCH'
!                Select(?tmp:esn)
!            Else
!                Select(?tmp:esn:2)
!        End!Case f_type
!        Display()
!    End!if access:logtemp.tryfetch(logtmp:imeikey) = Level:benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AllocateStock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    
      Loop SolaceDim1# = 1 to 2
        SolaceFieldName" = 'tmp:insertype' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:insertype[SolaceDim1#],'AllocateStock',1)
      End
    
    
    SolaceViewVars('tmp:recordnumber',tmp:recordnumber,'AllocateStock',1)
    SolaceViewVars('tmp:recordnumber2',tmp:recordnumber2,'AllocateStock',1)
    SolaceViewVars('save_logasttmp_id',save_logasttmp_id,'AllocateStock',1)
    SolaceViewVars('save_logast_id',save_logast_id,'AllocateStock',1)
    SolaceViewVars('save_logtmp_id',save_logtmp_id,'AllocateStock',1)
    SolaceViewVars('tmp:tolocation',tmp:tolocation,'AllocateStock',1)
    SolaceViewVars('tmp:tosalescode',tmp:tosalescode,'AllocateStock',1)
    SolaceViewVars('tmp:todescription',tmp:todescription,'AllocateStock',1)
    SolaceViewVars('tmp:toclubnokia',tmp:toclubnokia,'AllocateStock',1)
    SolaceViewVars('tmp:q_imported:tmp:importedesn',tmp:q_imported:tmp:importedesn,'AllocateStock',1)
    SolaceViewVars('tmp:q_scanned:tmp:scannedesn',tmp:q_scanned:tmp:scannedesn,'AllocateStock',1)
    SolaceViewVars('tmp:location',tmp:location,'AllocateStock',1)
    SolaceViewVars('tmp:salescode',tmp:salescode,'AllocateStock',1)
    SolaceViewVars('tmp:description',tmp:description,'AllocateStock',1)
    SolaceViewVars('tmp:RefNumber',tmp:RefNumber,'AllocateStock',1)
    SolaceViewVars('tmp:esn',tmp:esn,'AllocateStock',1)
    SolaceViewVars('tmp:importpath',tmp:importpath,'AllocateStock',1)
    SolaceViewVars('tmp:clubnokia',tmp:clubnokia,'AllocateStock',1)
    SolaceViewVars('tmp:desclubnokia',tmp:desclubnokia,'AllocateStock',1)
    SolaceViewVars('tmp:totalstock',tmp:totalstock,'AllocateStock',1)
    SolaceViewVars('tmp:totalDespatch',tmp:totalDespatch,'AllocateStock',1)
    SolaceViewVars('tmp:refnumber2',tmp:refnumber2,'AllocateStock',1)
    SolaceViewVars('tmp:smpfnumber',tmp:smpfnumber,'AllocateStock',1)
    SolaceViewVars('tmp:DeliveryNote',tmp:DeliveryNote,'AllocateStock',1)
    SolaceViewVars('tmp:OrderRefNo',tmp:OrderRefNo,'AllocateStock',1)
    SolaceViewVars('tmp:modelnumber',tmp:modelnumber,'AllocateStock',1)
    SolaceViewVars('tmp:tomodelnumber',tmp:tomodelnumber,'AllocateStock',1)
    SolaceViewVars('tmp:Export_To_ServiceBase',tmp:Export_To_ServiceBase,'AllocateStock',1)
    SolaceViewVars('tmp:AllocQty',tmp:AllocQty,'AllocateStock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:salescode;  SolaceCtrlName = '?tmp:salescode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupSalesCode;  SolaceCtrlName = '?LookupSalesCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location;  SolaceCtrlName = '?tmp:location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt;  SolaceCtrlName = '?tmp:description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt:2;  SolaceCtrlName = '?tmp:description:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description;  SolaceCtrlName = '?tmp:description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:modelnumber:Prompt;  SolaceCtrlName = '?tmp:modelnumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:modelnumber;  SolaceCtrlName = '?tmp:modelnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt:3;  SolaceCtrlName = '?tmp:description:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:clubnokia;  SolaceCtrlName = '?tmp:clubnokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:desclubnokia:prompt;  SolaceCtrlName = '?tmp:desclubnokia:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:desclubnokia;  SolaceCtrlName = '?tmp:desclubnokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:smpfnumber:Prompt;  SolaceCtrlName = '?tmp:smpfnumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:smpfnumber;  SolaceCtrlName = '?tmp:smpfnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DeliveryNote:Prompt;  SolaceCtrlName = '?tmp:DeliveryNote:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DeliveryNote;  SolaceCtrlName = '?tmp:DeliveryNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String3;  SolaceCtrlName = '?String3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AllocQty;  SolaceCtrlName = '?tmp:AllocQty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2:2;  SolaceCtrlName = '?Sheet2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatchtitle;  SolaceCtrlName = '?despatchtitle';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:totalstock;  SolaceCtrlName = '?tmp:totalstock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10:2;  SolaceCtrlName = '?Prompt10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:totalDespatch;  SolaceCtrlName = '?tmp:totalDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:4;  SolaceCtrlName = '?List:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String4;  SolaceCtrlName = '?String4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AllocateStock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'AllocateStock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='AllocateStock'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOGALLOC.Open
  Relate:LOGASSST.Open
  Relate:LOGCLSTE.Open
  Relate:LOGDEFLT.Open
  Relate:LOGEXCH.Open
  Relate:LOGSERST_ALIAS.Open
  Relate:LOGTEMP.Open
  Relate:STOCK.Open
  Access:LOGSTOCK.UseFile
  Access:LOGSERST.UseFile
  Access:LOGSTHIS.UseFile
  Access:STOHIST.UseFile
  Access:LOGDESNO.UseFile
  Access:USERS.UseFile
  Access:LOGEXHE.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:LOGASSST,SELF)
  BRW12.Init(?List:2,Queue:Browse:1.ViewPosition,BRW12::View:Browse,Queue:Browse:1,Relate:LOGSERST,SELF)
  BRW20.Init(?List:4,Queue:Browse:3.ViewPosition,BRW20::View:Browse,Queue:Browse:3,Relate:LOGTEMP,SELF)
  OPEN(window)
  SELF.Opened=True
  
      Unhide(?tmp:smpfnumber)
      Unhide(?tmp:smpfnumber:prompt)
      unhide(?tmp:DeliveryNote)
      unhide(?tmp:DeliveryNote:prompt)
  
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,logast:RefNumberKey)
  BRW5.AddRange(logast:RefNumber,tmp:RefNumber)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,logast:PartNumber,1,BRW5)
  BRW5.AddField(logast:Description,BRW5.Q.logast:Description)
  BRW5.AddField(logast:RecordNumber,BRW5.Q.logast:RecordNumber)
  BRW5.AddField(logast:RefNumber,BRW5.Q.logast:RefNumber)
  BRW5.AddField(logast:PartNumber,BRW5.Q.logast:PartNumber)
  BRW12.Q &= Queue:Browse:1
  BRW12.AddSortOrder(,logser:NokiaStatusKey)
  BRW12.AddRange(logser:RefNumber,tmp:recordnumber)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,logser:Status,1,BRW12)
  BRW12.SetFilter('(Upper(logser:Status) = ''AVAILABLE'' And Upper(logser:ClubNokia) = Upper(tmp:clubnokia))')
  BIND('tmp:clubnokia',tmp:clubnokia)
  BRW12.AddField(logser:ESN,BRW12.Q.logser:ESN)
  BRW12.AddField(logser:RecordNumber,BRW12.Q.logser:RecordNumber)
  BRW12.AddField(logser:RefNumber,BRW12.Q.logser:RefNumber)
  BRW12.AddField(logser:Status,BRW12.Q.logser:Status)
  BRW12.AddField(logser:ClubNokia,BRW12.Q.logser:ClubNokia)
  BRW20.Q &= Queue:Browse:3
  BRW20.AddSortOrder(,logtmp:IMEIKey)
  BRW20.AddLocator(BRW20::Sort0:Locator)
  BRW20::Sort0:Locator.Init(,logtmp:IMEI,1,BRW20)
  BRW20.AddField(logtmp:IMEI,BRW20.Q.logtmp:IMEI)
  BRW20.AddField(logtmp:RefNumber,BRW20.Q.logtmp:RefNumber)
  ThisMakeover.SetWindow(Win:Wizard)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,?Sheet2)
  window{prop:buffer} = 1
  FDCB3.Init(tmp:location,?tmp:location,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:LOGSTLOC,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(lstl:RefLocationKey)
  FDCB3.AddRange(lstl:RefNumber,tmp:RefNumber)
  FDCB3.AddField(lstl:Location,FDCB3.Q.lstl:Location)
  FDCB3.AddField(lstl:RecordNumber,FDCB3.Q.lstl:RecordNumber)
  FDCB3.AddUpdateField(lstl:Location,tmp:location)
  FDCB3.AddUpdateField(lstl:RecordNumber,tmp:recordnumber)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  FDCB11.Init(tmp:clubnokia,?tmp:clubnokia,Queue:FileDropCombo:2.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo:2,Relate:LOGCLSTE,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo:2
  FDCB11.AddSortOrder(logclu:ClubNokiaKey)
  FDCB11.AddField(logclu:ClubNokia,FDCB11.Q.logclu:ClubNokia)
  FDCB11.AddField(logclu:RecordNumber,FDCB11.Q.logclu:RecordNumber)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  FDCB9.Init(tmp:desclubnokia,?tmp:desclubnokia,Queue:FileDropCombo:3.ViewPosition,FDCB9::View:FileDropCombo,Queue:FileDropCombo:3,Relate:LOGCLSTE,ThisWindow,GlobalErrors,0,1,0)
  FDCB9.Q &= Queue:FileDropCombo:3
  FDCB9.AddSortOrder(logclu:ClubNokiaKey)
  FDCB9.AddField(logclu:ClubNokia,FDCB9.Q.logclu:ClubNokia)
  FDCB9.AddField(logclu:RecordNumber,FDCB9.Q.logclu:RecordNumber)
  ThisWindow.AddItem(FDCB9.WindowComponent)
  FDCB9.DefaultFill = 0
  BRW5.AddToolbarTarget(Toolbar)
  BRW20.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGALLOC.Close
    Relate:LOGASSST.Close
    Relate:LOGCLSTE.Close
    Relate:LOGDEFLT.Close
    Relate:LOGEXCH.Close
    Relate:LOGSERST_ALIAS.Close
    Relate:LOGTEMP.Close
    Relate:STOCK.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='AllocateStock'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  Remove(glo:file_name2)
  If Error()
      access:logtemp.close()
      Remove(glo:file_name2)
  End!If Error()
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'AllocateStock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:salescode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:salescode, Accepted)
      BRW5.ResetSort(1)
      BRW12.ResetSort(1)
      FDCB3.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:salescode, Accepted)
    OF ?LookupSalesCode
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickSalesCode
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSalesCode, Accepted)
      case globalresponse
          of requestcompleted
              tmp:salescode = logsto:salescode
              tmp:modelnumber    = logsto:modelnumber
              tmp:description = logsto:description
              tmp:refnumber   = logsto:refnumber
          of requestcancelled
      !        tmp:salescode = ''
              select(?-1)
      end!case globalreponse
      display(?)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSalesCode, Accepted)
    OF ?tmp:location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:location, Accepted)
      BRW5.ResetSort(1)
      BRW12.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:location, Accepted)
    OF ?tmp:clubnokia
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:clubnokia, Accepted)
      BRW12.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:clubnokia, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      error# = 0
      If tmp:refnumber = ''
          Case MessageEx('You must select a Valid Sales Code.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp:refnumber = ''
      
      If tmp:location = '' and error# = 0
          Case MessageEx('You must select a Stock Location.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp:location = ''
      
      If tmp:clubnokia = '' and error# = 0
          Case MessageEx('You must select a Club Nokia Site.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp:clubnokiasite = '' and error# = 0
      If error# = 0
        !Basicall "allocate" these to a parent record.
        Access:LogAlloc.PrimeRecord()
        LOG2:Date = TODAY()
        LOG2:UserCode = glo:Password
        LOG2:ModelNumber = tmp:modelnumber
        LOG2:Location    = tmp:location
        LOG2:SMPFNumber  = tmp:smpfnumber
        LOG2:DespatchNo  = tmp:DeliveryNote
        LOG2:SalesCode   = tmp:salescode
        LOG2:Description = tmp:description
        LOG2:From        = tmp:clubnokia
        LOG2:To_Site     = tmp:desclubnokia
        LOG2:Qty         = tmp:AllocQty
        Access:LogAlloc.Update()
        x# = 0
        !Right, now loop through and grab x# items
        Access:logserst.clearkey(logser:Status_Alloc_Key)
        logser:refnumber = tmp:recordnumber
        logser:clubnokia = tmp:clubnokia
        logser:status    = 'AVAILABLE'
        SET(logser:Status_Alloc_Key,logser:Status_Alloc_Key)
        LOOP
          IF Access:Logserst.Next()
            BREAK
          END
          IF logser:refnumber <> tmp:recordnumber
            BREAK
          END
          IF logser:clubnokia <> tmp:clubnokia
            BREAK
          END
          IF logser:status <> 'AVAILABLE'
            BREAK
          END
          IF x# = tmp:AllocQty
            BREAK
          END
          MESSAGE(x#)
          MESSAGE(tmp:AllocQty)
          logser:status    = 'ALLOCATED'
          IF Access:Logserst.Update()
            MESSAGE('ERROR')
          END
          logser:refnumber = tmp:recordnumber
          logser:clubnokia = tmp:clubnokia
          logser:status    = 'AVAILABLE'
         
          SET(logser:Status_Alloc_Key,logser:Status_Alloc_Key)
          x#+=1
        END
         WorkOutQty(tmp:refnumber)
                            !glo:select1 = tmp:desclubnokia
                            !LogDespatchNote(ldes:despatchno)
                            !glo:select1 = ''
                                    
      
       POST(Event:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'AllocateStock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Wizard,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet2
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,Field())
    OF ?Sheet2:2
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW12.ResetFromView PROCEDURE

tmp:totalstock:Cnt   LONG
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:LOGSERST.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:totalstock:Cnt += 1
  END
  tmp:totalstock = tmp:totalstock:Cnt
  PARENT.ResetFromView
  Relate:LOGSERST.SetQuickScan(0)
  SETCURSOR()


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW20.ResetFromView PROCEDURE

tmp:totalDespatch:Cnt LONG
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:LOGTEMP.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:totalDespatch:Cnt += 1
  END
  tmp:totalDespatch = tmp:totalDespatch:Cnt
  PARENT.ResetFromView
  Relate:LOGTEMP.SetQuickScan(0)
  SETCURSOR()


BRW20.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW20.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end

