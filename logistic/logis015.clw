

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS015.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('LOGIS027.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS030.INC'),ONCE        !Req'd for module callout resolution
                     END


ReceiveStock PROCEDURE                                !Generated from procedure template - Window

ThisThreadActive BYTE
tmp:insertype        BYTE,DIM(2)
save_logser_ali_id   USHORT,AUTO
tmp:q_imported       QUEUE,PRE(tmpimp)
tmp:importedesn      STRING(30)
                     END
tmp:q_scanned        QUEUE,PRE(tmpscn)
tmp:scannedesn       STRING(30)
                     END
tmp:location         STRING(30)
tmp:salescode        STRING(30)
tmp:description      STRING(30)
tmp:RefNumber        LONG
tmp:esn              STRING(30)
tmp:importpath       STRING(255)
tmp:clubnokia        STRING(30)
tmp:smpfnumber       STRING(30)
tmp:DeliveryNote     STRING(30)
tmp:ModelNumber      STRING(30)
tmp:qtyscanned       LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:location
logstl:Location        LIKE(logstl:Location)          !List box control field - type derived from field
logstl:RefNumber       LIKE(logstl:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?tmp:clubnokia
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(LOGSTOLC)
                       PROJECT(logstl:Location)
                       PROJECT(logstl:RefNumber)
                     END
FDCB11::View:FileDropCombo VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
BRW5::View:Browse    VIEW(LOGASSST)
                       PROJECT(logast:Description)
                       PROJECT(logast:RecordNumber)
                       PROJECT(logast:RefNumber)
                       PROJECT(logast:PartNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
logast:Description     LIKE(logast:Description)       !List box control field - type derived from field
logast:RecordNumber    LIKE(logast:RecordNumber)      !Primary key field - type derived from field
logast:RefNumber       LIKE(logast:RefNumber)         !Browse key field - type derived from field
logast:PartNumber      LIKE(logast:PartNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(LOGTEMP)
                       PROJECT(logtmp:IMEI)
                       PROJECT(logtmp:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
logtmp:IMEI            LIKE(logtmp:IMEI)              !List box control field - type derived from field
logtmp:IMEI_NormalFG   LONG                           !Normal forground color
logtmp:IMEI_NormalBG   LONG                           !Normal background color
logtmp:IMEI_SelectedFG LONG                           !Selected forground color
logtmp:IMEI_SelectedBG LONG                           !Selected background color
logtmp:RefNumber       LIKE(logtmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(LOG2TEMP)
                       PROJECT(lo2tmp:IMEI)
                       PROJECT(lo2tmp:RefNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
lo2tmp:IMEI            LIKE(lo2tmp:IMEI)              !List box control field - type derived from field
lo2tmp:RefNumber       LIKE(lo2tmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Wizard
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Wizard
mo:SelectedField  Long
window               WINDOW('Receiving Stock'),AT(,,455,279),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,448,28),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           CHECK('Manual'),AT(84,12),USE(tmp:insertype[1]),VALUE('1','0')
                           PROMPT('Input Type'),AT(8,12),USE(?Prompt1)
                           CHECK('Import'),AT(156,12),USE(tmp:insertype[2]),VALUE('1','0')
                           PROMPT('Import Path'),AT(228,12),USE(?tmp:importpath:Prompt),TRN,HIDE,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(276,12,124,10),USE(tmp:importpath),HIDE,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           BUTTON,AT(404,12,10,10),USE(?LookupPath),SKIP,HIDE,ICON('list3.ico')
                           BUTTON('Read'),AT(416,12,32,10),USE(?Read),HIDE
                         END
                       END
                       SHEET,AT(4,36,220,212),USE(?Sheet2),SPREAD
                         TAB('Manual Details'),USE(?Tab2)
                           PROMPT('Sales Code'),AT(8,52),USE(?Prompt2:2)
                           ENTRY(@s30),AT(84,52,124,10),USE(tmp:salescode),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupSalesCode),SKIP,ICON('list3.ico')
                           COMBO(@s30),AT(84,100,124,10),USE(tmp:location),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Description'),AT(8,68),USE(?tmp:description:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           PROMPT('Associated Stock'),AT(8,164),USE(?tmp:description:Prompt:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(tmp:description),SKIP,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Model Number'),AT(8,84),USE(?tmp:ModelNumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,124,10),USE(tmp:ModelNumber),SKIP,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Club Nokia Site'),AT(8,116),USE(?tmp:description:Prompt:3),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           COMBO(@s30),AT(84,116,124,10),USE(tmp:clubnokia),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           PROMPT('SMPF Number'),AT(8,132),USE(?tmp:smpfnumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,132,124,10),USE(tmp:smpfnumber),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Delivery Note'),AT(8,148),USE(?tmp:DeliveryNote:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,148,124,10),USE(tmp:DeliveryNote),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           LIST,AT(84,164,124,64),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse)
                           PROMPT('I.M.E.I. Number'),AT(8,232),USE(?tmp:esn:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,232,124,10),USE(tmp:esn),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Stock Location'),AT(8,100),USE(?Prompt2)
                         END
                       END
                       SHEET,AT(228,36,224,212),USE(?Sheet2:2),WIZARD,SPREAD
                         TAB('Tab 3'),USE(?Tab3)
                           PROMPT('Imported I.M.E.I. Numbers'),AT(232,40),USE(?Prompt6)
                           PROMPT('Scanned I.M.E.I. Numbers'),AT(344,40,84,12),USE(?Prompt6:2)
                           LIST,AT(344,52,104,180),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M*~I.M.E.I.~@s30@'),FROM(Queue:Browse:1)
                           STRING('Total:'),AT(344,236),USE(?String1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(368,236),USE(tmp:qtyscanned),FONT(,,,FONT:bold,CHARSET:ANSI)
                           LIST,AT(232,52,104,180),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~I.M.E.I.~@s30@'),FROM(Queue:Browse:2)
                         END
                       END
                       PANEL,AT(4,252,448,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('ESN-Model'),AT(8,256,64,16),USE(?Button6),LEFT,ICON('NWRENCH.ICO')
                       BUTTON('Finish'),AT(336,256,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                       BUTTON('Cancel'),AT(392,256,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW12                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
BRW14                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
FileLookup9          SelectFileClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ReceiveStock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    
      Loop SolaceDim1# = 1 to 2
        SolaceFieldName" = 'tmp:insertype' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:insertype[SolaceDim1#],'ReceiveStock',1)
      End
    
    
    SolaceViewVars('save_logser_ali_id',save_logser_ali_id,'ReceiveStock',1)
    SolaceViewVars('tmp:q_imported:tmp:importedesn',tmp:q_imported:tmp:importedesn,'ReceiveStock',1)
    SolaceViewVars('tmp:q_scanned:tmp:scannedesn',tmp:q_scanned:tmp:scannedesn,'ReceiveStock',1)
    SolaceViewVars('tmp:location',tmp:location,'ReceiveStock',1)
    SolaceViewVars('tmp:salescode',tmp:salescode,'ReceiveStock',1)
    SolaceViewVars('tmp:description',tmp:description,'ReceiveStock',1)
    SolaceViewVars('tmp:RefNumber',tmp:RefNumber,'ReceiveStock',1)
    SolaceViewVars('tmp:esn',tmp:esn,'ReceiveStock',1)
    SolaceViewVars('tmp:importpath',tmp:importpath,'ReceiveStock',1)
    SolaceViewVars('tmp:clubnokia',tmp:clubnokia,'ReceiveStock',1)
    SolaceViewVars('tmp:smpfnumber',tmp:smpfnumber,'ReceiveStock',1)
    SolaceViewVars('tmp:DeliveryNote',tmp:DeliveryNote,'ReceiveStock',1)
    SolaceViewVars('tmp:ModelNumber',tmp:ModelNumber,'ReceiveStock',1)
    SolaceViewVars('tmp:qtyscanned',tmp:qtyscanned,'ReceiveStock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:insertype_1;  SolaceCtrlName = '?tmp:insertype_1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:insertype_2;  SolaceCtrlName = '?tmp:insertype_2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:importpath:Prompt;  SolaceCtrlName = '?tmp:importpath:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:importpath;  SolaceCtrlName = '?tmp:importpath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupPath;  SolaceCtrlName = '?LookupPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Read;  SolaceCtrlName = '?Read';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:salescode;  SolaceCtrlName = '?tmp:salescode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupSalesCode;  SolaceCtrlName = '?LookupSalesCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location;  SolaceCtrlName = '?tmp:location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt;  SolaceCtrlName = '?tmp:description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt:2;  SolaceCtrlName = '?tmp:description:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description;  SolaceCtrlName = '?tmp:description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber:Prompt;  SolaceCtrlName = '?tmp:ModelNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber;  SolaceCtrlName = '?tmp:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt:3;  SolaceCtrlName = '?tmp:description:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:clubnokia;  SolaceCtrlName = '?tmp:clubnokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:smpfnumber:Prompt;  SolaceCtrlName = '?tmp:smpfnumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:smpfnumber;  SolaceCtrlName = '?tmp:smpfnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DeliveryNote:Prompt;  SolaceCtrlName = '?tmp:DeliveryNote:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DeliveryNote;  SolaceCtrlName = '?tmp:DeliveryNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn:Prompt;  SolaceCtrlName = '?tmp:esn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn;  SolaceCtrlName = '?tmp:esn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2:2;  SolaceCtrlName = '?Sheet2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:2;  SolaceCtrlName = '?Prompt6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:qtyscanned;  SolaceCtrlName = '?tmp:qtyscanned';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button6;  SolaceCtrlName = '?Button6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ReceiveStock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ReceiveStock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:insertype_1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='ReceiveStock'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ESNMODEL.Open
  Relate:LOG2TEMP.Open
  Relate:LOGASSST.Open
  Relate:LOGCLSTE.Open
  Relate:LOGSERST_ALIAS.Open
  Relate:LOGSTOCK_ALIAS.Open
  Relate:LOGSTOLC.Open
  Relate:LOGTEMP.Open
  Relate:MODELNUM.Open
  Access:LOGSTOCK.UseFile
  Access:LOGSERST.UseFile
  Access:LOGSTHII.UseFile
  Access:LOGSTHIS.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  tmp:insertype[1] = 1
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:LOGASSST,SELF)
  BRW12.Init(?List:2,Queue:Browse:1.ViewPosition,BRW12::View:Browse,Queue:Browse:1,Relate:LOGTEMP,SELF)
  BRW14.Init(?List:3,Queue:Browse:2.ViewPosition,BRW14::View:Browse,Queue:Browse:2,Relate:LOG2TEMP,SELF)
  OPEN(window)
  SELF.Opened=True
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,logast:RefNumberKey)
  BRW5.AddRange(logast:RefNumber,tmp:RefNumber)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,logast:PartNumber,1,BRW5)
  BRW5.AddField(logast:Description,BRW5.Q.logast:Description)
  BRW5.AddField(logast:RecordNumber,BRW5.Q.logast:RecordNumber)
  BRW5.AddField(logast:RefNumber,BRW5.Q.logast:RefNumber)
  BRW5.AddField(logast:PartNumber,BRW5.Q.logast:PartNumber)
  BRW12.Q &= Queue:Browse:1
  BRW12.AddSortOrder(,logtmp:IMEIKey)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,logtmp:IMEI,1,BRW12)
  BRW12.AddField(logtmp:IMEI,BRW12.Q.logtmp:IMEI)
  BRW12.AddField(logtmp:RefNumber,BRW12.Q.logtmp:RefNumber)
  BRW14.Q &= Queue:Browse:2
  BRW14.AddSortOrder(,lo2tmp:IMEIKey)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,lo2tmp:IMEI,1,BRW14)
  BRW14.SetFilter('(lo2tmp:marker <<> 1)')
  BRW14.AddField(lo2tmp:IMEI,BRW14.Q.lo2tmp:IMEI)
  BRW14.AddField(lo2tmp:RefNumber,BRW14.Q.lo2tmp:RefNumber)
  ThisMakeover.SetWindow(Win:Wizard)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,?Sheet1)
  window{prop:buffer} = 1
  IF ?tmp:insertype_1{Prop:Checked} = True
    tmp:insertype[2] = 0
  END
  IF ?tmp:insertype_2{Prop:Checked} = True
    tmp:insertype[1] = 0
    UNHIDE(?tmp:importpath:Prompt)
    UNHIDE(?tmp:importpath)
    UNHIDE(?LookupPath)
    UNHIDE(?Read)
  END
  IF ?tmp:insertype_2{Prop:Checked} = False
    HIDE(?tmp:importpath:Prompt)
    HIDE(?tmp:importpath)
    HIDE(?LookupPath)
    HIDE(?Read)
  END
  FDCB3.Init(tmp:location,?tmp:location,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:LOGSTOLC,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(logstl:LocationKey)
  FDCB3.AddField(logstl:Location,FDCB3.Q.logstl:Location)
  FDCB3.AddField(logstl:RefNumber,FDCB3.Q.logstl:RefNumber)
  FDCB3.AddUpdateField(logstl:Location,tmp:location)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  FDCB11.Init(tmp:clubnokia,?tmp:clubnokia,Queue:FileDropCombo:2.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo:2,Relate:LOGCLSTE,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo:2
  FDCB11.AddSortOrder(logclu:ClubNokiaKey)
  FDCB11.AddField(logclu:ClubNokia,FDCB11.Q.logclu:ClubNokia)
  FDCB11.AddField(logclu:RecordNumber,FDCB11.Q.logclu:RecordNumber)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  FileLookup9.Init
  FileLookup9.Flags=BOR(FileLookup9.Flags,FILE:LongName)
  FileLookup9.SetMask('Text Files','*.TXT')
  FileLookup9.AddMask('All Files','*.*')
  FileLookup9.WindowTitle='Import File'
  BRW14.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESNMODEL.Close
    Relate:LOG2TEMP.Close
    Relate:LOGASSST.Close
    Relate:LOGCLSTE.Close
    Relate:LOGSERST_ALIAS.Close
    Relate:LOGSTOCK_ALIAS.Close
    Relate:LOGSTOLC.Close
    Relate:LOGTEMP.Close
    Relate:MODELNUM.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='ReceiveStock'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ReceiveStock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  Remove(glo:file_name2)
  If error()
      access:logtemp.close()
      Remove(glo:file_name2)
      glo:file_name2  = ''
  End!If error()
  Remove(glo:file_name3)
  If error()
      access:logtemp.close()
      Remove(glo:file_name3)
      glo:file_name3  = ''
  End!If error()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:insertype_1
      IF ?tmp:insertype_1{Prop:Checked} = True
        tmp:insertype[2] = 0
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:insertype_1, Accepted)
      Post(Event:Accepted,?tmp:insertype_2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:insertype_1, Accepted)
    OF ?tmp:insertype_2
      IF ?tmp:insertype_2{Prop:Checked} = True
        tmp:insertype[1] = 0
        UNHIDE(?tmp:importpath:Prompt)
        UNHIDE(?tmp:importpath)
        UNHIDE(?LookupPath)
        UNHIDE(?Read)
      END
      IF ?tmp:insertype_2{Prop:Checked} = False
        HIDE(?tmp:importpath:Prompt)
        HIDE(?tmp:importpath)
        HIDE(?LookupPath)
        HIDE(?Read)
      END
      ThisWindow.Reset
    OF ?LookupPath
      ThisWindow.Update
      tmp:importpath = Upper(FileLookup9.Ask(1)  )
      DISPLAY
    OF ?Read
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Read, Accepted)
      If tmp:importpath = ''
      
      Else!If tmp:importpath = ''
          glo:file_name   = tmp:importpath
          access:expgen.open()
          access:expgen.usefile()
      
          setcursor(cursor:wait)
          Clear(expgen)
          Set(expgen,0)
          Loop
              If access:expgen.next()
                  Break
              End!If access:expgen.next()
              get(log2temp,0)
              if access:log2temp.primerecord() = Level:Benign
                  lo2tmp:imei      = Sub(gen:line1,11,15)
                  if access:log2temp.insert()
                      access:log2temp.cancelautoinc()
                  end
              End!if access:log2temp.primerecord() = Level:Benign
          End!Loop
          setcursor()
      
          Case MessageEx('File Read.','ServiceBase 2000',|
                         'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          access:expgen.close()
      End!If tmp:importpath = ''
      BRW14.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Read, Accepted)
    OF ?tmp:salescode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:salescode, Accepted)
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:salescode, Accepted)
    OF ?LookupSalesCode
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickSalesCode
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSalesCode, Accepted)
      case globalresponse
          of requestcompleted
              tmp:salescode = logsto:salescode
              tmp:modelnumber    = logsto:modelnumber
              tmp:description = logsto:description
              tmp:refnumber   = logsto:refnumber
      !        select(?+2)
          of requestcancelled
      !        tmp:salescode = ''
              select(?-1)
      end!case globalreponse
      display(?)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSalesCode, Accepted)
    OF ?tmp:location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:location, Accepted)
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:location, Accepted)
    OF ?tmp:esn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
      error# = 0
      IF tmp:smpfnumber = ''
        Case MessageEx('Please insert an SMPF number','ServiceBase 2000',|
                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
          Of 1 ! &OK Button
            Error# = 1
        End!Case MessageEx
      END
      
      IF tmp:DeliveryNote = ''
        Case MessageEx('Please insert aa Delivery Note number','ServiceBase 2000',|
                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
          Of 1 ! &OK Button
            Error# = 1
        End!Case MessageEx
      END
      
      
      access:logstock.clearkey(logsto:refnumberkey)
      logsto:refnumber = tmp:refnumber
      if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
          access:esnmodel.clearkey(esn:esn_key)
          esn:esn          = tmp:esn
          esn:model_number = logsto:modelnumber
          if access:esnmodel.tryfetch(esn:esn_key)
              error# = 1
              Case MessageEx('The selected I.M.E.I. Number does not match the Model Number of this Sales Code.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
          End!if access:esnmodel.tryfetch(esn:esn_key)
      End!if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
      If error# = 1
          Select(?tmp:esn)
      Else!If error# = 1
          access:logtemp.clearkey(logtmp:imeikey)
          logtmp:imei = tmp:esn
          if access:logtemp.tryfetch(logtmp:imeikey) = Level:benign
              Case MessageEx('The selected I.M.E.I. Number has already been entered.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Select(?tmp:esn)
          Else!if access:logtemp.tryfetch(logtmp:imeikey) = Level:benign
          
      
              If CheckLength('IMEI',logsto:modelnumber,tmp:esn)
                  Select(?tmp:esn)
              Else!If CheckLength('IMEI',logsto:modelnumber,tmp:esn)
                  access:logtemp.clearkey(logtmp:imeiKey)
                  logtmp:imei = tmp:esn
                  If access:logtemp.tryfetch(logtmp:imeikey) = Level:Benign
                      Case MessageEx('You are already scanned in this I.M.E.I. number.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  Else!If ~Error()
                  !Loop through serial stock
                      save_logser_ali_id = access:logserst_alias.savefile()
                      access:logserst_alias.clearkey(logser_ali:esnkey)
                      logser_ali:esn = tmp:esn
                      set(logser_ali:esnkey,logser_ali:esnkey)
                      loop
                          if access:logserst_alias.next()
                             break
                          end !if
                          if logser_ali:esn <> tmp:esn      |
                              then break.  ! end if
      
                  !Get SalesCode - Stock Location
                          access:logstloc.clearkey(lstl:recordnumberkey)
                          lstl:recordnumber = logser_ali:refnumber
                          if access:logstloc.tryfetch(lstl:recordnumberkey) = Level:benign
                  !Get SalesCode
                              access:logstock_alias.clearkey(logsto_ali:refnumberkey)
                              logsto_ali:refnumber = lstl:refnumber
                              if access:logstock_alias.tryfetch(logsto_ali:refnumberkey) = Level:Benign
                                  Case MessageEx('The select I.M.E.I. number has already been entered at:<13,10><13,10>Stock Location: '&lstl:location&'<13,10>Sales Code: '&logsto_ali:salescode&'<13,10>Club Nokia Site: '&logser_ali:clubnokia&' .','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                                  error# = 1
                                  Break
                              End!if access:logstock_alias.tryfetch(logsto_ali:refnumberkey) = Level:Benign
      
                          End!if access:logstloc.tryfetch(lstl:recordnumberkey) = Level:benign
      
                      end !loop
                      access:logserst_alias.restorefile(save_logser_ali_id)
                  End!If ~Error()
      
                  If error# = 0
                      Get(logtemp,0)
                      If access:logtemp.primerecord() = Level:Benign
                          access:log2temp.clearkey(lo2tmp:imeikey)
                          lo2tmp:imei = tmp:esn
                          if access:log2temp.tryfetch(lo2tmp:imeikey)
                              logtmp:marker = 1
                          Else!if access:log2temp.tryfetch(lo2tmp:imeikey)
                              lo2tmp:marker = 1
                              access:log2temp.update()
                          End!if access:log2temp.tryfetch(lo2tmp:imeikey)
                          logtmp:imei = tmp:esn
                          access:logtemp.insert()
                      End!If access:logtemp.primerecord() = Level:Benign
                      tmp:esn = ''
                  End!If error# = 0
                  Select(?tmp:esn)
                  Display()
      
              End!If CheckLength('IMEI',logsto:modelnumber,tmp:esn)
          End!if access:logtemp.tryfetch(logtmp:imeikey) = Level:benign
      End!If error# = 0
      BRW12.ResetSort(1)
      BRW14.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
    OF ?Button6
      ThisWindow.Update
      START(Browse_ESN_Model, 25000)
      ThisWindow.Reset
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      If tmp:refnumber = ''
          Case MessageEx('You must select a valid Sales Code.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp;refnumber = ''
      If tmp:location = '' and error# = 0
          Case MessageEx('You must select a Stock Location.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp:location = '' and error# = 0
      If tmp:clubnokia = '' and error# = 0
          Case MessageEx('You must select a Club Nokia Site.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp:clubnokia = '' and error# = 0
      If tmp:smpfnumber = '' and error# = 0
          Case MessageEx('You must insert a SMPF Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp:smpfnumber = '' and error# = 0
      If tmp:deliverynote = '' and error# = 0
          Case MessageEx('You must insert a Delivery Note.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp:deliverynote = '' and error# = 0
      
      If error# = 0
          Case MessageEx('Are you sure you want to finish?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  If ~Records(logtemp)
                      Case MessageEx('You have not select to receive any units.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If ~Records(logtemp)
                      Get(logsthis,0)
                      access:logstock.clearkey(logsto:refnumberkey)
                      logsto:refnumber = tmp:refnumber
                      if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
                          access:logstloc.clearkey(lstl:reflocationkey)
                          lstl:refnumber = logsto:refnumber
                          lstl:location  = tmp:location
                          if access:logstloc.tryfetch(lstl:reflocationkey) = Level:Benign
                              get(logsthis,0)
                              if access:logsthis.primerecord() = Level:Benign
      
                                  access:logtemp.clearkey(logtmp:imeikey)
                                  Set(logtmp:imeikey,logtmp:imeikey)
                                  Loop
                                      If access:logtemp.next()
                                          Break
                                      End!If access:logtemp.next()
                                      get(logserst,0)
                                      if access:logserst.primerecord() = Level:Benign
                                          logser:refnumber    = lstl:recordnumber
                                          logser:esn          = logtmp:imei
                                          logser:status       = 'AVAILABLE'
                                          logser:clubnokia    = tmp:clubnokia
      
                                          if access:logserst.insert()
                                              access:logserst.cancelautoinc()
                                          end
                                      End!if access:logserst.primerecord() = Level:Benign
      
                                      Get(logsthii,0)
                                      If access:logsthii.primerecord() = Level:Benign
                                          logsti:RefNumber    = logsth:RecordNumber
                                          logsti:IMEI         = logtmp:imei
                                          If access:logsthii.tryinsert()
                                              access:logsthii.cancelautoinc()
                                          End!If access:logsthii.tryinsert()
                                      End!If access:logsthii.primerecord() = Level:Benign
                                  End!Loop
      
                                  logsth:refnumber      = tmp:refnumber
                                  logsth:stockin        = Records(logtemp)
                                  logsth:status         = 'AVAILABLE'
                                  logsth:clubnokia      = tmp:clubnokia
                                  logsth:smpfnumber     = tmp:smpfnumber
                                  logsth:despatchnoteno = tmp:deliverynote
                                  logsth:modelnumber    = logsto:ModelNumber
                                  logsth:location       = tmp:location
                                  logsth:date           = Today()
                                  access:users.clearkey(use:password_key)
                                  use:password    = glo:password
                                  access:users.tryfetch(use:password_key)
                                  logsth:usercode     = use:user_code
                                  if access:logsthis.insert()
                                      access:logsthis.cancelautoinc()
                                  end
                              End!if access:logsthis.primerecord() = Level:Benign
      
                          End!if access:logstloc.tryfetch(lstl:reflocationkey) = Level:Benign
                      End!if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
      
                      WorkOutQty(tmp:RefNumber)
      
                      If tmp:insertype[2] = 1
                          ExcessReport('EXCESS')
                      End!If tmp:insertype =
                      Post(Event:closewindow)
      
                  End!If Records(logtemp)
              Of 2 ! &No Button
          End!Case MessageEx
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ReceiveStock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Wizard,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,Field())
    OF ?Sheet2
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,Field())
    OF ?Sheet2:2
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW12.ResetFromView PROCEDURE

tmp:qtyscanned:Cnt   LONG
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:LOGTEMP.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:qtyscanned:Cnt += 1
  END
  tmp:qtyscanned = tmp:qtyscanned:Cnt
  PARENT.ResetFromView
  Relate:LOGTEMP.SetQuickScan(0)
  SETCURSOR()


BRW12.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.logtmp:IMEI_NormalFG = -1
  SELF.Q.logtmp:IMEI_NormalBG = -1
  SELF.Q.logtmp:IMEI_SelectedFG = -1
  SELF.Q.logtmp:IMEI_SelectedBG = -1


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end

