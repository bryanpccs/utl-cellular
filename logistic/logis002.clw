

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABASCII.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS002.INC'),ONCE        !Local module procedure declarations
                     END


Start_Screen PROCEDURE                                !Generated from procedure template - Window

LocalRequest         LONG
OriginalRequest      LONG
LocalResponse        LONG
FilesOpened          BYTE
WindowOpened         LONG
WindowInitialized    LONG
ForceRefresh         LONG
ASCIIFileSize        LONG
ASCIIBytesThisRead   LONG
ASCIIBytesRead       LONG
ASCIIBytesThisCycle  LONG
ASCIIPercentProgress BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ViewerActive1        BYTE(False)
AsciiFilename1       STRING(FILE:MaxFilePath),AUTO,STATIC,THREAD
AsciiFile1           FILE,DRIVER('ASCII'),NAME(AsciiFilename1),PRE(A1),THREAD
RECORD                RECORD,PRE()
TextLine                STRING(255)
                      END
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Other3
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Other3
mo:SelectedField  Long
window               WINDOW,AT(,,292,270),FONT('Arial',8,,),COLOR(COLOR:White),CENTER,IMM,ALRT(AltShiftF12),TIMER(100),GRAY,DOUBLE
                       IMAGE('loglti2k.jpg'),AT(4,4,284,80),USE(?Image3)
                       LIST,AT(4,88,284,148),USE(?List),IMM,SKIP,NOBAR,VSCROLL,COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),FROM(''),GRID(COLOR:Silver)
                       STRING('Click ''OK'' to assent to the aforementioned conditions.'),AT(61,240),USE(?OKText),TRN,CENTER
                       BUTTON('&OK'),AT(61,252,56,16),USE(?Ok),FLAT,LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(173,252,56,16),USE(?Cancel),FLAT,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       STRING('DEBUG VERSION'),AT(4,88,284,148),USE(?warning_text),HIDE,FONT(,16,COLOR:Red,FONT:bold),ANGLE(300)
                       PANEL,AT(4,4,284,80),USE(?Panel1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

Viewer1              AsciiViewerClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Start_Screen',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Start_Screen',1)
    SolaceViewVars('OriginalRequest',OriginalRequest,'Start_Screen',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Start_Screen',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Start_Screen',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Start_Screen',1)
    SolaceViewVars('WindowInitialized',WindowInitialized,'Start_Screen',1)
    SolaceViewVars('ForceRefresh',ForceRefresh,'Start_Screen',1)
    SolaceViewVars('ASCIIFileSize',ASCIIFileSize,'Start_Screen',1)
    SolaceViewVars('ASCIIBytesThisRead',ASCIIBytesThisRead,'Start_Screen',1)
    SolaceViewVars('ASCIIBytesRead',ASCIIBytesRead,'Start_Screen',1)
    SolaceViewVars('ASCIIBytesThisCycle',ASCIIBytesThisCycle,'Start_Screen',1)
    SolaceViewVars('ASCIIPercentProgress',ASCIIPercentProgress,'Start_Screen',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Image3;  SolaceCtrlName = '?Image3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OKText;  SolaceCtrlName = '?OKText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ok;  SolaceCtrlName = '?Ok';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?warning_text;  SolaceCtrlName = '?warning_text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Start_Screen')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Start_Screen')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCESDEF.Open
  Relate:ACTION.Open
  Relate:CITYSERV.Open
  Relate:COLOUR.Open
  Relate:COMMCAT.Open
  Relate:COMMONCP_ALIAS.Open
  Relate:COMMONFA_ALIAS.Open
  Relate:COMMONWP_ALIAS.Open
  Relate:CONSIGN.Open
  Relate:CONTACTS.Open
  Relate:DEFAULTS.Open
  Relate:DEFEDI.Open
  Relate:DEFEDI2.Open
  Relate:DEFEPS.Open
  Relate:DEFPRINT.Open
  Relate:DEFRAPID.Open
  Relate:DEFSTOCK.Open
  Relate:DEFWEB.Open
  Relate:DESBATCH.Open
  Relate:DISCOUNT.Open
  Relate:EDIBATCH.Open
  Relate:ESNMODEL.Open
  Relate:EXCAUDIT.Open
  Relate:EXCCHRGE.Open
  Relate:INVPARTS.Open
  Relate:JOBBATCH.Open
  Relate:JOBEXACC.Open
  Relate:JOBPAYMT.Open
  Relate:JOBSVODA.Open
  Relate:LETTERS.Open
  Relate:LOAN_ALIAS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:LOGASSST.Open
  Relate:LOGCLSTE.Open
  Relate:LOGGED.Open
  Relate:LOGRETRN.Open
  Relate:LOGSALCD.Open
  Relate:LOGSTOLC.Open
  Relate:MERGE.Open
  Relate:MERGELET.Open
  Relate:MESSAGES.Open
  Relate:NEWFEAT.Open
  Relate:NOTESENG.Open
  Relate:NOTESINV.Open
  Relate:ORDPARTS_ALIAS.Open
  Relate:ORDPEND_ALIAS.Open
  Relate:PARTS_ALIAS.Open
  Relate:PAYTYPES.Open
  Relate:PROCCODE.Open
  Relate:PROINV.Open
  Relate:QAREASON.Open
  Relate:QUERYREA.Open
  Relate:REPTYDEF.Open
  Relate:RETDESNO.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:STAHEAD.Open
  Relate:STANTEXT.Open
  Relate:STDCHRGE_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WARPARTS_ALIAS.Open
  Access:ACCESSOR.UseFile
  Access:ALLLEVEL.UseFile
  Access:AUDIT.UseFile
  Access:BOUNCER.UseFile
  Access:CHARTYPE.UseFile
  Access:COMMONCP.UseFile
  Access:COMMONFA.UseFile
  Access:COMMONWP.UseFile
  Access:CONTHIST.UseFile
  Access:COURIER.UseFile
  Access:DEFCHRGE.UseFile
  Access:ESTPARTS.UseFile
  Access:EXCHACC.UseFile
  Access:EXCHANGE.UseFile
  Access:EXCHHIST.UseFile
  Access:INVOICE.UseFile
  Access:JOBACC.UseFile
  Access:JOBLOHIS.UseFile
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBVODAC.UseFile
  Access:LOAN.UseFile
  Access:LOANACC.UseFile
  Access:LOANHIST.UseFile
  Access:LOCATION.UseFile
  Access:LOCINTER.UseFile
  Access:LOCSHELF.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAUPA.UseFile
  Access:MANFPALO.UseFile
  Access:MANUFACT.UseFile
  Access:MODELCOL.UseFile
  Access:MODELNUM.UseFile
  Access:NOTESFAU.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:PARTS.UseFile
  Access:PRIORITY.UseFile
  Access:REPAIRTY.UseFile
  Access:STATUS.UseFile
  Access:STDCHRGE.UseFile
  Access:STOCK.UseFile
  Access:STOCKTYP.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  Access:SUBCHRGE.UseFile
  Access:SUBTRACC.UseFile
  Access:SUPPLIER.UseFile
  Access:TEAMS.UseFile
  Access:TRACHAR.UseFile
  Access:TRACHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:TRDMODEL.UseFile
  Access:TRDPARTY.UseFile
  Access:TRDSPEC.UseFile
  Access:UNITTYPE.UseFile
  Access:USELEVEL.UseFile
  Access:USERS.UseFile
  Access:USMASSIG.UseFile
  Access:USUASSIG.UseFile
  Access:WARPARTS.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBEXHIS.UseFile
  Access:RETPAY.UseFile
  Access:RETSALES.UseFile
  Access:RETSTOCK.UseFile
  Access:STOESN.UseFile
  Access:TRDBATCH.UseFile
  Access:JOBNOTES.UseFile
  Access:MERGETXT.UseFile
  Access:LOGSERST.UseFile
  Access:LOGSTHIS.UseFile
  Access:LOGSTOCK.UseFile
  Access:LOGSTHII.UseFile
  Access:LOGRTHIS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  glo:select1 = 'CANCEL'
  ?List{prop:vcr} = TRUE
  ?List{prop:vcr} = False
  ThisMakeover.SetWindow(Win:Other3)
  window{prop:buffer} = 1
  AsciiFilename1='Cellular.txt'
  ViewerActive1=Viewer1.Init(AsciiFile1,A1:Textline,AsciiFilename1,?List,GlobalErrors,EnablePrint)
  IF ~ViewerActive1 THEN RETURN Level:Fatal.
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCESDEF.Close
    Relate:ACTION.Close
    Relate:CITYSERV.Close
    Relate:COLOUR.Close
    Relate:COMMCAT.Close
    Relate:COMMONCP_ALIAS.Close
    Relate:COMMONFA_ALIAS.Close
    Relate:COMMONWP_ALIAS.Close
    Relate:CONSIGN.Close
    Relate:CONTACTS.Close
    Relate:DEFAULTS.Close
    Relate:DEFEDI.Close
    Relate:DEFEDI2.Close
    Relate:DEFEPS.Close
    Relate:DEFPRINT.Close
    Relate:DEFRAPID.Close
    Relate:DEFSTOCK.Close
    Relate:DEFWEB.Close
    Relate:DESBATCH.Close
    Relate:DISCOUNT.Close
    Relate:EDIBATCH.Close
    Relate:ESNMODEL.Close
    Relate:EXCAUDIT.Close
    Relate:EXCCHRGE.Close
    Relate:INVPARTS.Close
    Relate:JOBBATCH.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:JOBSVODA.Close
    Relate:LETTERS.Close
    Relate:LOAN_ALIAS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:LOGASSST.Close
    Relate:LOGCLSTE.Close
    Relate:LOGGED.Close
    Relate:LOGRETRN.Close
    Relate:LOGSALCD.Close
    Relate:LOGSTOLC.Close
    Relate:MERGE.Close
    Relate:MERGELET.Close
    Relate:MESSAGES.Close
    Relate:NEWFEAT.Close
    Relate:NOTESENG.Close
    Relate:NOTESINV.Close
    Relate:ORDPARTS_ALIAS.Close
    Relate:ORDPEND_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:PAYTYPES.Close
    Relate:PROCCODE.Close
    Relate:PROINV.Close
    Relate:QAREASON.Close
    Relate:QUERYREA.Close
    Relate:REPTYDEF.Close
    Relate:RETDESNO.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:STAHEAD.Close
    Relate:STANTEXT.Close
    Relate:STDCHRGE_ALIAS.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WARPARTS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Start_Screen',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF window{Prop:AcceptAll} THEN RETURN.
  IF ViewerActive1 THEN Viewer1.TakeEvent(EVENT()).
  PARENT.Reset(Force)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Ok
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
      glo:select1 = 'OK'
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Start_Screen')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Other3,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?List
    IF ViewerActive1
      IF Viewer1.TakeEvent(EVENT())=Level:Notify THEN CYCLE.
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      IF ViewerActive1
        Viewer1.Kill
        ViewerActive1=False
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      If KeyCode() = AltShiftF12
        MessageEx('ACCAREAS: ' & Format((Size(acc:record)/1024),@n6.2) & '<13,10>' & |
                  'ACCESDEF: ' & Format((Size(ACD:record)/1024),@n6.2) & '<13,10>' & |
                  'ACCESSOR: ' & Format((Size(ACR:record)/1024),@n6.2) & '<13,10>' & |
                  'ACTION:   ' & Format((Size(ACT:record)/1024),@n6.2) & '<13,10>' & |
                  'ALLLEVEL: ' & Format((Size(ALL:record)/1024),@n6.2) & '<13,10>' & |
                  'AUDIT:    ' & Format((Size(AUD:record)/1024),@n6.2) & '<13,10>' & |
                  'BOUNCER:  ' & Format((Size(BOU:record)/1024),@n6.2) & '<13,10>' & |
                  'CHARTYPE: ' & Format((Size(CHA:record)/1024),@n6.2) & '<13,10>' & |
                  'CITYSERV: ' & Format((Size(CIT:record)/1024),@n6.2) & '<13,10>' & |
                  'COLOUR:   ' & Format((Size(COL:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMCAT:  ' & Format((Size(CMC:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMONCP: ' & Format((Size(CCP:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMONFA: ' & Format((Size(COM:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMONWP: ' & Format((Size(CWP:record)/1024),@n6.2) & '<13,10>' & |
                  'CONSIGN:  ' & Format((Size(CNS:record)/1024),@n6.2) & '<13,10>' & |
                  'CONTACTS: ' & Format((Size(CON:record)/1024),@n6.2)& '<13,10>' & |
                  'CONTHIST: ' & Format((Size(CHT:record)/1024),@n6.2) & '<13,10>' & |
                  'COURIER:  ' & Format((Size(COU:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFAULTS: ' & Format((Size(DEF:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFCHRGE: ' & Format((Size(DEC:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFEDI:   ' & Format((Size(EDI:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFEDI2:  ' & Format((Size(ED2:record)/1024),@n6.2),'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
      
      
        MessageEx('DEFPRINT: ' & Format((Size(DEP:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFRAPID: ' & Format((Size(DER:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFSTOCK: ' & Format((Size(DST:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFWEB:   ' & Format((Size(DEW:record)/1024),@n6.2) & '<13,10>' & |
                  'DESBATCH: ' & Format((Size(DBT:record)/1024),@n6.2) & '<13,10>' & |
                  'DISCOUNT: ' & Format((Size(DIS:record)/1024),@n6.2) & '<13,10>' & |
                  'EDIBATCH: ' & Format((Size(EBT:record)/1024),@n6.2) & '<13,10>' & |
                  'ESNMODEL: ' & Format((Size(ESN:record)/1024),@n6.2) & '<13,10>' & |
                  'ESTPARTS: ' & Format((Size(EPR:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCAUDIT: ' & Format((Size(EXA:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCCHRGE: ' & Format((Size(EXC:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCHACC:  ' & Format((Size(XCA:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCHANGE: ' & Format((Size(XCH:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCHHIST: ' & Format((Size(EXH:record)/1024),@n6.2) & '<13,10>' & |
                  'INVOICE:  ' & Format((Size(INV:record)/1024),@n6.2) & '<13,10>' & |
                  'INVPARTS: ' & Format((Size(IVP:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBACC:   ' & Format((Size(JAC:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBBATCH: ' & Format((Size(JBT:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBEXACC: ' & Format((Size(JEA:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBEXHIS: ' & Format((Size(JXH:record)/1024),@n6.2) ,'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
      
        MessageEx('JOBLOHIS: ' & Format((Size(JLH:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBPAYMT: ' & Format((Size(JPT:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBS:     ' & Format((Size(JOB:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBNOTES: ' & Format((Size(JBN:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBSTAGE: ' & Format((Size(JST:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBSVODA: ' & Format((Size(JVF:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBVODAC: ' & Format((Size(JVA:record)/1024),@n6.2) & '<13,10>' & |
                  'LETTERS:  ' & Format((Size(LET:record)/1024),@n6.2) & '<13,10>' & |
                  'LOAN:     ' & Format((Size(LOA:record)/1024),@n6.2) & '<13,10>' & |
                  'LOANACC:  ' & Format((Size(LAC:record)/1024),@n6.2) & '<13,10>' & |
                  'LOANHIST: ' & Format((Size(LOH:record)/1024),@n6.2) & '<13,10>' & |
                  'LOCATION: ' & Format((Size(LOC:record)/1024),@n6.2) & '<13,10>' & |
                  'LOCINTER: ' & Format((Size(LOI:record)/1024),@n6.2) & '<13,10>' & |
                  'LOCSHELF: ' & Format((Size(LOS:record)/1024),@n6.2) & '<13,10>' & |
                  'LOGGED:   ' & Format((Size(LOG:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFAULO: ' & Format((Size(MFO:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFAULT: ' & Format((Size(MAF:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFAUPA: ' & Format((Size(MAP:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFPALO: ' & Format((Size(MFP:record)/1024),@n6.2) & '<13,10>' & |
                  'MANUFACT: ' & Format((Size(MAN:record)/1024),@n6.2) & '<13,10>' & |
                  'MERGE:    ' & Format((Size(MER:record)/1024),@n6.2) ,'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
        MessageEx('MERGELET: ' & Format((Size(MRG:record)/1024),@n6.2) & '<13,10>' & |
                  'MERGETXT: ' & Format((Size(MRT:record)/1024),@n6.2) & '<13,10>' & |
                  'MESSAGES: ' & Format((Size(MES:record)/1024),@n6.2) & '<13,10>' & |
                  'MODELCOL: ' & Format((Size(MOC:record)/1024),@n6.2) & '<13,10>' & |
                  'MODELNUM: ' & Format((Size(MOD:record)/1024),@n6.2) & '<13,10>' & |
                  'NEWFEAT:  ' & Format((Size(FEA:record)/1024),@n6.2) & '<13,10>' & |
                  'NOTESENG: ' & Format((Size(NOE:record)/1024),@n6.2) & '<13,10>' & |
                  'NOTESFAU: ' & Format((Size(NOF:record)/1024),@n6.2) & '<13,10>' & |
                  'NOTESINV: ' & Format((Size(NOI:record)/1024),@n6.2) & '<13,10>' & |
                  'ORDERS:   ' & Format((Size(ORD:record)/1024),@n6.2) & '<13,10>' & |
                  'ORDPARTS: ' & Format((Size(ORP:record)/1024),@n6.2) & '<13,10>' & |
                  'ORDPEND:  ' & Format((Size(OPE:record)/1024),@n6.2) & '<13,10>' & |
                  'PARTS:    ' & Format((Size(PAR:record)/1024),@n6.2) & '<13,10>' & |
                  'PAYTYPES: ' & Format((Size(PAY:record)/1024),@n6.2) & '<13,10>' & |
                  'PRIORITY: ' & Format((Size(PRI:record)/1024),@n6.2) & '<13,10>' & |
                  'PROCCODE: ' & Format((Size(PRO:record)/1024),@n6.2) & '<13,10>' & |
                  'PROINV:   ' & Format((Size(PRV:record)/1024),@n6.2) & '<13,10>' & |
                  'QAREASON: ' & Format((Size(QAR:record)/1024),@n6.2) & '<13,10>' & |
                  'QUERYREA: ' & Format((Size(QUE:record)/1024),@n6.2) & '<13,10>' & |
                  'REPAIRTY: ' & Format((Size(REP:record)/1024),@n6.2),'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
        MessageEx('REPTYDEF: ' & Format((Size(RTD:record)/1024),@n6.2) & '<13,10>' & |
                  'RETDESNO: ' & Format((Size(RdN:record)/1024),@n6.2) & '<13,10>' & |
                  'RETPAY:   ' & Format((Size(RTP:record)/1024),@n6.2) & '<13,10>' & |
                  'RETSALES: ' & Format((Size(RET:record)/1024),@n6.2) & '<13,10>' & |   
                  'RETSTOCK: ' & Format((Size(RES:record)/1024),@n6.2) & '<13,10>' & |
                  'STAHEAD:  ' & Format((Size(STH:record)/1024),@n6.2) & '<13,10>' & |
                  'STANTEXT: ' & Format((Size(STT:record)/1024),@n6.2) & '<13,10>' & |
                  'STATUS:   ' & Format((Size(STS:record)/1024),@n6.2) & '<13,10>' & |
                  'STDCHRGE: ' & Format((Size(STA:record)/1024),@n6.2) & '<13,10>' & |
                  'STOCK:    ' & Format((Size(STO:record)/1024),@n6.2) & '<13,10>' & |
                  'STOCKTYP: ' & Format((Size(STP:record)/1024),@n6.2) & '<13,10>' & |
                  'STOESN:   ' & Format((Size(STE:record)/1024),@n6.2) & '<13,10>' & |
                  'STOHIST:  ' & Format((Size(SHI:record)/1024),@n6.2) & '<13,10>' & |
                  'STOMODEL: ' & Format((Size(STM:record)/1024),@n6.2) & '<13,10>' & |
                  'SUBCHRGE: ' & Format((Size(SUC:record)/1024),@n6.2) & '<13,10>' & |
                  'SUBTRACC: ' & Format((Size(SUB:record)/1024),@n6.2) & '<13,10>' & |
                  'SUPPLIER: ' & Format((Size(SUP:record)/1024),@n6.2) & '<13,10>' & |
                  'TEAMS:    ' & Format((Size(TEA:record)/1024),@n6.2) & '<13,10>' & |
                  'TRACHAR:  ' & Format((Size(TCH:record)/1024),@n6.2) & '<13,10>' & |
                  'TRADEACC: ' & Format((Size(TRA:record)/1024),@n6.2) ,'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
        MessageEx('TRANTYPE: ' & Format((Size(TRT:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDBATCH: ' & Format((Size(TRB:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDMODEL: ' & Format((Size(TRM:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDPARTY: ' & Format((Size(TRD:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDSPEC:  ' & Format((Size(TSP:record)/1024),@n6.2) & '<13,10>' & |
                  'TURNARND: ' & Format((Size(TUR:record)/1024),@n6.2) & '<13,10>' & |
                  'UNITTYPE: ' & Format((Size(UNI:record)/1024),@n6.2) & '<13,10>' & |
                  'USELEVEL: ' & Format((Size(LEV:record)/1024),@n6.2) & '<13,10>' & |
                  'USERS:    ' & Format((Size(USE:record)/1024),@n6.2) & '<13,10>' & |
                  'USMASSIG: ' & Format((Size(USM:record)/1024),@n6.2) & '<13,10>' & |
                  'USUMASIG: ' & Format((Size(USU:record)/1024),@n6.2) & '<13,10>' & |
                  'VATCODE:  ' & Format((Size(VAT:record)/1024),@n6.2) & '<13,10>' & |
                  'WARPARTS: ' & Format((Size(WPR:record)/1024),@n6.2),'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
      End!If KeyCode() = ShiftAltF12
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Compile('***',debug=1)
          unhide(?warning_text)
      ***
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

