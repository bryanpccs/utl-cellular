

   MEMBER('logistic.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOGIS016.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('LOGIS020.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS027.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('LOGIS030.INC'),ONCE        !Req'd for module callout resolution
                     END


DespatchStock PROCEDURE (f_type,logalloc_no)          !Generated from procedure template - Window

ThisThreadActive BYTE
tmp:insertype        BYTE,DIM(2)
Counter_IMEI         REAL
tmp:recordnumber     LONG
tmp:recordnumber2    LONG
save_logasttmp_id    USHORT,AUTO
save_logast_id       USHORT,AUTO
save_logtmp_id       USHORT,AUTO
tmp:tolocation       STRING(30)
tmp:tosalescode      STRING(30)
tmp:todescription    STRING(30)
tmp:toclubnokia      STRING(30)
tmp:q_imported       QUEUE,PRE(tmpimp)
tmp:importedesn      STRING(30)
                     END
tmp:q_scanned        QUEUE,PRE(tmpscn)
tmp:scannedesn       STRING(30)
                     END
tmp:location         STRING(30)
tmp:salescode        STRING(30)
tmp:description      STRING(30)
tmp:RefNumber        LONG
tmp:esn              STRING(30)
tmp:importpath       STRING(255)
tmp:clubnokia        STRING(30)
tmp:desclubnokia     STRING(30)
tmp:totalstock       LONG
tmp:totalDespatch    LONG
tmp:refnumber2       LONG
tmp:smpfnumber       STRING(30)
tmp:DeliveryNote     STRING(30)
tmp:OrderRefNo       STRING(30)
tmp:modelnumber      STRING(30)
tmp:tomodelnumber    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Export_To_ServiceBase STRING(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:location
lstl:Location          LIKE(lstl:Location)            !List box control field - type derived from field
lstl:RecordNumber      LIKE(lstl:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?tmp:clubnokia
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?tmp:desclubnokia
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?tmp:tolocation
lstl:Location          LIKE(lstl:Location)            !List box control field - type derived from field
lstl:RecordNumber      LIKE(lstl:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:6 QUEUE                           !Queue declaration for browse/combo box using ?tmp:toclubnokia
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(LOGSTLOC)
                       PROJECT(lstl:Location)
                       PROJECT(lstl:RecordNumber)
                     END
FDCB11::View:FileDropCombo VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
FDCB9::View:FileDropCombo VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
FDCB15::View:FileDropCombo VIEW(LOGSTLOC)
                       PROJECT(lstl:Location)
                       PROJECT(lstl:RecordNumber)
                     END
FDCB17::View:FileDropCombo VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
BRW5::View:Browse    VIEW(LOGASSST)
                       PROJECT(logast:Description)
                       PROJECT(logast:RecordNumber)
                       PROJECT(logast:RefNumber)
                       PROJECT(logast:PartNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
logast:Description     LIKE(logast:Description)       !List box control field - type derived from field
logast:RecordNumber    LIKE(logast:RecordNumber)      !Primary key field - type derived from field
logast:RefNumber       LIKE(logast:RefNumber)         !Browse key field - type derived from field
logast:PartNumber      LIKE(logast:PartNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(LOGSERST)
                       PROJECT(logser:ESN)
                       PROJECT(logser:RecordNumber)
                       PROJECT(logser:RefNumber)
                       PROJECT(logser:Status)
                       PROJECT(logser:ClubNokia)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
logser:ESN             LIKE(logser:ESN)               !List box control field - type derived from field
logser:RecordNumber    LIKE(logser:RecordNumber)      !Primary key field - type derived from field
logser:RefNumber       LIKE(logser:RefNumber)         !Browse key field - type derived from field
logser:Status          LIKE(logser:Status)            !Browse key field - type derived from field
logser:ClubNokia       LIKE(logser:ClubNokia)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW18::View:Browse   VIEW(LOGASSST_ALIAS)
                       PROJECT(logast_alias:Description)
                       PROJECT(logast_alias:RefNumber)
                       PROJECT(logast_alias:PartNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
logast_alias:Description LIKE(logast_alias:Description) !List box control field - type derived from field
logast_alias:RefNumber LIKE(logast_alias:RefNumber)   !Browse key field - type derived from field
logast_alias:PartNumber LIKE(logast_alias:PartNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW20::View:Browse   VIEW(LOGTEMP)
                       PROJECT(logtmp:IMEI)
                       PROJECT(logtmp:RefNumber)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
logtmp:IMEI            LIKE(logtmp:IMEI)              !List box control field - type derived from field
logtmp:RefNumber       LIKE(logtmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Wizard
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Wizard
mo:SelectedField  Long
window               WINDOW('Despatch Stock'),AT(,,456,280),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,220,244),USE(?Sheet2),SPREAD
                         TAB('From Location'),USE(?Tab2)
                           PROMPT('Sales Code'),AT(8,20),USE(?Prompt2:2)
                           ENTRY(@s30),AT(84,20,124,10),USE(tmp:salescode),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           BUTTON,AT(212,20,10,10),USE(?LookupSalesCode),SKIP,ICON('list3.ico')
                           COMBO(@s30),AT(84,68,124,10),USE(tmp:location),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Description'),AT(8,36),USE(?tmp:description:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           PROMPT('Associated Accessories'),AT(8,148,68,16),USE(?tmp:description:Prompt:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(tmp:description),SKIP,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Model Number'),AT(8,52),USE(?tmp:modelnumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(tmp:modelnumber),SKIP,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('FROM Club Nokia Site'),AT(8,84),USE(?tmp:description:Prompt:3),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           COMBO(@s30),AT(84,84,124,10),USE(tmp:clubnokia),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           PROMPT('TO Club Nokia Site'),AT(8,100,68,10),USE(?tmp:desclubnokia:prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           COMBO(@s30),AT(84,100,124,10),USE(tmp:desclubnokia),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                           PROMPT('SMPF Number'),AT(8,116),USE(?tmp:smpfnumber:Prompt),TRN,HIDE,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,116,124,10),USE(tmp:smpfnumber),HIDE,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Delivery Note No'),AT(8,132),USE(?tmp:DeliveryNote:Prompt),TRN,HIDE,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,132,124,10),USE(tmp:DeliveryNote),HIDE,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           LIST,AT(84,148,124,64),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse)
                           STRING('Export To SB2000'),AT(8,216),USE(?String3)
                           PROMPT('E.S.N. / I.M.E.I.'),AT(8,232),USE(?tmp:esn:Prompt),TRN,HIDE,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,232,124,10),USE(tmp:esn),HIDE,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           CHECK,AT(84,216),USE(tmp:Export_To_ServiceBase),VALUE('Y','N')
                           PROMPT('Stock Location'),AT(8,68),USE(?Prompt2)
                         END
                         TAB('To Location'),USE(?Tab4),HIDE
                           BUTTON,AT(212,20,10,10),USE(?LookupToSalesCode),SKIP,ICON('list3.ico')
                           PROMPT('Sales Code'),AT(8,20),USE(?Prompt2:4)
                           ENTRY(@s30),AT(84,20,124,10),USE(tmp:tosalescode),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Stock Location'),AT(8,64),USE(?Prompt2:3)
                           COMBO(@s30),AT(84,64,124,10),USE(tmp:tolocation),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4)
                           PROMPT('Description'),AT(8,32),USE(?tmp:description:Prompt:5),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,32,124,10),USE(tmp:todescription),SKIP,FONT(,,,FONT:bold),READONLY
                           PROMPT('Model Number'),AT(8,48),USE(?tmp:tomodelnumber:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,48,124,10),USE(tmp:tomodelnumber),SKIP,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Club Nokia Site'),AT(8,84),USE(?tmp:description:Prompt:6),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           COMBO(@s30),AT(84,84,124,10),USE(tmp:toclubnokia),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:6)
                           PROMPT('Order Ref. No.'),AT(8,100),USE(?tmp:OrderRefNo:Prompt),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,100,124,10),USE(tmp:OrderRefNo),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           LIST,AT(84,116,124,72),USE(?List:3),IMM,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse:2)
                           PROMPT('E.S.N. / I.M.E.I.'),AT(12,200),USE(?tmp:esn:Prompt:2),TRN,HIDE,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,200,124,10),USE(tmp:esn,,?tmp:esn:2),HIDE,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Associated Stock'),AT(8,116),USE(?tmp:description:Prompt:7),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         END
                       END
                       SHEET,AT(228,4,224,244),USE(?Sheet2:2),WIZARD,SPREAD
                         TAB('Tab 3'),USE(?Tab3)
                           PROMPT('Stock Items'),AT(232,8),USE(?Prompt6)
                           PROMPT('Despatch Items'),AT(344,8,51,10),USE(?despatchtitle)
                           STRING(@s8),AT(259,236),USE(tmp:totalstock),FONT(,,,FONT:bold)
                           PROMPT('Total: '),AT(344,236),USE(?Prompt10:2)
                           STRING(@s8),AT(376,236),USE(tmp:totalDespatch),FONT(,,,FONT:bold)
                           PROMPT('Total: '),AT(232,236),USE(?Prompt10)
                           LIST,AT(232,20,104,212),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~E.S.N. / I.M.E.I.~@s30@'),FROM(Queue:Browse:1)
                           LIST,AT(344,20,104,212),USE(?List:4),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~I.M.E.I.~@s30@'),FROM(Queue:Browse:3)
                         END
                       END
                       PANEL,AT(4,252,448,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Finish'),AT(336,256,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                       BUTTON('Cancel'),AT(392,256,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB9                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB15               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

FDCB17               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:6         !Reference to browse queue type
                     END

BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW12                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ResetFromView          PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
BRW18                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW18::Sort0:Locator StepLocatorClass                 !Default Locator
BRW20                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
ResetFromView          PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW20::Sort0:Locator StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
IMEI        Routine
    access:logtemp.clearkey(logtmp:imeikey)
    logtmp:imei = tmp:esn
    if access:logtemp.tryfetch(logtmp:imeikey) = Level:benign
        Case MessageEx('The selected I.M.E.I. Number has already been entered.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
        Case f_type
            Of 'DESPATCH'
                Select(?tmp:esn)
            Else
                Select(?tmp:esn:2)
        End!Case f_type
    Else!if access:logtemp.tryfetch(logtmp:imeikey) = Level:benign
        !Normal circumstances!
        found# = 0
        access:logserst_alias.clearkey(logser_ali:nokiastatuskey)
        logser_ali:refnumber = tmp:recordnumber
        logser_ali:status    = 'AVAILABLE'
        logser_ali:clubnokia = tmp:clubnokia
        logser_ali:esn          = tmp:esn
        if access:logserst_alias.tryfetch(logser_ali:nokiastatuskey)
          !None Available!
          IF logalloc_no > 0
            access:logserst_alias.clearkey(logser_ali:nokiastatuskey)
            logser_ali:refnumber = tmp:recordnumber
            logser_ali:status    = 'ALLOCATED'
            logser_ali:clubnokia = tmp:clubnokia
            logser_ali:esn          = tmp:esn
            if access:logserst_alias.tryfetch(logser_ali:nokiastatuskey)
              !Still nothing!
            ELSE
              Found# = 1
            END
          END
        ELSE
          Found# = 1
          IF logalloc_no > 0 !normal situation!
            access:logserst_alias.clearkey(logser_ali:nokiastatuskey)
            logser_ali:refnumber = tmp:recordnumber
            logser_ali:status    = 'ALLOCATED'
            logser_ali:clubnokia = tmp:clubnokia
            SET(logser_ali:nokiastatuskey,logser_ali:nokiastatuskey)
            LOOP
              IF Access:Logserst_alias.Next()
                BREAK
              END
              IF logser_ali:refnumber <> tmp:recordnumber
                BREAK
              END
              IF  logser_ali:status    <> 'ALLOCATED'
                BREAK
              END
              IF logser_ali:clubnokia <> tmp:clubnokia
                BREAK
              END
              logser_ali:status = 'AVAILABLE'
              Access:Logserst_alias.Update()
            END
          END
        END
        IF found# = 1 and logalloc_no > 0
          counter_imei +=1
          IF Counter_imei = LOG2:Qty
            Case MessageEx('You have despatched the required amount','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                      LOG2:Done_Flag = TRUE
                      Access:LogAlloc.Update()
                End!Case MessageEx
          END
        END
        IF found# = 0
            If f_type = 'DESPATCH'
                Case MessageEx('The selected I.M.E.I. is not available to be despatched.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            Else!If f_type = 'DESPATCH'
                Case MessageEx('The selected I.M.E.I. is not available to be transferred.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            End!If f_type = 'DESPATCH'
        Else!if access:logserst_alias.tryfetch(logser_ali:nokiastatuskey)
            get(logtemp,0)
            if access:logtemp.primerecord() = Level:Benign
                logtmp:imei      = tmp:esn
                if access:logtemp.insert()
                    access:logtemp.cancelautoinc()
                end
            End!if access:logtemp.primerecord() = Level:Benign
            tmp:esn = ''
        End!if access:logserst_alias.tryfetch(logser_ali:nokiastatuskey)
        Case f_type
            Of 'DESPATCH'
                Select(?tmp:esn)
            Else
                Select(?tmp:esn:2)
        End!Case f_type
        Display()
    End!if access:logtemp.tryfetch(logtmp:imeikey) = Level:benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchStock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    
      Loop SolaceDim1# = 1 to 2
        SolaceFieldName" = 'tmp:insertype' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:insertype[SolaceDim1#],'DespatchStock',1)
      End
    
    
    SolaceViewVars('Counter_IMEI',Counter_IMEI,'DespatchStock',1)
    SolaceViewVars('tmp:recordnumber',tmp:recordnumber,'DespatchStock',1)
    SolaceViewVars('tmp:recordnumber2',tmp:recordnumber2,'DespatchStock',1)
    SolaceViewVars('save_logasttmp_id',save_logasttmp_id,'DespatchStock',1)
    SolaceViewVars('save_logast_id',save_logast_id,'DespatchStock',1)
    SolaceViewVars('save_logtmp_id',save_logtmp_id,'DespatchStock',1)
    SolaceViewVars('tmp:tolocation',tmp:tolocation,'DespatchStock',1)
    SolaceViewVars('tmp:tosalescode',tmp:tosalescode,'DespatchStock',1)
    SolaceViewVars('tmp:todescription',tmp:todescription,'DespatchStock',1)
    SolaceViewVars('tmp:toclubnokia',tmp:toclubnokia,'DespatchStock',1)
    SolaceViewVars('tmp:q_imported:tmp:importedesn',tmp:q_imported:tmp:importedesn,'DespatchStock',1)
    SolaceViewVars('tmp:q_scanned:tmp:scannedesn',tmp:q_scanned:tmp:scannedesn,'DespatchStock',1)
    SolaceViewVars('tmp:location',tmp:location,'DespatchStock',1)
    SolaceViewVars('tmp:salescode',tmp:salescode,'DespatchStock',1)
    SolaceViewVars('tmp:description',tmp:description,'DespatchStock',1)
    SolaceViewVars('tmp:RefNumber',tmp:RefNumber,'DespatchStock',1)
    SolaceViewVars('tmp:esn',tmp:esn,'DespatchStock',1)
    SolaceViewVars('tmp:importpath',tmp:importpath,'DespatchStock',1)
    SolaceViewVars('tmp:clubnokia',tmp:clubnokia,'DespatchStock',1)
    SolaceViewVars('tmp:desclubnokia',tmp:desclubnokia,'DespatchStock',1)
    SolaceViewVars('tmp:totalstock',tmp:totalstock,'DespatchStock',1)
    SolaceViewVars('tmp:totalDespatch',tmp:totalDespatch,'DespatchStock',1)
    SolaceViewVars('tmp:refnumber2',tmp:refnumber2,'DespatchStock',1)
    SolaceViewVars('tmp:smpfnumber',tmp:smpfnumber,'DespatchStock',1)
    SolaceViewVars('tmp:DeliveryNote',tmp:DeliveryNote,'DespatchStock',1)
    SolaceViewVars('tmp:OrderRefNo',tmp:OrderRefNo,'DespatchStock',1)
    SolaceViewVars('tmp:modelnumber',tmp:modelnumber,'DespatchStock',1)
    SolaceViewVars('tmp:tomodelnumber',tmp:tomodelnumber,'DespatchStock',1)
    SolaceViewVars('tmp:Export_To_ServiceBase',tmp:Export_To_ServiceBase,'DespatchStock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:salescode;  SolaceCtrlName = '?tmp:salescode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupSalesCode;  SolaceCtrlName = '?LookupSalesCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location;  SolaceCtrlName = '?tmp:location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt;  SolaceCtrlName = '?tmp:description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt:2;  SolaceCtrlName = '?tmp:description:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description;  SolaceCtrlName = '?tmp:description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:modelnumber:Prompt;  SolaceCtrlName = '?tmp:modelnumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:modelnumber;  SolaceCtrlName = '?tmp:modelnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt:3;  SolaceCtrlName = '?tmp:description:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:clubnokia;  SolaceCtrlName = '?tmp:clubnokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:desclubnokia:prompt;  SolaceCtrlName = '?tmp:desclubnokia:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:desclubnokia;  SolaceCtrlName = '?tmp:desclubnokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:smpfnumber:Prompt;  SolaceCtrlName = '?tmp:smpfnumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:smpfnumber;  SolaceCtrlName = '?tmp:smpfnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DeliveryNote:Prompt;  SolaceCtrlName = '?tmp:DeliveryNote:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DeliveryNote;  SolaceCtrlName = '?tmp:DeliveryNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String3;  SolaceCtrlName = '?String3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn:Prompt;  SolaceCtrlName = '?tmp:esn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn;  SolaceCtrlName = '?tmp:esn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Export_To_ServiceBase;  SolaceCtrlName = '?tmp:Export_To_ServiceBase';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupToSalesCode;  SolaceCtrlName = '?LookupToSalesCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:4;  SolaceCtrlName = '?Prompt2:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:tosalescode;  SolaceCtrlName = '?tmp:tosalescode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:3;  SolaceCtrlName = '?Prompt2:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:tolocation;  SolaceCtrlName = '?tmp:tolocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt:5;  SolaceCtrlName = '?tmp:description:Prompt:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:todescription;  SolaceCtrlName = '?tmp:todescription';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:tomodelnumber:Prompt;  SolaceCtrlName = '?tmp:tomodelnumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:tomodelnumber;  SolaceCtrlName = '?tmp:tomodelnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt:6;  SolaceCtrlName = '?tmp:description:Prompt:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:toclubnokia;  SolaceCtrlName = '?tmp:toclubnokia';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:OrderRefNo:Prompt;  SolaceCtrlName = '?tmp:OrderRefNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:OrderRefNo;  SolaceCtrlName = '?tmp:OrderRefNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn:Prompt:2;  SolaceCtrlName = '?tmp:esn:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn:2;  SolaceCtrlName = '?tmp:esn:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description:Prompt:7;  SolaceCtrlName = '?tmp:description:Prompt:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2:2;  SolaceCtrlName = '?Sheet2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatchtitle;  SolaceCtrlName = '?despatchtitle';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:totalstock;  SolaceCtrlName = '?tmp:totalstock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10:2;  SolaceCtrlName = '?Prompt10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:totalDespatch;  SolaceCtrlName = '?tmp:totalDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:4;  SolaceCtrlName = '?List:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('DespatchStock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'DespatchStock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='DespatchStock'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOGALLOC.Open
  Relate:LOGASSST.Open
  Relate:LOGASSST_ALIAS.Open
  Relate:LOGCLSTE.Open
  Relate:LOGDEFLT.Open
  Relate:LOGEXCH.Open
  Relate:LOGSERST_ALIAS.Open
  Relate:LOGTEMP.Open
  Relate:STOCK.Open
  Access:LOGSTOCK.UseFile
  Access:LOGSERST.UseFile
  Access:LOGSTHIS.UseFile
  Access:STOHIST.UseFile
  Access:LOGDESNO.UseFile
  Access:USERS.UseFile
  Access:LOGEXHE.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:LOGASSST,SELF)
  BRW12.Init(?List:2,Queue:Browse:1.ViewPosition,BRW12::View:Browse,Queue:Browse:1,Relate:LOGSERST,SELF)
  BRW18.Init(?List:3,Queue:Browse:2.ViewPosition,BRW18::View:Browse,Queue:Browse:2,Relate:LOGASSST_ALIAS,SELF)
  BRW20.Init(?List:4,Queue:Browse:3.ViewPosition,BRW20::View:Browse,Queue:Browse:3,Relate:LOGTEMP,SELF)
  OPEN(window)
  SELF.Opened=True
  If f_type <> 'DESPATCH'
      Unhide(?tab4)
      Hide(?tmp:desclubnokia)
      Hide(?tmp:desclubnokia:prompt)
      0{prop:text} = 'Transfer Stock'
      Unhide(?tmp:esn:2)
      Unhide(?tmp:esn:prompt:2)
      ?despatchtitle{prop:text} = 'Transfer Items'
      HIDE(?tmp:Export_To_ServiceBase)
  Else!If f_type <> 'DESPATCH'
      Unhide(?tmp:smpfnumber)
      Unhide(?tmp:smpfnumber:prompt)
      unhide(?tmp:DeliveryNote)
      unhide(?tmp:DeliveryNote:prompt)
      Unhide(?tmp:esn)
      Unhide(?tmp:esn:prompt)
      UNHIDE(?tmp:Export_To_ServiceBase)
  End!If f_type <> 'DESPATCH'
  IF logalloc_no > 0
    Access:LogAlloc.ClearKey(LOG2:AutoNumber_Key)
    LOG2:RefNumber = logalloc_no
    IF Access:LogAlloc.Fetch(LOG2:AutoNumber_Key)
      !Error!
    ELSE
     tmp:modelnumber = LOG2:ModelNumber
     tmp:location = LOG2:Location
     tmp:smpfnumber = LOG2:SMPFNumber
     tmp:DeliveryNote = LOG2:DespatchNo
     tmp:salescode = LOG2:SalesCode
     tmp:description = LOG2:Description
     tmp:clubnokia = LOG2:From
     tmp:desclubnokia = LOG2:To_Site
     Access:Logstock.ClearKey(logsto:SalesKey)
     logsto:SalesCode = tmp:salescode
     IF Access:Logstock.Fetch(logsto:SalesKey)
       !Error!
     ELSE
       Tmp:RefNumber = logsto:RefNumber
     END
     Counter_IMEI = 0
     Update()
     Display()
     
   END
  END
  
  
  
  ?List{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,logast:RefNumberKey)
  BRW5.AddRange(logast:RefNumber,tmp:RefNumber)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,logast:PartNumber,1,BRW5)
  BRW5.AddField(logast:Description,BRW5.Q.logast:Description)
  BRW5.AddField(logast:RecordNumber,BRW5.Q.logast:RecordNumber)
  BRW5.AddField(logast:RefNumber,BRW5.Q.logast:RefNumber)
  BRW5.AddField(logast:PartNumber,BRW5.Q.logast:PartNumber)
  BRW12.Q &= Queue:Browse:1
  BRW12.AddSortOrder(,logser:NokiaStatusKey)
  BRW12.AddRange(logser:RefNumber,tmp:recordnumber)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,logser:Status,1,BRW12)
  BIND('tmp:clubnokia',tmp:clubnokia)
  BRW12.AddField(logser:ESN,BRW12.Q.logser:ESN)
  BRW12.AddField(logser:RecordNumber,BRW12.Q.logser:RecordNumber)
  BRW12.AddField(logser:RefNumber,BRW12.Q.logser:RefNumber)
  BRW12.AddField(logser:Status,BRW12.Q.logser:Status)
  BRW12.AddField(logser:ClubNokia,BRW12.Q.logser:ClubNokia)
  BRW18.Q &= Queue:Browse:2
  BRW18.AddSortOrder(,logast_alias:RefNumberKey)
  BRW18.AddRange(logast_alias:RefNumber,tmp:refnumber2)
  BRW18.AddLocator(BRW18::Sort0:Locator)
  BRW18::Sort0:Locator.Init(,logast_alias:PartNumber,1,BRW18)
  BRW18.AddField(logast_alias:Description,BRW18.Q.logast_alias:Description)
  BRW18.AddField(logast_alias:RefNumber,BRW18.Q.logast_alias:RefNumber)
  BRW18.AddField(logast_alias:PartNumber,BRW18.Q.logast_alias:PartNumber)
  BRW20.Q &= Queue:Browse:3
  BRW20.AddSortOrder(,logtmp:IMEIKey)
  BRW20.AddLocator(BRW20::Sort0:Locator)
  BRW20::Sort0:Locator.Init(,logtmp:IMEI,1,BRW20)
  BRW20.AddField(logtmp:IMEI,BRW20.Q.logtmp:IMEI)
  BRW20.AddField(logtmp:RefNumber,BRW20.Q.logtmp:RefNumber)
     POST(Event:Accepted,?tmp:salescode)
  ThisMakeover.SetWindow(Win:Wizard)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,?Sheet2)
  window{prop:buffer} = 1
  FDCB3.Init(tmp:location,?tmp:location,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:LOGSTLOC,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(lstl:RefLocationKey)
  FDCB3.AddRange(lstl:RefNumber,tmp:RefNumber)
  FDCB3.AddField(lstl:Location,FDCB3.Q.lstl:Location)
  FDCB3.AddField(lstl:RecordNumber,FDCB3.Q.lstl:RecordNumber)
  FDCB3.AddUpdateField(lstl:Location,tmp:location)
  FDCB3.AddUpdateField(lstl:RecordNumber,tmp:recordnumber)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  FDCB11.Init(tmp:clubnokia,?tmp:clubnokia,Queue:FileDropCombo:2.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo:2,Relate:LOGCLSTE,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo:2
  FDCB11.AddSortOrder(logclu:ClubNokiaKey)
  FDCB11.AddField(logclu:ClubNokia,FDCB11.Q.logclu:ClubNokia)
  FDCB11.AddField(logclu:RecordNumber,FDCB11.Q.logclu:RecordNumber)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  FDCB9.Init(tmp:desclubnokia,?tmp:desclubnokia,Queue:FileDropCombo:3.ViewPosition,FDCB9::View:FileDropCombo,Queue:FileDropCombo:3,Relate:LOGCLSTE,ThisWindow,GlobalErrors,0,1,0)
  FDCB9.Q &= Queue:FileDropCombo:3
  FDCB9.AddSortOrder(logclu:ClubNokiaKey)
  FDCB9.AddField(logclu:ClubNokia,FDCB9.Q.logclu:ClubNokia)
  FDCB9.AddField(logclu:RecordNumber,FDCB9.Q.logclu:RecordNumber)
  ThisWindow.AddItem(FDCB9.WindowComponent)
  FDCB9.DefaultFill = 0
  FDCB15.Init(tmp:tolocation,?tmp:tolocation,Queue:FileDropCombo:4.ViewPosition,FDCB15::View:FileDropCombo,Queue:FileDropCombo:4,Relate:LOGSTLOC,ThisWindow,GlobalErrors,0,1,0)
  FDCB15.Q &= Queue:FileDropCombo:4
  FDCB15.AddSortOrder(lstl:RefLocationKey)
  FDCB15.AddRange(lstl:RefNumber,tmp:refnumber2)
  FDCB15.AddField(lstl:Location,FDCB15.Q.lstl:Location)
  FDCB15.AddField(lstl:RecordNumber,FDCB15.Q.lstl:RecordNumber)
  ThisWindow.AddItem(FDCB15.WindowComponent)
  FDCB15.DefaultFill = 0
  FDCB17.Init(tmp:toclubnokia,?tmp:toclubnokia,Queue:FileDropCombo:6.ViewPosition,FDCB17::View:FileDropCombo,Queue:FileDropCombo:6,Relate:LOGCLSTE,ThisWindow,GlobalErrors,0,1,0)
  FDCB17.Q &= Queue:FileDropCombo:6
  FDCB17.AddSortOrder(logclu:ClubNokiaKey)
  FDCB17.AddField(logclu:ClubNokia,FDCB17.Q.logclu:ClubNokia)
  FDCB17.AddField(logclu:RecordNumber,FDCB17.Q.logclu:RecordNumber)
  ThisWindow.AddItem(FDCB17.WindowComponent)
  FDCB17.DefaultFill = 0
  BRW5.AddToolbarTarget(Toolbar)
  BRW18.AddToolbarTarget(Toolbar)
  BRW20.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGALLOC.Close
    Relate:LOGASSST.Close
    Relate:LOGASSST_ALIAS.Close
    Relate:LOGCLSTE.Close
    Relate:LOGDEFLT.Close
    Relate:LOGEXCH.Close
    Relate:LOGSERST_ALIAS.Close
    Relate:LOGTEMP.Close
    Relate:STOCK.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='DespatchStock'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  Remove(glo:file_name2)
  If Error()
      access:logtemp.close()
      Remove(glo:file_name2)
  End!If Error()
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'DespatchStock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:salescode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:salescode, Accepted)
      BRW5.ResetSort(1)
      BRW12.ResetSort(1)
      FDCB3.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:salescode, Accepted)
    OF ?LookupSalesCode
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickSalesCode
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSalesCode, Accepted)
      case globalresponse
          of requestcompleted
              tmp:salescode = logsto:salescode
              tmp:modelnumber    = logsto:modelnumber
              tmp:description = logsto:description
              tmp:refnumber   = logsto:refnumber
          of requestcancelled
      !        tmp:salescode = ''
              select(?-1)
      end!case globalreponse
      display(?)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSalesCode, Accepted)
    OF ?tmp:location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:location, Accepted)
      BRW5.ResetSort(1)
      BRW12.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:location, Accepted)
    OF ?tmp:clubnokia
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:clubnokia, Accepted)
      BRW12.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:clubnokia, Accepted)
    OF ?tmp:esn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
      Do IMEI
      BRW20.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
    OF ?LookupToSalesCode
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickSalesCode
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupToSalesCode, Accepted)
      case globalresponse
          of requestcompleted
              tmp:tosalescode = logsto:salescode
              tmp:tomodelnumber    = logsto:modelnumber
              tmp:todescription = logsto:description
              tmp:refnumber2   = logsto:refnumber
             ! select(?+2)
          of requestcancelled
      !        tmp:salescode = ''
              select(?-1)
      end!case globalreponse
      display(?)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupToSalesCode, Accepted)
    OF ?tmp:tosalescode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:tosalescode, Accepted)
      BRW18.ResetSort(1)
      FDCB15.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:tosalescode, Accepted)
    OF ?tmp:esn:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn:2, Accepted)
      Do IMEI
      BRW20.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn:2, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      error# = 0
      If tmp:refnumber = ''
          Case MessageEx('You must select a Valid Sales Code.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp:refnumber = ''
      
      If tmp:location = '' and error# = 0
          Case MessageEx('You must select a Stock Location.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp:location = ''
      
      If tmp:clubnokia = '' and error# = 0
          Case MessageEx('You must select a Club Nokia Site.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If tmp:clubnokiasite = '' and error# = 0
      If error# = 0
          If f_Type = 'DESPATCH'
              If tmp:desclubnokia = '' and error# = 0
                  Case MessageEx('You must select a Club Nokia Site to Despatch to.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tmp:desclubnokia = '' and error# = 0
              If tmp:smpfnumber = '' and error# = 0
                  Case MessageEx('You must insert a SMPF Number.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tmp:smpfnumber = '' and error# = 0
              If tmp:deliverynote = '' and error# = 0
                  Case MessageEx('You must insert a Delivery Note.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tmp:deliverynote = '' and error# = 0
              If error# = 0
      
                  Case MessageEx('Are you sure you want to finish?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          If ~Records(logtemp)
                              Case MessageEx('You have not selected any units to Despatch.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                          Else!If ~Records(logtemp)
                              !IF Despatch the SB2000 ticked!
                              IF tmp:Export_To_ServiceBase = 'Y'
                                !Fill in the Batch Header!
                                access:logexhe.primerecord()
                                LOG1:Processed = 'N'
                                LOG1:Date = TODAY()
                                LOG1:SMPF_No = tmp:smpfnumber
                                LOG1:ModelNumber = logsto:modelnumber
                                LOG1:Qty = Records(logtemp)
                                Access:LogExhe.Update()
                              END
                              Get(logsthis,0)
                              If access:logsthis.primerecord() = Level:Benign
                                  access:logstock.clearkey(logsto:refnumberkey)
                                  logsto:refnumber = tmp:RefNumber
                                  if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
                                      access:logstloc.clearkey(lstl:reflocationkey)
                                      lstl:refnumber = logsto:refnumber
                                      lstl:location  = tmp:location
                                      if access:logstloc.tryfetch(lstl:reflocationkey) = Level:Benign
                                          Get(logsthis,0)
                                          If access:logsthis.primerecord() = Level:Benign
      
                                              save_logtmp_id = access:logtemp.savefile()
                                              set(logtmp:imeikey)
                                              loop
                                                  if access:logtemp.next()
                                                     break
                                                  end !if
                                                  access:logserst.clearkey(logser:refnumberkey)
                                                  logser:refnumber = lstl:recordnumber
                                                  logser:esn       = logtmp:imei
                                                  if access:logserst.tryfetch(logser:refnumberkey) = Level:Benign
                                                      logser:status   = 'DESPATCHED'
                                                      logser:ClubNokia    = tmp:desclubnokia
                                                      access:logserst.update()
      
                                                      IF tmp:Export_To_ServiceBase = 'Y'
                                                        Access:LogExch.PrimeRecord()
                                                        xch1:Batch_Number = LOG1:Batch_No
                                                        xch1:Model_Number = logsto:modelnumber
                                                        xch1:Manufacturer = 'NOKIA'
                                                        xch1:ESN          = logtmp:imei
                                                        xch1:MSN          = ''
                                                        xch1:Colour       = ''
                                                        xch1:Location     = ''
                                                        xch1:Shelf_Location = ''
                                                        xch1:Date_Booked  = TODAY()
                                                        xch1:Times_Issued = CLOCK()
                                                        xch1:Available = 'AVAILABLE'
                                                        xch1:Job_Number = ''
                                                        xch1:Stock_Type = ''
                                                        Access:LogExch.Update()
                                                      END
                                                      Get(logsthii,0)
                                                      If access:logsthii.primerecord() = Level:benign
                                                          logsti:refnumber    = logsth:recordnumber
                                                          logsti:imei         = logtmp:imei
                                                          If access:logsthii.tryinsert()
                                                              access:logsthii.cancelautoinc()
                                                          End!If access:logsthii.tryinsert()
                                                      End!If access:logsthii.primerecord() = Level:benign
                                                  End!if access:logserst.tryfetch(logser:refnumberkey) = Level:Benign
                                              end !loop
                                              access:logtemp.restorefile(save_logtmp_id)
                                              get(logdesno,0)
                                              if access:logdesno.primerecord() = Level:Benign
                                                  access:logdesno.insert()
                                              End!if access:logdesno.primerecord() = Level:Benign
      
                                              logsth:refnumber        = tmp:refnumber
                                              logsth:stockout         = Records(logtemp)
                                              logsth:status           = 'DESPATCHED'
                                              logsth:clubnokia        = tmp:desclubnokia
                                              logsth:smpfnumber       = tmp:smpfnumber
                                              logsth:despatchnoteno   = tmp:deliverynote
                                              logsth:modelnumber      = logsto:modelnumber
                                              logsth:location         = tmp:location
                                              logsth:DespatchNo       = ldes:DespatchNo
                                              access:users.clearkey(use:password_key)
                                              use:password    = glo:password
                                              access:users.tryfetch(use:password_key)
                                              logsth:usercode     = use:user_code
      
      Compile('***',Debug=1)
          Message('ldes:DespatchNo: ' & ldes:DespatchNo,'Debug Message',icon:exclamation)
      ***
                                              logsth:date             = Today()
                                              If access:logsthis.insert()
                                                  access:logsthis.cancelautoinc()
                                              End!If access:logsthis.insert()
                                          End!If access:logsthis.primerecord() = Level:Benign
                                      End!if access:logstloc.tryfetch(lstl:reflocationkey) = Level:Benign
      
                                  End!if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
      
                              End!If access:logsthis.primerecord() = Level:Benign
                              WorkOutQty(tmp:refnumber)
                              glo:select1 = tmp:desclubnokia
                              LogDespatchNote(ldes:despatchno)
                              glo:select1 = ''
                              Post(Event:closewindow)
                          End!If Records(tmp:q_scanned)
                      Of 2 ! &No Button
                  End!Case MessageEx
              End!If error# = 0
      
          Else!If f_Type = 'DESPATCH'
              error# = 0
              If tmp:Refnumber2 = '' and error# = 0
                  Case MessageEx('You must select a location to transfer the items to.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Error# = 1
              End!If tmp:Refnumber2 = ''
              If tmp:tolocation = '' and error# = 0
                  Case MessageEx('You must select a Stock Location to Transfer to.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tmp:tolocation = '' and error# = 0
              If tmp:toclubnokia = '' and error# = 0
                  Case MessageEx('You must select a Club Nokia Site to Transfer to.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tmp:toclubnokia = '' and error# = 0
              If tmp:orderrefno = '' And error# = 0
                  Case MessageEx('You must insert an Order Ref. No.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tmp:orderrefno = '' And error# = 0
              If error# = 0
                  Case MessageEx('Are you sure you want to finish?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          If ~Records(logtemp)
                              Case MessageEx('You have not selected any units to Transfer.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                          Else!If Records(logtemp)
      
          !Returning The Associated Items To Stock
                              glo:file_name4  = 'RS' & Format(clock(),@n06) & '.DAT'
                              Remove(glo:file_name4)
                              access:logasssttemp.open()
                              access:logasssttemp.usefile()
                              save_logast_id = access:logassst.savefile()
                              access:logassst.clearkey(logast:refnumberkey)
                              logast:refnumber  = tmp:RefNumber
                              set(logast:refnumberkey,logast:refnumberkey)
                              loop
                                  if access:logassst.next()
                                     break
                                  end !if
                                  if logast:refnumber  <> tmp:RefNumber      |
                                      then break.  ! end if
                                  access:logassst_alias.clearkey(logast_alias:refnumberkey)
                                  logast_alias:refnumber  = tmp:refNumber2
                                  logast_alias:partnumber = logast:partnumber
                                  if access:logassst_alias.tryfetch(logast_alias:refnumberkey)
                                      get(logasssttemp,0)
                                      if access:logasssttemp.primerecord() = Level:benign
                                          set(logdeflt)
                                          access:logdeflt.next()
                                          logasttmp:partnumber   = logast:Partnumber
                                          logasttmp:description  = logast:description
                                          logasttmp:quantity     = Records(logtemp)
                                          logasttmp:refnumber    = logast:RecordNumber
                                          logasttmp:location     = ldef:location
                                          if access:logasssttemp.insert()
                                              access:logasssttemp.cancelautoinc()
                                          end
                                      End!if access:logasssttemp.primerecord() = Level:benign
                                  end
                              end !loop
                              access:logassst.restorefile(save_logast_id)
                              access:logasssttemp.close()
                              glo:select1 = ''
      
                              ReturnToStock
      
                              If glo:select1 <> 'OK'
                                  glo:select1 = ''
                              Else!If glo:select1 <> 'OK'
      
                                  access:logasssttemp.open()
                                  access:logasssttemp.usefile()
      
                                  save_logasttmp_id = access:logasssttemp.savefile()
                                  set(logasttmp:partnumberkey)
                                  loop
                                      if access:logasssttemp.next()
                                         break
                                      end !if
                                      access:logassst.clearkey(logast:recordnumberkey)
                                      logast:recordnumber = logasttmp:refnumber
                                      if access:logassst.tryfetch(logast:recordnumberkey) = Level:Benign
                                          access:stock.clearkey(sto:location_part_description_key)
                                          sto:location    = logasttmp:location
                                          sto:part_number = logast:partnumber
                                          sto:description = logast:description
      Compile('***',Debug=1)
          Message('sto:location: ' & sto:location & |
                  '|sto:part_number: ' & sto:part_number & |
                  '|sto:description: ' & sto:description,'Debug Message',icon:exclamation)
      ***
                                          if access:stock.tryfetch(sto:location_part_description_key)
                                              Case MessageEx('The following part does not exist in the location: '&clip(logasttmp:location)&':<13,10><13,10>Part Number: '&Clip(logast:partnumber)&'<13,10>Description: '&Clip(logast:description)&'<13,10><13,10>Click ''OK'' to add the part to stock.','ServiceBase 2000',|
                                                             'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                                  Of 1 ! &OK Button
                                              End!Case MessageEx
                                              get(stock,0)
                                              if access:stock.primerecord() = Level:benign
                                                  sto:sundry_item               = 'NO'
                                                  sto:part_number               = logast:partnumber
                                                  sto:description               = logast:description
                                                  sto:location                  = logasttmp:location
                                                  sto:quantity_stock            = logasttmp:quantity
                                                  if access:stock.insert()
                                                      access:stock.cancelautoinc()
                                                  end
                                                  get(stohist,0)
                                                  if access:stohist.primerecord() = level:benign
                                                      shi:ref_number           = sto:ref_number
                                                      access:users.clearkey(use:password_key)
                                                      use:password              = glo:password
                                                      access:users.fetch(use:password_key)
                                                      shi:user                  = use:user_code    
                                                      shi:date                 = today()
                                                      shi:transaction_type     = 'ADD'
                                                      shi:despatch_note_number = ''
                                                      shi:job_number           = ''
                                                      shi:quantity             = logasttmp:quantity
                                                      shi:purchase_cost        = sto:purchase_cost
                                                      shi:sale_cost            = sto:sale_cost
                                                      shi:retail_cost          = sto:retail_cost
                                                      shi:information          = 'TRANSFER FROM:' & |
                                                                                  '<13,10>' & Clip(tmp:location) & |
                                                                                  ' - ' & Clip(tmp:salescode) & |
                                                                                  ' - ' & Clip(tmp:clubnokia) & |
                                                                                  '<13,10,13,10>TRANSFER TO: ' & |
                                                                                  '<13,10>' & Clip(tmp:tolocation) & |
                                                                                  ' - ' & Clip(tmp:tosalescode) & |
                                                                                  ' - ' & Clip(tmp:toclubnokia)
                                                      shi:notes                = 'STOCK ADDED FROM LOGISTIC TRANSFER'
                                                      if access:stohist.insert()
                                                         access:stohist.cancelautoinc()
                                                      end
                                                  end!if access:stohist.primerecord() = level:benign
      
                                                  SaveRequest#    = Globalrequest
                                                  Globalrequest = Changerecord
                                                  updatestock
                                                  Globalrequest   = SaveRequest#
                                              End!if access:stock.primerecord() = Level:benign
                                          Else!if access:stock.tryfetch(sto:location_part_description_key)
                                              If sto:sundry_item <> 'YES'
                                                  sto:quantity_stock += logasttmp:quantity
                                                  access:stock.update()
                                                  get(stohist,0)
                                                  if access:stohist.primerecord() = level:benign
                                                      shi:ref_number           = sto:ref_number
                                                      access:users.clearkey(use:password_key)
                                                      use:password              = glo:password
                                                      access:users.fetch(use:password_key)
                                                      shi:user                  = use:user_code    
                                                      shi:date                 = today()
                                                      shi:transaction_type     = 'ADD'
                                                      shi:despatch_note_number = ''
                                                      shi:job_number           = ''
                                                      shi:quantity             = logasttmp:quantity
                                                      shi:purchase_cost        = sto:purchase_cost
                                                      shi:sale_cost            = sto:sale_cost
                                                      shi:retail_cost          = sto:retail_cost
                                                      shi:information          = 'TRANSFER FROM:' & |
                                                                                  '<13,10>' & Clip(tmp:location) & |
                                                                                  ' - ' & Clip(tmp:salescode) & |
                                                                                  ' - ' & Clip(tmp:clubnokia) & |
                                                                                  '<13,10,13,10>TRANSFER TO: ' & |
                                                                                  '<13,10>' & Clip(tmp:tolocation) & |
                                                                                  ' - ' & Clip(tmp:tosalescode) & |
                                                                                  ' - ' & Clip(tmp:toclubnokia)
                                                      shi:notes                = 'STOCK ADDED FROM LOGISTIC TRANSFER'
                                                      if access:stohist.insert()
                                                         access:stohist.cancelautoinc()
                                                      end
                                                  end!if access:stohist.primerecord() = level:benign
                                              End!If sto:sundry_item <> 'YES'
      
                                          End!if access:stock.tryfetch(sto:location_part_description_key)
      
                                      End!if access:logassst.tryfetch(logast:recordnumberkey) = Level:Benign
                                  end !loop
                                  access:logasssttemp.restorefile(save_logasttmp_id)
                                  access:logasssttemp.close()
      
                                  Remove(glo:file_name4)
      
                                  Get(logsthis,0)
                                  If access:logsthis.primerecord() = Level:Benign
                                      FromNo#           = logsth:recordnumber
                                      access:logstock.clearkey(logsto:refnumberkey)
                                      logsto:refnumber = tmp:RefNumber
                                      if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
                                      End!if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
                                      logsth:RefNumber    = tmp:refnumber
                                      logsth:Date         = Today()
                                      logsth:StockOut      = Records(logtemp)
                                      logsth:Status       = 'TRANSFER'
                                      logsth:ClubNokia    = tmp:clubnokia
                                      logsth:ModelNumber  = logsto:ModelNumber
                                      logsth:location     = tmp:tolocation
                                      access:users.clearkey(use:password_key)
                                      use:password    = glo:password
                                      access:users.tryfetch(use:password_key)
                                      logsth:usercode     = use:user_code
      
                                      If access:logsthis.tryinsert()
                                          access:logsthis.cancelautoinc()
                                      End!If access:logsthis.tryinsert().
                                  End!If access:logsthis.primerecord() = Level:Benign
                                  Get(logsthis,0)
                                  If access:logsthis.primerecord() = Level:Benign
                                      ToNo#             = logsth:RecordNumber
                                      logsth:RefNumber    = tmp:refnumber2
                                      access:logstock.clearkey(logsto:refnumberkey)
                                      logsto:refnumber = tmp:RefNumber2
                                      if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
      
                                      End!if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
                                      logsth:Date         = Today()
                                      logsth:StockIn      = Records(logtemp)
                                      logsth:Status       = 'TRANSFER'
                                      logsth:ClubNokia    = tmp:toclubnokia
                                      logsth:ModelNumber  = logsto:ModelNumber
                                      logsth:location     = tmp:tolocation
                                      logsth:orderrefno   = tmp:orderrefno
                                      access:users.clearkey(use:password_key)
                                      use:password    = glo:password
                                      access:users.tryfetch(use:password_key)
                                      logsth:usercode     = use:user_code
      
                                      If access:logsthis.tryinsert()
                                          access:logsthis.cancelautoinc()
                                      End!If access:logsthis.tryinsert()
                                  End!If access:logsthis.primerecord() = Level:Benign
                                  save_logtmp_id = access:logtemp.savefile()
                                  set(logtmp:imeikey)
                                  loop
                                      if access:logtemp.next()
                                         break
                                      end !if
                                      get(logserst,0)
                                      if access:logserst.primerecord() = Level:benign
      
                                          access:logstloc.clearkey(lstl:reflocationkey)
                                          lstl:refnumber = tmp:refnumber2
                                          lstl:location  = tmp:tolocation
                                          if access:logstloc.tryfetch(lstl:reflocationkey) = Level:Benign
                                              logser:refnumber    = lstl:recordnumber
                                              logser:esn         = logtmp:imei
                                              logser:status       = 'AVAILABLE'
                                              logser:clubnokia    = tmp:toclubnokia
                                              if access:logserst.tryinsert()
                                                 access:logserst.cancelautoinc()
                                              end
                                          End!if access:logstloc.tryfetch(lstl:reflocationkey) = Level:Benign
      
                                          Get(logsthii,0)
                                          If access:logsthii.primerecord() = Level:Benign
                                              logsti:RefNumber    = ToNo#
                                              logsti:IMEI         = logtmp:imei
                                              If access:logsthii.tryinsert()
                                                  access:logsthii.cancelautoinc()
                                              End!If access:logsthii.tryinsert()
                                          End!If access:logsthii.primerecord() = Level:Benign
                                      End!if access:logserst.primerecord() = Level:benign
                                      access:logstloc.clearkey(lstl:reflocationkey)
                                      lstl:refnumber = tmp:refnumber
                                      lstl:location  = tmp:location
                                      if access:logstloc.tryfetch(lstl:reflocationkey) = Level:Benign
                                          access:logserst.clearkey(logser:RefNumberKey)
                                          logser:RefNumber = lstl:recordnumber
                                          logser:esn       = logtmp:imei
                                          if access:logserst.tryfetch(logser:RefNumberKey) = Level:Benign
                                              Delete(logserst)
                                          End!if access:logserst.tryfetch(logser:RefNumberKey) = Level:Benign
                                          Get(logsthii,0)
                                          If access:logsthii.primerecord() = Level:Benign
                                              logsti:RefNumber    = FromNo#
                                              logsti:IMEI         = logtmp:imei
                                              If access:logsthii.tryinsert()
                                                  access:logsthii.cancelautoinc()
                                              End!If access:logsthii.tryinsert()
                                          End!If access:logsthii.primerecord() = Level:Benign
      
                                      End!if access:logstloc.tryfetch(lstl:reflocationkey) = Level:Benign
      
                                  end !loop
                                  access:logtemp.restorefile(save_logtmp_id)
                                  Post(Event:closewindow)
      
                              End!If glo:select1 <> 'OK'
      
                          End!If Records(tmp:q_scanned)
                      Of 2 ! &No Button
                  End!Case MessageEx
              End!If error# = 0
      
          End!If f_Type = 'DESPATCH'
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'DespatchStock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Wizard,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet2
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,Field())
    OF ?Sheet2:2
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW12.ResetFromView PROCEDURE

tmp:totalstock:Cnt   LONG
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:LOGSERST.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:totalstock:Cnt += 1
  END
  tmp:totalstock = tmp:totalstock:Cnt
  PARENT.ResetFromView
  Relate:LOGSERST.SetQuickScan(0)
  SETCURSOR()


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(12, ValidateRecord, (),BYTE)
  IF logalloc_no > 0
    !Show all!
    IF (Upper(logser:Status) = 'AVAILABLE' OR Upper(logser:Status) = 'ALLOCATED') And Upper(logser:ClubNokia) = Upper(tmp:clubnokia)
       RETURN Record:OK
    ELSE
       RETURN Record:Filtered
    END
  ELSE
    IF Upper(logser:Status) = 'AVAILABLE' And Upper(logser:ClubNokia) = Upper(tmp:clubnokia)
       RETURN Record:OK
    ELSE
       RETURN Record:Filtered
    END
  END
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(12, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW18.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW18.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW20.ResetFromView PROCEDURE

tmp:totalDespatch:Cnt LONG
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:LOGTEMP.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:totalDespatch:Cnt += 1
  END
  tmp:totalDespatch = tmp:totalDespatch:Cnt
  PARENT.ResetFromView
  Relate:LOGTEMP.SetQuickScan(0)
  SETCURSOR()


BRW20.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW20.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end

