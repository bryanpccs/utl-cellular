   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
INCLUDE('MSGEX.INC')

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('CUSTRBC.CLW')
DctInit     PROCEDURE
DctKill     PROCEDURE
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('CUSTR001.CLW')
Main                   PROCEDURE   !
     END
          MODULE('MsgEx Runtime Library')
            MessageEx(<STRING TheText>,      |
                      <STRING TheTitel>,     |
                      <STRING TheImage>,     |
                      <STRING TheButtons>,   |
                       SIGNED DefButton=0,   |
                       SIGNED EscButton=0,   |
                      <STRING CheckText>,    |
                      <*?     CheckVar>,     |
                      <STRING Fontname>,     |
                      SIGNED  Fontsize=8,    |
                      LONG    Fontcolor=0,   |
                      SIGNED  Fontstyle=0,   |
                      LONG    Charset=0,     |
                      LONG    BackColor=COLOR:NONE, |
                      <STRING Wallpaper>,    |
                      LONG    WallMode=0,    |
                      <STRING Sound>,        |
                      LONG    Options=0,     |
                      SIGNED  MinWidth=0,    |
                      SIGNED  MinHeight=0,   |
                      LONG    TimeOut=0      ),SIGNED,PROC,NAME('MESSAGEEX')
          END
   END

SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

REPEXTTP             FILE,DRIVER('Btrieve'),OEM,NAME('REPEXTTP.DAT'),PRE(RPT),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(RPT:RecordNumber),NOCASE,PRIMARY
ReportTypeKey            KEY(RPT:ReportType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReportType                  STRING(80)
                         END
                     END                       

REPEXTRP             FILE,DRIVER('Btrieve'),OEM,NAME('REPEXTRP.DAT'),PRE(REX),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(REX:RecordNumber),NOCASE,PRIMARY
ReportNameKey            KEY(REX:ReportType,REX:ReportName),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReportType                  STRING(80)
ReportName                  STRING(80)
EXEName                     STRING(30)
                         END
                     END                       

REPNAME              FILE,DRIVER('Btrieve'),OEM,NAME('REPNAME.DAT'),PRE(REP),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(REP:RecordNumber),NOCASE,PRIMARY
ReportNameKey            KEY(REP:CustomerName,REP:ReportType,REP:ReportName),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
CustomerName                STRING(30)
IDField                     LONG
ReportType                  STRING(30)
ReportName                  STRING(80)
EXEName                     STRING(30)
                         END
                     END                       



Access:REPEXTTP      &FileManager
Relate:REPEXTTP      &RelationManager
Access:REPEXTRP      &FileManager
Relate:REPEXTRP      &RelationManager
Access:REPNAME       &FileManager
Relate:REPNAME       &RelationManager
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE(0),THREAD
GlobalResponse       BYTE(0),THREAD
VCRRequest           LONG(0),THREAD
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  DctInit
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('custrep.INI')
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  DctKill
  GlobalErrors.Kill


