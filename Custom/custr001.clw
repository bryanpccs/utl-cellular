

   MEMBER('custrep.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CUSTR001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

progress:thermometer BYTE
tmp:Password         STRING(30)
window               WINDOW('Custom Report Activation'),AT(,,220,111),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),WALLPAPER('cloud_b.gif'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,76),USE(?Sheet1),COLOR(COLOR:Silver),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('This utility will activate your Custom Reports.'),AT(8,8,204,12),USE(?Prompt1),CENTER,FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Insert Password, then click ''OK'''),AT(8,24,204,12),USE(?Prompt2),CENTER
                           PROMPT('Password'),AT(12,44),USE(?tmp:Password:Prompt),TRN
                           ENTRY(@s30),AT(84,44,124,10),USE(tmp:Password),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,PASSWORD
                           PROGRESS,USE(progress:thermometer),AT(8,68,204,8),RANGE(0,100)
                         END
                       END
                       PANEL,AT(4,84,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,88,56,16),USE(?Button2),FLAT,LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,88,56,16),USE(?Cancel),FLAT,LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:REPEXTRP.Open
  Relate:REPNAME.Open
  Access:REPEXTTP.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPEXTRP.Close
    Relate:REPNAME.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button2
      ThisWindow.Update
      !Check Password
      continue# = 1
      If tmp:password = ''
          Select(?tmp:Password)
          continue# = 0
      End!If tmp:password = ''
          Count# = 0
          Set(REPEXTTP,0)
          Loop
              If Access:REPEXTTP.NEXT()
                 Break
              End !If
              Relate:REPEXTTP.Delete(0)
          End !Loop
      
          recordspercycle         = 25
          recordsprocessed        = 0
          percentprogress         = 0
          progress:thermometer    = 0
      
          recordstoprocess    = Records(REPNAME)
      
          Access:REPNAME.Clearkey(rep:RecordNumberKey)
          Set(rep:RecordNumberKey)
          Loop
              If Access:REPNAME.Next()
                  Break
              End!If Access:REPNAME.Next()
              Do GetNextRecord2
      
              If rep:IDField = INT((tmp:Password/2.5) * 9.7)
                  Access:REPEXTTP.ClearKey(RPT:ReportTypeKey)
                  RPT:ReportType = REP:ReportType
                  If Access:REPEXTTP.TryFetch(RPT:ReportTypeKey)
                      If access:REPEXTTP.primerecord() = Level:Benign
                          rpt:ReportType  = REP:ReportType
                          If access:REPEXTTP.tryinsert()
                              access:REPEXTTP.cancelautoinc()
                          End!If access:REPEXTTP.tryinsert()
                      End!If access:REPEXTTP.primerecord() = Level:Benign
                  End!If Access:REPEXTTP.TryFetch(RPT:ReportTypeKey) = Level:Benign
      
                  Access:REPEXTRP.ClearKey(REX:ReportNameKey)
                  REX:ReportType = rep:ReportType
                  REX:ReportName = rep:ReportName
                  If Access:REPEXTRP.TryFetch(REX:ReportNameKey) = Level:Benign
                      REX:EXEName = REP:EXEName
                      Access:REPEXTRP.Update()
                  Else!If Access:REPEXTRP.TryFetch(REX:ReportNameKey) = Level:Benign
                      If access:REPEXTRP.primerecord() = Level:Benign
                          REX:ReportType  = rep:ReportType
                          REX:ReportName  = rep:ReportName
                          REX:EXEName     = rep:EXEName
                          If access:REPEXTRP.tryinsert()
                              access:REPEXTRP.cancelautoinc()
                          End!If access:REPEXTRP.tryinsert()
                      End!If access:REPEXTRP.primerecord() = Level:Benign
                  End!If Access:REPEXTRP.TryFetch(REX:ReportNameKey) = Level:Benign
                  count# += 1
              End!If rep:IDField = INT((tmp:Password/2.5) * 9.7)
          End!Loop
      
      
          Do EndPrintRun
      
          If count# = 0
              Case MessageEx('Error. Please check your password and retry.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Select(?tmp:Password)
          Else!If count# = 0
              Case MessageEx('Custom Reports Updated.','ServiceBase 2000',|
                             'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Post(Event:CloseWindow)
      
          End!If count# = 0
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

