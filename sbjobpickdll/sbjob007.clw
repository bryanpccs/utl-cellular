

   MEMBER('sbjobpickdll.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJOB007.INC'),ONCE        !Local module procedure declarations
                     END


OldPickingNoteExport PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'OldPickingNoteExport')      !Add Procedure to Log
  end


!        savepath = path()
!        set(defaults)
!        access:defaults.next()
!        If def:exportpath <> ''
!            glo:file_name = Clip(def:exportpath) & '\PICKNOTE.CSV'
!        Else!If def:exportpath <> ''
!            glo:file_name = 'C:\PICKNOTE.CSV'
!        End!If def:exportpath <> ''
!
!        if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
!                    file:keepdir + file:noerror + file:longname)
!            !failed
!            setpath(savepath)
!        else!if not filedialog
!            !found
!            setpath(savepath)
!            Remove(glo:file_name)
!            access:expgen.open()
!            access:expgen.usefile()
!
!            Clear(gen:record)
!            gen:line1   = 'Picking Note Export'
!            access:expgen.insert()
!
!            Clear(gen:record)
!            gen:line1   = 'Job Number,Date Completed,Request No,Eng,Part Ref,Description,Quantity,Part Ref,Description,Quantity,Part Ref,Description,Quantity,Part Ref,Description,Quantity,Part Ref,Description,Quantity,'
!            access:expgen.insert()
!            count# = 0
!!            tmp:costtotal = 0
!!            tmp:saletotal = 0
!            setcursor(cursor:wait)
!            set(JOBS)
!            loop while access:JOBS.next() = level:benign
!                access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
!                pnt:JobReference = job:ref_number
!                set(pnt:keyonjobno,pnt:keyonjobno)
!                    Clear(gen:record)
!                    gen:line1   = Clip(job:ref_number)
!                    gen:line1   = CLip(gen:line1) & ',' & CLip(res:description)
!                    gen:line1   = Clip(gen:line1) & ',' & Clip(Round(res:purchase_cost,.01))
!                    gen:line1   = Clip(gen:line1) & ',' & Clip(Round(res:item_cost,.01))
!                    gen:line1   = Clip(gen:line1) & ',' & CLip(res:quantity)
!                    gen:line1   = CLip(gen:line1) & ',' & Clip(Round(Round(res:item_cost,.01) * Round(res:quantity,.01),.01))
!                    gen:line1   = Clip(gen:line1) & ',' & CLip(ret:account_number)
!                    gen:line1   = Clip(gen:line1) & ',' & Clip(Format(ret:date_booked,@d6))
!                    gen:line1   = Clip(gen:line1) & ',' & 'Retail'
!                    access:expgen.insert()
!                    count# += 1
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!            end !loop
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!            save_ret_id = access:retsales.savefile()
!            access:retsales.clearkey(ret:date_booked_key)
!            ret:date_booked = tmp:startdate
!            set(ret:date_booked_key,ret:date_booked_key)
!            loop
!                if access:retsales.next()
!                   break
!                end !if
!                if ret:date_booked > tmp:endDate      |
!                    then break.  ! end if
!                Sort(glo:queue,glo:pointer)
!                glo:pointer = ret:account_number
!                Get(glo:queue,glo:pointer)
!                If Error()
!                    Cycle
!                End!If Error()
!
!                save_res_id = access:retstock.savefile()
!                access:retstock.clearkey(res:part_number_key)
!                res:ref_number  = ret:ref_number
!                set(res:part_number_key,res:part_number_key)
!                loop
!                    if access:retstock.next()
!                       break
!                    end !if
!                    if res:ref_number  <> ret:ref_number      |
!                        then break.  ! end if
!                    If res:Despatched <> 'YES'
!                        Cycle
!                    End !If res:Despatched <> 'YES'
!
!                    Sort(glo:queue2,glo:pointer2)
!                    glo:pointer2    = res:part_number
!                    Get(glo:queue2,glo:pointer2)
!                    If Error()
!                        Cycle
!                    End!If Error()
!
!                   ! Start Change 2279 BE(04/03/03)
!                   !If tmp:anylocation <> 1
!                   ! End Change 2279 BE(04/03/03)
!                        If res:part_ref_number <> ''
!                            access:stock.clearkey(sto:ref_number_key)
!                            sto:ref_number   = res:part_ref_number
!                            If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
!                                ! Start Change 2279 BE(04/03/03)
!                                !    If sto:location <> tmp:sitelocation
!                                !        Cycle
!                                !    End!If sto:location <> tmp:sitelocation
!                                SORT(glo:queue3, glo:pointer3)
!                                glo:pointer3 = sto:location
!                                GET(glo:queue3, glo:pointer3)
!                                IF (ERROR()) THEN
!                                    CYCLE
!                                END
!                                ! End Change 2279 BE(04/03/03)
!                            End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
!                        End!If tmp:anylocation <> 1
!                    ! Start Change 2279 BE(04/03/03)
!                    !End!If tmp:anylocation <> 1
!                    ! end Change 2279 BE(04/03/03)
!
!                    Clear(gen:record)
!                    gen:line1   = Clip(res:part_number)
!                    gen:line1   = CLip(gen:line1) & ',' & CLip(res:description)
!                    gen:line1   = Clip(gen:line1) & ',' & Clip(Round(res:purchase_cost,.01))
!                    gen:line1   = Clip(gen:line1) & ',' & Clip(Round(res:item_cost,.01))
!                    gen:line1   = Clip(gen:line1) & ',' & CLip(res:quantity)
!                    gen:line1   = CLip(gen:line1) & ',' & Clip(Round(Round(res:item_cost,.01) * Round(res:quantity,.01),.01))
!                    gen:line1   = Clip(gen:line1) & ',' & CLip(ret:account_number)
!                    gen:line1   = Clip(gen:line1) & ',' & Clip(Format(ret:date_booked,@d6))
!                    gen:line1   = Clip(gen:line1) & ',' & 'Retail'
!                    access:expgen.insert()
!                    count# += 1
!                    tmp:costtotal += Round(Round(res:item_cost,.01) * Round(res:quantity,.01),.01)
!                end !loop
!                access:retstock.restorefile(save_res_id)
!
!            end !loop
!            access:retsales.restorefile(save_ret_id)
!            setcursor()
!            If count# <> 0
!                Clear(gen:record)
!                gen:line1   = 'Totals:, ' & Clip(count#) & ',,,,' & Clip(tmp:costtotal)
!                access:expgen.insert()
!            End!If count# <> 0
!
!            access:expgen.close()
!            Case MessageEx('Export Completed.','ServiceBase 2000',|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!            End!Case MessageEx
!        end!if not filedialog
!    End!If tmp:retail = 0 And tmp:service = 0


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'OldPickingNoteExport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


