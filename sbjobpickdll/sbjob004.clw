

   MEMBER('sbjobpickdll.clw')                         ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJOB004.INC'),ONCE        !Local module procedure declarations
                     END


UpdatePICKNOTE PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::pnt:Record  LIKE(pnt:RECORD),STATIC
QuickWindow          WINDOW('Update the PICKNOTE File'),AT(,,358,182),FONT('MS Sans Serif',8,,),IMM,HLP('UpdatePICKNOTE'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,350,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Pick Note Ref:'),AT(8,20),USE(?pnt:PickNoteRef:Prompt)
                           ENTRY(@n-14),AT(80,20,64,10),USE(pnt:PickNoteRef),RIGHT(1)
                           PROMPT('Job Reference:'),AT(8,34),USE(?pnt:JobReference:Prompt)
                           ENTRY(@n-14),AT(80,34,64,10),USE(pnt:JobReference),RIGHT(1)
                           PROMPT('Pick Note Number:'),AT(8,48),USE(?pnt:PickNoteNumber:Prompt)
                           ENTRY(@n-14),AT(80,48,64,10),USE(pnt:PickNoteNumber),RIGHT(1)
                           PROMPT('Engineer Code:'),AT(8,62),USE(?pnt:EngineerCode:Prompt)
                           ENTRY(@s3),AT(80,62,40,10),USE(pnt:EngineerCode)
                           PROMPT('Location:'),AT(8,76),USE(?pnt:Location:Prompt)
                           ENTRY(@s30),AT(80,76,124,10),USE(pnt:Location)
                           PROMPT('Is Printed:'),AT(8,90),USE(?pnt:IsPrinted:Prompt)
                           ENTRY(@n3),AT(80,90,40,10),USE(pnt:IsPrinted)
                           PROMPT('Printed By:'),AT(8,104),USE(?pnt:PrintedBy:Prompt)
                           ENTRY(@s3),AT(80,104,40,10),USE(pnt:PrintedBy)
                           PROMPT('Printed Date:'),AT(8,118),USE(?pnt:PrintedDate:Prompt)
                           ENTRY(@d17),AT(80,118,104,10),USE(pnt:PrintedDate)
                           PROMPT('Printed Time:'),AT(8,132),USE(?pnt:PrintedTime:Prompt)
                           ENTRY(@t7),AT(80,132,104,10),USE(pnt:PrintedTime)
                           PROMPT('Is Confirmed:'),AT(8,146),USE(?pnt:IsConfirmed:Prompt)
                           ENTRY(@n3),AT(80,146,40,10),USE(pnt:IsConfirmed)
                         END
                         TAB('General (cont.)'),USE(?Tab:2)
                           PROMPT('Confirmed By:'),AT(8,20),USE(?pnt:ConfirmedBy:Prompt)
                           ENTRY(@s3),AT(80,20,40,10),USE(pnt:ConfirmedBy)
                           PROMPT('Confirmed Date:'),AT(8,34),USE(?pnt:ConfirmedDate:Prompt)
                           ENTRY(@d17),AT(80,34,104,10),USE(pnt:ConfirmedDate)
                           PROMPT('Confirmed Time:'),AT(8,48),USE(?pnt:ConfirmedTime:Prompt)
                           ENTRY(@t7),AT(80,48,104,10),USE(pnt:ConfirmedTime)
                           PROMPT('Confirmed Notes:'),AT(8,62),USE(?pnt:ConfirmedNotes:Prompt)
                           ENTRY(@s255),AT(80,62,270,10),USE(pnt:ConfirmedNotes)
                           PROMPT('Usercode:'),AT(8,76),USE(?pnt:Usercode:Prompt)
                           ENTRY(@s3),AT(80,76,40,10),USE(pnt:Usercode)
                         END
                       END
                       BUTTON('OK'),AT(211,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(260,164,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(309,164,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdatePICKNOTE',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdatePICKNOTE',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdatePICKNOTE',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:PickNoteRef:Prompt;  SolaceCtrlName = '?pnt:PickNoteRef:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:PickNoteRef;  SolaceCtrlName = '?pnt:PickNoteRef';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:JobReference:Prompt;  SolaceCtrlName = '?pnt:JobReference:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:JobReference;  SolaceCtrlName = '?pnt:JobReference';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:PickNoteNumber:Prompt;  SolaceCtrlName = '?pnt:PickNoteNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:PickNoteNumber;  SolaceCtrlName = '?pnt:PickNoteNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:EngineerCode:Prompt;  SolaceCtrlName = '?pnt:EngineerCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:EngineerCode;  SolaceCtrlName = '?pnt:EngineerCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:Location:Prompt;  SolaceCtrlName = '?pnt:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:Location;  SolaceCtrlName = '?pnt:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:IsPrinted:Prompt;  SolaceCtrlName = '?pnt:IsPrinted:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:IsPrinted;  SolaceCtrlName = '?pnt:IsPrinted';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:PrintedBy:Prompt;  SolaceCtrlName = '?pnt:PrintedBy:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:PrintedBy;  SolaceCtrlName = '?pnt:PrintedBy';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:PrintedDate:Prompt;  SolaceCtrlName = '?pnt:PrintedDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:PrintedDate;  SolaceCtrlName = '?pnt:PrintedDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:PrintedTime:Prompt;  SolaceCtrlName = '?pnt:PrintedTime:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:PrintedTime;  SolaceCtrlName = '?pnt:PrintedTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:IsConfirmed:Prompt;  SolaceCtrlName = '?pnt:IsConfirmed:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:IsConfirmed;  SolaceCtrlName = '?pnt:IsConfirmed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:ConfirmedBy:Prompt;  SolaceCtrlName = '?pnt:ConfirmedBy:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:ConfirmedBy;  SolaceCtrlName = '?pnt:ConfirmedBy';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:ConfirmedDate:Prompt;  SolaceCtrlName = '?pnt:ConfirmedDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:ConfirmedDate;  SolaceCtrlName = '?pnt:ConfirmedDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:ConfirmedTime:Prompt;  SolaceCtrlName = '?pnt:ConfirmedTime:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:ConfirmedTime;  SolaceCtrlName = '?pnt:ConfirmedTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:ConfirmedNotes:Prompt;  SolaceCtrlName = '?pnt:ConfirmedNotes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:ConfirmedNotes;  SolaceCtrlName = '?pnt:ConfirmedNotes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:Usercode:Prompt;  SolaceCtrlName = '?pnt:Usercode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:Usercode;  SolaceCtrlName = '?pnt:Usercode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a PICKNOTE Record'
  OF ChangeRecord
    ActionMessage = 'Changing a PICKNOTE Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdatePICKNOTE')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdatePICKNOTE')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?pnt:PickNoteRef:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(pnt:Record,History::pnt:Record)
  SELF.AddHistoryField(?pnt:PickNoteRef,1)
  SELF.AddHistoryField(?pnt:JobReference,2)
  SELF.AddHistoryField(?pnt:PickNoteNumber,3)
  SELF.AddHistoryField(?pnt:EngineerCode,4)
  SELF.AddHistoryField(?pnt:Location,5)
  SELF.AddHistoryField(?pnt:IsPrinted,6)
  SELF.AddHistoryField(?pnt:PrintedBy,7)
  SELF.AddHistoryField(?pnt:PrintedDate,8)
  SELF.AddHistoryField(?pnt:PrintedTime,9)
  SELF.AddHistoryField(?pnt:IsConfirmed,10)
  SELF.AddHistoryField(?pnt:ConfirmedBy,11)
  SELF.AddHistoryField(?pnt:ConfirmedDate,12)
  SELF.AddHistoryField(?pnt:ConfirmedTime,13)
  SELF.AddHistoryField(?pnt:ConfirmedNotes,14)
  SELF.AddHistoryField(?pnt:Usercode,15)
  SELF.AddUpdateFile(Access:PICKNOTE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:PICKNOTE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PICKNOTE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PICKNOTE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdatePICKNOTE',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdatePICKNOTE')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

