

   MEMBER('sbjobpickdll.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJOB011.INC'),ONCE        !Local module procedure declarations
                     END


ReprintPartReceivedPickNote PROCEDURE  (arg:JobNumber, arg:PartRefNumber, arg:IsOrderReceived) ! Declare Procedure
PickNoteFound        BYTE
SavePickNoteRef      LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'ReprintPartReceivedPickNote')      !Add Procedure to Log
  end


! Routine Added Change 4699 BE(22/09/2004)

    PickNoteFound = false

    access:PICKNOTE.clearkey(pnt:keyonjobno)
    pnt:JobReference = arg:JobNumber
    SET(pnt:keyonjobno,pnt:keyonjobno)
    LOOP WHILE NOT PickNoteFound
        IF ((access:picknote.next() <> Level:Benign) OR |
            (pnt:JobReference <> arg:JobNumber)) THEN
            BREAK
        END

        access:pickdet.clearkey(pdt:keyonpicknote)
        pdt:PickNoteRef = pnt:PickNoteRef
        SET(pdt:keyonpicknote,pdt:keyonpicknote)
        LOOP
            IF ((access:pickdet.next() <> Level:Benign) OR |
                 (pdt:PickNoteRef <> pnt:PickNoteRef)) THEN
                BREAK
            END

            IF (pdt:IsDelete) THEN
                CYCLE
            END

            ! Start Change 4803 BE(11/10/2004)
            ! if PickNote hasn't already been printed then
            ! print it now.
            !IF (NOT pnt:IsPrinted) THEN
            !    CYCLE
            !END
            ! End Change 4803 BE(11/10/2004)

            IF ((pdt:StockPartRefNumber = arg:PartRefNumber) AND |
                (NOT pdt:IsInstock)) THEN
                ! mark picknote as found but keep on looking because the pick note reprint
                ! should be for the most recent reference 
                PickNoteFound     = true
                SavePickNoteRef   = pnt:PickNoteRef
                BREAK
            END
        END
    END

    IF (PickNoteFound) THEN
        access:picknote.clearkey(pnt:keypicknote)
        pnt:PickNoteRef = SavePickNoteRef
        IF (access:picknote.fetch(pnt:keypicknote) = Level:Benign) THEN

            ! Lop through and mark all the detail records for this part
            ! as received stock.
            access:pickdet.clearkey(pdt:keyonpicknote)
            pdt:PickNoteRef = pnt:PickNoteRef
            SET(pdt:keyonpicknote,pdt:keyonpicknote)
            LOOP
                IF ((access:pickdet.next() <> Level:Benign) OR |
                     (pdt:PickNoteRef <> pnt:PickNoteRef)) THEN
                    BREAK
                END

                IF (pdt:IsDelete) THEN
                    CYCLE
                END

                IF ((pdt:StockPartRefNumber = arg:PartRefNumber) AND |
                    (NOT pdt:IsInstock)) THEN
                    pdt:IsPrinted = true      ! This field not used (Pnt:IsPrinted has this function)
                                              ! So use this field to indicate Part Received Status
                    access:pickdet.update()
                END

            END

            ! Start Change 4803 BE(12/10/2004)
            ! Print The report
            !PickReport()
            IF (pnt:IsPrinted) THEN
                ! Re-Print The report
                PickReport()
            ELSE
                ! Print the Report
                access:users.clearkey(use:password_key)
                use:password = glo:Password
                access:users.fetch(use:password_key)
                pnt:Printedby = use:user_code
                pnt:PrintedDate = TODAY()
                pnt:PrintedTime = CLOCK()
                access:PICKNOTE.update()
                PickReport()
                pnt:IsPrinted = true
                access:PICKNOTE.update()

                ! Start Change 4801 BE(12/10/2004)
                IF (access:audit.primerecord() = level:Benign) THEN
                    aud:ref_number    = pnt:JobReference
                    aud:date          = today()
                    aud:time          = clock()
                    aud:type          = 'JOB'
                    aud:user          = pnt:PrintedBy
                    aud:action        = 'PICKNOTE PRINTED'
                    IF (arg:IsOrderReceived) THEN
                        aud:notes     = 'Pick Note Number ' & LEFT(pnt:PickNoteRef) & |
                                        ' created automatically.<13,10>Part Order Received'
                    ELSE
                        aud:notes     = 'Pick Note Number ' & LEFT(pnt:PickNoteRef) & |
                                        ' created automatically.<13,10>Pending Order Fulfilled.'
                    END
                    IF (access:audit.insert() <> Level:Benign) THEN
                        access:audit.cancelautoinc()
                    END
                END
                ! End Change 4801 BE(12/10/2004)

            END
            ! End Change 4803 BE(12/10/2004)

            ! Loop through and mark all the detail records for this part
            ! as now in stock.
            access:picknote.clearkey(pnt:keypicknote)
            pnt:PickNoteRef = SavePickNoteRef
            IF (access:picknote.fetch(pnt:keypicknote) = Level:Benign) THEN
                ! Start Change 4803 BE(11/10/2004)
                ! if neccessary mark pick note as printed
                IF (NOT pnt:IsPrinted) THEN
                    pnt:IsPrinted = true
                    access:picknote.update()
                END
                ! End Change 4803 BE(11/10/2004)
                access:pickdet.clearkey(pdt:keyonpicknote)
                pdt:PickNoteRef = pnt:PickNoteRef
                SET(pdt:keyonpicknote,pdt:keyonpicknote)
                LOOP
                    IF ((access:pickdet.next() <> Level:Benign) OR |
                         (pdt:PickNoteRef <> pnt:PickNoteRef)) THEN
                        BREAK
                    END

                    IF (pdt:IsDelete) THEN
                        CYCLE
                    END

                    IF ((pdt:StockPartRefNumber = arg:PartRefNumber) AND |
                        (NOT pdt:IsInstock)) THEN
                        pdt:IsInStock = true      ! Indicate part is now in stock so it is not marked as Received
                                                  ! when pick note is next reprinted.
                                                  ! leave pdt:IsPrinted true in case it is ever necessary
                                                  ! to distinguish those parts that were originally out of stock
                        access:pickdet.update()
                    END

                END
            END
        END
    END




SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ReprintPartReceivedPickNote',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('PickNoteFound',PickNoteFound,'ReprintPartReceivedPickNote',1)
    SolaceViewVars('SavePickNoteRef',SavePickNoteRef,'ReprintPartReceivedPickNote',1)


