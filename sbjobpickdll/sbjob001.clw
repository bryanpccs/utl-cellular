

   MEMBER('sbjobpickdll.clw')                         ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJOB001.INC'),ONCE        !Local module procedure declarations
                     END


UpdatePICKDET PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::pdt:Record  LIKE(pdt:RECORD),STATIC
QuickWindow          WINDOW('Update the PICKDET File'),AT(,,358,182),FONT('MS Sans Serif',8,,),IMM,HLP('UpdatePICKDET'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,350,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Pick Detail Ref:'),AT(8,20),USE(?pdt:PickDetailRef:Prompt)
                           ENTRY(@n-14),AT(76,20,64,10),USE(pdt:PickDetailRef),RIGHT(1)
                           PROMPT('Pick Note Ref:'),AT(8,34),USE(?pdt:PickNoteRef:Prompt)
                           ENTRY(@n-14),AT(76,34,64,10),USE(pdt:PickNoteRef),RIGHT(1)
                           PROMPT('Part Ref Number:'),AT(8,48),USE(?pdt:PartRefNumber:Prompt)
                           ENTRY(@n-14),AT(76,48,64,10),USE(pdt:PartRefNumber),RIGHT(1)
                           PROMPT('Quantity:'),AT(8,62),USE(?pdt:Quantity:Prompt)
                           ENTRY(@n-14),AT(76,62,64,10),USE(pdt:Quantity),RIGHT(1)
                           PROMPT('Is Chargeable:'),AT(8,76),USE(?pdt:IsChargeable:Prompt)
                           ENTRY(@n3),AT(76,76,40,10),USE(pdt:IsChargeable)
                           PROMPT('Request Date:'),AT(8,90),USE(?pdt:RequestDate:Prompt)
                           ENTRY(@d17),AT(76,90,104,10),USE(pdt:RequestDate)
                           PROMPT('Request Time:'),AT(8,104),USE(?pdt:RequestTime:Prompt)
                           ENTRY(@t7),AT(76,104,104,10),USE(pdt:RequestTime)
                           PROMPT('Engineer Code:'),AT(8,118),USE(?pdt:EngineerCode:Prompt)
                           ENTRY(@s3),AT(76,118,40,10),USE(pdt:EngineerCode)
                           PROMPT('Is In Stock:'),AT(8,132),USE(?pdt:IsInStock:Prompt)
                           ENTRY(@n3),AT(76,132,40,10),USE(pdt:IsInStock)
                           PROMPT('Is Printed:'),AT(8,146),USE(?pdt:IsPrinted:Prompt)
                           ENTRY(@n3),AT(76,146,40,10),USE(pdt:IsPrinted)
                         END
                         TAB('General (cont.)'),USE(?Tab:2)
                           PROMPT('Is Delete:'),AT(8,20),USE(?pdt:IsDelete:Prompt)
                           ENTRY(@n3),AT(76,20,40,10),USE(pdt:IsDelete)
                           PROMPT('Delete Reason:'),AT(8,34),USE(?pdt:DeleteReason:Prompt)
                           ENTRY(@s255),AT(76,34,274,10),USE(pdt:DeleteReason)
                           PROMPT('Usercode:'),AT(8,48),USE(?pdt:Usercode:Prompt)
                           ENTRY(@s3),AT(76,48,40,10),USE(pdt:Usercode)
                           PROMPT('Stock Part Ref Number:'),AT(8,64),USE(?pdt:StockPartRefNumber:Prompt)
                           ENTRY(@n-14),AT(88,64,40,10),USE(pdt:StockPartRefNumber),RIGHT(1)
                           PROMPT('Part Number:'),AT(8,80),USE(?pdt:PartNumber:Prompt)
                           ENTRY(@s30),AT(88,80,40,10),USE(pdt:PartNumber)
                         END
                       END
                       BUTTON('OK'),AT(211,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(260,164,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(309,164,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdatePICKDET',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdatePICKDET',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdatePICKDET',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:PickDetailRef:Prompt;  SolaceCtrlName = '?pdt:PickDetailRef:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:PickDetailRef;  SolaceCtrlName = '?pdt:PickDetailRef';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:PickNoteRef:Prompt;  SolaceCtrlName = '?pdt:PickNoteRef:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:PickNoteRef;  SolaceCtrlName = '?pdt:PickNoteRef';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:PartRefNumber:Prompt;  SolaceCtrlName = '?pdt:PartRefNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:PartRefNumber;  SolaceCtrlName = '?pdt:PartRefNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:Quantity:Prompt;  SolaceCtrlName = '?pdt:Quantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:Quantity;  SolaceCtrlName = '?pdt:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:IsChargeable:Prompt;  SolaceCtrlName = '?pdt:IsChargeable:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:IsChargeable;  SolaceCtrlName = '?pdt:IsChargeable';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:RequestDate:Prompt;  SolaceCtrlName = '?pdt:RequestDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:RequestDate;  SolaceCtrlName = '?pdt:RequestDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:RequestTime:Prompt;  SolaceCtrlName = '?pdt:RequestTime:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:RequestTime;  SolaceCtrlName = '?pdt:RequestTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:EngineerCode:Prompt;  SolaceCtrlName = '?pdt:EngineerCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:EngineerCode;  SolaceCtrlName = '?pdt:EngineerCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:IsInStock:Prompt;  SolaceCtrlName = '?pdt:IsInStock:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:IsInStock;  SolaceCtrlName = '?pdt:IsInStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:IsPrinted:Prompt;  SolaceCtrlName = '?pdt:IsPrinted:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:IsPrinted;  SolaceCtrlName = '?pdt:IsPrinted';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:IsDelete:Prompt;  SolaceCtrlName = '?pdt:IsDelete:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:IsDelete;  SolaceCtrlName = '?pdt:IsDelete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:DeleteReason:Prompt;  SolaceCtrlName = '?pdt:DeleteReason:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:DeleteReason;  SolaceCtrlName = '?pdt:DeleteReason';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:Usercode:Prompt;  SolaceCtrlName = '?pdt:Usercode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:Usercode;  SolaceCtrlName = '?pdt:Usercode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:StockPartRefNumber:Prompt;  SolaceCtrlName = '?pdt:StockPartRefNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:StockPartRefNumber;  SolaceCtrlName = '?pdt:StockPartRefNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:PartNumber:Prompt;  SolaceCtrlName = '?pdt:PartNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pdt:PartNumber;  SolaceCtrlName = '?pdt:PartNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a PICKDET Record'
  OF ChangeRecord
    ActionMessage = 'Changing a PICKDET Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdatePICKDET')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdatePICKDET')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?pdt:PickDetailRef:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(pdt:Record,History::pdt:Record)
  SELF.AddHistoryField(?pdt:PickDetailRef,1)
  SELF.AddHistoryField(?pdt:PickNoteRef,2)
  SELF.AddHistoryField(?pdt:PartRefNumber,3)
  SELF.AddHistoryField(?pdt:Quantity,4)
  SELF.AddHistoryField(?pdt:IsChargeable,5)
  SELF.AddHistoryField(?pdt:RequestDate,6)
  SELF.AddHistoryField(?pdt:RequestTime,7)
  SELF.AddHistoryField(?pdt:EngineerCode,8)
  SELF.AddHistoryField(?pdt:IsInStock,9)
  SELF.AddHistoryField(?pdt:IsPrinted,10)
  SELF.AddHistoryField(?pdt:IsDelete,11)
  SELF.AddHistoryField(?pdt:DeleteReason,12)
  SELF.AddHistoryField(?pdt:Usercode,13)
  SELF.AddHistoryField(?pdt:StockPartRefNumber,14)
  SELF.AddHistoryField(?pdt:PartNumber,15)
  SELF.AddUpdateFile(Access:PICKDET)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:PICKDET.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PICKDET
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PICKDET.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdatePICKDET',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdatePICKDET')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

