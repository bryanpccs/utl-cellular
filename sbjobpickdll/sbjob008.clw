

   MEMBER('sbjobpickdll.clw')                         ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJOB008.INC'),ONCE        !Local module procedure declarations
                     END


Pick_Export_Criteria PROCEDURE                        !Generated from procedure template - Window

tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:Multiple         STRING(1)
savepath             STRING(255)
save_job_id          USHORT,AUTO
tmp:Line1            STRING(2000)
tmp:Line2            STRING(2000)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Pick Note Export Criteria'),AT(,,220,139),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,104),USE(?Sheet1),SPREAD
                         TAB('Pick Note Export Criteria'),USE(?Tab1),FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Start Date'),AT(8,20),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(84,20,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('End Date'),AT(8,36),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6b),AT(84,36,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           OPTION('Include Completed Picks'),AT(84,52,120,44),USE(tmp:Multiple),BOXED,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                             RADIO('All Jobs'),AT(96,64),USE(?Option2:Radio1),VALUE('0')
                             RADIO('With Multiple Requests Only'),AT(96,80),USE(?Option2:Radio3),VALUE('1')
                           END
                         END
                       END
                       PANEL,AT(4,112,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Export'),AT(96,116,56,16),USE(?Export),LEFT,ICON('Disk.gif')
                       BUTTON('Close'),AT(156,116,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Pick_Export_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'Pick_Export_Criteria',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'Pick_Export_Criteria',1)
    SolaceViewVars('tmp:Multiple',tmp:Multiple,'Pick_Export_Criteria',1)
    SolaceViewVars('savepath',savepath,'Pick_Export_Criteria',1)
    SolaceViewVars('save_job_id',save_job_id,'Pick_Export_Criteria',1)
    SolaceViewVars('tmp:Line1',tmp:Line1,'Pick_Export_Criteria',1)
    SolaceViewVars('tmp:Line2',tmp:Line2,'Pick_Export_Criteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Multiple;  SolaceCtrlName = '?tmp:Multiple';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option2:Radio1;  SolaceCtrlName = '?Option2:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option2:Radio3;  SolaceCtrlName = '?Option2:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Export;  SolaceCtrlName = '?Export';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Pick_Export_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Pick_Export_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:StartDate:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  SELF.FilesOpened = True
  tmp:StartDate = Deformat('1/1/1999',@d6)
  tmp:EndDate = Today()
  tmp:Multiple = 0
  OPEN(window)
  SELF.Opened=True
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Pick_Export_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Export
      ThisWindow.Update
      savepath = path()
      set(defaults)
      access:defaults.next()
      
      If def:exportpath <> ''
          glo:file_name = Clip(def:exportpath) & '\PICKEXP.CSV'
      Else!If def:exportpath <> ''
          glo:file_name = 'C:\PICKEXP.CSV'
      End!If def:exportpath <> ''
      if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !failed
          setpath(savepath)
      else!if not filedialog
          !found
          setpath(savepath)
      
          access:expgen.open()
          access:expgen.usefile()
      
          Clear(gen:record)
          gen:line1   = 'Picking Note Export'
          access:expgen.insert()
      
          Clear(gen:record)
          gen:line1   = 'Job Number,Date Completed,Request No,Eng,Part Ref,Description,Quantity,Status,Part Ref,Description,Quantity,Status,Part Ref,Description,Quantity,Status,Part Ref,Description,Quantity,Status,Part Ref,Description,Quantity,Status'
          access:expgen.insert()
      !    count# = 0
      !            tmp:costtotal = 0
      !            tmp:saletotal = 0
          setcursor(cursor:wait)
          set(job:Ref_Number_Key)
          loop while access:JOBS.next() = level:benign
              count# = 0
              access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record get details
              pnt:JobReference = job:ref_number
              set(pnt:keyonjobno,pnt:keyonjobno)
              loop
                  if access:picknote.next() then break. !no records
                  if pnt:JobReference <> job:ref_number then break.
                  count# += 1
                  if pnt:ConfirmedDate >= tmp:StartDate and pnt:ConfirmedDate <= tmp:EndDate then
                      !need another loop here
                      Clear(gen:record)
                      gen:line1   = Clip(pnt:JobReference)
                      gen:line1   = CLip(gen:line1) & ',' & format(pnt:ConfirmedDate,@d17)
                      gen:line1   = Clip(gen:line1) & ',' & count#
                      gen:line1   = Clip(gen:line1) & ',' & Clip(pnt:EngineerCode)
                      !loop thru pickdets
      
                      Partslistsplit
                      LOOP x# = 1 TO RECORDS(compoundparts)
                          GET(compoundparts,x#)
      
                          gen:line1   = Clip(gen:line1) & ',' & CLip(compoundparts.partnum)
                          gen:line1   = CLip(gen:line1) & ',' & Clip(compoundparts.desc)
                          gen:line1   = Clip(gen:line1) & ',' & CLip(compoundparts.quantity)
                          if compoundparts.isinstock then
                              gen:line1   = Clip(gen:line1) & ',' & 'From Stock'
                          else
                              gen:line1   = Clip(gen:line1) & ',' & 'On Order'
                          end
                      END !Loop
                      if tmp:Multiple = 1 then
                          if count# = 1
                              tmp:Line1 = gen:line1
                          end
                          if count# = 2 then
                              tmp:Line2 = gen:Line1
                              gen:Line1 = tmp:Line1
                              access:expgen.insert()
                              gen:Line1 = tmp:Line2
                              access:expgen.insert()
                          end
                          if count# > 2 then
                              access:expgen.insert()
                          end
                      else
                          access:expgen.insert()
                      end
                  end
              end !picknote loop
          end !job loop
      
          recordspercycle     = 25
          recordsprocessed    = 0
          percentprogress     = 0
          setcursor(cursor:wait)
          open(progresswindow)
          progress:thermometer    = 0
          ?progress:pcttext{prop:text} = '0% Completed'
      
          recordstoprocess    = Records(Jobs)
      
          setcursor()
          close(progresswindow)
      
          access:expgen.close()
          Case MessageEx('Export Completed.','ServiceBase 2000',|
                         'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      end!if not filedialog
    OF ?Close
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Pick_Export_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

