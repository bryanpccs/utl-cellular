

   MEMBER('sbjobpickdll.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJOB010.INC'),ONCE        !Local module procedure declarations
                     END


Partslistsplit       PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Partslistsplit')      !Add Procedure to Log
  end


    free(compoundparts)
    clear(compoundparts)

    access:users.clearkey(use:password_key)
    use:password = glo:password
    access:users.fetch(use:password_key)

    access:PICKDET.clearkey(pdt:keyonpicknote) !check for existing picking parts record
    pdt:PickNoteRef = pnt:PickNoteRef
    set(pdt:keyonpicknote,pdt:keyonpicknote)
    loop
        if access:pickdet.next() then break. !no records
        if pdt:PickNoteRef <> pnt:PickNoteRef THEN BREAK.
        !if pdt:Quantity <> 0 then
            if pdt:IsInStock and (~pdt:IsDelete) then !is in stock not deleted
                !if pdt:IsChargeable then !chargeable part
                    Access:stock.clearkey(sto:Ref_Number_Key)
                    sto:Ref_number = pdt:StockPartRefNumber
                    !sto:Location = pnt:Location
                    if Access:Stock.Fetch(sto:Ref_Number_Key) then
!                    Access:parts.CLEARKEY(par:Part_Number_Key)
!                    par:Part_Number = pdt:PartNumber
!                    set(par:Part_Number_Key,par:Part_Number_Key)
!                    if Access:parts.Fetch(par:Part_Number_Key) then
                        !error
                        !compoundparts.desc = 'parts deleted'
                    !message('couldnt find stock')
                    else
                        sort(compoundparts,'+stockref,+isinstock')
                        compoundparts.stockref = pdt:StockPartRefNumber
                        !compoundparts.partnum = sto:Part_Number!par:Part_Number
                        compoundparts.isinstock = true
                        get(compoundparts,compoundparts.stockref,compoundparts.isinstock)
                        if error() then !no existing
                            !sort(compoundparts,'+recno,+desc,+desc1,+desc2,+desc3,+desc4,+partnum,+partnum1,+partnum2,+partnum3,+partnum4,+quantity,+quantity1,+quantity2,+quantity3,+quantity4,+typepart,+typepart1,+typepart2,+typepart3,+typepart4,+isinstock')
                            clear(compoundparts)
                            compoundparts.stockref = pdt:StockPartRefNumber
                            compoundparts.recno = pdt:PickNoteRef
                            compoundparts.desc = sto:Description!par:Description
                            if pnt:IsConfirmed then
                                compoundparts.desc1 = 0C0C0C0h !grey text
                                compoundparts.desc2 = 0FFFFFFh !white back
                                compoundparts.desc3 = 0C0C0C0h !grey text
                                compoundparts.desc4 = 0FFFFFFh !white back
                            else
                                compoundparts.desc1 = 0000000h !black text
                                compoundparts.desc2 = 0FFFFFFh !white background
                                compoundparts.desc3 = 0000000h !black text
                                compoundparts.desc4 = 0FFFFFFh !white background
                            end
                            compoundparts.partnum = sto:Part_Number!par:Part_Number
                            if pnt:IsConfirmed then
                                compoundparts.partnum1 = 0C0C0C0h !grey text
                                compoundparts.partnum2 = 0FFFFFFh !white back
                                compoundparts.partnum3 = 0C0C0C0h !grey text
                                compoundparts.partnum4 = 0FFFFFFh !white back
                            else
                                compoundparts.partnum1 = 0000000h !black text
                                compoundparts.partnum2 = 0FFFFFFh !white background
                                compoundparts.partnum3 = 0000000h !black text
                                compoundparts.partnum4 = 0FFFFFFh !white background
                            end
                            compoundparts.quantity = pdt:Quantity
                            if pnt:IsConfirmed then
                                compoundparts.quantity1 = 0C0C0C0h !grey text
                                compoundparts.quantity2 = 0FFFFFFh !white back
                                compoundparts.quantity3 = 0C0C0C0h !grey text
                                compoundparts.quantity4 = 0FFFFFFh !white back
                            else
                                compoundparts.quantity1 = 0000000h !black text
                                compoundparts.quantity2 = 0FFFFFFh !white background
                                compoundparts.quantity3 = 0000000h !black text
                                compoundparts.quantity4 = 0FFFFFFh !white background
                            end
                            if pdt:IsChargeable then
                                compoundparts.typepart = 'chargeable'
                            else
                                compoundparts.typepart = 'warranty'
                            end
                            if pnt:IsConfirmed then
                                compoundparts.typepart1 = 0C0C0C0h !grey text
                                compoundparts.typepart2 = 0FFFFFFh !white back
                                compoundparts.typepart3 = 0C0C0C0h !grey text
                                compoundparts.typepart4 = 0FFFFFFh !white back
                            else
                                compoundparts.typepart1 = 0000000h !black text
                                compoundparts.typepart2 = 0FFFFFFh !white background
                                compoundparts.typepart3 = 0000000h !black text
                                compoundparts.typepart4 = 0FFFFFFh !white background
                            end
                            compoundparts.isinstock = true
                            ! Start Change 4699 BE(22/09/2004)
                            compoundparts.isreceived = pdt:isprinted
                            ! End Change 4699 BE(22/09/2004)
                            add(compoundparts)
                        else
                            !update value
                            compoundparts.quantity += pdt:Quantity
                            ! Start Change 4699 BE(22/09/2004)
                            compoundparts.isreceived = pdt:isprinted
                            ! End Change 4699 BE(22/09/2004)
                            put(compoundparts)
                        end
                    end
                !else !warranty part
!                    Access:stock.clearkey(sto:Location_Key)
!                    sto:Part_number = pdt:PartNumber
!                    sto:Location = pnt:Location
!                    if Access:Stock.Fetch(sto:Location_Key) then
!!                    Access:warparts.CLEARKEY(wpr:Part_Number_Key)
!!                    wpr:Part_Number = pdt:PartNumber
!!                    set(wpr:Part_Number_Key,wpr:Part_Number_Key)
!!                    if Access:parts.Fetch(wpr:Part_Number_Key) then
!                        !error
!                        !compoundparts.desc = 'parts deleted'
!                    else
!                        sort(compoundparts,'+partnum,+isinstock')
!                        compoundparts.partnum = sto:Part_Number
!                        compoundparts.isinstock = true
!                        get(compoundparts,compoundparts.partnum,compoundparts.isinstock)
!                        if error() then !no existing
!                            !sort(compoundparts,'+recno,+desc,+desc1,+desc2,+desc3,+desc4,+partnum,+partnum1,+partnum2,+partnum3,+partnum4,+quantity,+quantity1,+quantity2,+quantity3,+quantity4,+typepart,+typepart1,+typepart2,+typepart3,+typepart4,+isinstock')
!                            clear(compoundparts)
!                            compoundparts.recno = pdt:PickNoteRef
!                            compoundparts.desc = sto:Description!wpr:Description
!                            if pnt:IsConfirmed then
!                                compoundparts.desc1 = 0C0C0C0h !grey text
!                                compoundparts.desc2 = 0FFFFFFh !white back
!                                compoundparts.desc3 = 0C0C0C0h !grey text
!                                compoundparts.desc4 = 0FFFFFFh !white back
!                            else
!                                compoundparts.desc1 = 0000000h !black text
!                                compoundparts.desc2 = 0FFFFFFh !white background
!                                compoundparts.desc3 = 0000000h !black text
!                                compoundparts.desc4 = 0FFFFFFh !white background
!                            end
!                            compoundparts.partnum = sto:Part_Number!wpr:Part_Number
!                            if pnt:IsConfirmed then
!                                compoundparts.partnum1 = 0C0C0C0h !grey text
!                                compoundparts.partnum2 = 0FFFFFFh !white back
!                                compoundparts.partnum3 = 0C0C0C0h !grey text
!                                compoundparts.partnum4 = 0FFFFFFh !white back
!                            else
!                                compoundparts.partnum1 = 0000000h !black text
!                                compoundparts.partnum2 = 0FFFFFFh !white background
!                                compoundparts.partnum3 = 0000000h !black text
!                                compoundparts.partnum4 = 0FFFFFFh !white background
!                            end
!                            compoundparts.quantity = pdt:Quantity
!                            if pnt:IsConfirmed then
!                                compoundparts.quantity1 = 0C0C0C0h !grey text
!                                compoundparts.quantity2 = 0FFFFFFh !white back
!                                compoundparts.quantity3 = 0C0C0C0h !grey text
!                                compoundparts.quantity4 = 0FFFFFFh !white back
!                            else
!                                compoundparts.quantity1 = 0000000h !black text
!                                compoundparts.quantity2 = 0FFFFFFh !white background
!                                compoundparts.quantity3 = 0000000h !black text
!                                compoundparts.quantity4 = 0FFFFFFh !white background
!                            end
!                            compoundparts.typepart = 'warranty'
!                            if pnt:IsConfirmed then
!                                compoundparts.typepart1 = 0C0C0C0h !grey text
!                                compoundparts.typepart2 = 0FFFFFFh !white back
!                                compoundparts.typepart3 = 0C0C0C0h !grey text
!                                compoundparts.typepart4 = 0FFFFFFh !white back
!                            else
!                                compoundparts.typepart1 = 0000000h !black text
!                                compoundparts.typepart2 = 0FFFFFFh !white background
!                                compoundparts.typepart3 = 0000000h !black text
!                                compoundparts.typepart4 = 0FFFFFFh !white background
!                            end
!                            compoundparts.isinstock = true
!                            add(compoundparts)
!                        else
!                            !update value
!                            compoundparts.quantity += pdt:Quantity
!                            put(compoundparts)
!                        end
!                    end
!                end
            end
            if (~pdt:IsInStock) and (~pdt:IsDelete) then !is not in stock and not deleted
                !if pdt:IsChargeable then !chargeable part
                    Access:stock.clearkey(sto:Ref_Number_Key)
                    sto:Ref_number = pdt:StockPartRefNumber
                    !sto:Location = pnt:Location
                    if Access:Stock.Fetch(sto:Ref_Number_Key) then
!                    Access:parts.CLEARKEY(par:Part_Number_Key)
!                    par:Part_Number = pdt:PartNumber
!                    set(par:Part_Number_Key,par:Part_Number_Key)
!                    if Access:parts.Fetch(par:Part_Number_Key) then
                        !error
                        !compoundparts.desc = 'parts deleted'
                    !message('couldnt find stock')
                    else
                        sort(compoundparts,'+stockref,+isinstock')
                        compoundparts.stockref = pdt:StockPartRefNumber
                        !compoundparts.partnum = sto:Part_Number!par:Part_Number
                        compoundparts.isinstock = false
                        get(compoundparts,compoundparts.stockref,compoundparts.isinstock)
                        if error() then !no existing
                            !sort(compoundparts,'+recno,+desc,+desc1,+desc2,+desc3,+desc4,+partnum,+partnum1,+partnum2,+partnum3,+partnum4,+quantity,+quantity1,+quantity2,+quantity3,+quantity4,+typepart,+typepart1,+typepart2,+typepart3,+typepart4,+isinstock')
                            clear(compoundparts)
                            compoundparts.stockref = pdt:StockPartRefNumber
                            compoundparts.recno = pdt:PickNoteRef
                            compoundparts.desc = sto:Description!par:Description
                            if pnt:IsConfirmed then
                                compoundparts.desc1 = 0C0C0C0h !grey text
                                compoundparts.desc2 = 0FFFFFFh !white back
                                compoundparts.desc3 = 0C0C0C0h !grey text
                                compoundparts.desc4 = 0FFFFFFh !white back
                            else
                                compoundparts.desc1 = 0FFFFFFh !white text
                                compoundparts.desc2 = 0008000h !green background
                                compoundparts.desc3 = 0FFFFFFh !white text
                                compoundparts.desc4 = 0008000h !green background
                            end
                            compoundparts.partnum = sto:Part_Number!par:Part_Number
                            if pnt:IsConfirmed then
                                compoundparts.partnum1 = 0C0C0C0h !grey text
                                compoundparts.partnum2 = 0FFFFFFh !white back
                                compoundparts.partnum3 = 0C0C0C0h !grey text
                                compoundparts.partnum4 = 0FFFFFFh !white back
                            else
                                compoundparts.partnum1 = 0FFFFFFh !white text
                                compoundparts.partnum2 = 0008000h !green background
                                compoundparts.partnum3 = 0FFFFFFh !white text
                                compoundparts.partnum4 = 0008000h !green background
                            end
                            compoundparts.quantity = pdt:Quantity
                            if pnt:IsConfirmed then
                                compoundparts.quantity1 = 0C0C0C0h !grey text
                                compoundparts.quantity2 = 0FFFFFFh !white back
                                compoundparts.quantity3 = 0C0C0C0h !grey text
                                compoundparts.quantity4 = 0FFFFFFh !white back
                            else
                                compoundparts.quantity1 = 0FFFFFFh !white text
                                compoundparts.quantity2 = 0008000h !green background
                                compoundparts.quantity3 = 0FFFFFFh !white text
                                compoundparts.quantity4 = 0008000h !green background
                            end
                            if pdt:IsChargeable then
                                compoundparts.typepart = 'chargeable'
                            else
                                compoundparts.typepart = 'warranty'
                            end
                            if pnt:IsConfirmed then
                                compoundparts.typepart1 = 0C0C0C0h !grey text
                                compoundparts.typepart2 = 0FFFFFFh !white back
                                compoundparts.typepart3 = 0C0C0C0h !grey text
                                compoundparts.typepart4 = 0FFFFFFh !white back
                            else
                                compoundparts.typepart1 = 0FFFFFFh !white text
                                compoundparts.typepart2 = 0008000h !green background
                                compoundparts.typepart3 = 0FFFFFFh !white text
                                compoundparts.typepart4 = 0008000h !green background
                            end
                            compoundparts.isinstock = false
                             ! Start Change 4699 BE(22/09/2004)
                            compoundparts.isreceived = pdt:isprinted
                            ! End Change 4699 BE(22/09/2004)
                            add(compoundparts)
                        else
                            !update value
                            compoundparts.quantity += pdt:Quantity
                            ! Start Change 4699 BE(22/09/2004)
                            compoundparts.isreceived = pdt:isprinted
                            ! End Change 4699 BE(22/09/2004)
                            put(compoundparts)
                        end
                    end
!                else !warranty part
!                    Access:stock.clearkey(sto:Location_Key)
!                    sto:Part_number = pdt:PartNumber
!                    sto:Location = pnt:Location
!                    if Access:Stock.Fetch(sto:Location_Key) then
!!                    Access:warparts.CLEARKEY(wpr:Part_Number_Key)
!!                    wpr:Part_Number = pdt:PartNumber
!!                    set(wpr:Part_Number_Key,wpr:Part_Number_Key)
!!                    if Access:parts.Fetch(wpr:Part_Number_Key) then
!                        !error
!                        !compoundparts.desc = 'parts deleted'
!                    else
!                        sort(compoundparts,'+partnum,+isinstock')
!                        compoundparts.partnum = sto:Part_Number
!                        compoundparts.isinstock = false
!                        get(compoundparts,compoundparts.partnum,compoundparts.isinstock)
!                        if error() then !no existing
!                            !sort(compoundparts,'+recno,+desc,+desc1,+desc2,+desc3,+desc4,+partnum,+partnum1,+partnum2,+partnum3,+partnum4,+quantity,+quantity1,+quantity2,+quantity3,+quantity4,+typepart,+typepart1,+typepart2,+typepart3,+typepart4,+isinstock')
!                            clear(compoundparts)
!                            compoundparts.recno = pdt:PickNoteRef
!                            compoundparts.desc = sto:Description!wpr:Description
!                            if pnt:IsConfirmed then
!                                compoundparts.desc1 = 0C0C0C0h !grey text
!                                compoundparts.desc2 = 0FFFFFFh !white back
!                                compoundparts.desc3 = 0C0C0C0h !grey text
!                                compoundparts.desc4 = 0FFFFFFh !white back
!                            else
!                                compoundparts.desc1 = 0FFFFFFh !white text
!                                compoundparts.desc2 = 0008000h !green background
!                                compoundparts.desc3 = 0FFFFFFh !white text
!                                compoundparts.desc4 = 0008000h !green background
!                            end
!                            compoundparts.partnum = sto:Part_Number!wpr:Part_Number
!                            if pnt:IsConfirmed then
!                                compoundparts.partnum1 = 0C0C0C0h !grey text
!                                compoundparts.partnum2 = 0FFFFFFh !white back
!                                compoundparts.partnum3 = 0C0C0C0h !grey text
!                                compoundparts.partnum4 = 0FFFFFFh !white back
!                            else
!                                compoundparts.partnum1 = 0FFFFFFh !white text
!                                compoundparts.partnum2 = 0008000h !green background
!                                compoundparts.partnum3 = 0FFFFFFh !white text
!                                compoundparts.partnum4 = 0008000h !green background
!                            end
!                            compoundparts.quantity = pdt:Quantity
!                            if pnt:IsConfirmed then
!                                compoundparts.quantity1 = 0C0C0C0h !grey text
!                                compoundparts.quantity2 = 0FFFFFFh !white back
!                                compoundparts.quantity3 = 0C0C0C0h !grey text
!                                compoundparts.quantity4 = 0FFFFFFh !white back
!                            else
!                                compoundparts.quantity1 = 0FFFFFFh !white text
!                                compoundparts.quantity2 = 0008000h !green background
!                                compoundparts.quantity3 = 0FFFFFFh !white text
!                                compoundparts.quantity4 = 0008000h !green background
!                            end
!                            compoundparts.typepart = 'warranty'
!                            if pnt:IsConfirmed then
!                                compoundparts.typepart1 = 0C0C0C0h !grey text
!                                compoundparts.typepart2 = 0FFFFFFh !white back
!                                compoundparts.typepart3 = 0C0C0C0h !grey text
!                                compoundparts.typepart4 = 0FFFFFFh !white back
!                            else
!                                compoundparts.typepart1 = 0FFFFFFh !white text
!                                compoundparts.typepart2 = 0008000h !green background
!                                compoundparts.typepart3 = 0FFFFFFh !white text
!                                compoundparts.typepart4 = 0008000h !green background
!                            end
!                            compoundparts.isinstock = false
!                            add(compoundparts)
!                        else
!                            !update value
!                            compoundparts.quantity += pdt:Quantity
!                            put(compoundparts)
!                        end
!                    end
!                end
            end
        !print (rpt:detail1)
        !end !if pdt:quantity > 0
    end !loop through pickdets
    sort(compoundparts,'+partnum,-isinstock')


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Partslistsplit',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


