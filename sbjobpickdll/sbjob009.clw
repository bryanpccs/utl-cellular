

   MEMBER('sbjobpickdll.clw')                         ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJOB009.INC'),ONCE        !Local module procedure declarations
                     END








PickReport PROCEDURE
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
NeedPreview          BYTE
stocklev             STRING(20)
stockstat            STRING(12)
ChargeTypeString     STRING(20)
TeamName             STRING(30)
EngineerName         STRING(66)
EstimateString       STRING(14)
ShelfLocationString  STRING(30)
MakeModelString      STRING(61)
code_bc              BYTE
option_bc            BYTE
string_bc            CSTRING(21)
jobref_bc            CSTRING(21)
pickref_bc           CSTRING(21)
Location2String      STRING(30)
save_jobs            USHORT
JobNoString          STRING(20)
JobLocation          STRING(30)
JobStatus            STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(PICKNOTE)
                       PROJECT(pnt:JobReference)
                       PROJECT(pnt:PickNoteNumber)
                       PROJECT(pnt:PickNoteRef)
                       PROJECT(pnt:PrintedDate)
                       PROJECT(pnt:PrintedTime)
                       PROJECT(pnt:Usercode)
                     END
report               REPORT,AT(10,71,138,125),PAPER(PAPER:A5),PRE(RPT),FONT('Arial',10,,),MM
                       HEADER,AT(10,10,138,61)
                         STRING('SPARE PARTS PICK NOTE'),AT(1,1,44,5),USE(?unnamed:4),LEFT,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         STRING(@D7-),AT(107,15),USE(pnt:PrintedDate),TRN,RIGHT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING(@t7),AT(115,19),USE(pnt:PrintedTime),TRN,RIGHT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('Date Printed :'),AT(1,15),USE(?String13),TRN
                         STRING(@s20),AT(94,5,31,4),USE(JobNoString),TRN,RIGHT,FONT('Arial',6,,)
                         STRING(@s20),AT(2,8,63,6),USE(pickref_bc),TRN,LEFT,FONT('C128 High 12pt LJ3',16,,)
                         STRING(@s12),AT(15,5),USE(pnt:PickNoteRef),TRN,LEFT,FONT('Arial',6,,)
                         STRING('Pick Note No.'),AT(2,5),USE(?String37),TRN,FONT('Arial',6,,)
                         STRING('Team:'),AT(1,27),USE(?String32),TRN
                         STRING(@s30),AT(31,27,94,5),USE(TeamName),TRN,RIGHT,FONT('Arial',10,,)
                         STRING(@n-14),AT(98,47),USE(pnt:PickNoteNumber),TRN,RIGHT
                         STRING(@s8),AT(108,31),USE(pnt:JobReference),TRN,RIGHT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING(@s66),AT(31,23,94,5),USE(EngineerName),TRN,RIGHT,FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         STRING('Engineer :'),AT(1,23),USE(?String10),TRN
                         STRING('Charge Type :'),AT(1,35),USE(?String11),TRN
                         STRING(@s20),AT(82,35,43,5),USE(ChargeTypeString),TRN,RIGHT
                         STRING('Job Location'),AT(1,39),USE(?String40),TRN
                         STRING(@s30),AT(87,39,38,5),USE(JobLocation),TRN,RIGHT
                         STRING(@s13),AT(87,43,38,5),USE(EstimateString),TRN,RIGHT,FONT('Arial',10,,)
                         STRING(@s10),AT(92,0,33,8),USE(reprintnote),TRN,FONT('Arial',16,COLOR:Red,FONT:bold,CHARSET:ANSI)
                         STRING(@s61),AT(50,1,38,4),USE(MakeModelString),TRN,FONT('Arial',10,,)
                         STRING(@s20),AT(67,8,58,6),USE(jobref_bc),TRN,RIGHT,FONT('C128 High 12pt LJ3',16,,)
                         STRING('Time Printed :'),AT(1,19),USE(?String14),TRN
                         STRING('Pick Note Request Number :'),AT(1,47),USE(?String9),TRN
                         STRING('Job Number :'),AT(1,31),USE(?String8),TRN
                         STRING('Requested'),AT(1,53),USE(?String5),TRN
                         STRING('Qty'),AT(19,53),USE(?String4),TRN
                         STRING('Location'),AT(27,53),USE(?String6),TRN
                         STRING('Status'),AT(105,53),USE(?String12),TRN
                         LINE,AT(1,58,124,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Description'),AT(57,53),USE(?String7),TRN
                       END
detail1                DETAIL,AT(,,138,5),USE(?Stocklev:2)
                         STRING(@s20),AT(2,1,13,),USE(partnum),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s20),AT(17,1,8,),USE(quantity),TRN,RIGHT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(26,1,30,),USE(ShelfLocationString),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s255),AT(56,1,48,),USE(desc),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s12),AT(106,1,20,),USE(stockstat),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       END
detail2                DETAIL,AT(,,138,3),USE(?unnamed)
                         STRING('currently @'),AT(27,0),USE(?str),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('(engineer''s designated stock location)'),AT(62,0,57,5),USE(?String18),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(43,0,17,5),USE(use:Location),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s20),AT(17,0,8,5),USE(stocklev),TRN,RIGHT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       END
detail3                DETAIL,AT(,,138,3),USE(?unnamed:3)
                         STRING(@s30),AT(26,0),USE(Location2String),TRN,FONT('Arial',8,,)
                       END
                       FOOTER,AT(10,195,138,8),USE(?unnamed:2),FONT('Arial',,,)
                         STRING('Job Status:'),AT(2,1),USE(?String42),TRN
                         STRING(@s30),AT(21,1),USE(JobStatus),TRN
                         STRING(@pPage <<<#p),AT(115,3,17,3),PAGENO,USE(?PageCount),FONT('Arial',8,,FONT:regular)
                         LINE,AT(2,6,110,0),USE(?Line2),COLOR(COLOR:Black)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('PickReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
!marker buoy!!
  IF NeedPreview
    PreviewReq = True
  ELSE
    PreviewReq = False
  END
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:PICKNOTE.Open
  Relate:DEFPRINT.Open
  Relate:JOBS.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:JOBS_ALIAS.UseFile
  PRINTER{PROPPRINT:Copies} = 1
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'PICKING NOTE - '& pnt:Location
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      PreviewReq = false
      tempprint" = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      PreviewReq = true
      message('No printer set up for this report','Servicebase 2000 Warning')
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  
  
  RecordsToProcess = BYTES(PICKNOTE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  SEND(PICKNOTE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
        reprintnote = ''
        Partslistsplit
        
        access:users.clearkey(use:user_code_key)
        if pnt:EngineerCode <> '' then
            use:User_Code = pnt:EngineerCode
        else
            use:User_Code = pnt:PrintedBy !get who printed the pick note if we don't have an engineer
        end
        ! Start Change xxx BE(31/03/04)
        !access:users.tryfetch(use:user_code_key)
        IF (access:users.tryfetch(use:user_code_key) <> Level:Benign) THEN
            TeamName = ''
            EngineerName = use:User_Code
        ELSE
            TeamName = use:team
            IF (use:Forename = '') THEN
                IF (use:Surname = '') THEN
                    EngineerName = use:User_Code
                ELSE
                    EngineerName = CLIP(use:Surname) & ' (' & use:User_Code & ')'
                END
            ELSE
                EngineerName = CLIP(use:Forename) & ' ' & CLIP(use:Surname) & ' (' & use:User_Code & ')'
            END
        END
        
        ChargeTypeString = ''
        EstimateString = ''
        ! Start Change 4164 BE(21/04/04)
        !save_jobs = access:JOBS_ALIAS.SaveFile()
        !access:JOBS_ALIAS.clearkey(job_ali:ref_number_key)
        !job_ali:ref_number = pnt:JobReference
        !IF (access:JOBS_ALIAS.fetch(job_ali:ref_number_key) = Level:Benign) THEN
        !    IF (job_ali:Chargeable_Job = 'YES' AND job_ali:Warranty_Job = 'YES') THEN
        !        ChargeTypeString = 'Chargeable/Warranty'
        !    ELSIF (job_ali:Chargeable_Job = 'YES') THEN
        !        ChargeTypeString = 'Chargeable'
        !    ELSIF (job_ali:Warranty_Job = 'YES') THEN
        !        ChargeTypeString = 'Warranty'
        !    END
        !    IF (job_ali:Chargeable_Job = 'YES' AND job_ali:Estimate = 'YES') THEN
        !        EstimateString = 'Estimated Job'
        !    END
        !END
        !access:JOBS_ALIAS.RestoreFile(save_jobs)
        IF (job:ref_number = pnt:JobReference) THEN
            IF (job:Chargeable_Job = 'YES' AND job:Warranty_Job = 'YES') THEN
                ChargeTypeString = 'Chargeable/Warranty'
            ELSIF (job:Chargeable_Job = 'YES') THEN
                ChargeTypeString = 'Chargeable'
            ELSIF (job:Warranty_Job = 'YES') THEN
                ChargeTypeString = 'Warranty'
            END
            IF (job:Chargeable_Job = 'YES' AND job:Estimate = 'YES') THEN
                EstimateString = 'Estimated Job'
            END
            ! Start Change 4342 BE(28/06/2004)
            MakeModelString = CLIP(job:Manufacturer) & '/' & CLIP(job:Model_Number)
            ! End Change 4342 BE(28/06/2004)
            ! Start Change 4699 BE(09/09/2004)
            JobLocation = job:Location
            JobStatus = job:Current_Status
            ! End Change 4699 BE(09/09/2004)
        ELSE
            save_jobs = access:JOBS_ALIAS.SaveFile()
            access:JOBS_ALIAS.clearkey(job_ali:ref_number_key)
            job_ali:ref_number = pnt:JobReference
            IF (access:JOBS_ALIAS.fetch(job_ali:ref_number_key) = Level:Benign) THEN
                IF (job_ali:Chargeable_Job = 'YES' AND job_ali:Warranty_Job = 'YES') THEN
                    ChargeTypeString = 'Chargeable/Warranty'
                ELSIF (job_ali:Chargeable_Job = 'YES') THEN
                    ChargeTypeString = 'Chargeable'
                ELSIF (job_ali:Warranty_Job = 'YES') THEN
                    ChargeTypeString = 'Warranty'
                END
                IF (job_ali:Chargeable_Job = 'YES' AND job_ali:Estimate = 'YES') THEN
                    EstimateString = 'Estimated Job'
                END
            END
            ! Start Change 4342 BE(28/06/2004)
            MakeModelString = CLIP(job_ali:Manufacturer) & '/' & CLIP(job_ali:Model_Number)
            ! End Change 4342 BE(28/06/2004)
            ! Start Change 4699 BE(09/09/2004)
            JobLocation = job_ali:Location
            JobStatus = job_ali:Current_Status
            ! End Change 4699 BE(09/09/2004)
            access:JOBS_ALIAS.RestoreFile(save_jobs)
        END
        ! Start Change 4164 BE(21/04/04)
        ! End Change xxx BE(31/03/04)
        
        ! Start Change 4370 BE(28/06/2004)
        code_bc = 3
        option_bc = 0
        string_bc = Clip(pnt:JobReference)
        SEQUENCE2(code_bc,option_bc,string_bc,Jobref_bc)
        
        code_bc = 3
        option_bc = 0
        string_bc = Clip(pnt:PickNoteRef)
        SEQUENCE2(code_bc,option_bc,string_bc,pickref_bc)
        
        JobNoString = 'Job No. ' & CLIP(pnt:JobReference)
        ! End Change 4370 BE(28/06/2004)
        
        ! Start Change xxx BE(31/03/04)
        SORT(compoundparts, -compoundparts:isinstock, +compoundparts:partnum)
        ! Start Change xxx BE(31/03/04)
        
        !settarget(report)
        !message ('Queue Records: ' & RECORDS(compoundparts))
        LOOP x# = 1 TO RECORDS(compoundparts)
            GET(compoundparts,x#)
        !    message(compoundparts.stockref)
        !    message(compoundparts.recno)
        !    message(compoundparts.partnum)
        !    message(compoundparts.partnum1)
        !    message(compoundparts.partnum2)
        !    message(compoundparts.partnum3)
        !    message(compoundparts.partnum4)
        !    message(compoundparts.desc)
        !    message(compoundparts.desc1)
        !    message(compoundparts.desc2)
        !    message(compoundparts.desc3)
        !    message(compoundparts.desc4)
        !    message(compoundparts.quantity)
        !    message(compoundparts.quantity1)
        !    message(compoundparts.quantity2)
        !    message(compoundparts.quantity3)
        !    message(compoundparts.quantity4)
        !    message(compoundparts.typepart)
        !    message(compoundparts.typepart1)
        !    message(compoundparts.typepart2)
        !    message(compoundparts.typepart3)
        !    message(compoundparts.typepart4)
        !    message(compoundparts.isinstock)
        
            ! Start Change 4693 BE(28/09/2004)
            !access:stock.clearkey(sto:location_key) !find users current amount of stock in their designated location
            !sto:location = use:Location
            !sto:Part_Number = compoundparts.partnum
            !If access:stock.fetch(sto:location_key) = Level:Benign
            access:stock.clearkey(sto:ref_number_key)
            sto:ref_number = compoundparts.stockref
            If access:stock.fetch(sto:ref_number_key) = Level:Benign
            ! End Change 4693 BE(28/09/2004)
        
                !show stock level
                stocklev = ''
                ! Start Change xxx BE(31/03/04)
                ShelfLocationString = sto:Shelf_Location
                ! End Change xxx BE(31/03/04)
                ! Start Change 4418 BE(29/06/2004)
                Location2String = sto:Second_Location
                ! End Change 4418 BE(29/06/2004)
                if compoundparts.isinstock then
                    ! Start Change xxx BE(31/03/04)
                    !stockstat = 'Allocated'
                    stockstat = 'In Stock'
                    ! End Change xxx BE(31/03/04)
                else
                    ! Start Change xxx BE(31/03/04)
                    !stockstat = 'Outstanding'
                    ! Start Change 4699 BE(22/09/2004)
                    !stockstat = 'Out of Stock'
                    !! End Change xxx BE(31/03/04)
                    !stocklev = sto:Quantity_stock
                    IF (compoundparts.isreceived) THEN
                        stockstat = 'Received'
                    ELSE
                        stockstat = 'Out of Stock'
                        stocklev = sto:Quantity_stock
                    END
                    ! End Change 4699 BE(22/09/2004)
                end
            End
            if pnt:IsPrinted then reprintnote = 'REPRINT'.
        !    if (pnt:IsPrinted) and (compoundparts.quantity < 0) then reprintnote = 'AMENDMENT REPRINT'.
        !    if (~pnt:IsPrinted) and (compoundparts.quantity < 0) then reprintnote = 'AMENDMENT'.
            PRINT(RPT:Detail1)
            ! Start Change 4418 BE(29/06/2004)
            IF Location2String <> '' THEN
                PRINT(RPT:Detail3)
            END
            ! End Change 4418 BE(29/06/2004)
            if stocklev <> '' then PRINT(RPT:Detail2).
        END !LOOP
        
        LocalResponse = RequestCompleted
        Break
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(PICKNOTE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  Printer{PROPPRINT:Device} = tempprint"
  CLOSE(report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFPRINT.Close
    Relate:JOBS.Close
    Relate:PICKNOTE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='PickReport'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END




SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PickReport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'PickReport',1)
    SolaceViewVars('LocalRequest',LocalRequest,'PickReport',1)
    SolaceViewVars('LocalResponse',LocalResponse,'PickReport',1)
    SolaceViewVars('FilesOpened',FilesOpened,'PickReport',1)
    SolaceViewVars('WindowOpened',WindowOpened,'PickReport',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'PickReport',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'PickReport',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'PickReport',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'PickReport',1)
    SolaceViewVars('PercentProgress',PercentProgress,'PickReport',1)
    SolaceViewVars('RecordStatus',RecordStatus,'PickReport',1)
    SolaceViewVars('EndOfReport',EndOfReport,'PickReport',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'PickReport',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'PickReport',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'PickReport',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'PickReport',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'PickReport',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'PickReport',1)
    SolaceViewVars('InitialPath',InitialPath,'PickReport',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'PickReport',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'PickReport',1)
    SolaceViewVars('NeedPreview',NeedPreview,'PickReport',1)
    SolaceViewVars('stocklev',stocklev,'PickReport',1)
    SolaceViewVars('stockstat',stockstat,'PickReport',1)
    SolaceViewVars('ChargeTypeString',ChargeTypeString,'PickReport',1)
    SolaceViewVars('TeamName',TeamName,'PickReport',1)
    SolaceViewVars('EngineerName',EngineerName,'PickReport',1)
    SolaceViewVars('EstimateString',EstimateString,'PickReport',1)
    SolaceViewVars('ShelfLocationString',ShelfLocationString,'PickReport',1)
    SolaceViewVars('MakeModelString',MakeModelString,'PickReport',1)
    SolaceViewVars('code_bc',code_bc,'PickReport',1)
    SolaceViewVars('option_bc',option_bc,'PickReport',1)
    SolaceViewVars('string_bc',string_bc,'PickReport',1)
    SolaceViewVars('jobref_bc',jobref_bc,'PickReport',1)
    SolaceViewVars('pickref_bc',pickref_bc,'PickReport',1)
    SolaceViewVars('Location2String',Location2String,'PickReport',1)
    SolaceViewVars('save_jobs',save_jobs,'PickReport',1)
    SolaceViewVars('JobNoString',JobNoString,'PickReport',1)
    SolaceViewVars('JobLocation',JobLocation,'PickReport',1)
    SolaceViewVars('JobStatus',JobStatus,'PickReport',1)


BuildCtrlQueue      Routine







