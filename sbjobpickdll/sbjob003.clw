

   MEMBER('sbjobpickdll.clw')                         ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJOB003.INC'),ONCE        !Local module procedure declarations
                     END


BrowsePick PROCEDURE                                  !Generated from procedure template - Window

CurrentTab           STRING(80)
temp_desc            STRING(255)
tempvar              LONG
sortbylocation       BYTE
sortbypicknote       BYTE
proceed              BYTE
tempPickNoteRef      LONG
locatePickNoteRef    LONG
LocatePickNoteFlag   BYTE(0)
tempJobRef           LONG
savePickNote         USHORT
locateJobRef         LONG
locateJobFlag        BYTE(0)
SavePicknoteSort     LONG
SaveLocationSort     LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(PICKNOTE)
                       PROJECT(pnt:JobReference)
                       PROJECT(pnt:PickNoteNumber)
                       PROJECT(pnt:EngineerCode)
                       PROJECT(pnt:Location)
                       PROJECT(pnt:PickNoteRef)
                       PROJECT(pnt:PrintedBy)
                       PROJECT(pnt:PrintedDate)
                       PROJECT(pnt:PrintedTime)
                       PROJECT(pnt:ConfirmedNotes)
                       PROJECT(pnt:IsPrinted)
                       PROJECT(pnt:IsConfirmed)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
pnt:JobReference       LIKE(pnt:JobReference)         !List box control field - type derived from field
pnt:JobReference_NormalFG LONG                        !Normal forground color
pnt:JobReference_NormalBG LONG                        !Normal background color
pnt:JobReference_SelectedFG LONG                      !Selected forground color
pnt:JobReference_SelectedBG LONG                      !Selected background color
pnt:PickNoteNumber     LIKE(pnt:PickNoteNumber)       !List box control field - type derived from field
pnt:PickNoteNumber_NormalFG LONG                      !Normal forground color
pnt:PickNoteNumber_NormalBG LONG                      !Normal background color
pnt:PickNoteNumber_SelectedFG LONG                    !Selected forground color
pnt:PickNoteNumber_SelectedBG LONG                    !Selected background color
pnt:EngineerCode       LIKE(pnt:EngineerCode)         !List box control field - type derived from field
pnt:EngineerCode_NormalFG LONG                        !Normal forground color
pnt:EngineerCode_NormalBG LONG                        !Normal background color
pnt:EngineerCode_SelectedFG LONG                      !Selected forground color
pnt:EngineerCode_SelectedBG LONG                      !Selected background color
pnt:Location           LIKE(pnt:Location)             !List box control field - type derived from field
pnt:Location_NormalFG  LONG                           !Normal forground color
pnt:Location_NormalBG  LONG                           !Normal background color
pnt:Location_SelectedFG LONG                          !Selected forground color
pnt:Location_SelectedBG LONG                          !Selected background color
pnt:PickNoteRef        LIKE(pnt:PickNoteRef)          !List box control field - type derived from field
pnt:PickNoteRef_NormalFG LONG                         !Normal forground color
pnt:PickNoteRef_NormalBG LONG                         !Normal background color
pnt:PickNoteRef_SelectedFG LONG                       !Selected forground color
pnt:PickNoteRef_SelectedBG LONG                       !Selected background color
pnt:PrintedBy          LIKE(pnt:PrintedBy)            !List box control field - type derived from field
pnt:PrintedBy_NormalFG LONG                           !Normal forground color
pnt:PrintedBy_NormalBG LONG                           !Normal background color
pnt:PrintedBy_SelectedFG LONG                         !Selected forground color
pnt:PrintedBy_SelectedBG LONG                         !Selected background color
pnt:PrintedDate        LIKE(pnt:PrintedDate)          !List box control field - type derived from field
pnt:PrintedDate_NormalFG LONG                         !Normal forground color
pnt:PrintedDate_NormalBG LONG                         !Normal background color
pnt:PrintedDate_SelectedFG LONG                       !Selected forground color
pnt:PrintedDate_SelectedBG LONG                       !Selected background color
pnt:PrintedTime        LIKE(pnt:PrintedTime)          !List box control field - type derived from field
pnt:PrintedTime_NormalFG LONG                         !Normal forground color
pnt:PrintedTime_NormalBG LONG                         !Normal background color
pnt:PrintedTime_SelectedFG LONG                       !Selected forground color
pnt:PrintedTime_SelectedBG LONG                       !Selected background color
pnt:ConfirmedNotes     LIKE(pnt:ConfirmedNotes)       !List box control field - type derived from field
pnt:ConfirmedNotes_NormalFG LONG                      !Normal forground color
pnt:ConfirmedNotes_NormalBG LONG                      !Normal background color
pnt:ConfirmedNotes_SelectedFG LONG                    !Selected forground color
pnt:ConfirmedNotes_SelectedBG LONG                    !Selected background color
pnt:IsPrinted          LIKE(pnt:IsPrinted)            !Browse hot field - type derived from field
pnt:IsConfirmed        LIKE(pnt:IsConfirmed)          !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(PICKDET)
                       PROJECT(pdt:PickDetailRef)
                       PROJECT(pdt:PickNoteRef)
                       PROJECT(pdt:PartRefNumber)
                       PROJECT(pdt:Quantity)
                       PROJECT(pdt:IsChargeable)
                       PROJECT(pdt:RequestDate)
                       PROJECT(pdt:RequestTime)
                       PROJECT(pdt:EngineerCode)
                       PROJECT(pdt:IsInStock)
                       PROJECT(pdt:IsPrinted)
                       PROJECT(pdt:IsDelete)
                       PROJECT(pdt:DeleteReason)
                       PROJECT(pdt:Usercode)
                       PROJECT(pdt:StockPartRefNumber)
                       PROJECT(pdt:PartNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?DebugWindow
pdt:PickDetailRef      LIKE(pdt:PickDetailRef)        !List box control field - type derived from field
pdt:PickNoteRef        LIKE(pdt:PickNoteRef)          !List box control field - type derived from field
pdt:PartRefNumber      LIKE(pdt:PartRefNumber)        !List box control field - type derived from field
pdt:Quantity           LIKE(pdt:Quantity)             !List box control field - type derived from field
pdt:IsChargeable       LIKE(pdt:IsChargeable)         !List box control field - type derived from field
pdt:RequestDate        LIKE(pdt:RequestDate)          !List box control field - type derived from field
pdt:RequestTime        LIKE(pdt:RequestTime)          !List box control field - type derived from field
pdt:EngineerCode       LIKE(pdt:EngineerCode)         !List box control field - type derived from field
pdt:IsInStock          LIKE(pdt:IsInStock)            !List box control field - type derived from field
pdt:IsPrinted          LIKE(pdt:IsPrinted)            !List box control field - type derived from field
pdt:IsDelete           LIKE(pdt:IsDelete)             !List box control field - type derived from field
pdt:DeleteReason       LIKE(pdt:DeleteReason)         !List box control field - type derived from field
pdt:Usercode           LIKE(pdt:Usercode)             !List box control field - type derived from field
pdt:StockPartRefNumber LIKE(pdt:StockPartRefNumber)   !List box control field - type derived from field
pdt:PartNumber         LIKE(pdt:PartNumber)           !List box control field - type derived from field
pnt:PickNoteRef        LIKE(pnt:PickNoteRef)          !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB5::View:FileDrop  VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Picking Requests'),AT(,,678,400),FONT('Tahoma',8,,),CENTER,IMM,HLP('BrowsePick'),SYSTEM,GRAY,RESIZE
                       PANEL,AT(4,374,672,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('PickDetail'),AT(548,378,56,16),USE(?Button3),HIDE
                       SHEET,AT(4,4,364,366),USE(?CurrentTab),SPREAD
                         TAB('Unprocessed Picking Requests'),USE(?Tab:2),FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@S8),AT(32,68,92,10),USE(tempJobRef)
                           PROMPT('Job'),AT(16,68),USE(?Prompt1)
                           ENTRY(@n-14),AT(176,68,52,10),USE(locateJobRef),HIDE
                           PANEL,AT(8,20,348,24),USE(?Panel2),FILL(COLOR:Silver)
                           SHEET,AT(8,48,352,316),USE(?PickingSheet),ABOVE,SPREAD
                             TAB('All Pick Notes'),USE(?AllPick),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                             END
                             TAB('Unconfirmed Only'),USE(?Unconf),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                             END
                           END
                           LIST,AT(80,28,124,10),USE(GLO:Select1),DISABLE,FORMAT('120L(2)|M~Location~@s30@'),DROP(5),FROM(Queue:FileDrop)
                           CHECK('Sort all by pick note number'),AT(240,28),USE(sortbypicknote),LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           CHECK('By Location'),AT(16,28),USE(sortbylocation),LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),VALUE('1','0')
                         END
                       END
                       SHEET,AT(372,4,304,366),USE(?Sheet2),SPREAD
                         TAB('Items Requested'),USE(?Tab2),FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                           LIST,AT(380,20,288,152),USE(?List3),HSCROLL,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),FORMAT('0R(2)|FM*@n-14@0L(2)|FM*@s20@#2#60L(2)|FM*~Part Number~@s20@#3#150L(2)|FM*~Descr' &|
   'iption~@s255@#8#20L(2)|FM*~Qty~@s20@#13#60L(2)|FM*~Part Type~@s20@#18#'),FROM(compoundparts)
                           STRING('Confirmation Notes'),AT(480,176),USE(?String1),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING('Pick Note No.'),AT(380,176),USE(?String2),FONT('Arial',8,,)
                           TEXT,AT(480,186,188,72),USE(pnt:ConfirmedNotes),DISABLE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@S8),AT(380,188,92,10),USE(tempPickNoteRef)
                           ENTRY(@n-14),AT(380,244,92,10),USE(locatePickNoteRef),HIDE
                           BUTTON('Confirm Pick'),AT(380,200,65,16),USE(?Button4),LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),ICON('ok.gif')
                           BUTTON('Reprint Pick'),AT(380,224,65,16),USE(?Button2),LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),ICON('pick.gif')
                           LIST,AT(380,264,288,102),USE(?DebugWindow),IMM,HIDE,HVSCROLL,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),MSG('Browsing Records'),FORMAT('51L|M~Pick Detail Ref~C(2)@n-14@56L|M~Pick Note Ref~C(2)@n-14@57L|M~Part Ref Num' &|
   'ber~C(2)@n-14@30L|M~Quantity~C(2)@n-14@49L|M~Is Chargeable~C(2)@n3@40R|M~Request' &|
   ' Date~C(2)@d17@20R|M~Request Time~C(2)@t7@12R|M~Engineer Code~C(2)@s3@25L|M~Is I' &|
   'n Stock~C(2)@n3@24L|M~Is Printed~C(2)@n3@33L|M~Is Delete~C(2)@n3@100L|M~Delete R' &|
   'eason~C(2)@s255@12L|M~Usercode~C(2)@s3@56L|M~Stock Part Ref Number~C(2)@n-14@120' &|
   'L|M~Part Number~C(2)@s30@'),FROM(Queue:Browse)
                         END
                       END
                       LIST,AT(16,84,336,276),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),ALRT(EnterKey),FORMAT('50L(2)|*~Job Reference~C(0)@s8@18R(2)|*~Req~C(0)@n-14@34L(2)|*~Engineer~C@s3@55L' &|
   '(2)|*~Location~C@s30@36R(2)|*~Pick Note~C(0)@n-14@36L(2)|*~Printed By~C@s3@45R(2' &|
   ')|*~Printed Date~C(0)@d17@69R(2)|*~Printed Time~C(0)@t7@0R(2)|*@s255@'),FROM(Queue:Browse:1)
                       BUTTON('Close'),AT(616,378,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       BUTTON('PickNote'),AT(484,378,56,16),USE(?Button5),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?PickingSheet) = 2 and sortbylocation = 1 and sortbypicknote = 0
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?PickingSheet) = 2 and sortbylocation = 0 and sortbypicknote = 0
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - sortbylocation = 0 and sortbypicknote = 0
BRW1::Sort4:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?PickingSheet) = 2 and sortbylocation = 1 and sortbypicknote = 1
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?PickingSheet) = 2 and sortbylocation = 0 and sortbypicknote = 1
BRW1::Sort6:Locator  EntryLocatorClass                !Conditional Locator - sortbylocation = 0 and sortbypicknote = 1
BRW1::Sort7:Locator  StepLocatorClass                 !Conditional Locator - sortbylocation = 1 and sortbypicknote = 1
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW8                 CLASS(BrowseClass)               !Browse using ?DebugWindow
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB5                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

Partslist ROUTINE
    brw1.updatebuffer

    free(compoundparts)
    clear(compoundparts)

    access:PICKDET.clearkey(pdt:keyonpicknote) !check for existing picking parts record
    pdt:PickNoteRef = pnt:PickNoteRef
    set(pdt:keyonpicknote,pdt:keyonpicknote)
    loop
        if access:pickdet.next() then break. !no records
        if pdt:PickNoteRef <> pnt:PickNoteRef THEN BREAK.
        !if pdt:Quantity <> 0 then
            if pdt:IsChargeable and (~pdt:IsDelete) then !its a chargeable part not deleted
                Access:parts.CLEARKEY(par:recordnumberkey)
                par:Record_Number = pdt:PartRefNumber
                if Access:parts.Fetch(par:recordnumberkey) then
                    !error
                    !compoundparts.desc = 'parts deleted'
                else
                    compoundparts.partnum = par:Part_Number
                    get(compoundparts,compoundparts.partnum)
                    if error() then !no existing
                        clear(compoundparts)
                        compoundparts.recno = pdt:PickNoteRef
                        compoundparts.quantity = pdt:Quantity
                        compoundparts.desc = par:Description
                        compoundparts.partnum = par:Part_Number
                        compoundparts.typepart = 'chargeable'
                        add(compoundparts,compoundparts.partnum)
                    else
                        compoundparts.quantity += pdt:Quantity
                        put(compoundparts,compoundparts.partnum)
                    end
                end
            else
                if (~pdt:IsChargeable) and (~pdt:IsDelete) !Warranty parts
                    Access:warparts.CLEARKEY(wpr:recordnumberkey)
                    wpr:Record_Number = pdt:PartRefNumber
                    if Access:warparts.Fetch(wpr:recordnumberkey) then
                       !error
                        !compoundparts.desc = 'warparts deleted'
                    else
                        compoundparts.partnum = wpr:Part_Number
                        get(compoundparts,compoundparts.partnum)
                        if error() then!no existing
                            clear(compoundparts)
                            compoundparts.recno = pdt:PickNoteRef
                            compoundparts.quantity = pdt:Quantity
                            compoundparts.desc = wpr:Description
                            compoundparts.partnum = wpr:Part_Number
                            compoundparts.typepart = 'warranty'
                            add(compoundparts,compoundparts.partnum)
                        else
                            !update value
                            compoundparts.quantity += pdt:Quantity
                            put(compoundparts,compoundparts.partnum)
                        end
                    end
                end
            end
        !end !if pdt:quantity > 0
    end !loop through pickdets
    brw1.resetsort(1)
GetCMDline ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
           BEEP(beep:systemhand)
           message('Attempting to use sbjobpick.exe without using ServiceBase 2000' & '.|'                        & |
                'Start ServiceBase 2000 and run the program from there.','ServiceBase 2000 Error',ICON:Exclamation)

           POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
    EXIT


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BrowsePick',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'BrowsePick',1)
    SolaceViewVars('temp_desc',temp_desc,'BrowsePick',1)
    SolaceViewVars('tempvar',tempvar,'BrowsePick',1)
    SolaceViewVars('sortbylocation',sortbylocation,'BrowsePick',1)
    SolaceViewVars('sortbypicknote',sortbypicknote,'BrowsePick',1)
    SolaceViewVars('proceed',proceed,'BrowsePick',1)
    SolaceViewVars('tempPickNoteRef',tempPickNoteRef,'BrowsePick',1)
    SolaceViewVars('locatePickNoteRef',locatePickNoteRef,'BrowsePick',1)
    SolaceViewVars('LocatePickNoteFlag',LocatePickNoteFlag,'BrowsePick',1)
    SolaceViewVars('tempJobRef',tempJobRef,'BrowsePick',1)
    SolaceViewVars('savePickNote',savePickNote,'BrowsePick',1)
    SolaceViewVars('locateJobRef',locateJobRef,'BrowsePick',1)
    SolaceViewVars('locateJobFlag',locateJobFlag,'BrowsePick',1)
    SolaceViewVars('SavePicknoteSort',SavePicknoteSort,'BrowsePick',1)
    SolaceViewVars('SaveLocationSort',SaveLocationSort,'BrowsePick',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tempJobRef;  SolaceCtrlName = '?tempJobRef';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?locateJobRef;  SolaceCtrlName = '?locateJobRef';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PickingSheet;  SolaceCtrlName = '?PickingSheet';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AllPick;  SolaceCtrlName = '?AllPick';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Unconf;  SolaceCtrlName = '?Unconf';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sortbypicknote;  SolaceCtrlName = '?sortbypicknote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sortbylocation;  SolaceCtrlName = '?sortbylocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List3;  SolaceCtrlName = '?List3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pnt:ConfirmedNotes;  SolaceCtrlName = '?pnt:ConfirmedNotes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tempPickNoteRef;  SolaceCtrlName = '?tempPickNoteRef';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?locatePickNoteRef;  SolaceCtrlName = '?locatePickNoteRef';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button2;  SolaceCtrlName = '?Button2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DebugWindow;  SolaceCtrlName = '?DebugWindow';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button5;  SolaceCtrlName = '?Button5';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowsePick')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BrowsePick')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Access:STOCK.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:LOCATION.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:PICKNOTE,SELF)
  BRW8.Init(?DebugWindow,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:PICKDET,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ?GLO:Select1{prop:vcr} = TRUE
  ?List3{prop:vcr} = TRUE
  ?DebugWindow{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,pnt:keyonlocation)
  BRW1.AddRange(pnt:Location,GLO:Select1)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,pnt:Location,1,BRW1)
  BRW1.AppendOrder('pnt:JobReference')
  BRW1.SetFilter('((pnt:IsConfirmed = 0) and (pnt:IsPrinted = 1))')
  BRW1.AddSortOrder(,pnt:keyonjobno)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?locateJobRef,pnt:JobReference,1,BRW1)
  BRW1.SetFilter('((pnt:IsConfirmed = 0) and (pnt:IsPrinted = 1))')
  BRW1.AddSortOrder(,pnt:keyonjobno)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?locateJobRef,pnt:JobReference,1,BRW1)
  BRW1.AppendOrder('pnt:PickNoteRef')
  BRW1.SetFilter('(pnt:IsPrinted = 1)')
  BRW1.AddSortOrder(,pnt:keyonlocation)
  BRW1.AddRange(pnt:Location,GLO:Select1)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(,pnt:Location,1,BRW1)
  BRW1.AppendOrder('pnt:PickNoteRef')
  BRW1.SetFilter('((pnt:IsConfirmed = 0) and (pnt:IsPrinted = 1))')
  BRW1.AddSortOrder(,pnt:keypicknote)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?locatePickNoteRef,pnt:PickNoteRef,1,BRW1)
  BRW1.SetFilter('((pnt:IsConfirmed = 0) and (pnt:IsPrinted = 1))')
  BRW1.AddSortOrder(,pnt:keypicknote)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?locatePickNoteRef,pnt:PickNoteRef,1,BRW1)
  BRW1.AppendOrder('pnt:JobReference')
  BRW1.SetFilter('(pnt:IsPrinted = 1)')
  BRW1.AddSortOrder(,pnt:keyonlocation)
  BRW1.AddRange(pnt:Location,GLO:Select1)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(,pnt:Location,1,BRW1)
  BRW1.AppendOrder('pnt:PickNoteRef')
  BRW1.SetFilter('(pnt:IsPrinted = 1)')
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,pnt:keyonlocation)
  BRW1.AddRange(pnt:Location,GLO:Select1)
  BRW1.AppendOrder('pnt:JobReference')
  BRW1.SetFilter('(pnt:IsPrinted = 1)')
  BRW1.AddField(pnt:JobReference,BRW1.Q.pnt:JobReference)
  BRW1.AddField(pnt:PickNoteNumber,BRW1.Q.pnt:PickNoteNumber)
  BRW1.AddField(pnt:EngineerCode,BRW1.Q.pnt:EngineerCode)
  BRW1.AddField(pnt:Location,BRW1.Q.pnt:Location)
  BRW1.AddField(pnt:PickNoteRef,BRW1.Q.pnt:PickNoteRef)
  BRW1.AddField(pnt:PrintedBy,BRW1.Q.pnt:PrintedBy)
  BRW1.AddField(pnt:PrintedDate,BRW1.Q.pnt:PrintedDate)
  BRW1.AddField(pnt:PrintedTime,BRW1.Q.pnt:PrintedTime)
  BRW1.AddField(pnt:ConfirmedNotes,BRW1.Q.pnt:ConfirmedNotes)
  BRW1.AddField(pnt:IsPrinted,BRW1.Q.pnt:IsPrinted)
  BRW1.AddField(pnt:IsConfirmed,BRW1.Q.pnt:IsConfirmed)
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,pdt:keyonpicknote)
  BRW8.AddRange(pdt:PickNoteRef,pnt:PickNoteRef)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,pdt:PickNoteRef,1,BRW8)
  BRW8.AppendOrder('pdt:PickDetailRef')
  BRW8.AddField(pdt:PickDetailRef,BRW8.Q.pdt:PickDetailRef)
  BRW8.AddField(pdt:PickNoteRef,BRW8.Q.pdt:PickNoteRef)
  BRW8.AddField(pdt:PartRefNumber,BRW8.Q.pdt:PartRefNumber)
  BRW8.AddField(pdt:Quantity,BRW8.Q.pdt:Quantity)
  BRW8.AddField(pdt:IsChargeable,BRW8.Q.pdt:IsChargeable)
  BRW8.AddField(pdt:RequestDate,BRW8.Q.pdt:RequestDate)
  BRW8.AddField(pdt:RequestTime,BRW8.Q.pdt:RequestTime)
  BRW8.AddField(pdt:EngineerCode,BRW8.Q.pdt:EngineerCode)
  BRW8.AddField(pdt:IsInStock,BRW8.Q.pdt:IsInStock)
  BRW8.AddField(pdt:IsPrinted,BRW8.Q.pdt:IsPrinted)
  BRW8.AddField(pdt:IsDelete,BRW8.Q.pdt:IsDelete)
  BRW8.AddField(pdt:DeleteReason,BRW8.Q.pdt:DeleteReason)
  BRW8.AddField(pdt:Usercode,BRW8.Q.pdt:Usercode)
  BRW8.AddField(pdt:StockPartRefNumber,BRW8.Q.pdt:StockPartRefNumber)
  BRW8.AddField(pdt:PartNumber,BRW8.Q.pdt:PartNumber)
  BRW8.AddField(pnt:PickNoteRef,BRW8.Q.pnt:PickNoteRef)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDB5.Init(?GLO:Select1,Queue:FileDrop.ViewPosition,FDB5::View:FileDrop,Queue:FileDrop,Relate:LOCATION,ThisWindow)
  FDB5.Q &= Queue:FileDrop
  FDB5.AddSortOrder(loc:Location_Key)
  FDB5.AddField(loc:Location,FDB5.Q.loc:Location)
  FDB5.AddField(loc:RecordNumber,FDB5.Q.loc:RecordNumber)
  FDB5.AddUpdateField(loc:Location,GLO:Select1)
  ThisWindow.AddItem(FDB5.WindowComponent)
  FDB5.DefaultFill = 0
  BRW8.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BrowsePick',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?tempJobRef
      ! Start Change 4706 BE(18/10/2004)
      IF (tempJobRef <> '') THEN
          IF (sortbylocation = 0) THEN
              IF (sortbypicknote = 0) THEN
                  SELECT(?locateJobRef)
                  PRESS(CLIP(tempJobRef) & CHR(9))
              ELSE
                  savePicknote = access:picknote.savefile()
                  picknote# = -1
                  access:picknote.clearkey(pnt:keyonjobno)
                  pnt:JobReference = tempJobRef
                  IF (access:picknote.fetch(pnt:keyonjobno) = Level:Benign) THEN
                      picknote# = pnt:PickNoteRef
                  END
                  access:picknote.restorefile(savePickNote)
                  IF (picknote# <> -1) THEN
                      SELECT(?locatePickNoteRef)
                      PRESS(CLIP(picknote#) & CHR(9))
                  END
              END
          ELSE
              savePicknote = access:picknote.savefile()
              picknote# = -1
      
              access:picknote.clearkey(pnt:keyonjobno)
              pnt:JobReference = tempJobRef
              SET(pnt:keyonjobno,pnt:keyonjobno)
              LOOP
                  IF ((access:picknote.next() <> Level:Benign) OR |
                      (pnt:JobReference <> tempJobRef)) THEN
                      BREAK
                  END
                  IF (pnt:Location = glo:select1) THEN
                      picknote# = pnt:PickNoteRef
                      BREAK
                  END
              END
              access:picknote.restorefile(savePickNote)
      
              IF (picknote# <> -1) THEN
                  ! Save Sort States so they can be restored after locator seek
                  savepicknotesort = sortbypicknote
                  savelocationsort = sortbylocation
                  ! indicate to locator that  Sort States to be restored
                  locateJobRef = 1
                  ! set sort state to picknote order
                  sortbypicknote = 1
                  sortbylocation = 0
                  ! and locate picknote
                  SELECT(?locatePickNoteRef)
                  PRESS(CLIP(picknote#) & CHR(9))
              END
          END
      END
      ! End Change 4706 BE(18/10/2004)
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button3
      ThisWindow.Update
      DebugPickDet
      ThisWindow.Reset
    OF ?CurrentTab
      brw1.updatebuffer
      partslistsplit
    OF ?GLO:Select1
      brw1.updatebuffer
      BRW1.ResetSort(1)
    OF ?sortbypicknote
      brw1.updatebuffer
      brw1.resetsort(1)
    OF ?sortbylocation
      brw1.updatebuffer
      if sortbylocation then
          enable(?GLO:Select1)
          brw1.resetsort(1)
      else
          disable(?GLO:select1)
          glo:select1=''
          brw1.resetsort(1)
      end
    OF ?tempPickNoteRef
      ! Start Change 4370 BE(30/06/2004)
      IF (tempPickNoteRef <> '') THEN
          sortbylocation = 0
          sortbypicknote = 1
          brw1.updatebuffer
          brw1.resetsort(1)
      
          locatePickNoteFlag = 1
          SELECT(?locatePickNoteRef)
          PRESS(CLIP(tempPickNoteRef) & CHR(9))
      
      END
      ! Start Change 4370 BE(30/06/2004)
      
    OF ?locatePickNoteRef
      ! Start Change 4706 BE(20/10/2004)
      IF (locateJobRef) THEN
          locateJobRef = false
          IF (SaveLocationSort <> sortbylocation) THEN
              sortbylocation = SaveLocationSort
          END
          IF (sortbypicknote <> savepicknotesort) THEN
              sortbypicknote = savepicknotesort
          END
      END
      ! End Change 4706 BE(20/10/2004)
    OF ?Button4
      ThisWindow.Update
      ! Start Change 4370 BE(30/06/2004)
      IF (BRW1.Q.pnt:pickNoteRef <> tempPickNoteRef) THEN
          MessageEx('Pick Note ' & CLIP(tempPickNoteRef) & ' Not Found.','ServiceBase 2000',|
                   'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,|
                   CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
      ELSE
      ! End Change 4370 BE(30/06/2004)
          brw1.updatebuffer
      
          proceed = true
      
          LOOP x# = 1 TO RECORDS(compoundparts)
              GET(compoundparts,x#)
              if compoundparts.isinstock = false then proceed = false.
          END !LOOP
      
          if proceed then
              Confirmnotes
              brw1.resetsort(1)
              partslistsplit
          else
              if ~pnt:IsConfirmed then
                  Case MessageEx('There are still pending items on this pick note. It cannot be confirmed until stock is received.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                      Of 1 ! &OK Button
                  End!Case MessageEx
              end
          end
      
      ! Start Change 4370 BE(30/06/2004)
          SELECT(?tempPickNoteRef)
      END
      ! End Change 4370 BE(30/06/2004)
    OF ?Button2
      ThisWindow.Update
      ! Start Change 4370 BE(1/07/2004)
      brw1.updatebuffer
      PickReport
      Partslistsplit
      brw1.resetsort(1)
      !IF (locatePickNoteFlag) THEN
      !    locatePickNoteFlag = false
      !    SELECT(?Button4)
      !ELSE
      !    brw1.updatebuffer
      !    PickReport
      !    Partslistsplit
      !    brw1.resetsort(1)
      !END
      ! End Change 4370 BE(1/07/2004)
      
    OF ?Browse:1
      brw1.updatebuffer
      Partslistsplit
      brw1.resetsort(1)
      !?ConfirmedNotesBox{PROP:text} = pnt:ConfirmedNotes
      ! Start Change 4370 BE(30/06/2004)
      IF (locatePickNoteFlag = 0) THEN
          tempPickNoteRef = BRW1.Q.pnt:PickNoteRef
      END
      ! End Change 4370 BE(30/06/2004)
      DISPLAY()
    OF ?Button5
      ThisWindow.Update
      DebugPickNote
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BrowsePick')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Unconf
    brw1.updatebuffer
    partslistsplit
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:ScrollUp
      brw1.updatebuffer
      Partslistsplit
      brw1.resetsort(1)
      !?ConfirmedNotesBox{PROP:text} = pnt:ConfirmedNotes
      ! Start Change 4370 BE(30/06/2004)
      IF (locatePickNoteFlag = 0) THEN
          tempPickNoteRef = BRW1.Q.pnt:PickNoteRef
      END
      ! End Change 4370 BE(30/06/2004)
      DISPLAY()
    OF EVENT:ScrollDown
      brw1.updatebuffer
      Partslistsplit
      brw1.resetsort(1)
      !?ConfirmedNotesBox{PROP:text} = pnt:ConfirmedNotes
      ! Start Change 4370 BE(30/06/2004)
      IF (locatePickNoteFlag = 0) THEN
          tempPickNoteRef = BRW1.Q.pnt:PickNoteRef
      END
      ! End Change 4370 BE(30/06/2004)
      DISPLAY()
    OF EVENT:PageUp
      brw1.updatebuffer
      Partslistsplit
      brw1.resetsort(1)
      !?ConfirmedNotesBox{PROP:text} = pnt:ConfirmedNotes
      ! Start Change 4370 BE(30/06/2004)
      IF (locatePickNoteFlag = 0) THEN
          tempPickNoteRef = BRW1.Q.pnt:PickNoteRef
      END
      ! End Change 4370 BE(30/06/2004)
      DISPLAY()
    OF EVENT:PageDown
      brw1.updatebuffer
      Partslistsplit
      brw1.resetsort(1)
      !?ConfirmedNotesBox{PROP:text} = pnt:ConfirmedNotes
      ! Start Change 4370 BE(30/06/2004)
      IF (locatePickNoteFlag = 0) THEN
          tempPickNoteRef = BRW1.Q.pnt:PickNoteRef
      END
      ! End Change 4370 BE(30/06/2004)
      DISPLAY()
    OF EVENT:AlertKey
      ! Start Change 4370 BE(1/07/2004)
      POST(Event:Accepted, ?Button4)
      ! End Change 4370 BE(1/07/2004)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      brw1.updatebuffer
      Partslistsplit
      !brw1.resetsort(1)
      !?ConfirmedNotesBox{PROP:text} = pnt:ConfirmedNotes
      ! Start Change 4370 BE(30/06/2004)
      ! Event Firing Seuence after locator is new selection,new selection, selection, new selection
      ! hence incrementing locatePickNoteFlag each time here generates value 3 on final event...
      IF (locatePickNoteFlag = 3) THEN
          locatePickNoteFlag = 0
      ELSIF (locatePickNoteFlag > 0)
          locatePickNoteFlag += 1
      ELSE
          tempPickNoteRef = BRW1.Q.pnt:PickNoteRef
      END
      ! End Change 4370 BE(30/06/2004)
      DISPLAY()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?Browse:1
      brw1.updatebuffer
      Partslistsplit
      brw1.resetsort(1)
      !?ConfirmedNotesBox{PROP:text} = pnt:ConfirmedNotes
      ! Start Change 4370 BE(30/06/2004)
      IF (locatePickNoteFlag = 0) THEN
          tempPickNoteRef = BRW1.Q.pnt:PickNoteRef
      END
      ! End Change 4370 BE(30/06/2004)
      DISPLAY()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      sortbylocation = false
      glo:select1=''
      !DO GetCMDline
      brw1.updatebuffer
      Partslistsplit
      brw1.resetsort(1)
      !?ConfirmedNotesBox{PROP:text} = pnt:ConfirmedNotes
      ! Start Change 4370 BE(30/06/2004)
      !select(?Browse:1)
      SELECT(?tempPickNoteRef)
      ! End Change 4370 BE(30/06/2004)
      DISPLAY()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?PickingSheet) = 2 and sortbylocation = 1 and sortbypicknote = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?PickingSheet) = 2 and sortbylocation = 0 and sortbypicknote = 0
    RETURN SELF.SetSort(2,Force)
  ELSIF sortbylocation = 0 and sortbypicknote = 0
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?PickingSheet) = 2 and sortbylocation = 1 and sortbypicknote = 1
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?PickingSheet) = 2 and sortbylocation = 0 and sortbypicknote = 1
    RETURN SELF.SetSort(5,Force)
  ELSIF sortbylocation = 0 and sortbypicknote = 1
    RETURN SELF.SetSort(6,Force)
  ELSIF sortbylocation = 1 and sortbypicknote = 1
    RETURN SELF.SetSort(7,Force)
  ELSE
    RETURN SELF.SetSort(8,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (pnt:IsConfirmed = 1)
    SELF.Q.pnt:JobReference_NormalFG = 12632256
    SELF.Q.pnt:JobReference_NormalBG = -1
    SELF.Q.pnt:JobReference_SelectedFG = -1
    SELF.Q.pnt:JobReference_SelectedBG = -1
  ELSE
    SELF.Q.pnt:JobReference_NormalFG = -1
    SELF.Q.pnt:JobReference_NormalBG = -1
    SELF.Q.pnt:JobReference_SelectedFG = -1
    SELF.Q.pnt:JobReference_SelectedBG = -1
  END
  SELF.Q.pnt:PickNoteNumber_NormalFG = -1
  SELF.Q.pnt:PickNoteNumber_NormalBG = -1
  SELF.Q.pnt:PickNoteNumber_SelectedFG = -1
  SELF.Q.pnt:PickNoteNumber_SelectedBG = -1
  IF (pnt:IsConfirmed = 1)
    SELF.Q.pnt:EngineerCode_NormalFG = 12632256
    SELF.Q.pnt:EngineerCode_NormalBG = -1
    SELF.Q.pnt:EngineerCode_SelectedFG = -1
    SELF.Q.pnt:EngineerCode_SelectedBG = -1
  ELSE
    SELF.Q.pnt:EngineerCode_NormalFG = -1
    SELF.Q.pnt:EngineerCode_NormalBG = -1
    SELF.Q.pnt:EngineerCode_SelectedFG = -1
    SELF.Q.pnt:EngineerCode_SelectedBG = -1
  END
  IF (pnt:IsConfirmed = 1)
    SELF.Q.pnt:Location_NormalFG = 12632256
    SELF.Q.pnt:Location_NormalBG = -1
    SELF.Q.pnt:Location_SelectedFG = -1
    SELF.Q.pnt:Location_SelectedBG = -1
  ELSE
    SELF.Q.pnt:Location_NormalFG = -1
    SELF.Q.pnt:Location_NormalBG = -1
    SELF.Q.pnt:Location_SelectedFG = -1
    SELF.Q.pnt:Location_SelectedBG = -1
  END
  IF (pnt:IsConfirmed = 1)
    SELF.Q.pnt:PickNoteRef_NormalFG = 12632256
    SELF.Q.pnt:PickNoteRef_NormalBG = -1
    SELF.Q.pnt:PickNoteRef_SelectedFG = -1
    SELF.Q.pnt:PickNoteRef_SelectedBG = -1
  ELSE
    SELF.Q.pnt:PickNoteRef_NormalFG = -1
    SELF.Q.pnt:PickNoteRef_NormalBG = -1
    SELF.Q.pnt:PickNoteRef_SelectedFG = -1
    SELF.Q.pnt:PickNoteRef_SelectedBG = -1
  END
  IF (pnt:IsConfirmed = 1)
    SELF.Q.pnt:PrintedBy_NormalFG = 12632256
    SELF.Q.pnt:PrintedBy_NormalBG = -1
    SELF.Q.pnt:PrintedBy_SelectedFG = -1
    SELF.Q.pnt:PrintedBy_SelectedBG = -1
  ELSE
    SELF.Q.pnt:PrintedBy_NormalFG = -1
    SELF.Q.pnt:PrintedBy_NormalBG = -1
    SELF.Q.pnt:PrintedBy_SelectedFG = -1
    SELF.Q.pnt:PrintedBy_SelectedBG = -1
  END
  IF (pnt:IsConfirmed = 1)
    SELF.Q.pnt:PrintedDate_NormalFG = 12632256
    SELF.Q.pnt:PrintedDate_NormalBG = -1
    SELF.Q.pnt:PrintedDate_SelectedFG = -1
    SELF.Q.pnt:PrintedDate_SelectedBG = -1
  ELSE
    SELF.Q.pnt:PrintedDate_NormalFG = -1556852385
    SELF.Q.pnt:PrintedDate_NormalBG = -1
    SELF.Q.pnt:PrintedDate_SelectedFG = -1
    SELF.Q.pnt:PrintedDate_SelectedBG = -1
  END
  IF (pnt:IsConfirmed = 1)
    SELF.Q.pnt:PrintedTime_NormalFG = 12632256
    SELF.Q.pnt:PrintedTime_NormalBG = -1
    SELF.Q.pnt:PrintedTime_SelectedFG = -1
    SELF.Q.pnt:PrintedTime_SelectedBG = -1
  ELSE
    SELF.Q.pnt:PrintedTime_NormalFG = -1
    SELF.Q.pnt:PrintedTime_NormalBG = -1
    SELF.Q.pnt:PrintedTime_SelectedFG = -1
    SELF.Q.pnt:PrintedTime_SelectedBG = -1
  END
  SELF.Q.pnt:ConfirmedNotes_NormalFG = -1
  SELF.Q.pnt:ConfirmedNotes_NormalBG = -1
  SELF.Q.pnt:ConfirmedNotes_SelectedFG = -1
  SELF.Q.pnt:ConfirmedNotes_SelectedBG = -1


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

