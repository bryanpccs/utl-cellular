

   MEMBER('sidalertbrowse.clw')                       ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SIDAL001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SIDAL002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SIDAL004.INC'),ONCE        !Req'd for module callout resolution
                     END



Main PROCEDURE                                        !Generated from procedure template - Browse

locRetailer          STRING(30)
locBookingType       STRING(30)
tmp:DaysToBeSent     STRING(60)
tmp:SMSActive        STRING(3)
tmp:EmailActive      STRING(3)
tmp:SendTo           STRING(8)
tmp:Reminders        STRING(3)
tmp:RETAIL           STRING('RETAIL REPAIR')
tmp:RETAILACC        STRING('RETAIL REPAIR (ACC) {11}')
tmp:POSTALACC        STRING('POSTAL (ACC) {18}')
tmp:RETAILEXCHANGE   STRING('RETAIL IN-STORE EXCHANGE')
tmp:EXCHANGE         STRING('EXCHANGE')
tmp:POSTAL           STRING('POSTAL')
tmp:EXTERNALEXCHANGE STRING('RETAIL EXTERNAL EXCHANGE')
tmp:7DAYEXCHANGE     STRING('7 DAY EXCHANGE')
tmp:ACCESSORYORDER   STRING('ACCESSORY ORDER')
tmp:LEASEPHONEREPAIR STRING('LEASE PHONE REPAIR')
tmp:LEASEPHONEEXCHANGE STRING('LEASE PHONE EXCHANGE')
tmp:CSG              STRING('CSG')
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?locRetailer
rtr:Retailer           LIKE(rtr:Retailer)             !List box control field - type derived from field
rtr:RecordNumber       LIKE(rtr:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(SIDALERT)
                       PROJECT(sdl:Status)
                       PROJECT(sdl:Description)
                       PROJECT(sdl:RecordNumber)
                       PROJECT(sdl:BookingType)
                       PROJECT(sdl:Retailer)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Status_NormalFG    LONG                           !Normal forground color
sts:Status_NormalBG    LONG                           !Normal background color
sts:Status_SelectedFG  LONG                           !Selected forground color
sts:Status_SelectedBG  LONG                           !Selected background color
sdl:Status             LIKE(sdl:Status)               !List box control field - type derived from field
sdl:Status_NormalFG    LONG                           !Normal forground color
sdl:Status_NormalBG    LONG                           !Normal background color
sdl:Status_SelectedFG  LONG                           !Selected forground color
sdl:Status_SelectedBG  LONG                           !Selected background color
sdl:Description        LIKE(sdl:Description)          !List box control field - type derived from field
sdl:Description_NormalFG LONG                         !Normal forground color
sdl:Description_NormalBG LONG                         !Normal background color
sdl:Description_SelectedFG LONG                       !Selected forground color
sdl:Description_SelectedBG LONG                       !Selected background color
tmp:DaysToBeSent       LIKE(tmp:DaysToBeSent)         !List box control field - type derived from local data
tmp:DaysToBeSent_NormalFG LONG                        !Normal forground color
tmp:DaysToBeSent_NormalBG LONG                        !Normal background color
tmp:DaysToBeSent_SelectedFG LONG                      !Selected forground color
tmp:DaysToBeSent_SelectedBG LONG                      !Selected background color
tmp:SMSActive          LIKE(tmp:SMSActive)            !List box control field - type derived from local data
tmp:SMSActive_NormalFG LONG                           !Normal forground color
tmp:SMSActive_NormalBG LONG                           !Normal background color
tmp:SMSActive_SelectedFG LONG                         !Selected forground color
tmp:SMSActive_SelectedBG LONG                         !Selected background color
tmp:EmailActive        LIKE(tmp:EmailActive)          !List box control field - type derived from local data
tmp:EmailActive_NormalFG LONG                         !Normal forground color
tmp:EmailActive_NormalBG LONG                         !Normal background color
tmp:EmailActive_SelectedFG LONG                       !Selected forground color
tmp:EmailActive_SelectedBG LONG                       !Selected background color
tmp:SendTo             LIKE(tmp:SendTo)               !List box control field - type derived from local data
tmp:SendTo_NormalFG    LONG                           !Normal forground color
tmp:SendTo_NormalBG    LONG                           !Normal background color
tmp:SendTo_SelectedFG  LONG                           !Selected forground color
tmp:SendTo_SelectedBG  LONG                           !Selected background color
tmp:Reminders          LIKE(tmp:Reminders)            !List box control field - type derived from local data
tmp:Reminders_NormalFG LONG                           !Normal forground color
tmp:Reminders_NormalBG LONG                           !Normal background color
tmp:Reminders_SelectedFG LONG                         !Selected forground color
tmp:Reminders_SelectedBG LONG                         !Selected background color
sdl:RecordNumber       LIKE(sdl:RecordNumber)         !Primary key field - type derived from field
sdl:BookingType        LIKE(sdl:BookingType)          !Browse key field - type derived from field
sdl:Retailer           LIKE(sdl:Retailer)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::sdl:BookingType     LIKE(sdl:BookingType)
HK12::sdl:Retailer        LIKE(sdl:Retailer)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB8::View:FileDropCombo VIEW(RETAILER)
                       PROJECT(rtr:Retailer)
                       PROJECT(rtr:RecordNumber)
                     END
window               WINDOW('SID Alert Browse'),AT(,,776,243),FONT('Tahoma',,,),CENTER,ICON('pc.ico'),GRAY,MAX,DOUBLE,IMM
                       SHEET,AT(4,4,708,236),USE(?CurrentTab),WIZARD,SPREAD
                         TAB('By Retail Repair (Fonecare)'),USE(?Tab1)
                           PROMPT('Original Retailer'),AT(8,12),USE(?Prompt2)
                           COMBO(@s30),AT(88,12,124,10),USE(locRetailer),IMM,VSCROLL,FORMAT('120L(2)|M@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                           COMBO(@s30),AT(88,26,124,10),USE(locBookingType),VSCROLL,LEFT(2),FORMAT('120L(2)|M@s30@'),DROP(10,200),FROM('RETAIL REPAIR|RETAIL REPAIR (ACC)|RETAIL IN-STORE EXCHANGE|RETAIL EXTERNAL EXCHA' &|
   'NGE|EXCHANGE|POSTAL|POSTAL (ACC)|7 DAY EXCHANGE|ACCESSORY ORDER|LEASE PHONE REPA' &|
   'IR|LEASE PHONE EXCHANGE|CSG|JOHN LEWIS RETAIL REPAIRS|JOHN LEWIS POSTAL')
                           PROMPT('Booking Type'),AT(8,26),USE(?Prompt2:2)
                         END
                       END
                       LIST,AT(8,40,700,182),USE(?List),IMM,MSG('Browsing Records'),FORMAT('128L(2)|M*~Status~@s30@0L(2)|M*~Status~@s30@188L(2)|M*~Description~@s60@100L(2)|' &|
   'M*~Days To Be Sent~@s60@46L(2)|M*~SMS Active~@s3@46L(2)|M*~Email Active~@s3@43L(' &|
   '2)|M*~Send To~@s8@12L(2)|M*~Reminders~@s3@'),FROM(Queue:Browse)
                       BUTTON('Alert Defaults'),AT(716,18,56,16),USE(?Button:AlertDefaults)
                       BUTTON('Merge Fields'),AT(716,36,56,16),USE(?Button:MergeFields)
                       BUTTON('&Insert'),AT(716,160,56,16),USE(?Insert),LEFT,ICON('insert.ico')
                       BUTTON('&Change'),AT(716,180,56,16),USE(?Change),LEFT,ICON('edit.ico')
                       BUTTON('&Delete'),AT(716,200,56,16),USE(?Delete),LEFT,ICON('delete.ico')
                       BUTTON('Close'),AT(716,224,56,16),USE(?Close),LEFT,ICON('cancel.ico')
                       PANEL,AT(8,226,13,10),USE(?Panel1),FILL(COLOR:Red)
                       PROMPT('- Non Standard Notifications'),AT(24,226),USE(?Prompt1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?locRetailer{prop:ReadOnly} = True
        ?locRetailer{prop:FontColor} = 65793
        ?locRetailer{prop:Color} = 15066597
    Elsif ?locRetailer{prop:Req} = True
        ?locRetailer{prop:FontColor} = 65793
        ?locRetailer{prop:Color} = 8454143
    Else ! If ?locRetailer{prop:Req} = True
        ?locRetailer{prop:FontColor} = 65793
        ?locRetailer{prop:Color} = 16777215
    End ! If ?locRetailer{prop:Req} = True
    ?locRetailer{prop:Trn} = 0
    ?locRetailer{prop:FontStyle} = font:Bold
    If ?locBookingType{prop:ReadOnly} = True
        ?locBookingType{prop:FontColor} = 65793
        ?locBookingType{prop:Color} = 15066597
    Elsif ?locBookingType{prop:Req} = True
        ?locBookingType{prop:FontColor} = 65793
        ?locBookingType{prop:Color} = 8454143
    Else ! If ?locBookingType{prop:Req} = True
        ?locBookingType{prop:FontColor} = 65793
        ?locBookingType{prop:Color} = 16777215
    End ! If ?locBookingType{prop:Req} = True
    ?locBookingType{prop:Trn} = 0
    ?locBookingType{prop:FontStyle} = font:Bold
    ?Prompt2:2{prop:FontColor} = -1
    ?Prompt2:2{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
WorkOutDays        Routine
    tmp:DaysToBeSent = ''
    If sdl:Monday And sdl:Tuesday And sdl:Wednesday And sdl:Thursday And sdl:Friday And sdl:Saturday And sdl:Sunday
        tmp:DaysToBeSent = 'Everyday'
        Exit
    End ! If sdl:Monday & sdl:Tuesday & sdl:Wednesday & sdl:Thursday & sdl:Friday & sdl:Saturday & sdl:Sunday

    If sdl:Monday And sdl:Tuesday And sdl:Wednesday And sdl:Thursday And sdl:Friday And ~sdl:Saturday And ~sdl:Sunday
        tmp:DaysToBeSent = 'Mon to Fri'
        Exit
    End ! If sdl:Monday & sdl:Tuesday & sdl:Wednesday & sdl:Thursday & sdl:Friday & ~sdl:Saturday & ~sdl:Sunday

    If sdl:Monday And sdl:Tuesday And sdl:Wednesday And sdl:Thursday And ~sdl:Friday And ~sdl:Saturday And ~sdl:Sunday
        tmp:DaysToBeSent = 'Mon to Thu'
        Exit
    End ! If sdl:Monday & sdl:Tuesday & sdl:Wednesday & sdl:Thursday & sdl:Friday & ~sdl:Saturday & ~sdl:Sunday

    If sdl:Monday And sdl:Tuesday And sdl:Wednesday And ~sdl:Thursday And ~sdl:Friday And ~sdl:Saturday And ~sdl:Sunday
        tmp:DaysToBeSent = 'Mon to Wed'
        Exit
    End ! If sdl:Monday & sdl:Tuesday & sdl:Wednesday & sdl:Thursday & sdl:Friday & ~sdl:Saturday & ~sdl:Sunday

    If sdl:Monday
        tmp:DaysToBeSent = Clip(tmp:DaysToBeSent) & ', ' & 'Mon'
    End ! If sdl:Monday

    If sdl:Tuesday
        tmp:DaysToBeSent = Clip(tmp:DaysToBeSent) & ', ' & 'Tue'
    End ! If sdl:Tuesday

    If sdl:Wednesday
        tmp:DaysToBeSent = Clip(tmp:DaysToBeSent) & ', ' & 'Wed'
    End ! If sdl:Wednesday

    If sdl:Thursday
        tmp:DaysToBeSent = Clip(tmp:DaysToBeSent) & ', ' & 'Thu'
    End ! If sdl:Thursday

    If sdl:Friday
        tmp:DaysToBeSent = Clip(tmp:DaysToBeSent) & ', ' & 'Fri'
    End ! If sdl:Friday

    If sdl:Saturday
        tmp:DaysToBeSent = Clip(tmp:DaysToBeSent) & ', ' & 'Sat'
    End ! If sdl:Saturday

    If sdl:Sunday
        tmp:DaysToBeSent = Clip(tmp:DaysToBeSent) & ', ' & 'Sun'
    End ! If sdl:Sunday

    tmp:DaysToBeSent = Clip(Sub(tmp:DaysToBeSent,3,255))

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RETAILER.Open
  Relate:SIDALERT.Open
  Relate:STATUS.Open
  Relate:USERS.Open
  Access:SIDREMIN.UseFile
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:SIDALERT,SELF)
  OPEN(window)
  SELF.Opened=True
  error# = 1
  Loop x# = 1 To Len(Clip(Command()))
      If Sub(Command(),x#,1) = '%'
          glo:Password = Clip(Sub(Command(),x#+1,30))
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              error# = 0
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  
          Break
      End!If Sub(Command(),x#,1) = '%'
  End!Loop x# = 1 To Len(Comman())
  If error# = 1
      glo:Select1 = 'N'
      Insert_Password
      If glo:Select1 = 'Y'
          Halt()
      Else ! If glo:Select1 = 'Y'
  
      End ! If glo:Select1 = 'Y'
  End
  
  
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,sdl:BookingRetailerStatusKey)
  BRW2.AddRange(sdl:Retailer)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,sdl:Status,1,BRW2)
  BIND('tmp:DaysToBeSent',tmp:DaysToBeSent)
  BIND('tmp:SMSActive',tmp:SMSActive)
  BIND('tmp:EmailActive',tmp:EmailActive)
  BIND('tmp:SendTo',tmp:SendTo)
  BIND('tmp:Reminders',tmp:Reminders)
  BRW2.AddField(sts:Status,BRW2.Q.sts:Status)
  BRW2.AddField(sdl:Status,BRW2.Q.sdl:Status)
  BRW2.AddField(sdl:Description,BRW2.Q.sdl:Description)
  BRW2.AddField(tmp:DaysToBeSent,BRW2.Q.tmp:DaysToBeSent)
  BRW2.AddField(tmp:SMSActive,BRW2.Q.tmp:SMSActive)
  BRW2.AddField(tmp:EmailActive,BRW2.Q.tmp:EmailActive)
  BRW2.AddField(tmp:SendTo,BRW2.Q.tmp:SendTo)
  BRW2.AddField(tmp:Reminders,BRW2.Q.tmp:Reminders)
  BRW2.AddField(sdl:RecordNumber,BRW2.Q.sdl:RecordNumber)
  BRW2.AddField(sdl:BookingType,BRW2.Q.sdl:BookingType)
  BRW2.AddField(sdl:Retailer,BRW2.Q.sdl:Retailer)
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW2.AskProcedure = 1
  FDCB8.Init(locRetailer,?locRetailer,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:RETAILER,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder(rtr:RetailerKey)
  FDCB8.AddField(rtr:Retailer,FDCB8.Q.rtr:Retailer)
  FDCB8.AddField(rtr:RecordNumber,FDCB8.Q.rtr:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  BRW2.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETAILER.Close
    Relate:SIDALERT.Close
    Relate:STATUS.Close
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  If Request = DeleteRecord
      brw2.UpdateViewRecord()
      If sdl:SystemAlert
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('You cannot delete a "System Alert".','ServiceBase',|
                         icon:Hand,'&OK',1,1) 
              Of 1 ! &OK Button
          End!Case Message
          Return RequestCancelled
      End ! If sdl:SystemAlert
  End ! If self.Request = DeleteRecord
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSIDAlert
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?locRetailer
      BRW2.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW2.ApplyRange
      BRW2.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?locBookingType
      BRW2.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW2.ApplyRange
      BRW2.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Button:AlertDefaults
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      SIDAlertDefaults
      ThisWindow.Reset
    OF ?Button:MergeFields
      ThisWindow.Update
      BrowseMerge('')
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?locRetailer
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW2.ApplyRange
      BRW2.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?locBookingType
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW2.ApplyRange
      BRW2.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = locBookingType
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = locRetailer
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW2.SetQueueRecord PROCEDURE

  CODE
  IF (sdl:EmailAlertActive)
    tmp:EmailActive = 'Yes'
  ELSE
    tmp:EmailActive = 'No'
  END
  IF (sdl:SMSAlertActive)
    tmp:SMSActive = 'Yes'
  ELSE
    tmp:SMSActive = 'No'
  END
  CASE (sdl:SendTo)
  OF 1
    tmp:SendTo = 'Customer'
  OF 2
    tmp:SendTo = 'UTL'
  OF 3
    tmp:SendTo = 'Both'
  ELSE
    tmp:SendTo = 'Not Set'
  END
  Do WorkOutDays
  Found# = 0
  Access:SIDREMIN.Clearkey(sdm:ElapsedKey)
  sdm:SIDALERTRecordNumber = sdl:RecordNumber
  Set(sdm:ElapsedKey,sdm:ElapsedKey)
  Loop ! Begin Loop
      If Access:SIDREMIN.Next()
          Break
      End ! If Access:SIDREMIN.Next()
      If sdm:SIDALERTRecordNumber <> sdl:RecordNumber
          Break
      End ! If sdm:SIDALERTRecordNumber <> sdl:RecordNumber
      Found#  = 1
      Break
  End ! Loop
  If Found#
      tmp:Reminders = 'Yes'
  Else ! If Found#
      tmp:Reminders = 'No'
  End ! If Found#
  
  Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
  sts:Ref_Number = sdl:Status
  If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
      !Found
  Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
      !Error
  End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
  PARENT.SetQueueRecord
  SELF.Q.sts:Status_NormalFG = -1
  SELF.Q.sts:Status_NormalBG = -1
  SELF.Q.sts:Status_SelectedFG = -1
  SELF.Q.sts:Status_SelectedBG = -1
  SELF.Q.sdl:Status_NormalFG = -1
  SELF.Q.sdl:Status_NormalBG = -1
  SELF.Q.sdl:Status_SelectedFG = -1
  SELF.Q.sdl:Status_SelectedBG = -1
  SELF.Q.sdl:Description_NormalFG = -1
  SELF.Q.sdl:Description_NormalBG = -1
  SELF.Q.sdl:Description_SelectedFG = -1
  SELF.Q.sdl:Description_SelectedBG = -1
  SELF.Q.tmp:DaysToBeSent_NormalFG = -1
  SELF.Q.tmp:DaysToBeSent_NormalBG = -1
  SELF.Q.tmp:DaysToBeSent_SelectedFG = -1
  SELF.Q.tmp:DaysToBeSent_SelectedBG = -1
  SELF.Q.tmp:SMSActive_NormalFG = -1
  SELF.Q.tmp:SMSActive_NormalBG = -1
  SELF.Q.tmp:SMSActive_SelectedFG = -1
  SELF.Q.tmp:SMSActive_SelectedBG = -1
  SELF.Q.tmp:EmailActive_NormalFG = -1
  SELF.Q.tmp:EmailActive_NormalBG = -1
  SELF.Q.tmp:EmailActive_SelectedFG = -1
  SELF.Q.tmp:EmailActive_SelectedBG = -1
  SELF.Q.tmp:SendTo_NormalFG = -1
  SELF.Q.tmp:SendTo_NormalBG = -1
  SELF.Q.tmp:SendTo_SelectedFG = -1
  SELF.Q.tmp:SendTo_SelectedBG = -1
  SELF.Q.tmp:Reminders_NormalFG = -1
  SELF.Q.tmp:Reminders_NormalBG = -1
  SELF.Q.tmp:Reminders_SelectedFG = -1
  SELF.Q.tmp:Reminders_SelectedBG = -1
  SELF.Q.tmp:SMSActive = tmp:SMSActive                !Assign formula result to display queue
  SELF.Q.tmp:EmailActive = tmp:EmailActive            !Assign formula result to display queue
  SELF.Q.tmp:SendTo = tmp:SendTo                      !Assign formula result to display queue
   
   
   IF (sdl:SystemAlert)
     SELF.Q.sts:Status_NormalFG = 255
     SELF.Q.sts:Status_NormalBG = 16777215
     SELF.Q.sts:Status_SelectedFG = 16777215
     SELF.Q.sts:Status_SelectedBG = 255
   ELSE
     SELF.Q.sts:Status_NormalFG = -1
     SELF.Q.sts:Status_NormalBG = -1
     SELF.Q.sts:Status_SelectedFG = -1
     SELF.Q.sts:Status_SelectedBG = -1
   END
   IF (sdl:SystemAlert)
     SELF.Q.sdl:Status_NormalFG = 255
     SELF.Q.sdl:Status_NormalBG = 16777215
     SELF.Q.sdl:Status_SelectedFG = 16777215
     SELF.Q.sdl:Status_SelectedBG = 255
   ELSE
     SELF.Q.sdl:Status_NormalFG = -1
     SELF.Q.sdl:Status_NormalBG = -1
     SELF.Q.sdl:Status_SelectedFG = -1
     SELF.Q.sdl:Status_SelectedBG = -1
   END
   IF (sdl:SystemAlert)
     SELF.Q.sdl:Description_NormalFG = 255
     SELF.Q.sdl:Description_NormalBG = 16777215
     SELF.Q.sdl:Description_SelectedFG = 16777215
     SELF.Q.sdl:Description_SelectedBG = 255
   ELSE
     SELF.Q.sdl:Description_NormalFG = -1
     SELF.Q.sdl:Description_NormalBG = -1
     SELF.Q.sdl:Description_SelectedFG = -1
     SELF.Q.sdl:Description_SelectedBG = -1
   END
   IF (sdl:SystemAlert)
     SELF.Q.tmp:DaysToBeSent_NormalFG = 255
     SELF.Q.tmp:DaysToBeSent_NormalBG = 16777215
     SELF.Q.tmp:DaysToBeSent_SelectedFG = 16777215
     SELF.Q.tmp:DaysToBeSent_SelectedBG = 255
   ELSE
     SELF.Q.tmp:DaysToBeSent_NormalFG = -1
     SELF.Q.tmp:DaysToBeSent_NormalBG = -1
     SELF.Q.tmp:DaysToBeSent_SelectedFG = -1
     SELF.Q.tmp:DaysToBeSent_SelectedBG = -1
   END
   IF (sdl:SystemAlert)
     SELF.Q.tmp:SMSActive_NormalFG = 255
     SELF.Q.tmp:SMSActive_NormalBG = 16777215
     SELF.Q.tmp:SMSActive_SelectedFG = 16777215
     SELF.Q.tmp:SMSActive_SelectedBG = 255
   ELSE
     SELF.Q.tmp:SMSActive_NormalFG = -1
     SELF.Q.tmp:SMSActive_NormalBG = -1
     SELF.Q.tmp:SMSActive_SelectedFG = -1
     SELF.Q.tmp:SMSActive_SelectedBG = -1
   END
   IF (sdl:SystemAlert)
     SELF.Q.tmp:EmailActive_NormalFG = 255
     SELF.Q.tmp:EmailActive_NormalBG = 16777215
     SELF.Q.tmp:EmailActive_SelectedFG = 16777215
     SELF.Q.tmp:EmailActive_SelectedBG = 255
   ELSE
     SELF.Q.tmp:EmailActive_NormalFG = -1
     SELF.Q.tmp:EmailActive_NormalBG = -1
     SELF.Q.tmp:EmailActive_SelectedFG = -1
     SELF.Q.tmp:EmailActive_SelectedBG = -1
   END
   IF (sdl:SystemAlert)
     SELF.Q.tmp:SendTo_NormalFG = 255
     SELF.Q.tmp:SendTo_NormalBG = 16777215
     SELF.Q.tmp:SendTo_SelectedFG = 16777215
     SELF.Q.tmp:SendTo_SelectedBG = 255
   ELSE
     SELF.Q.tmp:SendTo_NormalFG = -1
     SELF.Q.tmp:SendTo_NormalBG = -1
     SELF.Q.tmp:SendTo_SelectedFG = -1
     SELF.Q.tmp:SendTo_SelectedBG = -1
   END
   IF (sdl:SystemAlert)
     SELF.Q.tmp:Reminders_NormalFG = 255
     SELF.Q.tmp:Reminders_NormalBG = 16777215
     SELF.Q.tmp:Reminders_SelectedFG = 16777215
     SELF.Q.tmp:Reminders_SelectedBG = 255
   ELSE
     SELF.Q.tmp:Reminders_NormalFG = -1
     SELF.Q.tmp:Reminders_NormalBG = -1
     SELF.Q.tmp:Reminders_SelectedFG = -1
     SELF.Q.tmp:Reminders_SelectedBG = -1
   END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Button:AlertDefaults, Resize:FixRight+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Button:MergeFields, Resize:FixRight+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom)
  SELF.SetStrategy(?List, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom)

