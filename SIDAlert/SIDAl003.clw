

   MEMBER('sidalertbrowse.clw')                       ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SIDAL003.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSIDAlertReminder PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
CutOffTimeList       QUEUE,PRE(cutoff)
CutOffTime           TIME
                     END
tmp:SMSActive        STRING(3)
tmp:EmailActive      STRING(3)
History::sdm:Record  LIKE(sdm:RECORD),STATIC
QuickWindow          WINDOW('Update the SIDALERT File'),AT(,,339,415),FONT('Tahoma',8,,),CENTER,IMM,HLP('UpdateSIDAlert'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,332,116),USE(?Sheet1),SPREAD
                         TAB('General Defaults'),USE(?Tab1)
                           PROMPT('Status'),AT(8,20),USE(?sdm:Status:Prompt)
                           STRING(@s30),AT(84,20),USE(sts:Status),FONT(,,,FONT:bold)
                           PROMPT('WARNING:  A reminder notification already exists with the same ELAPSED DAYS and ' &|
   'SEND TO combination.  Please amend your selection as you will be unable to save ' &|
   'this notification in its current state.'),AT(8,92,324,26),USE(?Prompt:Error),TRN,HIDE,FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(84,34,124,10),USE(sdm:Description),FONT(,,,,CHARSET:ANSI),REQ,UPR,SINGLE,MSG('Description')
                           TEXT,AT(84,48,64,10),USE(sdm:ElapsedDays),REQ,SINGLE,MSG('Elapsed Days')
                           PROMPT('Elapsed Days'),AT(8,48),USE(?sdm:ElapsedDays:2)
                           OPTION('Send To'),AT(84,62,228,28),USE(sdm:SendTo),BOXED,MSG('Send To')
                             RADIO('Customer Only'),AT(88,74),USE(?sdm:SendTo:Radio1),VALUE('1')
                             RADIO('UTL Only'),AT(180,74),USE(?sdm:SendTo:Radio2),VALUE('2')
                             RADIO('Both'),AT(256,74),USE(?sdm:SendTo:Radio3),VALUE('3')
                           END
                           PROMPT('Description'),AT(8,34),USE(?sdm:Description:Prompt)
                         END
                       END
                       CHECK('SMS Alert Active'),AT(8,126),USE(sdm:SMSAlertActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                       SHEET,AT(4,122,332,124),USE(?Sheet2),WIZARD,SPREAD
                         TAB('SMS Notification'),USE(?Tab2)
                           OPTION('Bank Holidays'),AT(80,138,156,38),USE(sdm:BankHolidays),BOXED,MSG('Bank Holidays')
                             RADIO('Send on bank holidays'),AT(84,150),USE(?sdm:BankHolidays:Radio1),TRN
                             RADIO('Send next working day after bank holiday'),AT(84,162),USE(?sdm:BankHolidays:Radio2),TRN
                           END
                           PROMPT('Message Text'),AT(8,178),USE(?sdm:SMSMessageText:Prompt)
                           TEXT,AT(80,180,252,51),USE(sdm:SMSMessageText),VSCROLL,LEFT,MSG('SMS Message Text'),TIP('SMS Message Text'),REQ
                           BUTTON('Add Merge Field'),AT(8,214,68,16),USE(?Button:AddMergeField:2)
                           PROMPT('Text Length (including merge fields): '),AT(80,231),USE(?Prompt6)
                           PROMPT('0'),AT(200,231),USE(?Prompt:SMSTextLength)
                         END
                       END
                       CHECK('Email Alert Active'),AT(8,252),USE(sdm:EmailAlertActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                       SHEET,AT(4,248,332,138),USE(?Sheet3),WIZARD,SPREAD
                         TAB('Email Notification'),USE(?Tab3)
                           PROMPT('Message Subject'),AT(8,266),USE(?sdm:EmailMessageSubject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           TEXT,AT(80,266,252,10),USE(sdm:EmailSubject),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Message Subject'),TIP('Email Message Subject'),REQ,SINGLE
                           PROMPT('Message Text'),AT(8,280),USE(?sdm2:EmailMessageText:Prompt)
                           TEXT,AT(80,282,252,82),USE(sdm2:EmailMessageText),VSCROLL,LEFT,MSG('Email Message Text'),TIP('Email Message Text'),REQ
                           BUTTON('Add Merge Field'),AT(8,346,68,16),USE(?Button:AddMergeField)
                           PROMPT('Text Length (including merge fields): '),AT(80,370),USE(?Prompt6:2)
                           PROMPT('0'),AT(200,370),USE(?Prompt:EmailTextLength)
                         END
                       END
                       BUTTON('OK'),AT(220,392,56,16),USE(?OK),LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('Cancel'),AT(280,392,56,16),USE(?Cancel),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?sdm:Status:Prompt{prop:FontColor} = -1
    ?sdm:Status:Prompt{prop:Color} = 15066597
    ?sts:Status{prop:FontColor} = -1
    ?sts:Status{prop:Color} = 15066597
    If ?sdm:Description{prop:ReadOnly} = True
        ?sdm:Description{prop:FontColor} = 65793
        ?sdm:Description{prop:Color} = 15066597
    Elsif ?sdm:Description{prop:Req} = True
        ?sdm:Description{prop:FontColor} = 65793
        ?sdm:Description{prop:Color} = 8454143
    Else ! If ?sdm:Description{prop:Req} = True
        ?sdm:Description{prop:FontColor} = 65793
        ?sdm:Description{prop:Color} = 16777215
    End ! If ?sdm:Description{prop:Req} = True
    ?sdm:Description{prop:Trn} = 0
    ?sdm:Description{prop:FontStyle} = font:Bold
    If ?sdm:ElapsedDays{prop:ReadOnly} = True
        ?sdm:ElapsedDays{prop:FontColor} = 65793
        ?sdm:ElapsedDays{prop:Color} = 15066597
    Elsif ?sdm:ElapsedDays{prop:Req} = True
        ?sdm:ElapsedDays{prop:FontColor} = 65793
        ?sdm:ElapsedDays{prop:Color} = 8454143
    Else ! If ?sdm:ElapsedDays{prop:Req} = True
        ?sdm:ElapsedDays{prop:FontColor} = 65793
        ?sdm:ElapsedDays{prop:Color} = 16777215
    End ! If ?sdm:ElapsedDays{prop:Req} = True
    ?sdm:ElapsedDays{prop:Trn} = 0
    ?sdm:ElapsedDays{prop:FontStyle} = font:Bold
    ?sdm:ElapsedDays:2{prop:FontColor} = -1
    ?sdm:ElapsedDays:2{prop:Color} = 15066597
    ?sdm:SendTo{prop:Font,3} = -1
    ?sdm:SendTo{prop:Color} = 15066597
    ?sdm:SendTo{prop:Trn} = 0
    ?sdm:SendTo:Radio1{prop:Font,3} = -1
    ?sdm:SendTo:Radio1{prop:Color} = 15066597
    ?sdm:SendTo:Radio1{prop:Trn} = 0
    ?sdm:SendTo:Radio2{prop:Font,3} = -1
    ?sdm:SendTo:Radio2{prop:Color} = 15066597
    ?sdm:SendTo:Radio2{prop:Trn} = 0
    ?sdm:SendTo:Radio3{prop:Font,3} = -1
    ?sdm:SendTo:Radio3{prop:Color} = 15066597
    ?sdm:SendTo:Radio3{prop:Trn} = 0
    ?sdm:Description:Prompt{prop:FontColor} = -1
    ?sdm:Description:Prompt{prop:Color} = 15066597
    ?sdm:SMSAlertActive{prop:Font,3} = -1
    ?sdm:SMSAlertActive{prop:Color} = 15066597
    ?sdm:SMSAlertActive{prop:Trn} = 0
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?sdm:BankHolidays{prop:Font,3} = -1
    ?sdm:BankHolidays{prop:Color} = 15066597
    ?sdm:BankHolidays{prop:Trn} = 0
    ?sdm:BankHolidays:Radio1{prop:Font,3} = -1
    ?sdm:BankHolidays:Radio1{prop:Color} = 15066597
    ?sdm:BankHolidays:Radio1{prop:Trn} = 0
    ?sdm:BankHolidays:Radio2{prop:Font,3} = -1
    ?sdm:BankHolidays:Radio2{prop:Color} = 15066597
    ?sdm:BankHolidays:Radio2{prop:Trn} = 0
    ?sdm:SMSMessageText:Prompt{prop:FontColor} = -1
    ?sdm:SMSMessageText:Prompt{prop:Color} = 15066597
    If ?sdm:SMSMessageText{prop:ReadOnly} = True
        ?sdm:SMSMessageText{prop:FontColor} = 65793
        ?sdm:SMSMessageText{prop:Color} = 15066597
    Elsif ?sdm:SMSMessageText{prop:Req} = True
        ?sdm:SMSMessageText{prop:FontColor} = 65793
        ?sdm:SMSMessageText{prop:Color} = 8454143
    Else ! If ?sdm:SMSMessageText{prop:Req} = True
        ?sdm:SMSMessageText{prop:FontColor} = 65793
        ?sdm:SMSMessageText{prop:Color} = 16777215
    End ! If ?sdm:SMSMessageText{prop:Req} = True
    ?sdm:SMSMessageText{prop:Trn} = 0
    ?sdm:SMSMessageText{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Prompt:SMSTextLength{prop:FontColor} = -1
    ?Prompt:SMSTextLength{prop:Color} = 15066597
    ?sdm:EmailAlertActive{prop:Font,3} = -1
    ?sdm:EmailAlertActive{prop:Color} = 15066597
    ?sdm:EmailAlertActive{prop:Trn} = 0
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?sdm:EmailMessageSubject:Prompt{prop:FontColor} = -1
    ?sdm:EmailMessageSubject:Prompt{prop:Color} = 15066597
    If ?sdm:EmailSubject{prop:ReadOnly} = True
        ?sdm:EmailSubject{prop:FontColor} = 65793
        ?sdm:EmailSubject{prop:Color} = 15066597
    Elsif ?sdm:EmailSubject{prop:Req} = True
        ?sdm:EmailSubject{prop:FontColor} = 65793
        ?sdm:EmailSubject{prop:Color} = 8454143
    Else ! If ?sdm:EmailSubject{prop:Req} = True
        ?sdm:EmailSubject{prop:FontColor} = 65793
        ?sdm:EmailSubject{prop:Color} = 16777215
    End ! If ?sdm:EmailSubject{prop:Req} = True
    ?sdm:EmailSubject{prop:Trn} = 0
    ?sdm:EmailSubject{prop:FontStyle} = font:Bold
    ?sdm2:EmailMessageText:Prompt{prop:FontColor} = -1
    ?sdm2:EmailMessageText:Prompt{prop:Color} = 15066597
    If ?sdm2:EmailMessageText{prop:ReadOnly} = True
        ?sdm2:EmailMessageText{prop:FontColor} = 65793
        ?sdm2:EmailMessageText{prop:Color} = 15066597
    Elsif ?sdm2:EmailMessageText{prop:Req} = True
        ?sdm2:EmailMessageText{prop:FontColor} = 65793
        ?sdm2:EmailMessageText{prop:Color} = 8454143
    Else ! If ?sdm2:EmailMessageText{prop:Req} = True
        ?sdm2:EmailMessageText{prop:FontColor} = 65793
        ?sdm2:EmailMessageText{prop:Color} = 16777215
    End ! If ?sdm2:EmailMessageText{prop:Req} = True
    ?sdm2:EmailMessageText{prop:Trn} = 0
    ?sdm2:EmailMessageText{prop:FontStyle} = font:Bold
    ?Prompt6:2{prop:FontColor} = -1
    ?Prompt6:2{prop:Color} = 15066597
    ?Prompt:EmailTextLength{prop:FontColor} = -1
    ?Prompt:EmailTextLength{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
DuplicateAlertCheck        Routine
    ?Prompt:Error{prop:Hide} = 1
    ?OK{prop:Disable} = 0

    Access:SIDREMIN_ALIAS.Clearkey(sdm_ali:ElapsedKey)
    sdm_ali:SIDALERTRecordNumber = sdm:SIDALERTRecordNumber
    sdm_ali:ElapsedDays = sdm:ElapsedDays
    Set(sdm_ali:ElapsedKey,sdm_ali:ElapsedKey)
    Loop ! Begin Loop
        If Access:SIDREMIN_ALIAS.Next()
            Break
        End ! If Access:SIDREMIN_ALIAS.Next()
        If sdm_ali:SIDALERTRecordNumber <> sdm:SIDALERTRecordNumber
            Break
        End ! If sdm_ali:SIDALERTRecordNumber <> sdm:SIDALERTRecordNumber
        If sdm_ali:ElapsedDays <> sdm:ElapsedDays
            Break
        End ! If sdm_ali:ElapsedDays <> sdm:ElapsedDays
        IF sdm_ali:RecordNumber = sdm:RecordNumber
            Cycle
        End ! IF sdm_ali:RecordNumber = sdm:RecordNumber

        If sdm_ali:SendTo <> 3 And sdm:SendTo <> 3
            If sdm_ali:SendTo <> sdm:SendTo
                Cycle
            End ! If sdm_ali:SendTo <> sdm:SendTo
        End ! If sdm_ali:SendTo <> 3 And sdm:SendTo <> 3
        ?Prompt:Error{prop:Hide} = 0
        ?OK{prop:Disable} = 1
        Break
    End ! Loop
    Display()
GetTextLength            Routine
Data
local:Length            Long()
local:Start             Long()
local:End               Long()
Code
    local:Start = 0
    local:End = 0
    local:Length = 0
    Loop x# = 1 To Len(Clip(sdm:SMSMessageText))
        If Sub(sdm:SMSMessageText,x#,1) = '<13>' Or Sub(sdm:SMSMessageText,x#,1) = '<10>'
            Cycle
        End ! If Sub(sdm:SMSMessageText,x#,1) = '<13' Or Sub(sdm:SMSMessageText,x#,1) = '<10>'

        If Sub(sdm:SMSMessageText,x#,2) = '[*'
            local:Start = x#
            local:End = 0
        End ! If Sub(sdm:SMSMessageText,x#,2) = '[*'
        If Sub(sdm:SMSMessageText,x#,2) = '*]'
            local:End = x#
        End ! If Sub(sdm:SMSMessageText,x#,2) = '*]'

        If local:Start = 0
            local:Length += 1
        Else ! If local:Start = 0
            If local:End > 0
                Access:MERGE.ClearKey(mer:FieldNameKey)
                mer:FieldName = Sub(sdm:SMSMessageText,local:Start + 2,local:End - local:Start - 2)
                If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Found
                    local:Length += (mer:TotalFieldLength - 1)
                    local:Start = 0
                    local:End = 0
                Else ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Error
                End ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
            End ! If local:End > 0
        End ! If local:Start = 0
    End ! Loop x# = 1 To 500

    ?Prompt:SMSTextLength{prop:Text} = local:Length & '/480'
    If local:Length > 480
        ?Prompt:SMSTextLength{prop:fontColor} = color:Red
    Else ! If local:Length > 480
        ?Prompt:SMSTextLength{prop:fontColor} = color:None
    End ! If local:Length > 480

    local:Start = 0
    local:End = 0
    local:Length = 0
    Loop x# = 1 To Len(Clip(sdm2:EmailMessageText))
        If Sub(sdm2:EmailMessageText,x#,1) = '<13>' Or Sub(sdm2:EMailMessageText,x#,1) = '<10>'
            Cycle
        End ! If Sub(sdm:SMSMessageText,x#,1) = '<13' Or Sub(sdm:SMSMessageText,x#,1) = '<10>'

        If Sub(sdm2:EmailMessageText,x#,2) = '[*'
            local:Start = x#
            local:End = 0
        End ! If Sub(sdm:SMSMessageText,x#,2) = '[*'
        If Sub(sdm2:EmailMessageText,x#,2) = '*]'
            local:End = x#
        End ! If Sub(sdm:SMSMessageText,x#,2) = '*]'

        If local:Start = 0
            local:Length += 1
        Else ! If local:Start = 0
            If local:End > 0
                Access:MERGE.ClearKey(mer:FieldNameKey)
                mer:FieldName = Sub(sdm2:EmailMessageText,local:Start + 2,local:End - local:Start - 2)
                If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Found
                    local:Length += (mer:TotalFieldLength - 1)
                    local:Start = 0
                    local:End = 0
                Else ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Error
                End ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
            End ! If local:End > 0
        End ! If local:Start = 0
    End ! Loop x# = 1 To 500

    ?Prompt:EmailTextLength{prop:Text} = local:Length & '/4000'
    If local:Length > 4000
        ?Prompt:EmailTextLength{prop:fontColor} = color:Red
    Else ! If local:Length > 480
        ?Prompt:EmailTextLength{prop:fontColor} = color:None
    End ! If local:Length > 480


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Alert Reminder'
  OF ChangeRecord
    ActionMessage = 'Changing An Alert Reminde'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSIDAlertReminder')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?sdm:Status:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Clear(CutOffTimeList)
  Free(CutOffTimeList)
  
  cutoff:CutOffTime = Deformat('08:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('09:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('10:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('11:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('12:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('13:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('14:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('15:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('16:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('17:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('18:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('19:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('20:00',@t1)
  Add(CutOffTimeList)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sdm:Record,History::sdm:Record)
  SELF.AddHistoryField(?sdm:Description,3)
  SELF.AddHistoryField(?sdm:ElapsedDays,4)
  SELF.AddHistoryField(?sdm:SendTo,5)
  SELF.AddHistoryField(?sdm:SMSAlertActive,6)
  SELF.AddHistoryField(?sdm:BankHolidays,7)
  SELF.AddHistoryField(?sdm:SMSMessageText,8)
  SELF.AddHistoryField(?sdm:EmailAlertActive,9)
  SELF.AddHistoryField(?sdm:EmailSubject,10)
  SELF.AddUpdateFile(Access:SIDREMIN)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MERGE.Open
  Relate:SIDREMIN.Open
  Relate:SIDREMIN_ALIAS.Open
  Relate:STATUS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SIDREMIN
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If ThisWindow.Request = InsertRecord
      If Access:SIDREMI2.PrimeRecord() = Level:Benign
          sdm2:SIDREMINRecordNumber = sdm:RecordNumber
          If Access:SIDREMI2.TryInsert() = Level:Benign
              !Insert
          Else ! If Access:SIDREMI2.TryInsert() = Level:Benign
              Access:SIDREMI2.CancelAutoInc()
          End ! If Access:SIDREMI2.TryInsert() = Level:Benign
      End ! If Access.SIDREMI2.PrimeRecord() = Level:Benign
  Else ! If ThisWindow.Request = InsertRecord
      Access:SIDREMI2.ClearKey(sdm2:SIDREMINRecordNumberKey)
      sdm2:SIDREMINRecordNumber = sdm:RecordNumber
      If Access:SIDREMI2.TryFetch(sdm2:SIDREMINRecordNumberKey) = Level:Benign
          !Found
      Else ! If Access:SIDREMI2.TryFetch(sdm2:SIDALERTRecordNumberKey) = Level:Benign
          !Error
          If Access:SIDREMI2.PrimeRecord() = Level:Benign
              sdm2:SIDREMINRecordNumber = sdm:RecordNumber
              If Access:SIDREMI2.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:SIDREMI2.TryInsert() = Level:Benign
                  Access:SIDREMI2.CancelAutoInc()
              End ! If Access:SIDREMI2.TryInsert() = Level:Benign
          End ! If Access.SIDREMI2.PrimeRecord() = Level:Benign
      End ! If Access:SIDREMI2.TryFetch(sdm2:SIDALERTRecordNumberKey) = Level:Benign
  End ! If ThisWindow.Request = InsertRecord
  
  Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
  sts:Ref_Number = sdl:Status
  If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
      !Found
  Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
      !Error
  End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
  OPEN(QuickWindow)
  SELF.Opened=True
  Do GetTextLength
  If FullAccess(glo:PassAccount,glo:Password)
      If sdl:SystemAlert
          sdm:SendTo = sdl:SendTo
          ?sdm:SendTo{prop:Disable} = 1
          ?sdm:BankHolidays{prop:Disable} = 1
      End ! If sdl:SystemAlert
  End ! ~If FullAccess(glo:Password)
  
  Do DuplicateAlertCheck
  Do RecolourWindow
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?sdm:SMSAlertActive{Prop:Checked} = True
    ENABLE(?Sheet2)
  END
  IF ?sdm:SMSAlertActive{Prop:Checked} = False
    DISABLE(?Sheet2)
  END
  IF ?sdm:EmailAlertActive{Prop:Checked} = True
    ENABLE(?Sheet3)
  END
  IF ?sdm:EmailAlertActive{Prop:Checked} = False
    DISABLE(?Sheet3)
  END
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MERGE.Close
    Relate:SIDREMIN.Close
    Relate:SIDREMIN_ALIAS.Close
    Relate:STATUS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  sdm2:SIDREMINRecordNumber = sdm:RecordNumber        ! Assign linking field value
  Access:SIDREMI2.Fetch(sdm2:SIDREMINRecordNumberKey)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button:AddMergeField
      Access:SIDREMI2.Update()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sdm:ElapsedDays
      Do DuplicateAlertCheck
    OF ?sdm:SendTo
      Do DuplicateAlertCheck
    OF ?sdm:SMSAlertActive
      IF ?sdm:SMSAlertActive{Prop:Checked} = True
        ENABLE(?Sheet2)
      END
      IF ?sdm:SMSAlertActive{Prop:Checked} = False
        DISABLE(?Sheet2)
      END
      ThisWindow.Reset
    OF ?sdm:SMSMessageText
      Do GetTextLength
    OF ?Button:AddMergeField:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseMerge('SMS')
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              x# = ?sdm:SMSMessageText{prop:SelStart}
              sdm:SMSMessageText = Sub(sdm:SMSMessageText,1,x# - 1) & ' [*' & Clip(mer:FieldName) & '*] ' & Sub(sdm:SMSMessageText,x#,500)
              Do GetTextLength
              Display()
          Of Requestcancelled
      End!Case Globalreponse
    OF ?sdm:EmailAlertActive
      IF ?sdm:EmailAlertActive{Prop:Checked} = True
        ENABLE(?Sheet3)
      END
      IF ?sdm:EmailAlertActive{Prop:Checked} = False
        DISABLE(?Sheet3)
      END
      ThisWindow.Reset
    OF ?sdm2:EmailMessageText
      Do GetTextLength
    OF ?Button:AddMergeField
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseMerge('EMAIL')
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              x# = ?sdm2:EmailMessageText{prop:SelStart}
              sdm2:EmailMessageText = Sub(sdm2:EmailMessageText,1,x# - 1) & ' [*' & Clip(mer:FieldName) & '*] ' & Sub(sdm2:EmailMessageText,x#,500)
              Do GetTextLength
              Display()
          Of Requestcancelled
      End!Case Globalreponse
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If sdm:SendTo = 0
      Beep(Beep:SystemHand)  ;  Yield()
      Case Message('You have not selected who to send the message to.','ServiceBase',|
                     icon:Hand,'&OK',1,1)
          Of 1 ! &OK Button
      End!Case Message
      Cycle
  End ! If sld:SentTo
  If sdm:SMSAlertActive
      If sdm:BankHolidays = 0
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('You have not completed the "Bank Holiday" options.','ServiceBase',|
                         icon:Hand,'&OK',1,1)
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      End ! If sld:BankHolidays = 0
  End ! If sld:SMSAlertActive
  If sdm:ElapsedDays = 0
      sdm:ElapsedDays = ''
      Select(?sdm:ElapsedDays)
      Cycle
  End ! If sdm:ElapsedDays = 0
  Access:SIDREMI2.TryUpdate()
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

