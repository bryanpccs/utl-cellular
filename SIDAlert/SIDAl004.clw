

   MEMBER('sidalertbrowse.clw')                       ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SIDAL004.INC'),ONCE        !Local module procedure declarations
                     END


SIDAlertDefaults PROCEDURE                            !Generated from procedure template - Window

ActionMessage        CSTRING(40)
window               WINDOW('SID Alert Defaults'),AT(,,259,206),FONT('Tahoma',,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,252,180),USE(?Sheet1),SPREAD
                         TAB('Defaults'),USE(?Tab1)
                           PROMPT('SMTP Server'),AT(8,20),USE(?sdd:SMTPServer:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           TEXT,AT(84,20,164,10),USE(sdd:SMTPServer),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Server'),TIP('SMTP Server'),SINGLE
                           PROMPT('SMTP Port Number'),AT(8,36),USE(?sdd:SMTPPortNumber:Prompt),TRN,FONT('Tahoma',,,,CHARSET:ANSI)
                           TEXT,AT(84,36,64,10),USE(sdd:SMTPPortNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Port Number'),TIP('SMTP Port Number'),SINGLE
                           PROMPT('SMTP Username'),AT(8,52),USE(?sdd:SMTPUserName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           TEXT,AT(84,52,164,10),USE(sdd:SMTPUserName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Username'),TIP('SMTP Username'),SINGLE
                           PROMPT('SMTP Password'),AT(8,68),USE(?sdd:SMTPPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s100),AT(84,68,164,10),USE(sdd:SMTPPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Password'),TIP('SMTP Password'),UPR,PASSWORD
                           PROMPT('Email From Address'),AT(8,82),USE(?sdd:EmailFromAddress:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           TEXT,AT(84,82,164,10),USE(sdd:EmailFromAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email From Address'),TIP('Email From Address'),SINGLE
                           PROMPT('Reply To Address'),AT(8,94),USE(?sdd:ReplyToAddress:Prompt),TRN
                           TEXT,AT(84,94,164,10),USE(sdd:ReplyToAddress),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Reply To Address'),TIP('Reply To Address'),SINGLE
                           PROMPT('UTL Email Address'),AT(8,108),USE(?sdd:UTLEmailAddress:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           TEXT,AT(84,108,164,10),USE(sdd:UTLEmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address'),SINGLE
                           PROMPT('SMS FTP Server'),AT(8,128),USE(?sdd:UTLEmailAddress:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           TEXT,AT(84,128,164,10),USE(sdd:SMSURL),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address'),SINGLE
                           TEXT,AT(84,142,164,10),USE(sdd:SMSUsername),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address'),SINGLE
                           PROMPT('SMS Password'),AT(8,156),USE(?sdd:SMSPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s100),AT(84,156,164,10),USE(sdd:SMSPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMS Password'),TIP('SMS Password'),PASSWORD
                           PROMPT('SMS FTP Path'),AT(9,170),USE(?sdd:SMSFTPPath:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s100),AT(84,170,164,10),USE(sdd:SMSFTPPath),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMS FTP Path'),TIP('SMS FTP Path')
                           PROMPT('SMS Username'),AT(8,142),USE(?sdd:SMSUsername:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           BUTTON('OK'),AT(140,188,56,16),USE(?OK),LEFT,ICON('ok.ico'),DEFAULT,REQ
                           BUTTON('Cancel'),AT(200,188,56,16),USE(?Cancel),LEFT,ICON('cancel.ico')
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?sdd:SMTPServer:Prompt{prop:FontColor} = -1
    ?sdd:SMTPServer:Prompt{prop:Color} = 15066597
    If ?sdd:SMTPServer{prop:ReadOnly} = True
        ?sdd:SMTPServer{prop:FontColor} = 65793
        ?sdd:SMTPServer{prop:Color} = 15066597
    Elsif ?sdd:SMTPServer{prop:Req} = True
        ?sdd:SMTPServer{prop:FontColor} = 65793
        ?sdd:SMTPServer{prop:Color} = 8454143
    Else ! If ?sdd:SMTPServer{prop:Req} = True
        ?sdd:SMTPServer{prop:FontColor} = 65793
        ?sdd:SMTPServer{prop:Color} = 16777215
    End ! If ?sdd:SMTPServer{prop:Req} = True
    ?sdd:SMTPServer{prop:Trn} = 0
    ?sdd:SMTPServer{prop:FontStyle} = font:Bold
    ?sdd:SMTPPortNumber:Prompt{prop:FontColor} = -1
    ?sdd:SMTPPortNumber:Prompt{prop:Color} = 15066597
    If ?sdd:SMTPPortNumber{prop:ReadOnly} = True
        ?sdd:SMTPPortNumber{prop:FontColor} = 65793
        ?sdd:SMTPPortNumber{prop:Color} = 15066597
    Elsif ?sdd:SMTPPortNumber{prop:Req} = True
        ?sdd:SMTPPortNumber{prop:FontColor} = 65793
        ?sdd:SMTPPortNumber{prop:Color} = 8454143
    Else ! If ?sdd:SMTPPortNumber{prop:Req} = True
        ?sdd:SMTPPortNumber{prop:FontColor} = 65793
        ?sdd:SMTPPortNumber{prop:Color} = 16777215
    End ! If ?sdd:SMTPPortNumber{prop:Req} = True
    ?sdd:SMTPPortNumber{prop:Trn} = 0
    ?sdd:SMTPPortNumber{prop:FontStyle} = font:Bold
    ?sdd:SMTPUserName:Prompt{prop:FontColor} = -1
    ?sdd:SMTPUserName:Prompt{prop:Color} = 15066597
    If ?sdd:SMTPUserName{prop:ReadOnly} = True
        ?sdd:SMTPUserName{prop:FontColor} = 65793
        ?sdd:SMTPUserName{prop:Color} = 15066597
    Elsif ?sdd:SMTPUserName{prop:Req} = True
        ?sdd:SMTPUserName{prop:FontColor} = 65793
        ?sdd:SMTPUserName{prop:Color} = 8454143
    Else ! If ?sdd:SMTPUserName{prop:Req} = True
        ?sdd:SMTPUserName{prop:FontColor} = 65793
        ?sdd:SMTPUserName{prop:Color} = 16777215
    End ! If ?sdd:SMTPUserName{prop:Req} = True
    ?sdd:SMTPUserName{prop:Trn} = 0
    ?sdd:SMTPUserName{prop:FontStyle} = font:Bold
    ?sdd:SMTPPassword:Prompt{prop:FontColor} = -1
    ?sdd:SMTPPassword:Prompt{prop:Color} = 15066597
    If ?sdd:SMTPPassword{prop:ReadOnly} = True
        ?sdd:SMTPPassword{prop:FontColor} = 65793
        ?sdd:SMTPPassword{prop:Color} = 15066597
    Elsif ?sdd:SMTPPassword{prop:Req} = True
        ?sdd:SMTPPassword{prop:FontColor} = 65793
        ?sdd:SMTPPassword{prop:Color} = 8454143
    Else ! If ?sdd:SMTPPassword{prop:Req} = True
        ?sdd:SMTPPassword{prop:FontColor} = 65793
        ?sdd:SMTPPassword{prop:Color} = 16777215
    End ! If ?sdd:SMTPPassword{prop:Req} = True
    ?sdd:SMTPPassword{prop:Trn} = 0
    ?sdd:SMTPPassword{prop:FontStyle} = font:Bold
    ?sdd:EmailFromAddress:Prompt{prop:FontColor} = -1
    ?sdd:EmailFromAddress:Prompt{prop:Color} = 15066597
    If ?sdd:EmailFromAddress{prop:ReadOnly} = True
        ?sdd:EmailFromAddress{prop:FontColor} = 65793
        ?sdd:EmailFromAddress{prop:Color} = 15066597
    Elsif ?sdd:EmailFromAddress{prop:Req} = True
        ?sdd:EmailFromAddress{prop:FontColor} = 65793
        ?sdd:EmailFromAddress{prop:Color} = 8454143
    Else ! If ?sdd:EmailFromAddress{prop:Req} = True
        ?sdd:EmailFromAddress{prop:FontColor} = 65793
        ?sdd:EmailFromAddress{prop:Color} = 16777215
    End ! If ?sdd:EmailFromAddress{prop:Req} = True
    ?sdd:EmailFromAddress{prop:Trn} = 0
    ?sdd:EmailFromAddress{prop:FontStyle} = font:Bold
    ?sdd:ReplyToAddress:Prompt{prop:FontColor} = -1
    ?sdd:ReplyToAddress:Prompt{prop:Color} = 15066597
    If ?sdd:ReplyToAddress{prop:ReadOnly} = True
        ?sdd:ReplyToAddress{prop:FontColor} = 65793
        ?sdd:ReplyToAddress{prop:Color} = 15066597
    Elsif ?sdd:ReplyToAddress{prop:Req} = True
        ?sdd:ReplyToAddress{prop:FontColor} = 65793
        ?sdd:ReplyToAddress{prop:Color} = 8454143
    Else ! If ?sdd:ReplyToAddress{prop:Req} = True
        ?sdd:ReplyToAddress{prop:FontColor} = 65793
        ?sdd:ReplyToAddress{prop:Color} = 16777215
    End ! If ?sdd:ReplyToAddress{prop:Req} = True
    ?sdd:ReplyToAddress{prop:Trn} = 0
    ?sdd:ReplyToAddress{prop:FontStyle} = font:Bold
    ?sdd:UTLEmailAddress:Prompt{prop:FontColor} = -1
    ?sdd:UTLEmailAddress:Prompt{prop:Color} = 15066597
    If ?sdd:UTLEmailAddress{prop:ReadOnly} = True
        ?sdd:UTLEmailAddress{prop:FontColor} = 65793
        ?sdd:UTLEmailAddress{prop:Color} = 15066597
    Elsif ?sdd:UTLEmailAddress{prop:Req} = True
        ?sdd:UTLEmailAddress{prop:FontColor} = 65793
        ?sdd:UTLEmailAddress{prop:Color} = 8454143
    Else ! If ?sdd:UTLEmailAddress{prop:Req} = True
        ?sdd:UTLEmailAddress{prop:FontColor} = 65793
        ?sdd:UTLEmailAddress{prop:Color} = 16777215
    End ! If ?sdd:UTLEmailAddress{prop:Req} = True
    ?sdd:UTLEmailAddress{prop:Trn} = 0
    ?sdd:UTLEmailAddress{prop:FontStyle} = font:Bold
    ?sdd:UTLEmailAddress:Prompt:2{prop:FontColor} = -1
    ?sdd:UTLEmailAddress:Prompt:2{prop:Color} = 15066597
    If ?sdd:SMSURL{prop:ReadOnly} = True
        ?sdd:SMSURL{prop:FontColor} = 65793
        ?sdd:SMSURL{prop:Color} = 15066597
    Elsif ?sdd:SMSURL{prop:Req} = True
        ?sdd:SMSURL{prop:FontColor} = 65793
        ?sdd:SMSURL{prop:Color} = 8454143
    Else ! If ?sdd:SMSURL{prop:Req} = True
        ?sdd:SMSURL{prop:FontColor} = 65793
        ?sdd:SMSURL{prop:Color} = 16777215
    End ! If ?sdd:SMSURL{prop:Req} = True
    ?sdd:SMSURL{prop:Trn} = 0
    ?sdd:SMSURL{prop:FontStyle} = font:Bold
    If ?sdd:SMSUsername{prop:ReadOnly} = True
        ?sdd:SMSUsername{prop:FontColor} = 65793
        ?sdd:SMSUsername{prop:Color} = 15066597
    Elsif ?sdd:SMSUsername{prop:Req} = True
        ?sdd:SMSUsername{prop:FontColor} = 65793
        ?sdd:SMSUsername{prop:Color} = 8454143
    Else ! If ?sdd:SMSUsername{prop:Req} = True
        ?sdd:SMSUsername{prop:FontColor} = 65793
        ?sdd:SMSUsername{prop:Color} = 16777215
    End ! If ?sdd:SMSUsername{prop:Req} = True
    ?sdd:SMSUsername{prop:Trn} = 0
    ?sdd:SMSUsername{prop:FontStyle} = font:Bold
    ?sdd:SMSPassword:Prompt{prop:FontColor} = -1
    ?sdd:SMSPassword:Prompt{prop:Color} = 15066597
    If ?sdd:SMSPassword{prop:ReadOnly} = True
        ?sdd:SMSPassword{prop:FontColor} = 65793
        ?sdd:SMSPassword{prop:Color} = 15066597
    Elsif ?sdd:SMSPassword{prop:Req} = True
        ?sdd:SMSPassword{prop:FontColor} = 65793
        ?sdd:SMSPassword{prop:Color} = 8454143
    Else ! If ?sdd:SMSPassword{prop:Req} = True
        ?sdd:SMSPassword{prop:FontColor} = 65793
        ?sdd:SMSPassword{prop:Color} = 16777215
    End ! If ?sdd:SMSPassword{prop:Req} = True
    ?sdd:SMSPassword{prop:Trn} = 0
    ?sdd:SMSPassword{prop:FontStyle} = font:Bold
    ?sdd:SMSFTPPath:Prompt{prop:FontColor} = -1
    ?sdd:SMSFTPPath:Prompt{prop:Color} = 15066597
    If ?sdd:SMSFTPPath{prop:ReadOnly} = True
        ?sdd:SMSFTPPath{prop:FontColor} = 65793
        ?sdd:SMSFTPPath{prop:Color} = 15066597
    Elsif ?sdd:SMSFTPPath{prop:Req} = True
        ?sdd:SMSFTPPath{prop:FontColor} = 65793
        ?sdd:SMSFTPPath{prop:Color} = 8454143
    Else ! If ?sdd:SMSFTPPath{prop:Req} = True
        ?sdd:SMSFTPPath{prop:FontColor} = 65793
        ?sdd:SMSFTPPath{prop:Color} = 16777215
    End ! If ?sdd:SMSFTPPath{prop:Req} = True
    ?sdd:SMSFTPPath{prop:Trn} = 0
    ?sdd:SMSFTPPath{prop:FontStyle} = font:Bold
    ?sdd:SMSUsername:Prompt{prop:FontColor} = -1
    ?sdd:SMSUsername:Prompt{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:SIDALDEF.Open
    SET(SIDALDEF)
    CASE Access:SIDALDEF.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:SIDALDEF.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('SIDAlertDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?sdd:SMTPServer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:SIDALDEF)
  Relate:SIDALDEF.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SIDALDEF
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(SIDALDEF)
      Access:SIDALDEF.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SIDALDEF.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

