

   MEMBER('sidalertbrowse.clw')                       ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SIDAL007.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SIDAL008.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateSIDAlertModelException PROCEDURE                !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
CutOffTimeList       QUEUE,PRE(cutoff)
CutOffTime           TIME
                     END
tmp:SMSActive        STRING(3)
tmp:EmailActive      STRING(3)
tmp:SendTo           STRING(8)
BRW3::View:Browse    VIEW(SIDREMIN)
                       PROJECT(sdm:Description)
                       PROJECT(sdm:ElapsedDays)
                       PROJECT(sdm:RecordNumber)
                       PROJECT(sdm:SIDALERTRecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
sdm:Description        LIKE(sdm:Description)          !List box control field - type derived from field
sdm:ElapsedDays        LIKE(sdm:ElapsedDays)          !List box control field - type derived from field
tmp:SMSActive          LIKE(tmp:SMSActive)            !List box control field - type derived from local data
tmp:EmailActive        LIKE(tmp:EmailActive)          !List box control field - type derived from local data
tmp:SendTo             LIKE(tmp:SendTo)               !List box control field - type derived from local data
sdm:RecordNumber       LIKE(sdm:RecordNumber)         !Browse hot field - type derived from field
sdm:SIDALERTRecordNumber LIKE(sdm:SIDALERTRecordNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB9::View:FileDrop  VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?sml:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB10::View:FileDrop VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
Queue:FileDrop:1     QUEUE                            !Queue declaration for browse/combo box using ?sml:ModelNumber
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::sml:Record  LIKE(sml:RECORD),STATIC
QuickWindow          WINDOW('Update the SIDALERT File'),AT(,,548,416),FONT('Tahoma',8,,),CENTER,IMM,HLP('UpdateSIDAlert'),SYSTEM,GRAY,DOUBLE
                       CHECK('SMS Alert Active'),AT(8,40),USE(sml:SMSAlertActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                       PROMPT('Manufacturer'),AT(8,12),USE(?sml:Manufacturer:Prompt)
                       LIST,AT(80,12,124,10),USE(sml:Manufacturer),LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDrop)
                       PROMPT('Model Number'),AT(8,26),USE(?sml:ModelNumber:Prompt)
                       LIST,AT(80,26,124,10),USE(sml:ModelNumber),VSCROLL,FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDrop:1)
                       CHECK('Email Alert Active'),AT(8,148),USE(sml:EmailAlertActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                       SHEET,AT(4,5,540,136),USE(?Sheet2),WIZARD,SPREAD
                         TAB('SMS Notification'),USE(?Tab2)
                           PROMPT('Message Text'),AT(8,54),USE(?sml:SMSMessageText:Prompt)
                           TEXT,AT(80,54,456,68),USE(sml:SMSMessageText),VSCROLL,LEFT,MSG('SMS Message Text'),TIP('SMS Message Text'),REQ
                           BUTTON('Add Merge Field'),AT(8,108,68,16),USE(?Button:AddMergeField)
                           PROMPT('Text Length (including merge fields): '),AT(80,128),USE(?Prompt6)
                           PROMPT('0'),AT(200,128),USE(?Prompt:SMSTextLength)
                         END
                       END
                       SHEET,AT(4,144,540,132),USE(?Sheet3),WIZARD,SPREAD
                         TAB('Email Notification'),USE(?Tab3)
                           PROMPT('Message Subject'),AT(8,162),USE(?sml:EmailMessagSubject)
                           TEXT,AT(80,162,252,10),USE(sml:EmailMessageSubject),FONT(,,,,CHARSET:ANSI),REQ,SINGLE,MSG('Email Message Subject')
                           PROMPT('Message Text'),AT(8,176),USE(?sml2:EmailMessageText:Prompt)
                           TEXT,AT(80,176,456,80),USE(sml2:EmailMessageText),VSCROLL,LEFT,MSG('Email Message Text'),TIP('Email Message Text'),REQ
                           BUTTON('Add Merge Field'),AT(8,242,68,16),USE(?Button:AddMergeField:2)
                           PROMPT('Text Length (including merge fields): '),AT(80,262),USE(?Prompt6:2)
                           PROMPT('0'),AT(200,262),USE(?Prompt:EmailTextLength)
                         END
                       END
                       SHEET,AT(4,282,540,109),USE(?Sheet4),SPREAD
                         TAB('Reminder Notifications'),USE(?Tab4)
                           LIST,AT(8,295,472,90),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('240L(2)|M~Description~@s60@60L(2)|M~Elapsed Days~@s8@50L(2)|M~SMS Active~@s3@48L' &|
   '(2)|M~Email Active~@s3@37L(2)|M~Send To~@s8@'),FROM(Queue:Browse)
                           BUTTON('Change'),AT(484,370,56,16),USE(?Button:Change),LEFT,ICON('edit.ico')
                         END
                       END
                       BUTTON('OK'),AT(428,396,56,16),USE(?OK),LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('Cancel'),AT(488,396,56,16),USE(?Cancel),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
FDB9                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB10                CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?sml:SMSAlertActive{prop:Font,3} = -1
    ?sml:SMSAlertActive{prop:Color} = 15066597
    ?sml:SMSAlertActive{prop:Trn} = 0
    ?sml:Manufacturer:Prompt{prop:FontColor} = -1
    ?sml:Manufacturer:Prompt{prop:Color} = 15066597
    ?sml:Manufacturer{prop:FontColor} = 65793
    ?sml:Manufacturer{prop:Color}= 16777215
    ?sml:Manufacturer{prop:Color,2} = 16777215
    ?sml:Manufacturer{prop:Color,3} = 12937777
    ?sml:ModelNumber:Prompt{prop:FontColor} = -1
    ?sml:ModelNumber:Prompt{prop:Color} = 15066597
    ?sml:ModelNumber{prop:FontColor} = 65793
    ?sml:ModelNumber{prop:Color}= 16777215
    ?sml:ModelNumber{prop:Color,2} = 16777215
    ?sml:ModelNumber{prop:Color,3} = 12937777
    ?sml:EmailAlertActive{prop:Font,3} = -1
    ?sml:EmailAlertActive{prop:Color} = 15066597
    ?sml:EmailAlertActive{prop:Trn} = 0
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?sml:SMSMessageText:Prompt{prop:FontColor} = -1
    ?sml:SMSMessageText:Prompt{prop:Color} = 15066597
    If ?sml:SMSMessageText{prop:ReadOnly} = True
        ?sml:SMSMessageText{prop:FontColor} = 65793
        ?sml:SMSMessageText{prop:Color} = 15066597
    Elsif ?sml:SMSMessageText{prop:Req} = True
        ?sml:SMSMessageText{prop:FontColor} = 65793
        ?sml:SMSMessageText{prop:Color} = 8454143
    Else ! If ?sml:SMSMessageText{prop:Req} = True
        ?sml:SMSMessageText{prop:FontColor} = 65793
        ?sml:SMSMessageText{prop:Color} = 16777215
    End ! If ?sml:SMSMessageText{prop:Req} = True
    ?sml:SMSMessageText{prop:Trn} = 0
    ?sml:SMSMessageText{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Prompt:SMSTextLength{prop:FontColor} = -1
    ?Prompt:SMSTextLength{prop:Color} = 15066597
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?sml:EmailMessagSubject{prop:FontColor} = -1
    ?sml:EmailMessagSubject{prop:Color} = 15066597
    If ?sml:EmailMessageSubject{prop:ReadOnly} = True
        ?sml:EmailMessageSubject{prop:FontColor} = 65793
        ?sml:EmailMessageSubject{prop:Color} = 15066597
    Elsif ?sml:EmailMessageSubject{prop:Req} = True
        ?sml:EmailMessageSubject{prop:FontColor} = 65793
        ?sml:EmailMessageSubject{prop:Color} = 8454143
    Else ! If ?sml:EmailMessageSubject{prop:Req} = True
        ?sml:EmailMessageSubject{prop:FontColor} = 65793
        ?sml:EmailMessageSubject{prop:Color} = 16777215
    End ! If ?sml:EmailMessageSubject{prop:Req} = True
    ?sml:EmailMessageSubject{prop:Trn} = 0
    ?sml:EmailMessageSubject{prop:FontStyle} = font:Bold
    ?sml2:EmailMessageText:Prompt{prop:FontColor} = -1
    ?sml2:EmailMessageText:Prompt{prop:Color} = 15066597
    If ?sml2:EmailMessageText{prop:ReadOnly} = True
        ?sml2:EmailMessageText{prop:FontColor} = 65793
        ?sml2:EmailMessageText{prop:Color} = 15066597
    Elsif ?sml2:EmailMessageText{prop:Req} = True
        ?sml2:EmailMessageText{prop:FontColor} = 65793
        ?sml2:EmailMessageText{prop:Color} = 8454143
    Else ! If ?sml2:EmailMessageText{prop:Req} = True
        ?sml2:EmailMessageText{prop:FontColor} = 65793
        ?sml2:EmailMessageText{prop:Color} = 16777215
    End ! If ?sml2:EmailMessageText{prop:Req} = True
    ?sml2:EmailMessageText{prop:Trn} = 0
    ?sml2:EmailMessageText{prop:FontStyle} = font:Bold
    ?Prompt6:2{prop:FontColor} = -1
    ?Prompt6:2{prop:Color} = 15066597
    ?Prompt:EmailTextLength{prop:FontColor} = -1
    ?Prompt:EmailTextLength{prop:Color} = 15066597
    ?Sheet4{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
GetTextLength            Routine
Data
local:Length            Long()
local:Start             Long()
local:End               Long()
Code
    local:Start = 0
    local:End = 0
    local:Length = 0
    Loop x# = 1 To Len(Clip(sml:SMSMessageText))
        If Sub(sml:SMSMessageText,x#,1) = '<13>' Or Sub(sml:SMSMessageText,x#,1) = '<10>'
            Cycle
        End ! If Sub(sml:SMSMessageText,x#,1) = '<13' Or Sub(sml:SMSMessageText,x#,1) = '<10>'

        If Sub(sml:SMSMessageText,x#,2) = '[*'
            local:Start = x#
            local:End = 0
        End ! If Sub(sml:SMSMessageText,x#,2) = '[*'
        If Sub(sml:SMSMessageText,x#,2) = '*]'
            local:End = x#
        End ! If Sub(sml:SMSMessageText,x#,2) = '*]'

        If local:Start = 0
            local:Length += 1
        Else ! If local:Start = 0
            If local:End > 0
                Access:MERGE.ClearKey(mer:FieldNameKey)
                mer:FieldName = Sub(sml:SMSMessageText,local:Start + 2,local:End - local:Start - 2)
                If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Found
                    local:Length += (mer:TotalFieldLength - 1)
                    local:Start = 0
                    local:End = 0
                Else ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Error
                End ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
            End ! If local:End > 0
        End ! If local:Start = 0
    End ! Loop x# = 1 To 500

    ?Prompt:SMSTextLength{prop:Text} = local:Length & '/480'
    If local:Length > 480
        ?Prompt:SMSTextLength{prop:fontColor} = color:Red
    Else ! If local:Length > 480
        ?Prompt:SMSTextLength{prop:fontColor} = color:None
    End ! If local:Length > 480

    local:Start = 0
    local:End = 0
    local:Length = 0
    Loop x# = 1 To Len(Clip(sml2:EmailMessageText))
        If Sub(sml2:EmailMessageText,x#,1) = '<13>' Or Sub(sml2:EMailMessageText,x#,1) = '<10>'
            Cycle
        End ! If Sub(sml:SMSMessageText,x#,1) = '<13' Or Sub(sml:SMSMessageText,x#,1) = '<10>'

        If Sub(sml2:EmailMessageText,x#,2) = '[*'
            local:Start = x#
            local:End = 0
        End ! If Sub(sml:SMSMessageText,x#,2) = '[*'
        If Sub(sml2:EmailMessageText,x#,2) = '*]'
            local:End = x#
        End ! If Sub(sml:SMSMessageText,x#,2) = '*]'

        If local:Start = 0
            local:Length += 1
        Else ! If local:Start = 0
            If local:End > 0
                Access:MERGE.ClearKey(mer:FieldNameKey)
                mer:FieldName = Sub(sml2:EmailMessageText,local:Start + 2,local:End - local:Start - 2)
                If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Found
                    local:Length += (mer:TotalFieldLength - 1)
                    local:Start = 0
                    local:End = 0
                Else ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Error
                End ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
            End ! If local:End > 0
        End ! If local:Start = 0
    End ! Loop x# = 1 To 500

    ?Prompt:EmailTextLength{prop:Text} = local:Length & '/4000'
    If local:Length > 4000
        ?Prompt:EmailTextLength{prop:fontColor} = color:Red
    Else ! If local:Length > 480
        ?Prompt:EmailTextLength{prop:fontColor} = color:None
    End ! If local:Length > 480

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Alert'
  OF ChangeRecord
    ActionMessage = 'Changing An Alert'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSIDAlertModelException')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?sml:SMSAlertActive
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sml:Record,History::sml:Record)
  SELF.AddHistoryField(?sml:SMSAlertActive,5)
  SELF.AddHistoryField(?sml:Manufacturer,3)
  SELF.AddHistoryField(?sml:ModelNumber,4)
  SELF.AddHistoryField(?sml:EmailAlertActive,7)
  SELF.AddHistoryField(?sml:SMSMessageText,6)
  SELF.AddHistoryField(?sml:EmailMessageSubject,8)
  SELF.AddUpdateFile(Access:SMOALERT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANUFACT.Open
  Relate:MERGE.Open
  Relate:SIDREMIN.Open
  Relate:STATUS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SMOALERT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If ThisWindow.Request = InsertRecord
      If Access:SMOALER2.PrimeRecord() = Level:Benign
          sml2:SMOALERTRecordNumber = sml:RecordNumber
          If Access:SMOALER2.TryInsert() = Level:Benign
              !Insert
          Else ! If Access:SMOALER2.TryInsert() = Level:Benign
              Access:SMOALER2.CancelAutoInc()
          End ! If Access:SMOALER2.TryInsert() = Level:Benign
      End ! If Access.SMOALER2.PrimeRecord() = Level:Benign
  Else ! If ThisWindow.Request = InsertRecord
      Access:SMOALER2.ClearKey(sml2:SMOALERTRecordNumberKey)
      sml2:SMOALERTRecordNumber = sml:RecordNumber
      If Access:SMOALER2.TryFetch(sml2:SMOALERTRecordNumberKey) = Level:Benign
          !Found
      Else ! If Access:SMOALER2.TryFetch(sml2:SMOALERTRecordNumberKey) = Level:Benign
          !Error
          If Access:SMOALER2.PrimeRecord() = Level:Benign
              sml2:SMOALERTRecordNumber = sml:RecordNumber
              If Access:SMOALER2.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:SMOALER2.TryInsert() = Level:Benign
                  Access:SMOALER2.CancelAutoInc()
              End ! If Access:SMOALER2.TryInsert() = Level:Benign
          End ! If Access.SMOALER2.PrimeRecord() = Level:Benign
      End ! If Access:SMOALER2.TryFetch(sml2:SMOALERTRecordNumberKey) = Level:Benign
  End ! If ThisWindow.Request = InsertRecord
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:SIDREMIN,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do GetTextLength
  Do RecolourWindow
  ?sml:Manufacturer{prop:vcr} = TRUE
  ?sml:ModelNumber{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW3.Q &= Queue:Browse
  BRW3.AddSortOrder(,sdm:ElapsedKey)
  BRW3.AddRange(sdm:SIDALERTRecordNumber,sml:SIDALERTRecordNumber)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,sdm:ElapsedDays,1,BRW3)
  BIND('tmp:SMSActive',tmp:SMSActive)
  BIND('tmp:EmailActive',tmp:EmailActive)
  BIND('tmp:SendTo',tmp:SendTo)
  BRW3.AddField(sdm:Description,BRW3.Q.sdm:Description)
  BRW3.AddField(sdm:ElapsedDays,BRW3.Q.sdm:ElapsedDays)
  BRW3.AddField(tmp:SMSActive,BRW3.Q.tmp:SMSActive)
  BRW3.AddField(tmp:EmailActive,BRW3.Q.tmp:EmailActive)
  BRW3.AddField(tmp:SendTo,BRW3.Q.tmp:SendTo)
  BRW3.AddField(sdm:RecordNumber,BRW3.Q.sdm:RecordNumber)
  BRW3.AddField(sdm:SIDALERTRecordNumber,BRW3.Q.sdm:SIDALERTRecordNumber)
  IF ?sml:SMSAlertActive{Prop:Checked} = True
    ENABLE(?Sheet2)
  END
  IF ?sml:SMSAlertActive{Prop:Checked} = False
    DISABLE(?Sheet2)
  END
  IF ?sml:EmailAlertActive{Prop:Checked} = True
    ENABLE(?Sheet3)
  END
  IF ?sml:EmailAlertActive{Prop:Checked} = False
    DISABLE(?Sheet3)
  END
  SELF.AddItem(ToolbarForm)
  FDB9.Init(?sml:Manufacturer,Queue:FileDrop.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop,Relate:MANUFACT,ThisWindow)
  FDB9.Q &= Queue:FileDrop
  FDB9.AddSortOrder(man:Manufacturer_Key)
  FDB9.AddField(man:Manufacturer,FDB9.Q.man:Manufacturer)
  FDB9.AddField(man:RecordNumber,FDB9.Q.man:RecordNumber)
  FDB9.AddUpdateField(man:Manufacturer,sml:Manufacturer)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  FDB10.Init(?sml:ModelNumber,Queue:FileDrop:1.ViewPosition,FDB10::View:FileDrop,Queue:FileDrop:1,Relate:MODELNUM,ThisWindow)
  FDB10.Q &= Queue:FileDrop:1
  FDB10.AddSortOrder(mod:Manufacturer_Key)
  FDB10.AddRange(mod:Manufacturer,sml:Manufacturer)
  FDB10.AddField(mod:Model_Number,FDB10.Q.mod:Model_Number)
  FDB10.AddUpdateField(mod:Model_Number,sml:ModelNumber)
  ThisWindow.AddItem(FDB10.WindowComponent)
  FDB10.DefaultFill = 0
  BRW3.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
    Relate:MERGE.Close
    Relate:SIDREMIN.Close
    Relate:STATUS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  sml2:SMOALERTRecordNumber = sml:RecordNumber        ! Assign linking field value
  Access:SMOALER2.Fetch(sml2:SMOALERTRecordNumberKey)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  Access:SMOALER2.TryUpdate()
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button:AddMergeField:2
      Access:SMOALER2.TryUpdate()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sml:SMSAlertActive
      IF ?sml:SMSAlertActive{Prop:Checked} = True
        ENABLE(?Sheet2)
      END
      IF ?sml:SMSAlertActive{Prop:Checked} = False
        DISABLE(?Sheet2)
      END
      ThisWindow.Reset
    OF ?sml:EmailAlertActive
      IF ?sml:EmailAlertActive{Prop:Checked} = True
        ENABLE(?Sheet3)
      END
      IF ?sml:EmailAlertActive{Prop:Checked} = False
        DISABLE(?Sheet3)
      END
      ThisWindow.Reset
    OF ?sml:SMSMessageText
      Do GetTextLength
    OF ?Button:AddMergeField
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseMerge('SMS')
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              x# = ?sml:SMSMessageText{prop:SelStart}
              sml:SMSMessageText = Sub(sml:SMSMessageText,1,x# - 1) & ' [*' & Clip(mer:FieldName) & '*] ' & Sub(sml:SMSMessageText,x#,500)
              Do GetTextLength
              Display()
          Of Requestcancelled
      End!Case Globalreponse
      
    OF ?sml2:EmailMessageText
      Do GetTextLength
    OF ?Button:AddMergeField:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseMerge('EMAIL')
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              x# = ?sml2:EmailMessageText{prop:SelStart}
              sml2:EmailMessageText = Sub(sml2:EmailMessageText,1,x# - 1) & ' [*' & Clip(mer:FieldName) & '*] ' & Sub(sml2:EmailMessageText,x#,500)
              Do GetTextLength
              Display()
          Of Requestcancelled
      End!Case Globalreponse
    OF ?Button:Change
      ThisWindow.Update
      BRW3.UpdateBuffer()
      GlobalRequest = ChangeRecord
      UpdateSIDAlertReminderModelException(sdm:RecordNumber, sml:ModelNumber)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Error Checking
  If ?OK{prop:Disable} = 1
      Cycle
  End ! If ?OK{prop:Disable} = 1
  Access:SMOALER2.TryUpdate()
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW3.SetQueueRecord PROCEDURE

  CODE
  IF (sdm:SMSAlertActive)
    tmp:SMSActive = 'Yes'
  ELSE
    tmp:SMSActive = 'No'
  END
  IF (sdm:EmailAlertActive)
    tmp:EmailActive = 'Yes'
  ELSE
    tmp:EmailActive = 'No'
  END
  CASE (sdm:SendTo)
  OF 1
    tmp:SendTo = 'Customer'
  OF 2
    tmp:SendTo = 'UTL'
  OF 3
    tmp:SendTo = 'Both'
  ELSE
    tmp:SendTo = 'Not Set'
  END
  PARENT.SetQueueRecord
  SELF.Q.tmp:SMSActive = tmp:SMSActive                !Assign formula result to display queue
  SELF.Q.tmp:EmailActive = tmp:EmailActive            !Assign formula result to display queue
  SELF.Q.tmp:SendTo = tmp:SendTo                      !Assign formula result to display queue


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

