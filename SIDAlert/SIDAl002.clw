

   MEMBER('sidalertbrowse.clw')                       ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SIDAL002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SIDAL003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SIDAL006.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateSIDAlert PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
CutOffTimeList       QUEUE,PRE(cutoff)
CutOffTime           TIME
                     END
tmp:SMSActive        STRING(3)
tmp:EmailActive      STRING(3)
tmp:SendTo           STRING(8)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?sdl:Retailer
rtr:Retailer           LIKE(rtr:Retailer)             !List box control field - type derived from field
rtr:RecordNumber       LIKE(rtr:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW3::View:Browse    VIEW(SIDREMIN)
                       PROJECT(sdm:Description)
                       PROJECT(sdm:ElapsedDays)
                       PROJECT(sdm:RecordNumber)
                       PROJECT(sdm:SIDALERTRecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
sdm:Description        LIKE(sdm:Description)          !List box control field - type derived from field
sdm:ElapsedDays        LIKE(sdm:ElapsedDays)          !List box control field - type derived from field
tmp:SMSActive          LIKE(tmp:SMSActive)            !List box control field - type derived from local data
tmp:EmailActive        LIKE(tmp:EmailActive)          !List box control field - type derived from local data
tmp:SendTo             LIKE(tmp:SendTo)               !List box control field - type derived from local data
sdm:RecordNumber       LIKE(sdm:RecordNumber)         !Primary key field - type derived from field
sdm:SIDALERTRecordNumber LIKE(sdm:SIDALERTRecordNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB11::View:FileDropCombo VIEW(RETAILER)
                       PROJECT(rtr:Retailer)
                       PROJECT(rtr:RecordNumber)
                     END
History::sdl:Record  LIKE(sdl:RECORD),STATIC
QuickWindow          WINDOW('Update the SIDALERT File'),AT(,,679,412),FONT('Tahoma',8,,),CENTER,IMM,HLP('UpdateSIDAlert'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,672,118),USE(?Sheet1),SPREAD
                         TAB('General Defaults'),USE(?Tab1)
                           PROMPT('Status'),AT(132,20,120,10),USE(?Prompt:Status),FONT(,,,FONT:bold)
                           PROMPT('Status'),AT(8,20),USE(?sdl:Status:Prompt)
                           TEXT,AT(84,20,28,10),USE(sdl:Status),SKIP,FONT(,,,,CHARSET:ANSI),REQ,UPR,SINGLE,MSG('Status')
                           BUTTON('...'),AT(116,20,12,10),USE(?FldLookup)
                           PROMPT('Booking Type'),AT(268,20),USE(?sdl:BookingType:Prompt)
                           LIST,AT(344,20,124,10),USE(sdl:BookingType),VSCROLL,FONT(,,,FONT:bold),COLOR(080FFFFH),FORMAT('120L(2)|M@s30@'),DROP(10),FROM('RETAIL REPAIR|RETAIL REPAIR (ACC)|RETAIL IN-STORE EXCHANGE|RETAIL EXTERNAL EXCHA' &|
   'NGE|EXCHANGE|POSTAL|POSTAL (ACC)|7 DAY EXCHANGE|ACCESSORY ORDER|LEASE PHONE REPA' &|
   'IR|LEASE PHONE EXCHANGE|CSG|JOHN LEWIS RETAIL REPAIRS|JOHN LEWIS POSTAL'),MSG('Booking Type')
                           TEXT,AT(84,32,384,10),USE(sdl:Description),FONT(,,,,CHARSET:ANSI),REQ,UPR,SINGLE,MSG('Description')
                           PROMPT('Original Retailer'),AT(8,44),USE(?sdl:Description:Prompt:2),TRN
                           COMBO(@s30),AT(84,44,124,10),USE(sdl:Retailer),IMM,VSCROLL,COLOR(080FFFFH),REQ,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           GROUP,AT(584,20,84,36),USE(?Group:System),HIDE
                             CHECK('System Alert'),AT(596,20),USE(sdl:SystemAlert),TRN,MSG('System Alert'),TIP('System Alert'),VALUE('1','0')
                             CHECK('Check Accessories'),AT(596,30),USE(sdl:CheckAccessories),TRN,MSG('Check Accessories'),TIP('Check Accessories'),VALUE('1','0')
                             CHECK('With Battery'),AT(596,40),USE(sdl:WithBattery),TRN,MSG('With Battery'),TIP('With Battery'),VALUE('1','0')
                           END
                           GROUP('Days To Be Sent'),AT(80,56,388,22),USE(?Group1),BOXED
                             CHECK('Monday'),AT(84,66),USE(sdl:Monday),TRN,MSG('Monday'),TIP('Monday'),VALUE('1','0')
                             CHECK('Tuesday'),AT(136,66),USE(sdl:Tuesday),TRN,MSG('Tuesday'),TIP('Tuesday'),VALUE('1','0')
                             CHECK('Wednesday'),AT(190,66),USE(sdl:Wednesday),TRN,MSG('Wednesday'),TIP('Wednesday'),VALUE('1','0')
                             CHECK('Thursday'),AT(255,67),USE(sdl:Thursday),TRN,MSG('Thursday'),TIP('Thursday'),VALUE('1','0')
                             CHECK('Friday'),AT(312,66),USE(sdl:Friday),TRN,MSG('Friday'),TIP('Friday'),VALUE('1','0')
                             CHECK('Saturday'),AT(359,66),USE(sdl:Saturday),TRN,MSG('Saturday'),TIP('Saturday'),VALUE('1','0')
                             CHECK('Sunday'),AT(415,66),USE(sdl:Sunday),TRN,MSG('Sunday'),TIP('Sunday'),VALUE('1','0')
                           END
                           PROMPT('WARNING:  A notification already exists with the same RETAILER, STATUS, BOOKING ' &|
   'TYPE, and DAYS TO BE SENT combination.  Please amend your selection as you will ' &|
   'be unable to save this notification in its current state.'),AT(8,104,664,18),USE(?Prompt:Error),HIDE,FONT(,,COLOR:Red,FONT:bold)
                           OPTION('Send To'),AT(80,78,228,24),USE(sdl:SendTo),BOXED,MSG('Send To')
                             RADIO('Customer Only'),AT(84,89),USE(?sdl:SendTo:Radio1),VALUE('1')
                             RADIO('UTL Only'),AT(176,89),USE(?sdl:SendTo:Radio2),VALUE('2')
                             RADIO('Both'),AT(252,89),USE(?sdl:SendTo:Radio3),VALUE('3')
                           END
                           BUTTON('Model Exceptions'),AT(400,78,68,16),USE(?Button:ModelExceptions)
                           PROMPT('Description'),AT(8,32),USE(?sdl:Description:Prompt)
                         END
                       END
                       CHECK('SMS Alert Active'),AT(8,128),USE(sdl:SMSAlertActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                       SHEET,AT(4,124,336,154),USE(?Sheet2),WIZARD,SPREAD
                         TAB('SMS Notification'),USE(?Tab2)
                           PROMPT('Cut Off Time'),AT(12,150),USE(?sdl:CutOffTime:Prompt)
                           LIST,AT(72,150,64,10),USE(sdl:CutOffTime),VSCROLL,FONT(,,,FONT:bold),FORMAT('20L(2)|M@T03@'),DROP(10),FROM(CutOffTimeList),MSG('Cut Off Time')
                           OPTION('Cut Off Times'),AT(8,140,168,50),USE(sdl:CutOffTimeType),BOXED,MSG('Cut Off Time Type')
                             RADIO('Send next day if generated after cut off time'),AT(12,164),USE(?Option2:Radio1),VALUE('1')
                             RADIO('Discard if generated after cut off time'),AT(12,176),USE(?Option2:Radio2),VALUE('2')
                           END
                           OPTION('Bank Holidays'),AT(176,140,156,50),USE(sdl:BankHolidays),BOXED,MSG('Bank Holidays')
                             RADIO('Send on bank holidays'),AT(180,152),USE(?sdl:BankHolidays:Radio1),VALUE('1')
                             RADIO('Send next working day after bank holiday'),AT(180,164),USE(?sdl:BankHolidays:Radio2),TRN,VALUE('2')
                             RADIO('Discard if generated on a bank holiday'),AT(180,176),USE(?sdl:BankHolidays:Radio3),VALUE('3')
                           END
                           PROMPT('Message Text'),AT(8,192),USE(?sdl:SMSMessageText:Prompt)
                           TEXT,AT(80,193,252,67),USE(sdl:SMSMessageText),VSCROLL,LEFT,MSG('SMS Message Text'),TIP('SMS Message Text'),REQ
                           BUTTON('Add Merge Field'),AT(8,246,68,16),USE(?Button:AddMergeField)
                           PROMPT('Text Length (including merge fields): '),AT(80,264),USE(?Prompt6)
                           PROMPT('0'),AT(200,264),USE(?Prompt:SMSTextLength)
                         END
                       END
                       CHECK('Email Alert Active'),AT(348,128),USE(sdl:EmailAlertActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                       SHEET,AT(344,124,332,154),USE(?Sheet3),WIZARD,SPREAD
                         TAB('Email Notification'),USE(?Tab3)
                           PROMPT('Message Subject'),AT(348,142),USE(?sld:EmailMessagSubject)
                           TEXT,AT(420,142,252,10),USE(sdl:EmailMessageSubject),FONT(,,,,CHARSET:ANSI),REQ,SINGLE,MSG('Email Message Subject')
                           PROMPT('Message Text'),AT(348,156),USE(?sdl2:EmailMessageText:Prompt)
                           TEXT,AT(420,156,252,104),USE(sdl2:EmailMessageText),VSCROLL,LEFT,MSG('Email Message Text'),TIP('Email Message Text'),REQ
                           BUTTON('Add Merge Field'),AT(348,246,68,16),USE(?Button:AddMergeField:2)
                           PROMPT('Text Length (including merge fields): '),AT(420,264),USE(?Prompt6:2)
                           PROMPT('0'),AT(540,264),USE(?Prompt:EmailTextLength)
                         END
                       END
                       SHEET,AT(4,282,672,108),USE(?Sheet4),SPREAD
                         TAB('Reminder Notifications'),USE(?Tab4)
                           LIST,AT(8,296,472,90),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('240L(2)|M~Description~@s60@60L(2)|M~Elapsed Days~@s8@50L(2)|M~SMS Active~@s3@48L' &|
   '(2)|M~Email Active~@s3@37L(2)|M~Send To~@s8@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(488,330,56,16),USE(?Insert),LEFT,ICON('insert.ico')
                           BUTTON('&Change'),AT(488,350,56,16),USE(?Change),LEFT,ICON('edit.ico')
                           BUTTON('&Delete'),AT(488,370,56,16),USE(?Delete),LEFT,ICON('delete.ico')
                         END
                       END
                       BUTTON('OK'),AT(560,394,56,16),USE(?OK),LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('Cancel'),AT(620,394,56,16),USE(?Cancel),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt:Status{prop:FontColor} = -1
    ?Prompt:Status{prop:Color} = 15066597
    ?sdl:Status:Prompt{prop:FontColor} = -1
    ?sdl:Status:Prompt{prop:Color} = 15066597
    If ?sdl:Status{prop:ReadOnly} = True
        ?sdl:Status{prop:FontColor} = 65793
        ?sdl:Status{prop:Color} = 15066597
    Elsif ?sdl:Status{prop:Req} = True
        ?sdl:Status{prop:FontColor} = 65793
        ?sdl:Status{prop:Color} = 8454143
    Else ! If ?sdl:Status{prop:Req} = True
        ?sdl:Status{prop:FontColor} = 65793
        ?sdl:Status{prop:Color} = 16777215
    End ! If ?sdl:Status{prop:Req} = True
    ?sdl:Status{prop:Trn} = 0
    ?sdl:Status{prop:FontStyle} = font:Bold
    ?sdl:BookingType:Prompt{prop:FontColor} = -1
    ?sdl:BookingType:Prompt{prop:Color} = 15066597
    If ?sdl:Description{prop:ReadOnly} = True
        ?sdl:Description{prop:FontColor} = 65793
        ?sdl:Description{prop:Color} = 15066597
    Elsif ?sdl:Description{prop:Req} = True
        ?sdl:Description{prop:FontColor} = 65793
        ?sdl:Description{prop:Color} = 8454143
    Else ! If ?sdl:Description{prop:Req} = True
        ?sdl:Description{prop:FontColor} = 65793
        ?sdl:Description{prop:Color} = 16777215
    End ! If ?sdl:Description{prop:Req} = True
    ?sdl:Description{prop:Trn} = 0
    ?sdl:Description{prop:FontStyle} = font:Bold
    ?sdl:Description:Prompt:2{prop:FontColor} = -1
    ?sdl:Description:Prompt:2{prop:Color} = 15066597
    If ?sdl:Retailer{prop:ReadOnly} = True
        ?sdl:Retailer{prop:FontColor} = 65793
        ?sdl:Retailer{prop:Color} = 15066597
    Elsif ?sdl:Retailer{prop:Req} = True
        ?sdl:Retailer{prop:FontColor} = 65793
        ?sdl:Retailer{prop:Color} = 8454143
    Else ! If ?sdl:Retailer{prop:Req} = True
        ?sdl:Retailer{prop:FontColor} = 65793
        ?sdl:Retailer{prop:Color} = 16777215
    End ! If ?sdl:Retailer{prop:Req} = True
    ?sdl:Retailer{prop:Trn} = 0
    ?sdl:Retailer{prop:FontStyle} = font:Bold
    ?Group:System{prop:Font,3} = -1
    ?Group:System{prop:Color} = 15066597
    ?Group:System{prop:Trn} = 0
    ?sdl:SystemAlert{prop:Font,3} = -1
    ?sdl:SystemAlert{prop:Color} = 15066597
    ?sdl:SystemAlert{prop:Trn} = 0
    ?sdl:CheckAccessories{prop:Font,3} = -1
    ?sdl:CheckAccessories{prop:Color} = 15066597
    ?sdl:CheckAccessories{prop:Trn} = 0
    ?sdl:WithBattery{prop:Font,3} = -1
    ?sdl:WithBattery{prop:Color} = 15066597
    ?sdl:WithBattery{prop:Trn} = 0
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?sdl:Monday{prop:Font,3} = -1
    ?sdl:Monday{prop:Color} = 15066597
    ?sdl:Monday{prop:Trn} = 0
    ?sdl:Tuesday{prop:Font,3} = -1
    ?sdl:Tuesday{prop:Color} = 15066597
    ?sdl:Tuesday{prop:Trn} = 0
    ?sdl:Wednesday{prop:Font,3} = -1
    ?sdl:Wednesday{prop:Color} = 15066597
    ?sdl:Wednesday{prop:Trn} = 0
    ?sdl:Thursday{prop:Font,3} = -1
    ?sdl:Thursday{prop:Color} = 15066597
    ?sdl:Thursday{prop:Trn} = 0
    ?sdl:Friday{prop:Font,3} = -1
    ?sdl:Friday{prop:Color} = 15066597
    ?sdl:Friday{prop:Trn} = 0
    ?sdl:Saturday{prop:Font,3} = -1
    ?sdl:Saturday{prop:Color} = 15066597
    ?sdl:Saturday{prop:Trn} = 0
    ?sdl:Sunday{prop:Font,3} = -1
    ?sdl:Sunday{prop:Color} = 15066597
    ?sdl:Sunday{prop:Trn} = 0
    ?sdl:SendTo{prop:Font,3} = -1
    ?sdl:SendTo{prop:Color} = 15066597
    ?sdl:SendTo{prop:Trn} = 0
    ?sdl:SendTo:Radio1{prop:Font,3} = -1
    ?sdl:SendTo:Radio1{prop:Color} = 15066597
    ?sdl:SendTo:Radio1{prop:Trn} = 0
    ?sdl:SendTo:Radio2{prop:Font,3} = -1
    ?sdl:SendTo:Radio2{prop:Color} = 15066597
    ?sdl:SendTo:Radio2{prop:Trn} = 0
    ?sdl:SendTo:Radio3{prop:Font,3} = -1
    ?sdl:SendTo:Radio3{prop:Color} = 15066597
    ?sdl:SendTo:Radio3{prop:Trn} = 0
    ?sdl:Description:Prompt{prop:FontColor} = -1
    ?sdl:Description:Prompt{prop:Color} = 15066597
    ?sdl:SMSAlertActive{prop:Font,3} = -1
    ?sdl:SMSAlertActive{prop:Color} = 15066597
    ?sdl:SMSAlertActive{prop:Trn} = 0
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?sdl:CutOffTime:Prompt{prop:FontColor} = -1
    ?sdl:CutOffTime:Prompt{prop:Color} = 15066597
    ?sdl:CutOffTime{prop:FontColor} = 65793
    ?sdl:CutOffTime{prop:Color}= 16777215
    ?sdl:CutOffTime{prop:Color,2} = 16777215
    ?sdl:CutOffTime{prop:Color,3} = 12937777
    ?sdl:CutOffTimeType{prop:Font,3} = -1
    ?sdl:CutOffTimeType{prop:Color} = 15066597
    ?sdl:CutOffTimeType{prop:Trn} = 0
    ?Option2:Radio1{prop:Font,3} = -1
    ?Option2:Radio1{prop:Color} = 15066597
    ?Option2:Radio1{prop:Trn} = 0
    ?Option2:Radio2{prop:Font,3} = -1
    ?Option2:Radio2{prop:Color} = 15066597
    ?Option2:Radio2{prop:Trn} = 0
    ?sdl:BankHolidays{prop:Font,3} = -1
    ?sdl:BankHolidays{prop:Color} = 15066597
    ?sdl:BankHolidays{prop:Trn} = 0
    ?sdl:BankHolidays:Radio1{prop:Font,3} = -1
    ?sdl:BankHolidays:Radio1{prop:Color} = 15066597
    ?sdl:BankHolidays:Radio1{prop:Trn} = 0
    ?sdl:BankHolidays:Radio2{prop:Font,3} = -1
    ?sdl:BankHolidays:Radio2{prop:Color} = 15066597
    ?sdl:BankHolidays:Radio2{prop:Trn} = 0
    ?sdl:BankHolidays:Radio3{prop:Font,3} = -1
    ?sdl:BankHolidays:Radio3{prop:Color} = 15066597
    ?sdl:BankHolidays:Radio3{prop:Trn} = 0
    ?sdl:SMSMessageText:Prompt{prop:FontColor} = -1
    ?sdl:SMSMessageText:Prompt{prop:Color} = 15066597
    If ?sdl:SMSMessageText{prop:ReadOnly} = True
        ?sdl:SMSMessageText{prop:FontColor} = 65793
        ?sdl:SMSMessageText{prop:Color} = 15066597
    Elsif ?sdl:SMSMessageText{prop:Req} = True
        ?sdl:SMSMessageText{prop:FontColor} = 65793
        ?sdl:SMSMessageText{prop:Color} = 8454143
    Else ! If ?sdl:SMSMessageText{prop:Req} = True
        ?sdl:SMSMessageText{prop:FontColor} = 65793
        ?sdl:SMSMessageText{prop:Color} = 16777215
    End ! If ?sdl:SMSMessageText{prop:Req} = True
    ?sdl:SMSMessageText{prop:Trn} = 0
    ?sdl:SMSMessageText{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Prompt:SMSTextLength{prop:FontColor} = -1
    ?Prompt:SMSTextLength{prop:Color} = 15066597
    ?sdl:EmailAlertActive{prop:Font,3} = -1
    ?sdl:EmailAlertActive{prop:Color} = 15066597
    ?sdl:EmailAlertActive{prop:Trn} = 0
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?sld:EmailMessagSubject{prop:FontColor} = -1
    ?sld:EmailMessagSubject{prop:Color} = 15066597
    If ?sdl:EmailMessageSubject{prop:ReadOnly} = True
        ?sdl:EmailMessageSubject{prop:FontColor} = 65793
        ?sdl:EmailMessageSubject{prop:Color} = 15066597
    Elsif ?sdl:EmailMessageSubject{prop:Req} = True
        ?sdl:EmailMessageSubject{prop:FontColor} = 65793
        ?sdl:EmailMessageSubject{prop:Color} = 8454143
    Else ! If ?sdl:EmailMessageSubject{prop:Req} = True
        ?sdl:EmailMessageSubject{prop:FontColor} = 65793
        ?sdl:EmailMessageSubject{prop:Color} = 16777215
    End ! If ?sdl:EmailMessageSubject{prop:Req} = True
    ?sdl:EmailMessageSubject{prop:Trn} = 0
    ?sdl:EmailMessageSubject{prop:FontStyle} = font:Bold
    ?sdl2:EmailMessageText:Prompt{prop:FontColor} = -1
    ?sdl2:EmailMessageText:Prompt{prop:Color} = 15066597
    If ?sdl2:EmailMessageText{prop:ReadOnly} = True
        ?sdl2:EmailMessageText{prop:FontColor} = 65793
        ?sdl2:EmailMessageText{prop:Color} = 15066597
    Elsif ?sdl2:EmailMessageText{prop:Req} = True
        ?sdl2:EmailMessageText{prop:FontColor} = 65793
        ?sdl2:EmailMessageText{prop:Color} = 8454143
    Else ! If ?sdl2:EmailMessageText{prop:Req} = True
        ?sdl2:EmailMessageText{prop:FontColor} = 65793
        ?sdl2:EmailMessageText{prop:Color} = 16777215
    End ! If ?sdl2:EmailMessageText{prop:Req} = True
    ?sdl2:EmailMessageText{prop:Trn} = 0
    ?sdl2:EmailMessageText{prop:FontStyle} = font:Bold
    ?Prompt6:2{prop:FontColor} = -1
    ?Prompt6:2{prop:Color} = 15066597
    ?Prompt:EmailTextLength{prop:FontColor} = -1
    ?Prompt:EmailTextLength{prop:Color} = 15066597
    ?Sheet4{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
DuplicateAlertCheck        Routine
    ?Prompt:Error{prop:Hide} = 1
    ?OK{prop:Disable} = 0
    Access:SIDALERT_ALIAS.Clearkey(sdlali:BookingTypeStatusKey)
    sdlali:BookingType = sdl:BookingType
    sdlali:Status = sdl:Status
    Set(sdlali:BookingTypeStatusKey,sdlali:BookingTypeStatusKey)
    Loop ! Begin Loop
        If Access:SIDALERT_ALIAS.Next()
            Break
        End ! If Access:SIDALERT_ALIAS.Next()
        If sdlali:BookingType <> sdl:BookingType
            Break
        End ! If sdlali:BookingType <> sdl:BookingType
        If sdlali:Status <> sdl:Status
            Break
        End ! If sdlali:Status <> sdl:Status
        If sdlali:RecordNumber = sdl:RecordNumber
            Cycle
        End ! If sdlali:RecordNumber <> sdl:RecordNumber
        IF (sdlali:Retailer <> sdl:Retailer)
            ! #Nick Allow the same alerts for different retailers (DBH: 27/08/2015)
            CYCLE
        END ! IF

        If sdlali:SendTo <> 3 And sdl:SendTo <> 3
            If sdlali:SendTo <> sdl:SendTo
                Cycle
            End ! If sdlali:SendTo <> sdl:SendTo
        End ! If sdlali:SendTo <> 3 And sdlali:SendTo <> 3

        If (sdl:Monday And sdlali:Monday) Or |
            (sdl:Tuesday And sdlali:Tuesday) Or |
            (sdl:Wednesday And sdlali:Wednesday) Or |
            (sdl:Thursday And sdlali:Thursday) Or |
            (sdl:Friday And sdlali:Friday) Or |
            (sdl:Saturday And sdlali:Saturday) Or |
            (sdl:Sunday And sdlali:Sunday)
            ?Prompt:Error{prop:Hide} = 0
            ?OK{prop:Disable} = 1
            Break
        End ! (sdl:Sunday And sdl:Sunday = sdlali:Sunday)
    End ! Loop
    Display()
GetTextLength            Routine
Data
local:Length            Long()
local:Start             Long()
local:End               Long()
Code
    local:Start = 0
    local:End = 0
    local:Length = 0
    Loop x# = 1 To Len(Clip(sdl:SMSMessageText))
        If Sub(sdl:SMSMessageText,x#,1) = '<13>' Or Sub(sdl:SMSMessageText,x#,1) = '<10>'
            Cycle
        End ! If Sub(sdl:SMSMessageText,x#,1) = '<13' Or Sub(sdl:SMSMessageText,x#,1) = '<10>'

        If Sub(sdl:SMSMessageText,x#,2) = '[*'
            local:Start = x#
            local:End = 0
        End ! If Sub(sdl:SMSMessageText,x#,2) = '[*'
        If Sub(sdl:SMSMessageText,x#,2) = '*]'
            local:End = x#
        End ! If Sub(sdl:SMSMessageText,x#,2) = '*]'

        If local:Start = 0
            local:Length += 1
        Else ! If local:Start = 0
            If local:End > 0
                Access:MERGE.ClearKey(mer:FieldNameKey)
                mer:FieldName = Sub(sdl:SMSMessageText,local:Start + 2,local:End - local:Start - 2)
                If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Found
                    local:Length += (mer:TotalFieldLength - 1)
                    local:Start = 0
                    local:End = 0
                Else ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Error
                End ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
            End ! If local:End > 0
        End ! If local:Start = 0
    End ! Loop x# = 1 To 500

    ?Prompt:SMSTextLength{prop:Text} = local:Length & '/480'
    If local:Length > 480
        ?Prompt:SMSTextLength{prop:fontColor} = color:Red
    Else ! If local:Length > 480
        ?Prompt:SMSTextLength{prop:fontColor} = color:None
    End ! If local:Length > 480

    local:Start = 0
    local:End = 0
    local:Length = 0
    Loop x# = 1 To Len(Clip(sdl2:EmailMessageText))
        If Sub(sdl2:EmailMessageText,x#,1) = '<13>' Or Sub(sdl2:EMailMessageText,x#,1) = '<10>'
            Cycle
        End ! If Sub(sdl:SMSMessageText,x#,1) = '<13' Or Sub(sdl:SMSMessageText,x#,1) = '<10>'

        If Sub(sdl2:EmailMessageText,x#,2) = '[*'
            local:Start = x#
            local:End = 0
        End ! If Sub(sdl:SMSMessageText,x#,2) = '[*'
        If Sub(sdl2:EmailMessageText,x#,2) = '*]'
            local:End = x#
        End ! If Sub(sdl:SMSMessageText,x#,2) = '*]'

        If local:Start = 0
            local:Length += 1
        Else ! If local:Start = 0
            If local:End > 0
                Access:MERGE.ClearKey(mer:FieldNameKey)
                mer:FieldName = Sub(sdl2:EmailMessageText,local:Start + 2,local:End - local:Start - 2)
                If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Found
                    local:Length += (mer:TotalFieldLength - 1)
                    local:Start = 0
                    local:End = 0
                Else ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Error
                End ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
            End ! If local:End > 0
        End ! If local:Start = 0
    End ! Loop x# = 1 To 500

    ?Prompt:EmailTextLength{prop:Text} = local:Length & '/4000'
    If local:Length > 4000
        ?Prompt:EmailTextLength{prop:fontColor} = color:Red
    Else ! If local:Length > 480
        ?Prompt:EmailTextLength{prop:fontColor} = color:None
    End ! If local:Length > 480
ShowStatus        Routine
    Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
    sts:Ref_Number = sdl:Status
    If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Found
        ?Prompt:Status{prop:Text} = sts:Status
        Display
    Else ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
        !Error
    End ! If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Alert'
  OF ChangeRecord
    ActionMessage = 'Changing An Alert'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSIDAlert')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt:Status
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Clear(CutOffTimeList)
  Free(CutOffTimeList)
  
  cutoff:CutOffTime = Deformat('08:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('09:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('10:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('11:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('12:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('13:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('14:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('15:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('16:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('17:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('18:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('19:00',@t1)
  Add(CutOffTimeList)
  cutoff:CutOffTime = Deformat('20:00',@t1)
  Add(CutOffTimeList)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sdl:Record,History::sdl:Record)
  SELF.AddHistoryField(?sdl:Status,2)
  SELF.AddHistoryField(?sdl:BookingType,3)
  SELF.AddHistoryField(?sdl:Description,4)
  SELF.AddHistoryField(?sdl:Retailer,23)
  SELF.AddHistoryField(?sdl:SystemAlert,20)
  SELF.AddHistoryField(?sdl:CheckAccessories,21)
  SELF.AddHistoryField(?sdl:WithBattery,22)
  SELF.AddHistoryField(?sdl:Monday,5)
  SELF.AddHistoryField(?sdl:Tuesday,6)
  SELF.AddHistoryField(?sdl:Wednesday,7)
  SELF.AddHistoryField(?sdl:Thursday,8)
  SELF.AddHistoryField(?sdl:Friday,9)
  SELF.AddHistoryField(?sdl:Saturday,10)
  SELF.AddHistoryField(?sdl:Sunday,11)
  SELF.AddHistoryField(?sdl:SendTo,12)
  SELF.AddHistoryField(?sdl:SMSAlertActive,13)
  SELF.AddHistoryField(?sdl:CutOffTime,14)
  SELF.AddHistoryField(?sdl:CutOffTimeType,15)
  SELF.AddHistoryField(?sdl:BankHolidays,16)
  SELF.AddHistoryField(?sdl:SMSMessageText,17)
  SELF.AddHistoryField(?sdl:EmailAlertActive,18)
  SELF.AddHistoryField(?sdl:EmailMessageSubject,19)
  SELF.AddUpdateFile(Access:SIDALERT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MERGE.Open
  Relate:RETAILER.Open
  Relate:SIDALERT.Open
  Relate:SIDALERT_ALIAS.Open
  Relate:STATUS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SIDALERT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If ThisWindow.Request = InsertRecord
      If Access:SIDALER2.PrimeRecord() = Level:Benign
          sdl2:SIDALERTRecordNumber = sdl:RecordNumber
          If Access:SIDALER2.TryInsert() = Level:Benign
              !Insert
          Else ! If Access:SIDALER2.TryInsert() = Level:Benign
              Access:SIDALER2.CancelAutoInc()
          End ! If Access:SIDALER2.TryInsert() = Level:Benign
      End ! If Access.SIDALER2.PrimeRecord() = Level:Benign
  Else ! If ThisWindow.Request = InsertRecord
      Access:SIDALER2.ClearKey(sdl2:SIDALERTRecordNumberKey)
      sdl2:SIDALERTRecordNumber = sdl:RecordNumber
      If Access:SIDALER2.TryFetch(sdl2:SIDALERTRecordNumberKey) = Level:Benign
          !Found
      Else ! If Access:SIDALER2.TryFetch(sdl2:SIDALERTRecordNumberKey) = Level:Benign
          !Error
          If Access:SIDALER2.PrimeRecord() = Level:Benign
              sdl2:SIDALERTRecordNumber = sdl:RecordNumber
              If Access:SIDALER2.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:SIDALER2.TryInsert() = Level:Benign
                  Access:SIDALER2.CancelAutoInc()
              End ! If Access:SIDALER2.TryInsert() = Level:Benign
          End ! If Access.SIDALER2.PrimeRecord() = Level:Benign
      End ! If Access:SIDALER2.TryFetch(sdl2:SIDALERTRecordNumberKey) = Level:Benign
  End ! If ThisWindow.Request = InsertRecord
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:SIDREMIN,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do GetTextLength
  Do DuplicateAlertCheck
  Do ShowStatus
  If ~FullAccess(glo:PassAccount,glo:Password)
      ! Full Access
      ?Group:System{prop:Hide} = 0
  Else  ! ~If FullAccess(glo:Password)
      If sdl:SystemAlert
          ?Sheet1{prop:Disable} = 1
          ?sdl:CutOffTimeType{prop:Disable} = 1
          ?sdl:CutOffTime:Prompt{prop:Disable} = 1
          ?sdl:CutOffTime{prop:Disable} = 1
          ?sdl:BankHolidays{prop:Disable} = 1
      End ! If sdl:SystemAlert
  End ! ~If FullAccess(glo:Password)
  Do RecolourWindow
  ?sdl:BookingType{prop:vcr} = TRUE
  ?sdl:CutOffTime{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  IF ?sdl:Status{Prop:Tip} AND ~?FldLookup{Prop:Tip}
     ?FldLookup{Prop:Tip} = 'Select ' & ?sdl:Status{Prop:Tip}
  END
  IF ?sdl:Status{Prop:Msg} AND ~?FldLookup{Prop:Msg}
     ?FldLookup{Prop:Msg} = 'Select ' & ?sdl:Status{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW3.Q &= Queue:Browse
  BRW3.AddSortOrder(,sdm:ElapsedKey)
  BRW3.AddRange(sdm:SIDALERTRecordNumber,sdl:RecordNumber)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,sdm:ElapsedDays,1,BRW3)
  BIND('tmp:SMSActive',tmp:SMSActive)
  BIND('tmp:EmailActive',tmp:EmailActive)
  BIND('tmp:SendTo',tmp:SendTo)
  BRW3.AddField(sdm:Description,BRW3.Q.sdm:Description)
  BRW3.AddField(sdm:ElapsedDays,BRW3.Q.sdm:ElapsedDays)
  BRW3.AddField(tmp:SMSActive,BRW3.Q.tmp:SMSActive)
  BRW3.AddField(tmp:EmailActive,BRW3.Q.tmp:EmailActive)
  BRW3.AddField(tmp:SendTo,BRW3.Q.tmp:SendTo)
  BRW3.AddField(sdm:RecordNumber,BRW3.Q.sdm:RecordNumber)
  BRW3.AddField(sdm:SIDALERTRecordNumber,BRW3.Q.sdm:SIDALERTRecordNumber)
  IF ?sdl:SystemAlert{Prop:Checked} = True
    ENABLE(?sdl:WithBattery)
    ENABLE(?sdl:CheckAccessories)
  END
  IF ?sdl:SystemAlert{Prop:Checked} = False
    sdl:CheckAccessories = 0
    sdl:WithBattery = 0
    DISABLE(?sdl:WithBattery)
    DISABLE(?sdl:CheckAccessories)
  END
  IF ?sdl:CheckAccessories{Prop:Checked} = True
    UNHIDE(?sdl:WithBattery)
  END
  IF ?sdl:CheckAccessories{Prop:Checked} = False
    HIDE(?sdl:WithBattery)
  END
  IF ?sdl:SMSAlertActive{Prop:Checked} = True
    ENABLE(?Sheet2)
  END
  IF ?sdl:SMSAlertActive{Prop:Checked} = False
    DISABLE(?Sheet2)
  END
  IF ?sdl:EmailAlertActive{Prop:Checked} = True
    ENABLE(?Sheet3)
  END
  IF ?sdl:EmailAlertActive{Prop:Checked} = False
    DISABLE(?Sheet3)
  END
  SELF.AddItem(ToolbarForm)
  BRW3.AskProcedure = 1
  FDCB11.Init(sdl:Retailer,?sdl:Retailer,Queue:FileDropCombo.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo,Relate:RETAILER,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo
  FDCB11.AddSortOrder(rtr:RetailerKey)
  FDCB11.AddField(rtr:Retailer,FDCB11.Q.rtr:Retailer)
  FDCB11.AddField(rtr:RecordNumber,FDCB11.Q.rtr:RecordNumber)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  BRW3.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MERGE.Close
    Relate:RETAILER.Close
    Relate:SIDALERT.Close
    Relate:SIDALERT_ALIAS.Close
    Relate:STATUS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  sdl2:SIDALERTRecordNumber = sdl:RecordNumber        ! Assign linking field value
  Access:SIDALER2.Fetch(sdl2:SIDALERTRecordNumberKey)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  
  ReturnValue = PARENT.Run(Number,Request)
  Access:SIDALER2.TryUpdate()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSIDAlertReminder
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button:AddMergeField:2
      Access:SIDALER2.TryUpdate()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sdl:Status
      Do DuplicateAlertCheck
    OF ?FldLookup
      ThisWindow.Update
      
      ! Lookup button pressed. Call select procedure if required
      sts:Status = sdl:Status                         ! Move value for lookup
      IF Access:STATUS.TryFetch(sts:Status_Key)       ! IF record not found
         sts:Status = sdl:Status                      ! Move value for lookup
         ! Based on ?sdl:Status
         SET(sts:Status_Key,sts:Status_Key)           ! Prime order for browse
         Access:STATUS.TryNext()
      END
      GlobalRequest = SelectRecord
      PickJobStatus                                   ! Allow user to select
      IF GlobalResponse <> RequestCompleted           ! User cancelled
         SELECT(?FldLookup)                           ! Reselect control
         CYCLE                                        ! Resume accept loop
      .
      CHANGE(?sdl:Status,sts:Status)
      GlobalResponse = RequestCancelled               ! Reset global response
      SELECT(?FldLookup+1)                            ! Select next control in sequence
      SELF.Request = SELF.OriginalRequest             ! Reset local request
      Do ShowStatus
    OF ?sdl:BookingType
      Do DuplicateAlertCheck
    OF ?sdl:Description
      Do DuplicateAlertCheck
    OF ?sdl:Retailer
      Do DuplicateAlertCheck
    OF ?sdl:SystemAlert
      IF ?sdl:SystemAlert{Prop:Checked} = True
        ENABLE(?sdl:WithBattery)
        ENABLE(?sdl:CheckAccessories)
      END
      IF ?sdl:SystemAlert{Prop:Checked} = False
        sdl:CheckAccessories = 0
        sdl:WithBattery = 0
        DISABLE(?sdl:WithBattery)
        DISABLE(?sdl:CheckAccessories)
      END
      ThisWindow.Reset
    OF ?sdl:CheckAccessories
      IF ?sdl:CheckAccessories{Prop:Checked} = True
        UNHIDE(?sdl:WithBattery)
      END
      IF ?sdl:CheckAccessories{Prop:Checked} = False
        HIDE(?sdl:WithBattery)
      END
      ThisWindow.Reset
    OF ?sdl:Monday
      Do DuplicateAlertCheck
    OF ?sdl:Tuesday
      Do DuplicateAlertCheck
    OF ?sdl:Wednesday
      Do DuplicateAlertCheck
    OF ?sdl:Thursday
      Do DuplicateAlertCheck
    OF ?sdl:Friday
      Do DuplicateAlertCheck
    OF ?sdl:Saturday
      Do DuplicateAlertCheck
    OF ?sdl:Sunday
      Do DuplicateAlertCheck
    OF ?sdl:SendTo
      Do DuplicateAlertCheck
    OF ?Button:ModelExceptions
      ThisWindow.Update
      BrowseSIDAlertModelExceptions(sdl:RecordNumber)
      ThisWindow.Reset
    OF ?sdl:SMSAlertActive
      IF ?sdl:SMSAlertActive{Prop:Checked} = True
        ENABLE(?Sheet2)
      END
      IF ?sdl:SMSAlertActive{Prop:Checked} = False
        DISABLE(?Sheet2)
      END
      ThisWindow.Reset
    OF ?sdl:SMSMessageText
      Do GetTextLength
    OF ?Button:AddMergeField
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseMerge('SMS')
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              x# = ?sdl:SMSMessageText{prop:SelStart}
              sdl:SMSMessageText = Sub(sdl:SMSMessageText,1,x# - 1) & ' [*' & Clip(mer:FieldName) & '*] ' & Sub(sdl:SMSMessageText,x#,500)
              Do GetTextLength
              Display()
          Of Requestcancelled
      End!Case Globalreponse
    OF ?sdl:EmailAlertActive
      IF ?sdl:EmailAlertActive{Prop:Checked} = True
        ENABLE(?Sheet3)
      END
      IF ?sdl:EmailAlertActive{Prop:Checked} = False
        DISABLE(?Sheet3)
      END
      ThisWindow.Reset
    OF ?sdl2:EmailMessageText
      Do GetTextLength
    OF ?Button:AddMergeField:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseMerge('EMAIL')
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              x# = ?sdl2:EmailMessageText{prop:SelStart}
              sdl2:EmailMessageText = Sub(sdl2:EmailMessageText,1,x# - 1) & ' [*' & Clip(mer:FieldName) & '*] ' & Sub(sdl2:EmailMessageText,x#,500)
              Do GetTextLength
              Display()
          Of Requestcancelled
      End!Case Globalreponse
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Error Checking
  If ?OK{prop:Disable} = 1
      Cycle
  End ! If ?OK{prop:Disable} = 1
  If ~sdl:Monday And ~sdl:Tuesday And ~sdl:Wednesday And ~sdl:Thursday And ~sdl:Friday And ~sdl:Saturday And ~sdl:Sunday
      Beep(Beep:SystemHand)  ;  Yield()
      Case Message('You have not selected which day(s) to send the message.','ServiceBase',|
                     icon:Hand,'&OK',1,1)
          Of 1 ! &OK Button
      End!Case Message
      Cycle
  End ! If ~sdl:Monday And ~sdl:Tuesday And ~sld:Wednesday And ~sld:Thursday And ~sld:Friday And ~sld:Saturday And ~sld:Sunday
  If sdl:SendTo = 0
      Beep(Beep:SystemHand)  ;  Yield()
      Case Message('You have not selected who to send the message to.','ServiceBase',|
                     icon:Hand,'&OK',1,1)
          Of 1 ! &OK Button
      End!Case Message
      Cycle
  End ! If sld:SentTo
  If sdl:SMSAlertActive And ~sdl:SystemAlert
      If sdl:CutOffTime = ''
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('You have not completed the "Cut Off Time" / "Bank Holiday" options.','ServiceBase',|
                         icon:Hand,'&OK',1,1)
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      End ! If sdl:CutOffTime = ''
      If sdl:CutOffTimeType = 0
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('You have not completed the "Cut Off Time" / "Bank Holiday" options.','ServiceBase',|
                         icon:Hand,'&OK',1,1)
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      End ! If sdl:CutOffTimeType = 0
      If sdl:BankHolidays = 0
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('You have not completed the "Cut Off Time" / "Bank Holiday" options.','ServiceBase',|
                         icon:Hand,'&OK',1,1)
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      End ! If sld:BankHolidays = 0
  End ! If sld:SMSAlertActive
  Access:SIDALER2.TryUpdate()
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW3.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW3.SetQueueRecord PROCEDURE

  CODE
  IF (sdm:SMSAlertActive)
    tmp:SMSActive = 'Yes'
  ELSE
    tmp:SMSActive = 'No'
  END
  IF (sdm:EmailAlertActive)
    tmp:EmailActive = 'Yes'
  ELSE
    tmp:EmailActive = 'No'
  END
  CASE (sdm:SendTo)
  OF 1
    tmp:SendTo = 'Customer'
  OF 2
    tmp:SendTo = 'UTL'
  OF 3
    tmp:SendTo = 'Both'
  ELSE
    tmp:SendTo = 'Not Set'
  END
  PARENT.SetQueueRecord
  SELF.Q.tmp:SMSActive = tmp:SMSActive                !Assign formula result to display queue
  SELF.Q.tmp:EmailActive = tmp:EmailActive            !Assign formula result to display queue
  SELF.Q.tmp:SendTo = tmp:SendTo                      !Assign formula result to display queue


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BrowseMergeFields PROCEDURE (f:xpos,f:ypos,f:Type)    !Generated from procedure template - Browse

window               WINDOW,AT(,,172,372),FONT('Tahoma',,,),GRAY,DOUBLE
                       PROMPT('Select Merge Field'),AT(4,4),USE(?Prompt1),TRN,FONT(,,,FONT:bold)
                       LIST,AT(4,16,164,334),USE(?List1),ALRT(MouseLeft2),FORMAT('120L(2)|M~Field~@s40@56R(2)|M~Max Length~@n3@'),FROM(MergeQueue)
                       BUTTON('Select'),AT(4,354,56,16),USE(?Select),LEFT,ICON('select.ico')
                       BUTTON('Cancel'),AT(112,354,56,16),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseMergeFields')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  Case f:Type
  Of 1
      0{prop:Xpos} = f:Xpos + 325
      0{prop:Ypos} = f:YPos + 35
  Of 2
      0{prop:Xpos} = f:Xpos + 245
      0{prop:Ypos} = f:YPos + 35
  Of 3
      0{prop:Xpos} = f:Xpos + 325
      0{prop:Ypos} = f:YPos + 40
  
  End ! Case f:Type
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Select
      ThisWindow.Update
      Get(MergeQueue,Choice(?List1))
      glo:ErrorText = merque:Field
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List1
    CASE EVENT()
    OF EVENT:AlertKey
      Post(Event:Accepted,?Select)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

