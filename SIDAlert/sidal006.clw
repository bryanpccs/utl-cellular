

   MEMBER('sidalertbrowse.clw')                       ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SIDAL006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SIDAL007.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseSIDAlertModelExceptions PROCEDURE (paramSIDAlertId) !Generated from procedure template - Window

tmp:SIDAlertId       LONG
BRW1::View:Browse    VIEW(SMOALERT)
                       PROJECT(sml:ModelNumber)
                       PROJECT(sml:RecordNumber)
                       PROJECT(sml:SIDALERTRecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
sml:ModelNumber        LIKE(sml:ModelNumber)          !List box control field - type derived from field
sml:RecordNumber       LIKE(sml:RecordNumber)         !Primary key field - type derived from field
sml:SIDALERTRecordNumber LIKE(sml:SIDALERTRecordNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BrowseWindow         WINDOW('Model Number Exceptions'),AT(,,248,188),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,156,180),USE(?Sheet1),SPREAD
                         TAB('By Model Number'),USE(?Tab1)
                           ENTRY(@s30),AT(8,20,124,10),USE(sml:ModelNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       LIST,AT(8,36,148,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse)
                       BUTTON('&Insert'),AT(168,88,76,20),USE(?Insert),LEFT,KEY(InsertKey),ICON('insert.ico')
                       BUTTON('&Change'),AT(168,112,76,20),USE(?Change),LEFT,KEY(CtrlEnter),ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(168,136,76,20),USE(?Delete),LEFT,KEY(DeleteKey),ICON('delete.ico')
                       BUTTON('Close'),AT(168,164,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?sml:ModelNumber{prop:ReadOnly} = True
        ?sml:ModelNumber{prop:FontColor} = 65793
        ?sml:ModelNumber{prop:Color} = 15066597
    Elsif ?sml:ModelNumber{prop:Req} = True
        ?sml:ModelNumber{prop:FontColor} = 65793
        ?sml:ModelNumber{prop:Color} = 8454143
    Else ! If ?sml:ModelNumber{prop:Req} = True
        ?sml:ModelNumber{prop:FontColor} = 65793
        ?sml:ModelNumber{prop:Color} = 16777215
    End ! If ?sml:ModelNumber{prop:Req} = True
    ?sml:ModelNumber{prop:Trn} = 0
    ?sml:ModelNumber{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseSIDAlertModelExceptions')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?sml:ModelNumber
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:SMOALERT.Open
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:SMOALERT,SELF)
  OPEN(BrowseWindow)
  SELF.Opened=True
  tmp:SIDAlertId = paramSIDAlertId
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,sml:SIDALERTRecordNumberKey)
  BRW1.AddRange(sml:SIDALERTRecordNumber,tmp:SIDAlertId)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sml:ModelNumber,sml:ModelNumber,1,BRW1)
  BRW1.AddField(sml:ModelNumber,BRW1.Q.sml:ModelNumber)
  BRW1.AddField(sml:RecordNumber,BRW1.Q.sml:RecordNumber)
  BRW1.AddField(sml:SIDALERTRecordNumber,BRW1.Q.sml:SIDALERTRecordNumber)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SMOALERT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSIDAlertModelException
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

