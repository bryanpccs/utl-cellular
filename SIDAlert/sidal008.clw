

   MEMBER('sidalertbrowse.clw')                       ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SIDAL008.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSIDAlertReminderModelException PROCEDURE (paramAlertReminderId, paramModelNo) !Generated from procedure template - Window

window               WINDOW('Changing An Alert Reminder'),AT(,,548,264),FONT('Tahoma',8,,,CHARSET:ANSI),CENTER,IMM,SYSTEM,GRAY,DOUBLE
                       CHECK('SMS Alert Active'),AT(8,5),USE(smm:SMSAlertActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                       CHECK('Email Alert Active'),AT(8,113),USE(smm:EmailAlertActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                       SHEET,AT(4,1,540,106),USE(?Sheet2),WIZARD,SPREAD
                         TAB('SMS Notification'),USE(?Tab2)
                           PROMPT('Message Text'),AT(8,19),USE(?sml:SMSMessageText:Prompt)
                           TEXT,AT(80,19,456,68),USE(smm:SMSMessageText),VSCROLL,LEFT,MSG('SMS Message Text'),TIP('SMS Message Text'),REQ
                           BUTTON('Add Merge Field'),AT(8,73,68,16),USE(?Button:AddMergeField)
                           PROMPT('Text Length (including merge fields): '),AT(80,93),USE(?Prompt6)
                           PROMPT('0'),AT(200,93),USE(?Prompt:SMSTextLength)
                         END
                       END
                       SHEET,AT(4,109,540,132),USE(?Sheet3),WIZARD,SPREAD
                         TAB('Email Notification'),USE(?Tab3)
                           PROMPT('Message Subject'),AT(8,126),USE(?sml:EmailMessagSubject)
                           TEXT,AT(80,126,252,10),USE(smm:EmailSubject),FONT(,,,,CHARSET:ANSI),REQ,SINGLE,MSG('Email Message Subject')
                           PROMPT('Message Text'),AT(8,141),USE(?sml2:EmailMessageText:Prompt)
                           TEXT,AT(80,141,456,80),USE(smm2:EmailMessageText),VSCROLL,LEFT,MSG('Email Message Text'),TIP('Email Message Text'),REQ
                           BUTTON('Add Merge Field'),AT(8,208,68,16),USE(?Button:AddMergeField:2)
                           PROMPT('Text Length (including merge fields): '),AT(80,227),USE(?Prompt6:2)
                           PROMPT('0'),AT(200,227),USE(?Prompt:EmailTextLength)
                         END
                       END
                       BUTTON('OK'),AT(428,244,56,16),USE(?OkButton),LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('Cancel'),AT(488,244,56,16),USE(?CancelButton),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?smm:SMSAlertActive{prop:Font,3} = -1
    ?smm:SMSAlertActive{prop:Color} = 15066597
    ?smm:SMSAlertActive{prop:Trn} = 0
    ?smm:EmailAlertActive{prop:Font,3} = -1
    ?smm:EmailAlertActive{prop:Color} = 15066597
    ?smm:EmailAlertActive{prop:Trn} = 0
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?sml:SMSMessageText:Prompt{prop:FontColor} = -1
    ?sml:SMSMessageText:Prompt{prop:Color} = 15066597
    If ?smm:SMSMessageText{prop:ReadOnly} = True
        ?smm:SMSMessageText{prop:FontColor} = 65793
        ?smm:SMSMessageText{prop:Color} = 15066597
    Elsif ?smm:SMSMessageText{prop:Req} = True
        ?smm:SMSMessageText{prop:FontColor} = 65793
        ?smm:SMSMessageText{prop:Color} = 8454143
    Else ! If ?smm:SMSMessageText{prop:Req} = True
        ?smm:SMSMessageText{prop:FontColor} = 65793
        ?smm:SMSMessageText{prop:Color} = 16777215
    End ! If ?smm:SMSMessageText{prop:Req} = True
    ?smm:SMSMessageText{prop:Trn} = 0
    ?smm:SMSMessageText{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Prompt:SMSTextLength{prop:FontColor} = -1
    ?Prompt:SMSTextLength{prop:Color} = 15066597
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?sml:EmailMessagSubject{prop:FontColor} = -1
    ?sml:EmailMessagSubject{prop:Color} = 15066597
    If ?smm:EmailSubject{prop:ReadOnly} = True
        ?smm:EmailSubject{prop:FontColor} = 65793
        ?smm:EmailSubject{prop:Color} = 15066597
    Elsif ?smm:EmailSubject{prop:Req} = True
        ?smm:EmailSubject{prop:FontColor} = 65793
        ?smm:EmailSubject{prop:Color} = 8454143
    Else ! If ?smm:EmailSubject{prop:Req} = True
        ?smm:EmailSubject{prop:FontColor} = 65793
        ?smm:EmailSubject{prop:Color} = 16777215
    End ! If ?smm:EmailSubject{prop:Req} = True
    ?smm:EmailSubject{prop:Trn} = 0
    ?smm:EmailSubject{prop:FontStyle} = font:Bold
    ?sml2:EmailMessageText:Prompt{prop:FontColor} = -1
    ?sml2:EmailMessageText:Prompt{prop:Color} = 15066597
    If ?smm2:EmailMessageText{prop:ReadOnly} = True
        ?smm2:EmailMessageText{prop:FontColor} = 65793
        ?smm2:EmailMessageText{prop:Color} = 15066597
    Elsif ?smm2:EmailMessageText{prop:Req} = True
        ?smm2:EmailMessageText{prop:FontColor} = 65793
        ?smm2:EmailMessageText{prop:Color} = 8454143
    Else ! If ?smm2:EmailMessageText{prop:Req} = True
        ?smm2:EmailMessageText{prop:FontColor} = 65793
        ?smm2:EmailMessageText{prop:Color} = 16777215
    End ! If ?smm2:EmailMessageText{prop:Req} = True
    ?smm2:EmailMessageText{prop:Trn} = 0
    ?smm2:EmailMessageText{prop:FontStyle} = font:Bold
    ?Prompt6:2{prop:FontColor} = -1
    ?Prompt6:2{prop:Color} = 15066597
    ?Prompt:EmailTextLength{prop:FontColor} = -1
    ?Prompt:EmailTextLength{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
GetTextLength            Routine
Data
local:Length            Long()
local:Start             Long()
local:End               Long()
Code
    local:Start = 0
    local:End = 0
    local:Length = 0
    Loop x# = 1 To Len(Clip(smm:SMSMessageText))
        If Sub(smm:SMSMessageText,x#,1) = '<13>' Or Sub(smm:SMSMessageText,x#,1) = '<10>'
            Cycle
        End ! If Sub(smm:SMSMessageText,x#,1) = '<13' Or Sub(smm:SMSMessageText,x#,1) = '<10>'

        If Sub(smm:SMSMessageText,x#,2) = '[*'
            local:Start = x#
            local:End = 0
        End ! If Sub(sdm:SMSMessageText,x#,2) = '[*'
        If Sub(smm:SMSMessageText,x#,2) = '*]'
            local:End = x#
        End ! If Sub(sdm:SMSMessageText,x#,2) = '*]'

        If local:Start = 0
            local:Length += 1
        Else ! If local:Start = 0
            If local:End > 0
                Access:MERGE.ClearKey(mer:FieldNameKey)
                mer:FieldName = Sub(smm:SMSMessageText,local:Start + 2,local:End - local:Start - 2)
                If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Found
                    local:Length += (mer:TotalFieldLength - 1)
                    local:Start = 0
                    local:End = 0
                Else ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Error
                End ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
            End ! If local:End > 0
        End ! If local:Start = 0
    End ! Loop x# = 1 To 500

    ?Prompt:SMSTextLength{prop:Text} = local:Length & '/480'
    If local:Length > 480
        ?Prompt:SMSTextLength{prop:fontColor} = color:Red
    Else ! If local:Length > 480
        ?Prompt:SMSTextLength{prop:fontColor} = color:None
    End ! If local:Length > 480

    local:Start = 0
    local:End = 0
    local:Length = 0
    Loop x# = 1 To Len(Clip(smm2:EmailMessageText))
        If Sub(smm2:EmailMessageText,x#,1) = '<13>' Or Sub(smm2:EMailMessageText,x#,1) = '<10>'
            Cycle
        End ! If Sub(smm:SMSMessageText,x#,1) = '<13' Or Sub(smm:SMSMessageText,x#,1) = '<10>'

        If Sub(smm2:EmailMessageText,x#,2) = '[*'
            local:Start = x#
            local:End = 0
        End ! If Sub(smm:SMSMessageText,x#,2) = '[*'
        If Sub(smm2:EmailMessageText,x#,2) = '*]'
            local:End = x#
        End ! If Sub(smm:SMSMessageText,x#,2) = '*]'

        If local:Start = 0
            local:Length += 1
        Else ! If local:Start = 0
            If local:End > 0
                Access:MERGE.ClearKey(mer:FieldNameKey)
                mer:FieldName = Sub(smm2:EmailMessageText,local:Start + 2,local:End - local:Start - 2)
                If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Found
                    local:Length += (mer:TotalFieldLength - 1)
                    local:Start = 0
                    local:End = 0
                Else ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Error
                End ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
            End ! If local:End > 0
        End ! If local:Start = 0
    End ! Loop x# = 1 To 500

    ?Prompt:EmailTextLength{prop:Text} = local:Length & '/4000'
    If local:Length > 4000
        ?Prompt:EmailTextLength{prop:fontColor} = color:Red
    Else ! If local:Length > 480
        ?Prompt:EmailTextLength{prop:fontColor} = color:None
    End ! If local:Length > 480

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSIDAlertReminderModelException')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?smm:SMSAlertActive
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MERGE.Open
  Relate:SMOREMI2.Open
  Access:SMOREMIN.UseFile
  SELF.FilesOpened = True
  ! Fetch the records
  Access:SMOREMIN.ClearKey(smm:SIDREMINRecordNumberKey)
  smm:SIDREMINRecordNumber = paramAlertReminderId
  smm:ModelNumber = paramModelNo
  If Access:SMOREMIN.TryFetch(smm:SIDREMINRecordNumberKey) = Level:Benign
      !Found
  Else ! If Access:SMOREMIN.TryFetch(smm:SIDREMINRecordNumberKey) = Level:Benign
      !Error
      If Access:SMOREMIN.PrimeRecord() = Level:Benign
          smm:SIDREMINRecordNumber = paramAlertReminderId
          smm:ModelNumber = paramModelNo
          If Access:SMOREMIN.TryInsert() = Level:Benign
              !Insert
          Else ! If Access:SMOREMIN.TryInsert() = Level:Benign
              Access:SMOREMIN.CancelAutoInc()
          End ! If Access:SMOREMIN.TryInsert() = Level:Benign
      End ! If Access.SMOREMIN.PrimeRecord() = Level:Benign
  End ! If Access:SMOREMIN.TryFetch(smm:SIDREMINRecordNumberKey) = Level:Benign
  
  Access:SMOREMI2.ClearKey(smm2:SMOREMINRecordNumberKey)
  smm2:SMOREMINRecordNumber = smm:RecordNumber
  If Access:SMOREMI2.TryFetch(smm2:SMOREMINRecordNumberKey) = Level:Benign
      !Found
  Else ! If Access:SMOREMI2.TryFetch(smm2:SMOREMINRecordNumberKey) = Level:Benign
      !Error
      If Access:SMOREMI2.PrimeRecord() = Level:Benign
          smm2:SMOREMINRecordNumber = smm:RecordNumber
          If Access:SMOREMI2.TryInsert() = Level:Benign
              !Insert
          Else ! If Access:SMOREMI2.TryInsert() = Level:Benign
              Access:SMOREMI2.CancelAutoInc()
          End ! If Access:SMOREMI2.TryInsert() = Level:Benign
      End ! If Access.SMOREMI2.PrimeRecord() = Level:Benign
  End ! If Access:SMOREMI2.TryFetch(smm2:SMOREMINRecordNumberKey) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  Do GetTextLength
  Do RecolourWindow
  IF ?smm:SMSAlertActive{Prop:Checked} = True
    ENABLE(?Sheet2)
  END
  IF ?smm:SMSAlertActive{Prop:Checked} = False
    DISABLE(?Sheet2)
  END
  IF ?smm:EmailAlertActive{Prop:Checked} = True
    ENABLE(?Sheet3)
  END
  IF ?smm:EmailAlertActive{Prop:Checked} = False
    DISABLE(?Sheet3)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MERGE.Close
    Relate:SMOREMI2.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button:AddMergeField:2
      Access:SMOREMI2.Update()
    OF ?OkButton
      Access:SMOREMIN.TryUpdate()
      Access:SMOREMI2.TryUpdate()
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?smm:SMSAlertActive
      IF ?smm:SMSAlertActive{Prop:Checked} = True
        ENABLE(?Sheet2)
      END
      IF ?smm:SMSAlertActive{Prop:Checked} = False
        DISABLE(?Sheet2)
      END
      ThisWindow.Reset
    OF ?smm:EmailAlertActive
      IF ?smm:EmailAlertActive{Prop:Checked} = True
        ENABLE(?Sheet3)
      END
      IF ?smm:EmailAlertActive{Prop:Checked} = False
        DISABLE(?Sheet3)
      END
      ThisWindow.Reset
    OF ?smm:SMSMessageText
      Do GetTextLength
    OF ?Button:AddMergeField
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseMerge('SMS')
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              x# = ?smm:SMSMessageText{prop:SelStart}
              smm:SMSMessageText = Sub(smm:SMSMessageText,1,x# - 1) & ' [*' & Clip(mer:FieldName) & '*] ' & Sub(smm:SMSMessageText,x#,500)
              Do GetTextLength
              Display()
          Of Requestcancelled
      End!Case Globalreponse
    OF ?smm2:EmailMessageText
      Do GetTextLength
    OF ?Button:AddMergeField:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseMerge('EMAIL')
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              x# = ?smm2:EmailMessageText{prop:SelStart}
              smm2:EmailMessageText = Sub(smm2:EmailMessageText,1,x# - 1) & ' [*' & Clip(mer:FieldName) & '*] ' & Sub(smm2:EmailMessageText,x#,500)
              Do GetTextLength
              Display()
          Of Requestcancelled
      End!Case Globalreponse
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

