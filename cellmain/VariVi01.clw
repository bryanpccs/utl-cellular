          MEMBER('cellmain.clw')                      ! This is a MEMBER module

   Map
   End

SolFileStatus       Procedure

  Code
  !if (SolaceCurrentTab = 5 and SolaceNoRefreshFileStatus = true) or SolaceNoRefreshFileStatus = false or SolaceSaveToFile = true then
    if Status(STDCHRGE) then
      SolaceViewVars('STDCHRGE',Thread(),'FileStatus',Status(STDCHRGE),Records(STDCHRGE))
    else
      SolaceViewVars('STDCHRGE',Thread(),'FileStatus',Status(STDCHRGE),0)
    end
    if Status(AccSKU) then
      SolaceViewVars('AccSKU',Thread(),'FileStatus',Status(AccSKU),Records(AccSKU))
    else
      SolaceViewVars('AccSKU',Thread(),'FileStatus',Status(AccSKU),0)
    end
    if Status(PROCFILE) then
      SolaceViewVars('PROCFILE',Thread(),'FileStatus',Status(PROCFILE),Records(PROCFILE))
    else
      SolaceViewVars('PROCFILE',Thread(),'FileStatus',Status(PROCFILE),0)
    end
    if Status(LOCDEFS) then
      SolaceViewVars('LOCDEFS',Thread(),'FileStatus',Status(LOCDEFS),Records(LOCDEFS))
    else
      SolaceViewVars('LOCDEFS',Thread(),'FileStatus',Status(LOCDEFS),0)
    end
    if Status(CARISMA) then
      SolaceViewVars('CARISMA',Thread(),'FileStatus',Status(CARISMA),Records(CARISMA))
    else
      SolaceViewVars('CARISMA',Thread(),'FileStatus',Status(CARISMA),0)
    end
    if Status(DEFEMAIL) then
      SolaceViewVars('DEFEMAIL',Thread(),'FileStatus',Status(DEFEMAIL),Records(DEFEMAIL))
    else
      SolaceViewVars('DEFEMAIL',Thread(),'FileStatus',Status(DEFEMAIL),0)
    end
    if Status(CRCDEFS) then
      SolaceViewVars('CRCDEFS',Thread(),'FileStatus',Status(CRCDEFS),Records(CRCDEFS))
    else
      SolaceViewVars('CRCDEFS',Thread(),'FileStatus',Status(CRCDEFS),0)
    end
    if Status(REGIONS) then
      SolaceViewVars('REGIONS',Thread(),'FileStatus',Status(REGIONS),Records(REGIONS))
    else
      SolaceViewVars('REGIONS',Thread(),'FileStatus',Status(REGIONS),0)
    end
    if Status(ACCREG) then
      SolaceViewVars('ACCREG',Thread(),'FileStatus',Status(ACCREG),Records(ACCREG))
    else
      SolaceViewVars('ACCREG',Thread(),'FileStatus',Status(ACCREG),0)
    end
    if Status(SIDACREG) then
      SolaceViewVars('SIDACREG',Thread(),'FileStatus',Status(SIDACREG),Records(SIDACREG))
    else
      SolaceViewVars('SIDACREG',Thread(),'FileStatus',Status(SIDACREG),0)
    end
    if Status(XMLJOBS) then
      SolaceViewVars('XMLJOBS',Thread(),'FileStatus',Status(XMLJOBS),Records(XMLJOBS))
    else
      SolaceViewVars('XMLJOBS',Thread(),'FileStatus',Status(XMLJOBS),0)
    end
    if Status(EXCHAMF) then
      SolaceViewVars('EXCHAMF',Thread(),'FileStatus',Status(EXCHAMF),Records(EXCHAMF))
    else
      SolaceViewVars('EXCHAMF',Thread(),'FileStatus',Status(EXCHAMF),0)
    end
    if Status(EXCHAUI) then
      SolaceViewVars('EXCHAUI',Thread(),'FileStatus',Status(EXCHAUI),Records(EXCHAUI))
    else
      SolaceViewVars('EXCHAUI',Thread(),'FileStatus',Status(EXCHAUI),0)
    end
    if Status(JOBSENG) then
      SolaceViewVars('JOBSENG',Thread(),'FileStatus',Status(JOBSENG),Records(JOBSENG))
    else
      SolaceViewVars('JOBSENG',Thread(),'FileStatus',Status(JOBSENG),0)
    end
    if Status(MULDESPJ) then
      SolaceViewVars('MULDESPJ',Thread(),'FileStatus',Status(MULDESPJ),Records(MULDESPJ))
    else
      SolaceViewVars('MULDESPJ',Thread(),'FileStatus',Status(MULDESPJ),0)
    end
    if Status(AUDSTAEX) then
      SolaceViewVars('AUDSTAEX',Thread(),'FileStatus',Status(AUDSTAEX),Records(AUDSTAEX))
    else
      SolaceViewVars('AUDSTAEX',Thread(),'FileStatus',Status(AUDSTAEX),0)
    end
    if Status(MULDESP) then
      SolaceViewVars('MULDESP',Thread(),'FileStatus',Status(MULDESP),Records(MULDESP))
    else
      SolaceViewVars('MULDESP',Thread(),'FileStatus',Status(MULDESP),0)
    end
    if Status(INSMODELS) then
      SolaceViewVars('INSMODELS',Thread(),'FileStatus',Status(INSMODELS),Records(INSMODELS))
    else
      SolaceViewVars('INSMODELS',Thread(),'FileStatus',Status(INSMODELS),0)
    end
    if Status(STOCKLOG) then
      SolaceViewVars('STOCKLOG',Thread(),'FileStatus',Status(STOCKLOG),Records(STOCKLOG))
    else
      SolaceViewVars('STOCKLOG',Thread(),'FileStatus',Status(STOCKLOG),0)
    end
    if Status(IMEILOG) then
      SolaceViewVars('IMEILOG',Thread(),'FileStatus',Status(IMEILOG),Records(IMEILOG))
    else
      SolaceViewVars('IMEILOG',Thread(),'FileStatus',Status(IMEILOG),0)
    end
    if Status(STMASAUD) then
      SolaceViewVars('STMASAUD',Thread(),'FileStatus',Status(STMASAUD),Records(STMASAUD))
    else
      SolaceViewVars('STMASAUD',Thread(),'FileStatus',Status(STMASAUD),0)
    end
    if Status(AUDSTATS) then
      SolaceViewVars('AUDSTATS',Thread(),'FileStatus',Status(AUDSTATS),Records(AUDSTATS))
    else
      SolaceViewVars('AUDSTATS',Thread(),'FileStatus',Status(AUDSTATS),0)
    end
    if Status(IEXPDEFS) then
      SolaceViewVars('IEXPDEFS',Thread(),'FileStatus',Status(IEXPDEFS),Records(IEXPDEFS))
    else
      SolaceViewVars('IEXPDEFS',Thread(),'FileStatus',Status(IEXPDEFS),0)
    end
    if Status(RAPIDLST) then
      SolaceViewVars('RAPIDLST',Thread(),'FileStatus',Status(RAPIDLST),Records(RAPIDLST))
    else
      SolaceViewVars('RAPIDLST',Thread(),'FileStatus',Status(RAPIDLST),0)
    end
    if Status(ICONTHIST) then
      SolaceViewVars('ICONTHIST',Thread(),'FileStatus',Status(ICONTHIST),Records(ICONTHIST))
    else
      SolaceViewVars('ICONTHIST',Thread(),'FileStatus',Status(ICONTHIST),0)
    end
    if Status(ORDTEMP) then
      SolaceViewVars('ORDTEMP',Thread(),'FileStatus',Status(ORDTEMP),Records(ORDTEMP))
    else
      SolaceViewVars('ORDTEMP',Thread(),'FileStatus',Status(ORDTEMP),0)
    end
    if Status(ORDITEMS) then
      SolaceViewVars('ORDITEMS',Thread(),'FileStatus',Status(ORDITEMS),Records(ORDITEMS))
    else
      SolaceViewVars('ORDITEMS',Thread(),'FileStatus',Status(ORDITEMS),0)
    end
    if Status(ORDHEAD) then
      SolaceViewVars('ORDHEAD',Thread(),'FileStatus',Status(ORDHEAD),Records(ORDHEAD))
    else
      SolaceViewVars('ORDHEAD',Thread(),'FileStatus',Status(ORDHEAD),0)
    end
    if Status(IMEISHIP) then
      SolaceViewVars('IMEISHIP',Thread(),'FileStatus',Status(IMEISHIP),Records(IMEISHIP))
    else
      SolaceViewVars('IMEISHIP',Thread(),'FileStatus',Status(IMEISHIP),0)
    end
    if Status(IACTION) then
      SolaceViewVars('IACTION',Thread(),'FileStatus',Status(IACTION),Records(IACTION))
    else
      SolaceViewVars('IACTION',Thread(),'FileStatus',Status(IACTION),0)
    end
    if Status(JOBTHIRD) then
      SolaceViewVars('JOBTHIRD',Thread(),'FileStatus',Status(JOBTHIRD),Records(JOBTHIRD))
    else
      SolaceViewVars('JOBTHIRD',Thread(),'FileStatus',Status(JOBTHIRD),0)
    end
    if Status(DEFAULTV) then
      SolaceViewVars('DEFAULTV',Thread(),'FileStatus',Status(DEFAULTV),Records(DEFAULTV))
    else
      SolaceViewVars('DEFAULTV',Thread(),'FileStatus',Status(DEFAULTV),0)
    end
    if Status(LOGRETRN) then
      SolaceViewVars('LOGRETRN',Thread(),'FileStatus',Status(LOGRETRN),Records(LOGRETRN))
    else
      SolaceViewVars('LOGRETRN',Thread(),'FileStatus',Status(LOGRETRN),0)
    end
    if Status(PRODCODE) then
      SolaceViewVars('PRODCODE',Thread(),'FileStatus',Status(PRODCODE),Records(PRODCODE))
    else
      SolaceViewVars('PRODCODE',Thread(),'FileStatus',Status(PRODCODE),0)
    end
    if Status(LOG2TEMP) then
      SolaceViewVars('LOG2TEMP',Thread(),'FileStatus',Status(LOG2TEMP),Records(LOG2TEMP))
    else
      SolaceViewVars('LOG2TEMP',Thread(),'FileStatus',Status(LOG2TEMP),0)
    end
    if Status(LOGSTOCK) then
      SolaceViewVars('LOGSTOCK',Thread(),'FileStatus',Status(LOGSTOCK),Records(LOGSTOCK))
    else
      SolaceViewVars('LOGSTOCK',Thread(),'FileStatus',Status(LOGSTOCK),0)
    end
    if Status(DEFEMAR1) then
      SolaceViewVars('DEFEMAR1',Thread(),'FileStatus',Status(DEFEMAR1),Records(DEFEMAR1))
    else
      SolaceViewVars('DEFEMAR1',Thread(),'FileStatus',Status(DEFEMAR1),0)
    end
    if Status(LETTERS) then
      SolaceViewVars('LETTERS',Thread(),'FileStatus',Status(LETTERS),Records(LETTERS))
    else
      SolaceViewVars('LETTERS',Thread(),'FileStatus',Status(LETTERS),0)
    end
    if Status(DEFEMAB2) then
      SolaceViewVars('DEFEMAB2',Thread(),'FileStatus',Status(DEFEMAB2),Records(DEFEMAB2))
    else
      SolaceViewVars('DEFEMAB2',Thread(),'FileStatus',Status(DEFEMAB2),0)
    end
    if Status(DEFEMAB3) then
      SolaceViewVars('DEFEMAB3',Thread(),'FileStatus',Status(DEFEMAB3),Records(DEFEMAB3))
    else
      SolaceViewVars('DEFEMAB3',Thread(),'FileStatus',Status(DEFEMAB3),0)
    end
    if Status(DEFEMAB4) then
      SolaceViewVars('DEFEMAB4',Thread(),'FileStatus',Status(DEFEMAB4),Records(DEFEMAB4))
    else
      SolaceViewVars('DEFEMAB4',Thread(),'FileStatus',Status(DEFEMAB4),0)
    end
    if Status(DEFEMAB5) then
      SolaceViewVars('DEFEMAB5',Thread(),'FileStatus',Status(DEFEMAB5),Records(DEFEMAB5))
    else
      SolaceViewVars('DEFEMAB5',Thread(),'FileStatus',Status(DEFEMAB5),0)
    end
    if Status(DEFEMAB6) then
      SolaceViewVars('DEFEMAB6',Thread(),'FileStatus',Status(DEFEMAB6),Records(DEFEMAB6))
    else
      SolaceViewVars('DEFEMAB6',Thread(),'FileStatus',Status(DEFEMAB6),0)
    end
    if Status(DEFEMAB7) then
      SolaceViewVars('DEFEMAB7',Thread(),'FileStatus',Status(DEFEMAB7),Records(DEFEMAB7))
    else
      SolaceViewVars('DEFEMAB7',Thread(),'FileStatus',Status(DEFEMAB7),0)
    end
    if Status(DEFEMAE2) then
      SolaceViewVars('DEFEMAE2',Thread(),'FileStatus',Status(DEFEMAE2),Records(DEFEMAE2))
    else
      SolaceViewVars('DEFEMAE2',Thread(),'FileStatus',Status(DEFEMAE2),0)
    end
    if Status(DEFEMAE3) then
      SolaceViewVars('DEFEMAE3',Thread(),'FileStatus',Status(DEFEMAE3),Records(DEFEMAE3))
    else
      SolaceViewVars('DEFEMAE3',Thread(),'FileStatus',Status(DEFEMAE3),0)
    end
    if Status(DEFEME2B) then
      SolaceViewVars('DEFEME2B',Thread(),'FileStatus',Status(DEFEME2B),Records(DEFEME2B))
    else
      SolaceViewVars('DEFEME2B',Thread(),'FileStatus',Status(DEFEME2B),0)
    end
    if Status(DEFEME3B) then
      SolaceViewVars('DEFEME3B',Thread(),'FileStatus',Status(DEFEME3B),Records(DEFEME3B))
    else
      SolaceViewVars('DEFEME3B',Thread(),'FileStatus',Status(DEFEME3B),0)
    end
    if Status(DEFEMAR2) then
      SolaceViewVars('DEFEMAR2',Thread(),'FileStatus',Status(DEFEMAR2),Records(DEFEMAR2))
    else
      SolaceViewVars('DEFEMAR2',Thread(),'FileStatus',Status(DEFEMAR2),0)
    end
    if Status(DEFEMAR3) then
      SolaceViewVars('DEFEMAR3',Thread(),'FileStatus',Status(DEFEMAR3),Records(DEFEMAR3))
    else
      SolaceViewVars('DEFEMAR3',Thread(),'FileStatus',Status(DEFEMAR3),0)
    end
    if Status(DEFEMAR4) then
      SolaceViewVars('DEFEMAR4',Thread(),'FileStatus',Status(DEFEMAR4),Records(DEFEMAR4))
    else
      SolaceViewVars('DEFEMAR4',Thread(),'FileStatus',Status(DEFEMAR4),0)
    end
    if Status(DEFEMAB1) then
      SolaceViewVars('DEFEMAB1',Thread(),'FileStatus',Status(DEFEMAB1),Records(DEFEMAB1))
    else
      SolaceViewVars('DEFEMAB1',Thread(),'FileStatus',Status(DEFEMAB1),0)
    end
    if Status(DEFEMAE1) then
      SolaceViewVars('DEFEMAE1',Thread(),'FileStatus',Status(DEFEMAE1),Records(DEFEMAE1))
    else
      SolaceViewVars('DEFEMAE1',Thread(),'FileStatus',Status(DEFEMAE1),0)
    end
    if Status(DEFEMAI2) then
      SolaceViewVars('DEFEMAI2',Thread(),'FileStatus',Status(DEFEMAI2),Records(DEFEMAI2))
    else
      SolaceViewVars('DEFEMAI2',Thread(),'FileStatus',Status(DEFEMAI2),0)
    end
    if Status(DEFSIDEX) then
      SolaceViewVars('DEFSIDEX',Thread(),'FileStatus',Status(DEFSIDEX),Records(DEFSIDEX))
    else
      SolaceViewVars('DEFSIDEX',Thread(),'FileStatus',Status(DEFSIDEX),0)
    end
    if Status(STOAUDIT) then
      SolaceViewVars('STOAUDIT',Thread(),'FileStatus',Status(STOAUDIT),Records(STOAUDIT))
    else
      SolaceViewVars('STOAUDIT',Thread(),'FileStatus',Status(STOAUDIT),0)
    end
    if Status(DEFAULT2) then
      SolaceViewVars('DEFAULT2',Thread(),'FileStatus',Status(DEFAULT2),Records(DEFAULT2))
    else
      SolaceViewVars('DEFAULT2',Thread(),'FileStatus',Status(DEFAULT2),0)
    end
    if Status(ACTION) then
      SolaceViewVars('ACTION',Thread(),'FileStatus',Status(ACTION),Records(ACTION))
    else
      SolaceViewVars('ACTION',Thread(),'FileStatus',Status(ACTION),0)
    end
    if Status(DEFRAPID) then
      SolaceViewVars('DEFRAPID',Thread(),'FileStatus',Status(DEFRAPID),Records(DEFRAPID))
    else
      SolaceViewVars('DEFRAPID',Thread(),'FileStatus',Status(DEFRAPID),0)
    end
    if Status(RETSALES) then
      SolaceViewVars('RETSALES',Thread(),'FileStatus',Status(RETSALES),Records(RETSALES))
    else
      SolaceViewVars('RETSALES',Thread(),'FileStatus',Status(RETSALES),0)
    end
    if Status(BOUNCER) then
      SolaceViewVars('BOUNCER',Thread(),'FileStatus',Status(BOUNCER),Records(BOUNCER))
    else
      SolaceViewVars('BOUNCER',Thread(),'FileStatus',Status(BOUNCER),0)
    end
    if Status(TEAMS) then
      SolaceViewVars('TEAMS',Thread(),'FileStatus',Status(TEAMS),Records(TEAMS))
    else
      SolaceViewVars('TEAMS',Thread(),'FileStatus',Status(TEAMS),0)
    end
    if Status(MERGE) then
      SolaceViewVars('MERGE',Thread(),'FileStatus',Status(MERGE),Records(MERGE))
    else
      SolaceViewVars('MERGE',Thread(),'FileStatus',Status(MERGE),0)
    end
    if Status(UPDDATA) then
      SolaceViewVars('UPDDATA',Thread(),'FileStatus',Status(UPDDATA),Records(UPDDATA))
    else
      SolaceViewVars('UPDDATA',Thread(),'FileStatus',Status(UPDDATA),0)
    end
    if Status(WEBDEFLT) then
      SolaceViewVars('WEBDEFLT',Thread(),'FileStatus',Status(WEBDEFLT),Records(WEBDEFLT))
    else
      SolaceViewVars('WEBDEFLT',Thread(),'FileStatus',Status(WEBDEFLT),0)
    end
    if Status(EXCAUDIT) then
      SolaceViewVars('EXCAUDIT',Thread(),'FileStatus',Status(EXCAUDIT),Records(EXCAUDIT))
    else
      SolaceViewVars('EXCAUDIT',Thread(),'FileStatus',Status(EXCAUDIT),0)
    end
    if Status(COMMONFA) then
      SolaceViewVars('COMMONFA',Thread(),'FileStatus',Status(COMMONFA),Records(COMMONFA))
    else
      SolaceViewVars('COMMONFA',Thread(),'FileStatus',Status(COMMONFA),0)
    end
    if Status(PARAMSS) then
      SolaceViewVars('PARAMSS',Thread(),'FileStatus',Status(PARAMSS),Records(PARAMSS))
    else
      SolaceViewVars('PARAMSS',Thread(),'FileStatus',Status(PARAMSS),0)
    end
    if Status(INSJOBS) then
      SolaceViewVars('INSJOBS',Thread(),'FileStatus',Status(INSJOBS),Records(INSJOBS))
    else
      SolaceViewVars('INSJOBS',Thread(),'FileStatus',Status(INSJOBS),0)
    end
    if Status(CONSIGN) then
      SolaceViewVars('CONSIGN',Thread(),'FileStatus',Status(CONSIGN),Records(CONSIGN))
    else
      SolaceViewVars('CONSIGN',Thread(),'FileStatus',Status(CONSIGN),0)
    end
    if Status(JOBEXACC) then
      SolaceViewVars('JOBEXACC',Thread(),'FileStatus',Status(JOBEXACC),Records(JOBEXACC))
    else
      SolaceViewVars('JOBEXACC',Thread(),'FileStatus',Status(JOBEXACC),0)
    end
    if Status(MODELCOL) then
      SolaceViewVars('MODELCOL',Thread(),'FileStatus',Status(MODELCOL),Records(MODELCOL))
    else
      SolaceViewVars('MODELCOL',Thread(),'FileStatus',Status(MODELCOL),0)
    end
    if Status(COMMCAT) then
      SolaceViewVars('COMMCAT',Thread(),'FileStatus',Status(COMMCAT),Records(COMMCAT))
    else
      SolaceViewVars('COMMCAT',Thread(),'FileStatus',Status(COMMCAT),0)
    end
    if Status(MESSAGES) then
      SolaceViewVars('MESSAGES',Thread(),'FileStatus',Status(MESSAGES),Records(MESSAGES))
    else
      SolaceViewVars('MESSAGES',Thread(),'FileStatus',Status(MESSAGES),0)
    end
    if Status(LOGEXHE) then
      SolaceViewVars('LOGEXHE',Thread(),'FileStatus',Status(LOGEXHE),Records(LOGEXHE))
    else
      SolaceViewVars('LOGEXHE',Thread(),'FileStatus',Status(LOGEXHE),0)
    end
    if Status(DEFCRC) then
      SolaceViewVars('DEFCRC',Thread(),'FileStatus',Status(DEFCRC),Records(DEFCRC))
    else
      SolaceViewVars('DEFCRC',Thread(),'FileStatus',Status(DEFCRC),0)
    end
    if Status(PAYTYPES) then
      SolaceViewVars('PAYTYPES',Thread(),'FileStatus',Status(PAYTYPES),Records(PAYTYPES))
    else
      SolaceViewVars('PAYTYPES',Thread(),'FileStatus',Status(PAYTYPES),0)
    end
    if Status(COMMONWP) then
      SolaceViewVars('COMMONWP',Thread(),'FileStatus',Status(COMMONWP),Records(COMMONWP))
    else
      SolaceViewVars('COMMONWP',Thread(),'FileStatus',Status(COMMONWP),0)
    end
    if Status(LOGEXCH) then
      SolaceViewVars('LOGEXCH',Thread(),'FileStatus',Status(LOGEXCH),Records(LOGEXCH))
    else
      SolaceViewVars('LOGEXCH',Thread(),'FileStatus',Status(LOGEXCH),0)
    end
    if Status(QAREASON) then
      SolaceViewVars('QAREASON',Thread(),'FileStatus',Status(QAREASON),Records(QAREASON))
    else
      SolaceViewVars('QAREASON',Thread(),'FileStatus',Status(QAREASON),0)
    end
    if Status(COMMONCP) then
      SolaceViewVars('COMMONCP',Thread(),'FileStatus',Status(COMMONCP),Records(COMMONCP))
    else
      SolaceViewVars('COMMONCP',Thread(),'FileStatus',Status(COMMONCP),0)
    end
    if Status(ADDSEARCH) then
      SolaceViewVars('ADDSEARCH',Thread(),'FileStatus',Status(ADDSEARCH),Records(ADDSEARCH))
    else
      SolaceViewVars('ADDSEARCH',Thread(),'FileStatus',Status(ADDSEARCH),0)
    end
    if Status(SIDDEF) then
      SolaceViewVars('SIDDEF',Thread(),'FileStatus',Status(SIDDEF),Records(SIDDEF))
    else
      SolaceViewVars('SIDDEF',Thread(),'FileStatus',Status(SIDDEF),0)
    end
    if Status(CONTHIST) then
      SolaceViewVars('CONTHIST',Thread(),'FileStatus',Status(CONTHIST),Records(CONTHIST))
    else
      SolaceViewVars('CONTHIST',Thread(),'FileStatus',Status(CONTHIST),0)
    end
    if Status(CONTACTS) then
      SolaceViewVars('CONTACTS',Thread(),'FileStatus',Status(CONTACTS),Records(CONTACTS))
    else
      SolaceViewVars('CONTACTS',Thread(),'FileStatus',Status(CONTACTS),0)
    end
    if Status(JOBBATCH) then
      SolaceViewVars('JOBBATCH',Thread(),'FileStatus',Status(JOBBATCH),Records(JOBBATCH))
    else
      SolaceViewVars('JOBBATCH',Thread(),'FileStatus',Status(JOBBATCH),0)
    end
    if Status(DEFEDI2) then
      SolaceViewVars('DEFEDI2',Thread(),'FileStatus',Status(DEFEDI2),Records(DEFEDI2))
    else
      SolaceViewVars('DEFEDI2',Thread(),'FileStatus',Status(DEFEDI2),0)
    end
    if Status(DEFPRINT) then
      SolaceViewVars('DEFPRINT',Thread(),'FileStatus',Status(DEFPRINT),Records(DEFPRINT))
    else
      SolaceViewVars('DEFPRINT',Thread(),'FileStatus',Status(DEFPRINT),0)
    end
    if Status(DEFWEB) then
      SolaceViewVars('DEFWEB',Thread(),'FileStatus',Status(DEFWEB),Records(DEFWEB))
    else
      SolaceViewVars('DEFWEB',Thread(),'FileStatus',Status(DEFWEB),0)
    end
    if Status(EXPCITY) then
      SolaceViewVars('EXPCITY',Thread(),'FileStatus',Status(EXPCITY),Records(EXPCITY))
    else
      SolaceViewVars('EXPCITY',Thread(),'FileStatus',Status(EXPCITY),0)
    end
    if Status(PROCCODE) then
      SolaceViewVars('PROCCODE',Thread(),'FileStatus',Status(PROCCODE),Records(PROCCODE))
    else
      SolaceViewVars('PROCCODE',Thread(),'FileStatus',Status(PROCCODE),0)
    end
    if Status(COLOUR) then
      SolaceViewVars('COLOUR',Thread(),'FileStatus',Status(COLOUR),Records(COLOUR))
    else
      SolaceViewVars('COLOUR',Thread(),'FileStatus',Status(COLOUR),0)
    end
    if Status(VODAIMP) then
      SolaceViewVars('VODAIMP',Thread(),'FileStatus',Status(VODAIMP),Records(VODAIMP))
    else
      SolaceViewVars('VODAIMP',Thread(),'FileStatus',Status(VODAIMP),0)
    end
    if Status(JOBSVODA) then
      SolaceViewVars('JOBSVODA',Thread(),'FileStatus',Status(JOBSVODA),Records(JOBSVODA))
    else
      SolaceViewVars('JOBSVODA',Thread(),'FileStatus',Status(JOBSVODA),0)
    end
    if Status(ACCESDEF) then
      SolaceViewVars('ACCESDEF',Thread(),'FileStatus',Status(ACCESDEF),Records(ACCESDEF))
    else
      SolaceViewVars('ACCESDEF',Thread(),'FileStatus',Status(ACCESDEF),0)
    end
    if Status(STANTEXT) then
      SolaceViewVars('STANTEXT',Thread(),'FileStatus',Status(STANTEXT),Records(STANTEXT))
    else
      SolaceViewVars('STANTEXT',Thread(),'FileStatus',Status(STANTEXT),0)
    end
    if Status(NOTESENG) then
      SolaceViewVars('NOTESENG',Thread(),'FileStatus',Status(NOTESENG),Records(NOTESENG))
    else
      SolaceViewVars('NOTESENG',Thread(),'FileStatus',Status(NOTESENG),0)
    end
    if Status(IAUDIT) then
      SolaceViewVars('IAUDIT',Thread(),'FileStatus',Status(IAUDIT),Records(IAUDIT))
    else
      SolaceViewVars('IAUDIT',Thread(),'FileStatus',Status(IAUDIT),0)
    end
    if Status(IINVJOB) then
      SolaceViewVars('IINVJOB',Thread(),'FileStatus',Status(IINVJOB),Records(IINVJOB))
    else
      SolaceViewVars('IINVJOB',Thread(),'FileStatus',Status(IINVJOB),0)
    end
    if Status(INSDEF) then
      SolaceViewVars('INSDEF',Thread(),'FileStatus',Status(INSDEF),Records(INSDEF))
    else
      SolaceViewVars('INSDEF',Thread(),'FileStatus',Status(INSDEF),0)
    end
    if Status(IEXPUSE) then
      SolaceViewVars('IEXPUSE',Thread(),'FileStatus',Status(IEXPUSE),Records(IEXPUSE))
    else
      SolaceViewVars('IEXPUSE',Thread(),'FileStatus',Status(IEXPUSE),0)
    end
    if Status(IRECEIVE) then
      SolaceViewVars('IRECEIVE',Thread(),'FileStatus',Status(IRECEIVE),Records(IRECEIVE))
    else
      SolaceViewVars('IRECEIVE',Thread(),'FileStatus',Status(IRECEIVE),0)
    end
    if Status(IJOBSTATUS) then
      SolaceViewVars('IJOBSTATUS',Thread(),'FileStatus',Status(IJOBSTATUS),Records(IJOBSTATUS))
    else
      SolaceViewVars('IJOBSTATUS',Thread(),'FileStatus',Status(IJOBSTATUS),0)
    end
    if Status(ICLCOST) then
      SolaceViewVars('ICLCOST',Thread(),'FileStatus',Status(ICLCOST),Records(ICLCOST))
    else
      SolaceViewVars('ICLCOST',Thread(),'FileStatus',Status(ICLCOST),0)
    end
    if Status(IVEHDETS) then
      SolaceViewVars('IVEHDETS',Thread(),'FileStatus',Status(IVEHDETS),Records(IVEHDETS))
    else
      SolaceViewVars('IVEHDETS',Thread(),'FileStatus',Status(IVEHDETS),0)
    end
    if Status(IAPPENG) then
      SolaceViewVars('IAPPENG',Thread(),'FileStatus',Status(IAPPENG),Records(IAPPENG))
    else
      SolaceViewVars('IAPPENG',Thread(),'FileStatus',Status(IAPPENG),0)
    end
    if Status(IAPPTYPE) then
      SolaceViewVars('IAPPTYPE',Thread(),'FileStatus',Status(IAPPTYPE),Records(IAPPTYPE))
    else
      SolaceViewVars('IAPPTYPE',Thread(),'FileStatus',Status(IAPPTYPE),0)
    end
    if Status(INSMAKE) then
      SolaceViewVars('INSMAKE',Thread(),'FileStatus',Status(INSMAKE),Records(INSMAKE))
    else
      SolaceViewVars('INSMAKE',Thread(),'FileStatus',Status(INSMAKE),0)
    end
    if Status(INSAPPS) then
      SolaceViewVars('INSAPPS',Thread(),'FileStatus',Status(INSAPPS),Records(INSAPPS))
    else
      SolaceViewVars('INSAPPS',Thread(),'FileStatus',Status(INSAPPS),0)
    end
    if Status(ISUBCONT) then
      SolaceViewVars('ISUBCONT',Thread(),'FileStatus',Status(ISUBCONT),Records(ISUBCONT))
    else
      SolaceViewVars('ISUBCONT',Thread(),'FileStatus',Status(ISUBCONT),0)
    end
    if Status(TRDSPEC) then
      SolaceViewVars('TRDSPEC',Thread(),'FileStatus',Status(TRDSPEC),Records(TRDSPEC))
    else
      SolaceViewVars('TRDSPEC',Thread(),'FileStatus',Status(TRDSPEC),0)
    end
    if Status(ITIMES) then
      SolaceViewVars('ITIMES',Thread(),'FileStatus',Status(ITIMES),Records(ITIMES))
    else
      SolaceViewVars('ITIMES',Thread(),'FileStatus',Status(ITIMES),0)
    end
    if Status(IEQUIP) then
      SolaceViewVars('IEQUIP',Thread(),'FileStatus',Status(IEQUIP),Records(IEQUIP))
    else
      SolaceViewVars('IEQUIP',Thread(),'FileStatus',Status(IEQUIP),0)
    end
    if Status(IENGINEERS) then
      SolaceViewVars('IENGINEERS',Thread(),'FileStatus',Status(IENGINEERS),Records(IENGINEERS))
    else
      SolaceViewVars('IENGINEERS',Thread(),'FileStatus',Status(IENGINEERS),0)
    end
    if Status(ICLIENTS) then
      SolaceViewVars('ICLIENTS',Thread(),'FileStatus',Status(ICLIENTS),Records(ICLIENTS))
    else
      SolaceViewVars('ICLIENTS',Thread(),'FileStatus',Status(ICLIENTS),0)
    end
    if Status(ISUBPOST) then
      SolaceViewVars('ISUBPOST',Thread(),'FileStatus',Status(ISUBPOST),Records(ISUBPOST))
    else
      SolaceViewVars('ISUBPOST',Thread(),'FileStatus',Status(ISUBPOST),0)
    end
    if Status(IJOBTYPE) then
      SolaceViewVars('IJOBTYPE',Thread(),'FileStatus',Status(IJOBTYPE),Records(IJOBTYPE))
    else
      SolaceViewVars('IJOBTYPE',Thread(),'FileStatus',Status(IJOBTYPE),0)
    end
    if Status(EDIBATCH) then
      SolaceViewVars('EDIBATCH',Thread(),'FileStatus',Status(EDIBATCH),Records(EDIBATCH))
    else
      SolaceViewVars('EDIBATCH',Thread(),'FileStatus',Status(EDIBATCH),0)
    end
    if Status(ORDPEND) then
      SolaceViewVars('ORDPEND',Thread(),'FileStatus',Status(ORDPEND),Records(ORDPEND))
    else
      SolaceViewVars('ORDPEND',Thread(),'FileStatus',Status(ORDPEND),0)
    end
    if Status(STOHIST) then
      SolaceViewVars('STOHIST',Thread(),'FileStatus',Status(STOHIST),Records(STOHIST))
    else
      SolaceViewVars('STOHIST',Thread(),'FileStatus',Status(STOHIST),0)
    end
    if Status(NEWFEAT) then
      SolaceViewVars('NEWFEAT',Thread(),'FileStatus',Status(NEWFEAT),Records(NEWFEAT))
    else
      SolaceViewVars('NEWFEAT',Thread(),'FileStatus',Status(NEWFEAT),0)
    end
    if Status(EXREASON) then
      SolaceViewVars('EXREASON',Thread(),'FileStatus',Status(EXREASON),Records(EXREASON))
    else
      SolaceViewVars('EXREASON',Thread(),'FileStatus',Status(EXREASON),0)
    end
    if Status(REPTYDEF) then
      SolaceViewVars('REPTYDEF',Thread(),'FileStatus',Status(REPTYDEF),Records(REPTYDEF))
    else
      SolaceViewVars('REPTYDEF',Thread(),'FileStatus',Status(REPTYDEF),0)
    end
    if Status(RETACCOUNTSLIST) then
      SolaceViewVars('RETACCOUNTSLIST',Thread(),'FileStatus',Status(RETACCOUNTSLIST),Records(RETACCOUNTSLIST))
    else
      SolaceViewVars('RETACCOUNTSLIST',Thread(),'FileStatus',Status(RETACCOUNTSLIST),0)
    end
    if Status(PRIORITY) then
      SolaceViewVars('PRIORITY',Thread(),'FileStatus',Status(PRIORITY),Records(PRIORITY))
    else
      SolaceViewVars('PRIORITY',Thread(),'FileStatus',Status(PRIORITY),0)
    end
    if Status(DEFEPS) then
      SolaceViewVars('DEFEPS',Thread(),'FileStatus',Status(DEFEPS),Records(DEFEPS))
    else
      SolaceViewVars('DEFEPS',Thread(),'FileStatus',Status(DEFEPS),0)
    end
    if Status(JOBSE) then
      SolaceViewVars('JOBSE',Thread(),'FileStatus',Status(JOBSE),Records(JOBSE))
    else
      SolaceViewVars('JOBSE',Thread(),'FileStatus',Status(JOBSE),0)
    end
    if Status(REPEXTTP) then
      SolaceViewVars('REPEXTTP',Thread(),'FileStatus',Status(REPEXTTP),Records(REPEXTTP))
    else
      SolaceViewVars('REPEXTTP',Thread(),'FileStatus',Status(REPEXTTP),0)
    end
    if Status(MANFAULT) then
      SolaceViewVars('MANFAULT',Thread(),'FileStatus',Status(MANFAULT),Records(MANFAULT))
    else
      SolaceViewVars('MANFAULT',Thread(),'FileStatus',Status(MANFAULT),0)
    end
    if Status(INVPARTS) then
      SolaceViewVars('INVPARTS',Thread(),'FileStatus',Status(INVPARTS),Records(INVPARTS))
    else
      SolaceViewVars('INVPARTS',Thread(),'FileStatus',Status(INVPARTS),0)
    end
    if Status(JOBSEARC) then
      SolaceViewVars('JOBSEARC',Thread(),'FileStatus',Status(JOBSEARC),Records(JOBSEARC))
    else
      SolaceViewVars('JOBSEARC',Thread(),'FileStatus',Status(JOBSEARC),0)
    end
    if Status(INVPATMP) then
      SolaceViewVars('INVPATMP',Thread(),'FileStatus',Status(INVPATMP),Records(INVPATMP))
    else
      SolaceViewVars('INVPATMP',Thread(),'FileStatus',Status(INVPATMP),0)
    end
    if Status(TRACHAR) then
      SolaceViewVars('TRACHAR',Thread(),'FileStatus',Status(TRACHAR),Records(TRACHAR))
    else
      SolaceViewVars('TRACHAR',Thread(),'FileStatus',Status(TRACHAR),0)
    end
    if Status(CITYSERV) then
      SolaceViewVars('CITYSERV',Thread(),'FileStatus',Status(CITYSERV),Records(CITYSERV))
    else
      SolaceViewVars('CITYSERV',Thread(),'FileStatus',Status(CITYSERV),0)
    end
    if Status(PICKNOTE) then
      SolaceViewVars('PICKNOTE',Thread(),'FileStatus',Status(PICKNOTE),Records(PICKNOTE))
    else
      SolaceViewVars('PICKNOTE',Thread(),'FileStatus',Status(PICKNOTE),0)
    end
    if Status(DISCOUNT) then
      SolaceViewVars('DISCOUNT',Thread(),'FileStatus',Status(DISCOUNT),Records(DISCOUNT))
    else
      SolaceViewVars('DISCOUNT',Thread(),'FileStatus',Status(DISCOUNT),0)
    end
    if Status(LOCVALUE) then
      SolaceViewVars('LOCVALUE',Thread(),'FileStatus',Status(LOCVALUE),Records(LOCVALUE))
    else
      SolaceViewVars('LOCVALUE',Thread(),'FileStatus',Status(LOCVALUE),0)
    end
    if Status(STOCKTYP) then
      SolaceViewVars('STOCKTYP',Thread(),'FileStatus',Status(STOCKTYP),Records(STOCKTYP))
    else
      SolaceViewVars('STOCKTYP',Thread(),'FileStatus',Status(STOCKTYP),0)
    end
    if Status(IEMAILS) then
      SolaceViewVars('IEMAILS',Thread(),'FileStatus',Status(IEMAILS),Records(IEMAILS))
    else
      SolaceViewVars('IEMAILS',Thread(),'FileStatus',Status(IEMAILS),0)
    end
    if Status(COURIER) then
      SolaceViewVars('COURIER',Thread(),'FileStatus',Status(COURIER),Records(COURIER))
    else
      SolaceViewVars('COURIER',Thread(),'FileStatus',Status(COURIER),0)
    end
    if Status(TRDPARTY) then
      SolaceViewVars('TRDPARTY',Thread(),'FileStatus',Status(TRDPARTY),Records(TRDPARTY))
    else
      SolaceViewVars('TRDPARTY',Thread(),'FileStatus',Status(TRDPARTY),0)
    end
    if Status(JOBSTMP) then
      SolaceViewVars('JOBSTMP',Thread(),'FileStatus',Status(JOBSTMP),Records(JOBSTMP))
    else
      SolaceViewVars('JOBSTMP',Thread(),'FileStatus',Status(JOBSTMP),0)
    end
    if Status(LOGALLOC) then
      SolaceViewVars('LOGALLOC',Thread(),'FileStatus',Status(LOGALLOC),Records(LOGALLOC))
    else
      SolaceViewVars('LOGALLOC',Thread(),'FileStatus',Status(LOGALLOC),0)
    end
    if Status(JOBNOTES) then
      SolaceViewVars('JOBNOTES',Thread(),'FileStatus',Status(JOBNOTES),Records(JOBNOTES))
    else
      SolaceViewVars('JOBNOTES',Thread(),'FileStatus',Status(JOBNOTES),0)
    end
    if Status(PROINV) then
      SolaceViewVars('PROINV',Thread(),'FileStatus',Status(PROINV),Records(PROINV))
    else
      SolaceViewVars('PROINV',Thread(),'FileStatus',Status(PROINV),0)
    end
    if Status(JOBACTMP) then
      SolaceViewVars('JOBACTMP',Thread(),'FileStatus',Status(JOBACTMP),Records(JOBACTMP))
    else
      SolaceViewVars('JOBACTMP',Thread(),'FileStatus',Status(JOBACTMP),0)
    end
    if Status(RETPARTSLIST) then
      SolaceViewVars('RETPARTSLIST',Thread(),'FileStatus',Status(RETPARTSLIST),Records(RETPARTSLIST))
    else
      SolaceViewVars('RETPARTSLIST',Thread(),'FileStatus',Status(RETPARTSLIST),0)
    end
    if Status(TRANTYPE) then
      SolaceViewVars('TRANTYPE',Thread(),'FileStatus',Status(TRANTYPE),Records(TRANTYPE))
    else
      SolaceViewVars('TRANTYPE',Thread(),'FileStatus',Status(TRANTYPE),0)
    end
    if Status(DESBATCH) then
      SolaceViewVars('DESBATCH',Thread(),'FileStatus',Status(DESBATCH),Records(DESBATCH))
    else
      SolaceViewVars('DESBATCH',Thread(),'FileStatus',Status(DESBATCH),0)
    end
    if Status(JOBVODAC) then
      SolaceViewVars('JOBVODAC',Thread(),'FileStatus',Status(JOBVODAC),Records(JOBVODAC))
    else
      SolaceViewVars('JOBVODAC',Thread(),'FileStatus',Status(JOBVODAC),0)
    end
    if Status(ESNMODEL) then
      SolaceViewVars('ESNMODEL',Thread(),'FileStatus',Status(ESNMODEL),Records(ESNMODEL))
    else
      SolaceViewVars('ESNMODEL',Thread(),'FileStatus',Status(ESNMODEL),0)
    end
    if Status(JOBPAYMT) then
      SolaceViewVars('JOBPAYMT',Thread(),'FileStatus',Status(JOBPAYMT),Records(JOBPAYMT))
    else
      SolaceViewVars('JOBPAYMT',Thread(),'FileStatus',Status(JOBPAYMT),0)
    end
    if Status(LOGASSSTTEMP) then
      SolaceViewVars('LOGASSSTTEMP',Thread(),'FileStatus',Status(LOGASSSTTEMP),Records(LOGASSSTTEMP))
    else
      SolaceViewVars('LOGASSSTTEMP',Thread(),'FileStatus',Status(LOGASSSTTEMP),0)
    end
    if Status(STOCK) then
      SolaceViewVars('STOCK',Thread(),'FileStatus',Status(STOCK),Records(STOCK))
    else
      SolaceViewVars('STOCK',Thread(),'FileStatus',Status(STOCK),0)
    end
    if Status(LOGGED) then
      SolaceViewVars('LOGGED',Thread(),'FileStatus',Status(LOGGED),Records(LOGGED))
    else
      SolaceViewVars('LOGGED',Thread(),'FileStatus',Status(LOGGED),0)
    end
    if Status(LOGRTHIS) then
      SolaceViewVars('LOGRTHIS',Thread(),'FileStatus',Status(LOGRTHIS),Records(LOGRTHIS))
    else
      SolaceViewVars('LOGRTHIS',Thread(),'FileStatus',Status(LOGRTHIS),0)
    end
    if Status(LOGSTHIS) then
      SolaceViewVars('LOGSTHIS',Thread(),'FileStatus',Status(LOGSTHIS),Records(LOGSTHIS))
    else
      SolaceViewVars('LOGSTHIS',Thread(),'FileStatus',Status(LOGSTHIS),0)
    end
    if Status(LOGASSST) then
      SolaceViewVars('LOGASSST',Thread(),'FileStatus',Status(LOGASSST),Records(LOGASSST))
    else
      SolaceViewVars('LOGASSST',Thread(),'FileStatus',Status(LOGASSST),0)
    end
    if Status(LOGSTOLC) then
      SolaceViewVars('LOGSTOLC',Thread(),'FileStatus',Status(LOGSTOLC),Records(LOGSTOLC))
    else
      SolaceViewVars('LOGSTOLC',Thread(),'FileStatus',Status(LOGSTOLC),0)
    end
    if Status(LOGTEMP) then
      SolaceViewVars('LOGTEMP',Thread(),'FileStatus',Status(LOGTEMP),Records(LOGTEMP))
    else
      SolaceViewVars('LOGTEMP',Thread(),'FileStatus',Status(LOGTEMP),0)
    end
    if Status(LOGSERST) then
      SolaceViewVars('LOGSERST',Thread(),'FileStatus',Status(LOGSERST),Records(LOGSERST))
    else
      SolaceViewVars('LOGSERST',Thread(),'FileStatus',Status(LOGSERST),0)
    end
    if Status(LOGDEFLT) then
      SolaceViewVars('LOGDEFLT',Thread(),'FileStatus',Status(LOGDEFLT),Records(LOGDEFLT))
    else
      SolaceViewVars('LOGDEFLT',Thread(),'FileStatus',Status(LOGDEFLT),0)
    end
    if Status(LOGSTLOC) then
      SolaceViewVars('LOGSTLOC',Thread(),'FileStatus',Status(LOGSTLOC),Records(LOGSTLOC))
    else
      SolaceViewVars('LOGSTLOC',Thread(),'FileStatus',Status(LOGSTLOC),0)
    end
    if Status(MULTIDEF) then
      SolaceViewVars('MULTIDEF',Thread(),'FileStatus',Status(MULTIDEF),Records(MULTIDEF))
    else
      SolaceViewVars('MULTIDEF',Thread(),'FileStatus',Status(MULTIDEF),0)
    end
    if Status(TRAFAULO) then
      SolaceViewVars('TRAFAULO',Thread(),'FileStatus',Status(TRAFAULO),Records(TRAFAULO))
    else
      SolaceViewVars('TRAFAULO',Thread(),'FileStatus',Status(TRAFAULO),0)
    end
    if Status(MPXJOBS) then
      SolaceViewVars('MPXJOBS',Thread(),'FileStatus',Status(MPXJOBS),Records(MPXJOBS))
    else
      SolaceViewVars('MPXJOBS',Thread(),'FileStatus',Status(MPXJOBS),0)
    end
    if Status(MPXSTAT) then
      SolaceViewVars('MPXSTAT',Thread(),'FileStatus',Status(MPXSTAT),Records(MPXSTAT))
    else
      SolaceViewVars('MPXSTAT',Thread(),'FileStatus',Status(MPXSTAT),0)
    end
    if Status(TRAFAULT) then
      SolaceViewVars('TRAFAULT',Thread(),'FileStatus',Status(TRAFAULT),Records(TRAFAULT))
    else
      SolaceViewVars('TRAFAULT',Thread(),'FileStatus',Status(TRAFAULT),0)
    end
    if Status(EXMINLEV) then
      SolaceViewVars('EXMINLEV',Thread(),'FileStatus',Status(EXMINLEV),Records(EXMINLEV))
    else
      SolaceViewVars('EXMINLEV',Thread(),'FileStatus',Status(EXMINLEV),0)
    end
    if Status(LOGSTHII) then
      SolaceViewVars('LOGSTHII',Thread(),'FileStatus',Status(LOGSTHII),Records(LOGSTHII))
    else
      SolaceViewVars('LOGSTHII',Thread(),'FileStatus',Status(LOGSTHII),0)
    end
    if Status(LOGCLSTE) then
      SolaceViewVars('LOGCLSTE',Thread(),'FileStatus',Status(LOGCLSTE),Records(LOGCLSTE))
    else
      SolaceViewVars('LOGCLSTE',Thread(),'FileStatus',Status(LOGCLSTE),0)
    end
    if Status(STATCRIT) then
      SolaceViewVars('STATCRIT',Thread(),'FileStatus',Status(STATCRIT),Records(STATCRIT))
    else
      SolaceViewVars('STATCRIT',Thread(),'FileStatus',Status(STATCRIT),0)
    end
    if Status(NETWORKS) then
      SolaceViewVars('NETWORKS',Thread(),'FileStatus',Status(NETWORKS),Records(NETWORKS))
    else
      SolaceViewVars('NETWORKS',Thread(),'FileStatus',Status(NETWORKS),0)
    end
    if Status(LOGDESNO) then
      SolaceViewVars('LOGDESNO',Thread(),'FileStatus',Status(LOGDESNO),Records(LOGDESNO))
    else
      SolaceViewVars('LOGDESNO',Thread(),'FileStatus',Status(LOGDESNO),0)
    end
    if Status(CPNDPRTS) then
      SolaceViewVars('CPNDPRTS',Thread(),'FileStatus',Status(CPNDPRTS),Records(CPNDPRTS))
    else
      SolaceViewVars('CPNDPRTS',Thread(),'FileStatus',Status(CPNDPRTS),0)
    end
    if Status(LOGSALCD) then
      SolaceViewVars('LOGSALCD',Thread(),'FileStatus',Status(LOGSALCD),Records(LOGSALCD))
    else
      SolaceViewVars('LOGSALCD',Thread(),'FileStatus',Status(LOGSALCD),0)
    end
    if Status(DEFSTOCK) then
      SolaceViewVars('DEFSTOCK',Thread(),'FileStatus',Status(DEFSTOCK),Records(DEFSTOCK))
    else
      SolaceViewVars('DEFSTOCK',Thread(),'FileStatus',Status(DEFSTOCK),0)
    end
    if Status(JOBLOHIS) then
      SolaceViewVars('JOBLOHIS',Thread(),'FileStatus',Status(JOBLOHIS),Records(JOBLOHIS))
    else
      SolaceViewVars('JOBLOHIS',Thread(),'FileStatus',Status(JOBLOHIS),0)
    end
    if Status(LOAN) then
      SolaceViewVars('LOAN',Thread(),'FileStatus',Status(LOAN),Records(LOAN))
    else
      SolaceViewVars('LOAN',Thread(),'FileStatus',Status(LOAN),0)
    end
    if Status(NOTESCON) then
      SolaceViewVars('NOTESCON',Thread(),'FileStatus',Status(NOTESCON),Records(NOTESCON))
    else
      SolaceViewVars('NOTESCON',Thread(),'FileStatus',Status(NOTESCON),0)
    end
    if Status(VATCODE) then
      SolaceViewVars('VATCODE',Thread(),'FileStatus',Status(VATCODE),Records(VATCODE))
    else
      SolaceViewVars('VATCODE',Thread(),'FileStatus',Status(VATCODE),0)
    end
    if Status(LOANHIST) then
      SolaceViewVars('LOANHIST',Thread(),'FileStatus',Status(LOANHIST),Records(LOANHIST))
    else
      SolaceViewVars('LOANHIST',Thread(),'FileStatus',Status(LOANHIST),0)
    end
    if Status(TURNARND) then
      SolaceViewVars('TURNARND',Thread(),'FileStatus',Status(TURNARND),Records(TURNARND))
    else
      SolaceViewVars('TURNARND',Thread(),'FileStatus',Status(TURNARND),0)
    end
    if Status(ORDJOBS) then
      SolaceViewVars('ORDJOBS',Thread(),'FileStatus',Status(ORDJOBS),Records(ORDJOBS))
    else
      SolaceViewVars('ORDJOBS',Thread(),'FileStatus',Status(ORDJOBS),0)
    end
    if Status(MERGETXT) then
      SolaceViewVars('MERGETXT',Thread(),'FileStatus',Status(MERGETXT),Records(MERGETXT))
    else
      SolaceViewVars('MERGETXT',Thread(),'FileStatus',Status(MERGETXT),0)
    end
    if Status(MERGELET) then
      SolaceViewVars('MERGELET',Thread(),'FileStatus',Status(MERGELET),Records(MERGELET))
    else
      SolaceViewVars('MERGELET',Thread(),'FileStatus',Status(MERGELET),0)
    end
    if Status(ORDSTOCK) then
      SolaceViewVars('ORDSTOCK',Thread(),'FileStatus',Status(ORDSTOCK),Records(ORDSTOCK))
    else
      SolaceViewVars('ORDSTOCK',Thread(),'FileStatus',Status(ORDSTOCK),0)
    end
    if Status(EXCHHIST) then
      SolaceViewVars('EXCHHIST',Thread(),'FileStatus',Status(EXCHHIST),Records(EXCHHIST))
    else
      SolaceViewVars('EXCHHIST',Thread(),'FileStatus',Status(EXCHHIST),0)
    end
    if Status(ACCESSOR) then
      SolaceViewVars('ACCESSOR',Thread(),'FileStatus',Status(ACCESSOR),Records(ACCESSOR))
    else
      SolaceViewVars('ACCESSOR',Thread(),'FileStatus',Status(ACCESSOR),0)
    end
    if Status(SUBACCAD) then
      SolaceViewVars('SUBACCAD',Thread(),'FileStatus',Status(SUBACCAD),Records(SUBACCAD))
    else
      SolaceViewVars('SUBACCAD',Thread(),'FileStatus',Status(SUBACCAD),0)
    end
    if Status(NOTESDEL) then
      SolaceViewVars('NOTESDEL',Thread(),'FileStatus',Status(NOTESDEL),Records(NOTESDEL))
    else
      SolaceViewVars('NOTESDEL',Thread(),'FileStatus',Status(NOTESDEL),0)
    end
    if Status(PICKDET) then
      SolaceViewVars('PICKDET',Thread(),'FileStatus',Status(PICKDET),Records(PICKDET))
    else
      SolaceViewVars('PICKDET',Thread(),'FileStatus',Status(PICKDET),0)
    end
    if Status(WARPARTS) then
      SolaceViewVars('WARPARTS',Thread(),'FileStatus',Status(WARPARTS),Records(WARPARTS))
    else
      SolaceViewVars('WARPARTS',Thread(),'FileStatus',Status(WARPARTS),0)
    end
    if Status(PARTS) then
      SolaceViewVars('PARTS',Thread(),'FileStatus',Status(PARTS),Records(PARTS))
    else
      SolaceViewVars('PARTS',Thread(),'FileStatus',Status(PARTS),0)
    end
    if Status(JOBACC) then
      SolaceViewVars('JOBACC',Thread(),'FileStatus',Status(JOBACC),Records(JOBACC))
    else
      SolaceViewVars('JOBACC',Thread(),'FileStatus',Status(JOBACC),0)
    end
    if Status(USELEVEL) then
      SolaceViewVars('USELEVEL',Thread(),'FileStatus',Status(USELEVEL),Records(USELEVEL))
    else
      SolaceViewVars('USELEVEL',Thread(),'FileStatus',Status(USELEVEL),0)
    end
    if Status(ALLLEVEL) then
      SolaceViewVars('ALLLEVEL',Thread(),'FileStatus',Status(ALLLEVEL),Records(ALLLEVEL))
    else
      SolaceViewVars('ALLLEVEL',Thread(),'FileStatus',Status(ALLLEVEL),0)
    end
    if Status(QAPARTSTEMP) then
      SolaceViewVars('QAPARTSTEMP',Thread(),'FileStatus',Status(QAPARTSTEMP),Records(QAPARTSTEMP))
    else
      SolaceViewVars('QAPARTSTEMP',Thread(),'FileStatus',Status(QAPARTSTEMP),0)
    end
    if Status(RETAILER) then
      SolaceViewVars('RETAILER',Thread(),'FileStatus',Status(RETAILER),Records(RETAILER))
    else
      SolaceViewVars('RETAILER',Thread(),'FileStatus',Status(RETAILER),0)
    end
    if Status(ACCAREAS) then
      SolaceViewVars('ACCAREAS',Thread(),'FileStatus',Status(ACCAREAS),Records(ACCAREAS))
    else
      SolaceViewVars('ACCAREAS',Thread(),'FileStatus',Status(ACCAREAS),0)
    end
    if Status(AUDIT) then
      SolaceViewVars('AUDIT',Thread(),'FileStatus',Status(AUDIT),Records(AUDIT))
    else
      SolaceViewVars('AUDIT',Thread(),'FileStatus',Status(AUDIT),0)
    end
    if Status(USERS) then
      SolaceViewVars('USERS',Thread(),'FileStatus',Status(USERS),Records(USERS))
    else
      SolaceViewVars('USERS',Thread(),'FileStatus',Status(USERS),0)
    end
    if Status(SIDREG) then
      SolaceViewVars('SIDREG',Thread(),'FileStatus',Status(SIDREG),Records(SIDREG))
    else
      SolaceViewVars('SIDREG',Thread(),'FileStatus',Status(SIDREG),0)
    end
    if Status(LOCSHELF) then
      SolaceViewVars('LOCSHELF',Thread(),'FileStatus',Status(LOCSHELF),Records(LOCSHELF))
    else
      SolaceViewVars('LOCSHELF',Thread(),'FileStatus',Status(LOCSHELF),0)
    end
    if Status(PENDMAIL) then
      SolaceViewVars('PENDMAIL',Thread(),'FileStatus',Status(PENDMAIL),Records(PENDMAIL))
    else
      SolaceViewVars('PENDMAIL',Thread(),'FileStatus',Status(PENDMAIL),0)
    end
    if Status(DEFAULTS) then
      SolaceViewVars('DEFAULTS',Thread(),'FileStatus',Status(DEFAULTS),Records(DEFAULTS))
    else
      SolaceViewVars('DEFAULTS',Thread(),'FileStatus',Status(DEFAULTS),0)
    end
    if Status(REPTYCAT) then
      SolaceViewVars('REPTYCAT',Thread(),'FileStatus',Status(REPTYCAT),Records(REPTYCAT))
    else
      SolaceViewVars('REPTYCAT',Thread(),'FileStatus',Status(REPTYCAT),0)
    end
    if Status(REPTYPETEMP) then
      SolaceViewVars('REPTYPETEMP',Thread(),'FileStatus',Status(REPTYPETEMP),Records(REPTYPETEMP))
    else
      SolaceViewVars('REPTYPETEMP',Thread(),'FileStatus',Status(REPTYPETEMP),0)
    end
    if Status(MANFPALO) then
      SolaceViewVars('MANFPALO',Thread(),'FileStatus',Status(MANFPALO),Records(MANFPALO))
    else
      SolaceViewVars('MANFPALO',Thread(),'FileStatus',Status(MANFPALO),0)
    end
    if Status(JOBSTAGE) then
      SolaceViewVars('JOBSTAGE',Thread(),'FileStatus',Status(JOBSTAGE),Records(JOBSTAGE))
    else
      SolaceViewVars('JOBSTAGE',Thread(),'FileStatus',Status(JOBSTAGE),0)
    end
    if Status(MANFAUPA) then
      SolaceViewVars('MANFAUPA',Thread(),'FileStatus',Status(MANFAUPA),Records(MANFAUPA))
    else
      SolaceViewVars('MANFAUPA',Thread(),'FileStatus',Status(MANFAUPA),0)
    end
    if Status(REPEXTRP) then
      SolaceViewVars('REPEXTRP',Thread(),'FileStatus',Status(REPEXTRP),Records(REPEXTRP))
    else
      SolaceViewVars('REPEXTRP',Thread(),'FileStatus',Status(REPEXTRP),0)
    end
    if Status(GENSHORT) then
      SolaceViewVars('GENSHORT',Thread(),'FileStatus',Status(GENSHORT),Records(GENSHORT))
    else
      SolaceViewVars('GENSHORT',Thread(),'FileStatus',Status(GENSHORT),0)
    end
    if Status(SIDALERT) then
      SolaceViewVars('SIDALERT',Thread(),'FileStatus',Status(SIDALERT),Records(SIDALERT))
    else
      SolaceViewVars('SIDALERT',Thread(),'FileStatus',Status(SIDALERT),0)
    end
    if Status(SIDALER2) then
      SolaceViewVars('SIDALER2',Thread(),'FileStatus',Status(SIDALER2),Records(SIDALER2))
    else
      SolaceViewVars('SIDALER2',Thread(),'FileStatus',Status(SIDALER2),0)
    end
    if Status(SIDALDEF) then
      SolaceViewVars('SIDALDEF',Thread(),'FileStatus',Status(SIDALDEF),Records(SIDALDEF))
    else
      SolaceViewVars('SIDALDEF',Thread(),'FileStatus',Status(SIDALDEF),0)
    end
    if Status(SMOALERT) then
      SolaceViewVars('SMOALERT',Thread(),'FileStatus',Status(SMOALERT),Records(SMOALERT))
    else
      SolaceViewVars('SMOALERT',Thread(),'FileStatus',Status(SMOALERT),0)
    end
    if Status(SIDREMIN) then
      SolaceViewVars('SIDREMIN',Thread(),'FileStatus',Status(SIDREMIN),Records(SIDREMIN))
    else
      SolaceViewVars('SIDREMIN',Thread(),'FileStatus',Status(SIDREMIN),0)
    end
    if Status(LOCINTER) then
      SolaceViewVars('LOCINTER',Thread(),'FileStatus',Status(LOCINTER),Records(LOCINTER))
    else
      SolaceViewVars('LOCINTER',Thread(),'FileStatus',Status(LOCINTER),0)
    end
    if Status(STAHEAD) then
      SolaceViewVars('STAHEAD',Thread(),'FileStatus',Status(STAHEAD),Records(STAHEAD))
    else
      SolaceViewVars('STAHEAD',Thread(),'FileStatus',Status(STAHEAD),0)
    end
    if Status(SIDAUD) then
      SolaceViewVars('SIDAUD',Thread(),'FileStatus',Status(SIDAUD),Records(SIDAUD))
    else
      SolaceViewVars('SIDAUD',Thread(),'FileStatus',Status(SIDAUD),0)
    end
    if Status(SIDRRCT) then
      SolaceViewVars('SIDRRCT',Thread(),'FileStatus',Status(SIDRRCT),Records(SIDRRCT))
    else
      SolaceViewVars('SIDRRCT',Thread(),'FileStatus',Status(SIDRRCT),0)
    end
    if Status(SIDCAPSC) then
      SolaceViewVars('SIDCAPSC',Thread(),'FileStatus',Status(SIDCAPSC),Records(SIDCAPSC))
    else
      SolaceViewVars('SIDCAPSC',Thread(),'FileStatus',Status(SIDCAPSC),0)
    end
    if Status(SIDMODTT) then
      SolaceViewVars('SIDMODTT',Thread(),'FileStatus',Status(SIDMODTT),Records(SIDMODTT))
    else
      SolaceViewVars('SIDMODTT',Thread(),'FileStatus',Status(SIDMODTT),0)
    end
    if Status(SIDEXUPD) then
      SolaceViewVars('SIDEXUPD',Thread(),'FileStatus',Status(SIDEXUPD),Records(SIDEXUPD))
    else
      SolaceViewVars('SIDEXUPD',Thread(),'FileStatus',Status(SIDEXUPD),0)
    end
    if Status(SIDSRVCN) then
      SolaceViewVars('SIDSRVCN',Thread(),'FileStatus',Status(SIDSRVCN),Records(SIDSRVCN))
    else
      SolaceViewVars('SIDSRVCN',Thread(),'FileStatus',Status(SIDSRVCN),0)
    end
    if Status(SMOREMI2) then
      SolaceViewVars('SMOREMI2',Thread(),'FileStatus',Status(SMOREMI2),Records(SMOREMI2))
    else
      SolaceViewVars('SMOREMI2',Thread(),'FileStatus',Status(SMOREMI2),0)
    end
    if Status(SIDMODSC) then
      SolaceViewVars('SIDMODSC',Thread(),'FileStatus',Status(SIDMODSC),Records(SIDMODSC))
    else
      SolaceViewVars('SIDMODSC',Thread(),'FileStatus',Status(SIDMODSC),0)
    end
    if Status(SIDREMI2) then
      SolaceViewVars('SIDREMI2',Thread(),'FileStatus',Status(SIDREMI2),Records(SIDREMI2))
    else
      SolaceViewVars('SIDREMI2',Thread(),'FileStatus',Status(SIDREMI2),0)
    end
    if Status(SIDNWDAY) then
      SolaceViewVars('SIDNWDAY',Thread(),'FileStatus',Status(SIDNWDAY),Records(SIDNWDAY))
    else
      SolaceViewVars('SIDNWDAY',Thread(),'FileStatus',Status(SIDNWDAY),0)
    end
    if Status(SMOREMIN) then
      SolaceViewVars('SMOREMIN',Thread(),'FileStatus',Status(SMOREMIN),Records(SMOREMIN))
    else
      SolaceViewVars('SMOREMIN',Thread(),'FileStatus',Status(SMOREMIN),0)
    end
    if Status(SMOALER2) then
      SolaceViewVars('SMOALER2',Thread(),'FileStatus',Status(SMOALER2),Records(SMOALER2))
    else
      SolaceViewVars('SMOALER2',Thread(),'FileStatus',Status(SMOALER2),0)
    end
    if Status(NOTESINV) then
      SolaceViewVars('NOTESINV',Thread(),'FileStatus',Status(NOTESINV),Records(NOTESINV))
    else
      SolaceViewVars('NOTESINV',Thread(),'FileStatus',Status(NOTESINV),0)
    end
    if Status(NOTESFAU) then
      SolaceViewVars('NOTESFAU',Thread(),'FileStatus',Status(NOTESFAU),Records(NOTESFAU))
    else
      SolaceViewVars('NOTESFAU',Thread(),'FileStatus',Status(NOTESFAU),0)
    end
    if Status(MANFAULO) then
      SolaceViewVars('MANFAULO',Thread(),'FileStatus',Status(MANFAULO),Records(MANFAULO))
    else
      SolaceViewVars('MANFAULO',Thread(),'FileStatus',Status(MANFAULO),0)
    end
    if Status(STATREP) then
      SolaceViewVars('STATREP',Thread(),'FileStatus',Status(STATREP),Records(STATREP))
    else
      SolaceViewVars('STATREP',Thread(),'FileStatus',Status(STATREP),0)
    end
    if Status(JOBEXHIS) then
      SolaceViewVars('JOBEXHIS',Thread(),'FileStatus',Status(JOBEXHIS),Records(JOBEXHIS))
    else
      SolaceViewVars('JOBEXHIS',Thread(),'FileStatus',Status(JOBEXHIS),0)
    end
    if Status(STARECIP) then
      SolaceViewVars('STARECIP',Thread(),'FileStatus',Status(STARECIP),Records(STARECIP))
    else
      SolaceViewVars('STARECIP',Thread(),'FileStatus',Status(STARECIP),0)
    end
    if Status(RETSTOCK) then
      SolaceViewVars('RETSTOCK',Thread(),'FileStatus',Status(RETSTOCK),Records(RETSTOCK))
    else
      SolaceViewVars('RETSTOCK',Thread(),'FileStatus',Status(RETSTOCK),0)
    end
    if Status(RETPAY) then
      SolaceViewVars('RETPAY',Thread(),'FileStatus',Status(RETPAY),Records(RETPAY))
    else
      SolaceViewVars('RETPAY',Thread(),'FileStatus',Status(RETPAY),0)
    end
    if Status(RETDESNO) then
      SolaceViewVars('RETDESNO',Thread(),'FileStatus',Status(RETDESNO),Records(RETDESNO))
    else
      SolaceViewVars('RETDESNO',Thread(),'FileStatus',Status(RETDESNO),0)
    end
    if Status(ORDPARTS) then
      SolaceViewVars('ORDPARTS',Thread(),'FileStatus',Status(ORDPARTS),Records(ORDPARTS))
    else
      SolaceViewVars('ORDPARTS',Thread(),'FileStatus',Status(ORDPARTS),0)
    end
    if Status(STOCKMIN) then
      SolaceViewVars('STOCKMIN',Thread(),'FileStatus',Status(STOCKMIN),Records(STOCKMIN))
    else
      SolaceViewVars('STOCKMIN',Thread(),'FileStatus',Status(STOCKMIN),0)
    end
    if Status(LOCATION) then
      SolaceViewVars('LOCATION',Thread(),'FileStatus',Status(LOCATION),Records(LOCATION))
    else
      SolaceViewVars('LOCATION',Thread(),'FileStatus',Status(LOCATION),0)
    end
    if Status(WPARTTMP) then
      SolaceViewVars('WPARTTMP',Thread(),'FileStatus',Status(WPARTTMP),Records(WPARTTMP))
    else
      SolaceViewVars('WPARTTMP',Thread(),'FileStatus',Status(WPARTTMP),0)
    end
    if Status(PARTSTMP) then
      SolaceViewVars('PARTSTMP',Thread(),'FileStatus',Status(PARTSTMP),Records(PARTSTMP))
    else
      SolaceViewVars('PARTSTMP',Thread(),'FileStatus',Status(PARTSTMP),0)
    end
    if Status(EXCHANGE) then
      SolaceViewVars('EXCHANGE',Thread(),'FileStatus',Status(EXCHANGE),Records(EXCHANGE))
    else
      SolaceViewVars('EXCHANGE',Thread(),'FileStatus',Status(EXCHANGE),0)
    end
    if Status(STOESN) then
      SolaceViewVars('STOESN',Thread(),'FileStatus',Status(STOESN),Records(STOESN))
    else
      SolaceViewVars('STOESN',Thread(),'FileStatus',Status(STOESN),0)
    end
    if Status(LOANACC) then
      SolaceViewVars('LOANACC',Thread(),'FileStatus',Status(LOANACC),Records(LOANACC))
    else
      SolaceViewVars('LOANACC',Thread(),'FileStatus',Status(LOANACC),0)
    end
    if Status(SUPPLIER) then
      SolaceViewVars('SUPPLIER',Thread(),'FileStatus',Status(SUPPLIER),Records(SUPPLIER))
    else
      SolaceViewVars('SUPPLIER',Thread(),'FileStatus',Status(SUPPLIER),0)
    end
    if Status(EXCHACC) then
      SolaceViewVars('EXCHACC',Thread(),'FileStatus',Status(EXCHACC),Records(EXCHACC))
    else
      SolaceViewVars('EXCHACC',Thread(),'FileStatus',Status(EXCHACC),0)
    end
    if Status(RECIPTYP) then
      SolaceViewVars('RECIPTYP',Thread(),'FileStatus',Status(RECIPTYP),Records(RECIPTYP))
    else
      SolaceViewVars('RECIPTYP',Thread(),'FileStatus',Status(RECIPTYP),0)
    end
    if Status(SUPVALA) then
      SolaceViewVars('SUPVALA',Thread(),'FileStatus',Status(SUPVALA),Records(SUPVALA))
    else
      SolaceViewVars('SUPVALA',Thread(),'FileStatus',Status(SUPVALA),0)
    end
    if Status(ESTPARTS) then
      SolaceViewVars('ESTPARTS',Thread(),'FileStatus',Status(ESTPARTS),Records(ESTPARTS))
    else
      SolaceViewVars('ESTPARTS',Thread(),'FileStatus',Status(ESTPARTS),0)
    end
    if Status(TRAEMAIL) then
      SolaceViewVars('TRAEMAIL',Thread(),'FileStatus',Status(TRAEMAIL),Records(TRAEMAIL))
    else
      SolaceViewVars('TRAEMAIL',Thread(),'FileStatus',Status(TRAEMAIL),0)
    end
    if Status(SUPVALB) then
      SolaceViewVars('SUPVALB',Thread(),'FileStatus',Status(SUPVALB),Records(SUPVALB))
    else
      SolaceViewVars('SUPVALB',Thread(),'FileStatus',Status(SUPVALB),0)
    end
    if Status(JOBS) then
      SolaceViewVars('JOBS',Thread(),'FileStatus',Status(JOBS),Records(JOBS))
    else
      SolaceViewVars('JOBS',Thread(),'FileStatus',Status(JOBS),0)
    end
    if Status(MODELNUM) then
      SolaceViewVars('MODELNUM',Thread(),'FileStatus',Status(MODELNUM),Records(MODELNUM))
    else
      SolaceViewVars('MODELNUM',Thread(),'FileStatus',Status(MODELNUM),0)
    end
    if Status(SUBEMAIL) then
      SolaceViewVars('SUBEMAIL',Thread(),'FileStatus',Status(SUBEMAIL),Records(SUBEMAIL))
    else
      SolaceViewVars('SUBEMAIL',Thread(),'FileStatus',Status(SUBEMAIL),0)
    end
    if Status(SUPPTEMP) then
      SolaceViewVars('SUPPTEMP',Thread(),'FileStatus',Status(SUPPTEMP),Records(SUPPTEMP))
    else
      SolaceViewVars('SUPPTEMP',Thread(),'FileStatus',Status(SUPPTEMP),0)
    end
    if Status(ORDERS) then
      SolaceViewVars('ORDERS',Thread(),'FileStatus',Status(ORDERS),Records(ORDERS))
    else
      SolaceViewVars('ORDERS',Thread(),'FileStatus',Status(ORDERS),0)
    end
    if Status(UNITTYPE) then
      SolaceViewVars('UNITTYPE',Thread(),'FileStatus',Status(UNITTYPE),Records(UNITTYPE))
    else
      SolaceViewVars('UNITTYPE',Thread(),'FileStatus',Status(UNITTYPE),0)
    end
    if Status(STATUS) then
      SolaceViewVars('STATUS',Thread(),'FileStatus',Status(STATUS),Records(STATUS))
    else
      SolaceViewVars('STATUS',Thread(),'FileStatus',Status(STATUS),0)
    end
    if Status(REPAIRTY) then
      SolaceViewVars('REPAIRTY',Thread(),'FileStatus',Status(REPAIRTY),Records(REPAIRTY))
    else
      SolaceViewVars('REPAIRTY',Thread(),'FileStatus',Status(REPAIRTY),0)
    end
    if Status(TRADETMP) then
      SolaceViewVars('TRADETMP',Thread(),'FileStatus',Status(TRADETMP),Records(TRADETMP))
    else
      SolaceViewVars('TRADETMP',Thread(),'FileStatus',Status(TRADETMP),0)
    end
    if Status(EXCCHRGE) then
      SolaceViewVars('EXCCHRGE',Thread(),'FileStatus',Status(EXCCHRGE),Records(EXCCHRGE))
    else
      SolaceViewVars('EXCCHRGE',Thread(),'FileStatus',Status(EXCCHRGE),0)
    end
    if Status(INVOICE) then
      SolaceViewVars('INVOICE',Thread(),'FileStatus',Status(INVOICE),Records(INVOICE))
    else
      SolaceViewVars('INVOICE',Thread(),'FileStatus',Status(INVOICE),0)
    end
    if Status(TRDBATCH) then
      SolaceViewVars('TRDBATCH',Thread(),'FileStatus',Status(TRDBATCH),Records(TRDBATCH))
    else
      SolaceViewVars('TRDBATCH',Thread(),'FileStatus',Status(TRDBATCH),0)
    end
    if Status(TRACHRGE) then
      SolaceViewVars('TRACHRGE',Thread(),'FileStatus',Status(TRACHRGE),Records(TRACHRGE))
    else
      SolaceViewVars('TRACHRGE',Thread(),'FileStatus',Status(TRACHRGE),0)
    end
    if Status(TRDMODEL) then
      SolaceViewVars('TRDMODEL',Thread(),'FileStatus',Status(TRDMODEL),Records(TRDMODEL))
    else
      SolaceViewVars('TRDMODEL',Thread(),'FileStatus',Status(TRDMODEL),0)
    end
    if Status(STOMODEL) then
      SolaceViewVars('STOMODEL',Thread(),'FileStatus',Status(STOMODEL),Records(STOMODEL))
    else
      SolaceViewVars('STOMODEL',Thread(),'FileStatus',Status(STOMODEL),0)
    end
    if Status(TRADEACC) then
      SolaceViewVars('TRADEACC',Thread(),'FileStatus',Status(TRADEACC),Records(TRADEACC))
    else
      SolaceViewVars('TRADEACC',Thread(),'FileStatus',Status(TRADEACC),0)
    end
    if Status(QUERYREA) then
      SolaceViewVars('QUERYREA',Thread(),'FileStatus',Status(QUERYREA),Records(QUERYREA))
    else
      SolaceViewVars('QUERYREA',Thread(),'FileStatus',Status(QUERYREA),0)
    end
    if Status(SUBCHRGE) then
      SolaceViewVars('SUBCHRGE',Thread(),'FileStatus',Status(SUBCHRGE),Records(SUBCHRGE))
    else
      SolaceViewVars('SUBCHRGE',Thread(),'FileStatus',Status(SUBCHRGE),0)
    end
    if Status(DEFCHRGE) then
      SolaceViewVars('DEFCHRGE',Thread(),'FileStatus',Status(DEFCHRGE),Records(DEFCHRGE))
    else
      SolaceViewVars('DEFCHRGE',Thread(),'FileStatus',Status(DEFCHRGE),0)
    end
    if Status(SUBTRACC) then
      SolaceViewVars('SUBTRACC',Thread(),'FileStatus',Status(SUBTRACC),Records(SUBTRACC))
    else
      SolaceViewVars('SUBTRACC',Thread(),'FileStatus',Status(SUBTRACC),0)
    end
    if Status(CHARTYPE) then
      SolaceViewVars('CHARTYPE',Thread(),'FileStatus',Status(CHARTYPE),Records(CHARTYPE))
    else
      SolaceViewVars('CHARTYPE',Thread(),'FileStatus',Status(CHARTYPE),0)
    end
    if Status(MANUFACT) then
      SolaceViewVars('MANUFACT',Thread(),'FileStatus',Status(MANUFACT),Records(MANUFACT))
    else
      SolaceViewVars('MANUFACT',Thread(),'FileStatus',Status(MANUFACT),0)
    end
    if Status(USMASSIG) then
      SolaceViewVars('USMASSIG',Thread(),'FileStatus',Status(USMASSIG),Records(USMASSIG))
    else
      SolaceViewVars('USMASSIG',Thread(),'FileStatus',Status(USMASSIG),0)
    end
    if Status(USUASSIG) then
      SolaceViewVars('USUASSIG',Thread(),'FileStatus',Status(USUASSIG),Records(USUASSIG))
    else
      SolaceViewVars('USUASSIG',Thread(),'FileStatus',Status(USUASSIG),0)
    end
    if Status(XMLTRADE) then
      SolaceViewVars('XMLTRADE',Thread(),'FileStatus',Status(XMLTRADE),Records(XMLTRADE))
    else
      SolaceViewVars('XMLTRADE',Thread(),'FileStatus',Status(XMLTRADE),0)
    end
    if Status(XMLJOBQ) then
      SolaceViewVars('XMLJOBQ',Thread(),'FileStatus',Status(XMLJOBQ),Records(XMLJOBQ))
    else
      SolaceViewVars('XMLJOBQ',Thread(),'FileStatus',Status(XMLJOBQ),0)
    end
    if Status(XMLDEFS) then
      SolaceViewVars('XMLDEFS',Thread(),'FileStatus',Status(XMLDEFS),Records(XMLDEFS))
    else
      SolaceViewVars('XMLDEFS',Thread(),'FileStatus',Status(XMLDEFS),0)
    end
    if Status(XMLSB) then
      SolaceViewVars('XMLSB',Thread(),'FileStatus',Status(XMLSB),Records(XMLSB))
    else
      SolaceViewVars('XMLSB',Thread(),'FileStatus',Status(XMLSB),0)
    end
    if Status(MPXDEFS) then
      SolaceViewVars('MPXDEFS',Thread(),'FileStatus',Status(MPXDEFS),Records(MPXDEFS))
    else
      SolaceViewVars('MPXDEFS',Thread(),'FileStatus',Status(MPXDEFS),0)
    end
    if Status(XMLSAM) then
      SolaceViewVars('XMLSAM',Thread(),'FileStatus',Status(XMLSAM),Records(XMLSAM))
    else
      SolaceViewVars('XMLSAM',Thread(),'FileStatus',Status(XMLSAM),0)
    end
    if Status(XMLDOCS) then
      SolaceViewVars('XMLDOCS',Thread(),'FileStatus',Status(XMLDOCS),Records(XMLDOCS))
    else
      SolaceViewVars('XMLDOCS',Thread(),'FileStatus',Status(XMLDOCS),0)
    end
    if Status(DEFEDI) then
      SolaceViewVars('DEFEDI',Thread(),'FileStatus',Status(DEFEDI),Records(DEFEDI))
    else
      SolaceViewVars('DEFEDI',Thread(),'FileStatus',Status(DEFEDI),0)
    end
    if Status(SIDRRCT_ALIAS) then
      SolaceViewVars('SIDRRCT_ALIAS',Thread(),'FileStatus',Status(SIDRRCT_ALIAS),Records(SIDRRCT_ALIAS))
    else
      SolaceViewVars('SIDRRCT_ALIAS',Thread(),'FileStatus',Status(SIDRRCT_ALIAS),0)
    end
    if Status(MANUFACT_ALIAS) then
      SolaceViewVars('MANUFACT_ALIAS',Thread(),'FileStatus',Status(MANUFACT_ALIAS),Records(MANUFACT_ALIAS))
    else
      SolaceViewVars('MANUFACT_ALIAS',Thread(),'FileStatus',Status(MANUFACT_ALIAS),0)
    end
    if Status(SIDREMIN_ALIAS) then
      SolaceViewVars('SIDREMIN_ALIAS',Thread(),'FileStatus',Status(SIDREMIN_ALIAS),Records(SIDREMIN_ALIAS))
    else
      SolaceViewVars('SIDREMIN_ALIAS',Thread(),'FileStatus',Status(SIDREMIN_ALIAS),0)
    end
    if Status(PENDMAIL_ALIAS) then
      SolaceViewVars('PENDMAIL_ALIAS',Thread(),'FileStatus',Status(PENDMAIL_ALIAS),Records(PENDMAIL_ALIAS))
    else
      SolaceViewVars('PENDMAIL_ALIAS',Thread(),'FileStatus',Status(PENDMAIL_ALIAS),0)
    end
    if Status(SIDALERT_ALIAS) then
      SolaceViewVars('SIDALERT_ALIAS',Thread(),'FileStatus',Status(SIDALERT_ALIAS),Records(SIDALERT_ALIAS))
    else
      SolaceViewVars('SIDALERT_ALIAS',Thread(),'FileStatus',Status(SIDALERT_ALIAS),0)
    end
    if Status(SIDSRVCN_ALIAS) then
      SolaceViewVars('SIDSRVCN_ALIAS',Thread(),'FileStatus',Status(SIDSRVCN_ALIAS),Records(SIDSRVCN_ALIAS))
    else
      SolaceViewVars('SIDSRVCN_ALIAS',Thread(),'FileStatus',Status(SIDSRVCN_ALIAS),0)
    end
    if Status(MPXJOBS_ALIAS) then
      SolaceViewVars('MPXJOBS_ALIAS',Thread(),'FileStatus',Status(MPXJOBS_ALIAS),Records(MPXJOBS_ALIAS))
    else
      SolaceViewVars('MPXJOBS_ALIAS',Thread(),'FileStatus',Status(MPXJOBS_ALIAS),0)
    end
    if Status(MPXSTAT_ALIAS) then
      SolaceViewVars('MPXSTAT_ALIAS',Thread(),'FileStatus',Status(MPXSTAT_ALIAS),Records(MPXSTAT_ALIAS))
    else
      SolaceViewVars('MPXSTAT_ALIAS',Thread(),'FileStatus',Status(MPXSTAT_ALIAS),0)
    end
    if Status(XMLJOBS_ALIAS) then
      SolaceViewVars('XMLJOBS_ALIAS',Thread(),'FileStatus',Status(XMLJOBS_ALIAS),Records(XMLJOBS_ALIAS))
    else
      SolaceViewVars('XMLJOBS_ALIAS',Thread(),'FileStatus',Status(XMLJOBS_ALIAS),0)
    end
    if Status(AUDIT_ALIAS) then
      SolaceViewVars('AUDIT_ALIAS',Thread(),'FileStatus',Status(AUDIT_ALIAS),Records(AUDIT_ALIAS))
    else
      SolaceViewVars('AUDIT_ALIAS',Thread(),'FileStatus',Status(AUDIT_ALIAS),0)
    end
    if Status(XMLSAM_ALIAS) then
      SolaceViewVars('XMLSAM_ALIAS',Thread(),'FileStatus',Status(XMLSAM_ALIAS),Records(XMLSAM_ALIAS))
    else
      SolaceViewVars('XMLSAM_ALIAS',Thread(),'FileStatus',Status(XMLSAM_ALIAS),0)
    end
    if Status(MULDESP_ALIAS) then
      SolaceViewVars('MULDESP_ALIAS',Thread(),'FileStatus',Status(MULDESP_ALIAS),Records(MULDESP_ALIAS))
    else
      SolaceViewVars('MULDESP_ALIAS',Thread(),'FileStatus',Status(MULDESP_ALIAS),0)
    end
    if Status(MULDESPJ_ALIAS) then
      SolaceViewVars('MULDESPJ_ALIAS',Thread(),'FileStatus',Status(MULDESPJ_ALIAS),Records(MULDESPJ_ALIAS))
    else
      SolaceViewVars('MULDESPJ_ALIAS',Thread(),'FileStatus',Status(MULDESPJ_ALIAS),0)
    end
    if Status(SUBCONTALIAS) then
      SolaceViewVars('SUBCONTALIAS',Thread(),'FileStatus',Status(SUBCONTALIAS),Records(SUBCONTALIAS))
    else
      SolaceViewVars('SUBCONTALIAS',Thread(),'FileStatus',Status(SUBCONTALIAS),0)
    end
    if Status(INSJOBSALIAS) then
      SolaceViewVars('INSJOBSALIAS',Thread(),'FileStatus',Status(INSJOBSALIAS),Records(INSJOBSALIAS))
    else
      SolaceViewVars('INSJOBSALIAS',Thread(),'FileStatus',Status(INSJOBSALIAS),0)
    end
    if Status(ESTPARTS_ALIAS) then
      SolaceViewVars('ESTPARTS_ALIAS',Thread(),'FileStatus',Status(ESTPARTS_ALIAS),Records(ESTPARTS_ALIAS))
    else
      SolaceViewVars('ESTPARTS_ALIAS',Thread(),'FileStatus',Status(ESTPARTS_ALIAS),0)
    end
    if Status(IEQUIP_ALIAS) then
      SolaceViewVars('IEQUIP_ALIAS',Thread(),'FileStatus',Status(IEQUIP_ALIAS),Records(IEQUIP_ALIAS))
    else
      SolaceViewVars('IEQUIP_ALIAS',Thread(),'FileStatus',Status(IEQUIP_ALIAS),0)
    end
    if Status(JOBS2_ALIAS) then
      SolaceViewVars('JOBS2_ALIAS',Thread(),'FileStatus',Status(JOBS2_ALIAS),Records(JOBS2_ALIAS))
    else
      SolaceViewVars('JOBS2_ALIAS',Thread(),'FileStatus',Status(JOBS2_ALIAS),0)
    end
    if Status(LOGASSST_ALIAS) then
      SolaceViewVars('LOGASSST_ALIAS',Thread(),'FileStatus',Status(LOGASSST_ALIAS),Records(LOGASSST_ALIAS))
    else
      SolaceViewVars('LOGASSST_ALIAS',Thread(),'FileStatus',Status(LOGASSST_ALIAS),0)
    end
    if Status(LOGSTOCK_ALIAS) then
      SolaceViewVars('LOGSTOCK_ALIAS',Thread(),'FileStatus',Status(LOGSTOCK_ALIAS),Records(LOGSTOCK_ALIAS))
    else
      SolaceViewVars('LOGSTOCK_ALIAS',Thread(),'FileStatus',Status(LOGSTOCK_ALIAS),0)
    end
    if Status(LOGSERST_ALIAS) then
      SolaceViewVars('LOGSERST_ALIAS',Thread(),'FileStatus',Status(LOGSERST_ALIAS),Records(LOGSERST_ALIAS))
    else
      SolaceViewVars('LOGSERST_ALIAS',Thread(),'FileStatus',Status(LOGSERST_ALIAS),0)
    end
    if Status(COURIER_ALIAS) then
      SolaceViewVars('COURIER_ALIAS',Thread(),'FileStatus',Status(COURIER_ALIAS),Records(COURIER_ALIAS))
    else
      SolaceViewVars('COURIER_ALIAS',Thread(),'FileStatus',Status(COURIER_ALIAS),0)
    end
    if Status(JOBNOTES_ALIAS) then
      SolaceViewVars('JOBNOTES_ALIAS',Thread(),'FileStatus',Status(JOBNOTES_ALIAS),Records(JOBNOTES_ALIAS))
    else
      SolaceViewVars('JOBNOTES_ALIAS',Thread(),'FileStatus',Status(JOBNOTES_ALIAS),0)
    end
    if Status(TRDBATCH_ALIAS) then
      SolaceViewVars('TRDBATCH_ALIAS',Thread(),'FileStatus',Status(TRDBATCH_ALIAS),Records(TRDBATCH_ALIAS))
    else
      SolaceViewVars('TRDBATCH_ALIAS',Thread(),'FileStatus',Status(TRDBATCH_ALIAS),0)
    end
    if Status(INVOICE_ALIAS) then
      SolaceViewVars('INVOICE_ALIAS',Thread(),'FileStatus',Status(INVOICE_ALIAS),Records(INVOICE_ALIAS))
    else
      SolaceViewVars('INVOICE_ALIAS',Thread(),'FileStatus',Status(INVOICE_ALIAS),0)
    end
    if Status(ORDPEND_ALIAS) then
      SolaceViewVars('ORDPEND_ALIAS',Thread(),'FileStatus',Status(ORDPEND_ALIAS),Records(ORDPEND_ALIAS))
    else
      SolaceViewVars('ORDPEND_ALIAS',Thread(),'FileStatus',Status(ORDPEND_ALIAS),0)
    end
    if Status(RETSALES_ALIAS) then
      SolaceViewVars('RETSALES_ALIAS',Thread(),'FileStatus',Status(RETSALES_ALIAS),Records(RETSALES_ALIAS))
    else
      SolaceViewVars('RETSALES_ALIAS',Thread(),'FileStatus',Status(RETSALES_ALIAS),0)
    end
    if Status(RETSTOCK_ALIAS) then
      SolaceViewVars('RETSTOCK_ALIAS',Thread(),'FileStatus',Status(RETSTOCK_ALIAS),Records(RETSTOCK_ALIAS))
    else
      SolaceViewVars('RETSTOCK_ALIAS',Thread(),'FileStatus',Status(RETSTOCK_ALIAS),0)
    end
    if Status(LOCATION_ALIAS) then
      SolaceViewVars('LOCATION_ALIAS',Thread(),'FileStatus',Status(LOCATION_ALIAS),Records(LOCATION_ALIAS))
    else
      SolaceViewVars('LOCATION_ALIAS',Thread(),'FileStatus',Status(LOCATION_ALIAS),0)
    end
    if Status(COMMONFA_ALIAS) then
      SolaceViewVars('COMMONFA_ALIAS',Thread(),'FileStatus',Status(COMMONFA_ALIAS),Records(COMMONFA_ALIAS))
    else
      SolaceViewVars('COMMONFA_ALIAS',Thread(),'FileStatus',Status(COMMONFA_ALIAS),0)
    end
    if Status(COMMONCP_ALIAS) then
      SolaceViewVars('COMMONCP_ALIAS',Thread(),'FileStatus',Status(COMMONCP_ALIAS),Records(COMMONCP_ALIAS))
    else
      SolaceViewVars('COMMONCP_ALIAS',Thread(),'FileStatus',Status(COMMONCP_ALIAS),0)
    end
    if Status(COMMONWP_ALIAS) then
      SolaceViewVars('COMMONWP_ALIAS',Thread(),'FileStatus',Status(COMMONWP_ALIAS),Records(COMMONWP_ALIAS))
    else
      SolaceViewVars('COMMONWP_ALIAS',Thread(),'FileStatus',Status(COMMONWP_ALIAS),0)
    end
    if Status(STOMODEL_ALIAS) then
      SolaceViewVars('STOMODEL_ALIAS',Thread(),'FileStatus',Status(STOMODEL_ALIAS),Records(STOMODEL_ALIAS))
    else
      SolaceViewVars('STOMODEL_ALIAS',Thread(),'FileStatus',Status(STOMODEL_ALIAS),0)
    end
    if Status(JOBPAYMT_ALIAS) then
      SolaceViewVars('JOBPAYMT_ALIAS',Thread(),'FileStatus',Status(JOBPAYMT_ALIAS),Records(JOBPAYMT_ALIAS))
    else
      SolaceViewVars('JOBPAYMT_ALIAS',Thread(),'FileStatus',Status(JOBPAYMT_ALIAS),0)
    end
    if Status(PARTSTMP_ALIAS) then
      SolaceViewVars('PARTSTMP_ALIAS',Thread(),'FileStatus',Status(PARTSTMP_ALIAS),Records(PARTSTMP_ALIAS))
    else
      SolaceViewVars('PARTSTMP_ALIAS',Thread(),'FileStatus',Status(PARTSTMP_ALIAS),0)
    end
    if Status(MPXDEFS_ALIAS) then
      SolaceViewVars('MPXDEFS_ALIAS',Thread(),'FileStatus',Status(MPXDEFS_ALIAS),Records(MPXDEFS_ALIAS))
    else
      SolaceViewVars('MPXDEFS_ALIAS',Thread(),'FileStatus',Status(MPXDEFS_ALIAS),0)
    end
    if Status(WPARTTMP_ALIAS) then
      SolaceViewVars('WPARTTMP_ALIAS',Thread(),'FileStatus',Status(WPARTTMP_ALIAS),Records(WPARTTMP_ALIAS))
    else
      SolaceViewVars('WPARTTMP_ALIAS',Thread(),'FileStatus',Status(WPARTTMP_ALIAS),0)
    end
    if Status(LOAN_ALIAS) then
      SolaceViewVars('LOAN_ALIAS',Thread(),'FileStatus',Status(LOAN_ALIAS),Records(LOAN_ALIAS))
    else
      SolaceViewVars('LOAN_ALIAS',Thread(),'FileStatus',Status(LOAN_ALIAS),0)
    end
    if Status(EXCHANGE_ALIAS) then
      SolaceViewVars('EXCHANGE_ALIAS',Thread(),'FileStatus',Status(EXCHANGE_ALIAS),Records(EXCHANGE_ALIAS))
    else
      SolaceViewVars('EXCHANGE_ALIAS',Thread(),'FileStatus',Status(EXCHANGE_ALIAS),0)
    end
    if Status(ACCAREAS_ALIAS) then
      SolaceViewVars('ACCAREAS_ALIAS',Thread(),'FileStatus',Status(ACCAREAS_ALIAS),Records(ACCAREAS_ALIAS))
    else
      SolaceViewVars('ACCAREAS_ALIAS',Thread(),'FileStatus',Status(ACCAREAS_ALIAS),0)
    end
    if Status(USERS_ALIAS) then
      SolaceViewVars('USERS_ALIAS',Thread(),'FileStatus',Status(USERS_ALIAS),Records(USERS_ALIAS))
    else
      SolaceViewVars('USERS_ALIAS',Thread(),'FileStatus',Status(USERS_ALIAS),0)
    end
    if Status(WARPARTS_ALIAS) then
      SolaceViewVars('WARPARTS_ALIAS',Thread(),'FileStatus',Status(WARPARTS_ALIAS),Records(WARPARTS_ALIAS))
    else
      SolaceViewVars('WARPARTS_ALIAS',Thread(),'FileStatus',Status(WARPARTS_ALIAS),0)
    end
    if Status(ORDPARTS_ALIAS) then
      SolaceViewVars('ORDPARTS_ALIAS',Thread(),'FileStatus',Status(ORDPARTS_ALIAS),Records(ORDPARTS_ALIAS))
    else
      SolaceViewVars('ORDPARTS_ALIAS',Thread(),'FileStatus',Status(ORDPARTS_ALIAS),0)
    end
    if Status(PARTS_ALIAS) then
      SolaceViewVars('PARTS_ALIAS',Thread(),'FileStatus',Status(PARTS_ALIAS),Records(PARTS_ALIAS))
    else
      SolaceViewVars('PARTS_ALIAS',Thread(),'FileStatus',Status(PARTS_ALIAS),0)
    end
    if Status(STOCK_ALIAS) then
      SolaceViewVars('STOCK_ALIAS',Thread(),'FileStatus',Status(STOCK_ALIAS),Records(STOCK_ALIAS))
    else
      SolaceViewVars('STOCK_ALIAS',Thread(),'FileStatus',Status(STOCK_ALIAS),0)
    end
    if Status(JOBS_ALIAS) then
      SolaceViewVars('JOBS_ALIAS',Thread(),'FileStatus',Status(JOBS_ALIAS),Records(JOBS_ALIAS))
    else
      SolaceViewVars('JOBS_ALIAS',Thread(),'FileStatus',Status(JOBS_ALIAS),0)
    end
    if Status(XMLTRADE_ALIAS) then
      SolaceViewVars('XMLTRADE_ALIAS',Thread(),'FileStatus',Status(XMLTRADE_ALIAS),Records(XMLTRADE_ALIAS))
    else
      SolaceViewVars('XMLTRADE_ALIAS',Thread(),'FileStatus',Status(XMLTRADE_ALIAS),0)
    end
    if Status(XMLSB_ALIAS) then
      SolaceViewVars('XMLSB_ALIAS',Thread(),'FileStatus',Status(XMLSB_ALIAS),Records(XMLSB_ALIAS))
    else
      SolaceViewVars('XMLSB_ALIAS',Thread(),'FileStatus',Status(XMLSB_ALIAS),0)
    end
    if Status(STDCHRGE_ALIAS) then
      SolaceViewVars('STDCHRGE_ALIAS',Thread(),'FileStatus',Status(STDCHRGE_ALIAS),Records(STDCHRGE_ALIAS))
    else
      SolaceViewVars('STDCHRGE_ALIAS',Thread(),'FileStatus',Status(STDCHRGE_ALIAS),0)
    end
  !End


SolSingleFileStatus     Procedure
  Code
  !if (SolaceCurrentTab = 3 and SolaceNoRefreshOneFile = true) or SolaceNoRefreshOneFile = false or SolaceSaveToFile = true then
    Free(FileFieldsQueue)
    Loop Solace# = 1 to Records(SolaceAllFiles)
      Get(SolaceAllFiles,Solace#)
      if Clip(SOLALL:FileName) = clip(SolaceCurrentFile) then
        SOLFFV:FieldName = SOLALL:FieldName
        SOLFFV:FieldValue = SOLALL:FieldValue
        Add(FileFieldsQueue)
      end
    end
  !end
