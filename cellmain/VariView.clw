          MEMBER('cellmain.clw')                      ! This is a MEMBER module

   Map
   End

SolaceAddEvents         Procedure(P:Event,P:Proc,P:Control)
  Code
  Case P:Event
  of EVENT:Accepted
    SOLEVT:EventName = 'Accepted'
  of EVENT:Selected
    SOLEVT:EventName = 'Selected'
  of EVENT:NewSelection
    SOLEVT:EventName = 'NewSelection'
  of EVENT:MouseDown
    SOLEVT:EventName = 'MouseDown'
  of EVENT:MouseUp
    SOLEVT:EventName = 'MouseUp'
  of EVENT:MouseIn
    SOLEVT:EventName = 'MouseIn'
  of EVENT:MouseOut
    SOLEVT:EventName = 'MouseOut'
  of EVENT:MouseMove
    SOLEVT:EventName = 'MouseMove'
  of EVENT:AlertKey
    SOLEVT:EventName = 'AlertKey'
  of EVENT:PreAlertKey
    SOLEVT:EventName = 'PreAlertKey'
  of EVENT:Dragging
    SOLEVT:EventName = 'Dragging'
  of EVENT:Drag
    SOLEVT:EventName = 'Drag'
  of EVENT:Drop
    SOLEVT:EventName = 'Drop'
  of EVENT:ScrollDrag
    SOLEVT:EventName = 'ScrollDrag'
  of EVENT:ScrollUp
    SOLEVT:EventName = 'ScrollUp'
  of EVENT:ScrollDown
    SOLEVT:EventName = 'ScrollDown'
  of Event:PageUp
    SOLEVT:EventName = 'PageUp'
  of Event:PageDown
    SOLEVT:EventName = 'PageDown'
  of EVENT:ScrollTop
    SOLEVT:EventName = 'ScrollTop'
  of EVENT:ScrollBottom
    SOLEVT:EventName = 'ScrollBottom'
  of EVENT:Locate
    SOLEVT:EventName = 'Locate'
  of EVENT:VBXevent
    SOLEVT:EventName = 'VBXevent'
  of EVENT:TabChanging
    SOLEVT:EventName = 'TabChanging'
  of EVENT:Expanding
    SOLEVT:EventName = 'Expanding'
  of EVENT:Contracting
    SOLEVT:EventName = 'Contracting'
  of EVENT:Contracted
    SOLEVT:EventName = 'Contracted'
  of EVENT:Rejected
    SOLEVT:EventName = 'Rejected'
  of EVENT:DroppingDown
    SOLEVT:EventName = 'DroppingDown'
  of EVENT:DroppedDown
    SOLEVT:EventName = 'DroppedDown'
  of EVENT:ScrollTrack
    SOLEVT:EventName = 'ScrollTrack'
  of EVENT:ColumnResize
    SOLEVT:EventName = 'ColumnResize'
  of EVENT:Selecting
    SOLEVT:EventName = 'Selecting'
  of EVENT:Selected
    SOLEVT:EventName = 'Selected'
  of EVENT:CloseWindow
    SOLEVT:EventName = 'CloseWindow'
  of EVENT:CloseDown
    SOLEVT:EventName = 'CloseDown'
  of EVENT:OpenWindow
    SOLEVT:EventName = 'OpenWindow'
  of EVENT:OpenFailed
    SOLEVT:EventName = 'OpenFailed'
  of EVENT:LoseFocus
    SOLEVT:EventName = 'LoseFocus'
  of EVENT:GainFocus
    SOLEVT:EventName = 'GainFocus'
  of EVENT:Suspend
    SOLEVT:EventName = 'Suspend'
  of EVENT:Resume
    SOLEVT:EventName = 'Resume'
  of EVENT:Timer
    SOLEVT:EventName = 'Timer'
  of EVENT:DDErequest
    SOLEVT:EventName = 'DDErequest'
  of EVENT:DDEadvise
    SOLEVT:EventName = 'DDEadvise'
  of EVENT:DDEdata
    SOLEVT:EventName = 'DDEdata'
  of EVENT:DDEcommand orof EVENT:DDEexecute
    SOLEVT:EventName = 'DDEcommand/DDEexecute'
  of EVENT:DDEpoke
    SOLEVT:EventName = 'DDEpoke'
  of EVENT:DDEclosed
    SOLEVT:EventName = 'DDEclosed'
  of EVENT:Move
    SOLEVT:EventName = 'Move'
  of EVENT:Size
    SOLEVT:EventName = 'Size'
  of EVENT:Restore
    SOLEVT:EventName = 'Restore'
  of EVENT:Maximize
    SOLEVT:EventName = 'Maximize'
  of EVENT:Iconize
    SOLEVT:EventName = 'Iconize'
  of EVENT:Completed
    SOLEVT:EventName = 'Completed'
  of EVENT:Moved
    SOLEVT:EventName = 'Moved'
  of EVENT:Sized
    SOLEVT:EventName = 'Sized'
  of EVENT:Restored
    SOLEVT:EventName = 'Restored'
  of EVENT:Maximized
    SOLEVT:EventName = 'Maximized'
  of EVENT:Iconized
    SOLEVT:EventName = 'Iconized'
  of EVENT:Docked
    SOLEVT:EventName = 'Docked'
  of EVENT:Undocked
    SOLEVT:EventName = 'Undocked'
  of EVENT:BuildFile
    SOLEVT:EventName = 'BuildFile'
  of EVENT:BuildKey
    SOLEVT:EventName = 'BuildKey'
  of EVENT:BuildDone
    SOLEVT:EventName = 'BuildDone'
  else
    SOLEVT:EventName = 'Unknown Event ' & P:Event
  end
  SOLEVT:ProcName = P:Proc
  SOLEVT:CtrlName = P:Control
  Add(SolaceEventQueue,1)


SolaceGlobalVariableView        Procedure
   Code
   do SolFileList
   do SolEnvironment
   do SolGlobalData
   SolDictFiles1
   SolDictFiles2
   SolDictFiles3
   SolDictFiles4
   SolDictFiles5
   SolDictFiles6
   SolDictFiles7
   SolDictFiles8
   SolFileStatus
   SolSingleFileStatus



SolFileList        Routine
   Free(SolaceGlobView)
   Free(SolaceFiles)
   Free(SolaceAllFiles)
   Free(SolaceEnvQueue)
   If ~Records(SolaceFilesNames)
    SOLFN:FileName = 'STDCHRGE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'AccSKU'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PROCFILE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOCDEFS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'CARISMA'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAIL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'CRCDEFS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'REGIONS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ACCREG'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDACREG'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLJOBS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXCHAMF'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXCHAUI'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSENG'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MULDESPJ'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'AUDSTAEX'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MULDESP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'INSMODELS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STOCKLOG'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IMEILOG'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STMASAUD'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'AUDSTATS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IEXPDEFS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RAPIDLST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ICONTHIST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ORDTEMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ORDITEMS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ORDHEAD'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IMEISHIP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IACTION'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBTHIRD'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFAULTV'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGRETRN'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PRODCODE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOG2TEMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGSTOCK'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAR1'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LETTERS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAB2'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAB3'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAB4'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAB5'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAB6'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAB7'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAE2'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAE3'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEME2B'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEME3B'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAR2'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAR3'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAR4'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAB1'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAE1'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEMAI2'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFSIDEX'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STOAUDIT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFAULT2'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ACTION'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFRAPID'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RETSALES'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'BOUNCER'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TEAMS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MERGE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'UPDDATA'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'WEBDEFLT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXCAUDIT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'COMMONFA'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PARAMSS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'INSJOBS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'CONSIGN'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBEXACC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MODELCOL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'COMMCAT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MESSAGES'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGEXHE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFCRC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PAYTYPES'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'COMMONWP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGEXCH'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'QAREASON'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'COMMONCP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ADDSEARCH'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDDEF'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'CONTHIST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'CONTACTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBBATCH'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEDI2'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFPRINT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFWEB'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXPCITY'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PROCCODE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'COLOUR'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'VODAIMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSVODA'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ACCESDEF'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STANTEXT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'NOTESENG'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IAUDIT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IINVJOB'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'INSDEF'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IEXPUSE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IRECEIVE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IJOBSTATUS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ICLCOST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IVEHDETS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IAPPENG'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IAPPTYPE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'INSMAKE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'INSAPPS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ISUBCONT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRDSPEC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ITIMES'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IEQUIP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IENGINEERS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ICLIENTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ISUBPOST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IJOBTYPE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EDIBATCH'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ORDPEND'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STOHIST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'NEWFEAT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXREASON'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'REPTYDEF'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RETACCOUNTSLIST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PRIORITY'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEPS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'REPEXTTP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MANFAULT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'INVPARTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSEARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'INVPATMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRACHAR'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'CITYSERV'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PICKNOTE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DISCOUNT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOCVALUE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STOCKTYP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IEMAILS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'COURIER'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRDPARTY'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSTMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGALLOC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBNOTES'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PROINV'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBACTMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RETPARTSLIST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRANTYPE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DESBATCH'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBVODAC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ESNMODEL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBPAYMT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGASSSTTEMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STOCK'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGGED'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGRTHIS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGSTHIS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGASSST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGSTOLC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGTEMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGSERST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGDEFLT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGSTLOC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MULTIDEF'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRAFAULO'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MPXJOBS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MPXSTAT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRAFAULT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXMINLEV'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGSTHII'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGCLSTE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STATCRIT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'NETWORKS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGDESNO'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'CPNDPRTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGSALCD'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFSTOCK'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBLOHIS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOAN'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'NOTESCON'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'VATCODE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOANHIST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TURNARND'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ORDJOBS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MERGETXT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MERGELET'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ORDSTOCK'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXCHHIST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ACCESSOR'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SUBACCAD'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'NOTESDEL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PICKDET'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'WARPARTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PARTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBACC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'USELEVEL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ALLLEVEL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'QAPARTSTEMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RETAILER'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ACCAREAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'AUDIT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'USERS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDREG'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOCSHELF'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PENDMAIL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFAULTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'REPTYCAT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'REPTYPETEMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MANFPALO'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSTAGE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MANFAUPA'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'REPEXTRP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'GENSHORT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDALERT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDALER2'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDALDEF'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SMOALERT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDREMIN'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOCINTER'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STAHEAD'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDAUD'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDRRCT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDCAPSC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDMODTT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDEXUPD'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDSRVCN'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SMOREMI2'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDMODSC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDREMI2'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDNWDAY'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SMOREMIN'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SMOALER2'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'NOTESINV'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'NOTESFAU'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MANFAULO'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STATREP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBEXHIS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STARECIP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RETSTOCK'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RETPAY'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RETDESNO'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ORDPARTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STOCKMIN'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOCATION'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'WPARTTMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PARTSTMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXCHANGE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STOESN'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOANACC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SUPPLIER'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXCHACC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RECIPTYP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SUPVALA'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ESTPARTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRAEMAIL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SUPVALB'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MODELNUM'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SUBEMAIL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SUPPTEMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ORDERS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'UNITTYPE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STATUS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'REPAIRTY'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRADETMP'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXCCHRGE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'INVOICE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRDBATCH'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRACHRGE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRDMODEL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STOMODEL'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRADEACC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'QUERYREA'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SUBCHRGE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFCHRGE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SUBTRACC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'CHARTYPE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MANUFACT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'USMASSIG'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'USUASSIG'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLTRADE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLJOBQ'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLDEFS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLSB'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MPXDEFS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLSAM'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLDOCS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFEDI'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDRRCT_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MANUFACT_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDREMIN_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PENDMAIL_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDALERT_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SIDSRVCN_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MPXJOBS_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MPXSTAT_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLJOBS_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'AUDIT_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLSAM_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MULDESP_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MULDESPJ_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'SUBCONTALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'INSJOBSALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ESTPARTS_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'IEQUIP_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBS2_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGASSST_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGSTOCK_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGSERST_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'COURIER_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBNOTES_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'TRDBATCH_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'INVOICE_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ORDPEND_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RETSALES_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'RETSTOCK_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOCATION_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'COMMONFA_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'COMMONCP_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'COMMONWP_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STOMODEL_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBPAYMT_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PARTSTMP_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'MPXDEFS_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'WPARTTMP_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOAN_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'EXCHANGE_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ACCAREAS_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'USERS_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'WARPARTS_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ORDPARTS_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PARTS_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STOCK_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBS_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLTRADE_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'XMLSB_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'STDCHRGE_ALIAS'
    Add(SolaceFilesNames,+SOLFN:FileName)
   END


SolEnvironment     Routine
   if (SolaceCurrentTab = 7 and SolaceNoRefreshEnvironment = true) or SolaceNoRefreshEnvironment = false or SolaceSaveToFile = true then
     GlobalMemoryStatus(CurrentMem)
     SOLENV:EnvName = 'Total Physical Memory'
     SOLENV:EnvValue = CurrentMem.dwTotalPhys & '  (' & Int(CurrentMem.dwTotalPhys / 1024000) & 'MB)'
     Add(SolaceEnvQueue)

     SOLENV:EnvName = 'Available Physical Memory'
     SOLENV:EnvValue = CurrentMem.dwAvailPhys & '  (' & Int(CurrentMem.dwAvailPhys / 1024000) & 'MB)'
     Add(SolaceEnvQueue)

     SOLENV:EnvName = 'Total Virtual Memory'
     SOLENV:EnvValue = CurrentMem.dwTotalVirtual & '  (' & Int(CurrentMem.dwTotalVirtual / 1024000) & 'MB)'
     Add(SolaceEnvQueue)

     SOLENV:EnvName = 'Available Virtual Memory'
     SOLENV:EnvValue = CurrentMem.dwAvailVirtual & '  (' & Int(CurrentMem.dwAvailVirtual / 1024000) & 'MB)'
     Add(SolaceEnvQueue)
     SOLENV:EnvName = 'Current Path'
     SOLENV:EnvValue = LongPath()
     Add(SolaceEnvQueue)

     drives" = 'CDEFGHIJKLMNOPQRSTUVWXYZ'
     Loop x# = 1 to 24
       SolaceRootPath = Sub(drives",x#,1) & ':'
       Case GetDriveType(SolaceRootPath)
       of 0 orof 1
         cycle
       of 2
         SOLENV:EnvName = clip(SolaceRootPath) & ' - Removeable Drive'
       of 3
         SOLENV:EnvName = clip(SolaceRootPath) & ' - Local Hard Disk'
       of 4
         SOLENV:EnvName = clip(SolaceRootPath) & ' - Network Drive'
       of 5
         SOLENV:EnvName = clip(SolaceRootPath) & ' - CD Rom'
       of 6
         SOLENV:EnvName = clip(SolaceRootPath) & ' - RAM Disk'
       end
       SolaceRootPath = SolaceRootPath & '\'
       SOLENV:EnvValue = ''
       if GetDriveType(SolaceRootPath) <> 4 and Sub(Path(),1,1) <> drives" then       !Ignore Network drives
       if GetDriveType(SolaceRootPath) <> 5 then       !Ignore CD ROMs
         Ret# = GetDiskFreeSpaceA(SolaceRootPath,SolaceSectPerClust,SolaceBytesPerSect,SolaceFreeClust,SolaceClusters)
         TotalDiskSpace# = (SolaceBytesPerSect * SolaceSectPerClust * SolaceClusters) / 1024000
         FreeDiskSpace# = (SolaceBytesPerSect * SolaceSectPerClust * SolaceFreeClust) / 1024000
         SOLENV:EnvValue = 'Disk Space: ' & TotalDiskSpace# & 'MB  Free: ' & FreeDiskSpace# & 'MB'
       End
       End
       Add(SolaceEnvQueue)
     end


     WinVersion.dwOSVersionInfoSize = Size(OSVERSIONINFO)

     Ret# = GetVersionEx(WinVersion)
     SOLENV:EnvName = 'OS Version'
     Case WinVersion.dwPlatformId
     of 1
       SOLENV:EnvValue = 'Win95/98/Me'
     of 2
       if WinVersion.dwMajorVersion >= 5 then
         SOLENV:EnvValue = '2000'
       else
         SOLENV:EnvValue = 'NT'
       end
     of 0
       SOLENV:EnvValue = 'Win 3.1 with Win32s'
     end

     SOLENV:EnvValue = clip(SOLENV:EnvValue) & '   ' & WinVersion.dwMajorVersion & '.' & WinVersion.dwMinorVersion & '.' & WinVersion.dwBuildNumber
     Add(SolaceEnvQueue)
   end


SolGlobalData       Routine
   SolaceViewVars('glo:ErrorText',glo:ErrorText,'Solace_Global',1)
   SolaceViewVars('glo:owner',glo:owner,'Solace_Global',1)
   SolaceViewVars('glo:DateModify',glo:DateModify,'Solace_Global',1)
   SolaceViewVars('glo:TimeModify',glo:TimeModify,'Solace_Global',1)
   SolaceViewVars('glo:Notes_Global',glo:Notes_Global,'Solace_Global',1)
   SolaceViewVars('glo:Q_Invoice:Invoice_Number',glo:Q_Invoice:Invoice_Number,'Solace_Global',1)
   SolaceViewVars('glo:Q_Invoice:Account_Number',glo:Q_Invoice:Account_Number,'Solace_Global',1)
   SolaceViewVars('glo:afe_path',glo:afe_path,'Solace_Global',1)
   SolaceViewVars('glo:Password',glo:Password,'Solace_Global',1)
   SolaceViewVars('glo:PassAccount',glo:PassAccount,'Solace_Global',1)
   SolaceViewVars('glo:Preview',glo:Preview,'Solace_Global',1)
   SolaceViewVars('glo:q_JobNumber:Job_Number_Pointer',glo:q_JobNumber:Job_Number_Pointer,'Solace_Global',1)
   SolaceViewVars('glo:Q_PartsOrder:Q_Order_Number',glo:Q_PartsOrder:Q_Order_Number,'Solace_Global',1)
   SolaceViewVars('glo:Q_PartsOrder:Q_Part_Number',glo:Q_PartsOrder:Q_Part_Number,'Solace_Global',1)
   SolaceViewVars('glo:Q_PartsOrder:Q_Description',glo:Q_PartsOrder:Q_Description,'Solace_Global',1)
   SolaceViewVars('glo:Q_PartsOrder:Q_Supplier',glo:Q_PartsOrder:Q_Supplier,'Solace_Global',1)
   SolaceViewVars('glo:Q_PartsOrder:Q_Quantity',glo:Q_PartsOrder:Q_Quantity,'Solace_Global',1)
   SolaceViewVars('glo:Q_PartsOrder:Q_Purchase_Cost',glo:Q_PartsOrder:Q_Purchase_Cost,'Solace_Global',1)
   SolaceViewVars('glo:Q_PartsOrder:Q_Sale_Cost',glo:Q_PartsOrder:Q_Sale_Cost,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select1',glo:G_Select1:Select1,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select2',glo:G_Select1:Select2,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select3',glo:G_Select1:Select3,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select4',glo:G_Select1:Select4,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select5',glo:G_Select1:Select5,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select6',glo:G_Select1:Select6,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select7',glo:G_Select1:Select7,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select8',glo:G_Select1:Select8,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select9',glo:G_Select1:Select9,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select10',glo:G_Select1:Select10,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select11',glo:G_Select1:Select11,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select12',glo:G_Select1:Select12,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select13',glo:G_Select1:Select13,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select14',glo:G_Select1:Select14,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select15',glo:G_Select1:Select15,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select16',glo:G_Select1:Select16,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select17',glo:G_Select1:Select17,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select18',glo:G_Select1:Select18,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select19',glo:G_Select1:Select19,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select20',glo:G_Select1:Select20,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select21',glo:G_Select1:Select21,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select22',glo:G_Select1:Select22,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select23',glo:G_Select1:Select23,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select24',glo:G_Select1:Select24,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select25',glo:G_Select1:Select25,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select26',glo:G_Select1:Select26,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select27',glo:G_Select1:Select27,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select28',glo:G_Select1:Select28,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select29',glo:G_Select1:Select29,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select30',glo:G_Select1:Select30,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select31',glo:G_Select1:Select31,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select32',glo:G_Select1:Select32,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select33',glo:G_Select1:Select33,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select34',glo:G_Select1:Select34,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select35',glo:G_Select1:Select35,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select36',glo:G_Select1:Select36,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select37',glo:G_Select1:Select37,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select38',glo:G_Select1:Select38,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select39',glo:G_Select1:Select39,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select40',glo:G_Select1:Select40,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select41',glo:G_Select1:Select41,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select42',glo:G_Select1:Select42,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select43',glo:G_Select1:Select43,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select44',glo:G_Select1:Select44,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select45',glo:G_Select1:Select45,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select46',glo:G_Select1:Select46,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select47',glo:G_Select1:Select47,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select48',glo:G_Select1:Select48,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select49',glo:G_Select1:Select49,'Solace_Global',1)
   SolaceViewVars('glo:G_Select1:Select50',glo:G_Select1:Select50,'Solace_Global',1)
   SolaceViewVars('glo:q_RepairType:Repair_Type_Pointer',glo:q_RepairType:Repair_Type_Pointer,'Solace_Global',1)
   SolaceViewVars('glo:Q_UnitType:Unit_Type_Pointer',glo:Q_UnitType:Unit_Type_Pointer,'Solace_Global',1)
   SolaceViewVars('glo:Q_ChargeType:Charge_Type_Pointer',glo:Q_ChargeType:Charge_Type_Pointer,'Solace_Global',1)
   SolaceViewVars('glo:Q_ModelNumber:Model_Number_Pointer',glo:Q_ModelNumber:Model_Number_Pointer,'Solace_Global',1)
   SolaceViewVars('glo:Q_Accessory:Accessory_Pointer',glo:Q_Accessory:Accessory_Pointer,'Solace_Global',1)
   SolaceViewVars('glo:Q_AccessLevel:Access_Level_Pointer',glo:Q_AccessLevel:Access_Level_Pointer,'Solace_Global',1)
   SolaceViewVars('glo:Q_AccessAreas:Access_Areas_Pointer',glo:Q_AccessAreas:Access_Areas_Pointer,'Solace_Global',1)
   SolaceViewVars('glo:Q_Notes:notes_pointer',glo:Q_Notes:notes_pointer,'Solace_Global',1)
   SolaceViewVars('glo:EnableFlag',glo:EnableFlag,'Solace_Global',1)
   SolaceViewVars('glo:TimeLogged',glo:TimeLogged,'Solace_Global',1)
   SolaceViewVars('glo:GLO:ApplicationName',glo:GLO:ApplicationName,'Solace_Global',1)
   SolaceViewVars('glo:GLO:ApplicationCreationDate',glo:GLO:ApplicationCreationDate,'Solace_Global',1)
   SolaceViewVars('glo:GLO:ApplicationCreationTime',glo:GLO:ApplicationCreationTime,'Solace_Global',1)
   SolaceViewVars('glo:GLO:ApplicationChangeDate',glo:GLO:ApplicationChangeDate,'Solace_Global',1)
   SolaceViewVars('glo:GLO:ApplicationChangeTime',glo:GLO:ApplicationChangeTime,'Solace_Global',1)
   SolaceViewVars('glo:GLO:Compiled32',glo:GLO:Compiled32,'Solace_Global',1)
   SolaceViewVars('glo:GLO:AppINIFile',glo:GLO:AppINIFile,'Solace_Global',1)
   SolaceViewVars('glo:Queue:Pointer',glo:Queue:Pointer,'Solace_Global',1)
   SolaceViewVars('glo:Queue2:Pointer2',glo:Queue2:Pointer2,'Solace_Global',1)
   SolaceViewVars('glo:Queue3:Pointer3',glo:Queue3:Pointer3,'Solace_Global',1)
   SolaceViewVars('glo:Queue4:Pointer4',glo:Queue4:Pointer4,'Solace_Global',1)
   SolaceViewVars('glo:Queue5:Pointer5',glo:Queue5:Pointer5,'Solace_Global',1)
   SolaceViewVars('glo:Queue6:Pointer6',glo:Queue6:Pointer6,'Solace_Global',1)
   SolaceViewVars('glo:Queue7:Pointer7',glo:Queue7:Pointer7,'Solace_Global',1)
   SolaceViewVars('glo:Queue8:Pointer8',glo:Queue8:Pointer8,'Solace_Global',1)
   SolaceViewVars('glo:Queue9:Pointer9',glo:Queue9:Pointer9,'Solace_Global',1)
   SolaceViewVars('glo:Queue10:Pointer10',glo:Queue10:Pointer10,'Solace_Global',1)
   SolaceViewVars('glo:Queue11:Pointer11',glo:Queue11:Pointer11,'Solace_Global',1)
   SolaceViewVars('glo:Queue12:Pointer12',glo:Queue12:Pointer12,'Solace_Global',1)
   SolaceViewVars('glo:Queue13:Pointer13',glo:Queue13:Pointer13,'Solace_Global',1)
   SolaceViewVars('glo:Queue14:Pointer14',glo:Queue14:Pointer14,'Solace_Global',1)
   SolaceViewVars('glo:Queue15:Pointer15',glo:Queue15:Pointer15,'Solace_Global',1)
   SolaceViewVars('glo:Queue16:Pointer16',glo:Queue16:Pointer16,'Solace_Global',1)
   SolaceViewVars('glo:Queue17:Pointer17',glo:Queue17:Pointer17,'Solace_Global',1)
   SolaceViewVars('glo:Queue18:Pointer18',glo:Queue18:Pointer18,'Solace_Global',1)
   SolaceViewVars('glo:Queue19:Pointer19',glo:Queue19:Pointer19,'Solace_Global',1)
   SolaceViewVars('glo:Queue20:Pointer20',glo:Queue20:Pointer20,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode1',glo:FaultCode1,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode2',glo:FaultCode2,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode3',glo:FaultCode3,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode4',glo:FaultCode4,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode5',glo:FaultCode5,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode6',glo:FaultCode6,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode7',glo:FaultCode7,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode8',glo:FaultCode8,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode9',glo:FaultCode9,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode10',glo:FaultCode10,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode11',glo:FaultCode11,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode12',glo:FaultCode12,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode13',glo:FaultCode13,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode14',glo:FaultCode14,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode15',glo:FaultCode15,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode16',glo:FaultCode16,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode17',glo:FaultCode17,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode18',glo:FaultCode18,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode19',glo:FaultCode19,'Solace_Global',1)
   SolaceViewVars('glo:FaultCode20',glo:FaultCode20,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode1',glo:TradeFaultCode1,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode2',glo:TradeFaultCode2,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode3',glo:TradeFaultCode3,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode4',glo:TradeFaultCode4,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode5',glo:TradeFaultCode5,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode6',glo:TradeFaultCode6,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode7',glo:TradeFaultCode7,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode8',glo:TradeFaultCode8,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode9',glo:TradeFaultCode9,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode10',glo:TradeFaultCode10,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode11',glo:TradeFaultCode11,'Solace_Global',1)
   SolaceViewVars('glo:TradeFaultCode12',glo:TradeFaultCode12,'Solace_Global',1)
   SolaceViewVars('glo:file_name',glo:file_name,'Solace_Global',1)
   SolaceViewVars('glo:RepairTypes',glo:RepairTypes,'Solace_Global',1)
   SolaceViewVars('glo:file_name2',glo:file_name2,'Solace_Global',1)
   SolaceViewVars('glo:file_name3',glo:file_name3,'Solace_Global',1)
   SolaceViewVars('glo:file_name4',glo:file_name4,'Solace_Global',1)
   SolaceViewVars('glo:tradetmp',glo:tradetmp,'Solace_Global',1)
   SolaceViewVars('glo:FileName',glo:FileName,'Solace_Global',1)
   SolaceViewVars('glo:FileName2',glo:FileName2,'Solace_Global',1)
   SolaceViewVars('glo:WebJobConnection',glo:WebJobConnection,'Solace_Global',1)
