  MEMBER('cellmain.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
CELLMBC4:DctInit    PROCEDURE
CELLMBC4:DctKill    PROCEDURE
CELLMBC4:FilesInit  PROCEDURE
  END

Hide:Access:COMMONWP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:COMMONWP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGEXCH  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOGEXCH  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:QAREASON CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:QAREASON CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:COMMONCP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:COMMONCP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ADDSEARCH CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ADDSEARCH CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SIDDEF   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SIDDEF   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CONTHIST CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:CONTHIST CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CONTACTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:CONTACTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXPJOBS  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXPJOBS  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXPAUDIT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXPAUDIT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBBATCH CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBBATCH CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:DEFEDI2  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:DEFEDI2  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:DEFPRINT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:DEFPRINT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:DEFWEB   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:DEFWEB   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXPCITY  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXPCITY  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PROCCODE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
ValidateFieldServer    PROCEDURE(UNSIGNED Id,BYTE HandleErrors),BYTE,PROC,DERIVED
                     END


Hide:Relate:PROCCODE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:COLOUR   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:COLOUR   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:VODAIMP  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:VODAIMP  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSVODA CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
ValidateFieldServer    PROCEDURE(UNSIGNED Id,BYTE HandleErrors),BYTE,PROC,DERIVED
                     END


Hide:Relate:JOBSVODA CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:XREPACT  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:XREPACT  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

CELLMBC4:DctInit PROCEDURE
  CODE
  Relate:COMMONWP &= Hide:Relate:COMMONWP
  Relate:LOGEXCH &= Hide:Relate:LOGEXCH
  Relate:QAREASON &= Hide:Relate:QAREASON
  Relate:COMMONCP &= Hide:Relate:COMMONCP
  Relate:ADDSEARCH &= Hide:Relate:ADDSEARCH
  Relate:SIDDEF &= Hide:Relate:SIDDEF
  Relate:CONTHIST &= Hide:Relate:CONTHIST
  Relate:CONTACTS &= Hide:Relate:CONTACTS
  Relate:EXPJOBS &= Hide:Relate:EXPJOBS
  Relate:EXPAUDIT &= Hide:Relate:EXPAUDIT
  Relate:JOBBATCH &= Hide:Relate:JOBBATCH
  Relate:DEFEDI2 &= Hide:Relate:DEFEDI2
  Relate:DEFPRINT &= Hide:Relate:DEFPRINT
  Relate:DEFWEB &= Hide:Relate:DEFWEB
  Relate:EXPCITY &= Hide:Relate:EXPCITY
  Relate:PROCCODE &= Hide:Relate:PROCCODE
  Relate:COLOUR &= Hide:Relate:COLOUR
  Relate:VODAIMP &= Hide:Relate:VODAIMP
  Relate:JOBSVODA &= Hide:Relate:JOBSVODA
  Relate:XREPACT &= Hide:Relate:XREPACT

CELLMBC4:FilesInit PROCEDURE
  CODE
  Hide:Relate:COMMONWP.Init
  Hide:Relate:LOGEXCH.Init
  Hide:Relate:QAREASON.Init
  Hide:Relate:COMMONCP.Init
  Hide:Relate:ADDSEARCH.Init
  Hide:Relate:SIDDEF.Init
  Hide:Relate:CONTHIST.Init
  Hide:Relate:CONTACTS.Init
  Hide:Relate:EXPJOBS.Init
  Hide:Relate:EXPAUDIT.Init
  Hide:Relate:JOBBATCH.Init
  Hide:Relate:DEFEDI2.Init
  Hide:Relate:DEFPRINT.Init
  Hide:Relate:DEFWEB.Init
  Hide:Relate:EXPCITY.Init
  Hide:Relate:PROCCODE.Init
  Hide:Relate:COLOUR.Init
  Hide:Relate:VODAIMP.Init
  Hide:Relate:JOBSVODA.Init
  Hide:Relate:XREPACT.Init


CELLMBC4:DctKill PROCEDURE
  CODE
  Hide:Relate:COMMONWP.Kill
  Hide:Relate:LOGEXCH.Kill
  Hide:Relate:QAREASON.Kill
  Hide:Relate:COMMONCP.Kill
  Hide:Relate:ADDSEARCH.Kill
  Hide:Relate:SIDDEF.Kill
  Hide:Relate:CONTHIST.Kill
  Hide:Relate:CONTACTS.Kill
  Hide:Relate:EXPJOBS.Kill
  Hide:Relate:EXPAUDIT.Kill
  Hide:Relate:JOBBATCH.Kill
  Hide:Relate:DEFEDI2.Kill
  Hide:Relate:DEFPRINT.Kill
  Hide:Relate:DEFWEB.Kill
  Hide:Relate:EXPCITY.Kill
  Hide:Relate:PROCCODE.Kill
  Hide:Relate:COLOUR.Kill
  Hide:Relate:VODAIMP.Kill
  Hide:Relate:JOBSVODA.Kill
  Hide:Relate:XREPACT.Kill


Hide:Access:COMMONWP.Init PROCEDURE
  CODE
  SELF.Init(COMMONWP,GlobalErrors)
  SELF.Buffer &= cwp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cwp:RecordNumberKey,'cwp:RecordNumberKey',1)
  SELF.AddKey(cwp:Ref_Number_Key,'By Ref Number',0)
  SELF.AddKey(cwp:Description_Key,'By Description',0)
  SELF.AddKey(cwp:RefPartNumberKey,'cwp:RefPartNumberKey',0)
  SELF.AddKey(cwp:PartNumberKey,'By Part Number',0)
  SELF.LazyOpen = False
  Access:COMMONWP &= SELF


Hide:Relate:COMMONWP.Init PROCEDURE
  CODE
  Hide:Access:COMMONWP.Init
  SELF.Init(Access:COMMONWP,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:COMMONFA)


Hide:Access:COMMONWP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:COMMONWP &= NULL


Hide:Access:COMMONWP.PrimeFields PROCEDURE

  CODE
  cwp:Quantity = 1
  PARENT.PrimeFields


Hide:Relate:COMMONWP.Kill PROCEDURE

  CODE
  Hide:Access:COMMONWP.Kill
  PARENT.Kill
  Relate:COMMONWP &= NULL


Hide:Access:LOGEXCH.Init PROCEDURE
  CODE
  SELF.Init(LOGEXCH,GlobalErrors)
  SELF.Buffer &= xch1:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(xch1:Batch_Number_Key,'xch1:Batch_Number_Key',0)
  SELF.AddKey(xch1:Ref_Number_Key,'By Loan Unit Number',1)
  SELF.AddKey(xch1:ESN_Only_Key,'xch1:ESN_Only_Key',0)
  SELF.AddKey(xch1:MSN_Only_Key,'xch1:MSN_Only_Key',0)
  SELF.AddKey(xch1:Ref_Number_Stock_Key,'By Ref Number',0)
  SELF.AddKey(xch1:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(xch1:ESN_Key,'By E.S.N. / I.M.E.I. Number',0)
  SELF.AddKey(xch1:MSN_Key,'By M.S.N. Number',0)
  SELF.AddKey(xch1:ESN_Available_Key,'By E.S.N. / I.M.E.I.',0)
  SELF.AddKey(xch1:MSN_Available_Key,'By M.S.N.',0)
  SELF.AddKey(xch1:Ref_Available_Key,'By Exchange Unit Number',0)
  SELF.AddKey(xch1:Model_Available_Key,'By Model Number',0)
  SELF.AddKey(xch1:Stock_Type_Key,'By Stock Type',0)
  SELF.AddKey(xch1:ModelRefNoKey,'By Unit Number',0)
  SELF.AddKey(xch1:AvailIMEIOnlyKey,'By I.M.E.I. Number',0)
  SELF.AddKey(xch1:AvailMSNOnlyKey,'By M.S.N.',0)
  SELF.AddKey(xch1:AvailRefOnlyKey,'By Unit Number',0)
  SELF.AddKey(xch1:AvailModOnlyKey,'By Unit Number',0)
  SELF.LazyOpen = False
  Access:LOGEXCH &= SELF


Hide:Relate:LOGEXCH.Init PROCEDURE
  CODE
  Hide:Access:LOGEXCH.Init
  SELF.Init(Access:LOGEXCH,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOGEXHE)


Hide:Access:LOGEXCH.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGEXCH &= NULL


Hide:Relate:LOGEXCH.Kill PROCEDURE

  CODE
  Hide:Access:LOGEXCH.Kill
  PARENT.Kill
  Relate:LOGEXCH &= NULL


Hide:Access:QAREASON.Init PROCEDURE
  CODE
  SELF.Init(QAREASON,GlobalErrors)
  SELF.Buffer &= qar:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(qar:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(qar:ReasonKey,'By Reason',0)
  SELF.LazyOpen = False
  Access:QAREASON &= SELF


Hide:Relate:QAREASON.Init PROCEDURE
  CODE
  Hide:Access:QAREASON.Init
  SELF.Init(Access:QAREASON,1)


Hide:Access:QAREASON.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:QAREASON &= NULL


Hide:Relate:QAREASON.Kill PROCEDURE

  CODE
  Hide:Access:QAREASON.Kill
  PARENT.Kill
  Relate:QAREASON &= NULL


Hide:Access:COMMONCP.Init PROCEDURE
  CODE
  SELF.Init(COMMONCP,GlobalErrors)
  SELF.Buffer &= ccp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ccp:RecordNumberKey,'ccp:RecordNumberKey',1)
  SELF.AddKey(ccp:Ref_Number_Key,'By Ref Number',0)
  SELF.AddKey(ccp:Description_Key,'By Description',0)
  SELF.AddKey(ccp:RefPartNumberKey,'ccp:RefPartNumberKey',0)
  SELF.AddKey(ccp:PartNumberKey,'By Part Number',0)
  SELF.LazyOpen = False
  Access:COMMONCP &= SELF


Hide:Relate:COMMONCP.Init PROCEDURE
  CODE
  Hide:Access:COMMONCP.Init
  SELF.Init(Access:COMMONCP,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:COMMONFA)


Hide:Access:COMMONCP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:COMMONCP &= NULL


Hide:Access:COMMONCP.PrimeFields PROCEDURE

  CODE
  ccp:Quantity = 1
  PARENT.PrimeFields


Hide:Relate:COMMONCP.Kill PROCEDURE

  CODE
  Hide:Access:COMMONCP.Kill
  PARENT.Kill
  Relate:COMMONCP &= NULL


Hide:Access:ADDSEARCH.Init PROCEDURE
  CODE
  SELF.Init(ADDSEARCH,GlobalErrors)
  SELF.FileName &= glo:File_Name
  SELF.Buffer &= addtmp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(addtmp:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(addtmp:AddressLine1Key,'By Address',0)
  SELF.AddKey(addtmp:PostcodeKey,'By Postcode',0)
  SELF.AddKey(addtmp:IncomingIMEIKey,'By Incoming I.M.E.I. No',0)
  SELF.AddKey(addtmp:ExchangedIMEIKey,'By Exchanged I.M.E.I. No',0)
  SELF.AddKey(addtmp:FinalIMEIKey,'By Final I.M.E.I. No',0)
  SELF.AddKey(addtmp:JobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:ADDSEARCH &= SELF


Hide:Relate:ADDSEARCH.Init PROCEDURE
  CODE
  Hide:Access:ADDSEARCH.Init
  SELF.Init(Access:ADDSEARCH,1)


Hide:Access:ADDSEARCH.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ADDSEARCH &= NULL


Hide:Relate:ADDSEARCH.Kill PROCEDURE

  CODE
  Hide:Access:ADDSEARCH.Kill
  PARENT.Kill
  Relate:ADDSEARCH &= NULL


Hide:Access:SIDDEF.Init PROCEDURE
  CODE
  SELF.Init(SIDDEF,GlobalErrors)
  SELF.FileNameValue = 'SIDDEF'
  SELF.Buffer &= SID:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(SID:RecordNumberKey,'SID:RecordNumberKey',1)
  SELF.LazyOpen = False
  Access:SIDDEF &= SELF


Hide:Relate:SIDDEF.Init PROCEDURE
  CODE
  Hide:Access:SIDDEF.Init
  SELF.Init(Access:SIDDEF,1)


Hide:Access:SIDDEF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SIDDEF &= NULL


Hide:Relate:SIDDEF.Kill PROCEDURE

  CODE
  Hide:Access:SIDDEF.Kill
  PARENT.Kill
  Relate:SIDDEF &= NULL


Hide:Access:CONTHIST.Init PROCEDURE
  CODE
  SELF.Init(CONTHIST,GlobalErrors)
  SELF.Buffer &= cht:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cht:Ref_Number_Key,'By Date',0)
  SELF.AddKey(cht:Action_Key,'By Action',0)
  SELF.AddKey(cht:User_Key,'By User',0)
  SELF.AddKey(cht:Record_Number_Key,'cht:Record_Number_Key',1)
  SELF.LazyOpen = False
  Access:CONTHIST &= SELF


Hide:Relate:CONTHIST.Init PROCEDURE
  CODE
  Hide:Access:CONTHIST.Init
  SELF.Init(Access:CONTHIST,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:CONTHIST.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CONTHIST &= NULL


Hide:Relate:CONTHIST.Kill PROCEDURE

  CODE
  Hide:Access:CONTHIST.Kill
  PARENT.Kill
  Relate:CONTHIST &= NULL


Hide:Access:CONTACTS.Init PROCEDURE
  CODE
  SELF.Init(CONTACTS,GlobalErrors)
  SELF.Buffer &= con:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(con:Name_Key,'By Name',0)
  SELF.LazyOpen = False
  Access:CONTACTS &= SELF


Hide:Relate:CONTACTS.Init PROCEDURE
  CODE
  Hide:Access:CONTACTS.Init
  SELF.Init(Access:CONTACTS,1)


Hide:Access:CONTACTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CONTACTS &= NULL


Hide:Relate:CONTACTS.Kill PROCEDURE

  CODE
  Hide:Access:CONTACTS.Kill
  PARENT.Kill
  Relate:CONTACTS &= NULL


Hide:Access:EXPJOBS.Init PROCEDURE
  CODE
  SELF.Init(EXPJOBS,GlobalErrors)
  SELF.FileName &= glo:file_name
  SELF.Buffer &= expjob:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:EXPJOBS &= SELF


Hide:Relate:EXPJOBS.Init PROCEDURE
  CODE
  Hide:Access:EXPJOBS.Init
  SELF.Init(Access:EXPJOBS,1)


Hide:Access:EXPJOBS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXPJOBS &= NULL


Hide:Relate:EXPJOBS.Kill PROCEDURE

  CODE
  Hide:Access:EXPJOBS.Kill
  PARENT.Kill
  Relate:EXPJOBS &= NULL


Hide:Access:EXPAUDIT.Init PROCEDURE
  CODE
  SELF.Init(EXPAUDIT,GlobalErrors)
  SELF.FileName &= glo:file_name4
  SELF.Buffer &= expaud:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:EXPAUDIT &= SELF


Hide:Relate:EXPAUDIT.Init PROCEDURE
  CODE
  Hide:Access:EXPAUDIT.Init
  SELF.Init(Access:EXPAUDIT,1)


Hide:Access:EXPAUDIT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXPAUDIT &= NULL


Hide:Relate:EXPAUDIT.Kill PROCEDURE

  CODE
  Hide:Access:EXPAUDIT.Kill
  PARENT.Kill
  Relate:EXPAUDIT &= NULL


Hide:Access:JOBBATCH.Init PROCEDURE
  CODE
  SELF.Init(JOBBATCH,GlobalErrors)
  SELF.Buffer &= jbt:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jbt:Batch_Number_Key,'By Batch Number',1)
  SELF.LazyOpen = False
  Access:JOBBATCH &= SELF


Hide:Relate:JOBBATCH.Init PROCEDURE
  CODE
  Hide:Access:JOBBATCH.Init
  SELF.Init(Access:JOBBATCH,1)


Hide:Access:JOBBATCH.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBBATCH &= NULL


Hide:Access:JOBBATCH.PrimeFields PROCEDURE

  CODE
  jbt:Date = Today()
  jbt:Time = Clock()
  PARENT.PrimeFields


Hide:Relate:JOBBATCH.Kill PROCEDURE

  CODE
  Hide:Access:JOBBATCH.Kill
  PARENT.Kill
  Relate:JOBBATCH &= NULL


Hide:Access:DEFEDI2.Init PROCEDURE
  CODE
  SELF.Init(DEFEDI2,GlobalErrors)
  SELF.Buffer &= ed2:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ed2:record_number_key,'ed2:record_number_key',1)
  SELF.LazyOpen = False
  Access:DEFEDI2 &= SELF


Hide:Relate:DEFEDI2.Init PROCEDURE
  CODE
  Hide:Access:DEFEDI2.Init
  SELF.Init(Access:DEFEDI2,1)


Hide:Access:DEFEDI2.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:DEFEDI2 &= NULL


Hide:Relate:DEFEDI2.Kill PROCEDURE

  CODE
  Hide:Access:DEFEDI2.Kill
  PARENT.Kill
  Relate:DEFEDI2 &= NULL


Hide:Access:DEFPRINT.Init PROCEDURE
  CODE
  SELF.Init(DEFPRINT,GlobalErrors)
  SELF.Buffer &= dep:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(dep:Printer_Name_Key,'By Printer Name',0)
  SELF.LazyOpen = False
  Access:DEFPRINT &= SELF


Hide:Relate:DEFPRINT.Init PROCEDURE
  CODE
  Hide:Access:DEFPRINT.Init
  SELF.Init(Access:DEFPRINT,1)


Hide:Access:DEFPRINT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:DEFPRINT &= NULL


Hide:Access:DEFPRINT.PrimeFields PROCEDURE

  CODE
  dep:Background = 'NO'
  dep:Copies = 1
  PARENT.PrimeFields


Hide:Relate:DEFPRINT.Kill PROCEDURE

  CODE
  Hide:Access:DEFPRINT.Kill
  PARENT.Kill
  Relate:DEFPRINT &= NULL


Hide:Access:DEFWEB.Init PROCEDURE
  CODE
  SELF.Init(DEFWEB,GlobalErrors)
  SELF.Buffer &= dew:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(dew:Record_Number_Key,'dew:Record_Number_Key',1)
  SELF.AddKey(dew:Account_Number_Key,'By Account Number',0)
  SELF.LazyOpen = False
  Access:DEFWEB &= SELF


Hide:Relate:DEFWEB.Init PROCEDURE
  CODE
  Hide:Access:DEFWEB.Init
  SELF.Init(Access:DEFWEB,1)


Hide:Access:DEFWEB.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:DEFWEB &= NULL


Hide:Access:DEFWEB.PrimeFields PROCEDURE

  CODE
  dew:AllowJobBooking = 0
  dew:AllowJobProgress = 0
  dew:AllowOrderParts = 0
  dew:AllowOrderProgress = 0
  dew:Option5 = 0
  dew:Option6 = 0
  dew:Option7 = 0
  dew:Option8 = 0
  dew:Option9 = 0
  dew:Option10 = 0
  PARENT.PrimeFields


Hide:Relate:DEFWEB.Kill PROCEDURE

  CODE
  Hide:Access:DEFWEB.Kill
  PARENT.Kill
  Relate:DEFWEB &= NULL


Hide:Access:EXPCITY.Init PROCEDURE
  CODE
  SELF.Init(EXPCITY,GlobalErrors)
  SELF.FileName &= glo:file_name
  SELF.Buffer &= epc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:EXPCITY &= SELF


Hide:Relate:EXPCITY.Init PROCEDURE
  CODE
  Hide:Access:EXPCITY.Init
  SELF.Init(Access:EXPCITY,1)


Hide:Access:EXPCITY.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXPCITY &= NULL


Hide:Relate:EXPCITY.Kill PROCEDURE

  CODE
  Hide:Access:EXPCITY.Kill
  PARENT.Kill
  Relate:EXPCITY &= NULL


Hide:Access:PROCCODE.Init PROCEDURE
  CODE
  SELF.Init(PROCCODE,GlobalErrors)
  SELF.Buffer &= pro:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(pro:Code_Number_Key,'By Code Number',0)
  SELF.LazyOpen = False
  Access:PROCCODE &= SELF


Hide:Relate:PROCCODE.Init PROCEDURE
  CODE
  Hide:Access:PROCCODE.Init
  SELF.Init(Access:PROCCODE,1)


Hide:Access:PROCCODE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PROCCODE &= NULL


Hide:Access:PROCCODE.ValidateFieldServer PROCEDURE(UNSIGNED Id,BYTE HandleErrors)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateFieldServer(Id,HandleErrors)
  CASE Id
  OF 3
    GlobalErrors.SetField('pro:Allow_Loan')
    IF INSTRING(']' & CLIP(pro:Allow_Loan) & '[', ']YES[]NO[', 1, 1) = 0
      ReturnValue = Level:Notify
    END
    IF ReturnValue <> Level:Benign
      IF HandleErrors
        ReturnValue = GlobalErrors.ThrowMessage(Msg:FieldNotInList,'''YES'',''NO''')
      END
    END
  END
  RETURN ReturnValue


Hide:Relate:PROCCODE.Kill PROCEDURE

  CODE
  Hide:Access:PROCCODE.Kill
  PARENT.Kill
  Relate:PROCCODE &= NULL


Hide:Access:COLOUR.Init PROCEDURE
  CODE
  SELF.Init(COLOUR,GlobalErrors)
  SELF.Buffer &= col:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(col:Colour_Key,'By Colour',0)
  SELF.LazyOpen = False
  Access:COLOUR &= SELF


Hide:Relate:COLOUR.Init PROCEDURE
  CODE
  Hide:Access:COLOUR.Init
  SELF.Init(Access:COLOUR,1)


Hide:Access:COLOUR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:COLOUR &= NULL


Hide:Relate:COLOUR.Kill PROCEDURE

  CODE
  Hide:Access:COLOUR.Kill
  PARENT.Kill
  Relate:COLOUR &= NULL


Hide:Access:VODAIMP.Init PROCEDURE
  CODE
  SELF.Init(VODAIMP,GlobalErrors)
  SELF.FileName &= glo:file_name
  SELF.Buffer &= vdi:Record
  SELF.Create = 0
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:VODAIMP &= SELF


Hide:Relate:VODAIMP.Init PROCEDURE
  CODE
  Hide:Access:VODAIMP.Init
  SELF.Init(Access:VODAIMP,1)


Hide:Access:VODAIMP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:VODAIMP &= NULL


Hide:Relate:VODAIMP.Kill PROCEDURE

  CODE
  Hide:Access:VODAIMP.Kill
  PARENT.Kill
  Relate:VODAIMP &= NULL


Hide:Access:JOBSVODA.Init PROCEDURE
  CODE
  SELF.Init(JOBSVODA,GlobalErrors)
  SELF.Buffer &= jvf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jvf:Ref_Number_Key,'By Ref Number',1)
  SELF.AddKey(jvf:Ref_Pending_Key,'By Ref Number',0)
  SELF.AddKey(jvf:Order_Number_Key,'By Order Number',0)
  SELF.AddKey(jvf:Process_Code_Key,'By Process Code',0)
  SELF.AddKey(jvf:Model_Number_Key,'By Unit Type',0)
  SELF.AddKey(jvf:ESN_Key,'By ESN',0)
  SELF.AddKey(jvf:Job_Number_Key,'By Job Number',0)
  SELF.AddKey(jvf:Model_Ref_key,'By Model Number',0)
  SELF.LazyOpen = False
  Access:JOBSVODA &= SELF


Hide:Relate:JOBSVODA.Init PROCEDURE
  CODE
  Hide:Access:JOBSVODA.Init
  SELF.Init(Access:JOBSVODA,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBVODAC,RI:CASCADE,RI:CASCADE,jva:Ref_Number_Key)
  SELF.AddRelationLink(jvf:Ref_Number,jva:Ref_Number)


Hide:Access:JOBSVODA.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSVODA &= NULL


Hide:Access:JOBSVODA.PrimeFields PROCEDURE

  CODE
  jvf:Pending = 'NO'
  PARENT.PrimeFields


Hide:Access:JOBSVODA.ValidateFieldServer PROCEDURE(UNSIGNED Id,BYTE HandleErrors)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateFieldServer(Id,HandleErrors)
  CASE Id
  OF 33
    GlobalErrors.SetField('jvf:Pending')
    IF INSTRING(']' & CLIP(jvf:Pending) & '[', ']NO[]YES[', 1, 1) = 0
      ReturnValue = Level:Notify
    END
    IF ReturnValue <> Level:Benign
      IF HandleErrors
        ReturnValue = GlobalErrors.ThrowMessage(Msg:FieldNotInList,'''NO'',''YES''')
      END
    END
  END
  RETURN ReturnValue


Hide:Relate:JOBSVODA.Kill PROCEDURE

  CODE
  Hide:Access:JOBSVODA.Kill
  PARENT.Kill
  Relate:JOBSVODA &= NULL


Hide:Access:XREPACT.Init PROCEDURE
  CODE
  SELF.Init(XREPACT,GlobalErrors)
  SELF.FileName &= glo:File_Name
  SELF.Buffer &= xre:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:XREPACT &= SELF


Hide:Relate:XREPACT.Init PROCEDURE
  CODE
  Hide:Access:XREPACT.Init
  SELF.Init(Access:XREPACT,1)


Hide:Access:XREPACT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:XREPACT &= NULL


Hide:Relate:XREPACT.Kill PROCEDURE

  CODE
  Hide:Access:XREPACT.Kill
  PARENT.Kill
  Relate:XREPACT &= NULL

