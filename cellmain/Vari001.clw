         MEMBER('cellmain.clw')                       ! This is a MEMBER module

   Map
   End

SolDictFiles1        Procedure
  Code

     if Status(STDCHRGE) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('Charge_Type',sta:Charge_Type,'Solace_Files',1,'STDCHRGE')
       SolaceViewVars('Model_Number',sta:Model_Number,'Solace_Files',1,'STDCHRGE')
       SolaceViewVars('Unit_Type',sta:Unit_Type,'Solace_Files',1,'STDCHRGE')
       SolaceViewVars('Repair_Type',sta:Repair_Type,'Solace_Files',1,'STDCHRGE')
       SolaceViewVars('Cost',sta:Cost,'Solace_Files',1,'STDCHRGE')
       SolaceViewVars('dummy',sta:dummy,'Solace_Files',1,'STDCHRGE')
     End
     if Status(AccSKU) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('AccessSKUID',ASKU:AccessSKUID,'Solace_Files',1,'AccSKU')
       SolaceViewVars('OracleCode',ASKU:OracleCode,'Solace_Files',1,'AccSKU')
       SolaceViewVars('AccessoryName',ASKU:AccessoryName,'Solace_Files',1,'AccSKU')
       SolaceViewVars('AccessorySKUCode',ASKU:AccessorySKUCode,'Solace_Files',1,'AccSKU')
     End
     if Status(PROCFILE) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',PROC:RecordNumber,'Solace_Files',1,'PROCFILE')
       SolaceViewVars('FileName',PROC:FileName,'Solace_Files',1,'PROCFILE')
     End
     if Status(LOCDEFS) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',locdef:RecordNumber,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('FTPServer',locdef:FTPServer,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('FTPUserName',locdef:FTPUserName,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('FTPPassword',locdef:FTPPassword,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('FTPImportPath',locdef:FTPImportPath,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('FTPExportPath',locdef:FTPExportPath,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('EmailServer',locdef:EmailServer,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('EmailServerPort',locdef:EmailServerPort,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('EmailFromAddress',locdef:EmailFromAddress,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('CRCEmailName',locdef:CRCEmailName,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('CRCEmailAddress',locdef:CRCEmailAddress,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('PCCSEmailName',locdef:PCCSEmailName,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('PCCSEmailAddress',locdef:PCCSEmailAddress,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('VodafoneEmailName',locdef:VodafoneEmailName,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('VodafoneEmailAddress',locdef:VodafoneEmailAddress,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('AlertDays',locdef:AlertDays,'Solace_Files',1,'LOCDEFS')
       SolaceViewVars('LastImportDate',locdef:LastImportDate,'Solace_Files',1,'LOCDEFS')
     End
     if Status(CARISMA) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',cma:RecordNumber,'Solace_Files',1,'CARISMA')
       SolaceViewVars('Manufacturer',cma:Manufacturer,'Solace_Files',1,'CARISMA')
       SolaceViewVars('ModelNo',cma:ModelNo,'Solace_Files',1,'CARISMA')
       SolaceViewVars('Colour',cma:Colour,'Solace_Files',1,'CARISMA')
       SolaceViewVars('ContractOracleCode',cma:ContractOracleCode,'Solace_Files',1,'CARISMA')
       SolaceViewVars('PAYTOracleCode',cma:PAYTOracleCode,'Solace_Files',1,'CARISMA')
       SolaceViewVars('ContractActive',cma:ContractActive,'Solace_Files',1,'CARISMA')
       SolaceViewVars('PAYTActive',cma:PAYTActive,'Solace_Files',1,'CARISMA')
     End
     if Status(DEFEMAIL) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',dem:RecordNumber,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('SMTPServer',dem:SMTPServer,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('SMTPPort',dem:SMTPPort,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('SMTPUserName',dem:SMTPUserName,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('SMTPPassword',dem:SMTPPassword,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('EmailFromAddress',dem:EmailFromAddress,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('SMSToAddress',dem:SMSToAddress,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('EmailToAddress',dem:EmailToAddress,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('AdminToAddress',dem:AdminToAddress,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('DespFromSCText',dem:DespFromSCText,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('ArrivedAtRetailText',dem:ArrivedAtRetailText,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('SMSFTPAddress',dem:SMSFTPAddress,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('SMSFTPUsername',dem:SMSFTPUsername,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('SMSFTPPassword',dem:SMSFTPPassword,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('SMSFTPLocation',dem:SMSFTPLocation,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('R1Active',dem:R1Active,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('R2Active',dem:R2Active,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('R3Active',dem:R3Active,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('R1SMSActive',dem:R1SMSActive,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('R2SMSActive',dem:R2SMSActive,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('R3SMSActive',dem:R3SMSActive,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('BookedRetailText',dem:BookedRetailText,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('R4Active',dem:R4Active,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('R4SMSActive',dem:R4SMSActive,'Solace_Files',1,'DEFEMAIL')
       SolaceViewVars('DespFromStoreText',dem:DespFromStoreText,'Solace_Files',1,'DEFEMAIL')
     End
     if Status(CRCDEFS) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',crcdef:RecordNumber,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('FTPServer',crcdef:FTPServer,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('FTPUserName',crcdef:FTPUserName,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('FTPPassword',crcdef:FTPPassword,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('FTPImportPath',crcdef:FTPImportPath,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('FTPExportPath',crcdef:FTPExportPath,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('EmailServer',crcdef:EmailServer,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('EmailServerPort',crcdef:EmailServerPort,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('EmailFromAddress',crcdef:EmailFromAddress,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('CRCEmailName',crcdef:CRCEmailName,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('CRCEmailAddress',crcdef:CRCEmailAddress,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('PCCSEmailName',crcdef:PCCSEmailName,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('PCCSEmailAddress',crcdef:PCCSEmailAddress,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('VodafoneEmailName',crcdef:VodafoneEmailName,'Solace_Files',1,'CRCDEFS')
       SolaceViewVars('VodafoneEmailAddress',crcdef:VodafoneEmailAddress,'Solace_Files',1,'CRCDEFS')
     End
     if Status(REGIONS) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RegionID',reg:RegionID,'Solace_Files',1,'REGIONS')
       SolaceViewVars('RegionName',reg:RegionName,'Solace_Files',1,'REGIONS')
       SolaceViewVars('BankHolidaysFileName',reg:BankHolidaysFileName,'Solace_Files',1,'REGIONS')
     End
     if Status(ACCREG) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('AccRegID',acg:AccRegID,'Solace_Files',1,'ACCREG')
       SolaceViewVars('AccountNo',acg:AccountNo,'Solace_Files',1,'ACCREG')
       SolaceViewVars('RegionName',acg:RegionName,'Solace_Files',1,'ACCREG')
     End
     if Status(SIDACREG) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',sar:RecordNumber,'Solace_Files',1,'SIDACREG')
       SolaceViewVars('SCAccountID',sar:SCAccountID,'Solace_Files',1,'SIDACREG')
       SolaceViewVars('AccRegID',sar:AccRegID,'Solace_Files',1,'SIDACREG')
       SolaceViewVars('TransitDaysException',sar:TransitDaysException,'Solace_Files',1,'SIDACREG')
       SolaceViewVars('TransitDaysToSC',sar:TransitDaysToSC,'Solace_Files',1,'SIDACREG')
       SolaceViewVars('TransitDaysToStore',sar:TransitDaysToStore,'Solace_Files',1,'SIDACREG')
     End
     if Status(XMLJOBS) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RECORD_NUMBER',XJB:RECORD_NUMBER,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('RECORD_STATE',XJB:RECORD_STATE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('EXCEPTION_CODE',XJB:EXCEPTION_CODE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('EXCEPTION_DESC',XJB:EXCEPTION_DESC,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('JOB_NO',XJB:JOB_NO,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('COMPANY',XJB:COMPANY,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('TR_NO',XJB:TR_NO,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('ASC_CODE',XJB:ASC_CODE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('MODEL_CODE',XJB:MODEL_CODE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('SERIAL_NO',XJB:SERIAL_NO,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('PURCHASE_DATE',XJB:PURCHASE_DATE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('SERVICE_TYPE',XJB:SERVICE_TYPE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('IN_OUT_WARRANTY',XJB:IN_OUT_WARRANTY,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('REQUEST_DATE',XJB:REQUEST_DATE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('REQUEST_TIME',XJB:REQUEST_TIME,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('REPAIR_ETD_DATE',XJB:REPAIR_ETD_DATE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('REPAIR_ETD_TIME',XJB:REPAIR_ETD_TIME,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('TR_STATUS',XJB:TR_STATUS,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('TR_REASON',XJB:TR_REASON,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_TITLE',XJB:CONSUMER_TITLE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_FIRST_NAME',XJB:CONSUMER_FIRST_NAME,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_LAST_NAME',XJB:CONSUMER_LAST_NAME,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_TEL_NUMBER1',XJB:CONSUMER_TEL_NUMBER1,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_FAX_NUMBER',XJB:CONSUMER_FAX_NUMBER,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_EMAIL',XJB:CONSUMER_EMAIL,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_COUNTRY',XJB:CONSUMER_COUNTRY,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_REGION',XJB:CONSUMER_REGION,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_POSTCODE',XJB:CONSUMER_POSTCODE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_CITY',XJB:CONSUMER_CITY,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_STREET',XJB:CONSUMER_STREET,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('CONSUMER_HOUSE_NUMBER',XJB:CONSUMER_HOUSE_NUMBER,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('REPAIR_COMPLETE_DATE',XJB:REPAIR_COMPLETE_DATE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('REPAIR_COMPLETE_TIME',XJB:REPAIR_COMPLETE_TIME,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('GOODS_DELIVERY_DATE',XJB:GOODS_DELIVERY_DATE,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('GOODS_DELIVERY_TIME',XJB:GOODS_DELIVERY_TIME,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('SYMPTOM1_DESC',XJB:SYMPTOM1_DESC,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('SYMPTOM2_DESC',XJB:SYMPTOM2_DESC,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('SYMPTOM3_DESC',XJB:SYMPTOM3_DESC,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('BP_NO',XJB:BP_NO,'Solace_Files',1,'XMLJOBS')
       SolaceViewVars('INQUIRY_TEXT',XJB:INQUIRY_TEXT,'Solace_Files',1,'XMLJOBS')
     End
     if Status(EXCHAMF) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('Audit_Number',emf:Audit_Number,'Solace_Files',1,'EXCHAMF')
       SolaceViewVars('Date',emf:Date,'Solace_Files',1,'EXCHAMF')
       SolaceViewVars('Time',emf:Time,'Solace_Files',1,'EXCHAMF')
       SolaceViewVars('User',emf:User,'Solace_Files',1,'EXCHAMF')
       SolaceViewVars('Status',emf:Status,'Solace_Files',1,'EXCHAMF')
       SolaceViewVars('Complete_Flag',emf:Complete_Flag,'Solace_Files',1,'EXCHAMF')
     End
     if Status(EXCHAUI) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('Internal_No',eau:Internal_No,'Solace_Files',1,'EXCHAUI')
       SolaceViewVars('Audit_Number',eau:Audit_Number,'Solace_Files',1,'EXCHAUI')
       SolaceViewVars('Ref_Number',eau:Ref_Number,'Solace_Files',1,'EXCHAUI')
       SolaceViewVars('Exists',eau:Exists,'Solace_Files',1,'EXCHAUI')
       SolaceViewVars('New_IMEI',eau:New_IMEI,'Solace_Files',1,'EXCHAUI')
     End
     if Status(JOBSENG) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',joe:RecordNumber,'Solace_Files',1,'JOBSENG')
       SolaceViewVars('JobNumber',joe:JobNumber,'Solace_Files',1,'JOBSENG')
       SolaceViewVars('UserCode',joe:UserCode,'Solace_Files',1,'JOBSENG')
       SolaceViewVars('DateAllocated',joe:DateAllocated,'Solace_Files',1,'JOBSENG')
       SolaceViewVars('TimeAllocated',joe:TimeAllocated,'Solace_Files',1,'JOBSENG')
       SolaceViewVars('AllocatedBy',joe:AllocatedBy,'Solace_Files',1,'JOBSENG')
       SolaceViewVars('EngSkillLevel',joe:EngSkillLevel,'Solace_Files',1,'JOBSENG')
       SolaceViewVars('JobSkillLevel',joe:JobSkillLevel,'Solace_Files',1,'JOBSENG')
     End
     if Status(MULDESPJ) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',mulj:RecordNumber,'Solace_Files',1,'MULDESPJ')
       SolaceViewVars('RefNumber',mulj:RefNumber,'Solace_Files',1,'MULDESPJ')
       SolaceViewVars('JobNumber',mulj:JobNumber,'Solace_Files',1,'MULDESPJ')
       SolaceViewVars('IMEINumber',mulj:IMEINumber,'Solace_Files',1,'MULDESPJ')
       SolaceViewVars('MSN',mulj:MSN,'Solace_Files',1,'MULDESPJ')
       SolaceViewVars('AccountNumber',mulj:AccountNumber,'Solace_Files',1,'MULDESPJ')
       SolaceViewVars('Courier',mulj:Courier,'Solace_Files',1,'MULDESPJ')
       SolaceViewVars('Current',mulj:Current,'Solace_Files',1,'MULDESPJ')
     End
     if Status(AUDSTAEX) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',aux:RecordNumber,'Solace_Files',1,'AUDSTAEX')
       SolaceViewVars('RefNumber',aux:RefNumber,'Solace_Files',1,'AUDSTAEX')
       SolaceViewVars('AgentName',aux:AgentName,'Solace_Files',1,'AUDSTAEX')
     End
     if Status(MULDESP) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',muld:RecordNumber,'Solace_Files',1,'MULDESP')
       SolaceViewVars('BatchNumber',muld:BatchNumber,'Solace_Files',1,'MULDESP')
       SolaceViewVars('AccountNumber',muld:AccountNumber,'Solace_Files',1,'MULDESP')
       SolaceViewVars('Courier',muld:Courier,'Solace_Files',1,'MULDESP')
       SolaceViewVars('BatchTotal',muld:BatchTotal,'Solace_Files',1,'MULDESP')
     End
     if Status(INSMODELS) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('ModelNo',INS3:ModelNo,'Solace_Files',1,'INSMODELS')
       SolaceViewVars('MakeNo',INS3:MakeNo,'Solace_Files',1,'INSMODELS')
       SolaceViewVars('ModelNumber',INS3:ModelNumber,'Solace_Files',1,'INSMODELS')
     End
     if Status(STOCKLOG) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('Audit_Number',sal:Audit_Number,'Solace_Files',1,'STOCKLOG')
       SolaceViewVars('Date_created',sal:Date_created,'Solace_Files',1,'STOCKLOG')
       SolaceViewVars('Time_Created',sal:Time_Created,'Solace_Files',1,'STOCKLOG')
       SolaceViewVars('User',sal:User,'Solace_Files',1,'STOCKLOG')
       SolaceViewVars('Checked_Date',sal:Checked_Date,'Solace_Files',1,'STOCKLOG')
       SolaceViewVars('Checked_Time',sal:Checked_Time,'Solace_Files',1,'STOCKLOG')
       SolaceViewVars('Checked_By',sal:Checked_By,'Solace_Files',1,'STOCKLOG')
       SolaceViewVars('Status',sal:Status,'Solace_Files',1,'STOCKLOG')
       SolaceViewVars('Scanned_Count',sal:Scanned_Count,'Solace_Files',1,'STOCKLOG')
       SolaceViewVars('Stock_Status',sal:Stock_Status,'Solace_Files',1,'STOCKLOG')
     End
     if Status(IMEILOG) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',ime:RecordNumber,'Solace_Files',1,'IMEILOG')
       SolaceViewVars('Audit_Number',ime:Audit_Number,'Solace_Files',1,'IMEILOG')
       SolaceViewVars('IMEI_Number',ime:IMEI_Number,'Solace_Files',1,'IMEILOG')
       SolaceViewVars('Stock_Type',ime:Stock_Type,'Solace_Files',1,'IMEILOG')
       SolaceViewVars('Status_Code',ime:Status_Code,'Solace_Files',1,'IMEILOG')
       SolaceViewVars('Model_No',ime:Model_No,'Solace_Files',1,'IMEILOG')
       SolaceViewVars('Manufacturer',ime:Manufacturer,'Solace_Files',1,'IMEILOG')
     End
     if Status(STMASAUD) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('Audit_No',stom:Audit_No,'Solace_Files',1,'STMASAUD')
       SolaceViewVars('branch',stom:branch,'Solace_Files',1,'STMASAUD')
       SolaceViewVars('branch_id',stom:branch_id,'Solace_Files',1,'STMASAUD')
       SolaceViewVars('Audit_Date',stom:Audit_Date,'Solace_Files',1,'STMASAUD')
       SolaceViewVars('Audit_Time',stom:Audit_Time,'Solace_Files',1,'STMASAUD')
       SolaceViewVars('End_Date',stom:End_Date,'Solace_Files',1,'STMASAUD')
       SolaceViewVars('End_Time',stom:End_Time,'Solace_Files',1,'STMASAUD')
       SolaceViewVars('Audit_User',stom:Audit_User,'Solace_Files',1,'STMASAUD')
       SolaceViewVars('Complete',stom:Complete,'Solace_Files',1,'STMASAUD')
       SolaceViewVars('Send',stom:Send,'Solace_Files',1,'STMASAUD')
     End
     if Status(AUDSTATS) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',aus:RecordNumber,'Solace_Files',1,'AUDSTATS')
       SolaceViewVars('RefNumber',aus:RefNumber,'Solace_Files',1,'AUDSTATS')
       SolaceViewVars('Type',aus:Type,'Solace_Files',1,'AUDSTATS')
       SolaceViewVars('DateChanged',aus:DateChanged,'Solace_Files',1,'AUDSTATS')
       SolaceViewVars('TimeChanged',aus:TimeChanged,'Solace_Files',1,'AUDSTATS')
       SolaceViewVars('OldStatus',aus:OldStatus,'Solace_Files',1,'AUDSTATS')
       SolaceViewVars('NewStatus',aus:NewStatus,'Solace_Files',1,'AUDSTATS')
       SolaceViewVars('UserCode',aus:UserCode,'Solace_Files',1,'AUDSTATS')
     End
     if Status(IEXPDEFS) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecNo',iex:RecNo,'Solace_Files',1,'IEXPDEFS')
       SolaceViewVars('Name',iex:Name,'Solace_Files',1,'IEXPDEFS')
       SolaceViewVars('Type',iex:Type,'Solace_Files',1,'IEXPDEFS')
     End
     if Status(RAPIDLST) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',RAP:RecordNumber,'Solace_Files',1,'RAPIDLST')
       SolaceViewVars('RapidOrder',RAP:RapidOrder,'Solace_Files',1,'RAPIDLST')
       SolaceViewVars('ProgramName',RAP:ProgramName,'Solace_Files',1,'RAPIDLST')
       SolaceViewVars('Description',RAP:Description,'Solace_Files',1,'RAPIDLST')
       SolaceViewVars('EXEPath',RAP:EXEPath,'Solace_Files',1,'RAPIDLST')
     End
     if Status(ICONTHIST) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',ico:RecordNumber,'Solace_Files',1,'ICONTHIST')
       SolaceViewVars('RefNumber',ico:RefNumber,'Solace_Files',1,'ICONTHIST')
       SolaceViewVars('Date',ico:Date,'Solace_Files',1,'ICONTHIST')
       SolaceViewVars('Time',ico:Time,'Solace_Files',1,'ICONTHIST')
       SolaceViewVars('User',ico:User,'Solace_Files',1,'ICONTHIST')
       SolaceViewVars('Action',ico:Action,'Solace_Files',1,'ICONTHIST')
       SolaceViewVars('Notes',ico:Notes,'Solace_Files',1,'ICONTHIST')
       SolaceViewVars('Dummy',ico:Dummy,'Solace_Files',1,'ICONTHIST')
     End
     if Status(ORDTEMP) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('recordnumber',ort:recordnumber,'Solace_Files',1,'ORDTEMP')
       SolaceViewVars('ordhno',ort:ordhno,'Solace_Files',1,'ORDTEMP')
       SolaceViewVars('manufact',ort:manufact,'Solace_Files',1,'ORDTEMP')
       SolaceViewVars('partno',ort:partno,'Solace_Files',1,'ORDTEMP')
       SolaceViewVars('partdiscription',ort:partdiscription,'Solace_Files',1,'ORDTEMP')
       SolaceViewVars('qty',ort:qty,'Solace_Files',1,'ORDTEMP')
       SolaceViewVars('itemcost',ort:itemcost,'Solace_Files',1,'ORDTEMP')
       SolaceViewVars('totalcost',ort:totalcost,'Solace_Files',1,'ORDTEMP')
       SolaceViewVars('thedate',ort:thedate,'Solace_Files',1,'ORDTEMP')
     End
     if Status(ORDITEMS) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('recordnumber',ori:recordnumber,'Solace_Files',1,'ORDITEMS')
       SolaceViewVars('ordhno',ori:ordhno,'Solace_Files',1,'ORDITEMS')
       SolaceViewVars('manufact',ori:manufact,'Solace_Files',1,'ORDITEMS')
       SolaceViewVars('partno',ori:partno,'Solace_Files',1,'ORDITEMS')
       SolaceViewVars('partdiscription',ori:partdiscription,'Solace_Files',1,'ORDITEMS')
       SolaceViewVars('qty',ori:qty,'Solace_Files',1,'ORDITEMS')
       SolaceViewVars('itemcost',ori:itemcost,'Solace_Files',1,'ORDITEMS')
       SolaceViewVars('totalcost',ori:totalcost,'Solace_Files',1,'ORDITEMS')
     End
     if Status(ORDHEAD) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('Order_no',orh:Order_no,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('account_no',orh:account_no,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('CustName',orh:CustName,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('CustAdd1',orh:CustAdd1,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('CustAdd2',orh:CustAdd2,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('CustAdd3',orh:CustAdd3,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('CustPostCode',orh:CustPostCode,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('CustTel',orh:CustTel,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('CustFax',orh:CustFax,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('dName',orh:dName,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('dAdd1',orh:dAdd1,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('dAdd2',orh:dAdd2,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('dAdd3',orh:dAdd3,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('dPostCode',orh:dPostCode,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('dTel',orh:dTel,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('dFax',orh:dFax,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('thedate',orh:thedate,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('thetime',orh:thetime,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('procesed',orh:procesed,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('pro_date',orh:pro_date,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('WhoBooked',orh:WhoBooked,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('Department',orh:Department,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('CustOrderNumber',orh:CustOrderNumber,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('DateDespatched',orh:DateDespatched,'Solace_Files',1,'ORDHEAD')
       SolaceViewVars('SalesNumber',orh:SalesNumber,'Solace_Files',1,'ORDHEAD')
     End
     if Status(IMEISHIP) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',IMEI:RecordNumber,'Solace_Files',1,'IMEISHIP')
       SolaceViewVars('IMEINumber',IMEI:IMEINumber,'Solace_Files',1,'IMEISHIP')
       SolaceViewVars('ShipDate',IMEI:ShipDate,'Solace_Files',1,'IMEISHIP')
       SolaceViewVars('BER',IMEI:BER,'Solace_Files',1,'IMEISHIP')
       SolaceViewVars('ManualEntry',IMEI:ManualEntry,'Solace_Files',1,'IMEISHIP')
       SolaceViewVars('ProductCode',IMEI:ProductCode,'Solace_Files',1,'IMEISHIP')
     End
     if Status(IACTION) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('ActionNo',iac:ActionNo,'Solace_Files',1,'IACTION')
       SolaceViewVars('Action',iac:Action,'Solace_Files',1,'IACTION')
     End
     if Status(JOBTHIRD) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',jot:RecordNumber,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('RefNumber',jot:RefNumber,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('OriginalIMEI',jot:OriginalIMEI,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('OutIMEI',jot:OutIMEI,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('InIMEI',jot:InIMEI,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('DateOut',jot:DateOut,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('DateDespatched',jot:DateDespatched,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('DateIn',jot:DateIn,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('ThirdPartyNumber',jot:ThirdPartyNumber,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('OriginalMSN',jot:OriginalMSN,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('OutMSN',jot:OutMSN,'Solace_Files',1,'JOBTHIRD')
       SolaceViewVars('InMSN',jot:InMSN,'Solace_Files',1,'JOBTHIRD')
     End
     if Status(DEFAULTV) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',defv:RecordNumber,'Solace_Files',1,'DEFAULTV')
       SolaceViewVars('VersionNumber',defv:VersionNumber,'Solace_Files',1,'DEFAULTV')
       SolaceViewVars('NagDate',defv:NagDate,'Solace_Files',1,'DEFAULTV')
       SolaceViewVars('ReUpdate',defv:ReUpdate,'Solace_Files',1,'DEFAULTV')
     End
     if Status(LOGRETRN) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RefNumber',lrtn:RefNumber,'Solace_Files',1,'LOGRETRN')
       SolaceViewVars('ClubNokiaSite',lrtn:ClubNokiaSite,'Solace_Files',1,'LOGRETRN')
       SolaceViewVars('ModelNumber',lrtn:ModelNumber,'Solace_Files',1,'LOGRETRN')
       SolaceViewVars('Quantity',lrtn:Quantity,'Solace_Files',1,'LOGRETRN')
       SolaceViewVars('ReturnsNumber',lrtn:ReturnsNumber,'Solace_Files',1,'LOGRETRN')
       SolaceViewVars('Date',lrtn:Date,'Solace_Files',1,'LOGRETRN')
       SolaceViewVars('ReturnType',lrtn:ReturnType,'Solace_Files',1,'LOGRETRN')
       SolaceViewVars('DummyField',lrtn:DummyField,'Solace_Files',1,'LOGRETRN')
       SolaceViewVars('To_Site',lrtn:To_Site,'Solace_Files',1,'LOGRETRN')
       SolaceViewVars('SalesCode',lrtn:SalesCode,'Solace_Files',1,'LOGRETRN')
     End
     if Status(PRODCODE) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',prd:RecordNumber,'Solace_Files',1,'PRODCODE')
       SolaceViewVars('ModelNumber',prd:ModelNumber,'Solace_Files',1,'PRODCODE')
       SolaceViewVars('ProductCode',prd:ProductCode,'Solace_Files',1,'PRODCODE')
       SolaceViewVars('WarrantyPeriod',prd:WarrantyPeriod,'Solace_Files',1,'PRODCODE')
     End
     if Status(LOG2TEMP) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RefNumber',lo2tmp:RefNumber,'Solace_Files',1,'LOG2TEMP')
       SolaceViewVars('IMEI',lo2tmp:IMEI,'Solace_Files',1,'LOG2TEMP')
       SolaceViewVars('Marker',lo2tmp:Marker,'Solace_Files',1,'LOG2TEMP')
     End
     if Status(LOGSTOCK) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RefNumber',logsto:RefNumber,'Solace_Files',1,'LOGSTOCK')
       SolaceViewVars('SalesCode',logsto:SalesCode,'Solace_Files',1,'LOGSTOCK')
       SolaceViewVars('Description',logsto:Description,'Solace_Files',1,'LOGSTOCK')
       SolaceViewVars('ModelNumber',logsto:ModelNumber,'Solace_Files',1,'LOGSTOCK')
       SolaceViewVars('DummyField',logsto:DummyField,'Solace_Files',1,'LOGSTOCK')
     End
     if Status(DEFEMAR1) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',der1:RecordNumber,'Solace_Files',1,'DEFEMAR1')
       SolaceViewVars('R1Email',der1:R1Email,'Solace_Files',1,'DEFEMAR1')
       SolaceViewVars('Subject',der1:Subject,'Solace_Files',1,'DEFEMAR1')
     End
     if Status(LETTERS) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',let:RecordNumber,'Solace_Files',1,'LETTERS')
       SolaceViewVars('Description',let:Description,'Solace_Files',1,'LETTERS')
       SolaceViewVars('Subject',let:Subject,'Solace_Files',1,'LETTERS')
       SolaceViewVars('TelephoneNumber',let:TelephoneNumber,'Solace_Files',1,'LETTERS')
       SolaceViewVars('FaxNumber',let:FaxNumber,'Solace_Files',1,'LETTERS')
       SolaceViewVars('LetterText',let:LetterText,'Solace_Files',1,'LETTERS')
       SolaceViewVars('UseStatus',let:UseStatus,'Solace_Files',1,'LETTERS')
       SolaceViewVars('Status',let:Status,'Solace_Files',1,'LETTERS')
       SolaceViewVars('DummyField',let:DummyField,'Solace_Files',1,'LETTERS')
     End
     if Status(DEFEMAB2) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',deb2:RecordNumber,'Solace_Files',1,'DEFEMAB2')
       SolaceViewVars('B2Email',deb2:B2Email,'Solace_Files',1,'DEFEMAB2')
       SolaceViewVars('Subject',deb2:Subject,'Solace_Files',1,'DEFEMAB2')
     End
     if Status(DEFEMAB3) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',deb3:RecordNumber,'Solace_Files',1,'DEFEMAB3')
       SolaceViewVars('B3Email',deb3:B3Email,'Solace_Files',1,'DEFEMAB3')
       SolaceViewVars('Subject',deb3:Subject,'Solace_Files',1,'DEFEMAB3')
     End
     if Status(DEFEMAB4) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',deb4:RecordNumber,'Solace_Files',1,'DEFEMAB4')
       SolaceViewVars('B4Email',deb4:B4Email,'Solace_Files',1,'DEFEMAB4')
       SolaceViewVars('Subject',deb4:Subject,'Solace_Files',1,'DEFEMAB4')
     End
     if Status(DEFEMAB5) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',deb5:RecordNumber,'Solace_Files',1,'DEFEMAB5')
       SolaceViewVars('B5Email',deb5:B5Email,'Solace_Files',1,'DEFEMAB5')
       SolaceViewVars('Subject',deb5:Subject,'Solace_Files',1,'DEFEMAB5')
     End
     if Status(DEFEMAB6) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',deb6:RecordNumber,'Solace_Files',1,'DEFEMAB6')
       SolaceViewVars('B6Email',deb6:B6Email,'Solace_Files',1,'DEFEMAB6')
       SolaceViewVars('Subject',deb6:Subject,'Solace_Files',1,'DEFEMAB6')
     End
     if Status(DEFEMAB7) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',deb7:RecordNumber,'Solace_Files',1,'DEFEMAB7')
       SolaceViewVars('B7Email',deb7:B7Email,'Solace_Files',1,'DEFEMAB7')
       SolaceViewVars('Subject',deb7:Subject,'Solace_Files',1,'DEFEMAB7')
     End
     if Status(DEFEMAE2) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',dee2:RecordNumber,'Solace_Files',1,'DEFEMAE2')
       SolaceViewVars('E2Email',dee2:E2Email,'Solace_Files',1,'DEFEMAE2')
       SolaceViewVars('Subject',dee2:Subject,'Solace_Files',1,'DEFEMAE2')
     End
     if Status(DEFEMAE3) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',dee3:RecordNumber,'Solace_Files',1,'DEFEMAE3')
       SolaceViewVars('E3Email',dee3:E3Email,'Solace_Files',1,'DEFEMAE3')
       SolaceViewVars('Subject',dee3:Subject,'Solace_Files',1,'DEFEMAE3')
     End
     if Status(DEFEME2B) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',dee2B:RecordNumber,'Solace_Files',1,'DEFEME2B')
       SolaceViewVars('E2BEmail',dee2B:E2BEmail,'Solace_Files',1,'DEFEME2B')
       SolaceViewVars('Subject',dee2B:Subject,'Solace_Files',1,'DEFEME2B')
     End
     if Status(DEFEME3B) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',dee3B:RecordNumber,'Solace_Files',1,'DEFEME3B')
       SolaceViewVars('E3BEmail',dee3B:E3BEmail,'Solace_Files',1,'DEFEME3B')
       SolaceViewVars('Subject',dee3B:Subject,'Solace_Files',1,'DEFEME3B')
     End
     if Status(DEFEMAR2) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',der2:RecordNumber,'Solace_Files',1,'DEFEMAR2')
       SolaceViewVars('R2Email',der2:R2Email,'Solace_Files',1,'DEFEMAR2')
       SolaceViewVars('Subject',der2:Subject,'Solace_Files',1,'DEFEMAR2')
     End
     if Status(DEFEMAR3) or SolaceInit then   !Only refresh if file is open!
       SolaceViewVars('RecordNumber',der3:RecordNumber,'Solace_Files',1,'DEFEMAR3')
       SolaceViewVars('R3Email',der3:R3Email,'Solace_Files',1,'DEFEMAR3')
       SolaceViewVars('Subject',der3:Subject,'Solace_Files',1,'DEFEMAR3')
     End
