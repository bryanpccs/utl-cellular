

   MEMBER('sbcr0083.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBCR0001.INC'),ONCE        !Local module procedure declarations
                     END


ChargeStructureExport PROCEDURE                       !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::2:TAGFLAG          BYTE(0)
DASBRW::2:TAGMOUSE         BYTE(0)
DASBRW::2:TAGDISPSTATUS    BYTE(0)
DASBRW::2:QUEUE           QUEUE
Pointer10                     LIKE(GLO:Pointer10)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
ManufacturerTag      STRING(1)
Pathname             STRING(255)
BRW1::View:Browse    VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
ManufacturerTag        LIKE(ManufacturerTag)          !List box control field - type derived from local data
ManufacturerTag_Icon   LONG                           !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Charge Structure Export'),AT(,,212,203),FONT('Tahoma',8,,),CENTER,ICON('PC.ICO'),SYSTEM,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,204,168),USE(?Sheet1),WIZARD,SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('Filename'),AT(8,12),USE(?Prompt2)
                           ENTRY(@s255),AT(40,12,148,10),USE(Pathname)
                           BUTTON('...'),AT(192,12,10,10),USE(?FileButton),LEFT,ICON('list3.ico')
                           PROMPT('Choose Manufacturer'),AT(8,28),USE(?Prompt1)
                           LIST,AT(8,40,196,104),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11LJ@s1@120L|M~Manufacturer~L(2)@s30@'),FROM(Queue:Browse)
                           BUTTON('sho&W tags'),AT(124,92,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Rev tags'),AT(136,116,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('&Untag all'),AT(140,148,60,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                           BUTTON('tag &All'),AT(76,148,60,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Tag'),AT(12,148,60,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                         END
                       END
                       PANEL,AT(4,176,204,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Export'),AT(76,180,60,16),USE(?ExportButton),LEFT,ICON('ok.ico')
                       BUTTON('Cancel'),AT(140,180,60,16),USE(?CancelButton),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
! ProgressWindow Declarations

RecordsToProcess        LONG,AUTO
RecordsProcessed        LONG,AUTO
RecordsPerCycle         LONG,AUTO
RecordsThisCycle        LONG,AUTO
PercentProgress         LONG,AUTO
ProgressThermometer     LONG,AUTO
AppName                 STRING(30)
ProgressWindow          WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
                            PROGRESS,USE(ProgressThermometer),AT(25,15,111,12),RANGE(0,100)
                            STRING(''),AT(0,3,161,10),USE(?ProgressUserString),CENTER,FONT('Arial',8,,)
                            STRING(''),AT(0,30,161,10),USE(?ProgressText),TRN,CENTER,FONT('Arial',8,,)
                            BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
                        END

               MAP
ProgressBarShow         PROCEDURE(STRING arg:AppNameString, LONG arg:RecordCount=1000,  |
                                  LONG arg:CycleCount=25,   <STRING arg:UserText>)
ProgressBarReset        PROCEDURE(LONG arg:RecordCount=1000, <LONG arg:CycleCount>, <STRING arg:UserText>)
ProgressBarUpdate       PROCEDURE(),LONG
ProgressBarCancelled    PROCEDURE(),LONG
ProgressBarClose        PROCEDURE()
              END
LocalVariables      GROUP,PRE(LOC)
ApplicationName         STRING('ServiceBase 2000')
ProgramName             STRING('Charge Structure Export')
UserName                STRING(100)
UserCode                STRING(3)
                    END

IniFilepath          STRING(255)
RecordCount          LONG

ExportQueue         QUEUE,PRE(EXQ)
UnitType                      STRING(30)
ChargeType                    STRING(30)
DefaultCharge                 STRING(10)
TradeCharge                   STRING(10)
ZeroValue                     STRING(3)
                    END

EXFILE              FILE,DRIVER('BASIC'),PRE(expfile),CREATE,BINDABLE,THREAD
EXRECORD                 RECORD,PRE(exprec)
Manufacturer                  STRING(30)
Model                         STRING(30)
UnitType                      STRING(30)
ChargeType                    STRING(30)
RepairType                    STRING(30)
TradeCharge                   STRING(30)
DefaultCharge                 STRING(30)
TradeAccount                  STRING(30)
ZeroValue                     STRING(30)
                         END
                    END

                    MAP
ExportManufacturer  PROCEDURE(STRING In:manufacturer, STRING IN:Account_Number, |
                              STRING IN_Trade_Account_Name, STRING ZeroChargeable),LONG
                    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?Pathname{prop:ReadOnly} = True
        ?Pathname{prop:FontColor} = 65793
        ?Pathname{prop:Color} = 15066597
    Elsif ?Pathname{prop:Req} = True
        ?Pathname{prop:FontColor} = 65793
        ?Pathname{prop:Color} = 8454143
    Else ! If ?Pathname{prop:Req} = True
        ?Pathname{prop:FontColor} = 65793
        ?Pathname{prop:Color} = 16777215
    End ! If ?Pathname{prop:Req} = True
    ?Pathname{prop:Trn} = 0
    ?Pathname{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::2:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW1.UpdateBuffer
   glo:Queue10.Pointer10 = man:Manufacturer
   GET(glo:Queue10,glo:Queue10.Pointer10)
  IF ERRORCODE()
     glo:Queue10.Pointer10 = man:Manufacturer
     ADD(glo:Queue10,glo:Queue10.Pointer10)
    ManufacturerTag = '*'
  ELSE
    DELETE(glo:Queue10)
    ManufacturerTag = ''
  END
    Queue:Browse.ManufacturerTag = ManufacturerTag
  IF (ManufacturerTag='*')
    Queue:Browse.ManufacturerTag_Icon = 2
  ELSE
    Queue:Browse.ManufacturerTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue10)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue10.Pointer10 = man:Manufacturer
     ADD(glo:Queue10,glo:Queue10.Pointer10)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue10)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::2:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue10)
    GET(glo:Queue10,QR#)
    DASBRW::2:QUEUE = glo:Queue10
    ADD(DASBRW::2:QUEUE)
  END
  FREE(glo:Queue10)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::2:QUEUE.Pointer10 = man:Manufacturer
     GET(DASBRW::2:QUEUE,DASBRW::2:QUEUE.Pointer10)
    IF ERRORCODE()
       glo:Queue10.Pointer10 = man:Manufacturer
       ADD(glo:Queue10,glo:Queue10.Pointer10)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASSHOWTAG Routine
   CASE DASBRW::2:TAGDISPSTATUS
   OF 0
      DASBRW::2:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::2:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::2:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
GetUserName         ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('%', CommandLine)
        IF (NOT tmpPos) THEN
            MessageEx('Attempting to use ' & LOC:ProgramName & '<10,13>'          & |
                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
                LOC:ApplicationName,                                                           |
                'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                beep:systemhand,msgex:samewidths,84,26,0)
           HALT
        END

        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        IF (Access:USERS.Tryfetch(use:Password_Key) <> Level:Benign) THEN
            MessageEx('Unable to find your logged in user details.', |
                    LOC:ApplicationName,                                  |
                    'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
            HALT
        END

        LOC:UserCode = use:User_Code

        LOC:UserName = use:Forename
        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ChargeStructureExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Access:MODELNUM.UseFile
  Access:REPAIRTY.UseFile
  Access:TRACHRGE.UseFile
  Access:UNITTYPE.UseFile
  Access:SUBTRACC.UseFile
  Access:STDCHRGE.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:MANUFACT,SELF)
  DO GetUserName
  
  IniFilepath =  CLIP(PATH()) & '\SBCR0083.INI'
  Pathname = GETINI('DEFAULTS','Pathname',,CLIP(IniFilepath))
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,man:Manufacturer_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,man:Manufacturer,1,BRW1)
  BIND('ManufacturerTag',ManufacturerTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(ManufacturerTag,BRW1.Q.ManufacturerTag)
  BRW1.AddField(man:Manufacturer,BRW1.Q.man:Manufacturer)
  BRW1.AddField(man:RecordNumber,BRW1.Q.man:RecordNumber)
  BRW1.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue10)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue10)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?FileButton
      ThisWindow.Update
      FILEDIALOG('Choose Export File',pathname,'CSV Files|*.csv', |
                        file:save + file:keepdir + file:noerror + file:longname)
      strlen# = LEN(CLIP(Pathname))
      IF ((strlen# < 4) OR (UPPER(Pathname[strlen#-3 : strlen#]) <> '.CSV')) THEN
          Pathname = CLIP(Pathname) & '.csv'
      END
      DISPLAY(?pathname)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ExportButton
      ThisWindow.Update
      IF (Pathname = '') THEN
          MESSAGE('Please Select a Filename')
          SELECT(?Pathname)
          CYCLE
      END
      
      EXFILE{Prop:NAME} = CLIP(Pathname)
      
      IF (EXISTS(CLIP(Pathname))) THEN
          REMOVE(EXFILE)
      END
      CREATE(EXFILE)
      
      OPEN(EXFILE, 42h)
      IF (ERRORCODE()) THEN
          MESSAGE('Error Opening ' & CLIP(Pathname))
          SELECT(?Pathname)
          CYCLE
      END
      SET(EXFILE)
      
      manq# = RECORDS(glo:Queue10)
      manf# = RECORDS(MANUFACT)
      RecordCount = 0
      cancelled# = 0
      
      IF ((manq# = manf#) OR (manq# = 0)) THEN
          ProgressBarShow(LOC:ApplicationName, RECORDS(MODELNUM) * RECORDS(TRADEACC))
          access:MANUFACT.Clearkey(man:Manufacturer_Key)
          SET(man:Manufacturer_Key, man:Manufacturer_Key)
          LOOP WHILE (access:MANUFACT.next() = Level:Benign)
              access:TRADEACC.Clearkey(tra:Company_Name_Key)
              SET(tra:Company_Name_Key)
              LOOP WHILE (access:TRADEACC.next() = Level:Benign)
                  IF ((tra:Company_Name = '') OR (tra:Company_Name = '.')) THEN
                      AccountName" = tra:Account_Number
                  ELSE
                      AccountName" = tra:Company_Name
                  END
                  IF (NOT ExportManufacturer(man:Manufacturer, tra:Account_Number, AccountName", tra:ZeroChargeable)) THEN
                      cancelled# = 1
                      BREAK
                  END
              END
              IF (cancelled# = 1) THEN
                  BREAK
              END
          END
          ProgressBarClose()
      ELSE
          ProgressBarShow(LOC:ApplicationName, RECORDS(MODELNUM) * RECORDS(TRADEACC))
          LOOP ix# = 1 TO manq#
              GET(glo:Queue10, ix#)
              access:MANUFACT.Clearkey(man:Manufacturer_Key)
              man:MANUFACTURER = glo:Pointer10
              IF (access:MANUFACT.fetch(man:Manufacturer_Key) = Level:Benign) THEN
                  access:TRADEACC.Clearkey(tra:Company_Name_Key)
                  SET(tra:Company_Name_Key)
                  LOOP WHILE (access:TRADEACC.next() = Level:Benign)
                      IF ((tra:Company_Name = '') OR (tra:Company_Name = '.')) THEN
                          AccountName" = tra:Account_Number
                      ELSE
                          AccountName" = tra:Company_Name
                      END
                      IF (NOT ExportManufacturer(man:Manufacturer, tra:Account_Number, AccountName", tra:ZeroChargeable)) THEN
                          cancelled# = 1
                          BREAK
                      END
                  END
              END
              IF (cancelled# = 1) THEN
                  BREAK
              END
          END
          ProgressBarClose()
      END
      
      CLOSE(EXFILE)
      
      PUTINI('DEFAULTS','Pathname',CLIP(Pathname),CLIP(IniFilepath))
      
      IF (cancelled#) THEN
          REMOVE(EXFILE)
      ELSIF (RecordCount = 0) THEN
          REMOVE(EXFILE)
          MessageEx('No Records Found',  |
                    LOC:ApplicationName,'Styles\warn.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
          MessageEx('Export Completed OK',  |
                    LOC:ApplicationName,,'&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
          POST(Event:CloseWindow)
      END
      
      
    OF ?CancelButton
      ThisWindow.Update
      POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::2:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Progress Bar Window
ProgressBarShow    PROCEDURE(arg:AppNameString, arg:RecordCount, arg:CycleCount, arg:UserText)
    CODE
    AppName              = arg:AppNameString
    RecordsPerCycle      = arg:CycleCount
    RecordsThisCycle     = 0
    RecordsProcessed     = 0
    RecordsToProcess     = arg:RecordCount
    PercentProgress      = 0
    ProgressThermometer  = 0

    !thiswindow.reset(1) !This may error, if so, remove this line.
    open(ProgressWindow)
    !ProgressWindow{PROP:Timer} = 1
         
    IF (OMITTED(4)) THEN
        ?ProgressUserString{PROP:Text} = 'Running...'
    ELSE
        ?ProgressUserString{PROP:Text} = CLIP(arg:UserText)
    END
    ?progressText{prop:text}    = '0% Completed'
    DISPLAY()

    RETURN

ProgressBarReset    PROCEDURE(arg:RecordCount, arg:CycleCount, arg:UserText)
    CODE
    IF (NOT OMITTED(2)) THEN
        RecordsPerCycle      = arg:CycleCount
    END
    RecordsThisCycle     = 0
    RecordsProcessed     = 0
    RecordsToProcess     = arg:RecordCount
    PercentProgress      = 0
    ProgressThermometer  = 0

    IF (NOT OMITTED(3)) THEN
        ?ProgressUserString{PROP:Text} = CLIP(arg:UserText)
    END
    ?progressText{prop:text}    = '0% Completed'
    DISPLAY()

    RETURN

ProgressBarUpdate     PROCEDURE()
result  LONG
    CODE
    result = -1
    RecordsProcessed += 1
    RecordsThisCycle += 1
    IF (RecordsThisCycle > RecordsPerCycle) THEN
        RecordsThisCycle = 0
        IF (ProgressBarCancelled()) THEN
            !CLOSE(ProgressWindow)
            result = 0
        ELSE
            IF (PercentProgress < 100) THEN
                PercentProgress = ROUND((RecordsProcessed / RecordsToProcess) * 100.0, 1)
                IF (PercentProgress > 100) THEN
                    PercentProgress = 100
                END
                IF (PercentProgress <> ProgressThermometer) THEN
                    ProgressThermometer = PercentProgress
                    ?ProgressText{prop:text} = format(PercentProgress,@n3) & '% Completed'
                    DISPLAY()
                END
            END
        END
    END
    RETURN result

ProgressBarCancelled       PROCEDURE()
result  LONG
    CODE
    result = 0
    cancel# = 0

    ACCEPT
        CASE EVENT()
        OF Event:Timer
            BREAK
        OF Event:CloseWindow
            cancel# = 1
            BREAK
        OF Event:accepted
            IF (FIELD() = ?ProgressCancel) THEN
                cancel# = 1
                BREAK
            END
        END
    END

    IF (cancel# = 1) THEN
        BEEP(BEEP:SystemAsterisk)
        CASE MessageEx('Are you sure you want to cancel?', CLIP(AppName),|
                       'Styles\question.ico','&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                       beep:systemquestion,msgex:samewidths,84,26,0)
            OF 1 ! &Yes Button
                result = -1
            OF 2 ! &No Button
        END
    END
    RETURN result

ProgressBarClose       PROCEDURE()
    CODE
    CLOSE(ProgressWindow)
    RETURN
ExportManufacturer      PROCEDURE(IN:Manufacturer, In:Trade_Account, IN:Trade_Account_Name, IN:ZeroChargeable)
result      LONG
    CODE
    result = 1

    IF (IN:ZeroChargeable = 'YES') THEN
        ZeroValue" = 'YES'
    ELSE
        ZeroValue" = 'NO'
    END

    access:MODELNUM.Clearkey(mod:Manufacturer_Key)
    mod:Manufacturer = IN:Manufacturer
    SET(mod:Manufacturer_Key, mod:Manufacturer_Key)
    LOOP WHILE ((access:MODELNUM.next() = Level:Benign) AND |
                (mod:Manufacturer = man:Manufacturer))

        IF (NOT ProgressBarUpdate()) THEN
            result = 0
           Return result
        END

        access:REPAIRTY.Clearkey(rep:Model_Number_Key)
        rep:Model_Number = mod:Model_Number
        SET(rep:Model_Number_Key, rep:Model_Number_Key)
        LOOP WHILE((access:REPAIRTY.next() = Level:Benign) AND |
                   (rep:Model_Number = mod:Model_Number))

            FREE(ExportQueue)

            access:TRACHRGE.Clearkey(trc:Repair_Type_Key)
            trc:Account_Number = CLIP(IN:Trade_Account)
            trc:Model_Number = mod:Model_Number
            trc:Repair_Type = rep:Repair_type
            SET(trc:Repair_Type_Key, trc:Repair_Type_Key)
            LOOP WHILE ((access:TRACHRGE.next() = Level:Benign) AND |
                        (trc:Account_Number = Clip(IN:Trade_Account)) AND |
                        (trc:Model_Number = mod:Model_Number) AND |
                        (trc:Repair_Type = rep:Repair_type))

                !IF (NOT ProgressBarUpdate()) THEN
                !    result = 0
                !   Return result
                !END

                !access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
                !cha:Charge_Type = trc:Charge_type
                !IF (access:CHARTYPE.fetch(cha:Charge_Type_Key) = Level:Benign) THEN
                !    IF (cha:Zero_Parts = 'YES') THEN
                !        zerovalue"      = 'YES'
                !    ELSE
                !        zerovalue"      = 'NO'
                !    END
                !ELSE
                !    zerovalue" = ''
                !END

                CLEAR(ExportQueue)
                EXQ:UnitType       =  CLIP(trc:Unit_Type)
                EXQ:ChargeType     =  CLIP(trc:Charge_Type)
                EXQ:DefaultCharge  =  ''
                EXQ:TradeCharge    =  LEFT(FORMAT(trc:Cost, @n7.2))
                EXQ:ZeroValue      =  CLIP(zerovalue")
                ADD(ExportQueue)
            END

            SORT(ExportQueue, EXQ:UnitType, EXQ:ChargeType)

            access:STDCHRGE.Clearkey(sta:Repair_Type_Key)
            sta:Model_Number = mod:Model_Number
            sta:Repair_Type = rep:Repair_type
            SET(sta:Repair_Type_Key, sta:Repair_Type_Key)
            LOOP WHILE ((access:STDCHRGE.next() = Level:Benign) AND |
                        (sta:Model_Number = mod:Model_Number) AND |
                        (sta:Repair_Type = rep:Repair_type))

                !IF (NOT ProgressBarUpdate()) THEN
                !    result = 0
                !    Return result
                !END

                !access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
                !cha:Charge_Type = sta:Charge_type
                !IF (access:CHARTYPE.fetch(cha:Charge_Type_Key) = Level:Benign) THEN
                !    IF (cha:Zero_Parts = 'YES') THEN
                !        zerovalue"      = 'YES'
                !    ELSE
                !        zerovalue"      = 'NO'
                !    END
                !ELSE
                !    zerovalue" = ''
                !END

                EXQ:UnitType =  CLIP(sta:Unit_Type)
                EXQ:ChargeType  =  CLIP(sta:Charge_Type)
                GET(ExportQueue, +EXQ:UnitType, +EXQ:ChargeType)
                IF (ERRORCODE()) THEN
                    CLEAR(ExportQueue)
                    EXQ:UnitType       =  CLIP(sta:Unit_Type)
                    EXQ:ChargeType     =  CLIP(sta:Charge_Type)
                    EXQ:DefaultCharge  =  LEFT(FORMAT(sta:Cost, @n7.2))
                    EXQ:TradeCharge    =  ''
                    EXQ:ZeroValue      =  CLIP(zerovalue")
                    ADD(ExportQueue, +EXQ:UnitType, +EXQ:ChargeType)
                ELSE
                    EXQ:DefaultCharge  =  LEFT(FORMAT(sta:Cost, @n7.2))
                    PUT(ExportQueue)
                END
            END

            recount# = RECORDS(ExportQueue)
            IF (recount# > 0) THEN
                IF (RecordCount = 0) THEN
                    exprec:Manufacturer   =  'Manufacturer'
                    exprec:Model          =  'Model Number'
                    exprec:UnitType       =  'Unit Type'
                    exprec:ChargeType     =  'Charge Type'
                    exprec:RepairType     =  'Repair Type'
                    exprec:DefaultCharge  =  'Default Charge'
                    exprec:TradeCharge    =  'Trade A/C Charge'
                    exprec:TradeAccount   =  'Trade Account'
                    exprec:ZeroValue      =  'Zero Value Parts'
                    ADD(EXFILE)
                END
                RecordCount += recount#
                LOOP ix# = 1 TO recount#
                    GET(ExportQueue, ix#)
                    CLEAR(EXFILE)
                    exprec:Manufacturer   =  CLIP(IN:Manufacturer)
                    exprec:Model          =  CLIP(mod:Model_Number)
                    exprec:UnitType       =  EXQ:UnitType
                    exprec:ChargeType     =  EXQ:ChargeType
                    exprec:RepairType     =  CLIP(rep:Repair_type)
                    exprec:DefaultCharge  =  EXQ:DefaultCharge
                    exprec:TradeCharge    =  EXQ:TradeCharge
                    exprec:TradeAccount   =  CLIP(IN:Trade_Account_Name)
                    exprec:ZeroValue      =  EXQ:ZeroValue
                    ADD(EXFILE)
                END
            END
        END
    END


    FREE(ExportQueue)
    RETURN result

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue10.Pointer10 = man:Manufacturer
     GET(glo:Queue10,glo:Queue10.Pointer10)
    IF ERRORCODE()
      ManufacturerTag = ''
    ELSE
      ManufacturerTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (ManufacturerTag='*')
    SELF.Q.ManufacturerTag_Icon = 2
  ELSE
    SELF.Q.ManufacturerTag_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue10.Pointer10 = man:Manufacturer
     GET(glo:Queue10,glo:Queue10.Pointer10)
    EXECUTE DASBRW::2:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue

