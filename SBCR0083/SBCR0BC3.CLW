  MEMBER('sbcr0083.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
SBCR0BC3:DctInit    PROCEDURE
SBCR0BC3:DctKill    PROCEDURE
SBCR0BC3:FilesInit  PROCEDURE
  END

Hide:Access:TRACHRGE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRACHRGE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRDMODEL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRDMODEL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOMODEL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:STOMODEL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRADEACC CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:TRADEACC CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUBCHRGE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SUBCHRGE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:DEFCHRGE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:DEFCHRGE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUBTRACC CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:SUBTRACC CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CHARTYPE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:CHARTYPE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANUFACT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANUFACT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:USMASSIG CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:USMASSIG CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:USUASSIG CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:USUASSIG CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBPAYMT_ALIAS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBPAYMT_ALIAS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBS_ALIAS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBS_ALIAS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

SBCR0BC3:DctInit PROCEDURE
  CODE
  Relate:TRACHRGE &= Hide:Relate:TRACHRGE
  Relate:TRDMODEL &= Hide:Relate:TRDMODEL
  Relate:STOMODEL &= Hide:Relate:STOMODEL
  Relate:TRADEACC &= Hide:Relate:TRADEACC
  Relate:SUBCHRGE &= Hide:Relate:SUBCHRGE
  Relate:DEFCHRGE &= Hide:Relate:DEFCHRGE
  Relate:SUBTRACC &= Hide:Relate:SUBTRACC
  Relate:CHARTYPE &= Hide:Relate:CHARTYPE
  Relate:MANUFACT &= Hide:Relate:MANUFACT
  Relate:USMASSIG &= Hide:Relate:USMASSIG
  Relate:USUASSIG &= Hide:Relate:USUASSIG
  Relate:JOBPAYMT_ALIAS &= Hide:Relate:JOBPAYMT_ALIAS
  Relate:JOBS_ALIAS &= Hide:Relate:JOBS_ALIAS

SBCR0BC3:FilesInit PROCEDURE
  CODE
  Hide:Relate:TRACHRGE.Init
  Hide:Relate:TRDMODEL.Init
  Hide:Relate:STOMODEL.Init
  Hide:Relate:TRADEACC.Init
  Hide:Relate:SUBCHRGE.Init
  Hide:Relate:DEFCHRGE.Init
  Hide:Relate:SUBTRACC.Init
  Hide:Relate:CHARTYPE.Init
  Hide:Relate:MANUFACT.Init
  Hide:Relate:USMASSIG.Init
  Hide:Relate:USUASSIG.Init
  Hide:Relate:JOBPAYMT_ALIAS.Init
  Hide:Relate:JOBS_ALIAS.Init


SBCR0BC3:DctKill PROCEDURE
  CODE
  Hide:Relate:TRACHRGE.Kill
  Hide:Relate:TRDMODEL.Kill
  Hide:Relate:STOMODEL.Kill
  Hide:Relate:TRADEACC.Kill
  Hide:Relate:SUBCHRGE.Kill
  Hide:Relate:DEFCHRGE.Kill
  Hide:Relate:SUBTRACC.Kill
  Hide:Relate:CHARTYPE.Kill
  Hide:Relate:MANUFACT.Kill
  Hide:Relate:USMASSIG.Kill
  Hide:Relate:USUASSIG.Kill
  Hide:Relate:JOBPAYMT_ALIAS.Kill
  Hide:Relate:JOBS_ALIAS.Kill


Hide:Access:TRACHRGE.Init PROCEDURE
  CODE
  SELF.Init(TRACHRGE,GlobalErrors)
  SELF.Buffer &= trc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(trc:Account_Charge_Key,'By Charge Type',0)
  SELF.AddKey(trc:Model_Repair_Key,'trc:Model_Repair_Key',0)
  SELF.AddKey(trc:Charge_Type_Key,'By Charge Type',0)
  SELF.AddKey(trc:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(trc:Repair_Type_Key,'By Repair Type',0)
  SELF.AddKey(trc:Cost_Key,'By Cost',0)
  SELF.AddKey(trc:Charge_Type_Only_Key,'trc:Charge_Type_Only_Key',0)
  SELF.AddKey(trc:Repair_Type_Only_Key,'trc:Repair_Type_Only_Key',0)
  SELF.AddKey(trc:Unit_Type_Only_Key,'trc:Unit_Type_Only_Key',0)
  Access:TRACHRGE &= SELF


Hide:Relate:TRACHRGE.Init PROCEDURE
  CODE
  Hide:Access:TRACHRGE.Init
  SELF.Init(Access:TRACHRGE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:CHARTYPE)
  SELF.AddRelation(Relate:UNITTYPE)
  SELF.AddRelation(Relate:REPAIRTY)
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRACHRGE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRACHRGE &= NULL


Hide:Relate:TRACHRGE.Kill PROCEDURE

  CODE
  Hide:Access:TRACHRGE.Kill
  PARENT.Kill
  Relate:TRACHRGE &= NULL


Hide:Access:TRDMODEL.Init PROCEDURE
  CODE
  SELF.Init(TRDMODEL,GlobalErrors)
  SELF.Buffer &= trm:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(trm:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(trm:Model_Number_Only_Key,'By Model Number',0)
  Access:TRDMODEL &= SELF


Hide:Relate:TRDMODEL.Init PROCEDURE
  CODE
  Hide:Access:TRDMODEL.Init
  SELF.Init(Access:TRDMODEL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRDPARTY)
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:TRDMODEL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRDMODEL &= NULL


Hide:Relate:TRDMODEL.Kill PROCEDURE

  CODE
  Hide:Access:TRDMODEL.Kill
  PARENT.Kill
  Relate:TRDMODEL &= NULL


Hide:Access:STOMODEL.Init PROCEDURE
  CODE
  SELF.Init(STOMODEL,GlobalErrors)
  SELF.Buffer &= stm:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stm:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(stm:Model_Part_Number_Key,'By Model Number',0)
  SELF.AddKey(stm:Description_Key,'By Description',0)
  SELF.AddKey(stm:Manufacturer_Key,'By Manufacturer',0)
  SELF.AddKey(stm:Ref_Part_Description,'Why?',0)
  SELF.AddKey(stm:Location_Part_Number_Key,'stm:Location_Part_Number_Key',0)
  SELF.AddKey(stm:Location_Description_Key,'stm:Location_Description_Key',0)
  SELF.AddKey(stm:Mode_Number_Only_Key,'By Model Number',0)
  Access:STOMODEL &= SELF


Hide:Relate:STOMODEL.Init PROCEDURE
  CODE
  Hide:Access:STOMODEL.Init
  SELF.Init(Access:STOMODEL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOCK)
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:STOMODEL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOMODEL &= NULL


Hide:Access:STOMODEL.PrimeFields PROCEDURE

  CODE
  stm:Accessory = 'NO'
  PARENT.PrimeFields


Hide:Relate:STOMODEL.Kill PROCEDURE

  CODE
  Hide:Access:STOMODEL.Kill
  PARENT.Kill
  Relate:STOMODEL &= NULL


Hide:Access:TRADEACC.Init PROCEDURE
  CODE
  SELF.Init(TRADEACC,GlobalErrors)
  SELF.Buffer &= tra:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tra:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(tra:Account_Number_Key,'By Account Number',0)
  SELF.AddKey(tra:Company_Name_Key,'By Company Name',0)
  Access:TRADEACC &= SELF


Hide:Relate:TRADEACC.Init PROCEDURE
  CODE
  Hide:Access:TRADEACC.Init
  SELF.Init(Access:TRADEACC,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRAEMAIL,RI:CASCADE,RI:CASCADE,tre:RecipientKey)
  SELF.AddRelationLink(tra:RecordNumber,tre:RefNumber)
  SELF.AddRelation(Relate:TRAFAULT,RI:CASCADE,RI:CASCADE,taf:Field_Number_Key)
  SELF.AddRelationLink(tra:Account_Number,taf:AccountNumber)
  SELF.AddRelation(Relate:TRACHAR,RI:CASCADE,RI:CASCADE,tch:Account_Number_Key)
  SELF.AddRelationLink(tra:Account_Number,tch:Account_Number)
  SELF.AddRelation(Relate:SUBTRACC,RI:CASCADE,RI:CASCADE,sub:Main_Account_Key)
  SELF.AddRelationLink(tra:Account_Number,sub:Main_Account_Number)
  SELF.AddRelation(Relate:TRACHRGE,RI:CASCADE,RI:CASCADE,trc:Account_Charge_Key)
  SELF.AddRelationLink(tra:Account_Number,trc:Account_Number)


Hide:Access:TRADEACC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRADEACC &= NULL


Hide:Access:TRADEACC.PrimeFields PROCEDURE

  CODE
  tra:Print_Despatch_Complete = 'NO'
  tra:Print_Despatch_Despatch = 'NO'
  tra:UseCustDespAdd = 'NO'
  tra:Allow_Loan = 'NO'
  tra:Allow_Exchange = 'NO'
  tra:Print_Retail_Despatch_Note = 'NO'
  tra:Print_Retail_Picking_Note = 'NO'
  tra:Despatch_Note_Per_Item = 'NO'
  tra:Skip_Despatch = 'NO'
  tra:IgnoreDespatch = 'NO'
  tra:Summary_Despatch_Notes = 'NO'
  tra:Force_Estimate = 'NO'
  tra:Use_Customer_Address = 'NO'
  tra:Invoice_Customer_Address = 'YES'
  tra:ZeroChargeable = 'NO'
  tra:RefurbCharge = 'NO'
  tra:ExchangeAcc = 'NO'
  tra:MultiInvoice = 'SIN'
  tra:EDIInvoice = 'NO'
  tra:ExcludeBouncer = 0
  tra:RetailZeroParts = 0
  tra:HideDespAdd = 0
  tra:EuroApplies = 0
  tra:ForceCommonFault = 0
  tra:ForceOrderNumber = 0
  tra:ShowRepairTypes = 0
  tra:InvoiceAtDespatch = 0
  tra:InvoiceType = 0
  tra:IndividualSummary = 0
  tra:UseTradeContactNo = 0
  tra:StopThirdParty = 0
  tra:E1 = 1
  tra:E2 = 1
  tra:E3 = 1
  tra:UseDespatchDetails = 0
  tra:AutoSendStatusEmails = 0
  tra:EmailRecipientList = 0
  tra:EmailEndUser = 0
  tra:MakeSplitJob = 0
  tra:CheckChargeablePartsCost = 0
  tra:MaxChargeablePartsCost = 0.0
  PARENT.PrimeFields


Hide:Relate:TRADEACC.Kill PROCEDURE

  CODE
  Hide:Access:TRADEACC.Kill
  PARENT.Kill
  Relate:TRADEACC &= NULL


Hide:Access:SUBCHRGE.Init PROCEDURE
  CODE
  SELF.Init(SUBCHRGE,GlobalErrors)
  SELF.Buffer &= suc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(suc:Model_Repair_Type_Key,'By Repair Type',0)
  SELF.AddKey(suc:Account_Charge_Key,'By Charge Type',0)
  SELF.AddKey(suc:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(suc:Repair_Type_Key,'By Repair Type',0)
  SELF.AddKey(suc:Cost_Key,'By Cost',0)
  SELF.AddKey(suc:Charge_Type_Only_Key,'suc:Charge_Type_Only_Key',0)
  SELF.AddKey(suc:Repair_Type_Only_Key,'suc:Repair_Type_Only_Key',0)
  SELF.AddKey(suc:Unit_Type_Only_Key,'suc:Unit_Type_Only_Key',0)
  SELF.AddKey(suc:Model_Repair_Key,'suc:Model_Repair_Key',0)
  Access:SUBCHRGE &= SELF


Hide:Relate:SUBCHRGE.Init PROCEDURE
  CODE
  Hide:Access:SUBCHRGE.Init
  SELF.Init(Access:SUBCHRGE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:CHARTYPE)
  SELF.AddRelation(Relate:UNITTYPE)
  SELF.AddRelation(Relate:REPAIRTY)
  SELF.AddRelation(Relate:SUBTRACC)


Hide:Access:SUBCHRGE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUBCHRGE &= NULL


Hide:Relate:SUBCHRGE.Kill PROCEDURE

  CODE
  Hide:Access:SUBCHRGE.Kill
  PARENT.Kill
  Relate:SUBCHRGE &= NULL


Hide:Access:DEFCHRGE.Init PROCEDURE
  CODE
  SELF.Init(DEFCHRGE,GlobalErrors)
  SELF.Buffer &= dec:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(dec:Charge_Type_Key,'By Charge Type',0)
  SELF.AddKey(dec:Repair_Type_Only_Key,'dec:Repair_Type_Only_Key',0)
  SELF.AddKey(dec:Unit_Type_Only_Key,'dec:Unit_Type_Only_Key',0)
  Access:DEFCHRGE &= SELF


Hide:Relate:DEFCHRGE.Init PROCEDURE
  CODE
  Hide:Access:DEFCHRGE.Init
  SELF.Init(Access:DEFCHRGE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:CHARTYPE)
  SELF.AddRelation(Relate:REPAIRTY)
  SELF.AddRelation(Relate:UNITTYPE)


Hide:Access:DEFCHRGE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:DEFCHRGE &= NULL


Hide:Relate:DEFCHRGE.Kill PROCEDURE

  CODE
  Hide:Access:DEFCHRGE.Kill
  PARENT.Kill
  Relate:DEFCHRGE &= NULL


Hide:Access:SUBTRACC.Init PROCEDURE
  CODE
  SELF.Init(SUBTRACC,GlobalErrors)
  SELF.Buffer &= sub:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sub:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(sub:Main_Account_Key,'By Account Number',0)
  SELF.AddKey(sub:Main_Name_Key,'By Company Name',0)
  SELF.AddKey(sub:Account_Number_Key,'By Account Number',0)
  SELF.AddKey(sub:Branch_Key,'By Branch',0)
  SELF.AddKey(sub:Main_Branch_Key,'By Branch',0)
  SELF.AddKey(sub:Company_Name_Key,'By Company Name',0)
  SELF.AddKey(sub:Contact_Centre_Key,'By Contact Centre',0)
  SELF.AddKey(sub:RetailStoreKey,'By Account Number',0)
  Access:SUBTRACC &= SELF


Hide:Relate:SUBTRACC.Init PROCEDURE
  CODE
  Hide:Access:SUBTRACC.Init
  SELF.Init(Access:SUBTRACC,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUBEMAIL,RI:CASCADE,RI:CASCADE,sue:RecipientTypeKey)
  SELF.AddRelationLink(sub:RecordNumber,sue:RefNumber)
  SELF.AddRelation(Relate:SUBACCAD,RI:CASCADE,RI:CASCADE,sua:AccountNumberKey)
  SELF.AddRelationLink(sub:RecordNumber,sua:RefNumber)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:None,job:AccountNumberKey)
  SELF.AddRelationLink(sub:Account_Number,job:Account_Number)
  SELF.AddRelation(Relate:SUBCHRGE,RI:CASCADE,RI:CASCADE,suc:Account_Charge_Key)
  SELF.AddRelationLink(sub:Account_Number,suc:Account_Number)
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:SUBTRACC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUBTRACC &= NULL


Hide:Access:SUBTRACC.PrimeFields PROCEDURE

  CODE
  sub:UseCustDespAdd = 'NO'
  sub:PriceDespatchNotes = 0
  sub:Print_Despatch_Complete = 'NO'
  sub:Print_Despatch_Despatch = 'NO'
  sub:Print_Retail_Despatch_Note = 'NO'
  sub:Print_Retail_Picking_Note = 'NO'
  sub:Despatch_Note_Per_Item = 'NO'
  sub:Summary_Despatch_Notes = 'NO'
  sub:Use_Customer_Address = 'NO'
  sub:Invoice_Customer_Address = 'YES'
  sub:ZeroChargeable = 'NO'
  sub:MultiInvoice = 'SIN'
  sub:EDIInvoice = 'NO'
  sub:HideDespAdd = 0
  sub:EuroApplies = 0
  sub:ForceCommonFault = 0
  sub:ForceOrderNumber = 0
  sub:InvoiceAtDespatch = 0
  sub:InvoiceType = 0
  sub:IndividualSummary = 0
  sub:UseTradeContactNo = 0
  sub:E1 = 1
  sub:E2 = 1
  sub:E3 = 1
  sub:UseDespatchDetails = 0
  sub:UseAlternativeAdd = 0
  sub:AutoSendStatusEmails = 0
  sub:EmailRecipientList = 0
  sub:EmailEndUser = 0
  sub:VodafoneRetailStore = 0
  PARENT.PrimeFields


Hide:Relate:SUBTRACC.Kill PROCEDURE

  CODE
  Hide:Access:SUBTRACC.Kill
  PARENT.Kill
  Relate:SUBTRACC &= NULL


Hide:Access:CHARTYPE.Init PROCEDURE
  CODE
  SELF.Init(CHARTYPE,GlobalErrors)
  SELF.Buffer &= cha:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cha:Charge_Type_Key,'By Charge Type',0)
  SELF.AddKey(cha:Ref_Number_Key,'By Charge Type',0)
  SELF.AddKey(cha:Physical_Damage_Key,'By Charge Type',0)
  SELF.AddKey(cha:Warranty_Key,'By Charge Type',0)
  SELF.AddKey(cha:Warranty_Ref_Number_Key,'By Charge Type',0)
  Access:CHARTYPE &= SELF


Hide:Relate:CHARTYPE.Init PROCEDURE
  CODE
  Hide:Access:CHARTYPE.Init
  SELF.Init(Access:CHARTYPE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STDCHRGE,RI:CASCADE,RI:RESTRICT,sta:Charge_Type_Only_Key)
  SELF.AddRelationLink(cha:Charge_Type,sta:Charge_Type)
  SELF.AddRelation(Relate:DEFCHRGE,RI:CASCADE,RI:RESTRICT,dec:Charge_Type_Key)
  SELF.AddRelationLink(cha:Charge_Type,dec:Charge_Type)
  SELF.AddRelation(Relate:TRACHRGE,RI:CASCADE,RI:RESTRICT,trc:Charge_Type_Only_Key)
  SELF.AddRelationLink(cha:Charge_Type,trc:Charge_Type)
  SELF.AddRelation(Relate:SUBCHRGE,RI:CASCADE,RI:RESTRICT,suc:Charge_Type_Only_Key)
  SELF.AddRelationLink(cha:Charge_Type,suc:Charge_Type)


Hide:Access:CHARTYPE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CHARTYPE &= NULL


Hide:Access:CHARTYPE.PrimeFields PROCEDURE

  CODE
  cha:Force_Warranty = 'NO'
  cha:Allow_Physical_Damage = 'NO'
  cha:Allow_Estimate = 'NO'
  cha:Force_Estimate = 'NO'
  cha:Invoice_Customer = 'NO'
  cha:Invoice_Trade_Customer = 'NO'
  cha:Invoice_Manufacturer = 'NO'
  cha:No_Charge = 'NO'
  cha:Zero_Parts = 'NO'
  cha:Warranty = 'NO'
  cha:Exclude_EDI = 'NO'
  cha:ExcludeInvoice = 0
  PARENT.PrimeFields


Hide:Relate:CHARTYPE.Kill PROCEDURE

  CODE
  Hide:Access:CHARTYPE.Kill
  PARENT.Kill
  Relate:CHARTYPE &= NULL


Hide:Access:MANUFACT.Init PROCEDURE
  CODE
  SELF.Init(MANUFACT,GlobalErrors)
  SELF.Buffer &= man:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(man:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(man:Manufacturer_Key,'By Manufacturer',0)
  Access:MANUFACT &= SELF


Hide:Relate:MANUFACT.Init PROCEDURE
  CODE
  Hide:Access:MANUFACT.Init
  SELF.Init(Access:MANUFACT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:NOTESFAU,RI:CASCADE,RI:CASCADE,nof:Notes_Key)
  SELF.AddRelationLink(man:Manufacturer,nof:Manufacturer)
  SELF.AddRelation(Relate:MODELNUM,RI:CASCADE,RI:CASCADE,mod:Manufacturer_Key)
  SELF.AddRelationLink(man:Manufacturer,mod:Manufacturer)
  SELF.AddRelation(Relate:REPAIRTY,RI:CASCADE,RI:CASCADE,rep:Manufacturer_Key)
  SELF.AddRelationLink(man:Manufacturer,rep:Manufacturer)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:None,job:EDI_Key)
  SELF.AddRelationLink(man:Manufacturer,job:Manufacturer)
  SELF.AddRelation(Relate:MANFAULO,RI:CASCADE,RI:CASCADE,mfo:Field_Key)
  SELF.AddRelationLink(man:Manufacturer,mfo:Manufacturer)
  SELF.AddRelation(Relate:MANFAUPA,RI:CASCADE,RI:CASCADE,map:Field_Number_Key)
  SELF.AddRelationLink(man:Manufacturer,map:Manufacturer)
  SELF.AddRelation(Relate:MANFPALO,RI:CASCADE,RI:CASCADE,mfp:Field_Key)
  SELF.AddRelationLink(man:Manufacturer,mfp:Manufacturer)
  SELF.AddRelation(Relate:MANFAULT,RI:CASCADE,RI:CASCADE,maf:Field_Number_Key)
  SELF.AddRelationLink(man:Manufacturer,maf:Manufacturer)
  SELF.AddRelation(Relate:STDCHRGE,RI:CASCADE,RI:CASCADE,sta:Model_Number_Charge_Key)
  SELF.AddRelationLink(man:Manufacturer,sta:Model_Number)


Hide:Access:MANUFACT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANUFACT &= NULL


Hide:Access:MANUFACT.PrimeFields PROCEDURE

  CODE
  man:IncludeAdjustment = 'NO'
  man:AdjustPart = 0
  man:ForceParts = 0
  man:RemAccCosts = 0
  man:SiemensNewEDI = 0
  man:DOPCompulsory = 0
  man:UseQA = 0
  man:UseElectronicQA = 0
  man:QALoanExchange = 0
  man:QAAtCompletion = 0
  man:UseProductCode = 0
  man:QAParts = 0
  man:QANetwork = 0
  man:DisallowNA = 0
  PARENT.PrimeFields


Hide:Relate:MANUFACT.Kill PROCEDURE

  CODE
  Hide:Access:MANUFACT.Kill
  PARENT.Kill
  Relate:MANUFACT &= NULL


Hide:Access:USMASSIG.Init PROCEDURE
  CODE
  SELF.Init(USMASSIG,GlobalErrors)
  SELF.Buffer &= usm:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(usm:Model_Number_Key,'By Model Number',0)
  Access:USMASSIG &= SELF


Hide:Relate:USMASSIG.Init PROCEDURE
  CODE
  Hide:Access:USMASSIG.Init
  SELF.Init(Access:USMASSIG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:USERS)


Hide:Access:USMASSIG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:USMASSIG &= NULL


Hide:Relate:USMASSIG.Kill PROCEDURE

  CODE
  Hide:Access:USMASSIG.Kill
  PARENT.Kill
  Relate:USMASSIG &= NULL


Hide:Access:USUASSIG.Init PROCEDURE
  CODE
  SELF.Init(USUASSIG,GlobalErrors)
  SELF.Buffer &= usu:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(usu:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(usu:Unit_Type_Only_Key,'By Unit Type',0)
  Access:USUASSIG &= SELF


Hide:Relate:USUASSIG.Init PROCEDURE
  CODE
  Hide:Access:USUASSIG.Init
  SELF.Init(Access:USUASSIG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:USERS)
  SELF.AddRelation(Relate:UNITTYPE)


Hide:Access:USUASSIG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:USUASSIG &= NULL


Hide:Relate:USUASSIG.Kill PROCEDURE

  CODE
  Hide:Access:USUASSIG.Kill
  PARENT.Kill
  Relate:USUASSIG &= NULL


Hide:Access:JOBPAYMT_ALIAS.Init PROCEDURE
  CODE
  SELF.Init(JOBPAYMT_ALIAS,GlobalErrors)
  SELF.Buffer &= jpt_ali:Record
  SELF.AliasedFile &= Access:JOBPAYMT                 !This is a File Alias, so assign aliased file
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jpt_ali:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(jpt_ali:All_Date_Key,'By Date',0)
  Access:JOBPAYMT_ALIAS &= SELF


Hide:Relate:JOBPAYMT_ALIAS.Init PROCEDURE
  CODE
  Hide:Access:JOBPAYMT_ALIAS.Init
  SELF.Init(Access:JOBPAYMT_ALIAS,1)
  SELF.SetAlias(Relate:JOBPAYMT)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBPAYMT_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBPAYMT_ALIAS &= NULL


Hide:Relate:JOBPAYMT_ALIAS.Kill PROCEDURE

  CODE
  Hide:Access:JOBPAYMT_ALIAS.Kill
  PARENT.Kill
  Relate:JOBPAYMT_ALIAS &= NULL


Hide:Access:JOBS_ALIAS.Init PROCEDURE
  CODE
  SELF.Init(JOBS_ALIAS,GlobalErrors)
  SELF.Buffer &= job_ali:Record
  SELF.AliasedFile &= Access:JOBS                     !This is a File Alias, so assign aliased file
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(job_ali:Ref_Number_Key,'By Job Number',1)
  SELF.AddKey(job_ali:Model_Unit_Key,'By Job Number',0)
  SELF.AddKey(job_ali:EngCompKey,'By Job Number',0)
  SELF.AddKey(job_ali:EngWorkKey,'By Job Number',0)
  SELF.AddKey(job_ali:Surname_Key,'By Surname',0)
  SELF.AddKey(job_ali:MobileNumberKey,'By Mobile Number',0)
  SELF.AddKey(job_ali:ESN_Key,'By E.S.N. / I.M.E.I.',0)
  SELF.AddKey(job_ali:MSN_Key,'By M.S.N.',0)
  SELF.AddKey(job_ali:AccountNumberKey,'By Account Number',0)
  SELF.AddKey(job_ali:AccOrdNoKey,'By Order Number',0)
  SELF.AddKey(job_ali:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(job_ali:Engineer_Key,'By Engineer',0)
  SELF.AddKey(job_ali:Date_Booked_Key,'By Date Booked',0)
  SELF.AddKey(job_ali:DateCompletedKey,'By Date Completed',0)
  SELF.AddKey(job_ali:ModelCompKey,'By Completed Date',0)
  SELF.AddKey(job_ali:By_Status,'By Job Number',0)
  SELF.AddKey(job_ali:StatusLocKey,'By Job Number',0)
  SELF.AddKey(job_ali:Location_Key,'By Location',0)
  SELF.AddKey(job_ali:Third_Party_Key,'By Third Party',0)
  SELF.AddKey(job_ali:ThirdEsnKey,'By ESN',0)
  SELF.AddKey(job_ali:ThirdMsnKey,'By MSN',0)
  SELF.AddKey(job_ali:PriorityTypeKey,'By Priority',0)
  SELF.AddKey(job_ali:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(job_ali:EDI_Key,'By Job Number',0)
  SELF.AddKey(job_ali:InvoiceNumberKey,'By Invoice_Number',0)
  SELF.AddKey(job_ali:WarInvoiceNoKey,'By Invoice Number',0)
  SELF.AddKey(job_ali:Batch_Number_Key,'By Job Number',0)
  SELF.AddKey(job_ali:Batch_Status_Key,'By Ref Number',0)
  SELF.AddKey(job_ali:BatchModelNoKey,'By Job Number',0)
  SELF.AddKey(job_ali:BatchInvoicedKey,'By Ref Number',0)
  SELF.AddKey(job_ali:BatchCompKey,'By Job Number',0)
  SELF.AddKey(job_ali:ChaInvoiceKey,'job_ali:ChaInvoiceKey',0)
  SELF.AddKey(job_ali:InvoiceExceptKey,'By Job Number',0)
  SELF.AddKey(job_ali:ConsignmentNoKey,'By Cosignment Number',0)
  SELF.AddKey(job_ali:InConsignKey,'By Consignment Number',0)
  SELF.AddKey(job_ali:ReadyToDespKey,'By Job Number',0)
  SELF.AddKey(job_ali:ReadyToTradeKey,'By Job Number',0)
  SELF.AddKey(job_ali:ReadyToCouKey,'By Job Number',0)
  SELF.AddKey(job_ali:ReadyToAllKey,'By Job Number',0)
  SELF.AddKey(job_ali:DespJobNumberKey,'By Job Number',0)
  SELF.AddKey(job_ali:DateDespatchKey,'By Job Number',0)
  SELF.AddKey(job_ali:DateDespLoaKey,'By Job Number',0)
  SELF.AddKey(job_ali:DateDespExcKey,'By Job Number',0)
  SELF.AddKey(job_ali:ChaRepTypeKey,'By Chargeable Repair Type',0)
  SELF.AddKey(job_ali:WarRepTypeKey,'By Warranty Repair Type',0)
  SELF.AddKey(job_ali:ChaTypeKey,'job_ali:ChaTypeKey',0)
  SELF.AddKey(job_ali:WarChaTypeKey,'job_ali:WarChaTypeKey',0)
  SELF.AddKey(job_ali:Bouncer_Key,'By Job Number',0)
  SELF.AddKey(job_ali:EngDateCompKey,'By Date Completed',0)
  SELF.AddKey(job_ali:ExcStatusKey,'By Job Number',0)
  SELF.AddKey(job_ali:LoanStatusKey,'By Job Number',0)
  SELF.AddKey(job_ali:ExchangeLocKey,'By Job Number',0)
  SELF.AddKey(job_ali:LoanLocKey,'By Job Number',0)
  SELF.AddKey(job_ali:BatchJobKey,'By Job Number',0)
  SELF.AddKey(job_ali:BatchStatusKey,'By Job Number',0)
  Access:JOBS_ALIAS &= SELF


Hide:Relate:JOBS_ALIAS.Init PROCEDURE
  CODE
  Hide:Access:JOBS_ALIAS.Init
  SELF.Init(Access:JOBS_ALIAS,1)
  SELF.SetAlias(Relate:JOBS)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:BOUNCER,RI:CASCADE,RI:CASCADE,bou:Bouncer_Job_Only_Key)
  SELF.AddRelationLink(job_ali:Ref_Number,bou:Bouncer_Job_Number)
  SELF.AddRelation(Relate:REPAIRTY)


Hide:Access:JOBS_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBS_ALIAS &= NULL


Hide:Relate:JOBS_ALIAS.Kill PROCEDURE

  CODE
  Hide:Access:JOBS_ALIAS.Kill
  PARENT.Kill
  Relate:JOBS_ALIAS &= NULL

