   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
INCLUDE('MSGEX.INC')

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('SBRAI001.CLW')
Date_Range             PROCEDURE   !
     END
          MODULE('MsgEx Runtime Library')
            MessageEx(<STRING TheText>,      |
                      <STRING TheTitel>,     |
                      <STRING TheImage>,     |
                      <STRING TheButtons>,   |
                       SIGNED DefButton=0,   |
                       SIGNED EscButton=0,   |
                      <STRING CheckText>,    |
                      <*?     CheckVar>,     |
                      <STRING Fontname>,     |
                      SIGNED  Fontsize=8,    |
                      LONG    Fontcolor=0,   |
                      SIGNED  Fontstyle=0,   |
                      LONG    Charset=0,     |
                      LONG    BackColor=COLOR:NONE, |
                      <STRING Wallpaper>,    |
                      LONG    WallMode=0,    |
                      <STRING Sound>,        |
                      LONG    Options=0,     |
                      SIGNED  MinWidth=0,    |
                      SIGNED  MinHeight=0,   |
                      LONG    TimeOut=0      ),SIGNED,PROC,NAME('MESSAGEEX')
          END
     MODULE('SWiz255.lib')
       SSWizInit(<string>),DLL(dll_mode)
       SpreadWizard(QUEUE,<BYTE CalledFrom>,<LONG SysID>,<STRING ViewClass>,<tqField>,<*CSTRING Sort>,<QUEUE KeyQ>,tqField),LONG,PROC,DLL(dll_mode)
       SpreadStyle(*GROUP StyleSettings),DLL(dll_mode)
       UsBrowse(tqField,STRING ViewClass,<tqField>,<*CSTRING Sort>,<QUEUE KeyQ>,tqField,<*CSTRING SheetDesc>,<*LONG StartRow>,<*LONG StartCol>,<*CSTRING Template>),LONG,PROC,DLL(dll_mode)
       UsGetFileName(*CSTRING f1FileName, BYTE CallFileDialog, BYTE f1Action),BYTE,DLL(dll_mode)
       UsSaveType(*CSTRING f1FileName,LONG F1SS),BYTE,DLL(dll_mode)
       UsInitDoc(tqField,LONG,LONG,LONG,STRING),DLL(dll_mode)
       UsFillRow(tqField,LONG,*USHORT,LONG),DLL(dll_mode)
       BuildSQ(string,tqField,tqField),DLL(dll_mode)
       UsDesigner(LONG F1SS),BYTE,DLL(dll_mode)
       UsDeInit(*CSTRING f1FileName,LONG F1SS, BYTE f1Action),DLL(dll_mode)
     END
     !--------------------------------------------------------------------------
     ! -----------Tintools-------------
     !--------------------------------------------------------------------------
      MODULE('sbrai_VW.CLW')    !REPORT VIEWER
         TINCALENDARSTYLE1(<LONG>),LONG
         TINCALENDARSTYLE2(<STRING>,<LONG>),LONG
         TINCALENDARSTYLE3(<STRING>,<LONG>),LONG
         TINCALENDARSTYLE4(<STRING>,<LONG>),LONG
         TINCALENDARmonyear(<LONG>),LONG
         TINCALENDARmonth(<LONG>),LONG
      END
     !--------------------------------------------------------------------------
     ! -----------Tintools-------------
     !--------------------------------------------------------------------------
        MODULE('CELLMAIN.DLL')
cellmain:Init          PROCEDURE(<ErrorClass curGlobalErrors>, <INIClass curINIMgr>),DLL
cellmain:Kill          PROCEDURE,DLL
        END
   END

SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

STDCHRGE             FILE,DRIVER('Btrieve'),PRE(sta),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Model_Number_Charge_Key  KEY(sta:Model_Number,sta:Charge_Type,sta:Unit_Type,sta:Repair_Type),NOCASE,PRIMARY
Charge_Type_Key          KEY(sta:Model_Number,sta:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(sta:Model_Number,sta:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(sta:Model_Number,sta:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(sta:Model_Number,sta:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(sta:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(sta:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(sta:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Model_Number                STRING(30)
Unit_Type                   STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
                         END
                     END                       

JOBSENG              FILE,DRIVER('Btrieve'),OEM,PRE(joe),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(joe:RecordNumber),NOCASE,PRIMARY
UserCodeKey              KEY(joe:JobNumber,joe:UserCode,joe:DateAllocated),DUP,NOCASE
UserCodeOnlyKey          KEY(joe:UserCode,joe:DateAllocated),DUP,NOCASE
AllocatedKey             KEY(joe:JobNumber,joe:AllocatedBy,joe:DateAllocated),DUP,NOCASE
JobNumberKey             KEY(joe:JobNumber,-joe:DateAllocated,-joe:TimeAllocated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
UserCode                    STRING(3)
DateAllocated               DATE
TimeAllocated               STRING(20)
AllocatedBy                 STRING(3)
EngSkillLevel               LONG
JobSkillLevel               STRING(30)
                         END
                     END                       

AUDSTAEX             FILE,DRIVER('Btrieve'),OEM,PRE(aux),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(aux:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(aux:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AgentName                   STRING(30)
                         END
                     END                       

STMASAUD             FILE,DRIVER('Btrieve'),PRE(stom),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
AutoIncrement_Key        KEY(-stom:Audit_No),NOCASE,PRIMARY
Compeleted_Key           KEY(stom:Complete),DUP,NOCASE
Sent_Key                 KEY(stom:Send),DUP,NOCASE
Record                   RECORD,PRE()
Audit_No                    LONG
branch                      STRING(30)
branch_id                   LONG
Audit_Date                  LONG
Audit_Time                  LONG
End_Date                    LONG
End_Time                    LONG
Audit_User                  STRING(3)
Complete                    STRING(1)
Send                        STRING(1)
                         END
                     END                       

AUDSTATS             FILE,DRIVER('Btrieve'),OEM,PRE(aus),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(aus:RecordNumber),NOCASE,PRIMARY
DateChangedKey           KEY(aus:RefNumber,aus:Type,aus:DateChanged),DUP,NOCASE
NewStatusKey             KEY(aus:RefNumber,aus:Type,aus:NewStatus),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Type                        STRING(3)
DateChanged                 DATE
TimeChanged                 STRING(20)
OldStatus                   STRING(30)
NewStatus                   STRING(30)
UserCode                    STRING(3)
                         END
                     END                       

JOBTHIRD             FILE,DRIVER('Btrieve'),OEM,PRE(jot),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(jot:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jot:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(jot:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(jot:InIMEI),DUP,NOCASE
OutDateKey               KEY(jot:RefNumber,jot:DateOut,jot:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(jot:ThirdPartyNumber),DUP,NOCASE
OriginalIMEIKey          KEY(jot:OriginalIMEI),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                     END                       

PRODCODE             FILE,DRIVER('Btrieve'),OEM,PRE(prd),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(prd:RecordNumber),NOCASE,PRIMARY
ProductCodeKey           KEY(prd:ProductCode),NOCASE
ModelProductKey          KEY(prd:ModelNumber,prd:ProductCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ModelNumber                 STRING(30)
ProductCode                 STRING(30)
DummyField                  STRING(2)
                         END
                     END                       

STOAUDIT             FILE,DRIVER('Btrieve'),PRE(stoa),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Internal_AutoNumber_Key  KEY(stoa:Internal_AutoNumber),NOCASE,PRIMARY
Audit_Ref_No_Key         KEY(stoa:Audit_Ref_No),DUP,NOCASE
Cellular_Key             KEY(stoa:Site_Location,stoa:Stock_Ref_No),DUP,NOCASE
Stock_Ref_No_Key         KEY(stoa:Stock_Ref_No),DUP,NOCASE
Stock_Site_Number_Key    KEY(stoa:Audit_Ref_No,stoa:Site_Location),DUP,NOCASE
Lock_Down_Key            KEY(stoa:Audit_Ref_No,stoa:Stock_Ref_No),NOCASE
Record                   RECORD,PRE()
Internal_AutoNumber         LONG
Site_Location               STRING(30)
Stock_Ref_No                LONG
Original_Level              LONG
New_Level                   LONG
Audit_Reason                STRING(255)
Audit_Ref_No                LONG
Preliminary                 STRING(1)
Confirmed                   STRING(1)
                         END
                     END                       

BOUNCER              FILE,DRIVER('Btrieve'),OEM,PRE(bou),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Record_Number_Key        KEY(bou:Record_Number),NOCASE,PRIMARY
Bouncer_Job_Number_Key   KEY(bou:Original_Ref_Number,bou:Bouncer_Job_Number),NOCASE
Bouncer_Job_Only_Key     KEY(bou:Bouncer_Job_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Original_Ref_Number         REAL
Bouncer_Job_Number          REAL
                         END
                     END                       

TEAMS                FILE,DRIVER('Btrieve'),OEM,PRE(tea),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Record_Number_Key        KEY(tea:Record_Number),NOCASE,PRIMARY
Team_Key                 KEY(tea:Team),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Team                        STRING(30)
Completed_Jobs              LONG
                         END
                     END                       

MODELCOL             FILE,DRIVER('Btrieve'),OEM,PRE(moc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Record_Number_Key        KEY(moc:Record_Number),NOCASE,PRIMARY
Colour_Key               KEY(moc:Model_Number,moc:Colour),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Model_Number                STRING(30)
Colour                      STRING(30)
                         END
                     END                       

CONTHIST             FILE,DRIVER('Btrieve'),PRE(cht),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(cht:Ref_Number,-cht:Date,-cht:Time,cht:Action),DUP,NOCASE
Action_Key               KEY(cht:Ref_Number,cht:Action,-cht:Date),DUP,NOCASE
User_Key                 KEY(cht:Ref_Number,cht:User,-cht:Date),DUP,NOCASE
Record_Number_Key        KEY(cht:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(1000)
                         END
                     END                       

ACCESDEF             FILE,DRIVER('Btrieve'),PRE(acd),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Accessory_Key            KEY(acd:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Accessory                   STRING(30)
                         END
                     END                       

TRDSPEC              FILE,DRIVER('Btrieve'),PRE(tsp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Short_Description_Key    KEY(tsp:Short_Description),NOCASE,PRIMARY
Record                   RECORD,PRE()
Short_Description           STRING(30)
Long_Description            STRING(255)
                         END
                     END                       

ORDPEND              FILE,DRIVER('Btrieve'),PRE(ope),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(ope:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ope:Supplier,ope:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ope:Supplier,ope:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ope:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ope:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ope:Awaiting_Stock,ope:Supplier,ope:Part_Number),DUP,NOCASE
PartRecordNumberKey      KEY(ope:PartRecordNumber),DUP,NOCASE
Job_Number_Key           KEY(ope:Job_Number),DUP,NOCASE
Supplier_Job_Key         KEY(ope:Supplier,ope:Job_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
PartRecordNumber            LONG
                         END
                     END                       

STOHIST              FILE,DRIVER('Btrieve'),PRE(shi),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(shi:Ref_Number,shi:Date),DUP,NOCASE
record_number_key        KEY(shi:Record_Number),NOCASE,PRIMARY
Transaction_Type_Key     KEY(shi:Ref_Number,shi:Transaction_Type,shi:Date),DUP,NOCASE
DateKey                  KEY(shi:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
User                        STRING(3)
Transaction_Type            STRING(3)
Despatch_Note_Number        STRING(30)
Job_Number                  LONG
Sales_Number                LONG
Quantity                    REAL
Date                        DATE
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Notes                       STRING(255)
Information                 STRING(255)
                         END
                     END                       

PRIORITY             FILE,DRIVER('Btrieve'),PRE(pri),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Priority_Type_Key        KEY(pri:Priority_Type),NOCASE,PRIMARY
Record                   RECORD,PRE()
Priority_Type               STRING(30)
Time                        REAL
Book_Before                 TIME
                         END
                     END                       

JOBSE                FILE,DRIVER('Btrieve'),OEM,PRE(jobe),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(jobe:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobe:RefNumber),DUP,NOCASE
InWorkshopDateKey        KEY(jobe:InWorkshopDate),DUP,NOCASE
CompleteRepairKey        KEY(jobe:CompleteRepairDate,jobe:CompleteRepairTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
CustomerCollectionTime      TIME
EstimatedDespatchDate       DATE
EstimatedDespatchTime       TIME
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                     END                       

MANFAULT             FILE,DRIVER('Btrieve'),PRE(maf),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Field_Number_Key         KEY(maf:Manufacturer,maf:Field_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
                         END
                     END                       

TRACHAR              FILE,DRIVER('Btrieve'),PRE(tch),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Account_Number_Key       KEY(tch:Account_Number,tch:Charge_Type),NOCASE,PRIMARY
Record                   RECORD,PRE()
Account_Number              STRING(15)
Charge_Type                 STRING(30)
                         END
                     END                       

LOCVALUE             FILE,DRIVER('Btrieve'),OEM,PRE(lov),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(lov:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(lov:Location,lov:TheDate),DUP,NOCASE
DateOnly                 KEY(lov:TheDate),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
TheDate                     DATE
PurchaseTotal               REAL
SaleCostTotal               REAL
RetailCostTotal             REAL
QuantityTotal               LONG
                         END
                     END                       

COURIER              FILE,DRIVER('Btrieve'),PRE(cou),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Courier_Key              KEY(cou:Courier),NOCASE,PRIMARY
Courier_Type_Key         KEY(cou:Courier_Type,cou:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Courier                     STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Export_Path                 STRING(255)
Include_Estimate            STRING(3)
Include_Chargeable          STRING(3)
Include_Warranty            STRING(3)
Service                     STRING(15)
Import_Path                 STRING(255)
Courier_Type                STRING(30)
Last_Despatch_Time          TIME
ICOLSuffix                  STRING(3)
ContractNumber              STRING(20)
ANCPath                     STRING(255)
ANCCount                    LONG
DespatchClose               STRING(3)
LabGOptions                 STRING(60)
CustomerCollection          BYTE
NewStatus                   STRING(30)
NoOfDespatchNotes           LONG
AutoConsignmentNo           BYTE
LastConsignmentNo           LONG
PrintLabel                  BYTE
                         END
                     END                       

TRDPARTY             FILE,DRIVER('Btrieve'),PRE(trd),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Company_Name_Key         KEY(trd:Company_Name),NOCASE,PRIMARY
Account_Number_Key       KEY(trd:Account_Number),NOCASE
Special_Instructions_Key KEY(trd:Special_Instructions),DUP,NOCASE
Courier_Only_Key         KEY(trd:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Account_Number              STRING(30)
Contact_Name                STRING(60)
Address_Group               GROUP
Postcode                      STRING(15)
Address_Line1                 STRING(30)
Address_Line2                 STRING(30)
Address_Line3                 STRING(30)
Telephone_Number              STRING(15)
Fax_Number                    STRING(15)
                            END
Turnaround_Time             REAL
Courier_Direct              STRING(3)
Courier                     STRING(30)
Print_Order                 STRING(15)
Special_Instructions        STRING(30)
Batch_Number                LONG
BatchLimit                  LONG
                         END
                     END                       

JOBNOTES             FILE,DRIVER('Btrieve'),OEM,PRE(jbn),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RefNumberKey             KEY(jbn:RefNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RefNumber                   LONG
Fault_Description           STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Collection_Text             STRING(255)
Delivery_Text               STRING(255)
ColContatName               STRING(30)
ColDepartment               STRING(30)
DelContactName              STRING(30)
DelDepartment               STRING(30)
                         END
                     END                       

JOBPAYMT             FILE,DRIVER('Btrieve'),PRE(jpt),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Record_Number_Key        KEY(jpt:Record_Number),NOCASE,PRIMARY
All_Date_Key             KEY(jpt:Ref_Number,jpt:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Date                        DATE
Payment_Type                STRING(30)
Credit_Card_Number          STRING(20)
Expiry_Date                 STRING(5)
Issue_Number                STRING(5)
Amount                      REAL
User_Code                   STRING(3)
                         END
                     END                       

STOCK                FILE,DRIVER('Btrieve'),PRE(sto),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(sto:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(sto:Sundry_Item),DUP,NOCASE
Description_Key          KEY(sto:Location,sto:Description),DUP,NOCASE
Description_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Description),DUP,NOCASE
Shelf_Location_Key       KEY(sto:Location,sto:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(sto:Location,sto:Accessory,sto:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(sto:Manufacturer,sto:Part_Number),DUP,NOCASE
Supplier_Key             KEY(sto:Location,sto:Supplier),DUP,NOCASE
Location_Key             KEY(sto:Location,sto:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(sto:Location,sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(sto:Location,sto:Manufacturer,sto:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Manufacturer,sto:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(sto:Location,sto:Part_Number,sto:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Description),DUP,NOCASE
SecondLocKey             KEY(sto:Location,sto:Shelf_Location,sto:Second_Location,sto:Part_Number),DUP,NOCASE
RequestedKey             KEY(sto:QuantityRequested,sto:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(sto:ExchangeUnit,sto:Location,sto:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(sto:ExchangeUnit,sto:Location,sto:Description),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
AllowDuplicate              BYTE
ExcludeFromEDI              BYTE
RF_Board                    BYTE
                         END
                     END                       

TRAFAULO             FILE,DRIVER('Btrieve'),PRE(tfo),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Field_Key                KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Field,tfo:Description),NOCASE,PRIMARY
DescriptionKey           KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                     END                       

TRAFAULT             FILE,DRIVER('Btrieve'),PRE(taf),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(taf:RecordNumber),NOCASE,PRIMARY
Field_Number_Key         KEY(taf:AccountNumber,taf:Field_Number),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
                         END
                     END                       

JOBLOHIS             FILE,DRIVER('Btrieve'),PRE(jlh),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(jlh:Ref_Number,jlh:Date,jlh:Time),DUP,NOCASE
record_number_key        KEY(jlh:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Loan_Unit_Number            REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

ACCESSOR             FILE,DRIVER('Btrieve'),PRE(acr),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Accesory_Key             KEY(acr:Model_Number,acr:Accessory),NOCASE,PRIMARY
Model_Number_Key         KEY(acr:Accessory,acr:Model_Number),NOCASE
AccessOnlyKey            KEY(acr:Accessory),DUP,NOCASE
Record                   RECORD,PRE()
Accessory                   STRING(30)
Model_Number                STRING(30)
                         END
                     END                       

SUBACCAD             FILE,DRIVER('Btrieve'),OEM,PRE(sua),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(sua:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(sua:RefNumber,sua:AccountNumber),DUP,NOCASE
CompanyNameKey           KEY(sua:RefNumber,sua:CompanyName),DUP,NOCASE
AccountNumberOnlyKey     KEY(sua:AccountNumber),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AccountNumber               STRING(15)
CompanyName                 STRING(30)
Postcode                    STRING(15)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
TelephoneNumber             STRING(15)
FaxNumber                   STRING(15)
EmailAddress                STRING(255)
ContactName                 STRING(30)
                         END
                     END                       

WARPARTS             FILE,DRIVER('Btrieve'),PRE(wpr),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Part_Number_Key          KEY(wpr:Ref_Number,wpr:Part_Number),DUP,NOCASE
RecordNumberKey          KEY(wpr:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(wpr:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(wpr:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(wpr:Ref_Number,wpr:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(wpr:Ref_Number,wpr:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(wpr:Ref_Number,wpr:Order_Number),DUP,NOCASE
Supplier_Key             KEY(wpr:Supplier),DUP,NOCASE
Order_Part_Key           KEY(wpr:Ref_Number,wpr:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(wpr:Supplier,wpr:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(wpr:Supplier,wpr:Date_Received),DUP,NOCASE
RequestedKey             KEY(wpr:Requested,wpr:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           REAL
Date_Received               DATE
Status_Date                 DATE
Main_Part                   STRING(3)
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
                         END
                     END                       

PARTS                FILE,DRIVER('Btrieve'),PRE(par),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Part_Number_Key          KEY(par:Ref_Number,par:Part_Number),DUP,NOCASE
recordnumberkey          KEY(par:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(par:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(par:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(par:Ref_Number,par:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(par:Ref_Number,par:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(par:Ref_Number,par:Order_Number),DUP,NOCASE
Supplier_Key             KEY(par:Supplier),DUP,NOCASE
Order_Part_Key           KEY(par:Ref_Number,par:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(par:Supplier,par:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(par:Supplier,par:Date_Received),DUP,NOCASE
RequestedKey             KEY(par:Requested,par:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           LONG
Date_Received               DATE
Status_Date                 DATE
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
                         END
                     END                       

JOBACC               FILE,DRIVER('Btrieve'),PRE(jac),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(jac:Ref_Number,jac:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
                         END
                     END                       

AUDIT                FILE,DRIVER('Btrieve'),PRE(aud),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(aud:Ref_Number,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE
Action_Key               KEY(aud:Ref_Number,aud:Action,-aud:Date),DUP,NOCASE
User_Key                 KEY(aud:Ref_Number,aud:User,-aud:Date),DUP,NOCASE
Record_Number_Key        KEY(aud:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(aud:Action,aud:Date),DUP,NOCASE
TypeRefKey               KEY(aud:Ref_Number,aud:Type,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE
TypeActionKey            KEY(aud:Ref_Number,aud:Type,aud:Action,-aud:Date),DUP,NOCASE
TypeUserKey              KEY(aud:Ref_Number,aud:Type,aud:User,-aud:Date),DUP,NOCASE
DateActionJobKey         KEY(aud:Date,aud:Action,aud:Ref_Number),DUP,NOCASE
DateJobKey               KEY(aud:Date,aud:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(aud:Date,aud:Type,aud:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(aud:Date,aud:Type,aud:Action,aud:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
DummyField                  STRING(2)
Notes                       STRING(255)
Type                        STRING(3)
                         END
                     END                       

USERS                FILE,DRIVER('Btrieve'),PRE(use),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
User_Code_Key            KEY(use:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(use:User_Type,use:Active,use:Surname),DUP,NOCASE
Surname_Active_Key       KEY(use:Active,use:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(use:Active,use:User_Code),DUP,NOCASE
User_Type_Key            KEY(use:User_Type,use:Surname),DUP,NOCASE
surname_key              KEY(use:Surname),DUP,NOCASE
password_key             KEY(use:Password),NOCASE
Logged_In_Key            KEY(use:Logged_In,use:Surname),DUP,NOCASE
Team_Surname             KEY(use:Team,use:Surname),DUP,NOCASE
Active_Team_Surname      KEY(use:Team,use:Active,use:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
StockFromLocationOnly       BYTE
EmailAddress                STRING(255)
                         END
                     END                       

LOCSHELF             FILE,DRIVER('Btrieve'),PRE(los),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Shelf_Location_Key       KEY(los:Site_Location,los:Shelf_Location),NOCASE,PRIMARY
Record                   RECORD,PRE()
Site_Location               STRING(30)
Shelf_Location              STRING(30)
                         END
                     END                       

MANFPALO             FILE,DRIVER('Btrieve'),PRE(mfp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Field_Key                KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Field),NOCASE,PRIMARY
DescriptionKey           KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Description),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                     END                       

JOBSTAGE             FILE,DRIVER('Btrieve'),PRE(jst),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(jst:Ref_Number,-jst:Date),DUP,NOCASE
Job_Stage_Key            KEY(jst:Ref_Number,jst:Job_Stage),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Job_Stage                   STRING(30)
Date                        DATE
Time                        TIME
User                        STRING(3)
                         END
                     END                       

MANFAUPA             FILE,DRIVER('Btrieve'),PRE(map),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Field_Number_Key         KEY(map:Manufacturer,map:Field_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
                         END
                     END                       

LOCINTER             FILE,DRIVER('Btrieve'),PRE(loi),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Location_Key             KEY(loi:Location),NOCASE,PRIMARY
Location_Available_Key   KEY(loi:Location_Available,loi:Location),DUP,NOCASE
Record                   RECORD,PRE()
Location                    STRING(30)
Location_Available          STRING(3)
Allocate_Spaces             STRING(3)
Total_Spaces                REAL
Current_Spaces              STRING(6)
                         END
                     END                       

NOTESFAU             FILE,DRIVER('Btrieve'),PRE(nof),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Notes_Key                KEY(nof:Manufacturer,nof:Reference,nof:Notes),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Reference                   STRING(30)
Notes                       STRING(80)
                         END
                     END                       

MANFAULO             FILE,DRIVER('Btrieve'),PRE(mfo),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Field_Key                KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Field),NOCASE,PRIMARY
DescriptionKey           KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Description),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                     END                       

JOBEXHIS             FILE,DRIVER('Btrieve'),PRE(jxh),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(jxh:Ref_Number,jxh:Date,jxh:Time),DUP,NOCASE
record_number_key        KEY(jxh:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Loan_Unit_Number            REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

ORDPARTS             FILE,DRIVER('Btrieve'),PRE(orp),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Order_Number_Key         KEY(orp:Order_Number,orp:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(orp:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(orp:Part_Ref_Number,orp:Part_Number,orp:Description),DUP,NOCASE
Part_Number_Key          KEY(orp:Order_Number,orp:Part_Number,orp:Description),DUP,NOCASE
Description_Key          KEY(orp:Order_Number,orp:Description,orp:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(orp:All_Received,orp:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(orp:Account_Number,orp:Part_Type,orp:All_Received,orp:Part_Number),DUP,NOCASE
Allocated_Key            KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Account_Number,orp:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(orp:Order_Number,orp:Part_Type,orp:All_Received,orp:Part_Number,orp:Description),DUP,NOCASE
PartRecordNumberKey      KEY(orp:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
PartRecordNumber            LONG
                         END
                     END                       

LOCATION             FILE,DRIVER('Btrieve'),PRE(loc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(loc:RecordNumber),NOCASE,PRIMARY
Location_Key             KEY(loc:Location),NOCASE
Main_Store_Key           KEY(loc:Main_Store,loc:Location),DUP,NOCASE
ActiveLocationKey        KEY(loc:Active,loc:Location),DUP,NOCASE
ActiveMainStoreKey       KEY(loc:Active,loc:Main_Store,loc:Location),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
Main_Store                  STRING(3)
Active                      BYTE
PickNoteEnable              BYTE
                         END
                     END                       

STOESN               FILE,DRIVER('Btrieve'),OEM,PRE(ste),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Record_Number_Key        KEY(ste:Record_Number),NOCASE,PRIMARY
Sold_Key                 KEY(ste:Ref_Number,ste:Sold,ste:Serial_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Serial_Number               STRING(16)
Sold                        STRING(3)
                         END
                     END                       

SUPPLIER             FILE,DRIVER('Btrieve'),PRE(sup),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(sup:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(sup:Account_Number),DUP,NOCASE
Company_Name_Key         KEY(sup:Company_Name),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Company_Name                STRING(30)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(60)
Account_Number              STRING(15)
Order_Period                REAL
Normal_Supply_Period        REAL
Minimum_Order_Value         REAL
HistoryUsage                LONG
Factor                      REAL
Notes                       STRING(255)
                         END
                     END                       

SUPVALA              FILE,DRIVER('Btrieve'),OEM,PRE(suva),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(suva:RecordNumber),NOCASE,PRIMARY
RunDateKey               KEY(suva:Supplier,suva:RunDate),DUP,NOCASE
DateOnly                 KEY(suva:RunDate),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Supplier                    STRING(30)
RunDate                     DATE
OldBackOrder                DATE
OldOutOrder                 DATE
                         END
                     END                       

ESTPARTS             FILE,DRIVER('Btrieve'),PRE(epr),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Part_Number_Key          KEY(epr:Ref_Number,epr:Part_Number),DUP,NOCASE
record_number_key        KEY(epr:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(epr:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(epr:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(epr:Ref_Number,epr:Part_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(epr:Ref_Number,epr:Order_Number),DUP,NOCASE
Supplier_Key             KEY(epr:Supplier),DUP,NOCASE
Order_Part_Key           KEY(epr:Ref_Number,epr:Order_Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Date_Received               DATE
Order_Part_Number           REAL
Status_Date                 DATE
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

TRAEMAIL             FILE,DRIVER('Btrieve'),OEM,PRE(tre),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(tre:RecordNumber),NOCASE,PRIMARY
RecipientKey             KEY(tre:RefNumber,tre:RecipientType),DUP,NOCASE
ContactNameKey           KEY(tre:RefNumber,tre:ContactName),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
RecipientType               STRING(30)
ContactName                 STRING(60)
EmailAddress                STRING(255)
                         END
                     END                       

SUPVALB              FILE,DRIVER('Btrieve'),OEM,PRE(suvb),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(suvb:RecordNumber),NOCASE,PRIMARY
RunDateKey               KEY(suvb:Supplier,suvb:RunDate),DUP,NOCASE
LocationKey              KEY(suvb:Supplier,suvb:RunDate,suvb:Location),DUP,NOCASE
DateOnly                 KEY(suvb:RunDate),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Supplier                    STRING(30)
RunDate                     DATE
Location                    STRING(30)
BackOrderValue              REAL
                         END
                     END                       

JOBS                 FILE,DRIVER('Btrieve'),PRE(job),BINDABLE,CREATE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(job:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(job:Model_Number,job:Unit_Type),DUP,NOCASE
EngCompKey               KEY(job:Engineer,job:Completed,job:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(job:Engineer,job:Workshop,job:Ref_Number),DUP,NOCASE
Surname_Key              KEY(job:Surname),DUP,NOCASE
MobileNumberKey          KEY(job:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(job:ESN),DUP,NOCASE
MSN_Key                  KEY(job:MSN),DUP,NOCASE
AccountNumberKey         KEY(job:Account_Number,job:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(job:Account_Number,job:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(job:Model_Number),DUP,NOCASE
Engineer_Key             KEY(job:Engineer,job:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(job:date_booked),DUP,NOCASE
DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(job:Model_Number,job:Date_Completed),DUP,NOCASE
By_Status                KEY(job:Current_Status,job:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(job:Current_Status,job:Location,job:Ref_Number),DUP,NOCASE
Location_Key             KEY(job:Location,job:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(job:Third_Party_Site,job:Third_Party_Printed,job:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:ESN),DUP,NOCASE
ThirdMsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:MSN),DUP,NOCASE
PriorityTypeKey          KEY(job:Job_Priority,job:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(job:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(job:Manufacturer,job:EDI,job:EDI_Batch_Number,job:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(job:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(job:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(job:Batch_Number,job:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(job:Batch_Number,job:Current_Status,job:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(job:Batch_Number,job:Model_Number,job:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(job:Batch_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(job:Batch_Number,job:Completed,job:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(job:Chargeable_Job,job:Account_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(job:Invoice_Exception,job:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(job:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(job:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(job:Despatched,job:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(job:Despatched,job:Account_Number,job:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(job:Despatched,job:Current_Courier,job:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(job:Despatched,job:Account_Number,job:Current_Courier,job:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(job:Despatch_Number,job:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(job:Courier,job:Date_Despatched,job:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(job:Loan_Courier,job:Loan_Despatched,job:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(job:Exchange_Courier,job:Exchange_Despatched,job:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(job:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(job:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(job:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(job:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(job:Bouncer,job:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(job:Engineer,job:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(job:Exchange_Status,job:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(job:Loan_Status,job:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(job:Exchange_Status,job:Location,job:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(job:Loan_Status,job:Location,job:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(job:InvoiceAccount,job:InvoiceBatch,job:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(job:InvoiceAccount,job:InvoiceBatch,job:InvoiceStatus,job:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

MODELNUM             FILE,DRIVER('Btrieve'),PRE(mod),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Model_Number_Key         KEY(mod:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(mod:Manufacturer,mod:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(mod:Manufacturer,mod:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
                         END
                     END                       

SUBEMAIL             FILE,DRIVER('Btrieve'),OEM,PRE(sue),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(sue:RecordNumber),NOCASE,PRIMARY
RecipientTypeKey         KEY(sue:RefNumber,sue:RecipientType),DUP,NOCASE
ContactNameKey           KEY(sue:RefNumber,sue:ContactName),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
RecipientType               STRING(30)
ContactName                 STRING(60)
EmailAddress                STRING(255)
                         END
                     END                       

ORDERS               FILE,DRIVER('Btrieve'),PRE(ord),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Order_Number_Key         KEY(ord:Order_Number),NOCASE,PRIMARY
Printed_Key              KEY(ord:Printed,ord:Order_Number),DUP,NOCASE
Supplier_Printed_Key     KEY(ord:Printed,ord:Supplier,ord:Order_Number),DUP,NOCASE
Received_Key             KEY(ord:All_Received,ord:Order_Number),DUP,NOCASE
Supplier_Key             KEY(ord:Supplier,ord:Order_Number),DUP,NOCASE
Supplier_Received_Key    KEY(ord:Supplier,ord:All_Received,ord:Order_Number),DUP,NOCASE
DateKey                  KEY(ord:Date),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                REAL
Supplier                    STRING(30)
Date                        DATE
Printed                     STRING(3)
All_Received                STRING(3)
User                        STRING(3)
Dummy_Field                 STRING(1)
                         END
                     END                       

UNITTYPE             FILE,DRIVER('Btrieve'),PRE(uni),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Unit_Type_Key            KEY(uni:Unit_Type),NOCASE,PRIMARY
ActiveKey                KEY(uni:Active,uni:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Unit_Type                   STRING(30)
Active                      BYTE
                         END
                     END                       

REPAIRTY             FILE,DRIVER('Btrieve'),PRE(rep),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Repair_Type_Key          KEY(rep:Manufacturer,rep:Model_Number,rep:Repair_Type),NOCASE,PRIMARY
Manufacturer_Key         KEY(rep:Manufacturer,rep:Repair_Type),DUP,NOCASE
Model_Number_Key         KEY(rep:Model_Number,rep:Repair_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(rep:Repair_Type),DUP,NOCASE
Model_Chargeable_Key     KEY(rep:Model_Number,rep:Chargeable,rep:Repair_Type),DUP,NOCASE
Model_Warranty_Key       KEY(rep:Model_Number,rep:Warranty,rep:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
                         END
                     END                       

INVOICE              FILE,DRIVER('Btrieve'),PRE(inv),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Invoice_Number_Key       KEY(inv:Invoice_Number),NOCASE,PRIMARY
Invoice_Type_Key         KEY(inv:Invoice_Type,inv:Invoice_Number),DUP,NOCASE
Account_Number_Type_Key  KEY(inv:Invoice_Type,inv:Account_Number,inv:Invoice_Number),DUP,NOCASE
Account_Number_Key       KEY(inv:Account_Number,inv:Invoice_Number),DUP,NOCASE
Date_Created_Key         KEY(inv:Date_Created),DUP,NOCASE
Batch_Number_Key         KEY(inv:Manufacturer,inv:Batch_Number),DUP,NOCASE
Record                   RECORD,PRE()
Invoice_Number              REAL
Invoice_Type                STRING(3)
Job_Number                  REAL
Date_Created                DATE
Account_Number              STRING(15)
AccountType                 STRING(3)
Total                       REAL
Vat_Rate_Labour             REAL
Vat_Rate_Parts              REAL
Vat_Rate_Retail             REAL
VAT_Number                  STRING(30)
Invoice_VAT_Number          STRING(30)
Currency                    STRING(30)
Batch_Number                REAL
Manufacturer                STRING(30)
Claim_Reference             STRING(30)
Total_Claimed               REAL
Courier_Paid                REAL
Labour_Paid                 REAL
Parts_Paid                  REAL
Reconciled_Date             DATE
jobs_count                  REAL
PrevInvoiceNo               LONG
InvoiceCredit               STRING(3)
UseAlternativeAddress       BYTE
EuroExhangeRate             REAL
                         END
                     END                       

TRDBATCH             FILE,DRIVER('Btrieve'),OEM,PRE(trb),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(trb:RecordNumber),NOCASE,PRIMARY
Batch_Number_Key         KEY(trb:Company_Name,trb:Batch_Number),DUP,NOCASE
Batch_Number_Status_Key  KEY(trb:Status,trb:Batch_Number),DUP,NOCASE
BatchOnlyKey             KEY(trb:Batch_Number),DUP,NOCASE
Batch_Company_Status_Key KEY(trb:Status,trb:Company_Name,trb:Batch_Number),DUP,NOCASE
ESN_Only_Key             KEY(trb:ESN),DUP,NOCASE
ESN_Status_Key           KEY(trb:Status,trb:ESN),DUP,NOCASE
Company_Batch_ESN_Key    KEY(trb:Company_Name,trb:Batch_Number,trb:ESN),DUP,NOCASE
AuthorisationKey         KEY(trb:AuthorisationNo),DUP,NOCASE
StatusAuthKey            KEY(trb:Status,trb:AuthorisationNo),DUP,NOCASE
CompanyDateKey           KEY(trb:Company_Name,trb:Status,trb:DateReturn),DUP,NOCASE
JobStatusKey             KEY(trb:Status,trb:Ref_Number),DUP,NOCASE
JobNumberKey             KEY(trb:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Batch_Number                LONG
Company_Name                STRING(30)
Ref_Number                  LONG
ESN                         STRING(20)
Status                      STRING(3)
Date                        DATE
Time                        TIME
AuthorisationNo             STRING(30)
Exchanged                   STRING(3)
DateReturn                  DATE
TimeReturn                  TIME
TurnTime                    LONG
MSN                         STRING(30)
                         END
                     END                       

TRACHRGE             FILE,DRIVER('Btrieve'),PRE(trc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Account_Charge_Key       KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type,trc:Unit_Type,trc:Repair_Type),NOCASE,PRIMARY
Model_Repair_Key         KEY(trc:Model_Number,trc:Repair_Type),DUP,NOCASE
Charge_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(trc:Account_Number,trc:Model_Number,trc:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(trc:Account_Number,trc:Model_Number,trc:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(trc:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(trc:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(trc:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Charge_Type                 STRING(30)
Unit_Type                   STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
                         END
                     END                       

TRDMODEL             FILE,DRIVER('Btrieve'),PRE(trm),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Model_Number_Key         KEY(trm:Company_Name,trm:Model_Number),NOCASE,PRIMARY
Model_Number_Only_Key    KEY(trm:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Model_Number                STRING(30)
                         END
                     END                       

STOMODEL             FILE,DRIVER('Btrieve'),PRE(stm),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Model_Number_Key         KEY(stm:Ref_Number,stm:Manufacturer,stm:Model_Number),NOCASE,PRIMARY
Model_Part_Number_Key    KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE
Description_Key          KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE
Manufacturer_Key         KEY(stm:Manufacturer,stm:Model_Number),DUP,NOCASE
Ref_Part_Description     KEY(stm:Ref_Number,stm:Part_Number,stm:Location,stm:Description),DUP,NOCASE
Location_Part_Number_Key KEY(stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE
Location_Description_Key KEY(stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE
Mode_Number_Only_Key     KEY(stm:Ref_Number,stm:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Manufacturer                STRING(30)
Model_Number                STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Location                    STRING(30)
Accessory                   STRING(3)
FaultCode1                  STRING(30)
FaultCode2                  STRING(30)
FaultCode3                  STRING(30)
FaultCode4                  STRING(30)
FaultCode5                  STRING(30)
FaultCode6                  STRING(30)
FaultCode7                  STRING(30)
FaultCode8                  STRING(30)
FaultCode9                  STRING(30)
FaultCode10                 STRING(30)
FaultCode11                 STRING(30)
FaultCode12                 STRING(30)
                         END
                     END                       

TRADEACC             FILE,DRIVER('Btrieve'),PRE(tra),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(tra:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(tra:Account_Number),NOCASE
Company_Name_Key         KEY(tra:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
MakeSplitJob                BYTE
SplitJobStatus              STRING(30)
SplitExchangeStatus         STRING(30)
SplitCChargeType            STRING(30)
SplitWChargeType            STRING(30)
CheckChargeablePartsCost    BYTE
MaxChargeablePartsCost      REAL
UseAlternateDespatchNote    BYTE
                         END
                     END                       

SUBCHRGE             FILE,DRIVER('Btrieve'),PRE(suc),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Model_Repair_Type_Key    KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type,suc:Unit_Type,suc:Repair_Type),NOCASE,PRIMARY
Account_Charge_Key       KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(suc:Account_Number,suc:Model_Number,suc:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(suc:Account_Number,suc:Model_Number,suc:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(suc:Account_Number,suc:Model_Number,suc:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(suc:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(suc:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(suc:Unit_Type),DUP,NOCASE
Model_Repair_Key         KEY(suc:Model_Number,suc:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Charge_Type                 STRING(30)
Unit_Type                   STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
                         END
                     END                       

DEFCHRGE             FILE,DRIVER('Btrieve'),PRE(dec),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Charge_Type_Key          KEY(dec:Charge_Type),NOCASE,PRIMARY
Repair_Type_Only_Key     KEY(dec:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(dec:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Unit_Type                   STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
                         END
                     END                       

SUBTRACC             FILE,DRIVER('Btrieve'),PRE(sub),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(sub:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(sub:Main_Account_Number,sub:Account_Number),NOCASE
Main_Name_Key            KEY(sub:Main_Account_Number,sub:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(sub:Account_Number),NOCASE
Branch_Key               KEY(sub:Branch),DUP,NOCASE
Main_Branch_Key          KEY(sub:Main_Account_Number,sub:Branch),DUP,NOCASE
Company_Name_Key         KEY(sub:Company_Name),DUP,NOCASE
Contact_Centre_Key       KEY(sub:IsContactCentre,sub:Account_Number),DUP,NOCASE
RetailStoreKey           KEY(sub:VodafoneRetailStore,sub:Account_Number),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
IsContactCentre             BYTE
VodafoneRetailStore         BYTE
                         END
                     END                       

CHARTYPE             FILE,DRIVER('Btrieve'),PRE(cha),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Charge_Type_Key          KEY(cha:Charge_Type),NOCASE,PRIMARY
Ref_Number_Key           KEY(cha:Ref_Number,cha:Charge_Type),DUP,NOCASE
Physical_Damage_Key      KEY(cha:Allow_Physical_Damage,cha:Charge_Type),DUP,NOCASE
Warranty_Key             KEY(cha:Warranty,cha:Charge_Type),DUP,NOCASE
Warranty_Ref_Number_Key  KEY(cha:Warranty,cha:Ref_Number,cha:Charge_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Force_Warranty              STRING(3)
Allow_Physical_Damage       STRING(3)
Allow_Estimate              STRING(3)
Force_Estimate              STRING(3)
Invoice_Customer            STRING(3)
Invoice_Trade_Customer      STRING(3)
Invoice_Manufacturer        STRING(3)
No_Charge                   STRING(3)
Zero_Parts                  STRING(3)
Warranty                    STRING(3)
Ref_Number                  REAL
Exclude_EDI                 STRING(3)
ExcludeInvoice              BYTE
                         END
                     END                       

MANUFACT             FILE,DRIVER('Btrieve'),PRE(man),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
RecordNumberKey          KEY(man:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(man:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
IsCCentreSpecific           BYTE
EDIFileType                 STRING(30)
                         END
                     END                       

USMASSIG             FILE,DRIVER('Btrieve'),PRE(usm),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Model_Number_Key         KEY(usm:User_Code,usm:Model_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Code                   STRING(3)
Model_Number                STRING(30)
                         END
                     END                       

USUASSIG             FILE,DRIVER('Btrieve'),PRE(usu),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Unit_Type_Key            KEY(usu:User_Code,usu:Unit_Type),NOCASE,PRIMARY
Unit_Type_Only_Key       KEY(usu:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Unit_Type                   STRING(30)
                         END
                     END                       

JOBPAYMT_ALIAS       FILE,DRIVER('Btrieve'),PRE(jpt_ali),CREATE,BINDABLE,THREAD,EXTERNAL(''),DLL(dll_mode)
Record_Number_Key        KEY(jpt_ali:Record_Number),NOCASE,PRIMARY
All_Date_Key             KEY(jpt_ali:Ref_Number,jpt_ali:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Date                        DATE
Payment_Type                STRING(30)
Credit_Card_Number          STRING(20)
Expiry_Date                 STRING(5)
Issue_Number                STRING(5)
Amount                      REAL
User_Code                   STRING(3)
                         END
                     END                       

JOBS_ALIAS           FILE,DRIVER('Btrieve'),PRE(job_ali),BINDABLE,CREATE,THREAD,EXTERNAL(''),DLL(dll_mode)
Ref_Number_Key           KEY(job_ali:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(job_ali:Model_Number,job_ali:Unit_Type),DUP,NOCASE
EngCompKey               KEY(job_ali:Engineer,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(job_ali:Engineer,job_ali:Workshop,job_ali:Ref_Number),DUP,NOCASE
Surname_Key              KEY(job_ali:Surname),DUP,NOCASE
MobileNumberKey          KEY(job_ali:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(job_ali:ESN),DUP,NOCASE
MSN_Key                  KEY(job_ali:MSN),DUP,NOCASE
AccountNumberKey         KEY(job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(job_ali:Account_Number,job_ali:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(job_ali:Model_Number),DUP,NOCASE
Engineer_Key             KEY(job_ali:Engineer,job_ali:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(job_ali:date_booked),DUP,NOCASE
DateCompletedKey         KEY(job_ali:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(job_ali:Model_Number,job_ali:Date_Completed),DUP,NOCASE
By_Status                KEY(job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(job_ali:Current_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
Location_Key             KEY(job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:ESN),DUP,NOCASE
ThirdMsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:MSN),DUP,NOCASE
PriorityTypeKey          KEY(job_ali:Job_Priority,job_ali:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(job_ali:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(job_ali:Manufacturer,job_ali:EDI,job_ali:EDI_Batch_Number,job_ali:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(job_ali:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(job_ali:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(job_ali:Batch_Number,job_ali:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(job_ali:Batch_Number,job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(job_ali:Batch_Number,job_ali:Model_Number,job_ali:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(job_ali:Batch_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(job_ali:Batch_Number,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(job_ali:Chargeable_Job,job_ali:Account_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(job_ali:Invoice_Exception,job_ali:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(job_ali:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(job_ali:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(job_ali:Despatched,job_ali:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(job_ali:Despatched,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(job_ali:Despatch_Number,job_ali:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(job_ali:Courier,job_ali:Date_Despatched,job_ali:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(job_ali:Loan_Courier,job_ali:Loan_Despatched,job_ali:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(job_ali:Exchange_Courier,job_ali:Exchange_Despatched,job_ali:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(job_ali:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(job_ali:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(job_ali:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(job_ali:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(job_ali:Bouncer,job_ali:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(job_ali:Engineer,job_ali:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(job_ali:Exchange_Status,job_ali:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(job_ali:Loan_Status,job_ali:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(job_ali:Exchange_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(job_ali:Loan_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:InvoiceStatus,job_ali:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       



glo:ErrorText        STRING(1000),EXTERNAL,DLL(_ABCDllMode_)
glo:owner            STRING(20),EXTERNAL,DLL(_ABCDllMode_)
glo:DateModify       DATE,EXTERNAL,DLL(_ABCDllMode_)
glo:TimeModify       TIME,EXTERNAL,DLL(_ABCDllMode_)
glo:Notes_Global     STRING(1000),EXTERNAL,DLL(_ABCDllMode_)
glo:Q_Invoice        QUEUE,PRE(glo),EXTERNAL,DLL(_ABCDllMode_)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255),EXTERNAL,DLL(_ABCDllMode_)
glo:Password         STRING(20),EXTERNAL,DLL(_ABCDllMode_)
glo:PassAccount      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:Preview          STRING('True'),EXTERNAL,DLL(_ABCDllMode_)
glo:q_JobNumber      QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(glo),EXTERNAL,DLL(_ABCDllMode_)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3),EXTERNAL,DLL(_ABCDllMode_)
glo:TimeLogged       TIME,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:ApplicationName STRING(8),EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:ApplicationCreationDate LONG,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:ApplicationCreationTime LONG,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:ApplicationChangeDate LONG,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:ApplicationChangeTime LONG,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:Compiled32   BYTE,EXTERNAL,DLL(_ABCDllMode_)
glo:GLO:AppINIFile   STRING(255),EXTERNAL,DLL(_ABCDllMode_)
glo:Queue            QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO),EXTERNAL,DLL(_ABCDllMode_)
Pointer20              STRING(40)
                     END
glo:FaultCode1       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode2       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode3       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode4       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode5       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode6       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode7       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode8       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode9       STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode10      STRING(255),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode11      STRING(255),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode12      STRING(255),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode13      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode14      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode15      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode16      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode17      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode18      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode19      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:FaultCode20      STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode1  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode2  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode3  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode4  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode5  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode6  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode7  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode8  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode9  STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode10 STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode11 STRING(30),EXTERNAL,DLL(_ABCDllMode_)
glo:TradeFaultCode12 STRING(30),EXTERNAL,DLL(_ABCDllMode_)
tqField         QUEUE,TYPE
Desc              CSTRING(30)
Name              CSTRING(40)
IsString          BYTE
                END
!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
TinToolsViewRecord  EQUATE(15)
TinToolsCopyRecord  EQUATE(16)
!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
Access:STDCHRGE      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:STDCHRGE      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBSENG       &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBSENG       &RelationManager,EXTERNAL,DLL(dll_mode)
Access:AUDSTAEX      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:AUDSTAEX      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:STMASAUD      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:STMASAUD      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:AUDSTATS      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:AUDSTATS      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBTHIRD      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBTHIRD      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:PRODCODE      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:PRODCODE      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:STOAUDIT      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:STOAUDIT      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:BOUNCER       &FileManager,EXTERNAL,DLL(dll_mode)
Relate:BOUNCER       &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TEAMS         &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TEAMS         &RelationManager,EXTERNAL,DLL(dll_mode)
Access:MODELCOL      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:MODELCOL      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:CONTHIST      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:CONTHIST      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:ACCESDEF      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:ACCESDEF      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TRDSPEC       &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TRDSPEC       &RelationManager,EXTERNAL,DLL(dll_mode)
Access:ORDPEND       &FileManager,EXTERNAL,DLL(dll_mode)
Relate:ORDPEND       &RelationManager,EXTERNAL,DLL(dll_mode)
Access:STOHIST       &FileManager,EXTERNAL,DLL(dll_mode)
Relate:STOHIST       &RelationManager,EXTERNAL,DLL(dll_mode)
Access:PRIORITY      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:PRIORITY      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBSE         &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBSE         &RelationManager,EXTERNAL,DLL(dll_mode)
Access:MANFAULT      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:MANFAULT      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TRACHAR       &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TRACHAR       &RelationManager,EXTERNAL,DLL(dll_mode)
Access:LOCVALUE      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:LOCVALUE      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:COURIER       &FileManager,EXTERNAL,DLL(dll_mode)
Relate:COURIER       &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TRDPARTY      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TRDPARTY      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBNOTES      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBNOTES      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBPAYMT      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBPAYMT      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:STOCK         &FileManager,EXTERNAL,DLL(dll_mode)
Relate:STOCK         &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TRAFAULO      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TRAFAULO      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TRAFAULT      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TRAFAULT      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBLOHIS      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBLOHIS      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:ACCESSOR      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:ACCESSOR      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:SUBACCAD      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:SUBACCAD      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:WARPARTS      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:WARPARTS      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:PARTS         &FileManager,EXTERNAL,DLL(dll_mode)
Relate:PARTS         &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBACC        &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBACC        &RelationManager,EXTERNAL,DLL(dll_mode)
Access:AUDIT         &FileManager,EXTERNAL,DLL(dll_mode)
Relate:AUDIT         &RelationManager,EXTERNAL,DLL(dll_mode)
Access:USERS         &FileManager,EXTERNAL,DLL(dll_mode)
Relate:USERS         &RelationManager,EXTERNAL,DLL(dll_mode)
Access:LOCSHELF      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:LOCSHELF      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:MANFPALO      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:MANFPALO      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBSTAGE      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBSTAGE      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:MANFAUPA      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:MANFAUPA      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:LOCINTER      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:LOCINTER      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:NOTESFAU      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:NOTESFAU      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:MANFAULO      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:MANFAULO      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBEXHIS      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBEXHIS      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:ORDPARTS      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:ORDPARTS      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:LOCATION      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:LOCATION      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:STOESN        &FileManager,EXTERNAL,DLL(dll_mode)
Relate:STOESN        &RelationManager,EXTERNAL,DLL(dll_mode)
Access:SUPPLIER      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:SUPPLIER      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:SUPVALA       &FileManager,EXTERNAL,DLL(dll_mode)
Relate:SUPVALA       &RelationManager,EXTERNAL,DLL(dll_mode)
Access:ESTPARTS      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:ESTPARTS      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TRAEMAIL      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TRAEMAIL      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:SUPVALB       &FileManager,EXTERNAL,DLL(dll_mode)
Relate:SUPVALB       &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBS          &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBS          &RelationManager,EXTERNAL,DLL(dll_mode)
Access:MODELNUM      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:MODELNUM      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:SUBEMAIL      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:SUBEMAIL      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:ORDERS        &FileManager,EXTERNAL,DLL(dll_mode)
Relate:ORDERS        &RelationManager,EXTERNAL,DLL(dll_mode)
Access:UNITTYPE      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:UNITTYPE      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:REPAIRTY      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:REPAIRTY      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:INVOICE       &FileManager,EXTERNAL,DLL(dll_mode)
Relate:INVOICE       &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TRDBATCH      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TRDBATCH      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TRACHRGE      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TRACHRGE      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TRDMODEL      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TRDMODEL      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:STOMODEL      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:STOMODEL      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:TRADEACC      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:TRADEACC      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:SUBCHRGE      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:SUBCHRGE      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:DEFCHRGE      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:DEFCHRGE      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:SUBTRACC      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:SUBTRACC      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:CHARTYPE      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:CHARTYPE      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:MANUFACT      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:MANUFACT      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:USMASSIG      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:USMASSIG      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:USUASSIG      &FileManager,EXTERNAL,DLL(dll_mode)
Relate:USUASSIG      &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBPAYMT_ALIAS &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBPAYMT_ALIAS &RelationManager,EXTERNAL,DLL(dll_mode)
Access:JOBS_ALIAS    &FileManager,EXTERNAL,DLL(dll_mode)
Relate:JOBS_ALIAS    &RelationManager,EXTERNAL,DLL(dll_mode)
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE,EXTERNAL,DLL(dll_mode)
GlobalResponse       BYTE,EXTERNAL,DLL(dll_mode)
VCRRequest           LONG,EXTERNAL,DLL(dll_mode)
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('sbrairbr.INI')
  SSWizInit('sbrairbr.tps')
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  cellmain:Init(GlobalErrors, INIMgr)                 ! Initialise dll (ABC)
  Date_Range
  INIMgr.Update
  cellmain:Kill()
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  GlobalErrors.Kill


