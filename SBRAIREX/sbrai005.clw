

   MEMBER('sbrairex.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBRAI005.INC'),ONCE        !Local module procedure declarations
                     END


Process_Export PROCEDURE (start_date,end_date,trade_acc) !Generated from procedure template - Process

Progress:Thermometer BYTE
Bouncer              STRING(20)
SheetDesc            CSTRING(41)
tmp:StartDate        LONG
tmp:EndDate          LONG
tmp:Trade_Acc        STRING(15)
tmp:CustomerFault    STRING(255)
tmp:fault            STRING(255)
tmp:Engineers_Notes  STRING(255)
Accessories          STRING(30)
Accessories1         STRING(30)
Accessories2         STRING(30)
Accessories3         STRING(30)
Accessories4         STRING(30)
Accessories5         STRING(30)
Workshop_Third       STRING(30)
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Date_Completed)
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Process  WinType = Process
mo:SelectedField  Long
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,,CHARSET:ANSI),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
?F1SS                EQUATE(1000)
!# SW2 Extension            STRING(3)
f1Action             BYTE
f1FileName           CSTRING(256)
RowNumber            USHORT
StartRow             LONG
StartCol             LONG
Template             CSTRING(129)
f1Disabled           BYTE(0)
ssFieldQ              QUEUE(tqField).
CQ                    QUEUE(tqField).
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

!---------------------
ssInit         ROUTINE
!---------------------
  DO ssBuildQueue                                     ! Initialize f1 field queue
  IF NOT UsBrowse(SsFieldQ,'Process_Export',,,,CQ,SheetDesc,StartRow,StartCol,Template)
    ThisWindow.Kill
    RETURN
  END

!---------------------
ssBuildQueue   ROUTINE
!---------------------
    ssFieldQ.Name = 'job:Order_Number'
    ssFieldQ.Desc = 'Repair Docket No'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'FORMAT(job:date_booked,@d06)'
    ssFieldQ.Desc = 'Date In'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Title'
    ssFieldQ.Desc = 'Customer Type'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Surname'
    ssFieldQ.Desc = 'Surname'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Address_Line1'
    ssFieldQ.Desc = 'Address1'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Address_Line2'
    ssFieldQ.Desc = 'Address2'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Address_Line3'
    ssFieldQ.Desc = 'Address3'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = ''''''
    ssFieldQ.Desc = 'Address4'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = ''''''
    ssFieldQ.Desc = 'Town'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = ''''''
    ssFieldQ.Desc = 'County'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Postcode'
    ssFieldQ.Desc = 'Postcode'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'CHR(39)&job:Telephone_Number'
    ssFieldQ.Desc = 'Telephone No'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'sub:Company_Name'
    ssFieldQ.Desc = 'Account Name'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Account_Number'
    ssFieldQ.Desc = 'A/C Code'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Company_Name'
    ssFieldQ.Desc = 'Mobile User'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'CHR(39)&job:Mobile_Number'
    ssFieldQ.Desc = 'Mobile No'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Company_Name_Delivery'
    ssFieldQ.Desc = 'Delivery To'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Company_Name_Delivery'
    ssFieldQ.Desc = 'Delivery Name'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Address_Line1_Delivery'
    ssFieldQ.Desc = 'DeliveryA1'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Address_Line2_Delivery'
    ssFieldQ.Desc = 'DeliveryA2'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Address_Line3_Delivery'
    ssFieldQ.Desc = 'DeliveryA3'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = ''''''
    ssFieldQ.Desc = 'DeliveryA4'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = ''''''
    ssFieldQ.Desc = 'Delivery County'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Ref_Number'
    ssFieldQ.Desc = 'AirCall Job No'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Address_Line1_Delivery'
    ssFieldQ.Desc = ' Branch Name'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Postcode_Delivery'
    ssFieldQ.Desc = 'Branch Contact'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Unit_Type'
    ssFieldQ.Desc = 'Equipment Type'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Manufacturer'
    ssFieldQ.Desc = 'Manufacturer'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Model_Number'
    ssFieldQ.Desc = 'Model'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'CHR(39)&Job:ESN'
    ssFieldQ.Desc = 'IMEI Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Authority_Number'
    ssFieldQ.Desc = 'Purchased From CW'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Warranty_Job'
    ssFieldQ.Desc = 'Warranty'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'FORMAT(job:DOP,@d06)'
    ssFieldQ.Desc = 'Date Of Purchase'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Bouncer'
    ssFieldQ.Desc = 'Previous Repair No'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Ref_Number'
    ssFieldQ.Desc = 'Second Repair Docket'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:CustomerFault'
    ssFieldQ.Desc = 'Fault Description'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Estimate'
    ssFieldQ.Desc = 'Estimate Required'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Accessories'
    ssFieldQ.Desc = 'Accessories'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Accessories1'
    ssFieldQ.Desc = 'Accessories1'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Accessories2'
    ssFieldQ.Desc = 'Accessories2'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Accessories3'
    ssFieldQ.Desc = 'Accessories3'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Accessories4'
    ssFieldQ.Desc = 'Accessories4'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Accessories5'
    ssFieldQ.Desc = 'Accessories5'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = ''''''
    ssFieldQ.Desc = 'Loan Unit IMEI'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Workshop_Third'
    ssFieldQ.Desc = 'Workshop Destination'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'FORMAT(job:date_booked,@d06)'
    ssFieldQ.Desc = 'Date Sent To Workshop'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = ''''''
    ssFieldQ.Desc = 'Date Branch Received'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
  SORT(ssFieldQ,+ssFieldQ.Desc)


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Process_Export')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
    BIND('Accessories',Accessories)
    BIND('Accessories1',Accessories1)
    BIND('Accessories2',Accessories2)
    BIND('Accessories3',Accessories3)
    BIND('Accessories4',Accessories4)
    BIND('Accessories5',Accessories5)
    BIND('Bouncer',Bouncer)
    BIND('Workshop_Third',Workshop_Third)
    BIND('tmp:CustomerFault',tmp:CustomerFault)
    BIND('tmp:Engineers_Notes',tmp:Engineers_Notes)
    BIND('tmp:StartDate',tmp:StartDate)
    BIND('tmp:Trade_Acc',tmp:Trade_Acc)
    BIND('tmp:fault',tmp:fault)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:StartDate = Start_Date
  tmp:EndDate   = End_Date
  tmp:Trade_Acc = Trade_Acc
  
  DO ssInit
  Relate:ACCESSOR.Open
  Access:JOBNOTES.UseFile
  Access:SUBTRACC.UseFile
  Access:BOUNCER.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  IF ThisWindow.Response <> RequestCancelled
    UsInitDoc(CQ,?F1ss,StartRow,StartCol,Template)
    ?F1SS{'Sheets("Sheet1").Name'} = SheetDesc
    IF Template = ''
      RowNumber = StartRow + 1
    ELSE
      RowNumber = StartRow
    END
  END
  ThisMakeover.SetWindow(Win:Process)
  ProgressWindow{prop:buffer} = 1
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:date_booked)
  ThisProcess.AddSortOrder(job:Date_Booked_Key)
  ThisProcess.AddRange(job:date_booked,tmp:StartDate,tmp:EndDate)
  ThisProcess.SetFilter('job:date_completed = ''''')
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(JOBS,'QUICKSCAN=on')
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCESSOR.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Progress:Cancel
                                                      ! SW2 This section changed
      IF Message('Would you like to view the partially created spreadsheet?','Process Cancelled',ICON:Question,Button:Yes+Button:No,Button:No)|
      = Button:Yes
        UsDeInit(f1FileName, ?F1SS, 1)
      END
      ReturnValue = Level:Fatal
      RETURN ReturnValue
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ThisWindow.Response = RequestCompleted
    f1FileName = 'airbook'
    f1Action = 4
  
    IF UsGetFileName( f1FileName, 1, f1Action )
      UsDeInit( f1FileName, ?F1SS, f1Action )
    END
    ?F1SS{'quit()'}
  ELSE
    IF RowNumber THEN ?F1SS{'quit()'}.
  END
  ReturnValue = PARENT.TakeCloseEvent()
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisMakeover.TakeEvent(Win:Process,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeRecord()
  Access:SubTracc.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = job:Account_Number
  IF Access:SubTracc.Fetch(sub:Account_Number_Key)
    Return Level:User
  ELSE
    IF CLIP(UPPER(sub:Main_Account_Number)) <> Tmp:Trade_Acc
      Return Level:User
    END
  END
  
  !!Get Notes
  !Access:Bouncer.ClearKey(bou:Bouncer_Job_Only_Key)
  !bou:Bouncer_Job_Number = job:ref_number
  !IF Access:Bouncer.Fetch(bou:Bouncer_Job_Only_Key)
  !  bou:Original_Ref_Number = ''
  !  !error!
  !END
  
  !Loop through Jobs Alias Key
  bouncer = ''
  Access:Jobs_Alias.ClearKey(job_ali:ESN_Key)
  job_ali:ESN = job:ESN
  SET(job_ali:ESN_Key,job_ali:ESN_Key)
  LOOP
    IF Access:Jobs_Alias.Next()
      BREAK
    END
    IF job_ali:ESN <> job:ESN
      BREAK
    END
    IF job_ali:ref_number = job:ref_number
      CYCLE
    END
    bouncer = job_ali:Order_Number
  END
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  Access:JOBNOTES.TryFetch(jbn:RefNumberKey)
  
  tmp:CustomerFault = jbn:Fault_Description
  tmp:Fault = jbn:Invoice_Text
  tmp:Engineers_Notes = jbn:Engineers_Notes
  
  counttemp# = 0
  Access:Jobacc.ClearKey(jac:Ref_Number_Key)
  jac:Ref_Number = job:Ref_Number
  SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
  LOOP
    IF Access:JobAcc.Next()
      BREAK
    END
    IF jac:Ref_Number <> job:Ref_Number
      BREAK
    END
    counttemp#+=1
    CASE counttemp#
      OF 1
        Accessories = jac:Accessory
      OF 2
        Accessories1 = jac:Accessory
      OF 3
        Accessories2 = jac:Accessory
      OF 4
        Accessories3 = jac:Accessory
      OF 5
        Accessories4 = jac:Accessory
      OF 6
        Accessories5 = jac:Accessory
    END
  END
  
  IF job:Workshop = 'YES'
    Workshop_Third = 'N/A'
  ELSE
    Workshop_Third = job:Third_Party_Site
  END
  UsFillRow(CQ,?F1SS,RowNumber,StartCol)
  RETURN ReturnValue

