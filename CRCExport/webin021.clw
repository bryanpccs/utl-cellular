

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN021.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBIN012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBIN013.INC'),ONCE        !Req'd for module callout resolution
                     END


FindFreeDateAtSC     PROCEDURE  (strDirection, scAccountID, strDate, strPath) ! Declare Procedure
result      long
strIniFile  like(reg:BankHolidaysFileName)
availableDate date
    map
FreeDate        procedure(date sourceDate), date
    end
  CODE
    !
    ! Description : Find the next free date (check weekends / bank holidays)
    !

    ! Make sure that the strDate is not empty (otherwise can cause a GPF)
    if strDate <> '' and exists(clip(strPath) & '\SIDSRVCN.DAT')
        ! Change the paths this way
        SIDSRVCN{Prop:Name} = clip(strPath) & '\SIDSRVCN.DAT'

        Access:SIDSRVCN.Open()
        Access:SIDSRVCN.UseFile()

        availableDate = deformat(strDate, @d06b)

        Access:SIDSRVCN.ClearKey(srv:SCAccountIDKey)
        srv:SCAccountID = scAccountID
        if Access:SIDSRVCN.Fetch(acg:AccountNoKey) = Level:Benign
            strIniFile = srv:BankHolidaysFileName
        end

        if strIniFile = ''
            strIniFile = 'BHENGWAL.DAT' ! Default
        end

        availableDate = FreeDate(availableDate)

        strDate = format(availableDate, @d06b)

        Access:SIDSRVCN.Close()

        result =  true
    end

    return result

FreeDate procedure(date sourceDate)

x   long(1)
bh  date, dim(15)

    code

    LoadBankHolidays(bh, strIniFile, strPath)

    loop x = 1 to 15
        if (GetDayOfWeek(sourceDate) = 'SAT')
            if strDirection = '-'
                sourceDate = sourceDate - 1
            else
                sourceDate = sourceDate + 2
            end
            sourceDate = FreeDate(sourceDate)
        end
        if (GetDayOfWeek(sourceDate) = 'SUN')
            if strDirection = '-'
                sourceDate = sourceDate - 2
            else
                sourceDate = sourceDate + 1
            end
            sourceDate = FreeDate(sourceDate)
        end
        if (bh[x] <> '')
            if (sourceDate = bh[x])
                if strDirection = '-'
                    sourceDate = sourceDate - 1
                else
                    sourceDate = sourceDate + 1
                end
                sourceDate = FreeDate(sourceDate)
            end
        end
    end ! loop

    return sourceDate
