

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN008.INC'),ONCE        !Local module procedure declarations
                     END


LoadBankHolidays     PROCEDURE  (bh, strFileName, strPath) ! Declare Procedure
fileName    string(260)
counter     long, auto
! Bank holidays data file

BANKHOL              file, driver('Btrieve'), name(dataFileName), pre(bkh), thread
BankHolIDKey             key(bkh:BankHolID), nocase, primary
Record                   record, pre()
BankHolID                   long
Description                 string(30)
HolidayDate                 date
                         end
                     end
  CODE
    !
    ! Description : Load the bank holiday dates for the specified file name
    !

    fileName = clip(strPath) & '\' & clip(strFileName)

    if (maximum(bh, 1) = 15)    ! Callee array safety check
        if instring('.INI', upper(fileName), 1, 1) = 0
            ! Data file
            dataFileName = clip(fileName)

            if exists(dataFileName)

                counter = 1

                share(BANKHOL)

                set(BANKHOL, 0)
                loop
                    next(BANKHOL)
                    if error() then break.

                    bh[counter] = bkh:HolidayDate
                    counter += 1
                    if counter > 15 then break.
                end ! loop

                close(BANKHOL)
            end
        else
            ! INI file
            bh[1] = deformat(getini('BankHols', 'BH1', '', clip(fileName)), @d05b)
            bh[2] = deformat(getini('BankHols', 'BH2', '', clip(fileName)), @d05b)
            bh[3] = deformat(getini('BankHols', 'BH3', '', clip(fileName)), @d05b)
            bh[4] = deformat(getini('BankHols', 'BH4', '', clip(fileName)), @d05b)
            bh[5] = deformat(getini('BankHols', 'BH5', '', clip(fileName)), @d05b)
            bh[6] = deformat(getini('BankHols', 'BH6', '', clip(fileName)), @d05b)
            bh[7] = deformat(getini('BankHols', 'BH7', '', clip(fileName)), @d05b)
            bh[8] = deformat(getini('BankHols', 'BH8', '', clip(fileName)), @d05b)
            bh[9] = deformat(getini('BankHols', 'BH9', '', clip(fileName)), @d05b)
            bh[10] = deformat(getini('BankHols', 'BH10', '', clip(fileName)), @d05b)
            bh[11] = deformat(getini('BankHols', 'BH11', '', clip(fileName)), @d05b)
            bh[12] = deformat(getini('BankHols', 'BH12', '', clip(fileName)), @d05b)
            bh[13] = deformat(getini('BankHols', 'BH13', '', clip(fileName)), @d05b)
            bh[14] = deformat(getini('BankHols', 'BH14', '', clip(fileName)), @d05b)
            bh[15] = deformat(getini('BankHols', 'BH15', '', clip(fileName)), @d05b)
        end
    end
