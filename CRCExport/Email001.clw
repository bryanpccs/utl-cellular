

   MEMBER('emailnotify.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('EMAIL001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('EMAIL002.INC'),ONCE        !Req'd for module callout resolution
                     END


Main                 PROCEDURE                        ! Declare Procedure
connectionStr   string(255), static
iniFilePath     string(255)

addressLines    string(255)
subject         string(255)
emailText       string(10000)
ToteRequest file, driver('Scalable'), oem, owner(ConnectionStr), pre(tote), name('ToteRequest'), bindable, thread
Record          record
ToteRequestID       long, name('ToteRequestID')
AccountNo           string(15), name('AccountNo')
                end
            end
  CODE
    ! Send notification emails

    Relate:SIDDEF.Open()
    Relate:SUBTRACC.Open()

    set(SIDDEF, 0)
    Access:SIDDEF.Next()

    iniFilePath     = clip(path()) & '\Webimp.ini'
    connectionStr   = getini('Defaults', 'Connection', '', clip(iniFilepath))

    open(ToteRequest)

    ToteRequest{Prop:SQL} = 'SELECT ToteRequestID, AccountNo FROM ToteRequest WHERE EmailSent = 0 AND EntryDate = CURDATE()'

    loop
        next(ToteRequest)
        if error() then break.

        Access:SUBTRACC.ClearKey(sub:account_number_key)
        sub:account_number = clip(tote:AccountNo)
        if Access:SUBTRACC.Fetch(sub:account_number_key) <> Level:Benign then cycle.

        addressLines = clip(SUB:Address_Line1)

        if clip(SUB:Address_Line2) <> ''
            if clip(addressLines) = ''
                addressLines = clip(SUB:Address_Line2)
            else
                addressLines = clip(addressLines) & '<13,10>' & clip(SUB:Address_Line2)
            end
        end

        if clip(SUB:Address_Line3) <> ''
            if clip(addressLines) = ''
                addressLines = clip(SUB:Address_Line3)
            else
                addressLines = clip(addressLines) & '<13,10>' & clip(SUB:Address_Line3)
            end
        end

        if clip(SUB:Postcode) <> ''
            if clip(addressLines) = ''
                addressLines = clip(SUB:Postcode)
            else
                addressLines = clip(addressLines) & '<13,10>' & clip(SUB:Postcode)
            end
        end

        subject = clip(tote:AccountNo) & ' - Vodafone Tote Request'
        emailText = 'The Vodafone retail store detailed below has requested that a new Tote is despatched to them' & '<13,10,13,10>' & |
                    clip(SUB:account_number) & '<13,10>' & |
                    clip(SUB:Company_Name) & '<13,10>' & |
                    clip(addressLines)

        if SendEmail(SID:ToteAlertEmail, subject, emailText) = true
            ! Update record as sent
            ToteRequest{Prop:SQL} = 'UPDATE ToteRequest SET EmailSent = 1 WHERE ToteRequestID = ' & tote:ToteRequestID
        end
    end

    close(ToteRequest)

    Relate:SUBTRACC.Close()
    Relate:SIDDEF.Close()
