   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('OLDIMBC.CLW')
DctInit     PROCEDURE
DctKill     PROCEDURE
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('OLDIM001.CLW')
Main                   PROCEDURE   !
     END
   END

SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

IMEISHP2             FILE,DRIVER('Btrieve'),OEM,NAME('IMEISHP2.DAT'),PRE(imei2),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(imei2:RecordNumber),NOCASE,PRIMARY
IMEIRecordNumberKey      KEY(imei2:IMEIRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEIRecordNumber            LONG
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
Colour                      STRING(30)
                         END
                     END                       

IMEISHIP             FILE,DRIVER('Btrieve'),OEM,NAME('IMEISHIP.DAT'),PRE(IMEI),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(IMEI:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEI:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEI:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
BER                         BYTE
                         END
                     END                       



Access:IMEISHP2      &FileManager
Relate:IMEISHP2      &RelationManager
Access:IMEISHIP      &FileManager
Relate:IMEISHIP      &RelationManager
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE(0),THREAD
GlobalResponse       BYTE(0),THREAD
VCRRequest           LONG(0),THREAD
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  DctInit
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('OldIMEIImport.INI')
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  DctKill
  GlobalErrors.Kill


