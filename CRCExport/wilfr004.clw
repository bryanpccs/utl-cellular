

   MEMBER('wilfredtosb.clw')                          ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('WILFR004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WILFR001.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WILFR007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WILFR009.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:ImportFile       CSTRING(255),STATIC
ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
tmp:BankHolidayFile  CSTRING(255),STATIC
BankHolidayQueue     QUEUE,PRE(bkhque)
HolidayDate          DATE
                     END
tmp:ExchangeImportType BYTE
window               WINDOW('Wilfred To SB'),AT(,,69,15),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       PROMPT('FTP In Progress...'),AT(4,0),USE(?Prompt1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ImportFile      File,Driver('BASIC', '/COMMA = 9' ),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record                  Record
RefNumber               Long()
ConsignmentNumber       String(60)
Manufacturer            String(30)
ModelNumber             String(30)
ExchangeIMEINumber      String(30) ! Customer IMEI Number
StatusCode              String(30)
ResolutionCode          String(30)
StatusDate              String(10)
StatusTime              String(10)
! Inserting (DBH 06/12/2005) #6436 - Exchange IMEI will now be passed seperately
! Repair IMEI is now passed at the end of the file - TrkBs: 6915 (DBH: 09-03-2006)
IMEINumber              String(30)
! End (DBH 06/12/2005) #6436
! Inserting (DBH 24/03/2006) #7413 - New field
IncomingConsignmentNumber   String(60)
! End (DBH 24/03/2006) #7413
                        End
                    End
! Dummy SQL table
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD
SQLRefNumber                Long, Name('Ref_Number')
! Inserting (DBH 27/02/2006) #6915 - Need to lookup extra fields
SQLSBJobNo                  Long, Name('SBJobNo')
SQLBookingType              String(1), Name('BookingType')
SQLSMSReq                   Byte(), Name('SMSReq')
SQLEmailReq                 Byte(), Name('EmailReq')
SQLCountryDelivery          String(20), Name('CountryDelivery') !ENGLAND, NORTHERN IRELAND, SCOTLAND, WALES
SQLValueSegment             String(20), Name('ValueSegment')
SQLSMSType                  String(1), Name('SMSType')
SQLSMSSent                  Long, Name('SMSSent')
SQLRetStoreCode             String(15), Name('RetStoreCode')
! End (DBH 27/02/2006) #6915
                        END
                    END

WebJobEx        file, driver('Scalable'), oem, owner(ConnectionStr), name('WEBJOBEX'), pre(webex), bindable, thread
Record              record
SCAccountID             long, name('SCAccountID')
                    end
                end

CCExchange      file, driver('Scalable'), oem, owner(ConnectionStr), name('CCExchange'), pre(ccex), bindable, thread
Record              record
TransitDays             long, name('TransitDays')
                    end
                end

SQLDefaults     file, driver('Scalable'), oem, owner(ConnectionStr), name('Defaults'), pre(sqldef), bindable, thread
Record              record
RepairAdminCharge       PDecimal(8,2),Name('RepairAdminCharge')
                    end
                end

!ValSegBkType    file, driver('Scalable'), oem, owner(ConnectionStr), name('ValSegBkType'), pre(vsbt), bindable, thread
!Record              record
!IsCharge                byte, name('IsCharge')
!ChargePercent           long, name('ChargePercent')
!                    end
!                end

!Bankholiday File
BANKHOL     File,Driver('Btrieve'),Name(tmp:BankHolidayFile), Pre(bkh), Thread
BankHolIDKey    Key(bkh:BankHolID), NoCase, Primary
Record          Record, Pre()
BankHolID           Long
Description         String(30)
HolidayDate         Date
                End ! Record          Record, Pre()
            End ! BANKHOL     File,Driver('Btrieve',Name(tmp:BankHolidayFile), Pre(bkh), Thread
TempFiles   Queue(file:Queue),pre(fil)
            End

i           long, auto

scQueue     queue, pre(scq)
scAccountID     like(srv:SCAccountID)
            end
local       Class
ImportJobs              Procedure(String func:FileName, String func:ShortName)
SetDateTime             Procedure(*Date func:Date,*Time func:Time)
UpdatePENDMAILRecord    Procedure(String func:SMSEmail),Byte
SetRepairAdminCharge    Procedure()
GetWorkDate             procedure(long scAccountID, string country, date startDate, long workingDays), date
FindNextFreeDate        procedure(string strAccount, date currentDate), date
            End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

Process:Directory routine
    !
    !
    !

    !Directory(TempFiles,Clip(Path()) & '\WilfredToSend\desp_?????????????.tab',ff_:DIRECTORY)
    Directory(TempFiles,Clip(Path()) & '\WilfredToSend\desp*.tab',ff_:DIRECTORY)
    Loop x# = 1 To Records(TempFiles)
        Get(TempFiles,x#)
        If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
            Cycle
        Else !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
!            ErrorLog('Importing: ' & Clip(fil:Name) & ' - ' & Round(fil:Size/1024,.01) & 'kb')
            Local.ImportJobs(Clip(Path()) & '\WilfredToSend\' & CLip(fil:name),Clip(fil:Name))
        End !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
    End !x# = 1 To Records(ScanFiles)
BuildBankHolidayQueue       Routine
    Case sql:SQLCountryDelivery
    Of 'SCOTLAND'
        tmp:BankHolidayFile = 'BHSCOT.DAT'
    Of 'NORTHERN IRELAND'
        tmp:BankHolidayFile = 'BHNIRE.DAT'
    Of 'ENGLAND' Orof 'WALES'
        tmp:BankHolidayFile = 'BHENGWAL.DAT'
    Else
        tmp:BankHolidayFile = 'BHSC.DAT'
    End ! Case sql:SQLCountryDelivery

    Free(BankHolidayQueue)
    Share(BANKHOL,0)
    Loop
        Next(BANKHOL)
        If Error()
            Break
        End ! If Error()
        If bkh:HolidayDate <> ''
            bkhque:HolidayDate = bkh:HolidayDate
            Add(BankHolidayQueue)
        End ! If bkh:HolidayDate <> ''
    End ! Loop
    Close(BANKHOL)
    Sort(BankHolidayQueue,bkhque:HolidayDate)

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  OPEN(window)
  SELF.Opened=True
  Free(FileQueue)
  Clear(FileQueue)
  
!  ErrorLog('')
!  ErrorLog('WILFRED2SB Started===========================')

  ! ------------------------------------------------------------------------------
  ! Import files from default FTP location
  Loop Until FTP(0) = 1
  End ! Until FTP() = 1
  
!  ErrorLog('FTP process (Default) : Completed')
  
  ! Import files
  Do Process:Directory

  ! ------------------------------------------------------------------------------
  ! Import files from Service Centre specific FTP location
  Relate:SIDSRVCN.Open()

  set(srv:SCAccountIDKey)
  loop
      if Access:SIDSRVCN.Next() <> Level:Benign then break.
      if srv:AccountActive = false then cycle.
      if srv:UseServiceCentreFTP = true
          scQueue.scAccountID = srv:SCAccountID
          add(scQueue)
!          ErrorLog('FTP process (' & clip(srv:ServiceCentreName) & ') : Completed')
      end
  end

  Relate:SIDSRVCN.Close()
  ! ------------------------------------------------------------------------------

  ! Changed to process queue to prevent 'record changed by another workstation' errors
  ! FTP() procedure updates the SIDSRVCN table
  loop i = 1 to records(scQueue)
      get(scQueue, i)
      if error() then break.
      Loop Until FTP(scQueue.scAccountID) = 1
      End ! Until FTP() = 1

      ! Import files
      Do Process:Directory
  end
!  ErrorLog('WILFRED2SB Finished==========================')
  
  Post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

local.UpdatePENDMAILRecord      Procedure(String func:SMSEmail)

withBattery byte

Code
    ! Inserting (DBH 26/04/2006) #7507 - Only send messages for B2B and Exchange
    If sql:SQLBookingType <> 'E' And sql:SQLBookingType <> 'B' And sql:SQLBookingType <> 'F' And sql:SQLBookingType <> 'G' And sql:SQLBookingType <> 'U'
        Return False
    End ! If sql:SQLBookingType <> 'E' And sql:SQLBookingType <> 'B'
    ! End (DBH 26/04/2006) #7507

    ! TrackerBase log 7939 (GK 31/07/2006) - E2A/E2B / E3A/E3B, messages split to contain different text if a battery is attached to the job
    withBattery = false

    Access:JOBACC.ClearKey(jac:Ref_Number_Key)
    jac:ref_number = job:Ref_Number
    jac:accessory = 'BATTERY'
    if Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
        withBattery = true
    end

    pem:SMSEmail = func:SMSEmail
    pem:RefNumber = job:Ref_Number
    If pem:SMSEmail = 'SMS'
        pem:MobileNumber = jobe:TraFaultCode5
    End ! If pem:SMSEmail = 'SMS'
    If pem:SMSEmail = 'EMAIL'
        pem:EmailAddress = jobe:EndUserEmailAddress
    End ! If pem:SMSEmail = 'EMAIL'
    
    Case Today() % 7
    Of 0 !Sunday
        Case Sub(job:Current_Status,1,3)
        Of '901'
            !Should sent message on Monday morning, unless bank holiday
            pem:DateToSend = Today() + 1
            local.SetDateTime(pem:DateToSend,pem:TimeToSend)
            Case sql:SQLBookingType
            Of 'E'
            OrOf 'F'
            OrOf 'U'
                if withBattery = false
                    If (func:SMSEmail = 'EMAIL' AND ~dem2:E2Active) OR (func:SMSEmail = 'SMS' AND ~dem2:E2SMSActive)
                        Return False
                    End ! If ~dem2:E2Active
                    pem:MessageType = 'E2'
                else
                    If (func:SMSEmail = 'EMAIL' AND ~dem2:E2BActive) OR (func:SMSEmail = 'SMS' AND ~dem2:E2BSMSActive)
                        Return False
                    End ! If ~dem2:E2BActive
                    pem:MessageType = 'E2B'
                end
            Of 'B'
            OrOf 'G'
                If (func:SMSEmail = 'EMAIL' AND  ~dem2:B4Active) OR (func:SMSEmail = 'SMS' AND  ~dem2:B4SMSActive)
                    Return False
                End ! If ~dem2:E2Active
                pem:MessageType = 'B4'
            End ! Case sql:SQLBookingType
        Of '315'
            Return False
        Of '140'
            Return False
        End ! Case Sub(job:Current_Status,1,3)

    Of 6 !Saturday
        Case Sub(job:Current_Status,1,3)
        Of '901'
            !Should send message on Monday norming, unless bank holiday
            pem:DateToSend = Today() + 2
            local.SetDateTime(pem:DateToSend,pem:TimeToSend)
            Case sql:SQLBookingType
            Of 'E'
            OrOf 'F'
            OrOf 'U'
                if withBattery = false
                    If (func:SMSEmail = 'EMAIL' AND ~dem2:E2Active) OR (func:SMSEmail = 'SMS' AND ~dem2:E2SMSActive)
                        Return False
                    End ! If ~dem2:E2Active
                    pem:MessageType = 'E2'
                else
                    If (func:SMSEmail = 'EMAIL' AND ~dem2:E2BActive) OR (func:SMSEmail = 'SMS' AND ~dem2:E2BSMSActive)
                        Return False
                    End ! If ~dem2:E2BActive
                    pem:MessageType = 'E2B'
                end
            Of 'B'
            OrOf 'G'
                If (func:SMSEmail = 'EMAIL' AND  ~dem2:B4Active) OR (func:SMSEmail = 'SMS' AND  ~dem2:B4SMSActive)
                    Return False
                End ! If ~dem2:E2Active
                pem:MessageType = 'B4'
            End ! Case sql:SQLBookingType
        Of '315'
            Return False
        Of '140'
            Return False
        End ! Case Sub(job:Current_Status,1,3)

    Of 5 !Friday
        Case Sub(job:Current_Status,1,3)
        Of '901'
            If Clock() < Deformat('08:00',@t4)
                Return False
            Elsif Clock() > Deformat('22:45',@t4)
                !SMS should be sent on Monday, unless bank holiday
                pem:DateToSend = Today() +  3
                local.SetDateTime(pem:DateToSend,pem:TimeToSend)
                Case sql:SQLBookingType
                Of 'E'
                OrOf 'F'
                OrOf 'U'
                    if withBattery = false
                        If (func:SMSEmail = 'EMAIL' AND ~dem2:E2Active) OR (func:SMSEmail = 'SMS' AND ~dem2:E2SMSActive)
                            Return False
                        End ! If ~dem2:E2Active
                        pem:MessageType = 'E2'
                    else
                        If (func:SMSEmail = 'EMAIL' AND ~dem2:E2BActive) OR (func:SMSEmail = 'SMS' AND ~dem2:E2BSMSActive)
                            Return False
                        End ! If ~dem2:E2BActive
                        pem:MessageType = 'E2B'
                    end
                Of 'B'
                OrOf 'G'
                    If (func:SMSEmail = 'EMAIL' AND  ~dem2:B4Active) OR (func:SMSEmail = 'SMS' AND  ~dem2:B4SMSActive)
                        Return False
                    End ! If ~dem2:E2Active
                    pem:MessageType = 'B4'
                End ! Case sql:SQLBookingType
            Else
                !Should be sent on Sunday
                pem:DateToSend = Today() + 2
                local.SetDateTime(pem:DateToSend,pem:TimeToSend)
                Case sql:SQLBookingType
                Of 'E'
                OrOf 'F'
                OrOf 'U'
                    if withBattery = false
                        If (func:SMSEmail = 'EMAIL' AND ~dem2:E3Active) OR (func:SMSEmail = 'SMS' AND ~dem2:E3SMSActive)
                            Return False
                        End ! If ~dem2:E2Active
                        pem:MessageType = 'E3'
                    else
                        If (func:SMSEmail = 'EMAIL' AND ~dem2:E3BActive) OR (func:SMSEmail = 'SMS' AND ~dem2:E3BSMSActive)
                            Return False
                        End ! If ~dem2:E3BActive
                        pem:MessageType = 'E3B'
                    end
                Of 'B'
                OrOf 'G'
                    If (func:SMSEmail = 'EMAIL' AND ~dem2:B5Active) OR (func:SMSEmail = 'SMS' AND ~dem2:B5SMSActive)
                        Return False
                    End ! If ~dem2:E2Active
                    pem:MessageType = 'B5'
                End ! Case sql:SQLBookingType
            End ! If ~Error()
        Of '315'
            If Clock() < Deformat('08:00',@t4)
                Return False
            Elsif Clock() > Deformat('20:00',@t4)
                Return False
            Else
                Case sql:SQLBookingType
                Of 'E'
                OrOf 'F'
                OrOf 'U'
                    Return False
                Of 'B'
                OrOf 'G'
                    If (func:SMSEMail = 'EMAIL' AND ~dem2:B3Active) OR (func:SMSEMail = 'SMS' AND ~dem2:B3SMSActive)
                        Return False
                    End ! If ~dem2:E2Active
                    pem:MessageType = 'B3'
                    pem:DateToSend = Today()
                    pem:TimeToSend = Clock()
                End ! Case sql:SQLBookingType
            End ! If ~Error()
        Of '140'
            If Clock() < Deformat('08:00',@t4)
                Return False
            ElsIf Clock() > Deformat('20:00',@t4)
                Return False
            Else ! If Clock() < Deformat('08:00',@t4)
                Case sql:SQLBookingType
                Of 'E'
                OrOf 'F'
                OrOf 'U'
                    Return False
                Of 'B'
                OrOf 'G'
                    If (func:SMSEmail = 'EMAIL' AND ~dem2:B2Active) OR (func:SMSEmail = 'SMS' AND ~dem2:B2SMSActive)
                        Return False
                    End ! If ~dem2:E2Active
                    pem:MessageType = 'B2'
                    pem:DateToSend = Today()
                    pem:TimeToSend = Clock()
                End ! Case sql:SQLBookingType
            End ! If Clock() < Deformat('08:00',@t4)
        End ! Case Sub(job:Current_Status,1,3)
    Else !Monday To Thursday
        Case Sub(job:Current_Status,1,3)
        Of '901'
            If Clock() < Deformat('08:00',@t4)
                Return False
            Elsif Clock() > Deformat('20:00',@t4)
                !Send message tomorrow
                Return False
            Else
                Case sql:SQLBookingType
                Of 'E'
                OrOf 'F'
                OrOf 'U'
                    if withBattery = false
                        If (func:SMSEmail = 'EMAIL' AND ~dem2:E2Active) OR (func:SMSEmail = 'SMS' AND ~dem2:E2SMSActive)
                            Return False
                        End ! If ~dem2:E2Active
                        pem:DateToSend = Today()
                        pem:TimeToSend = Clock()
                        pem:MessageType = 'E2'
                    else
                        If (func:SMSEmail = 'EMAIL' AND ~dem2:E2BActive) OR (func:SMSEmail = 'SMS' AND ~dem2:E2BSMSActive)
                            Return False
                        End ! If ~dem2:E2BActive
                        pem:DateToSend = Today()
                        pem:TimeToSend = Clock()
                        pem:MessageType = 'E2B'
                    end
                Of 'B'
                OrOf 'G'
                    If (func:SMSEmail = 'EMAIL' AND ~dem2:B4Active) OR (func:SMSEmail = 'SMS' AND ~dem2:B4SMSActive)
                        Return False
                    End ! If ~dem2:E2Active
                    pem:DateToSend = Today()
                    pem:TimeToSend = Clock()
                    pem:MessageType = 'B4'
                End ! Case sql:SQLBookingType
            End ! If Clock() < Deformat('08:00',@t4)
        Of '315'
            If Clock() < Deformat('08:00',@t4)
                Return False
            Elsif Clock() > Deformat('20:00',@t4)
                Return False
            Else
                Case sql:SQLBookingType
                Of 'E'
                OrOf 'F'
                OrOf 'U'
                    Return False
                Of 'B'
                OrOf 'G'
                    If (func:SMSEmail = 'EMAIL' AND ~dem2:B3Active) OR (func:SMSEmail = 'SMS' AND ~dem2:B3SMSActive)
                        Return False
                    End ! If ~dem2:E2Active
                    pem:MessageType = 'B3'
                    pem:DateToSend = Today()
                    pem:TimeToSend = Clock()
                End ! Case sql:SQLBookingType
            End ! If ~Error()
        Of '140'
            If Clock() < Deformat('08:00',@t4)
                Return False
            ElsIf Clock() > Deformat('20:00',@t4)
                Return False
            Else ! If Clock() < Deformat('08:00',@t4)
                Case sql:SQLBookingType
                Of 'E'
                OrOf 'F'
                OrOf 'U'
                    Return False
                Of 'B'
                OrOf 'G'
                    If (func:SMSEmail = 'EMAIL' AND ~dem2:B2Active) OR (func:SMSEmail = 'SMS' AND ~dem2:B2SMSActive)
                        Return False
                    End ! If ~dem2:E2Active
                    pem:MessageType = 'B2'
                    pem:DateToSend = Today()
                    pem:TimeToSend = Clock()
                End ! Case sql:SQLBookingType
            End ! If Clock() < Deformat('08:00',@t4)

        End ! Case Sub(job:Current_Status,1,3)
    End ! Case Today() % 7
    Return True
local.SetDateTime       Procedure(*Date func:Date,*Time func:Time)
Code
    !Check the next seven days for the first available NON bank holiday
    func:Date += 1
    Loop days# = 1 to 7
        If func:Date % 7 = 6
            !Skip Saturdays
            func:Date += 1
            Cycle
        End ! If func:Date % 7 = 6
        !Compare date with bankholiday list
        bkhque:HolidayDate = func:Date
        Get(BankHolidayQueue,bkhque:HolidayDate)
        If ~Error()
            func:Date += 1
            Cycle
        Else ! If ~Error()
            Break
        End ! If ~Error()
    End ! Loop days# = 1 to 7
    func:Date -= 1
    If InList(func:Date % 7,0,6)
        func:Time = Deformat('10:00',@t4)
    Else ! If InList(pem:DateToSend % 7,0,6)
        func:Time = Deformat('08:00',@t4)
    End ! If InList(pem:DateToSend % 7,0,6)
Local.ImportJobs        Procedure(String func:FileName, String func:ShortName)

local:UpdateStatus       Byte(0)
local:JobUpdated         Byte(0)
! Inserting (DBH 06/12/2005) #6436 - Record the specific changes in the Audit Trail
local:AuditText          String(255)
! End (DBH 06/12/2005) #6436
! Is the status in the file different? (DBH: 11-05-2006)
local:StatusChanged      Byte(0)
local:LastStatusDate     Date
local:LastStatusTime     Time
local:despatchDate       Date
local:actualExchangeDate Date
local:transitDays        Long

Code

    ! Start - Is this file a duplicate? (DBH: 01-04-2005)
    If Exists(Clip(Path()) & '\WilfredSent\' & Clip(func:ShortName))
        ErrorLog('Duplicate File Error: ' & Clip(func:ShortName))
        Return
    End ! If Exists(Clip(Path()) & '\WilfredSent\' & Clip(func:ShortName))
    ! End   - Is this file a duplicate? (DBH: 01-04-2005)

    Relate:JOBS.Open()
    Relate:DEFAULTS.Open()
    Relate:USERS.Open()
    Relate:EXCHANGE.Open()
    Relate:DESBATCH.Open()
    Relate:STATUS.Open()
    Relate:IMEISHIP.Open()
    Relate:PENDMAIL.Open()
    Relate:DEFEMAI2.Open()
    Set(DEFEMAI2)
    Access:DEFEMAI2.Next()
    Relate:AUDSTAEX.Open()
    Relate:CARISMA.Open()
    Relate:SIDSRVCN.Open()
    Relate:AUDSTATS.Open()
    !Relate:SIDEXUPD.Open()
    Relate:MODELNUM.Open()
    Relate:SIDNWDAY.Open()

    IniFilepath =  Clip(Path()) & '\Webimp.ini'
    ConnectionStr = GetIni('Defaults', 'Connection', '', Clip(IniFilepath))

    Open(SQLFile)
    Open(WebJobEx)
    Open(SQLDefaults)
    !Open(ValSegBkType)
    Open(CCExchange)

    CountNormal# = 0
    CountStatus# = 0

    Access:EXCHANGE.UseFile()
    tmp:ImportFile = func:FileName
    Open(ImportFile)
    Set(ImportFile,0)
    Loop
        Next(ImportFile)
        If Error()
            Break
        End !If Error()

        local:AuditText = ''

        !Is there a job number here? - TrkBs:  (DBH: 02-02-2005)
        If impfil:RefNumber <> ''
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = impfil:RefNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
                ! Inserting (DBH 27/02/2006) #6915 - Need the jobse file to get the email address
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    ! Found

                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    ! Error
                End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                ! End (DBH 27/02/2006) #6915
                local:JobUpdated = False
                !Is there an exchange unit to add? - TrkBs:  (DBH: 02-02-2005)
                ! Changing (DBH 06/12/2005) #6436 - They are now sending the exchange imei in a seperate field
                                 !Make sure they aren't sending the job imei back too - TrkBs:  (DBH: 11-02-2005)
                                ! If impfil:IMEINumber <> '' And impfil:IMEINumber <> job:ESN
                ! to (DBH 06/12/2005) #6436
                !Make sure they aren't sending the job imei back too - TrkBs:  (DBH: 11-02-2005)
                If impfil:ExchangeIMEINumber <> '' And impfil:ExchangeIMEINumber <> job:ESN
                ! End (DBH 06/12/2005) #6436
                    !Only add exchange if not already attached - TrkBs:  (DBH: 02-02-2005)
                    If job:Exchange_Unit_Number = ''
                        If Access:exchange.PrimeRecord() = Level:Benign
                            tmp:ExchangeImportType = 0

                            ! Get Service Centre Default
                            SQLFile{Prop:SQL} = 'SELECT Ref_Number FROM WebJob WHERE SBJobNo = ' & job:Ref_Number
                            next(SQLFile)
                            if not error()
                                WebJobEx{Prop:SQL} = 'SELECT SCAccountID FROM WebJobEx WHERE WebJobNo = ' & SQL:SQLRefNumber
                                next(WebJobEx)
                                if not error()
                                    Access:SIDSRVCN.ClearKey(srv:SCAccountIDKey)
                                    srv:SCAccountID = webex:SCAccountID
                                    if Access:SIDSRVCN.Fetch(srv:SCAccountIDKey) = Level:Benign
                                        tmp:ExchangeImportType = srv:ExchangeImportType
                                    end
                                end
                            end

                            if tmp:ExchangeImportType = 0 or tmp:ExchangeImportType = 2
                                ! Attempt to fetch Oracle Code (Contract)
                                Access:CARISMA.ClearKey(cma:ContractCodeKey)
                                cma:ContractOracleCode = impfil:ModelNumber
                                if Access:CARISMA.Fetch(cma:ContractCodeKey) = Level:Benign
                                    xch:Model_Number = cma:ModelNo
                                    xch:Manufacturer = cma:Manufacturer
                                Else
                                    ! Attempt to fetch Oracle Code (PAYT)
                                    Access:CARISMA.ClearKey(cma:PAYTCodeKey)
                                    cma:PAYTOracleCode = impfil:ModelNumber
                                    if Access:CARISMA.Fetch(cma:PAYTCodeKey) = Level:Benign
                                        xch:Model_Number = cma:ModelNo
                                        xch:Manufacturer = cma:Manufacturer
                                    Else
                                        !if tmp:ExchangeImportType = 0  ! 28/05/2010 - Do this anyway if we haven't found the details
                                            xch:Model_Number   = impfil:ModelNumber
                                            xch:Manufacturer   = impfil:Manufacturer
                                        !end
                                    End
                                End ! If ~Error()
                            else ! Model
                                xch:Model_Number   = impfil:ModelNumber
                                xch:Manufacturer   = impfil:Manufacturer
                            end

                            xch:ESN            = impfil:ExchangeIMEINumber
                            !xch:ESN            = impfil:IMEINumber
                            xch:Location       = 'WILFRED EXCHANGE'
                            xch:Shelf_Location = 'WILFRED EXCHANGE'
                            xch:Date_Booked    = Today()
                            xch:Available      = 'EXC'
                            xch:Job_Number     = job:Ref_Number
                            xch:Stock_Type     = 'WILFRED EXCHANGE'
                            If Access:exchange.TryInsert() = Level:Benign
                                !Insert Successful
                                job:Exchange_Unit_Number = xch:Ref_Number
                                job:Exchange_User       = 'WEB'
                                job:Exchange_Despatched = Today()
                                job:Exchange_Despatched_User = 'WEB'
                                job:Exchange_Consignment_Number = impfil:ConsignmentNumber
                                ! (GK 28/11/2007) - The 125 status needs to be set one minute before
                                ! the actual status in the import file rather than the current date/time.
                                ! Otherwise the actual status is treated as historic.
                                ! GetStatusAlternative(125,1,'EXC',Today(),Clock())
                                If Deformat(impfil:StatusTime,@t1) > 6000 ! Check for roll-over
                                    GetStatusAlternative(125, 1, 'EXC', Deformat(impfil:StatusDate,@d6), Deformat(impfil:StatusTime,@t1) - 6000)
                                Else
                                    GetStatusAlternative(125, 1, 'EXC', Deformat(impfil:StatusDate,@d6) - 1, Deformat('23:59',@t1))
                                End

                                CountNormal# += 1

! Changing (DBH 18/04/2008) # N/A - Stop updating error messages
!                Access:JOBS.Update()
! to (DBH 18/04/2008) # N/A
                                If Access:JOBS.TryUpdate()
                                    ErrorLog('Error updating job number: ' & job:Ref_Number)
                                End ! If Access:JOBS.TryUpdate()
! End (DBH 18/04/2008) #N/A
                                local:JobUpdated = True
                                local:AuditText = Clip(local:AuditText) & '<13,10>EXCHANGED: ' & Clip(impfil:ExchangeIMEINumber)
                                !local:AuditText = Clip(local:AuditText) & '<13,10>EXCHANGED: ' & Clip(impfil:IMEINumber)

                                If Access:AUDIT.PrimeRecord() = Level:Benign
                                    aud:Notes         = 'IMPORT FILE: ' & Clip(func:ShortName) & |
                                                        '<13,10>JOB UPDATED BY WILFRED<13,10>EXCHANGE UNIT NO: ' & Clip(job:Exchange_Unit_Number) & |
                                                        '<13,10>I.M.E.I. NO: ' & Clip(impfil:ExchangeIMEINumber)
                                                        !'<13,10>I.M.E.I. NO: ' & Clip(impfil:IMEINumber)
                                    aud:Ref_Number    = job:ref_number
                                    aud:Date          = Today()
                                    aud:Time          = Clock()
                                    aud:Type          = 'EXC'
                                    aud:User          = 'WEB'
                                    aud:Action        = 'EXCHANGE UNIT ATTACHED'
                                    Access:AUDIT.Insert()
                                End!If Access:AUDIT.PrimeRecord() = Level:Benign

                            Else !If Access:exchange.TryInsert() = Level:Benign
                                !Insert Failed
                                Access:exchange.CancelAutoInc()
                            End !If Access:exchange.TryInsert() = Level:Benign
                        End !If Access:exchange.PrimeRecord() = Level:Benign
                    End ! If job:Exchange_Unit_Number = ''
                Else ! If impfil:IMEINumber <> ''
                    If impfil:ExchangeIMEINUmber = job:ESN
                        ErrorLog('Repair IMEI In Exchange IMEI Field. Job No: ' & job:Ref_Number & 'File: ' & Clip(func:ShortName))
                    End ! If impfil:ExchangeIMEINUmber = job:ESN
                End ! If impfil:IMEINumber <> ''

                local:UpdateStatus = True

                ! Variables hold the last date/time of a status change
                local:LastStatusDate = ''
                local:LastStatusTime = ''

                ! Inserting (DBH 04/04/2007) # 8901 - Check if the status change has already happened on the date/time. This prevents duplicate entries
                Access:AUDSTATS.Clearkey(aus:NewStatusKey)
                aus:RefNumber = job:Ref_Number
                aus:Type = 'JOB'
                Set(aus:NewStatusKey,aus:NewStatusKey)
                Loop ! Begin Loop
                    If Access:AUDSTATS.Next()
                        Break
                    End ! If Access:AUDSTATS.Next()
                    If aus:RefNumber <> job:Ref_Number
                        Break
                    End ! If aus:RefNumber <> job:Ref_Number
                    If aus:Type <> 'JOB'
                        Break
                    End ! If aus:Type <> 'JOB'

                    ! (GK 08/05/2007 # 8966 - Ignore Historic Date and Time Stamped)
                    If local:LastStatusDate = ''
                        local:LastStatusDate = aus:DateChanged
                        local:LastStatusTime = aus:TimeChanged
                    Else
                        If aus:DateChanged > local:LastStatusDate
                            local:LastStatusDate = aus:DateChanged
                            local:LastStatusTime = aus:TimeChanged
                        Else
                            If aus:DateChanged = local:LastStatusDate
                                If aus:TimeChanged > local:LastStatusTime
                                    local:LastStatusTime = aus:TimeChanged
                                End
                            End
                        End
                    End

                    If Sub(aus:NewStatus,1,3) <> Sub(impfil:StatusCode,1,3)
                        Cycle
                    End ! If aus:NewStatus <> Sub(impfil:StatusCode,1,3)

                    If aus:DateChanged = Deformat(impfil:StatusDate,@d6)
                        If aus:TimeChanged = Deformat(impfil:StatusTime,@t1)
                            local:UpdateStatus = False
                            Break
                        End ! If aus:TimeChanged = impfil:StatusTime
                    End ! If aus:DateChanged = impfil:StatusDate
                End ! Loop
                ! End (DBH 04/04/2007) #8901

                ! (GK 08/05/2007 # 8966 - Ignore Historic Date and Time Stamped)
                If local:UpdateStatus = True
                    If local:LastStatusDate > Deformat(impfil:StatusDate,@d6)
                        local:UpdateStatus = False
                    Else
                        If local:LastStatusDate = Deformat(impfil:StatusDate,@d6)
                            If local:LastStatusTime > Deformat(impfil:StatusTime,@t1)
                                 local:UpdateStatus = False
                            End
                        End
                    End
                End

                !These Status are sent by SB, so ignore if Wilfred sends them - TrkBs:  (DBH: 02-02-2005)
                Case impfil:StatusCode
                    Of '114' OrOf |
                        '135' OrOf |
                        '798' OrOf |
                        '930' OrOf |
                        '940'
                        local:UpdateStatus = False
                End ! Case impfil:StatusCode

                !Do not update jobs with specific SID status  (DBH: 23-11-2004)
                !Allow them to change "114" jobs - TrkBs:  (DBH: 08-03-2005)
                Case Sub(job:Current_Status,1,3)
                    !Of '114'  OrOf |    ! Awaiting Arrival
                    Of  '798' OrOf |    ! Job Cancelled By Vodafone
                        '930' OrOf |    ! Arrived Back At Store
                        '940'           ! Phone Returned To Customer
                        Cycle            
                End ! Case Sub(job:Current_Status,1,3)

                If local:UpdateStatus = True
                    ! Is the status in the import file the same as the job status? (DBH: 11-05-2006)
                    If Sub(impfil:StatusCode,1,3) = Sub(job:Current_Status,1,3)
                        local:StatusChanged = False
                    Else ! If Sub(impfil:StatusCode,1,3) = Sub(job:Current_Status,1,3)
                        local:StatusChanged = True
                    End ! If Sub(impfil:StatusCode,1,3) = Sub(job:Current_Status,1,3)

                    ! Set the Status Change date/time from the import file - TrkBs: 5959 (DBH: 08-07-2005)
                    If impfil:StatusDate = ''
                        ! Incase by some miracle CRC forget to put a date in the file - TrkBs: 5959 (DBH: 08-07-2005)
                        GetStatusAlternative(Sub(impfil:StatusCode,1,3),1,'JOB',Today(),Clock())
                    Else ! If impfil:StatusDate = ''
                        GetStatusAlternative(Sub(impfil:StatusCode,1,3),1,'JOB',Deformat(impfil:StatusDate,@d6),Deformat(impfil:StatusTime,@t1))
                    End ! If impfil:StatusDate = ''
 
                    local:JobUpdated = True
                    local:AuditText = Clip(local:AuditText) & '<13,10>STATUS: ' & Clip(impfil:StatusCode)

                    ! Inserting (DBH 06/12/2005) #6436 - Is this imei a BER?
                    If impfil:StatusCode = '360' And impfil:IMEINumber <> ''
                        If impfil:IMEINumber <> job:ESN
                            ! They have sent and amendment to the IMEI Number. So need to un-ber the original, and ber the new one - TrkBs: 6436 (DBH: 06-12-2005)
                            ! Un-BER the original IMEI - TrkBs: 6436 (DBH: 06-12-2005)
                            Access:IMEISHIP.ClearKey(IMEI:IMEIKey)
                            IMEI:IMEINumber = job:ESN
                            If Access:IMEISHIP.TryFetch(IMEI:IMEIKey) = Level:Benign
                                !Found
                                imei:BER = False
                                Access:IMEISHIP.TryUpdate()
                            Else !If Access:IMEISHIP.TryFetch(IMEI:IMEIKey) = Level:Benign
                                !Error
                                ! Add a new record for this imei - TrkBs: 6436 (DBH: 06-12-2005)
                                If Access:IMEISHIP.PrimeRecord() = Level:Benign
                                    imei:IMEINumber = job:ESN
                                    imei:BER        = False
                                    If Access:IMEISHIP.TryInsert() = Level:Benign
                                        ! Insert Successful
                                    Else ! If Access:IMEISHIP.TryInsert() = Level:Benign
                                        ! Insert Failed
                                        Access:IMEISHIP.CancelAutoInc()
                                    End ! If Access:IMEISHIP.TryInsert() = Level:Benign
                                End !If Access:IMEISHIP.PrimeRecord() = Level:Benign
                            End !If Access:IMEISHIP.TryFetch(IMEI:IMEIKey) = Level:Benign

                            ! BER the new imei - TrkBs: 6436 (DBH: 06-12-2005)
                            Access:IMEISHIP.ClearKey(IMEI:IMEIKey)
                            IMEI:IMEINumber = impfil:IMEINumber
                            If Access:IMEISHIP.TryFetch(IMEI:IMEIKey) = Level:Benign
                                !Found
                                imei:BER = True
                                Access:IMEISHIP.TryUpdate()
                            Else !If Access:IMEISHIP.TryFetch(IMEI:IMEIKey) = Level:Benign
                                !Error
                                ! Add a new record for this imei - TrkBs: 6436 (DBH: 06-12-2005)
                                If Access:IMEISHIP.PrimeRecord() = Level:Benign
                                    imei:IMEINumber = impfil:IMEINumber
                                    imei:BER        = True
                                    If Access:IMEISHIP.TryInsert() = Level:Benign
                                        ! Insert Successful
                                    Else ! If Access:IMEISHIP.TryInsert() = Level:Benign
                                        ! Insert Failed
                                        Access:IMEISHIP.CancelAutoInc()
                                    End ! If Access:IMEISHIP.TryInsert() = Level:Benign
                                End !If Access:IMEISHIP.PrimeRecord() = Level:Benign
                            End !If Access:IMEISHIP.TryFetch(IMEI:IMEIKey) = Level:Benign

                            !local.SetBERCharge()
                            local.SetRepairAdminCharge()

                        Else ! If impfil:IMEINumber <> job:ESN
                            ! BER this imei - TrkBs: 6436 (DBH: 06-12-2005)
                            Access:IMEISHIP.ClearKey(IMEI:IMEIKey)
                            IMEI:IMEINumber = job:ESN
                            If Access:IMEISHIP.TryFetch(IMEI:IMEIKey) = Level:Benign
                                !Found
                                IMEI:BER = True
                                Access:IMEISHIP.TryUpdate()
                            Else !If Access:IMEISHIP.TryFetch(IMEI:IMEIKey) = Level:Benign
                                !Error
                                ! Add a new record for this imei - TrkBs: 6436 (DBH: 06-12-2005)
                                If Access:IMEISHIP.PrimeRecord() = Level:Benign
                                    imei:IMEINumber = job:ESN
                                    imei:BER        = True
                                    If Access:IMEISHIP.TryInsert() = Level:Benign
                                        ! Insert Successful

                                    Else ! If Access:IMEISHIP.TryInsert() = Level:Benign
                                        ! Insert Failed
                                        Access:IMEISHIP.CancelAutoInc()
                                    End ! If Access:IMEISHIP.TryInsert() = Level:Benign
                                End !If Access:IMEISHIP.PrimeRecord() = Level:Benign
                            End !If Access:IMEISHIP.TryFetch(IMEI:IMEIKey) = Level:Benign

                            !local.SetBERCharge()
                            local.SetRepairAdminCharge()

                        End ! If impfil:IMEINumber <> job:ESN
                    End ! If impfil:StatusCode = '360'
                    ! End (DBH 06/12/2005) #6436

                    ! TrackerBase 10569 - GK 22/04/2009, Set repair admin charge for the following incoming status types
                    !    360 BER - handled above
                    !    540 ESTIMATE REFUSED
                    !    530 ESTIMATE EXPIRED
                    If impfil:StatusCode = '530' or impfil:StatusCode = '540'
                        local.SetRepairAdminCharge()
                    End

!                    ! Inserting (DBH 27/02/2006) #6915 - Determine which sms alert to send
!                    If InList(Sub(job:Current_Status,1,3),'140','315','901') And local:StatusChanged = True
!                        SQLFile{prop:SQL} = 'SELECT Ref_Number,SBJobNo, BookingType, SMSReq, EmailReq, CountryDelivery, ValueSegment FROM WebJob Where SBJobNo = ' & job:Ref_Number
!                        Loop
!                            Next(SQLFile)
!                            If ~Error()
!                                Do BuildBankHolidayQueue
!                                If sql:SQLSMSReq = True
!                                    If Access:PENDMAIL.PrimeRecord() = Level:Benign
!                                        If local.UpdatePENDMAILRecord('SMS') = False
!                                            Access:PENDMAIL.CancelAutoInc()
!                                        Else ! If local.UpdatePENDMAILRecord('SMS') = False
!                                            If Access:PENDMAIL.TryInsert() = Level:Benign
!                                                ! Insert Successful
!
!                                            Else ! If Access:PENDMAIL.TryInsert() = Level:Benign
!                                                ! Insert Failed
!                                                Access:PENDMAIL.CancelAutoInc()
!                                            End ! If Access:PENDMAIL.TryInsert() = Level:Benign
!                                        End ! If local.UpdatePENDMAILRecord('SMS') = False
!                                    End !If Access:PENDMAIL.PrimeRecord() = Level:Benign
!                                End ! If sql:SQLSMSReq = True
!
!                                If sql:SQLEmailReq = True
!                                    If Access:PENDMAIL.PrimeRecord() = Level:Benign
!                                        If local.UpdatePENDMAILRecord('EMAIL') = False
!                                            Access:PENDMAIL.CancelAutoInc()
!                                        Else ! If local.UpdatePENDEMAILRecord('EMAIL') = False
!                                            !Always send the email straight away
!                                            pem:DateToSend = Today()
!                                            pem:TimeToSend = Clock()
!                                            If Access:PENDMAIL.TryInsert() = Level:Benign
!                                                ! Insert Successful
!
!                                            Else ! If Access:PENDMAIL.TryInsert() = Level:Benign
!                                                ! Insert Failed
!                                                Access:PENDMAIL.CancelAutoInc()
!                                            End ! If Access:PENDMAIL.TryInsert() = Level:Benign
!
!                                        End ! If local.UpdatePENDEMAILRecord('EMAIL') = False
!                                    End !If Access:PENDMAIL.PrimeRecord() = Level:Benign
!                                End ! If sql:SMSEmailReq = True
!                            End ! If ~Error()
!                            Break
!                        End ! Loop
!                    End ! If InList(Sub(job:Current_Status,1,3),'140','315','901')
!                    ! End (DBH 27/02/2006) #6915


                    !When a job is despatched from the service centre an SMS notification message is sent to the customer.
                    !If the job is being updated with a despatch status, update the WebJob table (GK 18-01-2005)
                    SQLFile{prop:SQL} = 'SELECT Ref_Number,SBJobNo, BookingType, SMSReq, EmailReq, CountryDelivery, ValueSegment, SMSType, SMSSent, RetStoreCode FROM WebJob Where SBJobNo = ' & job:Ref_Number
                    Next(SQLFile)
                    If ~Error()
                        If local:StatusChanged = True
                            Case Sub(job:Current_Status,1,3)
                                Of '901' OrOf |    ! 901 DESPATCHED
                                   '902' OrOf |    ! 902 RETAIL EXCHANGE DESPATCHED
                                   '905' OrOf |    ! 905 DESPATCHED UNPAID
                                   '910'           ! 910 DESPATCH PAID
                                    If sql:SQLBookingType = 'R' ! Retail jobs only
                                        !Set SMSType field to D = Send SMS with despatch from Service Centre text
                                        If (sql:SQLSMSType = 'D' and sql:SQLSMSSent = 1)
                                        Else
                                            SQLFile{Prop:SQL} = 'UPDATE WEBJOB SET SMSType = ''D'', SMSSent = 0, EmailSent = 0 WHERE SBJobNo = ' & job:Ref_Number
                                        End
                                    End

                                    WebJobEx{Prop:SQL} = 'SELECT SCAccountID FROM WebJobEx WHERE WebJobNo = ' & SQL:SQLRefNumber
                                    next(WebJobEx)

                                    case sql:SQLBookingType
                                        of 'E'
                                        orof 'F'
                                        orof 'U'
                                            ! Monday - Sent On Friday before 6pm
                                            ! Tuesday - Sent On Friday after 6pm, Saturday, Sunday, Monday
                                            ! Wednesday - Sent On Tuesday
                                            ! Thursday - Sent On Wednesday
                                            ! Friday - Sent On Thursday
                                            !

                                            local:DespatchDate = deformat(impfil:StatusDate, @d6)

                                            CCExchange{Prop:SQL} = 'SELECT TransitDays FROM CCExchange WHERE WebJobNo = ' & SQL:SQLRefNumber
                                            next(CCExchange)

                                            ! Get the transit days
                                            local:transitDays = ccex:TransitDays
                                            if local:transitDays = 0 then local:transitDays = 1.

                                            case (today() % 7)
                                                of 0 ! SUN
                                                    ! Send Tuesdays
                                                    local:actualExchangeDate = local.GetWorkDate(webex:SCAccountID, SQL:SQLCountryDelivery, local:DespatchDate, local:transitDays)
                                                of 1 ! MON
                                                orof 2 ! TUES
                                                orof 3 ! WED
                                                orof 4 ! THUR
                                                    if clock() < deformat('18:00', @T1)
                                                        ! Before 6pm - Send Next Day
                                                        local:actualExchangeDate = local.GetWorkDate(webex:SCAccountID, SQL:SQLCountryDelivery, local:DespatchDate, local:transitDays)
                                                    else
                                                        ! After 6pm - Add extra day
                                                        local:actualExchangeDate = local.GetWorkDate(webex:SCAccountID, SQL:SQLCountryDelivery, local:DespatchDate, local:transitDays + 1)
                                                    end
                                                of 5 ! FRI
                                                    if clock() < deformat('18:00', @T1)
                                                        ! Before 6pm - Send Mondays
                                                        local:actualExchangeDate = local.GetWorkDate(webex:SCAccountID, SQL:SQLCountryDelivery, local:DespatchDate, local:transitDays)
                                                    else
                                                        ! After 6pm - Send Tuesdays
                                                        local:actualExchangeDate = local.GetWorkDate(webex:SCAccountID, SQL:SQLCountryDelivery, local:DespatchDate, local:transitDays + 1)
                                                    end
                                                of 6 ! SAT
                                                    ! Send Tuesdays
                                                    local:actualExchangeDate = local.GetWorkDate(webex:SCAccountID, SQL:SQLCountryDelivery, local:DespatchDate, local:transitDays)
                                            end ! case

                                            CCExchange{Prop:SQL} = 'UPDATE CCExchange SET ActualExchangeDate = ''' & format(local:actualExchangeDate, @d010-) & ''' WHERE WebJobNo = ' & SQL:SQLRefNumber
                                        of 'Y'
                                            ! Monday - Sent On Friday before 6pm
                                            ! Tuesday - Sent On Friday after 6pm, Saturday, Sunday, Monday
                                            ! Wednesday - Sent On Tuesday
                                            ! Thursday - Sent On Wednesday
                                            ! Friday - Sent On Thursday
                                            !

                                            local:actualExchangeDate = deformat(impfil:StatusDate, @d6)

                                            case (today() % 7)
                                                of 0 ! SUN
                                                    ! Send Tuesdays
                                                    local:actualExchangeDate = local.FindNextFreeDate(SQL:SQLRetStoreCode, local:actualExchangeDate)
                                                of 1 ! MON
                                                orof 2 ! TUES
                                                orof 3 ! WED
                                                orof 4 ! THUR
                                                    if clock() < deformat('18:00', @T1)
                                                        ! Before 6pm - Send Next Day
                                                        local:actualExchangeDate = local.FindNextFreeDate(SQL:SQLRetStoreCode, local:actualExchangeDate)
                                                    else
                                                        ! After 6pm - Add extra day
                                                        local:actualExchangeDate = local.FindNextFreeDate(SQL:SQLRetStoreCode, local:actualExchangeDate + 1)
                                                    end
                                                of 5 ! FRI
                                                    if clock() < deformat('18:00', @T1)
                                                        ! Before 6pm - Send Mondays
                                                        local:actualExchangeDate = local.FindNextFreeDate(SQL:SQLRetStoreCode, local:actualExchangeDate)
                                                    else
                                                        ! After 6pm - Send Tuesdays
                                                        local:actualExchangeDate = local.FindNextFreeDate(SQL:SQLRetStoreCode, local:actualExchangeDate + 1)
                                                    end
                                                of 6 ! SAT
                                                    ! Send Tuesdays
                                                    local:actualExchangeDate = local.FindNextFreeDate(SQL:SQLRetStoreCode, local:actualExchangeDate)
                                            end ! case

                                            CCExchange{Prop:SQL} = 'UPDATE CCExchange SET ActualExchangeDate = ''' & format(local:actualExchangeDate, @d010-) & ''' WHERE WebJobNo = ' & SQL:SQLRefNumber
                                    End

                                    !TrackerBase 9631 - GK 17/12/2007
                                    WebJobEx{Prop:SQL} = 'UPDATE WebJobEx SET DespatchDate = ''' & format(deformat(impfil:StatusDate, @d6), @d010-) & ''' WHERE WebJobNo = ' & SQL:SQLRefNumber
                            End ! Case Sub(job:Current_Status,1,3)
                        End
                    End

                    ! Update completed flag in WEBJOB if status is 705 COMPLETED
                    if local:StatusChanged = true and sub(job:Current_Status,1,3) = '705'
                        SQLFile{Prop:SQL} = 'UPDATE WEBJOB SET Completed = 1 WHERE SBJobNo = ' & job:Ref_Number
                    end

                    CountStatus# += 1
                End ! If local:UpdateStatus = True

                !Is there a resolution code? - TrkBs:  (DBH: 02-02-2005)
                If Clip(impfil:ResolutionCode) <> ''
                    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                    jbn:RefNumber  = job:Ref_Number
                    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        !Found
                        ! Inserting (DBH 09/03/2006) #6915 - Don't allow duplicate entries for resolution code
                        If ~Instring(Clip(impfil:ResolutionCode),jbn:Invoice_Text,1,1)
                        ! End (DBH 09/03/2006) #6915
                            jbn:Invoice_Text = Clip(jbn:Invoice_Text) & '<13,10>' & Clip(impfil:ResolutionCode)
                            Access:JOBNOTES.TryUpdate()
                            local:JobUpdated = True
                            local:AuditText = Clip(local:AuditText) & '<13,10>RES CODE: ' & Clip(impfil:ResolutionCode)
                        End ! If ~Instring(Clip(impfil:ResolutionCode),jbn:Invoice_Text,1,1)
                    Else ! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
                        !Error
                    End ! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
                End ! If impfil:ResolutionCode <> ''

                !Is there a consignment number? - TrkBs:  (DBH: 02-02-2005)
                If Clip(impfil:ConsignmentNumber) <> ''
                    !If there is an exchange attached, update the exchange consignment no - TrkBs:  (DBH: 02-02-2005)
                    If job:Exchange_Unit_Number <> ''
                        job:Exchange_Consignment_Number = impfil:ConsignmentNumber
                    Else ! If job:Exchange_Unit_Number <> ''
                        job:Consignment_Number = impfil:ConsignmentNumber
                    End ! If job:Exchange_Unit_Number <> ''
                    local:JobUpdated = True
                    local:AuditText = Clip(local:AuditText) & '<13,10>CONS NO:' & Clip(impfil:ConsignmentNumber)
                End ! If Clip(impfil:ConsignmentNumber) <> ''

                If impfil:IMEINumber <> job:ESN And impfil:IMEINumber <> ''
                    job:ESN = impfil:IMEINumber
                    local:JobUpdated = True
                    local:AuditText = Clip(local:AuditText) & '<13,10>IMEI: ' & Clip(impfil:IMEINumber)
                End ! If impfil:IMEINumber <> job:ESN

                ! Inserting (DBH 24/03/2006) #7413 - Write the Incoming COnsignment Number
                If impfil:IncomingConsignmentNumber <> ''
                    job:Incoming_Consignment_Number = Clip(impfil:IncomingConsignmentNumber)
                    local:JobUpdated = True
                    local:AuditText = Clip(local:AuditText) & '<13,10>IN CONS NO: ' & Clip(impfil:IncomingConsignmentNumber)
                End ! If impfil:IncomingConsignmentNumber <> ''
                ! End (DBH 24/03/2006) #7413

! Changing (DBH 18/04/2008) # N/A - Stop updating error messages
!                Access:JOBS.Update()
! to (DBH 18/04/2008) # N/A
                If Access:JOBS.TryUpdate()
                    ErrorLog('Error updating job number: ' & job:Ref_Number)
                End ! If Access:JOBS.TryUpdate()
! End (DBH 18/04/2008) #N/A

                !The job has been updated by wilfred - TrkBs:  (DBH: 02-02-2005)
                If local:JobUpdated = True
                    ! GK 13/06/2007 TrackerBase 8830 - SID Extract SQL interface
                    ! Add SB job number to table if it does not already exist
                    !Access:SIDEXUPD.ClearKey(seu:SBJobNoKey)
                    !seu:SBJobNo = job:ref_number
                    !if Access:SIDEXUPD.Fetch(seu:SBJobNoKey) <> Level:Benign
                    !    seu:SBJobNo = job:ref_number
                    !    Access:SIDEXUPD.TryInsert()
                    !end

                    If Access:AUDIT.PrimeRecord() = Level:Benign
                        ! Changing (DBH 06/12/2005) #6436 - Show the specific changes
                        !                         aud:Notes         = 'IMPORT FILE: ' & Clip(func:ShortName) & |
                        !                                             '<13,10>Status Date: ' & Clip(impfil:StatusDate) & |
                        !                                             '<13,10>Status Time: ' & Clip(impfil:StatusTime) & |
                        !                                             '<13,10>Status Code: ' & Clip(impfil:StatusCode)
                        ! to (DBH 06/12/2005) #6436
                        aud:Notes         = 'IMPORT FILE: ' & Clip(func:ShortName) & |
                                            '<13,10>Status Date: ' & Clip(impfil:StatusDate) & |
                                            '<13,10>Status Time: ' & Clip(impfil:StatusTime) & |
                                            '<13,10>Status Code: ' & Clip(impfil:StatusCode) & Clip(local:AuditText)
                        ! End (DBH 06/12/2005) #6436
                        aud:Ref_Number    = job:ref_number
                        aud:Date          = Today()
                        aud:Time          = Clock()
                        aud:Type          = 'JOB'
                        aud:User          = 'WEB'
                        aud:Action        = 'JOB UPDATED BY WILFRED'
                        Access:AUDIT.Insert()
                    End!If Access:AUDIT.PrimeRecord() = Level:Benign
                End ! If local:JobUpdated = True
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        Else ! If impfil:RefNumber <> ''
            Cycle
        End ! If impfil:RefNumber <> ''
    End !Loop(ImportFile)
    Close(ImportFile)
    Close(SQLFile)
    Close(WebJobEx)
    Close(SQLDefaults)
    !Close(ValSegBkType)
    Close(CCExchange)
    Relate:JOBS.Close()
    Relate:DEFAULTS.Close()
    Relate:USERS.Close()
    Relate:EXCHANGE.Close()
    Relate:DESBATCH.Close()
    Relate:STATUS.Close()
    Relate:IMEISHIP.Close()
    Relate:PENDMAIL.Close()
    Relate:DEFEMAI2.Close()
    Relate:AUDSTAEX.Close()
    Relate:CARISMA.Close()
    Relate:SIDSRVCN.Close()
    Relate:AUDSTATS.Close()
    !Relate:SIDEXUPD.Close()
    Relate:MODELNUM.Close()
    Relate:SIDNWDAY.Close()

!    ErrorLog('Exchage Jobs Imported: ' & CountNormal#)
!    ErrorLog('Status Update Jobs Imported: ' & CountStatus#)

    Rename(Clip(Path()) & '\WilfredToSend\' & Clip(func:ShortName),Clip(Path()) & '\WilfredSent\' & Clip(func:ShortName))

     IF Error()
        Case Error()
            Of ''
                Error" = 'Same File Already Exists'
            Of 2
                Error" = 'File Not Found'
            Of 5
                Error" = 'Access Denied'
            Of 52
                Error" = 'File Aready Open'
            Else
                Error" = Error()
        End ! Case Error()
        ErrorLog('Rename Error (' & Clip(Error") & '): ' & Clip(func:ShortName))
    Else ! If Error()
!        ErrorLog('File Placed in ''Sent'' folder')
    End ! If Error()
    !End   - Debug - TrkBs:  (DBH: 25-01-2005)
local.SetRepairAdminCharge      Procedure

repairAdminCharge    pdecimal(8, 2)

    code
        ! TrackerBase 10569 - Section 12 Repair Admin Charge GK 22/04/2009

        ! Should be called for the following status types :-

        !    360 BER
        !    540 ESTIMATE REFUSED
        !    530 ESTIMATE EXPIRED

        ! Get the repair admin charge from the defaults table (only one record in table)
        SQLDefaults{Prop:SQL} = 'SELECT RepairAdminCharge FROM Defaults WHERE DefaultID = 1'

        next(SQLDefaults)
        if not error()
            repairAdminCharge = sqldef:RepairAdminCharge
        end

        ! Save repair admin charge
        WebJobEx{Prop:SQL} = 'UPDATE WebJobEx SET RepairAdminCharge = ' & repairAdminCharge & ' WHERE WebJobNo = ' & SQL:SQLRefNumber

!local.SetBERCharge      Procedure  ! No longer called, left here in case Vodafone change there mind  GK - (22 April 09)
!
!bookingType  string(1)
!valueSegment string(20)
!berCharge    pdecimal(8, 2)
!
!    code
!
!    SQLFile{Prop:SQL} = 'SELECT Ref_Number, SBJobNo, BookingType, SMSReq, EmailReq, CountryDelivery, ValueSegment, SMSType, SMSSent FROM WebJob Where SBJobNo = ' & job:Ref_Number
!    next(SQLFile)
!    if not error()
!        ! Get BER booking type
!        if SQL:SQLBookingType = 'E' or SQL:SQLBookingType = 'B'
!            ! Contact Centre
!            bookingType = '2'
!        else
!            ! Retail
!            bookingType = '1'
!        end
!
!        ! Get value segment
!        case clip(SQL:SQLValueSegment)
!            of 'CTN UNIDENTIFIED'
!                valueSegment = 'CTNU'
!            else
!                valueSegment = clip(SQL:SQLValueSegment)
!        end
!
!        ValSegBkType{Prop:SQL} = 'SELECT IsCharge, ChargePercent FROM ValSegBkType WHERE BookingType = ''' & bookingType & |
!                                ''' AND ValueSegment = ''' & clip(valueSegment) & ''''
!
!        next(ValSegBkType)
!        if not error()
!            if vsbt:IsCharge = 1 and vsbt:ChargePercent > 0 ! Is a charge percentage of the model cost applied or is this FOC ?
!                Access:MODELNUM.ClearKey(mod:Model_Number_Key)
!                mod:Model_Number = job:Model_Number
!                if Access:MODELNUM.Fetch(mod:Model_Number_Key) = Level:Benign
!                    ! All percentage charge calculations will be rounded-up to the nearest pound (�) value
!                    berCharge =  round(mod:UnitCost * (vsbt:ChargePercent / 100), 1)
!                end
!            end
!        end
!
!        ! Save BER charge
!        WebJobEx{Prop:SQL} = 'UPDATE WebJobEx SET BERCharge = ' & berCharge & ' WHERE WebJobNo = ' & SQL:SQLRefNumber
!
!    end
local.GetWorkDate           procedure(long scAccountID, string country, date startDate, long workingDays)

endDate         date
nonWorkingDay   byte

code
    endDate = startDate

    loop 

        case (endDate % 7)
            of 0 ! SUN
                endDate += 1
            of 6 ! SAT
                endDate += 2
            else ! Check bank holidays
                if IsBankHoliday(country, endDate) = true
                    endDate += 1
                else
                    if workingDays > 0
                        ! Check if non working day
                        nonWorkingDay = false

                        Access:SIDNWDAY.ClearKey(nwd:SCDayKey)
                        nwd:SCAccountID = scAccountID
                        set(nwd:SCDayKey, nwd:SCDayKey)
                        loop
                            if Access:SIDNWDAY.Next() <> Level:Benign then break.
                            if nwd:SCAccountID <> scAccountID then break.
                            if endDate = nwd:NonWorkingDay
                                nonWorkingDay = true
                                break
                            end
                        end

                        endDate += 1
                        if nonWorkingDay = false
                            workingDays -= 1
                        end
                    else
                        break
                    end

                end
        end ! case

    end


    return endDate
local.FindNextFreeDate procedure(string strAccount, date currentDate)
    !
    ! Description : Find the next free date (Just a wrapper for FindFreeDate)
    !

strNext          string('+')
strDateToProcess string(30)
    code

    strDateToProcess = format(currentDate, @d06b)

    FindFreeDate(strNext, strAccount, strDateToProcess)

    return deformat(strDateToProcess, @d06b)
