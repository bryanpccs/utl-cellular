

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN025.INC'),ONCE        !Local module procedure declarations
                     END


UpdEstimateCollectionDate PROCEDURE  (collectedToday, strTimeLimit, strCurrentTime, strCurrentStatus, scAccountID, strAccountFrom, strAccountTo, strDate, strTimeFrame, strModelNo, strPath) ! Declare Procedure
startDate   date
estimateCollectionDate date
timeLimit   time
currentTime time
result      long
strDirection     cstring('+')
calcDaysToSC byte
calcDaysAtSC byte
calcDaysToStore byte
strBookingType  cstring('R')

! strTest     string(10000)
    map
FindNextFreeDate procedure(*cstring strAccount, date currentDate), date
    end
  CODE
    !
    ! Description : Estimate the collection date
    !

    if strDate <> '' and exists(clip(strPath) & '\JOBS.DAT')
        startDate = deformat(strDate, @d06b)

        ! strTest = clip(strTest) & 'Estimating Collection Date At: ' & format(today(), @d06b) & ' ' & format(clock(), @t4) & '<13,10>'
        ! strTest = clip(strTest) & 'Start Date: ' & format(startDate, @d06b) & '<13,10>'

        ! Check if we need to add an extra day if todays collection has already occurred
        if collectedToday = true
            ! Parceline have already collected today, add an extra day
            startDate += 1
            ! strTest = clip(strTest) & 'Parceline Collected - Estimated Date: ' & format(startDate, @d06b) & '<13,10>'
        else
            ! Add an extra day if the time limit has been exceeded
            if strTimeLimit <> '' and strCurrentTime <> ''
                timeLimit = deformat(strTimeLimit, @t01b)
                currentTime = deformat(strCurrentTime, @t01b)
                ! Is current time later than the time limit?
                if currentTime > timeLimit
                    startDate += 1
                    ! strTest = clip(strTest) & 'Surpassed Time Limit - Estimated Date: ' & format(startDate, @d06b) & '<13,10>'
                end
            end
        end

        ! Find the actual starting date, current date might be a weekend / bank holiday 
        startDate = FindNextFreeDate(strAccountFrom, startDate)
        ! strTest = clip(strTest) & 'Initial Starting Date (BH check) - Estimated Date: ' & format(startDate, @d06b) & '<13,10>'

        calcDaysToSC = true
        calcDaysAtSC = true
        calcDaysToStore = true

        if strCurrentStatus <> ''
            case clip(upper(strCurrentStatus))
                of 'BOOKED IN STORE AWAITING DESPATCH'
                orof 'DESPATCHED FROM STORE'

                of 'BOOKED IN AT SERVICE CENTRE'
                orof 'BEING INSPECTED BY SERVICE TECHNICIAN'
                orof 'DESPATCHED FROM SERVICE CENTRE'
                    calcDaysToSC = false
                    calcDaysAtSC = false
                else
                    calcDaysToSC = false
                    calcDaysAtSC = false
                    calcDaysToStore = false
                    ! startDate = today()
                    ! Set start date to next working day
                    ! startDate = FindNextFreeDate(strAccountFrom, startDate)
            end ! case
        end

        ! -----------------------------------------------------------------------------
        ! Calculate the number of days in transit from the store to the service centre
        strDate = format(startDate, @d06b)
        ! LinePrint('Start Date: ' & strDate, 'c:\CollectionDate.log')
        if calcDaysToSC = true
            TransitToSCDate(strAccountFrom, scAccountID, strDate, strPath)
            ! LinePrint('After CalcDaysToSC: ' & strDate, 'c:\CollectionDate.log')
            ! strTest = clip(strTest) & 'Arrive At Service Centre - Store Account: ' & strAccountFrom & ' SC Account ID: ' & scAccountID & ' Estimated Date: ' & strDate & '<13,10>'
        end

        if calcDaysAtSC = true
            ! Calculate number of days at service centre
            TurnaroundAtSC(scAccountID, strBookingType, strDate, strTimeFrame, strModelNo, strPath)
            ! LinePrint('After CalcDaysAtSC: ' & strDate, 'c:\CollectionDate.log')
            ! strTest = clip(strTest) & 'Leave Service Centre - Estimated Date: ' & strDate & '<13,10>'
        end

        ! Calculate the number of days in transit from the service centre to the store
        if calcDaysToStore = true
            TransitToStoreDate(strDirection, strAccountTo, scAccountID, strDate, 2, strPath)   ! Update the capacity limit
            ! LinePrint('After CalcDaysToStore: ' & strDate, 'c:\CollectionDate.log')
            ! strTest = clip(strTest) & 'Arrive At Store - Estimated Date: ' & strDate & '<13,10>'
        end
        startDate = deformat(strDate, @d06b)
        ! -----------------------------------------------------------------------------

        ! LinePrint(strTest, clip(strPath) & '\CollectionDate.log')
        estimateCollectionDate = startDate
        strDate = format(estimateCollectionDate, @d06b)

        result = true
    end

    return result
FindNextFreeDate procedure(*cstring strAccount, date currentDate)
    !
    ! Description : Find the next free date (Just a wrapper for FindFreeDate)
    !

strDateToProcess cstring(30)
    code

    strDateToProcess = format(currentDate, @d06b)

    FindFreeDate(strDirection, strAccount, strDateToProcess, strPath)

    return deformat(strDateToProcess, @d06b)
