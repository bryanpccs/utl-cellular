

   MEMBER('locaterimport.clw')                        ! This is a MEMBER module


!------------ File: LOCDEFS, version 1 ----------------------
!------------ modified 29.03.2005 at 13:01:34 -----------------
MOD:stFileName:LOCDEFSVer1  STRING(260)
LOCDEFSVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:LOCDEFSVer1),PRE(LOCDEFSVer1)
RecordNumberKey          KEY(LOCDEFSVer1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
FTPServer                   STRING(255)
FTPUserName                 STRING(60)
FTPPassword                 STRING(60)
FTPImportPath               STRING(255)
FTPExportPath               STRING(255)
EmailServer                 STRING(255)
EmailServerPort             STRING(3)
EmailFromAddress            STRING(255)
CRCEmailName                STRING(60)
CRCEmailAddress             STRING(255)
PCCSEmailName               STRING(60)
PCCSEmailAddress            STRING(255)
VodafoneEmailName           STRING(60)
VodafoneEmailAddress        STRING(255)
                         END
                       END
!------------ End File: LOCDEFS, version 1 ------------------

!------------ File: IMEISHIP, version 1 ----------------------
!------------ modified 29.03.2005 at 13:01:34 -----------------
MOD:stFileName:IMEISHIPVer1  STRING(260)
IMEISHIPVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:IMEISHIPVer1),PRE(IMEISHIPVer1)
RecordNumberKey          KEY(IMEISHIPVer1:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEISHIPVer1:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEISHIPVer1:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
                         END
                       END
!------------ End File: IMEISHIP, version 1 ------------------

!------------ File: IMEISHIP, version 2 ----------------------
!------------ modified 14.02.2006 at 14:05:19 -----------------
MOD:stFileName:IMEISHIPVer2  STRING(260)
IMEISHIPVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:IMEISHIPVer2),PRE(IMEISHIPVer2),CREATE
RecordNumberKey          KEY(IMEISHIPVer2:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEISHIPVer2:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEISHIPVer2:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
BER                         BYTE
                         END
                       END
!------------ End File: IMEISHIP, version 2 ------------------

!------------ File: IMEISHIP, version 3 ----------------------
!------------ modified 10.02.2009 at 10:46:37 -----------------
MOD:stFileName:IMEISHIPVer3  STRING(260)
IMEISHIPVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:IMEISHIPVer3),PRE(IMEISHIPVer3),CREATE
RecordNumberKey          KEY(IMEISHIPVer3:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEISHIPVer3:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEISHIPVer3:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
BER                         BYTE
ManualEntry                 BYTE
                         END
                       END
!------------ End File: IMEISHIP, version 3 ------------------

!------------ Declaration Current Files Structure --------------------

!------------ modified 29.03.2005 at 14:27:59 -----------------
MOD:stFileName:LOCDEFSVer2  STRING(260)
LOCDEFSVer2            FILE,DRIVER('Btrieve'),OEM,NAME('LOCDEFS.DAT'),PRE(LOCDEFSVer2),CREATE
RecordNumberKey          KEY(LOCDEFSVer2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
FTPServer                   STRING(255)
FTPUserName                 STRING(60)
FTPPassword                 STRING(60)
FTPImportPath               STRING(255)
FTPExportPath               STRING(255)
EmailServer                 STRING(255)
EmailServerPort             STRING(3)
EmailFromAddress            STRING(255)
CRCEmailName                STRING(60)
CRCEmailAddress             STRING(255)
PCCSEmailName               STRING(60)
PCCSEmailAddress            STRING(255)
VodafoneEmailName           STRING(60)
VodafoneEmailAddress        STRING(255)
AlertDays                   LONG
LastImportDate              DATE
                         END
                       END

!------------ modified 12.02.2009 at 15:37:57 -----------------
MOD:stFileName:IMEISHIPVer4  STRING(260)
IMEISHIPVer4           FILE,DRIVER('Btrieve'),OEM,NAME('IMEISHIP.DAT'),PRE(IMEISHIPVer4),CREATE
RecordNumberKey          KEY(IMEISHIPVer4:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEISHIPVer4:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEISHIPVer4:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
BER                         BYTE
ManualEntry                 BYTE
ProductCode                 STRING(30)
                         END
                       END

!------------ End of Declaration Current Files Structure -------------

DCDummySQLTableOwner   STRING(260)
MOD:stFileName STRING(260)

FileQueue QUEUE,PRE()
FileLabel   &FILE
FileName    &STRING
          END

TempFileQueue QUEUE,PRE()
TempFileLabel   &FILE
TempFileName    &STRING
              END

ListExt       QUEUE,PRE()
Extention       CSTRING(260)
              END

                     MAP
                       INCLUDE('LOCAT006.INC'),ONCE        !Local module procedure declarations
ConvertLOCDEFS         PROCEDURE(BYTE),BYTE
ConvertIMEISHIP        PROCEDURE(BYTE),BYTE
ProcessFileQueue       PROCEDURE(),BYTE
ErrorBox               PROCEDURE(STRING)
FileErrorBox           PROCEDURE()
CreateTempName         PROCEDURE(*STRING,STRING,FILE)
RemoveFiles            PROCEDURE(FILE,STRING)
GetFileNameWithoutOwner PROCEDURE(STRING),STRING
RenameFile             PROCEDURE(FILE,STRING)
PrepareExtentionQueue  PROCEDURE(FILE)
PrepareMultiTableFileName PROCEDURE(FILE,STRING,*STRING),BYTE
PrepareDestFile        PROCEDURE(FILE,STRING,FILE,STRING,STRING),BYTE
RenameDestFile         PROCEDURE(FILE,STRING,STRING),BYTE
                       MODULE('ClarionRTL')
DC:FnMerge               PROCEDURE(*CSTRING,*CSTRING,*CSTRING,*CSTRING,*CSTRING),RAW,NAME('_fnmerge')
DC:FnSplit               PROCEDURE(*CSTRING,*CSTRING,*CSTRING,*CSTRING,*CSTRING),SHORT,RAW,NAME('_fnsplit')
DC:SetError              PROCEDURE(LONG),NAME('Cla$seterror')
DC:SetErrorFile          PROCEDURE(FILE),NAME('Cla$SetErrorFile')
                       END
                     END



DC:FilesConverter PROCEDURE(<STRING PAR:FileLabel>,<STRING PAR:FileName>)
  CODE
  IF ~OMITTED(1) AND ~OMITTED(2) AND PAR:FileLabel AND PAR:FileName
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOCDEFS')
      LOCDEFSVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOCDEFS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('IMEISHIP')
      IMEISHIPVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertIMEISHIP(0) THEN RETURN(1).
    END
  ELSE
    IF ConvertLOCDEFS(1) THEN RETURN(1).
    IF ConvertIMEISHIP(1) THEN RETURN(1).
  END
  RETURN(0)
!---------------------------------------------------------------------

ConvertLOCDEFS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOCDEFSVer2{PROP:Name}
  MOD:stFileName:LOCDEFSVer1 = 'LOCDEFS.DAT'
  FileQueue.FileLabel &= LOCDEFSVer1
  FileQueue.FileName &= MOD:stFileName:LOCDEFSVer1
  ADD(FileQueue)
  MOD:stFileName:LOCDEFSVer2 = 'LOCDEFS.DAT'
  FileQueue.FileLabel &= LOCDEFSVer2
  FileQueue.FileName &= MOD:stFileName:LOCDEFSVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOCDEFS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertIMEISHIP PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = IMEISHIPVer4{PROP:Name}
  MOD:stFileName:IMEISHIPVer1 = 'IMEISHIP.DAT'
  FileQueue.FileLabel &= IMEISHIPVer1
  FileQueue.FileName &= MOD:stFileName:IMEISHIPVer1
  ADD(FileQueue)
  MOD:stFileName:IMEISHIPVer2 = 'IMEISHIP.DAT'
  FileQueue.FileLabel &= IMEISHIPVer2
  FileQueue.FileName &= MOD:stFileName:IMEISHIPVer2
  ADD(FileQueue)
  MOD:stFileName:IMEISHIPVer3 = 'IMEISHIP.DAT'
  FileQueue.FileLabel &= IMEISHIPVer3
  FileQueue.FileName &= MOD:stFileName:IMEISHIPVer3
  ADD(FileQueue)
  MOD:stFileName:IMEISHIPVer4 = 'IMEISHIP.DAT'
  FileQueue.FileLabel &= IMEISHIPVer4
  FileQueue.FileName &= MOD:stFileName:IMEISHIPVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'IMEISHIP'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ProcessFileQueue PROCEDURE

LOC:lLoopIndex        LONG,AUTO
LOC:lTempIndex        LONG,AUTO
LOC:stStatusFullBuild STRING(3),AUTO
LOC:byReturnCode      BYTE(1)
LOC:byErrorStatus     BYTE(0)

  CODE
  FREE(TempFileQueue)
  LOOP LOC:lLoopIndex = RECORDS(FileQueue) TO 1 BY -1
    GET(FileQueue,LOC:lLoopIndex)
    OPEN(FileQueue.FileLabel,42h)
    IF ERRORCODE()
      IF ERRORCODE() = BadKeyErr
        IF UPPER(FileQueue.FileLabel{PROP:Driver}) <> 'CLARION' AND UPPER(FileQueue.FileLabel{PROP:Driver}) <> 'BTRIEVE'
          CLOSE(FileQueue.FileLabel)
          OPEN(FileQueue.FileLabel,12h)
          IF UPPER(FileQueue.FileLabel{PROP:Driver}) = 'TOPSPEED'
            LOC:stStatusFullBuild = SEND(FileQueue.FileLabel,'FULLBUILD')
            Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=On')
          END
          BUILD(FileQueue.FileLabel)
          IF UPPER(FileQueue.FileLabel{PROP:Driver}) = 'TOPSPEED'
            IF CLIP(UPPER(LOC:stStatusFullBuild)) = 'ON'
              Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=On')
            ELSE
              Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=Off')
            END
          END
        ELSE
          LOC:byErrorStatus = 1
          IF LOC:lLoopIndex <> 1
            CYCLE
          END
        END
      END
    ELSE
      IF LOC:lLoopIndex = RECORDS(FileQueue)
        CLOSE(FileQueue.FileLabel)
        LOC:byReturnCode = 0
        DO ProcedureReturn
      END
    END
    CLOSE(FileQueue.FileLabel)
    OPEN(FileQueue.FileLabel,12h)
    IF ERRORCODE()
      CASE ERRORCODE()
      OF NoAccessErr
        IF MESSAGE('Converting can not be finished because data file|"' & CLIP(ERRORFILE()) & '"|is locked by other station.|Ask users to quit all programs, which uses file|"' & CLIP(ERRORFILE()) & '"|and try again.','Conversion Error',ICON:Question,BUTTON:RETRY+BUTTON:ABORT,BUTTON:RETRY) = BUTTON:RETRY
          LOC:lLoopIndex += 1
          CYCLE
        ELSE
          DO ProcedureReturn
        END
      OF InvalidFileErr
      OROF NoPathErr
      OROF NoFileErr
      OROF BadKeyErr
        CASE ERRORCODE()
        OF NoPathErr
        OROF NoFileErr      ; IF LOC:byErrorStatus = 0 THEN LOC:byErrorStatus = 2.
        OF InvalidFileErr   ; IF LOC:byErrorStatus = 2 THEN LOC:byErrorStatus = 0.
        END
        IF LOC:lLoopIndex = 1
          CASE LOC:byErrorStatus
          OF 1 ; ErrorBox('Keys must be rebuild|File: ' & ERRORFILE())
          OF 2 ; LOC:byReturnCode = 0
          ELSE ; ErrorBox('No match file structure|File: ' & ERRORFILE())
          END  
          DO ProcedureReturn
        ELSE
        END
      ELSE
        IF LOC:lLoopIndex = 1 THEN FileErrorBox(); DO ProcedureReturn.
      END
    ELSE
      CLOSE(FileQueue.FileLabel)
      LOOP LOC:lTempIndex = 1 TO RECORDS(TempFileQueue)
        GET(TempFileQueue,LOC:lTempIndex)
        CreateTempName(TempFileQueue.TempFileName,'m' & LOC:lTempIndex,TempFileQueue.TempFileLabel)
        CASE UPPER(TempFileQueue.TempFileLabel{PROP:Driver})
        ELSE
          RemoveFiles(TempFileQueue.TempFileLabel,TempFileQueue.TempFileName)
        END
        CREATE(TempFileQueue.TempFileLabel)
        IF ERRORCODE() THEN FileErrorBox(); DO ProcedureReturn.
        OPEN(TempFileQueue.TempFileLabel,12h)
        IF ERRORCODE() THEN FileErrorBox(); DO ProcedureReturn.
      END
      LOC:byReturnCode = Converting(LOC:lLoopIndex)
      LOOP LOC:lTempIndex = 1 TO RECORDS(TempFileQueue)
        GET(TempFileQueue,LOC:lTempIndex)
        CLOSE(TempFileQueue.TempFileLabel)
        CASE UPPER(TempFileQueue.TempFileLabel{PROP:Driver})
        ELSE
          RemoveFiles(TempFileQueue.TempFileLabel,TempFileQueue.TempFileName)
        END
      END
      DO ProcedureReturn
    END
  END
  LOC:byReturnCode = 0
  DO ProcedureReturn
!----------------------------------------------------------

AddQueueTempFile ROUTINE
  CLEAR(TempFileQueue)
  TempFileQueue.TempFileLabel &= FileQueue.FileLabel
  TempFileQueue.TempFileName  &= FileQueue.FileName
  ADD(TempFileQueue)
  EXIT
!---------------------------------------------------------------------

ProcedureReturn ROUTINE
  FREE(FileQueue)
  FREE(TempFileQueue)
  RETURN(LOC:byReturnCode)
!---------------------------------------------------------------------

ErrorBox PROCEDURE(STRING ErrorMessage)
  CODE
  MESSAGE('Conversion impossible!|' & CLIP(ErrorMessage),'Conversion Error',ICON:Exclamation,BUTTON:ABORT)
  RETURN
!---------------------------------------------------------------------

FileErrorBox PROCEDURE
  CODE
  ErrorBox('Error: ' & CLIP(CHOOSE(ERRORCODE() = 90,FILEERROR(),ERROR())) & '|Error File: ' & CHOOSE(LEN(ERRORFILE()) > 0,CLIP(ERRORFILE()),CLIP(MOD:stFileName)))
  RETURN
!---------------------------------------------------------------------

CreateTempName PROCEDURE(*STRING PAR:ProcessFileName,STRING PAR:Postfix,FILE PAR:File)

LOC:cstPath     CSTRING(260),AUTO
LOC:cstDrive    CSTRING(260),AUTO
LOC:cstDir      CSTRING(260),AUTO
LOC:cstName     CSTRING(260),AUTO
LOC:cstExt      CSTRING(260),AUTO
LOC:cstCopyName CSTRING(260),AUTO
LOC:cstCopyExt  CSTRING(260),AUTO

  CODE
  CASE UPPER(PAR:File{PROP:Driver})
  OF 'MSSQL'
  OROF 'SQLANYWHERE'
    LOC:cstPath = CLIP(PAR:ProcessFileName) & '_' & CLIP(PAR:Postfix)
  ELSE
    LOC:cstPath = CLIP(PAR:ProcessFileName)
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF LOC:cstName[1] = '!'
      LOC:cstCopyName = LOC:cstName
      LOC:cstCopyExt  = LOC:cstExt
      LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
      Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
      LOC:cstDir = LOC:cstDir & LOC:cstName & '$' & LOC:cstCopyName[2 : LEN(LOC:cstCopyName)] & '$' & CLIP(PAR:Postfix) & LOC:cstExt & '\'
      DC:FnMerge(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstCopyName,LOC:cstCopyExt)
    ELSE
      LOC:cstName = CLIP(LOC:cstName & '$' & PAR:Postfix)
      DC:FnMerge(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    END
  END
  PAR:ProcessFileName = LOC:cstPath
  RETURN
!---------------------------------------------------------------------

RemoveFiles PROCEDURE(FILE PAR:ProcessFileLabel,STRING PAR:ProcessFileName)

LOC:cstPath    CSTRING(260),AUTO
LOC:cstDrive   CSTRING(260),AUTO
LOC:cstDir     CSTRING(260),AUTO
LOC:cstName    CSTRING(260),AUTO
LOC:cstExt     CSTRING(260),AUTO
LOC:lLoopIndex LONG,AUTO
LOC:lLoopExt   LONG,AUTO
ListDir        QUEUE(FILE:Queue),PRE(LD)
               END

  CODE
  PrepareExtentionQueue(PAR:ProcessFileLabel)
  LOC:cstPath = CLIP(PAR:ProcessFileName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOOP LOC:lLoopExt = 1 TO RECORDS(ListExt)
    GET(ListExt,LOC:lLoopExt)
    FREE(ListDir)
    DIRECTORY(ListDir,LOC:cstDrive & LOC:cstDir & LOC:cstName & ListExt.Extention,ff_:ARCHIVE)
    LOOP LOC:lLoopIndex = RECORDS(ListDir) TO 1 BY -1
      GET(ListDir,LOC:lLoopIndex)
      IF CLIP(LD:ShortName) = '..' OR CLIP(LD:ShortName) = '.' THEN CYCLE.
      REMOVE(LOC:cstDrive & LOC:cstDir & LD:Name)
    END
  END
  FREE(ListExt)
  FREE(ListDir)
  RETURN
!---------------------------------------------------------------------

GetFileNameWithoutOwner PROCEDURE(STRING PAR:FileName)

LOC:lPosition LONG,AUTO

  CODE
  IF PAR:FileName
    LOC:lPosition = INSTRING('.',PAR:FileName,1,1)
    IF LOC:lPosition
      RETURN(PAR:FileName[LOC:lPosition + 1 : LEN(CLIP(PAR:FileName))])
    ELSE
      RETURN(PAR:FileName)
    END
  ELSE
    RETURN('')
  END
!---------------------------------------------------------------------

RenameFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:TargetName)

LOC:lLoopIndex       LONG,AUTO
LOC:cstPath          CSTRING(260),AUTO
LOC:cstDrive         CSTRING(260),AUTO
LOC:cstDir           CSTRING(260),AUTO
LOC:cstName          CSTRING(260),AUTO
LOC:cstExt           CSTRING(260),AUTO
ListDir              QUEUE(FILE:Queue),PRE(LD)
                     END
LOC:cstTempName      CSTRING(260),AUTO
LOC:stTempSourceName STRING(260),AUTO
LOC:stTempTargetName STRING(260),AUTO
LOC:lErrorCode       LONG,AUTO
LOC:lLoopExt         LONG,AUTO
loc:extcount         LONG,AUTO
loc:Result           CSTRING(34),AUTO

  CODE
  LOC:cstPath = CLIP(PAR:SourceFile{PROP:Name})
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOC:cstTempName = LOC:cstDrive & LOC:cstDir & LOC:cstName
  LOC:stTempSourceName = LOC:cstDrive & LOC:cstDir
  LOC:cstPath = CLIP(PAR:TargetName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    RemoveFiles(PAR:SourceFile,LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1])
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOC:stTempTargetName = LOC:cstDrive & LOC:cstDir & LOC:cstName
  PrepareExtentionQueue(PAR:SourceFile)
  LOOP LOC:lLoopExt = 1 TO RECORDS(ListExt)
    GET(ListExt,LOC:lLoopExt)
    FREE(ListDir)
    DIRECTORY(ListDir,LOC:cstTempName & ListExt.Extention,ff_:ARCHIVE)
    LOOP LOC:lLoopIndex = RECORDS(ListDir) TO 1 BY -1
      GET(ListDir,LOC:lLoopIndex)
      IF CLIP(LD:ShortName) = '..' OR CLIP(LD:ShortName) = '.' THEN CYCLE.
      LOC:cstPath = CLIP(LOC:stTempSourceName) & CLIP(LD:Name)
      Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
      RENAME(CLIP(LOC:stTempSourceName) & LD:Name,CLIP(LOC:stTempTargetName) & LOC:cstExt)
      !Look for and rename extensions
      Loop loc:extcount = 1 to 255
          LtoA(loc:extcount,loc:Result,16)
          If Len(loc:Result) = 1
              loc:Result = '0' & loc:Result
          End ! If Len(loc:Result) = 1
          loc:Result = UPPER(loc:Result)

          If Exists(CLIP(LOC:stTempSourceName) & Clip(loc:cstName) & '.^' & loc:Result)
              Rename(CLIP(LOC:stTempSourceName) & Clip(loc:cstName) & '.^' & loc:Result,Clip(loc:stTempTargetName) & '.^' & loc:Result)
          Else
              Break
          End ! If Exists(CLip(loc:stTempSourceName) & '.^' & loc:Result)
      End ! Loop loc:extcount = 1 to 255

      IF ERRORCODE() 
        LOC:lErrorCode = ERRORCODE()
        FREE(ListExt)
        FREE(ListDir)
        DC:SetError(LOC:lErrorCode)
        RETURN
      END
    END
  END
  FREE(ListExt)
  FREE(ListDir)
  RETURN
!---------------------------------------------------------------------

PrepareExtentionQueue PROCEDURE(FILE PAR:ProcessFileLabel)

LOC:lLoopIndex LONG,AUTO
LOC:cstPath    CSTRING(260),AUTO
LOC:cstDrive   CSTRING(260),AUTO
LOC:cstDir     CSTRING(260),AUTO
LOC:cstName    CSTRING(260),AUTO
LOC:cstExt     CSTRING(260),AUTO

  CODE
  LOC:cstPath = CLIP(PAR:ProcessFileLabel{PROP:Name})
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  FREE(ListExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF ~LOC:cstExt
      ListExt.Extention = '.tps'
    ELSE
      ListExt.Extention = LOC:cstExt
    END
    ADD(ListExt)
  ELSE
    IF ~LOC:cstExt
      CASE UPPER(SUB(PAR:ProcessFileLabel{PROP:Driver},1,3))
      OF   'CLA'
      OROF   'BTR'
        ListExt.Extention = '.dat'
      OF   'TOP'
        ListExt.Extention = '.tps'
      OF   'DBA'
      OROF 'CLI'
      OROF 'FOX'
        ListExt.Extention = '.dbf'
      END
    ELSE
      ListExt.Extention = LOC:cstExt
    END
    ADD(ListExt)
    CASE UPPER(SUB(PAR:ProcessFileLabel{PROP:Driver},1,3))
    OF 'CLA'
      ListExt.Extention = '.mem'
      ADD(ListExt)
      LOOP LOC:lLoopIndex = 1 TO 99
        ListExt.Extention = '.k' & FORMAT(LOC:lLoopIndex,@N02)
        ADD(ListExt)
        ListExt.Extention = '.i' & FORMAT(LOC:lLoopIndex,@N02)
        ADD(ListExt)
      END
    OF 'BTR'
      ListExt.Extention = '.dbt'
      ADD(ListExt)
      ListExt.Extention = '.ndx'
      ADD(ListExt)
    OF 'DBA'
      ListExt.Extention = '.ndx'
      ADD(ListExt)
      ListExt.Extention = '.mdx'
      ADD(ListExt)
      ListExt.Extention = '.dbt'
      ADD(ListExt)
    OF 'CLI'
      ListExt.Extention = '.ntx'
      ADD(ListExt)
      ListExt.Extention = '.cdx'
      ADD(ListExt)
      ListExt.Extention = '.dbt'
      ADD(ListExt)
    OF 'FOX'
      ListExt.Extention = '.inx'
      ADD(ListExt)
      ListExt.Extention = '.cdx'
      ADD(ListExt)
      ListExt.Extention = '.fbt'
      ADD(ListExt)
    END
  END
  RETURN
!---------------------------------------------------------------------

PrepareMultiTableFileName PROCEDURE(FILE PAR:File,STRING PAR:SourceName,*STRING PAR:TargetName)

LOC:cstPath      CSTRING(260),AUTO
LOC:cstDrive     CSTRING(260),AUTO
LOC:cstDir       CSTRING(260),AUTO
LOC:cstName      CSTRING(260),AUTO
LOC:cstExt       CSTRING(260),AUTO
LOC:byMultyTable BYTE(False)

  CODE
  CLEAR(PAR:TargetName)
  LOC:cstPath = CLIP(PAR:SourceName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF UPPER(PAR:File{PROP:Driver}) = 'TOPSPEED' AND LOC:cstName[1] = '!'
    PAR:TargetName = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF ~LOC:cstExt
      PAR:TargetName = CLIP(PAR:TargetName) & '.tps'
    END
    LOC:byMultyTable = True
  END
  RETURN(LOC:byMultyTable)
!---------------------------------------------------------------------

PrepareDestFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:SourceName,FILE PAR:TargetFile,STRING PAR:TargetName,STRING PAR:OriginalTargetName)

LOC:stTempSourceFile STRING(260),AUTO
LOC:stTempTargetFile STRING(260),AUTO
LOC:byMultyTable     BYTE(False)

  CODE
  IF PrepareMultiTableFileName(PAR:SourceFile,PAR:SourceName,LOC:stTempSourceFile)
    IF PrepareMultiTableFileName(PAR:TargetFile,PAR:TargetName,LOC:stTempTargetFile)
      SETCURSOR(CURSOR:Wait)
      COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
      SETCURSOR
      IF ERRORCODE()
        DC:SetErrorFile(PAR:SourceFile)
        RETURN(1)
      END
      PAR:SourceFile{PROP:Name} = PAR:TargetName
      REMOVE(PAR:SourceFile)
      IF ERRORCODE()
        DC:SetErrorFile(PAR:SourceFile)
        RETURN(1)
      END
      PAR:SourceFile{PROP:Name} = PAR:SourceName
      LOC:byMultyTable = True
    END
  ELSE
    IF PrepareMultiTableFileName(PAR:TargetFile,PAR:TargetName,LOC:stTempTargetFile)
      IF PrepareMultiTableFileName(PAR:TargetFile,PAR:OriginalTargetName,LOC:stTempSourceFile)
        IF EXISTS(LOC:stTempSourceFile)
          SETCURSOR(CURSOR:Wait)
          COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
          SETCURSOR
          IF ERRORCODE()
            DC:SetErrorFile(PAR:SourceFile)
            RETURN(1)
          END
        END
        LOC:byMultyTable = True
      END
    END
  END
  IF LOC:byMultyTable = False
    RemoveFiles(PAR:TargetFile,PAR:TargetName)
  END
  RETURN(0)
!---------------------------------------------------------------------

RenameDestFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:TargetName,STRING PAR:OriginalTargetName)

LOC:stTempSourceFile STRING(260),AUTO
LOC:stTempTargetFile STRING(260),AUTO
LOC:byMultyTable     BYTE(False)
ROU:lErrorCode       LONG,AUTO

  CODE
  IF ~PrepareMultiTableFileName(PAR:SourceFile,PAR:OriginalTargetName,LOC:stTempSourceFile)
    IF PrepareMultiTableFileName(PAR:SourceFile,PAR:TargetName,LOC:stTempTargetFile)
      IF PrepareMultiTableFileName(PAR:SourceFile,PAR:SourceFile{PROP:Name},LOC:stTempSourceFile)
        SETCURSOR(CURSOR:Wait)
        COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
        SETCURSOR
        IF ERRORCODE()
          DC:SetErrorFile(PAR:SourceFile)
          RETURN(1)
        END
        REMOVE(PAR:SourceFile)
        IF ERRORCODE()
          ROU:lErrorCode = ERRORCODE()
          RemoveFiles(PAR:SourceFile,LOC:stTempTargetFile)
          DC:SetError(ROU:lErrorCode)
          DC:SetErrorFile(PAR:SourceFile)
          RETURN(1)
        END
        LOC:byMultyTable = True
      END
    END
  END
  IF LOC:byMultyTable = False
    RenameFile(PAR:SourceFile,PAR:TargetName)
    IF ERRORCODE()
      RETURN(1)
    END
  END
  RETURN(0)
!---------------------------------------------------------------------

Converting FUNCTION(LONG Version)

LocalRequest         LONG
OriginalRequest      LONG
LocalResponse        LONG
FilesOpened          LONG
WindowOpened         LONG
WindowInitialized    LONG
ForceRefresh         LONG
ReturnCode           BYTE
LOC:byTPSDriver         BYTE,AUTO
LOC:byDestSQLDriver     BYTE,AUTO
LOC:byDestMSSQLDriver   BYTE,AUTO
LOC:byDestASADriver     BYTE,AUTO
LOC:bySourceSQLDriver   BYTE,AUTO
LOC:bySourceMSSQLDriver BYTE,AUTO
LOC:bySourceASADriver   BYTE,AUTO
LOC:byTransactionActive BYTE,AUTO
LOC:rfSourceFileLabel   &FILE
LOC:stSourceFileName    STRING(260),AUTO
LOC:rfDestFileLabel     &FILE
LOC:stDestFileName      STRING(260),AUTO
LOC:stTargetFileName    STRING(260),AUTO
RecordProcessed      LONG
RecordTotal          LONG
InfoString           STRING(50)
DummyFileName1       STRING(260)
DummyFileName2       STRING(260)
SaveRecordProcessed  LONG
SavePercent          LONG
SaveFillBar          LONG

SPBBorderControl     SIGNED
SPBFillControl       SIGNED
SPBPercentControl    SIGNED

window               WINDOW('File Conversion'),AT(,,224,52),FONT('MS Sans Serif',8,,),CENTER,GRAY
                       PANEL,AT(4,4,216,44),USE(?Panel1),BEVEL(-1)
                       PROGRESS,USE(RecordProcessed),AT(12,12,200,12),RANGE(0,100)
                       STRING(@s50),AT(12,32,200,),USE(InfoString),CENTER,FONT(,,COLOR:Navy,,CHARSET:ANSI)
                     END
  CODE
  PUSHBIND
  SETCURSOR
  ReturnCode = 1
  LocalRequest = GlobalRequest
  OriginalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  ForceRefresh = False
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  IF KEYCODE() = MouseRight
    SETKEYCODE(0)
  END
  DO PrepareProcedure
  ACCEPT
    CASE EVENT()
    OF EVENT:OpenWindow
      IF NOT WindowInitialized
        DO InitializeWindow
        WindowInitialized = True
      END
      SELECT(?Panel1)
    OF EVENT:GainFocus
      ForceRefresh = True
      IF NOT WindowInitialized
        DO InitializeWindow
        WindowInitialized = True
      ELSE
        DO RefreshWindow
      END
    OF Event:Rejected
      BEEP
      DISPLAY(?)
      SELECT(?)
    END
  END
  DO ProcedureReturn
!---------------------------------------------------------------------------
PrepareProcedure ROUTINE
  FilesOpened = True
  OPEN(window)
  WindowOpened=True
  window{PROP:Text} = CLIP(window{PROP:Text}) & ' - ' & MOD:stFileName
  DO ConversionProcess
  DO ProcedureReturn
!---------------------------------------------------------------------------
ProcedureReturn ROUTINE
!|
!| This routine provides a common procedure exit point for all template
!| generated procedures.
!|
!| First, all of the files opened by this procedure are closed.
!|
!| Next, if it was opened by this procedure, the window is closed.
!|
!| Next, GlobalResponse is assigned a value to signal the calling procedure
!| what happened in this procedure.
!|
!| Next, we replace the BINDings that were in place when the procedure initialized
!| (and saved with PUSHBIND) using POPBIND.
!|
!| Finally, we return to the calling procedure, passing ReturnCode back.
!|
  IF FilesOpened
  END
  IF WindowOpened
    CLOSE(window)
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  POPBIND
  RETURN(ReturnCode)
!---------------------------------------------------------------------------
InitializeWindow ROUTINE
!|
!| This routine is used to prepare any control templates for use. It should be called once
!| per procedure.
!|
  DO RefreshWindow
!---------------------------------------------------------------------------
RefreshWindow ROUTINE
!|
!| This routine is used to keep all displays and control templates current.
!|
  IF window{Prop:AcceptAll} THEN EXIT.
  DISPLAY()
  ForceRefresh = False
!---------------------------------------------------------------------------
SyncWindow ROUTINE
!|
!| This routine is used to insure that any records pointed to in control
!| templates are fetched before any procedures are called via buttons or menu
!| options.
!|
!---------------------------------------------------------------------------

ConversionProcess ROUTINE
  DATA
ROU:fCurrentFile      &FILE
ROU:lConvertedRecNum  LONG,AUTO
ROU:lRecordsThisCycle LONG,AUTO
ROU:lDim1             LONG,AUTO
ROU:lDim2             LONG,AUTO
ROU:lDim3             LONG,AUTO
ROU:lDim4             LONG,AUTO
ROU:byError           BYTE,AUTO
  CODE
  GET(FileQueue,Version)
  LOC:rfSourceFileLabel &= FileQueue.FileLabel
  LOC:stSourceFileName   = FileQueue.FileName
  GET(FileQueue,RECORDS(FileQueue))
  LOC:rfDestFileLabel &= FileQueue.FileLabel
  LOC:stDestFileName   = FileQueue.FileName
  LOC:stTargetFileName = LOC:stDestFileName
  LOC:byTransactionActive = False
  LOC:byTPSDriver         = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'TOPSPEED',True,False)
  LOC:bySourceMSSQLDriver = CHOOSE(UPPER(LOC:rfSourceFileLabel{PROP:Driver}) = 'MSSQL',True,False)
  LOC:bySourceASADriver   = CHOOSE(UPPER(LOC:rfSourceFileLabel{PROP:Driver}) = 'SQLANYWHERE',True,False)
  LOC:bySourceSQLDriver   = LOC:bySourceMSSQLDriver + LOC:bySourceASADriver
  LOC:byDestMSSQLDriver   = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'MSSQL',True,False)
  LOC:byDestASADriver     = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'SQLANYWHERE',True,False)
  LOC:byDestSQLDriver     = LOC:byDestMSSQLDriver + LOC:byDestASADriver
  CreateTempName(LOC:stDestFileName,'tmp',LOC:rfDestFileLabel)
  IF LOC:byDestSQLDriver
    DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
  ELSE
    IF PrepareMultiTableFileName(LOC:rfDestFileLabel,LOC:stDestFileName,DummyFileName1) AND |
       (PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:stSourceFileName,DummyFileName1) OR |
        (PrepareMultiTableFileName(LOC:rfDestFileLabel,LOC:stTargetFileName,DummyFileName2) AND EXISTS(DummyFileName2)) |
       )
      InfoString = 'File copying...'
      DISPLAY
    END
    IF PrepareDestFile(LOC:rfSourceFileLabel,LOC:stSourceFileName,LOC:rfDestFileLabel,LOC:stDestFileName,LOC:stTargetFileName) THEN DO PreReturn; EXIT.
  END
  OPEN(LOC:rfSourceFileLabel,12h)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
  CREATE(LOC:rfDestFileLabel)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
  OPEN(LOC:rfDestFileLabel,12h)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  IF LOC:byTPSDriver 
    ROU:lConvertedRecNum = 0
  ELSE
    IF ~LOC:byDestSQLDriver THEN STREAM(LOC:rfDestFileLabel).
  END
  ?RecordProcessed{PROP:Use} = RecordProcessed
  ?InfoString{PROP:Use} = InfoString
  DO InitalizeSolidProgressBar
  ?RecordProcessed{PROP:RangeHigh} = RECORDS(LOC:rfSourceFileLabel)
  RecordProcessed = 0
  RecordTotal     = RECORDS(LOC:rfSourceFileLabel)
  SaveRecordProcessed = 0
  SavePercent         = 0
  SaveFillBar         = 0
  SETCURSOR(CURSOR:Wait)
  DISPLAY
  SET(LOC:rfSourceFileLabel)
  IF LOC:byTPSDriver
    LOGOUT(1,LOC:rfDestFileLabel)
    IF ERRORCODE() THEN DO PreReturn; EXIT.
    LOC:byTransactionActive = True
  END
  ROU:byError = 0
  window{PROP:Timer} = 1
  ACCEPT
    CASE EVENT()
    OF EVENT:GainFocus
      DISPLAY
    OF EVENT:CloseWindow
      CYCLE
    OF EVENT:Completed
      BREAK
    OF EVENT:Timer
      ROU:lRecordsThisCycle = 0
      LOOP WHILE ROU:lRecordsThisCycle < 25
        NEXT(LOC:rfSourceFileLabel)
        IF ERRORCODE()
          POST(EVENT:Completed)
          BREAK
        END
        ROU:fCurrentFile &= LOC:rfSourceFileLabel
        IF ROU:fCurrentFile &= LOCDEFSVer1
          ! Field "AlertDays" added
          ! Field "LastImportData" added
          CLEAR(LOCDEFSVer2:RECORD)
          LOCDEFSVer2:RECORD :=: LOCDEFSVer1:RECORD
        END
        IF ROU:fCurrentFile &= IMEISHIPVer1
          ! Field "BER" added
          CLEAR(IMEISHIPVer2:RECORD)
          IMEISHIPVer2:RECORD :=: IMEISHIPVer1:RECORD
          ROU:fCurrentFile &= IMEISHIPVer2
        END
        IF ROU:fCurrentFile &= IMEISHIPVer2
          ! Field "ManualEntry" added
          CLEAR(IMEISHIPVer3:RECORD)
          IMEISHIPVer3:RECORD :=: IMEISHIPVer2:RECORD
          ROU:fCurrentFile &= IMEISHIPVer3
        END
        IF ROU:fCurrentFile &= IMEISHIPVer3
          ! Field "ProductCode" added
          CLEAR(IMEISHIPVer4:RECORD)
          IMEISHIPVer4:RECORD :=: IMEISHIPVer3:RECORD
        END
        IF ROU:fCurrentFile &= LOCDEFSVer1
          IF ~LOCDEFSVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= IMEISHIPVer3
          IF ~IMEISHIPVer4:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF LOC:byTPSDriver
          ADD(LOC:rfDestFileLabel)
        ELSE
          APPEND(LOC:rfDestFileLabel)
        END
        IF ERRORCODE()
          DO PreReturn
          ROU:byError = 1
          POST(EVENT:Completed)
          BREAK
        END
        IF LOC:byTPSDriver
          ROU:lConvertedRecNum += 1
          IF ROU:lConvertedRecNum % 1000 = 0
            COMMIT
            IF ERRORCODE()
              DO PreReturn
              ROU:byError = 1
              POST(EVENT:Completed)
              BREAK
            END
            LOC:byTransactionActive = False
            LOGOUT(1,LOC:rfDestFileLabel)
            IF ERRORCODE()
              DO PreReturn
              ROU:byError = 1
              POST(EVENT:Completed)
              BREAK
            END
            LOC:byTransactionActive = True
          END
        END
        RecordProcessed += 1
        DO DisplaySolidProgressBar
        InfoString = 'Records processed ' & RecordProcessed & ' from ' & RecordTotal
        DISPLAY
        ROU:lRecordsThisCycle += 1
      END
    END
  END
  IF ROU:byError = 0
    IF LOC:byTPSDriver
      COMMIT
      IF ERRORCODE() THEN DO PreReturn; EXIT.
      LOC:byTransactionActive = False
    ELSE
      InfoString = 'Now rebuild keys...'
      DISPLAY
      IF ~LOC:byDestSQLDriver THEN FLUSH(LOC:rfDestFileLabel).
      BUILD(LOC:rfDestFileLabel)
    END

    ReturnCode = 0
    LocalResponse = RequestCompleted
    DO PreReturn
  END
  EXIT
!----------------------------------------------------------

PreReturn ROUTINE
  DATA
ROU:lErrorCode        LONG,AUTO
ROU:stTempFile        STRING(260),AUTO
  CODE
  SETCURSOR
  IF ReturnCode
    IF LOC:byTPSDriver AND LOC:byTransactionActive
      ROU:lErrorCode = ERRORCODE()
      ROLLBACK
      DC:SetError(ROU:lErrorCode)
      DC:SetErrorFile(LOC:rfSourceFileLabel)
    END
    IF ~LOC:byDestSQLDriver AND ~LOC:byTPSDriver THEN FLUSH(LOC:rfDestFileLabel).
    FileErrorBox()
  END
  CLOSE(LOC:rfSourceFileLabel)
  CLOSE(LOC:rfDestFileLabel)
  IF ReturnCode
    DO RemoveTempFile
  ELSE
    IF LOC:bySourceSQLDriver
      ROU:stTempFile = LOC:stSourceFileName
      CreateTempName(ROU:stTempFile,FORMAT(Version,@N03),LOC:rfSourceFileLabel)
      DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    ELSE
      ROU:stTempFile = LOC:stSourceFileName
      CreateTempName(ROU:stTempFile,FORMAT(Version,@N03),LOC:rfSourceFileLabel)
      RemoveFiles(LOC:rfSourceFileLabel,ROU:stTempFile)
      IF ~PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:stTargetFileName,DummyFileName1)
        IF PrepareMultiTableFileName(LOC:rfSourceFileLabel,ROU:stTempFile,DummyFileName1)
          IF PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:rfSourceFileLabel{PROP:Name},DummyFileName1)
            InfoString = 'File copying...'
            DISPLAY
          END
        END
      END
      IF RenameDestFile(LOC:rfSourceFileLabel,ROU:stTempFile,LOC:stTargetFileName)
        ROU:lErrorCode = ERRORCODE()
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    END
    IF LOC:byDestSQLDriver
      LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
      DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        IF LOC:bySourceSQLDriver
          DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
        ELSE
          LOC:rfSourceFileLabel{PROP:Name} = ROU:stTempFile
          RenameFile(LOC:rfSourceFileLabel,LOC:stSourceFileName)
        END
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    ELSE
      LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
      RemoveFiles(LOC:rfDestFileLabel,LOC:stTargetFileName)
      RenameFile(LOC:rfDestFileLabel,LOC:stTargetFileName)
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        IF LOC:bySourceSQLDriver
          DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
        ELSE
          LOC:rfSourceFileLabel{PROP:Name} = ROU:stTempFile
          RenameFile(LOC:rfSourceFileLabel,LOC:stSourceFileName)
        END
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    END
    IF LOC:bySourceSQLDriver
      DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
    ELSE
      RemoveFiles(LOC:rfSourceFileLabel,ROU:stTempFile)
    END
    IF LOC:byDestSQLDriver
      DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
    ELSE
      RemoveFiles(LOC:rfDestFileLabel,LOC:stDestFileName)
    END
  END
  EXIT
!----------------------------------------------------------

RemoveTempFile ROUTINE
  IF LOC:byDestSQLDriver
    DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
  ELSE
    LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
    REMOVE(LOC:rfDestFileLabel)
  END
  EXIT
!----------------------------------------------------------

InitalizeSolidProgressBar ROUTINE
  DATA
ROU:lPercentControlWidth   LONG
ROU:lPercentControlHeight  LONG
ROU:lProgressControlHeight LONG
ROU:byOldPropPixels        BYTE
  CODE
  ROU:byOldPropPixels = window{PROP:Pixels}
  window{PROP:Buffer} = True
  window{PROP:Pixels} = True
  ?RecordProcessed{PROP:Hide} = True

  SPBBorderControl = CREATE(0,CREATE:Panel,?RecordProcessed{PROP:Parent})
  SPBBorderControl{PROP:Xpos}   = ?RecordProcessed{PROP:Xpos}
  SPBBorderControl{PROP:Ypos}   = ?RecordProcessed{PROP:Ypos}
  SPBBorderControl{PROP:Width}  = ?RecordProcessed{PROP:Width}
  SPBBorderControl{PROP:Height} = ?RecordProcessed{PROP:Height}
  IF ?RecordProcessed{PROP:Background} <> 0FFFFFFFFH
    SPBBorderControl{PROP:Fill} = ?RecordProcessed{PROP:Background}
  END
  SPBBorderControl{PROP:BevelOuter} = -1
  SPBBorderControl{PROP:Hide} = False

  SPBFillControl = CREATE(0,CREATE:Box,?RecordProcessed{PROP:Parent})
  SPBFillControl{PROP:Xpos}   = ?RecordProcessed{PROP:Xpos} + 1
  SPBFillControl{PROP:Ypos}   = ?RecordProcessed{PROP:Ypos} + 1
  SPBFillControl{PROP:Width}  = 0
  SPBFillControl{PROP:Height} = ?RecordProcessed{PROP:Height} - 1
  IF ?RecordProcessed{PROP:SelectedFillColor} <> 0FFFFFFFFH
    SPBFillControl{PROP:Fill} = ?RecordProcessed{PROP:SelectedFillColor}
  ELSE
    SPBFillControl{PROP:Fill} = 08000000DH
  END
  SPBFillControl{PROP:Hide} = False

  SPBPercentControl = CREATE(0,CREATE:String,?RecordProcessed{PROP:Parent})
  SPBPercentControl{PROP:FontStyle} = FONT:bold
  SPBPercentControl{PROP:FontSize}  = ?RecordProcessed{PROP:Height}
  LOOP
    ROU:lPercentControlHeight  = SPBPercentControl{PROP:Height}
    ROU:lProgressControlHeight = ?RecordProcessed{PROP:Height}
    IF ROU:lPercentControlHeight <= ROU:lProgressControlHeight OR SPBPercentControl{PROP:FontSize} = 1
      BREAK
    END
    IF SPBPercentControl{PROP:FontSize} < 8
      SPBPercentControl{PROP:FontName} = 'Small Fonts'
    END
    SPBPercentControl{PROP:FontSize} = SPBPercentControl{PROP:FontSize} - 1
  END

  SPBPercentControl{PROP:Text}      = '100%'
  ROU:lPercentControlWidth          = SPBPercentControl{PROP:Width}
  SPBPercentControl{PROP:Xpos}      = ?RecordProcessed{PROP:Xpos} + (?RecordProcessed{PROP:Width} - ROU:lPercentControlWidth) / 2
  SPBPercentControl{PROP:Width}     = ROU:lPercentControlWidth
  SPBPercentControl{PROP:Ypos}      = ?RecordProcessed{PROP:Ypos} + (?RecordProcessed{PROP:Height} - SPBPercentControl{PROP:Height}) / 2 + 1
  IF ?RecordProcessed{PROP:SelectedColor} <> 0FFFFFFFFH
    SPBPercentControl{PROP:FontColor} = ?RecordProcessed{PROP:SelectedColor}
  ELSE
    SPBPercentControl{PROP:FontColor} = 0FFFFFFH
  END
  SPBPercentControl{PROP:Trn}       = True
  SPBPercentControl{PROP:Center}    = True
  SPBPercentControl{PROP:Text}      = '0%'
  SPBPercentControl{PROP:Hide}      = False
  window{PROP:Pixels} = ROU:byOldPropPixels
  EXIT
!---------------------------------------------------------------------

DisplaySolidProgressBar ROUTINE
  DATA
ROU:lRangeLow  LONG,AUTO
ROU:lRangeHigh LONG,AUTO
ROU:lPercent   LONG,AUTO
ROU:lFillBar   LONG,AUTO
  CODE
  IF SaveRecordProcessed <> RecordProcessed
    ROU:lRangeLow  = ?RecordProcessed{PROP:RangeLow}
    ROU:lRangeHigh = ?RecordProcessed{PROP:RangeHigh}
    IF RecordProcessed <= ROU:lRangeHigh
      ROU:lFillBar = (RecordProcessed / (ROU:lRangeHigh - ROU:lRangeLow)) * (?RecordProcessed{PROP:Width} - 1)
      IF ROU:lFillBar <> SaveFillBar
        SPBFillControl{PROP:Width} = ROU:lFillBar
        DISPLAY(SPBFillControl)
        SaveFillBar = ROU:lFillBar
      END
      ROU:lPercent = (RecordProcessed / (ROU:lRangeHigh - ROU:lRangeLow)) * 100
      IF ROU:lPercent <> SavePercent
        SPBPercentControl{PROP:Text} = FORMAT(ROU:lPercent,@N3) & '%'
        DISPLAY(SPBPercentControl)
        SavePercent = ROU:lPercent
      END
      SaveRecordProcessed = RecordProcessed
    ELSE
      RecordProcessed = ROU:lRangeHigh
    END
  END
  EXIT
!---------------------------------------------------------------------
