

   MEMBER('autostat.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('AUTOS002.INC'),ONCE        !Local module procedure declarations
                     END


GetStatusAlternative PROCEDURE  (f_number,f_audit,f_type) ! Declare Procedure
tmp:End_Days         LONG
tmp:End_Hours        LONG
tmp:PreviousStatus   STRING(30)
  CODE
    !f_number = status number
    !f_audit    = Write to audit trail? 1 = Do Not


    Case f_type
        Of 'JOB'
            sterror# = 0
            If job:cancelled = 'YES'    
                sterror# = 1
            End!If job:cancelled = 'YES'

!            If sterror# = 0
!                access:status.clearkey(sts:status_key)
!                sts:status  = job:current_status
!                If access:status.tryfetch(sts:status_key) = Level:Benign    
!                    IF sts:ref_number < 500 or sts:ref_number > 600
!                        If job:estimate = 'YES'
!                            If job:estimate_accepted <> 'YES' and job:estimate_rejected <> 'YES'
!                                sterror# = 1
!                            End!If job:estimate_accepted <> 'YES' and job:estimate_rejected <> 'YES'
!                        End!If job:estimate = 'YES'
!                    End!IF sts:ref_number > 500 and sts:ref_number < 600
!                End!If access:status.tryfetch(sts:status_key) = Level:Benign
!            End!If sterror# = 0
                                          
            If sterror# = 0                              
                access:status.clearkey(sts:ref_number_only_key)
                sts:ref_number  = f_number
                IF access:status.tryfetch(sts:ref_number_only_key)  
                    job:current_status  = 'ERROR'
                Else!IF access:status.tryfetch(sts:ref_number_only_key)
                    job:PreviousStatus  = job:current_status           
                    !access:users.clearkey(use:password_key)
                    !use:password    = glo:password
                    !access:users.fetch(use:password_key)
                    !job:statusUser      = use:user_code
                    job:statusUser      = 'WEB'
                    job:current_status  = sts:status

                    If job:PreviousStatus <> job:Current_Status
                        Access:AUDSTATS.Open()
                        Access:AUDSTATS.Usefile()
                        If Access:AUDSTATS.Primerecord() = Level:Benign
                            aus:RefNumber    = job:Ref_Number
                            aus:Type         = 'JOB'
                            aus:DateChanged  = Today()
                            aus:TimeChanged  = Clock()
                            aus:OldStatus    = job:PreviousStatus
                            aus:NewStatus    = job:Current_Status
                            aus:UserCode     = 'WEB' !use:User_Code
                            If Access:AUDSTATS.Tryinsert()
                                Access:AUDSTATS.Cancelautoinc()
                            End!If Access:AUDSTATS.Tryinsert()
                        End!If Access:AUDSTATS.Primerecord() = Level:Benign
                        Access:AUDSTATS.Close()

                        If f_audit <> 1
                            Get(audit,0)
                            If access:audit.primerecord() = Level:Benign            
                                aud:notes   = 'PREVIOUS STATUS: ' & Clip(job:PreviousStatus) & |
                                                '<13,10>NEW STATUS: ' & Clip(job:current_status)
                                aud:ref_number  = job:ref_number
                                aud:date        = Today()
                                aud:time        = Clock()
                                aud:user        = 'WEB' !use:user_code
                                aud:action      = 'STATUS CHANGED TO: ' & CLip(job:current_status)
                                access:audit.tryinsert()
                            End!If access:audit.primerecord() = Level:Benign
                        End!If status_audit# = 1
                    End!If job:PreviousStatus <> job:Current_Status     
                            
                End!IF access:status.tryfetch(sts:ref_number_only_key)

                access:stahead.clearkey(sth:ref_number_key)
                sth:ref_number  = sts:heading_ref_number
                If access:stahead.tryfetch(sth:ref_number_key) = Level:Benign
                    ! Start Change 4145 BE(21/04/04)
                    !access:jobstage.clearkey(jst:job_stage_key)
                    !jst:ref_number  = job:ref_number
                    !jst:job_stage   = sth:heading
                    !If access:jobstage.tryfetch(jst:job_stage_key)
                    !    Get(jobstage,0)
                    !    jst:ref_number  = job:ref_number
                    !    jst:job_stage   = sth:heading
                    !    jst:date        = Today()
                    !    jst:time        = Clock()
                    !    jst:user        = use:user_code
                    !    access:jobstage.tryinsert()
                    !End!If access:jobstage.tryfetch(jst:job_stage_key)
                    access:JOBSTAGE.clearkey(jst:job_stage_key)
                    jst:ref_number  = job:ref_number
                    jst:job_stage   = sth:heading
                    ! ideally the jobstage record should be repeated (unless it is
                    ! the same as the most recent entry).  Unfortunately poor database design
                    ! has included the job_stage as part of the primary key and must therefore
                    ! be UNIQUE.  Par for the course !!!!!
                    ! This kluge is second best and updates the date/time of any existing entry -
                    ! thus loosing the historical entry but at least the most recent entry reflects
                    ! curent status.

                    !Ah, if only we were all "experts". Stop whinging and get on with it  (DBH: 23-11-2004)
                    IF (access:JOBSTAGE.fetch(jst:job_stage_key) = Level:Benign) THEN
                        jst:date        = Today()
                        jst:time        = Clock()
                        jst:user        = 'WEB' ! use:user_code
                        access:JOBSTAGE.update()
                    ELSE
                        IF (access:JOBSTAGE.primerecord() = Level:Benign) THEN
                            jst:ref_number  = job:ref_number
                            jst:job_stage   = sth:heading
                            jst:date        = Today()
                            jst:time        = Clock()
                            jst:user        = 'WEB' ! use:user_code
                            IF (access:JOBSTAGE.insert() <> Level:Benign) THEN
                                access:JOBSTAGE.cancelautoinc()
                            END
                        END
                    END
                    ! End Change 4145 BE(21/04/04)
                End!If access:stahead.tryfetch(sth:ref_number_key) = Level:Benign
                If sts:use_turnaround_time  = 'YES'
                    tmp:End_Days = Today()
                    x# = 0
                    count# = 0
                    If sts:Turnaround_Days <> 0
                        Loop
                            count# += 1
                            day_number# = (Today() + count#) % 7
                            If def:include_saturday <> 'YES'
                                If day_number# = 6
                                    tmp:end_days += 1
                                    Cycle
                                End
                            End
                            If def:include_sunday <> 'YES'
                                If day_number# = 0
                                    tmp:end_days += 1
                                    Cycle
                                End
                            End
                            tmp:end_days += 1
                            x# += 1
                            If x# >= sts:Turnaround_Days
                                Break
                            End!If x# >= sts:turnaround_days
                        End!Loop
                    End!If f_days <> ''

?                    Message('tmp:End_Days: ' & Format(tmp:End_Days,@d6),'1')
? days# = 0
                    tmp:End_Hours = Clock()
                    If sts:Turnaround_Hours <> 0
                        start_time_temp# = Clock()
                        new_day# = 0
                        Loop x# = 1 To sts:Turnaround_Hours
                            tmp:End_Hours += 360000
                            If def:Start_Work_Hours <> '' And def:End_Work_Hours <> ''
                                If tmp:End_Hours > def:End_Work_Hours or tmp:End_Hours < def:Start_Work_Hours
                                    tmp:End_Hours = def:Start_Work_Hours
                                    tmp:End_Days += 1
                                End !If tmp:End_Hours > def:End_Work_Hours
                            End !If def:Start_Work_Hours <> '' And def:End_Work_Hours <> ''



!                            clock_temp# = start_time_temp# + (x# * 360000)
!                            If def:start_work_hours <> '' And def:end_work_hours <> ''
!                                If clock_temp# < def:start_work_hours Or clock_temp# > def:end_work_hours
!                                    tmp:end_hours = def:start_work_hours
!                                    start_time_temp# = def:start_work_hours
!                                    tmp:end_days += 1
!? days# += 1
!                                End!If clock_temp > def:start_work_hours And clock_temp < def:end_work_hours
!                            End!If def:start_work_hours <> '' And def:end_work_hours <> ''
!                            tmp:end_hours += 360000
?   Message('tmp:End_Hours: ' & Format(tmp:End_Hours,@t1),'Debug Message', Icon:Hand)
                        End!Loop x# = 1 To Abs(sts:turnaround_hours/6000)
                    End!If f_hours <> ''

                    job:Status_End_Time = tmp:End_Hours
                    job:status_end_date = tmp:End_Days
?                    Message('tmp:End_Days: ' & Format(tmp:End_Days,@d6),'2')
?                    Message(Format(job:status_end_time,@t1))
?   Message('days#: ' & days#,'Debug Message', Icon:Hand)
                Else!If sts:use_turnaround_time = 'YES'
                    job:status_end_date = Deformat('1/1/2050',@d6)
                    job:status_end_Time = Clock()           
                End!If sts:use_turnaround_time  = 'YES'
            End!If sterror# = 0
        Of 'EXC'
            
            tmp:PreviousStatus    = job:exchange_status
            access:status.clearkey(sts:ref_number_only_key)
            sts:ref_number    = f_number
            IF access:status.tryfetch(sts:ref_number_only_key)    
                job:exchange_status    = 'ERROR'
            Else!IF access:status.tryfetch(sts:ref_number_only_key)
                job:exchange_status    = sts:status
                If tmp:PreviousStatus <> job:exchange_Status
                    Access:AUDSTATS.Open()
                    Access:AUDSTATS.Usefile()
                    If Access:AUDSTATS.Primerecord() = Level:Benign
                        aus:RefNumber    = job:Ref_Number
                        aus:Type         = 'EXC'
                        aus:DateChanged  = Today()
                        aus:TimeChanged  = Clock()
                        aus:OldStatus    = tmp:PreviousStatus
                        aus:NewStatus    = job:Exchange_Status
                        aus:UserCode     = 'WEB' ! use:User_Code
                        If Access:AUDSTATS.Tryinsert()
                            Access:AUDSTATS.Cancelautoinc()
                        End!If Access:AUDSTATS.Tryinsert()
                    End!If Access:AUDSTATS.Primerecord() = Level:Benign
                    Access:AUDSTATS.Close()

                    If f_audit <> 1
                        Get(audit,0)
                        If access:audit.primerecord() = Level:Benign            
                            aud:notes    = 'PREVIOUS STATUS: ' & Clip(tmp:PreviousStatus) & |
                                            '<13,10>NEW STATUS: ' & Clip(job:exchange_status)
                            aud:ref_number    = job:ref_number
                            aud:date        = Today()
                            aud:time        = Clock()
                            aud:user        = 'WEB' ! use:user_code
                            aud:type        = 'EXC'
                            aud:action        = 'STATUS (EXCHANGE) CHANGED TO: ' & CLip(job:exchange_status)
                            access:audit.tryinsert()
                        End!If access:audit.primerecord() = Level:Benign
                    End!If status_audit# = 1
                End!If job:PreviousStatus <> job:Current_Status        
                        
            End!IF access:status.tryfetch(sts:ref_number_only_key)
    
    

        Of 'LOA'
            tmp:PreviousStatus    = job:loan_status
            access:status.clearkey(sts:ref_number_only_key)
            sts:ref_number    = f_number
            IF access:status.tryfetch(sts:ref_number_only_key)    
                job:loan_status    = 'ERROR'
            Else!IF access:status.tryfetch(sts:ref_number_only_key)
                job:loan_status    = sts:status
                If tmp:PreviousStatus <> job:loan_status
                    Access:AUDSTATS.Open()
                    Access:AUDSTATS.Usefile()
                    If Access:AUDSTATS.Primerecord() = Level:Benign
                        aus:RefNumber    = job:Ref_Number
                        aus:Type         = 'LOA'
                        aus:DateChanged  = Today()
                        aus:TimeChanged  = Clock()
                        aus:OldStatus    = tmp:PreviousStatus
                        aus:NewStatus    = job:Loan_Status
                        aus:UserCode     = 'WEB' ! use:User_Code
                        If Access:AUDSTATS.Tryinsert()
                            Access:AUDSTATS.Cancelautoinc()
                        End!If Access:AUDSTATS.Tryinsert()
                    End!If Access:AUDSTATS.Primerecord() = Level:Benign
                    Access:AUDSTATS.Close()

                    If f_audit <> 1
                        Get(audit,0)
                        If access:audit.primerecord() = Level:Benign            
                            aud:notes    = 'PREVIOUS STATUS: ' & Clip(tmp:PreviousStatus) & |
                                            '<13,10>NEW STATUS: ' & Clip(job:loan_status)
                            aud:ref_number    = job:ref_number
                            aud:date        = Today()
                            aud:time        = Clock()
                            aud:user        = 'WEB' ! use:user_code
                            aud:type        = 'LOA'
                            aud:action        = 'STATUS (LOAN) CHANGED TO: ' & CLip(job:loan_status)
                            access:audit.tryinsert()
                        End!If access:audit.primerecord() = Level:Benign
                    End!If status_audit# = 1
                End!If job:PreviousStatus <> job:Current_Status        
                        
            End!IF access:status.tryfetch(sts:ref_number_only_key)
    
    


    End!Case f_type

