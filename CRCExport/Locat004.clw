

   MEMBER('locaterimport.clw')                        ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOCAT004.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

Local                CLASS
ImportJobs           Procedure(String func:FileName,String func:ShortName)
                     END
tmp:ImportFile       CSTRING(255),STATIC
ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
tmp:LocaterToSend    CSTRING(255)
tmp:LocaterSent      CSTRING(255)
window               WINDOW('Wilfred To SB'),AT(,,69,15),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       PROMPT('FTP In Progress...'),AT(4,0),USE(?Prompt1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Delete --- Changed (DBH: 12/02/2009) #10672
!! Changing (DBH 17/01/2008) # 9698 - New file format
!!ImportFile      File,Driver('ASCII'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
!!Record                  Record
!!IMEINumber              String(15)
!!ShipDate                String(10)
!!TheRest                 String(255)
!!                        End
!!                    End
!! to (DBH 17/01/2008) # 9698
!ImportFile      File,Driver('ASCII'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
!Record              Record
!Field1                  String(2)
!IMEINumber              String(15)
!Field3                  String(132)
!ShipDate                String(10)
!                    End ! Record              Record
!                End ! ImportFile      File,Driver('BASIC'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
!! End (DBH 17/01/2008) #9698
!
! End --- (DBH: 12/02/2009) #10672
! Insert --- New format (DBH: 12/02/2009) #10672
ImportFile      File,Driver('ASCII'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record              Record
Field1                  String(2)
IMEINumber              String(15)
SIM                     String(20)
Tariff                  String(30)
ProductCode             String(20)
StU                     String(20)
Qty                     String(7)
SONumber                String(10)
CustomerDeliveryRef     String(15)
CustomerCode            String(10)
ShipDate                String(10)
                    End
                End
! End --- (DBH: 12/02/2009) #10672
! Dummy SQL table
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD
SQLRefNumber                Long, Name('Ref_Number')
                        END
                    END
TempFiles   Queue(file:Queue),pre(fil)
            End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GLODC:ConversionResult = DC:FilesConverter()
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  OPEN(window)
  SELF.Opened=True
  Free(FileQueue)
  Clear(FileQueue)
  
  ErrorLog('')
  ErrorLog('LOCATERIMPORT Started===========================')
  
  !Start - Create folders  (DBH: 23-11-2004)
  tmp:LocaterToSend = Clip(Path()) & '\LocaterDownloaded'
  If ~Exists(tmp:LocaterToSend)
      If MkDir(tmp:LocaterToSend)
  
      End !If MkDir(local:WilfredToSend)
  End !If ~Exists(local:WilfredToSend)
  
  tmp:LocaterSent = Clip(Path()) & '\LocaterImported'
  If ~Exists(tmp:LocaterSent)
      If MkDir(tmp:LocaterSent)
  
      End !If MkDir(Clip(Path()) & '\WilfredSent')
  End !If ~Exists(Clip(Path()) & '\WilfredSent')
  !End   - Create folders  (DBH: 23-11-2004)
  
  Loop Until FTP() = 1
  
  End ! Until FTP() = 1
  
  ErrorLog('FTP process: Completed')
  
  ! Changing (DBH 17/01/2008) # 9698 - Filename now ends in "dat"
  !Directory(TempFiles,Clip(Path()) & '\LocaterDownloaded\*.txt',ff_:DIRECTORY)
  ! to (DBH 17/01/2008) # 9698
  Directory(TempFiles,Clip(Path()) & '\LocaterDownloaded\*.dat',ff_:DIRECTORY)
  ! End (DBH 17/01/2008) #9698
  Loop x# = 1 To Records(TempFiles)
      Get(TempFiles,x#)
      If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
          Cycle
      Else !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
          ErrorLog('Importing: ' & Clip(fil:Name) & ' - ' & Round(fil:Size/1024,.01) & 'kb')
          Local.ImportJobs(Clip(Path()) & '\LocaterDownloaded\' & CLip(fil:name),Clip(fil:Name))
      End !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
  End !x# = 1 To Records(ScanFiles)
  
  ErrorLog('LOCATERIMPORT Finished==========================')
  
  Post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

Local.ImportJobs        Procedure(String func:FileName, String func:ShortName)
local:UpdateStatus      Byte(0)
local:JobUpdated        Byte(0)
Code
    Relate:IMEISHIP.Open()

    CountNormal# = 0
    CountUpdate# = 0
    tmp:ImportFile = func:FileName
    Open(ImportFile)
    Set(ImportFile,0)
    Loop
        Next(ImportFile)
        If Error()
            Break
        End !If Error()

! Deleting (DBH 17/01/2008) # 9698 - Not needed
!        ! Find the space in the field. Anything before that is the make (DBH: 29-03-2005)
!        Loop x# = 1 To Len(impfil:TheRest)
!            If Sub(impfil:TheRest,x#,1) = ' '
!                impfil:TheRest = Clip(Upper(Sub(impfil:TheRest,1,x# - 1)))
!                Break
!            End ! If Sub(impfil:TheRest,x#,1) = ' '
!        End ! Loop x# = 1 To Len(impfil:TheRest)
! End (DBH 17/01/2008) #9698

! Inserting (DBH 17/01/2008) # 9698 - Skip the first and last lines on import
        If impfil:Field1 = '01' Or impfil:Field1 = '99'
            Cycle
        End ! If impfil:Field1 = '01' Or impfil:Field1 = '99'
        If Clip(impfil:IMEINumber) = ''
            Cycle
        End ! If Clip(impfil:IMEINumber) = ''
! End (DBH 17/01/2008) #9698


        Access:IMEISHIP.Clearkey(imei:IMEIKey)
        imei:IMEINumber = impfil:IMEINumber
        If Access:IMEISHIP.Tryfetch(imei:IMEIKey) = Level:Benign
            ! Found
            imei:IMEINumber = impfil:IMEINumber
            imei:ShipDate   = Deformat(impfil:ShipDate,@d06)
            ! Insert --- New import field (DBH: 12/02/2009) #10672
            imei:ProductCode = impfil:ProductCode
            ! End --- (DBH: 12/02/2009) #10672
            If Access:IMEISHIP.TryUpdate() = Level:Benign
                CountUpdate# += 1
! Deleting (DBH 17/01/2008) # 9698 - Not needed
!                Access:IMEISHP2.Clearkey(imei2:IMEIRecordNumberKey)
!                imei2:IMEIRecordNumber  = imei:RecordNumber
!                If Access:IMEISHP2.Tryfetch(imei2:IMEIRecordNumberKey) = Level:Benign
!                    ! Found
!                    imei2:Manufacturer = impfil:TheRest
!                    Access:IMEISHP2.TryUpdate()
!                Else ! If Access:IMEISHP2.Tryfetch(imei2:IMEIRecordNumber) = Level:Benign
!                    ! Error
!                    If Access:IMEISHP2.PrimeRecord() = Level:Benign
!                        imei2:IMEIRecordNumber = imei:RecordNumber
!                        imei2:Manufacturer = impfil:TheRest
!                        If Access:IMEISHP2.TryInsert() = Level:Benign
!                            ! Insert Successful
!                        Else ! If Access:IMEISHP2.TryInsert() = Level:Benign
!                            ! Insert Failed
!                            Access:IMEISHP2.CancelAutoInc()
!                        End ! If Access:IMEISHP2.TryInsert() = Level:Benign
!                    End !If Access:IMEISHP2.PrimeRecord() = Level:Benign
!                End ! If Access:IMEISHP2.Tryfetch(imei2:IMEIRecordNumber) = Level:Benign
! End (DBH 17/01/2008) #9698
            End ! If Access:IMEISHIP.TryUpdate() = Level:Benign
        Else ! If Access:IMEISHIP.Tryfetch(imei:IMEIKey) = Level:Benign
            ! Error
            If Access:IMEISHIP.PrimeRecord() = Level:Benign
                imei:IMEINumber = impfil:IMEINumber
                imei:ShipDate   = Deformat(impfil:ShipDate,@d06)
                ! Insert --- New import field (DBH: 12/02/2009) #10672
                imei:ProductCode = impfil:ProductCode
                ! End --- (DBH: 12/02/2009) #10672
                If Access:IMEISHIP.TryInsert() = Level:Benign
                    ! Insert Successful
                    CountNormal# += 1
! Deleting (DBH 17/01/2008) # 9698 - Not needed
!                    If Access:IMEISHP2.PrimeRecord() = Level:Benign
!                        imei2:IMEIRecordNumber = imei:RecordNumber
!                        imei2:Manufacturer = impfil:TheRest
!                        If Access:IMEISHP2.TryInsert() = Level:Benign
!                            ! Insert Successful
!                        Else ! If Access:IMEISHP2.TryInsert() = Level:Benign
!                            ! Insert Failed
!                            Access:IMEISHP2.CancelAutoInc()
!                        End ! If Access:IMEISHP2.TryInsert() = Level:Benign
!                    End !If Access:IMEISHP2.PrimeRecord() = Level:Benign
! End (DBH 17/01/2008) #9698
                Else ! If Access:IMEISHIP.TryInsert() = Level:Benign
                    ! Insert Failed
                    Access:IMEISHIP.CancelAutoInc()
                End ! If Access:IMEISHIP.TryInsert() = Level:Benign
            End !If Access:IMEISHIP.PrimeRecord() = Level:Benign
        End ! If Access:IMEISHIP.Tryfetch(imei:IMEIKey) = Level:Benign

    End !Loop(ImportFile)
    Close(ImportFile)
    Relate:IMEISHIP.Close()

    ErrorLog('IMEIs Imported: ' & CountNormal#)
    ErrorLog('IMEIs Updated: ' & CountUpdate#)

    Rename(Clip(Path()) & '\LocaterDownloaded\' & Clip(func:ShortName),Clip(Path()) & '\LocaterImported\' & Clip(func:ShortName))

     IF Error()
        Case Error()
            Of ''
                Error" = 'Same File Already Exists'
            Of 2
                Error" = 'File Not Found'
            Of 5
                Error" = 'Access Denied'
            Of 52
                Error" = 'File Aready Open'
            Else
                Error" = Error()
        End ! Case Error()
        ErrorLog('Rename Error (' & Clip(Error") & '): ' & Clip(func:ShortName))
    Else ! If Error()
        ErrorLog('File Placed in ''Sent'' folder')
    End ! If Error()
    !End   - Debug - TrkBs:  (DBH: 25-01-2005)

