

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN012.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBIN009.INC'),ONCE        !Req'd for module callout resolution
                     END


BackflushStatus      PROCEDURE  (jobNo, entryDate, entryTime, importFileName, strPath) ! Declare Procedure
result  long
paramDate date
paramTime time
  CODE
    !
    ! Description : Update the current status of a job
    !

    result = false

    if jobNo <> 0

        if exists(clip(strPath) & '\JOBS.DAT')
            ! Change the paths this way
            JOBS{Prop:Name} = clip(strPath) & '\JOBS.DAT'
            AUDIT{Prop:Name} = clip(strPath) & '\AUDIT.DAT'

            Access:JOBS.Open()
            Access:JOBS.UseFile()
            Access:AUDIT.Open()
            Access:AUDIT.UseFile()

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = jobNo
            if not Access:JOBS.Fetch(job:Ref_Number_Key)

                if sub(job:Current_Status,1,3) <> '940'

                    paramDate = deformat(entryDate, @d06b)
                    paramTime = deformat(entryTime, @t01b)

                    ! The date and time the status change occurred
                    if paramDate = '' then paramDate = today().
                    if paramTime = '' then paramTime = clock().

                    GetStatus('940', 0, 'JOB', '', paramDate, paramTime, strPath)

                    if not Access:JOBS.TryUpdate()

                        ! The job has been updated
                        if Access:AUDIT.PrimeRecord() = Level:Benign
                            aud:Notes         = 'IMPORT FILE: ' & Clip(importFileName)
                            aud:Ref_Number    = job:ref_number
                            aud:Date          = today() ! Use the actual time the import took place
                            aud:Time          = clock() ! Use the actual time the import took place
                            aud:Type          = 'JOB'
                            aud:User          = 'WEB'
                            aud:Action        = 'JOB UPDATED BY CUSTOMER COLLECTION DATE BACKFLUSH'
                            if Access:AUDIT.Insert() <> Level:Benign
                                Access:AUDIT.CancelAutoInc()
                            end
                        end

                        result = true
                    end
                end
            end

            Access:AUDIT.Close()
            Access:JOBS.Close()
        end

    end

    return result
