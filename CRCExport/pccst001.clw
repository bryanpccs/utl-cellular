

   MEMBER('pccstest.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('PCCST001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
  CODE

    strPath = '\\COURTESY-LIVE\Data\Cellular'
    
    DEFAULTS{Prop:Name} = clip(strPath) & '\DEFAULTS.DAT'

    share(DEFAULTS)
    if errorcode()
        if errorcode() = 90
            LinePrint(format(today(),@d06b) & ' ' & format(clock(), @t06b) & ' : (OPEN) ' & fileerrorcode() & ' ' & fileerror(), path() & '\Errors.log')
        else
            LinePrint(format(today(),@d06b) & ' ' & format(clock(), @t06b) & ' : (OPEN) ' & errorcode() & ' ' & error(), path() & '\Errors.log')
        end
        halt()
    end

    set(DEFAULTS, 0)
    if errorcode()
        if errorcode() = 90
            LinePrint(format(today(),@d06b) & ' ' & format(clock(), @t06b) & ' : (SET) ' & fileerrorcode() & ' ' & fileerror(), path() & '\Errors.log')
        else
            LinePrint(format(today(),@d06b) & ' ' & format(clock(), @t06b) & ' : (SET) ' & errorcode() & ' ' & error(), path() & '\Errors.log')
        end
        halt()
    end

    next(DEFAULTS)
    if errorcode()
        if errorcode() = 90
            LinePrint(format(today(),@d06b) & ' ' & format(clock(), @t06b) & ' : (NEXT) ' & fileerrorcode() & ' ' & fileerror(), path() & '\Errors.log')
        else
            LinePrint(format(today(),@d06b) & ' ' & format(clock(), @t06b) & ' : (NEXT) ' & errorcode() & ' ' & error(), path() & '\Errors.log')
        end
        halt()
    end

    close(DEFAULTS)
    if errorcode()
        if errorcode() = 90
            LinePrint(format(today(),@d06b) & ' ' & format(clock(), @t06b) & ' : (CLOSE) ' & fileerrorcode() & ' ' & fileerror(), path() & '\Errors.log')
        else
            LinePrint(format(today(),@d06b) & ' ' & format(clock(), @t06b) & ' : (CLOSE) ' & errorcode() & ' ' & error(), path() & '\Errors.log')
        end
        halt()
    end

    halt()
