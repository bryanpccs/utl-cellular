

   MEMBER('imeifix.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('IMEIF001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

dateFrom             DATE
dateTo               DATE
window               WINDOW('Caption'),AT(,,185,73),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,SYSTEM,GRAY,DOUBLE
                       PROMPT('Date From'),AT(17,12),USE(?dateFrom:Prompt)
                       ENTRY(@d06b),AT(69,12,60,10),USE(dateFrom)
                       PROMPT('Date To'),AT(17,28),USE(?dateTo:Prompt)
                       ENTRY(@d06b),AT(69,28,60,10),USE(dateTo)
                       BUTTON('BER'),AT(9,50,35,14),USE(?BERButton)
                       BUTTON('Import'),AT(100,50,35,14),USE(?OkButton),DEFAULT
                       BUTTON('Cancel'),AT(144,50,36,14),USE(?CancelButton)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?dateFrom:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:IMEIFIX.Open
  Relate:IMEISHIP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:IMEIFIX.Close
    Relate:IMEISHIP.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?BERButton
      ThisWindow.Update
      Access:IMEIFIX.ClearKey(IMEF:ShipDateKey)
      IMEF:ShipDate = dateFrom
      set(IMEF:ShipDateKey, IMEF:ShipDateKey)
      loop
          if Access:IMEIFIX.Next() <> Level:Benign then break.
          if IMEF:ShipDate >= dateTo then break.
      
          Access:IMEISHIP.ClearKey(IMEI:IMEIKey)
          IMEI:IMEINumber = IMEF:IMEINumber
          if Access:IMEISHIP.Fetch(IMEI:IMEIKey) = Level:Benign
              ! Found
              IMEI:BER = IMEF:BER
              Access:IMEISHIP.TryUpdate()
          end
      end
      
      message('Finished')
    OF ?OkButton
      ThisWindow.Update
      Access:IMEIFIX.ClearKey(IMEF:ShipDateKey)
      IMEF:ShipDate = dateFrom
      set(IMEF:ShipDateKey, IMEF:ShipDateKey)
      loop
          if Access:IMEIFIX.Next() <> Level:Benign then break.
          if IMEF:ShipDate >= dateTo then break.
      
          Access:IMEISHIP.ClearKey(IMEI:IMEIKey)
          IMEI:IMEINumber = IMEF:IMEINumber
          if Access:IMEISHIP.Fetch(IMEI:IMEIKey) <> Level:Benign
              ! Not found
              if Access:IMEISHIP.PrimeRecord() = Level:Benign
                  IMEI:IMEINumber = IMEF:IMEINumber
                  IMEI:ShipDate = IMEF:ShipDate
                  IMEI:BER = IMEF:BER
                  if Access:IMEISHIP.TryUpdate() <> Level:Benign
                      Access:IMEISHIP.CancelAutoInc()
                  end
              end
          end
      end
      
      message('Finished')
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

