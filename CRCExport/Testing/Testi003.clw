

   MEMBER('Testing.clw')                              ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('TESTI003.INC'),ONCE        !Local module procedure declarations
                     END


UpdatePENDMAIL PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
History::pem:Record  LIKE(pem:RECORD),STATIC
QuickWindow          WINDOW('Update the PENDMAIL File'),AT(,,358,182),FONT('MS Sans Serif',8,,),IMM,HLP('UpdatePENDMAIL'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,350,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?pem:RecordNumber:Prompt),TRN,FONT('Tahoma',,,,CHARSET:ANSI)
                           ENTRY(@s8),AT(140,20,40,10),USE(pem:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Date'),AT(8,34),USE(?pem:DateCreated:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@d6b),AT(140,34,104,10),USE(pem:DateCreated),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Date'),TIP('Date'),UPR
                           PROMPT('Time Created'),AT(8,48),USE(?pem:TimeCreated:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@t1b),AT(140,48,104,10),USE(pem:TimeCreated),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Time Created'),TIP('Time Created'),UPR
                           PROMPT('Message Type'),AT(8,62),USE(?pem:MessageType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s3),AT(140,62,40,10),USE(pem:MessageType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Message Type'),TIP('Message Type'),UPR
                           PROMPT('SMS or Email'),AT(8,76),USE(?pem:SMSEmail:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s5),AT(140,76,40,10),USE(pem:SMSEmail),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMS or EMail'),TIP('SMS or EMail'),UPR
                           PROMPT('Link To JOBS Ref Number'),AT(8,90),USE(?pem:RefNumber:Prompt),TRN,FONT('Tahoma',,,,CHARSET:ANSI)
                           ENTRY(@s8),AT(140,90,40,10),USE(pem:RefNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Link To JOBS Ref Number'),TIP('Link To JOBS Ref Number'),UPR
                           PROMPT('Date To Send'),AT(8,104),USE(?pem:DateToSend:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@d6b),AT(140,104,104,10),USE(pem:DateToSend),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Date To Send'),TIP('Date To Send'),UPR
                           PROMPT('Time To Send'),AT(8,118),USE(?pem:TimeToSend:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@t1b),AT(140,118,104,10),USE(pem:TimeToSend),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Time To Send'),TIP('Time To Send'),UPR
                           PROMPT('Date Sent'),AT(8,132),USE(?pem:DateSent:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@d6b),AT(140,132,104,10),USE(pem:DateSent),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Date Sent'),TIP('Date Sent'),UPR
                           PROMPT('Time Sent'),AT(8,146),USE(?pem:TimeSent:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@t1b),AT(140,146,104,10),USE(pem:TimeSent),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Time Sent'),TIP('Time Sent'),UPR
                         END
                         TAB('General (cont.)'),USE(?Tab:2)
                           PROMPT('Error Message'),AT(8,20),USE(?pem:ErrorMessage:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(140,20,210,10),USE(pem:ErrorMessage),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Error Message'),TIP('Error Message'),UPR
                           PROMPT('Mobile Number'),AT(8,34),USE(?pem:MobileNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(140,34,124,10),USE(pem:MobileNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Mobile Number'),TIP('Mobile Number'),UPR
                           PROMPT('Email Address'),AT(8,48),USE(?pem:EmailAddress:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(140,48,210,10),USE(pem:EmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           PROMPT('Booking Type'),AT(8,62),USE(?pem:BookingType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(140,62,124,10),USE(pem:BookingType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Booking Type'),TIP('Booking Type'),UPR
                           PROMPT('Status'),AT(8,76),USE(?pem:Status:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(140,76,124,10),USE(pem:Status),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Status'),TIP('Status'),UPR
                           PROMPT('SID Job Number'),AT(8,90),USE(?pem:SIDJobNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(140,90,124,10),USE(pem:SIDJobNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SID Job Number'),TIP('SID Job Number'),UPR
                           CHECK('Reminder'),AT(140,104,70,8),USE(pem:Reminder),TRN,MSG('Reminder'),TIP('Reminder'),VALUE('1','0')
                           PROMPT('Message Sent To Customer or UTL?'),AT(8,116),USE(?pem:CustomerUTL:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s8),AT(140,116,40,10),USE(pem:CustomerUTL),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Message Sent To Customer or UTL?'),TIP('Message Sent To Customer or UTL?'),UPR
                         END
                       END
                       BUTTON('OK'),AT(260,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(309,164,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(309,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdatePENDMAIL')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?pem:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(pem:Record,History::pem:Record)
  SELF.AddHistoryField(?pem:RecordNumber,1)
  SELF.AddHistoryField(?pem:DateCreated,2)
  SELF.AddHistoryField(?pem:TimeCreated,3)
  SELF.AddHistoryField(?pem:MessageType,4)
  SELF.AddHistoryField(?pem:SMSEmail,5)
  SELF.AddHistoryField(?pem:RefNumber,6)
  SELF.AddHistoryField(?pem:DateToSend,7)
  SELF.AddHistoryField(?pem:TimeToSend,8)
  SELF.AddHistoryField(?pem:DateSent,9)
  SELF.AddHistoryField(?pem:TimeSent,10)
  SELF.AddHistoryField(?pem:ErrorMessage,11)
  SELF.AddHistoryField(?pem:MobileNumber,12)
  SELF.AddHistoryField(?pem:EmailAddress,13)
  SELF.AddHistoryField(?pem:BookingType,14)
  SELF.AddHistoryField(?pem:Status,15)
  SELF.AddHistoryField(?pem:SIDJobNumber,16)
  SELF.AddHistoryField(?pem:Reminder,17)
  SELF.AddHistoryField(?pem:CustomerUTL,18)
  SELF.AddUpdateFile(Access:PENDMAIL)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:PENDMAIL.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PENDMAIL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PENDMAIL.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

