

   MEMBER('Testing.clw')                              ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('TESTI002.INC'),ONCE        !Local module procedure declarations
                     END


UpdateAUDIT PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
History::aud:Record  LIKE(aud:RECORD),STATIC
QuickWindow          WINDOW('Update the AUDIT File'),AT(,,358,168),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateAUDIT'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,350,142),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('record number:'),AT(8,20),USE(?aud:record_number:Prompt)
                           ENTRY(@n12),AT(68,20,52,10),USE(aud:record_number),DECIMAL(14)
                           PROMPT('Ref Number'),AT(8,34),USE(?aud:Ref_Number:Prompt),TRN
                           ENTRY(@p<<<<<<<<#p),AT(68,34,44,10),USE(aud:Ref_Number),FONT('Arial',8,,FONT:bold),COLOR(COLOR:Yellow),UPR
                           PROMPT('Date'),AT(8,48),USE(?aud:Date:Prompt),TRN
                           ENTRY(@d6b),AT(68,48,104,10),USE(aud:Date),SKIP,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR
                           PROMPT('Time'),AT(8,62),USE(?aud:Time:Prompt),TRN
                           ENTRY(@t1),AT(68,62,104,10),USE(aud:Time),SKIP,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR
                           PROMPT('User'),AT(8,76),USE(?aud:User:Prompt),TRN
                           ENTRY(@s3),AT(68,76,40,10),USE(aud:User),SKIP,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR
                           PROMPT('Action'),AT(8,90),USE(?aud:Action:Prompt),TRN
                           ENTRY(@s80),AT(68,90,282,10),USE(aud:Action),SKIP,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR
                           PROMPT('Dummy Field:'),AT(8,104),USE(?aud:DummyField:Prompt)
                           ENTRY(@s2),AT(68,104,40,10),USE(aud:DummyField),MSG('For File Manager'),TIP('For File Manager')
                           PROMPT('Notes'),AT(8,118),USE(?aud:Notes:Prompt)
                           ENTRY(@s255),AT(68,118,282,10),USE(aud:Notes),UPR
                           PROMPT('Type'),AT(8,132),USE(?aud:Type:Prompt)
                           ENTRY(@s3),AT(68,132,40,10),USE(aud:Type),MSG('Job/Exchange/Loan'),TIP('Job/Exchange/Loan')
                         END
                       END
                       BUTTON('OK'),AT(260,150,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(309,150,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(309,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateAUDIT')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?aud:record_number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(aud:Record,History::aud:Record)
  SELF.AddHistoryField(?aud:record_number,1)
  SELF.AddHistoryField(?aud:Ref_Number,2)
  SELF.AddHistoryField(?aud:Date,3)
  SELF.AddHistoryField(?aud:Time,4)
  SELF.AddHistoryField(?aud:User,5)
  SELF.AddHistoryField(?aud:Action,6)
  SELF.AddHistoryField(?aud:DummyField,7)
  SELF.AddHistoryField(?aud:Notes,8)
  SELF.AddHistoryField(?aud:Type,9)
  SELF.AddUpdateFile(Access:AUDIT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:AUDIT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

