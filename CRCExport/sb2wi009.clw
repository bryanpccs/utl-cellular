

   MEMBER('sb2wilfred.clw')                           ! This is a MEMBER module

                     MAP
                       INCLUDE('SB2WI009.INC'),ONCE        !Local module procedure declarations
                     END


FormatPostcode       PROCEDURE  (postcode)            ! Declare Procedure
formattedPostcode   string(10)
i                   long, auto
j                   long(1)
pcLength            long, auto
  CODE
    ! Format the postcode so that it has a space between the inward and outward code
    
    ! Remove any surplus spaces, user could have entered several spaces
    loop i = 1 to len(clip(postcode))
        if postcode[i] = ' ' then cycle.
        formattedPostcode[j] = postcode[i]
        j += 1
    end

    pcLength = len(clip(formattedPostcode))

    if (pcLength > 3)
        formattedPostcode = sub(clip(formattedPostcode), 1, (pcLength - 3)) & ' ' & sub(clip(formattedPostcode), -3, 3)
    end

    return clip(formattedPostcode)
