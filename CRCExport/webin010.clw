

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN010.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBIN009.INC'),ONCE        !Req'd for module callout resolution
                     END


ChangeStatus         PROCEDURE  (jobNo, strStatusType, strAgentName, strPath) ! Declare Procedure
result long
  CODE
    !
    ! Description : Update the current status of a job
    !

    result = false

    if jobNo <> 0

        if exists(clip(strPath) & '\JOBS.DAT')
            ! Change the paths this way
            JOBS{Prop:Name} = clip(strPath) & '\JOBS.DAT'

            Access:JOBS.Open()
            Access:JOBS.UseFile()

            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = jobNo
            if not Access:JOBS.Fetch(job:Ref_Number_Key)

                GetStatus(strStatusType[1:3], 0, 'JOB', strAgentName, today(), clock(), strPath)

                if not Access:JOBS.TryUpdate()
                    result = true
                end
            end

            Access:JOBS.Close()
        end

    end

    return result
