

   MEMBER('sb2wilfred.clw')                           ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SB2WI001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SB2WI003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SB2WI004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SB2WI007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SB2WI009.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

FilesOpened          BYTE
save_aud_id          USHORT,AUTO
ConnectionStr        STRING(255),STATIC
DiagConnectionStr    STRING(255),STATIC
INIFilePath          STRING(255)
tmp:ExportFile       STRING(255),STATIC
tmp:ImportFile       STRING(255),STATIC
tmp:Filename         CSTRING(255)
Window               WINDOW('ServiceBase / Wilfred Import Export Facility'),AT(,,245,23),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,TIMER(100),SYSTEM,GRAY,DOUBLE
                       PROMPT('Running.....'),AT(3,4,241,12),USE(?Information)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ExportFile    File,Driver('ASCII'),Pre(expfil),Name(tmp:ExportFile),Create,Bindable,Thread
Record                  Record
Line                    String(2000)
                        End
                    End

TempFiles   Queue(file:Queue),pre(fil)
            End
! Dummy SQL tables
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD
SQLSBJobNo                  Long, Name('SBJobNo')
SQLBookingType              String(1), Name('BookingType')
SQLRepairType               String(2), Name('RepairType')
SQLPrimaryFault             String(255), Name('PrimaryFault')
SQLReason                   String(255), Name('Reason')
SQLCustCollectionDate       Date, Name('CustCollectionDate')
SQLRetStoreCode             String(15), Name('RetStoreCode')
SQLBookedBy                 String(30), Name('BookedBy')
! Inserting (DBH 23/02/2006) #6915 - New Fields to check for sms/email alerts
SQLSMSReq                   Byte(), Name('SMSReq')
SQLEmailReq                 Byte(), Name('EmailReq')
! End (DBH 23/02/2006) #6915
SQLCountryDelivery          String(20), Name('CountryDelivery')
! Inserting (DBH 12/06/2006) #7813 - Record free text in TraFaultCode3 field
SQLFreeTextFault            String(255), Name('FreeTextFault')
! End (DBH 12/06/2006) #7813
SQLDeviceCondition          String(40), Name('DeviceCondition')
SQLUnitCondition            String(255), Name('UnitCondition')
SQLPhoneLock                String(30), Name('Phone_Lock')
! Inserting (DBH 03/07/2008) # 10020 - Show the SMS alert number, rather than the mobile number
SQLSMSAlertNo               String(15), Name('SMSAlertNo')
! End (DBH 03/07/2008) #10020
                        END
                    END

! Contact Centre (Exchange Bookings) (#7957 11/08/2006 GK)
CCExchFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(ccex),Name('CCExchange'),Bindable,Thread
Record          Record
ExchangeDate            date, name('ExchangeDate')
TimeSlotID              long, name('TimeSlotID')
DeliveryCharge          pdecimal(8, 2), name('DeliveryCharge')
TransitDays             long, name('TransitDays')
WebJobNo                long, name('WebJobNo')
                End
           End

TimeSlotFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(tmsl),Name('TimeSlot'),Bindable,Thread
Record          Record
TimeSlotName            cstring(30), name('TimeSlotName')
                End
             End

WebJobExFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(webex),Name('WebJobEx'),Bindable,Thread
Record          Record
SCAccountID            long, name('SCAccountID')
ManifestNo             long, name('ManifestNo')
InterTimeFrame         cstring(31), name('InterTimeFrame')
ServiceCharge          pdecimal(8, 2), name('ServiceCharge')
DiagAvailable          byte, name('DiagAvailable')
Forename               cstring(31), name('Forename')
NoExceptionDate        date, name('NoExceptionDate')
KnownIssueID           long, name('KnownIssueID')
                End
             End

WebRetFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(webr),Name('WebRet'),Bindable,Thread
Record          Record
RetReasonCode          cstring(7), name('RetReasonCode')
RetReasonName          cstring(51), name('RetReasonName')
ReturnException        byte, name('ReturnException')
                End
             End

JobTxtFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(jobt),Name('JobTxt'),Bindable,Thread
Record          Record
Text1               cstring(255),Name('Text1')
Text2               cstring(255),Name('Text2')
                End
            End

KnownIssueFile File,Driver('Scalable'),OEM,Owner(DiagConnectionStr),Pre(knis),Name('KnownIssue'),Bindable,Thread
Record          Record
Summary             cstring(51),Name('Summary')
                End
            End
local       Class
AddToAlertViewer      Procedure(String f:Type,String f:Status,String f:ModelNo)
GetNextDateTime       Procedure(*Date func:DateToSend,*Time func:TimeToSend)
GetWorkDate           procedure(long scAccountID, string country, date startDate, long workingDays), date
IsNonWorkingDay       procedure(long scAccountID, string country), byte
            End ! local       Class
local:CurrentRefNumber          Long()
local:FoundJob                  Byte(0)
local:SIDBooking                Byte(1)
local:WilfredToSend             CString(255)
local:WilfredSent               CString(255)
local:TheJobNumber              Long()
local:CountJobs                 Long()
local:LastJobsCheckedDate       Date()
local:LastJobsCheckedTime       Time()
local:NewJob                    Byte(0)
local:IsThereAnAccessory        Byte(0)
local:JobAlreadyExported        Byte(0)
local:strStatusCode             String(3)
local:statusCode                Long()
local:skipJob                   Byte(0)
local:exported                  Byte(0)
local:exchangeJob               Byte(0)
local:returnJob                 Byte(0)
local:SCAccountID               Long()
local:emailMessage              String(8000)
local:OracleCode                String(30)
local:ContractType              String(10)
local:sendCopyToSMART           Byte(0)
local:SMARTCopy                 Byte(0)
local:ManifestNo                Long()
local:InterTimeFrame            CString(31)
local:ServiceCharge             PDecimal(8, 2)
local:DiagAvailable             Byte(0)
local:Forename                  CString(31)
local:SendStatusAlert           Byte(0)
local:MappedBookingType         String(1)
local:transitDays               Long()
local:NoExceptionDate           Date()
local:ExceptionDate             Date()
local:KnownIssueID              Long()
local:KnownIssueSummary         String(50)
local:AdditionalSymptoms        String(500)

!Keep track of which jobs have already been exported  (DBH: 23-11-2004)
JobsQueue                   Queue(),Pre(jobque)
JobNumber                     Long()
                                End

ExchangeQ                   queue(),pre(exchq)
JobNo                           long()
                            end

!End   - Keep track of which jobs have already been exported  (DBH: 23-11-2004)
local:SIDJobCount               Long()
local:NonSIDJobCount            Long()

ExportQueue                 Queue(),Pre(expque)
SCAccountID                   Long()
ExportFileName                CString(255)
                            End

! Keep track of audit records that need removing
AuditQueue                  Queue(),Pre(audq)
AuditRecordNumber               Long()
                            End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

ExportJobs          Routine
Data
i       long(1)
dateNow date()
timeNow time()
local:recordNumber long, auto
foundRecord byte(0)

Code

    Relate:JOBS.Open()
    Relate:DEFAULTS.Open()
    Relate:USERS.Open()
    Relate:EXCHANGE.Open()
    Relate:DESBATCH.Open()
    Relate:STATUS.Open()
    Relate:AUDIT_ALIAS.Open()
    Relate:PENDMAIL.Open()
    Relate:PENDMAIL_ALIAS.Open()
    Relate:DEFEMAI2.Open()
    Relate:SIDSRVCN.Open()
    Relate:SIDMODSC.Open()
    Relate:SIDDEF.Open()
    Relate:CARISMA.Open()
    Relate:SIDNWDAY.Open()
    !Relate:SIDEXUPD.Open()
    Relate:SIDALERT.Open()
    Relate:SIDREMIN.Open()
    Relate:SMOALERT.Open()
    Relate:SMOREMIN.Open()
    Relate:SIDALDEF.Open()
    Relate:MODELNUM.Open()
    Relate:IMEISHIP.Open()
    Relate:SIDAUD.Open()

    Set(SIDALDEF)
    Access:SIDALDEF.Next()

    Set(DEFEMAI2)
    Access:DEFEMAI2.Next()

    Set(SIDDEF, 0)
    Access:SIDDEF.Next()

    Open(SQLFile)
    Open(CCExchFile)
    Open(TimeSlotFile)
    Open(WebJobExFile)
    Open(WebRetFile)
    Open(JobTxtFile)
    Open(KnownIssueFile)

    !Start - Create folders  (DBH: 23-11-2004)
    local:WilfredToSend = Clip(Path()) & '\WilfredToSend'
    If ~Exists(local:WilfredToSend)
        If MkDir(local:WilfredToSend)

        End !If MkDir(local:WilfredToSend)
    End !If ~Exists(local:WilfredToSend)

    local:WilfredSent = Clip(Path()) & '\WilfredSent'
    If ~Exists(local:WilfredSent)
        If MkDir(local:WilfredSent)

        End !If MkDir(Clip(Path()) & '\WilfredSent')
    End !If ~Exists(Clip(Path()) & '\WilfredSent')
    !End   - Create folders  (DBH: 23-11-2004)

    ! Build a list of export filenames for each service centre
    ! Do not create the export files until a job is found to export
    Set(srv:SCAccountIDKey)
    Loop
        If Access:SIDSRVCN.Next() <> Level:Benign Then Break.
        If srv:AccountActive = false Then Cycle.
        ExportQueue.SCAccountID = srv:SCAccountID
        if srv:SCAccountID = 1
            ExportQueue.ExportFileName = local:WilFredToSend & '\sid_' & Format(Today(),@d012) & '_' & Format(Clock(),@t02)
        else
            ExportQueue.ExportFileName = local:WilFredToSend & '\sid' & srv:SCAccountID & '_' & Format(Today(),@d012) & '_' & Format(Clock(),@t02)
        end
        add(ExportQueue, ExportQueue.SCAccountID)

        tmp:ExportFile = Clip(ExportQueue.ExportFileName) & '.tmp'
        If Exists(tmp:ExportFile)
            Remove(ExportFile)
        End
    End

    ! ------------------------
    !tmp:FileName = local:WilFredToSend & '\sid_' & Format(Today(),@d012) & '_' & Format(Clock(),@t02)
    ! ------------------------

    ! Try to re-export records that previously failed
    set(sad:AuditRecordNumberKey)
    loop
        if Access:SIDAUD.Next() <> Level:Benign then break.
        if sad:EntryDate > today() - 14
            ! Attempt to re-export
            Access:AUDIT_ALIAS.ClearKey(audali:Record_Number_Key)
            audali:record_number = sad:AuditRecordNumber
            if Access:AUDIT_ALIAS.TryFetch(audali:Record_Number_Key) = Level:Benign
                local:recordNumber = audali:record_number

                local:SendStatusAlert = 0

                If Instring('JOB CREATED BY WEBMASTER',audali:Notes,1,1)
                    !Is this a new job, or an updated one  (DBH: 23-11-2004)
                    local:NewJob = True
                    local:JobAlreadyExported = False
                    !Start - Has this Yeah thnew job already been exported?  (DBH: 23-11-2004)
                    Save_aud_ID = Access:AUDIT.SaveFile()
                    Access:AUDIT.Clearkey(aud:Action_Key)
                    aud:Ref_Number = audali:Ref_Number
                    aud:Action = 'JOB EXPORTED TO WILFRED'
                    Set(aud:Action_Key,aud:Action_Key)
                    Loop ! Begin AUDIT Loop
                        If Access:AUDIT.Next()
                            Break
                        End ! If !Access
                        If aud:Ref_Number <> audali:Ref_Number
                            Break
                        End ! If
                        If aud:Action <> 'JOB EXPORTED TO WILFRED'
                            Break
                        End ! If
                        local:JobAlreadyExported = True
                        Break
                    End ! End AUDIT Loop
                    Access:AUDIT.RestoreFile(Save_aud_ID)

                    if local:JobAlreadyExported then cycle.

                Else ! If audali:Action = 'NEW JOB INITIAL BOOKING'
                    !Is this a new job, or an updated one?  (DBH: 23-11-2004)
                    local:NewJob = False

                    If Instring('STATUS CHANGED TO',audali:Action,1,1) Or Instring('JOB UPDATED BY WILFRED',audali:Action,1,1)
                        ! We now only send jobs to wilfred once they reach a status of despatched from store.
                        ! It is possible the job is booked but not despatched within the 5 days we are looking over.
                        ! Therefore first check that this job has been exported to wilfred

                        local:JobAlreadyExported = False
                        Save_aud_ID = Access:AUDIT.SaveFile()
                        Access:AUDIT.Clearkey(aud:Action_Key)
                        aud:Ref_Number = audali:Ref_Number
                        aud:Action = 'JOB EXPORTED TO WILFRED'
                        Set(aud:Action_Key,aud:Action_Key)
                        Loop ! Begin AUDIT Loop
                            If Access:AUDIT.Next()
                                Break
                            End ! If !Access
                            If aud:Ref_Number <> audali:Ref_Number
                                Break
                            End ! If
                            If aud:Action <> 'JOB EXPORTED TO WILFRED'
                                Break
                            End ! If
                            local:JobAlreadyExported = True
                            Break
                        End ! End AUDIT Loop
                        Access:AUDIT.RestoreFile(Save_aud_ID)

                        if local:JobAlreadyExported = False
                            local:NewJob = True
                        End

                        ! See if the status change needs to send an alert (DBH: 23/11/2007)
                        local:SendStatusAlert = 1

                    Else ! If Instring('STATUS CHANGED TO',audali:Action,1,1)
                        !Only looking for Status Changes  (DBH: 23-11-2004)
                        Cycle
                    End ! If Instring('STATUS CHANGED TO',audali:Action,1,1)
                    
                End ! If audali:Action = 'NEW JOB INITIAL BOOKING'

                local:TheJobNumber = audali:Ref_Number
                do ProcessJob
                if local:exported = true
                    ! Remove record
                    AuditQueue.AuditRecordNumber = audali:Record_Number
                    add(AuditQueue, AuditQueue.AuditRecordNumber)
                end
            else
                ! Remove record and log to file
                AuditQueue.AuditRecordNumber = audali:Record_Number
                add(AuditQueue, AuditQueue.AuditRecordNumber)
                ErrorLog('Have not exported audit record: ' & audali:Record_Number & ', Job number: ' & audali:Ref_Number)
            end
        else
            ! Remove record and log to file
            AuditQueue.AuditRecordNumber = audali:Record_Number
            add(AuditQueue, AuditQueue.AuditRecordNumber)
            ErrorLog('Have not exported audit record: ' & audali:Record_Number & ', Job number: ' & audali:Ref_Number)
        end
    end

    ! Delete any records from the SID Audit backlog
    i = 1

    loop 
        get(AuditQueue, i)
        if error() then break.
        Access:SIDAUD.ClearKey(sad:AuditRecordNumberKey)
        sad:AuditRecordNumber = AuditQueue.AuditRecordNumber
        if Access:SIDAUD.TryFetch(sad:AuditRecordNumberKey) = Level:Benign
            Access:SIDAUD.DeleteRecord(0)
        end

        i += 1
    end

    free(AuditQueue)
    clear(AuditQueue)

    !Start - Only check status changes since the last time the export was run  (DBH: 23-11-2004)
    local:LastJobsCheckedDate = Deformat(GETINI('Export','Jobs Checked Date',,CLIP(Path()) & '\CRCEXPORT.INI'),@d06)
    local:LastJobsCheckedTime = Deformat(GETINI('Export','Jobs Checked Time',,CLIP(Path()) & '\CRCEXPORT.INI'),@t01)
    !End   - Only check status changes since the last time the export was run  (DBH: 23-11-2004)

    ! GK 27/01/2010 - Cannot use the status date/time over the audit table. The status dates and times can be copied
    !                 over from import files/other tables. Therefore new records could be missed if they are created
    !                 with an older entry date.
    local:recordNumber = GETINI('Export','NextRecordNumber',,CLIP(Path()) & '\CRCEXPORT.INI')

    dateNow = today()
    timeNow = clock()

    !Save the time of this export  (DBH: 23-11-2004)
    PUTINI('Export','Jobs Checked Date',Format(dateNow,@d06),CLIP(Path()) & '\CRCEXPORT.INI')
    PUTINI('Export','Jobs Checked Time',Format(timeNow,@t01),CLIP(Path()) & '\CRCEXPORT.INI')
    !End   - Save the time of this export  (DBH: 23-11-2004)

    Clear(JobsQueue)
    Free(JobsQueue)

    !Go back over a day to make sure that no jobs are missed  (DBH: 23-11-2004)
    Access:AUDIT_ALIAS.ClearKey(audali:Record_Number_Key) ! audali:DateActionJobKey
    ! Was changed to 15 Days, verbal confirmation from Ian Walton 25/08/2006
    ! 28/05/2009 GK - Spoke to Nick about this after sb2wilfred was taking a long time to complete,
    !                 have reduced the number of days to 5. The code seems to be back checking anyway
    !                 so really could be set to one day.

    ! 15/10/2010 GK - Several months later and we notice that lots of exchange jobs have not been sent
    !                 via the interface. There is a rule where exchanges are only added to the export file the
    !                 day before the exchange is required. If the customer selects a future exchange date of more
    !                 than 5 days then the job will never get exported as we don't look back far enough...
    
    !audali:Date       = local:LastJobsCheckedDate
    audali:record_number = local:recordNumber
    set(audali:Record_Number_Key, audali:Record_Number_Key)
    !Set(audali:DateActionJobKey,audali:DateActionJobKey)
    Loop
        If Access:AUDIT_ALIAS.NEXT()
            ! Don't want to keep incrementing the record number if no new records are found
            if foundRecord
                PUTINI('Export','NextRecordNumber',local:recordNumber + 1,CLIP(Path()) & '\CRCEXPORT.INI')
            end
            Break
        End !If

        local:recordNumber = audali:record_number
        foundRecord = true

!        If audali:Date = local:LastJobsCheckedDate
!            If audali:Time < local:LastJobsCheckedTime
!                !If status change was today, but before the last export, skip  (DBH: 23-11-2004)
!                Cycle
!            End !If audali:Time < local:LastJobsCheckedTime
!        End
!
!        if audali:Date > dateNow
!            break
!        else
!            if audali:Date = dateNow
!                ! Don't process records for the next run
!                if audali:Time > timeNow then cycle.
!            end
!        end

        If local:CountJobs > 500
            ! To keep the export files from getting too large don't allow more than 500 records to be exported,
            ! if this happens update the last run time to the current record, so we continue from this point
            ! on the next run
            !Save the time of this export  (DBH: 23-11-2004)
            PUTINI('Export','Jobs Checked Date',Format(audali:Date,@d06),CLIP(Path()) & '\CRCEXPORT.INI')
            PUTINI('Export','Jobs Checked Time',Format(audali:Time,@t01),CLIP(Path()) & '\CRCEXPORT.INI')
            PUTINI('Export','NextRecordNumber',local:recordNumber,CLIP(Path()) & '\CRCEXPORT.INI')
            !End   - Save the time of this export  (DBH: 23-11-2004)
            break ! Stop the scheduled task from creating large export files
        end

        local:SendStatusAlert = 0

        If Instring('JOB CREATED BY WEBMASTER',audali:Notes,1,1)
            !Is this a new job, or an updated one  (DBH: 23-11-2004)
            local:NewJob = True
            local:JobAlreadyExported = False
            !Start - Has this Yeah thnew job already been exported?  (DBH: 23-11-2004)
            Save_aud_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = audali:Ref_Number
            aud:Action = 'JOB EXPORTED TO WILFRED'
            Set(aud:Action_Key,aud:Action_Key)
            Loop ! Begin AUDIT Loop
                If Access:AUDIT.Next()
                    Break
                End ! If !Access
                If aud:Ref_Number <> audali:Ref_Number
                    Break
                End ! If
                If aud:Action <> 'JOB EXPORTED TO WILFRED'
                    Break
                End ! If
                local:JobAlreadyExported = True
                Break
            End ! End AUDIT Loop
            Access:AUDIT.RestoreFile(Save_aud_ID)

            if local:JobAlreadyExported then cycle.

        Else ! If audali:Action = 'NEW JOB INITIAL BOOKING'
            !Is this a new job, or an updated one?  (DBH: 23-11-2004)
            local:NewJob = False

            If Instring('STATUS CHANGED TO',audali:Action,1,1) Or Instring('JOB UPDATED BY WILFRED',audali:Action,1,1)
                ! We now only send jobs to wilfred once they reach a status of despatched from store.
                ! It is possible the job is booked but not despatched within the 5 days we are looking over.
                ! Therefore first check that this job has been exported to wilfred

                local:JobAlreadyExported = False
                Save_aud_ID = Access:AUDIT.SaveFile()
                Access:AUDIT.Clearkey(aud:Action_Key)
                aud:Ref_Number = audali:Ref_Number
                aud:Action = 'JOB EXPORTED TO WILFRED'
                Set(aud:Action_Key,aud:Action_Key)
                Loop ! Begin AUDIT Loop
                    If Access:AUDIT.Next()
                        Break
                    End ! If !Access
                    If aud:Ref_Number <> audali:Ref_Number
                        Break
                    End ! If
                    If aud:Action <> 'JOB EXPORTED TO WILFRED'
                        Break
                    End ! If
                    local:JobAlreadyExported = True
                    Break
                End ! End AUDIT Loop
                Access:AUDIT.RestoreFile(Save_aud_ID)

                if local:JobAlreadyExported = False
                    local:NewJob = True
                End

                ! See if the status change needs to send an alert (DBH: 23/11/2007)
                local:SendStatusAlert = 1

            Else ! If Instring('STATUS CHANGED TO',audali:Action,1,1)
                !Only looking for Status Changes  (DBH: 23-11-2004)
                Cycle
            End ! If Instring('STATUS CHANGED TO',audali:Action,1,1)
            
        End ! If audali:Action = 'NEW JOB INITIAL BOOKING'

        local:TheJobNumber = audali:Ref_Number
        do ProcessJob
        if local:exported = false
            Access:SIDAUD.ClearKey(sad:AuditRecordNumberKey)
            sad:AuditRecordNumber = audali:record_number
            if Access:SIDAUD.Fetch(sad:AuditRecordNumberKey) <> Level:Benign
                sad:AuditRecordNumber = audali:record_number
                sad:EntryDate = today()
                sad:EntryTime = clock()
                Access:SIDAUD.TryInsert()
            end
        end
    End !Loop

!    ! Exchanges - Removed this section, future exchanges will end up in the SIDAUD table when they are attempted to be exported.
     !           - Records are kept alive in SIDAUD for 14 days which will cover all exchanges

!    CCExchFile{Prop:SQL} = 'SELECT ExchangeDate, TimeSlotID, DeliveryCharge, TransitDays, WebJobNo FROM CCExchange WHERE ExchangeDate > ''' & format(today() - 14, @d10-) & ''''
!    loop
!        next(CCExchFile)
!        if error() then break.
!        SQLFile{prop:SQL} = 'SELECT SBJobNo, BookingType, RepairType, PrimaryFault, Reason, CustCollectionDate, RetStoreCode, BookedBy, SMSReq, EmailReq, CountryDelivery, FreeTextFault, DeviceCondition, UnitCondition, Phone_Lock, SMSAlertNo FROM WebJob Where SBJobNo = ' & ccex:WebJobNo
!        next(SQLFile)
!        if error() then cycle.
!        ExchangeQ.JobNo =  SQL:SQLSBJobNo
!        add(ExchangeQ, ExchangeQ.JobNo)
!    end
!
!    i = 1
!
!    loop 
!        get(ExchangeQ, i)
!        if error() then break.
!
!        If local:CountJobs > 200 then break. ! Stop the scheduled task from creating large export files
!
!        local:SendStatusAlert = 0
!        local:NewJob = True
!        local:JobAlreadyExported = False
!
!        !Start - Has this new job already been exported?  (DBH: 23-11-2004)
!        Save_aud_ID = Access:AUDIT.SaveFile()
!        Access:AUDIT.Clearkey(aud:Action_Key)
!        aud:Ref_Number = ExchangeQ.JobNo
!        aud:Action = 'JOB EXPORTED TO WILFRED'
!        Set(aud:Action_Key,aud:Action_Key)
!        Loop ! Begin AUDIT Loop
!            If Access:AUDIT.Next()
!                Break
!            End ! If !Access
!            If aud:Ref_Number <> ExchangeQ.JobNo
!                Break
!            End ! If
!            If aud:Action <> 'JOB EXPORTED TO WILFRED'
!                Break
!            End ! If
!            local:JobAlreadyExported = True
!            Break
!        End ! End AUDIT Loop
!        Access:AUDIT.RestoreFile(Save_aud_ID)
!
!        if local:JobAlreadyExported then cycle.
!
!        ! Fetch the NEW JOB INITIAL BOOKING entry for this job
!        Access:AUDIT_ALIAS.ClearKey(audali:Ref_Number_Key)
!        audali:Ref_Number = ExchangeQ.JobNo
!        set(audali:Ref_Number_Key, audali:Ref_Number_Key)
!        loop
!            if Access:AUDIT_ALIAS.Next() <> Level:Benign then break.
!            if audali:Ref_Number <> ExchangeQ.JobNo then break.
!            If Instring('NEW JOB INITIAL BOOKING',audali:Action,1,1)
!                local:TheJobNumber = ExchangeQ.JobNo
!                do ProcessJob
!                break
!            end
!        end
!
!        i += 1
!    end
    Close(SQLFile)
    Close(CCExchFile)
    Close(TimeSlotFile)
    Close(WebJobExFile)
    Close(WebRetFile)
    Close(JobTxtFile)
    Close(KnownIssueFile)

    ! Reminder Maintenance. Go through any "reminder" alerts and check if the job is still at the correc status. If not clear. (DBH: 25/09/2007)
    Access:PENDMAIL_ALIAS.Clearkey(pem_ali:ReminderKey)
    pem_ali:Reminder = 1
    Set(pem_ali:ReminderKey,pem_ali:ReminderKey)
    Loop ! Begin Loop
        If Access:PENDMAIL_ALIAS.Next()
            Break
        End ! If Access:PENDMAIL_ALIAS.Next()
        If pem_ali:Reminder <> 1
            Break
        End ! If pem_ali:Reminder <> 1

        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = pem_ali:RefNumber
        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Found
            If pem_ali:Status <> Sub(job:Current_Status,1,3)
                Access:PENDMAIL.ClearKey(pem:RecordNumberKey)
                pem:RecordNumber = pem_ali:RecordNumber
                If Access:PENDMAIL.TryFetch(pem:RecordNumberKey) = Level:Benign
                    !Found
                    pem:Reminder = 0
                    Access:PENDMAIL.TryUpdate()
                Else ! If Access:PENDMAIL.TryFetch(pem:RecordNumberKey) = Level:Benign
                    !Error
                End ! If Access:PENDMAIL.TryFetch(pem:RecordNumberKey) = Level:Benign

            End ! If pem_ali:Status <> Sub(job:Current_Status,1,3)
        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Error
        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

    End ! Loop


    Relate:JOBS.Close()
    Relate:DEFAULTS.Close()
    Relate:USERS.Close()
    Relate:EXCHANGE.Close()
    Relate:DESBATCH.Close()
    Relate:STATUS.Close()
    Relate:AUDIT_ALIAS.Close()
    Relate:PENDMAIL.Close()
    Relate:PENDMAIL_ALIAS.Close()
    Relate:DEFEMAI2.Close()
    Relate:SIDSRVCN.Close()
    Relate:SIDMODSC.Close()
    Relate:SIDDEF.Close()
    Relate:CARISMA.Close()
    Relate:SIDNWDAY.Close()
    !Relate:SIDEXUPD.Close()
    Relate:SIDALERT.Close()
    Relate:SIDREMIN.Close()
    Relate:SMOALERT.Close()
    Relate:SMOREMIN.Close()
    Relate:SIDALDEF.Close()
    Relate:MODELNUM.Close()
    Relate:IMEISHIP.Close()
    Relate:SIDAUD.Close()

!    ErrorLog('Jobs Read: ' & local:CountJobs)
!    ErrorLog('SID Jobs Found: ' & local:SIDJobCount)
!    ErrorLog('Non-SID Jobs Found: ' & local:NonSIDJobCount)

    !If local:CountJobs = 0
    !    Remove(ExportFile)
    !End !Else !If local:CountJobs = 0

        !Directory(TempFiles,Clip(Path()) & '\WilfredToSend\sid_?????????????.tmp',ff_:DIRECTORY)
        Directory(TempFiles,Clip(Path()) & '\WilfredToSend\sid*.tmp',ff_:DIRECTORY)
        Loop x# = 1 To Records(TempFiles)
            Get(TempFiles,x#)
            If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
                Cycle
            Else !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
!                ErrorLog('Begin FTP: ' & Clip(fil:name) & ' - ' & Round(fil:Size/1024,.01) & 'kb')
                ! Get from queue
                local:SCAccountID = 0
                loop y# = 1 to records(ExportQueue)
                    get(ExportQueue, y#)
                    If Upper(Clip(fil:name)) = Upper(Clip(ExportQueue.ExportFileName)) & '.TMP'
                        local:SCAccountID = ExportQueue.SCAccountID
                        Break
                    End
                end
                if local:SCAccountID = 0
                    if ((len(clip(fil:name)) > 4) and (upper(fil:name[1:4]) = 'SID_'))
                        ! Old format
                        local:SCAccountID = 1
                    else
                         if ((len(clip(fil:name)) > 3) and (upper(fil:name[1:3]) = 'SID'))
                            ! New format
                            pos# = instring('_', clip(fil:name), 1, 1) - 1
                            if pos# > 3
                                local:SCAccountID = fil:Name[4:pos#]
                            end
                         end
                    end
                end
                FTP_JumpStart(Clip(Path()) & '\WilfredToSend\' & Clip(fil:name),Sub(fil:Name,1,Len(Clip(fil:Name))-4), local:SCAccountID)
            End !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
        End !x# = 1 To Records(ScanFiles)
    !End !If local:CountJobs > 0
ProcessJob routine

        local:exported = False
        local:skipJob = False

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = local:TheJobNumber
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Found
        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
            Stop(Error() & '<13,10>Jobs: ' & local:TheJobNumber)
            Exit
        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = local:TheJobNumber
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
            
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
            If Access:JOBSE.PrimeRecord() = Level:Benign
                jobe:RefNumber = job:Ref_Number
                If Access:JOBSE.TryInsert() = Level:Benign
                    ! Insert Successful

                Else ! If Access:JOBSE.TryInsert() = Level:Benign
                    ! Insert Failed
                    Access:JOBSE.CancelAutoInc()
                End ! If Access:JOBSE.TryInsert() = Level:Benign
            End !If Access:JOBSE.PrimeRecord() = Level:Benign
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
     
        Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
        jbn:RefNumber  = job:Ref_Number
        If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
            !Found
            
        Else ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
            !Error
            Stop(Error() & '<13,10>Jobnotes: ' & local:TheJobNumber)
            Exit
        End ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign

        ! SQLFile{prop:SQL} = 'SELECT SBJobNo, BookingType, RepairType, PrimaryFault, Reason, CustCollectionDate, RetStoreCode, BookedBy, SMSReq, EmailReq, CountryDelivery, FreeTextFault, DeviceCondition, UnitCondition, Phone_Lock, SMSAlertNo,Intermittent_Fault,TitleCollection,InitialCollection,SurnameCollect,TitleDelivery,InitialDelivery,SurnameDelivery FROM WebJob Where SBJobNo = ' & job:Ref_Number
        SQLFile{prop:SQL} = 'SELECT SBJobNo, BookingType, RepairType, PrimaryFault, Reason, CustCollectionDate, RetStoreCode, BookedBy, SMSReq, EmailReq, CountryDelivery, FreeTextFault, DeviceCondition, UnitCondition, Phone_Lock, SMSAlertNo FROM WebJob Where SBJobNo = ' & job:Ref_Number
        Loop
            Next(SQLFile)
            If Error()
                local:SIDBooking = False
                local:NonSIDJobCount += 1
                Break
            End ! If Error()
            ! TRKBASE 5836 GK 30/6/2005 - booking records from retail stores are only communicated to Wilfred when the job has reached the status of 135 Despatched From Store.
            If sql:SQLBookingType = 'R' ! Retail jobs only
                ! Was going to look through the status audit trail but checked with Nick, now just
                ! check if the current status is equal to or greater than 135 DESPATCHED FROM STORE
                local:strStatusCode = Sub(job:current_status, 1, 3)
                local:statusCode = local:strStatusCode
                If local:statusCode < 135
                    local:skipJob = True
                Else
                    ! Added by Gary (17/10/2005) - These variables were not being set due to the break.
                    local:SIDBooking = True
                    local:TheJobNumber = sql:SQLSBJobNo
                    local:SIDJobCount += 1
                End
                Break
            End

            local:SIDBooking = True
            local:TheJobNumber = sql:SQLSBJobNo
            local:SIDJobCount += 1
            Break
        End

        If local:SendStatusAlert = 1
            ! Look for the newest occurance of an SMS Message (DBH: 15/04/2008)
            StatusAlreadySent# = 0
            LatestDate# = 0
            LatestTime# = 0
            LastStatus" = ''
            Access:PENDMAIL.Clearkey(pem:RefNumberKey)
            pem:RefNumber = job:Ref_Number
            Set(pem:RefNumberKey,pem:RefNumberKey)
            Loop
                If Access:PENDMAIL.Next()
                    Break
                End ! If Access:PENDMAIL.Next()
                If pem:RefNumber <> job:Ref_Number
                    Break
                End ! If pem:RefNumber <> job:Ref_Number
                If pem:DateCreated > LatestDate#
                    LastStatus" = Sub(pem:Status,1,3)
                    LatestDate# = pem:DateCreated
                    LatestTime# = pem:TimeCreated
                    Cycle
                ElsIf pem:DateCreated = LatestDate#
                    If pem:TimeCreated > LatestTime#
                        LastStatus" = Sub(pem:Status,1,3)
                        LatestDate# = pem:DateCreated
                        LatestTime# = pem:TimeCreated
                        Cycle
                    End ! If pem:TimeSent > LatestTime#
                End ! If pem:DateSent > LatestDate#
            End ! Loop

            ! Found. Was this message for the same status as the job is at now? (DBH: 15/04/2008)
            If LastStatus" <> ''
                If LastStatus" = Sub(job:Current_Status,1,3)
                    StatusAlreadySent# = 1
                End ! If pem:Status = Sub(job:Current_Status,1,3)
            End ! If LastStatus# > 0

            If StatusAlreadySent# = 0
                Case sql:SQLBookingType
                Of 'E' ! Contact Centre - External Exchange
                OrOf 'F' ! Contact Centre - EBU Exchange
                    local.AddToAlertViewer('EXCHANGE',Sub(job:Current_Status,1,3),job:Model_Number)
                Of 'B' ! Contact Centre - Postal
                OrOf 'G' ! Contact Centre - EBU Postal
                    local.AddToAlertViewer('POSTAL',Sub(job:Current_Status,1,3),job:Model_Number)
                Of 'R' ! Retail - Repair
                    local.AddToAlertViewer('RETAIL REPAIR',Sub(job:Current_Status,1,3),job:Model_Number)
!                Of 'Y' ! Retail - In-Store Exchange
!                    local.AddToAlertViewer('RETAIL IN-STORE EXCHANGE',Sub(job:Current_Status,1,3),job:Model_Number)
                Of 'Y' ! Retail Repair (Accessory)  ! #5652 Replace Retail Repair (DBH: 04/06/2015)
                    local.AddToAlertViewer('RETAIL REPAIR (ACC)',Sub(job:Current_Status,1,3),job:Model_Number)
                Of '0' ! Postal (Accessory)  ! #5652 New BookingType (DBH: 04/06/2015)
                    local.AddToAlertViewer('POSTAL (ACC)',Sub(job:Current_Status,1,3),job:Model_Number)
                Of 'Z' ! Retail - External Exchange
                    local.AddToAlertViewer('RETAIL EXTERNAL EXCHANGE',Sub(job:Current_Status,1,3),job:Model_Number)
                Of 'U' ! Contact Centre - 7 Day Exchange
                    local.AddToAlertViewer('7 DAY EXCHANGE',Sub(job:Current_Status,1,3),job:Model_Number)
                End ! Case sql:SQLBookingType
            End ! If StatusAlreadySent# = 0
        End ! If SendAlert# = 1
        ! End (DBH 21/09/2007) #8943

        If Instring('JOB UPDATED BY WILFRED',audali:Action,1,1)
            ! Don't need to go any further
            local:exported = True
            Exit
        End ! If Instring('JOB UPDATED BY WILFRED',audali:Action,1,1)

        ! GK 23/04/2008 - Do not process any 'INITIAL STATUS' audit entries.
        ! These exist for the alert notifications only.
        If Instring('INITIAL STATUS:',audali:notes,1,1)
            local:exported = True
            Exit
        End


        !Has this job already been included in this export?  (DBH: 23-11-2004)
        jobque:JobNumber = local:TheJobNumber
        Get(JobsQueue,jobque:JobNumber)
        If ~Error()
            local:exported = True
            Exit
        End !If ~Error()

        ! Start - There is no thin client, so if cannot get webjob, don't continue (DBH: 20-04-2006)
        If local:SIDBooking = False
            local:exported = True
            Exit
        End ! If local:SIDBooking = False
        ! End   - There is no thin client, so if cannot get webjob, don't continue (DBH: 20-04-2006)

        ! Temp fix for when we had a mass update of jobs to 797 B2B EXPIRED
        !If Sub(job:Current_Status,1,3) = '797'
        !    local:exported = True
        !    Exit
        !End ! If Sub(job:Current_Status) = '135'
        ! End (DBH 18/09/2006) #N/A

        ! Inserting (DBH 18/09/2006) # N/A - Do not export 135 Despatch From Store.. This should be covered by the initial job export for retail jobs
        If Sub(job:Current_Status,1,3) = '135' And local:NewJob = False
            local:exported = True
            Exit
        End ! If Sub(job:Current_Status) = '135'
        ! End (DBH 18/09/2006) #N/A

        ! Start - Do not send status's 930 and 940 (DBH: 13-03-2006)
        If Sub(job:Current_Status,1,3) = '930' Or Sub(job:Current_Status,1,3) = '940'
            local:SkipJob = True
        End ! If Sub(job:Current_Status,1,3) = '930' Or Sub(job:Current_Status,1,3) = '940'
        ! End   - Do not send status's 930 and 940 (DBH: 13-03-2006)

        ! (GK 18/11/2008) Do not export 798 JOB CANCELLED BY VODAFONE unless the job has already been exported.
        If Sub(job:Current_Status,1,3) = '798' And local:NewJob = True
            local:SkipJob = True
        End

        If local:skipJob = True
            local:exported = True
            exit
        End

        ! Don't export resolved jobs
        If sql:SQLBookingType = 'V'
            local:exported = True
            exit
        End

        ! Map the booking type for the lookups below
        Case sql:SQLBookingType
            Of 'E' ! Contact Centre - External Exchange
            OrOf 'B' ! Contact Centre - Postal
            OrOf 'U' ! Contact Centre - 7 Day Exchange
            OrOf 'R' ! Retail - Repair
            OrOf 'W' ! Retail - Exchange - 7 Day
            OrOf 'Q' ! Retail - Customer Returns
            OrOf 'O' ! Retail - Other Returns
                local:MappedBookingType = sql:SQLBookingType
            Of 'Y' ! Retail - In-Store Exchange
                local:MappedBookingType = 'E'
            Of 'Z' ! Retail - External Exchange
                local:MappedBookingType = 'E'
            Of 'S' ! Retail - Store Returns
                local:MappedBookingType = 'Q'
            Of 'T' ! Retail - Store Recall
                local:MappedBookingType = 'Q'
            Of 'X' ! Retail - FEAST Returns
                local:MappedBookingType = 'Q'
            Of 'H' ! Contact Centre - Hardware Returns
                local:MappedBookingType = 'Q'
            Of 'J' ! Contact Centre - Employee Returns
                local:MappedBookingType = 'Q'
            Of 'D' ! Contact Centre - Package
                local:MappedBookingType = 'Q'
            Of 'I' ! Contact Centre - Ineligible
                local:MappedBookingType = 'Q'
            Of 'F' ! EBU Exchange
                local:MappedBookingType = 'E'
            Of 'G' ! EBU Postal
                local:MappedBookingType = 'B'
        End ! Case sql:SQLBookingType

        ! Get Service Centre
        local:SCAccountID = 0
        local:InterTimeFrame = ''
        local:ServiceCharge = 0
        local:DiagAvailable = 0
        local:Forename = ''
        local:NoExceptionDate = ''
        local:ExceptionDate = ''
        local:KnownIssueID = 0

        WebJobExFile{Prop:SQL} = 'SELECT SCAccountID, ManifestNo, InterTimeFrame, ServiceCharge, DiagAvailable, Forename, NoExceptionDate, KnownIssueID FROM WebJobEx WHERE WebJobNo = ' & clip(job:Order_Number)
        next(WebJobExFile)
        if not error()
            local:SCAccountID = webex:SCAccountID
            local:ManifestNo = webex:ManifestNo
            local:InterTimeFrame = webex:InterTimeFrame
            local:ServiceCharge = webex:ServiceCharge
            local:DiagAvailable = webex:DiagAvailable
            local:Forename = webex:Forename
            local:NoExceptionDate = webex:NoExceptionDate
            local:KnownIssueID = webex:KnownIssueID
        end
        
        If local:SCAccountID = 0
            If sql:SQLBookingType = 'D' or sql:SQLBookingType = 'I'
                ! Package /Ineligible returns do not have a model number, therefore the below lookup will fail. Hard code all these
                ! jobs to 4 (smart returns) as advised by Nick.
                local:SCAccountID = 4
            Else
                ! No Service Centre specified, attempt lookup of attached make/model
                Access:SIDMODSC.ClearKey(msc:ManModelTypeKey)
                msc:Manufacturer = Clip(job:Manufacturer)
                msc:ModelNo = Clip(job:Model_Number)
                msc:BookingType = local:MappedBookingType ! Retail / Exchange / Back To Base / 7 Day Replacement / Returns
                If Access:SIDMODSC.Fetch(msc:ManModelTypeKey) = Level:Benign
                    local:SCAccountID = msc:SCAccountID
                End
            End

            If local:SCAccountID > 0
                ! Update record with account ID
                WebJobExFile{Prop:SQL} = 'UPDATE WebJobEx SET SCAccountID = ' & local:SCAccountID & ' WHERE WebJobNo = ' & clip(job:Order_Number)
            End
        End

        ! Return jobs - 11/03/2009 GK
        local:returnJob = False

        if sql:SQLBookingType = 'Q' Or sql:SQLBookingType = 'S' Or sql:SQLBookingType = 'T' Or sql:SQLBookingType = 'W' Or sql:SQLBookingType = 'X' Or |
           sql:SQLBookingType = 'H' Or sql:SQLBookingType = 'J' Or sql:SQLBookingType = 'D' Or sql:SQLBookingType = 'I' Or sql:SQLBookingType = 'O' ! Customer returns
            WebRetFile{Prop:SQL} = 'SELECT RetReasonCode, RetReasonName, ReturnException FROM WebRet WHERE WebJobNo = ' & clip(job:Order_Number)
            next(WebRetFile)
            if not error()
                local:returnJob = True
            end
        end

        local:AdditionalSymptoms = ''
        JobTxtFile{Prop:SQL} = 'SELECT Text1, Text2 FROM JobTxt WHERE TxtType = 1 AND WebJobNo = ' & clip(job:Order_Number)
        next(JobTxtFile)
        if not error()
            ! Additional symptoms is split over several fields
            if clip(jobt:Text1) <> ''
                local:AdditionalSymptoms = clip(jobt:Text1)
            end
            if clip(jobt:Text2) <> ''
                local:AdditionalSymptoms = clip(local:AdditionalSymptoms) & clip(jobt:Text2)
            end
        end

        local:KnownIssueSummary = ''

        if local:KnownIssueID > 0
            KnownIssueFile{Prop:SQL} = 'SELECT Summary FROM KnownIssue WHERE KnownIssueID = ' & clip(local:KnownIssueID)
            next(KnownIssueFile)
            if not error()
                local:KnownIssueSummary = clip(knis:Summary)
            end
        end

        ! Exchange jobs - 12/09/2006 rule change.
        ! All exchange jobs that are due for an exchange on a Tuesday will be communicated in an interface file after 6pm on Friday onwards until 6pm Monday.
        local:exchangeJob = False
        
        CCExchFile{Prop:SQL} = 'SELECT ExchangeDate, TimeSlotID, DeliveryCharge, TransitDays, WebJobNo FROM CCExchange WHERE WebJobNo = ' & clip(job:Order_Number)
        next(CCExchFile)
        if not error()
            local:exchangeJob = True
            ! The interface routine will pass job booking data for retail exchanges that are to be delivered
            ! to a retail store to the service centre, immediately.
            if sql:SQLBookingType <> 'Y' ! Retail - In-Store Exchanges sent immediately
                ! Contact Centre / Retail External Exchanges
                ! SID will delay the transmission of the Exchange bookings to CRC until the day before the exchange is required.
                !
                ! Monday - Sent On Friday before 6pm
                ! Tuesday - Sent On Friday after 6pm, Saturday, Sunday, Monday
                ! Wednesday - Sent On Tuesday
                ! Thursday - Sent On Wednesday
                ! Friday - Sent On Thursday
                !

                ! Get the transit days
                local:transitDays = ccex:TransitDays
                if local:transitDays = 0 then local:transitDays = 1.

                ! 15/08/2008 GK - Change of logic on how the interface file is sent

!                case (today() % 7)
!                    of 0 ! SUN
!                        ! Send Tuesdays
!                        if local.GetWorkDate(local:SCAccountID, SQL:SQLCountryDelivery, today(), local:transitDays) < ccex:ExchangeDate Then Cycle.
!                    of 1 ! MON
!                    orof 2 ! TUES
!                    orof 3 ! WED
!                    orof 4 ! THUR
!                        if clock() < deformat('18:00', @T1)
!                            ! Before 6pm - Send Next Day
!                            if local.GetWorkDate(local:SCAccountID, SQL:SQLCountryDelivery, today(), local:transitDays) < ccex:ExchangeDate Then Cycle.
!                        else
!                            ! After 6pm - Add extra day
!                            if local.GetWorkDate(local:SCAccountID, SQL:SQLCountryDelivery, today(), local:transitDays + 1) < ccex:ExchangeDate Then Cycle.
!                        end
!                    of 5 ! FRI
!                        if clock() < deformat('18:00', @T1)
!                            ! Before 6pm - Send Mondays
!                            if local.GetWorkDate(local:SCAccountID, SQL:SQLCountryDelivery, today(), local:transitDays) < ccex:ExchangeDate Then Cycle.
!                        else
!                            ! After 6pm - Send Tuesdays
!                            if local.GetWorkDate(local:SCAccountID, SQL:SQLCountryDelivery, today(), local:transitDays + 1) < ccex:ExchangeDate Then Cycle.
!                        end
!                    of 6 ! SAT
!                        ! Send Tuesdays
!                        if local.GetWorkDate(local:SCAccountID, SQL:SQLCountryDelivery, today(), local:transitDays) < ccex:ExchangeDate Then Cycle.
!                end ! case

                ! NICK REQUESTED THIS CHANGE 15/08/2008
                if local.IsNonWorkingDay(local:SCAccountID, SQL:SQLCountryDelivery) = true then exit. ! Don't send files on non-working days
                if local.GetWorkDate(local:SCAccountID, SQL:SQLCountryDelivery, today(), local:transitDays) < ccex:ExchangeDate Then exit.
            end
        end

        local:OracleCode = ''

! Insert --- New booking types pick up the oracle code from the imeiship file (DBH: 12/02/2009) #10672
        If sql:SQLBookingType = 'Q' Or sql:SQLBookingType = 'S' Or sql:SQLBookingType = 'T' Or sql:SQLBookingType = 'W' Or sql:SQLBookingType = 'X' Or |
           sql:SQLBookingType = 'H' Or sql:SQLBookingType = 'J' Or sql:SQLBookingType = 'O' Or sql:SQLBookingType = 'U'
            Access:IMEISHIP.Clearkey(imei:IMEIKey)
            imei:IMEINumber    = job:ESN
            If Access:IMEISHIP.TryFetch(imei:IMEIKey) = Level:Benign
                ! Found
                local:OracleCode = imei:ProductCode
            Else ! If Access:IMEISHIP.TryFetch(imei:IMEIKey) = Level:Benign
                ! Error
            End !If Access:IMEISHIP.TryFetch(imei:IMEIKey) = Level:Benign
        Elsif sql:SQLBookingType = 'D'
            ! Package
            local:OracleCode = '800500'
        Elsif sql:SQLBookingType = 'I'
            ! Ineligible
            local:OracleCode = '800501'
        Else ! If sql:SQLBookingType = 'Q' Or sql:SQLBookingType = 'S' Or sql:SQLBookingType = 'T' Or sql:SQLBookingType = 'W'
! End --- (DBH: 12/02/2009) #10672
            Access:CARISMA.ClearKey(cma:ManufactModColourKey)
            cma:Manufacturer = Clip(job:Manufacturer)
            cma:ModelNo      = Clip(job:Model_Number)
            cma:Colour       = Clip(job:Colour)
            if Access:CARISMA.Fetch(cma:ManufactModColourKey) = Level:Benign
                If Sub(sql:SQLPrimaryFault,1,1) = 'C'
                    local:ContractType = 'Contract'
                    If cma:ContractActive
                        local:OracleCode = cma:ContractOracleCode
                    Else ! If cma:OracleActive
                        If cma:PAYTActive
                            local:OracleCode = cma:PAYTOracleCode
                        End ! If cma:PAYTActive
                    End ! If cma:OracleActive
                Else
                    If Sub(sql:SQLPrimaryFault,1,1) = 'P'
                        local:ContractType = 'PAYT'
                        If cma:PAYTActive
                            local:OracleCode = cma:PAYTOracleCode
                        Else ! If cma:PAYTActive
                            If cma:ContractActive
                                local:OracleCode = cma:ContractOracleCode
                            End ! If cma:ContractActive
                        End ! If cma:PAYTActive
                    End
                End
            end

            ! New booking types may not have Oracle Codes to start with, so don't fail them (DBH: 02/03/2009)
            if local:OracleCode = ''
                ! Cannot find carisma code - send an alert email
                local:emailMessage = 'No ' & clip(local:ContractType) & ' Oracle Code has been allocated to the following combination : ' & clip(job:Manufacturer) & ' ' & clip(job:Model_Number) & ' ' & |
                                     clip(job:Colour) & '<13,10>SID job number ' & Clip(job:Order_Number) & ' cannot be exported until an Oracle Code has been setup.'

                SendEmail(SID:ContactEmail, 'SID UNALLOCATED ORACLE CODE ALERT', local:emailMessage)
                exit
            end

        End ! If sql:SQLBookingType = 'Q' Or sql:SQLBookingType = 'S' Or sql:SQLBookingType = 'T' Or sql:SQLBookingType = 'W'

        ! If Service Centre not found Send unallocated model alert email
        If local:SCAccountID = 0
            local:emailMessage = 'The following job has an unallocated model :- <13,10,13,10>' & |
                                 'SID Job Number: ' & clip(job:Order_Number) & '<13,10>' & |
                                 'Manufacturer: ' & clip(job:Manufacturer) & '<13,10>' & |
                                 'Model: ' & clip(job:Model_Number) & '<13,10>'

            case sql:SQLBookingType
                of 'R'
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: RETAIL<13,10>'
                of 'E'
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: EXCHANGE<13,10>'
                of 'B'
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: BACK TO BASE<13,10>'
                ! Inserting (DBH 21/09/2007) # 8943 - Add new booking type
                of 'Y' ! Retail - In-Store Exchange
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: RETAIL EXCHANGE<13,10>'
                ! End (DBH 21/09/2007) #8943
                of 'Z' ! Retail - External Exchange
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: EXCHANGE<13,10>'
                of 'W' ! Retail - 7 Day Replacement
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: 7 DAY REPLACEMENT<13,10>'
                of 'Q' ! Retail - Customer Returns
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: CUSTOMER RETURNS<13,10>'
                of 'S' ! Retail - Store Returns
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: STORE RETURNS<13,10>'
                of 'T' ! Retail - Store Recall
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: STORE RECALL<13,10>'
                of 'X' ! Retail - FEAST Returns
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: FEAST RETURNS<13,10>'
                of 'O' ! Retail - Other Returns
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: OTHER RETURNS<13,10>'
                of 'H' ! Contact Centre - Hardware Return
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: HARDWARE RETURNS<13,10>'
                of 'J' ! Contact Centre - Employee Return
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: EMPLOYEE RETURNS<13,10>'
                of 'D' ! Contact Centre - Package
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: PACKAGE<13,10>'
                of 'I' ! Contact Centre - Ineligible
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: INELIGIBLE<13,10>'
                of 'F' ! Contact Centre - EBU Exchange
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: EBU EXCHANGE<13,10>'
                of 'G' ! Contact Centre - EBU Postal
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: EBU POSTAL<13,10>'
                of 'U' ! Contact Centre - 7 Day Exchange
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: 7 DAY EXCHANGE<13,10>'
                else
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: ' & sql:SQLBookingType & '<13,10>'
            end

            SendEmail(SID:ContactEmail, 'SID UNALLOCATED MODEL ALERT', local:emailMessage)
            exit ! Skip this job
        End

        ! If Service Centre inactive then Send Email
        Access:SIDSRVCN.ClearKey(srv:SCAccountIDKey)
        srv:SCAccountID = local:SCAccountID
        If Access:SIDSRVCN.Fetch(srv:SCAccountIDKey) = Level:Benign
            If srv:AccountActive = false
                local:emailMessage = 'The following job has been allocated to an inactive service centre :- <13,10,13,10>' & |
                                     'SID Job Number: ' & clip(job:Order_Number) & '<13,10>' & |
                                     'Service Centre: ' & clip(srv:ServiceCentreName) & '<13,10>' & |
                                     'Manufacturer: ' & clip(job:Manufacturer) & '<13,10>' & |
                                     'Model: ' & clip(job:Model_Number) & '<13,10>'

                case sql:SQLBookingType
                    of 'R'
                        local:emailMessage = clip(local:emailMessage) & 'Booking Type: RETAIL<13,10>'
                    of 'E'
                        local:emailMessage = clip(local:emailMessage) & 'Booking Type: EXCHANGE<13,10>'
                    of 'B'
                        local:emailMessage = clip(local:emailMessage) & 'Booking Type: BACK TO BASE<13,10>'
                    of 'F'
                        local:emailMessage = clip(local:emailMessage) & 'Booking Type: EBU EXCHANGE<13,10>'
                    of 'G'
                        local:emailMessage = clip(local:emailMessage) & 'Booking Type: EBU POSTAL<13,10>'
                    else
                        local:emailMessage = clip(local:emailMessage) & 'Booking Type: ' & sql:SQLBookingType & '<13,10>'
                end

                SendEmail(SID:ContactEmail, 'SID SERVICE CENTRE INACTIVE ALERT', local:emailMessage)
                Exit ! Skip this job
            End
        Else
            ! Service centre unknown
            local:emailMessage = 'The following job has been allocated to an unknown service centre :- <13,10,13,10>' & |
                                     'SID Job Number: ' & clip(job:Order_Number) & '<13,10>' & |
                                     'Service Centre ID: ' & clip(local:SCAccountID) & '<13,10>' & |
                                     'Manufacturer: ' & clip(job:Manufacturer) & '<13,10>' & |
                                     'Model: ' & clip(job:Model_Number) & '<13,10>'

            case sql:SQLBookingType
                of 'R'
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: RETAIL<13,10>'
                of 'E'
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: EXCHANGE<13,10>'
                of 'B'
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: BACK TO BASE<13,10>'
                of 'F'
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: EBU EXCHANGE<13,10>'
                of 'G'
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: EBU POSTAL<13,10>'
                else
                    local:emailMessage = clip(local:emailMessage) & 'Booking Type: ' & sql:SQLBookingType & '<13,10>'
            end

            SendEmail(SID:ContactEmail, 'SID UNKNOWN SERVICE CENTRE ALERT', local:emailMessage)
            Exit ! Skip this job
        End

        ! Had to split into a second embed - text truncates
        ! GK 30/04/2007 TrackerBase 8905 : Send a copy of CRC records to SMART
        local:sendCopyToSMART = false
        local:SMARTCopy = false

        loop
            if local:SMARTCopy = true
                local:SCAccountID = 2 ! SMART
            end

            ! We have a job to export - fetch the Service Centre specific filename
            ExportQueue.SCAccountID = local:SCAccountID
            get(ExportQueue, ExportQueue.SCAccountID)
            if error() then break.

            tmp:ExportFile = Clip(ExportQueue.ExportFileName) & '.tmp'
            if exists(tmp:ExportFile) = False
                Create(ExportFile)
            end

            if local:SCAccountID = 1 ! CRC job
                local:sendCopyToSMART = true
            end

            Open(ExportFile)


            !Let's export a job. Has it got any accessories? - TrkBs:  (DBH: 01-02-2005)
            local:FoundJob = True

            !Include One Line Per Accessory
            local:IsThereAnAccessory = False
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = job:Ref_Number
            Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
            Loop
                If Access:JOBACC.NEXT()
                   Break
                End !If
                If jac:Ref_Number <> job:Ref_Number      |
                    Then Break.  ! End If
                local:IsThereAnAccessory = True
                !Job Number
                expfil:Line = Clip(job:Ref_Number)
                If local:NewJob = False
                    Case Sub(audali:Action,20,3)
                    Of '220' !Return Address Changed
                        expfil:Line = Clip(expfil:Line) & '<9><9><9><9><9><9><9><9><9><9><9>'
                        !Delivery Company Name
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Company_Name_Delivery),' ')
                        !Delivery Address Line 1
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line1_Delivery),' ')
                        !Delivery Address Line 2
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line2_Delivery),' ')
                        !Delivery Address Line 3
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line3_Delivery),' ')
                        !Delivery Postcode
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(FormatPostcode(job:Postcode_Delivery),' ')
                        !Delivery Telephone Number
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Telephone_Delivery),' ')

                        expfil:LIne = Clip(expfil:LIne) & '<9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9>'
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(audali:Action,20,3)),' ')
                        expfil:Line = Clip(expfil:Line) & '<9><9><9><9>'
                    Of '145' !Re-send Evelope Request
                        expfil:Line = Clip(expfil:Line) & '<9><9><9><9><9>'
                        !Collection Company Name
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Company_Name_Collection),' ')
                        !Collection Address Line1
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line1_Collection),' ')
                        !Collection Address Line 2
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line2_Collection),' ')
                        !Collection Address Line 3
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line3_Collection),' ')
                        !Collection Postcode
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(FormatPostcode(job:Postcode_Collection),' ')
                        !Collection Telephone Number
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Telephone_Collection),' ')
                        expfil:Line = Clip(expfil:Line) & '<9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9>'
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(audali:Action,20,3)),' ')
                        expfil:Line = Clip(expfil:Line) & '<9><9><9><9>'
                    Else ! All other statuses
                        expfil:Line = Clip(expfil:Line) & '<9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9>'
                        !Only pass the status when sending a job update  (DBH: 23-11-2004)
                        !Without Mobile Number - TrkBs:  (DBH: 12-01-2005)
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(audali:Action,20,3)),' ') & '<9><9>'
                    End ! Case Sub(audali:Action,20,3)
                Else ! If local:NewJob = True

                    !SID REf
                    If local:SIDBooking = True
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Order_Number),' ')
                    Else ! If local:SIDBooking = True
                        expfil:Line = Clip(expfil:Line) & '<9>' & ''
                    End ! If local:SIDBooking = True
                    !ACcount Number
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Account_Number),' ')
                    !Title
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Title),' ')
                    !Intial
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(local:Forename),' ')  ! job:Initial
                    !Surname
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Surname),' ')
                    !Collection Company Name
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Company_Name_Collection),' ')
                    !Collection Address Line1
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line1_Collection),' ')
                    !Collection Address Line 2
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line2_Collection),' ')
                    !Collection Address Line 3
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line3_Collection),' ')
                    !Collection Postcode
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(FormatPostcode(job:Postcode_Collection),' ')
                    !Collection Telephone Number - Now SMS Alert number
                    ! expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Telephone_Collection),' ')
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLSMSALertNo),' ')
                    !Delivery Company Name
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Company_Name_Delivery),' ')
                    !Delivery Address Line 1
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line1_Delivery),' ')
                    !Delivery Address Line 2
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line2_Delivery),' ')
                    !Delivery Address Line 3
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line3_Delivery),' ')
                    !Delivery Postcode
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(FormatPostcode(job:Postcode_Delivery),' ')
                    !Delivery Telephone Number
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Telephone_Delivery),' ')
                    !Model Number
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Model_Number),' ')
                    !IMEI Number
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:ESN),' ')
                    !Trade Fault Code 1/2 - GK 30/04/2006 TrackerBase 8905
                    if local:SCAccountID = 1 ! CRC
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jobe:TraFaultCode1),' ')
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jobe:TraFaultCode2),' ')
                    else
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLDeviceCondition),' ')
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLUnitCondition),' ')
                    end
                    !Trade Fault Code 3
                    ! Additional Symptoms - TrackerBase 10978 GK 12/08/2009
                    If clip(local:AdditionalSymptoms) <> ''
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(local:AdditionalSymptoms),' ')
                    Else ! Old way
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLFreeTextFault),' ')
                    End
                    
                    !Trade Fault Code 4
                    If local:SIDBooking = True
                        ! Put who booked into Trade Fault Code 4 (DBH: 04-10-2005)
                        jobe:TraFaultCode4 = Clip(sql:SQLBookedBy)
                        Access:JOBSE.TryUpdate()
                    End ! If local:SIDBooking = True
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jobe:TraFaultCode4),' ')
                    !Accessory
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jac:Accessory),' ')
                    !Date Booked
                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip(Format(Job:Date_Booked,@d06b))
                    !Time Booked
                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip(Format(job:Time_Booked,@t02b))
                    !Repair Flag
                    If local:SMARTCopy = True
                        expfil:Line = Clip(expfil:Line) & '<9>X'
                    Else
                        IF local:SIDBooking = True
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLBookingType),' ')
                        Else ! IF local:SIDBooking = True
                            !Not a SID job. Assume it's an exchange only  (DBH: 22-11-2004)
                            expfil:Line = Clip(expfil:Line) & '<9>E'
                        End ! IF local:SIDBooking = True
                    End
                    
                    !Device Type Flag
                    !Fault Description
                    If local:SIDBooking = True
    ! Changing (DBH 12/06/2006) #7813 - Always write the web job fault description here
    !                     If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
    !                         ! If Primary Fault prefixed with 'C'/'P', strip this from the Primary Fault field (DBH: 04-10-2005)
    !                         expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,1,1)),' ')
    !                         expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(jbn:Fault_Description,3,255)),' ')
    !                     Else ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
    !                         expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
    !                         expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jbn:Fault_Description),' ')
    !                     End ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
    ! to (DBH 12/06/2006) #7813

                            If (sql:SQLBookingType = 'Q' Or sql:SQLBookingType = 'S' Or sql:SQLBookingType = 'T' Or sql:SQLBookingType = 'X' Or sql:SQLBookingType = 'H' Or sql:SQLBookingType = 'J' Or sql:SQLBookingType = 'D' Or sql:SQLBookingType = 'I' Or sql:SQLBookingType = 'O') and local:returnJob = True
                                If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                    ! If Primary Fault prefixed with 'C'/'P', strip this from the Primary Fault field (DBH: 04-10-2005)
                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,1,1)),' ')
                                Else ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                                End ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(webr:RetReasonCode) & ' ' & Clip(webr:RetReasonName), ' ')
                            Else
                                If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                    ! If Primary Fault prefixed with 'C'/'P', strip this from the Primary Fault field (DBH: 04-10-2005)
                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,1,1)),' ')
                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,3,255)),' ')
                                Else ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLPrimaryFault),' ')
                                End ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                ! GK 27/01/2010 - TrackerBase 10984
                                If Len(Clip(local:KnownIssueSummary)) > 0
                                    expfil:Line = clip(expfil:Line) & ' ' & BHStripNonAlphaNum(Clip(local:KnownIssueSummary), ' ')
                                End
                            End

!                            If sql:SQLBookingType = 'Q' and local:returnJob = True
!                                expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(webr:RetReasonCode), ' ')
!                                expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(webr:RetReasonName), ' ')
!                            Else
!                                If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
!                                    ! If Primary Fault prefixed with 'C'/'P', strip this from the Primary Fault field (DBH: 04-10-2005)
!                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,1,1)),' ')
!                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,3,255)),' ')
!                                Else ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
!                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
!                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLPrimaryFault),' ')
!                                End ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
!                            End

    ! End (DBH 12/06/2006) #7813
                    Else ! If local:SIDBooking = True
                        !Not a SID job, don't know the device type  (DBH: 23-11-2004)
                        expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jbn:Fault_Description),' ')
                    End ! If local:SIDBooking = True
                    !Intermittent
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(job:Intermittent_Fault,1,1)),' ')
                    !Job Type
                    If local:SIDBooking = True
                        Case sql:SQLRepairType
                            Of 'IW'
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip('IN MANUFACTURERS WARRANTY')
                            Of 'IV'
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip('IN VODAFONE WARRANTY')
                            Of 'ON'
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip('OUT OF WARRANTY BUT NOT CHARGEABLE')
                            Of 'IR'
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip('INSURANCE REPAIR')
                            Of 'OC'
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip('OUT OF WARRANTY AND CHARGEABLE')
                            Else
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                        End ! Case web:RepairType
                    Else ! If local:SIDBooking = True
                        !Not a SID job, use the job Charge Type  (DBH: 23-11-2004)
                        If job:Warranty_Job = 'YES'
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Warranty_Charge_Type),' ')
                        Else ! If job:Warranty_Job = 'YES'
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Charge_Type),' ')
                        End ! If job:Warranty_Job = 'YES'
                    End ! If local:SIDBooking = True
                    !DOP
                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip(Format(job:DOP,@d06b))
                    !Insurance Ref
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Insurance_Reference_Number),' ')
                    !OutWar Reason
                    If local:SIDBooking = True
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLReason),' ')
                    Else ! If local:SIDBooking = True
                        !Not a SID job, don't know the reason  (DBH: 23-11-2004)
                        expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                    End ! If local:SIDBooking = True
                    
                    !Customer Collection Date
                    If local:SIDBooking = True
                        If local:NoExceptionDate <> ''
                            expfil:Line = clip(expfil:Line) & '<9>' & clip(format(local:NoExceptionDate, @d06b))
                            local:ExceptionDate = sql:SQLCustCollectionDate
                        Else
                            expfil:Line = Clip(expfil:Line) & '<9>' & Clip(Format(sql:SQLCustCollectionDate,@d06b))
                        End
                    Else ! If local:SIDBooking = True
                        !Not a SID job, don't know the Customer Collection Date  (DBH: 23-11-2004)
                        expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                    End ! If local:SIDBooking = True
                    
                    !Status
                    ! Send the status - TrkBs: 6915 (DBH: 09-03-2006)
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(job:Current_Status,1,3)),' ')
                    !Mobile Number
! Changing (DBH 03/07/2008) # 10020 - Show SMS alert no - Back to mobile number now 29/07/2008 GK
                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Mobile_Number),' ')
! to (DBH 03/07/2008) # 10020
                    !expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLSMSALertNo),' ')
! End (DBH 03/07/2008) #10020
                    !Return Account Number
                    ! Only show account number if the "Repair Flag" is "R" - TrkBs: 5717 (DBH: 19-04-2005)
                    If local:SIDBooking = True
                        If Clip(sql:SQLBookingType) = 'R'
                            expfil:Line = Clip(expfil:Line) & '<9>' & Clip(sql:SQLRetStoreCode)
                        Else ! If Clip(sql:SQLBookingType) = 'R'
                            expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                        End ! If Clip(sql:SQLBookingType) = 'R'
                    Else ! If local:SIDBooking = True
                        expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                    End ! If local:SIDBooking = True
                    !Manufacturer
                    ! Inserting (DBH 03/07/2008) # 9715 - Use OEM Manufacturer, if exists
                    Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                    mod:Model_Number = job:Model_Number
                    If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                        !Found
                        If mod:OEM = 1 And mod:OEMManufacturer <> ''
                            expfil:Line = Clip(expfil:Line) & '<9>' & Clip(mod:OEMManufacturer)
                        Else ! If mod:OEM = 1 And mod:OEMManufacturer <> ''
                            expfil:Line = Clip(expfil:Line) & '<9>' & Clip(job:Manufacturer)
                        End ! If mod:OEM = 1 And mod:OEMManufacturer <> ''
                    Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                        !Error
                        expfil:Line = Clip(expfil:Line) & '<9>' & Clip(job:Manufacturer)
                    End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                    ! End (DBH 03/07/2008) #9715
                    
                    !Colour
                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip(job:Colour)

                    ! Contact Centre (Exchange Bookings) (#7957 11/08/2006 GK)
                    if local:SIDBooking = true
                        ! Exchange jobs only
                        if local:exchangeJob = False
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        else
                            if local:NoExceptionDate <> ''
                                expfil:Line = clip(expfil:Line) & '<9>' & clip(format(local:NoExceptionDate, @d06b))
                                local:ExceptionDate = ccex:ExchangeDate
                            else
                                expfil:Line = clip(expfil:Line) & '<9>' & clip(format(ccex:ExchangeDate, @d06b))
                            end
                            if ccex:TimeSlotID > 0
                                TimeSlotFile{Prop:SQL} = 'SELECT TimeSlotName FROM TimeSlot WHERE TimeSlotID = ' & ccex:TimeSlotID
                                next(TimeSlotFile)
                                if error()
                                    expfil:Line = clip(expfil:Line) & '<9>' & 'STANDARD'
                                else
                                    expfil:Line = clip(expfil:Line) & '<9>' & clip(tmsl:TimeSlotName)
                                end
                            else
                                expfil:Line = clip(expfil:Line) & '<9>' & 'STANDARD'
                            end
                        end
                    else
                        expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                    end

                    ! Oracle Code
                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip(local:OracleCode)

                    ! Manifest No
                    if local:SCAccountID <> 1 ! Not CRC
                        if local:ManifestNo > 0
                            expfil:Line = Clip(expfil:Line) & '<9>' & format(local:ManifestNo, @n07)
                        else
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        end
                    end

                    if local:SIDBooking = true
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(local:InterTimeFrame),' ')
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLPhoneLock),' ')
                        expfil:Line = Clip(expfil:Line) & '<9>' & clip(format(local:ServiceCharge, @n_11.2))
                        if local:exchangeJob = true
                            expfil:Line = Clip(expfil:Line) & '<9>' & clip(format(ccex:DeliveryCharge, @n_11.2))
                        else
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        end
                        if local:DiagAvailable = true
                            expfil:Line = Clip(expfil:Line) & '<9>' & 'Y'
                        else
                            expfil:Line = Clip(expfil:Line) & '<9>' & 'N'
                        end

                        if local:returnJob = true
                            if webr:ReturnException = true
                                expfil:Line = clip(expfil:Line) & '<9>' & 'N'
                            else
                                expfil:Line = clip(expfil:Line) & '<9>' & 'Y'
                            end
                        else
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        end

                        ! TrackerBase Log 10715 - GK 18/03/2009
                        case sql:SQLBookingType
                            of 'Q' ! Customer Return
                                expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '21'
                            of 'W' ! 7 Day Replacement
                                expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '24'
                            of 'S' ! Store Returns
                                expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '22'
                            of 'T' ! Store Recall
                                expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '23'
                            of 'X' ! FEAST Return
                                expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '26'
                            of 'O' ! Other Return
                                expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '27'
                            else ! All Others
                                expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        end

                        if local:ExceptionDate <> ''
                            expfil:Line = clip(expfil:Line) & '<9>' & clip(format(local:ExceptionDate, @d06b))
                        else
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        end
                    else
                        expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                    end

                End ! If local:NewJob = True

                Add(ExportFile)
                if not error()
                    local:exported = true
                    local:CountJobs += 1
                end
            End !Loop

            If local:FoundJob = True
                If local:IsThereAnAccessory = False
                    !An Accessory has not been attached. Will export with "NON"  (DBH: 26-11-2004)
                    !Job Number
                    expfil:Line = Clip(job:Ref_Number)
                    If local:NewJob = False
                        Case Sub(audali:Action,20,3)
                        Of '220' !Return Address Changed
                            expfil:Line = Clip(expfil:Line) & '<9><9><9><9><9><9><9><9><9><9><9>'
                            !Delivery Company Name
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Company_Name_Delivery),' ')
                            !Delivery Address Line 1
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line1_Delivery),' ')
                            !Delivery Address Line 2
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line2_Delivery),' ')
                            !Delivery Address Line 3
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line3_Delivery),' ')
                            !Delivery Postcode
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(FormatPostcode(job:Postcode_Delivery),' ')
                            !Delivery Telephone Number
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Telephone_Delivery),' ')

                            expfil:LIne = Clip(expfil:LIne) & '<9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9>'
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(audali:Action,20,3)),' ')
                            expfil:Line = Clip(expfil:Line) & '<9><9><9><9>'
                        Of '145' !Re-send Evelope Request
                            expfil:Line = Clip(expfil:Line) & 'B<9>C<9>D<9>E<9>F<9>'
                            !Collection Company Name
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Company_Name_Collection),' ')
                            !Collection Address Line1
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line1_Collection),' ')
                            !Collection Address Line 2
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line2_Collection),' ')
                            !Collection Address Line 3
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line3_Collection),' ')
                            !Collection Postcode
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(FormatPostcode(job:Postcode_Collection),' ')
                            !Collection Telephone Number
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Telephone_Collection),' ')
                            expfil:Line = Clip(expfil:Line) & '<9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9>'
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(audali:Action,20,3)),' ')
                            expfil:Line = Clip(expfil:Line) & '<9><9><9><9>'
                        Else ! All other statuses
                            expfil:Line = Clip(expfil:Line) & '<9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9><9>'
                            !Only pass the status when sending a job update  (DBH: 23-11-2004)
                            !Without Mobile Number - TrkBs:  (DBH: 12-01-2005)
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(audali:Action,20,3)),' ') & '<9><9>'
                        End ! Case Sub(audali:Action,20,3)
                    Else ! If local:NewJob = True

                    ! Text Truncated
                        !SID job number
                        If local:SIDBooking = True
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Order_Number),' ')
                        Else ! If local:SIDBooking = True
                            expfil:Line = Clip(expfil:Line) & '<9>' & ''
                        End ! If local:SIDBooking = True
                        !ACcount Number
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Account_Number),' ')
                        !Title
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Title),' ')
                        !Intial
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(local:Forename),' ') ! job:Initial
                        !Surname
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Surname),' ')
                        !Collection Company Name
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Company_Name_Collection),' ')
                        !Collection Address Line1
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line1_Collection),' ')
                        !Collection Address Line 2
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line2_Collection),' ')
                        !Collection Address Line 3
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line3_Collection),' ')
                        !Collection Postcode
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(FormatPostcode(job:Postcode_Collection),' ')
                        !Collection Telephone Number
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Telephone_Collection),' ')
                        !Delivery Company Name
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Company_Name_Delivery),' ')
                        !Delivery Address Line 1
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line1_Delivery),' ')
                        !Delivery Address Line 2
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line2_Delivery),' ')
                        !Delivery Address Line 3
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Address_Line3_Delivery),' ')
                        !Delivery Postcode
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(FormatPostcode(job:Postcode_Delivery),' ')
                        !Delivery Telephone Number
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Telephone_Delivery),' ')
                        !Model Number
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Model_Number),' ')
                        !IMEI Number
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:ESN),' ')
                        !Trade Fault Code 1/2 - GK 30/04/2006 TrackerBase 8905
                        if local:SCAccountID = 1 ! CRC
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jobe:TraFaultCode1),' ')
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jobe:TraFaultCode2),' ')
                        else
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLDeviceCondition),' ')
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLUnitCondition),' ')
                        end
                        !Trade Fault Code 3
                        ! Additional Symptoms - TrackerBase 10978 GK 12/08/2009
                        If clip(local:AdditionalSymptoms) <> ''
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(local:AdditionalSymptoms),' ')
                        Else ! Old way
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLFreeTextFault),' ')
                        End
                        !Trade Fault Code 4
                        If local:SIDBooking = True
                            ! Put who booked into Trade Fault Code 4 (DBH: 04-10-2005)
                            jobe:TraFaultCode4 = Clip(sql:SQLBookedBy)
                            Access:JOBSE.TryUpdate()
                        End ! If local:SIDBooking = True
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jobe:TraFaultCode4),' ')
                        !Accessory
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jac:Accessory),' ')
                        !Date Booked
                        expfil:Line = Clip(expfil:Line) & '<9>' & Clip(Format(Job:Date_Booked,@d06b))
                        !Time Booked
                        expfil:Line = Clip(expfil:Line) & '<9>' & Clip(Format(job:Time_Booked,@t02b))
                        !Repair Flag
                        If local:SMARTCopy = True
                            expfil:Line = Clip(expfil:Line) & '<9>X'
                        Else
                            IF local:SIDBooking = True
                                expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLBookingType),' ')
                            Else ! IF local:SIDBooking = True
                                !Not a SID job. Assume it's an exchange only  (DBH: 22-11-2004)
                                expfil:Line = Clip(expfil:Line) & '<9>E'
                            End ! IF local:SIDBooking = True
                        End
                        
                        !Device Type Flag
                        !Fault Description
                        If local:SIDBooking = True
        ! Changing (DBH 12/06/2006) #7813 - Always write the web job fault description here
        !                     If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
        !                         ! If Primary Fault prefixed with 'C'/'P', strip this from the Primary Fault field (DBH: 04-10-2005)
        !                         expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,1,1)),' ')
        !                         expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(jbn:Fault_Description,3,255)),' ')
        !                     Else ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
        !                         expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
        !                         expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jbn:Fault_Description),' ')
        !                     End ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
        ! to (DBH 12/06/2006) #7813

                            If (sql:SQLBookingType = 'Q' Or sql:SQLBookingType = 'S' Or sql:SQLBookingType = 'T' Or sql:SQLBookingType = 'X' Or sql:SQLBookingType = 'H' Or sql:SQLBookingType = 'J' Or sql:SQLBookingType = 'D' Or sql:SQLBookingType = 'I' Or sql:SQLBookingType = 'O') and local:returnJob = True
                                If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                    ! If Primary Fault prefixed with 'C'/'P', strip this from the Primary Fault field (DBH: 04-10-2005)
                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,1,1)),' ')
                                Else ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                                End ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(webr:RetReasonCode) & ' ' & Clip(webr:RetReasonName), ' ')
                            Else
                                If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                    ! If Primary Fault prefixed with 'C'/'P', strip this from the Primary Fault field (DBH: 04-10-2005)
                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,1,1)),' ')
                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,3,255)),' ')
                                Else ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLPrimaryFault),' ')
                                End ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'

                                ! GK 27/01/2010 - TrackerBase 10984
                                If Len(Clip(local:KnownIssueSummary)) > 0
                                    expfil:Line = clip(expfil:Line) & ' ' & BHStripNonAlphaNum(Clip(local:KnownIssueSummary), ' ')
                                End
                            End

!                            If sql:SQLBookingType = 'Q' and local:returnJob = True
!                                expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(webr:RetReasonCode), ' ')
!                                expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(webr:RetReasonName), ' ')
!                            Else
!                                If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
!                                    ! If Primary Fault prefixed with 'C'/'P', strip this from the Primary Fault field (DBH: 04-10-2005)
!                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,1,1)),' ')
!                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(sql:SQLPrimaryFault,3,255)),' ')
!                                Else ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
!                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
!                                    expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLPrimaryFault),' ')
!                                End ! If Sub(sql:SQLPrimaryFault,1,1) = 'C' Or Sub(sql:SQLPrimaryFault,1,1) = 'P'
!                            End
        ! End (DBH 12/06/2006) #7813
                        Else ! If local:SIDBooking = True
                            !Not a SID job, don't know the device type  (DBH: 23-11-2004)
                            expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(jbn:Fault_Description),' ')
                        End ! If local:SIDBooking = True

                        !Intermittent
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(job:Intermittent_Fault,1,1)),' ')
                        !Job Type
                        If local:SIDBooking = True
                            Case sql:SQLRepairType
                                Of 'IW'
                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('IN MANUFACTURERS WARRANTY')
                                Of 'IV'
                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('IN VODAFONE WARRANTY')
                                Of 'ON'
                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('OUT OF WARRANTY BUT NOT CHARGEABLE')
                                Of 'IR'
                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('INSURANCE REPAIR')
                                Of 'OC'
                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('OUT OF WARRANTY AND CHARGEABLE')
                                Else
                                    expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                            End ! Case web:RepairType
                        Else ! If local:SIDBooking = True
                            !Not a SID job, use the job Charge Type  (DBH: 23-11-2004)
                            If job:Warranty_Job = 'YES'
                                expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Warranty_Charge_Type),' ')
                            Else ! If job:Warranty_Job = 'YES'
                                expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Charge_Type),' ')
                            End ! If job:Warranty_Job = 'YES'
                        End ! If local:SIDBooking = True
                        !DOP
                        expfil:Line = Clip(expfil:Line) & '<9>' & Clip(Format(job:DOP,@d06b))
                        !Insurance Ref
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Insurance_Reference_Number),' ')
                        !OutWar Reason
                        If local:SIDBooking = True
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLReason),' ')
                        Else ! If local:SIDBooking = True
                            !Not a SID job, don't know the reason  (DBH: 23-11-2004)
                            expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                        End ! If local:SIDBooking = True
                        
                        !Customer Collection Date
                        If local:SIDBooking = True
                            If local:NoExceptionDate <> ''
                                expfil:Line = clip(expfil:Line) & '<9>' & clip(format(local:NoExceptionDate, @d06b))
                                local:ExceptionDate = sql:SQLCustCollectionDate
                            Else
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip(Format(sql:SQLCustCollectionDate,@d06b))
                            End
                        Else ! If local:SIDBooking = True
                            !Not a SID job, don't know the Customer Collection Date  (DBH: 23-11-2004)
                            expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                        End ! If local:SIDBooking = True
                        
                        !Status
                        ! Send the status - TrkBs: 6915 (DBH: 09-03-2006)
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(Sub(job:Current_Status,1,3)),' ')
                        !Mobile Number
! Changing (DBH 03/07/2008) # 10020 - Show SMS Alert Number
!                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(job:Mobile_Number),' ')
! to (DBH 03/07/2008) # 10020
                        expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLSMSALertNo),' ')
! End (DBH 03/07/2008) #10020
                        !Return Account Number
                        ! Only show account number if the "Repair Flag" is "R" - TrkBs: 5717 (DBH: 19-04-2005)
                        If local:SIDBooking = True
                            If Clip(sql:SQLBookingType) = 'R'
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip(sql:SQLRetStoreCode)
                            Else ! If Clip(sql:SQLBookingType) = 'R'
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                            End ! If Clip(sql:SQLBookingType) = 'R'
                        Else ! If local:SIDBooking = True
                            expfil:Line = Clip(expfil:Line) & '<9>' & Clip('')
                        End ! If local:SIDBooking = True
                        !Manufacturer
                        ! Inserting (DBH 03/07/2008) # 9715 - Use OEM Manufacturer, if exists
                        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                        mod:Model_Number = job:Model_Number
                        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                            !Found
                            If mod:OEM = 1 And mod:OEMManufacturer <> ''
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip(mod:OEMManufacturer)
                            Else ! If mod:OEM = 1 And mod:OEMManufacturer <> ''
                                expfil:Line = Clip(expfil:Line) & '<9>' & Clip(job:Manufacturer)
                            End ! If mod:OEM = 1 And mod:OEMManufacturer <> ''
                        Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                            !Error
                            expfil:Line = Clip(expfil:Line) & '<9>' & Clip(job:Manufacturer)
                        End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                        ! End (DBH 03/07/2008) #9715
                        !Colour
                        expfil:Line = Clip(expfil:Line) & '<9>' & Clip(job:Colour)

                        if local:SIDBooking = true
                            if local:exchangeJob = False
                                expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                                expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            else
                                if local:NoExceptionDate <> ''
                                    expfil:Line = clip(expfil:Line) & '<9>' & clip(format(local:NoExceptionDate, @d06b))
                                    local:ExceptionDate = ccex:ExchangeDate
                                else
                                    expfil:Line = clip(expfil:Line) & '<9>' & clip(format(ccex:ExchangeDate, @d06b))
                                end
                                if ccex:TimeSlotID > 0
                                    TimeSlotFile{Prop:SQL} = 'SELECT TimeSlotName FROM TimeSlot WHERE TimeSlotID = ' & ccex:TimeSlotID
                                    next(TimeSlotFile)
                                    if error()
                                        expfil:Line = clip(expfil:Line) & '<9>' & 'STANDARD'
                                    else
                                        expfil:Line = clip(expfil:Line) & '<9>' & clip(tmsl:TimeSlotName)
                                    end
                                else
                                    expfil:Line = clip(expfil:Line) & '<9>' & 'STANDARD'
                                end
                            end
                        else
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        end

                        ! Oracle Code
                        expfil:Line = Clip(expfil:Line) & '<9>' & Clip(local:OracleCode)

                        ! Manifest No
                        if local:SCAccountID <> 1 ! Not CRC
                            if local:ManifestNo > 0
                                expfil:Line = Clip(expfil:Line) & '<9>' & format(local:ManifestNo, @n07)
                            else
                                expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            end
                        end

                        if local:SIDBooking = true
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(local:InterTimeFrame),' ')
                            expfil:Line = Clip(expfil:Line) & '<9>' & BHStripNonAlphaNum(Clip(sql:SQLPhoneLock),' ')
                            expfil:Line = Clip(expfil:Line) & '<9>' & clip(format(local:ServiceCharge, @n_11.2))
                            if local:exchangeJob = true
                                expfil:Line = Clip(expfil:Line) & '<9>' & clip(format(ccex:DeliveryCharge, @n_11.2))
                            else
                                expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            end
                            if local:DiagAvailable = true
                                expfil:Line = Clip(expfil:Line) & '<9>' & 'Y'
                            else
                                expfil:Line = Clip(expfil:Line) & '<9>' & 'N'
                            end
                            if local:returnJob = true
                                if webr:ReturnException = true
                                    expfil:Line = clip(expfil:Line) & '<9>' & 'N'
                                else
                                    expfil:Line = clip(expfil:Line) & '<9>' & 'Y'
                                end
                            else
                                expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            end

                            ! TrackerBase Log 10715 - GK 18/03/2009
                            case sql:SQLBookingType
                                of 'Q' ! Customer Return
                                    expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '21'
                                of 'W' ! 7 Day Replacement
                                    expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '24'
                                of 'S' ! Store Returns
                                    expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '22'
                                of 'T' ! Store Recall
                                    expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '23'
                                of 'X' ! FEAST Return
                                    expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '26'
                                of 'O' ! Other Return
                                    expfil:Line = clip(expfil:Line) & '<9>' & '7019' & clip(job:Order_Number) & '27'
                                else ! All Others
                                    expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            end

                            if local:ExceptionDate <> ''
                                expfil:Line = clip(expfil:Line) & '<9>' & clip(format(local:ExceptionDate, @d06b))
                            else
                                expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            end
                        else
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                            expfil:Line = clip(expfil:Line) & '<9>' & clip('')
                        end

                    End ! If local:NewJob = True

                    Add(ExportFile)
                    if not error()
                        local:exported = true
                        local:FoundJob = True
                        local:CountJobs += 1
                    end
                End ! If local:IsThereAnAccessory = False

                ! If this is the copy for SMART then don't add audit trail / pendmail entries again
                if local:SMARTCopy = true
                    local:exported = false
                end

                 ! Only update if we added to the export file without any problems
                if local:exported = true
                    ! GK 13/06/2007 TrackerBase 8830 - SID Extract SQL interface
                    ! Add SB job number to table if it does not already exist
                    !Access:SIDEXUPD.ClearKey(seu:SBJobNoKey)
                    !seu:SBJobNo = job:Ref_Number
                    !if Access:SIDEXUPD.Fetch(seu:SBJobNoKey) <> Level:Benign
                    !    seu:SBJobNo = job:Ref_Number
                    !    Access:SIDEXUPD.TryInsert()
                    !end

                    !Save this job to make sure the same job isn't exported twice  (DBH: 23-11-2004)
                    jobque:JobNumber = job:Ref_Number
                    Add(JobsQueue, jobque:JobNumber)

                    If local:NewJob
                        !Start - Mark new job as "sent" (DBH: 16-09-2004 )
                        If Access:AUDIT.PrimeRecord() = Level:Benign
                            aud:Notes         = 'EXPORT FILE: ' & clip(tmp:ExportFile) & |
                                                '<13,10>STATUS: ' & Clip(job:Current_Status)
                            aud:Ref_Number    = job:ref_number
                            aud:Date          = Today()
                            aud:Time          = Clock()
                            aud:Type          = 'JOB'
                            aud:User          = 'WEB'
                            aud:Action        = 'JOB EXPORTED TO WILFRED'
                            Access:AUDIT.Insert()
                        End!If Access:AUDIT.PrimeRecord() = Level:Benign
                        !End   - Mark job as "sent" (DBH: 16-09-2004)

                    Else ! If local:NewJob
                        If Sub(audali:Action,20,3) = '940'
                            !SID has said that the job is completed. Do this now  (DBH: 24-11-2004)
                            If job:Date_Completed = ''
                                job:Date_Completed = Today()
                                job:Time_Completed = Clock()
                                job:Completed = 'Y'
                                GetStatusAlternative(705,0,'JOB')
                                ForceDespatch
                                Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                                mod:Model_Number    = job:Model_Number
                                If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                    !Found
                                    job:EDI = PendingJob(mod:Manufacturer)
                                    job:Manufacturer = mod:Manufacturer
                                Else ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                    !Error
                                End !If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
! Changing (DBH 18/04/2008) # N/A - Stop errors
!                                Access:JOBS.Update()
! to (DBH 18/04/2008) # N/A
                                If Access:JOBS.TryUpdate()
                                    ErrorLog('Error updating job number: ' & job:Ref_Number)
                                End ! If Access:JOBS.TryUpdate()
! End (DBH 18/04/2008) #N/A
                                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                jobe:RefNumber  = job:Ref_Number
                                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                    !Found
                                    IF (jobe:CompleteRepairDate = '') THEN
                                        jobe:CompleteRepairType = 2
                                        jobe:CompleteRepairDate = TODAY()
                                        jobe:CompleteRepairTime = CLOCK()
! Changing (DBH 18/04/2008) # N/A - Stop errors
!                                        access:jobse.update()
! to (DBH 18/04/2008) # N/A
                                        Access:JOBSE.TryUpdate()
! End (DBH 18/04/2008) #N/A
                                    END
                                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                    !Error
                                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                If Access:AUDIT.PrimeRecord() = Level:Benign
                                    aud:Notes         = 'EXPORT FILE: ' & clip(tmp:ExportFile)
                                    aud:Ref_Number    = job:ref_number
                                    aud:Date          = Today()
                                    aud:Time          = Clock()
                                    aud:Type          = 'JOB'
                                    aud:User          = 'WEB'
                                    aud:Action        = 'JOB COMPLETED BY WILFRED'
                                    Access:AUDIT.Insert()
                                End!If Access:AUDIT.PrimeRecord() = Level:Benign

                            End ! If job:Date_Completed = ''
                        Else
                            ! Inserting (DBH 15/03/2006) #6915 - Record job updates in the audit trail
                            If Access:AUDIT.PrimeRecord() = Level:Benign
                                aud:Notes         = 'EXPORT FILE: ' & clip(tmp:ExportFile) & |
                                                '<13,10>STATUS: ' & Clip(job:Current_Status)
                                aud:Ref_Number    = job:ref_number
                                aud:Date          = Today()
                                aud:Time          = Clock()
                                aud:Type          = 'JOB'
                                aud:User          = 'WEB'
                                aud:Action        = 'JOB UPDATE EXPORTED TO WILFRED'
                                Access:AUDIT.Insert()
                            End!If Access:AUDIT.PrimeRecord() = Level:Benign
                            ! End (DBH 15/03/2006) #6915
                        End ! If Sub(audali:Action,20,3) = '940'
                    End ! If local:NewJob
                End
            End !If local:FoundJob = True

            ! Close file here, next job might be for a different Service Centre (different filename)
            Close(ExportFile)

            if local:sendCopyToSMART = false then break.
            if local:SMARTCopy = true then break.
            local:SMARTCopy = true
        End !Loop
    

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Information
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(Window)
  SELF.Opened=True
  0{Prop:Hide} = true ! Hide the window
  
  IniFilepath =  CLIP(PATH()) & '\Webimp.ini'
  ConnectionStr = GETINI('Defaults', 'Connection', '', CLIP(IniFilepath))
  DiagConnectionStr = GETINI('Defaults', 'DiagConnection', '', CLIP(IniFilepath))
  
  !Start - Debug - TrkBs:  (DBH: 24-01-2005)
  glo:ID = RANDOM(1,1000)
  REMOVE(Clip(Path()) & '\CRCDEBUG.INI')
  PUTINI('Program Start','Date',Clip(glo:ID) & ': ' & Format(Today(),@d06),CLIP(Path()) & '\CRCDEBUG.INI')
  PUTINI('Program Start','Time',Clip(glo:ID) & ': ' & Format(Clock(),@t01),CLIP(Path()) & '\CRCDEBUG.INI')
  !End   - Debug - TrkBs:  (DBH: 24-01-2005)
  
  ErrorLog('')
  ErrorLog('SB2WILFRED Started===========================')
  
  Do ExportJobs
  
  ErrorLog('SB2WILFRED Finished==========================')
  ErrorLog('')
  
  POST(Event:CloseWindow)
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

local.AddToAlertViewer      Procedure(String f:Type,String f:Status,String f:ModelNo)
local:DateToSend        Date()
local:TimeToSend        Time()
local:SMSTimeToSend     Time()
local:Reminder          Byte()
local:ModelSMSActive    Byte()
local:ModelEmailActive  Byte()
local:FoundModelException Byte()
Code

    Access:SIDALERT.Clearkey(sdl:BookingTypeStatusKey)
    sdl:BookingType = f:Type
    sdl:Status = f:Status
    Set(sdl:BookingTypeStatusKey,sdl:BookingTypeStatusKey)
    Loop ! Begin Loop
        If Access:SIDALERT.Next()
            Break
        End ! If Access:SIDALERT.Next()
        If sdl:BookingType <> f:Type
            Break
        End ! If sdl:BookingType <> 'EXCHANGE'
        If sdl:Status <> f:Status
            Break
        End ! If sdl:Status <> Sub(job:Current_Status,1,3)

        Case Today() % 7
        Of 0 ! Sunday
            If ~sdl:Sunday
                Cycle
            End ! If sdl:Sunday
        Of 1 ! Monday
            If ~sdl:Monday
                Cycle
            End ! If sdl:Monday
        Of 2 ! Tuesday
            If ~sdl:Tuesday
                Cycle
            End ! If sdl:Tuesday
        Of 3 ! Wednesday
            If ~sdl:Wednesday
                Cycle
            End ! If sdl:Wednesday
        Of 4 ! Thursday
            If ~sdl:Thursday
                Cycle
            End ! If sdl:Thursday
        Of 5 ! Friday
            If ~sdl:Friday
                Cycle
            End ! If sdl:Friday
        Of 6 ! Saturday
            If ~sdl:Saturday
                Cycle
            End ! If sdl:Saturday
        End ! Case Today() % 7

        local:FoundModelException = 0
        local:ModelSMSActive = 0
        local:ModelEmailActive = 0

        ! TrackerBase 11310 - GK 11/03/2010
        Access:SMOALERT.ClearKey(sml:SIDALERTRecordNumberKey)
        sml:SIDALERTRecordNumber = sdl:RecordNumber
        sml:ModelNumber = f:ModelNo
        If Access:SMOALERT.TryFetch(sml:SIDALERTRecordNumberKey) = Level:Benign
            local:FoundModelException = 1
            local:ModelSMSActive = sml:SMSAlertActive
            local:ModelEmailActive = sml:EmailAlertActive
        End

        local:Reminder = 0

        Access:SIDREMIN.Clearkey(sdm:ElapsedKey)
        sdm:SIDALERTRecordNumber = sdl:RecordNumber
        Set(sdm:ElapsedKey,sdm:ElapsedKey)
        Loop ! Begin Loop
            If Access:SIDREMIN.Next()
                Break
            End ! If Access:SIDREMIN.Next()
            If sdm:SIDALERTRecordNumber <> sdl:RecordNumber
                Break
            End ! If sdm:SIDALERTRecordNumber <> sdl:RecordNumber
            local:Reminder = 1
            Break
        End ! Loop

        If sdl:EmailAlertActive = 1
            If local:FoundModelException = 0 or local:ModelEmailActive = 1
                ! Send UTL Mail First
                If sdl:SendTo <> 1
                    If Access:PENDMAIL.PrimeRecord() = Level:Benign
                        pem:SMSEmail = 'EMAIL'
                        pem:RefNumber = job:Ref_Number
                        pem:DateToSend = Today()
                        pem:TimeToSend = Clock()
                        pem:BookingType = sdl:BookingType
                        pem:Status = sdl:Status
                        pem:SIDJobNumber = job:Order_Number
                        pem:EmailAddress = sdd:UTLEmailAddress
                        pem:CustomerUTL = 'UTL'
                        pem:Reminder = local:Reminder
                        If Access:PENDMAIL.TryInsert() = Level:Benign
                            !Insert
                        Else ! If Access:PENDMAIL.TryInsert() = Level:Benign
                            Access:PENDMAIL.CancelAutoInc()
                        End ! If Access:PENDMAIL.TryInsert() = Level:Benign
                    End ! If Access.PENDMAIL.PrimeRecord() = Level:Benign
                End ! If sdl:SentTo = 2 Or sdl:SentTo = 3
                ! Send to Customer, if they want it

                If (sdl:SendTo <> 2) And sql:SQLEmailReq = 1
                    If Access:PENDMAIL.PrimeRecord() = Level:Benign
                        pem:SMSEmail = 'EMAIL'
                        pem:RefNumber = job:Ref_Number
                        pem:DateToSend = Today()
                        pem:TimeToSend = Clock()
                        pem:BookingType = sdl:BookingType
                        pem:Status = sdl:Status
                        pem:SIDJobNumber = job:Order_Number
                        pem:EmailAddress = jobe:EndUserEmailAddress
                        pem:CustomerUTL = 'CUSTOMER'
                        pem:Reminder = local:Reminder
                        If Access:PENDMAIL.Insert() = Level:Benign
                            !Insert
                        Else ! If Access:PENDMAIL.TryInsert() = Level:Benign
                            Access:PENDMAIL.CancelAutoInc()
                        End ! If Access:PENDMAIL.TryInsert() = Level:Benign
                    End ! If Access.PENDMAIL.PrimeRecord() = Level:Benign
                End ! If sdl:SendTo = 1

            End ! If local:FoundModelException = 0 or local:ModelEmailActive = 1
        End ! If sdl:EmailAlertActive = 1

! ---- Nick request to stop users putting bogus SMS number in ----
! Changing:  DBH 23/01/2009 #N/A
!        If sdl:SMSAlertActive = 1

! to: DBH 23/01/2009 # N/A
        If sdl:SMSAlertActive = 1  And jobe:TraFaultCode5 <> '07500000000'
! End: DBH 23/01/2009 #N/A
! -----------------------------------------
            If local:FoundModelException = 0 or local:ModelSMSActive = 1
                If (sdl:SendTo <> 1) And sql:SQLSMSReq = 1
                    ! Send SMS To UTL (Doesn't exist yet) (DBH: 25/09/2007)
                End ! If (sdl:SendTo = 2 Or sdl:SendTo = 3) And sql:SQLSMSReq = 1

                If (sdl:SendTo <> 2) And sql:SQLSMSReq = 1
                    ! Send SMS to Customer (DBH: 25/09/2007)
                    local:SMSTimeToSend = Clock()
                    local:DateToSend = Today()
                    local:TimeToSend = Clock()
                    If Clock() > sdl:CutOffTime
                        If sdl:CutOffTimeType = 2
                            ! Discard if generated after cut off time (DBH: 21/09/2007)
                            Cycle
                        End ! If sdl:CutOffTime = 2
                        ! After cut off time. Send next day (DBH: 21/09/2007)
                        local:DateToSend += 1

                        ! Determine time to send based on if next day is weekend or not (DBH: 21/09/2007)
                        If local:DateToSend % 7 = 6 Or local:DateToSend % 7 = 7
                            local:TimeToSend = Deformat('10:00',@t4)
                        Else ! If local:DateToSend % 7 = 6 Or local:DateToSend % 7 = 7
                            local:TimeToSend = Deformat('08:00',@t4)
                        End ! If local:DateToSend % 7 = 6 Or local:DateToSend % 7 = 7
                    End ! If Clock() > sdl:CutOffTime

                    ! Is the next day a bank holiday (DBH: 21/09/2007)
                    If IsBankHoliday(sql:SQLCountryDelivery,local:DateToSend)
                        Case sdl:BankHolidays
                        Of 1 ! Send
                            ! Send message on bank holidays (DBH: 21/09/2007)
                        Of 2 ! Send Next Day
                            ! Send on the next "non" bank holiday (DBH: 21/09/2007)
                            Loop
                                local:DateToSend += 1
                                If ~IsBankHoliday(sql:SQLCountryDelivery,local:DateToSend)
                                    ! Date is NOT a bank holiday (DBH: 21/09/2007)
                                    ! Determine time to send based on if next day is weekend or not (DBH: 21/09/2007)
                                    If local:DateToSend % 7 = 6 Or local:DateToSend % 7 = 7
                                        local:TimeToSend = Deformat('10:00',@t4)
                                    Else ! If local:DateToSend % 7 = 6 Or local:DateToSend % 7 = 7
                                        local:TimeToSend = Deformat('08:00',@t4)
                                    End ! If local:DateToSend % 7 = 6 Or local:DateToSend % 7 = 7
                                    Break
                                End ! If IsBankHoliday(sql:SQLCountryDelivery,local:DateToSend)

                            End ! Loop
                        Of 3 ! Discard
                            ! Discard if generated on a bank holiday (DBH: 21/09/2007)
                            Cycle
                        End ! Case sdl:BankHolidays
                    End ! If IsBankHoliday(sql:SQLCountryDelivery,local:DateToSend)

                    If Access:PENDMAIL.PrimeRecord() = Level:Benign
                        pem:DateToSend = local:DateToSend
                        pem:TimeToSend = local:TimeToSend
                        local:SMSTimeToSend = pem:TimeToSend
                        pem:SMSEmail = 'SMS'
                        pem:RefNumber = job:Ref_Number
                        pem:BookingType = sdl:BookingType
                        pem:Status = sdl:Status
                        pem:SIDJobNumber = job:Order_Number
                        pem:MobileNumber = jobe:TraFaultCode5
                        pem:CustomerUTL = 'CUSTOMER'
                        pem:Reminder = local:Reminder
                        If Access:PENDMAIL.TryInsert() = Level:Benign
                            !Insert
                        Else ! If Access:PENDMAIL.TryInsert() = Level:Benign
                            Access:PENDMAIL.CancelAutoInc()
                        End ! If Access:PENDMAIL.TryInsert() = Level:Benign
                    End ! If Access.PENDMAIL.PrimeRecord() = Level:Benign

                End ! If local:FoundModelException = 0 or local:ModelSMSActive = 1
            End !If (sdl:SendTo = 1 Or sdl:SendTo = 3) And sql:SQLSMSReq = 1
        End ! If sdl:SMSAlertActive = 1 And sql:SQLSMSReq = 1
    End ! Loop
local.GetNextDateTime       Procedure(*Date func:DateToSend,*Time func:TimeToSend)
Code
    Case Today() % 7
    Of 0 ! Sunday
        If Clock() < Deformat('10:00',@t4)
            func:DateToSend = Today()
            func:TimeToSend = Deformat('10:00',@t4)
        ElsIf Clock() > Deformat('20:00',@t4)
            func:DateToSend = Today() + 1
            func:TimeToSend = Deformat('08:00',@t4)
        Else
            func:DateToSend = Today()
            func:TimeToSend = Clock()
        End ! If Clock() < Deformat('10:00',@t4)
        
    Of 6 !Saturday
        If Clock() < Deformat('10:00',@t4)
            func:DateToSend = Today()
            func:TimeToSend = Deformat('10:00',@t4)
        ElsIf Clock() > Deformat('20:00',@t4)
            func:DateToSend = Today() + 1
            func:TimeToSend = Deformat('10:00',@t4)
        Else
            func:DateToSend = Today()
            func:TimeToSend = Clock()
        End ! If Clock() < Deformat('10:00',@t4)
    Of 5 !Friday
        If Clock() < Deformat('08:00',@t4)
            func:DateTosend = Today()
            func:TimeTOsend = Deformat('08:00',@t4)
        Elsif Clock() > Deformat('20:00',@t4)
            func:DateToSend = Today() + 1
            func:TimeToSend = Deformat('10:00',@t4)
        Else
            func:DateToSend = Today()
            func:TimeToSend = Clock()
        End ! If Clock() < Deformat('08:00',@t4)
    Else
        If Clock() < Deformat('08:00',@t4)
            func:DateTosend = Today()
            func:TimeTOsend = Deformat('08:00',@t4)
        Elsif Clock() > Deformat('20:00',@t4)
            func:DateToSend = Today() + 1
            func:TimeToSend = Deformat('08:00',@t4)
        Else
            func:DateToSend = Today()
            func:TimeToSend = Clock()
        End ! If Clock() < Deformat('08:00',@t4)
    End ! Case Today() % 7
local.GetWorkDate           procedure(long scAccountID, string country, date startDate, long workingDays)

endDate         date
nonWorkingDay   byte

code
    endDate = startDate

    loop 

        case (endDate % 7)
            of 0 ! SUN
                endDate += 1
            of 6 ! SAT
                endDate += 2
            else ! Check bank holidays
                if IsBankHoliday(country, endDate) = true
                    endDate += 1
                else
                    if workingDays > 0
                        ! Check if non working day
                        nonWorkingDay = false

                        Access:SIDNWDAY.ClearKey(nwd:SCDayKey)
                        nwd:SCAccountID = scAccountID
                        set(nwd:SCDayKey, nwd:SCDayKey)
                        loop
                            if Access:SIDNWDAY.Next() <> Level:Benign then break.
                            if nwd:SCAccountID <> scAccountID then break.
                            if endDate = nwd:NonWorkingDay
                                nonWorkingDay = true
                                break
                            end
                        end

                        endDate += 1
                        if nonWorkingDay = false
                            workingDays -= 1
                        end
                    else
                        break
                    end

                end
        end ! case

    end


    return endDate
local.IsNonWorkingDay  procedure(long scAccountID, string country)

isNonWorkingDay     byte
startDate           date

code
    isNonWorkingDay = false
    startDate = today()

    case (startDate % 7)
        of 0 ! SUN
            isNonWorkingDay = true
        of 6 ! SAT
            isNonWorkingDay = true
        else ! Check bank holidays
            if IsBankHoliday(country, startDate) = true
                isNonWorkingDay = true
            ! Changed on 11 Nov 2008 - Spoke to Nick.
            ! Interface files should still be received on non working days
            !else
            !    Access:SIDNWDAY.ClearKey(nwd:SCDayKey)
            !    nwd:SCAccountID = scAccountID
            !    set(nwd:SCDayKey, nwd:SCDayKey)
            !    loop
            !        if Access:SIDNWDAY.Next() <> Level:Benign then break.
            !        if nwd:SCAccountID <> scAccountID then break.
            !        if startDate = nwd:NonWorkingDay
            !            isNonWorkingDay = true
            !            break
            !        end
            !    end

            end
        end ! case

    return isNonWorkingDay
