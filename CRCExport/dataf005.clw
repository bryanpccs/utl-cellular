

   MEMBER('datafix.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DATAF005.INC'),ONCE        !Local module procedure declarations
                     END


Dummy PROCEDURE                                       !Generated from procedure template - Window

window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Dummy')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCREG.Open
  Relate:AUDIT.Open
  Relate:CRCDEFS.Open
  Relate:DEFAULTS.Open
  Relate:DEFEMAI2.Open
  Relate:DESBATCH.Open
  Relate:EXCHANGE.Open
  Relate:IMEISHIP.Open
  Relate:PENDMAIL.Open
  Relate:SIDDEF.Open
  Relate:SIDEXUPD.Open
  Relate:STAHEAD.Open
  Relate:XMLSAM.Open
  Relate:XMLTRADE.Open
  Access:JOBS.UseFile
  Access:JOBACC.UseFile
  Access:JOBSE.UseFile
  Access:USERS.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBNOTES.UseFile
  Access:AUDSTAEX.UseFile
  Access:SIDSRVCN.UseFile
  Access:CARISMA.UseFile
  Access:MODELNUM.UseFile
  Access:SIDNWDAY.UseFile
  Access:REGIONS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCREG.Close
    Relate:AUDIT.Close
    Relate:CRCDEFS.Close
    Relate:DEFAULTS.Close
    Relate:DEFEMAI2.Close
    Relate:DESBATCH.Close
    Relate:EXCHANGE.Close
    Relate:IMEISHIP.Close
    Relate:PENDMAIL.Close
    Relate:SIDDEF.Close
    Relate:SIDEXUPD.Close
    Relate:STAHEAD.Close
    Relate:XMLSAM.Close
    Relate:XMLTRADE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

