

   MEMBER('webalert.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetWww.inc'),ONCE

                     MAP
                       INCLUDE('WEBAL002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBAL001.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBAL004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBAL006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBAL008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBAL009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBAL010.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
ConnectionStr        STRING(255),STATIC
ExportFileName       STRING(255)
tmp:CSVFile          CSTRING(255),STATIC
tmp:RType            STRING(2)
tmp:SMS              BYTE
PostString           STRING(100000)
tmp:FinalString      STRING(100000)
tmp:CountSMS         LONG
SMSQueue             QUEUE,PRE(smsque)
PENDMAILRecordNumber LONG
                     END
tmp:ErrorMessage     STRING(255)
! Dummy SQL tables
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD, PRE()
SIDJobNo                    LONG, NAME('Ref_Number')
SBJobNo                     Long, Name('SBJobNo')
                        END
                    END

SQLFile2            FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL2),BINDABLE,THREAD
Record                  RECORD, PRE()
BookingType                 string(1), name('BookingType')
SMSReq                      long, name('SMSReq')
EmailReq                    byte, name('EmailReq')
                        END
                    END
SQLFile3            FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL3),BINDABLE,THREAD
Record                  RECORD, PRE()
SMSReq                      long, name('SMSReq')
EmailReq                    byte, name('EmailReq')
CountryDelivery             String(30), name('CountryDelivery')
                        END
                    END

SQLFile4             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL4),BINDABLE,THREAD
Record                  RECORD, PRE()
PrimaryFault        String(255), Name('PrimaryFault')
ContractPeriod      Long,Name('ContractPeriod')
CustCollectionDate  Date,Name('CustCollectionDate')
DeviceCondition     String(30),Name('DeviceCondition')
                        END
                    END
CCExchangeFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(ccex),Name('CCExchange'),Bindable,Thread
Record      Record
ExchangeDate       Date,Name('ExchangeDate')
ActualExchangeDate Date,Name('ActualExchangeDate')
            End
        End

TempFilePath         CSTRING(255)

CSVFile    File,Driver('ASCII'),Pre(csv),Name(tmp:CSVFile),Create,Bindable,Thread
Record                  Record
Line                    String(1000)
                        End
                    End
IniFilepath             string(255)
foundJobNotExported     byte

sendEmail               byte
sendSMS                 byte

emailSubject            string(255)

logEnabled              byte
i                       long

! For SMS messages only
JobsQ                   queue(), pre(sjq)
jobNo                       long
RecordNumber                long
updateWebJob                byte
                        end


ErrorDate               date()
ErrorTime               time()
    map
!RepairEnvelopeReminders procedure()
GetCompleteText         procedure(String f:Text, String f:Type),String
ExportMessageText       procedure(String f:Type, String f:Model),Byte
    end
window               WINDOW,AT(,,112,18),FONT('Tahoma',,,),CENTER,GRAY,DOUBLE
                       PROMPT('Processing Alert Messages ....'),AT(7,4),USE(?Prompt1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Local Data Classes
ThisWebClient        CLASS(NetWebClient)              !Generated by NetTalk Extension (Class Definition)

                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Not Used (DBH: 23/04/2008)
!AddToPendMail            routine
!    data
!smsEmail    string(10)
!skipInsert  byte
!    code
!    Exit
!
!    Relate:JOBS.Open()
!    Relate:PENDMAIL.Open()
!
!    loop
!        next(SQLFile)
!        if error() then break.
!
!        ! Check if SMS/Email is active. If inactive, just mark email/sms as sent.
!        case tmp:RType
!            of 'R1'
!                if tmp:SMS = true
!                    if dem:R1SMSActive = false
!                        ! Retail - update webjob
!                        SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & sql:SIDJobNo
!                        cycle
!                    end
!                else
!                    if dem:R1Active = false
!                        ! Retail - update webjob
!                        SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET EmailSent = 1 WHERE SBJobNo = ' & sql:SBJobNo
!                        cycle
!                    end
!                end
!            of 'R2'
!                if tmp:SMS = true
!                    if dem:R2SMSActive = false
!                        ! Retail - update webjob
!                        SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & sql:SIDJobNo
!                        cycle
!                    end
!                else
!                    if dem:R2Active = false
!                        ! Retail - update webjob
!                        SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET EmailSent = 1 WHERE SBJobNo = ' & sql:SBJobNo
!                        cycle
!                    end
!                end
!            of 'R3'
!                if tmp:SMS = true
!                    if dem:R3SMSActive = false
!                        ! Retail - update webjob
!                        SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & sql:SIDJobNo
!                        cycle
!                    end
!                else
!                    if dem:R3Active = false
!                        ! Retail - update webjob
!                        SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET EmailSent = 1 WHERE SBJobNo = ' & sql:SBJobNo
!                        cycle
!                    end
!                end
!            of 'R4'
!                if tmp:SMS = true
!                    if dem:R4SMSActive = false
!                        ! Retail - update webjob
!                        SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & sql:SIDJobNo
!                        cycle
!                    end
!                else
!                    if dem:R4Active = false
!                        ! Retail - update webjob
!                        SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET EmailSent = 1 WHERE SBJobNo = ' & sql:SBJobNo
!                        cycle
!                    end
!                end
!        end
!
!        if tmp:SMS = true
!            smsEmail = 'SMS'
!
!            ! Add entry to queue to update sent flag
!            JobsQ.jobNo = sql:SIDJobNo
!            JobsQ.RecordNumber = 0
!            JobsQ.updateWebJob = true
!            add(JobsQ)
!        else
!            smsEmail = 'EMAIL'
!        end
!
!        Access:JOBSE.ClearKey(jobe:RefNumberKey)
!        jobe:RefNumber = sql:SBJobNo
!        if Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!            ! Found record
!            skipInsert = false
!
!            ! Check we have not already sent this message
!            Access:PENDMAIL.ClearKey(pem:MessageRefKey)
!            pem:MessageType = tmp:RType
!            pem:RefNumber = sql:SBJobNo
!            set(pem:MessageRefKey, pem:MessageRefKey)
!            loop
!                if Access:PENDMAIL.Next() <> Level:Benign then break.
!                if clip(pem:MessageType) <> clip(tmp:RType) then break.
!                if pem:RefNumber <> sql:SBJobNo then break.
!                if pem:SMSEmail = smsEmail
!                    skipInsert = true
!                    break
!                end
!            end ! loop
!
!            if skipInsert = true then cycle.
!
!            if Access:PENDMAIL.PrimeRecord() = Level:Benign
!
!                pem:SMSEmail = smsEmail
!                pem:RefNumber = sql:SBJobNo
!                pem:MessageType = tmp:RType
!
!                pem:DateToSend = today()
!                pem:TimeToSend = clock()
!
!                if pem:SMSEmail = 'SMS'
!                    pem:MobileNumber = jobe:TraFaultCode5
!                end ! if pem:SMSEmail = 'SMS'
!                if pem:SMSEmail = 'EMAIL'
!                    pem:EmailAddress = jobe:EndUserEmailAddress
!                end ! If pem:SMSEmail = 'EMAIL'
!
!                if Access:PENDMAIL.TryInsert() = Level:Benign
!                    ! Insert Successful
!                else ! if Access:PENDMAIL.TryInsert() = Level:Benign
!                    ! Insert Failed
!                    Access:PENDMAIL.CancelAutoInc()
!                end ! if Access:PENDMAIL.TryInsert() = Level:Benign
!            end
!        end
!
!    end
!
!    Relate:PENDMAIL.Close()
!    Relate:JOBS.Close()
!
FindAndSendReminders        Routine
Data
local:FoundCustomerReminder    Byte()
local:FoundUTLReminder         Byte()
local:DateToSend               Date()
local:TimeToSend               Time()

local:SMSMessageText        Like(sdl:SMSMessageText)
local:EmailMessageSubject   Like(sdl:EmailMessageSubject)
local:EmailMessageText      Like(sdl2:EmailMessageText)

Code
    Open(SQLFile3)

    ! Check to see if there are any "reminder" entries? (DBH: 24/09/2007)
    tmp:CountSMS = 0
    PostString = ''
    tmp:FinalString = ''
    Free(SMSQueue)
    Clear(SMSQueue)

    Access:PENDMAIL.Clearkey(pem:ReminderKey)
    pem:Reminder    = 1
    Set(pem:ReminderKey,pem:ReminderKey)
    Loop ! Begin Loop
        If Access:PENDMAIL.Next()
            Break
        End ! If Access:PENDMAIL.Next()
        If pem:Reminder <> 1
            Break
        End ! If pem:Reminder <> 1
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = pem:RefNumber
        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Found
            SQLFile3{prop:SQL} = 'SELECT SMSReq, EmailReq, CountryDelivery FROM WebJob Where SBJobNo = ' & job:Ref_Number
            Loop
                Next(SQLFile3)
                If Error()
                    Break
                End ! If Error()

                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    !Found
                Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

                ! Found. Is the job STILL at the same status? (DBH: 24/09/2007)
                If Sub(job:Current_Status,1,3) = pem:Status
                    ! Find the initial alert details for this Status/Booking Type etc.

                    Found# = 0

                    Access:SIDALERT.Clearkey(sdl:BookingTypeStatusKey)
                    sdl:BookingType = pem:BookingType
                    sdl:Status = pem:Status
                    Set(sdl:BookingTypeStatusKey,sdl:BookingTypeStatusKey)
                    Loop ! Begin Loop
                        If Access:SIDALERT.Next()
                            Break
                        End ! If Access:SIDALERT.Next()
                        If sdl:BookingType <> pem:BookingType
                            Break
                        End ! If sdl:BookingType <> pem:BookingType
                        If sdl:Status <> pem:Status
                            Break
                        End ! If sdl:Status <> pem:Status
                        
!                        If sdl:SendTo <> 3
!                            If sdl:SendTo = 1 And pem:CustomerUTL <> 'CUSTOMER'
!                                ! The original message was for the customer, so find the appropriate alert for the "customer" (DBH: 25/09/2007)
!                                Cycle
!                            End ! If sdl:SendTo = 1 And pem:CustomerUTL <> 'CUSTOMER'
!                            If sdl:SendTo = 2 And pem:CustomerUTL <> 'UTL'
!                                ! The original message was for the UTL, so find the appropriate alert for "UTL" (DBH: 25/09/2007)
!                                Cycle
!                            End ! If sdl:SendTo = 2 And pem:CustomerUTL <> 'UTL'
!                        End ! If sdl:SendTo <> 3
                        ! Find the alert that corresponds to the day the original one was sent (DBH: 25/09/2007)
                        Case pem:DateSent % 7
                        Of 0 ! Sunday
                            If ~sdl:Sunday
                                Cycle
                            End ! If ~sdl:Sunday
                        Of 1 ! monday
                            If ~sdl:Monday
                                Cycle
                            End ! If ~sdl:Monday
                        Of 2 ! Tuesday
                            If ~sdl:Tuesday
                                Cycle
                            End ! If ~sdl:Tuesday
                        Of 3 ! Wednesday
                            If ~sdl:Wednesday
                                Cycle
                            End ! If ~sdl:Wednesday
                        Of 4 ! Thursday
                            If ~sdl:Thursday
                                Cycle
                            End ! If ~sdl:Thursday
                        Of 5 ! Friday
                            If ~sdl:Friday
                                Cycle
                            End ! If ~sdl:Friday
                        Of 6 ! Saturday
                            If ~sdl:Saturday
                                Cycle
                            End ! If ~sdl:Saturday
                        End ! Case pem:DateToSend % 7
                                    
                        Access:SIDREMIN.Clearkey(sdm:ElapsedKey)
                        sdm:SIDALERTRecordNumber = sdl:RecordNumber
                        sdm:ElapsedDays = (Today() - pem:DateSent)
                        Set(sdm:ElapsedKey,sdm:ElapsedKey)
                        Loop ! Begin Loop
                            If Access:SIDREMIN.Next()
                                Break
                            End ! If Access:SIDREMIN.Next()
                            If sdm:SIDALERTRecordNumber <> sdl:RecordNumber
                                Break
                            End ! If sdm:SIDALERTRecordNumber <> sdl:RecordNumber
                            If sdm:ElapsedDays <> (Today() - pem:DateSent)
                                Break
                            End ! If sdm:ElapsedDays <> (Today() - pem:DateSend)

                            ! See if a message has already been sent today (DBH: 25/09/2007)
                            ! It will be for the same job and status, but will not be a "reminder" (DBH: 25/09/2007)


                            local:FoundCustomerReminder = 0
                            local:FoundUTLReminder = 0
                            Access:PENDMAIL_ALIAS.Clearkey(pem_ali:ReminderRefNumberKey)
                            pem_ali:Reminder = 0
                            pem_ali:RefNumber = job:Ref_Number
                            Set(pem_ali:ReminderRefNumberKey,pem_ali:ReminderRefNumberKey)
                            Loop ! Begin Loop
                                If Access:PENDMAIL_ALIAS.Next()
                                    Break
                                End ! If Access:PENDMAIL_ALIAS.Next()
                                If pem_ali:Reminder <> 0
                                    Break
                                End ! If pem_ali:Reminder <> 0
                                If pem_ali:RefNumber <> job:Ref_Number
                                    Break
                                End ! If pem_ali:RefNumber <> job:Ref_Number
                                If pem_ali:RecordNumber = pem:RecordNumber
                                    ! Don't get the same record
                                    Cycle
                                End ! If pem_ali:RecordNumber = pem:RecordNumber
                                If pem_ali:Status <> pem:Status
                                    Cycle
                                End ! If pem_ali:Status <> pem:Status
                                ! If message sent previously, doesn't count (DBH: 25/09/2007)
                                If pem_ali:DateSent <> Today()
                                    Cycle
                                End ! If pem_ali:DateSent = Today()
                                ! If message hasn't been sent yet, but it's for another day, it doesn't matter (DBH: 27/09/2007)
                                If pem_ali:DateToSend <> Today()
                                    Cycle
                                End ! If pem_ali:DateToSend <> Today()

                                If sdm:SendTo <> 1
                                    If pem_ali:CustomerUTL = 'UTL'
                                        ! Found Reminder
                                        local:FoundUTLReminder = 1
                                    End ! If pem_ali:CustomerUTL = 'UTL'
                                End ! If sdm:Send = 2 Or sdm:SendTo = 3
                                If sdm:SendTo <> 2
                                    If pem_ali:CustomerUTL = 'CUSTOMER'
                                        ! Found Reminder
                                        local:FoundCustomerReminder = 1
                                    End ! If pem_ali:CustomerUTL = 'CUSTOMER'
                                End ! If sdm:SendTo = 1
                            End ! Loop

                            ! TrackerBase 11310 - GK 12/03/2010
                            local:SMSMessageText = sdm:SMSMessageText
                            local:EmailMessageSubject = sdm:EmailSubject
                            local:EmailMessageText = ''

                            Access:SIDREMI2.ClearKey(sdm2:SIDREMINRecordNumberKey)
                            sdm2:SIDREMINRecordNumber = sdm:RecordNumber
                            If Access:SIDREMI2.TryFetch(sdm2:SIDREMINRecordNumberKey) = Level:Benign
                                !Found
                                local:EmailMessageText = sdm2:EmailMessageText
                            End

                            Access:SMOREMIN.ClearKey(smm:SIDREMINRecordNumberKey)
                            smm:SIDREMINRecordNumber = sdm:RecordNumber
                            smm:ModelNumber = job:Model_Number
                            If Access:SMOREMIN.TryFetch(smm:SIDREMINRecordNumberKey) = Level:Benign
                                ! Exceptions only override if default reminder is enabled
                                If sdm:SMSAlertActive = 1 and smm:SMSAlertActive = 1
                                    local:SMSMessageText = smm:SMSMessageText
                                End
                                If sdm:EmailAlertActive = 1 and smm:EmailAlertActive = 1
                                    local:EmailMessageSubject = smm:EmailSubject

                                    Access:SMOREMI2.ClearKey(smm2:SMOREMINRecordNumberKey)
                                    smm2:SMOREMINRecordNumber = smm:RecordNumber
                                    If Access:SMOREMI2.TryFetch(smm2:SMOREMINRecordNumberKey) = Level:Benign
                                        !Found
                                        local:EmailMessageText = smm2:EmailMessageText
                                    End
                                End
                            End

                            If (sdm:SendTo <> 1) And local:FoundUTLReminder = 0
                                ! A UTL Reminder should be sent, and no other UTL message has been sent today (DBH: 25/09/2007)
                                If sdm:EmailAlertActive = 1
                                    ! Add to alert viewer and send straight away (DBH: 25/09/2007)
                                    If clip(local:EmailMessageText) <> ''
                                        !Found
                                        If Access:PENDMAIL.PrimeRecord() = Level:Benign
                                            pem:SMSEmail = 'EMAIL'
                                            pem:RefNumber = job:Ref_Number
                                            pem:DateToSend = Today()
                                            pem:TimeToSend = Clock()
                                            pem:BookingType = sdl:BookingType
                                            pem:Status = sdl:Status
                                            pem:SIDJobNumber = job:Order_Number
                                            pem:EmailAddress = sdd:UTLEmailAddress
                                            pem:CustomerUTL = 'UTL'
                                            pem:Reminder = 0
                                            If Access:PENDMAIL.TryInsert() = Level:Benign
                                                !Insert
                                                If SendEmail(pem:EmailAddress, sdd:EmailFromAddress, local:EmailMessageSubject, '', GetCompleteText(local:EmailMessageText, pem:SMSEmail),1,tmp:ErrorMessage) = 1
!                                                    ErrorLog('Found Job (Email): ' & Clip(pem:RefNumber) & '. Type: (' & CLip(sdl:Status) & ') ' & Clip(sdm:Description) & '. To: ' & Clip(pem:EmailAddress))
                                                    pem:DateSent = Today()
                                                    pem:TimeSent = Clock()
                                                    Access:PENDMAIL.Update()
                                                Else
                                                    ! Email failed to send, write error message. Attempt to send again on next run.
                                                    pem:ErrorMessage = clip(tmp:ErrorMessage)
                                                    Access:PENDMAIL.Update()
                                                End ! If SendEmail(pem:EmailAddress, sdd:EmailFromAddress, sdl:EmailSubject, '', sdl2:EmailMessageText,1) = 1
                                            Else ! If Access:PENDMAIL.TryInsert() = Level:Benign
                                                Access:PENDMAIL.CancelAutoInc()
                                            End ! If Access:PENDMAIL.TryInsert() = Level:Benign
                                        End ! If Access.PENDMAIL.PrimeRecord() = Level:Benign
                                    End
                                End ! If sdm:EmailAlertActive = 1
                                If sdm:SMSAlertActive = 1
                                    ! No UTL SMS at this time (DBH: 25/09/2007)
                                    local:SMSMessageText = BHStripReplace(local:SMSMessageText,'<13,10>',' ')
                                End ! If sdm:SMSAlertActive = 1

                            End ! If sdm:SendTo = 2 Or sdm:SendTo = 3

                            If (sdm:SendTo <> 2) And local:FoundCustomerReminder = 0
                                ! A customer reminder should be sent and no other message has been found (DBH: 25/09/2007)
                                If sdm:EmailAlertActive = 1
                                    If sql3:EMailReq = 1
                                        ! Add to alert viewer and send straight away (DBH: 25/09/2007)
                                        If clip(local:EmailMessageText) <> ''
                                            !Found
                                            If Access:PENDMAIL.PrimeRecord() = Level:Benign
                                                pem:SMSEmail = 'EMAIL'
                                                pem:RefNumber = job:Ref_Number
                                                pem:DateToSend = Today()
                                                pem:TimeToSend = Clock()
                                                pem:BookingType = sdl:BookingType
                                                pem:Status = sdl:Status
                                                pem:SIDJobNumber = job:Order_Number
                                                pem:EmailAddress = jobe:EndUserEmailAddress
                                                pem:CustomerUTL = 'CUSTOMER'
                                                pem:Reminder = 0
                                                If Access:PENDMAIL.TryInsert() = Level:Benign
                                                    !Insert
                                                    If SendEmail(pem:EmailAddress, sdd:EmailFromAddress, local:EmailMessageSubject, '', GetCompleteText(local:EmailMessageText, pem:SMSEmail),1,tmp:ErrorMessage) = 1
!                                                        ErrorLog('Found Job (Email): ' & Clip(pem:RefNumber) & '. Type: (' & CLip(sdl:Status) & ') ' & Clip(sdm:Description) & '. To: ' & Clip(pem:EmailAddress))
                                                        pem:DateSent = Today()
                                                        pem:TimeSent = Clock()
                                                        Access:PENDMAIL.Update()
                                                    Else
                                                        ! Email failed to send, write error message. Attempt to send again on next run.
                                                        pem:ErrorMessage = clip(tmp:ErrorMessage)
                                                        Access:PENDMAIL.Update()
                                                    End ! If SendEmail(pem:EmailAddress, sdd:EmailFromAddress, sdl:EmailSubject, '', sdl2:EmailMessageText,1) = 1
                                                Else ! If Access:PENDMAIL.TryInsert() = Level:Benign
                                                    Access:PENDMAIL.CancelAutoInc()
                                                End ! If Access:PENDMAIL.TryInsert() = Level:Benign

                                            End ! If Access.PENDMAIL.PrimeRecord() = Level:Benign


                                        Else ! If Access:SIDREMI2.TryFetch(sdm2:SIDREMINRecordNumberKey) = Level:Benign
                                            !Error
                                        End ! If Access:SIDREMI2.TryFetch(sdm2:SIDREMINRecordNumberKey) = Level:Benign

                                    End ! If sql3:SQLEMailReq = 1
                                End ! If sdm:EmailAlertActive = 1

                                If sdm:SMSAlertActive = 1 And sendSMS = 1
                                    If sql3:SMSReq = 1
                                        local:DateToSend = Today()
                                        local:TimeToSend = Clock()

                                        ! Is the next day a bank holiday (DBH: 21/09/2007)
                                        If IsBankHoliday(sql3:CountryDelivery,local:DateToSend)
                                            Case sdl:BankHolidays
                                            Of 1 ! Send
                                                ! Send message on bank holidays (DBH: 21/09/2007)
                                            Of 2 ! Send Next Day
                                                ! Send on the next "non" bank holiday (DBH: 21/09/2007)
                                                Loop
                                                    local:DateToSend += 1
                                                    If ~IsBankHoliday(sql3:CountryDelivery,local:DateToSend)
                                                        ! Date is NOT a bank holiday (DBH: 21/09/2007)
                                                        ! Determine time to send based on if next day is weekend or not (DBH: 21/09/2007)
                                                        If local:DateToSend % 7 = 6 Or local:DateToSend % 7 = 7
                                                            local:TimeToSend = Deformat('10:00',@t4)
                                                        Else ! If local:DateToSend % 7 = 6 Or local:DateToSend % 7 = 7
                                                            local:TimeToSend = Deformat('08:00',@t4)
                                                        End ! If local:DateToSend % 7 = 6 Or local:DateToSend % 7 = 7
                                                        Break
                                                    End ! If IsBankHoliday(sql3:SQLCountryDelivery,local:DateToSend)

                                                End ! Loop
                                            Of 3 ! Discard
                                                ! Discard if generated on a bank holiday (DBH: 21/09/2007)
                                                Cycle
                                            End ! Case sdl:BankHolidays
                                        End ! If IsBankHoliday(sql3:SQLCountryDelivery,local:DateToSend)

                                        if IsMobileNoExcluded(pem:MobileNumber) then cycle.

                                        If Access:PENDMAIL.PrimeRecord() = Level:Benign
                                            pem:DateToSend = local:DateToSend
                                            pem:TimeToSend = local:TimeToSend
                                            pem:SMSEmail = 'SMS'
                                            pem:RefNumber = job:Ref_Number
                                            pem:BookingType = sdl:BookingType
                                            pem:Status = sdl:Status
                                            pem:SIDJobNumber = job:Order_Number
                                            pem:MobileNumber = jobe:TraFaultCode5
                                            pem:CustomerUTL = 'CUSTOMER'
                                            If Access:PENDMAIL.TryInsert() = Level:Benign
                                                !Insert
                                                If Sub(pem:MobileNumber, 1, 1) = '0'
                                                    pem:MobileNumber = '+44' & Sub(pem:MobileNumber, 2, 30)
                                                    Access:PENDMAIL.TryUpdate()
                                                End ! If Sub(pem:MobileNumber,1,1) = '0'
                                                local:SMSMessageText = BHStripReplace(local:SMSMessageText,'<13,10>',' ')

!                                                ErrorLog('Found Job (SMS): ' & Clip(pem:RefNumber) & '. Type: (' & CLip(sdl:Status) & ') ' & Clip(sdl:Description) & '. To: ' & Clip(pem:MobileNumber))

!                                                SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & JobsQ.jobNo

                                                PostString = Clip(PostString) & '<SMSMESSAGE>'
                                                PostString = Clip(PostString) & '<DESTINATION_ADDR>' & Clip(pem:MobileNumber) & '</DESTINATION_ADDR>'
! Changing (DBH 30/04/2008) # N/A - Allow for multipart message
!                                                PostString = Clip(PostString) & '<TEXT>' & Clip(GetCompleteText(sdm:SMSMessageText)) & '</TEXT>'
!                                                PostString = Clip(PostString) & '<TRANSACTIONID>' & pem:RecordNumber & '</TRANSACTIONID>'
! to (DBH 30/04/2008) # N/A
                                                times# = 1000
                                                len# = Len(Clip(GetCompleteText(local:SMSMessageText, pem:SMSEmail)))
                                                text# = 1
                                                Loop Until text# >= len#
                                                    PostString = Clip(PostString) & '<TEXT>' & Sub(Clip(GetCompleteText(local:SMSMessageText, pem:SMSEmail)),text#,155) & '</TEXT>'
                                                    text# += 155
                                                    times# += 1
                                                End ! Loop x# = 1 To 1000 Step 156
                                                ! Trans ID to show the amount of messages sent (DBH: 30/04/2008)
                                                PostString = Clip(PostString) & '<TRANSACTIONID>1001:' & times# & '-' & pem:RecordNumber & '</TRANSACTIONID>'
! End (DBH 30/04/2008) #N/A
                                                PostString = Clip(PostString) & '<TYPEID>2</TYPEID>'
                                                PostString = Clip(PostString) & '<SERVICEID>3</SERVICEID>'
                                                PostString = Clip(PostString) & '<COSTID>1</COSTID>'
                                                PostString = Clip(PostString) & '<SOURCE_ADDR>VODAFONE</SOURCE_ADDR>'
                                                PostString = Clip(PostString) & '</SMSMESSAGE>'

                                                tmp:CountSMS += 1

! Deleting (DBH 23/04/2008) # N/A - Add to queue to mark as sent later
!                                                pem:DateSent = Today()
!                                                pem:TimeSent = Clock()
!                                                Access:PENDMAIL.Update()
! End (DBH 23/04/2008) #N/A

                                                ! Inserting (DBH 23/04/2008) # N/A - Add entry to queue so can mark as sent later
                                                smsque:PENDMAILRecordNumber = pem:RecordNumber
                                                ! Check this entry in pendmail hasn't already been added to the queue (DBH: 23/04/2008)
                                                Get(SMSQueue,smsque:PENDMAILRecordNumber)
                                                If Error()
                                                    smsque:PENDMAILRecordNumber = pem:RecordNumber
                                                    Add(SMSQueue)
                                                    ! Add the record of the pendmail entry, and the record of the message to send (DBH: 23/04/2008)
                                                End ! If Error()
                                                ! End (DBH 23/04/2008) #N/A

! Changing (DBH 23/04/2008) # N/A - Use queue to mark sms's as sent
!                                                If tmp:CountSMS >= 25
!                                                    ! SMS system can handle 50 messages at a time, so I'll send a little less, just be sure (DBH: 02/11/2007)
!                                                    PostString = '<?xml version="1.0" standalone="no"?><!DOCTYPE WIN_DELIVERY_2_SMS SYSTEM "winbound_messages_v1.dtd"><WIN_DELIVERY_2_SMS>' & Clip(PostString) & '</WIN_DELIVERY_2_SMS>'
!                                                    Loop x# = 1 To 200000 By 100
!                                                        tmp:FinalString = Clip(tmp:FinalString) & ThisWebClient.EncodeWebString(Sub(PostString,x#,100))
!                                                    End ! Loop x# = 1 To 100000 Step By 100
!
!                                                    tmp:FinalString = 'User=' & Clip(sdd:SMSUserName) & '&Password=' & CLip(sdd:SMSPassword) & '&RequestID=123_456&WIN_XML=' & CliP(tmp:FinalString)
!
!                                                    PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString))
!
!                                                    ! Reset count, and first line of post message (DBH: 02/11/2007)
!                                                    tmp:CountSMS = 0
!                                                    PostString = ''
!                                                    tmp:FinalString = ''
!                                                End ! If tmp:CountSMS = 49
! to (DBH 23/04/2008) # N/A
                                                If Records(SMSQueue) >= 25
                                                    PostString = '<?xml version="1.0" standalone="no"?><!DOCTYPE WIN_DELIVERY_2_SMS SYSTEM "winbound_messages_v1.dtd"><WIN_DELIVERY_2_SMS>' & Clip(PostString) & '</WIN_DELIVERY_2_SMS>'
                                                    Loop x# = 1 To 200000 By 100
                                                        tmp:FinalString = Clip(tmp:FinalString) & ThisWebClient.EncodeWebString(Sub(PostString,x#,100))
                                                    End ! Loop x# = 1 To 100000 Step By 100

                                                    tmp:FinalString = 'User=' & Clip(sdd:SMSUserName) & '&Password=' & CLip(sdd:SMSPassword) & '&RequestID=123_456&WIN_XML=' & CliP(tmp:FinalString)

                                                    If PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString)) = 0
                                                        ! Posting was a success. Now mark pendmail entries as sent. (DBH: 23/04/2008)
                                                        Loop x# = 1 To Records(SMSQueue)
                                                            Get(SMSQueue,x#)
                                                            Access:PENDMAIL_ALIAS.Clearkey(pem_ali:RecordNumberKey)
                                                            pem_ali:RecordNumber = smsque:PENDMAILRecordNumber
                                                            If Access:PENDMAIL_ALIAS.TryFetch(pem_ali:RecordNumberKey) = Level:Benign
                                                                pem_ali:DateSent = Today()
                                                                pem_ali:TimeSent = Clock()
                                                                Access:PENDMAIL_ALIAS.TryUpdate()
                                                                SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & pem_ali:SIDJobNumber
                                                            End ! If Access:PENDMAIL.TryFetch(pem:RecordNumberKey) = Level:Benign
                                                        End ! Loop x# = 1 To Records(SMSQueue)
                                                    End ! If PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString)) = 0
                                                    Free(SMSQueue)
                                                    Clear(SMSQueue)
                                                    PostString = ''
                                                    tmp:FinalString = ''
                                                    ! Ok start again (DBH: 23/04/2008)
                                                End ! If Records(SMSQueue) >= 25
! End (DBH 23/04/2008) #N/A

                                            Else ! If Access:PENDMAIL.TryInsert() = Level:Benign
                                                Access:PENDMAIL.CancelAutoInc()
                                            End ! If Access:PENDMAIL.TryInsert() = Level:Benign
                                        End ! If Access.PENDMAIL.PrimeRecord() = Level:Benign
                                    End ! If sql3:SQLSMSReq = 1
                                End ! If sdm:SMSAlertActive = 1
                            End ! If sdm:SendTo = 1 And local:FoundCustomerReminder = 0
                        End ! Loop

                    End ! Loop

                Else ! If Sub(job:Current_Status,1,3) = pem:Status
                    ! The job status has changed (DBH: 25/09/2007)
                    ! Find any entires with live "reminder" for this status. They no longer apply. (DBH: 25/09/2007)
                    Access:PENDMAIL_ALIAS.Clearkey(pem_ali:ReminderRefNumberKey)
                    pem_ali:Reminder = 1
                    pem_ali:RefNumber = job:Ref_Number
                    Set(pem_ali:ReminderRefNumberKey,pem_ali:ReminderRefNumberKey)
                    Loop ! Begin Loop
                        If Access:PENDMAIL_ALIAS.Next()
                            Break
                        End ! If Access:PENDMAIL_ALIAS.Next()
                        If pem_ali:Reminder <> 1
                            Break
                        End ! If pem_ali:Reminder <> 1
                        If pem_ali:RefNumber <> job:Ref_Number
                            Break
                        End ! If pem_ali:RefNumber <> job:Ref_Number
                        If pem_ali:Status = pem:Status
                            pem_ali:Reminder = 0
                            Access:PENDMAIL_ALIAS.Update()
                        End ! If pem_ali:Status = pem:Status
                    End ! Loop


                End ! If Sub(job:Current_Status,1,3) = pem:Status
                Break
            End ! Loop

        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Error
        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
    End ! Loop
! Changing (DBH 23/04/2008) # N/A - Use queue to send sms's
!    If tmp:CountSMS > 0
!        PostString = '<?xml version="1.0" standalone="no"?><!DOCTYPE WIN_DELIVERY_2_SMS SYSTEM "winbound_messages_v1.dtd"><WIN_DELIVERY_2_SMS>' & Clip(PostString) & '</WIN_DELIVERY_2_SMS>'
!        Loop x# = 1 To 200000 By 100
!            tmp:FinalString = Clip(tmp:FinalString) & ThisWebClient.EncodeWebString(Sub(PostString,x#,100))
!        End ! Loop x# = 1 To 100000 Step By 100
!
!        tmp:FinalString = 'User=' & Clip(sdd:SMSUserName) & '&Password=' & CLip(sdd:SMSPassword) & '&RequestID=123_456&WIN_XML=' & CliP(tmp:FinalString)
!
!        PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString))
!    End ! If tmp:CountSMS > 0
! to (DBH 23/04/2008) # N/A
    ! Are they any messages left to send? (DBH: 23/04/2008)
    If Records(SMSQueue) > 0
        PostString = '<?xml version="1.0" standalone="no"?><!DOCTYPE WIN_DELIVERY_2_SMS SYSTEM "winbound_messages_v1.dtd"><WIN_DELIVERY_2_SMS>' & Clip(PostString) & '</WIN_DELIVERY_2_SMS>'
        Loop x# = 1 To 200000 By 100
            tmp:FinalString = Clip(tmp:FinalString) & ThisWebClient.EncodeWebString(Sub(PostString,x#,100))
        End ! Loop x# = 1 To 100000 Step By 100

        tmp:FinalString = 'User=' & Clip(sdd:SMSUserName) & '&Password=' & CLip(sdd:SMSPassword) & '&RequestID=123_456&WIN_XML=' & CliP(tmp:FinalString)

        If PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString)) = 0
            ! Posting was a success. Now mark pendmail entries as sent. (DBH: 23/04/2008)
            Loop x# = 1 To Records(SMSQueue)
                Get(SMSQueue,x#)
                Access:PENDMAIL_ALIAS.Clearkey(pem_ali:RecordNumberKey)
                pem_ali:RecordNumber = smsque:PENDMAILRecordNumber
                If Access:PENDMAIL_ALIAS.TryFetch(pem_ali:RecordNumberKey) = Level:Benign
                    pem_ali:DateSent = Today()
                    pem_ali:TimeSent = Clock()
                    Access:PENDMAIL_ALIAS.TryUpdate()
                    SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & pem_ali:SIDJobNumber
                End ! If Access:PENDMAIL.TryFetch(pem:RecordNumberKey) = Level:Benign
            End ! Loop x# = 1 To Records(SMSQueue)
        End ! If PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString)) = 0
        Free(SMSQueue)
        Clear(SMSQueue)
        PostString = ''
        tmp:FinalString = ''
    End ! If Records(SMSQueue) > 0
! End (DBH 23/04/2008) #N/A

    Close(SQLFile3)

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  Relate:DEFEMAIL.Open
  Relate:JOBS.Open
  Relate:MERGE.Open
  Relate:PENDMAIL.Open
  Relate:PENDMAIL_ALIAS.Open
  Relate:SIDALDEF.Open
  Relate:SIDALER2.Open
  Access:SIDALERT.UseFile
  Access:SIDREMI2.UseFile
  Access:SIDREMIN.UseFile
  Access:JOBSE.UseFile
  Access:MODELNUM.UseFile
  Access:SMOALERT.UseFile
  Access:SMOALER2.UseFile
  Access:SMOREMIN.UseFile
  Access:SMOREMI2.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
                                               ! Generated by NetTalk Extension (Start)
  ThisWebClient.SuppressErrorMsg = 1         ! No Object Generated Error Messages ! Generated by NetTalk Extension
  ThisWebClient.init()
  if ThisWebClient.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisWebClient.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFEMAIL.Close
    Relate:JOBS.Close
    Relate:MERGE.Close
    Relate:PENDMAIL.Close
    Relate:PENDMAIL_ALIAS.Close
    Relate:SIDALDEF.Close
    Relate:SIDALER2.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ================================================================
  ! Initialise Xml Export Object
    objXmlExport.FInit(fileXmlExport)
  ! ================================================================
  ReturnValue = PARENT.Run()
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisWebClient.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !== Processed Code Goes Here ==
          iniFilePath = path() & '\Webimp.ini'
          connectionStr = getini('Defaults', 'Connection','', clip(iniFilePath))
      
          ! Some errors such as the email address field being empty mean that the pendmail record will never
          ! be sent. For these cases write the following error date/time to stop them from being read on each run.
          ! Once Vodafone/UTL allow us to add more validation into Contact Centre Lite this should not be necessary.
          ErrorDate = deformat('05/11/1955', @d06b) ! Nick's chosen date from 'Back To The Future'
          ErrorTime = ''
      
          sendEmail = 1
          sendSMS   = 1
      
          ! If the current time is after 9pm or before 4am then do not send SMS's
          If Clock() > Deformat('21:00',@t01b) Or Clock() < Deformat('08:00',@t01b)
              sendSMS = 0
          End ! If Clock() > Deformat('21:00',@t01b) Or Clock() < Deformat('04:00',@t01b)
      
          Set(SIDALDEF, 0)
          Access:SIDALDEF.Next()
      
          Do FindAndSendReminders
      
          If sendSMS = 1
              tmp:CountSMS = 0
              PostString = ''
              tmp:FinalString = ''
              Free(SMSQueue)
              Clear(SMSQueue)
              Access:PENDMAIL.Clearkey(pem:SMSSentDateCreatedKey)
              pem:SMSEmail = 'SMS'
              pem:DateSent = ''
              pem:DateCreated = today()
              pem:TimeCreated = clock()
              Set(pem:SMSSentDateCreatedKey,pem:SMSSentDateCreatedKey)
              Loop ! Begin Loop
                  If Access:PENDMAIL.Next()
                      Break
                  End ! If Access:PENDMAIL.Next()
                  If pem:SMSEmail <> 'SMS'
                      Break
                  End ! If pem:SMSEmail <> 'SMS'
                  If pem:DateSent <> ''
                      Break
                  End ! If pem:DateSent <> ''
      
                  ! Has the date/time to send past?
                  If Today() > pem:DateToSend Or (Today() = pem:DateToSend And Clock() >= pem:TimeToSend)
      
                      If IsMobileNoExcluded(pem:MobileNumber)
                          pem:DateSent = ErrorDate
                          pem:TimeSent = ErrorTime
                          Access:PENDMAIL.TryUpdate()
                          Cycle
                      End
      
                      ! Reformat the mobile number if necessary
                      If Sub(pem:MobileNumber,1,1) = '0'
                          pem:MobileNumber = '+44' & Sub(pem:MobileNumber,2,30)
                          Access:PENDMAIL.TryUpdate()
                      End ! If Sub(pem:MobileNumber,1,1) = '0'
      
                      Access:JOBS.Clearkey(job:Ref_Number_Key)
                      Job:Ref_Number = pem:RefNumber
                      If Access:JOBS.TryFetch(job:Ref_Number_Key) <> Level:Benign
                          pem:DateSent = ErrorDate
                          pem:TimeSent = ErrorTime
                          Access:PENDMAIL.TryUpdate()
                          Cycle
                      End
      
                      ! Find the message text to send and send it
                      If ExportMessageText('SMS', job:Model_Number)
                          ! An Error Has Occured
                          Cycle
                      Else ! If ExportMessageText('SMS')
      ! Deleting (DBH 23/04/2008) # N/A - Pendmail will be updated IF the posting is success
      !                    tmp:CountSMS += 1
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                    Access:PENDMAIL.Update()
      ! End (DBH 23/04/2008) #N/A
      
      ! Changing (DBH 23/04/2008) # N/A - User a queue to send the sms's
      !                    If tmp:CountSMS >= 25
      !                        ! SMS system can handle 50 messages at a time, so I'll send a little less, just be sure (DBH: 02/11/2007)
      !                        PostString = '<?xml version="1.0" standalone="no"?><!DOCTYPE WIN_DELIVERY_2_SMS SYSTEM "winbound_messages_v1.dtd"><WIN_DELIVERY_2_SMS>' & Clip(PostString) & '</WIN_DELIVERY_2_SMS>'
      !                        Loop x# = 1 To 200000 By 100
      !                            tmp:FinalString = Clip(tmp:FinalString) & ThisWebClient.EncodeWebString(Sub(PostString,x#,100))
      !                        End ! Loop x# = 1 To 100000 Step By 100
      !
      !                        tmp:FinalString = 'User=' & Clip(sdd:SMSUserName) & '&Password=' & CLip(sdd:SMSPassword) & '&RequestID=123_456&WIN_XML=' & CliP(tmp:FinalString)
      !
      !                        PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString))
      !
      !                        ! Reset count, and first line of post message (DBH: 02/11/2007)
      !                        tmp:CountSMS = 0
      !                        PostString = ''
      !                        tmp:FinalString = ''
      !                    End ! If tmp:CountSMS = 49
      ! to (DBH 23/04/2008) # N/A
                          If Records(SMSQueue) >= 25
                              PostString = '<?xml version="1.0" standalone="no"?><!DOCTYPE WIN_DELIVERY_2_SMS SYSTEM "winbound_messages_v1.dtd"><WIN_DELIVERY_2_SMS>' & Clip(PostString) & '</WIN_DELIVERY_2_SMS>'
                              Loop x# = 1 To 200000 By 100
                                  tmp:FinalString = Clip(tmp:FinalString) & ThisWebClient.EncodeWebString(Sub(PostString,x#,100))
                              End ! Loop x# = 1 To 100000 Step By 100
      
                              tmp:FinalString = 'User=' & Clip(sdd:SMSUserName) & '&Password=' & CLip(sdd:SMSPassword) & '&RequestID=123_456&WIN_XML=' & CliP(tmp:FinalString)
      
                              If PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString)) = 0
                                  
                                  ! Posting was a success. Now mark pendmail entries as sent. (DBH: 23/04/2008)
                                  Loop x# = 1 To Records(SMSQueue)
                                      Get(SMSQueue,x#)
                                      Access:PENDMAIL_ALIAS.Clearkey(pem_ali:RecordNumberKey)
                                      pem_ali:RecordNumber = smsque:PENDMAILRecordNumber
                                      If Access:PENDMAIL_ALIAS.TryFetch(pem_ali:RecordNumberKey) = Level:Benign
                                          pem_ali:DateSent = Today()
                                          pem_ali:TimeSent = Clock()
                                          Access:PENDMAIL_ALIAS.TryUpdate()
                                          SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & pem_ali:SIDJobNumber
                                      End ! If Access:PENDMAIL.TryFetch(pem:RecordNumberKey) = Level:Benign
                                  End ! Loop x# = 1 To Records(SMSQueue)
                              End ! If PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString)) = 0
                              Free(SMSQueue)
                              Clear(SMSQueue)
                              PostString = ''
                              tmp:FinalString = ''
                              ! Ok start again (DBH: 23/04/2008)
                          End ! If Records(SMSQueue) >= 25
      ! End (DBH 23/04/2008) #N/A
                      End ! If ExportMessageText('SMS')
                  End ! If Today() > pem:DateToSend Or (Today() = pem:DateToSend And Clock() >= pem:TimeToSend)
              End ! Loop
      
      ! Changing (DBH 23/04/2008) # N/A - Use a queue to send sms's
      !        If tmp:CountSMS > 0
      !            PostString = '<?xml version="1.0" standalone="no"?><!DOCTYPE WIN_DELIVERY_2_SMS SYSTEM "winbound_messages_v1.dtd"><WIN_DELIVERY_2_SMS>' & Clip(PostString) & '</WIN_DELIVERY_2_SMS>'
      !            Loop x# = 1 To 200000 By 100
      !                tmp:FinalString = Clip(tmp:FinalString) & ThisWebClient.EncodeWebString(Sub(PostString,x#,100))
      !            End ! Loop x# = 1 To 100000 Step By 100
      !
      !            tmp:FinalString = 'User=' & Clip(sdd:SMSUserName) & '&Password=' & CLip(sdd:SMSPassword) & '&RequestID=123_456&WIN_XML=' & CliP(tmp:FinalString)
      !
      !            PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString))
      !        End ! If tmp:CountSMS > 0
      ! to (DBH 23/04/2008) # N/A
              ! Are they any messages left to send? (DBH: 23/04/2008)
              If Records(SMSQueue) > 0
                  PostString = '<?xml version="1.0" standalone="no"?><!DOCTYPE WIN_DELIVERY_2_SMS SYSTEM "winbound_messages_v1.dtd"><WIN_DELIVERY_2_SMS>' & Clip(PostString) & '</WIN_DELIVERY_2_SMS>'
                  Loop x# = 1 To 200000 By 100
                      tmp:FinalString = Clip(tmp:FinalString) & ThisWebClient.EncodeWebString(Sub(PostString,x#,100))
                  End ! Loop x# = 1 To 100000 Step By 100
      
                  tmp:FinalString = 'User=' & Clip(sdd:SMSUserName) & '&Password=' & CLip(sdd:SMSPassword) & '&RequestID=123_456&WIN_XML=' & CliP(tmp:FinalString)
      
                  !SetClipBoard(Clip(sdd:SMSURL) & ' == ' & Clip(tmp:FinalString))
                  If PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString)) = 0
                      ! Posting was a success. Now mark pendmail entries as sent. (DBH: 23/04/2008)
                      Loop x# = 1 To Records(SMSQueue)
                          Get(SMSQueue,x#)
                          Access:PENDMAIL_ALIAS.Clearkey(pem_ali:RecordNumberKey)
                          pem_ali:RecordNumber = smsque:PENDMAILRecordNumber
                          If Access:PENDMAIL_ALIAS.TryFetch(pem_ali:RecordNumberKey) = Level:Benign
                              pem_ali:DateSent = Today()
                              pem_ali:TimeSent = Clock()
                              Access:PENDMAIL_ALIAS.TryUpdate()
                              SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & pem_ali:SIDJobNumber
                          End ! If Access:PENDMAIL.TryFetch(pem:RecordNumberKey) = Level:Benign
                      End ! Loop x# = 1 To Records(SMSQueue)
                  End ! If PostSMS(Clip(sdd:SMSURL),Clip(tmp:FinalString)) = 0
                  Free(SMSQueue)
                  Clear(SMSQueue)
                  PostString = ''
                  tmp:FinalString = ''
              End ! If Records(SMSQueue) > 0
      ! End (DBH 23/04/2008) #N/A
          End ! If sendSMS = 1
      
          If sendEmail = 1
              Access:PENDMAIL.Clearkey(pem:SMSSentDateCreatedKey)
              pem:SMSEmail = 'EMAIL'
              pem:DateSent = ''
              pem:DateCreated = today()
              pem:TimeCreated = clock()
              Set(pem:SMSSentDateCreatedKey,pem:SMSSentDateCreatedKey)
              Loop ! Begin Loop
                  If Access:PENDMAIL.Next()
                      Break
                  End ! If Access:PENDMAIL.Next()
                  If pem:SMSEmail <> 'EMAIL'
                      Break
                  End ! If pem:SMSEmail <> 'EMAIL'
                  If pem:DateSent <> ''
                      Break
                  End ! If pem:DateSent <> ''
                  ! Has the date/time to send past?
                  If Today() > pem:DateToSend Or (Today() = pem:DateToSend And Clock() >= pem:TimeToSend)
      
                      Access:JOBS.Clearkey(job:Ref_Number_Key)
                      Job:Ref_Number = pem:RefNumber
                      If Access:JOBS.TryFetch(job:Ref_Number_Key) <> Level:Benign
                          pem:DateSent = ErrorDate
                          pem:TimeSent = ErrorTime
                          Access:PENDMAIL.TryUpdate()
                          Cycle
                      End
      
                      ! Find the message text to send and send it
                      If ExportMessageText('EMAIL', job:Model_Number)
                          ! Error has occured with email
                          Cycle
                      Else ! If ExportMessageText('EMAIL')
                          pem:DateSent = Today()
                          pem:TimeSent = Clock()
                          Access:PENDMAIL.TryUpdate()
                      End ! If ExportMessageText('EMAIL')
                  End ! If Today() > pem:DateToSend Or (Today() = pem:DateToSend And Clock() >= pem:TimeToSend)
              End ! Loop
      
          End ! If sendEmail = 1
      ! Deleting (DBH 24/09/2007) # 8943 - New process below
      !    iniFilepath   = path() & '\Webimp.ini'
      !    connectionStr = getini('Defaults', 'Connection', '', clip(iniFilepath))
      !
      !    ErrorLog('==== Logging Started At ' & format(today(), @d06b) & ' ' & format(clock(), @t06b) & ' ====')
      !
      !    sendEmail = true
      !    sendSMS   = true
      !
      !    ! If the current time is after 9pm or before 4am then do not send any SMS's
      !    if clock() > deformat('21:00', @t01b) or clock() < deformat('04:00', @t01b)
      !        sendSMS = false
      !    end ! if
      !
      !    free(JobsQ)
      !
      !    ! Changing (DBH 03/01/2006) #6907 - Change subject
      !    ! emailSubject = 'VODAFONE REPAIRS'
      !    ! to (DBH 03/01/2006) #6907
      !    emailSubject = 'Vodafone ULS Outbound'
      !    ! End (DBH 03/01/2006) #6907
      !
      !    if sendSMS = true
      !        ! ---------------------------------------------------------------------------------------------
      !        ! Send SMS messages
      !
      !        ! Inserting (DBH 03/01/2006) #6907 - Create the temp csv file that will be emailled
      !        tmp:CSVFile = 'MOBILEFILE ' & Format(Today(), @d12) & Format(Clock(), @t5) & '.CSV'
      !        If GetTempPathA(255, TempFilePath)
      !            If Sub(TempFilePath, -1, 1) = '\'
      !                tmp:CSVFile = Clip(TempFilePath) & Clip(tmp:CSVFile)
      !            Else ! If Sub(TempFilePath,-1,1) = '\'
      !                tmp:CSVFile = Clip(TempFilePath) & '\' & Clip(tmp:CSVFile)
      !            End ! If Sub(TempFilePath,-1,1) = '\'
      !        End ! If
      !        Create(CSVFile)
      !        Open(CSVFile)
      !        ! End (DBH 03/01/2006) #6907
      !
      !        open(SQLFile)
      !
      !        Relate:DEFEMAIL.Open()
      !
      !        set(DEFEMAIL)
      !        Access:DEFEMAIL.Next()
      !
      !        ! Initial booking
      !
      !        ! Get job numbers from webjobs where SMS notifications are enabled, initial booking, sms has not been sent
      !        SQLFile{PROP:SQL} = 'SELECT Ref_Number, SBJobNo FROM WebJob WHERE SMSType = ''I'' AND SMSReq = 1 AND SMSSent = 0' ! SMS
      !        tmp:RType         = 'R1'
      !        tmp:SMS           = true
      !        
      !        Do AddToPendMail
      !
      !        SQLFile{PROP:SQL} = 'SELECT Ref_Number, SBJobNo FROM WebJob WHERE SMSType = ''I'' AND EmailReq = 1 AND EmailSent = 0' ! Email
      !        tmp:RType         = 'R1'
      !        tmp:SMS           = false
      !        
      !        Do AddToPendMail
      !
      !        ! Device despatched from store
      !
      !        ! Get job numbers from webjobs where SMS notifications are enabled, initial booking, sms has not been sent
      !        SQLFile{PROP:SQL} = 'SELECT Ref_Number, SBJobNo FROM WebJob WHERE SMSType = ''S'' AND SMSReq = 1 AND SMSSent = 0' ! SMS
      !        tmp:RType         = 'R2'
      !        tmp:SMS           = true
      !        
      !        Do AddToPendMail
      !
      !        SQLFile{PROP:SQL} = 'SELECT Ref_Number, SBJobNo FROM WebJob WHERE SMSType = ''S'' AND EmailReq = 1 AND EmailSent = 0' ! Email
      !        tmp:RType         = 'R2'
      !        tmp:SMS           = false
      !        
      !        Do AddToPendMail
      !
      !        ! Device despatched from service centre
      !
      !        ! Get job numbers from webjobs where SMS notifications are enabled, unit despatched from service centre, sms has not been sent
      !        SQLFile{PROP:SQL} = 'SELECT Ref_Number, SBJobNo FROM WebJob WHERE SMSType = ''D'' AND SMSReq = 1 AND SMSSent = 0' ! SMS
      !        tmp:RType         = 'R3'
      !        tmp:SMS           = true
      !        
      !        Do AddToPendMail
      !
      !        SQLFile{PROP:SQL} = 'SELECT Ref_Number, SBJobNo FROM WebJob WHERE SMSType = ''D'' AND EmailReq = 1 AND EmailSent = 0' ! Email
      !        tmp:RType         = 'R3'
      !        tmp:SMS           = false
      !        
      !        Do AddToPendMail
      !
      !        ! Unit received at retail store
      !        ! Get job numbers from webjobs where SMS notifications are enabled, unit received at retail store, sms has not been sent
      !        SQLFile{PROP:SQL} = 'SELECT Ref_Number, SBJobNo FROM WebJob WHERE SMSType = ''R'' AND SMSReq = 1 AND SMSSent = 0' ! SMS
      !        tmp:RType         = 'R4'
      !        tmp:SMS           = true
      !
      !        Do AddToPendMail
      !
      !        SQLFile{PROP:SQL} = 'SELECT Ref_Number, SBJobNo FROM WebJob WHERE SMSType = ''R'' AND EmailReq = 1 AND EmailSent = 0' ! Email
      !        tmp:RType         = 'R4'
      !        tmp:SMS           = false
      !
      !        Do AddToPendMail
      !
      !        Relate:DEFEMAIL.Close()
      !        close(SQLFile)
      !    end ! if
      !
      !    ! TrackerBase log 7940 (GK 27/07/2006) - Send sms/email reminder to customer if they have not returned their repair envelope.
      !    RepairEnvelopeReminders()
      !
      !    ! Inserting (DBH 28/02/2006) #6915 - Loop through alert file for any SMS (add to csv) or Emails to send
      !    Relate:DEFEMAIL.Open()
      !    Relate:PENDMAIL.Open()
      !    Relate:DEFEMAB1.Open()
      !    Relate:DEFEMAB2.Open()
      !    Relate:DEFEMAB3.Open()
      !    Relate:DEFEMAB4.Open()
      !    Relate:DEFEMAB5.Open()
      !    Relate:DEFEMAB6.Open()
      !    Relate:DEFEMAB7.Open()
      !    Relate:DEFEMAE1.Open()
      !    Relate:DEFEMAE2.Open()
      !    Relate:DEFEMAE3.Open()
      !    Relate:DEFEMAI2.Open()
      !    Relate:DEFEME2B.Open()
      !    Relate:DEFEME3B.Open()
      !    Relate:DEFEMAR1.Open()
      !    Relate:DEFEMAR2.Open()
      !    Relate:DEFEMAR3.Open()
      !    Relate:DEFEMAR4.Open()
      !    Set(DEFEMAI2)
      !    Access:DEFEMAI2.Next()
      !    Set(DEFEMAB1)
      !    Access:DEFEMAB1.Next()
      !    Set(DEFEMAB2)
      !    Access:DEFEMAB2.Next()
      !    Set(DEFEMAB3)
      !    Access:DEFEMAB3.Next()
      !    Set(DEFEMAB4)
      !    Access:DEFEMAB4.Next()
      !    Set(DEFEMAB5)
      !    Access:DEFEMAB5.Next()
      !    Set(DEFEMAB6)
      !    Access:DEFEMAB6.Next()
      !    Set(DEFEMAB7)
      !    Access:DEFEMAB7.Next()
      !    Set(DEFEMAE1)
      !    Access:DEFEMAE1.Next()
      !    Set(DEFEMAE2)
      !    Access:DEFEMAE2.Next()
      !    Set(DEFEMAE3)
      !    Access:DEFEMAE3.Next()
      !    Set(DEFEME2B)
      !    Access:DEFEME2B.Next()
      !    Set(DEFEME3B)
      !    Access:DEFEME3B.Next()
      !    Set(DEFEMAR1)
      !    Access:DEFEMAR1.Next()
      !    Set(DEFEMAR2)
      !    Access:DEFEMAR2.Next()
      !    Set(DEFEMAR3)
      !    Access:DEFEMAR3.Next()
      !    Set(DEFEMAR4)
      !    Access:DEFEMAR4.Next()
      !    Set(DEFEMAIL)
      !    Access:DEFEMAIL.Next()
      !
      !    Open(SQLFile)
      !
      !    if sendSMS = true
      !
      !        Access:PENDMAIL.ClearKey(pem:SMSSentKey)
      !        pem:SMSEmail = 'SMS'
      !        pem:DateSent = ''
      !        Set(pem:SMSSentKey, pem:SMSSentKey)
      !        Loop
      !            If Access:PENDMAIL.NEXT()
      !                Break
      !            End ! If
      !            If pem:SMSEmail <> 'SMS'  |
      !                Or pem:DateSent <> '' |
      !                Then Break   ! End If
      !            End ! If
      !            If Today() > pem:DateToSend Or (Today() = pem:DateToSend AND Clock() >= pem:TimeToSend)
      !                If Sub(pem:MobileNumber, 1, 1) = '0'
      !                    pem:MobileNumber = '+44' & Sub(pem:MobileNumber, 2, 30)
      !                End ! If Sub(pem:MobileNumber,1,1) = '0'
      !                csv:Line = Clip(pem:MobileNumber) & ',",",'
      !                Case pem:MessageType
      !                Of 'E1'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:E1SMS, '<13,10>', '')
      !                Of 'E2'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:E2SMS, '<13,10>', '')
      !                Of 'E2B'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:E2BSMS, '<13,10>', '')
      !                Of 'E3'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:E3SMS, '<13,10>', '')
      !                Of 'E3B'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:E3BSMS, '<13,10>', '')
      !                Of 'B1'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:B1SMS, '<13,10>', '')
      !                Of 'B2'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:B2SMS, '<13,10>', '')
      !                Of 'B3'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:B3SMS, '<13,10>', '')
      !                Of 'B4'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:B4SMS, '<13,10>', '')
      !                Of 'B5'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:B5SMS, '<13,10>', '')
      !                Of 'B6'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:B6SMS, '<13,10>', '')
      !                Of 'B7'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem2:B7SMS, '<13,10>', '')
      !                Of 'R1'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem:BookedRetailText, '<13,10>', '')
      !                Of 'R2'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem:DespFromStoreText, '<13,10>', '')
      !                Of 'R3'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem:DespFromSCText, '<13,10>', '')
      !                Of 'R4'
      !                    csv:Line = Clip(csv:Line) & BHStripReplace(dem:ArrivedAtRetailText, '<13,10>', '')
      !                End ! Case pem:MessageType
      !                Add(CSVFile)
      !                ErrorLog('Found Job (SMS): ' & Clip(pem:RefNumber) & '. Type: ' & Clip(pem:MessageType) & '. To: ' & Clip(pem:MobileNumber))
      !                SQLFile{PROP:SQL} = 'SELECT Ref_Number, SBJobNo FROM WebJob WHERE SBJobNo = ' & pem:RefNumber
      !                Loop
      !                    Next(SQLFILE)
      !                    If ~Error()
      !                        jobsq.JobNo        = sql:SIDJobNo
      !                        jobsq.RecordNumber = pem:RecordNumber
      !                        JobsQ.updateWebJob = false
      !                        Add(JobsQ)
      !                        Break
      !                    Else
      !                        Break
      !                    End ! If ~Error()
      !                End ! Loop
      !            End ! If pem:DateToSend >= Today()
      !        End ! Loop
      !
      !        Close(SQLFile)
      !    end ! if
      !
      !    Access:PENDMAIL.ClearKey(pem:SMSSentKey)
      !    pem:SMSEmail = 'EMAIL'
      !    pem:DateSent = ''
      !    Set(pem:SMSSentKey, pem:SMSSentKey)
      !    Loop
      !        If Access:PENDMAIL.NEXT()
      !            Break
      !        End ! If
      !        If pem:SMSEmail <> 'EMAIL' |
      !            Or pem:DateSent <> ''  |
      !            Then Break   ! End If
      !        End ! If
      !        If Today() > pem:DateToSend Or (Today() = pem:DateToSend AND Clock() >= pem:TimeToSend)
      !            ErrorLog('Found Job (EMAIL): ' & Clip(pem:RefNumber) & '. Type: ' & Clip(pem:MessageType) & '. To: ' & Clip(pem:EmailAddress))
      !            Case pem:MessageType
      !            Of 'E1'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, dee1:Subject, '', dee1:E1Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'E2'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, dee2:Subject, '', dee2:E2Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'E2B'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, dee2B:Subject, '', dee2B:E2BEmail, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'E3'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, dee3:Subject, '', dee3:E3Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'E3B'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, dee3B:Subject, '', dee3B:E3BEmail, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'B1'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, deb1:Subject, '', deb1:B1Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'B2'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, deb2:Subject, '', deb2:B2Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'B3'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, deb3:Subject, '', deb3:B3Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'B4'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, deb4:Subject, '', deb4:B4Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'B5'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, deb5:Subject, '', deb5:B5Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'B6'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, deb6:Subject, '', deb6:B6Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'B7'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, deb7:Subject, '', deb7:B7Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'R1'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, der1:Subject, '', der1:R1Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                    ! Retail - update webjob
      !                    SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET EmailSent = 1 WHERE SBJobNo = ' & pem:RefNumber
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'R2'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, der2:Subject, '', der2:R2Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                    ! Retail - update webjob
      !                    SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET EmailSent = 1 WHERE SBJobNo = ' & pem:RefNumber
      !                End
      !            Of 'R3'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, der3:Subject, '', der3:R3Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                    ! Retail - update webjob
      !                    SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET EmailSent = 1 WHERE SBJobNo = ' & pem:RefNumber
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            Of 'R4'
      !                If SendEmail(pem:EmailAddress, dem:EmailFromAddress, der4:Subject, '', der4:R4Email, 1) = True
      !                    ErrorLog('Email Sent: ' & pem:RefNumber)
      !                    pem:DateSent = Today()
      !                    pem:TimeSent = Clock()
      !                    ! Retail - update webjob
      !                    SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET EmailSent = 1 WHERE SBJobNo = ' & pem:RefNumber
      !                End ! If SendEmail(emailTo, emailFrom, emailSubject, '',dee1:E1Email) = True
      !            End ! Case pem:MessageType
      !
      !            Access:PENDMAIL.Update()
      !        End ! If Clock() >= pem:TimeToSend
      !    End ! Loop
      !
      !    Relate:DEFEMAB1.Close()
      !    Relate:DEFEMAB2.Close()
      !    Relate:DEFEMAB3.Close()
      !    Relate:DEFEMAB4.Close()
      !    Relate:DEFEMAB5.Close()
      !    Relate:DEFEMAB6.Close()
      !    Relate:DEFEMAB7.Close()
      !    Relate:DEFEMAE1.Close()
      !    Relate:DEFEMAE2.Close()
      !    Relate:DEFEMAE3.Close()
      !    Relate:DEFEMAI2.Close()
      !    Relate:DEFEME2B.Close()
      !    Relate:DEFEME3B.Close()
      !    Relate:DEFEMAR1.Close()
      !    Relate:DEFEMAR2.Close()
      !    Relate:DEFEMAR3.Close()
      !    Relate:DEFEMAR4.Close()
      !
      !    ! End (DBH 28/02/2006) #6915
      !
      !    if sendSMS = true
      !
      !        ! Inserting (DBH 03/01/2006) #6907 - Email the created CSV File
      !        Close(CSVFILE)
      !        If Records(JobsQ)
      !
      !! Changing (DBH 12/06/2006) #7824 - Send via FTP
      !!             If SendEmail(emailTo, emailFrom, emailSubject, tmp:CSVFile, '', 0) = True
      !!                 ! Email should of been sent correctly. Update jobs and delete csv file - TrkBs: 6907 (DBH: 03-01-2006)
      !! to (DBH 12/06/2006) #7824
      !
      !            x# = Instring('\',tmp:CSVFile,-1,Len(tmp:CSVFile))
      !            If FTP_JumpStart(tmp:CSVFile,Sub(tmp:CSVFile,x#+1,40))
      !                ! Email should of been sent correctly. Update jobs and delete csv file - TrkBs: 6907 (DBH: 03-01-2006)
      !! End (DBH 12/06/2006) #7824
      !                loop i = 1 to records(JobsQ)
      !                    get(JobsQ, i)
      !                    if error() then break.
      !
      !                    if JobsQ.updateWebJob = true
      !                        ! if the email was sent successfully, update the SMS sent flag
      !                        SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & JobsQ.jobNo
      !                    end
      !
      !                    ! Inserting (DBH 28/02/2006) #6915 - Update the alert record to show it's been sent.
      !                    If jobsq.RecordNumber > 0
      !                        Access:PENDMAIL.ClearKey(pem:RecordNumberKey)
      !                        pem:RecordNumber = jobsq.RecordNumber
      !                        If Access:PENDMAIL.TryFetch(pem:RecordNumberKey) = Level:Benign
      !                            ! Found
      !                            pem:DateSent = Today()
      !                            pem:TimeSent = Clock()
      !                            Access:PENDMAIL.Update()
      !                        Else ! If Access:PENDMAIL.TryFetch(pem:RecordNumberKey) = Level:Benign
      !                        ! Error
      !                        End ! If Access:PENDMAIL.TryFetch(pem:RecordNumberKey) = Level:Benign
      !                    End ! If jobsq.RecordNumber > 0
      !                ! End (DBH 28/02/2006) #6915
      !                end ! loop
      !                ErrorLog('No. Of Jobs Sent Successfully: ' & Records(JobsQ))
      !                ErrorLog('FileName: ' & CLip(tmp:CSVFile))
      !                ! Remove the file
      !                remove(clip(tmp:CSVFile))
      !            Else
      !                ErrorLog('Email Error Occured')
      !            End ! If SendEmail(emailTo, emailFrom, emailSubject, dstrEmailText.Str())
      !        Else ! If Records(JobsQ)
      !            ErrorLog('No Jobs To Send')
      !        End ! If Records(JobsQ)
      !
      !    end ! if
      !    ! End (DBH 03/01/2006) #6907
      !    ErrorLog('Logging Finished At ' & format(today(), @d06b) & ' ' & format(clock(), @t06b))
      !    ErrorLog('====================')
      !
      !    Relate:PENDMAIL.Close()
      !    Relate:DEFEMAIL.Close()
      ! End (DBH 24/09/2007) #8943
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ExportMessageText        Procedure(String f:Type, String f:Model)

local:SMSActive             Byte()
local:EmailActive           Byte()

local:SMSMessageText        Like(sdl:SMSMessageText)
local:EmailMessageSubject   Like(sdl:EmailMessageSubject)
local:EmailMessageText      Like(sdl2:EmailMessageText)

Code
    Access:SIDALERT.Clearkey(sdl:BookingTypeStatusKey)
    sdl:BookingType = pem:BookingType
    sdl:Status = pem:Status
    Set(sdl:BookingTypeStatusKey,sdl:BookingTypeStatusKey)
    Loop ! Begin Loop
        If Access:SIDALERT.Next()
            Break
        End ! If Access:SIDALERT.Next()
        If sdl:BookingType <> pem:BookingType
            Break
        End ! If sdl:BookingType <> pem:BookingType
        If sdl:Status <> pem:Status
            Break
        End ! If sdl:Status <> pem:Status
        Case pem:DateToSend % 7
        Of 0 ! Sunday
            If ~sdl:Sunday
                Cycle
            End ! If ~sdl:Sunday
        Of 1 ! Monday
            If ~sdl:Monday
                Cycle
            End ! If ~sdl:Monday
        Of 2 ! Tuesday
            If ~sdl:Tuesday
                Cycle
            End ! If ~sdl:Tuesday
        Of 3 ! Wednesday
            If ~sdl:Wednesday
                Cycle
            End ! If ~sdl:Wednesday
        Of 4 ! Thursday
            If ~sdl:Thursday
                Cycle
            End ! If ~sdl:Thursday
        Of 5 ! Friday
            If ~sdl:Friday
                Cycle
            End ! If ~sdl:Friday
        Of 6 ! Saturday
            If ~sdl:Saturday
                Cycle
            End ! If ~sdl:Saturday
        End ! Case Today() % 7
        If pem:CustomerUTL = 'CUSTOMER' And sdl:SendTo = 2
            Cycle
        End ! If pem:CustomerUTL = 'CUSTOMER' And sdl:SendTo = 2
        If pem:CustomerUTL = 'UTL' And sdl:SendTo = 1
            Cycle
        End ! If pem:CustomerUTL = 'UTL' And sdl:SendTo = 1

        ! TrackerBase 11310 - GK 11/03/2010
        local:SMSActive = sdl:SMSAlertActive
        local:EmailActive = sdl:EmailAlertActive

        local:SMSMessageText = sdl:SMSMessageText
        local:EmailMessageSubject = sdl:EmailMessageSubject
        local:EmailMessageText = ''

        Access:SIDALER2.ClearKey(sdl2:SIDALERTRecordNumberKey)
        sdl2:SIDALERTRecordNumber = sdl:RecordNumber
        If Access:SIDALER2.TryFetch(sdl2:SIDALERTRecordNumberKey) = Level:Benign
            local:EmailMessageText = sdl2:EmailMessageText
        End

        Access:SMOALERT.ClearKey(sml:SIDALERTRecordNumberKey)
        sml:SIDALERTRecordNumber = sdl:RecordNumber
        sml:ModelNumber = f:Model
        If Access:SMOALERT.TryFetch(sml:SIDALERTRecordNumberKey) = Level:Benign
            !Found
            local:SMSActive = sml:SMSAlertActive
            local:EmailActive = sml:EmailAlertActive

            local:SMSMessageText = sml:SMSMessageText
            local:EmailMessageSubject = sml:EmailMessageSubject

            Access:SMOALER2.ClearKey(sml2:SMOALERTRecordNumberKey)
            sml2:SMOALERTRecordNumber = sml:RecordNumber
            If Access:SMOALER2.TryFetch(sml2:SMOALERTRecordNumberKey) = Level:Benign
                local:EmailMessageText = sml2:EmailMessageText
            End
        End

        Case f:Type
        Of 'EMAIL'
            If local:EmailActive
                If clip(local:EmailMessageText) <> '' and clip(pem:EmailAddress) <> ''
                    !Found
                    If SendEmail(pem:EmailAddress, sdd:EmailFromAddress, local:EmailMessageSubject, '', GetCompleteText(local:EmailMessageText, f:Type),1,tmp:ErrorMessage) = 1
!                        ErrorLog('Found Job (Email): ' & Clip(pem:RefNumber) & '. Type: (' & CLip(sdl:Status) & ') ' & Clip(sdl:Description) & '. To: ' & Clip(pem:EmailAddress))
                        pem:ErrorMessage = '' ! Clear any previous error messages
                        Return 0
                    Else
                        ! Email failed to send, write error message
                        pem:ErrorMessage = clip(tmp:ErrorMessage)
                        ! Confirmed with Nick 10/09/2009
                        ! Webalert constantly tries to send emails until they are successfully sent. This was causing a build up of records
                        ! in pendmail with invalid email addresses which were failing constantly. If the email has not been sent a day later
                        ! after it was intended then mark the email as sent to remove it from the process. The job would have likely moved out
                        ! of its status by that time anyhow.
                        If Today() > (pem:DateToSend + 1)
                            If Clock() > pem:TimeToSend
                                pem:DateSent = Today()
                                pem:TimeSent = Clock()
                            End
                        End
                        Access:PENDMAIL.Update()
                    End ! If SendEmail(pem:EmailAddress, sdd:EmailFromAddress, sdl:EmailSubject, '', sdl2:EmailMessageText,1) = 1
                Else
                    pem:ErrorMessage = 'INVALID EMAIL ADDRESS/TEXT'
                    pem:DateSent = ErrorDate
                    pem:TimeSent = ErrorTime
                    Access:PENDMAIL.Update()
                End
            End ! If sdL:EmailAlertActive
        Of 'SMS'
            If local:SMSActive
                local:SMSMessageText = BHStripReplace(local:SMSMessageText,'<13,10>',' ')

!                ErrorLog('Found Job (SMS): ' & Clip(pem:RefNumber) & '. Type: (' & CLip(sdl:Status) & ') ' & Clip(sdl:Description) & '. To: ' & Clip(pem:MobileNumber))

!                SQLFile{PROP:SQL} = 'UPDATE WEBJOB SET SMSSent = 1 WHERE Ref_Number = ' & JobsQ.jobNo

                PostString = Clip(PostString) & '<SMSMESSAGE>'
                PostString = Clip(PostString) & '<DESTINATION_ADDR>' & Clip(pem:MobileNumber) & '</DESTINATION_ADDR>'
! Changing (DBH 30/04/2008) # N/A - Allow for multipart messages
!                PostString = Clip(PostString) & '<TEXT>' & Clip(GetCompleteText(sdl:SMSMessageText)) & '</TEXT>'
!                PostString = Clip(PostString) & '<TRANSACTIONID>' & pem:RecordNumber & '</TRANSACTIONID>'
! to (DBH 30/04/2008) # N/A
                times# = 1000
                len# = Len(Clip(GetCompleteText(local:SMSMessageText, f:Type)))
                ! Text lines should be exported in 156 chunks (DBH: 30/04/2008)
                text# = 1
                Loop Until text# >= len#
                    PostString = Clip(PostString) & '<TEXT>' & Sub(Clip(GetCompleteText(local:SMSMessageText, f:Type)),text#,155) & '</TEXT>'
                    text# += 155
                    times# += 1
                End ! Loop x# = 1 To 1000 Step 156
                ! Trans ID to show the amount of messages sent (DBH: 30/04/2008)
                PostString = Clip(PostString) & '<TRANSACTIONID>1001:' & times# & '-' & pem:RecordNumber & '</TRANSACTIONID>'
! End (DBH 30/04/2008) #N/A
                PostString = Clip(PostString) & '<TYPEID>2</TYPEID>'
                PostString = Clip(PostString) & '<SERVICEID>3</SERVICEID>'
                PostString = Clip(PostString) & '<COSTID>1</COSTID>'
                PostString = Clip(PostString) & '<SOURCE_ADDR>VODAFONE</SOURCE_ADDR>'
                PostString = Clip(PostString) & '</SMSMESSAGE>'

                ! Inserting (DBH 23/04/2008) # N/A - Add entry to queue so can mark as sent later
                smsque:PENDMAILRecordNumber = pem:RecordNumber
                ! Check this entry in pendmail hasn't already been added to the queue (DBH: 23/04/2008)
                Get(SMSQueue,smsque:PENDMAILRecordNumber)
                If Error()
                    smsque:PENDMAILRecordNumber = pem:RecordNumber
                    Add(SMSQueue)
                    ! Add the record of the pendmail entry, and the record of the message to send (DBH: 23/04/2008)
                End ! If Error()
                ! End (DBH 23/04/2008) #N/A

                Return 0
            End
        End ! Case f:Type
        Break
    End ! Loop
    Return 1
GetCompleteText            Procedure(String f:Text, String f:Type)
local:Length            Long()
local:Start             Long()
local:End               Long()
local:TotalLength       Long()
local:EvaluatedString    CString(256)
Code
    local:Start = 0
    local:End = 0
    local:Length = 0
    local:TotalLength = Len(Clip(f:text))

    Access:JOBS.Clearkey(job:Ref_Number_Key)
    Job:Ref_Number = pem:RefNumber
    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

    End ! If Access:JOB.TryFetch(job:Ref_Number_Key) = Level:Benign

    Access:MODELNUM.ClearKey(mod:Model_Number_Key)
    mod:Model_Number = job:Model_Number
    Access:MODELNUM.TryFetch(mod:Model_Number_Key)

    Open(SQLFile4)
    Open(CCExchangeFile)

    SQLFile4{prop:SQL} = 'SELECT PrimaryFault, ContractPeriod, CustCollectionDate, DeviceCondition FROM WEBJOB Where SBJobNo = ' & job:Order_Number
    Next(SQLFile4)

    CCExchangeFile{prop:SQL} = 'SELECT ExchangeDate, ActualExchangeDate FROM CCExchange Where WebJobNo = ' & job:Order_Number
    Next(CCExchangeFile)

    Loop x# = 1 To local:TotalLength
        If Sub(f:Text,x#,2) = '[*'

            local:Start = x#
            local:End = 0
        End ! If Sub(f:Text,x#,2) = '[*'
        If Sub(f:Text,x#,2) = '*]'
            local:End = x#
        End ! If Sub(f:Text,x#,2) = '*]'

        If local:Start = 0
            local:Length += 1
        Else ! If local:Start = 0
            If local:End > 0
                Access:MERGE.ClearKey(mer:FieldNameKey)
                mer:FieldName = Sub(f:Text,local:Start + 2,local:End - local:Start - 2)
                If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Found

                    local:EvaluatedString = Evaluate(mer:FileName)

                    ! Don't ask me. Evaluate doesn't work for the sql fields (DBH: 27/09/2007)
                    Case Upper(mer:FileName)
                    Of 'WEBJOB.PRIMARYFAULT'
                        local:EvaluatedString = CLip(Sub(sql4:PrimaryFault,2,255))
                    Of 'WEBJOB.CONTRACTPERIOD'
                        local:EvaluatedString = Clip(sql4:ContractPeriod)
                    Of 'WEBJOB.CUSTCOLLECTIONDATE'
                        local:EvaluatedString = Clip(sql4:CustCollectionDate)
                    Of 'WEBJOB.DEVICECONDITION'
                        local:EvaluatedString = Clip(sql4:DeviceCondition)
                    Of 'CCEXCHANGE.EXCHANGEDATE'
                        local:EvaluatedString = Clip(ccex:ExchangeDate)
                    Of 'CCEXCHANGE.ACTUALEXCHANGEDATE'
                        local:EvaluatedString = Clip(ccex:ActualExchangeDate)
                    Of 'MOD:MODELSPECIFICURL1'
                    OrOf 'MOD:MODELSPECIFICURL2'
                    OrOf 'MOD:MODELSPECIFICURL3'
                        If f:Type = 'EMAIL' ! Only valid for emails
                            ! html anchor
                            if len(Clip(local:EvaluatedString)) > 0
                                local:EvaluatedString = '<a href="' & Clip(local:EvaluatedString) & '">Click Here</a>'
                            end
                        Else
                            ! SMS
                            local:EvaluatedString = ''
                        End
                    End ! Case mer:FileName

                    Case mer:Type
                    Of 'DEC' ! Decimal
                        local:EvaluatedString = Format(local:EvaluatedString,@n-14.2)
                    Of 'DAT' ! Date
                        local:EvaluatedString = Format(local:EvaluatedString,@d06b)
                    Of 'TIM' ! Time
                        local:EvaluatedString = Format(local:EvaluatedString,@t01b)
                    End ! Case mer:Type

                    Case mer:Capitals
                    Of 1 ! Upper
                        local:EvaluatedString = Upper(local:EvaluatedString)
                    Of 2 ! Caps
                        local:EvaluatedString = Capitalize(local:EvaluatedString)
                    Of 3 ! Lower
                        local:EvaluatedString = Lower(local:EvaluatedString)
                    End ! Case mer:Upper
                    f:Text = Sub(f:Text,1,local:Start - 1) & Clip(local:EvaluatedString) & Sub(f:Text,local:End + 2,local:TotalLength)
                    x# = local:Start

                    local:Start = 0
                    local:End = 0
                Else ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
                    !Error
                End ! If Access:MERGE.TryFetch(mer:FieldNameKey) = Level:Benign
            End ! If local:End > 0
        End ! If local:Start = 0
    End ! Loop x# = 1 To 500
    Close(SQLFile4)
    Close(CCExchangeFile)
    Return f:Text
!RepairEnvelopeReminders procedure()
!    !
!    ! Description : Add any jobs that are overdue from status 140 REPAIR ENVELOPE SENT
!    !
!smsEmail string(10)
!msgType  string(3)
!    code
!
!    Relate:JOBS.Open()
!    Relate:PENDMAIL.Open()
!
!    Relate:DEFEMAI2.Open()
!    set(DEFEMAI2)
!    Access:DEFEMAI2.Next()
!
!    open(SQLFile2)
!
!    if dem2:B6Active = false and dem2:B7Active = false and dem2:B6SMSActive = false and dem2:B7SMSActive = false
!        ! Inactive
!    else
!        Access:JOBS.ClearKey(job:By_Status)
!        job:Current_Status = '140 REPAIR ENVELOPE SENT'
!        set(job:By_Status, job:By_Status)
!        loop
!            if Access:JOBS.Next() <> Level:Benign then break.
!            if job:Current_Status <> '140 REPAIR ENVELOPE SENT' then break.
!
!            msgType = ''
!
!            ! Check the number of days this job has been at this status
!            Access:AUDSTATS.ClearKey(aus:DateChangedKey)
!            aus:RefNumber   = job:Ref_Number
!            aus:Type        = 'JOB'
!            aus:DateChanged = today()
!            set(aus:DateChangedKey, aus:DateChangedKey)
!            if Access:AUDSTATS.Previous() = Level:Benign
!                if aus:RefNumber = job:Ref_Number and aus:Type = 'JOB'
!                    if today() > (aus:DateChanged + dem2:B6NotifyDays)
!                        msgType = 'B6'
!                    end
!                    if today() > (aus:DateChanged + dem2:B6NotifyDays + dem2:B7NotifyDays)
!                        msgType = 'B7'
!                    end
!                end
!            end
!
!            if msgType = '' then cycle.
!
!            SQLFile2{PROP:SQL} = 'SELECT BookingType, SMSReq, EmailReq FROM WebJob WHERE SBJobNo = ' & job:Ref_Number
!            next(SQLFile2)
!            if error() then cycle.
!
!            ! Back To Base Only - Though status 140 REPAIR ENVELOPE SENT should only be used by back to base jobs..
!            if sql2:BookingType <> 'B' then cycle.
!            if sql2:BookingType <> 'G' then cycle. ! EBU Postal
!
!            ! Customer does not want any notifications
!            if sql2:SMSReq = false and sql2:EmailReq = false then cycle.
!
!            Access:JOBSE.ClearKey(jobe:RefNumberKey)
!            jobe:RefNumber = job:Ref_Number
!            if Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!                ! Found record
!
!                ! B6
!                if msgType = 'B6'
!                    if dem2:B6SMSActive = true and sql2:SMSReq = true
!                        smsEmail = 'SMS'
!                        do AddPendMail
!                    end
!
!                    if dem2:B6Active = true and sql2:EmailReq = true
!                        smsEmail = 'EMAIL'
!                        do AddPendMail
!                    end
!                end
!
!                ! B7
!                if msgType = 'B7'
!                    if dem2:B7SMSActive = true and sql2:SMSReq = true
!                        smsEmail = 'SMS'
!                        do AddPendMail
!                    end
!
!                    if dem2:B7Active = true and sql2:EmailReq = true
!                        smsEmail = 'EMAIL'
!                        do AddPendMail
!                    end
!                end
!
!            end
!        end
!
!    end
!
!    close(SQLFile2)
!
!    Relate:DEFEMAI2.Close()
!    Relate:PENDMAIL.Close()
!    Relate:JOBS.Close()
!
!
!AddPendMail routine
!    !
!    ! Description: Add record
!    !
!    Exit
!
!    ! Check we have not already sent this message
!    Access:PENDMAIL.ClearKey(pem:MessageRefKey)
!    pem:MessageType = msgType
!    pem:RefNumber = job:Ref_Number
!    set(pem:MessageRefKey, pem:MessageRefKey)
!    loop
!        if Access:PENDMAIL.Next() <> Level:Benign then break.
!        if clip(pem:MessageType) <> clip(msgType) then break.
!        if pem:RefNumber <> job:Ref_Number then break.
!        if pem:SMSEmail = smsEmail
!            exit
!        end
!    end ! loop
!
!    if Access:PENDMAIL.PrimeRecord() = Level:Benign
!
!        pem:SMSEmail = smsEmail
!        pem:RefNumber = job:Ref_Number
!        pem:MessageType = msgType
!
!        pem:DateToSend = today()
!        pem:TimeToSend = clock()
!
!        if pem:SMSEmail = 'SMS'
!            pem:MobileNumber = jobe:TraFaultCode5
!        end ! if pem:SMSEmail = 'SMS'
!        if pem:SMSEmail = 'EMAIL'
!            pem:EmailAddress = jobe:EndUserEmailAddress
!        end ! If pem:SMSEmail = 'EMAIL'
!
!        if Access:PENDMAIL.TryInsert() = Level:Benign
!            ! Insert Successful
!        else ! if Access:PENDMAIL.TryInsert() = Level:Benign
!            ! Insert Failed
!            Access:PENDMAIL.CancelAutoInc()
!        end ! if Access:PENDMAIL.TryInsert() = Level:Benign
!    end
