

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN004.INC'),ONCE        !Local module procedure declarations
                     END


TransitToSCDate      PROCEDURE  (strAccountFrom, scAccountID, strDate, strPath) ! Declare Procedure
startDate   date
result      long
transitDaysToSC long
    map
FindNextFreeDate procedure(*cstring strAccount, date currentDate), date
FindNextFreeDateAtSC procedure(long scID, date currentDate), date
    end
  CODE
    !
    ! Description : Get the number of transit days from the store to the service centre
    !

    if strDate <> '' and exists(clip(strPath) & '\ACCREG.DAT')
        ! Change the paths this way
        ACCREG{Prop:Name} = clip(strPath) & '\ACCREG.DAT'
        SIDACREG{Prop:Name} = clip(strPath) & '\SIDACREG.DAT'
        SIDREG{Prop:Name} = clip(strPath) & '\SIDREG.DAT'
        SIDDEF{Prop:Name} = clip(strPath) & '\SIDDEF.DAT'
        
        Access:AccReg.Open()
        Access:AccReg.UseFile()

        Access:SIDACREG.Open()
        Access:SIDACREG.UseFile()

        Access:SIDREG.Open()
        Access:SIDREG.UseFile()

        Access:SIDDEF.Open()
        Access:SIDDEF.UseFile()


        startDate = deformat(strDate, @d06b)

        !strTest = clip(strTest) & 'Transit To SC - Estimated Date: ' & format(startDate, @d06b) & '<13,10>'

        ! Days to service centre from store
        Access:AccReg.ClearKey(acg:AccountNoKey)
        acg:AccountNo = strAccountFrom
        if Access:AccReg.Fetch(acg:AccountNoKey) = Level:Benign

            !strTest = clip(strTest) & 'Transit To SC - Found Account ' & acg:AccountNo & '<13,10>'

            Access:SIDACREG.ClearKey(sar:SCAccRegIDKey)
            sar:SCAccountID = scAccountID
            sar:AccRegID = acg:AccRegID
            if Access:SIDACREG.Fetch(sar:SCAccRegIDKey) = Level:Benign

                !strTest = clip(strTest) & 'Transit To SC - Found Account Region For SC Account ID: ' & scAccountID & '<13,10>'

                if sar:TransitDaysException = true
                    transitDaysToSC = sar:TransitDaysToSC
                    !strTest = clip(strTest) & 'Transit To SC - Transit Days To SC (Override): ' & transitDaysToSC & '<13,10>'
                else
                    ! Lookup region by Service Centre
                    Access:SIDREG.ClearKey(srg:SCRegionNameKey)
                    srg:SCAccountID = scAccountID
                    srg:RegionName = acg:RegionName
                    if Access:SIDREG.Fetch(srg:SCRegionNameKey) = Level:Benign
                        transitDaysToSC = srg:TransitDaysToSC
                        !strTest = clip(strTest) & 'Transit To SC - Transit Days To SC (Found): ' & transitDaysToSC & '<13,10>'
                    end
                end

            else
                ! Does not exist
                ! Lookup region by Service Centre
                Access:SIDREG.ClearKey(srg:SCRegionNameKey)
                srg:SCAccountID = scAccountID
                srg:RegionName = acg:RegionName
                if Access:SIDREG.Fetch(srg:SCRegionNameKey) = Level:Benign
                    transitDaysToSC = srg:TransitDaysToSC
                    !strTest = clip(strTest) & 'Transit To SC - Transit Days To SC (Found): ' & transitDaysToSC & '<13,10>'
                end
            end

        end

        !strTest = clip(strTest) & 'Transit To SC - Transit Days To SC: ' & transitDaysToSC & '<13,10>'

        if transitDaysToSC = 0
            if scAccountID = 0 ! Record might not exist, but we have a valid Service Centre
                ! Fetch unallocated model default
                set(SIDDEF, 0)
                if Access:SIDDEF.Next() = Level:Benign
                    transitDaysToSC = SID:UnallocTransitToSC
                    !strTest = clip(strTest) & 'Transit To SC - Transit Days To SC (Default): ' & transitDaysToSC & '<13,10>'
                end
            end
            ! Cannot be zero
            if transitDaysToSC = 0 then transitDaysToSC = 1. ! Default
        end

        !strTest = clip(strTest) & 'Transit To SC - Transit Days To SC: ' & transitDaysToSC & '<13,10>'

        ! Calculate number of days in transit to service centre
        loop transitDaysToSC times
            startDate += 1
            ! Check date is working day
            startDate = FindNextFreeDate(strAccountFrom, startDate)
            !strTest2 = clip(strTest2) & 'Day: ' & format(startDate, @d06b) & '<13,10>'
        end ! loop

        !strTest = clip(strTest) & 'Transit To SC - Estimated Date With Transit Days: ' & format(startDate, @d06b) & '<13,10>'

        ! Current working day should be date at repair centre
        startDate = FindNextFreeDateAtSC(scAccountID, startDate)

        !strTest = clip(strTest) & 'Transit To SC - Final Estimated Date (Date Check Against SC BH): ' & format(startDate, @d06b) & '<13,10>'

        strDate = format(startDate, @d06b)

        !LinePrint(strTest, clip(strPath) & '\CollectionDate2.log')
        !LinePrint(strTest2, clip(strPath) & '\CollectionDateList.log')

        Access:AccReg.Close()
        Access:SIDACREG.Close()
        Access:SIDREG.Close()
        Access:SIDDEF.Close()

        result = true
    end

    return result
FindNextFreeDate procedure(*cstring strAccount, date currentDate)
    !
    ! Description : Find the next free date (Just a wrapper for FindFreeDate)
    !

strNext          cstring('+')
strDateToProcess cstring(30)
    code

    strDateToProcess = format(currentDate, @d06b)

    FindFreeDate(strNext, strAccount, strDateToProcess, strPath)

    return deformat(strDateToProcess, @d06b)
FindNextFreeDateAtSC procedure(long scID, date currentDate)
    !
    ! Description : Find the next free date (Just a wrapper for FindFreeDateAtSC)
    !

strNext          cstring('+')
strDateToProcess cstring(30)
    code

    strDateToProcess = format(currentDate, @d06b)

    FindFreeDateAtSC(strNext, scID, strDateToProcess, strPath)

    return deformat(strDateToProcess, @d06b)
