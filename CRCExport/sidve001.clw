

   MEMBER('sidversion.clw')                           ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SIDVE001.INC'),ONCE        !Local module procedure declarations
                     END


BrowseRetailStores PROCEDURE                          !Generated from procedure template - Window

retailStoreFilter    BYTE(1)
option1              BYTE
option2              BYTE
strEmpty             STRING(20)
BRW1::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                       PROJECT(sub:VodafoneRetailStore)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
option1                LIKE(option1)                  !List box control field - type derived from local data
option1_Icon           LONG                           !Entry's icon ID
option2                LIKE(option2)                  !List box control field - type derived from local data
option2_Icon           LONG                           !Entry's icon ID
strEmpty               LIKE(strEmpty)                 !List box control field - type derived from local data
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
sub:VodafoneRetailStore LIKE(sub:VodafoneRetailStore) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BrowseWindow         WINDOW('Browse Retail Stores'),AT(,,440,188),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Sub_Accounts'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,36,340,144),USE(?Browse:1),IMM,VSCROLL,COLOR(,,COLOR:Silver),MSG('Browsing Records'),FORMAT('70L(2)|~Store Code~@s15@232L(2)|~Company Name~@s30@[10RJ~1~L(1)@n1@10RJ~2~L(1)@n' &|
   '1@80L@s20@](156)~Option~L'),FROM(Queue:Browse:1)
                       BUTTON('Insert'),AT(360,88,76,20),USE(?Insert),DISABLE,HIDE,LEFT,ICON('insert.ico')
                       BUTTON('Change'),AT(360,112,76,20),USE(?Change),DISABLE,HIDE,LEFT,ICON('edit.ico')
                       BUTTON('&Delete'),AT(360,136,76,20),USE(?Delete),DISABLE,HIDE,LEFT,ICON('delete.ico')
                       BUTTON('&Select'),AT(360,20,76,20),USE(?Select:2),DISABLE,HIDE,LEFT,ICON('select.ico')
                       BUTTON('Change Version'),AT(360,112,76,20),USE(?BtnVersion),LEFT,ICON('edit.ico')
                       SHEET,AT(4,4,348,180),USE(?CurrentTab),SPREAD
                         TAB('By Store Code'),USE(?Tab:4)
                           ENTRY(@s15),AT(8,20,64,10),USE(sub:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('Close'),AT(360,164,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

! Local Variables
iniFilePath     string(260)
strConnection   string(260), static

versionQueue    queue(), pre(vq)
accountNo           string(15)
version             byte
                end
! SQL File
SQLFile             file, driver('Scalable'), oem, owner(strConnection), name('VERSION'), pre(SQL)
Record                  record
AccountNo                   string(15), name('AccountNo')
Version                     byte, name('Version')
                        end
                    end

VerFile             file, driver('Scalable'), oem, owner(strConnection), name('VERSION'), pre(SQLVER)
Record                  record
Version                     byte, name('Version')
                        end
                    end
! Procedure Prototypes

    map
ChangeVersion   procedure(string accountNo), bool, proc
GenerateQueue   procedure, bool, proc
    end
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseRetailStores')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SUBTRACC.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:SUBTRACC,SELF)
  OPEN(BrowseWindow)
  SELF.Opened=True
  iniFilePath     = clip(path()) & '\Webimp.ini'
  strConnection   = getini('Defaults', 'Connection', '', clip(iniFilePath))
  
  GenerateQueue()
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sub:RetailStoreKey)
  BRW1.AddRange(sub:VodafoneRetailStore,retailStoreFilter)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sub:Account_Number,sub:Account_Number,1,BRW1)
  BIND('option1',option1)
  BIND('option2',option2)
  BIND('strEmpty',strEmpty)
  ?Browse:1{PROP:IconList,1} = '~tick.ico'
  BRW1.AddField(sub:Account_Number,BRW1.Q.sub:Account_Number)
  BRW1.AddField(sub:Company_Name,BRW1.Q.sub:Company_Name)
  BRW1.AddField(option1,BRW1.Q.option1)
  BRW1.AddField(option2,BRW1.Q.option2)
  BRW1.AddField(strEmpty,BRW1.Q.strEmpty)
  BRW1.AddField(sub:RecordNumber,BRW1.Q.sub:RecordNumber)
  BRW1.AddField(sub:VodafoneRetailStore,BRW1.Q.sub:VodafoneRetailStore)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SUBTRACC.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?BtnVersion
      ! Change Version Button
      BRW1.UpdateBuffer()
      
      ! Change the version of SID that this account points to
      if ChangeVersion(sub:Account_Number)
      
          ! Update the queue
          GenerateQueue()
      
          BRW1.ResetSort(1)
      
      end
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Close
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sub:Account_Number
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ChangeVersion   procedure(string accountNo)

insert          bool(true)
currentVersion  byte(0) ! Default SID V3
    code

    if accountNo = '' then return false.

    ! Check if we need to insert / update the record
    open(VerFile)
    if error() then return false.

    VerFile{Prop:SQL} = 'SELECT Version FROM Version WHERE AccountNo =''' & clip(upper(accountNo)) & ''''

    next(VerFile)
    if not error()
        insert = false
        currentVersion = SQLVER:Version
    end

    ! Switch version
    if currentVersion = 0
        currentVersion = 1  
    else
        currentVersion = 0
    end

    if insert
        VerFile{Prop:SQL} = 'INSERT INTO Version (AccountNo, Version) VALUES(''' & clip(upper(accountNo)) & ''',' & currentVersion & ')'
    else
        VerFile{Prop:SQL} = 'UPDATE Version SET Version = ' & currentVersion & ' WHERE AccountNo = ''' & clip(upper(accountNo)) & ''''
    end

    close(VerFile)

    return true
GenerateQueue   procedure

    code

    free(versionQueue)
    clear(versionQueue)

    open(SQLFile)
    if error() then return false.

    SQLFile{Prop:SQL} = 'SELECT AccountNo, Version FROM Version ORDER BY AccountNo'

    loop
        next(SQLFile)
        if error() then break.

        ! Add all records to queue
        vq:accountNo    = SQL:AccountNo
        vq:version      = SQL:Version
        add(versionQueue, vq:accountNo)

    end ! loop

    close(SQLFile)

    return true

BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (option1 = 1)
    SELF.Q.option1_Icon = 1
  ELSE
    SELF.Q.option1_Icon = 0
  END
  IF (option2 = 1)
    SELF.Q.option2_Icon = 1
  ELSE
    SELF.Q.option2_Icon = 0
  END


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Lookup the account version number
  option1 = 0
  option2 = 0
  
  vq:accountNo    = sub:Account_Number
  get(versionQueue, vq:accountNo)
  if error()
      ! Default to option 1
      option1 = 1
  else
      case vq:version
          of 0 ! Option 1 (Existing Insurance Process)
              option1 = 1
          else ! Option 2 (New Insurance Process)
              option2 = 1
      end ! case
  end
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue

