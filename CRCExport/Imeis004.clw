

   MEMBER('imeishipimp.clw')                          ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('IMEIS004.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

Local                CLASS
ImportJobs           Procedure(String func:FileName,String func:ShortName)
                     END
tmp:ImportFile       CSTRING(255),STATIC
ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
tmp:LocaterToSend    CSTRING(255)
tmp:LocaterSent      CSTRING(255)
window               WINDOW('Test Import'),AT(,,69,15),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       PROMPT('Import In Progress...'),AT(4,0),USE(?Prompt1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ImportFile      File,Driver('BASIC'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record                  Record
IMEINumber              String(15)
ShipDate                String(10)
BERFlag                 String(1)
                        End
                    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GLODC:ConversionResult = DC:FilesConverter()
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  Local.ImportJobs(Clip(Path()) & '\Test.csv', 'Test.csv')
  
  Post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

Local.ImportJobs        Procedure(String func:FileName, String func:ShortName)
local:UpdateStatus      Byte(0)
local:JobUpdated        Byte(0)
Code
    Relate:IMEISHIP.Open()

    CountNormal# = 0
    CountUpdate# = 0
    tmp:ImportFile = func:FileName
    Open(ImportFile)
    Set(ImportFile,0)
    Loop
        Next(ImportFile)
        If Error()
            Break
        End !If Error()

        Access:IMEISHIP.Clearkey(imei:IMEIKey)
        imei:IMEINumber = impfil:IMEINumber
        If Access:IMEISHIP.Tryfetch(imei:IMEIKey) = Level:Benign
            ! Found
            imei:IMEINumber = impfil:IMEINumber
            imei:ShipDate   = Deformat(impfil:ShipDate,@d06)
            IMEI:BER        = impfil:BERFlag
            IMEI:ManualEntry = 0
            IMEI:ProductCode = ''
            If Access:IMEISHIP.TryUpdate() = Level:Benign
                CountUpdate# += 1
!                Access:IMEISHP2.Clearkey(imei2:IMEIRecordNumberKey)
!                imei2:IMEIRecordNumber  = imei:RecordNumber
!                If Access:IMEISHP2.Tryfetch(imei2:IMEIRecordNumberKey) = Level:Benign
!                    ! Found
!                    imei2:Manufacturer = ''
!                    Access:IMEISHP2.TryUpdate()
!                Else ! If Access:IMEISHP2.Tryfetch(imei2:IMEIRecordNumber) = Level:Benign
!                    ! Error
!                    If Access:IMEISHP2.PrimeRecord() = Level:Benign
!                        imei2:IMEIRecordNumber = imei:RecordNumber
!                        imei2:Manufacturer = ''
!                        If Access:IMEISHP2.TryInsert() = Level:Benign
!                            ! Insert Successful
!                        Else ! If Access:IMEISHP2.TryInsert() = Level:Benign
!                            ! Insert Failed
!                            Access:IMEISHP2.CancelAutoInc()
!                        End ! If Access:IMEISHP2.TryInsert() = Level:Benign
!                    End !If Access:IMEISHP2.PrimeRecord() = Level:Benign
!                End ! If Access:IMEISHP2.Tryfetch(imei2:IMEIRecordNumber) = Level:Benign
            End ! If Access:IMEISHIP.TryUpdate() = Level:Benign
        Else ! If Access:IMEISHIP.Tryfetch(imei:IMEIKey) = Level:Benign
            ! Error
            If Access:IMEISHIP.PrimeRecord() = Level:Benign
                imei:IMEINumber = impfil:IMEINumber
                imei:ShipDate   = Deformat(impfil:ShipDate,@d06)
                IMEI:BER        = impfil:BERFlag
                IMEI:ManualEntry = 0
                IMEI:ProductCode = ''
                If Access:IMEISHIP.TryInsert() = Level:Benign
                    ! Insert Successful
                    CountNormal# += 1
!                    If Access:IMEISHP2.PrimeRecord() = Level:Benign
!                        imei2:IMEIRecordNumber = imei:RecordNumber
!                        imei2:Manufacturer = ''
!                        If Access:IMEISHP2.TryInsert() = Level:Benign
!                            ! Insert Successful
!                        Else ! If Access:IMEISHP2.TryInsert() = Level:Benign
!                            ! Insert Failed
!                            Access:IMEISHP2.CancelAutoInc()
!                        End ! If Access:IMEISHP2.TryInsert() = Level:Benign
!                    End !If Access:IMEISHP2.PrimeRecord() = Level:Benign
                Else ! If Access:IMEISHIP.TryInsert() = Level:Benign
                    ! Insert Failed
                    Access:IMEISHIP.CancelAutoInc()
                End ! If Access:IMEISHIP.TryInsert() = Level:Benign
            End !If Access:IMEISHIP.PrimeRecord() = Level:Benign
        End ! If Access:IMEISHIP.Tryfetch(imei:IMEIKey) = Level:Benign

    End !Loop(ImportFile)
    Close(ImportFile)
    Relate:IMEISHIP.Close()

    Message('IMEIs Imported: ' & CountNormal#)
    Message('IMEIs Updated: ' & CountUpdate#)
