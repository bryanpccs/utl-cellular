

   MEMBER('autostat.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('AUTOS001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('AUTOS002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('AUTOS003.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

FilesOpened          BYTE
ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
Window               WINDOW('Auto Status Change Utility'),AT(,,245,23),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,TIMER(100),SYSTEM,GRAY,DOUBLE
                       PROMPT('Running.....'),AT(3,4,241,12),USE(?Information)
                     END

    map
Over8HoursOld  procedure(date lastDate, long lastTime), byte
    end
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Dummy SQL tables
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD
SQLBookingType              String(1), Name('BookingType')
                        END
                    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

ProcessJobs          routine

    data

countJobs                       long(0)

lastDate                        date
lastTime                        long

    code

    Relate:JOBS.Open()
    Relate:AUDSTATS.Open()

    open(SQLFile)

    ! Check all jobs that have a status of 901 DESPATCHED
    Access:JOBS.ClearKey(job:By_Status)
    job:Current_Status = '901 DESPATCHED'
    set(job:By_Status, job:By_Status)
    loop
        if Access:JOBS.Next() <> Level:Benign then break.
        if job:Current_Status <> '901 DESPATCHED' then break.

        ! Filter out any jobs which are not exchanges
        SQLFile{prop:SQL} = 'SELECT BookingType FROM WebJob Where SBJobNo = ' & job:Ref_Number
        next(SQLFile)
        if error() then cycle.
        if sql:SQLBookingType <> 'E' then cycle. ! Exchange jobs only

        ! Get the date/time that the job was last changed to 901 DESPATCHED
        ! Note : There should only be one 901 entry in the audstats table for each job
        ! but occasionally jobs statuses get changed manually in ServiceBase. Therefore
        ! its possible to find multiple 901 DESPATCHED entries. Either way get the last entry.
        lastDate = 0
        lastTime = 0

        Access:AUDSTATS.ClearKey(aus:NewStatusKey)
        aus:RefNumber = job:Ref_Number
        aus:Type = 'JOB'
        aus:NewStatus = '901 DESPATCHED'
        set(aus:NewStatusKey, aus:NewStatusKey)
        loop
            if Access:AUDSTATS.Next() <> Level:Benign then break.
            if aus:RefNumber <> job:Ref_Number then break.
            if aus:Type <> 'JOB' then break.
            if aus:NewStatus <> '901 DESPATCHED' then break.

            if aus:DateChanged > lastDate
                lastDate = aus:DateChanged
                lastTime = aus:TimeChanged
            elsif aus:DateChanged = lastDate
                if aus:TimeChanged > lastTime
                    lastTime = aus:TimeChanged
                end
            end

        end

        if lastDate = 0 then cycle.

        ! Check if status change is over 8 hours old
        if Over8HoursOld(lastDate, lastTime)
            ! Change the status to 910 TRACKING DATA AVAILABLE
            GetStatusAlternative(910,0,'JOB')
            if Access:JOBS.TryUpdate() = Level:Benign
                countJobs += 1
            else
                ErrorLog('Error updating job number: ' & job:Ref_Number)
            end
        end

    end

    close(SQLFile)

    Relate:AUDSTATS.Close()
    Relate:JOBS.Close()

    ErrorLog('Updated Jobs Total : ' & countJobs)
!Tests   routine
!
!    data
!
!lastDate                        date
!lastTime                        long
!
!    code
!
!    lastDate = today()
!    lastTime = clock()
!
!    ErrorLog('Test with : ' & format(lastDate, @d06b) & ' ' & format(lastTime, @t04b))
!
!    if Over8HoursOld(lastDate, lastTime)
!        ErrorLog('Over 8 hours old')
!    else
!        ErrorLog('Under 8 hours old')
!    end
!
!    lastDate = today() - 2
!    lastTime = clock()
!
!    ErrorLog('Test with : ' & format(lastDate, @d06b) & ' ' & format(lastTime, @t04b))
!
!    if Over8HoursOld(lastDate, lastTime)
!        ErrorLog('Over 8 hours old')
!    else
!        ErrorLog('Under 8 hours old')
!    end
!
!    lastDate = today()
!    lastTime = deformat('17:50', @t1)
!
!    ErrorLog('Test with : ' & format(lastDate, @d06b) & ' ' & format(lastTime, @t04b))
!
!    if Over8HoursOld(lastDate, lastTime)
!        ErrorLog('Over 8 hours old')
!    else
!        ErrorLog('Under 8 hours old')
!    end
!
!    lastDate = today()
!    lastTime = deformat('08:00', @t1)
!
!    ErrorLog('Test with : ' & format(lastDate, @d06b) & ' ' & format(lastTime, @t04b))
!
!    if Over8HoursOld(lastDate, lastTime)
!        ErrorLog('Over 8 hours old')
!    else
!        ErrorLog('Under 8 hours old')
!    end
!
!    lastDate = today()
!    lastTime = deformat('01:00', @t1)
!
!    ErrorLog('Test with : ' & format(lastDate, @d06b) & ' ' & format(lastTime, @t04b))
!
!    if Over8HoursOld(lastDate, lastTime)
!        ErrorLog('Over 8 hours old')
!    else
!        ErrorLog('Under 8 hours old')
!    end
!
!    lastDate = today() - 1
!    lastTime = deformat('21:00', @t1)
!
!    ErrorLog('Test with : ' & format(lastDate, @d06b) & ' ' & format(lastTime, @t04b))
!
!    if Over8HoursOld(lastDate, lastTime)
!        ErrorLog('Over 8 hours old')
!    else
!        ErrorLog('Under 8 hours old')
!    end

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Information
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDSTATS.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  0{Prop:Hide} = true ! Hide the window
  
  IniFilepath =  CLIP(PATH()) & '\Webimp.ini'
  ConnectionStr = GETINI('Defaults', 'Connection', '', CLIP(IniFilepath))
  
  ErrorLog('')
  ErrorLog('Auto Status Change Started===========================')
  
  do ProcessJobs
  
  ErrorLog('Auto Status Change Finished==========================')
  ErrorLog('')
  
  POST(Event:CloseWindow)
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDSTATS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

Over8HoursOld      procedure(date lastDate, long lastTime)

totalDays                       long, auto
isOver8Hours                    byte(0)

    code

    ! Check if status change is over 8 hours old
    if today() > lastDate
        ! Get the total days since the status change
        totalDays = today() - lastDate
        if totalDays > 1
            ! More than one day, no need to check time as its over 8 hours
            isOver8Hours = true
        else
           ! Status change was yesterday, check if current time + time from yesterday > 8 hours
           if (clock() + (8640000 - lastTime)) > 2880000
               isOver8Hours = true
           end
        end
    elsif today() = lastDate
       ! If current date matches, check if current time > (status change time + 8 hours)
       if clock() > (lastTime + 2880000)
            isOver8Hours = true
       end
    end

    return isOver8Hours
