

   MEMBER('fixsb2wilf.clw')                           ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('FIXSB001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

FilesOpened          BYTE
save_aud_id          USHORT,AUTO
ConnectionStr        STRING(255),STATIC
DiagConnectionStr    STRING(255),STATIC
INIFilePath          STRING(255)
tmp:ExportFile       STRING(255),STATIC
tmp:ImportFile       STRING(255),STATIC
tmp:Filename         CSTRING(255)
Window               WINDOW('ServiceBase / Wilfred Import Export Facility'),AT(,,245,23),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,TIMER(100),SYSTEM,GRAY,DOUBLE
                       PROMPT('Running.....'),AT(3,4,241,12),USE(?Information)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ExportFile    File,Driver('ASCII'),Pre(expfil),Name(tmp:ExportFile),Create,Bindable,Thread
Record                  Record
Line                    String(2000)
                        End
                    End

TempFiles   Queue(file:Queue),pre(fil)
            End
! Dummy SQL tables
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD
SQLSBJobNo                  Long, Name('SBJobNo')
SQLBookingType              String(1), Name('BookingType')
SQLRepairType               String(2), Name('RepairType')
SQLPrimaryFault             String(255), Name('PrimaryFault')
SQLReason                   String(255), Name('Reason')
SQLCustCollectionDate       Date, Name('CustCollectionDate')
SQLRetStoreCode             String(15), Name('RetStoreCode')
SQLBookedBy                 String(30), Name('BookedBy')
! Inserting (DBH 23/02/2006) #6915 - New Fields to check for sms/email alerts
SQLSMSReq                   Byte(), Name('SMSReq')
SQLEmailReq                 Byte(), Name('EmailReq')
! End (DBH 23/02/2006) #6915
SQLCountryDelivery          String(20), Name('CountryDelivery')
! Inserting (DBH 12/06/2006) #7813 - Record free text in TraFaultCode3 field
SQLFreeTextFault            String(255), Name('FreeTextFault')
! End (DBH 12/06/2006) #7813
SQLDeviceCondition          String(40), Name('DeviceCondition')
SQLUnitCondition            String(255), Name('UnitCondition')
SQLPhoneLock                String(30), Name('Phone_Lock')
! Inserting (DBH 03/07/2008) # 10020 - Show the SMS alert number, rather than the mobile number
SQLSMSAlertNo               String(15), Name('SMSAlertNo')
! End (DBH 03/07/2008) #10020
                        END
                    END

! Contact Centre (Exchange Bookings) (#7957 11/08/2006 GK)
CCExchFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(ccex),Name('CCExchange'),Bindable,Thread
Record          Record
ExchangeDate            date, name('ExchangeDate')
TimeSlotID              long, name('TimeSlotID')
DeliveryCharge          pdecimal(8, 2), name('DeliveryCharge')
TransitDays             long, name('TransitDays')
WebJobNo                long, name('WebJobNo')
                End
           End

TimeSlotFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(tmsl),Name('TimeSlot'),Bindable,Thread
Record          Record
TimeSlotName            cstring(30), name('TimeSlotName')
                End
             End

WebJobExFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(webex),Name('WebJobEx'),Bindable,Thread
Record          Record
SCAccountID            long, name('SCAccountID')
ManifestNo             long, name('ManifestNo')
InterTimeFrame         cstring(31), name('InterTimeFrame')
ServiceCharge          pdecimal(8, 2), name('ServiceCharge')
DiagAvailable          byte, name('DiagAvailable')
Forename               cstring(31), name('Forename')
NoExceptionDate        date, name('NoExceptionDate')
KnownIssueID           long, name('KnownIssueID')
                End
             End

WebRetFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(webr),Name('WebRet'),Bindable,Thread
Record          Record
RetReasonCode          cstring(7), name('RetReasonCode')
RetReasonName          cstring(51), name('RetReasonName')
ReturnException        byte, name('ReturnException')
                End
             End

JobTxtFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(jobt),Name('JobTxt'),Bindable,Thread
Record          Record
Text1               cstring(255),Name('Text1')
Text2               cstring(255),Name('Text2')
                End
            End

KnownIssueFile File,Driver('Scalable'),OEM,Owner(DiagConnectionStr),Pre(knis),Name('KnownIssue'),Bindable,Thread
Record          Record
Summary             cstring(51),Name('Summary')
                End
            End
local:CurrentRefNumber          Long()
local:FoundJob                  Byte(0)
local:SIDBooking                Byte(1)
local:WilfredToSend             CString(255)
local:WilfredSent               CString(255)
local:TheJobNumber              Long()
local:CountJobs                 Long()
local:LastJobsCheckedDate       Date()
local:LastJobsCheckedTime       Time()
local:NewJob                    Byte(0)
local:IsThereAnAccessory        Byte(0)
local:JobAlreadyExported        Byte(0)
local:strStatusCode             String(3)
local:statusCode                Long()
local:skipJob                   Byte(0)
local:exported                  Byte(0)
local:exchangeJob               Byte(0)
local:returnJob                 Byte(0)
local:SCAccountID               Long()
local:emailMessage              String(8000)
local:OracleCode                String(30)
local:ContractType              String(10)
local:sendCopyToSMART           Byte(0)
local:SMARTCopy                 Byte(0)
local:ManifestNo                Long()
local:InterTimeFrame            CString(31)
local:ServiceCharge             PDecimal(8, 2)
local:DiagAvailable             Byte(0)
local:Forename                  CString(31)
local:SendStatusAlert           Byte(0)
local:MappedBookingType         String(1)
local:transitDays               Long()
local:NoExceptionDate           Date()
local:ExceptionDate             Date()
local:KnownIssueID              Long()
local:KnownIssueSummary         String(50)
local:AdditionalSymptoms        String(500)

!Keep track of which jobs have already been exported  (DBH: 23-11-2004)
JobsQueue                   Queue(),Pre(jobque)
JobNumber                     Long()
                                End

ExchangeQ                   queue(),pre(exchq)
JobNo                           long()
                            end

!End   - Keep track of which jobs have already been exported  (DBH: 23-11-2004)
local:SIDJobCount               Long()
local:NonSIDJobCount            Long()

ExportQueue                 Queue(),Pre(expque)
SCAccountID                   Long()
ExportFileName                CString(255)
                            End

! Keep track of audit records that need removing
AuditQueue                  Queue(),Pre(audq)
AuditRecordNumber               Long()
                            End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

ExportJobs          Routine
Data
i       long(1)
dateNow date()
timeNow time()
local:recordNumber long, auto
foundRecord byte(0)

Code

    Relate:JOBS.Open()
    Relate:DEFAULTS.Open()
    Relate:USERS.Open()
    Relate:EXCHANGE.Open()
    Relate:DESBATCH.Open()
    Relate:STATUS.Open()
    Relate:AUDIT_ALIAS.Open()
    Relate:PENDMAIL.Open()
    Relate:PENDMAIL_ALIAS.Open()
    Relate:DEFEMAI2.Open()
    Relate:SIDSRVCN.Open()
    Relate:SIDMODSC.Open()
    Relate:SIDDEF.Open()
    Relate:CARISMA.Open()
    Relate:SIDNWDAY.Open()
    !Relate:SIDEXUPD.Open()
    Relate:SIDALERT.Open()
    Relate:SIDREMIN.Open()
    Relate:SMOALERT.Open()
    Relate:SMOREMIN.Open()
    Relate:SIDALDEF.Open()
    Relate:MODELNUM.Open()
    Relate:IMEISHIP.Open()
    Relate:SIDAUD.Open()

    Set(SIDALDEF)
    Access:SIDALDEF.Next()

    Set(DEFEMAI2)
    Access:DEFEMAI2.Next()

    Set(SIDDEF, 0)
    Access:SIDDEF.Next()

    Open(SQLFile)
    Open(CCExchFile)
    Open(TimeSlotFile)
    Open(WebJobExFile)
    Open(WebRetFile)
    Open(JobTxtFile)
    Open(KnownIssueFile)


    dateNow = today()
    timeNow = clock()

    Clear(JobsQueue)
    Free(JobsQueue)

    !Go back over a day to make sure that no jobs are missed  (DBH: 23-11-2004)
    Access:AUDIT_ALIAS.ClearKey(audali:DateJobKey)
    ! Was changed to 15 Days, verbal confirmation from Ian Walton 25/08/2006
    ! 28/05/2009 GK - Spoke to Nick about this after sb2wilfred was taking a long time to complete,
    !                 have reduced the number of days to 5. The code seems to be back checking anyway
    !                 so really could be set to one day.

    ! 15/10/2010 GK - Several months later and we notice that lots of exchange jobs have not been sent
    !                 via the interface. There is a rule where exchanges are only added to the export file the
    !                 day before the exchange is required. If the customer selects a future exchange date of more
    !                 than 5 days then the job will never get exported as we don't look back far enough...
    
    audali:Date       = today() - 10
    set(audali:DateJobKey, audali:DateJobKey)
    Loop
        If Access:AUDIT_ALIAS.NEXT()
            Break
        End !If

        if audali:Date = today() then break.

        local:recordNumber = audali:record_number
        foundRecord = true

        local:SendStatusAlert = 0

        If Instring('JOB CREATED BY WEBMASTER',audali:Notes,1,1)
            !Is this a new job, or an updated one  (DBH: 23-11-2004)
            local:NewJob = True
            local:JobAlreadyExported = False
            !Start - Has this new job already been exported?  (DBH: 23-11-2004)
            Save_aud_ID = Access:AUDIT.SaveFile()
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = audali:Ref_Number
            aud:Action = 'JOB EXPORTED TO WILFRED'
            Set(aud:Action_Key,aud:Action_Key)
            Loop ! Begin AUDIT Loop
                If Access:AUDIT.Next()
                    Break
                End ! If !Access
                If aud:Ref_Number <> audali:Ref_Number
                    Break
                End ! If
                If aud:Action <> 'JOB EXPORTED TO WILFRED'
                    Break
                End ! If
                local:JobAlreadyExported = True
                Break
            End ! End AUDIT Loop
            Access:AUDIT.RestoreFile(Save_aud_ID)

            if local:JobAlreadyExported then cycle.

        Else ! If audali:Action = 'NEW JOB INITIAL BOOKING'
            !Is this a new job, or an updated one?  (DBH: 23-11-2004)
            local:NewJob = False

            If Instring('STATUS CHANGED TO',audali:Action,1,1) Or Instring('JOB UPDATED BY WILFRED',audali:Action,1,1)
                ! We now only send jobs to wilfred once they reach a status of despatched from store.
                ! It is possible the job is booked but not despatched within the 5 days we are looking over.
                ! Therefore first check that this job has been exported to wilfred

                local:JobAlreadyExported = False
                Save_aud_ID = Access:AUDIT.SaveFile()
                Access:AUDIT.Clearkey(aud:Action_Key)
                aud:Ref_Number = audali:Ref_Number
                aud:Action = 'JOB EXPORTED TO WILFRED'
                Set(aud:Action_Key,aud:Action_Key)
                Loop ! Begin AUDIT Loop
                    If Access:AUDIT.Next()
                        Break
                    End ! If !Access
                    If aud:Ref_Number <> audali:Ref_Number
                        Break
                    End ! If
                    If aud:Action <> 'JOB EXPORTED TO WILFRED'
                        Break
                    End ! If
                    local:JobAlreadyExported = True
                    Break
                End ! End AUDIT Loop
                Access:AUDIT.RestoreFile(Save_aud_ID)

                if local:JobAlreadyExported = False
                    local:NewJob = True
                End

                ! See if the status change needs to send an alert (DBH: 23/11/2007)
                local:SendStatusAlert = 1

            Else ! If Instring('STATUS CHANGED TO',audali:Action,1,1)
                !Only looking for Status Changes  (DBH: 23-11-2004)
                Cycle
            End ! If Instring('STATUS CHANGED TO',audali:Action,1,1)
            
        End ! If audali:Action = 'NEW JOB INITIAL BOOKING'

        local:TheJobNumber = audali:Ref_Number

        if local:NewJob = true
            Access:SIDAUD.ClearKey(sad:AuditRecordNumberKey)
            sad:AuditRecordNumber = audali:record_number
            if Access:SIDAUD.Fetch(sad:AuditRecordNumberKey) <> Level:Benign
                sad:AuditRecordNumber = audali:record_number
                sad:EntryDate = today()
                sad:EntryTime = clock()
                Access:SIDAUD.TryInsert()
                ErrorLog('Added missing job: ' & local:TheJobNumber)
            end
        end
    End !Loop
    Close(SQLFile)
    Close(CCExchFile)
    Close(TimeSlotFile)
    Close(WebJobExFile)
    Close(WebRetFile)
    Close(JobTxtFile)
    Close(KnownIssueFile)


    Relate:JOBS.Close()
    Relate:DEFAULTS.Close()
    Relate:USERS.Close()
    Relate:EXCHANGE.Close()
    Relate:DESBATCH.Close()
    Relate:STATUS.Close()
    Relate:AUDIT_ALIAS.Close()
    Relate:PENDMAIL.Close()
    Relate:PENDMAIL_ALIAS.Close()
    Relate:DEFEMAI2.Close()
    Relate:SIDSRVCN.Close()
    Relate:SIDMODSC.Close()
    Relate:SIDDEF.Close()
    Relate:CARISMA.Close()
    Relate:SIDNWDAY.Close()
    !Relate:SIDEXUPD.Close()
    Relate:SIDALERT.Close()
    Relate:SIDREMIN.Close()
    Relate:SMOALERT.Close()
    Relate:SMOREMIN.Close()
    Relate:SIDALDEF.Close()
    Relate:MODELNUM.Close()
    Relate:IMEISHIP.Close()
    Relate:SIDAUD.Close()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Information
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(Window)
  SELF.Opened=True
  0{Prop:Hide} = true ! Hide the window
  
  IniFilepath =  CLIP(PATH()) & '\Webimp.ini'
  ConnectionStr = GETINI('Defaults', 'Connection', '', CLIP(IniFilepath))
  DiagConnectionStr = GETINI('Defaults', 'DiagConnection', '', CLIP(IniFilepath))
  
  ErrorLog('')
  ErrorLog('SB2WILFRED Started===========================')
  
  Do ExportJobs
  
  ErrorLog('SB2WILFRED Finished==========================')
  ErrorLog('')
  
  POST(Event:CloseWindow)
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

