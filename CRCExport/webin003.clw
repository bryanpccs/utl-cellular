

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN003.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBIN007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBIN008.INC'),ONCE        !Req'd for module callout resolution
                     END


FindFreeDate         PROCEDURE  (strDirection, strAccount, strDate, strPath) ! Declare Procedure
result      long
strIniFile  like(reg:BankHolidaysFileName)
availableDate date
    map
FreeDate        procedure(date sourceDate), date
    end
  CODE
    !
    ! Description : Find the next free date (check weekends / bank holidays)
    !

    ! Make sure that the strDate is not empty (otherwise can cause a GPF)
    if strDate <> '' and exists(clip(strPath) & '\ACCREG.DAT')
        ! Change the paths this way
        ACCREG{Prop:Name} = clip(strPath) & '\ACCREG.DAT'
        REGIONS{Prop:Name} = clip(strPath) & '\REGIONS.DAT'

        Access:AccReg.Open()
        Access:AccReg.UseFile()

        Access:Regions.Open()
        Access:Regions.UseFile()

        availableDate = deformat(strDate, @d06b)

        Access:AccReg.ClearKey(acg:AccountNoKey)
        acg:AccountNo = strAccount
        if Access:AccReg.Fetch(acg:AccountNoKey) = Level:Benign

            Access:Regions.ClearKey(reg:RegionNameKey)
            reg:RegionName = acg:RegionName
            if Access:Regions.Fetch(reg:RegionNameKey) = Level:Benign
                strIniFile = reg:BankHolidaysFileName
            end

        end

        if strIniFile = ''
            strIniFile = 'BHENGWAL.DAT' ! Default
        end

        !strTest = clip(strTest) & 'BH Check - Bank Holiday File: ' & strIniFile & '<13,10>'

        availableDate = FreeDate(availableDate)

        strDate = format(availableDate, @d06b)

        !LinePrint(strTest, clip(strPath) & '\CollectionDate3.log')

        Access:Regions.Close()
        Access:AccReg.Close()

        result =  true
    end

    return result

FreeDate procedure(date sourceDate)

x   long(1)
bh  date, dim(15)

    code

    LoadBankHolidays(bh, strIniFile, strPath)

    loop x = 1 to 15
        if (GetDayOfWeek(sourceDate) = 'SAT')
            !strTest = clip(strTest) & 'BH Check - Found Saturday: ' & format(sourceDate, @d06b) & '<13,10>'
            if strDirection = '-'
                sourceDate = sourceDate - 1
            else
                sourceDate = sourceDate + 2
            end
            sourceDate = FreeDate(sourceDate)
        end
        if (GetDayOfWeek(sourceDate) = 'SUN')
            !strTest = clip(strTest) & 'BH Check - Found Sunday: ' & format(sourceDate, @d06b) & '<13,10>'
            if strDirection = '-'
                sourceDate = sourceDate - 2
            else
                sourceDate = sourceDate + 1
            end
            sourceDate = FreeDate(sourceDate)
        end
        if (bh[x] <> '')
            if (sourceDate = bh[x])
                !strTest = clip(strTest) & 'BH Check - Found Bank Holiday: ' & format(sourceDate, @d06b) & '<13,10>'
                if strDirection = '-'
                    sourceDate = sourceDate - 1
                else
                    sourceDate = sourceDate + 1
                end
                sourceDate = FreeDate(sourceDate)
            end
        end
    end ! loop

    return sourceDate
