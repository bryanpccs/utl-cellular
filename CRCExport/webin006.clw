

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBIN007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBIN008.INC'),ONCE        !Req'd for module callout resolution
                     END


DateAvailable        PROCEDURE  (strAccount, strDate, scAccountID, strPath) ! Declare Procedure
result      long
strIniFile  like(reg:BankHolidaysFileName)
availableDate date

connectionStr       string(260), static
dayOfWeek           string(10)
repairLimit         long
! SQL tables

SQLFile             file, driver('Scalable'), oem, owner(connectionStr), pre(sql), name('WebJob'), bindable, thread
Record                  record
RecordCount                 long, name('Ref_Number')
                        end
                    end
    map
IsDateAvailable procedure(date sourceDate), long
    end
  CODE
    !
    ! Description : Check if the date selected is an available date
    !               Returns true/false
    !               Unavailable dates are Sundays and bank holidays
    !

    result = true   ! Default to available date

    if strDate <> '' and exists(clip(strPath) & '\ACCREG.DAT')
        ! Change the paths this way
        ACCREG{Prop:Name} = clip(strPath) & '\ACCREG.DAT'
        REGIONS{Prop:Name} = clip(strPath) & '\REGIONS.DAT'
        SIDDEF{Prop:Name} = clip(strPath) & '\SIDDEF.DAT'
        SIDSRVCN{Prop:Name} = clip(strPath) & '\SIDSRVCN.DAT'

        Access:AccReg.Open()
        Access:AccReg.UseFile()

        Access:Regions.Open()
        Access:Regions.UseFile()

        Access:SIDDEF.Open()
        Access:SIDDEF.UseFile()

        Access:SIDSRVCN.Open()
        Access:SIDSRVCN.UseFile()

        availableDate = deformat(strDate, @d06b)

        Access:AccReg.ClearKey(acg:AccountNoKey)
        acg:AccountNo = strAccount
        if not Access:AccReg.Fetch(acg:AccountNoKey)

            Access:Regions.ClearKey(reg:RegionNameKey)
            reg:RegionName = acg:RegionName
            if not Access:Regions.Fetch(reg:RegionNameKey)
                strIniFile = reg:BankHolidaysFileName
            end

        end

        if strIniFile = ''
            strIniFile = 'BHENGWAL.DAT' ! Default
        end

        result = IsDateAvailable(availableDate)

        if result = true
            ! Retail Repair Capacity Limit
            set(SIDDEF, 0)
            if Access:SIDDEF.Next() = Level:Benign
                connectionStr = clip(SID:WebJobConn)
            end

            open(SQLFile)

            dayOfWeek = GetDayOfWeek(availableDate)

            case dayOfWeek
                of 'FRI' ! Total jobs booked on Friday and Saturday
                    SQLFile{prop:SQL} = 'SELECT COUNT(*) FROM WebJob LEFT JOIN WebJobEx ON WebJobEx.WebJobNo = WebJob.Ref_Number WHERE Completed = 0 AND SCAccountID = ' & scAccountID & ' AND (CustCollectionDate = ''' & format(availableDate, @d10-) & ''' OR CustCollectionDate = ''' & format(availableDate + 1, @d10-) & ''')'
                of 'SAT' ! Total jobs booked on Saturday and Friday
                    SQLFile{prop:SQL} = 'SELECT COUNT(*) FROM WebJob LEFT JOIN WebJobEx ON WebJobEx.WebJobNo = WebJob.Ref_Number WHERE Completed = 0 AND SCAccountID = ' & scAccountID & ' AND (CustCollectionDate = ''' & format(availableDate, @d10-) & ''' OR CustCollectionDate = ''' & format(availableDate - 1, @d10-) & ''')'
                else ! Single day
                    SQLFile{prop:SQL} = 'SELECT COUNT(*) FROM WebJob LEFT JOIN WebJobEx ON WebJobEx.WebJobNo = WebJob.Ref_Number WHERE Completed = 0 AND SCAccountID = ' & scAccountID & ' AND CustCollectionDate = ''' & format(availableDate, @d10-) & ''''
            end ! case

            next(SQLFile)
            !if error()
                !LinePrint('Error: ' & error() & ' ' & errorcode() & ' ' & fileerror() & ' ' & fileerrorcode(), clip(strPath) & '\Repair.log')
            !end

            repairLimit = 0

            Access:SIDSRVCN.ClearKey(srv:SCAccountIDKey)
            srv:SCAccountID = scAccountID
            if Access:SIDSRVCN.Fetch(srv:SCAccountIDKey) = Level:Benign
                case dayOfWeek
                    of 'SUN' ! Sunday is not applicable
                    of 'MON'
                        repairLimit = srv:MonRepairCapacity
                    of 'TUE'
                        repairLimit = srv:TueRepairCapacity
                    of 'WED'
                        repairLimit = srv:WedRepairCapacity
                    of 'THU'
                        repairLimit = srv:ThuRepairCapacity
                    of 'FRI'
                        repairLimit = srv:FriRepairCapacity
                    of 'SAT' ! Saturday should use Fridays repair limit
                        repairLimit = srv:FriRepairCapacity
                end ! case
            end

            ! LinePrint('Repair Limit: ' & repairLimit, clip(strPath) & '\Repair.log')
            if repairLimit > 0
                if sql:RecordCount >= repairLimit ! Also equal to because the current job has not yet been created
                    result = false
                end
            end

            close(SQLFile)
        end

        Access:Regions.Close()
        Access:AccReg.Close()
        Access:SIDDEF.Close()
        Access:SIDSRVCN.Close()

    end

    return result
IsDateAvailable procedure(date sourceDate)

x                   long(1)
bh                  date, dim(15)
dateIsAvailable     long

    code

    dateIsAvailable = true

    if (GetDayOfWeek(sourceDate) = 'SUN')
        dateIsAvailable = false
    else
        LoadBankHolidays(bh, strIniFile, strPath)

        loop x = 1 to 15
            if (bh[x] <> '')
                if (sourceDate = bh[x])
                    dateIsAvailable = false
                    break
                end
            end
        end ! loop

    end

    return dateIsAvailable
