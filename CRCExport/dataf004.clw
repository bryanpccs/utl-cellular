

   MEMBER('datafix.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('DATAF004.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:ImportFile       CSTRING(255),STATIC
ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
window               WINDOW('Data Fix'),AT(,,69,15),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ImportFile      File,Driver('BASIC', '/COMMA = 9' ),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record                  Record
RefNumber               Long()
ConsignmentNumber       String(60)
Manufacturer            String(30)
ModelNumber             String(30)
ExchangeIMEINumber      String(30) ! Customer IMEI Number
StatusCode              String(30)
ResolutionCode          String(30)
StatusDate              String(10)
StatusTime              String(10)
! Inserting (DBH 06/12/2005) #6436 - Exchange IMEI will now be passed seperately
! Repair IMEI is now passed at the end of the file - TrkBs: 6915 (DBH: 09-03-2006)
IMEINumber              String(30)
! End (DBH 06/12/2005) #6436
! Inserting (DBH 24/03/2006) #7413 - New field
IncomingConsignmentNumber   String(60)
! End (DBH 24/03/2006) #7413
                        End
                    End
! Dummy SQL table
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD
SQLRefNumber                Long, Name('Ref_Number')
! Inserting (DBH 27/02/2006) #6915 - Need to lookup extra fields
SQLSBJobNo                  Long, Name('SBJobNo')
SQLBookingType              String(1), Name('BookingType')
SQLSMSReq                   Byte(), Name('SMSReq')
SQLEmailReq                 Byte(), Name('EmailReq')
SQLCountryDelivery          String(20), Name('CountryDelivery') !ENGLAND, NORTHERN IRELAND, SCOTLAND, WALES
SQLValueSegment             String(20), Name('ValueSegment')
SQLSMSType                  String(1), Name('SMSType')
SQLSMSSent                  Long, Name('SMSSent')
SQLRetStoreCode             String(15), Name('RetStoreCode')
! End (DBH 27/02/2006) #6915
                        END
                    END

WebJobEx        file, driver('Scalable'), oem, owner(ConnectionStr), name('WEBJOBEX'), pre(webex), bindable, thread
Record              record
SCAccountID             long, name('SCAccountID')
                    end
                end

CCExchange      file, driver('Scalable'), oem, owner(ConnectionStr), name('CCExchange'), pre(ccex), bindable, thread
Record              record
TransitDays             long, name('TransitDays')
                    end
                end

ValSegBkType    file, driver('Scalable'), oem, owner(ConnectionStr), name('ValSegBkType'), pre(vsbt), bindable, thread
Record              record
IsCharge                byte, name('IsCharge')
ChargePercent           long, name('ChargePercent')
                    end
                end
TempFiles   Queue(file:Queue),pre(fil)
            End

i           long, auto
local       Class
ImportJobs          Procedure(String func:FileName, String func:ShortName)
            End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

Process:Directory routine
    !
    !
    !

    !Directory(TempFiles,Clip(Path()) & '\WilfredToSend\desp_?????????????.tab',ff_:DIRECTORY)
    Directory(TempFiles,Clip(Path()) & '\WilfredToSend\desp*.fix',ff_:DIRECTORY)
    Loop x# = 1 To Records(TempFiles)
        Get(TempFiles,x#)
        If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
            Cycle
        Else !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
!            ErrorLog('Importing: ' & Clip(fil:Name) & ' - ' & Round(fil:Size/1024,.01) & 'kb')
            Local.ImportJobs(Clip(Path()) & '\WilfredToSend\' & CLip(fil:name),Clip(fil:Name))
        End !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
    End !x# = 1 To Records(ScanFiles)

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  ! Import files
  Do Process:Directory

  Post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

Local.ImportJobs        Procedure(String func:FileName, String func:ShortName)

local:UpdateStatus       Byte(0)
local:JobUpdated         Byte(0)
! Inserting (DBH 06/12/2005) #6436 - Record the specific changes in the Audit Trail
local:AuditText          String(255)
! End (DBH 06/12/2005) #6436
! Is the status in the file different? (DBH: 11-05-2006)
local:StatusChanged      Byte(0)
local:LastStatusDate     Date
local:LastStatusTime     Time
local:despatchDate       Date
local:actualExchangeDate Date
local:transitDays        Long

local:LastStatusCode     String(3)

Code

    ! Start - Is this file a duplicate? (DBH: 01-04-2005)
    If Exists(Clip(Path()) & '\WilfredSent\' & Clip(func:ShortName))
        ErrorLog('Duplicate File Error: ' & Clip(func:ShortName))
        Return
    End ! If Exists(Clip(Path()) & '\WilfredSent\' & Clip(func:ShortName))
    ! End   - Is this file a duplicate? (DBH: 01-04-2005)

    Relate:JOBS.Open()
    Relate:DEFAULTS.Open()
    Relate:USERS.Open()
    Relate:EXCHANGE.Open()
    Relate:DESBATCH.Open()
    Relate:STATUS.Open()
    Relate:IMEISHIP.Open()
    Relate:PENDMAIL.Open()
    Relate:DEFEMAI2.Open()
    Set(DEFEMAI2)
    Access:DEFEMAI2.Next()
    Relate:AUDSTAEX.Open()
    Relate:CARISMA.Open()
    Relate:SIDSRVCN.Open()
    Relate:AUDSTATS.Open()
    Relate:SIDEXUPD.Open()
    Relate:MODELNUM.Open()
    Relate:SIDNWDAY.Open()

    IniFilepath =  Clip(Path()) & '\Webimp.ini'
    ConnectionStr = GetIni('Defaults', 'Connection', '', Clip(IniFilepath))

    Open(SQLFile)
    Open(WebJobEx)
    Open(ValSegBkType)
    Open(CCExchange)

    CountNormal# = 0
    CountStatus# = 0

    Access:EXCHANGE.UseFile()
    tmp:ImportFile = func:FileName
    Open(ImportFile)
    Set(ImportFile,0)
    Loop
        Next(ImportFile)
        If Error()
            Break
        End !If Error()

        local:AuditText = ''

        !Is there a job number here? - TrkBs:  (DBH: 02-02-2005)
        If impfil:RefNumber <> ''
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = impfil:RefNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
                ! Inserting (DBH 27/02/2006) #6915 - Need the jobse file to get the email address
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    ! Found
                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    ! Error
                End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                ! End (DBH 27/02/2006) #6915
                local:JobUpdated = False
                !Is there an exchange unit to add? - TrkBs:  (DBH: 02-02-2005)
                ! Changing (DBH 06/12/2005) #6436 - They are now sending the exchange imei in a seperate field
                                 !Make sure they aren't sending the job imei back too - TrkBs:  (DBH: 11-02-2005)
                                ! If impfil:IMEINumber <> '' And impfil:IMEINumber <> job:ESN
                ! to (DBH 06/12/2005) #6436
                !Make sure they aren't sending the job imei back too - TrkBs:  (DBH: 11-02-2005)
                If impfil:ExchangeIMEINumber <> '' And impfil:ExchangeIMEINumber <> job:ESN
                ! End (DBH 06/12/2005) #6436
                    !Only add exchange if not already attached - TrkBs:  (DBH: 02-02-2005)
                    If job:Exchange_Unit_Number = ''
                        Access:Exchange.ClearKey(xch:ESN_Only_Key)
                        xch:ESN            = impfil:ExchangeIMEINumber
                        if Access:Exchange.Fetch(xch:ESN_Only_Key) = Level:Benign
                            job:Exchange_Unit_Number = xch:Ref_Number
                            job:Exchange_User       = 'WEB'
                            job:Exchange_Despatched = Today()
                            job:Exchange_Despatched_User = 'WEB'
                            job:Exchange_Consignment_Number = impfil:ConsignmentNumber

                            If Access:JOBS.TryUpdate()
                                ErrorLog('Error updating job number: ' & job:Ref_Number)
                            End ! If Access:JOBS.TryUpdate()
                            local:JobUpdated = True
                        end
                    End ! If job:Exchange_Unit_Number = ''
                Else ! If impfil:IMEINumber <> ''
                    If impfil:ExchangeIMEINUmber = job:ESN
                        ErrorLog('Repair IMEI In Exchange IMEI Field. Job No: ' & job:Ref_Number & 'File: ' & Clip(func:ShortName))
                    End ! If impfil:ExchangeIMEINUmber = job:ESN
                End ! If impfil:IMEINumber <> ''

                local:UpdateStatus = True

                ! Variables hold the last date/time of a status change
                local:LastStatusDate = ''
                local:LastStatusTime = ''

                ! Inserting (DBH 04/04/2007) # 8901 - Check if the status change has already happened on the date/time. This prevents duplicate entries
                Access:AUDSTATS.Clearkey(aus:NewStatusKey)
                aus:RefNumber = job:Ref_Number
                aus:Type = 'JOB'
                Set(aus:NewStatusKey,aus:NewStatusKey)
                Loop ! Begin Loop
                    If Access:AUDSTATS.Next()
                        Break
                    End ! If Access:AUDSTATS.Next()
                    If aus:RefNumber <> job:Ref_Number
                        Break
                    End ! If aus:RefNumber <> job:Ref_Number
                    If aus:Type <> 'JOB'
                        Break
                    End ! If aus:Type <> 'JOB'

                    ! (GK 08/05/2007 # 8966 - Ignore Historic Date and Time Stamped)
                    If local:LastStatusDate = ''
                        local:LastStatusDate = aus:DateChanged
                        local:LastStatusTime = aus:TimeChanged
                        local:LastStatusCode = Sub(aus:NewStatus,1,3)
                    Else
                        If aus:DateChanged > local:LastStatusDate
                            local:LastStatusDate = aus:DateChanged
                            local:LastStatusTime = aus:TimeChanged
                            local:LastStatusCode = Sub(aus:NewStatus,1,3)
                        Else
                            If aus:DateChanged = local:LastStatusDate
                                If aus:TimeChanged > local:LastStatusTime
                                    local:LastStatusTime = aus:TimeChanged
                                    local:LastStatusCode = Sub(aus:NewStatus,1,3)
                                End
                            End
                        End
                    End
                End ! Loop
                ! End (DBH 04/04/2007) #8901

                If local:LastStatusCode <> Sub(job:Current_Status,1,3)
                    access:status.clearkey(sts:ref_number_only_key)
                    sts:ref_number  = Sub(impfil:StatusCode,1,3)
                    IF access:status.tryfetch(sts:ref_number_only_key)
                        job:current_status  = 'ERROR'
                    Else!IF access:status.tryfetch(sts:ref_number_only_key)
                        job:PreviousStatus  = job:current_status
                        !access:users.clearkey(use:password_key)
                        !use:password    = glo:password
                        !access:users.fetch(use:password_key)
                        !job:statusUser      = use:user_code
                        job:statusUser      = 'WEB'
                        job:current_status  = sts:status
                    End
                End

                local:JobUpdated = True
                    
                ! Update completed flag in WEBJOB if status is 705 COMPLETED
                if local:StatusChanged = true and sub(job:Current_Status,1,3) = '705'
                    SQLFile{Prop:SQL} = 'UPDATE WEBJOB SET Completed = 1 WHERE SBJobNo = ' & job:Ref_Number
                end

                CountStatus# += 1

                !Is there a consignment number? - TrkBs:  (DBH: 02-02-2005)
                If Clip(impfil:ConsignmentNumber) <> ''
                    !If there is an exchange attached, update the exchange consignment no - TrkBs:  (DBH: 02-02-2005)
                    If job:Exchange_Unit_Number <> ''
                        job:Exchange_Consignment_Number = impfil:ConsignmentNumber
                    Else ! If job:Exchange_Unit_Number <> ''
                        job:Consignment_Number = impfil:ConsignmentNumber
                    End ! If job:Exchange_Unit_Number <> ''
                    local:JobUpdated = True
                End ! If Clip(impfil:ConsignmentNumber) <> ''

                If impfil:IMEINumber <> job:ESN And impfil:IMEINumber <> ''
                    job:ESN = impfil:IMEINumber
                    local:JobUpdated = True
                End ! If impfil:IMEINumber <> job:ESN

                ! Inserting (DBH 24/03/2006) #7413 - Write the Incoming COnsignment Number
                If impfil:IncomingConsignmentNumber <> ''
                    job:Incoming_Consignment_Number = Clip(impfil:IncomingConsignmentNumber)
                    local:JobUpdated = True
                End ! If impfil:IncomingConsignmentNumber <> ''
                ! End (DBH 24/03/2006) #7413

! Changing (DBH 18/04/2008) # N/A - Stop updating error messages
!                Access:JOBS.Update()
! to (DBH 18/04/2008) # N/A
                If Access:JOBS.TryUpdate()
                    ErrorLog('Error updating job number: ' & job:Ref_Number)
                End ! If Access:JOBS.TryUpdate()
! End (DBH 18/04/2008) #N/A
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        Else ! If impfil:RefNumber <> ''
            Cycle
        End ! If impfil:RefNumber <> ''
    End !Loop(ImportFile)
    Close(ImportFile)
    Close(SQLFile)
    Close(WebJobEx)
    Close(ValSegBkType)
    Close(CCExchange)
    Relate:JOBS.Close()
    Relate:DEFAULTS.Close()
    Relate:USERS.Close()
    Relate:EXCHANGE.Close()
    Relate:DESBATCH.Close()
    Relate:STATUS.Close()
    Relate:IMEISHIP.Close()
    Relate:PENDMAIL.Close()
    Relate:DEFEMAI2.Close()
    Relate:AUDSTAEX.Close()
    Relate:CARISMA.Close()
    Relate:SIDSRVCN.Close()
    Relate:AUDSTATS.Close()
    Relate:SIDEXUPD.Close()
    Relate:MODELNUM.Close()
    Relate:SIDNWDAY.Close()

!    ErrorLog('Exchage Jobs Imported: ' & CountNormal#)
!    ErrorLog('Status Update Jobs Imported: ' & CountStatus#)

    Rename(Clip(Path()) & '\WilfredToSend\' & Clip(func:ShortName),Clip(Path()) & '\WilfredSent\' & Clip(func:ShortName))

     IF Error()
        Case Error()
            Of ''
                Error" = 'Same File Already Exists'
            Of 2
                Error" = 'File Not Found'
            Of 5
                Error" = 'Access Denied'
            Of 52
                Error" = 'File Aready Open'
            Else
                Error" = Error()
        End ! Case Error()
        ErrorLog('Rename Error (' & Clip(Error") & '): ' & Clip(func:ShortName))
    Else ! If Error()
!        ErrorLog('File Placed in ''Sent'' folder')
    End ! If Error()
    !End   - Debug - TrkBs:  (DBH: 25-01-2005)
