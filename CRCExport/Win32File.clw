
  member

  ! Includes

  include('Win32file.inc'), once

CWin32File.Construct procedure

  code

CWin32File.Destruct procedure

  code
    if self.fileHandle
      self.Close().

CWin32File.Init procedure(*cstring pFileName, byte bAccessMode)

result bool

  code
    self.fileName = pFileName
    self.accessMode = bAccessMode

    case self.accessMode
      of READ_MODE
        self.fileHandle = CreateFile(self.fileName, GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0)
      of WRITE_MODE
        self.fileHandle = CreateFile(self.fileName, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0)
      of APPEND_MODE
        if exists(self.fileName)
          self.fileHandle = CreateFile(self.fileName, GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0)
        else
          ! Create the file if it does not exists
          self.fileHandle = CreateFile(self.fileName, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0)
          self.accessMode = WRITE_MODE
        end
    end

?   assert(self.fileHandle)

    if self.fileHandle <> INVALID_HANDLE_VALUE
      if self.accessMode = APPEND_MODE
        self.GotoEOF()
      end

      result = 1
    end

    return result

CWin32File.Init procedure(*string pFileName, byte bAccessMode)

result bool
szFileName &cstring 
  code
    szFileName &= new(cstring(size(pFileName) + 1))
    szFileName = clip(pFileName) & '<0>'

    result = self.Init(szFileName, bAccessMode)

    dispose(szFileName)

    return result

CWin32File.Init procedure(string strFileName, byte bAccessMode)

result bool
szFileName &cstring 
  code
    szFileName &= new(cstring(len(clip(strFileName)) + 1))
    szFileName = clip(strFileName) & '<0>'

    result = self.Init(szFileName, bAccessMode)

    dispose(szFileName)

    return result     

CWin32File.Read procedure(*cstring pBuffer, long BytesToRead)
 
result bool
BytesRead long
 
  code
    if BytesToRead = 0 then BytesToRead = size(pBuffer).
    if size(pBuffer) = BytesToRead then BytesToRead -= 1.

    if size(pBuffer) > BytesToRead      
      result = ReadFile(self.fileHandle, pBuffer, BytesToRead, BytesRead, 0)
?     assert(result)

      pBuffer[BytesRead + 1] = '<0>'
    else
?     assert(0)
    end

    return BytesRead

CWin32File.Read procedure(*string pBuffer, long BytesToRead)
 
result bool
BytesRead long
szBuffer &cstring 
  code
    szBuffer &= new(cstring(size(pBuffer) + 1))
    szBuffer = clip(pBuffer) & '<0>'

    BytesRead = self.Read(szBuffer, BytesToRead)

    pBuffer = clip(szBuffer)

    dispose(szBuffer)

    return BytesRead

CWin32File.Write procedure(*cstring pBuffer, long BytesToWrite)

result bool
BytesWritten long
  
  code
    if BytesToWrite = 0 then BytesToWrite = len(clip(pBuffer)). 
    result = WriteFile(self.fileHandle, pBuffer, BytesToWrite, BytesWritten, 0)
?   assert(result)

    return BytesWritten

CWin32File.Write procedure(*string pBuffer, long BytesToWrite)

result bool
BytesWritten long
szBuffer &cstring  
  code
    szBuffer &= new(cstring(size(pBuffer) + 1))
    szBuffer = clip(pBuffer) & '<0>'

    BytesWritten = self.Write(szBuffer, BytesToWrite)

    dispose(szBuffer)

    return BytesWritten

CWin32File.Write procedure(string strBuffer, long BytesToWrite)

result bool
BytesWritten long
szBuffer &cstring  
  code
    szBuffer &= new(cstring(len(clip(strBuffer)) + 1))
    szBuffer = clip(strBuffer) & '<0>'

    BytesWritten = self.Write(szBuffer, BytesToWrite)

    dispose(szBuffer)

    return BytesWritten

CWin32File.GotoEOF procedure()

result ulong

  code
    result = SetFilePointer(self.fileHandle, 0, 0, FILE_END)
    
    return result    

CWin32File.Close procedure

result bool

  code
    if self.fileHandle
      result = CloseHandle(self.fileHandle)
?     assert(result)
      self.fileHandle = 0
    end
