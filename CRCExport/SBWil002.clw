

   MEMBER('SBWilfredDefaults.clw')                    ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBWIL002.INC'),ONCE        !Local module procedure declarations
                     END


FTPDefaults PROCEDURE                                 !Generated from procedure template - Window

tmp:RemoteServer     STRING(255)
tmp:RemoteUsername   STRING(255)
tmp:RemotePassword   STRING(255)
tmp:RemoteExportPath STRING(255)
tmp:RemoteImportPath STRING(255)
ActionMessage        CSTRING(40)
window               WINDOW('Defaults'),AT(,,311,310),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       GROUP('FTP Defaults'),AT(4,0,304,96),USE(?Group1),BOXED
                         ENTRY(@s255),AT(100,12,200,8),USE(crcdef:FTPServer),FONT(,,,FONT:bold),MSG('FTPServer')
                         ENTRY(@s60),AT(100,26,200,8),USE(crcdef:FTPUserName),FONT(,,,FONT:bold),MSG('FTPUserName')
                         ENTRY(@s60),AT(100,42,200,8),USE(crcdef:FTPPassword),FONT(,,,FONT:bold),MSG('FTPPassword')
                         ENTRY(@s255),AT(100,58,200,8),USE(crcdef:FTPExportPath),FONT(,,,FONT:bold),MSG('FTPExportPath')
                         ENTRY(@s255),AT(100,74,200,8),USE(crcdef:FTPImportPath),FONT(,,,FONT:bold),MSG('FTPImportPath')
                       END
                       PROMPT('Server'),AT(12,12),USE(?tmp:RemoteServer:Prompt)
                       PROMPT('Username'),AT(12,28),USE(?tmp:RemoteUsername:Prompt)
                       PROMPT('Password'),AT(12,44),USE(?tmp:RemotePassword:Prompt)
                       PROMPT('Export Path'),AT(12,60),USE(?tmp:RemoteExportPath:Prompt)
                       PROMPT('Import Path'),AT(12,76),USE(?tmp:RemoteImportPath:Prompt)
                       GROUP('General Email Defaults'),AT(4,100,304,64),USE(?Group5),BOXED
                         PROMPT('Email Server'),AT(12,112),USE(?tmp:RemoteImportPath:Prompt:8)
                         ENTRY(@s255),AT(100,110,200,8),USE(crcdef:EmailServer),FONT(,,,FONT:bold),MSG('EmailServer')
                         ENTRY(@s3),AT(100,126,200,8),USE(crcdef:EmailServerPort),FONT(,,,FONT:bold),MSG('EmailServerPort')
                         PROMPT('Email Server Port'),AT(12,128),USE(?tmp:RemoteImportPath:Prompt:9)
                         PROMPT('"Email From" Address'),AT(12,144),USE(?tmp:RemoteImportPath:Prompt:10)
                         ENTRY(@s255),AT(100,142,200,8),USE(crcdef:EmailFromAddress),FONT(,,,FONT:bold),MSG('EmailFromAddress')
                       END
                       GROUP('CRC Email Defaults'),AT(4,176,304,32),USE(?Group2),BOXED
                         PROMPT('Email Address'),AT(12,188),USE(?tmp:RemoteImportPath:Prompt:2)
                         ENTRY(@s255),AT(100,188,200,8),USE(crcdef:CRCEmailAddress),FONT(,,,FONT:bold),MSG('CRCEmailAddress')
                       END
                       GROUP('PC Control Systems Email Defaults'),AT(4,212,304,32),USE(?Group2:2),BOXED
                         PROMPT('Email Address'),AT(12,224),USE(?tmp:RemoteImportPath:Prompt:4)
                         ENTRY(@s255),AT(100,224,200,8),USE(crcdef:PCCSEmailAddress),FONT(,,,FONT:bold),MSG('PCCSEmailAddress')
                       END
                       GROUP('Vodafone Email Defaults'),AT(4,252,304,32),USE(?Group2:3),BOXED
                         PROMPT('Email Address'),AT(12,264),USE(?tmp:RemoteImportPath:Prompt:7)
                         ENTRY(@s255),AT(100,262,200,8),USE(crcdef:PCCSEmailAddress,,?crcdef:PCCSEmailAddress:2),FONT(,,,FONT:bold),MSG('PCCSEmailAddress')
                       END
                       BUTTON('OK'),AT(192,288,56,16),USE(?OK),DEFAULT,REQ
                       BUTTON('Cancel'),AT(252,288,56,16),USE(?Cancel)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:CRCDEFS.Open
    SET(CRCDEFS)
    CASE Access:CRCDEFS.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:CRCDEFS.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('FTPDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?crcdef:FTPServer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:CRCDEFS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CRCDEFS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:CRCDEFS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(CRCDEFS)
      Access:CRCDEFS.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CRCDEFS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

