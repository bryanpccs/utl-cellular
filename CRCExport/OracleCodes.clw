   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('ORACLBC.CLW')
DctInit     PROCEDURE
DctKill     PROCEDURE
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('ORACL001.CLW')
Main                   PROCEDURE   !Browse the MODELNUM File
     END
         Module('clib')
             LtoA(Long num,*Cstring s,Signed radix),ULONG,RAW,NAME('_ltoa'),PROC
         End ! Module('clib)
     MODULE('Oracl003.clw')
       DC:FilesConverter(<STRING PAR:FileLabel>,<STRING PAR:FileName>),BYTE,PROC
     END
   END

SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

    Map
BHStripReplace           Procedure(String func:String,String func:Strip,String func:Replace),String
BHStripNonAlphaNum           Procedure(String func:String,String func:Replace),String
BHReturnDaysHoursMins       Procedure(Long func:TotalMins,*Long func:Days,*Long func:Hours,*Long func:Mins)
BHTimeDifference24Hr        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime),Long
BHTimeDifference24Hr5Days   Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime),Long
BHTimeDifference24Hr5DaysMonday   Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime,String func:MondayStart),Long
    End
CARISMA              FILE,DRIVER('Btrieve'),NAME('CARISMA.DAT'),PRE(cma),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(cma:RecordNumber),NOCASE,PRIMARY
ManufactModColourKey     KEY(cma:Manufacturer,cma:ModelNo,cma:Colour),DUP,NOCASE
CarismaCodeKey           KEY(cma:CarismaCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
Colour                      STRING(30)
CarismaCode                 STRING(30)
                         END
                     END                       

MANUFACT             FILE,DRIVER('Btrieve'),NAME('MANUFACT.DAT'),PRE(man),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(man:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(man:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
IsCCentreSpecific           BYTE
EDIFileType                 STRING(30)
                         END
                     END                       

MODELNUM             FILE,DRIVER('Btrieve'),NAME('MODELNUM.DAT'),PRE(mod),CREATE,BINDABLE,THREAD
Model_Number_Key         KEY(mod:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(mod:Manufacturer,mod:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(mod:Manufacturer,mod:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
                         END
                     END                       

MODELCOL             FILE,DRIVER('Btrieve'),OEM,NAME('MODELCOL.DAT'),PRE(moc),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(moc:Record_Number),NOCASE,PRIMARY
Colour_Key               KEY(moc:Model_Number,moc:Colour),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Model_Number                STRING(30)
Colour                      STRING(30)
                         END
                     END                       




GLODC:ConversionResult BYTE
Access:CARISMA       &FileManager
Relate:CARISMA       &RelationManager
Access:MANUFACT      &FileManager
Relate:MANUFACT      &RelationManager
Access:MODELNUM      &FileManager
Relate:MODELNUM      &RelationManager
Access:MODELCOL      &FileManager
Relate:MODELCOL      &RelationManager
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE(0),THREAD
GlobalResponse       BYTE(0),THREAD
VCRRequest           LONG(0),THREAD
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  DctInit
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('OracleCodes.INI')
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  DctKill
  GlobalErrors.Kill
    
BHStripReplace            Procedure(String func:String,String func:Strip,String func:Replace)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1
    StripLength#    = Len(func:Strip)

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

       !Exception for space

        IF SUB(func:String,STR_POS#,StripLength#) = func:Strip
            If func:Replace <> ''
                func:String = SUB(func:String,1,STR_POS#-1) & func:Replace & SUB(func:String,STR_POS#+StripLength#,STR_LEN#-1)

            Else !If func:Replace <> ''
                func:String = SUB(func:String,1,STR_POS#-1) &  SUB(func:String,STR_POS#+StripLength#,STR_LEN#-1)

            End !If func:Replace <> ''

        End
    End
    RETURN(func:String)
BHStripNonAlphaNum      Procedure(String func:String,String func:Replace)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

       !Exception for space

     IF VAL(SUB(func:string,STR_POS#,1)) < 32 Or VAL(SUB(func:string,STR_POS#,1)) > 126 Or |
        VAL(SUB(func:string,STR_POS#,1)) = 34 Or VAL(SUB(func:string,STR_POS#,1)) = 44
           func:String = SUB(func:String,1,STR_POS#-1) & func:Replace & SUB(func:String,STR_POS#+1,STR_LEN#-1)
        End
    End
    RETURN(func:String)
BHReturnDaysHoursMins       Procedure(Long func:TotalMins,*Long func:Days,*Long func:Hours,*Long func:Mins)
Code
    func:Hours = 0
    func:Days = 0
    func:Mins = func:TotalMins
    If func:Totalmins >= 60
        Loop Until func:Mins < 60
            func:Mins -= 60
            func:Hours += 1
            If func:Hours > 23
                func:Days += 1
                func:Hours = 0
            End !If func:Hours > 23
        End !Loop Until local:MinutesLeft < 60
    End !If func:Minutes > 60
BHTimeDifference24Hr        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        local:StartTime   = Deformat('00:00:00',@t4)
        local:EndTime     = Deformat('23:59:59',@t4)

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2

        local:2a = 0
        local:2b = 0
        local:2c = 0
        local:2d = 0

        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)

BHTimeDifference24Hr5Days    Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        local:StartTime   = Deformat('00:00:00',@t4)
        local:EndTime     = Deformat('23:59:59',@t4)

        If (x# % 7) = 0
            Cycle
        End
        If (x# % 7) = 6
            Cycle
        End

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2

        local:2a = 0
        local:2b = 0
        local:2c = 0
        local:2d = 0

        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)

BHTimeDifference24Hr5DaysMonday    Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime,String func:MondayStart)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        If (x# % 7) = 1
            local:StartTime   = Deformat(Clip(func:MondayStart),@t4)
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)
        Else !If (x# % 7) = 1
            local:StartTime   = Deformat('00:00:00',@t4)
            local:2a = 0
            local:2b = 0
            local:2c = 0
            local:2d = 0
        End !If (x# % 7) = 1

        local:EndTime     = Deformat('23:59:59',@t4)

        If (x# % 7) = 0
            Cycle
        End
        If (x# % 7) = 6
            Cycle
        End

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2



        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)




