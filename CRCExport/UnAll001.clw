

   MEMBER('UnAllocModels.clw')                        ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('UNALL001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

window               WINDOW('UnAlloc Models Check'),AT(,,69,15),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       PROMPT('In Progress...'),AT(4,0),USE(?Prompt1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MANUFACT.Open
  Relate:SIDMODSC.Open
  Access:MODELNUM.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  0{Prop:Hide} = true
  
  ! A small utility to generate a log file of any unallocated models (models which do not have a service centre attached)
  
  set(mod:Model_Number_Key)
  loop
      if Access:MODELNUM.Next() <> Level:Benign then break.
      if mod:IsVodafoneSpecific = false and mod:IsCCentreSpecific = false then cycle.
  
      Access:MANUFACT.ClearKey(man:Manufacturer_Key)
      man:Manufacturer = mod:Manufacturer
      if Access:MANUFACT.Fetch(man:Manufacturer_Key) <> Level:Benign then cycle.
      if man:IsVodafoneSpecific = false and man:IsCCentreSpecific = false then cycle.
  
      if man:IsVodafoneSpecific = true and mod:IsVodafoneSpecific = true
          ! Retail
          Access:SIDMODSC.ClearKey(msc:ManModelTypeKey)
          msc:Manufacturer = mod:Manufacturer
          msc:ModelNo = mod:Model_Number
          msc:BookingType = 'R'
          if Access:SIDMODSC.Fetch(msc:ManModelTypeKey) <> Level:Benign
              ErrorLog('Retail - No Service Centre Attached For : ' & clip(mod:Manufacturer) & ' ' & clip(mod:Model_Number))
          end
      end
  
      if man:IsCCentreSpecific = true and mod:IsCCentreSpecific = true
          ! Exchange / Back To Base
          Access:SIDMODSC.ClearKey(msc:ManModelTypeKey)
          msc:Manufacturer = mod:Manufacturer
          msc:ModelNo = mod:Model_Number
          msc:BookingType = 'E'
          if Access:SIDMODSC.Fetch(msc:ManModelTypeKey) <> Level:Benign
              ErrorLog('Exchange - No Service Centre Attached For : ' & clip(mod:Manufacturer) & ' ' & clip(mod:Model_Number))
          end
  
          Access:SIDMODSC.ClearKey(msc:ManModelTypeKey)
          msc:Manufacturer = mod:Manufacturer
          msc:ModelNo = mod:Model_Number
          msc:BookingType = 'B'
          if Access:SIDMODSC.Fetch(msc:ManModelTypeKey) <> Level:Benign
              ErrorLog('Back To Base - No Service Centre Attached For : ' & clip(mod:Manufacturer) & ' ' & clip(mod:Model_Number))
          end
      end
  
  end
  
  post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
    Relate:SIDMODSC.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

