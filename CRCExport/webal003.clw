

   MEMBER('webalert.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('WEBAL003.INC'),ONCE        !Local module procedure declarations
                     END


Dummy PROCEDURE                                       !Generated from procedure template - Window

window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Dummy')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  Relate:DEFEMAB1.Open
  Relate:DEFEMAB2.Open
  Relate:DEFEMAB3.Open
  Relate:DEFEMAB4.Open
  Relate:DEFEMAB5.Open
  Relate:DEFEMAB6.Open
  Relate:DEFEMAB7.Open
  Relate:DEFEMAE1.Open
  Relate:DEFEMAE2.Open
  Relate:DEFEMAE3.Open
  Relate:DEFEMAI2.Open
  Relate:DEFEMAIL.Open
  Relate:DEFEMAR1.Open
  Relate:DEFEMAR2.Open
  Relate:DEFEMAR3.Open
  Relate:DEFEMAR4.Open
  Relate:DEFEME2B.Open
  Relate:DEFEME3B.Open
  Relate:JOBS.Open
  Relate:MERGE.Open
  Relate:PENDMAIL.Open
  Relate:PENDMAIL_ALIAS.Open
  Relate:REGIONS.Open
  Relate:SIDALDEF.Open
  Relate:SIDALER2.Open
  Access:SIDALERT.UseFile
  Access:SIDREMI2.UseFile
  Access:SIDREMIN.UseFile
  Access:JOBSE.UseFile
  Access:SMOALERT.UseFile
  Access:SMOALER2.UseFile
  Access:SMOREMIN.UseFile
  Access:SMOREMI2.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFEMAB1.Close
    Relate:DEFEMAB2.Close
    Relate:DEFEMAB3.Close
    Relate:DEFEMAB4.Close
    Relate:DEFEMAB5.Close
    Relate:DEFEMAB6.Close
    Relate:DEFEMAB7.Close
    Relate:DEFEMAE1.Close
    Relate:DEFEMAE2.Close
    Relate:DEFEMAE3.Close
    Relate:DEFEMAI2.Close
    Relate:DEFEMAIL.Close
    Relate:DEFEMAR1.Close
    Relate:DEFEMAR2.Close
    Relate:DEFEMAR3.Close
    Relate:DEFEMAR4.Close
    Relate:DEFEME2B.Close
    Relate:DEFEME3B.Close
    Relate:JOBS.Close
    Relate:MERGE.Close
    Relate:PENDMAIL.Close
    Relate:PENDMAIL_ALIAS.Close
    Relate:REGIONS.Close
    Relate:SIDALDEF.Close
    Relate:SIDALER2.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

