

   MEMBER('sidjobextract.clw')                        ! This is a MEMBER module

                     MAP
                       INCLUDE('SIDJO001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SIDJO002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SIDJO003.INC'),ONCE        !Req'd for module callout resolution
                     END


Main                 PROCEDURE                        ! Declare Procedure
tmp:ExportFile       STRING(255)
  CODE
    ErrorLog('Begin Extract =============')
    tmp:ExportFile = ''
    tmp:ExportFile = SIDExtract(1)
    If tmp:ExportFile <> ''
        x# = Instring('\',tmp:ExportFile,-1,Len(tmp:ExportFile))
        ErrorLog('File Path: ' & CLip(tmp:ExportFile))
        ErrorLog('File Created: ' & Clip(Sub(tmp:ExportFile,x# + 1,40)))
        SendEmail(tmp:ExportFile)
        FTP_JumpStart(tmp:ExportFile,Sub(tmp:ExportFile,x# + 1,40))
        Remove(tmp:ExportFile)
    End ! If tmp:ExportFile <> ''
    ErrorLog('End Extract =============')
