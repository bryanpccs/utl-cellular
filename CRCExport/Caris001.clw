

   MEMBER('CarismaCodes.clw')                         ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CARIS001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CARIS002.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:Manufacturer     STRING(30)
tmp:CarismaCode      STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:IsVodafoneSpecific LIKE(man:IsVodafoneSpecific)   !Browse hot field - type derived from field
man:IsCCentreSpecific  LIKE(man:IsCCentreSpecific)    !Browse hot field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                       PROJECT(mod:IsVodafoneSpecific)
                       PROJECT(mod:IsCCentreSpecific)
                       JOIN(MODELCOL,'mod:Model_Number = moc:Model_Number'),INNER
                         PROJECT(moc:Colour)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !List box control field - type derived from field
moc:Colour             LIKE(moc:Colour)               !List box control field - type derived from field
tmp:CarismaCode        LIKE(tmp:CarismaCode)          !List box control field - type derived from local data
mod:IsVodafoneSpecific LIKE(mod:IsVodafoneSpecific)   !Browse hot field - type derived from field
mod:IsCCentreSpecific  LIKE(mod:IsCCentreSpecific)    !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:IsVodafoneSpecific)
                       PROJECT(man:IsCCentreSpecific)
                       PROJECT(man:RecordNumber)
                     END
QuickWindow          WINDOW('Browse Oracle Codes'),AT(,,440,172),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('Main'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,34,344,128),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('80L(2)|M~Model Number~@s30@80L(2)|M~Manufacturer~@s30@80L(2)|M~Colour~@s30@120L(' &|
         '2)|M~Oracle Code~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Change'),AT(364,73,72,20),USE(?BtnChange),LEFT,ICON('edit.ico')
                       SHEET,AT(4,4,352,162),USE(?CurrentTab),SPREAD
                         TAB('By Model Number'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,20,124,10),USE(mod:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Manufacturer'),USE(?Tab2)
                           ENTRY(@s30),AT(132,20,124,10),USE(mod:Model_Number,,?mod:Model_Number:2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           COMBO(@s30),AT(8,20,120,10),USE(tmp:Manufacturer),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L|@s30@'),DROP(10,120),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON('Close'),AT(364,146,72,20),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GLODC:ConversionResult = DC:FilesConverter()
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CARISMA.Open
  Relate:MANUFACT.Open
  Relate:MODELCOL.Open
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MODELNUM,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ?Browse:1{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:Manufacturer)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?mod:Model_Number:2,mod:Model_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mod:Model_Number,mod:Model_Number,1,BRW1)
  BIND('tmp:CarismaCode',tmp:CarismaCode)
  BRW1.AddField(mod:Model_Number,BRW1.Q.mod:Model_Number)
  BRW1.AddField(mod:Manufacturer,BRW1.Q.mod:Manufacturer)
  BRW1.AddField(moc:Colour,BRW1.Q.moc:Colour)
  BRW1.AddField(tmp:CarismaCode,BRW1.Q.tmp:CarismaCode)
  BRW1.AddField(mod:IsVodafoneSpecific,BRW1.Q.mod:IsVodafoneSpecific)
  BRW1.AddField(mod:IsCCentreSpecific,BRW1.Q.mod:IsCCentreSpecific)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB2.Init(tmp:Manufacturer,?tmp:Manufacturer,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(man:Manufacturer_Key)
  FDCB2.AddField(man:Manufacturer,FDCB2.Q.man:Manufacturer)
  FDCB2.AddField(man:IsVodafoneSpecific,FDCB2.Q.man:IsVodafoneSpecific)
  FDCB2.AddField(man:IsCCentreSpecific,FDCB2.Q.man:IsCCentreSpecific)
  FDCB2.AddField(man:RecordNumber,FDCB2.Q.man:RecordNumber)
  FDCB2.AddUpdateField(man:Manufacturer,tmp:Manufacturer)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CARISMA.Close
    Relate:MANUFACT.Close
    Relate:MODELCOL.Close
    Relate:MODELNUM.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?BtnChange
      BRW1.UpdateBuffer()
      
      if mod:Model_Number <> ''
          if UpdateCarismaCode(mod:Model_Number, mod:Manufacturer, moc:Colour) = true
              BRW1.ResetSort(1)
          end
      end
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Manufacturer
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?mod:Model_Number
      select(?Browse:1)
    OF ?mod:Model_Number:2
      select(?Browse:1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  if mod:IsVodafoneSpecific = false and mod:IsCCentreSpecific = false then ReturnValue = Record:Filtered.
  
  tmp:CarismaCode = ''
  
  Access:CARISMA.ClearKey(cma:ManufactModColourKey)
  cma:Manufacturer = mod:Manufacturer
  cma:ModelNo = mod:Model_Number
  cma:Colour = moc:Colour
  if Access:CARISMA.Fetch(cma:ManufactModColourKey) = Level:Benign
      tmp:CarismaCode = cma:CarismaCode
  end
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


FDCB2.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateRecord()
  if man:IsVodafoneSpecific = false and man:IsCCentreSpecific = false then ReturnValue = Record:Filtered.
  RETURN ReturnValue

