

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN007.INC'),ONCE        !Local module procedure declarations
                     END


GetDayOfWeek         PROCEDURE  (paramDate)                ! Declare Procedure
dayOfWeek string(3)
  CODE
    !
    ! GetDayOfTheWeek(date paramDate), string
    !
    ! Description : Get the day of the week for a specified date.
    !               Return a three character string of the day name.
    !


    case (paramDate % 7)
        of 0
            dayOfWeek = 'SUN'
        of 1
            dayOfWeek = 'MON'
        of 2
            dayOfWeek = 'TUE'
        of 3
            dayOfWeek = 'WED'
        of 4
            dayOfWeek = 'THU'
        of 5
            dayOfWeek = 'FRI'
        of 6
            dayOfWeek = 'SAT'
    end ! case

    return dayOfWeek
