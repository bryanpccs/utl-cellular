

   MEMBER('alertviewer.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('ALERT001.INC'),ONCE        !Local module procedure declarations
                     END



Main PROCEDURE                                        !Generated from procedure template - Browse

CurrentTab           STRING(80)
tmp:BrowseFilter     BYTE
tmp:False            BYTE(0)
BRW1::View:Browse    VIEW(PENDMAIL)
                       PROJECT(pem:DateCreated)
                       PROJECT(pem:TimeCreated)
                       PROJECT(pem:MessageType)
                       PROJECT(pem:SMSEmail)
                       PROJECT(pem:MobileNumber)
                       PROJECT(pem:EmailAddress)
                       PROJECT(pem:RefNumber)
                       PROJECT(pem:DateToSend)
                       PROJECT(pem:TimeToSend)
                       PROJECT(pem:DateSent)
                       PROJECT(pem:TimeSent)
                       PROJECT(pem:BookingType)
                       PROJECT(pem:Status)
                       PROJECT(pem:SIDJobNumber)
                       PROJECT(pem:CustomerUTL)
                       PROJECT(pem:ErrorMessage)
                       PROJECT(pem:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
pem:DateCreated        LIKE(pem:DateCreated)          !List box control field - type derived from field
pem:DateCreated_NormalFG LONG                         !Normal forground color
pem:DateCreated_NormalBG LONG                         !Normal background color
pem:DateCreated_SelectedFG LONG                       !Selected forground color
pem:DateCreated_SelectedBG LONG                       !Selected background color
pem:TimeCreated        LIKE(pem:TimeCreated)          !List box control field - type derived from field
pem:TimeCreated_NormalFG LONG                         !Normal forground color
pem:TimeCreated_NormalBG LONG                         !Normal background color
pem:TimeCreated_SelectedFG LONG                       !Selected forground color
pem:TimeCreated_SelectedBG LONG                       !Selected background color
pem:MessageType        LIKE(pem:MessageType)          !List box control field - type derived from field
pem:MessageType_NormalFG LONG                         !Normal forground color
pem:MessageType_NormalBG LONG                         !Normal background color
pem:MessageType_SelectedFG LONG                       !Selected forground color
pem:MessageType_SelectedBG LONG                       !Selected background color
pem:SMSEmail           LIKE(pem:SMSEmail)             !List box control field - type derived from field
pem:SMSEmail_NormalFG  LONG                           !Normal forground color
pem:SMSEmail_NormalBG  LONG                           !Normal background color
pem:SMSEmail_SelectedFG LONG                          !Selected forground color
pem:SMSEmail_SelectedBG LONG                          !Selected background color
pem:MobileNumber       LIKE(pem:MobileNumber)         !List box control field - type derived from field
pem:MobileNumber_NormalFG LONG                        !Normal forground color
pem:MobileNumber_NormalBG LONG                        !Normal background color
pem:MobileNumber_SelectedFG LONG                      !Selected forground color
pem:MobileNumber_SelectedBG LONG                      !Selected background color
pem:EmailAddress       LIKE(pem:EmailAddress)         !List box control field - type derived from field
pem:EmailAddress_NormalFG LONG                        !Normal forground color
pem:EmailAddress_NormalBG LONG                        !Normal background color
pem:EmailAddress_SelectedFG LONG                      !Selected forground color
pem:EmailAddress_SelectedBG LONG                      !Selected background color
pem:RefNumber          LIKE(pem:RefNumber)            !List box control field - type derived from field
pem:RefNumber_NormalFG LONG                           !Normal forground color
pem:RefNumber_NormalBG LONG                           !Normal background color
pem:RefNumber_SelectedFG LONG                         !Selected forground color
pem:RefNumber_SelectedBG LONG                         !Selected background color
pem:DateToSend         LIKE(pem:DateToSend)           !List box control field - type derived from field
pem:DateToSend_NormalFG LONG                          !Normal forground color
pem:DateToSend_NormalBG LONG                          !Normal background color
pem:DateToSend_SelectedFG LONG                        !Selected forground color
pem:DateToSend_SelectedBG LONG                        !Selected background color
pem:TimeToSend         LIKE(pem:TimeToSend)           !List box control field - type derived from field
pem:TimeToSend_NormalFG LONG                          !Normal forground color
pem:TimeToSend_NormalBG LONG                          !Normal background color
pem:TimeToSend_SelectedFG LONG                        !Selected forground color
pem:TimeToSend_SelectedBG LONG                        !Selected background color
pem:DateSent           LIKE(pem:DateSent)             !List box control field - type derived from field
pem:DateSent_NormalFG  LONG                           !Normal forground color
pem:DateSent_NormalBG  LONG                           !Normal background color
pem:DateSent_SelectedFG LONG                          !Selected forground color
pem:DateSent_SelectedBG LONG                          !Selected background color
pem:TimeSent           LIKE(pem:TimeSent)             !List box control field - type derived from field
pem:TimeSent_NormalFG  LONG                           !Normal forground color
pem:TimeSent_NormalBG  LONG                           !Normal background color
pem:TimeSent_SelectedFG LONG                          !Selected forground color
pem:TimeSent_SelectedBG LONG                          !Selected background color
pem:BookingType        LIKE(pem:BookingType)          !List box control field - type derived from field
pem:BookingType_NormalFG LONG                         !Normal forground color
pem:BookingType_NormalBG LONG                         !Normal background color
pem:BookingType_SelectedFG LONG                       !Selected forground color
pem:BookingType_SelectedBG LONG                       !Selected background color
pem:Status             LIKE(pem:Status)               !List box control field - type derived from field
pem:Status_NormalFG    LONG                           !Normal forground color
pem:Status_NormalBG    LONG                           !Normal background color
pem:Status_SelectedFG  LONG                           !Selected forground color
pem:Status_SelectedBG  LONG                           !Selected background color
pem:SIDJobNumber       LIKE(pem:SIDJobNumber)         !List box control field - type derived from field
pem:SIDJobNumber_NormalFG LONG                        !Normal forground color
pem:SIDJobNumber_NormalBG LONG                        !Normal background color
pem:SIDJobNumber_SelectedFG LONG                      !Selected forground color
pem:SIDJobNumber_SelectedBG LONG                      !Selected background color
pem:CustomerUTL        LIKE(pem:CustomerUTL)          !List box control field - type derived from field
pem:CustomerUTL_NormalFG LONG                         !Normal forground color
pem:CustomerUTL_NormalBG LONG                         !Normal background color
pem:CustomerUTL_SelectedFG LONG                       !Selected forground color
pem:CustomerUTL_SelectedBG LONG                       !Selected background color
pem:ErrorMessage       LIKE(pem:ErrorMessage)         !List box control field - type derived from field
pem:ErrorMessage_NormalFG LONG                        !Normal forground color
pem:ErrorMessage_NormalBG LONG                        !Normal background color
pem:ErrorMessage_SelectedFG LONG                      !Selected forground color
pem:ErrorMessage_SelectedBG LONG                      !Selected background color
pem:RecordNumber       LIKE(pem:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the SMS / Email Log'),AT(,,673,400),FONT('Tahoma',8,,),IMM,ICON('pc.ico'),HLP('Main'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,50,660,324),USE(?Browse:1),IMM,HVSCROLL,MSG('Browse Message Log'),TIP('Browse Message Log'),FORMAT('52R(2)|M*~Date Created~@d6b@28R(2)|M*~Time~@t1b@0L(2)|M*~ID~@s3@45L(2)|M*~SMS / ' &|
   'Email~@s5@79L(2)|M*~Mobile Number~@s30@178L(2)|M*~Email Address~@s255@42R(2)|M*~' &|
   'SB Job No~@s8@51R(2)|M*~Date To Send~@d6b@51R(2)|M*~Time To Send~@t1b@54R(2)|M*~' &|
   'Date Sent~@d6b@29R(2)|M*~Time~@t1b@120L(2)|M*~Booking Type~@s30@120L(2)|M*~Statu' &|
   's~@s30@60L(2)|M*~SID Job Number~@s30@32L(2)|M*~Sent To~@s8@178L(2)|M*~Error Mess' &|
   'age~@s255@'),FROM(Queue:Browse:1)
                       OPTION('Browse Filter'),AT(209,18,307,28),USE(tmp:BrowseFilter),BOXED
                         RADIO('Show All'),AT(213,31),USE(?tmp:BrowseFilter:Radio1),VALUE('0')
                         RADIO('All Sent'),AT(261,31),USE(?tmp:BrowseFilter:Radio6),VALUE('1')
                         RADIO('All Unsent'),AT(301,31),USE(?tmp:BrowseFilter:Radio2),VALUE('2')
                         RADIO('Unsent SMS'),AT(349,31),USE(?tmp:BrowseFilter:Radio3),VALUE('3')
                         RADIO('Unsent Email'),AT(405,31),USE(?tmp:BrowseFilter:Radio4),VALUE('4')
                         RADIO('All Errors'),AT(465,31),USE(?tmp:BrowseFilter:Radio5),VALUE('5')
                       END
                       SHEET,AT(4,4,668,374),USE(?CurrentTab),SPREAD
                         TAB('By Date Created'),USE(?Tab:2)
                         END
                         TAB('By SID Job Number'),USE(?Tab2)
                           ENTRY(@s30),AT(8,36,124,10),USE(pem:SIDJobNumber),MSG('SID Job Number')
                         END
                       END
                       BUTTON('Close'),AT(616,382,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?tmp:BrowseFilter{prop:Font,3} = -1
    ?tmp:BrowseFilter{prop:Color} = 15066597
    ?tmp:BrowseFilter{prop:Trn} = 0
    ?tmp:BrowseFilter:Radio1{prop:Font,3} = -1
    ?tmp:BrowseFilter:Radio1{prop:Color} = 15066597
    ?tmp:BrowseFilter:Radio1{prop:Trn} = 0
    ?tmp:BrowseFilter:Radio6{prop:Font,3} = -1
    ?tmp:BrowseFilter:Radio6{prop:Color} = 15066597
    ?tmp:BrowseFilter:Radio6{prop:Trn} = 0
    ?tmp:BrowseFilter:Radio2{prop:Font,3} = -1
    ?tmp:BrowseFilter:Radio2{prop:Color} = 15066597
    ?tmp:BrowseFilter:Radio2{prop:Trn} = 0
    ?tmp:BrowseFilter:Radio3{prop:Font,3} = -1
    ?tmp:BrowseFilter:Radio3{prop:Color} = 15066597
    ?tmp:BrowseFilter:Radio3{prop:Trn} = 0
    ?tmp:BrowseFilter:Radio4{prop:Font,3} = -1
    ?tmp:BrowseFilter:Radio4{prop:Color} = 15066597
    ?tmp:BrowseFilter:Radio4{prop:Trn} = 0
    ?tmp:BrowseFilter:Radio5{prop:Font,3} = -1
    ?tmp:BrowseFilter:Radio5{prop:Color} = 15066597
    ?tmp:BrowseFilter:Radio5{prop:Trn} = 0
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?pem:SIDJobNumber{prop:ReadOnly} = True
        ?pem:SIDJobNumber{prop:FontColor} = 65793
        ?pem:SIDJobNumber{prop:Color} = 15066597
    Elsif ?pem:SIDJobNumber{prop:Req} = True
        ?pem:SIDJobNumber{prop:FontColor} = 65793
        ?pem:SIDJobNumber{prop:Color} = 8454143
    Else ! If ?pem:SIDJobNumber{prop:Req} = True
        ?pem:SIDJobNumber{prop:FontColor} = 65793
        ?pem:SIDJobNumber{prop:Color} = 16777215
    End ! If ?pem:SIDJobNumber{prop:Req} = True
    ?pem:SIDJobNumber{prop:Trn} = 0
    ?pem:SIDJobNumber{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:PENDMAIL.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:PENDMAIL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,pem:JobNumberKey)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?pem:SIDJobNumber,pem:SIDJobNumber,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,pem:DateCreatedKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,pem:DateCreated,1,BRW1)
  BRW1.AddField(pem:DateCreated,BRW1.Q.pem:DateCreated)
  BRW1.AddField(pem:TimeCreated,BRW1.Q.pem:TimeCreated)
  BRW1.AddField(pem:MessageType,BRW1.Q.pem:MessageType)
  BRW1.AddField(pem:SMSEmail,BRW1.Q.pem:SMSEmail)
  BRW1.AddField(pem:MobileNumber,BRW1.Q.pem:MobileNumber)
  BRW1.AddField(pem:EmailAddress,BRW1.Q.pem:EmailAddress)
  BRW1.AddField(pem:RefNumber,BRW1.Q.pem:RefNumber)
  BRW1.AddField(pem:DateToSend,BRW1.Q.pem:DateToSend)
  BRW1.AddField(pem:TimeToSend,BRW1.Q.pem:TimeToSend)
  BRW1.AddField(pem:DateSent,BRW1.Q.pem:DateSent)
  BRW1.AddField(pem:TimeSent,BRW1.Q.pem:TimeSent)
  BRW1.AddField(pem:BookingType,BRW1.Q.pem:BookingType)
  BRW1.AddField(pem:Status,BRW1.Q.pem:Status)
  BRW1.AddField(pem:SIDJobNumber,BRW1.Q.pem:SIDJobNumber)
  BRW1.AddField(pem:CustomerUTL,BRW1.Q.pem:CustomerUTL)
  BRW1.AddField(pem:ErrorMessage,BRW1.Q.pem:ErrorMessage)
  BRW1.AddField(pem:RecordNumber,BRW1.Q.pem:RecordNumber)
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Date Created'
    ?Tab2{PROP:TEXT} = 'By SID Job Number'
    ?Browse:1{PROP:FORMAT} ='52R(2)|M*~Date Created~@d6b@#1#28R(2)|M*~Time~@t1b@#6#45L(2)|M*~SMS / Email~@s5@#16#79L(2)|M*~Mobile Number~@s30@#21#178L(2)|M*~Email Address~@s255@#26#42R(2)|M*~SB Job No~@s8@#31#51R(2)|M*~Date To Send~@d6b@#36#51R(2)|M*~Time To Send~@t1b@#41#54R(2)|M*~Date Sent~@d6b@#46#29R(2)|M*~Time~@t1b@#51#120L(2)|M*~Booking Type~@s30@#56#120L(2)|M*~Status~@s30@#61#60L(2)|M*~SID Job Number~@s30@#66#32L(2)|M*~Sent To~@s8@#71#178L(2)|M*~Error Message~@s255@#76#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PENDMAIL.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:BrowseFilter
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='52R(2)|M*~Date Created~@d6b@#1#28R(2)|M*~Time~@t1b@#6#45L(2)|M*~SMS / Email~@s5@#16#79L(2)|M*~Mobile Number~@s30@#21#178L(2)|M*~Email Address~@s255@#26#42R(2)|M*~SB Job No~@s8@#31#51R(2)|M*~Date To Send~@d6b@#36#51R(2)|M*~Time To Send~@t1b@#41#54R(2)|M*~Date Sent~@d6b@#46#29R(2)|M*~Time~@t1b@#51#120L(2)|M*~Booking Type~@s30@#56#120L(2)|M*~Status~@s30@#61#60L(2)|M*~SID Job Number~@s30@#66#32L(2)|M*~Sent To~@s8@#71#178L(2)|M*~Error Message~@s255@#76#'
          ?Tab:2{PROP:TEXT} = 'By Date Created'
        OF 2
          ?Browse:1{PROP:FORMAT} ='60L(2)|M*~SID Job Number~@s30@#66#52R(2)|M*~Date Created~@d6b@#1#28R(2)|M*~Time~@t1b@#6#45L(2)|M*~SMS / Email~@s5@#16#79L(2)|M*~Mobile Number~@s30@#21#178L(2)|M*~Email Address~@s255@#26#42R(2)|M*~SB Job No~@s8@#31#51R(2)|M*~Date To Send~@d6b@#36#51R(2)|M*~Time To Send~@t1b@#41#54R(2)|M*~Date Sent~@d6b@#46#29R(2)|M*~Time~@t1b@#51#120L(2)|M*~Booking Type~@s30@#56#120L(2)|M*~Status~@s30@#61#32L(2)|M*~Sent To~@s8@#71#178L(2)|M*~Error Message~@s255@#76#'
          ?Tab2{PROP:TEXT} = 'By SID Job Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.pem:DateCreated_NormalFG = -1
  SELF.Q.pem:DateCreated_NormalBG = -1
  SELF.Q.pem:DateCreated_SelectedFG = -1
  SELF.Q.pem:DateCreated_SelectedBG = -1
  SELF.Q.pem:TimeCreated_NormalFG = -1
  SELF.Q.pem:TimeCreated_NormalBG = -1
  SELF.Q.pem:TimeCreated_SelectedFG = -1
  SELF.Q.pem:TimeCreated_SelectedBG = -1
  SELF.Q.pem:MessageType_NormalFG = -1
  SELF.Q.pem:MessageType_NormalBG = -1
  SELF.Q.pem:MessageType_SelectedFG = -1
  SELF.Q.pem:MessageType_SelectedBG = -1
  SELF.Q.pem:SMSEmail_NormalFG = -1
  SELF.Q.pem:SMSEmail_NormalBG = -1
  SELF.Q.pem:SMSEmail_SelectedFG = -1
  SELF.Q.pem:SMSEmail_SelectedBG = -1
  SELF.Q.pem:MobileNumber_NormalFG = -1
  SELF.Q.pem:MobileNumber_NormalBG = -1
  SELF.Q.pem:MobileNumber_SelectedFG = -1
  SELF.Q.pem:MobileNumber_SelectedBG = -1
  SELF.Q.pem:EmailAddress_NormalFG = -1
  SELF.Q.pem:EmailAddress_NormalBG = -1
  SELF.Q.pem:EmailAddress_SelectedFG = -1
  SELF.Q.pem:EmailAddress_SelectedBG = -1
  SELF.Q.pem:RefNumber_NormalFG = -1
  SELF.Q.pem:RefNumber_NormalBG = -1
  SELF.Q.pem:RefNumber_SelectedFG = -1
  SELF.Q.pem:RefNumber_SelectedBG = -1
  SELF.Q.pem:DateToSend_NormalFG = -1
  SELF.Q.pem:DateToSend_NormalBG = -1
  SELF.Q.pem:DateToSend_SelectedFG = -1
  SELF.Q.pem:DateToSend_SelectedBG = -1
  SELF.Q.pem:TimeToSend_NormalFG = -1
  SELF.Q.pem:TimeToSend_NormalBG = -1
  SELF.Q.pem:TimeToSend_SelectedFG = -1
  SELF.Q.pem:TimeToSend_SelectedBG = -1
  SELF.Q.pem:DateSent_NormalFG = -1
  SELF.Q.pem:DateSent_NormalBG = -1
  SELF.Q.pem:DateSent_SelectedFG = -1
  SELF.Q.pem:DateSent_SelectedBG = -1
  SELF.Q.pem:TimeSent_NormalFG = -1
  SELF.Q.pem:TimeSent_NormalBG = -1
  SELF.Q.pem:TimeSent_SelectedFG = -1
  SELF.Q.pem:TimeSent_SelectedBG = -1
  SELF.Q.pem:BookingType_NormalFG = -1
  SELF.Q.pem:BookingType_NormalBG = -1
  SELF.Q.pem:BookingType_SelectedFG = -1
  SELF.Q.pem:BookingType_SelectedBG = -1
  SELF.Q.pem:Status_NormalFG = -1
  SELF.Q.pem:Status_NormalBG = -1
  SELF.Q.pem:Status_SelectedFG = -1
  SELF.Q.pem:Status_SelectedBG = -1
  SELF.Q.pem:SIDJobNumber_NormalFG = -1
  SELF.Q.pem:SIDJobNumber_NormalBG = -1
  SELF.Q.pem:SIDJobNumber_SelectedFG = -1
  SELF.Q.pem:SIDJobNumber_SelectedBG = -1
  SELF.Q.pem:CustomerUTL_NormalFG = -1
  SELF.Q.pem:CustomerUTL_NormalBG = -1
  SELF.Q.pem:CustomerUTL_SelectedFG = -1
  SELF.Q.pem:CustomerUTL_SelectedBG = -1
  SELF.Q.pem:ErrorMessage_NormalFG = -1
  SELF.Q.pem:ErrorMessage_NormalBG = -1
  SELF.Q.pem:ErrorMessage_SelectedFG = -1
  SELF.Q.pem:ErrorMessage_SelectedBG = -1
   
   
   IF (pem:DateSent <> '')
     SELF.Q.pem:DateCreated_NormalFG = 8421504
     SELF.Q.pem:DateCreated_NormalBG = 16777215
     SELF.Q.pem:DateCreated_SelectedFG = 16777215
     SELF.Q.pem:DateCreated_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:DateCreated_NormalFG = 0
     SELF.Q.pem:DateCreated_NormalBG = 16777215
     SELF.Q.pem:DateCreated_SelectedFG = 16777215
     SELF.Q.pem:DateCreated_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:TimeCreated_NormalFG = 8421504
     SELF.Q.pem:TimeCreated_NormalBG = 16777215
     SELF.Q.pem:TimeCreated_SelectedFG = 16777215
     SELF.Q.pem:TimeCreated_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:TimeCreated_NormalFG = 0
     SELF.Q.pem:TimeCreated_NormalBG = 16777215
     SELF.Q.pem:TimeCreated_SelectedFG = 16777215
     SELF.Q.pem:TimeCreated_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:MessageType_NormalFG = 8421504
     SELF.Q.pem:MessageType_NormalBG = 16777215
     SELF.Q.pem:MessageType_SelectedFG = 16777215
     SELF.Q.pem:MessageType_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:MessageType_NormalFG = 0
     SELF.Q.pem:MessageType_NormalBG = 16777215
     SELF.Q.pem:MessageType_SelectedFG = 16777215
     SELF.Q.pem:MessageType_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:SMSEmail_NormalFG = 8421504
     SELF.Q.pem:SMSEmail_NormalBG = 16777215
     SELF.Q.pem:SMSEmail_SelectedFG = 16777215
     SELF.Q.pem:SMSEmail_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:SMSEmail_NormalFG = 0
     SELF.Q.pem:SMSEmail_NormalBG = 16777215
     SELF.Q.pem:SMSEmail_SelectedFG = 16777215
     SELF.Q.pem:SMSEmail_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:MobileNumber_NormalFG = 8421504
     SELF.Q.pem:MobileNumber_NormalBG = 16777215
     SELF.Q.pem:MobileNumber_SelectedFG = 16777215
     SELF.Q.pem:MobileNumber_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:MobileNumber_NormalFG = 0
     SELF.Q.pem:MobileNumber_NormalBG = 16777215
     SELF.Q.pem:MobileNumber_SelectedFG = 16777215
     SELF.Q.pem:MobileNumber_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:EmailAddress_NormalFG = 8421504
     SELF.Q.pem:EmailAddress_NormalBG = 16777215
     SELF.Q.pem:EmailAddress_SelectedFG = 16777215
     SELF.Q.pem:EmailAddress_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:EmailAddress_NormalFG = 0
     SELF.Q.pem:EmailAddress_NormalBG = 16777215
     SELF.Q.pem:EmailAddress_SelectedFG = 16777215
     SELF.Q.pem:EmailAddress_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:RefNumber_NormalFG = 8421504
     SELF.Q.pem:RefNumber_NormalBG = 16777215
     SELF.Q.pem:RefNumber_SelectedFG = 16777215
     SELF.Q.pem:RefNumber_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:RefNumber_NormalFG = 0
     SELF.Q.pem:RefNumber_NormalBG = 16777215
     SELF.Q.pem:RefNumber_SelectedFG = 16777215
     SELF.Q.pem:RefNumber_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:DateToSend_NormalFG = 8421504
     SELF.Q.pem:DateToSend_NormalBG = 16777215
     SELF.Q.pem:DateToSend_SelectedFG = 16777215
     SELF.Q.pem:DateToSend_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:DateToSend_NormalFG = 0
     SELF.Q.pem:DateToSend_NormalBG = 16777215
     SELF.Q.pem:DateToSend_SelectedFG = 16777215
     SELF.Q.pem:DateToSend_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:TimeToSend_NormalFG = 8421504
     SELF.Q.pem:TimeToSend_NormalBG = 16777215
     SELF.Q.pem:TimeToSend_SelectedFG = 16777215
     SELF.Q.pem:TimeToSend_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:TimeToSend_NormalFG = 0
     SELF.Q.pem:TimeToSend_NormalBG = 16777215
     SELF.Q.pem:TimeToSend_SelectedFG = 16777215
     SELF.Q.pem:TimeToSend_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:DateSent_NormalFG = 8421504
     SELF.Q.pem:DateSent_NormalBG = 16777215
     SELF.Q.pem:DateSent_SelectedFG = 16777215
     SELF.Q.pem:DateSent_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:DateSent_NormalFG = 0
     SELF.Q.pem:DateSent_NormalBG = 16777215
     SELF.Q.pem:DateSent_SelectedFG = 16777215
     SELF.Q.pem:DateSent_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:TimeSent_NormalFG = 8421504
     SELF.Q.pem:TimeSent_NormalBG = 16777215
     SELF.Q.pem:TimeSent_SelectedFG = 16777215
     SELF.Q.pem:TimeSent_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:TimeSent_NormalFG = 0
     SELF.Q.pem:TimeSent_NormalBG = 16777215
     SELF.Q.pem:TimeSent_SelectedFG = 16777215
     SELF.Q.pem:TimeSent_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:BookingType_NormalFG = 8421504
     SELF.Q.pem:BookingType_NormalBG = 16777215
     SELF.Q.pem:BookingType_SelectedFG = 16777215
     SELF.Q.pem:BookingType_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:BookingType_NormalFG = 0
     SELF.Q.pem:BookingType_NormalBG = 16777215
     SELF.Q.pem:BookingType_SelectedFG = 16777215
     SELF.Q.pem:BookingType_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:Status_NormalFG = 8421504
     SELF.Q.pem:Status_NormalBG = 16777215
     SELF.Q.pem:Status_SelectedFG = 16777215
     SELF.Q.pem:Status_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:Status_NormalFG = 0
     SELF.Q.pem:Status_NormalBG = 16777215
     SELF.Q.pem:Status_SelectedFG = 16777215
     SELF.Q.pem:Status_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:SIDJobNumber_NormalFG = 8421504
     SELF.Q.pem:SIDJobNumber_NormalBG = 16777215
     SELF.Q.pem:SIDJobNumber_SelectedFG = 16777215
     SELF.Q.pem:SIDJobNumber_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:SIDJobNumber_NormalFG = 0
     SELF.Q.pem:SIDJobNumber_NormalBG = 16777215
     SELF.Q.pem:SIDJobNumber_SelectedFG = 16777215
     SELF.Q.pem:SIDJobNumber_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:CustomerUTL_NormalFG = 8421504
     SELF.Q.pem:CustomerUTL_NormalBG = 16777215
     SELF.Q.pem:CustomerUTL_SelectedFG = 16777215
     SELF.Q.pem:CustomerUTL_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:CustomerUTL_NormalFG = 0
     SELF.Q.pem:CustomerUTL_NormalBG = 16777215
     SELF.Q.pem:CustomerUTL_SelectedFG = 16777215
     SELF.Q.pem:CustomerUTL_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:ErrorMessage_NormalFG = 8421504
     SELF.Q.pem:ErrorMessage_NormalBG = 16777215
     SELF.Q.pem:ErrorMessage_SelectedFG = 16777215
     SELF.Q.pem:ErrorMessage_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:ErrorMessage_NormalFG = 0
     SELF.Q.pem:ErrorMessage_NormalBG = 16777215
     SELF.Q.pem:ErrorMessage_SelectedFG = 16777215
     SELF.Q.pem:ErrorMessage_SelectedBG = 0
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  Case tmp:BrowseFilter
  Of 0 !Show All
  Of 1 !ALl Sent
      If pem:DateSent = ''
          Return Record:Filtered
      End ! If pem:DateSent = ''
  Of 2 !All Unsent
      If pem:DateSent <> ''
          Return Record:Filtered
      End ! If pem:DateSent <> ''
  Of 3 !Unsent SMS
      If pem:SMSEmail = 'EMAIL'
          Return Record:Filtered
      Else ! If pem:SMSEmail = 'EMAIL'
          If pem:DateSent <> ''
              Return Record:Filtered
          End ! If pem:DateSent <> ''
      End ! If pem:SMSEmail = 'EMAIL'
  Of 4 !Unsent Email
      If pem:SMSEmail = 'SMS'
          Return Record:Filtered
      Else ! If pem:SMSEmail = 'SMS'
          If pem:DateSent <> ''
              Return Record:Filtered
          End ! If pem:DateSent <> ''
      End ! If pem:SMSEmail = 'SMS'
  Of 5 !All Errors - GK 10/09/2009 - TRK 11032, have re-enabled this as webalert now writes errors to the ErrorMessage field.
      If pem:ErrorMessage = ''
          Return Record:Filtered
      End ! If pem:ErrorMessage <> ''
  End ! Case tmp:BrowseFilter
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom)
  SELF.SetStrategy(?tmp:BrowseFilter, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?pem:SIDJobNumber, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?tmp:BrowseFilter:Radio1, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?tmp:BrowseFilter:Radio6, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?tmp:BrowseFilter:Radio2, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?tmp:BrowseFilter:Radio3, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?tmp:BrowseFilter:Radio4, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?tmp:BrowseFilter:Radio5, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

