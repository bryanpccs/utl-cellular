

   MEMBER('webalert.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBAL010.INC'),ONCE        !Local module procedure declarations
                     END


IsMobileNoExcluded   PROCEDURE  (mobileNo)            ! Declare Procedure
  CODE
    ! Is the mobile number to be excluded?

    ! Exclude the following :-
    ! 07000000000 - 07000000010
    ! 07100000000 - 07100000010
    ! 07200000000 - 07200000010
    ! 07300000000 - 07300000010
    ! 07400000000 - 07400000010
    ! 07500000000 - 07500000010
    ! 07600000000 - 07600000010
    ! 07700000000 - 07700000010
    ! 07800000000 - 07800000010
    ! 07900000000 - 07900000010

    if len(clip(mobileNo)) <> 11 then return false. ! 11 characters in length
    if mobileNo[1:2] <> '07' then return false. ! Starts 07

    if clip(mobileNo) = '07901009151' then return true.
    if clip(mobileNo) = '07968280916' then return true.

    if not numeric(mobileNo[3]) then return false. ! 0-9
    if mobileNo[3] = '-' or mobileNo[3] = '.' then return false.  ! Numeric includes - and .

    if mobileNo[4:9] <> '000000' then return false.

    if mobileNo[10:11] <> '10'
        if mobileNo[10] <> '0' then return false.
        if not numeric(mobileNo[11]) then return false.
        if mobileNo[11] = '-' or mobileNo[11] = '.' then return false.  ! Numeric includes - and .
    end

    return true
