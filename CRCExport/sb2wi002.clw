

   MEMBER('sb2wilfred.clw')                           ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SB2WI002.INC'),ONCE        !Local module procedure declarations
                     END


Dummy PROCEDURE                                       !Generated from procedure template - Window

window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Dummy')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:AUDIT_ALIAS.Open
  Relate:CRCDEFS.Open
  Relate:DEFAULTS.Open
  Relate:DEFEMAI2.Open
  Relate:DESBATCH.Open
  Relate:EXCHANGE.Open
  Relate:IMEISHIP.Open
  Relate:PENDMAIL.Open
  Relate:PENDMAIL_ALIAS.Open
  Relate:SIDALDEF.Open
  Relate:SIDALERT.Open
  Relate:SIDAUD.Open
  Relate:SIDDEF.Open
  Relate:SIDEXUPD.Open
  Relate:SIDMODSC.Open
  Relate:STAHEAD.Open
  Relate:XMLSAM.Open
  Relate:XMLTRADE.Open
  Access:JOBS.UseFile
  Access:JOBACC.UseFile
  Access:JOBSE.UseFile
  Access:USERS.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBNOTES.UseFile
  Access:SIDSRVCN.UseFile
  Access:CARISMA.UseFile
  Access:SIDNWDAY.UseFile
  Access:SMOALERT.UseFile
  Access:SMOREMIN.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:AUDIT_ALIAS.Close
    Relate:CRCDEFS.Close
    Relate:DEFAULTS.Close
    Relate:DEFEMAI2.Close
    Relate:DESBATCH.Close
    Relate:EXCHANGE.Close
    Relate:IMEISHIP.Close
    Relate:PENDMAIL.Close
    Relate:PENDMAIL_ALIAS.Close
    Relate:SIDALDEF.Close
    Relate:SIDALERT.Close
    Relate:SIDAUD.Close
    Relate:SIDDEF.Close
    Relate:SIDEXUPD.Close
    Relate:SIDMODSC.Close
    Relate:STAHEAD.Close
    Relate:XMLSAM.Close
    Relate:XMLTRADE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

