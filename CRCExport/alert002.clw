

   MEMBER('alertviewer.clw')                          ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('ALERT002.INC'),ONCE        !Local module procedure declarations
                     END



Main PROCEDURE                                        !Generated from procedure template - Browse

CurrentTab           STRING(80)
tmp:BrowseFilter     BYTE
BRW1::View:Browse    VIEW(PENDMAIL)
                       PROJECT(pem:DateCreated)
                       PROJECT(pem:TimeCreated)
                       PROJECT(pem:MessageType)
                       PROJECT(pem:SMSEmail)
                       PROJECT(pem:MobileNumber)
                       PROJECT(pem:EmailAddress)
                       PROJECT(pem:RefNumber)
                       PROJECT(pem:DateToSend)
                       PROJECT(pem:TimeToSend)
                       PROJECT(pem:DateSent)
                       PROJECT(pem:TimeSent)
                       PROJECT(pem:ErrorMessage)
                       PROJECT(pem:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
pem:DateCreated        LIKE(pem:DateCreated)          !List box control field - type derived from field
pem:DateCreated_NormalFG LONG                         !Normal forground color
pem:DateCreated_NormalBG LONG                         !Normal background color
pem:DateCreated_SelectedFG LONG                       !Selected forground color
pem:DateCreated_SelectedBG LONG                       !Selected background color
pem:TimeCreated        LIKE(pem:TimeCreated)          !List box control field - type derived from field
pem:TimeCreated_NormalFG LONG                         !Normal forground color
pem:TimeCreated_NormalBG LONG                         !Normal background color
pem:TimeCreated_SelectedFG LONG                       !Selected forground color
pem:TimeCreated_SelectedBG LONG                       !Selected background color
pem:MessageType        LIKE(pem:MessageType)          !List box control field - type derived from field
pem:MessageType_NormalFG LONG                         !Normal forground color
pem:MessageType_NormalBG LONG                         !Normal background color
pem:MessageType_SelectedFG LONG                       !Selected forground color
pem:MessageType_SelectedBG LONG                       !Selected background color
pem:SMSEmail           LIKE(pem:SMSEmail)             !List box control field - type derived from field
pem:SMSEmail_NormalFG  LONG                           !Normal forground color
pem:SMSEmail_NormalBG  LONG                           !Normal background color
pem:SMSEmail_SelectedFG LONG                          !Selected forground color
pem:SMSEmail_SelectedBG LONG                          !Selected background color
pem:MobileNumber       LIKE(pem:MobileNumber)         !List box control field - type derived from field
pem:MobileNumber_NormalFG LONG                        !Normal forground color
pem:MobileNumber_NormalBG LONG                        !Normal background color
pem:MobileNumber_SelectedFG LONG                      !Selected forground color
pem:MobileNumber_SelectedBG LONG                      !Selected background color
pem:EmailAddress       LIKE(pem:EmailAddress)         !List box control field - type derived from field
pem:EmailAddress_NormalFG LONG                        !Normal forground color
pem:EmailAddress_NormalBG LONG                        !Normal background color
pem:EmailAddress_SelectedFG LONG                      !Selected forground color
pem:EmailAddress_SelectedBG LONG                      !Selected background color
pem:RefNumber          LIKE(pem:RefNumber)            !List box control field - type derived from field
pem:RefNumber_NormalFG LONG                           !Normal forground color
pem:RefNumber_NormalBG LONG                           !Normal background color
pem:RefNumber_SelectedFG LONG                         !Selected forground color
pem:RefNumber_SelectedBG LONG                         !Selected background color
pem:DateToSend         LIKE(pem:DateToSend)           !List box control field - type derived from field
pem:DateToSend_NormalFG LONG                          !Normal forground color
pem:DateToSend_NormalBG LONG                          !Normal background color
pem:DateToSend_SelectedFG LONG                        !Selected forground color
pem:DateToSend_SelectedBG LONG                        !Selected background color
pem:TimeToSend         LIKE(pem:TimeToSend)           !List box control field - type derived from field
pem:TimeToSend_NormalFG LONG                          !Normal forground color
pem:TimeToSend_NormalBG LONG                          !Normal background color
pem:TimeToSend_SelectedFG LONG                        !Selected forground color
pem:TimeToSend_SelectedBG LONG                        !Selected background color
pem:DateSent           LIKE(pem:DateSent)             !List box control field - type derived from field
pem:DateSent_NormalFG  LONG                           !Normal forground color
pem:DateSent_NormalBG  LONG                           !Normal background color
pem:DateSent_SelectedFG LONG                          !Selected forground color
pem:DateSent_SelectedBG LONG                          !Selected background color
pem:TimeSent           LIKE(pem:TimeSent)             !List box control field - type derived from field
pem:TimeSent_NormalFG  LONG                           !Normal forground color
pem:TimeSent_NormalBG  LONG                           !Normal background color
pem:TimeSent_SelectedFG LONG                          !Selected forground color
pem:TimeSent_SelectedBG LONG                          !Selected background color
pem:ErrorMessage       LIKE(pem:ErrorMessage)         !List box control field - type derived from field
pem:ErrorMessage_NormalFG LONG                        !Normal forground color
pem:ErrorMessage_NormalBG LONG                        !Normal background color
pem:ErrorMessage_SelectedFG LONG                      !Selected forground color
pem:ErrorMessage_SelectedBG LONG                      !Selected background color
pem:ErrorMessage_Tip   STRING(80)                     !Field tooltip
pem:RecordNumber       LIKE(pem:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Browse  WinType = Browse
mo:SelectedField  Long
QuickWindow          WINDOW('Browse the SMS / Email Log'),AT(,,673,400),FONT('Tahoma',8,,),IMM,ICON('pc.ico'),HLP('Main'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,50,660,324),USE(?Browse:1),IMM,HVSCROLL,MSG('Browse Message Log'),TIP('Browse Message Log'),FORMAT('52R(2)|M*~Date Created~@d6b@28R(2)|M*~Time~@t1b@22L(2)|M*~ID~@s3@45L(2)|M*~SMS /' &|
         ' Email~@s5@79L(2)|M*~Mobile Number~@s30@178L(2)|M*~Email Address~@s255@42R(2)|M*' &|
         '~SB Job No~@s8@51R(2)|M*~Date To Send~@d6b@51R(2)|M*~Time To Send~@t1b@54R(2)|M*' &|
         '~Date Sent~@d6b@29R(2)|M*~Time~@t1b@0L(2)|M*P~Error Message~@s255@'),FROM(Queue:Browse:1)
                       SHEET,AT(4,4,668,374),USE(?CurrentTab),SPREAD
                         TAB('SMS / Mail Log'),USE(?Tab:2)
                           OPTION('Browse Filter'),AT(12,18,256,28),USE(tmp:BrowseFilter),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI)
                             RADIO('Show All'),AT(16,30),USE(?tmp:BrowseFilter:Radio1),FONT(,,,,CHARSET:ANSI),VALUE('0')
                             RADIO('All Sent'),AT(64,30),USE(?tmp:BrowseFilter:Radio6),FONT(,,,,CHARSET:ANSI),VALUE('1')
                             RADIO('All Unsent'),AT(104,30),USE(?tmp:BrowseFilter:Radio2),FONT(,,,,CHARSET:ANSI),VALUE('2')
                             RADIO('Unsent SMS'),AT(152,30),USE(?tmp:BrowseFilter:Radio3),FONT(,,,,CHARSET:ANSI),VALUE('3')
                             RADIO('Unsent Email'),AT(208,30),USE(?tmp:BrowseFilter:Radio4),FONT(,,,,CHARSET:ANSI),VALUE('4')
                             RADIO('All Errors'),AT(268,30),USE(?tmp:BrowseFilter:Radio5),HIDE,FONT(,,,,CHARSET:ANSI),VALUE('5')
                           END
                         END
                       END
                       BUTTON('Close'),AT(616,382,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:PENDMAIL.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:PENDMAIL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ?Browse:1{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,pem:DateCreatedKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,pem:DateCreated,1,BRW1)
  BRW1.AddField(pem:DateCreated,BRW1.Q.pem:DateCreated)
  BRW1.AddField(pem:TimeCreated,BRW1.Q.pem:TimeCreated)
  BRW1.AddField(pem:MessageType,BRW1.Q.pem:MessageType)
  BRW1.AddField(pem:SMSEmail,BRW1.Q.pem:SMSEmail)
  BRW1.AddField(pem:MobileNumber,BRW1.Q.pem:MobileNumber)
  BRW1.AddField(pem:EmailAddress,BRW1.Q.pem:EmailAddress)
  BRW1.AddField(pem:RefNumber,BRW1.Q.pem:RefNumber)
  BRW1.AddField(pem:DateToSend,BRW1.Q.pem:DateToSend)
  BRW1.AddField(pem:TimeToSend,BRW1.Q.pem:TimeToSend)
  BRW1.AddField(pem:DateSent,BRW1.Q.pem:DateSent)
  BRW1.AddField(pem:TimeSent,BRW1.Q.pem:TimeSent)
  BRW1.AddField(pem:ErrorMessage,BRW1.Q.pem:ErrorMessage)
  BRW1.AddField(pem:RecordNumber,BRW1.Q.pem:RecordNumber)
  ThisMakeover.SetWindow(Win:Browse)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Browse,mo:SelectedTab,?CurrentTab)
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PENDMAIL.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:BrowseFilter
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisMakeover.TakeEvent(Win:Browse,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Browse,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      BRW1.ResetQueue(Reset:Queue)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.pem:DateCreated_NormalFG = -1
  SELF.Q.pem:DateCreated_NormalBG = -1
  SELF.Q.pem:DateCreated_SelectedFG = -1
  SELF.Q.pem:DateCreated_SelectedBG = -1
  SELF.Q.pem:TimeCreated_NormalFG = -1
  SELF.Q.pem:TimeCreated_NormalBG = -1
  SELF.Q.pem:TimeCreated_SelectedFG = -1
  SELF.Q.pem:TimeCreated_SelectedBG = -1
  SELF.Q.pem:MessageType_NormalFG = -1
  SELF.Q.pem:MessageType_NormalBG = -1
  SELF.Q.pem:MessageType_SelectedFG = -1
  SELF.Q.pem:MessageType_SelectedBG = -1
  SELF.Q.pem:SMSEmail_NormalFG = -1
  SELF.Q.pem:SMSEmail_NormalBG = -1
  SELF.Q.pem:SMSEmail_SelectedFG = -1
  SELF.Q.pem:SMSEmail_SelectedBG = -1
  SELF.Q.pem:MobileNumber_NormalFG = -1
  SELF.Q.pem:MobileNumber_NormalBG = -1
  SELF.Q.pem:MobileNumber_SelectedFG = -1
  SELF.Q.pem:MobileNumber_SelectedBG = -1
  SELF.Q.pem:EmailAddress_NormalFG = -1
  SELF.Q.pem:EmailAddress_NormalBG = -1
  SELF.Q.pem:EmailAddress_SelectedFG = -1
  SELF.Q.pem:EmailAddress_SelectedBG = -1
  SELF.Q.pem:RefNumber_NormalFG = -1
  SELF.Q.pem:RefNumber_NormalBG = -1
  SELF.Q.pem:RefNumber_SelectedFG = -1
  SELF.Q.pem:RefNumber_SelectedBG = -1
  SELF.Q.pem:DateToSend_NormalFG = -1
  SELF.Q.pem:DateToSend_NormalBG = -1
  SELF.Q.pem:DateToSend_SelectedFG = -1
  SELF.Q.pem:DateToSend_SelectedBG = -1
  SELF.Q.pem:TimeToSend_NormalFG = -1
  SELF.Q.pem:TimeToSend_NormalBG = -1
  SELF.Q.pem:TimeToSend_SelectedFG = -1
  SELF.Q.pem:TimeToSend_SelectedBG = -1
  SELF.Q.pem:DateSent_NormalFG = -1
  SELF.Q.pem:DateSent_NormalBG = -1
  SELF.Q.pem:DateSent_SelectedFG = -1
  SELF.Q.pem:DateSent_SelectedBG = -1
  SELF.Q.pem:TimeSent_NormalFG = -1
  SELF.Q.pem:TimeSent_NormalBG = -1
  SELF.Q.pem:TimeSent_SelectedFG = -1
  SELF.Q.pem:TimeSent_SelectedBG = -1
  SELF.Q.pem:ErrorMessage_NormalFG = -1
  SELF.Q.pem:ErrorMessage_NormalBG = -1
  SELF.Q.pem:ErrorMessage_SelectedFG = -1
  SELF.Q.pem:ErrorMessage_SelectedBG = -1
  CLEAR (SELF.Q.pem:ErrorMessage_Tip)
   
   
   IF (pem:DateSent <> '')
     SELF.Q.pem:DateCreated_NormalFG = 8421504
     SELF.Q.pem:DateCreated_NormalBG = 16777215
     SELF.Q.pem:DateCreated_SelectedFG = 16777215
     SELF.Q.pem:DateCreated_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:DateCreated_NormalFG = 0
     SELF.Q.pem:DateCreated_NormalBG = 16777215
     SELF.Q.pem:DateCreated_SelectedFG = 16777215
     SELF.Q.pem:DateCreated_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:TimeCreated_NormalFG = 8421504
     SELF.Q.pem:TimeCreated_NormalBG = 16777215
     SELF.Q.pem:TimeCreated_SelectedFG = 16777215
     SELF.Q.pem:TimeCreated_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:TimeCreated_NormalFG = 0
     SELF.Q.pem:TimeCreated_NormalBG = 16777215
     SELF.Q.pem:TimeCreated_SelectedFG = 16777215
     SELF.Q.pem:TimeCreated_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:MessageType_NormalFG = 8421504
     SELF.Q.pem:MessageType_NormalBG = 16777215
     SELF.Q.pem:MessageType_SelectedFG = 16777215
     SELF.Q.pem:MessageType_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:MessageType_NormalFG = 0
     SELF.Q.pem:MessageType_NormalBG = 16777215
     SELF.Q.pem:MessageType_SelectedFG = 16777215
     SELF.Q.pem:MessageType_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:SMSEmail_NormalFG = 8421504
     SELF.Q.pem:SMSEmail_NormalBG = 16777215
     SELF.Q.pem:SMSEmail_SelectedFG = 16777215
     SELF.Q.pem:SMSEmail_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:SMSEmail_NormalFG = 0
     SELF.Q.pem:SMSEmail_NormalBG = 16777215
     SELF.Q.pem:SMSEmail_SelectedFG = 16777215
     SELF.Q.pem:SMSEmail_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:MobileNumber_NormalFG = 8421504
     SELF.Q.pem:MobileNumber_NormalBG = 16777215
     SELF.Q.pem:MobileNumber_SelectedFG = 16777215
     SELF.Q.pem:MobileNumber_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:MobileNumber_NormalFG = 0
     SELF.Q.pem:MobileNumber_NormalBG = 16777215
     SELF.Q.pem:MobileNumber_SelectedFG = 16777215
     SELF.Q.pem:MobileNumber_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:EmailAddress_NormalFG = 8421504
     SELF.Q.pem:EmailAddress_NormalBG = 16777215
     SELF.Q.pem:EmailAddress_SelectedFG = 16777215
     SELF.Q.pem:EmailAddress_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:EmailAddress_NormalFG = 0
     SELF.Q.pem:EmailAddress_NormalBG = 16777215
     SELF.Q.pem:EmailAddress_SelectedFG = 16777215
     SELF.Q.pem:EmailAddress_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:RefNumber_NormalFG = 8421504
     SELF.Q.pem:RefNumber_NormalBG = 16777215
     SELF.Q.pem:RefNumber_SelectedFG = 16777215
     SELF.Q.pem:RefNumber_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:RefNumber_NormalFG = 0
     SELF.Q.pem:RefNumber_NormalBG = 16777215
     SELF.Q.pem:RefNumber_SelectedFG = 16777215
     SELF.Q.pem:RefNumber_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:DateToSend_NormalFG = 8421504
     SELF.Q.pem:DateToSend_NormalBG = 16777215
     SELF.Q.pem:DateToSend_SelectedFG = 16777215
     SELF.Q.pem:DateToSend_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:DateToSend_NormalFG = 0
     SELF.Q.pem:DateToSend_NormalBG = 16777215
     SELF.Q.pem:DateToSend_SelectedFG = 16777215
     SELF.Q.pem:DateToSend_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:TimeToSend_NormalFG = 8421504
     SELF.Q.pem:TimeToSend_NormalBG = 16777215
     SELF.Q.pem:TimeToSend_SelectedFG = 16777215
     SELF.Q.pem:TimeToSend_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:TimeToSend_NormalFG = 0
     SELF.Q.pem:TimeToSend_NormalBG = 16777215
     SELF.Q.pem:TimeToSend_SelectedFG = 16777215
     SELF.Q.pem:TimeToSend_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:DateSent_NormalFG = 8421504
     SELF.Q.pem:DateSent_NormalBG = 16777215
     SELF.Q.pem:DateSent_SelectedFG = 16777215
     SELF.Q.pem:DateSent_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:DateSent_NormalFG = 0
     SELF.Q.pem:DateSent_NormalBG = 16777215
     SELF.Q.pem:DateSent_SelectedFG = 16777215
     SELF.Q.pem:DateSent_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:TimeSent_NormalFG = 8421504
     SELF.Q.pem:TimeSent_NormalBG = 16777215
     SELF.Q.pem:TimeSent_SelectedFG = 16777215
     SELF.Q.pem:TimeSent_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:TimeSent_NormalFG = 0
     SELF.Q.pem:TimeSent_NormalBG = 16777215
     SELF.Q.pem:TimeSent_SelectedFG = 16777215
     SELF.Q.pem:TimeSent_SelectedBG = 0
   END
   IF (pem:DateSent <> '')
     SELF.Q.pem:ErrorMessage_NormalFG = 8421504
     SELF.Q.pem:ErrorMessage_NormalBG = 16777215
     SELF.Q.pem:ErrorMessage_SelectedFG = 16777215
     SELF.Q.pem:ErrorMessage_SelectedBG = 8421504
   ELSE
     SELF.Q.pem:ErrorMessage_NormalFG = 0
     SELF.Q.pem:ErrorMessage_NormalBG = 16777215
     SELF.Q.pem:ErrorMessage_SelectedFG = 16777215
     SELF.Q.pem:ErrorMessage_SelectedBG = 0
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  Case tmp:BrowseFilter
  Of 0 !Show All
  Of 1 !ALl Sent
      If pem:DateSent = ''
          Return Record:Filtered
      End ! If pem:DateSent = ''
  Of 2 !All Unsent
      If pem:DateSent <> ''
          Return Record:Filtered
      End ! If pem:DateSent <> ''
  Of 3 !Unsent SMS
      If pem:SMSEmail = 'EMAIL'
          Return Record:Filtered
      Else ! If pem:SMSEmail = 'EMAIL'
          If pem:DateSent <> ''
              Return Record:Filtered
          End ! If pem:DateSent <> ''
      End ! If pem:SMSEmail = 'EMAIL'
  Of 4 !Unsent Email
      If pem:SMSEmail = 'SMS'
          Return Record:Filtered
      Else ! If pem:SMSEmail = 'SMS'
          If pem:DateSent <> ''
              Return Record:Filtered
          End ! If pem:DateSent <> ''
      End ! If pem:SMSEmail = 'SMS'
  Of 5 !All Errors
      If pem:ErrorMessage <> ''
          Return Record:Filtered
      End ! If pem:ErrorMessage <> ''
  End ! Case tmp:BrowseFilter
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue

