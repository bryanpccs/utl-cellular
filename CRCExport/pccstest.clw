   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE

!* * * * Line Print Template Generated Code * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

OFSTRUCT    GROUP,TYPE                                                                  
cBytes       BYTE                                                                       ! Specifies the length, in bytes, of the structure
cFixedDisk   BYTE                                                                       ! Specifies whether the file is on a hard (fixed) disk. This member is nonzero if the file is on a hard disk
nErrCode     SIGNED                                                                     ! Specifies the MS-DOS error code if the OpenFile function failed
Reserved1    SIGNED                                                                     ! Reserved, Don't use
Reserved2    SIGNED                                                                     ! Reserved, Don't use
szPathName   BYTE,DIM(128)                                                              ! Specifies the path and filename of the file
            END

LF                  CSTRING(CHR(10))                                                    ! Line Feed
FF                  CSTRING(CHR(12))                                                    ! Form Feed
CR                  CSTRING(CHR(13))                                                    ! Carriage Return

lpszFilename        CSTRING(144)                                                        ! File Name
fnAttribute         SIGNED                                                              ! Attribute
hf                  SIGNED                                                              ! File Handle
hpvBuffer           STRING(5000)                                                        ! String To Write
cbBuffer            SIGNED                                                              ! Characters to Write
BytesWritten        SIGNED                                                              ! Characters Written

Succeeded           EQUATE(0)                                                           ! Function Succeeded
OpenError           EQUATE(1)                                                           ! Can Not Open File or Device
WriteError          EQUATE(2)                                                           ! Can not Write to File or Device
CloseError          EQUATE(3)                                                           ! Can not Close File or Device
SeekError           EQUATE(4)                                                           ! Can not Seek File or Device

ATTR_Normal         EQUATE(0)                                                           ! File Attribute Normal
ATTR_ReadOnly       EQUATE(1)                                                           ! File Attribute Read Only
ATTR_Hidden         EQUATE(2)                                                           ! File Attribute Hidden
ATTR_System         EQUATE(3)                                                           ! File Attribute System

OF_READ             EQUATE(0000h)                                                       ! Opens the file for reading only
OF_WRITE            EQUATE(0001h)                                                       ! Opens the file for writing only
OF_READWRITE        EQUATE(0002h)                                                       ! Opens the file for reading and writing
OF_SHARE_COMPAT     EQUATE(0000h)                                                       ! Opens the file with compatibility mode, allowing any program on a given machine to open the file any number of times.
OF_SHARE_EXCLUSIVE  EQUATE(0010h)                                                       ! Opens the file with exclusive mode, denying other programs both read and write access to the file
OF_SHARE_DENY_WRITE EQUATE(0020h)                                                       ! Opens the file and denies other programs write access to the file
OF_SHARE_DENY_READ  EQUATE(0030h)                                                       ! Opens the file and denies other programs read access to the file
OF_SHARE_DENY_NONE  EQUATE(0040h)                                                       ! Opens the file without denying other programs read or write access to the file
OF_PARSE            EQUATE(0100h)                                                       ! Fills the OFSTRUCT structure but carries out no other action
OF_DELETE           EQUATE(0200h)                                                       ! Deletes the file
OF_VERIFY           EQUATE(0400h)                                                       ! Compares the time and date in the OF_STRUCT with the time and date of the specified file
OF_SEARCH           EQUATE(0400h)                                                       ! Windows searches in directories even when the file name includes a full path
OF_CANCEL           EQUATE(0800h)                                                       ! Adds a Cancel button to the OF_PROMPT dialog box. Pressing the Cancel button directs OpenFile to return a file-not-found error message
OF_CREATE           EQUATE(1000h)                                                       ! Creates a new file. If the file already exists, it is truncated to zero length
OF_PROMPT           EQUATE(2000h)                                                       ! Displays a dialog box if the requested file does not exist.
OF_EXIST            EQUATE(4000h)                                                       ! Opens the file, and then closes it. This value is used to test for file existence
OF_REOPEN           EQUATE(8000h)                                                       ! Opens the file using information in the reopen buffer

OF_STRUCT           LIKE(OFSTRUCT)                                                      ! Will Be loaded with File information. See OFSTRUCT above

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('PCCSTBC.CLW')
DctInit     PROCEDURE
DctKill     PROCEDURE
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('PCCST001.CLW')
Main                   PROCEDURE   !
     END
     
     !* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
          LinePrint(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>),BYTE,PROC          ! Declare LinePrint Function
          DeleteFile(STRING FileToDelete),BYTE,PROC                                          ! Declare DeleteFile Function
            MODULE('')                                                                       ! MODULE Start
             OpenFile(*CSTRING,*OFSTRUCT,SIGNED),SIGNED,PASCAL,RAW                           ! Open/Create/Delete File
             _llseek(SIGNED,LONG,SIGNED),LONG,PASCAL                                         ! Control File Pointer
             _lwrite(SIGNED,*STRING,UNSIGNED),UNSIGNED,PASCAL,RAW                           ! Write to File
             _lclose(SIGNED),SIGNED,PASCAL                                                   ! Close File
            END                                                                              ! Terminate Module
     
     !* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
   END

strPath            STRING(260)
SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

DEFAULTS             FILE,DRIVER('Btrieve'),NAME('DEFAULTS.DAT'),PRE(def),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(def:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Name                   STRING(30)
record_number               REAL
Version_Number              STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
VAT_Number                  STRING(30)
Use_Invoice_Address         STRING(3)
Use_For_Order               STRING(3)
Invoice_Company_Name        STRING(30)
Invoice_Address_Line1       STRING(30)
Invoice_Address_Line2       STRING(30)
Invoice_Address_Line3       STRING(30)
Invoice_Postcode            STRING(15)
Invoice_Telephone_Number    STRING(15)
Invoice_Fax_Number          STRING(15)
InvoiceEmailAddress         STRING(255)
Invoice_VAT_Number          STRING(30)
OrderCompanyName            STRING(30)
OrderAddressLine1           STRING(30)
OrderAddressLine2           STRING(30)
OrderAddressLine3           STRING(30)
OrderPostcode               STRING(30)
OrderTelephoneNumber        STRING(30)
OrderFaxNumber              STRING(30)
OrderEmailAddress           STRING(255)
Use_Postcode                STRING(3)
Postcode_Path               STRING(255)
PostcodeDll                 STRING(3)
Vat_Rate_Labour             STRING(2)
Vat_Rate_Parts              STRING(2)
License_Number              STRING(10)
Maximum_Users               REAL
Warranty_Period             REAL
Automatic_Replicate         STRING(3)
Automatic_Replicate_Field   STRING(15)
Estimate_If_Over            REAL
Start_Work_Hours            TIME
End_Work_Hours              TIME
Include_Saturday            STRING(3)
Include_Sunday              STRING(3)
Show_Mobile_Number          STRING(3)
Show_Loan_Exchange_Details  STRING(3)
Use_Credit_Limit            STRING(3)
Hide_Physical_Damage        STRING(3)
Hide_Insurance              STRING(3)
Hide_Authority_Number       STRING(3)
Hide_Advance_Payment        STRING(3)
HideColour                  STRING(3)
HideInCourier               STRING(3)
HideIntFault                STRING(3)
HideProduct                 STRING(3)
HideLocation                BYTE
Allow_Bouncer               STRING(3)
Job_Batch_Number            REAL
QA_Required                 STRING(3)
QA_Before_Complete          STRING(3)
QAPreliminary               STRING(3)
QAExchLoan                  STRING(3)
CompleteAtQA                BYTE
RapidQA                     STRING(3)
ValidateESN                 STRING(3)
ValidateDesp                STRING(3)
Force_Accessory_Check       STRING(3)
Force_Initial_Transit_Type  STRING(1)
Force_Mobile_Number         STRING(1)
Force_Model_Number          STRING(1)
Force_Unit_Type             STRING(1)
Force_Fault_Description     STRING(1)
Force_ESN                   STRING(1)
Force_MSN                   STRING(1)
Force_Job_Type              STRING(1)
Force_Engineer              STRING(1)
Force_Invoice_Text          STRING(1)
Force_Repair_Type           STRING(1)
Force_Authority_Number      STRING(1)
Force_Fault_Coding          STRING(1)
Force_Spares                STRING(1)
Force_Incoming_Courier      STRING(1)
Force_Outoing_Courier       STRING(1)
Force_DOP                   STRING(1)
Order_Number                STRING(1)
Customer_Name               STRING(1)
ForcePostcode               STRING(1)
ForceDelPostcode            STRING(1)
ForceCommonFault            STRING(1)
Remove_Backgrounds          STRING(3)
Use_Loan_Exchange_Label     STRING(3)
Use_Job_Label               STRING(3)
UseSmallLabel               BYTE
Job_Label                   STRING(255)
add_stock_label             STRING(3)
receive_stock_label         STRING(3)
QA_Failed_Label             STRING(3)
QA_Failed_Report            STRING(3)
QAPassLabel                 BYTE
ThirdPartyNote              STRING(3)
Vodafone_Import_Path        STRING(255)
Label_Printer_Type          STRING(30)
Browse_Option               STRING(3)
ANCCollectionNo             LONG
Use_Sage                    STRING(3)
Global_Nominal_Code         STRING(30)
Parts_Stock_Code            STRING(30)
Labour_Stock_Code           STRING(30)
Courier_Stock_Code          STRING(30)
Parts_Description           STRING(60)
Labour_Description          STRING(60)
Courier_Description         STRING(60)
Parts_Code                  STRING(30)
Labour_Code                 STRING(30)
Courier_Code                STRING(30)
User_Name_Sage              STRING(30)
Password_Sage               STRING(30)
Company_Number_Sage         STRING(30)
Path_Sage                   STRING(255)
BouncerTime                 LONG
ExportPath                  STRING(255)
PrintBarcode                BYTE
ChaChargeType               STRING(30)
WarChargeType               STRING(30)
RetBackOrders               BYTE
RemoveWorkshopDespatch      BYTE
SummaryOrders               BYTE
EuroRate                    REAL
IrishVatRate                REAL
EmailServerAddress          STRING(255)
EmailServerPort             LONG
Job_Label_Accessories       STRING(3)
ShowRepTypeCosts            BYTE
PickNoteNormal              BYTE
PickNoteMainStore           BYTE
PickNoteChangeStatus        BYTE
PickNoteJobStatus           STRING(30)
TeamPerformanceTicker       BYTE
TickerRefreshRate           SHORT
UseOriginalBookingTemplate  BYTE
                         END
                     END                       



glo:ErrorText        STRING(1000)
glo:owner            STRING(20)
glo:DateModify       DATE
glo:TimeModify       TIME
glo:Notes_Global     STRING(1000)
glo:Q_Invoice        QUEUE,PRE(glo)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255)
glo:Password         STRING(20)
glo:PassAccount      STRING(30)
glo:Preview          STRING('True')
glo:q_JobNumber      QUEUE,PRE(GLO)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(glo)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3)
glo:TimeLogged       TIME
glo:GLO:ApplicationName STRING(8)
glo:GLO:ApplicationCreationDate LONG
glo:GLO:ApplicationCreationTime LONG
glo:GLO:ApplicationChangeDate LONG
glo:GLO:ApplicationChangeTime LONG
glo:GLO:Compiled32   BYTE
glo:GLO:AppINIFile   STRING(255)
glo:Queue            QUEUE,PRE(GLO)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO)
Pointer20              STRING(40)
                     END
glo:FaultCode1       STRING(30)
glo:FaultCode2       STRING(30)
glo:FaultCode3       STRING(30)
glo:FaultCode4       STRING(30)
glo:FaultCode5       STRING(30)
glo:FaultCode6       STRING(30)
glo:FaultCode7       STRING(30)
glo:FaultCode8       STRING(30)
glo:FaultCode9       STRING(30)
glo:FaultCode10      STRING(255)
glo:FaultCode11      STRING(255)
glo:FaultCode12      STRING(255)
glo:FaultCode13      STRING(30)
glo:FaultCode14      STRING(30)
glo:FaultCode15      STRING(30)
glo:FaultCode16      STRING(30)
glo:FaultCode17      STRING(30)
glo:FaultCode18      STRING(30)
glo:FaultCode19      STRING(30)
glo:FaultCode20      STRING(30)
glo:TradeFaultCode1  STRING(30)
glo:TradeFaultCode2  STRING(30)
glo:TradeFaultCode3  STRING(30)
glo:TradeFaultCode4  STRING(30)
glo:TradeFaultCode5  STRING(30)
glo:TradeFaultCode6  STRING(30)
glo:TradeFaultCode7  STRING(30)
glo:TradeFaultCode8  STRING(30)
glo:TradeFaultCode9  STRING(30)
glo:TradeFaultCode10 STRING(30)
glo:TradeFaultCode11 STRING(30)
glo:TradeFaultCode12 STRING(30)
Access:DEFAULTS      &FileManager
Relate:DEFAULTS      &RelationManager
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE(0),THREAD
GlobalResponse       BYTE(0),THREAD
VCRRequest           LONG(0),THREAD
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  DctInit
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('pccstest.INI')
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  DctKill
  GlobalErrors.Kill
    

!* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

LinePrint FUNCTION(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>)                ! LinePrint Function

   CODE                                                                                 ! Fuction Code Starts Here
    IF OMITTED(2)                                                                       ! If Device Name is Omitted
       IF SUB(PRINTER{07B29H},1,2) = '\\' 
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})))               ! Use Default Device
       ELSE
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})) - 1)           ! Use Default Device
       END 
    ELSE                                                                                ! Otherwise
       lpszFilename = CLIP(DeviceName)                                                  ! Use passed Device Name
    END                                                                                 ! Terminate IF

    IF (OMITTED(3) OR CRLF = True) AND CLIP(StringToPrint) <> FF                        ! If CRLF parameter is set or omitted and user did not pass FF
        hpvBuffer = StringToPrint & CR & LF                                             ! Print String with CR & LF
    ELSE                                                                                ! Otherwise
       hpvBuffer = StringToPrint                                                        ! Print Text As Is
    END                                                                                 ! Terminate IF

    cbBuffer = LEN(CLIP(hpvBuffer))                                                     ! Check Length of the Data to be Printed

     hf = OpenFile(lpszFilename,OF_STRUCT,OF_WRITE)                                     ! Open file and obtain file handle
     IF hf = -1                                                                         ! If File does not exist
      hf = OpenFile(lpszFilename,OF_STRUCT,OF_CREATE)                                   ! Create file and obtain file handle
      IF hf = -1 THEN RETURN(OpenError).                                                ! If Error then return OpenError
     END                                                                                ! Terminate IF

    IF SUB(lpszFilename,1,3) <> 'COM' AND |                                             ! If user prints to a file
       SUB(lpszFilename,1,3) <> 'LPT' AND |                                            
       SUB(lpszFilename,1,2) <> '\\'
       IF _llseek(hf,0,2) = -1 THEN RETURN(4).                                          ! Set file pointer to the end of the file (Append lines)
    END                                                                                 ! Terminate IF

    BytesWritten = _lwrite(hf,hpvBuffer,cbBuffer)                                       ! Write to the file
    IF BytesWritten < cbBuffer THEN RETURN(WriteError).                                 ! IF Writing to a device or file is not possible, return Write Error
    IF _lclose(hf) THEN RETURN(CloseError) ELSE RETURN(Succeeded).                      ! If error in closing device or a file, return CloseError otherwise return Succeeded


DeleteFile FUNCTION(STRING FileToDelete)                                                ! DeleteFile Function

    CODE                                                                                ! Function Code Starts Here
    lpszFilename = CLIP(FileToDelete)                                                   ! Put file name in the buffer
    hf = OpenFile(lpszFilename,OF_STRUCT,OF_DELETE)                                     ! and delete it
     IF hf = -1 THEN RETURN(OpenError) ELSE RETURN(Succeeded).                          ! If error, return OpenError otherwise Return Succeeded

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


