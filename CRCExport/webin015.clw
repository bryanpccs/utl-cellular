

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN015.INC'),ONCE        !Local module procedure declarations
                     END


InsertIMEIShip       PROCEDURE  (updated, strIMEI, strShippingDate, strProductCode, strPath) ! Declare Procedure
result long
  CODE
    !
    ! Description : Insert a new IMEISHIP record
    !

    result = false
    updated = 0 ! Has the record been inserted or updated?

    if strIMEI <> '' and strShippingDate <> ''

        if exists(clip(strPath) & '\IMEISHIP.DAT')
            ! Change the paths this way
            IMEISHIP{Prop:Name} = clip(strPath) & '\IMEISHIP.DAT'

            Access:IMEISHIP.Open()
            Access:IMEISHIP.UseFile()

            Access:IMEISHIP.Clearkey(IMEI:IMEIKey)
            IMEI:IMEINumber = strIMEI
            if Access:IMEISHIP.Tryfetch(IMEI:IMEIKey) = Level:Benign
                ! Found
                IMEI:IMEINumber  = strIMEI
                IMEI:ShipDate    = deformat(strShippingDate, @d06b)
                IMEI:ManualEntry = 1
                IMEI:ProductCode = strProductCode
                if Access:IMEISHIP.TryUpdate() = Level:Benign
                    updated = 1
                    result = true
                end
            else
                ! Insert record
                if Access:IMEISHIP.PrimeRecord() = Level:Benign
                    IMEI:IMEINumber  = strIMEI
                    IMEI:ShipDate    = deformat(strShippingDate, @d06b)
                    IMEI:ManualEntry = 1
                    IMEI:ProductCode = strProductCode
                    if Access:IMEISHIP.TryInsert() = Level:Benign
                        result = true
                    else
                        Access:IMEISHIP.CancelAutoInc()
                    end
                end
            end

            Access:IMEISHIP.Close()
        end

    end

    return result
