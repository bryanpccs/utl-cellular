

   MEMBER('importoraclecodes.clw')                    ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('IMPOR001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:ImportFile       CSTRING(255),STATIC
tmp:LocaterToSend    CSTRING(255)
tmp:LocaterSent      CSTRING(255)
window               WINDOW('Import Carisma Codes'),AT(,,69,15),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       PROMPT('Import In Progress...'),AT(4,0),USE(?Prompt1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ImportFile      File,Driver('BASIC'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record                  Record
Manufacturer                String(30)
ModelNo                     String(30)
Colour                      String(30)
ContractOracleCode          String(30)
PAYTOracleCode              String(30)
                        End
                    End
!CARISMA              FILE,DRIVER('Btrieve'),NAME('CARISMA.DAT'),PRE(cma),CREATE,BINDABLE,THREAD
!RecordNumberKey          KEY(cma:RecordNumber),NOCASE,PRIMARY
!ManufactModColourKey     KEY(cma:Manufacturer,cma:ModelNo,cma:Colour),DUP,NOCASE
!ContractCodeKey          KEY(cma:ContractOracleCode),DUP,NOCASE
!PAYTCodeKey              KEY(cma:PAYTOracleCode),DUP,NOCASE
!Record                   RECORD,PRE()
!RecordNumber                LONG
!Manufacturer                STRING(30)
!ModelNo                     STRING(30)
!Colour                      STRING(30)
!ContractOracleCode          STRING(30)
!PAYTOracleCode              STRING(30)
!OracleActive                BYTE()
!PAYTActive                  BYTE()
!                         END
!                     END 
    map
ImportCodes procedure(string func:FileName)
    end
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  ImportCodes(Clip(Path()) & '\oracle.csv')
  
  Post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

ImportCodes        Procedure(String func:FileName)

Code
    Relate:MANUFACT.Open()
    Relate:MODELNUM.Open()
    Relate:MODELCOL.Open()
    Relate:CARISMA.Open()


    CountNormal# = 0
    CountUpdate# = 0
    tmp:ImportFile = func:FileName
    Open(ImportFile)
    Set(ImportFile,0)
    Loop
        Next(ImportFile)
        If Error()
            Break
        End !If Error()

        Access:CARISMA.ClearKey(cma:ContractCodeKey)
        cma:ContractOracleCode = impfil:ContractOracleCode
        If Access:CARISMA.TryFetch(cma:ContractCodeKey) = Level:Benign
            !Found
            ErrorLog('Record Not Imported (Duplicate Contract Oracle Code) : ' & clip(impfil:Manufacturer) & ' ' & clip(impfil:ModelNo) & ' ' & clip(impfil:Colour) & ' ' & clip(impfil:ContractOracleCode) & ' ' & clip(impfil:PAYTOracleCode))
            cycle
        Else ! If Access:CARISMA.TryFetch(cma:ContractCodeKey) = Level:Benign
            !Error
        End ! If Access:CARISMA.TryFetch(cma:ContractCodeKey) = Level:Benign

        Access:CARISMA.ClearKey(cma:PAYTCodeKey)
        cma:PAYTOracleCode = impfil:PAYTOracleCode
        If Access:CARISMA.TryFetch(cma:PAYTCodeKey) = Level:Benign
            !Found
            ErrorLog('Record Not Imported (Duplicate PAYT Oracle Code) : ' & clip(impfil:Manufacturer) & ' ' & clip(impfil:ModelNo) & ' ' & clip(impfil:Colour) & ' ' & clip(impfil:ContractOracleCode) & ' ' & clip(impfil:PAYTOracleCode))
            cycle
        Else ! If Access:CARISMA.TryFetch(cma:PAYTOracleCode) = Level:Benign
            !Error
        End ! If Access:CARISMA.TryFetch(cma:PAYTOracleCode) = Level:Benign

        Access:CARISMA.ClearKey(cma:ManufactModColourKey)
        cma:Manufacturer = impfil:Manufacturer
        cma:ModelNo      = impfil:ModelNo
        cma:Colour       = impfil:Colour
        If Access:CARISMA.TryFetch(cma:ManufactModColourKey) = Level:Benign
            !Found
            cma:ContractOracleCode = impfil:ContractOracleCode
            cma:PAYTOracleCode     = impfil:PAYTOracleCode
            If Access:CARISMA.TryUpdate() = Level:Benign
                CountUpdate# += 1
            End ! If Access:CARISMA.TryUpdate() = Level:Benign
        Else ! If Access:CARISMA.TryFetch(cma:ManufactModColourKey) = Level:Benign
            !Error
            ! Insert record
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer = impfil:Manufacturer
            If Access:MANUFACT.Fetch(man:Manufacturer_Key) = Level:Benign
                ! Found
                if man:IsVodafoneSpecific = false and man:IsCCentreSpecific = false
                    ErrorLog('Record Not Imported (Manufacturer Not Used By Vodafone) : ' & clip(impfil:Manufacturer) & ' ' & clip(impfil:ModelNo) & ' ' & clip(impfil:Colour) & ' ' & clip(impfil:ContractOracleCode) & ' ' & clip(impfil:PAYTOracleCode))
                    cycle
                end
                Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
                mod:Manufacturer = impfil:Manufacturer
                mod:Model_Number = impfil:ModelNo
                if Access:MODELNUM.Fetch(mod:Manufacturer_Key) = Level:Benign
                    ! Found
                    if mod:IsVodafoneSpecific = false and mod:IsCCentreSpecific = false
                        ErrorLog('Record Not Imported (Model Number Not Used By Vodafone) : ' & clip(impfil:Manufacturer) & ' ' & clip(impfil:ModelNo) & ' ' & clip(impfil:Colour) & ' ' & clip(impfil:ContractOracleCode) & ' ' & clip(impfil:PAYTOracleCode))
                        cycle
                    end
                    Access:MODELCOL.ClearKey(moc:Colour_Key)
                    moc:Model_Number = impfil:ModelNo
                    moc:Colour = impfil:Colour
                    if Access:MODELCOL.Fetch(moc:Colour_Key) = Level:Benign
                        ! Add record
                        If Access:CARISMA.PrimeRecord() = Level:Benign
                            cma:Manufacturer        = impfil:Manufacturer
                            cma:ModelNo             = impfil:ModelNo
                            cma:Colour              = impfil:Colour
                            cma:ContractOracleCode  = impfil:ContractOracleCode
                            cma:PAYTOracleCode      = impfil:PAYTOracleCode
                            If Access:CARISMA.TryInsert() = Level:Benign
                                !Insert
                                CountNormal# += 1
                            Else ! If Access:CARISMA.TryInsert() = Level:Benign
                                Access:CARISMA.CancelAutoInc()
                            End ! If Access:CARISMA.TryInsert() = Level:Benign
                        End ! If Access.CARISMA.PrimeRecord() = Level:Benign
                    Else
                        ErrorLog('Record Not Imported (Colour Not Attached To Model) : ' & clip(impfil:Manufacturer) & ' ' & clip(impfil:ModelNo) & ' ' & clip(impfil:Colour) & ' ' & clip(impfil:ContractOracleCode) & ' ' & clip(impfil:PAYTOracleCode))
                    End
                Else
                    ErrorLog('Record Not Imported (Model Number Not Found) : ' & clip(impfil:Manufacturer) & ' ' & clip(impfil:ModelNo) & ' ' & clip(impfil:Colour) & ' ' & clip(impfil:ContractOracleCode) & ' ' & clip(impfil:PAYTOracleCode))
                End
            Else
                ErrorLog('Record Not Imported (Manufacturer Not Found) : ' & clip(impfil:Manufacturer) & ' ' & clip(impfil:ModelNo) & ' ' & clip(impfil:Colour) & ' ' & clip(impfil:ContractOracleCode) & ' ' & clip(impfil:PAYTOracleCode))
            End
        End ! If Access:CARISMA.TryFetch(cma:ManufactModColourKey) = Level:Benign

    End !Loop(ImportFile)

    Close(ImportFile)

    Relate:MANUFACT.Close()
    Relate:MODELNUM.Close()
    Relate:MODELCOL.Close()
    Relate:CARISMA.Close()

    Message('Carisma Codes Imported: ' & CountNormal#)
    Message('Carisma Codes Updated: ' & CountUpdate#)

