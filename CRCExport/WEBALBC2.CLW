  MEMBER('webalert.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
WEBALBC2:DctInit    PROCEDURE
WEBALBC2:DctKill    PROCEDURE
WEBALBC2:FilesInit  PROCEDURE
  END

Hide:Access:JOBSE    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSE    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAULT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANFAULT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRACHAR  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRACHAR  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOCVALUE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOCVALUE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:COURIER  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:COURIER  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRDPARTY CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:TRDPARTY CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBNOTES CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBNOTES CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBPAYMT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBPAYMT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOCK    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:STOCK    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAFAULO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRAFAULO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAFAULT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
ValidateFieldServer    PROCEDURE(UNSIGNED Id,BYTE HandleErrors),BYTE,PROC,DERIVED
                     END


Hide:Relate:TRAFAULT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBLOHIS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBLOHIS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ACCESSOR CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ACCESSOR CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUBACCAD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SUBACCAD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WARPARTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:WARPARTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PARTS    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:PARTS    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBACC   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBACC   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:AUDIT    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:AUDIT    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:USERS    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:USERS    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SIDREG   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SIDREG   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

WEBALBC2:DctInit PROCEDURE
  CODE
  Relate:JOBSE &= Hide:Relate:JOBSE
  Relate:MANFAULT &= Hide:Relate:MANFAULT
  Relate:TRACHAR &= Hide:Relate:TRACHAR
  Relate:LOCVALUE &= Hide:Relate:LOCVALUE
  Relate:COURIER &= Hide:Relate:COURIER
  Relate:TRDPARTY &= Hide:Relate:TRDPARTY
  Relate:JOBNOTES &= Hide:Relate:JOBNOTES
  Relate:JOBPAYMT &= Hide:Relate:JOBPAYMT
  Relate:STOCK &= Hide:Relate:STOCK
  Relate:TRAFAULO &= Hide:Relate:TRAFAULO
  Relate:TRAFAULT &= Hide:Relate:TRAFAULT
  Relate:JOBLOHIS &= Hide:Relate:JOBLOHIS
  Relate:ACCESSOR &= Hide:Relate:ACCESSOR
  Relate:SUBACCAD &= Hide:Relate:SUBACCAD
  Relate:WARPARTS &= Hide:Relate:WARPARTS
  Relate:PARTS &= Hide:Relate:PARTS
  Relate:JOBACC &= Hide:Relate:JOBACC
  Relate:AUDIT &= Hide:Relate:AUDIT
  Relate:USERS &= Hide:Relate:USERS
  Relate:SIDREG &= Hide:Relate:SIDREG

WEBALBC2:FilesInit PROCEDURE
  CODE
  Hide:Relate:JOBSE.Init
  Hide:Relate:MANFAULT.Init
  Hide:Relate:TRACHAR.Init
  Hide:Relate:LOCVALUE.Init
  Hide:Relate:COURIER.Init
  Hide:Relate:TRDPARTY.Init
  Hide:Relate:JOBNOTES.Init
  Hide:Relate:JOBPAYMT.Init
  Hide:Relate:STOCK.Init
  Hide:Relate:TRAFAULO.Init
  Hide:Relate:TRAFAULT.Init
  Hide:Relate:JOBLOHIS.Init
  Hide:Relate:ACCESSOR.Init
  Hide:Relate:SUBACCAD.Init
  Hide:Relate:WARPARTS.Init
  Hide:Relate:PARTS.Init
  Hide:Relate:JOBACC.Init
  Hide:Relate:AUDIT.Init
  Hide:Relate:USERS.Init
  Hide:Relate:SIDREG.Init


WEBALBC2:DctKill PROCEDURE
  CODE
  Hide:Relate:JOBSE.Kill
  Hide:Relate:MANFAULT.Kill
  Hide:Relate:TRACHAR.Kill
  Hide:Relate:LOCVALUE.Kill
  Hide:Relate:COURIER.Kill
  Hide:Relate:TRDPARTY.Kill
  Hide:Relate:JOBNOTES.Kill
  Hide:Relate:JOBPAYMT.Kill
  Hide:Relate:STOCK.Kill
  Hide:Relate:TRAFAULO.Kill
  Hide:Relate:TRAFAULT.Kill
  Hide:Relate:JOBLOHIS.Kill
  Hide:Relate:ACCESSOR.Kill
  Hide:Relate:SUBACCAD.Kill
  Hide:Relate:WARPARTS.Kill
  Hide:Relate:PARTS.Kill
  Hide:Relate:JOBACC.Kill
  Hide:Relate:AUDIT.Kill
  Hide:Relate:USERS.Kill
  Hide:Relate:SIDREG.Kill


Hide:Access:JOBSE.Init PROCEDURE
  CODE
  SELF.Init(JOBSE,GlobalErrors)
  SELF.Buffer &= jobe:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jobe:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jobe:RefNumberKey,'By Ref Number',0)
  SELF.AddKey(jobe:InWorkshopDateKey,'jobe:InWorkshopDateKey',0)
  SELF.AddKey(jobe:CompleteRepairKey,'jobe:CompleteRepairKey',0)
  Access:JOBSE &= SELF


Hide:Relate:JOBSE.Init PROCEDURE
  CODE
  Hide:Access:JOBSE.Init
  SELF.Init(Access:JOBSE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSE &= NULL


Hide:Access:JOBSE.PrimeFields PROCEDURE

  CODE
  jobe:JobMark = 0
  jobe:JobReceived = 0
  jobe:FailedDelivery = 0
  jobe:CConfirmSecondEntry = 0
  jobe:CompleteRepairType = 0
  PARENT.PrimeFields


Hide:Relate:JOBSE.Kill PROCEDURE

  CODE
  Hide:Access:JOBSE.Kill
  PARENT.Kill
  Relate:JOBSE &= NULL


Hide:Access:MANFAULT.Init PROCEDURE
  CODE
  SELF.Init(MANFAULT,GlobalErrors)
  SELF.Buffer &= maf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(maf:Field_Number_Key,'By Field Number',0)
  Access:MANFAULT &= SELF


Hide:Relate:MANFAULT.Init PROCEDURE
  CODE
  Hide:Access:MANFAULT.Init
  SELF.Init(Access:MANFAULT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANUFACT)
  SELF.AddRelation(Relate:MANFAULO,RI:CASCADE,RI:CASCADE,mfo:Field_Key)
  SELF.AddRelationLink(maf:Manufacturer,mfo:Manufacturer)
  SELF.AddRelationLink(maf:Field_Number,mfo:Field_Number)


Hide:Access:MANFAULT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANFAULT &= NULL


Hide:Access:MANFAULT.PrimeFields PROCEDURE

  CODE
  maf:Compulsory = 'NO'
  maf:Compulsory_At_Booking = 'NO'
  maf:RestrictLength = 0
  maf:ForceFormat = 0
  PARENT.PrimeFields


Hide:Relate:MANFAULT.Kill PROCEDURE

  CODE
  Hide:Access:MANFAULT.Kill
  PARENT.Kill
  Relate:MANFAULT &= NULL


Hide:Access:TRACHAR.Init PROCEDURE
  CODE
  SELF.Init(TRACHAR,GlobalErrors)
  SELF.Buffer &= tch:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tch:Account_Number_Key,'By Charge Type',0)
  Access:TRACHAR &= SELF


Hide:Relate:TRACHAR.Init PROCEDURE
  CODE
  Hide:Access:TRACHAR.Init
  SELF.Init(Access:TRACHAR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRACHAR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRACHAR &= NULL


Hide:Relate:TRACHAR.Kill PROCEDURE

  CODE
  Hide:Access:TRACHAR.Kill
  PARENT.Kill
  Relate:TRACHAR &= NULL


Hide:Access:LOCVALUE.Init PROCEDURE
  CODE
  SELF.Init(LOCVALUE,GlobalErrors)
  SELF.Buffer &= lov:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(lov:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(lov:DateKey,'By Date',0)
  SELF.AddKey(lov:DateOnly,'lov:DateOnly',0)
  Access:LOCVALUE &= SELF


Hide:Relate:LOCVALUE.Init PROCEDURE
  CODE
  Hide:Access:LOCVALUE.Init
  SELF.Init(Access:LOCVALUE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOCATION)


Hide:Access:LOCVALUE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOCVALUE &= NULL


Hide:Relate:LOCVALUE.Kill PROCEDURE

  CODE
  Hide:Access:LOCVALUE.Kill
  PARENT.Kill
  Relate:LOCVALUE &= NULL


Hide:Access:COURIER.Init PROCEDURE
  CODE
  SELF.Init(COURIER,GlobalErrors)
  SELF.Buffer &= cou:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cou:Courier_Key,'By Courier',0)
  SELF.AddKey(cou:Courier_Type_Key,'By Courier_Type',0)
  Access:COURIER &= SELF


Hide:Relate:COURIER.Init PROCEDURE
  CODE
  Hide:Access:COURIER.Init
  SELF.Init(Access:COURIER,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRDPARTY,RI:CASCADE,RI:RESTRICT,trd:Courier_Only_Key)
  SELF.AddRelationLink(cou:Courier,trd:Courier)


Hide:Access:COURIER.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:COURIER &= NULL


Hide:Access:COURIER.PrimeFields PROCEDURE

  CODE
  cou:Courier_Type = 'NA'
  cou:DespatchClose = 'NO'
  cou:CustomerCollection = 0
  cou:AutoConsignmentNo = 0
  cou:PrintLabel = 0
  PARENT.PrimeFields


Hide:Relate:COURIER.Kill PROCEDURE

  CODE
  Hide:Access:COURIER.Kill
  PARENT.Kill
  Relate:COURIER &= NULL


Hide:Access:TRDPARTY.Init PROCEDURE
  CODE
  SELF.Init(TRDPARTY,GlobalErrors)
  SELF.Buffer &= trd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(trd:Company_Name_Key,'By Company Name',0)
  SELF.AddKey(trd:Account_Number_Key,'By Account_Number',0)
  SELF.AddKey(trd:Special_Instructions_Key,'By Special Instructions',0)
  SELF.AddKey(trd:Courier_Only_Key,'trd:Courier_Only_Key',0)
  Access:TRDPARTY &= SELF


Hide:Relate:TRDPARTY.Init PROCEDURE
  CODE
  Hide:Access:TRDPARTY.Init
  SELF.Init(Access:TRDPARTY,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRDMODEL,RI:CASCADE,RI:CASCADE,trm:Model_Number_Key)
  SELF.AddRelationLink(trd:Company_Name,trm:Company_Name)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:RESTRICT,job:Third_Party_Key)
  SELF.AddRelationLink(trd:Company_Name,job:Third_Party_Site)
  SELF.AddRelation(Relate:TRDSPEC)
  SELF.AddRelation(Relate:COURIER)


Hide:Access:TRDPARTY.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRDPARTY &= NULL


Hide:Access:TRDPARTY.PrimeFields PROCEDURE

  CODE
  trd:Batch_Number = 0
  PARENT.PrimeFields


Hide:Relate:TRDPARTY.Kill PROCEDURE

  CODE
  Hide:Access:TRDPARTY.Kill
  PARENT.Kill
  Relate:TRDPARTY &= NULL


Hide:Access:JOBNOTES.Init PROCEDURE
  CODE
  SELF.Init(JOBNOTES,GlobalErrors)
  SELF.Buffer &= jbn:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jbn:RefNumberKey,'By Job Number',0)
  Access:JOBNOTES &= SELF


Hide:Relate:JOBNOTES.Init PROCEDURE
  CODE
  Hide:Access:JOBNOTES.Init
  SELF.Init(Access:JOBNOTES,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBNOTES.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBNOTES &= NULL


Hide:Relate:JOBNOTES.Kill PROCEDURE

  CODE
  Hide:Access:JOBNOTES.Kill
  PARENT.Kill
  Relate:JOBNOTES &= NULL


Hide:Access:JOBPAYMT.Init PROCEDURE
  CODE
  SELF.Init(JOBPAYMT,GlobalErrors)
  SELF.Buffer &= jpt:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jpt:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(jpt:All_Date_Key,'By Date',0)
  Access:JOBPAYMT &= SELF


Hide:Relate:JOBPAYMT.Init PROCEDURE
  CODE
  Hide:Access:JOBPAYMT.Init
  SELF.Init(Access:JOBPAYMT,1)


Hide:Access:JOBPAYMT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBPAYMT &= NULL


Hide:Access:JOBPAYMT.PrimeFields PROCEDURE

  CODE
  jpt:Date = Today()
  PARENT.PrimeFields


Hide:Relate:JOBPAYMT.Kill PROCEDURE

  CODE
  Hide:Access:JOBPAYMT.Kill
  PARENT.Kill
  Relate:JOBPAYMT &= NULL


Hide:Access:STOCK.Init PROCEDURE
  CODE
  SELF.Init(STOCK,GlobalErrors)
  SELF.Buffer &= sto:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sto:Ref_Number_Key,'By Ref Number',1)
  SELF.AddKey(sto:Sundry_Item_Key,'By Sundry Item',0)
  SELF.AddKey(sto:Description_Key,'By Description',0)
  SELF.AddKey(sto:Description_Accessory_Key,'By Description',0)
  SELF.AddKey(sto:Shelf_Location_Key,'By Shelf Location',0)
  SELF.AddKey(sto:Shelf_Location_Accessory_Key,'By Shelf Location',0)
  SELF.AddKey(sto:Supplier_Accessory_Key,'By Supplier',0)
  SELF.AddKey(sto:Manufacturer_Key,'By Manufacturer',0)
  SELF.AddKey(sto:Supplier_Key,'By Supplier',0)
  SELF.AddKey(sto:Location_Key,'By Location',0)
  SELF.AddKey(sto:Part_Number_Accessory_Key,'By Part Number',0)
  SELF.AddKey(sto:Ref_Part_Description_Key,'Why?',0)
  SELF.AddKey(sto:Location_Manufacturer_Key,'By Manufacturer',0)
  SELF.AddKey(sto:Manufacturer_Accessory_Key,'By Part Number',0)
  SELF.AddKey(sto:Location_Part_Description_Key,'By Part Number',0)
  SELF.AddKey(sto:Ref_Part_Description2_Key,'CASCADE Key',0)
  SELF.AddKey(sto:Minimum_Part_Number_Key,'By Part Number',0)
  SELF.AddKey(sto:Minimum_Description_Key,'sto:Minimum_Description_Key',0)
  SELF.AddKey(sto:SecondLocKey,'By Part Number',0)
  SELF.AddKey(sto:RequestedKey,'By Part Number',0)
  SELF.AddKey(sto:ExchangeAccPartKey,'By Part Number',0)
  SELF.AddKey(sto:ExchangeAccDescKey,'By Description',0)
  Access:STOCK &= SELF


Hide:Relate:STOCK.Init PROCEDURE
  CODE
  Hide:Access:STOCK.Init
  SELF.Init(Access:STOCK,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOAUDIT,RI:CASCADE,RI:CASCADE,stoa:Stock_Ref_No_Key)
  SELF.AddRelationLink(sto:Ref_Number,stoa:Stock_Ref_No)
  SELF.AddRelation(Relate:STOHIST,RI:CASCADE,RI:CASCADE,shi:Ref_Number_Key)
  SELF.AddRelationLink(sto:Ref_Number,shi:Ref_Number)
  SELF.AddRelation(Relate:STOESN,RI:CASCADE,RI:CASCADE,ste:Sold_Key)
  SELF.AddRelationLink(sto:Ref_Number,ste:Ref_Number)
  SELF.AddRelation(Relate:ORDPARTS,RI:CASCADE,RI:None,orp:Ref_Number_Key)
  SELF.AddRelationLink(sto:Ref_Number,orp:Part_Ref_Number)
  SELF.AddRelationLink(sto:Part_Number,orp:Part_Number)
  SELF.AddRelationLink(sto:Description,orp:Description)
  SELF.AddRelation(Relate:STOMODEL,RI:CASCADE,RI:CASCADE,stm:Ref_Part_Description)
  SELF.AddRelationLink(sto:Ref_Number,stm:Ref_Number)
  SELF.AddRelationLink(sto:Part_Number,stm:Part_Number)
  SELF.AddRelationLink(sto:Location,stm:Location)
  SELF.AddRelationLink(sto:Description,stm:Description)
  SELF.AddRelation(Relate:LOCATION)
  SELF.AddRelation(Relate:LOCSHELF)


Hide:Access:STOCK.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOCK &= NULL


Hide:Access:STOCK.PrimeFields PROCEDURE

  CODE
  sto:Accessory = 'NO'
  sto:Minimum_Stock = 'NO'
  sto:Assign_Fault_Codes = 'NO'
  sto:Individual_Serial_Numbers = 'NO'
  sto:ExchangeUnit = 'NO'
  sto:Suspend = 0
  sto:E1 = 1
  sto:E2 = 1
  sto:E3 = 1
  sto:AllowDuplicate = 0
  sto:ExcludeFromEDI = 0
  sto:RF_Board = 0
  PARENT.PrimeFields


Hide:Relate:STOCK.Kill PROCEDURE

  CODE
  Hide:Access:STOCK.Kill
  PARENT.Kill
  Relate:STOCK &= NULL


Hide:Access:TRAFAULO.Init PROCEDURE
  CODE
  SELF.Init(TRAFAULO,GlobalErrors)
  SELF.Buffer &= tfo:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tfo:Field_Key,'By Field',0)
  SELF.AddKey(tfo:DescriptionKey,'By Description',0)
  Access:TRAFAULO &= SELF


Hide:Relate:TRAFAULO.Init PROCEDURE
  CODE
  Hide:Access:TRAFAULO.Init
  SELF.Init(Access:TRAFAULO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRAFAULT)


Hide:Access:TRAFAULO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRAFAULO &= NULL


Hide:Relate:TRAFAULO.Kill PROCEDURE

  CODE
  Hide:Access:TRAFAULO.Kill
  PARENT.Kill
  Relate:TRAFAULO &= NULL


Hide:Access:TRAFAULT.Init PROCEDURE
  CODE
  SELF.Init(TRAFAULT,GlobalErrors)
  SELF.Buffer &= taf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(taf:RecordNumberKey,'taf:RecordNumberKey',1)
  SELF.AddKey(taf:Field_Number_Key,'By Field Number',0)
  Access:TRAFAULT &= SELF


Hide:Relate:TRAFAULT.Init PROCEDURE
  CODE
  Hide:Access:TRAFAULT.Init
  SELF.Init(Access:TRAFAULT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRAFAULO,RI:CASCADE,RI:CASCADE,tfo:Field_Key)
  SELF.AddRelationLink(taf:AccountNumber,tfo:AccountNumber)
  SELF.AddRelationLink(taf:Field_Number,tfo:Field_Number)
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRAFAULT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRAFAULT &= NULL


Hide:Access:TRAFAULT.PrimeFields PROCEDURE

  CODE
  taf:Compulsory = 'NO'
  taf:Compulsory_At_Booking = 'NO'
  taf:RestrictLength = 0
  PARENT.PrimeFields


Hide:Access:TRAFAULT.ValidateFieldServer PROCEDURE(UNSIGNED Id,BYTE HandleErrors)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateFieldServer(Id,HandleErrors)
  CASE Id
  OF 3
    GlobalErrors.SetField('Fault Code Field Number')
    IF NOT INRANGE(taf:Field_Number,1,12)
      ReturnValue = Level:Notify
    END
    IF ReturnValue <> Level:Benign
      IF HandleErrors
        ReturnValue = GlobalErrors.ThrowMessage(Msg:FieldOutOfRange,'1 .. 12')
      END
    END
  END
  RETURN ReturnValue


Hide:Relate:TRAFAULT.Kill PROCEDURE

  CODE
  Hide:Access:TRAFAULT.Kill
  PARENT.Kill
  Relate:TRAFAULT &= NULL


Hide:Access:JOBLOHIS.Init PROCEDURE
  CODE
  SELF.Init(JOBLOHIS,GlobalErrors)
  SELF.Buffer &= jlh:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jlh:Ref_Number_Key,'By Date',0)
  SELF.AddKey(jlh:record_number_key,'jlh:record_number_key',1)
  Access:JOBLOHIS &= SELF


Hide:Relate:JOBLOHIS.Init PROCEDURE
  CODE
  Hide:Access:JOBLOHIS.Init
  SELF.Init(Access:JOBLOHIS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBLOHIS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBLOHIS &= NULL


Hide:Relate:JOBLOHIS.Kill PROCEDURE

  CODE
  Hide:Access:JOBLOHIS.Kill
  PARENT.Kill
  Relate:JOBLOHIS &= NULL


Hide:Access:ACCESSOR.Init PROCEDURE
  CODE
  SELF.Init(ACCESSOR,GlobalErrors)
  SELF.Buffer &= acr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(acr:Accesory_Key,'By Accessory',0)
  SELF.AddKey(acr:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(acr:AccessOnlyKey,'By Accessory',0)
  Access:ACCESSOR &= SELF


Hide:Relate:ACCESSOR.Init PROCEDURE
  CODE
  Hide:Access:ACCESSOR.Init
  SELF.Init(Access:ACCESSOR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ACCESDEF)
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:ACCESSOR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ACCESSOR &= NULL


Hide:Relate:ACCESSOR.Kill PROCEDURE

  CODE
  Hide:Access:ACCESSOR.Kill
  PARENT.Kill
  Relate:ACCESSOR &= NULL


Hide:Access:SUBACCAD.Init PROCEDURE
  CODE
  SELF.Init(SUBACCAD,GlobalErrors)
  SELF.Buffer &= sua:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sua:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(sua:AccountNumberKey,'By Account Number',0)
  SELF.AddKey(sua:CompanyNameKey,'By Company Name',0)
  SELF.AddKey(sua:AccountNumberOnlyKey,'By Account Number',0)
  Access:SUBACCAD &= SELF


Hide:Relate:SUBACCAD.Init PROCEDURE
  CODE
  Hide:Access:SUBACCAD.Init
  SELF.Init(Access:SUBACCAD,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUBTRACC)


Hide:Access:SUBACCAD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUBACCAD &= NULL


Hide:Relate:SUBACCAD.Kill PROCEDURE

  CODE
  Hide:Access:SUBACCAD.Kill
  PARENT.Kill
  Relate:SUBACCAD &= NULL


Hide:Access:WARPARTS.Init PROCEDURE
  CODE
  SELF.Init(WARPARTS,GlobalErrors)
  SELF.Buffer &= wpr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wpr:Part_Number_Key,'By Part Number',0)
  SELF.AddKey(wpr:RecordNumberKey,'wpr:RecordNumberKey',1)
  SELF.AddKey(wpr:PartRefNoKey,'By Part Ref Number',0)
  SELF.AddKey(wpr:Date_Ordered_Key,'By Date Ordered',0)
  SELF.AddKey(wpr:RefPartRefNoKey,'By Part Ref Number',0)
  SELF.AddKey(wpr:PendingRefNoKey,'wpr:PendingRefNoKey',0)
  SELF.AddKey(wpr:Order_Number_Key,'wpr:Order_Number_Key',0)
  SELF.AddKey(wpr:Supplier_Key,'By Supplier',0)
  SELF.AddKey(wpr:Order_Part_Key,'By Order Part Number',0)
  SELF.AddKey(wpr:SuppDateOrdKey,'By Date Ordered',0)
  SELF.AddKey(wpr:SuppDateRecKey,'By Date Received',0)
  SELF.AddKey(wpr:RequestedKey,'By Part Number',0)
  Access:WARPARTS &= SELF


Hide:Relate:WARPARTS.Init PROCEDURE
  CODE
  Hide:Access:WARPARTS.Init
  SELF.Init(Access:WARPARTS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPPLIER)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:WARPARTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WARPARTS &= NULL


Hide:Access:WARPARTS.PrimeFields PROCEDURE

  CODE
  wpr:Main_Part = 'NO'
  wpr:Fault_Codes_Checked = 'NO'
  wpr:Credit = 'NO'
  wpr:Requested = 0
  PARENT.PrimeFields


Hide:Relate:WARPARTS.Kill PROCEDURE

  CODE
  Hide:Access:WARPARTS.Kill
  PARENT.Kill
  Relate:WARPARTS &= NULL


Hide:Access:PARTS.Init PROCEDURE
  CODE
  SELF.Init(PARTS,GlobalErrors)
  SELF.Buffer &= par:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(par:Part_Number_Key,'By Part Number',0)
  SELF.AddKey(par:recordnumberkey,'par:recordnumberkey',1)
  SELF.AddKey(par:PartRefNoKey,'By Part_Ref_Number',0)
  SELF.AddKey(par:Date_Ordered_Key,'By Date Ordered',0)
  SELF.AddKey(par:RefPartRefNoKey,'By Part Ref Number',0)
  SELF.AddKey(par:PendingRefNoKey,'par:PendingRefNoKey',0)
  SELF.AddKey(par:Order_Number_Key,'par:Order_Number_Key',0)
  SELF.AddKey(par:Supplier_Key,'By Supplier',0)
  SELF.AddKey(par:Order_Part_Key,'By Order Part Number',0)
  SELF.AddKey(par:SuppDateOrdKey,'By Date Ordered',0)
  SELF.AddKey(par:SuppDateRecKey,'By Date Received',0)
  SELF.AddKey(par:RequestedKey,'By Part Number',0)
  Access:PARTS &= SELF


Hide:Relate:PARTS.Init PROCEDURE
  CODE
  Hide:Access:PARTS.Init
  SELF.Init(Access:PARTS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPPLIER)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:PARTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PARTS &= NULL


Hide:Access:PARTS.PrimeFields PROCEDURE

  CODE
  par:Fault_Codes_Checked = 'NO'
  par:Credit = 'NO'
  par:Requested = 0
  PARENT.PrimeFields


Hide:Relate:PARTS.Kill PROCEDURE

  CODE
  Hide:Access:PARTS.Kill
  PARENT.Kill
  Relate:PARTS &= NULL


Hide:Access:JOBACC.Init PROCEDURE
  CODE
  SELF.Init(JOBACC,GlobalErrors)
  SELF.Buffer &= jac:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jac:Ref_Number_Key,'By Accessory',0)
  Access:JOBACC &= SELF


Hide:Relate:JOBACC.Init PROCEDURE
  CODE
  Hide:Access:JOBACC.Init
  SELF.Init(Access:JOBACC,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBACC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBACC &= NULL


Hide:Relate:JOBACC.Kill PROCEDURE

  CODE
  Hide:Access:JOBACC.Kill
  PARENT.Kill
  Relate:JOBACC &= NULL


Hide:Access:AUDIT.Init PROCEDURE
  CODE
  SELF.Init(AUDIT,GlobalErrors)
  SELF.Buffer &= aud:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(aud:Ref_Number_Key,'By Date',0)
  SELF.AddKey(aud:Action_Key,'By Action',0)
  SELF.AddKey(aud:User_Key,'By User',0)
  SELF.AddKey(aud:Record_Number_Key,'aud:Record_Number_Key',1)
  SELF.AddKey(aud:ActionOnlyKey,'By Action',0)
  SELF.AddKey(aud:TypeRefKey,'By Date',0)
  SELF.AddKey(aud:TypeActionKey,'By Action',0)
  SELF.AddKey(aud:TypeUserKey,'By User',0)
  SELF.AddKey(aud:DateActionJobKey,'By Job Number',0)
  SELF.AddKey(aud:DateJobKey,'By Job Number',0)
  SELF.AddKey(aud:DateTypeJobKey,'By Job Number',0)
  SELF.AddKey(aud:DateTypeActionKey,'By Job Number',0)
  Access:AUDIT &= SELF


Hide:Relate:AUDIT.Init PROCEDURE
  CODE
  Hide:Access:AUDIT.Init
  SELF.Init(Access:AUDIT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:AUDIT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:AUDIT &= NULL


Hide:Access:AUDIT.PrimeFields PROCEDURE

  CODE
  aud:Type = 'JOB'
  PARENT.PrimeFields


Hide:Relate:AUDIT.Kill PROCEDURE

  CODE
  Hide:Access:AUDIT.Kill
  PARENT.Kill
  Relate:AUDIT &= NULL


Hide:Access:USERS.Init PROCEDURE
  CODE
  SELF.Init(USERS,GlobalErrors)
  SELF.Buffer &= use:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(use:User_Code_Key,'By User Code',0)
  SELF.AddKey(use:User_Type_Active_Key,'By Surname',0)
  SELF.AddKey(use:Surname_Active_Key,'By Surname',0)
  SELF.AddKey(use:User_Code_Active_Key,'use:User_Code_Active_Key',0)
  SELF.AddKey(use:User_Type_Key,'By Surname',0)
  SELF.AddKey(use:surname_key,'use:surname_key',0)
  SELF.AddKey(use:password_key,'use:password_key',0)
  SELF.AddKey(use:Logged_In_Key,'By Name',0)
  SELF.AddKey(use:Team_Surname,'Surname',0)
  SELF.AddKey(use:Active_Team_Surname,'By Surname',0)
  Access:USERS &= SELF


Hide:Relate:USERS.Init PROCEDURE
  CODE
  Hide:Access:USERS.Init
  SELF.Init(Access:USERS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TEAMS)
  SELF.AddRelation(Relate:USUASSIG,RI:CASCADE,RI:CASCADE,usu:Unit_Type_Key)
  SELF.AddRelationLink(use:User_Code,usu:User_Code)
  SELF.AddRelation(Relate:USMASSIG,RI:CASCADE,RI:CASCADE,usm:Model_Number_Key)
  SELF.AddRelationLink(use:User_Code,usm:User_Code)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:RESTRICT,job:Engineer_Key)
  SELF.AddRelationLink(use:User_Code,job:Engineer)


Hide:Access:USERS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:USERS &= NULL


Hide:Access:USERS.PrimeFields PROCEDURE

  CODE
  use:StockFromLocationOnly = 0
  PARENT.PrimeFields


Hide:Relate:USERS.Kill PROCEDURE

  CODE
  Hide:Access:USERS.Kill
  PARENT.Kill
  Relate:USERS &= NULL


Hide:Access:SIDREG.Init PROCEDURE
  CODE
  SELF.Init(SIDREG,GlobalErrors)
  SELF.Buffer &= srg:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(srg:SIDRegIDKey,'srg:SIDRegIDKey',1)
  SELF.AddKey(srg:SCRegionNameKey,'srg:SCRegionNameKey',0)
  SELF.AddKey(srg:RegionNameKey,'srg:RegionNameKey',0)
  Access:SIDREG &= SELF


Hide:Relate:SIDREG.Init PROCEDURE
  CODE
  Hide:Access:SIDREG.Init
  SELF.Init(Access:SIDREG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SIDSRVCN)
  SELF.AddRelation(Relate:REGIONS)


Hide:Access:SIDREG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SIDREG &= NULL


Hide:Relate:SIDREG.Kill PROCEDURE

  CODE
  Hide:Access:SIDREG.Kill
  PARENT.Kill
  Relate:SIDREG &= NULL

