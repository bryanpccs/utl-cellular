

   MEMBER('CarismaCodes.clw')                         ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CARIS002.INC'),ONCE        !Local module procedure declarations
                     END


UpdateCarismaCode PROCEDURE (func:ModelNo, func:Manufacturer, func:Colour) !Generated from procedure template - Window

result               BYTE
tmp:Manufacturer     STRING(30)
tmp:ModelNo          STRING(30)
tmp:Colour           STRING(30)
tmp:CarismaCode      STRING(30)
tmp:VerifyCarismaCode STRING(30)
tmp:SaveCarismaCode  STRING(30)
window               WINDOW('Update Oracle Code'),AT(,,216,135),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,208,100),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Manufacturer'),AT(8,24),USE(?tmp:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(76,24,124,10),USE(tmp:Manufacturer),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(8,40),USE(?tmp:ModelNo:Prompt),TRN
                           ENTRY(@s30),AT(76,40,124,10),USE(tmp:ModelNo),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Colour'),AT(8,56),USE(?tmp:Colour:Prompt),TRN
                           ENTRY(@s30),AT(76,56,124,10),USE(tmp:Colour),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Oracle Code'),AT(8,72),USE(?tmp:CarismaCode:Prompt),TRN
                           ENTRY(@s30),AT(76,72,124,10),USE(tmp:CarismaCode),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR
                           PROMPT('Verify Oracle Code'),AT(8,88),USE(?tmp:VerifyCarismaCode:Prompt),HIDE
                           ENTRY(@s30),AT(76,88,124,10),USE(tmp:VerifyCarismaCode),HIDE,FONT(,,,,CHARSET:ANSI),COLOR(080FFFFH),MSG('Verify Carisma Code'),TIP('Verify Carisma Code'),PASSWORD
                           PANEL,AT(4,108,208,24),USE(?Panel1)
                         END
                       END
                       BUTTON('OK'),AT(96,112,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(152,112,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(result)


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateCarismaCode')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CARISMA.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  tmp:Manufacturer = func:Manufacturer
  tmp:ModelNo = func:ModelNo
  tmp:Colour = func:Colour
  
  Access:CARISMA.ClearKey(cma:ManufactModColourKey)
  cma:Manufacturer = func:Manufacturer
  cma:ModelNo = func:ModelNo
  cma:Colour = func:Colour
  if Access:CARISMA.Fetch(cma:ManufactModColourKey) = Level:Benign
      tmp:CarismaCode = cma:CarismaCode
  end
  ! Inserting (DBH 20/09/2006) # N/A - Save the code to see if it's changing
  tmp:SaveCarismaCode = tmp:CarismaCode
  ! End (DBH 20/09/2006) #N/A
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CARISMA.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! OK Button
      
      if tmp:CarismaCode = ''
          select(?tmp:CarismaCode)
          Display()
          cycle
      end
      
      If tmp:CarismaCode <> tmp:SaveCarismaCode
          If tmp:VerifyCarismaCode = ''
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('Please verify that the Oracle Code.','Error',|
                             icon:Hand,'&OK',1,1) 
                  Of 1 ! &OK Button
              End!Case Message
              Select(?tmp:VerifyCarismaCode)
              Display()
              Cycle
          End ! If tmp:VerifyCarismaCode = ''
      End ! If tmp:CarismaCode <> tmp:SaveCarismaCode
      
      Access:CARISMA.ClearKey(cma:ManufactModColourKey)
      cma:Manufacturer = func:Manufacturer
      cma:ModelNo = func:ModelNo
      cma:Colour = func:Colour
      if Access:CARISMA.Fetch(cma:ManufactModColourKey) = Level:Benign
          cma:CarismaCode = tmp:CarismaCode
          if Access:CARISMA.Update() = Level:Benign
              result = true
          end
      else
          if Access:CARISMA.PrimeRecord() = Level:Benign
              cma:Manufacturer = func:Manufacturer
              cma:ModelNo      = func:ModelNo
              cma:Colour       = func:Colour
              cma:CarismaCode  = tmp:CarismaCode
              if Access:CARISMA.Update() = Level:Benign
                  result = true
              else
                  Access:CARISMA.CancelAutoInc()
              end
          end
      end
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:CarismaCode
      If tmp:CarismaCode <> tmp:SaveCarismaCode
          Access:CARISMA.ClearKey(cma:CarismaCodeKey)
          cma:CarismaCode = tmp:CarismaCode
          If Access:CARISMA.TryFetch(cma:CarismaCodeKey) = Level:Benign
              !Found
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('The entered Code has already been applied to the follow Model:'&|
                  '|' & Clip(cma:Manufacturer) & ''&|
                  '|' & Clip(cma:ModelNo) & ''&|
                  '|' & Clip(cma:Colour) & '','Error',|
                             icon:Hand,'&OK',1,1) 
                  Of 1 ! &OK Button
              End!Case Message
              tmp:CarismaCode = ''
              Display()
              Cycle
          Else ! If Access:CARISMA.TryFetch(cma:CarismaCodeKey) = Level:Benign
              !Error
          End ! If Access:CARISMA.TryFetch(cma:CarismaCodeKey) = Level:Benign
          ?tmp:VerifyCarismaCode{prop:Hide} = False
          ?tmp:VerifyCarismaCode:Prompt{prop:Hide} = False
          Select(?tmp:VerifyCarismaCode)
      End ! If tmp:CarismaCode <> tmp:SaveCarismaCode
      Display()
    OF ?tmp:VerifyCarismaCode
      If tmp:VerifyCarismaCode <> tmp:CarismaCode
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('The entered codes do not match.','Error',|
                         icon:Hand,'&OK',1,1) 
              Of 1 ! &OK Button
          End!Case Message
          tmp:VerifyCarismaCode = ''
          Select(?tmp:CarismaCode)
          Display()
      End ! If tmp:VerifyCarismaCode <> tmp:CarismaCode
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

