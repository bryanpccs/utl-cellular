

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN023.INC'),ONCE        !Local module procedure declarations
                     END


GetExceptionText     PROCEDURE  (strBookingType, strModelNo, strExceptText, strPath) ! Declare Procedure
result      long

  CODE
    !
    ! Description : Get the model exception text if active
    !

    strExceptText = ''

    if exists(clip(strPath) & '\SIDMODTT.DAT')
        ! Change the paths this way
        SIDMODTT{Prop:Name} = clip(strPath) & '\SIDMODTT.DAT'

        Access:SIDMODTT.Open()
        Access:SIDMODTT.UseFile()

        Access:SIDMODTT.ClearKey(smt:ModelNoKey)
        smt:ModelNo = strModelNo
        if Access:SIDMODTT.Fetch(smt:ModelNoKey) = Level:Benign
            if smt:TurnaroundException
                if strBookingType = 'R' and smt:TurnaroundDaysRetail > 0
                    if smt:ActiveTextRetail
                        strExceptText = smt:TextRetail
                    end
                end
                if strBookingType = 'E' and smt:TurnaroundDaysExch > 0
                    if smt:ActiveTextExch
                        strExceptText = smt:TextExch
                    end
                end
            end
        end

        Access:SIDMODTT.Close()

        result = true
    end

    return result
