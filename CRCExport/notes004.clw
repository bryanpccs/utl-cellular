

   MEMBER('notesfix.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('NOTES004.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:ImportFile       CSTRING(255),STATIC
ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
window               WINDOW('Notes Fix'),AT(,,69,15),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Dummy SQL table
StoreNote       file, driver('Scalable'), oem, owner(ConnectionStr), name('STORENOTE'), pre(ston), bindable, thread
Record              record
StoreNoteID             long, name('StoreNoteID')
                    end
                end
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

Import:Data routine
    !
    !
    !

    data

startingRecord  long, auto

    code

    Relate:JOBS.Open()
    Relate:CONTHIST.Open()

    IniFilepath =  Clip(Path()) & '\Webimp.ini'
    ConnectionStr = GetIni('Defaults', 'Connection', '', Clip(IniFilepath))

    startingRecord = GetIni('Defaults', 'ContHistId', '', Clip(IniFilepath))

    ErrorLog('Starting import of CONTHIST records from : ' & startingRecord)

    Open(StoreNote)

    Access:CONTHIST.ClearKey(cht:Record_Number_Key)
    cht:Record_Number = startingRecord
    set(cht:Record_Number_Key, cht:Record_Number_Key)
    loop
        if Access:CONTHIST.Next() <> Level:Benign then break.

        ! Fetch the job record to get the webjob number
        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = cht:Ref_Number
        if Access:JOBS.Tryfetch(job:Ref_Number_Key) <> Level:Benign
            ErrorLog('ServiceBase Job number not found: ' & job:Ref_Number)
            cycle
        end

        if clip(job:Order_Number) = ''
            ErrorLog('Web job number not found for ServiceBase job : ' & job:Ref_Number)
            cycle
        end

        StoreNote{Prop:SQL} = 'INSERT INTO StoreNote (SIDRef, EntryDate, EntryTime, UserID, Action, Notes) VALUES(''' & |
                              clip(job:Order_Number) & ''',''' & |
                              format(cht:Date, @d10-) & ''',''' & |
                              format(cht:Time, @t04) & ''',''0'',''' & |
                              clip(cht:Action) & ''',''' & |
                              clip(cht:Notes) & ''')'
    end

    ErrorLog('Import complete')

    Close(StoreNote)

    Relate:JOBS.Close()
    Relate:CONTHIST.Close()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  ! Import data file
  Do Import:Data

  Post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

