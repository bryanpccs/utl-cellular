

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN011.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBIN008.INC'),ONCE        !Req'd for module callout resolution
                     END


IsBankHoliday        PROCEDURE  (strCountry, strDate, strPath) ! Declare Procedure
result      long
strIniFile  like(reg:BankHolidaysFileName)
availableDate date
    map
IsBH procedure(date sourceDate), long
    end
  CODE
    !
    ! Description : Check if the date selected is a bank holiday
    !               Returns true/false
    !

    result = false   ! Default to not a bank holiday

    if strDate <> ''

        availableDate = deformat(strDate, @d06b)

        case strCountry
            of 'NORTHERN IRELAND'
                strIniFile = 'BHNIRE.DAT'
            of 'SCOTLAND'
                strIniFile = 'BHSCOT.DAT'
            else ! Default to England / Wales
                strIniFile = 'BHENGWAL.DAT'
        end

        result = IsBH(availableDate)
    end

    return result
IsBH procedure(date sourceDate)

x                   long(1)
bh                  date, dim(15)
isBH                long

    code

    isBH = false

    LoadBankHolidays(bh, strIniFile, strPath)

    loop x = 1 to 15
        if (bh[x] <> '')
            if (sourceDate = bh[x])
                isBH = true
                break
            end
        end
    end ! loop

    return isBH
