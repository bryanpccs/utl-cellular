  MEMBER('imeishipimp.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
IMEISBC0:DctInit    PROCEDURE
IMEISBC0:DctKill    PROCEDURE
IMEISBC0:FilesInit  PROCEDURE
  END

Hide:Access:LOCDEFS  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:LOCDEFS  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:IMEISHP2 CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:IMEISHP2 CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:IMEISHIP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:IMEISHIP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

IMEISBC0:DctInit PROCEDURE
  CODE
  Relate:LOCDEFS &= Hide:Relate:LOCDEFS
  Relate:IMEISHP2 &= Hide:Relate:IMEISHP2
  Relate:IMEISHIP &= Hide:Relate:IMEISHIP

IMEISBC0:FilesInit PROCEDURE
  CODE
  Hide:Relate:LOCDEFS.Init
  Hide:Relate:IMEISHP2.Init
  Hide:Relate:IMEISHIP.Init


IMEISBC0:DctKill PROCEDURE
  CODE
  Hide:Relate:LOCDEFS.Kill
  Hide:Relate:IMEISHP2.Kill
  Hide:Relate:IMEISHIP.Kill


Hide:Access:LOCDEFS.Init PROCEDURE
  CODE
  SELF.Init(LOCDEFS,GlobalErrors)
  SELF.Buffer &= locdef:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(locdef:RecordNumberKey,'By Record Number',1)
  Access:LOCDEFS &= SELF


Hide:Relate:LOCDEFS.Init PROCEDURE
  CODE
  Hide:Access:LOCDEFS.Init
  SELF.Init(Access:LOCDEFS,1)


Hide:Access:LOCDEFS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOCDEFS &= NULL


Hide:Access:LOCDEFS.PrimeFields PROCEDURE

  CODE
  locdef:AlertDays = 7
  PARENT.PrimeFields


Hide:Relate:LOCDEFS.Kill PROCEDURE

  CODE
  Hide:Access:LOCDEFS.Kill
  PARENT.Kill
  Relate:LOCDEFS &= NULL


Hide:Access:IMEISHP2.Init PROCEDURE
  CODE
  SELF.Init(IMEISHP2,GlobalErrors)
  SELF.Buffer &= imei2:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(imei2:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(imei2:IMEIRecordNumberKey,'By Record Number',0)
  Access:IMEISHP2 &= SELF


Hide:Relate:IMEISHP2.Init PROCEDURE
  CODE
  Hide:Access:IMEISHP2.Init
  SELF.Init(Access:IMEISHP2,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:IMEISHIP)


Hide:Access:IMEISHP2.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:IMEISHP2 &= NULL


Hide:Relate:IMEISHP2.Kill PROCEDURE

  CODE
  Hide:Access:IMEISHP2.Kill
  PARENT.Kill
  Relate:IMEISHP2 &= NULL


Hide:Access:IMEISHIP.Init PROCEDURE
  CODE
  SELF.Init(IMEISHIP,GlobalErrors)
  SELF.Buffer &= IMEI:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(IMEI:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(IMEI:IMEIKey,'By I.M.E.I. Number',0)
  SELF.AddKey(IMEI:ShipDateKey,'By Shipment Date',0)
  Access:IMEISHIP &= SELF


Hide:Relate:IMEISHIP.Init PROCEDURE
  CODE
  Hide:Access:IMEISHIP.Init
  SELF.Init(Access:IMEISHIP,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:IMEISHP2,RI:CASCADE,RI:CASCADE,imei2:IMEIRecordNumberKey)
  SELF.AddRelationLink(IMEI:RecordNumber,imei2:IMEIRecordNumber)


Hide:Access:IMEISHIP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:IMEISHIP &= NULL


Hide:Access:IMEISHIP.PrimeFields PROCEDURE

  CODE
  IMEI:BER = 0
  IMEI:ManualEntry = 0
  PARENT.PrimeFields


Hide:Relate:IMEISHIP.Kill PROCEDURE

  CODE
  Hide:Access:IMEISHIP.Kill
  PARENT.Kill
  Relate:IMEISHIP &= NULL

