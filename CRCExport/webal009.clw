

   MEMBER('webalert.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBAL009.INC'),ONCE        !Local module procedure declarations
                     END


Capitalize           PROCEDURE  (f_string)            ! Declare Procedure
  CODE
    f_string = CLIP(LEFT(f_string))

    STR_LEN#  = LEN(f_string)
    STR_POS#  = 1

    f_string = UPPER(SUB(f_string,STR_POS#,1)) & SUB(f_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF SUB(f_string,STR_POS#,1) = ' ' OR SUB(f_string,STR_POS#,1) = '-'  |
        OR SUB(f_string,STR_POS#,1) = '.' OR SUB(f_string,STR_POS#,1) = '/' |
        OR SUB(f_string,STR_POS#,1) = '&' OR SUB(f_string,STR_POS#,1) = '(' OR SUB(f_string,STR_POS#,1) = CHR(39)
        f_string = SUB(f_string,1,STR_POS#) & UPPER(SUB(f_string,STR_POS#+1,1)) & SUB(f_string,STR_POS#+2,STR_LEN#-1)
     ELSE
        f_string = SUB(f_string,1,STR_POS#) & LOWER(SUB(f_string,STR_POS#+1,1)) & SUB(f_string,STR_POS#+2,STR_LEN#-1)

     .
    .

    RETURN(f_string)

