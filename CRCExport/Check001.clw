

   MEMBER('CheckOraclesCodes.clw')                    ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CHECK001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

window               WINDOW('UnAlloc Models Check'),AT(,,69,15),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       PROMPT('In Progress...'),AT(4,0),USE(?Prompt1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
tmp:ContractOracleCode  string(30)
tmp:PAYTOracleCode      string(30)
CARISMA              FILE,DRIVER('Btrieve'),NAME('CARISMA.DAT'),PRE(cma),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(cma:RecordNumber),NOCASE,PRIMARY
ManufactModColourKey     KEY(cma:Manufacturer,cma:ModelNo,cma:Colour),DUP,NOCASE
ContractCodeKey          KEY(cma:ContractOracleCode),DUP,NOCASE
PAYTCodeKey              KEY(cma:PAYTOracleCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
Colour                      STRING(30)
ContractOracleCode          STRING(30)
PAYTOracleCode              STRING(30)
ContractActive              BYTE()
PAYTActive                  BYTE()
                         END
                     END 
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MANUFACT.Open
  Access:MODELNUM.UseFile
  Access:MODELCOL.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  0{Prop:Hide} = true
  
  ! A small utility to generate a log file of any unallocated oracle codes (models/colour combinations which do not have a oracle code attached)
  
  share(CARISMA)
  
  set(mod:Model_Number_Key)
  loop
      if Access:MODELNUM.Next() <> Level:Benign then break.
      if mod:IsVodafoneSpecific = false and mod:IsCCentreSpecific = false then cycle.
  
      Access:MANUFACT.ClearKey(man:Manufacturer_Key)
      man:Manufacturer = mod:Manufacturer
      if Access:MANUFACT.Fetch(man:Manufacturer_Key) <> Level:Benign then cycle.
      if man:IsVodafoneSpecific = false and man:IsCCentreSpecific = false then cycle.
  
      Access:MODELCOL.ClearKey(moc:Colour_Key)
      moc:Model_Number = mod:Model_Number
      set(moc:Colour_Key, moc:Colour_Key)
      loop
          if Access:MODELCOL.Next() <> Level:Benign then break.
          if moc:Model_Number <> mod:Model_Number then break.
  
          tmp:ContractOracleCode  = ''
          tmp:PAYTOracleCode      = ''
  
          clear(cma:record)
          cma:Manufacturer = mod:Manufacturer
          cma:ModelNo      = mod:Model_Number
          cma:Colour       = moc:Colour
          get(CARISMA, cma:ManufactModColourKey)
          if not error()
              tmp:ContractOracleCode  = cma:ContractOracleCode
              tmp:PAYTOracleCode      = cma:PAYTOracleCode
          end
  
          if tmp:ContractOracleCode = '' and tmp:PAYTOracleCode = ''
              ErrorLog('No Contract/PAYT Oracle Code Attached To : ' & clip(mod:Manufacturer) & ' ' & clip(mod:Model_Number) & ' ' & clip(moc:Colour))
          else
              if tmp:ContractOracleCode = ''
                  ErrorLog('No Contract Oracle Code Attached To : ' & clip(mod:Manufacturer) & ' ' & clip(mod:Model_Number) & ' ' & clip(moc:Colour))
              end
  
              if tmp:PAYTOracleCode = ''
                  ErrorLog('No PAYT Oracle Code Attached To : ' & clip(mod:Manufacturer) & ' ' & clip(mod:Model_Number) & ' ' & clip(moc:Colour))
              end
          end
  
      end
  
  end
  
  close(CARISMA)
  
  post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

