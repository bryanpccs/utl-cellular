

   MEMBER('LocaterImportDefaults.clw')                ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOCAT001.INC'),ONCE        !Local module procedure declarations
                     END


FTPDefaults PROCEDURE                                 !Generated from procedure template - Window

tmp:RemoteServer     STRING(255)
tmp:RemoteUsername   STRING(255)
tmp:RemotePassword   STRING(255)
tmp:RemoteExportPath STRING(255)
tmp:RemoteImportPath STRING(255)
ActionMessage        CSTRING(40)
window               WINDOW('Defaults'),AT(,,311,297),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       GROUP('FTP Defaults'),AT(4,0,304,96),USE(?Group1),BOXED
                         ENTRY(@s255),AT(112,10,188,10),USE(locdef:FTPServer),FONT(,,,FONT:bold),MSG('FTPServer')
                         ENTRY(@s60),AT(112,26,188,10),USE(locdef:FTPUserName),FONT(,,,FONT:bold),MSG('FTPUserName')
                         ENTRY(@s60),AT(112,42,188,10),USE(locdef:FTPPassword),FONT(,,,FONT:bold),PASSWORD,MSG('FTPPassword')
                         ENTRY(@s255),AT(112,74,188,10),USE(locdef:FTPImportPath),FONT(,,,FONT:bold),MSG('FTPImportPath')
                         ENTRY(@s255),AT(112,58,188,10),USE(locdef:FTPExportPath),FONT(,,,FONT:bold),MSG('FTPExportPath')
                       END
                       PROMPT('Server'),AT(12,12),USE(?tmp:RemoteServer:Prompt)
                       PROMPT('Username'),AT(12,28),USE(?tmp:RemoteUsername:Prompt)
                       PROMPT('Password'),AT(12,44),USE(?tmp:RemotePassword:Prompt)
                       PROMPT('Export Path'),AT(12,60),USE(?tmp:RemoteExportPath:Prompt)
                       PROMPT('Import Path'),AT(12,76),USE(?tmp:RemoteImportPath:Prompt)
                       GROUP('General Email Defaults'),AT(4,100,304,64),USE(?Group5),BOXED
                         PROMPT('Email Server'),AT(12,112),USE(?tmp:RemoteImportPath:Prompt:8)
                         PROMPT('Email Server Port'),AT(12,128),USE(?tmp:RemoteImportPath:Prompt:9)
                         ENTRY(@s255),AT(112,110,188,10),USE(locdef:EmailServer),FONT(,,,FONT:bold),MSG('EmailServer')
                         ENTRY(@s255),AT(112,142,188,10),USE(locdef:EmailFromAddress),FONT(,,,FONT:bold),MSG('EmailFromAddress')
                         PROMPT('"Email From" Address'),AT(12,144),USE(?tmp:RemoteImportPath:Prompt:10)
                         ENTRY(@s3),AT(112,126,188,10),USE(locdef:EmailServerPort),FONT(,,,FONT:bold),MSG('EmailServerPort')
                       END
                       GROUP('File Not Received Alert'),AT(4,168,304,32),USE(?Group6),BOXED
                         PROMPT('Send Alert After (Days)'),AT(12,182),USE(?Prompt11)
                         ENTRY(@s8),AT(112,180,188,10),USE(locdef:AlertDays),FONT(,,,FONT:bold),MSG('Alert Days')
                       END
                       GROUP('PC Control Systems Email Defaults'),AT(4,202,304,32),USE(?Group2:2),BOXED
                         PROMPT('Email Address'),AT(12,214),USE(?tmp:RemoteImportPath:Prompt:4)
                         ENTRY(@s255),AT(112,214,188,10),USE(locdef:PCCSEmailAddress),FONT(,,,FONT:bold),MSG('PCCSEmailAddress')
                       END
                       GROUP('Vodafone Email Defaults'),AT(4,242,304,32),USE(?Group2:3),BOXED
                         ENTRY(@s255),AT(112,254,188,10),USE(locdef:VodafoneEmailAddress),FONT(,,,FONT:bold),MSG('VodafoneEmailAddress')
                         PROMPT('Email Address'),AT(12,254),USE(?tmp:RemoteImportPath:Prompt:7)
                       END
                       BUTTON('OK'),AT(192,278,56,16),USE(?OK),DEFAULT,REQ
                       BUTTON('Cancel'),AT(252,278,56,16),USE(?Cancel)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:LOCDEFS.Open
    SET(LOCDEFS)
    CASE Access:LOCDEFS.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:LOCDEFS.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('FTPDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?locdef:FTPServer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddUpdateFile(Access:LOCDEFS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOCDEFS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOCDEFS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(LOCDEFS)
      Access:LOCDEFS.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCDEFS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

