

   MEMBER('OldIMEIImport.clw')                        ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('OLDIM001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:ImportFile       STRING(255),STATIC
tmp:SavePath         STRING(255)
window               WINDOW('IMEI File Import'),AT(,,143,68),FONT('Tahoma',,,),CENTER,GRAY,DOUBLE
                       BUTTON('Import File'),AT(29,6,84,36),USE(?Button:ImportFile),LEFT,ICON('import.gif')
                       BUTTON('Close'),AT(84,50,56,16),USE(?Close),LEFT,ICON('close.gif')
                     END

! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('wizcncl.ico'),HIDE
     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ImportFile    File,Driver('ASCII'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record              Record
IMEINumber              String(15)
Filler                  String(132)
ShipDate                String(10)
                    End
                End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    ?Prog:Cancel{prop:Hide} = 0
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button:ImportFile
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:IMEISHIP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:IMEISHIP.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button:ImportFile
      ThisWindow.Update
      tmp:SavePath = Path()
      tmp:ImportFile = ''
      If FileDialog('Choose File',tmp:ImportFile,'INT Files|*.int',file:KeepDir + file:NoError + File:LongName)
          !Found
          SetPath(tmp:SavePath)
      
          Count# = 0
          Setcursor(cursor:Wait)
          Open(ImportFile)
          Set(ImportFile,0)
          Loop
              Next(ImportFile)
              If Error()
                  Break
              End ! If Error()
              Count# += 1
          End ! Loop
          SetCursor()
      
          Do Prog:ProgressSetup
          Prog:TotalRecords = Count#
          Prog:ShowPercentage = 1 !Show Percentage Figure
      
          Clear(ImportFile)
          Set(ImportFile,0)
      
          Accept
              Case Event()
                  Of Event:Timer
                      Loop 25 Times
                          !Inside Loop
                          Next(ImportFile)
                          If Error()
                              Prog:Exit = 1
                              Break
                          End ! If Error()
      
                          Prog:RecordCount += 1
      
                          ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
      
                          Do Prog:UpdateScreen
      
                          Access:IMEISHIP.Clearkey(imei:IMEIKey)
                          imei:IMEINumber = impfil:IMEINumber
                          If Access:IMEISHIP.TryFetch(imei:IMEIKey) = Level:Benign
                              imei:IMEINumber = impfil:IMEINumber
                              imei:ShipDate = Deformat(impfil:Shipdate,@d06)
                              If Access:IMEISHIP.TryUpdate() = Level:Benign
                                  Count# += 1
                              End ! If Access:IMEISHIP.TryUpdate() = Level:Benign
                          Else ! If Access:IMEISHIP.TryFetch(imei:IMEIKey) = Level:Benign
                              If Access:IMEISHIP.PrimeRecord() = Level:Benign
                                  imei:IMEINumber = impfil:IMEINumber
                                  imei:ShipDate = Deformat(impfil:ShipDate,@d06)
                                  If Access:IMEISHIP.TryInsert() = Level:Benign
                                      Count# += 1
                                  End ! If Access:IMEISHIP.TryInsert() = Level:Benign
                              Else ! If Access:IMEISHIP.PrimeRecord() = Level:Benign
                                  Access:IMEISHIP.CancelAutoInc()
                              End ! If Access:IMEISHIP.PrimeRecord() = Level:Benign
                          End ! If Access:IMEISHIP.TryFetch(imei:IMEIKey) = Level:Benign
      
                      End ! Loop 25 Times
                  Of Event:CloseWindow
          !            Prog:Exit = 1
          !            Prog:Cancelled = 1
          !            Break
                  Of Event:Accepted
                      If Field() = ?Prog:Cancel
                          Beep(Beep:SystemQuestion)  ;  Yield()
                          Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                         icon:Question,'&Yes|&No',2,2)
                              Of 1 ! &Yes Button
                                  Prog:Exit = 1
                                  Prog:Cancelled = 1
                                  Break
                              Of 2 ! &No Button
                          End!Case Message
                      End ! If Field() = ?ProgressCancel
              End ! Case Event()
              If Prog:Exit
                  Break
              End ! If Prog:Exit
          End ! Accept
          Do Prog:ProgressFinished
      
          Close(ImportFile)
      
          Beep(Beep:SystemAsterisk)  ;  Yield()
          Case Message('Import Complete.','IMEI Import',|
                         icon:Asterisk,'&OK',1,1) 
              Of 1 ! &OK Button
          End!Case Message
      
      Else ! If FileDialog('Choose File',tmp:ImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
          !Error
          SetPath(tmp:SavePath)
      End ! If FileDialog('Choose File',tmp:ImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

