

   MEMBER('CollImportDefaults.clw')                   ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('COLLI001.INC'),ONCE        !Local module procedure declarations
                     END


FTPDefaults PROCEDURE                                 !Generated from procedure template - Window

tmp:RemoteServer     STRING(255)
tmp:RemoteUsername   STRING(255)
tmp:RemotePassword   STRING(255)
tmp:RemoteExportPath STRING(255)
tmp:RemoteImportPath STRING(255)
ActionMessage        CSTRING(40)
window               WINDOW('Defaults'),AT(,,311,210),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       GROUP('FTP Defaults'),AT(4,0,304,76),USE(?Group1),BOXED
                         ENTRY(@s255),AT(112,10,188,10),USE(coldef:FTPServer),FONT(,,,FONT:bold),MSG('FTPServer')
                         ENTRY(@s60),AT(112,26,188,10),USE(coldef:FTPUserName),FONT(,,,FONT:bold),MSG('FTPUserName')
                         ENTRY(@s60),AT(112,42,188,10),USE(coldef:FTPPassword),FONT(,,,FONT:bold),PASSWORD,MSG('FTPPassword')
                         ENTRY(@s255),AT(112,58,188,10),USE(coldef:FTPImportPath),FONT(,,,FONT:bold),MSG('FTPImportPath')
                       END
                       PROMPT('Server'),AT(12,12),USE(?tmp:RemoteServer:Prompt)
                       PROMPT('Username'),AT(12,28),USE(?tmp:RemoteUsername:Prompt)
                       PROMPT('Password'),AT(12,44),USE(?tmp:RemotePassword:Prompt)
                       PROMPT('Import Path'),AT(12,60),USE(?tmp:RemoteImportPath:Prompt)
                       GROUP('General Email Defaults'),AT(4,82,304,64),USE(?Group5),BOXED
                         ENTRY(@s255),AT(112,93,188,10),USE(coldef:EmailServer),FONT(,,,FONT:bold),MSG('EmailServer')
                         ENTRY(@s3),AT(112,109,188,10),USE(coldef:EmailServerPort),FONT(,,,FONT:bold),MSG('EmailServerPort')
                         ENTRY(@s255),AT(112,125,188,10),USE(coldef:EmailFromAddress),FONT(,,,FONT:bold),MSG('EmailFromAddress')
                         PROMPT('Email Server'),AT(12,94),USE(?tmp:RemoteImportPath:Prompt:8)
                         PROMPT('Email Server Port'),AT(12,110),USE(?tmp:RemoteImportPath:Prompt:9)
                         PROMPT('"Email From" Address'),AT(12,126),USE(?tmp:RemoteImportPath:Prompt:10)
                       END
                       GROUP('Vodafone Email Defaults'),AT(4,152,304,32),USE(?Group2:3),BOXED
                         ENTRY(@s255),AT(112,164,188,10),USE(coldef:VodafoneEmailAddress),FONT(,,,FONT:bold),MSG('VodafoneEmailAddress')
                         PROMPT('Email Address'),AT(12,164),USE(?tmp:RemoteImportPath:Prompt:7)
                       END
                       BUTTON('OK'),AT(192,190,56,16),USE(?OK),DEFAULT,REQ
                       BUTTON('Cancel'),AT(252,190,56,16),USE(?Cancel)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:COLDEFS.Open
    SET(COLDEFS)
    CASE Access:COLDEFS.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:COLDEFS.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('FTPDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?coldef:FTPServer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:COLDEFS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:COLDEFS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:COLDEFS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(COLDEFS)
      Access:COLDEFS.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COLDEFS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

