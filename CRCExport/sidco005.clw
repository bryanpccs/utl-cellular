

   MEMBER('sidcoll.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SIDCO005.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SIDCO001.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SIDCO007.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

Local                CLASS
ImportJobs           Procedure(String func:FileName,String func:ShortName)
                     END
tmp:ImportFile       CSTRING(255),STATIC
ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
szDirectory          CSTRING(260)
window               WINDOW('SID Collection Import'),AT(,,69,15),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       PROMPT('FTP In Progress...'),AT(4,0),USE(?Prompt1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ImportFile          file, driver('BASIC'), name(tmp:ImportFile), pre(impfil), bindable, thread
Record                  record
CollectionDate              string(20)
SIDJobNo                    long
                        end
                    end
COLDEFS              FILE,DRIVER('Btrieve'),OEM,NAME('COLDEFS.DAT'),PRE(coldef),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(coldef:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
FTPServer                   STRING(255)
FTPUserName                 STRING(60)
FTPPassword                 STRING(60)
FTPImportPath               STRING(255)
EmailServer                 STRING(255)
EmailServerPort             STRING(3)
EmailFromAddress            STRING(255)
CRCEmailName                STRING(60)
CRCEmailAddress             STRING(255)
PCCSEmailName               STRING(60)
PCCSEmailAddress            STRING(255)
VodafoneEmailName           STRING(60)
VodafoneEmailAddress        STRING(255)
LastImportDate              DATE
                         END
                     END
! Dummy SQL table
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD
SQLRefNumber                Long, Name('Ref_Number')
SQLUnitReturned             Byte, Name('UnitReturned')
                        END
                    END

SQLUpdate           FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(UPD),BINDABLE,THREAD
Record                  RECORD
SQLRefNumber                Long, Name('Ref_Number')
                        END
                    END
TempFiles   Queue(file:Queue),pre(fil)
            End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  ! A scheduled task to import a file via ftp that contains a list of SID job numbers and dates when the customer
  ! collected their handset. If a store has not updated a job to the status of '940 PHONE RETURNED TO CUSTOMER'
  ! through SID then this import procedure, through the wonders of modern science will do it for them.
  
  ! Much of the code has been stolen/borrowed from wilfredtosb.app
  
  ! Fetch defaults table (defined in colldef.dct)
  if exists(clip(path()) & '\COLDEFS.DAT')
      share(COLDEFS)
  
      if not error()
          set(COLDEFS, 0)
          next(COLDEFS)
  
          ! Set global variables as the file declaration is defined in source code,
          ! We can then get away with just one table declaration in this procedure.
          glo:FTPServer = coldef:FTPServer
          glo:FTPUserName = coldef:FTPUserName
          glo:FTPPassword = coldef:FTPPassword
          glo:FTPImportPath = coldef:FTPImportPath
  
          glo:EmailServer = coldef:EmailServer
          glo:EmailServerPort = coldef:EmailServerPort
          glo:EmailFromAddress = coldef:EmailFromAddress
  
          glo:VodafoneEmailAddress = coldef:VodafoneEmailAddress
      end
  
      close(COLDEFS)
  end
  
  Free(FileQueue)
  Clear(FileQueue)
  
  ErrorLog('')
  ErrorLog('SIDCOLL Started===========================')
  
  !Start - Create folders  (DBH: 23-11-2004)
  szDirectory = Clip(Path()) & '\CollectionDownloaded' & '<0>'
  If ~Exists(szDirectory)
      If MkDir(szDirectory)
    
      End
  End
    
  szDirectory = Clip(Path()) & '\CollectionImported' & '<0>'
  If ~Exists(szDirectory)
      If MkDir(szDirectory)
  
      End
  End
  
  Loop Until FTP() = 1
  
  End ! Until FTP() = 1
  
  ErrorLog('FTP process: Completed')
  
  Directory(TempFiles,Clip(Path()) & '\CollectionDownloaded\*.csv',ff_:DIRECTORY)
  Loop x# = 1 To Records(TempFiles)
      Get(TempFiles,x#)
      If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
          Cycle
      Else !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
          ErrorLog('Importing: ' & Clip(fil:Name) & ' - ' & Round(fil:Size/1024,.01) & 'kb')
          Local.ImportJobs(Clip(Path()) & '\CollectionDownloaded\' & Clip(fil:name),Clip(fil:Name))
      End !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
  End !x# = 1 To Records(ScanFiles)
  
  !Loop x# = 1 To Records(FileQueue)
  !    Get(FileQueue,x#)
  !    ErrorLog('Begin Importing: ' & Clip(filque:FileName))
  !    Local.ImportJobs(filque:FileName,filque:ShortName)
  !End ! x# = 1 To Records(FileQueue)
  
  ErrorLog('SIDCOLL Finished==========================')
  
  Post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

Local.ImportJobs        Procedure(String func:FileName, String func:ShortName)
local:UpdateStatus      Byte(0)
local:JobUpdated        Byte(0)
local:collectionDate    date
LogDynStr               &IDynStr
Code

    ! Log file text, dynamic string as we don't know the length
    LogDynStr &= NewDynStr()

    Relate:JOBS.Open()
    Relate:AUDIT.Open()
    Relate:STATUS.Open()
    Relate:AUDSTATS.Open()
    Relate:STAHEAD.Open()
    Relate:JOBSTAGE.Open()
    Relate:DEFAULTS.Open()

    IniFilepath =  Clip(Path()) & '\Webimp.ini'
    ConnectionStr = GetIni('Defaults', 'Connection', '', Clip(IniFilepath))

    Open(SQLFile)
    Open(SQLUpdate)

    CountNormal# = 0
    CountStatus# = 0

    ! Defaults table used by GetStatusAltDate procedure
    Set(DEFAULTS, 0)
    Access:DEFAULTS.Next()

    LogDynStr.Cat('Importing File: ' & func:FileName & '<13,10>')
    LogDynStr.Cat('Date: ' & format(today(), @d06b) & '<13,10>')
    LogDynStr.Cat('Time: ' & format(clock(), @t06b) & '<13,10,13,10>')

    tmp:ImportFile = func:FileName
    Open(ImportFile)
    Set(ImportFile,0)
    Loop
        Next(ImportFile)
        If Error()
            Break
        End !If Error()

        CountNormal# += 1

        if impfil:SIDJobNo = ''
            ErrorLog('SID Job Number Not Specified')
            LogDynStr.Cat('SID Job Number Not Specified' & '<13,10>')
            cycle
        end

        local:collectionDate = deformat(impfil:CollectionDate, @d06)

        if local:collectionDate = ''
            ErrorLog('Collection Date Not Recognised: ' & impfil:CollectionDate)
            LogDynStr.Cat('Collection Date Not Recognised: ' & impfil:CollectionDate & '<13,10>')
            cycle
        end

        !SQLFile{Prop:SQL} = 'SELECT SBJobNo, UnitReturned FROM WebJob WHERE Ref_Number = ' & impfil:SIDJobNo

!        next(SQLFile)
!        if error()
!            ErrorLog('SID Job Number Not Recognised: ' & impfil:SIDJobNo)
!            LogDynStr.Cat('SID Job Number Not Recognised: ' & impfil:SIDJobNo & '<13,10>')
!            cycle
!        end

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = impfil:SIDJobNo
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Found
            local:JobUpdated = False
            local:UpdateStatus = True

            Case Sub(job:Current_Status,1,3)
                of '930' ! Arrived Back At Store
                of '940' ! Phone Returned To Customer
                    local:UpdateStatus = False
                    ErrorLog('Job Already At "Customer Collected Status", SID Job Number: ' & impfil:SIDJobNo)
                    LogDynStr.Cat('Job Already At "Customer Collected Status", SID Job Number: ' & impfil:SIDJobNo & '<13,10>')
                    if (SQL:SQLUnitReturned = 0)
                        SQLUpdate{Prop:SQL} = 'UPDATE WEBJOB SET UnitReturned = 1, DateUnitReturned = ''' & format(local:collectionDate, @D10-) & ''' WHERE Ref_Number = ' & impfil:SIDJobNo
                    end
                    cycle
                else
                    ! Request by Nick, don't log these jobs because we are still updating them to the new status
                    !ErrorLog('Job Not In The "Received At Store Status", SID Job Number: ' & impfil:SIDJobNo)
                    !LogDynStr.Cat('Job Not In The "Received At Store Status", SID Job Number: ' & impfil:SIDJobNo & '<13,10>')
            End ! Case Sub(job:Current_Status,1,3)

            If local:UpdateStatus = True
                GetStatusAltDate('940', 0, 'JOB', local:collectionDate)
                local:JobUpdated = True
                SQLUpdate{Prop:SQL} = 'UPDATE WEBJOB SET UnitReturned = 1, DateUnitReturned = ''' & format(local:collectionDate, @D10-) & ''' WHERE Ref_Number = ' & impfil:SIDJobNo
                CountStatus# += 1
            End ! If local:UpdateStatus = True

            Access:JOBS.Update()

            !The job has been updated by wilfred - TrkBs:  (DBH: 02-02-2005)
            If local:JobUpdated = True
                If Access:AUDIT.PrimeRecord() = Level:Benign
                    aud:Notes         = 'IMPORT FILE: ' & Clip(func:ShortName)
                    aud:Ref_Number    = job:ref_number
                    aud:Date          = Today()
                    aud:Time          = Clock()
                    aud:Type          = 'JOB'
                    aud:User          = 'WEB'
                    aud:Action        = 'JOB UPDATED BY SID COLLECTION IMPORT'
                    Access:AUDIT.Insert()
                End!If Access:AUDIT.PrimeRecord() = Level:Benign
            End ! If local:JobUpdated = True

        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

    End !Loop(ImportFile)
    Close(ImportFile)
    Close(SQLFile)
    Relate:JOBS.Close()
    Relate:AUDIT.Close()
    Relate:STATUS.Close()
    Relate:AUDSTATS.Close()
    Relate:STAHEAD.Close()
    Relate:JOBSTAGE.Close()
    Relate:DEFAULTS.Close()

    ErrorLog('Total Records In File: ' & CountNormal#)
    LogDynStr.Cat('<13,10>' & 'Total Records In File: ' & CountNormal# & '<13,10>')
    ErrorLog('Total Jobs Updated: ' & CountStatus#)
    LogDynStr.Cat('Total Jobs Updated: ' & CountStatus# & '<13,10>')

    ! Email the log to an administrator
    SendEmailLog(LogDynStr)

    LogDynStr.Kill()

    DisposeDynStr(LogDynStr)

    Rename(Clip(Path()) & '\CollectionDownloaded\' & Clip(func:ShortName),Clip(Path()) & '\CollectionImported\' & Clip(func:ShortName))

    iF Error()
        Case Error()
            Of ''
                Error" = 'Same File Already Exists'
            Of 2
                Error" = 'File Not Found'
            Of 5
                Error" = 'Access Denied'
            Of 52
                Error" = 'File Aready Open'
            Else
                Error" = Error()
        End ! Case Error()
        ErrorLog('Rename Error (' & Clip(Error") & '): ' & Clip(func:ShortName))
    Else ! If Error()
        ErrorLog('File Placed in ''Sent'' folder')
    End ! If Error()
    !End   - Debug - TrkBs:  (DBH: 25-01-2005)

