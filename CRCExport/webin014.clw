

   MEMBER('webint.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBIN014.INC'),ONCE        !Local module procedure declarations
                     END


TurnaroundAtSC       PROCEDURE  (scAccountID, strBookingType, strDate, strTimeFrame, strModelNo, strPath) ! Declare Procedure
startDate   date
result      long
turnaroundDays long

nwDaysQ     queue, pre(nwq)
nwDate          date
            end

i           long
counter     long
    map
FindNextFreeDate procedure(*cstring strAccount, date currentDate), date
FindNextFreeDateAtSC procedure(long scID, date currentDate), date
    end
  CODE
    !
    ! Description : Get the number of transit days from the store to the service centre
    !

    if strDate <> '' and exists(clip(strPath) & '\SIDSRVCN.DAT')
        ! Change the paths this way
        SIDSRVCN{Prop:Name} = clip(strPath) & '\SIDSRVCN.DAT'
        SIDDEF{Prop:Name} = clip(strPath) & '\SIDDEF.DAT'
        SIDNWDAY{Prop:Name} = clip(strPath) & '\SIDNWDAY.DAT'
        SIDMODTT{Prop:Name} = clip(strPath) & '\SIDMODTT.DAT'

        Access:SIDSRVCN.Open()
        Access:SIDSRVCN.UseFile()

        Access:SIDDEF.Open()
        Access:SIDDEF.UseFile()

        Access:SIDNWDAY.Open()
        Access:SIDNWDAY.UseFile()

        Access:SIDMODTT.Open()
        Access:SIDMODTT.UseFile()

        startDate = deformat(strDate, @d06b)

        Access:SIDSRVCN.ClearKey(srv:SCAccountIDKey)
        srv:SCAccountID = scAccountID
        if Access:SIDSRVCN.Fetch(srv:SCAccountIDKey) = Level:Benign
            if strBookingType = 'R'
                turnaroundDays = srv:TurnaroundDaysRetail
            end
            if strBookingType = 'E'
                turnaroundDays = srv:TurnaroundDaysExch
            end

            if clip(strModelNo) <> ''
                Access:SIDMODTT.ClearKey(smt:ModelNoKey)
                smt:ModelNo = strModelNo
                if Access:SIDMODTT.Fetch(smt:ModelNoKey) = Level:Benign
                    if smt:TurnaroundException
                        if strBookingType = 'R'
                            turnaroundDays = smt:TurnaroundDaysRetail
                        end
                        if strBookingType = 'E'
                            turnaroundDays = smt:TurnaroundDaysExch
                        end
                    end
                end
            end

        else
            ! Fetch unallocated model default
            set(SIDDEF, 0)
            if Access:SIDDEF.Next() = Level:Benign
                turnaroundDays = SID:UnallocTurnaround
            end
        end

        ! Cannot be zero
        if turnaroundDays = 0 then turnaroundDays = 1. ! Default

        Access:SIDNWDAY.ClearKey(nwd:SCDayKey)
        nwd:SCAccountID = scAccountID
        set(nwd:SCDayKey, nwd:SCDayKey)
        loop
            if Access:SIDNWDAY.Next() <> Level:Benign then break.
            if nwd:SCAccountID <> scAccountID then break.
            nwDaysQ.nwDate = nwd:NonWorkingDay
            add(nwDaysQ)
        end

        if strTimeFrame = 'DAILY'
            ! When an intermittent timeframe of �Daily� has been identified,
            ! the earliest available collection date calculation will be adjusted to
            ! include one additional day at the service centre for the extended testing
            ! of the reported intermittent fault.
            turnaroundDays += 1
        else
            if strTimeFrame = 'WEEKLY'
                ! When an intermittent timeframe of �Weekly� has been identified, the earliest
                ! available collection date will be adjusted to include seven additional days
                ! at the service centre for the extended testing of the reported intermittent fault.
                turnaroundDays += 7
            end
        end

        if strBookingType = 'R'
            ! Retail - Unit arrives in the morning so we get a transit day
            counter = turnaroundDays - 1
        else
            counter = turnaroundDays
        end

        loop counter times
            if records(nwDaysQ) > 0
                loop i = 1 to records(nwDaysQ)
                    get(nwDaysQ, i)
                    if error() then break.
                    if startDate = nwDaysQ.nwDate
                        ! Non working day
                        startDate += 1
                    end
                end
            end
            startDate = FindNextFreeDateAtSC(scAccountID, startDate + 1)
        end ! loop


        ! If the turnaround days was one then the loop above would have been skipped,
        ! Still need to check the current day is not a non working day
        if records(nwDaysQ) > 0
            loop i = 1 to records(nwDaysQ)
                get(nwDaysQ, i)
                if error() then break.
                if startDate = nwDaysQ.nwDate
                    ! Non working day
                    startDate = FindNextFreeDateAtSC(scAccountID, startDate + 1)
                end
            end
        end

        strDate = format(startDate, @d06b)

        Access:SIDSRVCN.Close()
        Access:SIDDEF.Close()
        Access:SIDNWDAY.Close()
        Access:SIDMODTT.Close()

        result = true
    end

    return result
FindNextFreeDate procedure(*cstring strAccount, date currentDate)
    !
    ! Description : Find the next free date (Just a wrapper for FindFreeDate)
    !

strNext          cstring('+')
strDateToProcess cstring(30)
    code

    strDateToProcess = format(currentDate, @d06b)

    FindFreeDate(strNext, strAccount, strDateToProcess, strPath)

    return deformat(strDateToProcess, @d06b)
FindNextFreeDateAtSC procedure(long scID, date currentDate)
    !
    ! Description : Find the next free date (Just a wrapper for FindFreeDateAtSC)
    !

strNext          cstring('+')
strDateToProcess cstring(30)
    code

    strDateToProcess = format(currentDate, @d06b)

    FindFreeDateAtSC(strNext, scID, strDateToProcess, strPath)

    return deformat(strDateToProcess, @d06b)
