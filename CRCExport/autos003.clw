

   MEMBER('autostat.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('AUTOS003.INC'),ONCE        !Local module procedure declarations
                     END


ErrorLog             PROCEDURE  (func:ErrorLog)       ! Declare Procedure
tmp:ErrorLog         CSTRING(255),STATIC
ErrorLog    File,Driver('BASIC'),Pre(errlog),Name(tmp:ErrorLog),Create,Bindable,Thread
Record                  Record
TheDate                 String(10)
TheTime                 String(10)
Line                    String(1000)
                        End
                    End
  CODE
    tmp:ErrorLog = Clip(Path()) & '\AUTOSTATUSCHANGE.LOG'
    Open(ErrorLog)
    If Error()
        Create(ErrorLog)
        Open(ErrorLog)
    End ! If Error()

    If Clip(func:ErrorLog) = ''
        Clear(errlog:Record)
    Else ! If Clip(func:ErrorLog) = ''
        errlog:TheDate = Format(Today(),@d06)
        errlog:TheTime = Format(Clock(),@t01)
        errlog:Line    = Clip(func:ErrorLog)
    End ! If Clip(func:ErrorLog) = ''

    Add(ErrorLog)
    Close(ErrorLog)
