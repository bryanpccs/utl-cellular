

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02018.INC'),ONCE        !Local module procedure declarations
                     END


Import_Logistics PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
Stock_Type_Temp      STRING(30)
location_temp        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOGEXHE)
                       PROJECT(log1:Batch_No)
                       PROJECT(log1:Date)
                       PROJECT(log1:SMPF_No)
                       PROJECT(log1:ModelNumber)
                       PROJECT(log1:Qty)
                       PROJECT(log1:Processed)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
log1:Batch_No          LIKE(log1:Batch_No)            !List box control field - type derived from field
log1:Date              LIKE(log1:Date)                !List box control field - type derived from field
log1:SMPF_No           LIKE(log1:SMPF_No)             !List box control field - type derived from field
log1:ModelNumber       LIKE(log1:ModelNumber)         !List box control field - type derived from field
log1:Qty               LIKE(log1:Qty)                 !List box control field - type derived from field
log1:Processed         LIKE(log1:Processed)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK2::log1:Processed       LIKE(log1:Processed)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Import Exchange Stock From Logistics'),AT(,,444,188),FONT('MS Sans Serif',8,,),CENTER,IMM,HLP('Import_Logistics'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,36,348,144),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('64L(2)|M~Batch No~@n-14@80L(2)|M~Date~@d17@80L(2)|M~SMPF Number~@s30@80L(2)|M~Mo' &|
   'delNumber~@s30@64L(2)|M~Qty~@n-14@'),FROM(Queue:Browse:1)
                       BUTTON('Import'),AT(364,35,76,20),USE(?Button3),LEFT,ICON('book.ico')
                       BUTTON('&Delete'),AT(364,136,76,20),USE(?Delete:3),LEFT,ICON('delete.gif')
                       SHEET,AT(4,4,356,180),USE(?CurrentTab),SPREAD
                         TAB('Import Exchange Stock From Logistics'),USE(?Tab:2)
                           PROMPT('Batch No.'),AT(136,20),USE(?LOG1:Batch_No:Prompt),FONT(,,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@n-14b),AT(8,20,124,10),USE(log1:Batch_No),LEFT(2),FONT(,,,FONT:bold)
                         END
                       END
                       BUTTON('Close'),AT(364,164,76,20),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
! progress bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
               double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?LOG1:Batch_No:Prompt{prop:FontColor} = -1
    ?LOG1:Batch_No:Prompt{prop:Color} = 15066597
    If ?log1:Batch_No{prop:ReadOnly} = True
        ?log1:Batch_No{prop:FontColor} = 65793
        ?log1:Batch_No{prop:Color} = 15066597
    Elsif ?log1:Batch_No{prop:Req} = True
        ?log1:Batch_No{prop:FontColor} = 65793
        ?log1:Batch_No{prop:Color} = 8454143
    Else ! If ?log1:Batch_No{prop:Req} = True
        ?log1:Batch_No{prop:FontColor} = 65793
        ?log1:Batch_No{prop:Color} = 16777215
    End ! If ?log1:Batch_No{prop:Req} = True
    ?log1:Batch_No{prop:Trn} = 0
    ?log1:Batch_No{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      ROUTINE   !Put Into Calculation Loop
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end

endprintrun         ROUTINE    !Put At End Of Loop
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()






! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Import_Logistics')
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:LOGEXCH.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOGEXHE,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,log1:Processed_Key)
  BRW1.AddRange(log1:Processed)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?LOG1:Batch_No,log1:Batch_No,1,BRW1)
  BRW1.AddField(log1:Batch_No,BRW1.Q.log1:Batch_No)
  BRW1.AddField(log1:Date,BRW1.Q.log1:Date)
  BRW1.AddField(log1:SMPF_No,BRW1.Q.log1:SMPF_No)
  BRW1.AddField(log1:ModelNumber,BRW1.Q.log1:ModelNumber)
  BRW1.AddField(log1:Qty,BRW1.Q.log1:Qty)
  BRW1.AddField(log1:Processed,BRW1.Q.log1:Processed)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:LOGEXCH.Close
  END
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateLOGEXHE
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    
    
    CASE ACCEPTED()
    OF ?Button3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      BRW1.UpdateBuffer()
      Case MessageEx('Are you sure you wish to import Batch No. '&FORMAT(LOG1:Batch_No,@n06)&' into the Exchange Database?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
             Confirm_import(Stock_Type_Temp,Location_temp)
             RecordsPerCycle     = 25
             RecordsProcessed    = 0
             PercentProgress     = 0
             Setcursor(cursor:wait)
             Open(ProgressWindow)
             Progress:Thermometer    = 0
             ?Progress:PctText{prop:text} = '0% Completed'
             temp_batch# =  LOG1:Batch_No
             Access:LogExHe.ClearKey(LOG1:Batch_Number_Key)
             LOG1:Batch_No = temp_batch#
             Access:LogExHe.Fetch(LOG1:Batch_Number_Key)
             RecordsToProcess    =   LOG1:Qty
      
             
      
            Access:LogExch.ClearKey(xch1:Batch_Number_Key)
            xch1:Batch_Number = LOG1:Batch_No
            SET(xch1:Batch_Number_Key,xch1:Batch_Number_Key)
            LOOP
              IF Access:LogExch.Next()
                BREAK
              END
              IF xch1:Batch_Number <> LOG1:Batch_No
                BREAK
              END
              DO GetNextRecord2
              Access:Exchange.PrimeRecord()
              xch:Model_Number   = xch1:Model_Number
              xch:Manufacturer   = xch1:Manufacturer
              xch:ESN            = xch1:ESN
              xch:MSN            = xch1:MSN
              xch:Colour         = xch1:Colour
              xch:Location       = location_temp
              xch:Shelf_Location = xch1:Shelf_Location
              xch:Date_Booked    = TODAY()
              xch:Times_Issued   = ''
              xch:Available      = 'AVL'
              xch:Job_Number     = xch1:Job_Number
              xch:Stock_Type     = Stock_Type_Temp
              xch:Audit_Number   = ''
              Access:Exchange.Update()
            END
            Access:LogExHe.ClearKey(LOG1:Batch_Number_Key)
            LOG1:Batch_No = temp_batch#
            Access:LogExHe.Fetch(LOG1:Batch_Number_Key)
            LOG1:Processed = 'Y'
            IF Access:LogExHe.Update()
              !Error
            END
            Setcursor()
            Close(ProgressWindow)
            BRW1.ResetSort(1)
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    OF ?Delete:3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete:3, Accepted)
      Case MessageEx('Are you sure you want to delete this entire Batch?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          Of 2 ! &No Button
            CYCLE
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete:3, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?log1:Batch_No
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = 'N'
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

