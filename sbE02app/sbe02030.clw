

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02030.INC'),ONCE        !Local module procedure declarations
                     END


Add_Exchange_Audit_Numbers PROCEDURE (f_stock_type)   !Generated from procedure template - Window

FilesOpened          BYTE
new_audit_numbers_temp LONG(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Add Exchange Audit Numbers'),AT(,,188,92),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,180,56),USE(?Sheet1),SPREAD
                         TAB('Add Exchange Audit Numbers'),USE(?Tab1)
                           PROMPT('How many new Audit Numbers do you want to add?'),AT(8,24),USE(?Prompt1)
                           PROMPT('New Audit Numbers'),AT(20,40),USE(?new_audit_numbers_temp:Prompt),TRN
                           SPIN(@n8),AT(92,40,64,10),USE(new_audit_numbers_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,STEP(1)
                         END
                       END
                       BUTTON('&OK'),AT(68,68,56,16),USE(?OkButton),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(124,68,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,64,180,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?new_audit_numbers_temp:Prompt{prop:FontColor} = -1
    ?new_audit_numbers_temp:Prompt{prop:Color} = 15066597
    If ?new_audit_numbers_temp{prop:ReadOnly} = True
        ?new_audit_numbers_temp{prop:FontColor} = 65793
        ?new_audit_numbers_temp{prop:Color} = 15066597
    Elsif ?new_audit_numbers_temp{prop:Req} = True
        ?new_audit_numbers_temp{prop:FontColor} = 65793
        ?new_audit_numbers_temp{prop:Color} = 8454143
    Else ! If ?new_audit_numbers_temp{prop:Req} = True
        ?new_audit_numbers_temp{prop:FontColor} = 65793
        ?new_audit_numbers_temp{prop:Color} = 16777215
    End ! If ?new_audit_numbers_temp{prop:Req} = True
    ?new_audit_numbers_temp{prop:Trn} = 0
    ?new_audit_numbers_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Add_Exchange_Audit_Numbers',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Add_Exchange_Audit_Numbers',1)
    SolaceViewVars('new_audit_numbers_temp',new_audit_numbers_temp,'Add_Exchange_Audit_Numbers',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?new_audit_numbers_temp:Prompt;  SolaceCtrlName = '?new_audit_numbers_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?new_audit_numbers_temp;  SolaceCtrlName = '?new_audit_numbers_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Add_Exchange_Audit_Numbers')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Add_Exchange_Audit_Numbers')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXCAUDIT.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCAUDIT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Add_Exchange_Audit_Numbers',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      Compile('***',Debug=1)
          message('f_stock_type: '&f_stock_type,'Debug Message', icon:exclamation)
      ***
      access:excaudit.clearkey(exa:audit_number_key)
      start# = 0
      exa:stock_type = f_stock_type
      set(exa:audit_number_key,exa:audit_number_key)
      Loop
          If access:excaudit.next()
              Break
          End!If access:excaudit.next()
          If exa:stock_type   <> f_stock_type
              Break
          End!If exa:stock_type   <> f_stock_type
          start# = exa:audit_number
      End!Loop
      Compile('***',Debug=1)
          message('start#: '&start#,'Debug Message', icon:exclamation)
      ***
      
      Loop x# = 1 To new_audit_numbers_temp
          number# = start# + x#
          get(excaudit,0)
          if access:excaudit.primerecord() = Level:Benign
              exa:stock_type              = f_stock_type
              exa:audit_number            = number#
              exa:stock_unit_number       = ''
              exa:replacement_unit_number = ''
      Compile('***',Debug=1)
          message('Adding:|exa:stock:type: ' & exa:stock_type & '|exa:audit_number: ' &exa:audit_number,'Debug Message', icon:exclamation)
      ***
              If access:excaudit.tryinsert()
                  access:excaudit.cancelautoinc()
              End!If access:excaudit.tryinsert()
          end!if access:excaudit.primerecord() = Level:Benign
      End!Loop x# = 1 To new_audit_numbers_temp
      Post(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Add_Exchange_Audit_Numbers')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

