

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02017.INC'),ONCE        !Local module procedure declarations
                     END


UpdateLOGEXHE PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::log1:Record LIKE(log1:RECORD),STATIC
QuickWindow          WINDOW('Update the LOGEXHE File'),AT(,,193,112),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateLOGEXHE'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,185,86),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Batch No:'),AT(8,20),USE(?LOG1:Batch_No:Prompt)
                           ENTRY(@n-14),AT(61,20,64,10),USE(log1:Batch_No),RIGHT(1)
                           PROMPT('Date:'),AT(8,34),USE(?LOG1:Date:Prompt)
                           ENTRY(@d17),AT(61,34,104,10),USE(log1:Date)
                           PROMPT('SMPF Number'),AT(8,48),USE(?LOG1:SMPF_No:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(61,48,124,10),USE(log1:SMPF_No),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('ModelNumber'),AT(8,62),USE(?LOG1:ModelNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(61,62,124,10),USE(log1:ModelNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Qty:'),AT(8,76),USE(?LOG1:Qty:Prompt)
                           ENTRY(@n-14),AT(61,76,64,10),USE(log1:Qty),RIGHT(1)
                         END
                       END
                       BUTTON('OK'),AT(46,94,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(95,94,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(144,94,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?LOG1:Batch_No:Prompt{prop:FontColor} = -1
    ?LOG1:Batch_No:Prompt{prop:Color} = 15066597
    If ?log1:Batch_No{prop:ReadOnly} = True
        ?log1:Batch_No{prop:FontColor} = 65793
        ?log1:Batch_No{prop:Color} = 15066597
    Elsif ?log1:Batch_No{prop:Req} = True
        ?log1:Batch_No{prop:FontColor} = 65793
        ?log1:Batch_No{prop:Color} = 8454143
    Else ! If ?log1:Batch_No{prop:Req} = True
        ?log1:Batch_No{prop:FontColor} = 65793
        ?log1:Batch_No{prop:Color} = 16777215
    End ! If ?log1:Batch_No{prop:Req} = True
    ?log1:Batch_No{prop:Trn} = 0
    ?log1:Batch_No{prop:FontStyle} = font:Bold
    ?LOG1:Date:Prompt{prop:FontColor} = -1
    ?LOG1:Date:Prompt{prop:Color} = 15066597
    If ?log1:Date{prop:ReadOnly} = True
        ?log1:Date{prop:FontColor} = 65793
        ?log1:Date{prop:Color} = 15066597
    Elsif ?log1:Date{prop:Req} = True
        ?log1:Date{prop:FontColor} = 65793
        ?log1:Date{prop:Color} = 8454143
    Else ! If ?log1:Date{prop:Req} = True
        ?log1:Date{prop:FontColor} = 65793
        ?log1:Date{prop:Color} = 16777215
    End ! If ?log1:Date{prop:Req} = True
    ?log1:Date{prop:Trn} = 0
    ?log1:Date{prop:FontStyle} = font:Bold
    ?LOG1:SMPF_No:Prompt{prop:FontColor} = -1
    ?LOG1:SMPF_No:Prompt{prop:Color} = 15066597
    If ?log1:SMPF_No{prop:ReadOnly} = True
        ?log1:SMPF_No{prop:FontColor} = 65793
        ?log1:SMPF_No{prop:Color} = 15066597
    Elsif ?log1:SMPF_No{prop:Req} = True
        ?log1:SMPF_No{prop:FontColor} = 65793
        ?log1:SMPF_No{prop:Color} = 8454143
    Else ! If ?log1:SMPF_No{prop:Req} = True
        ?log1:SMPF_No{prop:FontColor} = 65793
        ?log1:SMPF_No{prop:Color} = 16777215
    End ! If ?log1:SMPF_No{prop:Req} = True
    ?log1:SMPF_No{prop:Trn} = 0
    ?log1:SMPF_No{prop:FontStyle} = font:Bold
    ?LOG1:ModelNumber:Prompt{prop:FontColor} = -1
    ?LOG1:ModelNumber:Prompt{prop:Color} = 15066597
    If ?log1:ModelNumber{prop:ReadOnly} = True
        ?log1:ModelNumber{prop:FontColor} = 65793
        ?log1:ModelNumber{prop:Color} = 15066597
    Elsif ?log1:ModelNumber{prop:Req} = True
        ?log1:ModelNumber{prop:FontColor} = 65793
        ?log1:ModelNumber{prop:Color} = 8454143
    Else ! If ?log1:ModelNumber{prop:Req} = True
        ?log1:ModelNumber{prop:FontColor} = 65793
        ?log1:ModelNumber{prop:Color} = 16777215
    End ! If ?log1:ModelNumber{prop:Req} = True
    ?log1:ModelNumber{prop:Trn} = 0
    ?log1:ModelNumber{prop:FontStyle} = font:Bold
    ?LOG1:Qty:Prompt{prop:FontColor} = -1
    ?LOG1:Qty:Prompt{prop:Color} = 15066597
    If ?log1:Qty{prop:ReadOnly} = True
        ?log1:Qty{prop:FontColor} = 65793
        ?log1:Qty{prop:Color} = 15066597
    Elsif ?log1:Qty{prop:Req} = True
        ?log1:Qty{prop:FontColor} = 65793
        ?log1:Qty{prop:Color} = 8454143
    Else ! If ?log1:Qty{prop:Req} = True
        ?log1:Qty{prop:FontColor} = 65793
        ?log1:Qty{prop:Color} = 16777215
    End ! If ?log1:Qty{prop:Req} = True
    ?log1:Qty{prop:Trn} = 0
    ?log1:Qty{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateLOGEXHE',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateLOGEXHE',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateLOGEXHE',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOG1:Batch_No:Prompt;  SolaceCtrlName = '?LOG1:Batch_No:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?log1:Batch_No;  SolaceCtrlName = '?log1:Batch_No';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOG1:Date:Prompt;  SolaceCtrlName = '?LOG1:Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?log1:Date;  SolaceCtrlName = '?log1:Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOG1:SMPF_No:Prompt;  SolaceCtrlName = '?LOG1:SMPF_No:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?log1:SMPF_No;  SolaceCtrlName = '?log1:SMPF_No';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOG1:ModelNumber:Prompt;  SolaceCtrlName = '?LOG1:ModelNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?log1:ModelNumber;  SolaceCtrlName = '?log1:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOG1:Qty:Prompt;  SolaceCtrlName = '?LOG1:Qty:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?log1:Qty;  SolaceCtrlName = '?log1:Qty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a LOGEXHE Record'
  OF ChangeRecord
    ActionMessage = 'Changing a LOGEXHE Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateLOGEXHE')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateLOGEXHE')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOG1:Batch_No:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(log1:Record,History::log1:Record)
  SELF.AddHistoryField(?log1:Batch_No,1)
  SELF.AddHistoryField(?log1:Date,3)
  SELF.AddHistoryField(?log1:SMPF_No,4)
  SELF.AddHistoryField(?log1:ModelNumber,5)
  SELF.AddHistoryField(?log1:Qty,6)
  SELF.AddUpdateFile(Access:LOGEXHE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOGEXHE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOGEXHE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGEXHE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateLOGEXHE',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateLOGEXHE')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

