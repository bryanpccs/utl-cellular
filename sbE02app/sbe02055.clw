

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBE02055.INC'),ONCE        !Local module procedure declarations
                     END


ErrorLog             PROCEDURE  (func:ErrorLog, func:FileName) ! Declare Procedure
tmp:ErrorLog         CSTRING(255),STATIC
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
ErrorLog    File,Driver('BASIC'),Pre(errlog),Name(tmp:ErrorLog),Create,Bindable,Thread
Record                  Record
TheDate                 String(10)
TheTime                 String(10)
Line                    String(1000)
                        End
                    End
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'ErrorLog')      !Add Procedure to Log
  end


    tmp:ErrorLog = func:FileName
    Open(ErrorLog)
    If Error()
        Create(ErrorLog)
        Open(ErrorLog)
    End ! If Error()

    If Clip(func:ErrorLog) = ''
        Clear(errlog:Record)
    Else ! If Clip(func:ErrorLog) = ''
        errlog:TheDate = Format(Today(),@d06)
        errlog:TheTime = Format(Clock(),@t01)
        errlog:Line    = Clip(func:ErrorLog)
    End ! If Clip(func:ErrorLog) = ''

    Add(ErrorLog)
    Close(ErrorLog)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ErrorLog',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:ErrorLog',tmp:ErrorLog,'ErrorLog',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
