

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02011.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSubAddresses PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::sua:Record  LIKE(sua:RECORD),STATIC
QuickWindow          WINDOW('Update Sub Addresses'),AT(,,220,199),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('UpdateSubAddresses'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,164),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Account Number'),AT(8,20),USE(?sua:AccountNumber:Prompt),TRN
                           ENTRY(@s15),AT(84,20,124,10),USE(sua:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Company Name'),AT(8,36),USE(?sua:CompanyName:Prompt),TRN
                           ENTRY(@s30),AT(84,36,123,11),USE(sua:CompanyName),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Postcode'),AT(8,52),USE(?sua:Postcode:Prompt),TRN
                           ENTRY(@s15),AT(84,52,64,10),USE(sua:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Address'),AT(8,64),USE(?sua:AddressLine1:Prompt),TRN
                           ENTRY(@s30),AT(84,64,124,10),USE(sua:AddressLine1),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(84,76,124,10),USE(sua:AddressLine2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(84,88,124,10),USE(sua:AddressLine3),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(8,104),USE(?sua:TelephoneNumber:Prompt),TRN
                           ENTRY(@s15),AT(84,104,64,10),USE(sua:TelephoneNumber),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Fax Number'),AT(8,120),USE(?sua:FaxNumber:Prompt),TRN
                           ENTRY(@s15),AT(84,120,64,10),USE(sua:FaxNumber),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Email Address'),AT(8,136),USE(?sua:EmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,136,124,10),USE(sua:EmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           PROMPT('Contact Name'),AT(8,152),USE(?sua:ContactName:Prompt),TRN
                           ENTRY(@s30),AT(84,152,124,10),USE(sua:ContactName),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('&OK'),AT(100,176,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,176,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,172,212,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?sua:AccountNumber:Prompt{prop:FontColor} = -1
    ?sua:AccountNumber:Prompt{prop:Color} = 15066597
    If ?sua:AccountNumber{prop:ReadOnly} = True
        ?sua:AccountNumber{prop:FontColor} = 65793
        ?sua:AccountNumber{prop:Color} = 15066597
    Elsif ?sua:AccountNumber{prop:Req} = True
        ?sua:AccountNumber{prop:FontColor} = 65793
        ?sua:AccountNumber{prop:Color} = 8454143
    Else ! If ?sua:AccountNumber{prop:Req} = True
        ?sua:AccountNumber{prop:FontColor} = 65793
        ?sua:AccountNumber{prop:Color} = 16777215
    End ! If ?sua:AccountNumber{prop:Req} = True
    ?sua:AccountNumber{prop:Trn} = 0
    ?sua:AccountNumber{prop:FontStyle} = font:Bold
    ?sua:CompanyName:Prompt{prop:FontColor} = -1
    ?sua:CompanyName:Prompt{prop:Color} = 15066597
    If ?sua:CompanyName{prop:ReadOnly} = True
        ?sua:CompanyName{prop:FontColor} = 65793
        ?sua:CompanyName{prop:Color} = 15066597
    Elsif ?sua:CompanyName{prop:Req} = True
        ?sua:CompanyName{prop:FontColor} = 65793
        ?sua:CompanyName{prop:Color} = 8454143
    Else ! If ?sua:CompanyName{prop:Req} = True
        ?sua:CompanyName{prop:FontColor} = 65793
        ?sua:CompanyName{prop:Color} = 16777215
    End ! If ?sua:CompanyName{prop:Req} = True
    ?sua:CompanyName{prop:Trn} = 0
    ?sua:CompanyName{prop:FontStyle} = font:Bold
    ?sua:Postcode:Prompt{prop:FontColor} = -1
    ?sua:Postcode:Prompt{prop:Color} = 15066597
    If ?sua:Postcode{prop:ReadOnly} = True
        ?sua:Postcode{prop:FontColor} = 65793
        ?sua:Postcode{prop:Color} = 15066597
    Elsif ?sua:Postcode{prop:Req} = True
        ?sua:Postcode{prop:FontColor} = 65793
        ?sua:Postcode{prop:Color} = 8454143
    Else ! If ?sua:Postcode{prop:Req} = True
        ?sua:Postcode{prop:FontColor} = 65793
        ?sua:Postcode{prop:Color} = 16777215
    End ! If ?sua:Postcode{prop:Req} = True
    ?sua:Postcode{prop:Trn} = 0
    ?sua:Postcode{prop:FontStyle} = font:Bold
    ?sua:AddressLine1:Prompt{prop:FontColor} = -1
    ?sua:AddressLine1:Prompt{prop:Color} = 15066597
    If ?sua:AddressLine1{prop:ReadOnly} = True
        ?sua:AddressLine1{prop:FontColor} = 65793
        ?sua:AddressLine1{prop:Color} = 15066597
    Elsif ?sua:AddressLine1{prop:Req} = True
        ?sua:AddressLine1{prop:FontColor} = 65793
        ?sua:AddressLine1{prop:Color} = 8454143
    Else ! If ?sua:AddressLine1{prop:Req} = True
        ?sua:AddressLine1{prop:FontColor} = 65793
        ?sua:AddressLine1{prop:Color} = 16777215
    End ! If ?sua:AddressLine1{prop:Req} = True
    ?sua:AddressLine1{prop:Trn} = 0
    ?sua:AddressLine1{prop:FontStyle} = font:Bold
    If ?sua:AddressLine2{prop:ReadOnly} = True
        ?sua:AddressLine2{prop:FontColor} = 65793
        ?sua:AddressLine2{prop:Color} = 15066597
    Elsif ?sua:AddressLine2{prop:Req} = True
        ?sua:AddressLine2{prop:FontColor} = 65793
        ?sua:AddressLine2{prop:Color} = 8454143
    Else ! If ?sua:AddressLine2{prop:Req} = True
        ?sua:AddressLine2{prop:FontColor} = 65793
        ?sua:AddressLine2{prop:Color} = 16777215
    End ! If ?sua:AddressLine2{prop:Req} = True
    ?sua:AddressLine2{prop:Trn} = 0
    ?sua:AddressLine2{prop:FontStyle} = font:Bold
    If ?sua:AddressLine3{prop:ReadOnly} = True
        ?sua:AddressLine3{prop:FontColor} = 65793
        ?sua:AddressLine3{prop:Color} = 15066597
    Elsif ?sua:AddressLine3{prop:Req} = True
        ?sua:AddressLine3{prop:FontColor} = 65793
        ?sua:AddressLine3{prop:Color} = 8454143
    Else ! If ?sua:AddressLine3{prop:Req} = True
        ?sua:AddressLine3{prop:FontColor} = 65793
        ?sua:AddressLine3{prop:Color} = 16777215
    End ! If ?sua:AddressLine3{prop:Req} = True
    ?sua:AddressLine3{prop:Trn} = 0
    ?sua:AddressLine3{prop:FontStyle} = font:Bold
    ?sua:TelephoneNumber:Prompt{prop:FontColor} = -1
    ?sua:TelephoneNumber:Prompt{prop:Color} = 15066597
    If ?sua:TelephoneNumber{prop:ReadOnly} = True
        ?sua:TelephoneNumber{prop:FontColor} = 65793
        ?sua:TelephoneNumber{prop:Color} = 15066597
    Elsif ?sua:TelephoneNumber{prop:Req} = True
        ?sua:TelephoneNumber{prop:FontColor} = 65793
        ?sua:TelephoneNumber{prop:Color} = 8454143
    Else ! If ?sua:TelephoneNumber{prop:Req} = True
        ?sua:TelephoneNumber{prop:FontColor} = 65793
        ?sua:TelephoneNumber{prop:Color} = 16777215
    End ! If ?sua:TelephoneNumber{prop:Req} = True
    ?sua:TelephoneNumber{prop:Trn} = 0
    ?sua:TelephoneNumber{prop:FontStyle} = font:Bold
    ?sua:FaxNumber:Prompt{prop:FontColor} = -1
    ?sua:FaxNumber:Prompt{prop:Color} = 15066597
    If ?sua:FaxNumber{prop:ReadOnly} = True
        ?sua:FaxNumber{prop:FontColor} = 65793
        ?sua:FaxNumber{prop:Color} = 15066597
    Elsif ?sua:FaxNumber{prop:Req} = True
        ?sua:FaxNumber{prop:FontColor} = 65793
        ?sua:FaxNumber{prop:Color} = 8454143
    Else ! If ?sua:FaxNumber{prop:Req} = True
        ?sua:FaxNumber{prop:FontColor} = 65793
        ?sua:FaxNumber{prop:Color} = 16777215
    End ! If ?sua:FaxNumber{prop:Req} = True
    ?sua:FaxNumber{prop:Trn} = 0
    ?sua:FaxNumber{prop:FontStyle} = font:Bold
    ?sua:EmailAddress:Prompt{prop:FontColor} = -1
    ?sua:EmailAddress:Prompt{prop:Color} = 15066597
    If ?sua:EmailAddress{prop:ReadOnly} = True
        ?sua:EmailAddress{prop:FontColor} = 65793
        ?sua:EmailAddress{prop:Color} = 15066597
    Elsif ?sua:EmailAddress{prop:Req} = True
        ?sua:EmailAddress{prop:FontColor} = 65793
        ?sua:EmailAddress{prop:Color} = 8454143
    Else ! If ?sua:EmailAddress{prop:Req} = True
        ?sua:EmailAddress{prop:FontColor} = 65793
        ?sua:EmailAddress{prop:Color} = 16777215
    End ! If ?sua:EmailAddress{prop:Req} = True
    ?sua:EmailAddress{prop:Trn} = 0
    ?sua:EmailAddress{prop:FontStyle} = font:Bold
    ?sua:ContactName:Prompt{prop:FontColor} = -1
    ?sua:ContactName:Prompt{prop:Color} = 15066597
    If ?sua:ContactName{prop:ReadOnly} = True
        ?sua:ContactName{prop:FontColor} = 65793
        ?sua:ContactName{prop:Color} = 15066597
    Elsif ?sua:ContactName{prop:Req} = True
        ?sua:ContactName{prop:FontColor} = 65793
        ?sua:ContactName{prop:Color} = 8454143
    Else ! If ?sua:ContactName{prop:Req} = True
        ?sua:ContactName{prop:FontColor} = 65793
        ?sua:ContactName{prop:Color} = 16777215
    End ! If ?sua:ContactName{prop:Req} = True
    ?sua:ContactName{prop:Trn} = 0
    ?sua:ContactName{prop:FontStyle} = font:Bold
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSubAddresses',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateSubAddresses',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateSubAddresses',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:AccountNumber:Prompt;  SolaceCtrlName = '?sua:AccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:AccountNumber;  SolaceCtrlName = '?sua:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:CompanyName:Prompt;  SolaceCtrlName = '?sua:CompanyName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:CompanyName;  SolaceCtrlName = '?sua:CompanyName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:Postcode:Prompt;  SolaceCtrlName = '?sua:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:Postcode;  SolaceCtrlName = '?sua:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:AddressLine1:Prompt;  SolaceCtrlName = '?sua:AddressLine1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:AddressLine1;  SolaceCtrlName = '?sua:AddressLine1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:AddressLine2;  SolaceCtrlName = '?sua:AddressLine2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:AddressLine3;  SolaceCtrlName = '?sua:AddressLine3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:TelephoneNumber:Prompt;  SolaceCtrlName = '?sua:TelephoneNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:TelephoneNumber;  SolaceCtrlName = '?sua:TelephoneNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:FaxNumber:Prompt;  SolaceCtrlName = '?sua:FaxNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:FaxNumber;  SolaceCtrlName = '?sua:FaxNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:EmailAddress:Prompt;  SolaceCtrlName = '?sua:EmailAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:EmailAddress;  SolaceCtrlName = '?sua:EmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:ContactName:Prompt;  SolaceCtrlName = '?sua:ContactName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:ContactName;  SolaceCtrlName = '?sua:ContactName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Sub Account Address'
  OF ChangeRecord
    ActionMessage = 'Changing A Sub Account Address'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSubAddresses')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSubAddresses')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?sua:AccountNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sua:Record,History::sua:Record)
  SELF.AddHistoryField(?sua:AccountNumber,3)
  SELF.AddHistoryField(?sua:CompanyName,4)
  SELF.AddHistoryField(?sua:Postcode,5)
  SELF.AddHistoryField(?sua:AddressLine1,6)
  SELF.AddHistoryField(?sua:AddressLine2,7)
  SELF.AddHistoryField(?sua:AddressLine3,8)
  SELF.AddHistoryField(?sua:TelephoneNumber,9)
  SELF.AddHistoryField(?sua:FaxNumber,10)
  SELF.AddHistoryField(?sua:EmailAddress,11)
  SELF.AddHistoryField(?sua:ContactName,12)
  SELF.AddUpdateFile(Access:SUBACCAD)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:SUBACCAD.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SUBACCAD
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SUBACCAD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSubAddresses',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSubAddresses')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

