

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02013.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Sub_Accounts PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
main_account_temp    STRING(15)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?main_account_temp
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Main_Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:Branch)
                       PROJECT(sub:Postcode)
                       PROJECT(sub:Telephone_Number)
                       PROJECT(sub:Fax_Number)
                       PROJECT(sub:Contact_Name)
                       PROJECT(sub:Enquiry_Source)
                       PROJECT(sub:Labour_Discount_Code)
                       PROJECT(sub:Labour_VAT_Code)
                       PROJECT(sub:Account_Type)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Main_Account_Number LIKE(sub:Main_Account_Number) !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:Branch             LIKE(sub:Branch)               !List box control field - type derived from field
sub:Postcode           LIKE(sub:Postcode)             !List box control field - type derived from field
sub:Telephone_Number   LIKE(sub:Telephone_Number)     !List box control field - type derived from field
sub:Fax_Number         LIKE(sub:Fax_Number)           !List box control field - type derived from field
sub:Contact_Name       LIKE(sub:Contact_Name)         !List box control field - type derived from field
sub:Enquiry_Source     LIKE(sub:Enquiry_Source)       !List box control field - type derived from field
sub:Labour_Discount_Code LIKE(sub:Labour_Discount_Code) !List box control field - type derived from field
sub:Labour_VAT_Code    LIKE(sub:Labour_VAT_Code)      !List box control field - type derived from field
sub:Account_Type       LIKE(sub:Account_Type)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB10::View:FileDropCombo VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Sub Account File'),AT(,,440,188),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Sub_Accounts'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,36,340,144),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),ALRT(InsertKey),FORMAT('66L(2)|M~Account Number~@s15@80L(2)|M~Main Account Number~@s15@80L(2)|M~Company ' &|
   'Name~@s40@107L(2)|M~Branch~@s30@64L(2)|M~Postcode~@s15@68L(2)|M~Telephone Number' &|
   '~@s15@64L(2)|M~Fax Number~@s15@80L(2)|M~Contact Name~@s30@80L(2)|M~Enquiry Sourc' &|
   'e~@s30@56L(2)|M~Discount Code~@s2@36L(2)|M~VAT Code~@s2@52L(2)|M~Account Type~@s' &|
   '6@'),FROM(Queue:Browse:1)
                       BUTTON('Insert'),AT(360,88,76,20),USE(?Insert),LEFT,ICON('insert.ico')
                       BUTTON('Change'),AT(360,112,76,20),USE(?Change),LEFT,ICON('edit.ico')
                       BUTTON('&Delete'),AT(360,136,76,20),USE(?Delete),LEFT,ICON('delete.ico')
                       BUTTON('&Select'),AT(360,20,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       SHEET,AT(4,4,348,180),USE(?CurrentTab),SPREAD
                         TAB('By Account Number'),USE(?Tab:4)
                           ENTRY(@s15),AT(8,20,64,10),USE(sub:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Company Name'),USE(?Tab3)
                           ENTRY(@s30),AT(8,20,124,10),USE(sub:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By Branch'),USE(?Tab2)
                           ENTRY(@s30),AT(8,20,124,10),USE(sub:Branch),FONT('Tahoma',8,,FONT:bold),UPR
                           COMBO(@s15),AT(136,20,124,10),USE(main_account_temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('65L(2)|M@s15@120L(2)|M@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                           PROMPT('Main Account'),AT(264,20),USE(?Prompt1),FONT(,,COLOR:White,,CHARSET:ANSI)
                         END
                       END
                       BUTTON('Close'),AT(360,164,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                       BUTTON('DELETE THIS'),AT(276,200,76,20),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:4{prop:Color} = 15066597
    If ?sub:Account_Number{prop:ReadOnly} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 15066597
    Elsif ?sub:Account_Number{prop:Req} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 8454143
    Else ! If ?sub:Account_Number{prop:Req} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 16777215
    End ! If ?sub:Account_Number{prop:Req} = True
    ?sub:Account_Number{prop:Trn} = 0
    ?sub:Account_Number{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?sub:Company_Name{prop:ReadOnly} = True
        ?sub:Company_Name{prop:FontColor} = 65793
        ?sub:Company_Name{prop:Color} = 15066597
    Elsif ?sub:Company_Name{prop:Req} = True
        ?sub:Company_Name{prop:FontColor} = 65793
        ?sub:Company_Name{prop:Color} = 8454143
    Else ! If ?sub:Company_Name{prop:Req} = True
        ?sub:Company_Name{prop:FontColor} = 65793
        ?sub:Company_Name{prop:Color} = 16777215
    End ! If ?sub:Company_Name{prop:Req} = True
    ?sub:Company_Name{prop:Trn} = 0
    ?sub:Company_Name{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?sub:Branch{prop:ReadOnly} = True
        ?sub:Branch{prop:FontColor} = 65793
        ?sub:Branch{prop:Color} = 15066597
    Elsif ?sub:Branch{prop:Req} = True
        ?sub:Branch{prop:FontColor} = 65793
        ?sub:Branch{prop:Color} = 8454143
    Else ! If ?sub:Branch{prop:Req} = True
        ?sub:Branch{prop:FontColor} = 65793
        ?sub:Branch{prop:Color} = 16777215
    End ! If ?sub:Branch{prop:Req} = True
    ?sub:Branch{prop:Trn} = 0
    ?sub:Branch{prop:FontStyle} = font:Bold
    If ?main_account_temp{prop:ReadOnly} = True
        ?main_account_temp{prop:FontColor} = 65793
        ?main_account_temp{prop:Color} = 15066597
    Elsif ?main_account_temp{prop:Req} = True
        ?main_account_temp{prop:FontColor} = 65793
        ?main_account_temp{prop:Color} = 8454143
    Else ! If ?main_account_temp{prop:Req} = True
        ?main_account_temp{prop:FontColor} = 65793
        ?main_account_temp{prop:Color} = 16777215
    End ! If ?main_account_temp{prop:Req} = True
    ?main_account_temp{prop:Trn} = 0
    ?main_account_temp{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Sub_Accounts',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Sub_Accounts',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Sub_Accounts',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Sub_Accounts',1)
    SolaceViewVars('main_account_temp',main_account_temp,'Browse_Sub_Accounts',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Account_Number;  SolaceCtrlName = '?sub:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Company_Name;  SolaceCtrlName = '?sub:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Branch;  SolaceCtrlName = '?sub:Branch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?main_account_temp;  SolaceCtrlName = '?main_account_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Sub_Accounts')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Sub_Accounts')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:SUBTRACC.Open
  Relate:USELEVEL.Open
  Relate:USERS_ALIAS.Open
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:SUBTRACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sub:Company_Name_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?sub:Company_Name,sub:Company_Name,1,BRW1)
  BRW1.AddSortOrder(,sub:Main_Branch_Key)
  BRW1.AddRange(sub:Main_Account_Number,main_account_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?sub:Branch,sub:Branch,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sub:Account_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sub:Account_Number,sub:Account_Number,1,BRW1)
  BRW1.AddField(sub:Account_Number,BRW1.Q.sub:Account_Number)
  BRW1.AddField(sub:Main_Account_Number,BRW1.Q.sub:Main_Account_Number)
  BRW1.AddField(sub:Company_Name,BRW1.Q.sub:Company_Name)
  BRW1.AddField(sub:Branch,BRW1.Q.sub:Branch)
  BRW1.AddField(sub:Postcode,BRW1.Q.sub:Postcode)
  BRW1.AddField(sub:Telephone_Number,BRW1.Q.sub:Telephone_Number)
  BRW1.AddField(sub:Fax_Number,BRW1.Q.sub:Fax_Number)
  BRW1.AddField(sub:Contact_Name,BRW1.Q.sub:Contact_Name)
  BRW1.AddField(sub:Enquiry_Source,BRW1.Q.sub:Enquiry_Source)
  BRW1.AddField(sub:Labour_Discount_Code,BRW1.Q.sub:Labour_Discount_Code)
  BRW1.AddField(sub:Labour_VAT_Code,BRW1.Q.sub:Labour_VAT_Code)
  BRW1.AddField(sub:Account_Type,BRW1.Q.sub:Account_Type)
  BRW1.AddField(sub:RecordNumber,BRW1.Q.sub:RecordNumber)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB10.Init(main_account_temp,?main_account_temp,Queue:FileDropCombo.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo,Relate:TRADEACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo
  FDCB10.AddSortOrder(tra:Account_Number_Key)
  FDCB10.AddField(tra:Account_Number,FDCB10.Q.tra:Account_Number)
  FDCB10.AddField(tra:Company_Name,FDCB10.Q.tra:Company_Name)
  FDCB10.AddField(tra:RecordNumber,FDCB10.Q.tra:RecordNumber)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:4{PROP:TEXT} = 'By Account Number'
    ?Tab3{PROP:TEXT} = 'By Company Name'
    ?Tab2{PROP:TEXT} = 'By Branch'
    ?Browse:1{PROP:FORMAT} ='66L(2)|M~Account Number~@s15@#1#80L(2)|M~Main Account Number~@s15@#2#80L(2)|M~Company Name~@s40@#3#107L(2)|M~Branch~@s30@#4#64L(2)|M~Postcode~@s15@#5#68L(2)|M~Telephone Number~@s15@#6#64L(2)|M~Fax Number~@s15@#7#80L(2)|M~Contact Name~@s30@#8#80L(2)|M~Enquiry Source~@s30@#9#56L(2)|M~Discount Code~@s2@#10#36L(2)|M~VAT Code~@s2@#11#52L(2)|M~Account Type~@s6@#12#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:SUBTRACC.Close
    Relate:USELEVEL.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Sub_Accounts',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of insertrecord
          If SecurityCheck('TRADE ACCOUNTS - INSERT')
              case messageex('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  of 1 ! &ok button
              end!case messageex
              do_update# = false
          end
      of changerecord
          If SecurityCheck('TRADE ACCOUNTS - CHANGE')
              case messageex('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  of 1 ! &ok button
              end!case messageex
              do_update# = false
          end
      of deleterecord
          If SecurityCheck('TRADE ACCOUNTS - DELETE')
              case messageex('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  of 1 ! &ok button
              end!case messageex
              do_update# = false
          end
  end !case request
  
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSUBTRACC
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Insert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
      If SecurityCheck('TRADE ACCOUNTS - INSERT')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          do_update# = false
      Else
          ThisWindow.Update
          GlobalRequest = InsertRecord
          UpdateTRADEACC
          ThisWindow.Reset
      end
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
    OF ?Change
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
      If SecurityCheck('TRADE ACCOUNTS - CHANGE')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          do_update# = false
      Else
          ThisWindow.Update
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = sub:main_account_number
          if access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
              GlobalRequest = ChangeRecord
              UpdateTRADEACC
              ThisWindow.Reset
          end!if access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
      
      end
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
    OF ?main_account_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?main_account_temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?main_account_temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Sub_Accounts')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      Case Keycode()
          Of Insertkey
              Post(event:accepted,?Insert)
      End!Case Keycode()
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='66L(2)|M~Account Number~@s15@#1#80L(2)|M~Main Account Number~@s15@#2#80L(2)|M~Company Name~@s40@#3#107L(2)|M~Branch~@s30@#4#64L(2)|M~Postcode~@s15@#5#68L(2)|M~Telephone Number~@s15@#6#64L(2)|M~Fax Number~@s15@#7#80L(2)|M~Contact Name~@s30@#8#80L(2)|M~Enquiry Source~@s30@#9#56L(2)|M~Discount Code~@s2@#10#36L(2)|M~VAT Code~@s2@#11#52L(2)|M~Account Type~@s6@#12#'
          ?Tab:4{PROP:TEXT} = 'By Account Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='80L(2)|M~Company Name~@s40@#3#66L(2)|M~Account Number~@s15@#1#80L(2)|M~Main Account Number~@s15@#2#107L(2)|M~Branch~@s30@#4#64L(2)|M~Postcode~@s15@#5#68L(2)|M~Telephone Number~@s15@#6#64L(2)|M~Fax Number~@s15@#7#80L(2)|M~Contact Name~@s30@#8#80L(2)|M~Enquiry Source~@s30@#9#56L(2)|M~Discount Code~@s2@#10#36L(2)|M~VAT Code~@s2@#11#52L(2)|M~Account Type~@s6@#12#'
          ?Tab3{PROP:TEXT} = 'By Company Name'
        OF 3
          ?Browse:1{PROP:FORMAT} ='107L(2)|M~Branch~@s30@#4#66L(2)|M~Account Number~@s15@#1#80L(2)|M~Main Account Number~@s15@#2#80L(2)|M~Company Name~@s40@#3#64L(2)|M~Postcode~@s15@#5#68L(2)|M~Telephone Number~@s15@#6#64L(2)|M~Fax Number~@s15@#7#80L(2)|M~Contact Name~@s30@#8#80L(2)|M~Enquiry Source~@s30@#9#56L(2)|M~Discount Code~@s2@#10#36L(2)|M~VAT Code~@s2@#11#52L(2)|M~Account Type~@s6@#12#'
          ?Tab2{PROP:TEXT} = 'By Branch'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sub:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Account_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Account_Number, Selected)
    OF ?sub:Branch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Branch, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Branch, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?sub:Company_Name
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Company_Name, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Company_Name, Selected)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Loop While Keyboard()
          Ask
      End
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

