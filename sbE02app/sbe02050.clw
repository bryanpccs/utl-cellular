

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02050.INC'),ONCE        !Local module procedure declarations
                     END


BrowseSIDSCAccountRegions PROCEDURE (scAccountID)     !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Branch           STRING(30)
tmp:CompanyName      STRING(30)
tmp:RegionName       STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:RegionName
reg:RegionName         LIKE(reg:RegionName)           !List box control field - type derived from field
reg:RegionID           LIKE(reg:RegionID)             !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(ACCREG)
                       PROJECT(acg:AccountNo)
                       PROJECT(acg:RegionName)
                       PROJECT(acg:AccRegID)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
acg:AccountNo          LIKE(acg:AccountNo)            !List box control field - type derived from field
tmp:Branch             LIKE(tmp:Branch)               !List box control field - type derived from local data
tmp:CompanyName        LIKE(tmp:CompanyName)          !List box control field - type derived from local data
acg:RegionName         LIKE(acg:RegionName)           !List box control field - type derived from field
acg:AccRegID           LIKE(acg:AccRegID)             !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB8::View:FileDropCombo VIEW(REGIONS)
                       PROJECT(reg:RegionName)
                       PROJECT(reg:RegionID)
                     END
QuickWindow          WINDOW('Browse the Account Regions'),AT(,,543,172),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('BrowseAccountRegions'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,32,444,132),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('90L(2)|M~Account No~@s30@120L(2)|M~Branch~@s30@120L(2)|M~Company Name~@s30@80L(2' &|
   ')|M~Region Name~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(464,32,76,20),USE(?Select:2),DISABLE,HIDE,LEFT,ICON('select.ico')
                       BUTTON('&Change'),AT(464,96,76,20),USE(?BtnUpdate),LEFT,ICON('edit.ico')
                       BUTTON('&Insert'),AT(464,72,76,20),USE(?Insert:3),DISABLE,HIDE,LEFT,ICON('insert.ico')
                       BUTTON('&Change'),AT(464,120,76,20),USE(?Change:3),DISABLE,HIDE,LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(464,120,76,20),USE(?Delete:3),DISABLE,HIDE,LEFT,ICON('delete.ico')
                       SHEET,AT(4,4,452,164),USE(?CurrentTab),SPREAD
                         TAB('By Account No'),USE(?Tab1)
                           ENTRY(@s15),AT(8,20,124,10),USE(acg:AccountNo)
                         END
                         TAB('By Region'),USE(?Tab2)
                           ENTRY(@s15),AT(8,20,124,10),USE(acg:AccountNo,,?acg:AccountNo:2)
                           COMBO(@s30),AT(136,20,124,10),USE(tmp:RegionName),IMM,VSCROLL,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Region Name'),AT(264,20),USE(?Prompt1),FONT(,,COLOR:White,,CHARSET:ANSI)
                         END
                       END
                       BUTTON('Default Regions'),AT(464,4,76,20),USE(?DefaultRegions),LEFT,ICON('wizextf.ico')
                       BUTTON('Close'),AT(464,148,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?acg:AccountNo{prop:ReadOnly} = True
        ?acg:AccountNo{prop:FontColor} = 65793
        ?acg:AccountNo{prop:Color} = 15066597
    Elsif ?acg:AccountNo{prop:Req} = True
        ?acg:AccountNo{prop:FontColor} = 65793
        ?acg:AccountNo{prop:Color} = 8454143
    Else ! If ?acg:AccountNo{prop:Req} = True
        ?acg:AccountNo{prop:FontColor} = 65793
        ?acg:AccountNo{prop:Color} = 16777215
    End ! If ?acg:AccountNo{prop:Req} = True
    ?acg:AccountNo{prop:Trn} = 0
    ?acg:AccountNo{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?acg:AccountNo:2{prop:ReadOnly} = True
        ?acg:AccountNo:2{prop:FontColor} = 65793
        ?acg:AccountNo:2{prop:Color} = 15066597
    Elsif ?acg:AccountNo:2{prop:Req} = True
        ?acg:AccountNo:2{prop:FontColor} = 65793
        ?acg:AccountNo:2{prop:Color} = 8454143
    Else ! If ?acg:AccountNo:2{prop:Req} = True
        ?acg:AccountNo:2{prop:FontColor} = 65793
        ?acg:AccountNo:2{prop:Color} = 16777215
    End ! If ?acg:AccountNo:2{prop:Req} = True
    ?acg:AccountNo:2{prop:Trn} = 0
    ?acg:AccountNo:2{prop:FontStyle} = font:Bold
    If ?tmp:RegionName{prop:ReadOnly} = True
        ?tmp:RegionName{prop:FontColor} = 65793
        ?tmp:RegionName{prop:Color} = 15066597
    Elsif ?tmp:RegionName{prop:Req} = True
        ?tmp:RegionName{prop:FontColor} = 65793
        ?tmp:RegionName{prop:Color} = 8454143
    Else ! If ?tmp:RegionName{prop:Req} = True
        ?tmp:RegionName{prop:FontColor} = 65793
        ?tmp:RegionName{prop:Color} = 16777215
    End ! If ?tmp:RegionName{prop:Req} = True
    ?tmp:RegionName{prop:Trn} = 0
    ?tmp:RegionName{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BrowseSIDSCAccountRegions',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'BrowseSIDSCAccountRegions',1)
    SolaceViewVars('tmp:Branch',tmp:Branch,'BrowseSIDSCAccountRegions',1)
    SolaceViewVars('tmp:CompanyName',tmp:CompanyName,'BrowseSIDSCAccountRegions',1)
    SolaceViewVars('tmp:RegionName',tmp:RegionName,'BrowseSIDSCAccountRegions',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BtnUpdate;  SolaceCtrlName = '?BtnUpdate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?acg:AccountNo;  SolaceCtrlName = '?acg:AccountNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?acg:AccountNo:2;  SolaceCtrlName = '?acg:AccountNo:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:RegionName;  SolaceCtrlName = '?tmp:RegionName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DefaultRegions;  SolaceCtrlName = '?DefaultRegions';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseSIDSCAccountRegions')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BrowseSIDSCAccountRegions')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCREG.Open
  Relate:SUBTRACC.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ACCREG,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  BRW1.InsertControl = 0
  BRW1.ChangeControl = 0
  BRW1.DeleteControl = 0
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,acg:RegionNameKey)
  BRW1.AddRange(acg:RegionName,tmp:RegionName)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?acg:AccountNo:2,acg:AccountNo,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,acg:AccountNoKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?acg:AccountNo,acg:AccountNo,1,BRW1)
  BIND('tmp:Branch',tmp:Branch)
  BIND('tmp:CompanyName',tmp:CompanyName)
  BRW1.AddField(acg:AccountNo,BRW1.Q.acg:AccountNo)
  BRW1.AddField(tmp:Branch,BRW1.Q.tmp:Branch)
  BRW1.AddField(tmp:CompanyName,BRW1.Q.tmp:CompanyName)
  BRW1.AddField(acg:RegionName,BRW1.Q.acg:RegionName)
  BRW1.AddField(acg:AccRegID,BRW1.Q.acg:AccRegID)
  FDCB8.Init(tmp:RegionName,?tmp:RegionName,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:REGIONS,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder(reg:RegionNameKey)
  FDCB8.AddField(reg:RegionName,FDCB8.Q.reg:RegionName)
  FDCB8.AddField(reg:RegionID,FDCB8.Q.reg:RegionID)
  FDCB8.AddUpdateField(reg:RegionName,tmp:RegionName)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCREG.Close
    Relate:SUBTRACC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BrowseSIDSCAccountRegions',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?BtnUpdate
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnUpdate, Accepted)
      BRW1.UpdateBuffer()
      UpdateSIDSCAccountRegion(scAccountID, acg:AccRegID)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnUpdate, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DefaultRegions
      ThisWindow.Update
      BrowseSCRegions(scAccountID)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BrowseSIDSCAccountRegions')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  tmp:Branch      = ''
  tmp:CompanyName = ''
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number  = acg:AccountNo
  If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      !Found
      tmp:Branch = sub:Branch
      tmp:CompanyName = sub:Company_Name
  Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

