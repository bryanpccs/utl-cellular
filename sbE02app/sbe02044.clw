

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02044.INC'),ONCE        !Local module procedure declarations
                     END


BrowseSIDServiceCentres PROCEDURE                     !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Branch           STRING(30)
tmp:CompanyName      STRING(30)
tmp:RegionName       STRING(30)
tmp:Blank            STRING(1)
BRW1::View:Browse    VIEW(SIDSRVCN)
                       PROJECT(srv:SCAccountID)
                       PROJECT(srv:ServiceCentreName)
                       PROJECT(srv:AddressLine1)
                       PROJECT(srv:Postcode)
                       PROJECT(srv:AccessoryOrders)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
srv:SCAccountID        LIKE(srv:SCAccountID)          !List box control field - type derived from field
srv:ServiceCentreName  LIKE(srv:ServiceCentreName)    !List box control field - type derived from field
srv:AddressLine1       LIKE(srv:AddressLine1)         !List box control field - type derived from field
srv:Postcode           LIKE(srv:Postcode)             !List box control field - type derived from field
tmp:Blank              LIKE(tmp:Blank)                !List box control field - type derived from local data
tmp:Blank_Icon         LONG                           !Entry's icon ID
srv:AccessoryOrders    LIKE(srv:AccessoryOrders)      !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the SID Service Centres'),AT(,,543,172),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('BrowseAccountRegions'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,32,444,132),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('50R(2)|M~Account ID~L@n-10@120L(2)|M~Service Centre~@s30@120L(2)|M~Address~@s30@' &|
   '60L(2)|M~Postcode~@s15@12L(2)|MJ~Accessory Orders~@s1@'),FROM(Queue:Browse:1)
                       BUTTON('Unallocated Models'),AT(464,4,76,20),USE(?BtnUnAllocModels),LEFT,ICON('pick.gif')
                       BUTTON('Accessory Orders'),AT(464,28,76,20),USE(?BtnAccessoryOrders)
                       BUTTON('&Select'),AT(464,52,76,20),USE(?Select:2),DISABLE,HIDE,LEFT,ICON('select.ico')
                       BUTTON('&Insert'),AT(464,76,76,20),USE(?Insert:3),LEFT,ICON('insert.ico')
                       BUTTON('&Change'),AT(464,100,76,20),USE(?Change:3),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(464,124,76,20),USE(?Delete:3),LEFT,ICON('delete.ico')
                       SHEET,AT(4,4,452,164),USE(?CurrentTab),SPREAD
                         TAB('By Account ID'),USE(?Tab1)
                           ENTRY(@n10),AT(8,20,52,10),USE(srv:SCAccountID),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         END
                         TAB('By Service Centre'),USE(?Tab2)
                           ENTRY(@s30),AT(8,20,120,10),USE(srv:ServiceCentreName),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON('Close'),AT(464,148,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?srv:SCAccountID{prop:ReadOnly} = True
        ?srv:SCAccountID{prop:FontColor} = 65793
        ?srv:SCAccountID{prop:Color} = 15066597
    Elsif ?srv:SCAccountID{prop:Req} = True
        ?srv:SCAccountID{prop:FontColor} = 65793
        ?srv:SCAccountID{prop:Color} = 8454143
    Else ! If ?srv:SCAccountID{prop:Req} = True
        ?srv:SCAccountID{prop:FontColor} = 65793
        ?srv:SCAccountID{prop:Color} = 16777215
    End ! If ?srv:SCAccountID{prop:Req} = True
    ?srv:SCAccountID{prop:Trn} = 0
    ?srv:SCAccountID{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?srv:ServiceCentreName{prop:ReadOnly} = True
        ?srv:ServiceCentreName{prop:FontColor} = 65793
        ?srv:ServiceCentreName{prop:Color} = 15066597
    Elsif ?srv:ServiceCentreName{prop:Req} = True
        ?srv:ServiceCentreName{prop:FontColor} = 65793
        ?srv:ServiceCentreName{prop:Color} = 8454143
    Else ! If ?srv:ServiceCentreName{prop:Req} = True
        ?srv:ServiceCentreName{prop:FontColor} = 65793
        ?srv:ServiceCentreName{prop:Color} = 16777215
    End ! If ?srv:ServiceCentreName{prop:Req} = True
    ?srv:ServiceCentreName{prop:Trn} = 0
    ?srv:ServiceCentreName{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
SetAccessoryOrder   routine
    data
scAccountId like(srv:SCAccountID)
    code

    ! Save the selected account
    scAccountId = srv:SCAccountID

    ! Only one service centre can be set to handle the accessory orders
    ! Clear the accessory orders flag from all records
    set(srv:SCAccountIDKey)
    loop
        if Access:SIDSRVCN.Next() <> Level:Benign then break.
        if srv:AccessoryOrders = 1
            srv:AccessoryOrders = 0
            Access:SIDSRVCN.Update()
        end
    end

    ! Now set the selected service centre as active
    Access:SIDSRVCN.ClearKey(srv:SCAccountIDKey)
    srv:SCAccountID = scAccountId
    if Access:SIDSRVCN.Fetch(srv:SCAccountIDKey) = Level:Benign
        srv:AccessoryOrders = 1
        Access:SIDSRVCN.Update()
    end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BrowseSIDServiceCentres',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'BrowseSIDServiceCentres',1)
    SolaceViewVars('tmp:Branch',tmp:Branch,'BrowseSIDServiceCentres',1)
    SolaceViewVars('tmp:CompanyName',tmp:CompanyName,'BrowseSIDServiceCentres',1)
    SolaceViewVars('tmp:RegionName',tmp:RegionName,'BrowseSIDServiceCentres',1)
    SolaceViewVars('tmp:Blank',tmp:Blank,'BrowseSIDServiceCentres',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BtnUnAllocModels;  SolaceCtrlName = '?BtnUnAllocModels';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BtnAccessoryOrders;  SolaceCtrlName = '?BtnAccessoryOrders';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:SCAccountID;  SolaceCtrlName = '?srv:SCAccountID';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ServiceCentreName;  SolaceCtrlName = '?srv:ServiceCentreName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseSIDServiceCentres')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BrowseSIDServiceCentres')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:SIDSRVCN.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:SIDSRVCN,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,srv:ServiceCentreNameKey)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?srv:ServiceCentreName,srv:ServiceCentreName,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,srv:SCAccountIDKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?srv:SCAccountID,srv:SCAccountID,1,BRW1)
  BIND('tmp:Blank',tmp:Blank)
  ?Browse:1{PROP:IconList,1} = '~tick.ico'
  BRW1.AddField(srv:SCAccountID,BRW1.Q.srv:SCAccountID)
  BRW1.AddField(srv:ServiceCentreName,BRW1.Q.srv:ServiceCentreName)
  BRW1.AddField(srv:AddressLine1,BRW1.Q.srv:AddressLine1)
  BRW1.AddField(srv:Postcode,BRW1.Q.srv:Postcode)
  BRW1.AddField(tmp:Blank,BRW1.Q.tmp:Blank)
  BRW1.AddField(srv:AccessoryOrders,BRW1.Q.srv:AccessoryOrders)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'By Account ID'
    ?Tab2{PROP:TEXT} = 'By Service Centre'
    ?Browse:1{PROP:FORMAT} ='50R(2)|M~Account ID~L@n-10@#1#120L(2)|M~Service Centre~@s30@#2#120L(2)|M~Address~@s30@#3#60L(2)|M~Postcode~@s15@#4#12L(2)|MJ~Accessory Orders~@s1@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SIDSRVCN.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BrowseSIDServiceCentres',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSIDServiceCentre
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?BtnAccessoryOrders
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnAccessoryOrders, Accepted)
      BRW1.UpdateBuffer()
      
      do SetAccessoryOrder
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnAccessoryOrders, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?BtnUnAllocModels
      ThisWindow.Update
      UpdateUnAllocatedModels
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BrowseSIDServiceCentres')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='50R(2)|M~Account ID~L@n-10@#1#120L(2)|M~Service Centre~@s30@#2#120L(2)|M~Address~@s30@#3#60L(2)|M~Postcode~@s15@#4#12L(2)|MJ~Accessory Orders~@s1@#5#'
          ?Tab1{PROP:TEXT} = 'By Account ID'
        OF 2
          ?Browse:1{PROP:FORMAT} ='120L(2)|M~Service Centre~@s30@#2#50R(2)|M~Account ID~L@n-10@#1#120L(2)|M~Address~@s30@#3#60L(2)|M~Postcode~@s15@#4#12L(2)|MJ~Accessory Orders~@s1@#5#'
          ?Tab2{PROP:TEXT} = 'By Service Centre'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?srv:SCAccountID
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?srv:SCAccountID, Selected)
      select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?srv:SCAccountID, Selected)
    OF ?srv:ServiceCentreName
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?srv:ServiceCentreName, Selected)
      select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?srv:ServiceCentreName, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (srv:AccessoryOrders = 1)
    SELF.Q.tmp:Blank_Icon = 1
  ELSE
    SELF.Q.tmp:Blank_Icon = 0
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

