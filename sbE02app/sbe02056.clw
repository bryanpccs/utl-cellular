

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02056.INC'),ONCE        !Local module procedure declarations
                     END


SIDRepairCapacityLimit PROCEDURE                      !Generated from procedure template - Form

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('SID Repair Capacity Limit'),AT(,,191,131),FONT('Tahoma',,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,184,102),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Mon Repair Capacity'),AT(8,20),USE(?srv:MonRepairCapacity:Prompt)
                           ENTRY(@n5),AT(116,20,64,10),USE(srv:MonRepairCapacity),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Tue Repair Capacity'),AT(8,36),USE(?srv:TueRepairCapacity:Prompt)
                           ENTRY(@n5),AT(116,36,64,10),USE(srv:TueRepairCapacity),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Wed Repair Capacity'),AT(8,52),USE(?srv:WedRepairCapacity:Prompt)
                           ENTRY(@n5),AT(116,52,64,10),USE(srv:WedRepairCapacity),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Thu Repair Capacity'),AT(8,70),USE(?srv:ThuRepairCapacity:Prompt)
                           ENTRY(@n5),AT(116,70,64,10),USE(srv:ThuRepairCapacity),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Fri Repair Capacity'),AT(8,88),USE(?srv:FriRepairCapacity:Prompt)
                           ENTRY(@n5),AT(116,88,64,10),USE(srv:FriRepairCapacity),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON('Close'),AT(132,112,56,16),USE(?Close),LEFT,ICON('ok.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?srv:MonRepairCapacity:Prompt{prop:FontColor} = -1
    ?srv:MonRepairCapacity:Prompt{prop:Color} = 15066597
    If ?srv:MonRepairCapacity{prop:ReadOnly} = True
        ?srv:MonRepairCapacity{prop:FontColor} = 65793
        ?srv:MonRepairCapacity{prop:Color} = 15066597
    Elsif ?srv:MonRepairCapacity{prop:Req} = True
        ?srv:MonRepairCapacity{prop:FontColor} = 65793
        ?srv:MonRepairCapacity{prop:Color} = 8454143
    Else ! If ?srv:MonRepairCapacity{prop:Req} = True
        ?srv:MonRepairCapacity{prop:FontColor} = 65793
        ?srv:MonRepairCapacity{prop:Color} = 16777215
    End ! If ?srv:MonRepairCapacity{prop:Req} = True
    ?srv:MonRepairCapacity{prop:Trn} = 0
    ?srv:MonRepairCapacity{prop:FontStyle} = font:Bold
    ?srv:TueRepairCapacity:Prompt{prop:FontColor} = -1
    ?srv:TueRepairCapacity:Prompt{prop:Color} = 15066597
    If ?srv:TueRepairCapacity{prop:ReadOnly} = True
        ?srv:TueRepairCapacity{prop:FontColor} = 65793
        ?srv:TueRepairCapacity{prop:Color} = 15066597
    Elsif ?srv:TueRepairCapacity{prop:Req} = True
        ?srv:TueRepairCapacity{prop:FontColor} = 65793
        ?srv:TueRepairCapacity{prop:Color} = 8454143
    Else ! If ?srv:TueRepairCapacity{prop:Req} = True
        ?srv:TueRepairCapacity{prop:FontColor} = 65793
        ?srv:TueRepairCapacity{prop:Color} = 16777215
    End ! If ?srv:TueRepairCapacity{prop:Req} = True
    ?srv:TueRepairCapacity{prop:Trn} = 0
    ?srv:TueRepairCapacity{prop:FontStyle} = font:Bold
    ?srv:WedRepairCapacity:Prompt{prop:FontColor} = -1
    ?srv:WedRepairCapacity:Prompt{prop:Color} = 15066597
    If ?srv:WedRepairCapacity{prop:ReadOnly} = True
        ?srv:WedRepairCapacity{prop:FontColor} = 65793
        ?srv:WedRepairCapacity{prop:Color} = 15066597
    Elsif ?srv:WedRepairCapacity{prop:Req} = True
        ?srv:WedRepairCapacity{prop:FontColor} = 65793
        ?srv:WedRepairCapacity{prop:Color} = 8454143
    Else ! If ?srv:WedRepairCapacity{prop:Req} = True
        ?srv:WedRepairCapacity{prop:FontColor} = 65793
        ?srv:WedRepairCapacity{prop:Color} = 16777215
    End ! If ?srv:WedRepairCapacity{prop:Req} = True
    ?srv:WedRepairCapacity{prop:Trn} = 0
    ?srv:WedRepairCapacity{prop:FontStyle} = font:Bold
    ?srv:ThuRepairCapacity:Prompt{prop:FontColor} = -1
    ?srv:ThuRepairCapacity:Prompt{prop:Color} = 15066597
    If ?srv:ThuRepairCapacity{prop:ReadOnly} = True
        ?srv:ThuRepairCapacity{prop:FontColor} = 65793
        ?srv:ThuRepairCapacity{prop:Color} = 15066597
    Elsif ?srv:ThuRepairCapacity{prop:Req} = True
        ?srv:ThuRepairCapacity{prop:FontColor} = 65793
        ?srv:ThuRepairCapacity{prop:Color} = 8454143
    Else ! If ?srv:ThuRepairCapacity{prop:Req} = True
        ?srv:ThuRepairCapacity{prop:FontColor} = 65793
        ?srv:ThuRepairCapacity{prop:Color} = 16777215
    End ! If ?srv:ThuRepairCapacity{prop:Req} = True
    ?srv:ThuRepairCapacity{prop:Trn} = 0
    ?srv:ThuRepairCapacity{prop:FontStyle} = font:Bold
    ?srv:FriRepairCapacity:Prompt{prop:FontColor} = -1
    ?srv:FriRepairCapacity:Prompt{prop:Color} = 15066597
    If ?srv:FriRepairCapacity{prop:ReadOnly} = True
        ?srv:FriRepairCapacity{prop:FontColor} = 65793
        ?srv:FriRepairCapacity{prop:Color} = 15066597
    Elsif ?srv:FriRepairCapacity{prop:Req} = True
        ?srv:FriRepairCapacity{prop:FontColor} = 65793
        ?srv:FriRepairCapacity{prop:Color} = 8454143
    Else ! If ?srv:FriRepairCapacity{prop:Req} = True
        ?srv:FriRepairCapacity{prop:FontColor} = 65793
        ?srv:FriRepairCapacity{prop:Color} = 16777215
    End ! If ?srv:FriRepairCapacity{prop:Req} = True
    ?srv:FriRepairCapacity{prop:Trn} = 0
    ?srv:FriRepairCapacity{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'SIDRepairCapacityLimit',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:MonRepairCapacity:Prompt;  SolaceCtrlName = '?srv:MonRepairCapacity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:MonRepairCapacity;  SolaceCtrlName = '?srv:MonRepairCapacity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:TueRepairCapacity:Prompt;  SolaceCtrlName = '?srv:TueRepairCapacity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:TueRepairCapacity;  SolaceCtrlName = '?srv:TueRepairCapacity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:WedRepairCapacity:Prompt;  SolaceCtrlName = '?srv:WedRepairCapacity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:WedRepairCapacity;  SolaceCtrlName = '?srv:WedRepairCapacity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ThuRepairCapacity:Prompt;  SolaceCtrlName = '?srv:ThuRepairCapacity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ThuRepairCapacity;  SolaceCtrlName = '?srv:ThuRepairCapacity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:FriRepairCapacity:Prompt;  SolaceCtrlName = '?srv:FriRepairCapacity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:FriRepairCapacity;  SolaceCtrlName = '?srv:FriRepairCapacity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SIDRepairCapacityLimit')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'SIDRepairCapacityLimit')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?srv:MonRepairCapacity:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:SIDSRVCN.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SIDSRVCN.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'SIDRepairCapacityLimit',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'SIDRepairCapacityLimit')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

