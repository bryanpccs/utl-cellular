

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02040.INC'),ONCE        !Local module procedure declarations
                     END


UpdateRegion PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::reg:Record  LIKE(reg:RECORD),STATIC
QuickWindow          WINDOW('Update the Regions File'),AT(,,240,86),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('UpdateRegion'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,232,50),USE(?CurrentTab),SPREAD
                         TAB('Region Details'),USE(?Tab:1)
                           PROMPT('Region Name'),AT(8,20),USE(?REG:RegionName:Prompt),TRN
                           ENTRY(@s30),AT(96,20,124,10),USE(reg:RegionName),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Bank Holidays File Name'),AT(8,36),USE(?reg:BankHolidaysFileName:Prompt),TRN
                           ENTRY(@s100),AT(96,36,124,10),USE(reg:BankHolidaysFileName),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                         END
                       END
                       PANEL,AT(4,58,232,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(120,62,56,16),USE(?OK),LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('Cancel'),AT(176,62,56,16),USE(?Cancel),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?REG:RegionName:Prompt{prop:FontColor} = -1
    ?REG:RegionName:Prompt{prop:Color} = 15066597
    If ?reg:RegionName{prop:ReadOnly} = True
        ?reg:RegionName{prop:FontColor} = 65793
        ?reg:RegionName{prop:Color} = 15066597
    Elsif ?reg:RegionName{prop:Req} = True
        ?reg:RegionName{prop:FontColor} = 65793
        ?reg:RegionName{prop:Color} = 8454143
    Else ! If ?reg:RegionName{prop:Req} = True
        ?reg:RegionName{prop:FontColor} = 65793
        ?reg:RegionName{prop:Color} = 16777215
    End ! If ?reg:RegionName{prop:Req} = True
    ?reg:RegionName{prop:Trn} = 0
    ?reg:RegionName{prop:FontStyle} = font:Bold
    ?reg:BankHolidaysFileName:Prompt{prop:FontColor} = -1
    ?reg:BankHolidaysFileName:Prompt{prop:Color} = 15066597
    If ?reg:BankHolidaysFileName{prop:ReadOnly} = True
        ?reg:BankHolidaysFileName{prop:FontColor} = 65793
        ?reg:BankHolidaysFileName{prop:Color} = 15066597
    Elsif ?reg:BankHolidaysFileName{prop:Req} = True
        ?reg:BankHolidaysFileName{prop:FontColor} = 65793
        ?reg:BankHolidaysFileName{prop:Color} = 8454143
    Else ! If ?reg:BankHolidaysFileName{prop:Req} = True
        ?reg:BankHolidaysFileName{prop:FontColor} = 65793
        ?reg:BankHolidaysFileName{prop:Color} = 16777215
    End ! If ?reg:BankHolidaysFileName{prop:Req} = True
    ?reg:BankHolidaysFileName{prop:Trn} = 0
    ?reg:BankHolidaysFileName{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateRegion',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateRegion',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateRegion',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?REG:RegionName:Prompt;  SolaceCtrlName = '?REG:RegionName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?reg:RegionName;  SolaceCtrlName = '?reg:RegionName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?reg:BankHolidaysFileName:Prompt;  SolaceCtrlName = '?reg:BankHolidaysFileName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?reg:BankHolidaysFileName;  SolaceCtrlName = '?reg:BankHolidaysFileName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Region'
  OF ChangeRecord
    ActionMessage = 'Changing A Region'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRegion')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateRegion')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REG:RegionName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(reg:Record,History::reg:Record)
  SELF.AddHistoryField(?reg:RegionName,2)
  SELF.AddHistoryField(?reg:BankHolidaysFileName,3)
  SELF.AddUpdateFile(Access:REGIONS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:REGIONS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:REGIONS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REGIONS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateRegion',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateRegion')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

