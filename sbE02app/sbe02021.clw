

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02021.INC'),ONCE        !Local module procedure declarations
                     END


Update_Exchange_By_Audit PROCEDURE                    !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
stock_unit_status    STRING(30)
replacement_unit_status STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::exa:Record  LIKE(exa:RECORD),STATIC
QuickWindow          WINDOW('Update the EXCHANGE File'),AT(,,452,232),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Exchange_By_Audit'),TILED,SYSTEM,GRAY,DOUBLE
                       BUTTON('&OK'),AT(328,208,56,16),USE(?OK),FLAT,LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(388,208,56,16),USE(?Cancel),FLAT,LEFT,ICON('cancel.gif')
                       PANEL,AT(4,204,444,24),USE(?Panel1),FILL(COLOR:Silver)
                       PANEL,AT(4,4,444,20),USE(?Panel1:2),FILL(COLOR:Silver)
                       PROMPT('Audit Number'),AT(8,8),USE(?EXA:Audit_Number:Prompt),TRN
                       ENTRY(@s8),AT(84,8,64,10),USE(exa:Audit_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                       SHEET,AT(4,28,220,172),USE(?Sheet1),SPREAD
                         TAB('Stock Unit'),USE(?Tab1)
                           PROMPT('Exchange Unit Number'),AT(8,44),USE(?XCH:Ref_Number:Prompt),TRN
                           ENTRY(@s8),AT(84,44,64,10),USE(exa:Stock_Unit_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Status'),AT(8,60),USE(?stock_unit_status:Prompt),TRN
                           ENTRY(@s30),AT(84,60,124,10),USE(stock_unit_status),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('E.S.N. / I.M.E.I.'),AT(8,76),USE(?XCH:ESN:Prompt),TRN
                           ENTRY(@s30),AT(84,76,64,10),USE(xch:ESN),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('M.S.N.'),AT(8,88),USE(?XCH:MSN:Prompt)
                           ENTRY(@s30),AT(84,88,64,10),USE(xch:MSN),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(8,100),USE(?XCH:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,100,124,10),USE(xch:Model_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR
                           PROMPT('Manufacturer'),AT(8,116),USE(?XCH:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(84,112,124,10),USE(xch:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Location'),AT(8,132),USE(?XCH:Location:Prompt),TRN
                           ENTRY(@s30),AT(84,132,124,10),USE(xch:Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Shelf Location'),AT(8,148),USE(?XCH:Shelf_Location:Prompt),TRN
                           ENTRY(@s30),AT(84,148,124,10),USE(xch:Shelf_Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Stock Type'),AT(8,164),USE(?XCH:Stock_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,164,124,10),USE(xch:Stock_Type),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Exchanged On Job'),AT(8,180),USE(?XCH:Job_Number:Prompt),TRN
                           ENTRY(@p<<<<<<<#pb),AT(84,180,64,10),USE(xch:Job_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Job Number')
                         END
                       END
                       SHEET,AT(228,28,220,172),USE(?Sheet1:2),SPREAD
                         TAB('Replacement Unit'),USE(?Tab2)
                           PROMPT('Exchange Unit Number'),AT(232,44),USE(?XCH:Ref_Number:Prompt:2),TRN
                           ENTRY(@s8),AT(308,44,64,10),USE(exa:Replacement_Unit_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Status'),AT(232,60),USE(?replacement_unit_status:Prompt),TRN
                           ENTRY(@s30),AT(308,60,124,10),USE(replacement_unit_status),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('E.S.N. / I.M.E.I.'),AT(232,76),USE(?XCH:ESN:Prompt:2),TRN
                           ENTRY(@s30),AT(308,76,64,10),USE(xch_ali:ESN),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('M.S.N.'),AT(232,88),USE(?XCH:MSN:Prompt:2)
                           ENTRY(@s30),AT(308,88,64,10),USE(xch_ali:MSN),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(232,100),USE(?XCH:Model_Number:Prompt:2),TRN
                           ENTRY(@s30),AT(308,100,124,10),USE(xch_ali:Model_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Manufacturer'),AT(232,112),USE(?XCH:Manufacturer:Prompt:2),TRN
                           ENTRY(@s30),AT(308,112,124,10),USE(xch_ali:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Location'),AT(232,132),USE(?XCH:Location:Prompt:2),TRN
                           ENTRY(@s30),AT(308,132,124,10),USE(xch_ali:Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Shelf Location'),AT(232,148),USE(?XCH:Shelf_Location:Prompt:2),TRN
                           ENTRY(@s30),AT(308,148,124,10),USE(xch_ali:Shelf_Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Stock Type'),AT(232,164),USE(?XCH:Stock_Type:Prompt:2),TRN
                           ENTRY(@s30),AT(308,164,124,10),USE(xch_ali:Stock_Type),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Exchanged On Job'),AT(232,180),USE(?XCH:Job_Number:Prompt:2),TRN
                           ENTRY(@p<<<<<<<#pb),AT(308,180,64,10),USE(xch_ali:Job_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),REQ,UPR,READONLY,MSG('Job Number')
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1{prop:Fill} = 15066597

    ?Panel1:2{prop:Fill} = 15066597

    ?EXA:Audit_Number:Prompt{prop:FontColor} = -1
    ?EXA:Audit_Number:Prompt{prop:Color} = 15066597
    If ?exa:Audit_Number{prop:ReadOnly} = True
        ?exa:Audit_Number{prop:FontColor} = 65793
        ?exa:Audit_Number{prop:Color} = 15066597
    Elsif ?exa:Audit_Number{prop:Req} = True
        ?exa:Audit_Number{prop:FontColor} = 65793
        ?exa:Audit_Number{prop:Color} = 8454143
    Else ! If ?exa:Audit_Number{prop:Req} = True
        ?exa:Audit_Number{prop:FontColor} = 65793
        ?exa:Audit_Number{prop:Color} = 16777215
    End ! If ?exa:Audit_Number{prop:Req} = True
    ?exa:Audit_Number{prop:Trn} = 0
    ?exa:Audit_Number{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?XCH:Ref_Number:Prompt{prop:FontColor} = -1
    ?XCH:Ref_Number:Prompt{prop:Color} = 15066597
    If ?exa:Stock_Unit_Number{prop:ReadOnly} = True
        ?exa:Stock_Unit_Number{prop:FontColor} = 65793
        ?exa:Stock_Unit_Number{prop:Color} = 15066597
    Elsif ?exa:Stock_Unit_Number{prop:Req} = True
        ?exa:Stock_Unit_Number{prop:FontColor} = 65793
        ?exa:Stock_Unit_Number{prop:Color} = 8454143
    Else ! If ?exa:Stock_Unit_Number{prop:Req} = True
        ?exa:Stock_Unit_Number{prop:FontColor} = 65793
        ?exa:Stock_Unit_Number{prop:Color} = 16777215
    End ! If ?exa:Stock_Unit_Number{prop:Req} = True
    ?exa:Stock_Unit_Number{prop:Trn} = 0
    ?exa:Stock_Unit_Number{prop:FontStyle} = font:Bold
    ?stock_unit_status:Prompt{prop:FontColor} = -1
    ?stock_unit_status:Prompt{prop:Color} = 15066597
    If ?stock_unit_status{prop:ReadOnly} = True
        ?stock_unit_status{prop:FontColor} = 65793
        ?stock_unit_status{prop:Color} = 15066597
    Elsif ?stock_unit_status{prop:Req} = True
        ?stock_unit_status{prop:FontColor} = 65793
        ?stock_unit_status{prop:Color} = 8454143
    Else ! If ?stock_unit_status{prop:Req} = True
        ?stock_unit_status{prop:FontColor} = 65793
        ?stock_unit_status{prop:Color} = 16777215
    End ! If ?stock_unit_status{prop:Req} = True
    ?stock_unit_status{prop:Trn} = 0
    ?stock_unit_status{prop:FontStyle} = font:Bold
    ?XCH:ESN:Prompt{prop:FontColor} = -1
    ?XCH:ESN:Prompt{prop:Color} = 15066597
    If ?xch:ESN{prop:ReadOnly} = True
        ?xch:ESN{prop:FontColor} = 65793
        ?xch:ESN{prop:Color} = 15066597
    Elsif ?xch:ESN{prop:Req} = True
        ?xch:ESN{prop:FontColor} = 65793
        ?xch:ESN{prop:Color} = 8454143
    Else ! If ?xch:ESN{prop:Req} = True
        ?xch:ESN{prop:FontColor} = 65793
        ?xch:ESN{prop:Color} = 16777215
    End ! If ?xch:ESN{prop:Req} = True
    ?xch:ESN{prop:Trn} = 0
    ?xch:ESN{prop:FontStyle} = font:Bold
    ?XCH:MSN:Prompt{prop:FontColor} = -1
    ?XCH:MSN:Prompt{prop:Color} = 15066597
    If ?xch:MSN{prop:ReadOnly} = True
        ?xch:MSN{prop:FontColor} = 65793
        ?xch:MSN{prop:Color} = 15066597
    Elsif ?xch:MSN{prop:Req} = True
        ?xch:MSN{prop:FontColor} = 65793
        ?xch:MSN{prop:Color} = 8454143
    Else ! If ?xch:MSN{prop:Req} = True
        ?xch:MSN{prop:FontColor} = 65793
        ?xch:MSN{prop:Color} = 16777215
    End ! If ?xch:MSN{prop:Req} = True
    ?xch:MSN{prop:Trn} = 0
    ?xch:MSN{prop:FontStyle} = font:Bold
    ?XCH:Model_Number:Prompt{prop:FontColor} = -1
    ?XCH:Model_Number:Prompt{prop:Color} = 15066597
    If ?xch:Model_Number{prop:ReadOnly} = True
        ?xch:Model_Number{prop:FontColor} = 65793
        ?xch:Model_Number{prop:Color} = 15066597
    Elsif ?xch:Model_Number{prop:Req} = True
        ?xch:Model_Number{prop:FontColor} = 65793
        ?xch:Model_Number{prop:Color} = 8454143
    Else ! If ?xch:Model_Number{prop:Req} = True
        ?xch:Model_Number{prop:FontColor} = 65793
        ?xch:Model_Number{prop:Color} = 16777215
    End ! If ?xch:Model_Number{prop:Req} = True
    ?xch:Model_Number{prop:Trn} = 0
    ?xch:Model_Number{prop:FontStyle} = font:Bold
    ?XCH:Manufacturer:Prompt{prop:FontColor} = -1
    ?XCH:Manufacturer:Prompt{prop:Color} = 15066597
    If ?xch:Manufacturer{prop:ReadOnly} = True
        ?xch:Manufacturer{prop:FontColor} = 65793
        ?xch:Manufacturer{prop:Color} = 15066597
    Elsif ?xch:Manufacturer{prop:Req} = True
        ?xch:Manufacturer{prop:FontColor} = 65793
        ?xch:Manufacturer{prop:Color} = 8454143
    Else ! If ?xch:Manufacturer{prop:Req} = True
        ?xch:Manufacturer{prop:FontColor} = 65793
        ?xch:Manufacturer{prop:Color} = 16777215
    End ! If ?xch:Manufacturer{prop:Req} = True
    ?xch:Manufacturer{prop:Trn} = 0
    ?xch:Manufacturer{prop:FontStyle} = font:Bold
    ?XCH:Location:Prompt{prop:FontColor} = -1
    ?XCH:Location:Prompt{prop:Color} = 15066597
    If ?xch:Location{prop:ReadOnly} = True
        ?xch:Location{prop:FontColor} = 65793
        ?xch:Location{prop:Color} = 15066597
    Elsif ?xch:Location{prop:Req} = True
        ?xch:Location{prop:FontColor} = 65793
        ?xch:Location{prop:Color} = 8454143
    Else ! If ?xch:Location{prop:Req} = True
        ?xch:Location{prop:FontColor} = 65793
        ?xch:Location{prop:Color} = 16777215
    End ! If ?xch:Location{prop:Req} = True
    ?xch:Location{prop:Trn} = 0
    ?xch:Location{prop:FontStyle} = font:Bold
    ?XCH:Shelf_Location:Prompt{prop:FontColor} = -1
    ?XCH:Shelf_Location:Prompt{prop:Color} = 15066597
    If ?xch:Shelf_Location{prop:ReadOnly} = True
        ?xch:Shelf_Location{prop:FontColor} = 65793
        ?xch:Shelf_Location{prop:Color} = 15066597
    Elsif ?xch:Shelf_Location{prop:Req} = True
        ?xch:Shelf_Location{prop:FontColor} = 65793
        ?xch:Shelf_Location{prop:Color} = 8454143
    Else ! If ?xch:Shelf_Location{prop:Req} = True
        ?xch:Shelf_Location{prop:FontColor} = 65793
        ?xch:Shelf_Location{prop:Color} = 16777215
    End ! If ?xch:Shelf_Location{prop:Req} = True
    ?xch:Shelf_Location{prop:Trn} = 0
    ?xch:Shelf_Location{prop:FontStyle} = font:Bold
    ?XCH:Stock_Type:Prompt{prop:FontColor} = -1
    ?XCH:Stock_Type:Prompt{prop:Color} = 15066597
    If ?xch:Stock_Type{prop:ReadOnly} = True
        ?xch:Stock_Type{prop:FontColor} = 65793
        ?xch:Stock_Type{prop:Color} = 15066597
    Elsif ?xch:Stock_Type{prop:Req} = True
        ?xch:Stock_Type{prop:FontColor} = 65793
        ?xch:Stock_Type{prop:Color} = 8454143
    Else ! If ?xch:Stock_Type{prop:Req} = True
        ?xch:Stock_Type{prop:FontColor} = 65793
        ?xch:Stock_Type{prop:Color} = 16777215
    End ! If ?xch:Stock_Type{prop:Req} = True
    ?xch:Stock_Type{prop:Trn} = 0
    ?xch:Stock_Type{prop:FontStyle} = font:Bold
    ?XCH:Job_Number:Prompt{prop:FontColor} = -1
    ?XCH:Job_Number:Prompt{prop:Color} = 15066597
    If ?xch:Job_Number{prop:ReadOnly} = True
        ?xch:Job_Number{prop:FontColor} = 65793
        ?xch:Job_Number{prop:Color} = 15066597
    Elsif ?xch:Job_Number{prop:Req} = True
        ?xch:Job_Number{prop:FontColor} = 65793
        ?xch:Job_Number{prop:Color} = 8454143
    Else ! If ?xch:Job_Number{prop:Req} = True
        ?xch:Job_Number{prop:FontColor} = 65793
        ?xch:Job_Number{prop:Color} = 16777215
    End ! If ?xch:Job_Number{prop:Req} = True
    ?xch:Job_Number{prop:Trn} = 0
    ?xch:Job_Number{prop:FontStyle} = font:Bold
    ?Sheet1:2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?XCH:Ref_Number:Prompt:2{prop:FontColor} = -1
    ?XCH:Ref_Number:Prompt:2{prop:Color} = 15066597
    If ?exa:Replacement_Unit_Number{prop:ReadOnly} = True
        ?exa:Replacement_Unit_Number{prop:FontColor} = 65793
        ?exa:Replacement_Unit_Number{prop:Color} = 15066597
    Elsif ?exa:Replacement_Unit_Number{prop:Req} = True
        ?exa:Replacement_Unit_Number{prop:FontColor} = 65793
        ?exa:Replacement_Unit_Number{prop:Color} = 8454143
    Else ! If ?exa:Replacement_Unit_Number{prop:Req} = True
        ?exa:Replacement_Unit_Number{prop:FontColor} = 65793
        ?exa:Replacement_Unit_Number{prop:Color} = 16777215
    End ! If ?exa:Replacement_Unit_Number{prop:Req} = True
    ?exa:Replacement_Unit_Number{prop:Trn} = 0
    ?exa:Replacement_Unit_Number{prop:FontStyle} = font:Bold
    ?replacement_unit_status:Prompt{prop:FontColor} = -1
    ?replacement_unit_status:Prompt{prop:Color} = 15066597
    If ?replacement_unit_status{prop:ReadOnly} = True
        ?replacement_unit_status{prop:FontColor} = 65793
        ?replacement_unit_status{prop:Color} = 15066597
    Elsif ?replacement_unit_status{prop:Req} = True
        ?replacement_unit_status{prop:FontColor} = 65793
        ?replacement_unit_status{prop:Color} = 8454143
    Else ! If ?replacement_unit_status{prop:Req} = True
        ?replacement_unit_status{prop:FontColor} = 65793
        ?replacement_unit_status{prop:Color} = 16777215
    End ! If ?replacement_unit_status{prop:Req} = True
    ?replacement_unit_status{prop:Trn} = 0
    ?replacement_unit_status{prop:FontStyle} = font:Bold
    ?XCH:ESN:Prompt:2{prop:FontColor} = -1
    ?XCH:ESN:Prompt:2{prop:Color} = 15066597
    If ?xch_ali:ESN{prop:ReadOnly} = True
        ?xch_ali:ESN{prop:FontColor} = 65793
        ?xch_ali:ESN{prop:Color} = 15066597
    Elsif ?xch_ali:ESN{prop:Req} = True
        ?xch_ali:ESN{prop:FontColor} = 65793
        ?xch_ali:ESN{prop:Color} = 8454143
    Else ! If ?xch_ali:ESN{prop:Req} = True
        ?xch_ali:ESN{prop:FontColor} = 65793
        ?xch_ali:ESN{prop:Color} = 16777215
    End ! If ?xch_ali:ESN{prop:Req} = True
    ?xch_ali:ESN{prop:Trn} = 0
    ?xch_ali:ESN{prop:FontStyle} = font:Bold
    ?XCH:MSN:Prompt:2{prop:FontColor} = -1
    ?XCH:MSN:Prompt:2{prop:Color} = 15066597
    If ?xch_ali:MSN{prop:ReadOnly} = True
        ?xch_ali:MSN{prop:FontColor} = 65793
        ?xch_ali:MSN{prop:Color} = 15066597
    Elsif ?xch_ali:MSN{prop:Req} = True
        ?xch_ali:MSN{prop:FontColor} = 65793
        ?xch_ali:MSN{prop:Color} = 8454143
    Else ! If ?xch_ali:MSN{prop:Req} = True
        ?xch_ali:MSN{prop:FontColor} = 65793
        ?xch_ali:MSN{prop:Color} = 16777215
    End ! If ?xch_ali:MSN{prop:Req} = True
    ?xch_ali:MSN{prop:Trn} = 0
    ?xch_ali:MSN{prop:FontStyle} = font:Bold
    ?XCH:Model_Number:Prompt:2{prop:FontColor} = -1
    ?XCH:Model_Number:Prompt:2{prop:Color} = 15066597
    If ?xch_ali:Model_Number{prop:ReadOnly} = True
        ?xch_ali:Model_Number{prop:FontColor} = 65793
        ?xch_ali:Model_Number{prop:Color} = 15066597
    Elsif ?xch_ali:Model_Number{prop:Req} = True
        ?xch_ali:Model_Number{prop:FontColor} = 65793
        ?xch_ali:Model_Number{prop:Color} = 8454143
    Else ! If ?xch_ali:Model_Number{prop:Req} = True
        ?xch_ali:Model_Number{prop:FontColor} = 65793
        ?xch_ali:Model_Number{prop:Color} = 16777215
    End ! If ?xch_ali:Model_Number{prop:Req} = True
    ?xch_ali:Model_Number{prop:Trn} = 0
    ?xch_ali:Model_Number{prop:FontStyle} = font:Bold
    ?XCH:Manufacturer:Prompt:2{prop:FontColor} = -1
    ?XCH:Manufacturer:Prompt:2{prop:Color} = 15066597
    If ?xch_ali:Manufacturer{prop:ReadOnly} = True
        ?xch_ali:Manufacturer{prop:FontColor} = 65793
        ?xch_ali:Manufacturer{prop:Color} = 15066597
    Elsif ?xch_ali:Manufacturer{prop:Req} = True
        ?xch_ali:Manufacturer{prop:FontColor} = 65793
        ?xch_ali:Manufacturer{prop:Color} = 8454143
    Else ! If ?xch_ali:Manufacturer{prop:Req} = True
        ?xch_ali:Manufacturer{prop:FontColor} = 65793
        ?xch_ali:Manufacturer{prop:Color} = 16777215
    End ! If ?xch_ali:Manufacturer{prop:Req} = True
    ?xch_ali:Manufacturer{prop:Trn} = 0
    ?xch_ali:Manufacturer{prop:FontStyle} = font:Bold
    ?XCH:Location:Prompt:2{prop:FontColor} = -1
    ?XCH:Location:Prompt:2{prop:Color} = 15066597
    If ?xch_ali:Location{prop:ReadOnly} = True
        ?xch_ali:Location{prop:FontColor} = 65793
        ?xch_ali:Location{prop:Color} = 15066597
    Elsif ?xch_ali:Location{prop:Req} = True
        ?xch_ali:Location{prop:FontColor} = 65793
        ?xch_ali:Location{prop:Color} = 8454143
    Else ! If ?xch_ali:Location{prop:Req} = True
        ?xch_ali:Location{prop:FontColor} = 65793
        ?xch_ali:Location{prop:Color} = 16777215
    End ! If ?xch_ali:Location{prop:Req} = True
    ?xch_ali:Location{prop:Trn} = 0
    ?xch_ali:Location{prop:FontStyle} = font:Bold
    ?XCH:Shelf_Location:Prompt:2{prop:FontColor} = -1
    ?XCH:Shelf_Location:Prompt:2{prop:Color} = 15066597
    If ?xch_ali:Shelf_Location{prop:ReadOnly} = True
        ?xch_ali:Shelf_Location{prop:FontColor} = 65793
        ?xch_ali:Shelf_Location{prop:Color} = 15066597
    Elsif ?xch_ali:Shelf_Location{prop:Req} = True
        ?xch_ali:Shelf_Location{prop:FontColor} = 65793
        ?xch_ali:Shelf_Location{prop:Color} = 8454143
    Else ! If ?xch_ali:Shelf_Location{prop:Req} = True
        ?xch_ali:Shelf_Location{prop:FontColor} = 65793
        ?xch_ali:Shelf_Location{prop:Color} = 16777215
    End ! If ?xch_ali:Shelf_Location{prop:Req} = True
    ?xch_ali:Shelf_Location{prop:Trn} = 0
    ?xch_ali:Shelf_Location{prop:FontStyle} = font:Bold
    ?XCH:Stock_Type:Prompt:2{prop:FontColor} = -1
    ?XCH:Stock_Type:Prompt:2{prop:Color} = 15066597
    If ?xch_ali:Stock_Type{prop:ReadOnly} = True
        ?xch_ali:Stock_Type{prop:FontColor} = 65793
        ?xch_ali:Stock_Type{prop:Color} = 15066597
    Elsif ?xch_ali:Stock_Type{prop:Req} = True
        ?xch_ali:Stock_Type{prop:FontColor} = 65793
        ?xch_ali:Stock_Type{prop:Color} = 8454143
    Else ! If ?xch_ali:Stock_Type{prop:Req} = True
        ?xch_ali:Stock_Type{prop:FontColor} = 65793
        ?xch_ali:Stock_Type{prop:Color} = 16777215
    End ! If ?xch_ali:Stock_Type{prop:Req} = True
    ?xch_ali:Stock_Type{prop:Trn} = 0
    ?xch_ali:Stock_Type{prop:FontStyle} = font:Bold
    ?XCH:Job_Number:Prompt:2{prop:FontColor} = -1
    ?XCH:Job_Number:Prompt:2{prop:Color} = 15066597
    If ?xch_ali:Job_Number{prop:ReadOnly} = True
        ?xch_ali:Job_Number{prop:FontColor} = 65793
        ?xch_ali:Job_Number{prop:Color} = 15066597
    Elsif ?xch_ali:Job_Number{prop:Req} = True
        ?xch_ali:Job_Number{prop:FontColor} = 65793
        ?xch_ali:Job_Number{prop:Color} = 8454143
    Else ! If ?xch_ali:Job_Number{prop:Req} = True
        ?xch_ali:Job_Number{prop:FontColor} = 65793
        ?xch_ali:Job_Number{prop:Color} = 16777215
    End ! If ?xch_ali:Job_Number{prop:Req} = True
    ?xch_ali:Job_Number{prop:Trn} = 0
    ?xch_ali:Job_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Exchange_By_Audit',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Exchange_By_Audit',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Exchange_By_Audit',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Exchange_By_Audit',1)
    SolaceViewVars('stock_unit_status',stock_unit_status,'Update_Exchange_By_Audit',1)
    SolaceViewVars('replacement_unit_status',replacement_unit_status,'Update_Exchange_By_Audit',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1:2;  SolaceCtrlName = '?Panel1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EXA:Audit_Number:Prompt;  SolaceCtrlName = '?EXA:Audit_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?exa:Audit_Number;  SolaceCtrlName = '?exa:Audit_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Ref_Number:Prompt;  SolaceCtrlName = '?XCH:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?exa:Stock_Unit_Number;  SolaceCtrlName = '?exa:Stock_Unit_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stock_unit_status:Prompt;  SolaceCtrlName = '?stock_unit_status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stock_unit_status;  SolaceCtrlName = '?stock_unit_status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:ESN:Prompt;  SolaceCtrlName = '?XCH:ESN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:ESN;  SolaceCtrlName = '?xch:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:MSN:Prompt;  SolaceCtrlName = '?XCH:MSN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:MSN;  SolaceCtrlName = '?xch:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Model_Number:Prompt;  SolaceCtrlName = '?XCH:Model_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Model_Number;  SolaceCtrlName = '?xch:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Manufacturer:Prompt;  SolaceCtrlName = '?XCH:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Manufacturer;  SolaceCtrlName = '?xch:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Location:Prompt;  SolaceCtrlName = '?XCH:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Location;  SolaceCtrlName = '?xch:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Shelf_Location:Prompt;  SolaceCtrlName = '?XCH:Shelf_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Shelf_Location;  SolaceCtrlName = '?xch:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Stock_Type:Prompt;  SolaceCtrlName = '?XCH:Stock_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Stock_Type;  SolaceCtrlName = '?xch:Stock_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Job_Number:Prompt;  SolaceCtrlName = '?XCH:Job_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Job_Number;  SolaceCtrlName = '?xch:Job_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1:2;  SolaceCtrlName = '?Sheet1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Ref_Number:Prompt:2;  SolaceCtrlName = '?XCH:Ref_Number:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?exa:Replacement_Unit_Number;  SolaceCtrlName = '?exa:Replacement_Unit_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?replacement_unit_status:Prompt;  SolaceCtrlName = '?replacement_unit_status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?replacement_unit_status;  SolaceCtrlName = '?replacement_unit_status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:ESN:Prompt:2;  SolaceCtrlName = '?XCH:ESN:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch_ali:ESN;  SolaceCtrlName = '?xch_ali:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:MSN:Prompt:2;  SolaceCtrlName = '?XCH:MSN:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch_ali:MSN;  SolaceCtrlName = '?xch_ali:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Model_Number:Prompt:2;  SolaceCtrlName = '?XCH:Model_Number:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch_ali:Model_Number;  SolaceCtrlName = '?xch_ali:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Manufacturer:Prompt:2;  SolaceCtrlName = '?XCH:Manufacturer:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch_ali:Manufacturer;  SolaceCtrlName = '?xch_ali:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Location:Prompt:2;  SolaceCtrlName = '?XCH:Location:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch_ali:Location;  SolaceCtrlName = '?xch_ali:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Shelf_Location:Prompt:2;  SolaceCtrlName = '?XCH:Shelf_Location:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch_ali:Shelf_Location;  SolaceCtrlName = '?xch_ali:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Stock_Type:Prompt:2;  SolaceCtrlName = '?XCH:Stock_Type:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch_ali:Stock_Type;  SolaceCtrlName = '?xch_ali:Stock_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Job_Number:Prompt:2;  SolaceCtrlName = '?XCH:Job_Number:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch_ali:Job_Number;  SolaceCtrlName = '?xch_ali:Job_Number';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Exchange_By_Audit')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Exchange_By_Audit')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?OK
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(exa:Record,History::exa:Record)
  SELF.AddHistoryField(?exa:Audit_Number,3)
  SELF.AddHistoryField(?exa:Stock_Unit_Number,4)
  SELF.AddHistoryField(?exa:Replacement_Unit_Number,5)
  SELF.AddUpdateFile(Access:EXCAUDIT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EXCAUDIT.Open
  Access:EXCHANGE.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:EXCAUDIT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  access:exchange.clearkey(xch:ref_number_key)
  xch:ref_number = EXA:Stock_Unit_Number
  access:exchange.fetch(xch:ref_number_key)
  
  access:exchange_alias.clearkey(xch_ali:ref_number_key)
  xch_ali:ref_number = EXA:Replacement_Unit_Number
  If access:exchange_alias.fetch(xch_ali:ref_number_key)
      hide(?tab2)
  End!If access:exchange_alias.fetch(xch_ali:ref_number_key)
  
  Case XCH:Available
      Of 'AVL'
          stock_unit_status = 'AVAILABLE'
      Of 'EXC'
          stock_unit_status = 'EXCHANGED'
      Of 'INC'
          stock_unit_status = 'INCOMING TRANSIT'
      Of 'REP'
          stock_unit_status = 'IN REPAIR'
      Of 'DES'
          stock_unit_status = 'DESPATCHED'
      OF 'SUS'
          stock_unit_status = 'SUSPENDED'
  End!Case XCH:Available
  Case XCH_ALI:Available
      Of 'AVL'
          replacement_unit_status = 'AVAILABLE'
      Of 'EXC'
          replacement_unit_status = 'EXCHANGED'
      Of 'INC'
          replacement_unit_status = 'INCOMING TRANSIT'
      Of 'REP'
          replacement_unit_status = 'IN REPAIR'
      Of 'DES'
          replacement_unit_status = 'DESPATCHED'
      OF 'SUS'
          replacement_unit_status = 'SUSPENDED'
  End!Case XCH:Available
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCAUDIT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Exchange_By_Audit',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Exchange_By_Audit')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

