

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02010.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSUBTRACC PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
Discount_Rate_Labour_Temp REAL
Discount_Rate_Parts_Temp REAL
VAT_Rate_Labour_Temp REAL
VAT_Rate_Parts_Temp  REAL
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
Discount_Rate_Retail_Temp REAL
VAT_Rate_Retail_Temp REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?sub:Labour_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?sub:Parts_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:7 QUEUE                           !Queue declaration for browse/combo box using ?sub:Retail_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB14::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB15::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB26::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
BRW8::View:Browse    VIEW(SUBACCAD)
                       PROJECT(sua:AccountNumber)
                       PROJECT(sua:CompanyName)
                       PROJECT(sua:Postcode)
                       PROJECT(sua:RecordNumber)
                       PROJECT(sua:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
sua:AccountNumber      LIKE(sua:AccountNumber)        !List box control field - type derived from field
sua:CompanyName        LIKE(sua:CompanyName)          !List box control field - type derived from field
sua:Postcode           LIKE(sua:Postcode)             !List box control field - type derived from field
sua:RecordNumber       LIKE(sua:RecordNumber)         !Primary key field - type derived from field
sua:RefNumber          LIKE(sua:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(SUBEMAIL)
                       PROJECT(sue:RecipientType)
                       PROJECT(sue:ContactName)
                       PROJECT(sue:RecordNumber)
                       PROJECT(sue:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
sue:RecipientType      LIKE(sue:RecipientType)        !List box control field - type derived from field
sue:ContactName        LIKE(sue:ContactName)          !List box control field - type derived from field
sue:RecordNumber       LIKE(sue:RecordNumber)         !Primary key field - type derived from field
sue:RefNumber          LIKE(sue:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::sub:Record  LIKE(sub:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string String(255)
QuickWindow          WINDOW('Window Name'),AT(,,491,279),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateSUBTRACC'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,220,244),USE(?Sheet1),SPREAD
                         TAB('Sub Account &Details'),USE(?Tab1)
                           PROMPT('Main Account Number'),AT(8,20),USE(?SUB:Main_Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,20,,10),USE(sub:Main_Account_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),REQ,UPR,READONLY
                           PROMPT('Account Number'),AT(8,36),USE(?SUB:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,36,67,10),USE(sub:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Company Name'),AT(8,52),USE(?SUB:Company_Name:Prompt),TRN
                           ENTRY(@s40),AT(84,52,136,10),USE(sub:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Branch'),AT(8,64),USE(?SUB:Branch:Prompt),TRN
                           ENTRY(@s30),AT(84,64,136,10),USE(sub:Branch),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Postcode'),AT(8,80),USE(?SUB:Postcode:Prompt),TRN
                           ENTRY(@s15),AT(84,80,67,10),USE(sub:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('Clear'),AT(156,80,64,10),USE(?Clear_Address),SKIP,LEFT,ICON(ICON:Cut)
                           PROMPT('Address'),AT(8,92),USE(?SUB:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(84,92,136,10),USE(sub:Address_Line1),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,104,136,10),USE(sub:Address_Line2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,116,136,10),USE(sub:Address_Line3),FONT('Tahoma',8,,FONT:bold),UPR
                           STRING('Telephone Number'),AT(84,128),USE(?String1),TRN,FONT(,7,,)
                           STRING('Fax Number'),AT(156,128),USE(?String3),TRN,FONT(,7,,)
                           PROMPT('Contact Numbers'),AT(8,136),USE(?SUB:Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,136,67,10),USE(sub:Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s15),AT(156,136,64,10),USE(sub:Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Email Address'),AT(8,152),USE(?sub:EmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,152,136,10),USE(sub:EmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           PROMPT('Contact Name'),AT(8,168),USE(?SUB:Contact_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,168,136,10),USE(sub:Contact_Name),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Password'),AT(8,184),USE(?SUB:Password:Prompt),TRN
                           ENTRY(@s20),AT(84,184,136,10),USE(sub:Password),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,PASSWORD
                           PROMPT('Courier - Incoming'),AT(8,200),USE(?sub:Courier_Incoming:Prompt),TRN
                           ENTRY(@s30),AT(84,200,124,10),USE(sub:Courier_Incoming),FONT('Tahoma',8,,FONT:bold),ALRT(DownKey),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),UPR
                           BUTTON,AT(212,200,10,10),USE(?LookupIncomingCourier),SKIP,ICON('List3.ico')
                           PROMPT('Courier - Outgoing'),AT(8,212),USE(?sub:Courier_Outgoing:Prompt),TRN
                           ENTRY(@s30),AT(84,212,124,10),USE(sub:Courier_Outgoing),FONT('Tahoma',8,,FONT:bold),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,212,10,10),USE(?LookupOutgoingCourier),SKIP,ICON('List3.ico')
                         END
                       END
                       SHEET,AT(228,4,260,244),USE(?Sheet2),SPREAD
                         TAB('General'),USE(?Tab4)
                           CHECK('Use Address As Delivery Address'),AT(232,20),USE(sub:Use_Delivery_Address),VALUE('YES','NO')
                           CHECK('Use Address As Collection Address'),AT(232,32),USE(sub:Use_Collection_Address),VALUE('YES','NO')
                           CHECK('Record End User Address'),AT(232,44),USE(sub:Use_Customer_Address),TIP('Do not use the Trade Account''s Address<13,10>as the Job''s "Customer Address".'),VALUE('YES','NO')
                           CHECK('Invoice End User Address'),AT(232,56),USE(sub:Invoice_Customer_Address),TIP('Do not use the Trade Account''s Address as <13,10>the Invoice Address. Use the Job''s "' &|
   'Customer Address".'),VALUE('YES','NO')
                           CHECK('Despatch Paid Jobs Only'),AT(232,68),USE(sub:Despatch_Paid_Jobs),VALUE('YES','NO')
                           CHECK('Despatch Invoiced Jobs Only'),AT(232,80),USE(sub:Despatch_Invoiced_Jobs),VALUE('YES','NO')
                           CHECK('Produce Multiple Invoices'),AT(232,92),USE(sub:MultiInvoice),RIGHT,MSG('Multiple Invoice Account?'),TIP('Multiple Invoice Account?'),VALUE('YES','NO')
                           CHECK('Auto Send Status Emails'),AT(232,104),USE(sub:AutoSendStatusEmails),MSG('Auto Send Status Emails'),TIP('Auto Send Status Emails'),VALUE('1','0')
                           CHECK('Vodafone Retail Store'),AT(232,116),USE(sub:VodafoneRetailStore),MSG('Vodafone Retail Store'),TIP('Vodafone Retail Store'),VALUE('1','0')
                           CHECK('Vodafone Contact Centre'),AT(232,128),USE(sub:IsContactCentre),MSG('Is This A Vodafone Contact Centre?'),TIP('Is This A Vodafone Contact Centre?'),VALUE('1','0')
                           OPTION,AT(240,138,96,38),USE(sub:ContactCentreAccType),HIDE,MSG('Contact Centre Account Type : 0 - CBU, 1 - ISP, 2 - EBU')
                             RADIO('Vodafone CBU Account'),AT(240,140),USE(?sub:ContactCentreAccType:Radio1),VALUE('0')
                             RADIO('Vodafone ISP Account'),AT(240,152),USE(?sub:ContactCentreAccType:Radio2),VALUE('1')
                             RADIO('Vodafone EBU Account'),AT(240,164),USE(?sub:ContactCentreAccType:Radio3),VALUE('2')
                           END
                           GROUP,AT(228,172,256,74),USE(?EmailGroup),BOXED,HIDE
                             LIST,AT(232,182,168,40),USE(?List:2),IMM,MSG('Browsing Records'),FORMAT('107L(2)|M~Recipient Type~@s30@240L(2)|M~Contact Name~@s60@'),FROM(Queue:Browse:1)
                             BUTTON('&Insert'),AT(232,228,42,12),USE(?Insert:2)
                             BUTTON('&Change'),AT(276,228,42,12),USE(?Change:2)
                             BUTTON('&Delete'),AT(320,228,42,12),USE(?Delete:2)
                             CHECK('Email Recipient List'),AT(408,180),USE(sub:EmailRecipientList),MSG('Email Recipient List'),TIP('Email Recipient List'),VALUE('1','0')
                             CHECK('Email End User'),AT(408,190),USE(sub:EmailEndUser),MSG('Email End User'),TIP('Email End User'),VALUE('1','0')
                           END
                         END
                         TAB('Service'),USE(?ServiceDefaults)
                           PROMPT('V.A.T. Code Labour'),AT(232,20),USE(?Prompt14)
                           COMBO(@s2),AT(308,20,64,10),USE(sub:Labour_VAT_Code),VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('16L(2)|M@s2@24L(2)|M@n6.2@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           ENTRY(@n6.2b),AT(372,20,64,10),USE(VAT_Rate_Labour_Temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('V.A.T. Code Parts'),AT(232,32),USE(?Prompt17:4),TRN,FONT(,8,,)
                           COMBO(@s2),AT(308,32,64,10),USE(sub:Parts_VAT_Code),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('13L(2)|M@s2@24L(2)|M@n6.2@'),DROP(10),FROM(Queue:FileDropCombo:5)
                           ENTRY(@n6.2b),AT(372,32,64,10),USE(VAT_Rate_Parts_Temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('V.A.T. Code Retail'),AT(232,44),USE(?Prompt17:5),TRN,FONT(,8,,)
                           COMBO(@s2),AT(308,44,64,10),USE(sub:Retail_VAT_Code),IMM,VSCROLL,FONT(,,,FONT:bold,CHARSET:ANSI),FORMAT('14L(2)|M@s2@24L(2)|M@n6.2@'),DROP(10),FROM(Queue:FileDropCombo:7)
                           ENTRY(@n6.2b),AT(372,44,64,10),USE(VAT_Rate_Retail_Temp),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Default Courier Cost'),AT(232,60),USE(?SUB:Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(308,60,64,10),USE(sub:Courier_Cost),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Charge In Euros'),AT(232,200),USE(sub:EuroApplies),VALUE('1','0'),MSG('Apply Euro')
                           CHECK('Stop Account'),AT(232,232),USE(sub:Stop_Account),VALUE('YES','NO')
                           CHECK('Zero Value Chargeable Parts'),AT(232,212),USE(sub:ZeroChargeable),MSG('Don''t show Chargeable Costs'),TIP('Don''t show Chargeable Costs'),VALUE('YES','NO')
                           OPTION('Invoice Type'),AT(232,76,116,64),USE(sub:MultiInvoice,,?SUB:MultiInvoice:2),BOXED,TRN,COLOR(COLOR:Silver),MSG('Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)')
                             RADIO('Single Invoice (No Summary)'),AT(240,88),USE(?SUB:MultiIn:Radio1),VALUE('SIN')
                             RADIO('Single Invoice (Summary)'),AT(240,100),USE(?SUB:MultiIn:Radio2),VALUE('SIM')
                             RADIO('Multiple Invoice'),AT(240,112),USE(?SUB:MultiIn:Radio3),VALUE('MUL')
                             RADIO('Batch Invoice'),AT(240,124),USE(?SUB:MultiIn:Radio4),VALUE('BAT')
                           END
                           CHECK('Price Despatch Notes'),AT(232,176),USE(sub:PriceDespatchNotes),MSG('Price Despatch Notes'),TIP('Price Despatch Notes'),VALUE('1','0')
                           CHECK('Print Invoice At Despatch'),AT(232,188),USE(sub:InvoiceAtDespatch),MSG('Print Invoice At Despatch'),TIP('Print Invoice At Despatch'),VALUE('1','0')
                           OPTION('Invoice Print Type'),AT(232,144,116,27),USE(sub:InvoiceType),BOXED,MSG('Invoice Type')
                             RADIO('Manual'),AT(240,156),USE(?sub:InvoiceType:Radio3),VALUE('0')
                             RADIO('Automatic'),AT(292,156),USE(?sub:InvoiceType:Radio4),VALUE('1')
                           END
                         END
                         TAB('Retail'),USE(?RetailDefaults)
                           OPTION('Retail Sales Payment Type'),AT(232,20,140,28),USE(sub:Retail_Payment_Type),BOXED,TRN,COLOR(COLOR:Silver)
                             RADIO('Account'),AT(240,32),USE(?SUB:Retail_Payment_Type:Radio1),VALUE('ACC')
                             RADIO('Cash'),AT(320,32),USE(?SUB:Retail_Payment_Type:Radio2),VALUE('CAS')
                           END
                           OPTION('Retail Sales Price Structure'),AT(232,52,208,28),USE(sub:Retail_Price_Structure),BOXED
                             RADIO('Trade'),AT(319,64),USE(?sub:Retail_Price_Structure:Radio2),VALUE('TRA')
                             RADIO('Purchase'),AT(388,64),USE(?sub:Retail_Price_Structure:Radio3),VALUE('PUR')
                             RADIO('Retail'),AT(239,64),USE(?Option3:Radio1),VALUE('RET')
                           END
                           CHECK('Print Retail Picking Note'),AT(232,84),USE(sub:Print_Retail_Picking_Note),VALUE('YES','NO')
                           CHECK('Print Retail Despatch Note'),AT(232,96),USE(sub:Print_Retail_Despatch_Note),VALUE('YES','NO')
                         END
                         TAB('Despatch'),USE(?Tab6)
                           CHECK('Use Customer Address For Despatch'),AT(232,20),USE(sub:UseCustDespAdd),RIGHT,MSG('Use Customer Address As Despatch Address'),TIP('Do not use the Trade Account''s Address as the<13,10>Despatch Address. Use the Job''s "' &|
   'Customer Address".'),VALUE('YES','NO')
                           CHECK('Print Service Despatch Notes'),AT(232,32),USE(sub:Print_Despatch_Notes),VALUE('YES','NO')
                           CHECK('Print Despatch Note On Completion'),AT(240,44),USE(sub:Print_Despatch_Complete),VALUE('YES','NO'),MSG('Print Despatch Note At Completion')
                           CHECK('Print Despatch Note On Despatch'),AT(240,52),USE(sub:Print_Despatch_Despatch),VALUE('YES','NO'),MSG('Print Despatch Note At Despatch')
                           CHECK('Hide Address On Despatch Note'),AT(240,60),USE(sub:HideDespAdd),MSG('Hide Address On Despatch Note'),TIP('Hide Address On Despatch Note'),VALUE('1','0')
                           GROUP('Despatch Note Type'),AT(232,72,148,48),USE(?despatch_note_type),DISABLE,BOXED,TRN
                             CHECK('Despatch Note Per Item'),AT(240,84),USE(sub:Despatch_Note_Per_Item,,?SUB:Despatch_Note_Per_Item:2),VALUE('YES','NO')
                             CHECK('Multiple Despatch Note Summary'),AT(240,96),USE(sub:Summary_Despatch_Notes),VALUE('YES','NO')
                             CHECK('Print Summary Note For Individual Desp'),AT(240,108),USE(sub:IndividualSummary),MSG('Print Individual Summary Report At Despatch'),TIP('Print Individual Summary Report At Despatch'),VALUE('1','0')
                           END
                           CHECK('Use Trade Contact No on Despatch Notes'),AT(232,124),USE(sub:UseTradeContactNo),MSG('Use Trade Contact Name'),TIP('Use Trade Contact Name'),VALUE('1','0')
                           CHECK('Use Alternative Contact Nos On Despatch Note'),AT(232,140),USE(sub:UseDespatchDetails),MSG('Use Alternative Contact Nos On Despatch Note#'),TIP('Use Alternative Contact Nos On Despatch Note#'),VALUE('1','0')
                           GROUP('Alternative Contact Numbers'),AT(232,152,252,60),USE(?AlternativeNumbers),BOXED
                             PROMPT('Telephone Number'),AT(236,164),USE(?sub:AltTelephoneNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(316,164,124,10),USE(sub:AltTelephoneNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Telephone Number'),TIP('Telephone Number'),UPR
                             PROMPT('Fax Number'),AT(237,180),USE(?sub:AltFaxNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(316,180,124,10),USE(sub:AltFaxNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fax Number'),TIP('Fax Number'),UPR
                             PROMPT('Email Address'),AT(236,196),USE(?sub:AltEmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s255),AT(316,196,164,10),USE(sub:AltEmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           END
                           CHECK('Use Alternative Delivery Addresses'),AT(232,220),USE(sub:UseAlternativeAdd),MSG('Use Alternative Delivery Addresses'),TIP('Use Alternative Delivery Addresses'),VALUE('1','0')
                         END
                         TAB('Delivery Addresses'),USE(?Tab7),HIDE
                           PROMPT('Account Number'),AT(360,20),USE(?sua:AccountNumber:Prompt),TRN
                           ENTRY(@s15),AT(232,20,124,10),USE(sua:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           LIST,AT(232,36,252,160),USE(?List),IMM,MSG('Browsing Records'),FORMAT('60L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@60L(2)|M~Postcode~@s15@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(232,200,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(288,200,56,16),USE(?Change),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(344,200,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                         END
                       END
                       PANEL,AT(4,252,484,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(372,256,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(428,256,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB14               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB15               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

FDCB26               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:7         !Reference to browse queue type
                     END

BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW12                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
look:sub:Courier_Incoming                Like(sub:Courier_Incoming)
look:sub:Courier_Outgoing                Like(sub:Courier_Outgoing)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?SUB:Main_Account_Number:Prompt{prop:FontColor} = -1
    ?SUB:Main_Account_Number:Prompt{prop:Color} = 15066597
    If ?sub:Main_Account_Number{prop:ReadOnly} = True
        ?sub:Main_Account_Number{prop:FontColor} = 65793
        ?sub:Main_Account_Number{prop:Color} = 15066597
    Elsif ?sub:Main_Account_Number{prop:Req} = True
        ?sub:Main_Account_Number{prop:FontColor} = 65793
        ?sub:Main_Account_Number{prop:Color} = 8454143
    Else ! If ?sub:Main_Account_Number{prop:Req} = True
        ?sub:Main_Account_Number{prop:FontColor} = 65793
        ?sub:Main_Account_Number{prop:Color} = 16777215
    End ! If ?sub:Main_Account_Number{prop:Req} = True
    ?sub:Main_Account_Number{prop:Trn} = 0
    ?sub:Main_Account_Number{prop:FontStyle} = font:Bold
    ?SUB:Account_Number:Prompt{prop:FontColor} = -1
    ?SUB:Account_Number:Prompt{prop:Color} = 15066597
    If ?sub:Account_Number{prop:ReadOnly} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 15066597
    Elsif ?sub:Account_Number{prop:Req} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 8454143
    Else ! If ?sub:Account_Number{prop:Req} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 16777215
    End ! If ?sub:Account_Number{prop:Req} = True
    ?sub:Account_Number{prop:Trn} = 0
    ?sub:Account_Number{prop:FontStyle} = font:Bold
    ?SUB:Company_Name:Prompt{prop:FontColor} = -1
    ?SUB:Company_Name:Prompt{prop:Color} = 15066597
    If ?sub:Company_Name{prop:ReadOnly} = True
        ?sub:Company_Name{prop:FontColor} = 65793
        ?sub:Company_Name{prop:Color} = 15066597
    Elsif ?sub:Company_Name{prop:Req} = True
        ?sub:Company_Name{prop:FontColor} = 65793
        ?sub:Company_Name{prop:Color} = 8454143
    Else ! If ?sub:Company_Name{prop:Req} = True
        ?sub:Company_Name{prop:FontColor} = 65793
        ?sub:Company_Name{prop:Color} = 16777215
    End ! If ?sub:Company_Name{prop:Req} = True
    ?sub:Company_Name{prop:Trn} = 0
    ?sub:Company_Name{prop:FontStyle} = font:Bold
    ?SUB:Branch:Prompt{prop:FontColor} = -1
    ?SUB:Branch:Prompt{prop:Color} = 15066597
    If ?sub:Branch{prop:ReadOnly} = True
        ?sub:Branch{prop:FontColor} = 65793
        ?sub:Branch{prop:Color} = 15066597
    Elsif ?sub:Branch{prop:Req} = True
        ?sub:Branch{prop:FontColor} = 65793
        ?sub:Branch{prop:Color} = 8454143
    Else ! If ?sub:Branch{prop:Req} = True
        ?sub:Branch{prop:FontColor} = 65793
        ?sub:Branch{prop:Color} = 16777215
    End ! If ?sub:Branch{prop:Req} = True
    ?sub:Branch{prop:Trn} = 0
    ?sub:Branch{prop:FontStyle} = font:Bold
    ?SUB:Postcode:Prompt{prop:FontColor} = -1
    ?SUB:Postcode:Prompt{prop:Color} = 15066597
    If ?sub:Postcode{prop:ReadOnly} = True
        ?sub:Postcode{prop:FontColor} = 65793
        ?sub:Postcode{prop:Color} = 15066597
    Elsif ?sub:Postcode{prop:Req} = True
        ?sub:Postcode{prop:FontColor} = 65793
        ?sub:Postcode{prop:Color} = 8454143
    Else ! If ?sub:Postcode{prop:Req} = True
        ?sub:Postcode{prop:FontColor} = 65793
        ?sub:Postcode{prop:Color} = 16777215
    End ! If ?sub:Postcode{prop:Req} = True
    ?sub:Postcode{prop:Trn} = 0
    ?sub:Postcode{prop:FontStyle} = font:Bold
    ?SUB:Address_Line1:Prompt{prop:FontColor} = -1
    ?SUB:Address_Line1:Prompt{prop:Color} = 15066597
    If ?sub:Address_Line1{prop:ReadOnly} = True
        ?sub:Address_Line1{prop:FontColor} = 65793
        ?sub:Address_Line1{prop:Color} = 15066597
    Elsif ?sub:Address_Line1{prop:Req} = True
        ?sub:Address_Line1{prop:FontColor} = 65793
        ?sub:Address_Line1{prop:Color} = 8454143
    Else ! If ?sub:Address_Line1{prop:Req} = True
        ?sub:Address_Line1{prop:FontColor} = 65793
        ?sub:Address_Line1{prop:Color} = 16777215
    End ! If ?sub:Address_Line1{prop:Req} = True
    ?sub:Address_Line1{prop:Trn} = 0
    ?sub:Address_Line1{prop:FontStyle} = font:Bold
    If ?sub:Address_Line2{prop:ReadOnly} = True
        ?sub:Address_Line2{prop:FontColor} = 65793
        ?sub:Address_Line2{prop:Color} = 15066597
    Elsif ?sub:Address_Line2{prop:Req} = True
        ?sub:Address_Line2{prop:FontColor} = 65793
        ?sub:Address_Line2{prop:Color} = 8454143
    Else ! If ?sub:Address_Line2{prop:Req} = True
        ?sub:Address_Line2{prop:FontColor} = 65793
        ?sub:Address_Line2{prop:Color} = 16777215
    End ! If ?sub:Address_Line2{prop:Req} = True
    ?sub:Address_Line2{prop:Trn} = 0
    ?sub:Address_Line2{prop:FontStyle} = font:Bold
    If ?sub:Address_Line3{prop:ReadOnly} = True
        ?sub:Address_Line3{prop:FontColor} = 65793
        ?sub:Address_Line3{prop:Color} = 15066597
    Elsif ?sub:Address_Line3{prop:Req} = True
        ?sub:Address_Line3{prop:FontColor} = 65793
        ?sub:Address_Line3{prop:Color} = 8454143
    Else ! If ?sub:Address_Line3{prop:Req} = True
        ?sub:Address_Line3{prop:FontColor} = 65793
        ?sub:Address_Line3{prop:Color} = 16777215
    End ! If ?sub:Address_Line3{prop:Req} = True
    ?sub:Address_Line3{prop:Trn} = 0
    ?sub:Address_Line3{prop:FontStyle} = font:Bold
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?String3{prop:FontColor} = -1
    ?String3{prop:Color} = 15066597
    ?SUB:Telephone_Number:Prompt{prop:FontColor} = -1
    ?SUB:Telephone_Number:Prompt{prop:Color} = 15066597
    If ?sub:Telephone_Number{prop:ReadOnly} = True
        ?sub:Telephone_Number{prop:FontColor} = 65793
        ?sub:Telephone_Number{prop:Color} = 15066597
    Elsif ?sub:Telephone_Number{prop:Req} = True
        ?sub:Telephone_Number{prop:FontColor} = 65793
        ?sub:Telephone_Number{prop:Color} = 8454143
    Else ! If ?sub:Telephone_Number{prop:Req} = True
        ?sub:Telephone_Number{prop:FontColor} = 65793
        ?sub:Telephone_Number{prop:Color} = 16777215
    End ! If ?sub:Telephone_Number{prop:Req} = True
    ?sub:Telephone_Number{prop:Trn} = 0
    ?sub:Telephone_Number{prop:FontStyle} = font:Bold
    If ?sub:Fax_Number{prop:ReadOnly} = True
        ?sub:Fax_Number{prop:FontColor} = 65793
        ?sub:Fax_Number{prop:Color} = 15066597
    Elsif ?sub:Fax_Number{prop:Req} = True
        ?sub:Fax_Number{prop:FontColor} = 65793
        ?sub:Fax_Number{prop:Color} = 8454143
    Else ! If ?sub:Fax_Number{prop:Req} = True
        ?sub:Fax_Number{prop:FontColor} = 65793
        ?sub:Fax_Number{prop:Color} = 16777215
    End ! If ?sub:Fax_Number{prop:Req} = True
    ?sub:Fax_Number{prop:Trn} = 0
    ?sub:Fax_Number{prop:FontStyle} = font:Bold
    ?sub:EmailAddress:Prompt{prop:FontColor} = -1
    ?sub:EmailAddress:Prompt{prop:Color} = 15066597
    If ?sub:EmailAddress{prop:ReadOnly} = True
        ?sub:EmailAddress{prop:FontColor} = 65793
        ?sub:EmailAddress{prop:Color} = 15066597
    Elsif ?sub:EmailAddress{prop:Req} = True
        ?sub:EmailAddress{prop:FontColor} = 65793
        ?sub:EmailAddress{prop:Color} = 8454143
    Else ! If ?sub:EmailAddress{prop:Req} = True
        ?sub:EmailAddress{prop:FontColor} = 65793
        ?sub:EmailAddress{prop:Color} = 16777215
    End ! If ?sub:EmailAddress{prop:Req} = True
    ?sub:EmailAddress{prop:Trn} = 0
    ?sub:EmailAddress{prop:FontStyle} = font:Bold
    ?SUB:Contact_Name:Prompt{prop:FontColor} = -1
    ?SUB:Contact_Name:Prompt{prop:Color} = 15066597
    If ?sub:Contact_Name{prop:ReadOnly} = True
        ?sub:Contact_Name{prop:FontColor} = 65793
        ?sub:Contact_Name{prop:Color} = 15066597
    Elsif ?sub:Contact_Name{prop:Req} = True
        ?sub:Contact_Name{prop:FontColor} = 65793
        ?sub:Contact_Name{prop:Color} = 8454143
    Else ! If ?sub:Contact_Name{prop:Req} = True
        ?sub:Contact_Name{prop:FontColor} = 65793
        ?sub:Contact_Name{prop:Color} = 16777215
    End ! If ?sub:Contact_Name{prop:Req} = True
    ?sub:Contact_Name{prop:Trn} = 0
    ?sub:Contact_Name{prop:FontStyle} = font:Bold
    ?SUB:Password:Prompt{prop:FontColor} = -1
    ?SUB:Password:Prompt{prop:Color} = 15066597
    If ?sub:Password{prop:ReadOnly} = True
        ?sub:Password{prop:FontColor} = 65793
        ?sub:Password{prop:Color} = 15066597
    Elsif ?sub:Password{prop:Req} = True
        ?sub:Password{prop:FontColor} = 65793
        ?sub:Password{prop:Color} = 8454143
    Else ! If ?sub:Password{prop:Req} = True
        ?sub:Password{prop:FontColor} = 65793
        ?sub:Password{prop:Color} = 16777215
    End ! If ?sub:Password{prop:Req} = True
    ?sub:Password{prop:Trn} = 0
    ?sub:Password{prop:FontStyle} = font:Bold
    ?sub:Courier_Incoming:Prompt{prop:FontColor} = -1
    ?sub:Courier_Incoming:Prompt{prop:Color} = 15066597
    If ?sub:Courier_Incoming{prop:ReadOnly} = True
        ?sub:Courier_Incoming{prop:FontColor} = 65793
        ?sub:Courier_Incoming{prop:Color} = 15066597
    Elsif ?sub:Courier_Incoming{prop:Req} = True
        ?sub:Courier_Incoming{prop:FontColor} = 65793
        ?sub:Courier_Incoming{prop:Color} = 8454143
    Else ! If ?sub:Courier_Incoming{prop:Req} = True
        ?sub:Courier_Incoming{prop:FontColor} = 65793
        ?sub:Courier_Incoming{prop:Color} = 16777215
    End ! If ?sub:Courier_Incoming{prop:Req} = True
    ?sub:Courier_Incoming{prop:Trn} = 0
    ?sub:Courier_Incoming{prop:FontStyle} = font:Bold
    ?sub:Courier_Outgoing:Prompt{prop:FontColor} = -1
    ?sub:Courier_Outgoing:Prompt{prop:Color} = 15066597
    If ?sub:Courier_Outgoing{prop:ReadOnly} = True
        ?sub:Courier_Outgoing{prop:FontColor} = 65793
        ?sub:Courier_Outgoing{prop:Color} = 15066597
    Elsif ?sub:Courier_Outgoing{prop:Req} = True
        ?sub:Courier_Outgoing{prop:FontColor} = 65793
        ?sub:Courier_Outgoing{prop:Color} = 8454143
    Else ! If ?sub:Courier_Outgoing{prop:Req} = True
        ?sub:Courier_Outgoing{prop:FontColor} = 65793
        ?sub:Courier_Outgoing{prop:Color} = 16777215
    End ! If ?sub:Courier_Outgoing{prop:Req} = True
    ?sub:Courier_Outgoing{prop:Trn} = 0
    ?sub:Courier_Outgoing{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    ?sub:Use_Delivery_Address{prop:Font,3} = -1
    ?sub:Use_Delivery_Address{prop:Color} = 15066597
    ?sub:Use_Delivery_Address{prop:Trn} = 0
    ?sub:Use_Collection_Address{prop:Font,3} = -1
    ?sub:Use_Collection_Address{prop:Color} = 15066597
    ?sub:Use_Collection_Address{prop:Trn} = 0
    ?sub:Use_Customer_Address{prop:Font,3} = -1
    ?sub:Use_Customer_Address{prop:Color} = 15066597
    ?sub:Use_Customer_Address{prop:Trn} = 0
    ?sub:Invoice_Customer_Address{prop:Font,3} = -1
    ?sub:Invoice_Customer_Address{prop:Color} = 15066597
    ?sub:Invoice_Customer_Address{prop:Trn} = 0
    ?sub:Despatch_Paid_Jobs{prop:Font,3} = -1
    ?sub:Despatch_Paid_Jobs{prop:Color} = 15066597
    ?sub:Despatch_Paid_Jobs{prop:Trn} = 0
    ?sub:Despatch_Invoiced_Jobs{prop:Font,3} = -1
    ?sub:Despatch_Invoiced_Jobs{prop:Color} = 15066597
    ?sub:Despatch_Invoiced_Jobs{prop:Trn} = 0
    ?sub:MultiInvoice{prop:Font,3} = -1
    ?sub:MultiInvoice{prop:Color} = 15066597
    ?sub:MultiInvoice{prop:Trn} = 0
    ?sub:AutoSendStatusEmails{prop:Font,3} = -1
    ?sub:AutoSendStatusEmails{prop:Color} = 15066597
    ?sub:AutoSendStatusEmails{prop:Trn} = 0
    ?sub:VodafoneRetailStore{prop:Font,3} = -1
    ?sub:VodafoneRetailStore{prop:Color} = 15066597
    ?sub:VodafoneRetailStore{prop:Trn} = 0
    ?sub:IsContactCentre{prop:Font,3} = -1
    ?sub:IsContactCentre{prop:Color} = 15066597
    ?sub:IsContactCentre{prop:Trn} = 0
    ?sub:ContactCentreAccType{prop:Font,3} = -1
    ?sub:ContactCentreAccType{prop:Color} = 15066597
    ?sub:ContactCentreAccType{prop:Trn} = 0
    ?sub:ContactCentreAccType:Radio1{prop:Font,3} = -1
    ?sub:ContactCentreAccType:Radio1{prop:Color} = 15066597
    ?sub:ContactCentreAccType:Radio1{prop:Trn} = 0
    ?sub:ContactCentreAccType:Radio2{prop:Font,3} = -1
    ?sub:ContactCentreAccType:Radio2{prop:Color} = 15066597
    ?sub:ContactCentreAccType:Radio2{prop:Trn} = 0
    ?sub:ContactCentreAccType:Radio3{prop:Font,3} = -1
    ?sub:ContactCentreAccType:Radio3{prop:Color} = 15066597
    ?sub:ContactCentreAccType:Radio3{prop:Trn} = 0
    ?EmailGroup{prop:Font,3} = -1
    ?EmailGroup{prop:Color} = 15066597
    ?EmailGroup{prop:Trn} = 0
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?sub:EmailRecipientList{prop:Font,3} = -1
    ?sub:EmailRecipientList{prop:Color} = 15066597
    ?sub:EmailRecipientList{prop:Trn} = 0
    ?sub:EmailEndUser{prop:Font,3} = -1
    ?sub:EmailEndUser{prop:Color} = 15066597
    ?sub:EmailEndUser{prop:Trn} = 0
    ?ServiceDefaults{prop:Color} = 15066597
    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    If ?sub:Labour_VAT_Code{prop:ReadOnly} = True
        ?sub:Labour_VAT_Code{prop:FontColor} = 65793
        ?sub:Labour_VAT_Code{prop:Color} = 15066597
    Elsif ?sub:Labour_VAT_Code{prop:Req} = True
        ?sub:Labour_VAT_Code{prop:FontColor} = 65793
        ?sub:Labour_VAT_Code{prop:Color} = 8454143
    Else ! If ?sub:Labour_VAT_Code{prop:Req} = True
        ?sub:Labour_VAT_Code{prop:FontColor} = 65793
        ?sub:Labour_VAT_Code{prop:Color} = 16777215
    End ! If ?sub:Labour_VAT_Code{prop:Req} = True
    ?sub:Labour_VAT_Code{prop:Trn} = 0
    ?sub:Labour_VAT_Code{prop:FontStyle} = font:Bold
    If ?VAT_Rate_Labour_Temp{prop:ReadOnly} = True
        ?VAT_Rate_Labour_Temp{prop:FontColor} = 65793
        ?VAT_Rate_Labour_Temp{prop:Color} = 15066597
    Elsif ?VAT_Rate_Labour_Temp{prop:Req} = True
        ?VAT_Rate_Labour_Temp{prop:FontColor} = 65793
        ?VAT_Rate_Labour_Temp{prop:Color} = 8454143
    Else ! If ?VAT_Rate_Labour_Temp{prop:Req} = True
        ?VAT_Rate_Labour_Temp{prop:FontColor} = 65793
        ?VAT_Rate_Labour_Temp{prop:Color} = 16777215
    End ! If ?VAT_Rate_Labour_Temp{prop:Req} = True
    ?VAT_Rate_Labour_Temp{prop:Trn} = 0
    ?VAT_Rate_Labour_Temp{prop:FontStyle} = font:Bold
    ?Prompt17:4{prop:FontColor} = -1
    ?Prompt17:4{prop:Color} = 15066597
    If ?sub:Parts_VAT_Code{prop:ReadOnly} = True
        ?sub:Parts_VAT_Code{prop:FontColor} = 65793
        ?sub:Parts_VAT_Code{prop:Color} = 15066597
    Elsif ?sub:Parts_VAT_Code{prop:Req} = True
        ?sub:Parts_VAT_Code{prop:FontColor} = 65793
        ?sub:Parts_VAT_Code{prop:Color} = 8454143
    Else ! If ?sub:Parts_VAT_Code{prop:Req} = True
        ?sub:Parts_VAT_Code{prop:FontColor} = 65793
        ?sub:Parts_VAT_Code{prop:Color} = 16777215
    End ! If ?sub:Parts_VAT_Code{prop:Req} = True
    ?sub:Parts_VAT_Code{prop:Trn} = 0
    ?sub:Parts_VAT_Code{prop:FontStyle} = font:Bold
    If ?VAT_Rate_Parts_Temp{prop:ReadOnly} = True
        ?VAT_Rate_Parts_Temp{prop:FontColor} = 65793
        ?VAT_Rate_Parts_Temp{prop:Color} = 15066597
    Elsif ?VAT_Rate_Parts_Temp{prop:Req} = True
        ?VAT_Rate_Parts_Temp{prop:FontColor} = 65793
        ?VAT_Rate_Parts_Temp{prop:Color} = 8454143
    Else ! If ?VAT_Rate_Parts_Temp{prop:Req} = True
        ?VAT_Rate_Parts_Temp{prop:FontColor} = 65793
        ?VAT_Rate_Parts_Temp{prop:Color} = 16777215
    End ! If ?VAT_Rate_Parts_Temp{prop:Req} = True
    ?VAT_Rate_Parts_Temp{prop:Trn} = 0
    ?VAT_Rate_Parts_Temp{prop:FontStyle} = font:Bold
    ?Prompt17:5{prop:FontColor} = -1
    ?Prompt17:5{prop:Color} = 15066597
    If ?sub:Retail_VAT_Code{prop:ReadOnly} = True
        ?sub:Retail_VAT_Code{prop:FontColor} = 65793
        ?sub:Retail_VAT_Code{prop:Color} = 15066597
    Elsif ?sub:Retail_VAT_Code{prop:Req} = True
        ?sub:Retail_VAT_Code{prop:FontColor} = 65793
        ?sub:Retail_VAT_Code{prop:Color} = 8454143
    Else ! If ?sub:Retail_VAT_Code{prop:Req} = True
        ?sub:Retail_VAT_Code{prop:FontColor} = 65793
        ?sub:Retail_VAT_Code{prop:Color} = 16777215
    End ! If ?sub:Retail_VAT_Code{prop:Req} = True
    ?sub:Retail_VAT_Code{prop:Trn} = 0
    ?sub:Retail_VAT_Code{prop:FontStyle} = font:Bold
    If ?VAT_Rate_Retail_Temp{prop:ReadOnly} = True
        ?VAT_Rate_Retail_Temp{prop:FontColor} = 65793
        ?VAT_Rate_Retail_Temp{prop:Color} = 15066597
    Elsif ?VAT_Rate_Retail_Temp{prop:Req} = True
        ?VAT_Rate_Retail_Temp{prop:FontColor} = 65793
        ?VAT_Rate_Retail_Temp{prop:Color} = 8454143
    Else ! If ?VAT_Rate_Retail_Temp{prop:Req} = True
        ?VAT_Rate_Retail_Temp{prop:FontColor} = 65793
        ?VAT_Rate_Retail_Temp{prop:Color} = 16777215
    End ! If ?VAT_Rate_Retail_Temp{prop:Req} = True
    ?VAT_Rate_Retail_Temp{prop:Trn} = 0
    ?VAT_Rate_Retail_Temp{prop:FontStyle} = font:Bold
    ?SUB:Courier_Cost:Prompt{prop:FontColor} = -1
    ?SUB:Courier_Cost:Prompt{prop:Color} = 15066597
    If ?sub:Courier_Cost{prop:ReadOnly} = True
        ?sub:Courier_Cost{prop:FontColor} = 65793
        ?sub:Courier_Cost{prop:Color} = 15066597
    Elsif ?sub:Courier_Cost{prop:Req} = True
        ?sub:Courier_Cost{prop:FontColor} = 65793
        ?sub:Courier_Cost{prop:Color} = 8454143
    Else ! If ?sub:Courier_Cost{prop:Req} = True
        ?sub:Courier_Cost{prop:FontColor} = 65793
        ?sub:Courier_Cost{prop:Color} = 16777215
    End ! If ?sub:Courier_Cost{prop:Req} = True
    ?sub:Courier_Cost{prop:Trn} = 0
    ?sub:Courier_Cost{prop:FontStyle} = font:Bold
    ?sub:EuroApplies{prop:Font,3} = -1
    ?sub:EuroApplies{prop:Color} = 15066597
    ?sub:EuroApplies{prop:Trn} = 0
    ?sub:Stop_Account{prop:Font,3} = -1
    ?sub:Stop_Account{prop:Color} = 15066597
    ?sub:Stop_Account{prop:Trn} = 0
    ?sub:ZeroChargeable{prop:Font,3} = -1
    ?sub:ZeroChargeable{prop:Color} = 15066597
    ?sub:ZeroChargeable{prop:Trn} = 0
    ?SUB:MultiInvoice:2{prop:Font,3} = -1
    ?SUB:MultiInvoice:2{prop:Color} = 15066597
    ?SUB:MultiInvoice:2{prop:Trn} = 0
    ?SUB:MultiIn:Radio1{prop:Font,3} = -1
    ?SUB:MultiIn:Radio1{prop:Color} = 15066597
    ?SUB:MultiIn:Radio1{prop:Trn} = 0
    ?SUB:MultiIn:Radio2{prop:Font,3} = -1
    ?SUB:MultiIn:Radio2{prop:Color} = 15066597
    ?SUB:MultiIn:Radio2{prop:Trn} = 0
    ?SUB:MultiIn:Radio3{prop:Font,3} = -1
    ?SUB:MultiIn:Radio3{prop:Color} = 15066597
    ?SUB:MultiIn:Radio3{prop:Trn} = 0
    ?SUB:MultiIn:Radio4{prop:Font,3} = -1
    ?SUB:MultiIn:Radio4{prop:Color} = 15066597
    ?SUB:MultiIn:Radio4{prop:Trn} = 0
    ?sub:PriceDespatchNotes{prop:Font,3} = -1
    ?sub:PriceDespatchNotes{prop:Color} = 15066597
    ?sub:PriceDespatchNotes{prop:Trn} = 0
    ?sub:InvoiceAtDespatch{prop:Font,3} = -1
    ?sub:InvoiceAtDespatch{prop:Color} = 15066597
    ?sub:InvoiceAtDespatch{prop:Trn} = 0
    ?sub:InvoiceType{prop:Font,3} = -1
    ?sub:InvoiceType{prop:Color} = 15066597
    ?sub:InvoiceType{prop:Trn} = 0
    ?sub:InvoiceType:Radio3{prop:Font,3} = -1
    ?sub:InvoiceType:Radio3{prop:Color} = 15066597
    ?sub:InvoiceType:Radio3{prop:Trn} = 0
    ?sub:InvoiceType:Radio4{prop:Font,3} = -1
    ?sub:InvoiceType:Radio4{prop:Color} = 15066597
    ?sub:InvoiceType:Radio4{prop:Trn} = 0
    ?RetailDefaults{prop:Color} = 15066597
    ?sub:Retail_Payment_Type{prop:Font,3} = -1
    ?sub:Retail_Payment_Type{prop:Color} = 15066597
    ?sub:Retail_Payment_Type{prop:Trn} = 0
    ?SUB:Retail_Payment_Type:Radio1{prop:Font,3} = -1
    ?SUB:Retail_Payment_Type:Radio1{prop:Color} = 15066597
    ?SUB:Retail_Payment_Type:Radio1{prop:Trn} = 0
    ?SUB:Retail_Payment_Type:Radio2{prop:Font,3} = -1
    ?SUB:Retail_Payment_Type:Radio2{prop:Color} = 15066597
    ?SUB:Retail_Payment_Type:Radio2{prop:Trn} = 0
    ?sub:Retail_Price_Structure{prop:Font,3} = -1
    ?sub:Retail_Price_Structure{prop:Color} = 15066597
    ?sub:Retail_Price_Structure{prop:Trn} = 0
    ?sub:Retail_Price_Structure:Radio2{prop:Font,3} = -1
    ?sub:Retail_Price_Structure:Radio2{prop:Color} = 15066597
    ?sub:Retail_Price_Structure:Radio2{prop:Trn} = 0
    ?sub:Retail_Price_Structure:Radio3{prop:Font,3} = -1
    ?sub:Retail_Price_Structure:Radio3{prop:Color} = 15066597
    ?sub:Retail_Price_Structure:Radio3{prop:Trn} = 0
    ?Option3:Radio1{prop:Font,3} = -1
    ?Option3:Radio1{prop:Color} = 15066597
    ?Option3:Radio1{prop:Trn} = 0
    ?sub:Print_Retail_Picking_Note{prop:Font,3} = -1
    ?sub:Print_Retail_Picking_Note{prop:Color} = 15066597
    ?sub:Print_Retail_Picking_Note{prop:Trn} = 0
    ?sub:Print_Retail_Despatch_Note{prop:Font,3} = -1
    ?sub:Print_Retail_Despatch_Note{prop:Color} = 15066597
    ?sub:Print_Retail_Despatch_Note{prop:Trn} = 0
    ?Tab6{prop:Color} = 15066597
    ?sub:UseCustDespAdd{prop:Font,3} = -1
    ?sub:UseCustDespAdd{prop:Color} = 15066597
    ?sub:UseCustDespAdd{prop:Trn} = 0
    ?sub:Print_Despatch_Notes{prop:Font,3} = -1
    ?sub:Print_Despatch_Notes{prop:Color} = 15066597
    ?sub:Print_Despatch_Notes{prop:Trn} = 0
    ?sub:Print_Despatch_Complete{prop:Font,3} = -1
    ?sub:Print_Despatch_Complete{prop:Color} = 15066597
    ?sub:Print_Despatch_Complete{prop:Trn} = 0
    ?sub:Print_Despatch_Despatch{prop:Font,3} = -1
    ?sub:Print_Despatch_Despatch{prop:Color} = 15066597
    ?sub:Print_Despatch_Despatch{prop:Trn} = 0
    ?sub:HideDespAdd{prop:Font,3} = -1
    ?sub:HideDespAdd{prop:Color} = 15066597
    ?sub:HideDespAdd{prop:Trn} = 0
    ?despatch_note_type{prop:Font,3} = -1
    ?despatch_note_type{prop:Color} = 15066597
    ?despatch_note_type{prop:Trn} = 0
    ?SUB:Despatch_Note_Per_Item:2{prop:Font,3} = -1
    ?SUB:Despatch_Note_Per_Item:2{prop:Color} = 15066597
    ?SUB:Despatch_Note_Per_Item:2{prop:Trn} = 0
    ?sub:Summary_Despatch_Notes{prop:Font,3} = -1
    ?sub:Summary_Despatch_Notes{prop:Color} = 15066597
    ?sub:Summary_Despatch_Notes{prop:Trn} = 0
    ?sub:IndividualSummary{prop:Font,3} = -1
    ?sub:IndividualSummary{prop:Color} = 15066597
    ?sub:IndividualSummary{prop:Trn} = 0
    ?sub:UseTradeContactNo{prop:Font,3} = -1
    ?sub:UseTradeContactNo{prop:Color} = 15066597
    ?sub:UseTradeContactNo{prop:Trn} = 0
    ?sub:UseDespatchDetails{prop:Font,3} = -1
    ?sub:UseDespatchDetails{prop:Color} = 15066597
    ?sub:UseDespatchDetails{prop:Trn} = 0
    ?AlternativeNumbers{prop:Font,3} = -1
    ?AlternativeNumbers{prop:Color} = 15066597
    ?AlternativeNumbers{prop:Trn} = 0
    ?sub:AltTelephoneNumber:Prompt{prop:FontColor} = -1
    ?sub:AltTelephoneNumber:Prompt{prop:Color} = 15066597
    If ?sub:AltTelephoneNumber{prop:ReadOnly} = True
        ?sub:AltTelephoneNumber{prop:FontColor} = 65793
        ?sub:AltTelephoneNumber{prop:Color} = 15066597
    Elsif ?sub:AltTelephoneNumber{prop:Req} = True
        ?sub:AltTelephoneNumber{prop:FontColor} = 65793
        ?sub:AltTelephoneNumber{prop:Color} = 8454143
    Else ! If ?sub:AltTelephoneNumber{prop:Req} = True
        ?sub:AltTelephoneNumber{prop:FontColor} = 65793
        ?sub:AltTelephoneNumber{prop:Color} = 16777215
    End ! If ?sub:AltTelephoneNumber{prop:Req} = True
    ?sub:AltTelephoneNumber{prop:Trn} = 0
    ?sub:AltTelephoneNumber{prop:FontStyle} = font:Bold
    ?sub:AltFaxNumber:Prompt{prop:FontColor} = -1
    ?sub:AltFaxNumber:Prompt{prop:Color} = 15066597
    If ?sub:AltFaxNumber{prop:ReadOnly} = True
        ?sub:AltFaxNumber{prop:FontColor} = 65793
        ?sub:AltFaxNumber{prop:Color} = 15066597
    Elsif ?sub:AltFaxNumber{prop:Req} = True
        ?sub:AltFaxNumber{prop:FontColor} = 65793
        ?sub:AltFaxNumber{prop:Color} = 8454143
    Else ! If ?sub:AltFaxNumber{prop:Req} = True
        ?sub:AltFaxNumber{prop:FontColor} = 65793
        ?sub:AltFaxNumber{prop:Color} = 16777215
    End ! If ?sub:AltFaxNumber{prop:Req} = True
    ?sub:AltFaxNumber{prop:Trn} = 0
    ?sub:AltFaxNumber{prop:FontStyle} = font:Bold
    ?sub:AltEmailAddress:Prompt{prop:FontColor} = -1
    ?sub:AltEmailAddress:Prompt{prop:Color} = 15066597
    If ?sub:AltEmailAddress{prop:ReadOnly} = True
        ?sub:AltEmailAddress{prop:FontColor} = 65793
        ?sub:AltEmailAddress{prop:Color} = 15066597
    Elsif ?sub:AltEmailAddress{prop:Req} = True
        ?sub:AltEmailAddress{prop:FontColor} = 65793
        ?sub:AltEmailAddress{prop:Color} = 8454143
    Else ! If ?sub:AltEmailAddress{prop:Req} = True
        ?sub:AltEmailAddress{prop:FontColor} = 65793
        ?sub:AltEmailAddress{prop:Color} = 16777215
    End ! If ?sub:AltEmailAddress{prop:Req} = True
    ?sub:AltEmailAddress{prop:Trn} = 0
    ?sub:AltEmailAddress{prop:FontStyle} = font:Bold
    ?sub:UseAlternativeAdd{prop:Font,3} = -1
    ?sub:UseAlternativeAdd{prop:Color} = 15066597
    ?sub:UseAlternativeAdd{prop:Trn} = 0
    ?Tab7{prop:Color} = 15066597
    ?sua:AccountNumber:Prompt{prop:FontColor} = -1
    ?sua:AccountNumber:Prompt{prop:Color} = 15066597
    If ?sua:AccountNumber{prop:ReadOnly} = True
        ?sua:AccountNumber{prop:FontColor} = 65793
        ?sua:AccountNumber{prop:Color} = 15066597
    Elsif ?sua:AccountNumber{prop:Req} = True
        ?sua:AccountNumber{prop:FontColor} = 65793
        ?sua:AccountNumber{prop:Color} = 8454143
    Else ! If ?sua:AccountNumber{prop:Req} = True
        ?sua:AccountNumber{prop:FontColor} = 65793
        ?sua:AccountNumber{prop:Color} = 16777215
    End ! If ?sua:AccountNumber{prop:Req} = True
    ?sua:AccountNumber{prop:Trn} = 0
    ?sua:AccountNumber{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Fill_Rates      Routine
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = sub:labour_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_rate_labour_temp  = vat:vat_rate
    end
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = sub:parts_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_rate_parts_temp  = vat:vat_rate
    end
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = sub:retail_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_rate_retail_temp  = vat:vat_rate
    end

    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSUBTRACC',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateSUBTRACC',1)
    SolaceViewVars('Discount_Rate_Labour_Temp',Discount_Rate_Labour_Temp,'UpdateSUBTRACC',1)
    SolaceViewVars('Discount_Rate_Parts_Temp',Discount_Rate_Parts_Temp,'UpdateSUBTRACC',1)
    SolaceViewVars('VAT_Rate_Labour_Temp',VAT_Rate_Labour_Temp,'UpdateSUBTRACC',1)
    SolaceViewVars('VAT_Rate_Parts_Temp',VAT_Rate_Parts_Temp,'UpdateSUBTRACC',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateSUBTRACC',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateSUBTRACC',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateSUBTRACC',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateSUBTRACC',1)
    SolaceViewVars('Discount_Rate_Retail_Temp',Discount_Rate_Retail_Temp,'UpdateSUBTRACC',1)
    SolaceViewVars('VAT_Rate_Retail_Temp',VAT_Rate_Retail_Temp,'UpdateSUBTRACC',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Main_Account_Number:Prompt;  SolaceCtrlName = '?SUB:Main_Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Main_Account_Number;  SolaceCtrlName = '?sub:Main_Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Account_Number:Prompt;  SolaceCtrlName = '?SUB:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Account_Number;  SolaceCtrlName = '?sub:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Company_Name:Prompt;  SolaceCtrlName = '?SUB:Company_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Company_Name;  SolaceCtrlName = '?sub:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Branch:Prompt;  SolaceCtrlName = '?SUB:Branch:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Branch;  SolaceCtrlName = '?sub:Branch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Postcode:Prompt;  SolaceCtrlName = '?SUB:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Postcode;  SolaceCtrlName = '?sub:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Clear_Address;  SolaceCtrlName = '?Clear_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Address_Line1:Prompt;  SolaceCtrlName = '?SUB:Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Address_Line1;  SolaceCtrlName = '?sub:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Address_Line2;  SolaceCtrlName = '?sub:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Address_Line3;  SolaceCtrlName = '?sub:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String3;  SolaceCtrlName = '?String3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Telephone_Number:Prompt;  SolaceCtrlName = '?SUB:Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Telephone_Number;  SolaceCtrlName = '?sub:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Fax_Number;  SolaceCtrlName = '?sub:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:EmailAddress:Prompt;  SolaceCtrlName = '?sub:EmailAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:EmailAddress;  SolaceCtrlName = '?sub:EmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Contact_Name:Prompt;  SolaceCtrlName = '?SUB:Contact_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Contact_Name;  SolaceCtrlName = '?sub:Contact_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Password:Prompt;  SolaceCtrlName = '?SUB:Password:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Password;  SolaceCtrlName = '?sub:Password';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Courier_Incoming:Prompt;  SolaceCtrlName = '?sub:Courier_Incoming:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Courier_Incoming;  SolaceCtrlName = '?sub:Courier_Incoming';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupIncomingCourier;  SolaceCtrlName = '?LookupIncomingCourier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Courier_Outgoing:Prompt;  SolaceCtrlName = '?sub:Courier_Outgoing:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Courier_Outgoing;  SolaceCtrlName = '?sub:Courier_Outgoing';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupOutgoingCourier;  SolaceCtrlName = '?LookupOutgoingCourier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Use_Delivery_Address;  SolaceCtrlName = '?sub:Use_Delivery_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Use_Collection_Address;  SolaceCtrlName = '?sub:Use_Collection_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Use_Customer_Address;  SolaceCtrlName = '?sub:Use_Customer_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Invoice_Customer_Address;  SolaceCtrlName = '?sub:Invoice_Customer_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Despatch_Paid_Jobs;  SolaceCtrlName = '?sub:Despatch_Paid_Jobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Despatch_Invoiced_Jobs;  SolaceCtrlName = '?sub:Despatch_Invoiced_Jobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:MultiInvoice;  SolaceCtrlName = '?sub:MultiInvoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:AutoSendStatusEmails;  SolaceCtrlName = '?sub:AutoSendStatusEmails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:VodafoneRetailStore;  SolaceCtrlName = '?sub:VodafoneRetailStore';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:IsContactCentre;  SolaceCtrlName = '?sub:IsContactCentre';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:ContactCentreAccType;  SolaceCtrlName = '?sub:ContactCentreAccType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:ContactCentreAccType:Radio1;  SolaceCtrlName = '?sub:ContactCentreAccType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:ContactCentreAccType:Radio2;  SolaceCtrlName = '?sub:ContactCentreAccType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:ContactCentreAccType:Radio3;  SolaceCtrlName = '?sub:ContactCentreAccType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EmailGroup;  SolaceCtrlName = '?EmailGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:EmailRecipientList;  SolaceCtrlName = '?sub:EmailRecipientList';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:EmailEndUser;  SolaceCtrlName = '?sub:EmailEndUser';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ServiceDefaults;  SolaceCtrlName = '?ServiceDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14;  SolaceCtrlName = '?Prompt14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Labour_VAT_Code;  SolaceCtrlName = '?sub:Labour_VAT_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VAT_Rate_Labour_Temp;  SolaceCtrlName = '?VAT_Rate_Labour_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17:4;  SolaceCtrlName = '?Prompt17:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Parts_VAT_Code;  SolaceCtrlName = '?sub:Parts_VAT_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VAT_Rate_Parts_Temp;  SolaceCtrlName = '?VAT_Rate_Parts_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17:5;  SolaceCtrlName = '?Prompt17:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Retail_VAT_Code;  SolaceCtrlName = '?sub:Retail_VAT_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VAT_Rate_Retail_Temp;  SolaceCtrlName = '?VAT_Rate_Retail_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Courier_Cost:Prompt;  SolaceCtrlName = '?SUB:Courier_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Courier_Cost;  SolaceCtrlName = '?sub:Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:EuroApplies;  SolaceCtrlName = '?sub:EuroApplies';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Stop_Account;  SolaceCtrlName = '?sub:Stop_Account';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:ZeroChargeable;  SolaceCtrlName = '?sub:ZeroChargeable';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:MultiInvoice:2;  SolaceCtrlName = '?SUB:MultiInvoice:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:MultiIn:Radio1;  SolaceCtrlName = '?SUB:MultiIn:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:MultiIn:Radio2;  SolaceCtrlName = '?SUB:MultiIn:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:MultiIn:Radio3;  SolaceCtrlName = '?SUB:MultiIn:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:MultiIn:Radio4;  SolaceCtrlName = '?SUB:MultiIn:Radio4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:PriceDespatchNotes;  SolaceCtrlName = '?sub:PriceDespatchNotes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:InvoiceAtDespatch;  SolaceCtrlName = '?sub:InvoiceAtDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:InvoiceType;  SolaceCtrlName = '?sub:InvoiceType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:InvoiceType:Radio3;  SolaceCtrlName = '?sub:InvoiceType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:InvoiceType:Radio4;  SolaceCtrlName = '?sub:InvoiceType:Radio4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailDefaults;  SolaceCtrlName = '?RetailDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Retail_Payment_Type;  SolaceCtrlName = '?sub:Retail_Payment_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Retail_Payment_Type:Radio1;  SolaceCtrlName = '?SUB:Retail_Payment_Type:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Retail_Payment_Type:Radio2;  SolaceCtrlName = '?SUB:Retail_Payment_Type:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Retail_Price_Structure;  SolaceCtrlName = '?sub:Retail_Price_Structure';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Retail_Price_Structure:Radio2;  SolaceCtrlName = '?sub:Retail_Price_Structure:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Retail_Price_Structure:Radio3;  SolaceCtrlName = '?sub:Retail_Price_Structure:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option3:Radio1;  SolaceCtrlName = '?Option3:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Print_Retail_Picking_Note;  SolaceCtrlName = '?sub:Print_Retail_Picking_Note';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Print_Retail_Despatch_Note;  SolaceCtrlName = '?sub:Print_Retail_Despatch_Note';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:UseCustDespAdd;  SolaceCtrlName = '?sub:UseCustDespAdd';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Print_Despatch_Notes;  SolaceCtrlName = '?sub:Print_Despatch_Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Print_Despatch_Complete;  SolaceCtrlName = '?sub:Print_Despatch_Complete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Print_Despatch_Despatch;  SolaceCtrlName = '?sub:Print_Despatch_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:HideDespAdd;  SolaceCtrlName = '?sub:HideDespAdd';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_note_type;  SolaceCtrlName = '?despatch_note_type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SUB:Despatch_Note_Per_Item:2;  SolaceCtrlName = '?SUB:Despatch_Note_Per_Item:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Summary_Despatch_Notes;  SolaceCtrlName = '?sub:Summary_Despatch_Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:IndividualSummary;  SolaceCtrlName = '?sub:IndividualSummary';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:UseTradeContactNo;  SolaceCtrlName = '?sub:UseTradeContactNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:UseDespatchDetails;  SolaceCtrlName = '?sub:UseDespatchDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AlternativeNumbers;  SolaceCtrlName = '?AlternativeNumbers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:AltTelephoneNumber:Prompt;  SolaceCtrlName = '?sub:AltTelephoneNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:AltTelephoneNumber;  SolaceCtrlName = '?sub:AltTelephoneNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:AltFaxNumber:Prompt;  SolaceCtrlName = '?sub:AltFaxNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:AltFaxNumber;  SolaceCtrlName = '?sub:AltFaxNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:AltEmailAddress:Prompt;  SolaceCtrlName = '?sub:AltEmailAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:AltEmailAddress;  SolaceCtrlName = '?sub:AltEmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:UseAlternativeAdd;  SolaceCtrlName = '?sub:UseAlternativeAdd';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab7;  SolaceCtrlName = '?Tab7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:AccountNumber:Prompt;  SolaceCtrlName = '?sua:AccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sua:AccountNumber;  SolaceCtrlName = '?sua:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Sub Account'
  OF ChangeRecord
    ActionMessage = 'Changing A Sub Account'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSUBTRACC')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSUBTRACC')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SUB:Main_Account_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sub:Record,History::sub:Record)
  SELF.AddHistoryField(?sub:Main_Account_Number,2)
  SELF.AddHistoryField(?sub:Account_Number,3)
  SELF.AddHistoryField(?sub:Company_Name,5)
  SELF.AddHistoryField(?sub:Branch,12)
  SELF.AddHistoryField(?sub:Postcode,4)
  SELF.AddHistoryField(?sub:Address_Line1,6)
  SELF.AddHistoryField(?sub:Address_Line2,7)
  SELF.AddHistoryField(?sub:Address_Line3,8)
  SELF.AddHistoryField(?sub:Telephone_Number,9)
  SELF.AddHistoryField(?sub:Fax_Number,10)
  SELF.AddHistoryField(?sub:EmailAddress,11)
  SELF.AddHistoryField(?sub:Contact_Name,13)
  SELF.AddHistoryField(?sub:Password,43)
  SELF.AddHistoryField(?sub:Courier_Incoming,29)
  SELF.AddHistoryField(?sub:Courier_Outgoing,30)
  SELF.AddHistoryField(?sub:Use_Delivery_Address,26)
  SELF.AddHistoryField(?sub:Use_Collection_Address,27)
  SELF.AddHistoryField(?sub:Use_Customer_Address,46)
  SELF.AddHistoryField(?sub:Invoice_Customer_Address,47)
  SELF.AddHistoryField(?sub:Despatch_Paid_Jobs,33)
  SELF.AddHistoryField(?sub:Despatch_Invoiced_Jobs,32)
  SELF.AddHistoryField(?sub:MultiInvoice,49)
  SELF.AddHistoryField(?sub:AutoSendStatusEmails,72)
  SELF.AddHistoryField(?sub:VodafoneRetailStore,76)
  SELF.AddHistoryField(?sub:IsContactCentre,75)
  SELF.AddHistoryField(?sub:ContactCentreAccType,77)
  SELF.AddHistoryField(?sub:EmailRecipientList,73)
  SELF.AddHistoryField(?sub:EmailEndUser,74)
  SELF.AddHistoryField(?sub:Labour_VAT_Code,18)
  SELF.AddHistoryField(?sub:Parts_VAT_Code,20)
  SELF.AddHistoryField(?sub:Retail_VAT_Code,19)
  SELF.AddHistoryField(?sub:Courier_Cost,42)
  SELF.AddHistoryField(?sub:EuroApplies,55)
  SELF.AddHistoryField(?sub:Stop_Account,24)
  SELF.AddHistoryField(?sub:ZeroChargeable,48)
  SELF.AddHistoryField(?SUB:MultiInvoice:2,49)
  SELF.AddHistoryField(?sub:PriceDespatchNotes,35)
  SELF.AddHistoryField(?sub:InvoiceAtDespatch,60)
  SELF.AddHistoryField(?sub:InvoiceType,61)
  SELF.AddHistoryField(?sub:Retail_Payment_Type,44)
  SELF.AddHistoryField(?sub:Retail_Price_Structure,45)
  SELF.AddHistoryField(?sub:Print_Retail_Picking_Note,39)
  SELF.AddHistoryField(?sub:Print_Retail_Despatch_Note,38)
  SELF.AddHistoryField(?sub:UseCustDespAdd,28)
  SELF.AddHistoryField(?sub:Print_Despatch_Notes,34)
  SELF.AddHistoryField(?sub:Print_Despatch_Complete,36)
  SELF.AddHistoryField(?sub:Print_Despatch_Despatch,37)
  SELF.AddHistoryField(?sub:HideDespAdd,54)
  SELF.AddHistoryField(?SUB:Despatch_Note_Per_Item:2,40)
  SELF.AddHistoryField(?sub:Summary_Despatch_Notes,41)
  SELF.AddHistoryField(?sub:IndividualSummary,62)
  SELF.AddHistoryField(?sub:UseTradeContactNo,63)
  SELF.AddHistoryField(?sub:UseDespatchDetails,67)
  SELF.AddHistoryField(?sub:AltTelephoneNumber,68)
  SELF.AddHistoryField(?sub:AltFaxNumber,69)
  SELF.AddHistoryField(?sub:AltEmailAddress,70)
  SELF.AddHistoryField(?sub:UseAlternativeAdd,71)
  SELF.AddUpdateFile(Access:SUBTRACC)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:COURIER.Open
  Relate:DEFAULTS.Open
  Relate:USELEVEL.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SUBTRACC
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:SUBACCAD,SELF)
  BRW12.Init(?List:2,Queue:Browse:1.ViewPosition,BRW12::View:Browse,Queue:Browse:1,Relate:SUBEMAIL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List:2{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  IF ?sub:Courier_Incoming{Prop:Tip} AND ~?LookupIncomingCourier{Prop:Tip}
     ?LookupIncomingCourier{Prop:Tip} = 'Select ' & ?sub:Courier_Incoming{Prop:Tip}
  END
  IF ?sub:Courier_Incoming{Prop:Msg} AND ~?LookupIncomingCourier{Prop:Msg}
     ?LookupIncomingCourier{Prop:Msg} = 'Select ' & ?sub:Courier_Incoming{Prop:Msg}
  END
  IF ?sub:Courier_Outgoing{Prop:Tip} AND ~?LookupOutgoingCourier{Prop:Tip}
     ?LookupOutgoingCourier{Prop:Tip} = 'Select ' & ?sub:Courier_Outgoing{Prop:Tip}
  END
  IF ?sub:Courier_Outgoing{Prop:Msg} AND ~?LookupOutgoingCourier{Prop:Msg}
     ?LookupOutgoingCourier{Prop:Msg} = 'Select ' & ?sub:Courier_Outgoing{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,sua:AccountNumberKey)
  BRW8.AddRange(sua:RefNumber,sub:RecordNumber)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?sua:AccountNumber,sua:AccountNumber,1,BRW8)
  BRW8.AddField(sua:AccountNumber,BRW8.Q.sua:AccountNumber)
  BRW8.AddField(sua:CompanyName,BRW8.Q.sua:CompanyName)
  BRW8.AddField(sua:Postcode,BRW8.Q.sua:Postcode)
  BRW8.AddField(sua:RecordNumber,BRW8.Q.sua:RecordNumber)
  BRW8.AddField(sua:RefNumber,BRW8.Q.sua:RefNumber)
  BRW12.Q &= Queue:Browse:1
  BRW12.AddSortOrder(,sue:RecipientTypeKey)
  BRW12.AddRange(sue:RefNumber,sub:RecordNumber)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,sue:RecipientType,1,BRW12)
  BRW12.AddField(sue:RecipientType,BRW12.Q.sue:RecipientType)
  BRW12.AddField(sue:ContactName,BRW12.Q.sue:ContactName)
  BRW12.AddField(sue:RecordNumber,BRW12.Q.sue:RecordNumber)
  BRW12.AddField(sue:RefNumber,BRW12.Q.sue:RefNumber)
  IF ?sub:AutoSendStatusEmails{Prop:Checked} = True
    UNHIDE(?EmailGroup)
  END
  IF ?sub:AutoSendStatusEmails{Prop:Checked} = False
    HIDE(?EmailGroup)
  END
  IF ?sub:IsContactCentre{Prop:Checked} = True
    UNHIDE(?sub:ContactCentreAccType)
  END
  IF ?sub:IsContactCentre{Prop:Checked} = False
    sub:ContactCentreAccType = 0
    HIDE(?sub:ContactCentreAccType)
  END
  IF ?sub:InvoiceAtDespatch{Prop:Checked} = True
    ENABLE(?sub:InvoiceType)
  END
  IF ?sub:InvoiceAtDespatch{Prop:Checked} = False
    DISABLE(?sub:InvoiceType)
  END
  IF ?sub:Print_Despatch_Notes{Prop:Checked} = True
    ENABLE(?sub:Print_Despatch_Complete)
    ENABLE(?sub:Print_Despatch_Despatch)
    ENABLE(?sub:HideDespAdd)
    ENABLE(?sub:PriceDespatchNotes)
  END
  IF ?sub:Print_Despatch_Notes{Prop:Checked} = False
    sub:Summary_Despatch_Notes = 'NO'
    sub:Despatch_Note_Per_Item = 'NO'
    sub:Print_Despatch_Complete = 'NO'
    sub:Print_Despatch_Despatch = 'NO'
    DISABLE(?sub:Print_Despatch_Complete)
    DISABLE(?sub:Print_Despatch_Despatch)
    DISABLE(?sub:HideDespAdd)
    DISABLE(?sub:PriceDespatchNotes)
  END
  IF ?sub:Print_Despatch_Despatch{Prop:Checked} = True
    ENABLE(?despatch_note_type)
  END
  IF ?sub:Print_Despatch_Despatch{Prop:Checked} = False
    DISABLE(?despatch_note_type)
  END
  IF ?sub:Summary_Despatch_Notes{Prop:Checked} = True
    UNHIDE(?sub:IndividualSummary)
  END
  IF ?sub:Summary_Despatch_Notes{Prop:Checked} = False
    DISABLE(?sub:IndividualSummary)
  END
  IF ?sub:UseDespatchDetails{Prop:Checked} = True
    ENABLE(?AlternativeNumbers)
  END
  IF ?sub:UseDespatchDetails{Prop:Checked} = False
    DISABLE(?AlternativeNumbers)
  END
  IF ?sub:UseAlternativeAdd{Prop:Checked} = True
    UNHIDE(?Tab7)
  END
  IF ?sub:UseAlternativeAdd{Prop:Checked} = False
    HIDE(?Tab7)
  END
  FDCB14.Init(sub:Labour_VAT_Code,?sub:Labour_VAT_Code,Queue:FileDropCombo:1.ViewPosition,FDCB14::View:FileDropCombo,Queue:FileDropCombo:1,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB14.Q &= Queue:FileDropCombo:1
  FDCB14.AddSortOrder(vat:Vat_code_Key)
  FDCB14.AddField(vat:VAT_Code,FDCB14.Q.vat:VAT_Code)
  FDCB14.AddField(vat:VAT_Rate,FDCB14.Q.vat:VAT_Rate)
  FDCB14.AddUpdateField(vat:VAT_Code,sub:Labour_VAT_Code)
  ThisWindow.AddItem(FDCB14.WindowComponent)
  FDCB14.DefaultFill = 0
  FDCB15.Init(sub:Parts_VAT_Code,?sub:Parts_VAT_Code,Queue:FileDropCombo:5.ViewPosition,FDCB15::View:FileDropCombo,Queue:FileDropCombo:5,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB15.Q &= Queue:FileDropCombo:5
  FDCB15.AddSortOrder(vat:Vat_code_Key)
  FDCB15.AddField(vat:VAT_Code,FDCB15.Q.vat:VAT_Code)
  FDCB15.AddField(vat:VAT_Rate,FDCB15.Q.vat:VAT_Rate)
  ThisWindow.AddItem(FDCB15.WindowComponent)
  FDCB15.DefaultFill = 0
  FDCB26.Init(sub:Retail_VAT_Code,?sub:Retail_VAT_Code,Queue:FileDropCombo:7.ViewPosition,FDCB26::View:FileDropCombo,Queue:FileDropCombo:7,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB26.Q &= Queue:FileDropCombo:7
  FDCB26.AddSortOrder(vat:Vat_code_Key)
  FDCB26.AddField(vat:VAT_Code,FDCB26.Q.vat:VAT_Code)
  FDCB26.AddField(vat:VAT_Rate,FDCB26.Q.vat:VAT_Rate)
  ThisWindow.AddItem(FDCB26.WindowComponent)
  FDCB26.DefaultFill = 0
  BRW8.AskProcedure = 3
  BRW12.AskProcedure = 4
  BRW8.AddToolbarTarget(Toolbar)
  BRW12.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
    Relate:USELEVEL.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSUBTRACC',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    sub:Stop_Account = 'NO'
    sub:Allow_Cash_Sales = 'NO'
    sub:Main_Account_Number = glo:select12
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickCouriers
      PickCouriers
      UpdateSubAddresses
      UpdateSubRecipients
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sub:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Postcode, Accepted)
      If ~quickwindow{prop:acceptall}
          postcode_routine(sub:postcode,sub:address_line1,sub:address_line2,sub:address_line3)
          Select(?sub:address_line1,1)
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Postcode, Accepted)
    OF ?Clear_Address
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
      Case MessageEx('Are you sure you want to clear the Address?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
      
              sub:postcode      = ''
              sub:company_name  = ''
              sub:address_line1 = ''
              sub:address_line2 = ''
              sub:address_line3 = ''
              Display()
              Select(?sub:postcode)
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
    OF ?sub:Telephone_Number
      ! Before Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?sub:Telephone_Number, Accepted)
      
          temp_string = Clip(left(sub:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          sub:Telephone_Number    = temp_string
          Display(?sub:Telephone_Number)
      ! After Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?sub:Telephone_Number, Accepted)
    OF ?sub:Fax_Number
      ! Before Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?sub:Fax_Number, Accepted)
      
          temp_string = Clip(left(sub:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          sub:Fax_Number    = temp_string
          Display(?sub:Fax_Number)
      ! After Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?sub:Fax_Number, Accepted)
    OF ?sub:Courier_Incoming
      IF sub:Courier_Incoming OR ?sub:Courier_Incoming{Prop:Req}
        cou:Courier = sub:Courier_Incoming
        !Save Lookup Field Incase Of error
        look:sub:Courier_Incoming        = sub:Courier_Incoming
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            sub:Courier_Incoming = cou:Courier
          ELSE
            !Restore Lookup On Error
            sub:Courier_Incoming = look:sub:Courier_Incoming
            SELECT(?sub:Courier_Incoming)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupIncomingCourier
      ThisWindow.Update
      cou:Courier = sub:Courier_Incoming
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          sub:Courier_Incoming = cou:Courier
          Select(?+1)
      ELSE
          Select(?sub:Courier_Incoming)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sub:Courier_Incoming)
    OF ?sub:Courier_Outgoing
      IF sub:Courier_Outgoing OR ?sub:Courier_Outgoing{Prop:Req}
        cou:Courier = sub:Courier_Outgoing
        !Save Lookup Field Incase Of error
        look:sub:Courier_Outgoing        = sub:Courier_Outgoing
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            sub:Courier_Outgoing = cou:Courier
          ELSE
            !Restore Lookup On Error
            sub:Courier_Outgoing = look:sub:Courier_Outgoing
            SELECT(?sub:Courier_Outgoing)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupOutgoingCourier
      ThisWindow.Update
      cou:Courier = sub:Courier_Outgoing
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          sub:Courier_Outgoing = cou:Courier
          Select(?+1)
      ELSE
          Select(?sub:Courier_Outgoing)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sub:Courier_Outgoing)
    OF ?sub:AutoSendStatusEmails
      IF ?sub:AutoSendStatusEmails{Prop:Checked} = True
        UNHIDE(?EmailGroup)
      END
      IF ?sub:AutoSendStatusEmails{Prop:Checked} = False
        HIDE(?EmailGroup)
      END
      ThisWindow.Reset
    OF ?sub:IsContactCentre
      IF ?sub:IsContactCentre{Prop:Checked} = True
        UNHIDE(?sub:ContactCentreAccType)
      END
      IF ?sub:IsContactCentre{Prop:Checked} = False
        sub:ContactCentreAccType = 0
        HIDE(?sub:ContactCentreAccType)
      END
      ThisWindow.Reset
    OF ?sub:Labour_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Labour_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Labour_VAT_Code, Accepted)
    OF ?sub:Parts_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Parts_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Parts_VAT_Code, Accepted)
    OF ?sub:Retail_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Retail_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Retail_VAT_Code, Accepted)
    OF ?sub:InvoiceAtDespatch
      IF ?sub:InvoiceAtDespatch{Prop:Checked} = True
        ENABLE(?sub:InvoiceType)
      END
      IF ?sub:InvoiceAtDespatch{Prop:Checked} = False
        DISABLE(?sub:InvoiceType)
      END
      ThisWindow.Reset
    OF ?sub:Print_Despatch_Notes
      IF ?sub:Print_Despatch_Notes{Prop:Checked} = True
        ENABLE(?sub:Print_Despatch_Complete)
        ENABLE(?sub:Print_Despatch_Despatch)
        ENABLE(?sub:HideDespAdd)
        ENABLE(?sub:PriceDespatchNotes)
      END
      IF ?sub:Print_Despatch_Notes{Prop:Checked} = False
        sub:Summary_Despatch_Notes = 'NO'
        sub:Despatch_Note_Per_Item = 'NO'
        sub:Print_Despatch_Complete = 'NO'
        sub:Print_Despatch_Despatch = 'NO'
        DISABLE(?sub:Print_Despatch_Complete)
        DISABLE(?sub:Print_Despatch_Despatch)
        DISABLE(?sub:HideDespAdd)
        DISABLE(?sub:PriceDespatchNotes)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Print_Despatch_Notes, Accepted)
      Post(Event:Accepted,?sub:Print_Despatch_Despatch)
      !thismakeover.setwindow(win:form)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Print_Despatch_Notes, Accepted)
    OF ?sub:Print_Despatch_Despatch
      IF ?sub:Print_Despatch_Despatch{Prop:Checked} = True
        ENABLE(?despatch_note_type)
      END
      IF ?sub:Print_Despatch_Despatch{Prop:Checked} = False
        DISABLE(?despatch_note_type)
      END
      ThisWindow.Reset
    OF ?sub:Summary_Despatch_Notes
      IF ?sub:Summary_Despatch_Notes{Prop:Checked} = True
        UNHIDE(?sub:IndividualSummary)
      END
      IF ?sub:Summary_Despatch_Notes{Prop:Checked} = False
        DISABLE(?sub:IndividualSummary)
      END
      ThisWindow.Reset
    OF ?sub:UseDespatchDetails
      IF ?sub:UseDespatchDetails{Prop:Checked} = True
        ENABLE(?AlternativeNumbers)
      END
      IF ?sub:UseDespatchDetails{Prop:Checked} = False
        DISABLE(?AlternativeNumbers)
      END
      ThisWindow.Reset
    OF ?sub:UseAlternativeAdd
      IF ?sub:UseAlternativeAdd{Prop:Checked} = True
        UNHIDE(?Tab7)
      END
      IF ?sub:UseAlternativeAdd{Prop:Checked} = False
        HIDE(?Tab7)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If sub:Print_Despatch_Notes = 'YES'
      If sub:Print_Despatch_Despatch = 'YES'
          If sub:Despatch_Note_Per_Item <> 'YES' And sub:Summary_Despatch_Notes <> 'YES'
              Case MessageEx('You have selected to print a Despatch Note at Despatch but have not selected a Despatch Note Type!','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Select(?sub:Print_Despatch_Notes)
              Cycle
          End !If sub:Despatch_Note_Per_Item <> 'YES' And sub:Summary_Despatch_Notes <> 'YES'
      End !If sub:Print_Despatch_Despatch = 'YES'
  End !sub:Print_Despatch_Notes = 'YES'
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSUBTRACC')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?sub:Courier_Incoming
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Courier_Incoming, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Incoming Courier')
             Post(Event:Accepted,?LookupIncomingCourier)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupIncomingCourier)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Courier_Incoming, AlertKey)
    END
  OF ?sub:Courier_Outgoing
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Courier_Outgoing, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Outgoing Courier')
             Post(Event:Accepted,?LookupOutgoingCourier)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupOutgoingCourier)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Courier_Outgoing, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do Fill_Rates
      If glo:Select2 = 'FINANCIAL'
          ?ServiceDefaults{prop:Hide} = 0
          ?RetailDefaults{prop:Hide} = 0
      End !glo:Select2 = 'FINANCIAL'
      Post(Event:accepted,?sub:print_despatch_notes)
      
      If SecurityCheck('AMEND MAIN ACCOUNT NUMBER')
          ?sub:main_account_number{prop:readonly} = 0
      end
      
      !thismakeover.setwindow(win:form)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB14.ResetQueue(1)
      FDCB15.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

