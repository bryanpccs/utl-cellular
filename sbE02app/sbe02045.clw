

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02045.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSIDServiceCentre PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:TurnaroundDaysRetail LONG
tmp:TurnaroundDaysExch LONG
tmp:MonRepairCapacity LONG
tmp:TueRepairCapacity LONG
tmp:WedRepairCapacity LONG
tmp:ThuRepairCapacity LONG
tmp:FriRepairCapacity LONG
History::srv:Record  LIKE(srv:RECORD),STATIC
QuickWindow          WINDOW('Update the SIDSRVCN File'),AT(,,498,280),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('UpdateRegion'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,490,246),USE(?CurrentTab),SPREAD
                         TAB('Service Centre Details'),USE(?Tab:1)
                           PROMPT('Account ID'),AT(8,20),USE(?srv:SCAccountID:Prompt),TRN
                           ENTRY(@n-14),AT(96,20,60,10),USE(srv:SCAccountID),SKIP,RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                           PROMPT('Service Centre Name'),AT(8,36),USE(?srv:ServiceCentreName:Prompt),TRN
                           ENTRY(@s30),AT(96,36,124,10),USE(srv:ServiceCentreName),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Service Centre Name'),TIP('Service Centre Name'),REQ,UPR
                           PROMPT('Address'),AT(8,52),USE(?srv:AddressLine1:Prompt),TRN
                           ENTRY(@s30),AT(96,52,124,10),USE(srv:AddressLine1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(96,65,124,10),USE(srv:AddressLine2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(96,76,124,10),USE(srv:AddressLine3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Postcode'),AT(8,92),USE(?srv:Postcode:Prompt),TRN
                           ENTRY(@s15),AT(96,92,60,10),USE(srv:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           CHECK('Account Active'),AT(96,105),USE(srv:AccountActive),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),VALUE('1','0')
                           PROMPT('Turnaround Days'),AT(8,129),USE(?TurnaroundDays:Prompt),TRN
                           PROMPT('Retail'),AT(96,121),USE(?srv:TurnaroundDaysRetail:Prompt),TRN
                           ENTRY(@n-14),AT(96,129,60,10),USE(srv:TurnaroundDaysRetail),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Turnaround Days (Retail)'),TIP('Turnaround Days (Retail)')
                           PROMPT('Exchange'),AT(160,121),USE(?srv:TurnaroundDaysExch:Prompt),TRN
                           ENTRY(@n-14),AT(160,129,60,10),USE(srv:TurnaroundDaysExch),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Turnaround Days (Exchange)'),TIP('Turnaround Days (Exchange)')
                           PROMPT('Bank Holidays File Name'),AT(8,145),USE(?srv:BankHolidaysFileName:Prompt),TRN
                           ENTRY(@s100),AT(96,145,124,10),USE(srv:BankHolidaysFileName),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Bank Holidays File Name'),TIP('Bank Holidays File Name')
                           PROMPT('Contact Name'),AT(8,161),USE(?srv:ContactName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(96,161,124,10),USE(srv:ContactName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Contact Name'),TIP('Contact Name'),UPR
                           PROMPT('Contact Number'),AT(8,177),USE(?srv:ContactNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(96,177,124,10),USE(srv:ContactNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Contact Number'),TIP('Contact Number'),UPR
                           PROMPT('Contact Email'),AT(8,193),USE(?srv:ContactEmail:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(96,193,124,10),USE(srv:ContactEmail),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Contact Email'),TIP('Contact Email')
                           PROMPT('Exchange Import'),AT(8,209),USE(?srv:ExchangeImportType:Prompt),TRN
                           OPTION,AT(96,209,124,24),USE(srv:ExchangeImportType),TRN,MSG('0 - Not Set, 1 - Model Code, 2 - Oracle Code')
                             RADIO('Use ServiceBase Model Codes'),AT(96,209),USE(?Option1:Radio1),TRN,VALUE('1')
                             RADIO('Use Oracle Codes'),AT(96,223),USE(?Option1:Radio2),TRN,VALUE('2')
                           END
                           CHECK('Use Service Centre Specific FTP'),AT(244,20),USE(srv:UseServiceCentreFTP),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),VALUE('1','0')
                           PROMPT('FTP Server'),AT(244,36),USE(?srv:FTPServer:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(320,36,160,10),USE(srv:FTPServer),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Server'),TIP('FTP Server')
                           PROMPT('FTP Username'),AT(244,52),USE(?srv:FTPUsername:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(320,52,160,10),USE(srv:FTPUsername),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Username'),TIP('FTP Username')
                           PROMPT('FTP Password'),AT(244,68),USE(?srv:FTPPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(320,68,160,10),USE(srv:FTPPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Password'),TIP('FTP Password'),PASSWORD
                           PROMPT('FTP Path'),AT(244,84),USE(?srv:FTPPath:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(320,84,160,10),USE(srv:FTPPath),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Path'),TIP('FTP Path')
                           PROMPT('Email Alert Recipients'),AT(244,100),USE(?srv:EmailToAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(320,100,160,10),USE(srv:EmailToAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Alert Recipients'),TIP('Email Alert Recipients')
                           PROMPT('Send Alert Email After'),AT(244,116),USE(?srv:ProcessFailureCount:Prompt),TRN,FONT('Tahoma',,,,CHARSET:ANSI)
                           ENTRY(@s8),AT(320,116,60,10),USE(srv:ProcessFailureCount),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Send Alert Email After (X) Successive Process Failures'),TIP('Send Alert Email After (X) Successive Process Failures'),UPR
                           STRING('Successive Process Failures'),AT(392,116),USE(?StrProcessFailures),TRN
                         END
                       END
                       PANEL,AT(4,254,490,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Non Working Days'),AT(8,258,66,16),USE(?BtnNonWorking),LEFT,ICON('Calenda2.ico')
                       BUTTON('Attached Models'),AT(77,258,66,16),USE(?BtnModelsSC),LEFT,ICON('moving.ico')
                       BUTTON('Transit Days'),AT(145,258,56,16),USE(?BtnTransitDays),LEFT,ICON('Calenda2.ico')
                       BUTTON('Repair Capacity Limit'),AT(204,258,76,16),USE(?Button:RepairCapacityLimit),LEFT,ICON('label.ico')
                       BUTTON('OK'),AT(376,258,56,16),USE(?OK),LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('Cancel'),AT(432,258,56,16),USE(?Cancel),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?srv:SCAccountID:Prompt{prop:FontColor} = -1
    ?srv:SCAccountID:Prompt{prop:Color} = 15066597
    If ?srv:SCAccountID{prop:ReadOnly} = True
        ?srv:SCAccountID{prop:FontColor} = 65793
        ?srv:SCAccountID{prop:Color} = 15066597
    Elsif ?srv:SCAccountID{prop:Req} = True
        ?srv:SCAccountID{prop:FontColor} = 65793
        ?srv:SCAccountID{prop:Color} = 8454143
    Else ! If ?srv:SCAccountID{prop:Req} = True
        ?srv:SCAccountID{prop:FontColor} = 65793
        ?srv:SCAccountID{prop:Color} = 16777215
    End ! If ?srv:SCAccountID{prop:Req} = True
    ?srv:SCAccountID{prop:Trn} = 0
    ?srv:SCAccountID{prop:FontStyle} = font:Bold
    ?srv:ServiceCentreName:Prompt{prop:FontColor} = -1
    ?srv:ServiceCentreName:Prompt{prop:Color} = 15066597
    If ?srv:ServiceCentreName{prop:ReadOnly} = True
        ?srv:ServiceCentreName{prop:FontColor} = 65793
        ?srv:ServiceCentreName{prop:Color} = 15066597
    Elsif ?srv:ServiceCentreName{prop:Req} = True
        ?srv:ServiceCentreName{prop:FontColor} = 65793
        ?srv:ServiceCentreName{prop:Color} = 8454143
    Else ! If ?srv:ServiceCentreName{prop:Req} = True
        ?srv:ServiceCentreName{prop:FontColor} = 65793
        ?srv:ServiceCentreName{prop:Color} = 16777215
    End ! If ?srv:ServiceCentreName{prop:Req} = True
    ?srv:ServiceCentreName{prop:Trn} = 0
    ?srv:ServiceCentreName{prop:FontStyle} = font:Bold
    ?srv:AddressLine1:Prompt{prop:FontColor} = -1
    ?srv:AddressLine1:Prompt{prop:Color} = 15066597
    If ?srv:AddressLine1{prop:ReadOnly} = True
        ?srv:AddressLine1{prop:FontColor} = 65793
        ?srv:AddressLine1{prop:Color} = 15066597
    Elsif ?srv:AddressLine1{prop:Req} = True
        ?srv:AddressLine1{prop:FontColor} = 65793
        ?srv:AddressLine1{prop:Color} = 8454143
    Else ! If ?srv:AddressLine1{prop:Req} = True
        ?srv:AddressLine1{prop:FontColor} = 65793
        ?srv:AddressLine1{prop:Color} = 16777215
    End ! If ?srv:AddressLine1{prop:Req} = True
    ?srv:AddressLine1{prop:Trn} = 0
    ?srv:AddressLine1{prop:FontStyle} = font:Bold
    If ?srv:AddressLine2{prop:ReadOnly} = True
        ?srv:AddressLine2{prop:FontColor} = 65793
        ?srv:AddressLine2{prop:Color} = 15066597
    Elsif ?srv:AddressLine2{prop:Req} = True
        ?srv:AddressLine2{prop:FontColor} = 65793
        ?srv:AddressLine2{prop:Color} = 8454143
    Else ! If ?srv:AddressLine2{prop:Req} = True
        ?srv:AddressLine2{prop:FontColor} = 65793
        ?srv:AddressLine2{prop:Color} = 16777215
    End ! If ?srv:AddressLine2{prop:Req} = True
    ?srv:AddressLine2{prop:Trn} = 0
    ?srv:AddressLine2{prop:FontStyle} = font:Bold
    If ?srv:AddressLine3{prop:ReadOnly} = True
        ?srv:AddressLine3{prop:FontColor} = 65793
        ?srv:AddressLine3{prop:Color} = 15066597
    Elsif ?srv:AddressLine3{prop:Req} = True
        ?srv:AddressLine3{prop:FontColor} = 65793
        ?srv:AddressLine3{prop:Color} = 8454143
    Else ! If ?srv:AddressLine3{prop:Req} = True
        ?srv:AddressLine3{prop:FontColor} = 65793
        ?srv:AddressLine3{prop:Color} = 16777215
    End ! If ?srv:AddressLine3{prop:Req} = True
    ?srv:AddressLine3{prop:Trn} = 0
    ?srv:AddressLine3{prop:FontStyle} = font:Bold
    ?srv:Postcode:Prompt{prop:FontColor} = -1
    ?srv:Postcode:Prompt{prop:Color} = 15066597
    If ?srv:Postcode{prop:ReadOnly} = True
        ?srv:Postcode{prop:FontColor} = 65793
        ?srv:Postcode{prop:Color} = 15066597
    Elsif ?srv:Postcode{prop:Req} = True
        ?srv:Postcode{prop:FontColor} = 65793
        ?srv:Postcode{prop:Color} = 8454143
    Else ! If ?srv:Postcode{prop:Req} = True
        ?srv:Postcode{prop:FontColor} = 65793
        ?srv:Postcode{prop:Color} = 16777215
    End ! If ?srv:Postcode{prop:Req} = True
    ?srv:Postcode{prop:Trn} = 0
    ?srv:Postcode{prop:FontStyle} = font:Bold
    ?srv:AccountActive{prop:Font,3} = -1
    ?srv:AccountActive{prop:Color} = 15066597
    ?srv:AccountActive{prop:Trn} = 0
    ?TurnaroundDays:Prompt{prop:FontColor} = -1
    ?TurnaroundDays:Prompt{prop:Color} = 15066597
    ?srv:TurnaroundDaysRetail:Prompt{prop:FontColor} = -1
    ?srv:TurnaroundDaysRetail:Prompt{prop:Color} = 15066597
    If ?srv:TurnaroundDaysRetail{prop:ReadOnly} = True
        ?srv:TurnaroundDaysRetail{prop:FontColor} = 65793
        ?srv:TurnaroundDaysRetail{prop:Color} = 15066597
    Elsif ?srv:TurnaroundDaysRetail{prop:Req} = True
        ?srv:TurnaroundDaysRetail{prop:FontColor} = 65793
        ?srv:TurnaroundDaysRetail{prop:Color} = 8454143
    Else ! If ?srv:TurnaroundDaysRetail{prop:Req} = True
        ?srv:TurnaroundDaysRetail{prop:FontColor} = 65793
        ?srv:TurnaroundDaysRetail{prop:Color} = 16777215
    End ! If ?srv:TurnaroundDaysRetail{prop:Req} = True
    ?srv:TurnaroundDaysRetail{prop:Trn} = 0
    ?srv:TurnaroundDaysRetail{prop:FontStyle} = font:Bold
    ?srv:TurnaroundDaysExch:Prompt{prop:FontColor} = -1
    ?srv:TurnaroundDaysExch:Prompt{prop:Color} = 15066597
    If ?srv:TurnaroundDaysExch{prop:ReadOnly} = True
        ?srv:TurnaroundDaysExch{prop:FontColor} = 65793
        ?srv:TurnaroundDaysExch{prop:Color} = 15066597
    Elsif ?srv:TurnaroundDaysExch{prop:Req} = True
        ?srv:TurnaroundDaysExch{prop:FontColor} = 65793
        ?srv:TurnaroundDaysExch{prop:Color} = 8454143
    Else ! If ?srv:TurnaroundDaysExch{prop:Req} = True
        ?srv:TurnaroundDaysExch{prop:FontColor} = 65793
        ?srv:TurnaroundDaysExch{prop:Color} = 16777215
    End ! If ?srv:TurnaroundDaysExch{prop:Req} = True
    ?srv:TurnaroundDaysExch{prop:Trn} = 0
    ?srv:TurnaroundDaysExch{prop:FontStyle} = font:Bold
    ?srv:BankHolidaysFileName:Prompt{prop:FontColor} = -1
    ?srv:BankHolidaysFileName:Prompt{prop:Color} = 15066597
    If ?srv:BankHolidaysFileName{prop:ReadOnly} = True
        ?srv:BankHolidaysFileName{prop:FontColor} = 65793
        ?srv:BankHolidaysFileName{prop:Color} = 15066597
    Elsif ?srv:BankHolidaysFileName{prop:Req} = True
        ?srv:BankHolidaysFileName{prop:FontColor} = 65793
        ?srv:BankHolidaysFileName{prop:Color} = 8454143
    Else ! If ?srv:BankHolidaysFileName{prop:Req} = True
        ?srv:BankHolidaysFileName{prop:FontColor} = 65793
        ?srv:BankHolidaysFileName{prop:Color} = 16777215
    End ! If ?srv:BankHolidaysFileName{prop:Req} = True
    ?srv:BankHolidaysFileName{prop:Trn} = 0
    ?srv:BankHolidaysFileName{prop:FontStyle} = font:Bold
    ?srv:ContactName:Prompt{prop:FontColor} = -1
    ?srv:ContactName:Prompt{prop:Color} = 15066597
    If ?srv:ContactName{prop:ReadOnly} = True
        ?srv:ContactName{prop:FontColor} = 65793
        ?srv:ContactName{prop:Color} = 15066597
    Elsif ?srv:ContactName{prop:Req} = True
        ?srv:ContactName{prop:FontColor} = 65793
        ?srv:ContactName{prop:Color} = 8454143
    Else ! If ?srv:ContactName{prop:Req} = True
        ?srv:ContactName{prop:FontColor} = 65793
        ?srv:ContactName{prop:Color} = 16777215
    End ! If ?srv:ContactName{prop:Req} = True
    ?srv:ContactName{prop:Trn} = 0
    ?srv:ContactName{prop:FontStyle} = font:Bold
    ?srv:ContactNumber:Prompt{prop:FontColor} = -1
    ?srv:ContactNumber:Prompt{prop:Color} = 15066597
    If ?srv:ContactNumber{prop:ReadOnly} = True
        ?srv:ContactNumber{prop:FontColor} = 65793
        ?srv:ContactNumber{prop:Color} = 15066597
    Elsif ?srv:ContactNumber{prop:Req} = True
        ?srv:ContactNumber{prop:FontColor} = 65793
        ?srv:ContactNumber{prop:Color} = 8454143
    Else ! If ?srv:ContactNumber{prop:Req} = True
        ?srv:ContactNumber{prop:FontColor} = 65793
        ?srv:ContactNumber{prop:Color} = 16777215
    End ! If ?srv:ContactNumber{prop:Req} = True
    ?srv:ContactNumber{prop:Trn} = 0
    ?srv:ContactNumber{prop:FontStyle} = font:Bold
    ?srv:ContactEmail:Prompt{prop:FontColor} = -1
    ?srv:ContactEmail:Prompt{prop:Color} = 15066597
    If ?srv:ContactEmail{prop:ReadOnly} = True
        ?srv:ContactEmail{prop:FontColor} = 65793
        ?srv:ContactEmail{prop:Color} = 15066597
    Elsif ?srv:ContactEmail{prop:Req} = True
        ?srv:ContactEmail{prop:FontColor} = 65793
        ?srv:ContactEmail{prop:Color} = 8454143
    Else ! If ?srv:ContactEmail{prop:Req} = True
        ?srv:ContactEmail{prop:FontColor} = 65793
        ?srv:ContactEmail{prop:Color} = 16777215
    End ! If ?srv:ContactEmail{prop:Req} = True
    ?srv:ContactEmail{prop:Trn} = 0
    ?srv:ContactEmail{prop:FontStyle} = font:Bold
    ?srv:ExchangeImportType:Prompt{prop:FontColor} = -1
    ?srv:ExchangeImportType:Prompt{prop:Color} = 15066597
    ?srv:ExchangeImportType{prop:Font,3} = -1
    ?srv:ExchangeImportType{prop:Color} = 15066597
    ?srv:ExchangeImportType{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    ?srv:UseServiceCentreFTP{prop:Font,3} = -1
    ?srv:UseServiceCentreFTP{prop:Color} = 15066597
    ?srv:UseServiceCentreFTP{prop:Trn} = 0
    ?srv:FTPServer:Prompt{prop:FontColor} = -1
    ?srv:FTPServer:Prompt{prop:Color} = 15066597
    If ?srv:FTPServer{prop:ReadOnly} = True
        ?srv:FTPServer{prop:FontColor} = 65793
        ?srv:FTPServer{prop:Color} = 15066597
    Elsif ?srv:FTPServer{prop:Req} = True
        ?srv:FTPServer{prop:FontColor} = 65793
        ?srv:FTPServer{prop:Color} = 8454143
    Else ! If ?srv:FTPServer{prop:Req} = True
        ?srv:FTPServer{prop:FontColor} = 65793
        ?srv:FTPServer{prop:Color} = 16777215
    End ! If ?srv:FTPServer{prop:Req} = True
    ?srv:FTPServer{prop:Trn} = 0
    ?srv:FTPServer{prop:FontStyle} = font:Bold
    ?srv:FTPUsername:Prompt{prop:FontColor} = -1
    ?srv:FTPUsername:Prompt{prop:Color} = 15066597
    If ?srv:FTPUsername{prop:ReadOnly} = True
        ?srv:FTPUsername{prop:FontColor} = 65793
        ?srv:FTPUsername{prop:Color} = 15066597
    Elsif ?srv:FTPUsername{prop:Req} = True
        ?srv:FTPUsername{prop:FontColor} = 65793
        ?srv:FTPUsername{prop:Color} = 8454143
    Else ! If ?srv:FTPUsername{prop:Req} = True
        ?srv:FTPUsername{prop:FontColor} = 65793
        ?srv:FTPUsername{prop:Color} = 16777215
    End ! If ?srv:FTPUsername{prop:Req} = True
    ?srv:FTPUsername{prop:Trn} = 0
    ?srv:FTPUsername{prop:FontStyle} = font:Bold
    ?srv:FTPPassword:Prompt{prop:FontColor} = -1
    ?srv:FTPPassword:Prompt{prop:Color} = 15066597
    If ?srv:FTPPassword{prop:ReadOnly} = True
        ?srv:FTPPassword{prop:FontColor} = 65793
        ?srv:FTPPassword{prop:Color} = 15066597
    Elsif ?srv:FTPPassword{prop:Req} = True
        ?srv:FTPPassword{prop:FontColor} = 65793
        ?srv:FTPPassword{prop:Color} = 8454143
    Else ! If ?srv:FTPPassword{prop:Req} = True
        ?srv:FTPPassword{prop:FontColor} = 65793
        ?srv:FTPPassword{prop:Color} = 16777215
    End ! If ?srv:FTPPassword{prop:Req} = True
    ?srv:FTPPassword{prop:Trn} = 0
    ?srv:FTPPassword{prop:FontStyle} = font:Bold
    ?srv:FTPPath:Prompt{prop:FontColor} = -1
    ?srv:FTPPath:Prompt{prop:Color} = 15066597
    If ?srv:FTPPath{prop:ReadOnly} = True
        ?srv:FTPPath{prop:FontColor} = 65793
        ?srv:FTPPath{prop:Color} = 15066597
    Elsif ?srv:FTPPath{prop:Req} = True
        ?srv:FTPPath{prop:FontColor} = 65793
        ?srv:FTPPath{prop:Color} = 8454143
    Else ! If ?srv:FTPPath{prop:Req} = True
        ?srv:FTPPath{prop:FontColor} = 65793
        ?srv:FTPPath{prop:Color} = 16777215
    End ! If ?srv:FTPPath{prop:Req} = True
    ?srv:FTPPath{prop:Trn} = 0
    ?srv:FTPPath{prop:FontStyle} = font:Bold
    ?srv:EmailToAddress:Prompt{prop:FontColor} = -1
    ?srv:EmailToAddress:Prompt{prop:Color} = 15066597
    If ?srv:EmailToAddress{prop:ReadOnly} = True
        ?srv:EmailToAddress{prop:FontColor} = 65793
        ?srv:EmailToAddress{prop:Color} = 15066597
    Elsif ?srv:EmailToAddress{prop:Req} = True
        ?srv:EmailToAddress{prop:FontColor} = 65793
        ?srv:EmailToAddress{prop:Color} = 8454143
    Else ! If ?srv:EmailToAddress{prop:Req} = True
        ?srv:EmailToAddress{prop:FontColor} = 65793
        ?srv:EmailToAddress{prop:Color} = 16777215
    End ! If ?srv:EmailToAddress{prop:Req} = True
    ?srv:EmailToAddress{prop:Trn} = 0
    ?srv:EmailToAddress{prop:FontStyle} = font:Bold
    ?srv:ProcessFailureCount:Prompt{prop:FontColor} = -1
    ?srv:ProcessFailureCount:Prompt{prop:Color} = 15066597
    If ?srv:ProcessFailureCount{prop:ReadOnly} = True
        ?srv:ProcessFailureCount{prop:FontColor} = 65793
        ?srv:ProcessFailureCount{prop:Color} = 15066597
    Elsif ?srv:ProcessFailureCount{prop:Req} = True
        ?srv:ProcessFailureCount{prop:FontColor} = 65793
        ?srv:ProcessFailureCount{prop:Color} = 8454143
    Else ! If ?srv:ProcessFailureCount{prop:Req} = True
        ?srv:ProcessFailureCount{prop:FontColor} = 65793
        ?srv:ProcessFailureCount{prop:Color} = 16777215
    End ! If ?srv:ProcessFailureCount{prop:Req} = True
    ?srv:ProcessFailureCount{prop:Trn} = 0
    ?srv:ProcessFailureCount{prop:FontStyle} = font:Bold
    ?StrProcessFailures{prop:FontColor} = -1
    ?StrProcessFailures{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSIDServiceCentre',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateSIDServiceCentre',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateSIDServiceCentre',1)
    SolaceViewVars('tmp:TurnaroundDaysRetail',tmp:TurnaroundDaysRetail,'UpdateSIDServiceCentre',1)
    SolaceViewVars('tmp:TurnaroundDaysExch',tmp:TurnaroundDaysExch,'UpdateSIDServiceCentre',1)
    SolaceViewVars('tmp:MonRepairCapacity',tmp:MonRepairCapacity,'UpdateSIDServiceCentre',1)
    SolaceViewVars('tmp:TueRepairCapacity',tmp:TueRepairCapacity,'UpdateSIDServiceCentre',1)
    SolaceViewVars('tmp:WedRepairCapacity',tmp:WedRepairCapacity,'UpdateSIDServiceCentre',1)
    SolaceViewVars('tmp:ThuRepairCapacity',tmp:ThuRepairCapacity,'UpdateSIDServiceCentre',1)
    SolaceViewVars('tmp:FriRepairCapacity',tmp:FriRepairCapacity,'UpdateSIDServiceCentre',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:SCAccountID:Prompt;  SolaceCtrlName = '?srv:SCAccountID:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:SCAccountID;  SolaceCtrlName = '?srv:SCAccountID';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ServiceCentreName:Prompt;  SolaceCtrlName = '?srv:ServiceCentreName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ServiceCentreName;  SolaceCtrlName = '?srv:ServiceCentreName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:AddressLine1:Prompt;  SolaceCtrlName = '?srv:AddressLine1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:AddressLine1;  SolaceCtrlName = '?srv:AddressLine1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:AddressLine2;  SolaceCtrlName = '?srv:AddressLine2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:AddressLine3;  SolaceCtrlName = '?srv:AddressLine3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:Postcode:Prompt;  SolaceCtrlName = '?srv:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:Postcode;  SolaceCtrlName = '?srv:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:AccountActive;  SolaceCtrlName = '?srv:AccountActive';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TurnaroundDays:Prompt;  SolaceCtrlName = '?TurnaroundDays:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:TurnaroundDaysRetail:Prompt;  SolaceCtrlName = '?srv:TurnaroundDaysRetail:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:TurnaroundDaysRetail;  SolaceCtrlName = '?srv:TurnaroundDaysRetail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:TurnaroundDaysExch:Prompt;  SolaceCtrlName = '?srv:TurnaroundDaysExch:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:TurnaroundDaysExch;  SolaceCtrlName = '?srv:TurnaroundDaysExch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:BankHolidaysFileName:Prompt;  SolaceCtrlName = '?srv:BankHolidaysFileName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:BankHolidaysFileName;  SolaceCtrlName = '?srv:BankHolidaysFileName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ContactName:Prompt;  SolaceCtrlName = '?srv:ContactName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ContactName;  SolaceCtrlName = '?srv:ContactName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ContactNumber:Prompt;  SolaceCtrlName = '?srv:ContactNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ContactNumber;  SolaceCtrlName = '?srv:ContactNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ContactEmail:Prompt;  SolaceCtrlName = '?srv:ContactEmail:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ContactEmail;  SolaceCtrlName = '?srv:ContactEmail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ExchangeImportType:Prompt;  SolaceCtrlName = '?srv:ExchangeImportType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ExchangeImportType;  SolaceCtrlName = '?srv:ExchangeImportType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio1;  SolaceCtrlName = '?Option1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio2;  SolaceCtrlName = '?Option1:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:UseServiceCentreFTP;  SolaceCtrlName = '?srv:UseServiceCentreFTP';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:FTPServer:Prompt;  SolaceCtrlName = '?srv:FTPServer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:FTPServer;  SolaceCtrlName = '?srv:FTPServer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:FTPUsername:Prompt;  SolaceCtrlName = '?srv:FTPUsername:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:FTPUsername;  SolaceCtrlName = '?srv:FTPUsername';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:FTPPassword:Prompt;  SolaceCtrlName = '?srv:FTPPassword:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:FTPPassword;  SolaceCtrlName = '?srv:FTPPassword';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:FTPPath:Prompt;  SolaceCtrlName = '?srv:FTPPath:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:FTPPath;  SolaceCtrlName = '?srv:FTPPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:EmailToAddress:Prompt;  SolaceCtrlName = '?srv:EmailToAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:EmailToAddress;  SolaceCtrlName = '?srv:EmailToAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ProcessFailureCount:Prompt;  SolaceCtrlName = '?srv:ProcessFailureCount:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?srv:ProcessFailureCount;  SolaceCtrlName = '?srv:ProcessFailureCount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StrProcessFailures;  SolaceCtrlName = '?StrProcessFailures';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BtnNonWorking;  SolaceCtrlName = '?BtnNonWorking';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BtnModelsSC;  SolaceCtrlName = '?BtnModelsSC';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BtnTransitDays;  SolaceCtrlName = '?BtnTransitDays';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button:RepairCapacityLimit;  SolaceCtrlName = '?Button:RepairCapacityLimit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A SID Service Centre'
  OF ChangeRecord
    ActionMessage = 'Changing A SID Service Centre'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSIDServiceCentre')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSIDServiceCentre')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?srv:SCAccountID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(srv:Record,History::srv:Record)
  SELF.AddHistoryField(?srv:SCAccountID,1)
  SELF.AddHistoryField(?srv:ServiceCentreName,2)
  SELF.AddHistoryField(?srv:AddressLine1,3)
  SELF.AddHistoryField(?srv:AddressLine2,4)
  SELF.AddHistoryField(?srv:AddressLine3,5)
  SELF.AddHistoryField(?srv:Postcode,6)
  SELF.AddHistoryField(?srv:AccountActive,7)
  SELF.AddHistoryField(?srv:TurnaroundDaysRetail,8)
  SELF.AddHistoryField(?srv:TurnaroundDaysExch,9)
  SELF.AddHistoryField(?srv:BankHolidaysFileName,10)
  SELF.AddHistoryField(?srv:ContactName,11)
  SELF.AddHistoryField(?srv:ContactNumber,12)
  SELF.AddHistoryField(?srv:ContactEmail,13)
  SELF.AddHistoryField(?srv:ExchangeImportType,27)
  SELF.AddHistoryField(?srv:UseServiceCentreFTP,14)
  SELF.AddHistoryField(?srv:FTPServer,15)
  SELF.AddHistoryField(?srv:FTPUsername,16)
  SELF.AddHistoryField(?srv:FTPPassword,17)
  SELF.AddHistoryField(?srv:FTPPath,18)
  SELF.AddHistoryField(?srv:EmailToAddress,19)
  SELF.AddHistoryField(?srv:ProcessFailureCount,20)
  SELF.AddUpdateFile(Access:SIDSRVCN)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:SIDSRVCN.Open
  Relate:USERS.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SIDSRVCN
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save current turnaround times
  tmp:TurnaroundDaysRetail = srv:TurnaroundDaysRetail
  tmp:TurnaroundDaysExch = srv:TurnaroundDaysExch
  
  ! Save current repair limits
  tmp:MonRepairCapacity = srv:MonRepairCapacity
  tmp:TueRepairCapacity = srv:TueRepairCapacity
  tmp:WedRepairCapacity = srv:WedRepairCapacity
  tmp:ThuRepairCapacity = srv:ThuRepairCapacity
  tmp:FriRepairCapacity = srv:FriRepairCapacity
  ! Security Check
  If SecurityCheck('SSC - NON WORKING DAYS')
      ?BtnNonWorking{prop:Disable} = 1
  End ! If SecurityCheck('NON WORKING DAYS')
  If SecurityCheck('SSC - ATTACHED MODELS')
      ?BtnModelsSC{prop:Disable} = 1
  End ! If SecurityCheck('SSC- ATTACHED MODELS')
  If SecurityCheck('SSC - TRANSIT DAYS')
      ?BtnTransitDays{prop:Disable} = 1
  End ! If SecurityCheck('SSC - TRANSIT DAYS')
  If SecurityCheck('SSC - REPAIR CAPACITY LIMIT')
      ?Button:RepairCapacityLimit{prop:Disable} = 1
  End ! If SecurityCheck('SSC - TRANSIT DAYS')
  Do RecolourWindow
  ! support for CPCS
  IF ?srv:UseServiceCentreFTP{Prop:Checked} = True
    ENABLE(?srv:FTPServer)
    ENABLE(?srv:FTPUsername)
    ENABLE(?srv:FTPPassword)
    ENABLE(?srv:FTPPath)
    ENABLE(?srv:EmailToAddress)
    ENABLE(?srv:ProcessFailureCount)
  END
  IF ?srv:UseServiceCentreFTP{Prop:Checked} = False
    DISABLE(?srv:FTPServer)
    DISABLE(?srv:FTPUsername)
    DISABLE(?srv:FTPPassword)
    DISABLE(?srv:FTPPath)
    DISABLE(?srv:EmailToAddress)
    DISABLE(?srv:ProcessFailureCount)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:SIDSRVCN.Close
    Relate:USERS.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSIDServiceCentre',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      if srv:ExchangeImportType = 0
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('Please select an exchange import type.','Error',|
                       icon:Hand,'&OK',1,1)
              Of 1 ! &OK Button
          End!Case Message
          Select(?srv:ExchangeImportType)
          Display()
          Cycle
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?srv:UseServiceCentreFTP
      IF ?srv:UseServiceCentreFTP{Prop:Checked} = True
        ENABLE(?srv:FTPServer)
        ENABLE(?srv:FTPUsername)
        ENABLE(?srv:FTPPassword)
        ENABLE(?srv:FTPPath)
        ENABLE(?srv:EmailToAddress)
        ENABLE(?srv:ProcessFailureCount)
      END
      IF ?srv:UseServiceCentreFTP{Prop:Checked} = False
        DISABLE(?srv:FTPServer)
        DISABLE(?srv:FTPUsername)
        DISABLE(?srv:FTPPassword)
        DISABLE(?srv:FTPPath)
        DISABLE(?srv:EmailToAddress)
        DISABLE(?srv:ProcessFailureCount)
      END
      ThisWindow.Reset
    OF ?BtnNonWorking
      ThisWindow.Update
      BrowseSIDNonWorkingDays(srv:SCAccountID)
      ThisWindow.Reset
    OF ?BtnModelsSC
      ThisWindow.Update
      BrowseSIDModelsToServiceCentre(srv:SCAccountID, srv:TurnaroundDaysRetail, srv:TurnaroundDaysExch)
      ThisWindow.Reset
    OF ?BtnTransitDays
      ThisWindow.Update
      BrowseSIDSCAccountRegions(srv:SCAccountID)
      ThisWindow.Reset
    OF ?Button:RepairCapacityLimit
      ThisWindow.Update
      SIDRepairCapacityLimit
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeCompleted()
  Access:Users.ClearKey(use:password_key)
  use:password    = glo:password
  Access:Users.Fetch(use:password_key)
  
  if tmp:TurnaroundDaysRetail <> srv:TurnaroundDaysRetail
      ErrorLog(clip(srv:ServiceCentreName) & ' - Retail Turnaround Time Changed From: ' & tmp:TurnaroundDaysRetail & ' to ' & srv:TurnaroundDaysRetail & ' days by User: (' & clip(use:User_Code) & ') ' & clip(use:Forename) & ' ' & clip(use:Surname), clip(path()) & '\SIDCHANGE.LOG')
  end
  if tmp:TurnaroundDaysExch <> srv:TurnaroundDaysExch
      ErrorLog(clip(srv:ServiceCentreName) & ' - Exchange Turnaround Time Changed From: ' & tmp:TurnaroundDaysExch & ' to ' & srv:TurnaroundDaysExch & ' days by User: (' & clip(use:User_Code) & ') ' & clip(use:Forename) & ' ' & clip(use:Surname), clip(path()) & '\SIDCHANGE.LOG')
  end
  
  if tmp:MonRepairCapacity <> srv:MonRepairCapacity
      ErrorLog(clip(srv:ServiceCentreName) & ' - Monday Repair Capacity Changed From: ' & tmp:MonRepairCapacity & ' to ' & srv:MonRepairCapacity & ' by User: (' & clip(use:User_Code) & ') ' & clip(use:Forename) & ' ' & clip(use:Surname), clip(path()) & '\SIDCHANGE.LOG')
  end
  if tmp:TueRepairCapacity <> srv:TueRepairCapacity
      ErrorLog(clip(srv:ServiceCentreName) & ' - Tuesday Repair Capacity Changed From: ' & tmp:TueRepairCapacity & ' to ' & srv:TueRepairCapacity & ' by User: (' & clip(use:User_Code) & ') ' & clip(use:Forename) & ' ' & clip(use:Surname), clip(path()) & '\SIDCHANGE.LOG')
  end
  if tmp:WedRepairCapacity <> srv:WedRepairCapacity
      ErrorLog(clip(srv:ServiceCentreName) & ' - Wednesday Repair Capacity Changed From: ' & tmp:WedRepairCapacity & ' to ' & srv:WedRepairCapacity & ' by User: (' & clip(use:User_Code) & ') ' & clip(use:Forename) & ' ' & clip(use:Surname), clip(path()) & '\SIDCHANGE.LOG')
  end
  if tmp:ThuRepairCapacity <> srv:ThuRepairCapacity
      ErrorLog(clip(srv:ServiceCentreName) & ' - Thursday Repair Capacity Changed From: ' & tmp:ThuRepairCapacity & ' to ' & srv:ThuRepairCapacity & ' by User: (' & clip(use:User_Code) & ') ' & clip(use:Forename) & ' ' & clip(use:Surname), clip(path()) & '\SIDCHANGE.LOG')
  end
  if tmp:FriRepairCapacity <> srv:FriRepairCapacity
      ErrorLog(clip(srv:ServiceCentreName) & ' - Friday Repair Capacity Changed From: ' & tmp:FriRepairCapacity & ' to ' & srv:FriRepairCapacity & ' by User: (' & clip(use:User_Code) & ') ' & clip(use:Forename) & ' ' & clip(use:Surname), clip(path()) & '\SIDCHANGE.LOG')
  end
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSIDServiceCentre')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

