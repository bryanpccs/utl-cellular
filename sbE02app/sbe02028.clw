

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBE02028.INC'),ONCE        !Local module procedure declarations
                     END





BrowseTRADEACC PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BrLocator1           STRING(50)
BrFilter1            STRING(300)
BrFilter2            STRING(300)
BrLocator2           STRING(50)
BRW1::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:Telephone_Number)
                       PROJECT(tra:Fax_Number)
                       PROJECT(tra:Labour_VAT_Code)
                       PROJECT(tra:Parts_VAT_Code)
                       PROJECT(tra:Retail_VAT_Code)
                       PROJECT(tra:Use_Sub_Accounts)
                       PROJECT(tra:Invoice_Sub_Accounts)
                       PROJECT(tra:Stop_Account)
                       PROJECT(tra:Price_Despatch)
                       PROJECT(tra:Price_Retail_Despatch)
                       PROJECT(tra:Print_Despatch_Complete)
                       PROJECT(tra:Print_Despatch_Despatch)
                       PROJECT(tra:Standard_Repair_Type)
                       PROJECT(tra:Courier_Incoming)
                       PROJECT(tra:Courier_Outgoing)
                       PROJECT(tra:Use_Contact_Name)
                       PROJECT(tra:VAT_Number)
                       PROJECT(tra:Use_Delivery_Address)
                       PROJECT(tra:Use_Collection_Address)
                       PROJECT(tra:UseCustDespAdd)
                       PROJECT(tra:Allow_Loan)
                       PROJECT(tra:Allow_Exchange)
                       PROJECT(tra:Force_Fault_Codes)
                       PROJECT(tra:Despatch_Invoiced_Jobs)
                       PROJECT(tra:Despatch_Paid_Jobs)
                       PROJECT(tra:Print_Despatch_Notes)
                       PROJECT(tra:Print_Retail_Despatch_Note)
                       PROJECT(tra:Print_Retail_Picking_Note)
                       PROJECT(tra:Despatch_Note_Per_Item)
                       PROJECT(tra:Skip_Despatch)
                       PROJECT(tra:Summary_Despatch_Notes)
                       PROJECT(tra:Exchange_Stock_Type)
                       PROJECT(tra:Loan_Stock_Type)
                       PROJECT(tra:Courier_Cost)
                       PROJECT(tra:Force_Estimate)
                       PROJECT(tra:Estimate_If_Over)
                       PROJECT(tra:Turnaround_Time)
                       PROJECT(tra:Retail_Payment_Type)
                       PROJECT(tra:Retail_Price_Structure)
                       PROJECT(tra:Use_Customer_Address)
                       PROJECT(tra:Invoice_Customer_Address)
                       PROJECT(tra:ZeroChargeable)
                       PROJECT(tra:RefurbCharge)
                       PROJECT(tra:ChargeType)
                       PROJECT(tra:WarChargeType)
                       PROJECT(tra:ExchangeAcc)
                       PROJECT(tra:ExcludeBouncer)
                       PROJECT(tra:RetailZeroParts)
                       PROJECT(tra:HideDespAdd)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:Telephone_Number   LIKE(tra:Telephone_Number)     !List box control field - type derived from field
tra:Fax_Number         LIKE(tra:Fax_Number)           !List box control field - type derived from field
tra:Labour_VAT_Code    LIKE(tra:Labour_VAT_Code)      !List box control field - type derived from field
tra:Parts_VAT_Code     LIKE(tra:Parts_VAT_Code)       !List box control field - type derived from field
tra:Retail_VAT_Code    LIKE(tra:Retail_VAT_Code)      !List box control field - type derived from field
tra:Use_Sub_Accounts   LIKE(tra:Use_Sub_Accounts)     !List box control field - type derived from field
tra:Invoice_Sub_Accounts LIKE(tra:Invoice_Sub_Accounts) !List box control field - type derived from field
tra:Stop_Account       LIKE(tra:Stop_Account)         !List box control field - type derived from field
tra:Price_Despatch     LIKE(tra:Price_Despatch)       !List box control field - type derived from field
tra:Price_Retail_Despatch LIKE(tra:Price_Retail_Despatch) !List box control field - type derived from field
tra:Print_Despatch_Complete LIKE(tra:Print_Despatch_Complete) !List box control field - type derived from field
tra:Print_Despatch_Despatch LIKE(tra:Print_Despatch_Despatch) !List box control field - type derived from field
tra:Standard_Repair_Type LIKE(tra:Standard_Repair_Type) !List box control field - type derived from field
tra:Courier_Incoming   LIKE(tra:Courier_Incoming)     !List box control field - type derived from field
tra:Courier_Outgoing   LIKE(tra:Courier_Outgoing)     !List box control field - type derived from field
tra:Use_Contact_Name   LIKE(tra:Use_Contact_Name)     !List box control field - type derived from field
tra:VAT_Number         LIKE(tra:VAT_Number)           !List box control field - type derived from field
tra:Use_Delivery_Address LIKE(tra:Use_Delivery_Address) !List box control field - type derived from field
tra:Use_Collection_Address LIKE(tra:Use_Collection_Address) !List box control field - type derived from field
tra:UseCustDespAdd     LIKE(tra:UseCustDespAdd)       !List box control field - type derived from field
tra:Allow_Loan         LIKE(tra:Allow_Loan)           !List box control field - type derived from field
tra:Allow_Exchange     LIKE(tra:Allow_Exchange)       !List box control field - type derived from field
tra:Force_Fault_Codes  LIKE(tra:Force_Fault_Codes)    !List box control field - type derived from field
tra:Despatch_Invoiced_Jobs LIKE(tra:Despatch_Invoiced_Jobs) !List box control field - type derived from field
tra:Despatch_Paid_Jobs LIKE(tra:Despatch_Paid_Jobs)   !List box control field - type derived from field
tra:Print_Despatch_Notes LIKE(tra:Print_Despatch_Notes) !List box control field - type derived from field
tra:Print_Retail_Despatch_Note LIKE(tra:Print_Retail_Despatch_Note) !List box control field - type derived from field
tra:Print_Retail_Picking_Note LIKE(tra:Print_Retail_Picking_Note) !List box control field - type derived from field
tra:Despatch_Note_Per_Item LIKE(tra:Despatch_Note_Per_Item) !List box control field - type derived from field
tra:Skip_Despatch      LIKE(tra:Skip_Despatch)        !List box control field - type derived from field
tra:Summary_Despatch_Notes LIKE(tra:Summary_Despatch_Notes) !List box control field - type derived from field
tra:Exchange_Stock_Type LIKE(tra:Exchange_Stock_Type) !List box control field - type derived from field
tra:Loan_Stock_Type    LIKE(tra:Loan_Stock_Type)      !List box control field - type derived from field
tra:Courier_Cost       LIKE(tra:Courier_Cost)         !List box control field - type derived from field
tra:Force_Estimate     LIKE(tra:Force_Estimate)       !List box control field - type derived from field
tra:Estimate_If_Over   LIKE(tra:Estimate_If_Over)     !List box control field - type derived from field
tra:Turnaround_Time    LIKE(tra:Turnaround_Time)      !List box control field - type derived from field
tra:Retail_Payment_Type LIKE(tra:Retail_Payment_Type) !List box control field - type derived from field
tra:Retail_Price_Structure LIKE(tra:Retail_Price_Structure) !List box control field - type derived from field
tra:Use_Customer_Address LIKE(tra:Use_Customer_Address) !List box control field - type derived from field
tra:Invoice_Customer_Address LIKE(tra:Invoice_Customer_Address) !List box control field - type derived from field
tra:ZeroChargeable     LIKE(tra:ZeroChargeable)       !List box control field - type derived from field
tra:RefurbCharge       LIKE(tra:RefurbCharge)         !List box control field - type derived from field
tra:ChargeType         LIKE(tra:ChargeType)           !List box control field - type derived from field
tra:WarChargeType      LIKE(tra:WarChargeType)        !List box control field - type derived from field
tra:ExchangeAcc        LIKE(tra:ExchangeAcc)          !List box control field - type derived from field
tra:ExcludeBouncer     LIKE(tra:ExcludeBouncer)       !List box control field - type derived from field
tra:RetailZeroParts    LIKE(tra:RetailZeroParts)      !List box control field - type derived from field
tra:HideDespAdd        LIKE(tra:HideDespAdd)          !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse the Trade Account File'),AT(,,440,258),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('BrowseTRADEACC'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,36,340,216),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('65L(2)|M~Account Number~@s15@119L(2)|M~Company Name~@s30@70L(2)|M~Telephone Numb' &|
   'er~@s15@60L(2)|M~Fax Number~@s15@68L(2)|M~Labour VAT Code~@s2@68L(2)|M~Labour VA' &|
   'T Code~@s2@68L(2)|M~Labour VAT Code~@s2@68L(2)|M~Use Sub Accounts~@s3@68L(2)|M~I' &|
   'nvoice Sub Accounts~@s3@68L(2)|M~Stop Account~@s3@68L(2)|M~Price Despatch Notes~' &|
   '@s3@68L(2)|M~Price Retail Despatch Notes~@s3@68L(2)|M~Print Despatch Complete~@s' &|
   '3@68L(2)|M~Print Despatch Despatch~@s3@68L(2)|M~Standard Repair Type~@s15@120L(2' &|
   ')|M~Courier Incoming~@s30@120L(2)|M~Courier Outgoing~@s30@68L(2)|M~Use Contact N' &|
   'ame~@s3@68L(2)|M~V.A.T. Number~@s30@68L(2)|M~Use Delivery Address~@s3@68L(2)|M~U' &|
   'se Delivery Address~@s3@68R(2)|M~Use Cust Desp Add~L@s3@68R(2)|M~Allow Loan~L@s3' &|
   '@68R(2)|M~Allow Exchange~L@s3@68R(2)|M~Force Fault Codes~L@s3@68R(2)|M~Despatch ' &|
   'Invoiced Jobs~L@s3@68R(2)|M~Despatch Paid Jobs~L@s3@68R(2)|M~Print Despatch Note' &|
   's~L@s3@68R(2)|M~Print Retail Despatch Note~L@s3@68R(2)|M~Print Retail Picking No' &|
   'te~L@s3@68R(2)|M~Despatch Note Per Item~L@s3@68R(2)|M~Skip Despatch~L@s3@68R(2)|' &|
   'M~Summary Despatch Notes~L@s3@120R(2)|M~Exchange Stock Type~L@s30@120R(2)|M~Loan' &|
   ' Stock Type~L@s30@68R(2)|M~Courier Cost~L@n14.2@68R(2)|M~Force Estimate~L@s3@68R' &|
   '(2)|M~Estimate If Over~L@n14.2@120R(2)|M~Turnaround Time~L@s30@68R(2)|M~Retail S' &|
   'ales Payment Type~L@s3@68R(2)|M~Retail Price Structure~L@s3@68R(2)|M~Use Custome' &|
   'r Address~L@s3@68R(2)|M~Use Customer Address~L@s3@68R(2)|M~Zero Chargeable~L@s3@' &|
   '68L(2)|M~Refurb Charge~@s3@120L(2)|M~Charge Type~@s30@120L(2)|M~War Charge Type~' &|
   '@s30@68L(2)|M~Exchange Acc~@s3@68L(2)|M~Exclude From Bouncer~@n1@68L(2)|M~Zero V' &|
   'alue Parts~@n1@68L(2)|M~Hide Address On Despatch Note~@n1@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(360,20,76,20),USE(?Select:2),LEFT,ICON('SELECT.ICO')
                       BUTTON('&Add'),AT(360,160,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Open/Amend'),AT(360,184,76,20),USE(?Change:3),LEFT,ICON('EDIT.ICO'),DEFAULT
                       BUTTON('&Delete'),AT(360,208,76,20),USE(?Delete:3),LEFT,ICON('DELETE.ICO')
                       SHEET,AT(4,4,348,252),USE(?CurrentTab),SPREAD
                         TAB('By Account Number'),USE(?Tab:2)
                           ENTRY(@s15),AT(8,20,124,10),USE(tra:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('Close'),AT(360,236,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepStringClass !STRING          !Xplore: Column displaying tra:Account_Number
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying tra:Account_Number
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying tra:Company_Name
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying tra:Company_Name
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying tra:Labour_VAT_Code
Xplore1Locator5      IncrementalLocatorClass          !Xplore: Column displaying tra:Labour_VAT_Code
Xplore1Step6         StepStringClass !STRING          !Xplore: Column displaying tra:Parts_VAT_Code
Xplore1Locator6      IncrementalLocatorClass          !Xplore: Column displaying tra:Parts_VAT_Code
Xplore1Step7         StepStringClass !STRING          !Xplore: Column displaying tra:Retail_VAT_Code
Xplore1Locator7      IncrementalLocatorClass          !Xplore: Column displaying tra:Retail_VAT_Code
Xplore1Step8         StepStringClass !STRING          !Xplore: Column displaying tra:Use_Sub_Accounts
Xplore1Locator8      IncrementalLocatorClass          !Xplore: Column displaying tra:Use_Sub_Accounts
Xplore1Step9         StepStringClass !STRING          !Xplore: Column displaying tra:Invoice_Sub_Accounts
Xplore1Locator9      IncrementalLocatorClass          !Xplore: Column displaying tra:Invoice_Sub_Accounts
Xplore1Step10        StepStringClass !STRING          !Xplore: Column displaying tra:Stop_Account
Xplore1Locator10     IncrementalLocatorClass          !Xplore: Column displaying tra:Stop_Account
Xplore1Step11        StepStringClass !STRING          !Xplore: Column displaying tra:Price_Despatch
Xplore1Locator11     IncrementalLocatorClass          !Xplore: Column displaying tra:Price_Despatch
Xplore1Step12        StepStringClass !STRING          !Xplore: Column displaying tra:Price_Retail_Despatch
Xplore1Locator12     IncrementalLocatorClass          !Xplore: Column displaying tra:Price_Retail_Despatch
Xplore1Step13        StepStringClass !STRING          !Xplore: Column displaying tra:Print_Despatch_Complete
Xplore1Locator13     IncrementalLocatorClass          !Xplore: Column displaying tra:Print_Despatch_Complete
Xplore1Step14        StepStringClass !STRING          !Xplore: Column displaying tra:Print_Despatch_Despatch
Xplore1Locator14     IncrementalLocatorClass          !Xplore: Column displaying tra:Print_Despatch_Despatch
Xplore1Step15        StepStringClass !STRING          !Xplore: Column displaying tra:Standard_Repair_Type
Xplore1Locator15     IncrementalLocatorClass          !Xplore: Column displaying tra:Standard_Repair_Type
Xplore1Step16        StepStringClass !STRING          !Xplore: Column displaying tra:Courier_Incoming
Xplore1Locator16     IncrementalLocatorClass          !Xplore: Column displaying tra:Courier_Incoming
Xplore1Step17        StepStringClass !STRING          !Xplore: Column displaying tra:Courier_Outgoing
Xplore1Locator17     IncrementalLocatorClass          !Xplore: Column displaying tra:Courier_Outgoing
Xplore1Step18        StepStringClass !STRING          !Xplore: Column displaying tra:Use_Contact_Name
Xplore1Locator18     IncrementalLocatorClass          !Xplore: Column displaying tra:Use_Contact_Name
Xplore1Step19        StepStringClass !STRING          !Xplore: Column displaying tra:VAT_Number
Xplore1Locator19     IncrementalLocatorClass          !Xplore: Column displaying tra:VAT_Number
Xplore1Step20        StepStringClass !STRING          !Xplore: Column displaying tra:Use_Delivery_Address
Xplore1Locator20     IncrementalLocatorClass          !Xplore: Column displaying tra:Use_Delivery_Address
Xplore1Step21        StepStringClass !STRING          !Xplore: Column displaying tra:Use_Collection_Address
Xplore1Locator21     IncrementalLocatorClass          !Xplore: Column displaying tra:Use_Collection_Address
Xplore1Step22        StepStringClass !STRING          !Xplore: Column displaying tra:UseCustDespAdd
Xplore1Locator22     IncrementalLocatorClass          !Xplore: Column displaying tra:UseCustDespAdd
Xplore1Step23        StepStringClass !STRING          !Xplore: Column displaying tra:Allow_Loan
Xplore1Locator23     IncrementalLocatorClass          !Xplore: Column displaying tra:Allow_Loan
Xplore1Step24        StepStringClass !STRING          !Xplore: Column displaying tra:Allow_Exchange
Xplore1Locator24     IncrementalLocatorClass          !Xplore: Column displaying tra:Allow_Exchange
Xplore1Step25        StepStringClass !STRING          !Xplore: Column displaying tra:Force_Fault_Codes
Xplore1Locator25     IncrementalLocatorClass          !Xplore: Column displaying tra:Force_Fault_Codes
Xplore1Step26        StepStringClass !STRING          !Xplore: Column displaying tra:Despatch_Invoiced_Jobs
Xplore1Locator26     IncrementalLocatorClass          !Xplore: Column displaying tra:Despatch_Invoiced_Jobs
Xplore1Step27        StepStringClass !STRING          !Xplore: Column displaying tra:Despatch_Paid_Jobs
Xplore1Locator27     IncrementalLocatorClass          !Xplore: Column displaying tra:Despatch_Paid_Jobs
Xplore1Step28        StepStringClass !STRING          !Xplore: Column displaying tra:Print_Despatch_Notes
Xplore1Locator28     IncrementalLocatorClass          !Xplore: Column displaying tra:Print_Despatch_Notes
Xplore1Step29        StepStringClass !STRING          !Xplore: Column displaying tra:Print_Retail_Despatch_Note
Xplore1Locator29     IncrementalLocatorClass          !Xplore: Column displaying tra:Print_Retail_Despatch_Note
Xplore1Step30        StepStringClass !STRING          !Xplore: Column displaying tra:Print_Retail_Picking_Note
Xplore1Locator30     IncrementalLocatorClass          !Xplore: Column displaying tra:Print_Retail_Picking_Note
Xplore1Step31        StepStringClass !STRING          !Xplore: Column displaying tra:Despatch_Note_Per_Item
Xplore1Locator31     IncrementalLocatorClass          !Xplore: Column displaying tra:Despatch_Note_Per_Item
Xplore1Step32        StepStringClass !STRING          !Xplore: Column displaying tra:Skip_Despatch
Xplore1Locator32     IncrementalLocatorClass          !Xplore: Column displaying tra:Skip_Despatch
Xplore1Step33        StepStringClass !STRING          !Xplore: Column displaying tra:Summary_Despatch_Notes
Xplore1Locator33     IncrementalLocatorClass          !Xplore: Column displaying tra:Summary_Despatch_Notes
Xplore1Step34        StepStringClass !STRING          !Xplore: Column displaying tra:Exchange_Stock_Type
Xplore1Locator34     IncrementalLocatorClass          !Xplore: Column displaying tra:Exchange_Stock_Type
Xplore1Step35        StepStringClass !STRING          !Xplore: Column displaying tra:Loan_Stock_Type
Xplore1Locator35     IncrementalLocatorClass          !Xplore: Column displaying tra:Loan_Stock_Type
Xplore1Step36        StepRealClass   !REAL            !Xplore: Column displaying tra:Courier_Cost
Xplore1Locator36     IncrementalLocatorClass          !Xplore: Column displaying tra:Courier_Cost
Xplore1Step37        StepStringClass !STRING          !Xplore: Column displaying tra:Force_Estimate
Xplore1Locator37     IncrementalLocatorClass          !Xplore: Column displaying tra:Force_Estimate
Xplore1Step38        StepRealClass   !REAL            !Xplore: Column displaying tra:Estimate_If_Over
Xplore1Locator38     IncrementalLocatorClass          !Xplore: Column displaying tra:Estimate_If_Over
Xplore1Step39        StepStringClass !STRING          !Xplore: Column displaying tra:Turnaround_Time
Xplore1Locator39     IncrementalLocatorClass          !Xplore: Column displaying tra:Turnaround_Time
Xplore1Step40        StepStringClass !STRING          !Xplore: Column displaying tra:Retail_Payment_Type
Xplore1Locator40     IncrementalLocatorClass          !Xplore: Column displaying tra:Retail_Payment_Type
Xplore1Step41        StepStringClass !STRING          !Xplore: Column displaying tra:Retail_Price_Structure
Xplore1Locator41     IncrementalLocatorClass          !Xplore: Column displaying tra:Retail_Price_Structure
Xplore1Step42        StepStringClass !STRING          !Xplore: Column displaying tra:Use_Customer_Address
Xplore1Locator42     IncrementalLocatorClass          !Xplore: Column displaying tra:Use_Customer_Address
Xplore1Step43        StepStringClass !STRING          !Xplore: Column displaying tra:Invoice_Customer_Address
Xplore1Locator43     IncrementalLocatorClass          !Xplore: Column displaying tra:Invoice_Customer_Address
Xplore1Step44        StepStringClass !STRING          !Xplore: Column displaying tra:ZeroChargeable
Xplore1Locator44     IncrementalLocatorClass          !Xplore: Column displaying tra:ZeroChargeable
Xplore1Step45        StepStringClass !STRING          !Xplore: Column displaying tra:RefurbCharge
Xplore1Locator45     IncrementalLocatorClass          !Xplore: Column displaying tra:RefurbCharge
Xplore1Step46        StepStringClass !STRING          !Xplore: Column displaying tra:ChargeType
Xplore1Locator46     IncrementalLocatorClass          !Xplore: Column displaying tra:ChargeType
Xplore1Step47        StepStringClass !STRING          !Xplore: Column displaying tra:WarChargeType
Xplore1Locator47     IncrementalLocatorClass          !Xplore: Column displaying tra:WarChargeType
Xplore1Step48        StepStringClass !STRING          !Xplore: Column displaying tra:ExchangeAcc
Xplore1Locator48     IncrementalLocatorClass          !Xplore: Column displaying tra:ExchangeAcc
Xplore1Step49        StepCustomClass !BYTE            !Xplore: Column displaying tra:ExcludeBouncer
Xplore1Locator49     IncrementalLocatorClass          !Xplore: Column displaying tra:ExcludeBouncer
Xplore1Step50        StepCustomClass !BYTE            !Xplore: Column displaying tra:RetailZeroParts
Xplore1Locator50     IncrementalLocatorClass          !Xplore: Column displaying tra:RetailZeroParts
Xplore1Step51        StepCustomClass !BYTE            !Xplore: Column displaying tra:HideDespAdd
Xplore1Locator51     IncrementalLocatorClass          !Xplore: Column displaying tra:HideDespAdd
Xplore1Step52        StepLongClass   !LONG            !Xplore: Column displaying tra:RecordNumber
Xplore1Locator52     IncrementalLocatorClass          !Xplore: Column displaying tra:RecordNumber

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?tra:Account_Number{prop:ReadOnly} = True
        ?tra:Account_Number{prop:FontColor} = 65793
        ?tra:Account_Number{prop:Color} = 15066597
    Elsif ?tra:Account_Number{prop:Req} = True
        ?tra:Account_Number{prop:FontColor} = 65793
        ?tra:Account_Number{prop:Color} = 8454143
    Else ! If ?tra:Account_Number{prop:Req} = True
        ?tra:Account_Number{prop:FontColor} = 65793
        ?tra:Account_Number{prop:Color} = 16777215
    End ! If ?tra:Account_Number{prop:Req} = True
    ?tra:Account_Number{prop:Trn} = 0
    ?tra:Account_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BrowseTRADEACC',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'BrowseTRADEACC',1)
    SolaceViewVars('LocalRequest',LocalRequest,'BrowseTRADEACC',1)
    SolaceViewVars('FilesOpened',FilesOpened,'BrowseTRADEACC',1)
    SolaceViewVars('BrLocator1',BrLocator1,'BrowseTRADEACC',1)
    SolaceViewVars('BrFilter1',BrFilter1,'BrowseTRADEACC',1)
    SolaceViewVars('BrFilter2',BrFilter2,'BrowseTRADEACC',1)
    SolaceViewVars('BrLocator2',BrLocator2,'BrowseTRADEACC',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Account_Number;  SolaceCtrlName = '?tra:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('BrowseTRADEACC','?Browse:1')     !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseTRADEACC')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BrowseTRADEACC')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:TRADEACC.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TRADEACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbe02app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,tra:Company_Name_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,tra:Company_Name,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,tra:Account_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?tra:Account_Number,tra:Account_Number,1,BRW1)
  BRW1.AddField(tra:Account_Number,BRW1.Q.tra:Account_Number)
  BRW1.AddField(tra:Company_Name,BRW1.Q.tra:Company_Name)
  BRW1.AddField(tra:Telephone_Number,BRW1.Q.tra:Telephone_Number)
  BRW1.AddField(tra:Fax_Number,BRW1.Q.tra:Fax_Number)
  BRW1.AddField(tra:Labour_VAT_Code,BRW1.Q.tra:Labour_VAT_Code)
  BRW1.AddField(tra:Parts_VAT_Code,BRW1.Q.tra:Parts_VAT_Code)
  BRW1.AddField(tra:Retail_VAT_Code,BRW1.Q.tra:Retail_VAT_Code)
  BRW1.AddField(tra:Use_Sub_Accounts,BRW1.Q.tra:Use_Sub_Accounts)
  BRW1.AddField(tra:Invoice_Sub_Accounts,BRW1.Q.tra:Invoice_Sub_Accounts)
  BRW1.AddField(tra:Stop_Account,BRW1.Q.tra:Stop_Account)
  BRW1.AddField(tra:Price_Despatch,BRW1.Q.tra:Price_Despatch)
  BRW1.AddField(tra:Price_Retail_Despatch,BRW1.Q.tra:Price_Retail_Despatch)
  BRW1.AddField(tra:Print_Despatch_Complete,BRW1.Q.tra:Print_Despatch_Complete)
  BRW1.AddField(tra:Print_Despatch_Despatch,BRW1.Q.tra:Print_Despatch_Despatch)
  BRW1.AddField(tra:Standard_Repair_Type,BRW1.Q.tra:Standard_Repair_Type)
  BRW1.AddField(tra:Courier_Incoming,BRW1.Q.tra:Courier_Incoming)
  BRW1.AddField(tra:Courier_Outgoing,BRW1.Q.tra:Courier_Outgoing)
  BRW1.AddField(tra:Use_Contact_Name,BRW1.Q.tra:Use_Contact_Name)
  BRW1.AddField(tra:VAT_Number,BRW1.Q.tra:VAT_Number)
  BRW1.AddField(tra:Use_Delivery_Address,BRW1.Q.tra:Use_Delivery_Address)
  BRW1.AddField(tra:Use_Collection_Address,BRW1.Q.tra:Use_Collection_Address)
  BRW1.AddField(tra:UseCustDespAdd,BRW1.Q.tra:UseCustDespAdd)
  BRW1.AddField(tra:Allow_Loan,BRW1.Q.tra:Allow_Loan)
  BRW1.AddField(tra:Allow_Exchange,BRW1.Q.tra:Allow_Exchange)
  BRW1.AddField(tra:Force_Fault_Codes,BRW1.Q.tra:Force_Fault_Codes)
  BRW1.AddField(tra:Despatch_Invoiced_Jobs,BRW1.Q.tra:Despatch_Invoiced_Jobs)
  BRW1.AddField(tra:Despatch_Paid_Jobs,BRW1.Q.tra:Despatch_Paid_Jobs)
  BRW1.AddField(tra:Print_Despatch_Notes,BRW1.Q.tra:Print_Despatch_Notes)
  BRW1.AddField(tra:Print_Retail_Despatch_Note,BRW1.Q.tra:Print_Retail_Despatch_Note)
  BRW1.AddField(tra:Print_Retail_Picking_Note,BRW1.Q.tra:Print_Retail_Picking_Note)
  BRW1.AddField(tra:Despatch_Note_Per_Item,BRW1.Q.tra:Despatch_Note_Per_Item)
  BRW1.AddField(tra:Skip_Despatch,BRW1.Q.tra:Skip_Despatch)
  BRW1.AddField(tra:Summary_Despatch_Notes,BRW1.Q.tra:Summary_Despatch_Notes)
  BRW1.AddField(tra:Exchange_Stock_Type,BRW1.Q.tra:Exchange_Stock_Type)
  BRW1.AddField(tra:Loan_Stock_Type,BRW1.Q.tra:Loan_Stock_Type)
  BRW1.AddField(tra:Courier_Cost,BRW1.Q.tra:Courier_Cost)
  BRW1.AddField(tra:Force_Estimate,BRW1.Q.tra:Force_Estimate)
  BRW1.AddField(tra:Estimate_If_Over,BRW1.Q.tra:Estimate_If_Over)
  BRW1.AddField(tra:Turnaround_Time,BRW1.Q.tra:Turnaround_Time)
  BRW1.AddField(tra:Retail_Payment_Type,BRW1.Q.tra:Retail_Payment_Type)
  BRW1.AddField(tra:Retail_Price_Structure,BRW1.Q.tra:Retail_Price_Structure)
  BRW1.AddField(tra:Use_Customer_Address,BRW1.Q.tra:Use_Customer_Address)
  BRW1.AddField(tra:Invoice_Customer_Address,BRW1.Q.tra:Invoice_Customer_Address)
  BRW1.AddField(tra:ZeroChargeable,BRW1.Q.tra:ZeroChargeable)
  BRW1.AddField(tra:RefurbCharge,BRW1.Q.tra:RefurbCharge)
  BRW1.AddField(tra:ChargeType,BRW1.Q.tra:ChargeType)
  BRW1.AddField(tra:WarChargeType,BRW1.Q.tra:WarChargeType)
  BRW1.AddField(tra:ExchangeAcc,BRW1.Q.tra:ExchangeAcc)
  BRW1.AddField(tra:ExcludeBouncer,BRW1.Q.tra:ExcludeBouncer)
  BRW1.AddField(tra:RetailZeroParts,BRW1.Q.tra:RetailZeroParts)
  BRW1.AddField(tra:HideDespAdd,BRW1.Q.tra:HideDespAdd)
  BRW1.AddField(tra:RecordNumber,BRW1.Q.tra:RecordNumber)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:TRADEACC.Close
    Relate:USERS_ALIAS.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('BrowseTRADEACC','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BrowseTRADEACC',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            If SecurityCheck('TRADE ACCOUNTS - INSERT')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of changerecord
            If SecurityCheck('TRADE ACCOUNTS - CHANGE')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of deleterecord
            If SecurityCheck('TRADE ACCOUNTS - DELETE')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateTRADEACC
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(tra:Account_Number,BRW1.Q.tra:Account_Number)
  Xplore1.AddField(tra:Company_Name,BRW1.Q.tra:Company_Name)
  Xplore1.AddField(tra:Telephone_Number,BRW1.Q.tra:Telephone_Number)
  Xplore1.AddField(tra:Fax_Number,BRW1.Q.tra:Fax_Number)
  Xplore1.AddField(tra:Labour_VAT_Code,BRW1.Q.tra:Labour_VAT_Code)
  Xplore1.AddField(tra:Parts_VAT_Code,BRW1.Q.tra:Parts_VAT_Code)
  Xplore1.AddField(tra:Retail_VAT_Code,BRW1.Q.tra:Retail_VAT_Code)
  Xplore1.AddField(tra:Use_Sub_Accounts,BRW1.Q.tra:Use_Sub_Accounts)
  Xplore1.AddField(tra:Invoice_Sub_Accounts,BRW1.Q.tra:Invoice_Sub_Accounts)
  Xplore1.AddField(tra:Stop_Account,BRW1.Q.tra:Stop_Account)
  Xplore1.AddField(tra:Price_Despatch,BRW1.Q.tra:Price_Despatch)
  Xplore1.AddField(tra:Price_Retail_Despatch,BRW1.Q.tra:Price_Retail_Despatch)
  Xplore1.AddField(tra:Print_Despatch_Complete,BRW1.Q.tra:Print_Despatch_Complete)
  Xplore1.AddField(tra:Print_Despatch_Despatch,BRW1.Q.tra:Print_Despatch_Despatch)
  Xplore1.AddField(tra:Standard_Repair_Type,BRW1.Q.tra:Standard_Repair_Type)
  Xplore1.AddField(tra:Courier_Incoming,BRW1.Q.tra:Courier_Incoming)
  Xplore1.AddField(tra:Courier_Outgoing,BRW1.Q.tra:Courier_Outgoing)
  Xplore1.AddField(tra:Use_Contact_Name,BRW1.Q.tra:Use_Contact_Name)
  Xplore1.AddField(tra:VAT_Number,BRW1.Q.tra:VAT_Number)
  Xplore1.AddField(tra:Use_Delivery_Address,BRW1.Q.tra:Use_Delivery_Address)
  Xplore1.AddField(tra:Use_Collection_Address,BRW1.Q.tra:Use_Collection_Address)
  Xplore1.AddField(tra:UseCustDespAdd,BRW1.Q.tra:UseCustDespAdd)
  Xplore1.AddField(tra:Allow_Loan,BRW1.Q.tra:Allow_Loan)
  Xplore1.AddField(tra:Allow_Exchange,BRW1.Q.tra:Allow_Exchange)
  Xplore1.AddField(tra:Force_Fault_Codes,BRW1.Q.tra:Force_Fault_Codes)
  Xplore1.AddField(tra:Despatch_Invoiced_Jobs,BRW1.Q.tra:Despatch_Invoiced_Jobs)
  Xplore1.AddField(tra:Despatch_Paid_Jobs,BRW1.Q.tra:Despatch_Paid_Jobs)
  Xplore1.AddField(tra:Print_Despatch_Notes,BRW1.Q.tra:Print_Despatch_Notes)
  Xplore1.AddField(tra:Print_Retail_Despatch_Note,BRW1.Q.tra:Print_Retail_Despatch_Note)
  Xplore1.AddField(tra:Print_Retail_Picking_Note,BRW1.Q.tra:Print_Retail_Picking_Note)
  Xplore1.AddField(tra:Despatch_Note_Per_Item,BRW1.Q.tra:Despatch_Note_Per_Item)
  Xplore1.AddField(tra:Skip_Despatch,BRW1.Q.tra:Skip_Despatch)
  Xplore1.AddField(tra:Summary_Despatch_Notes,BRW1.Q.tra:Summary_Despatch_Notes)
  Xplore1.AddField(tra:Exchange_Stock_Type,BRW1.Q.tra:Exchange_Stock_Type)
  Xplore1.AddField(tra:Loan_Stock_Type,BRW1.Q.tra:Loan_Stock_Type)
  Xplore1.AddField(tra:Courier_Cost,BRW1.Q.tra:Courier_Cost)
  Xplore1.AddField(tra:Force_Estimate,BRW1.Q.tra:Force_Estimate)
  Xplore1.AddField(tra:Estimate_If_Over,BRW1.Q.tra:Estimate_If_Over)
  Xplore1.AddField(tra:Turnaround_Time,BRW1.Q.tra:Turnaround_Time)
  Xplore1.AddField(tra:Retail_Payment_Type,BRW1.Q.tra:Retail_Payment_Type)
  Xplore1.AddField(tra:Retail_Price_Structure,BRW1.Q.tra:Retail_Price_Structure)
  Xplore1.AddField(tra:Use_Customer_Address,BRW1.Q.tra:Use_Customer_Address)
  Xplore1.AddField(tra:Invoice_Customer_Address,BRW1.Q.tra:Invoice_Customer_Address)
  Xplore1.AddField(tra:ZeroChargeable,BRW1.Q.tra:ZeroChargeable)
  Xplore1.AddField(tra:RefurbCharge,BRW1.Q.tra:RefurbCharge)
  Xplore1.AddField(tra:ChargeType,BRW1.Q.tra:ChargeType)
  Xplore1.AddField(tra:WarChargeType,BRW1.Q.tra:WarChargeType)
  Xplore1.AddField(tra:ExchangeAcc,BRW1.Q.tra:ExchangeAcc)
  Xplore1.AddField(tra:ExcludeBouncer,BRW1.Q.tra:ExcludeBouncer)
  Xplore1.AddField(tra:RetailZeroParts,BRW1.Q.tra:RetailZeroParts)
  Xplore1.AddField(tra:HideDespAdd,BRW1.Q.tra:HideDespAdd)
  Xplore1.AddField(tra:RecordNumber,BRW1.Q.tra:RecordNumber)
  BRW1.FileOrderNbr = BRW1.AddSortOrder()             !Xplore Sort Order for File Sequence
  BRW1.SetOrder('')                                   !Xplore
  Xplore1.AddAllColumnSortOrders(0)                   !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BrowseTRADEACC')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?tra:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Account_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Account_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 1                           !Xplore
        Xplore1.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  END                                                 !Xplore
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !tra:Account_Number
  OF 2 !tra:Company_Name
  OF 3 !tra:Telephone_Number
        ColumnString     = SUB(LEFT(BRW1.Q.tra:Telephone_Number),1,20)
  OF 4 !tra:Fax_Number
        ColumnString     = SUB(LEFT(BRW1.Q.tra:Fax_Number),1,20)
  OF 5 !tra:Labour_VAT_Code
  OF 6 !tra:Parts_VAT_Code
  OF 7 !tra:Retail_VAT_Code
  OF 8 !tra:Use_Sub_Accounts
  OF 9 !tra:Invoice_Sub_Accounts
  OF 10 !tra:Stop_Account
  OF 11 !tra:Price_Despatch
  OF 12 !tra:Price_Retail_Despatch
  OF 13 !tra:Print_Despatch_Complete
  OF 14 !tra:Print_Despatch_Despatch
  OF 15 !tra:Standard_Repair_Type
  OF 16 !tra:Courier_Incoming
  OF 17 !tra:Courier_Outgoing
  OF 18 !tra:Use_Contact_Name
  OF 19 !tra:VAT_Number
  OF 20 !tra:Use_Delivery_Address
  OF 21 !tra:Use_Collection_Address
  OF 22 !tra:UseCustDespAdd
  OF 23 !tra:Allow_Loan
  OF 24 !tra:Allow_Exchange
  OF 25 !tra:Force_Fault_Codes
  OF 26 !tra:Despatch_Invoiced_Jobs
  OF 27 !tra:Despatch_Paid_Jobs
  OF 28 !tra:Print_Despatch_Notes
  OF 29 !tra:Print_Retail_Despatch_Note
  OF 30 !tra:Print_Retail_Picking_Note
  OF 31 !tra:Despatch_Note_Per_Item
  OF 32 !tra:Skip_Despatch
  OF 33 !tra:Summary_Despatch_Notes
  OF 34 !tra:Exchange_Stock_Type
  OF 35 !tra:Loan_Stock_Type
  OF 36 !tra:Courier_Cost
  OF 37 !tra:Force_Estimate
  OF 38 !tra:Estimate_If_Over
  OF 39 !tra:Turnaround_Time
  OF 40 !tra:Retail_Payment_Type
  OF 41 !tra:Retail_Price_Structure
  OF 42 !tra:Use_Customer_Address
  OF 43 !tra:Invoice_Customer_Address
  OF 44 !tra:ZeroChargeable
  OF 45 !tra:RefurbCharge
  OF 46 !tra:ChargeType
  OF 47 !tra:WarChargeType
  OF 48 !tra:ExchangeAcc
  OF 49 !tra:ExcludeBouncer
  OF 50 !tra:RetailZeroParts
  OF 51 !tra:HideDespAdd
  OF 52 !tra:RecordNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step52)
  SELF.FQ.SortField = SELF.BC.AddSortOrder()
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(?tra:Account_Number,tra:Account_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,tra:Company_Name,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(,tra:Labour_VAT_Code,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,tra:Parts_VAT_Code,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(,tra:Retail_VAT_Code,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(,tra:Use_Sub_Accounts,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(,tra:Invoice_Sub_Accounts,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(,tra:Stop_Account,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step11.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator11)
      Xplore1Locator11.Init(,tra:Price_Despatch,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step12.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator12)
      Xplore1Locator12.Init(,tra:Price_Retail_Despatch,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step13.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator13)
      Xplore1Locator13.Init(,tra:Print_Despatch_Complete,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step14.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator14)
      Xplore1Locator14.Init(,tra:Print_Despatch_Despatch,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step15.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator15)
      Xplore1Locator15.Init(,tra:Standard_Repair_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step16.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator16)
      Xplore1Locator16.Init(,tra:Courier_Incoming,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step17.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator17)
      Xplore1Locator17.Init(,tra:Courier_Outgoing,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step18.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator18)
      Xplore1Locator18.Init(,tra:Use_Contact_Name,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step19.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator19)
      Xplore1Locator19.Init(,tra:VAT_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step20.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator20)
      Xplore1Locator20.Init(,tra:Use_Delivery_Address,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step21.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator21)
      Xplore1Locator21.Init(,tra:Use_Collection_Address,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step22.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator22)
      Xplore1Locator22.Init(,tra:UseCustDespAdd,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step23.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator23)
      Xplore1Locator23.Init(,tra:Allow_Loan,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step24.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator24)
      Xplore1Locator24.Init(,tra:Allow_Exchange,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step25.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator25)
      Xplore1Locator25.Init(,tra:Force_Fault_Codes,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step26.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator26)
      Xplore1Locator26.Init(,tra:Despatch_Invoiced_Jobs,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step27.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator27)
      Xplore1Locator27.Init(,tra:Despatch_Paid_Jobs,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step28.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator28)
      Xplore1Locator28.Init(,tra:Print_Despatch_Notes,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step29.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator29)
      Xplore1Locator29.Init(,tra:Print_Retail_Despatch_Note,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step30.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator30)
      Xplore1Locator30.Init(,tra:Print_Retail_Picking_Note,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step31.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator31)
      Xplore1Locator31.Init(,tra:Despatch_Note_Per_Item,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step32.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator32)
      Xplore1Locator32.Init(,tra:Skip_Despatch,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step33.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator33)
      Xplore1Locator33.Init(,tra:Summary_Despatch_Notes,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step34.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator34)
      Xplore1Locator34.Init(,tra:Exchange_Stock_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step35.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator35)
      Xplore1Locator35.Init(,tra:Loan_Stock_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step36.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator36)
      Xplore1Locator36.Init(,tra:Courier_Cost,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step37.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator37)
      Xplore1Locator37.Init(,tra:Force_Estimate,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step38.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator38)
      Xplore1Locator38.Init(,tra:Estimate_If_Over,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step39.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator39)
      Xplore1Locator39.Init(,tra:Turnaround_Time,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step40.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator40)
      Xplore1Locator40.Init(,tra:Retail_Payment_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step41.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator41)
      Xplore1Locator41.Init(,tra:Retail_Price_Structure,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step42.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator42)
      Xplore1Locator42.Init(,tra:Use_Customer_Address,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step43.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator43)
      Xplore1Locator43.Init(,tra:Invoice_Customer_Address,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step44.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator44)
      Xplore1Locator44.Init(,tra:ZeroChargeable,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step45.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator45)
      Xplore1Locator45.Init(,tra:RefurbCharge,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step46.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator46)
      Xplore1Locator46.Init(,tra:ChargeType,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step47.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator47)
      Xplore1Locator47.Init(,tra:WarChargeType,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step48.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator48)
      Xplore1Locator48.Init(,tra:ExchangeAcc,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step49.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator49)
      Xplore1Locator49.Init(,tra:ExcludeBouncer,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step50.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator50)
      Xplore1Locator50.Init(,tra:RetailZeroParts,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step51.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator51)
      Xplore1Locator51.Init(,tra:HideDespAdd,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step52.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator52)
      Xplore1Locator52.Init(,tra:RecordNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(TRADEACC)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('tra:Account_Number')         !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Company_Name')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Company Name')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Company Name')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Telephone_Number')       !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Telephone Number')           !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Telephone Number')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tra:Fax_Number')             !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Fax Number')                 !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Fax Number')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tra:Labour_VAT_Code')        !Field Name
                SHORT(8)                              !Default Column Width
                PSTRING('Labour VAT Code')            !Header
                PSTRING('@s2')                        !Picture
                PSTRING('Labour VAT Code')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Parts_VAT_Code')         !Field Name
                SHORT(8)                              !Default Column Width
                PSTRING('Labour VAT Code')            !Header
                PSTRING('@s2')                        !Picture
                PSTRING('Labour VAT Code')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Retail_VAT_Code')        !Field Name
                SHORT(8)                              !Default Column Width
                PSTRING('Labour VAT Code')            !Header
                PSTRING('@s2')                        !Picture
                PSTRING('Labour VAT Code')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Use_Sub_Accounts')       !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Sub Accounts')           !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Sub Accounts')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Invoice_Sub_Accounts')   !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Invoice Sub Accounts')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Invoice Sub Accounts')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Stop_Account')           !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Stop Account')               !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Stop Account')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Price_Despatch')         !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Price Despatch Notes')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Price Despatch Notes')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Price_Retail_Despatch')  !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Price Retail Despatch Notes') !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Price Retail Despatch Notes') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Print_Despatch_Complete') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Print Despatch Complete')    !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Print Despatch Note At Completion') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Print_Despatch_Despatch') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Print Despatch Despatch')    !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Print Despatch Note At Despatch') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Standard_Repair_Type')   !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Standard Repair Type')       !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Standard Repair Type')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Courier_Incoming')       !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Courier Incoming')           !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Courier Incoming')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Courier_Outgoing')       !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Courier Outgoing')           !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Courier Outgoing')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Use_Contact_Name')       !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Contact Name')           !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Contact Name')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:VAT_Number')             !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('V.A.T. Number')              !Header
                PSTRING('@s30')                       !Picture
                PSTRING('V.A.T. Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Use_Delivery_Address')   !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Delivery Address')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Delivery Address')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Use_Collection_Address') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Delivery Address')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Delivery Address')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:UseCustDespAdd')         !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Cust Desp Add')          !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Customer Address As Despatch Address') !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Allow_Loan')             !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Allow Loan')                 !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Allow Loan')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Allow_Exchange')         !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Allow Exchange')             !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Allow Exchange')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Force_Fault_Codes')      !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Force Fault Codes')          !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Force Fault Codes')          !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Despatch_Invoiced_Jobs') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Despatch Invoiced Jobs')     !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Despatch Invoiced Jobs')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Despatch_Paid_Jobs')     !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Despatch Paid Jobs')         !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Despatch Paid Jobs')         !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Print_Despatch_Notes')   !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Print Despatch Notes')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Print Despatch Notes')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Print_Retail_Despatch_Note') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Print Retail Despatch Note') !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Print Retail Despatch Note') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Print_Retail_Picking_Note') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Print Retail Picking Note')  !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Print Retail Picking Note')  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Despatch_Note_Per_Item') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Despatch Note Per Item')     !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Despatch Note Per Item')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Skip_Despatch')          !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Skip Despatch')              !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Batch Despatch')         !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Summary_Despatch_Notes') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Summary Despatch Notes')     !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Summary Despatch Notes')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Exchange_Stock_Type')    !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Exchange Stock Type')        !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Exchange Stock Type')        !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Loan_Stock_Type')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Loan Stock Type')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Loan Stock Type')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Courier_Cost')           !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Courier Cost')               !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Courier Cost')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Force_Estimate')         !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Force Estimate')             !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Force Estimate')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Estimate_If_Over')       !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Estimate If Over')           !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Estimate If Over')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Turnaround_Time')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Turnaround Time')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Turnaround Time')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Retail_Payment_Type')    !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Retail Sales Payment Type')  !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Retail Sales Payment Type')  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Retail_Price_Structure') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Retail Price Structure')     !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Retail Price Structure')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Use_Customer_Address')   !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Customer Address')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Customer Address')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Invoice_Customer_Address') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Customer Address')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Customer Address')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:ZeroChargeable')         !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Zero Chargeable')            !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Don''t show Chargeable Costs') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:RefurbCharge')           !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Refurb Charge')              !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Refurb Charge')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:ChargeType')             !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Charge Type')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Charge Type')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:WarChargeType')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('War Charge Type')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('War Charge Type')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:ExchangeAcc')            !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Exchange Acc')               !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Account used for repairing exchange units') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:ExcludeBouncer')         !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Exclude From Bouncer')       !Header
                PSTRING('@n1')                        !Picture
                PSTRING('Exclude From Bouncer')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:RetailZeroParts')        !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Zero Value Parts')           !Header
                PSTRING('@n1')                        !Picture
                PSTRING('Zero Value Parts')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:HideDespAdd')            !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Hide Address On Despatch Note') !Header
                PSTRING('@n1')                        !Picture
                PSTRING('Hide Address On Despatch Note') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:RecordNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Entry Long')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(52)
!--------------------------
XpSortFields    GROUP,STATIC
                PSTRING('tra:Telephone_Number')       !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                PSTRING('tra:Fax_Number')             !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                END
XpSortDim       SHORT(2)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)

