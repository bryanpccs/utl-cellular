

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02057.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSIDModels PROCEDURE (manufacturer, modelNo, turnaroundDaysRetail, turnaroundDaysExch) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Manufacturer     STRING(30)
tmp:ModelNo          STRING(30)
tmp:DefaultTurnaroundDaysRetail LONG
tmp:DefaultTurnaroundDaysExch LONG
tmp:ModelSpecificException BYTE
tmp:TurnaroundDaysRetail LONG
tmp:TurnaroundDaysExch LONG
tmp:ActiveTextRetail BYTE
tmp:TextRetail       STRING(250)
tmp:ActiveTextExch   BYTE
tmp:TextExch         STRING(250)
window               WINDOW('Turnaround Exceptions'),AT(,,232,176),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,142),USE(?Sheet1),SPREAD
                         TAB('Model Details'),USE(?Tab1)
                           PROMPT('Model Number'),AT(8,36),USE(?tmp:ModelNo:Prompt),TRN
                           ENTRY(@s30),AT(92,36,124,10),USE(tmp:ModelNo),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           STRING('Default Turnaround Days'),AT(8,50),USE(?StrDefault),TRN,FONT('Arial',9,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Repair'),AT(8,64),USE(?tmp:DefaultTurnaroundDaysRetail:Prompt),TRN
                           ENTRY(@n-14),AT(92,64,60,10),USE(tmp:DefaultTurnaroundDaysRetail),SKIP,RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                           PROMPT('Exchange'),AT(8,80),USE(?tmp:DefaultTurnaroundDaysExch:Prompt),TRN
                           ENTRY(@n-14),AT(92,80,60,10),USE(tmp:DefaultTurnaroundDaysExch),SKIP,RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                           PROMPT('Manufacturer'),AT(8,20),USE(?tmp:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(92,20,124,10),USE(tmp:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           CHECK('Model Specific Exceptions'),AT(92,96),USE(tmp:ModelSpecificException),TRN,VALUE('1','0')
                           BUTTON,AT(156,112,10,10),USE(?BtnRetailText),SKIP,DISABLE,LEFT,ICON('List3.ico')
                           PROMPT('Repair'),AT(8,112),USE(?tmp:TurnaroundDaysRetail:Prompt),TRN
                           ENTRY(@n3),AT(92,112,60,10),USE(tmp:TurnaroundDaysRetail),SKIP,RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                           PROMPT('Exchange'),AT(8,128),USE(?tmp:TurnaroundDaysExch:Prompt),TRN
                           ENTRY(@n3),AT(92,128,60,10),USE(tmp:TurnaroundDaysExch),SKIP,RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                           BUTTON,AT(156,128,10,10),USE(?BtnExchangeText),SKIP,DISABLE,LEFT,ICON('List3.ico')
                         END
                       END
                       PANEL,AT(4,150,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(112,154,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(168,154,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:ModelNo:Prompt{prop:FontColor} = -1
    ?tmp:ModelNo:Prompt{prop:Color} = 15066597
    If ?tmp:ModelNo{prop:ReadOnly} = True
        ?tmp:ModelNo{prop:FontColor} = 65793
        ?tmp:ModelNo{prop:Color} = 15066597
    Elsif ?tmp:ModelNo{prop:Req} = True
        ?tmp:ModelNo{prop:FontColor} = 65793
        ?tmp:ModelNo{prop:Color} = 8454143
    Else ! If ?tmp:ModelNo{prop:Req} = True
        ?tmp:ModelNo{prop:FontColor} = 65793
        ?tmp:ModelNo{prop:Color} = 16777215
    End ! If ?tmp:ModelNo{prop:Req} = True
    ?tmp:ModelNo{prop:Trn} = 0
    ?tmp:ModelNo{prop:FontStyle} = font:Bold
    ?StrDefault{prop:FontColor} = -1
    ?StrDefault{prop:Color} = 15066597
    ?tmp:DefaultTurnaroundDaysRetail:Prompt{prop:FontColor} = -1
    ?tmp:DefaultTurnaroundDaysRetail:Prompt{prop:Color} = 15066597
    If ?tmp:DefaultTurnaroundDaysRetail{prop:ReadOnly} = True
        ?tmp:DefaultTurnaroundDaysRetail{prop:FontColor} = 65793
        ?tmp:DefaultTurnaroundDaysRetail{prop:Color} = 15066597
    Elsif ?tmp:DefaultTurnaroundDaysRetail{prop:Req} = True
        ?tmp:DefaultTurnaroundDaysRetail{prop:FontColor} = 65793
        ?tmp:DefaultTurnaroundDaysRetail{prop:Color} = 8454143
    Else ! If ?tmp:DefaultTurnaroundDaysRetail{prop:Req} = True
        ?tmp:DefaultTurnaroundDaysRetail{prop:FontColor} = 65793
        ?tmp:DefaultTurnaroundDaysRetail{prop:Color} = 16777215
    End ! If ?tmp:DefaultTurnaroundDaysRetail{prop:Req} = True
    ?tmp:DefaultTurnaroundDaysRetail{prop:Trn} = 0
    ?tmp:DefaultTurnaroundDaysRetail{prop:FontStyle} = font:Bold
    ?tmp:DefaultTurnaroundDaysExch:Prompt{prop:FontColor} = -1
    ?tmp:DefaultTurnaroundDaysExch:Prompt{prop:Color} = 15066597
    If ?tmp:DefaultTurnaroundDaysExch{prop:ReadOnly} = True
        ?tmp:DefaultTurnaroundDaysExch{prop:FontColor} = 65793
        ?tmp:DefaultTurnaroundDaysExch{prop:Color} = 15066597
    Elsif ?tmp:DefaultTurnaroundDaysExch{prop:Req} = True
        ?tmp:DefaultTurnaroundDaysExch{prop:FontColor} = 65793
        ?tmp:DefaultTurnaroundDaysExch{prop:Color} = 8454143
    Else ! If ?tmp:DefaultTurnaroundDaysExch{prop:Req} = True
        ?tmp:DefaultTurnaroundDaysExch{prop:FontColor} = 65793
        ?tmp:DefaultTurnaroundDaysExch{prop:Color} = 16777215
    End ! If ?tmp:DefaultTurnaroundDaysExch{prop:Req} = True
    ?tmp:DefaultTurnaroundDaysExch{prop:Trn} = 0
    ?tmp:DefaultTurnaroundDaysExch{prop:FontStyle} = font:Bold
    ?tmp:Manufacturer:Prompt{prop:FontColor} = -1
    ?tmp:Manufacturer:Prompt{prop:Color} = 15066597
    If ?tmp:Manufacturer{prop:ReadOnly} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 15066597
    Elsif ?tmp:Manufacturer{prop:Req} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 8454143
    Else ! If ?tmp:Manufacturer{prop:Req} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 16777215
    End ! If ?tmp:Manufacturer{prop:Req} = True
    ?tmp:Manufacturer{prop:Trn} = 0
    ?tmp:Manufacturer{prop:FontStyle} = font:Bold
    ?tmp:ModelSpecificException{prop:Font,3} = -1
    ?tmp:ModelSpecificException{prop:Color} = 15066597
    ?tmp:ModelSpecificException{prop:Trn} = 0
    ?tmp:TurnaroundDaysRetail:Prompt{prop:FontColor} = -1
    ?tmp:TurnaroundDaysRetail:Prompt{prop:Color} = 15066597
    If ?tmp:TurnaroundDaysRetail{prop:ReadOnly} = True
        ?tmp:TurnaroundDaysRetail{prop:FontColor} = 65793
        ?tmp:TurnaroundDaysRetail{prop:Color} = 15066597
    Elsif ?tmp:TurnaroundDaysRetail{prop:Req} = True
        ?tmp:TurnaroundDaysRetail{prop:FontColor} = 65793
        ?tmp:TurnaroundDaysRetail{prop:Color} = 8454143
    Else ! If ?tmp:TurnaroundDaysRetail{prop:Req} = True
        ?tmp:TurnaroundDaysRetail{prop:FontColor} = 65793
        ?tmp:TurnaroundDaysRetail{prop:Color} = 16777215
    End ! If ?tmp:TurnaroundDaysRetail{prop:Req} = True
    ?tmp:TurnaroundDaysRetail{prop:Trn} = 0
    ?tmp:TurnaroundDaysRetail{prop:FontStyle} = font:Bold
    ?tmp:TurnaroundDaysExch:Prompt{prop:FontColor} = -1
    ?tmp:TurnaroundDaysExch:Prompt{prop:Color} = 15066597
    If ?tmp:TurnaroundDaysExch{prop:ReadOnly} = True
        ?tmp:TurnaroundDaysExch{prop:FontColor} = 65793
        ?tmp:TurnaroundDaysExch{prop:Color} = 15066597
    Elsif ?tmp:TurnaroundDaysExch{prop:Req} = True
        ?tmp:TurnaroundDaysExch{prop:FontColor} = 65793
        ?tmp:TurnaroundDaysExch{prop:Color} = 8454143
    Else ! If ?tmp:TurnaroundDaysExch{prop:Req} = True
        ?tmp:TurnaroundDaysExch{prop:FontColor} = 65793
        ?tmp:TurnaroundDaysExch{prop:Color} = 16777215
    End ! If ?tmp:TurnaroundDaysExch{prop:Req} = True
    ?tmp:TurnaroundDaysExch{prop:Trn} = 0
    ?tmp:TurnaroundDaysExch{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Display:Exceptions routine

    if tmp:ModelSpecificException
        ?tmp:TurnaroundDaysRetail{Prop:Skip} = false
        ?tmp:TurnaroundDaysRetail{Prop:ReadOnly} = false
        ?tmp:TurnaroundDaysExch{Prop:Skip} = false
        ?tmp:TurnaroundDaysExch{Prop:ReadOnly} = false
        ?BtnRetailText{Prop:Disable} = false
        ?BtnExchangeText{Prop:Disable} = false
    else
        ?tmp:TurnaroundDaysRetail{Prop:Skip} = true
        ?tmp:TurnaroundDaysRetail{Prop:ReadOnly} = true
        ?tmp:TurnaroundDaysExch{Prop:Skip} = true
        ?tmp:TurnaroundDaysExch{Prop:ReadOnly} = true
        ?BtnRetailText{Prop:Disable} = true
        ?BtnExchangeText{Prop:Disable} = true
        tmp:TurnaroundDaysRetail = 0
        tmp:TurnaroundDaysExch = 0
    end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSIDModels',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:Manufacturer',tmp:Manufacturer,'UpdateSIDModels',1)
    SolaceViewVars('tmp:ModelNo',tmp:ModelNo,'UpdateSIDModels',1)
    SolaceViewVars('tmp:DefaultTurnaroundDaysRetail',tmp:DefaultTurnaroundDaysRetail,'UpdateSIDModels',1)
    SolaceViewVars('tmp:DefaultTurnaroundDaysExch',tmp:DefaultTurnaroundDaysExch,'UpdateSIDModels',1)
    SolaceViewVars('tmp:ModelSpecificException',tmp:ModelSpecificException,'UpdateSIDModels',1)
    SolaceViewVars('tmp:TurnaroundDaysRetail',tmp:TurnaroundDaysRetail,'UpdateSIDModels',1)
    SolaceViewVars('tmp:TurnaroundDaysExch',tmp:TurnaroundDaysExch,'UpdateSIDModels',1)
    SolaceViewVars('tmp:ActiveTextRetail',tmp:ActiveTextRetail,'UpdateSIDModels',1)
    SolaceViewVars('tmp:TextRetail',tmp:TextRetail,'UpdateSIDModels',1)
    SolaceViewVars('tmp:ActiveTextExch',tmp:ActiveTextExch,'UpdateSIDModels',1)
    SolaceViewVars('tmp:TextExch',tmp:TextExch,'UpdateSIDModels',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNo:Prompt;  SolaceCtrlName = '?tmp:ModelNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNo;  SolaceCtrlName = '?tmp:ModelNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StrDefault;  SolaceCtrlName = '?StrDefault';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DefaultTurnaroundDaysRetail:Prompt;  SolaceCtrlName = '?tmp:DefaultTurnaroundDaysRetail:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DefaultTurnaroundDaysRetail;  SolaceCtrlName = '?tmp:DefaultTurnaroundDaysRetail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DefaultTurnaroundDaysExch:Prompt;  SolaceCtrlName = '?tmp:DefaultTurnaroundDaysExch:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DefaultTurnaroundDaysExch;  SolaceCtrlName = '?tmp:DefaultTurnaroundDaysExch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Manufacturer:Prompt;  SolaceCtrlName = '?tmp:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Manufacturer;  SolaceCtrlName = '?tmp:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelSpecificException;  SolaceCtrlName = '?tmp:ModelSpecificException';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BtnRetailText;  SolaceCtrlName = '?BtnRetailText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TurnaroundDaysRetail:Prompt;  SolaceCtrlName = '?tmp:TurnaroundDaysRetail:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TurnaroundDaysRetail;  SolaceCtrlName = '?tmp:TurnaroundDaysRetail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TurnaroundDaysExch:Prompt;  SolaceCtrlName = '?tmp:TurnaroundDaysExch:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TurnaroundDaysExch;  SolaceCtrlName = '?tmp:TurnaroundDaysExch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BtnExchangeText;  SolaceCtrlName = '?BtnExchangeText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSIDModels')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSIDModels')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:ModelNo:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SIDMODTT.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ! After Opening Window
  
  tmp:Manufacturer = manufacturer
  tmp:ModelNo = modelNo
  tmp:DefaultTurnaroundDaysRetail = turnaroundDaysRetail
  tmp:DefaultTurnaroundDaysExch = turnaroundDaysExch
  
  Access:SIDMODTT.ClearKey(smt:ModelNoKey)
  smt:ModelNo = modelNo
  if Access:SIDMODTT.Fetch(smt:ModelNoKey) = Level:Benign
      tmp:ModelSpecificException = smt:TurnaroundException
      tmp:TurnaroundDaysRetail = smt:TurnaroundDaysRetail
      tmp:TurnaroundDaysExch = smt:TurnaroundDaysExch
      tmp:ActiveTextRetail = smt:ActiveTextRetail
      tmp:TextRetail = smt:TextRetail
      tmp:ActiveTextExch = smt:ActiveTextExch
      tmp:TextExch = smt:TextExch
  end
  
  if tmp:ModelSpecificException = true
      if tmp:TurnaroundDaysRetail = 0 then tmp:TurnaroundDaysRetail = 1.
      if tmp:TurnaroundDaysExch = 0 then tmp:TurnaroundDaysExch = 1.
  end
  
  do Display:Exceptions
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SIDMODTT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSIDModels',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?tmp:ModelSpecificException
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ModelSpecificException, Accepted)
      do Display:Exceptions
      do RecolourWindow
      display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ModelSpecificException, Accepted)
    OF ?BtnRetailText
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnRetailText, Accepted)
      UpdateSIDModelExceptionText('R', tmp:ModelNo, tmp:ActiveTextRetail, tmp:TextRetail)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnRetailText, Accepted)
    OF ?BtnExchangeText
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnExchangeText, Accepted)
      UpdateSIDModelExceptionText('E', tmp:ModelNo, tmp:ActiveTextExch, tmp:TextExch)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnExchangeText, Accepted)
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      ! OK Button
      
      if tmp:ModelSpecificException
          if tmp:TurnaroundDaysRetail = 0
              select(?tmp:TurnaroundDaysRetail)
              cycle
          end
          if tmp:TurnaroundDaysExch = 0
              select(?tmp:TurnaroundDaysExch)
              cycle
          end
      else
          tmp:TurnaroundDaysRetail = 0
          tmp:TurnaroundDaysExch = 0
      end
      
      Access:SIDMODTT.ClearKey(smt:ModelNoKey)
      smt:ModelNo = modelNo
      if Access:SIDMODTT.Fetch(smt:ModelNoKey) = Level:Benign
          smt:TurnaroundException = tmp:ModelSpecificException
          smt:TurnaroundDaysRetail = tmp:TurnaroundDaysRetail
          smt:TurnaroundDaysExch = tmp:TurnaroundDaysExch
          smt:ActiveTextRetail = tmp:ActiveTextRetail
          smt:TextRetail = tmp:TextRetail
          smt:ActiveTextExch = tmp:ActiveTextExch
          smt:TextExch = tmp:TextExch
          Access:SIDMODTT.Update()
      else
          if Access:SIDMODTT.PrimeRecord() = Level:Benign
              smt:Manufacturer = manufacturer
              smt:ModelNo = modelNo
              smt:TurnaroundException = tmp:ModelSpecificException
              smt:TurnaroundDaysRetail = tmp:TurnaroundDaysRetail
              smt:TurnaroundDaysExch = tmp:TurnaroundDaysExch
              smt:ActiveTextRetail = tmp:ActiveTextRetail
              smt:TextRetail = tmp:TextRetail
              smt:ActiveTextExch = tmp:ActiveTextExch
              smt:TextExch = tmp:TextExch
              if Access:SIDMODTT.Update() <> Level:Benign
                  Access:SIDMODTT.CancelAutoInc()
              end
          end
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSIDModels')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

