

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02029.INC'),ONCE        !Local module procedure declarations
                     END


Rapid_Exchange_Unit PROCEDURE                         !Generated from procedure template - Window

tmp:StockType        STRING(30)
save_xch_ali_id      USHORT,AUTO
tmp:count            LONG
tmp:UnitQueue        QUEUE,PRE(untque)
esn                  STRING(16)
number               LONG
                     END
tmp:ModelNumber      STRING(30)
tmp:Location         STRING(30)
tmp:ShelfLocation    STRING(30)
tmp:Manufacturer     STRING(30)
tmp:esn              STRING(16)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:MSN              STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:StockType
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ModelNumber
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?tmp:Location
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ShelfLocation
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?tmp:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
FDCB4::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
FDCB5::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB6::View:FileDropCombo VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:Site_Location)
                     END
FDCB7::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
window               WINDOW('Rapid Exchange Item Inserting'),AT(,,303,267),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,296,232),USE(?Sheet1),BELOW,SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('Common Fields'),AT(92,32),USE(?Prompt2),FONT(,,,FONT:bold)
                           PROMPT('Insert the fields that will be common to all the inserted Exchange Units.'),AT(92,44,204,24),USE(?Prompt3)
                           PROMPT('Manufacturer'),AT(100,100),USE(?Prompt4)
                           COMBO(@s30),AT(168,84,124,10),USE(tmp:StockType),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           COMBO(@s30),AT(168,100,124,10),USE(tmp:Manufacturer),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4)
                           PROMPT('Stock Type'),AT(100,84),USE(?Prompt4:2)
                           PROMPT('Model Number'),AT(100,116),USE(?Prompt4:3)
                           COMBO(@s30),AT(168,116,124,10),USE(tmp:ModelNumber),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Location'),AT(100,132),USE(?Prompt4:4)
                           COMBO(@s30),AT(168,132,124,10),USE(tmp:Location),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           PROMPT('Shelf Location'),AT(100,148),USE(?Prompt4:5)
                           COMBO(@s30),AT(168,148,124,10),USE(tmp:ShelfLocation),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                         END
                         TAB('Tab 2'),USE(?Tab2),HIDE
                           PROMPT('Create New Units'),AT(92,32),USE(?Prompt10),FONT(,,,FONT:bold)
                           PROMPT('Insert an I.M.E.I. to insert the unit into the Exchange Stock.'),AT(92,44),USE(?Prompt9)
                           PROMPT('Units Successfully inserted.'),AT(96,116),USE(?Prompt9:2),FONT(,,,FONT:bold)
                           PROMPT('I.M.E.I. Number'),AT(96,76),USE(?tmp:esn:Prompt)
                           ENTRY(@s16),AT(168,76,124,10),USE(tmp:esn),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('M.S.N.'),AT(96,92),USE(?tmp:MSN:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(168,92,124,10),USE(tmp:MSN),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('M.'),TIP('M.'),UPR
                           LIST,AT(168,128,128,100),USE(?List1),VSCROLL,FORMAT('64L(2)|M~I.M.E.I. Number~@s16@'),FROM(tmp:UnitQueue)
                         END
                       END
                       IMAGE('wizard.gif'),AT(12,84),USE(?Image1)
                       BUTTON('Close'),AT(240,244,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       BUTTON('&Next'),AT(64,244,56,16),USE(?Next),LEFT,ICON(ICON:VCRfastforward)
                       PANEL,AT(4,240,296,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,244,56,16),USE(?back),DISABLE,LEFT,ICON(ICON:VCRrewind)
                       PROMPT('Inserting Exchange Units Wizard'),AT(8,8,172,16),USE(?Prompt1),FONT(,12,,FONT:bold)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?tmp:StockType{prop:ReadOnly} = True
        ?tmp:StockType{prop:FontColor} = 65793
        ?tmp:StockType{prop:Color} = 15066597
    Elsif ?tmp:StockType{prop:Req} = True
        ?tmp:StockType{prop:FontColor} = 65793
        ?tmp:StockType{prop:Color} = 8454143
    Else ! If ?tmp:StockType{prop:Req} = True
        ?tmp:StockType{prop:FontColor} = 65793
        ?tmp:StockType{prop:Color} = 16777215
    End ! If ?tmp:StockType{prop:Req} = True
    ?tmp:StockType{prop:Trn} = 0
    ?tmp:StockType{prop:FontStyle} = font:Bold
    If ?tmp:Manufacturer{prop:ReadOnly} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 15066597
    Elsif ?tmp:Manufacturer{prop:Req} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 8454143
    Else ! If ?tmp:Manufacturer{prop:Req} = True
        ?tmp:Manufacturer{prop:FontColor} = 65793
        ?tmp:Manufacturer{prop:Color} = 16777215
    End ! If ?tmp:Manufacturer{prop:Req} = True
    ?tmp:Manufacturer{prop:Trn} = 0
    ?tmp:Manufacturer{prop:FontStyle} = font:Bold
    ?Prompt4:2{prop:FontColor} = -1
    ?Prompt4:2{prop:Color} = 15066597
    ?Prompt4:3{prop:FontColor} = -1
    ?Prompt4:3{prop:Color} = 15066597
    If ?tmp:ModelNumber{prop:ReadOnly} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 15066597
    Elsif ?tmp:ModelNumber{prop:Req} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 8454143
    Else ! If ?tmp:ModelNumber{prop:Req} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 16777215
    End ! If ?tmp:ModelNumber{prop:Req} = True
    ?tmp:ModelNumber{prop:Trn} = 0
    ?tmp:ModelNumber{prop:FontStyle} = font:Bold
    ?Prompt4:4{prop:FontColor} = -1
    ?Prompt4:4{prop:Color} = 15066597
    If ?tmp:Location{prop:ReadOnly} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 15066597
    Elsif ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 8454143
    Else ! If ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 16777215
    End ! If ?tmp:Location{prop:Req} = True
    ?tmp:Location{prop:Trn} = 0
    ?tmp:Location{prop:FontStyle} = font:Bold
    ?Prompt4:5{prop:FontColor} = -1
    ?Prompt4:5{prop:Color} = 15066597
    If ?tmp:ShelfLocation{prop:ReadOnly} = True
        ?tmp:ShelfLocation{prop:FontColor} = 65793
        ?tmp:ShelfLocation{prop:Color} = 15066597
    Elsif ?tmp:ShelfLocation{prop:Req} = True
        ?tmp:ShelfLocation{prop:FontColor} = 65793
        ?tmp:ShelfLocation{prop:Color} = 8454143
    Else ! If ?tmp:ShelfLocation{prop:Req} = True
        ?tmp:ShelfLocation{prop:FontColor} = 65793
        ?tmp:ShelfLocation{prop:Color} = 16777215
    End ! If ?tmp:ShelfLocation{prop:Req} = True
    ?tmp:ShelfLocation{prop:Trn} = 0
    ?tmp:ShelfLocation{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?Prompt9:2{prop:FontColor} = -1
    ?Prompt9:2{prop:Color} = 15066597
    ?tmp:esn:Prompt{prop:FontColor} = -1
    ?tmp:esn:Prompt{prop:Color} = 15066597
    If ?tmp:esn{prop:ReadOnly} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 15066597
    Elsif ?tmp:esn{prop:Req} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 8454143
    Else ! If ?tmp:esn{prop:Req} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 16777215
    End ! If ?tmp:esn{prop:Req} = True
    ?tmp:esn{prop:Trn} = 0
    ?tmp:esn{prop:FontStyle} = font:Bold
    ?tmp:MSN:Prompt{prop:FontColor} = -1
    ?tmp:MSN:Prompt{prop:Color} = 15066597
    If ?tmp:MSN{prop:ReadOnly} = True
        ?tmp:MSN{prop:FontColor} = 65793
        ?tmp:MSN{prop:Color} = 15066597
    Elsif ?tmp:MSN{prop:Req} = True
        ?tmp:MSN{prop:FontColor} = 65793
        ?tmp:MSN{prop:Color} = 8454143
    Else ! If ?tmp:MSN{prop:Req} = True
        ?tmp:MSN{prop:FontColor} = 65793
        ?tmp:MSN{prop:Color} = 16777215
    End ! If ?tmp:MSN{prop:Req} = True
    ?tmp:MSN{prop:Trn} = 0
    ?tmp:MSN{prop:FontStyle} = font:Bold
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
AddUnit     Routine

    error# = 0

    setcursor(cursor:wait)
    save_xch_ali_id = access:exchange_alias.savefile()
    access:exchange_alias.clearkey(xch_ali:esn_only_key)
    xch_ali:esn = tmp:esn
    set(xch_ali:esn_only_key,xch_ali:esn_only_key)
    loop
        if access:exchange_alias.next()
           break
        end !if
        if xch_ali:esn <> tmp:esn      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        Case MessageEx('This I.M.E.I. Number already exists in Exchange Stock.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
        error# = 1
    end !loop
    access:exchange_alias.restorefile(save_xch_ali_id)
    setcursor()

    If error# = 0
        access:loan.clearkey(loa:esn_key)
        loa:esn = tmp:esn
        if access:loan.fetch(loa:esn_key) = Level:Benign
            Case MessageEx('This I.M.E.I. Number has already been entered as a Loan Stock Item.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
            error# = 1
        End!if access:loan.fetch(loa:esn_key) = Level:Benign
    End!If error# = 0
    If error# = 0
        access:jobs_alias.clearkey(job_ali:esn_key)
        job_ali:esn = tmp:esn
        if access:jobs_alias.fetch(job_ali:esn_key) = Level:Benign
           If job_ali:date_completed = ''
                error# = 1
                Case MessageEx('You are attempting to insert an Exchange Unit with the same I.M.E.I. Number as job number '&clip(job_ali:ref_number)&'.<13,10><13,10>You must use Rapid Job Update if you wish to return this unit to Exchange Stock.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
           End!If job:date_completed <> ''
        end!if access:jobs.fetch(job:esn_key) = Level:Benign
    end!IF error# = 0
    If error# = 0
        If CheckLength('IMEI',tmp:ModelNumber,tmp:ESN)
            Error# = 1
        End !If CheckLength('IMEI',tmp:ModelNumber,tmp:ESN)
        If ?tmp:MSN{prop:Hide} = 0 And Error# = 0
            If CheckLength('MSN',tmp:ModelNumber,tmp:MSN)
                Error# = 1
            End !If CheckLength('MSN',tmp:ModelNumber,tmp:MSN)
        End !If ?tmp:MSN{prop:Hide} = 0
!
!        access:modelnum.clearkey(mod:model_number_key)
!        mod:model_number = tmp:modelnumber
!        if access:modelnum.fetch(mod:model_number_key) = Level:benign
!            If Len(Clip(tmp:esn)) < mod:esn_length_from Or Len(Clip(tmp:esn)) > mod:esn_length_to
!                Case MessageEx('The E.S.N. / I.M.E.I. entered should be between '&clip(mod:esn_length_from)&' and '&clip(mod:esn_length_to)&' characters in length.','ServiceBase 2000',|
!                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                    Of 1 ! &OK Button
!                End!Case MessageEx
!                error# = 1
!            End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
!    !        If man:use_msn = 'YES'
!    !            If len(Clip(xch:msn)) < mod:msn_length_from Or len(Clip(xch:msn)) > mod:msn_length_to
!    !                Case MessageEx('The M.S.N. entered should be between '&clip(mod:msn_length_from)&' and '&Clip(mod:msn_length_to)&' characters in length.','ServiceBase 2000',|
!    !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!    !                    Of 1 ! &OK Button
!    !                End!Case MessageEx
!    !                error# = 1
!    !            End!If len(job:msn) < mod:msn_length_from Or len(job:msn) > mod:msn_length_to
!    !        End!If man:use_msn = 'YES'
!        end !if access:modelnum.fetch(mod:model_number_key) = Level:benign
    End!IF error# = 0
    If error# = 1
        Select(?tmp:esn)
    Else
        get(exchange,0)
        if access:exchange.primerecord() = Level:Benign
            xch:model_number   = tmp:modelnumber
            xch:manufacturer   = tmp:manufacturer
            xch:esn            = tmp:esn
            If ?tmp:MSN{prop:Hide} = 0
                xch:msn            = tmp:MSN
            End !If tmp:MSN{prop:Hide} = 0
            xch:location       = tmp:location
            xch:shelf_location = tmp:shelflocation
            xch:date_booked    = Today()
            xch:available      = 'AVL'
            xch:stock_type     = tmp:stocktype
            if access:exchange.tryinsert()
                access:exchange.cancelautoinc()
            end
            untque:esn  = tmp:esn
            tmp:count   += 1
            untque:number = tmp:count
            Add(tmp:unitqueue)
            Sort(tmp:UnitQueue,-untque:number)
            tmp:esn = ''
            tmp:MSN = ''
            Select(?tmp:esn)
            Display()
        End!if access:exchange.primerecord() = Level:Benign
    End






SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Rapid_Exchange_Unit',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:StockType',tmp:StockType,'Rapid_Exchange_Unit',1)
    SolaceViewVars('save_xch_ali_id',save_xch_ali_id,'Rapid_Exchange_Unit',1)
    SolaceViewVars('tmp:count',tmp:count,'Rapid_Exchange_Unit',1)
    SolaceViewVars('tmp:UnitQueue:esn',tmp:UnitQueue:esn,'Rapid_Exchange_Unit',1)
    SolaceViewVars('tmp:UnitQueue:number',tmp:UnitQueue:number,'Rapid_Exchange_Unit',1)
    SolaceViewVars('tmp:ModelNumber',tmp:ModelNumber,'Rapid_Exchange_Unit',1)
    SolaceViewVars('tmp:Location',tmp:Location,'Rapid_Exchange_Unit',1)
    SolaceViewVars('tmp:ShelfLocation',tmp:ShelfLocation,'Rapid_Exchange_Unit',1)
    SolaceViewVars('tmp:Manufacturer',tmp:Manufacturer,'Rapid_Exchange_Unit',1)
    SolaceViewVars('tmp:esn',tmp:esn,'Rapid_Exchange_Unit',1)
    SolaceViewVars('tmp:MSN',tmp:MSN,'Rapid_Exchange_Unit',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StockType;  SolaceCtrlName = '?tmp:StockType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Manufacturer;  SolaceCtrlName = '?tmp:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:2;  SolaceCtrlName = '?Prompt4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:3;  SolaceCtrlName = '?Prompt4:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber;  SolaceCtrlName = '?tmp:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:4;  SolaceCtrlName = '?Prompt4:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location;  SolaceCtrlName = '?tmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:5;  SolaceCtrlName = '?Prompt4:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ShelfLocation;  SolaceCtrlName = '?tmp:ShelfLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9:2;  SolaceCtrlName = '?Prompt9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn:Prompt;  SolaceCtrlName = '?tmp:esn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn;  SolaceCtrlName = '?tmp:esn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:MSN:Prompt;  SolaceCtrlName = '?tmp:MSN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:MSN;  SolaceCtrlName = '?tmp:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List1;  SolaceCtrlName = '?List1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Next;  SolaceCtrlName = '?Next';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?back;  SolaceCtrlName = '?back';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Rapid_Exchange_Unit')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Rapid_Exchange_Unit')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHANGE_ALIAS.Open
  Relate:JOBS_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  !For Development Purposes
  ?Sheet1{prop:wizard} = 1
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  ! support for CPCS
  FDCB3.Init(tmp:StockType,?tmp:StockType,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(stp:Use_Exchange_Key)
  FDCB3.SetFilter('Upper(stp:use_exchange) = ''YES''')
  FDCB3.AddField(stp:Stock_Type,FDCB3.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  FDCB4.Init(tmp:ModelNumber,?tmp:ModelNumber,Queue:FileDropCombo:1.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo:1
  FDCB4.AddSortOrder(mod:Manufacturer_Key)
  FDCB4.AddRange(mod:Manufacturer,tmp:Manufacturer)
  FDCB4.AddField(mod:Model_Number,FDCB4.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB5.Init(tmp:Location,?tmp:Location,Queue:FileDropCombo:2.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:2,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:2
  FDCB5.AddSortOrder(loc:Location_Key)
  FDCB5.AddField(loc:Location,FDCB5.Q.loc:Location)
  FDCB5.AddField(loc:RecordNumber,FDCB5.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB6.Init(tmp:ShelfLocation,?tmp:ShelfLocation,Queue:FileDropCombo:3.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo:3,Relate:LOCSHELF,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo:3
  FDCB6.AddSortOrder(los:Shelf_Location_Key)
  FDCB6.AddRange(los:Site_Location,tmp:Location)
  FDCB6.AddField(los:Shelf_Location,FDCB6.Q.los:Shelf_Location)
  FDCB6.AddField(los:Site_Location,FDCB6.Q.los:Site_Location)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB7.Init(tmp:Manufacturer,?tmp:Manufacturer,Queue:FileDropCombo:4.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:4,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:4
  FDCB7.AddSortOrder(man:Manufacturer_Key)
  FDCB7.AddField(man:Manufacturer,FDCB7.Q.man:Manufacturer)
  FDCB7.AddField(man:RecordNumber,FDCB7.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE_ALIAS.Close
    Relate:JOBS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Rapid_Exchange_Unit',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      FDCB4.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?tmp:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, Accepted)
      FDCB6.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, Accepted)
    OF ?tmp:esn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
      If ?tmp:MSN{prop:hide} = 1
          Do AddUnit
      End !?tmp:MSN{prop:hide} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
    OF ?tmp:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Accepted)
      Do AddUnit
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Accepted)
    OF ?Next
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Next, Accepted)
      Case Choice(?Sheet1)
          Of 1
              If tmp:stocktype = '' Or |
                  tmp:manufacturer = '' Or |
                  tmp:modelnumber = '' Or |
                  tmp:location    = '' Or |
                  tmp:shelflocation = ''
                  Case MessageEx('You must completed all the fields before you can continue.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              Else!If tmp:stocktype = '' Or |
                  ?tmp:MSN{prop:Hide} = 1
                  ?tmp:MSN:Prompt{prop:Hide} = 1
                  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                  man:Manufacturer    = tmp:Manufacturer
                  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                      !Found
                      If man:Use_MSN = 'YES'
                          ?tmp:MSN{prop:Hide} = 0
                          ?tmp:MSN:Prompt{prop:Hide} = 0
                      End !If man:Use_MSN = 'YES'
                  Else! If Access:MANUFACTURER.Tryfetch(man:Manufacturer_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:MANUFACTURER.Tryfetch(man:Manufacturer_Key) = Level:Benign
      
                  Unhide(?tab2)
                  Select(?Sheet1,2)
                  Disable(?Next)
                  Enable(?Back)
      
              End!If tmp:stocktype = '' Or |
      
          Of 2
      
      End!Case Choice(?Sheet1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Next, Accepted)
    OF ?back
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?back, Accepted)
      Case Choice(?Sheet1)
          Of 1
      
          Of 2
              hide(?tab2)
              Select(?Sheet1,1)
              Disable(?back)
              Enable(?Next)
      End!Case Choice(?Sheet1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?back, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Rapid_Exchange_Unit')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

