

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02025.INC'),ONCE        !Local module procedure declarations
                     END


UpdateTradeAccFaultCodes PROCEDURE                    !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
Filepath             STRING(255)
SaveTFO              USHORT
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW2::View:Browse    VIEW(TRAFAULO)
                       PROJECT(tfo:Field)
                       PROJECT(tfo:Description)
                       PROJECT(tfo:AccountNumber)
                       PROJECT(tfo:Field_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
tfo:Field              LIKE(tfo:Field)                !List box control field - type derived from field
tfo:Description        LIKE(tfo:Description)          !List box control field - type derived from field
tfo:AccountNumber      LIKE(tfo:AccountNumber)        !Primary key field - type derived from field
tfo:Field_Number       LIKE(tfo:Field_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::tfo:AccountNumber   LIKE(tfo:AccountNumber)
HK12::tfo:Field_Number    LIKE(tfo:Field_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
History::taf:Record  LIKE(taf:RECORD),STATIC
QuickWindow          WINDOW('Update Trade Account Fault Codes'),AT(,,528,250),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateTradeAccFaultCodes'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,216),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Account Number'),AT(8,20),USE(?TAF:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(taf:AccountNumber),DISABLE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Account Number'),TIP('Account Number'),REQ,UPR,READONLY
                           PROMPT('Field Number'),AT(8,36),USE(?TAF:Field_Number:Prompt)
                           SPIN(@n2),AT(84,36,64,10),USE(taf:Field_Number),RIGHT,FONT('Tahoma',8,,FONT:bold),REQ,UPR,RANGE(1,12),MSG('Fault Code Field Number')
                           PROMPT('Field Name'),AT(8,52),USE(?TAF:Field_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(taf:Field_Name),FONT('Tahoma',8,,FONT:bold),REQ,CAP
                           PROMPT('Field Type'),AT(8,68),USE(?TAF:Field_Type:Prompt),TRN
                           LIST,AT(84,68,64,10),USE(taf:Field_Type),LEFT(2),FONT('Tahoma',8,,FONT:bold),DROP(5),FROM('STRING|NUMBER|DATE')
                           CHECK('Lookup'),AT(84,84,70,8),USE(taf:Lookup),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:Silver),VALUE('YES','NO')
                           CHECK('Force Lookup'),AT(156,84,58,8),USE(taf:Force_Lookup),HIDE,VALUE('YES','NO')
                           CHECK('Compulsory At Completion'),AT(84,108),USE(taf:Compulsory),VALUE('YES','NO')
                           CHECK('Compulsory At Point Of Booking'),AT(84,96,132,8),USE(taf:Compulsory_At_Booking),VALUE('YES','NO')
                           CHECK('Replicate To Fault Description'),AT(84,124,132,8),USE(taf:ReplicateFault),MSG('Replicate To Fault Description Field'),TIP('Replicate To Fault Description Field'),VALUE('YES','NO')
                           CHECK('Replicate To Invoice Text'),AT(84,136,112,8),USE(taf:ReplicateInvoice),MSG('Replicate To Fault Description Field'),TIP('Replicate To Fault Description Field'),VALUE('YES','NO')
                           CHECK('Restrict Length'),AT(84,152,72,8),USE(taf:RestrictLength),MSG('Restrict Length'),TIP('Restrict Length'),VALUE('1','0')
                           GROUP,AT(4,160,208,40),USE(?Group2),HIDE
                             PROMPT('Length From'),AT(8,164),USE(?TAF:LengthFrom:Prompt),TRN
                             ENTRY(@s8),AT(84,164,64,10),USE(taf:LengthFrom),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Length From'),TIP('Length From'),UPR
                             PROMPT('Length To'),AT(8,180),USE(?TAF:LengthTo:Prompt),TRN
                             ENTRY(@s8),AT(84,180,64,10),USE(taf:LengthTo),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Length To'),TIP('Length To'),UPR
                           END
                         END
                       END
                       SHEET,AT(220,4,304,216),USE(?Sheet2),SPREAD
                         TAB('Lookup'),USE(?Tab4)
                           ENTRY(@s30),AT(224,20,124,10),USE(tfo:Field),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           LIST,AT(224,36,296,160),USE(?Browse:2),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Field~@s30@80L(2)|M~Description~@s60@'),FROM(Queue:Browse:2)
                           BUTTON('&Insert'),AT(352,200,56,16),USE(?Insert:3),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(408,200,56,16),USE(?Change:3),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(464,200,56,16),USE(?Delete:3),LEFT,ICON('delete.gif')
                           BUTTON('Import'),AT(224,200,56,16),USE(?ImportButton),LEFT,ICON(ICON:Open)
                         END
                       END
                       BUTTON('&OK'),AT(408,228,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(464,228,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,224,520,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW2::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
! Start Change BE021 BE(2/12/03)
IMPORT_FILE          FILE,DRIVER('BASIC'),OEM,PRE(IMPF),CREATE,THREAD
Record                   RECORD,PRE(IMP)
Code                        STRING(30)
Description                 STRING(60)
                         END
                     END
! End Change BE021 BE(2/12/03)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?TAF:AccountNumber:Prompt{prop:FontColor} = -1
    ?TAF:AccountNumber:Prompt{prop:Color} = 15066597
    If ?taf:AccountNumber{prop:ReadOnly} = True
        ?taf:AccountNumber{prop:FontColor} = 65793
        ?taf:AccountNumber{prop:Color} = 15066597
    Elsif ?taf:AccountNumber{prop:Req} = True
        ?taf:AccountNumber{prop:FontColor} = 65793
        ?taf:AccountNumber{prop:Color} = 8454143
    Else ! If ?taf:AccountNumber{prop:Req} = True
        ?taf:AccountNumber{prop:FontColor} = 65793
        ?taf:AccountNumber{prop:Color} = 16777215
    End ! If ?taf:AccountNumber{prop:Req} = True
    ?taf:AccountNumber{prop:Trn} = 0
    ?taf:AccountNumber{prop:FontStyle} = font:Bold
    ?TAF:Field_Number:Prompt{prop:FontColor} = -1
    ?TAF:Field_Number:Prompt{prop:Color} = 15066597
    If ?taf:Field_Number{prop:ReadOnly} = True
        ?taf:Field_Number{prop:FontColor} = 65793
        ?taf:Field_Number{prop:Color} = 15066597
    Elsif ?taf:Field_Number{prop:Req} = True
        ?taf:Field_Number{prop:FontColor} = 65793
        ?taf:Field_Number{prop:Color} = 8454143
    Else ! If ?taf:Field_Number{prop:Req} = True
        ?taf:Field_Number{prop:FontColor} = 65793
        ?taf:Field_Number{prop:Color} = 16777215
    End ! If ?taf:Field_Number{prop:Req} = True
    ?taf:Field_Number{prop:Trn} = 0
    ?taf:Field_Number{prop:FontStyle} = font:Bold
    ?TAF:Field_Name:Prompt{prop:FontColor} = -1
    ?TAF:Field_Name:Prompt{prop:Color} = 15066597
    If ?taf:Field_Name{prop:ReadOnly} = True
        ?taf:Field_Name{prop:FontColor} = 65793
        ?taf:Field_Name{prop:Color} = 15066597
    Elsif ?taf:Field_Name{prop:Req} = True
        ?taf:Field_Name{prop:FontColor} = 65793
        ?taf:Field_Name{prop:Color} = 8454143
    Else ! If ?taf:Field_Name{prop:Req} = True
        ?taf:Field_Name{prop:FontColor} = 65793
        ?taf:Field_Name{prop:Color} = 16777215
    End ! If ?taf:Field_Name{prop:Req} = True
    ?taf:Field_Name{prop:Trn} = 0
    ?taf:Field_Name{prop:FontStyle} = font:Bold
    ?TAF:Field_Type:Prompt{prop:FontColor} = -1
    ?TAF:Field_Type:Prompt{prop:Color} = 15066597
    ?taf:Field_Type{prop:FontColor} = 65793
    ?taf:Field_Type{prop:Color}= 16777215
    ?taf:Field_Type{prop:Color,2} = 16777215
    ?taf:Field_Type{prop:Color,3} = 12937777
    ?taf:Lookup{prop:Font,3} = -1
    ?taf:Lookup{prop:Color} = 15066597
    ?taf:Lookup{prop:Trn} = 0
    ?taf:Force_Lookup{prop:Font,3} = -1
    ?taf:Force_Lookup{prop:Color} = 15066597
    ?taf:Force_Lookup{prop:Trn} = 0
    ?taf:Compulsory{prop:Font,3} = -1
    ?taf:Compulsory{prop:Color} = 15066597
    ?taf:Compulsory{prop:Trn} = 0
    ?taf:Compulsory_At_Booking{prop:Font,3} = -1
    ?taf:Compulsory_At_Booking{prop:Color} = 15066597
    ?taf:Compulsory_At_Booking{prop:Trn} = 0
    ?taf:ReplicateFault{prop:Font,3} = -1
    ?taf:ReplicateFault{prop:Color} = 15066597
    ?taf:ReplicateFault{prop:Trn} = 0
    ?taf:ReplicateInvoice{prop:Font,3} = -1
    ?taf:ReplicateInvoice{prop:Color} = 15066597
    ?taf:ReplicateInvoice{prop:Trn} = 0
    ?taf:RestrictLength{prop:Font,3} = -1
    ?taf:RestrictLength{prop:Color} = 15066597
    ?taf:RestrictLength{prop:Trn} = 0
    ?Group2{prop:Font,3} = -1
    ?Group2{prop:Color} = 15066597
    ?Group2{prop:Trn} = 0
    ?TAF:LengthFrom:Prompt{prop:FontColor} = -1
    ?TAF:LengthFrom:Prompt{prop:Color} = 15066597
    If ?taf:LengthFrom{prop:ReadOnly} = True
        ?taf:LengthFrom{prop:FontColor} = 65793
        ?taf:LengthFrom{prop:Color} = 15066597
    Elsif ?taf:LengthFrom{prop:Req} = True
        ?taf:LengthFrom{prop:FontColor} = 65793
        ?taf:LengthFrom{prop:Color} = 8454143
    Else ! If ?taf:LengthFrom{prop:Req} = True
        ?taf:LengthFrom{prop:FontColor} = 65793
        ?taf:LengthFrom{prop:Color} = 16777215
    End ! If ?taf:LengthFrom{prop:Req} = True
    ?taf:LengthFrom{prop:Trn} = 0
    ?taf:LengthFrom{prop:FontStyle} = font:Bold
    ?TAF:LengthTo:Prompt{prop:FontColor} = -1
    ?TAF:LengthTo:Prompt{prop:Color} = 15066597
    If ?taf:LengthTo{prop:ReadOnly} = True
        ?taf:LengthTo{prop:FontColor} = 65793
        ?taf:LengthTo{prop:Color} = 15066597
    Elsif ?taf:LengthTo{prop:Req} = True
        ?taf:LengthTo{prop:FontColor} = 65793
        ?taf:LengthTo{prop:Color} = 8454143
    Else ! If ?taf:LengthTo{prop:Req} = True
        ?taf:LengthTo{prop:FontColor} = 65793
        ?taf:LengthTo{prop:Color} = 16777215
    End ! If ?taf:LengthTo{prop:Req} = True
    ?taf:LengthTo{prop:Trn} = 0
    ?taf:LengthTo{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    If ?tfo:Field{prop:ReadOnly} = True
        ?tfo:Field{prop:FontColor} = 65793
        ?tfo:Field{prop:Color} = 15066597
    Elsif ?tfo:Field{prop:Req} = True
        ?tfo:Field{prop:FontColor} = 65793
        ?tfo:Field{prop:Color} = 8454143
    Else ! If ?tfo:Field{prop:Req} = True
        ?tfo:Field{prop:FontColor} = 65793
        ?tfo:Field{prop:Color} = 16777215
    End ! If ?tfo:Field{prop:Req} = True
    ?tfo:Field{prop:Trn} = 0
    ?tfo:Field{prop:FontStyle} = font:Bold
    ?Browse:2{prop:FontColor} = 65793
    ?Browse:2{prop:Color}= 16777215
    ?Browse:2{prop:Color,2} = 16777215
    ?Browse:2{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateTradeAccFaultCodes',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateTradeAccFaultCodes',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateTradeAccFaultCodes',1)
    SolaceViewVars('Filepath',Filepath,'UpdateTradeAccFaultCodes',1)
    SolaceViewVars('SaveTFO',SaveTFO,'UpdateTradeAccFaultCodes',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TAF:AccountNumber:Prompt;  SolaceCtrlName = '?TAF:AccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:AccountNumber;  SolaceCtrlName = '?taf:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TAF:Field_Number:Prompt;  SolaceCtrlName = '?TAF:Field_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:Field_Number;  SolaceCtrlName = '?taf:Field_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TAF:Field_Name:Prompt;  SolaceCtrlName = '?TAF:Field_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:Field_Name;  SolaceCtrlName = '?taf:Field_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TAF:Field_Type:Prompt;  SolaceCtrlName = '?TAF:Field_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:Field_Type;  SolaceCtrlName = '?taf:Field_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:Lookup;  SolaceCtrlName = '?taf:Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:Force_Lookup;  SolaceCtrlName = '?taf:Force_Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:Compulsory;  SolaceCtrlName = '?taf:Compulsory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:Compulsory_At_Booking;  SolaceCtrlName = '?taf:Compulsory_At_Booking';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:ReplicateFault;  SolaceCtrlName = '?taf:ReplicateFault';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:ReplicateInvoice;  SolaceCtrlName = '?taf:ReplicateInvoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:RestrictLength;  SolaceCtrlName = '?taf:RestrictLength';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group2;  SolaceCtrlName = '?Group2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TAF:LengthFrom:Prompt;  SolaceCtrlName = '?TAF:LengthFrom:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:LengthFrom;  SolaceCtrlName = '?taf:LengthFrom';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TAF:LengthTo:Prompt;  SolaceCtrlName = '?TAF:LengthTo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?taf:LengthTo;  SolaceCtrlName = '?taf:LengthTo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfo:Field;  SolaceCtrlName = '?tfo:Field';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:2;  SolaceCtrlName = '?Browse:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ImportButton;  SolaceCtrlName = '?ImportButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTradeAccFaultCodes')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateTradeAccFaultCodes')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TAF:AccountNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(taf:Record,History::taf:Record)
  SELF.AddHistoryField(?taf:AccountNumber,2)
  SELF.AddHistoryField(?taf:Field_Number,3)
  SELF.AddHistoryField(?taf:Field_Name,4)
  SELF.AddHistoryField(?taf:Field_Type,5)
  SELF.AddHistoryField(?taf:Lookup,6)
  SELF.AddHistoryField(?taf:Force_Lookup,7)
  SELF.AddHistoryField(?taf:Compulsory,8)
  SELF.AddHistoryField(?taf:Compulsory_At_Booking,9)
  SELF.AddHistoryField(?taf:ReplicateFault,10)
  SELF.AddHistoryField(?taf:ReplicateInvoice,11)
  SELF.AddHistoryField(?taf:RestrictLength,12)
  SELF.AddHistoryField(?taf:LengthFrom,13)
  SELF.AddHistoryField(?taf:LengthTo,14)
  SELF.AddUpdateFile(Access:TRAFAULT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TRAFAULO.Open
  Access:TRAFAULT.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRAFAULT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:TRAFAULO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?taf:Field_Type{prop:vcr} = TRUE
  ?Browse:2{prop:vcr} = TRUE
  ! support for CPCS
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,tfo:Field_Key)
  BRW2.AddRange(tfo:Field_Number)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,tfo:Field,1,BRW2)
  BRW2.AddField(tfo:Field,BRW2.Q.tfo:Field)
  BRW2.AddField(tfo:Description,BRW2.Q.tfo:Description)
  BRW2.AddField(tfo:AccountNumber,BRW2.Q.tfo:AccountNumber)
  BRW2.AddField(tfo:Field_Number,BRW2.Q.tfo:Field_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?taf:Lookup{Prop:Checked} = True
    UNHIDE(?TAF:Force_Lookup)
  END
  IF ?taf:Lookup{Prop:Checked} = False
    HIDE(?TAF:Force_Lookup)
  END
  IF ?taf:Compulsory{Prop:Checked} = True
    DISABLE(?taf:Compulsory_At_Booking)
  END
  IF ?taf:Compulsory{Prop:Checked} = False
    ENABLE(?taf:Compulsory_At_Booking)
  END
  IF ?taf:Compulsory_At_Booking{Prop:Checked} = True
    DISABLE(?taf:Compulsory)
  END
  IF ?taf:Compulsory_At_Booking{Prop:Checked} = False
    ENABLE(?taf:Compulsory)
  END
  IF ?taf:RestrictLength{Prop:Checked} = True
    UNHIDE(?Group2)
  END
  IF ?taf:RestrictLength{Prop:Checked} = False
    HIDE(?Group2)
  END
  BRW2.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRAFAULO.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateTradeAccFaultCodes',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If request = Insertrecord
      If taf:field_number = ''
          Case MessageEx('You have not entered a Field Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          do_update# = 0
      End!If maf:field_number = ''
  End!If request = Insertrecord
  If do_update# = 1 and taf:lookup = 'YES'
      glo:select1   = taf:AccountNumber
      glo:select2   = taf:Field_Number
      glo:Select3   = taf:Field_Name
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateTradeFaultCodesLookup
    ReturnValue = GlobalResponse
  END
  End!If do_update# = 1 and maf:lookup = 'YES'
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?taf:Field_Number
      IF Access:TRAFAULT.TryValidateField(3)
        SELECT(?taf:Field_Number)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?taf:Field_Number
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?taf:Field_Number{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:2)
      BRW2.ApplyRange
      BRW2.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?taf:Lookup
      IF ?taf:Lookup{Prop:Checked} = True
        UNHIDE(?TAF:Force_Lookup)
      END
      IF ?taf:Lookup{Prop:Checked} = False
        HIDE(?TAF:Force_Lookup)
      END
      ThisWindow.Reset
    OF ?taf:Compulsory
      IF ?taf:Compulsory{Prop:Checked} = True
        DISABLE(?taf:Compulsory_At_Booking)
      END
      IF ?taf:Compulsory{Prop:Checked} = False
        ENABLE(?taf:Compulsory_At_Booking)
      END
      ThisWindow.Reset
    OF ?taf:Compulsory_At_Booking
      IF ?taf:Compulsory_At_Booking{Prop:Checked} = True
        DISABLE(?taf:Compulsory)
      END
      IF ?taf:Compulsory_At_Booking{Prop:Checked} = False
        ENABLE(?taf:Compulsory)
      END
      ThisWindow.Reset
    OF ?taf:RestrictLength
      IF ?taf:RestrictLength{Prop:Checked} = True
        UNHIDE(?Group2)
      END
      IF ?taf:RestrictLength{Prop:Checked} = False
        HIDE(?Group2)
      END
      ThisWindow.Reset
    OF ?ImportButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
      ! Start Change BE021 BE(2/12/03)
      IF (FileDialog('Import File', Filepath, 'CSV Files|*.CSV|Text Files|*.TXT|All Files|*.*', |
                      file:keepdir + file:longname)) THEN
          IMPORT_FILE{PROP:NAME} = CLIP(Filepath)
          OPEN(IMPORT_FILE, 20h)
          IF (ERRORCODE()) THEN
              MESSAGE('FILE OPEN ERROR: ' & ERROR())
          ELSE
              SET(IMPORT_FILE)
              SaveTFO = access:TRAFAULO.SaveFile()
              SETCURSOR(Cursor:wait)
              LOOP
                  NEXT(IMPORT_FILE)
                  IF (ERRORCODE()) THEN
                      BREAK
                  END
                  access:TRAFAULO.ClearKey(tfo:Field_Key)
                  tfo:AccountNumber = taf:AccountNumber
                  tfo:Field_Number = taf:Field_Number
                  tfo:Field = IMP:Code
                  IF (access:TRAFAULO.TryFetch(tfo:Field_Key) = Level:Benign) THEN
                          tfo:Description = IMP:Description
                          access:TRAFAULO.Update()
                  ELSE
                      IF (access:TRAFAULO.Primerecord() = Level:benign) THEN
                          tfo:AccountNumber = taf:AccountNumber
                          tfo:Field_Number = taf:Field_Number
                          tfo:Field = UPPER(IMP:Code)
                          tfo:Description = UPPER(IMP:Description)
                          access:TRAFAULO.Insert()
                      END
                  END
              END
              CLOSE(IMPORT_FILE)
              access:TRAFAULO.restorefile(SaveTFO)
              BRW2.ResetSort(1)
              SETCURSOR()
          END
      END
      ! End Change BE021 BE(2/12/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateTradeAccFaultCodes')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?taf:Field_Number
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:2)
      BRW2.ApplyRange
      BRW2.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = taf:AccountNumber
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = taf:Field_Number
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

