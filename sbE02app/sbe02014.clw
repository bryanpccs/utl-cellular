

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBE02014.INC'),ONCE        !Local module procedure declarations
                     END





Browse_Loan PROCEDURE (f_stock_type)                  !Generated from procedure template - Window

CurrentTab           STRING(80)
pos                  STRING(255)
save_loa_id          USHORT,AUTO
LocalRequest         LONG
FilesOpened          BYTE
all_temp             STRING('AVL')
available_temp       STRING('YES')
status_temp          STRING(40)
Stock_Type_Temp      STRING(30)
Model_Number_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:allstock         BYTE(0)
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Model_Number_Temp
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Stock_Type_Temp
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(LOAN)
                       PROJECT(loa:ESN)
                       PROJECT(loa:Ref_Number)
                       PROJECT(loa:Model_Number)
                       PROJECT(loa:Manufacturer)
                       PROJECT(loa:MSN)
                       PROJECT(loa:Colour)
                       PROJECT(loa:Job_Number)
                       PROJECT(loa:Available)
                       PROJECT(loa:Stock_Type)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
loa:ESN                LIKE(loa:ESN)                  !List box control field - type derived from field
loa:Ref_Number         LIKE(loa:Ref_Number)           !List box control field - type derived from field
loa:Model_Number       LIKE(loa:Model_Number)         !List box control field - type derived from field
loa:Manufacturer       LIKE(loa:Manufacturer)         !List box control field - type derived from field
loa:MSN                LIKE(loa:MSN)                  !List box control field - type derived from field
loa:Colour             LIKE(loa:Colour)               !List box control field - type derived from field
status_temp            LIKE(status_temp)              !List box control field - type derived from local data
loa:Job_Number         LIKE(loa:Job_Number)           !List box control field - type derived from field
loa:Available          LIKE(loa:Available)            !Browse key field - type derived from field
loa:Stock_Type         LIKE(loa:Stock_Type)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK17::loa:Available       LIKE(loa:Available)
HK17::loa:Model_Number    LIKE(loa:Model_Number)
HK17::loa:Stock_Type      LIKE(loa:Stock_Type)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB13::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
FDCB12::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,538770663)          !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse The Loan Units File'),AT(,,662,262),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Loan'),SYSTEM,GRAY,MAX,DOUBLE
                       PANEL,AT(4,4,576,24),USE(?Panel1),FILL(COLOR:Gray)
                       LIST,AT(8,64,568,192),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('66L(2)|M~I.M.E.I. Number~@s15@38R(2)|M~Unit No~L@s8@94L(2)|M~Model Number~@s30@9' &|
   '4L(2)|M~Manufacturer~@s30@50L(2)|M~M.S.N.~@s15@76L(2)|M~Colour~@s30@130L(2)|M~St' &|
   'atus~@s30@32L(2)|M~Job Number~@p<<<<<<<<<<<<<<#p@'),FROM(Queue:Browse:1)
                       PROMPT('Stock Type'),AT(8,12),USE(?Prompt1),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                       COMBO(@s30),AT(84,12,124,10),USE(Stock_Type_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       CHECK('All Stock Types'),AT(396,12),USE(tmp:allstock),MSG('All Stock Types'),TIP('All Stock Types'),VALUE('1','0')
                       OPTION,AT(468,4,108,20),USE(all_temp),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                         RADIO('Available'),AT(480,12),USE(?Option1:Radio1),VALUE('AVL')
                         RADIO('All'),AT(536,12),USE(?Option1:Radio2),VALUE('YES')
                       END
                       BUTTON('Make Unit Available'),AT(584,80,76,20),USE(?ReturnUnitToStock),LEFT,ICON('arrow.ico')
                       BUTTON('Move Stock Type'),AT(584,104,76,20),USE(?MoveStockType),LEFT,ICON('moving.ico')
                       BUTTON('&Insert'),AT(584,164,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(584,188,76,20),USE(?Change:3),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(584,212,76,20),USE(?Delete:3),LEFT,ICON('delete.ico')
                       SHEET,AT(4,32,576,228),USE(?CurrentTab),SPREAD
                         TAB('I.M.E.I. Number'),USE(?Tab:4)
                           ENTRY(@s30),AT(8,48,108,10),USE(loa:ESN),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Loan Unit Number'),USE(?Tab:2)
                           ENTRY(@p<<<<<<<#pb),AT(8,48,64,10),USE(loa:Ref_Number),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Model Number'),USE(?Tab:3)
                           COMBO(@s30),AT(8,48,124,10),USE(Model_Number_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           ENTRY(@p<<<<<<<<#pb),AT(136,48,64,10),USE(loa:Ref_Number,,?LOA:Ref_Number:2),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                         END
                         TAB('By M.S.N.'),USE(?Tab4)
                           ENTRY(@s30),AT(8,48,104,10),USE(loa:MSN),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('&Select'),AT(584,32,76,20),USE(?Select),LEFT,ICON('select.ico')
                       BUTTON('Close'),AT(584,240,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
BRW1::Sort10:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
BRW1::Sort7:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
BRW1::Sort11:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
BRW1::Sort12:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
BRW1::Sort13:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
BRW1::Sort14:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
BRW1::Sort6:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
BRW1::Sort15:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
BRW1::Sort9:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
BRW1::Sort16:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
BRW1::Sort8:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
BRW1::Sort17:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
BRW1::Sort2:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
BRW1::Sort4:StepClass StepLongClass                   !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
BRW1::Sort5:StepClass StepLongClass                   !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
BRW1::Sort9:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 4 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
BRW1::Sort8:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 4 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB13               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB12               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepStringClass !STRING          !Xplore: Column displaying loa:ESN
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying loa:ESN
Xplore1Step2         StepLongClass   !LONG            !Xplore: Column displaying loa:Ref_Number
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying loa:Ref_Number
Xplore1Step3         StepStringClass !STRING          !Xplore: Column displaying loa:Model_Number
Xplore1Locator3      IncrementalLocatorClass          !Xplore: Column displaying loa:Model_Number
Xplore1Step4         StepStringClass !STRING          !Xplore: Column displaying loa:Manufacturer
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying loa:Manufacturer
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying loa:MSN
Xplore1Locator5      IncrementalLocatorClass          !Xplore: Column displaying loa:MSN
Xplore1Step6         StepStringClass !STRING          !Xplore: Column displaying loa:Colour
Xplore1Locator6      IncrementalLocatorClass          !Xplore: Column displaying loa:Colour
Xplore1Step7         StepCustomClass !                !Xplore: Column displaying status_temp
Xplore1Locator7      IncrementalLocatorClass          !Xplore: Column displaying status_temp
Xplore1Step8         StepLongClass   !LONG            !Xplore: Column displaying loa:Job_Number
Xplore1Locator8      IncrementalLocatorClass          !Xplore: Column displaying loa:Job_Number
Xplore1Step9         StepStringClass !STRING          !Xplore: Column displaying loa:Available
Xplore1Locator9      IncrementalLocatorClass          !Xplore: Column displaying loa:Available
Xplore1Step10        StepStringClass !STRING          !Xplore: Column displaying loa:Stock_Type
Xplore1Locator10     IncrementalLocatorClass          !Xplore: Column displaying loa:Stock_Type

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1{prop:Fill} = 15066597

    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?Stock_Type_Temp{prop:ReadOnly} = True
        ?Stock_Type_Temp{prop:FontColor} = 65793
        ?Stock_Type_Temp{prop:Color} = 15066597
    Elsif ?Stock_Type_Temp{prop:Req} = True
        ?Stock_Type_Temp{prop:FontColor} = 65793
        ?Stock_Type_Temp{prop:Color} = 8454143
    Else ! If ?Stock_Type_Temp{prop:Req} = True
        ?Stock_Type_Temp{prop:FontColor} = 65793
        ?Stock_Type_Temp{prop:Color} = 16777215
    End ! If ?Stock_Type_Temp{prop:Req} = True
    ?Stock_Type_Temp{prop:Trn} = 0
    ?Stock_Type_Temp{prop:FontStyle} = font:Bold
    ?tmp:allstock{prop:Font,3} = -1
    ?tmp:allstock{prop:Color} = 15066597
    ?tmp:allstock{prop:Trn} = 0
    ?all_temp{prop:Font,3} = -1
    ?all_temp{prop:Color} = 15066597
    ?all_temp{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:4{prop:Color} = 15066597
    If ?loa:ESN{prop:ReadOnly} = True
        ?loa:ESN{prop:FontColor} = 65793
        ?loa:ESN{prop:Color} = 15066597
    Elsif ?loa:ESN{prop:Req} = True
        ?loa:ESN{prop:FontColor} = 65793
        ?loa:ESN{prop:Color} = 8454143
    Else ! If ?loa:ESN{prop:Req} = True
        ?loa:ESN{prop:FontColor} = 65793
        ?loa:ESN{prop:Color} = 16777215
    End ! If ?loa:ESN{prop:Req} = True
    ?loa:ESN{prop:Trn} = 0
    ?loa:ESN{prop:FontStyle} = font:Bold
    ?Tab:2{prop:Color} = 15066597
    If ?loa:Ref_Number{prop:ReadOnly} = True
        ?loa:Ref_Number{prop:FontColor} = 65793
        ?loa:Ref_Number{prop:Color} = 15066597
    Elsif ?loa:Ref_Number{prop:Req} = True
        ?loa:Ref_Number{prop:FontColor} = 65793
        ?loa:Ref_Number{prop:Color} = 8454143
    Else ! If ?loa:Ref_Number{prop:Req} = True
        ?loa:Ref_Number{prop:FontColor} = 65793
        ?loa:Ref_Number{prop:Color} = 16777215
    End ! If ?loa:Ref_Number{prop:Req} = True
    ?loa:Ref_Number{prop:Trn} = 0
    ?loa:Ref_Number{prop:FontStyle} = font:Bold
    ?Tab:3{prop:Color} = 15066597
    If ?Model_Number_Temp{prop:ReadOnly} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 15066597
    Elsif ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 8454143
    Else ! If ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 16777215
    End ! If ?Model_Number_Temp{prop:Req} = True
    ?Model_Number_Temp{prop:Trn} = 0
    ?Model_Number_Temp{prop:FontStyle} = font:Bold
    If ?LOA:Ref_Number:2{prop:ReadOnly} = True
        ?LOA:Ref_Number:2{prop:FontColor} = 65793
        ?LOA:Ref_Number:2{prop:Color} = 15066597
    Elsif ?LOA:Ref_Number:2{prop:Req} = True
        ?LOA:Ref_Number:2{prop:FontColor} = 65793
        ?LOA:Ref_Number:2{prop:Color} = 8454143
    Else ! If ?LOA:Ref_Number:2{prop:Req} = True
        ?LOA:Ref_Number:2{prop:FontColor} = 65793
        ?LOA:Ref_Number:2{prop:Color} = 16777215
    End ! If ?LOA:Ref_Number:2{prop:Req} = True
    ?LOA:Ref_Number:2{prop:Trn} = 0
    ?LOA:Ref_Number:2{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    If ?loa:MSN{prop:ReadOnly} = True
        ?loa:MSN{prop:FontColor} = 65793
        ?loa:MSN{prop:Color} = 15066597
    Elsif ?loa:MSN{prop:Req} = True
        ?loa:MSN{prop:FontColor} = 65793
        ?loa:MSN{prop:Color} = 8454143
    Else ! If ?loa:MSN{prop:Req} = True
        ?loa:MSN{prop:FontColor} = 65793
        ?loa:MSN{prop:Color} = 16777215
    End ! If ?loa:MSN{prop:Req} = True
    ?loa:MSN{prop:Trn} = 0
    ?loa:MSN{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Loan',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Loan',1)
    SolaceViewVars('pos',pos,'Browse_Loan',1)
    SolaceViewVars('save_loa_id',save_loa_id,'Browse_Loan',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Loan',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Loan',1)
    SolaceViewVars('all_temp',all_temp,'Browse_Loan',1)
    SolaceViewVars('available_temp',available_temp,'Browse_Loan',1)
    SolaceViewVars('status_temp',status_temp,'Browse_Loan',1)
    SolaceViewVars('Stock_Type_Temp',Stock_Type_Temp,'Browse_Loan',1)
    SolaceViewVars('Model_Number_Temp',Model_Number_Temp,'Browse_Loan',1)
    SolaceViewVars('tmp:allstock',tmp:allstock,'Browse_Loan',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Stock_Type_Temp;  SolaceCtrlName = '?Stock_Type_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:allstock;  SolaceCtrlName = '?tmp:allstock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?all_temp;  SolaceCtrlName = '?all_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio1;  SolaceCtrlName = '?Option1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio2;  SolaceCtrlName = '?Option1:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReturnUnitToStock;  SolaceCtrlName = '?ReturnUnitToStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MoveStockType;  SolaceCtrlName = '?MoveStockType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:ESN;  SolaceCtrlName = '?loa:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:Ref_Number;  SolaceCtrlName = '?loa:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_Number_Temp;  SolaceCtrlName = '?Model_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOA:Ref_Number:2;  SolaceCtrlName = '?LOA:Ref_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:MSN;  SolaceCtrlName = '?loa:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select;  SolaceCtrlName = '?Select';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Browse_Loan','?Browse:1')        !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Loan')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Loan')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  If f_stock_Type <> ''
      stock_type_temp = f_stock_type
  End
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:JOBS.Open
  Relate:LOAN.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOAN,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbe02app.INI','>Header',1,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,loa:ESN_Available_Key)
  BRW1.AddRange(loa:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?LOA:ESN,loa:ESN,1,BRW1)
  BRW1.AddResetField(all_temp)
  BRW1.AddSortOrder(,loa:AvailIMEIOnlyKey)
  BRW1.AddRange(loa:Available)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?loa:ESN,loa:ESN,1,BRW1)
  BRW1.AddSortOrder(,loa:ESN_Key)
  BRW1.AddRange(loa:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?LOA:ESN,loa:ESN,1,BRW1)
  BRW1.AddSortOrder(,loa:ESN_Only_Key)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?loa:ESN,loa:ESN,1,BRW1)
  BRW1::Sort4:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort4:StepClass,loa:Ref_Available_Key)
  BRW1.AddRange(loa:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?LOA:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddResetField(all_temp)
  BRW1.AddSortOrder(,loa:AvailRefOnlyKey)
  BRW1.AddRange(loa:Available)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1::Sort5:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort5:StepClass,loa:Ref_Number_Stock_Key)
  BRW1.AddRange(loa:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?LOA:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddResetField(all_temp)
  BRW1.AddSortOrder(,loa:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,loa:Model_Available_Key)
  BRW1.AddRange(loa:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,loa:Model_Number,1,BRW1)
  BRW1.SetFilter('(Upper(loa:model_number) = Upper(model_number_temp))')
  BRW1.AddResetField(all_temp)
  BRW1.AddSortOrder(,loa:AvailModOnlyKey)
  BRW1.AddRange(loa:Model_Number)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,loa:Model_Number_Key)
  BRW1.AddRange(loa:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(,loa:Model_Number,1,BRW1)
  BRW1.SetFilter('(Upper(loa:model_number) = Upper(model_number_temp))')
  BRW1.AddResetField(all_temp)
  BRW1.AddSortOrder(,loa:ModelRefNoKey)
  BRW1.AddRange(loa:Model_Number)
  BRW1.AddLocator(BRW1::Sort15:Locator)
  BRW1::Sort15:Locator.Init(?loa:Ref_Number,loa:Ref_Number,1,BRW1)
  BRW1::Sort9:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort9:StepClass,loa:MSN_Available_Key)
  BRW1.AddRange(loa:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?LOA:MSN,loa:MSN,1,BRW1)
  BRW1.AddSortOrder(,loa:AvailMSNOnlyKey)
  BRW1.AddRange(loa:Available)
  BRW1.AddLocator(BRW1::Sort16:Locator)
  BRW1::Sort16:Locator.Init(?loa:MSN,loa:MSN,1,BRW1)
  BRW1::Sort8:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort8:StepClass,loa:MSN_Key)
  BRW1.AddRange(loa:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?LOA:MSN,loa:MSN,1,BRW1)
  BRW1.AddSortOrder(,loa:MSN_Key)
  BRW1.AddLocator(BRW1::Sort17:Locator)
  BRW1::Sort17:Locator.Init(,loa:Stock_Type,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,loa:Ref_Available_Key)
  BRW1.AddRange(loa:Stock_Type)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,loa:Ref_Number,1,BRW1)
  BRW1.AddResetField(all_temp)
  BIND('status_temp',status_temp)
  BIND('Model_Number_Temp',Model_Number_Temp)
  BIND('Stock_Type_Temp',Stock_Type_Temp)
  BRW1.AddField(loa:ESN,BRW1.Q.loa:ESN)
  BRW1.AddField(loa:Ref_Number,BRW1.Q.loa:Ref_Number)
  BRW1.AddField(loa:Model_Number,BRW1.Q.loa:Model_Number)
  BRW1.AddField(loa:Manufacturer,BRW1.Q.loa:Manufacturer)
  BRW1.AddField(loa:MSN,BRW1.Q.loa:MSN)
  BRW1.AddField(loa:Colour,BRW1.Q.loa:Colour)
  BRW1.AddField(status_temp,BRW1.Q.status_temp)
  BRW1.AddField(loa:Job_Number,BRW1.Q.loa:Job_Number)
  BRW1.AddField(loa:Available,BRW1.Q.loa:Available)
  BRW1.AddField(loa:Stock_Type,BRW1.Q.loa:Stock_Type)
  QuickWindow{PROP:MinWidth}=530
  QuickWindow{PROP:MinHeight}=214
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB13.Init(Model_Number_Temp,?Model_Number_Temp,Queue:FileDropCombo:1.ViewPosition,FDCB13::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB13.Q &= Queue:FileDropCombo:1
  FDCB13.AddSortOrder(mod:Model_Number_Key)
  FDCB13.AddField(mod:Model_Number,FDCB13.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB13.WindowComponent)
  FDCB13.DefaultFill = 0
  FDCB12.Init(Stock_Type_Temp,?Stock_Type_Temp,Queue:FileDropCombo.ViewPosition,FDCB12::View:FileDropCombo,Queue:FileDropCombo,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB12.Q &= Queue:FileDropCombo
  FDCB12.AddSortOrder(stp:Use_Loan_Key)
  FDCB12.SetFilter('Upper(stp:use_loan) = ''YES''')
  FDCB12.AddField(stp:Stock_Type,FDCB12.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB12.WindowComponent)
  FDCB12.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:4{PROP:TEXT} = 'By I.M.E.I. Number'
    ?Tab:2{PROP:TEXT} = 'By Loan Unit Number'
    ?Tab:3{PROP:TEXT} = 'By Model Number'
    ?Tab4{PROP:TEXT} = 'By M.S.N.'
    ?Browse:1{PROP:FORMAT} ='66L(2)|M~I.M.E.I. Number~@s15@#1#38R(2)|M~Unit No~L@s8@#2#94L(2)|M~Model Number~@s30@#3#94L(2)|M~Manufacturer~@s30@#4#50L(2)|M~M.S.N.~@s15@#5#76L(2)|M~Colour~@s30@#6#130L(2)|M~Status~@s30@#7#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:JOBS.Close
    Relate:LOAN.Close
    Relate:USERS_ALIAS.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Browse_Loan','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Loan',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
    of insertrecord
        If SecurityCheck('LOAN UNITS - INSERT')
            case messageex('You do not have access to this option.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                of 1 ! &ok button
            end!case messageex
            do_update# = false
            access:loan.cancelautoinc()
        end
    of changerecord
        If SecurityCheck('LOAN UNITS - CHANGE')
            case messageex('You do not have access to this option.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                of 1 ! &ok button
            end!case messageex
            do_update# = false
        end
    of deleterecord
        If SecurityCheck('LOAN UNITS - DELETE')
            case messageex('You do not have access to this option.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                of 1 ! &ok button
            end!case messageex
            do_update# = false
        end
  end !case request
  
  If do_update# = true
      If Thiswindow.request = Selectrecord
          Case MessageEx('You cannot Insert/Change/Delete an item while you are updating a Job.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          do_update# = false
          If request = Insertrecord
              access:loan.cancelautoinc()
          End!If request = Insertrecord
      End!If Globalrequest = Selectrecord
  End!If do_update# = true
  
  if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateLOAN
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(loa:ESN,BRW1.Q.loa:ESN)
  Xplore1.AddField(loa:Ref_Number,BRW1.Q.loa:Ref_Number)
  Xplore1.AddField(loa:Model_Number,BRW1.Q.loa:Model_Number)
  Xplore1.AddField(loa:Manufacturer,BRW1.Q.loa:Manufacturer)
  Xplore1.AddField(loa:MSN,BRW1.Q.loa:MSN)
  Xplore1.AddField(loa:Colour,BRW1.Q.loa:Colour)
  Xplore1.AddField(loa:Job_Number,BRW1.Q.loa:Job_Number)
  Xplore1.AddField(loa:Available,BRW1.Q.loa:Available)
  Xplore1.AddField(loa:Stock_Type,BRW1.Q.loa:Stock_Type)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,loa:Ref_Available_Key) !Xplore Sort Order for File Sequence
  BRW1.AddRange(loa:Stock_Type)                       !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Stock_Type_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
    OF ?tmp:allstock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:allstock, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:allstock, Accepted)
    OF ?all_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?all_temp, Accepted)
      Thiswindow.reset
      Select(?Browse:1)
      If thiswindow.request <> Selectrecord
          Case all_temp
              Of 'YES'
                  Unhide(?ReturnUnitToStock)
              Of 'NO'
                  Hide(?ReturnUnitToStock)
          End!Case all_temp
      End!If thiswindow.request <> Selectrecord
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?all_temp, Accepted)
    OF ?ReturnUnitToStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnUnitToStock, Accepted)
      thiswindow.reset
      Case MessageEx('You are about to remove this unit from job number '&clip(loa:job_number)&'.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = loa:job_number
              If access:jobs.fetch(job:Ref_number_key) = Level:Benign
              !Not sure what to do about this yet???
          !        job:loan_unit_number = ''
          !        access:jobs.update()
              End!If access:jobs.fetch(job:Ref_number_key) = Level:Benign
              loa:job_number = ''
              loa:available = 'AVL'
              access:loan.update()
              get(loanhist,0)
              if access:loanhist.primerecord() = level:benign
                  loh:ref_number    = loa:ref_number
                  loh:date          = Today()
                  loh:time          = Clock()
                  access:users.clearkey(use:password_key)
                  use:password =glo:password
                  access:users.fetch(use:password_key)
                  loh:user = use:user_code
                  loh:status        = 'UNIT RETURNED TO STOCK FROM JOB: ' & Clip(Format(job:ref_number,@p<<<<<#p))
                  if access:loanhist.insert()
                      access:loanhist.cancelautoinc()
                  end
              end!if access:loanhist.primerecord() = level:benign
          Of 2 ! &No Button
      End!Case MessageEx
      BRW1.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnUnitToStock, Accepted)
    OF ?MoveStockType
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MoveStockType, Accepted)
      Case MessageEx('Do you wish to move the selected unit to a new Stock Type, or move ALL units to another Stock Type?','ServiceBase 2000',|
                     'Styles\question.ico','|&Move Selected|&Move ALL|&Cancel',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Move Selected Button
              Case MessageEx('You have selected to move the SELECTED unit to another Stock Type. <13,10><13,10>Is this correct?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1       = 'LOA'
                      select_stock_type
                      if globalresponse = requestcompleted
                          access:loan.clearkey(loa:ref_number_key)
                          loa:ref_number = brw1.q.loa:ref_number
                          if access:loan.tryfetch(loa:ref_number_key) = Level:Benign
                              loa:stock_type  = stp:stock_type
                              access:loan.update()
                              get(loanhist,0)
                              if access:loanhist.primerecord() = level:benign
                                  loh:ref_number      = loa:ref_number
                                  loh:date            = today()
                                  loh:time            = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password        = glo:password
                                  access:users.fetch(use:password_key)
                                  loh:user = use:user_code
                                  loh:status          = 'UNIT MOVED FROM ' & Clip(stock_type_temp) & |
                                                         ' TO ' & Clip(stp:stock_type)
                                  access:loanhist.insert()
                              end!if access:loanhist.primerecord() = level:benign
      
                          End!if access:loan.tryfetch(loa:ref_number_key) = Level:Benign
                      end
                      globalrequest     = saverequest#
                      glo:select1       = ''
                  Of 2 ! &No Button
              End!Case MessageEx
          Of 2 ! &Move ALL Button
              Case MessageEx('You have selected to move ALL the units (not matter what status) to a new Stock Type<13,10><13,10>Is this correct?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1       = 'LOA'
                      select_stock_type
                      if globalresponse = requestcompleted
                          setcursor(cursor:wait)
                          save_loa_id = access:loan.savefile()
                          access:loan.clearkey(loa:ref_number_stock_key)
                          loa:stock_type = stock_type_temp
                          set(loa:ref_number_stock_key,loa:ref_number_stock_key)
                          loop
                              if access:loan.next()
                                 break
                              end !if
                              if loa:stock_type <> stock_type_temp      |
                                  then break.  ! end if
                              pos = Position(loa:ref_number_stock_key)
                              loa:stock_type  = stp:stock_type
                              access:loan.update()
                              get(loanhist,0)
                              if access:loanhist.primerecord() = level:benign
                                  loh:ref_number      = loa:ref_number
                                  loh:date            = today()
                                  loh:time            = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password        = glo:password
                                  access:users.fetch(use:password_key)
                                  loh:user = use:user_code
                                  loh:status          = 'UNIT MOVED FROM ' & Clip(stock_type_temp) & |
                                                         ' TO ' & Clip(stp:stock_type)
                                  access:loanhist.insert()
                              end!if access:loanhist.primerecord() = level:benign
      
                              Reset(loa:ref_number_stock_key,pos)
                          end !loop
                          access:loan.restorefile(save_loa_id)
                          setcursor()
                      end
                      globalrequest     = saverequest#
                      glo:select1       = ''
      
                  Of 2 ! &No Button
              End!Case MessageEx
          Of 3 ! &Cancel Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MoveStockType, Accepted)
    OF ?Model_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Loan')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Stock_Type_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='66L(2)|M~I.M.E.I. Number~@s15@#1#38R(2)|M~Unit No~L@s8@#2#94L(2)|M~Model Number~@s30@#3#94L(2)|M~Manufacturer~@s30@#4#50L(2)|M~M.S.N.~@s15@#5#76L(2)|M~Colour~@s30@#6#130L(2)|M~Status~@s30@#7#'
          ?Tab:4{PROP:TEXT} = 'By I.M.E.I. Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='38R(2)|M~Unit No~L@s8@#2#66L(2)|M~I.M.E.I. Number~@s15@#1#94L(2)|M~Model Number~@s30@#3#94L(2)|M~Manufacturer~@s30@#4#50L(2)|M~M.S.N.~@s15@#5#76L(2)|M~Colour~@s30@#6#130L(2)|M~Status~@s30@#7#'
          ?Tab:2{PROP:TEXT} = 'By Loan Unit Number'
        OF 3
          ?Browse:1{PROP:FORMAT} ='38R(2)|M~Unit No~L@s8@#2#66L(2)|M~I.M.E.I. Number~@s15@#1#94L(2)|M~Model Number~@s30@#3#94L(2)|M~Manufacturer~@s30@#4#50L(2)|M~M.S.N.~@s15@#5#76L(2)|M~Colour~@s30@#6#130L(2)|M~Status~@s30@#7#'
          ?Tab:3{PROP:TEXT} = 'By Model Number'
        OF 4
          ?Browse:1{PROP:FORMAT} ='50L(2)|M~M.S.N.~@s15@#5#66L(2)|M~I.M.E.I. Number~@s15@#1#38R(2)|M~Unit No~L@s8@#2#94L(2)|M~Model Number~@s30@#3#94L(2)|M~Manufacturer~@s30@#4#76L(2)|M~Colour~@s30@#6#130L(2)|M~Status~@s30@#7#'
          ?Tab4{PROP:TEXT} = 'By M.S.N.'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?Model_Number_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?loa:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:ESN, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:ESN, Selected)
    OF ?LOA:Ref_Number:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LOA:Ref_Number:2, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LOA:Ref_Number:2, Selected)
    OF ?loa:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:MSN, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:MSN, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      If thiswindow.request = Selectrecord
          Unhide(?Select)
          Hide(?ReturnUnitToStock)
      End
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      FDCB13.ResetQueue(1)
      FDCB12.ResetQueue(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore1.GetCurrentBbSize()                      !Xplore
      Xplore1.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 1                           !Xplore
        Xplore1.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
  ELSIF Choice(?CurrentTab) = 3 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Model_Number_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = all_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Stock_Type_Temp
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  END                                                 !Xplore
  IF Choice(?CurrentTab) = 1 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
    RETURN SELF.SetSort(10,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
    RETURN SELF.SetSort(11,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
    RETURN SELF.SetSort(12,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 0
    RETURN SELF.SetSort(13,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(all_temp) = 'AVL' And Upper(tmp:allstock) = 1
    RETURN SELF.SetSort(14,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 0
    RETURN SELF.SetSort(15,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(all_temp) = 'YES' And Upper(tmp:allstock) = 1
    RETURN SELF.SetSort(16,Force)
  ELSE
    RETURN SELF.SetSort(17,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  CASE (loa:Available)
  OF 'AVL'
    status_temp = 'AVAILABLE'
  OF 'LOA'
    status_temp = 'LOANED - JOB NO: ' & loa:Job_Number
  OF 'INC'
    status_temp = 'INCOMING TRANSIT - JOB NO: ' & loa:Job_Number
  OF 'FAU'
    status_temp = 'FAULTY'
  OF 'REP'
    status_temp = 'IN REPAIR - JOB NO: ' & loa:Job_Number
  OF 'SUS'
    status_temp = 'SUSPENDED'
  OF 'DES'
    status_temp = 'DESPATCHED - JOB NO: ' & loa:Job_Number
  OF 'QA1'
    status_temp = 'ELECTRONIC QA REQUIRED - JOB NO: ' & loa:Job_Number
  OF 'QA2'
    status_temp = 'MANUAL QA REQUIRED - JOB NO: ' & loa:Job_Number
  OF 'QAF'
    status_temp = 'QA FAILED'
  OF 'RTS'
    status_temp = 'RETURN TO STOCK'
  ELSE
    status_temp = 'IN REPAIR - JOB NO: ' & loa:Job_Number
  END
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !loa:ESN
  OF 2 !loa:Ref_Number
  OF 3 !loa:Model_Number
  OF 4 !loa:Manufacturer
  OF 5 !loa:MSN
  OF 6 !loa:Colour
  OF 7 !status_temp
  OF 8 !loa:Job_Number
  OF 9 !loa:Available
  OF 10 !loa:Stock_Type
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step10,loa:Ref_Available_Key)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,loa:Ref_Available_Key)
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(?loa:ESN,loa:ESN,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(?loa:Ref_Number,loa:Ref_Number,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,loa:Model_Number,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,loa:Manufacturer,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(?loa:MSN,loa:MSN,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,loa:Colour,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(,status_temp,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(,loa:Job_Number,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(,loa:Available,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(,loa:Stock_Type,1,SELF.BC)
      SELF.BC.AddResetField(all_temp)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(loa:Stock_Type)
  SELF.BC.AddResetField(all_temp)
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(LOAN)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('loa:ESN')                    !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('I.M.E.I. Number')            !Header
                PSTRING('@s15')                       !Picture
                PSTRING('I.M.E.I. Number')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Unit No')                    !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Unit No')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Manufacturer')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Manufacturer')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Manufacturer')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:MSN')                    !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('M.S.N.')                     !Header
                PSTRING('@s15')                       !Picture
                PSTRING('M.S.N.')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Colour')                 !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Colour')                     !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Colour')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('status_temp')                !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('status_temp')                !Header
                PSTRING('@S20')                       !Picture
                PSTRING('status_temp')                !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Job_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job Number')                 !Header
                PSTRING('@p<<<<<<<<<<<<<<#p')         !Picture
                PSTRING('Job Number')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Available')              !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Available')                  !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Available')                  !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('loa:Stock_Type')             !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Stock Type')                 !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Stock Type')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(10)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?Stock_Type_Temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?all_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Option1:Radio1, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Option1:Radio2, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?LOA:Ref_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Model_Number_Temp, Resize:FixLeft, Resize:LockSize)
  SELF.SetStrategy(?LOA:Ref_Number:2, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?LOA:ESN, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?LOA:MSN, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

