

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02053.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSIDSCAccountRegion PROCEDURE (scAccountID, accRegID) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:SCAccountID      LONG
tmp:AccountNo        STRING(15)
tmp:RegionName       STRING(30)
tmp:TransitDaysException BYTE
tmp:TransitDaysToSC  LONG
tmp:TransitDaysToStore LONG
window               WINDOW('Changing A Service Centre Account Region'),AT(,,232,132),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,98),USE(?Sheet1),SPREAD
                         TAB('Account Details'),USE(?Tab1)
                           PROMPT('Region Name'),AT(8,36),USE(?tmp:RegionName:Prompt),TRN
                           ENTRY(@s30),AT(92,36,124,10),USE(tmp:RegionName),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           PROMPT('Account No'),AT(8,20),USE(?tmp:AccountNo:Prompt),TRN
                           ENTRY(@s15),AT(92,20,124,10),USE(tmp:AccountNo),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                           CHECK('Transit Days Exception'),AT(92,52),USE(tmp:TransitDaysException),TRN,VALUE('1','0')
                           PROMPT('Transit Days To SC'),AT(8,68),USE(?tmp:TransitDaysToSC:Prompt),TRN
                           ENTRY(@n3),AT(92,68,60,10),USE(tmp:TransitDaysToSC),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Transit Days To Store'),AT(8,84),USE(?tmp:TransitDaysToStore:Prompt),TRN
                           ENTRY(@n3),AT(92,84,60,10),USE(tmp:TransitDaysToStore),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       PANEL,AT(4,106,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(112,110,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(168,110,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:RegionName:Prompt{prop:FontColor} = -1
    ?tmp:RegionName:Prompt{prop:Color} = 15066597
    If ?tmp:RegionName{prop:ReadOnly} = True
        ?tmp:RegionName{prop:FontColor} = 65793
        ?tmp:RegionName{prop:Color} = 15066597
    Elsif ?tmp:RegionName{prop:Req} = True
        ?tmp:RegionName{prop:FontColor} = 65793
        ?tmp:RegionName{prop:Color} = 8454143
    Else ! If ?tmp:RegionName{prop:Req} = True
        ?tmp:RegionName{prop:FontColor} = 65793
        ?tmp:RegionName{prop:Color} = 16777215
    End ! If ?tmp:RegionName{prop:Req} = True
    ?tmp:RegionName{prop:Trn} = 0
    ?tmp:RegionName{prop:FontStyle} = font:Bold
    ?tmp:AccountNo:Prompt{prop:FontColor} = -1
    ?tmp:AccountNo:Prompt{prop:Color} = 15066597
    If ?tmp:AccountNo{prop:ReadOnly} = True
        ?tmp:AccountNo{prop:FontColor} = 65793
        ?tmp:AccountNo{prop:Color} = 15066597
    Elsif ?tmp:AccountNo{prop:Req} = True
        ?tmp:AccountNo{prop:FontColor} = 65793
        ?tmp:AccountNo{prop:Color} = 8454143
    Else ! If ?tmp:AccountNo{prop:Req} = True
        ?tmp:AccountNo{prop:FontColor} = 65793
        ?tmp:AccountNo{prop:Color} = 16777215
    End ! If ?tmp:AccountNo{prop:Req} = True
    ?tmp:AccountNo{prop:Trn} = 0
    ?tmp:AccountNo{prop:FontStyle} = font:Bold
    ?tmp:TransitDaysException{prop:Font,3} = -1
    ?tmp:TransitDaysException{prop:Color} = 15066597
    ?tmp:TransitDaysException{prop:Trn} = 0
    ?tmp:TransitDaysToSC:Prompt{prop:FontColor} = -1
    ?tmp:TransitDaysToSC:Prompt{prop:Color} = 15066597
    If ?tmp:TransitDaysToSC{prop:ReadOnly} = True
        ?tmp:TransitDaysToSC{prop:FontColor} = 65793
        ?tmp:TransitDaysToSC{prop:Color} = 15066597
    Elsif ?tmp:TransitDaysToSC{prop:Req} = True
        ?tmp:TransitDaysToSC{prop:FontColor} = 65793
        ?tmp:TransitDaysToSC{prop:Color} = 8454143
    Else ! If ?tmp:TransitDaysToSC{prop:Req} = True
        ?tmp:TransitDaysToSC{prop:FontColor} = 65793
        ?tmp:TransitDaysToSC{prop:Color} = 16777215
    End ! If ?tmp:TransitDaysToSC{prop:Req} = True
    ?tmp:TransitDaysToSC{prop:Trn} = 0
    ?tmp:TransitDaysToSC{prop:FontStyle} = font:Bold
    ?tmp:TransitDaysToStore:Prompt{prop:FontColor} = -1
    ?tmp:TransitDaysToStore:Prompt{prop:Color} = 15066597
    If ?tmp:TransitDaysToStore{prop:ReadOnly} = True
        ?tmp:TransitDaysToStore{prop:FontColor} = 65793
        ?tmp:TransitDaysToStore{prop:Color} = 15066597
    Elsif ?tmp:TransitDaysToStore{prop:Req} = True
        ?tmp:TransitDaysToStore{prop:FontColor} = 65793
        ?tmp:TransitDaysToStore{prop:Color} = 8454143
    Else ! If ?tmp:TransitDaysToStore{prop:Req} = True
        ?tmp:TransitDaysToStore{prop:FontColor} = 65793
        ?tmp:TransitDaysToStore{prop:Color} = 16777215
    End ! If ?tmp:TransitDaysToStore{prop:Req} = True
    ?tmp:TransitDaysToStore{prop:Trn} = 0
    ?tmp:TransitDaysToStore{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSIDSCAccountRegion',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:SCAccountID',tmp:SCAccountID,'UpdateSIDSCAccountRegion',1)
    SolaceViewVars('tmp:AccountNo',tmp:AccountNo,'UpdateSIDSCAccountRegion',1)
    SolaceViewVars('tmp:RegionName',tmp:RegionName,'UpdateSIDSCAccountRegion',1)
    SolaceViewVars('tmp:TransitDaysException',tmp:TransitDaysException,'UpdateSIDSCAccountRegion',1)
    SolaceViewVars('tmp:TransitDaysToSC',tmp:TransitDaysToSC,'UpdateSIDSCAccountRegion',1)
    SolaceViewVars('tmp:TransitDaysToStore',tmp:TransitDaysToStore,'UpdateSIDSCAccountRegion',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:RegionName:Prompt;  SolaceCtrlName = '?tmp:RegionName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:RegionName;  SolaceCtrlName = '?tmp:RegionName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountNo:Prompt;  SolaceCtrlName = '?tmp:AccountNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountNo;  SolaceCtrlName = '?tmp:AccountNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitDaysException;  SolaceCtrlName = '?tmp:TransitDaysException';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitDaysToSC:Prompt;  SolaceCtrlName = '?tmp:TransitDaysToSC:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitDaysToSC;  SolaceCtrlName = '?tmp:TransitDaysToSC';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitDaysToStore:Prompt;  SolaceCtrlName = '?tmp:TransitDaysToStore:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitDaysToStore;  SolaceCtrlName = '?tmp:TransitDaysToStore';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSIDSCAccountRegion')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSIDSCAccountRegion')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:RegionName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCREG.Open
  Access:SIDACREG.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ! After Opening Window
  
  tmp:SCAccountID = scAccountID
  
  Access:ACCREG.ClearKey(acg:AccRegIDKey)
  acg:AccRegID = accRegID
  if Access:ACCREG.Fetch(acg:AccRegIDKey) = Level:Benign
      tmp:AccountNo = acg:AccountNo
      tmp:RegionName = acg:RegionName
  end
  
  Access:SIDACREG.ClearKey(sar:SCAccRegIDKey)
  sar:SCAccountID = tmp:SCAccountID
  sar:AccRegID = accRegID
  if Access:SIDACREG.Fetch(sar:SCAccRegIDKey) = Level:Benign
      tmp:TransitDaysException = sar:TransitDaysException
      tmp:TransitDaysToSC = sar:TransitDaysToSC
      tmp:TransitDaysToStore = sar:TransitDaysToStore
  end
  
  if tmp:TransitDaysException = true
      if tmp:TransitDaysToSC = 0 then tmp:TransitDaysToSC = 1.
      if tmp:TransitDaysToStore = 0 then tmp:TransitDaysToStore = 1.
  end
  Do RecolourWindow
  ! support for CPCS
  IF ?tmp:TransitDaysException{Prop:Checked} = True
    ENABLE(?tmp:TransitDaysToSC)
    ENABLE(?tmp:TransitDaysToStore)
  END
  IF ?tmp:TransitDaysException{Prop:Checked} = False
    DISABLE(?tmp:TransitDaysToSC)
    DISABLE(?tmp:TransitDaysToStore)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCREG.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSIDSCAccountRegion',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      ! OK Button
      
      if tmp:TransitDaysException = true
          if tmp:TransitDaysToSC = 0
              select(?tmp:TransitDaysToSC)
              cycle
          end
          if tmp:TransitDaysToStore = 0
              select(?tmp:TransitDaysToStore)
              cycle
          end
      end
      
      Access:SIDACREG.ClearKey(sar:SCAccRegIDKey)
      sar:SCAccountID = tmp:SCAccountID
      sar:AccRegID = accRegID
      if Access:SIDACREG.Fetch(sar:SCAccRegIDKey) = Level:Benign
          sar:TransitDaysException = tmp:TransitDaysException
          sar:TransitDaysToSC = tmp:TransitDaysToSC
          sar:TransitDaysToStore = tmp:TransitDaysToStore
          Access:SIDACREG.Update()
      else
          if Access:SIDACREG.PrimeRecord() = Level:Benign
              sar:SCAccountID = tmp:SCAccountID
              sar:AccRegID = accRegID
              sar:TransitDaysException = tmp:TransitDaysException
              sar:TransitDaysToSC = tmp:TransitDaysToSC
              sar:TransitDaysToStore = tmp:TransitDaysToStore
              if Access:SIDACREG.Update() <> Level:Benign
                  Access:SIDACREG.CancelAutoInc()
              end
          end
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:TransitDaysException
      IF ?tmp:TransitDaysException{Prop:Checked} = True
        ENABLE(?tmp:TransitDaysToSC)
        ENABLE(?tmp:TransitDaysToStore)
      END
      IF ?tmp:TransitDaysException{Prop:Checked} = False
        DISABLE(?tmp:TransitDaysToSC)
        DISABLE(?tmp:TransitDaysToStore)
      END
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSIDSCAccountRegion')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

