

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02048.INC'),ONCE        !Local module procedure declarations
                     END


BrowseSIDModelsToServiceCentre PROCEDURE (scAccountID, turnaroundDaysRetail, turnaroundDaysExch) !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::10:TAGFLAG         BYTE(0)
DASBRW::10:TAGMOUSE        BYTE(0)
DASBRW::10:TAGDISPSTATUS   BYTE(0)
DASBRW::10:QUEUE          QUEUE
Model_Number_Pointer          LIKE(GLO:Model_Number_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
tmp:SCAccountID      LONG
tmp:TurnaroundDaysRetail LONG
tmp:TurnaroundDaysExch LONG
tmp:Retail           STRING(1)
tmp:Exchange         STRING(1)
tmp:BookingType      STRING(1)
tmp:ServiceCentreName STRING(30)
tmp:ServiceCentreNameFilter STRING(30)
tmp:SCAccountIDFilter LONG
tmp:SingleSC         BYTE
tmp:Tag              STRING(1)
tmp:SingleManufacturerRetail BYTE
tmp:ManufacturerFilterRetail STRING(30)
tmp:SingleManufacturerExchange BYTE
tmp:ManufacturerFilterExchange STRING(30)
tmp:SingleManufacturerBTB BYTE
tmp:ManufacturerFilterBTB STRING(30)
tmp:SingleManufacturer28 BYTE
tmp:ManufacturerFilter28 STRING(30)
tmp:SingleManufacturerReturns BYTE
tmp:ManufacturerFilterReturns STRING(30)
tmp:SingleManufacturerOtherReturns BYTE
tmp:ManufacturerFilterOtherReturns STRING(30)
tmp:SingleManufacturer28Exchange BYTE
tmp:ManufacturerFilter28Exchange STRING(30)
tmp:SingleManufacturerLeaseReturns BYTE
tmp:ManufacturerFilterLeaseReturns STRING(30)
tmp:SingleManufacturerRouters LONG
tmp:ManufacturerFilterRouters STRING(30)
tmp:SingleManufacturerTab10 LONG
tmp:SingleManufacturerTab11 LONG
tmp:SingleManufacturerTab12 LONG
tmp:SingleManufacturerTab13 LONG
tmp:ManufacturerFilterTab10 STRING(30)
tmp:ManufacturerFilterTab11 STRING(30)
tmp:ManufacturerFilterTab12 STRING(30)
tmp:ManufacturerFilterTab13 STRING(30)
SCQueue              QUEUE,PRE(scq)
SCAccountID          LONG
ServiceCentreName    STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterRetail
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:IsVodafoneSpecific LIKE(man:IsVodafoneSpecific)   !Browse hot field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ServiceCentreNameFilter
srv_ali:ServiceCentreName LIKE(srv_ali:ServiceCentreName) !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterExchange
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:IsCCentreSpecific  LIKE(man:IsCCentreSpecific)    !Browse hot field - type derived from field
man:VodafoneISPAccount LIKE(man:VodafoneISPAccount)   !Browse hot field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterBTB
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:IsCCentreSpecific  LIKE(man:IsCCentreSpecific)    !Browse hot field - type derived from field
man:VodafoneISPAccount LIKE(man:VodafoneISPAccount)   !Browse hot field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ManufacturerFilter28
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:IsVodafoneSpecific LIKE(man:IsVodafoneSpecific)   !Browse hot field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterReturns
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:IsVodafoneSpecific LIKE(man:IsVodafoneSpecific)   !Browse hot field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:6 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterOtherReturns
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:IsVodafoneSpecific LIKE(man:IsVodafoneSpecific)   !Browse hot field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:7 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ManufacturerFilter28Exchange
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:IsVodafoneSpecific LIKE(man:IsVodafoneSpecific)   !Browse hot field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:8 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterLeaseReturns
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:9 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterRouters
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:10 QUEUE                          !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterTab10
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:11 QUEUE                          !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterTab11
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:12 QUEUE                          !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterTab12
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:13 QUEUE                          !Queue declaration for browse/combo box using ?tmp:ManufacturerFilterTab13
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                       PROJECT(mod:IsVodafoneSpecific)
                       PROJECT(mod:CBUPostal)
                       PROJECT(mod:CBUExchange)
                       PROJECT(mod:ISPPostal)
                       PROJECT(mod:ISPExchange)
                       PROJECT(mod:EBUPostal)
                       PROJECT(mod:EBUExchange)
                       PROJECT(mod:TabletDevice)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !List box control field - type derived from field
tmp:ServiceCentreName  LIKE(tmp:ServiceCentreName)    !List box control field - type derived from local data
mod:IsVodafoneSpecific LIKE(mod:IsVodafoneSpecific)   !Browse hot field - type derived from field
mod:CBUPostal          LIKE(mod:CBUPostal)            !Browse hot field - type derived from field
mod:CBUExchange        LIKE(mod:CBUExchange)          !Browse hot field - type derived from field
mod:ISPPostal          LIKE(mod:ISPPostal)            !Browse hot field - type derived from field
mod:ISPExchange        LIKE(mod:ISPExchange)          !Browse hot field - type derived from field
mod:EBUPostal          LIKE(mod:EBUPostal)            !Browse hot field - type derived from field
mod:EBUExchange        LIKE(mod:EBUExchange)          !Browse hot field - type derived from field
mod:TabletDevice       LIKE(mod:TabletDevice)         !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB8::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:IsVodafoneSpecific)
                       PROJECT(man:RecordNumber)
                     END
FDCB9::View:FileDropCombo VIEW(SIDSRVCN_ALIAS)
                       PROJECT(srv_ali:ServiceCentreName)
                     END
FDCB11::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:IsCCentreSpecific)
                       PROJECT(man:VodafoneISPAccount)
                       PROJECT(man:RecordNumber)
                     END
FDCB12::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:IsCCentreSpecific)
                       PROJECT(man:VodafoneISPAccount)
                       PROJECT(man:RecordNumber)
                     END
FDCB14::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:IsVodafoneSpecific)
                       PROJECT(man:RecordNumber)
                     END
FDCB15::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:IsVodafoneSpecific)
                       PROJECT(man:RecordNumber)
                     END
FDCB16::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:IsVodafoneSpecific)
                       PROJECT(man:RecordNumber)
                     END
FDCB17::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:IsVodafoneSpecific)
                       PROJECT(man:RecordNumber)
                     END
FDCB18::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
FDCB19::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
FDCB20::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
FDCB21::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
FDCB22::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
FDCB23::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
QuickWindow          WINDOW('Browse Models Attached To Service Centres'),AT(,,639,276),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('BrowseRegions'),SYSTEM,GRAY,DOUBLE
                       PANEL,AT(4,4,548,22),USE(?Panel1),FILL(COLOR:Gray)
                       PROMPT('Service Centre'),AT(8,10),USE(?ServiceCentre:Prompt),TRN,FONT('Tahoma',8,COLOR:White,FONT:regular,CHARSET:ANSI)
                       OPTION,AT(60,10,72,10),USE(tmp:SingleSC),FONT('Tahoma',8,COLOR:White,FONT:regular,CHARSET:ANSI)
                         RADIO('Single'),AT(97,10),USE(?Option2:Radio4),TRN,VALUE('1')
                         RADIO('All'),AT(64,10),USE(?Option2:Radio3),TRN,VALUE('0')
                       END
                       COMBO(@s30),AT(140,10,124,10),USE(tmp:ServiceCentreNameFilter),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                       LIST,AT(8,98,540,150),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('10L(2)J@s1@120L(2)|M~Model Number~@s30@120L(2)|M~Manufacturer~@s30@120L(2)|M~Ser' &|
   'vice Centre~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('Turnaround Exceptions'),AT(560,82,76,20),USE(?BtnExceptions),LEFT,ICON('moving.ico')
                       BUTTON('&Select'),AT(560,120,76,20),USE(?Select:2),DISABLE,HIDE,LEFT,ICON('select.ico')
                       BUTTON('&Insert'),AT(560,144,76,20),USE(?Insert:3),DISABLE,HIDE,LEFT,ICON('insert.ico')
                       BUTTON('&Change'),AT(560,168,76,20),USE(?Change:3),DISABLE,HIDE,LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(560,192,76,20),USE(?Delete:3),DISABLE,HIDE,LEFT,ICON('delete.ico')
                       SHEET,AT(4,34,548,238),USE(?CurrentTab),SPREAD
                         TAB('By Retail (Fonecare)'),USE(?Tab1)
                           PROMPT('Manufacturer'),AT(8,70),USE(?ManufacturerRetail:Prompt),TRN,FONT('Tahoma',8,COLOR:White,FONT:regular,CHARSET:ANSI)
                           OPTION,AT(60,70,72,10),USE(tmp:SingleManufacturerRetail),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             RADIO('All'),AT(64,70),USE(?Option1:Radio1),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('0')
                             RADIO('Single'),AT(97,70),USE(?Option1:Radio2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('1')
                           END
                           COMBO(@s30),AT(140,70,124,10),USE(tmp:ManufacturerFilterRetail),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L|@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Exchange (Refurb)'),USE(?Tab2)
                           PROMPT('Manufacturer'),AT(8,70),USE(?ManufacturerExchange:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           OPTION,AT(60,70,72,10),USE(tmp:SingleManufacturerExchange),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             RADIO('All'),AT(64,70),USE(?Option3:Radio5),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('0')
                             RADIO('Single'),AT(97,70),USE(?Option3:Radio6),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('1')
                           END
                           COMBO(@s30),AT(140,70,124,10),USE(tmp:ManufacturerFilterExchange),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L|@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Postal'),USE(?Tab3)
                           PROMPT('Manufacturer'),AT(8,70),USE(?ManufacturerBTB:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           OPTION,AT(60,70,72,10),USE(tmp:SingleManufacturerBTB),TRN
                             RADIO('All'),AT(64,70),USE(?Option4:Radio7),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('0')
                             RADIO('Single'),AT(97,70),USE(?Option4:Radio8),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('1')
                           END
                           COMBO(@s30),AT(140,70,124,10),USE(tmp:ManufacturerFilterBTB),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L|@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By 7 Day (COM)'),USE(?Tab4)
                           OPTION,AT(60,70,72,10),USE(tmp:SingleManufacturer28),TRN
                             RADIO('All'),AT(64,70),USE(?Option4:Radio7:2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('0')
                             RADIO('Single'),AT(97,70),USE(?Option4:Radio8:2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('1')
                           END
                           COMBO(@s30),AT(140,70,124,10),USE(tmp:ManufacturerFilter28),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L|@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4)
                           PROMPT('Manufacturer'),AT(8,70),USE(?Manufacturer28:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:4),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Returns'),USE(?Tab5)
                           OPTION,AT(60,70,72,10),USE(tmp:SingleManufacturerReturns),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             RADIO('All'),AT(64,70),USE(?Option3:Radio5:2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('0')
                             RADIO('Single'),AT(97,70),USE(?Option3:Radio6:2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('1')
                           END
                           COMBO(@s30),AT(140,70,124,10),USE(tmp:ManufacturerFilterReturns),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L|@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:5)
                           PROMPT('Manufacturer'),AT(8,70),USE(?ManufacturerReturns:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:5),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Lease Returns'),USE(?Tab8)
                           PROMPT('Manufacturer'),AT(8,70),USE(?ManufacturerReturns:Prompt:2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           COMBO(@s30),AT(144,70,124,10),USE(tmp:ManufacturerFilterLeaseReturns),IMM,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:8)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:8),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           OPTION,AT(60,70,72,10),USE(tmp:SingleManufacturerLeaseReturns),TRN,FONT(,,,,CHARSET:ANSI)
                             RADIO('All'),AT(64,70),USE(?Option9:Radio1),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('0')
                             RADIO('Single'),AT(96,70),USE(?Option9:Radio2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('1')
                           END
                         END
                         TAB('By Other Returns'),USE(?Tab6)
                           OPTION,AT(60,70,72,10),USE(tmp:SingleManufacturerOtherReturns),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             RADIO('Single'),AT(97,70),USE(?Option7:Radio2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('1')
                             RADIO('All'),AT(64,70),USE(?Option7:Radio1),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('0')
                           END
                           COMBO(@s30),AT(140,70,124,10),USE(tmp:ManufacturerFilterOtherReturns),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L|@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:6)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:6),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Manufacturer'),AT(8,70),USE(?ManufacturerOtherReturns:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                         END
                         TAB('By 7 Day Exchange'),USE(?Tab7)
                           PROMPT('Manufacturer'),AT(8,70),USE(?Manufacturer28Exchange:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           OPTION,AT(60,70,72,10),USE(tmp:SingleManufacturer28Exchange),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             RADIO('All'),AT(64,70),USE(?Option8:Radio1),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('0')
                             RADIO('Single'),AT(97,70),USE(?Option8:Radio2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('1')
                           END
                           COMBO(@s30),AT(140,70,124,10),USE(tmp:ManufacturerFilter28Exchange),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L|@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:7)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:7),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Router Returns'),USE(?Tab9)
                           OPTION,AT(52,65,76,20),USE(tmp:SingleManufacturerRouters),TRN
                             RADIO('All'),AT(60,68),USE(?tmp:SingleManufacturerRouters:Radio1),TRN,VALUE('0')
                             RADIO('Single'),AT(88,68),USE(?tmp:SingleManufacturerRouters:Radio2),TRN,VALUE('1')
                           END
                           PROMPT('Manufacturer'),AT(8,68),USE(?Prompt10)
                           COMBO(@s30),AT(136,68,124,10),USE(tmp:ManufacturerFilterRouters),IMM,VSCROLL,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:9)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:9),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('By Accessory Retail Repair'),USE(?Tab10)
                           PROMPT('Manufacturer'),AT(8,68),USE(?Prompt11)
                           OPTION('tmp : Single Manufacturer Tab 10'),AT(52,66,52,16),USE(tmp:SingleManufacturerTab10)
                             RADIO('All'),AT(60,69),USE(?tmp:SingleManufacturerTab10:Radio1),VALUE('0')
                             RADIO('Single'),AT(88,68),USE(?tmp:SingleManufacturerTab10:Radio2),VALUE('1')
                           END
                           COMBO(@s30),AT(132,68,124,10),USE(tmp:ManufacturerFilterTab10),IMM,VSCROLL,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:10)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:10),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('By Accessory Contact Centre Repair'),USE(?Tab11)
                           OPTION('tmp : Single Manufacturer Tab 11'),AT(60,64,52,18),USE(tmp:SingleManufacturerTab11)
                             RADIO('All'),AT(64,68),USE(?tmp:SingleManufacturerTab11:Radio1),VALUE('0')
                             RADIO('Single'),AT(88,68),USE(?tmp:SingleManufacturerTab11:Radio2),VALUE('1')
                           END
                           PROMPT('Manufacturer'),AT(8,68),USE(?Prompt12)
                           COMBO(@s30),AT(132,68,124,10),USE(tmp:ManufacturerFilterTab11),IMM,VSCROLL,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:11)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:11),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('By Accessory Returns (Retail)'),USE(?Tab12)
                           OPTION('tmp : Single Manufacturer Tab 12'),AT(56,64,52,16),USE(tmp:SingleManufacturerTab12)
                             RADIO('Single'),AT(85,68),USE(?tmp:SingleManufacturerTab12:Radio2),VALUE('1')
                             RADIO('All'),AT(61,68),USE(?tmp:SingleManufacturerTab12:Radio1),VALUE('0')
                           END
                           PROMPT('Manufacturer'),AT(8,68),USE(?Prompt13)
                           COMBO(@s30),AT(128,68,124,10),USE(tmp:ManufacturerFilterTab12),IMM,VSCROLL,FORMAT('120L|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:12)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:12),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('By Accessory Returns (Contact Centre)'),USE(?Tab13)
                           OPTION,AT(56,64,52,18),USE(tmp:SingleManufacturerTab13)
                             RADIO('All'),AT(60,68),USE(?tmp:SingleManufacturerTab13:Radio1),VALUE('0')
                             RADIO('Single'),AT(84,68),USE(?tmp:SingleManufacturerTab13:Radio2),VALUE('1')
                           END
                           PROMPT('Manufacturer'),AT(8,68),USE(?Prompt14)
                           COMBO(@s30),AT(124,68,124,10),USE(tmp:ManufacturerFilterTab13),IMM,VSCROLL,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:13)
                           ENTRY(@s30),AT(8,84,124,10),USE(mod:Model_Number,,?mod:Model_Number:13),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('&Tag'),AT(8,252,56,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                       BUTTON('Tag &All'),AT(68,252,56,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                       BUTTON('&Untag All'),AT(128,252,56,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                       BUTTON('sho&W tags'),AT(372,232,56,16),USE(?DASSHOWTAG),DISABLE,HIDE
                       BUTTON('&Rev tags'),AT(372,232,56,16),USE(?DASREVTAG),DISABLE,HIDE
                       BUTTON('Process Models'),AT(560,58,76,20),USE(?BtnProcess),LEFT,ICON('moving.ico')
                       BUTTON('Close'),AT(560,252,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 1 and tmp:SingleManufacturerRetail = 1
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 2 and tmp:SingleManufacturerExchange = 0
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 2 and tmp:SingleManufacturerExchange = 1
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 3 and tmp:SingleManufacturerBTB = 0
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 3 and tmp:SingleManufacturerBTB = 1
BRW1::Sort6:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 4 and tmp:SingleManufacturer28 = 0
BRW1::Sort8:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 4 and tmp:SingleManufacturer28 = 1
BRW1::Sort7:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 5 and tmp:SingleManufacturerReturns = 0
BRW1::Sort9:Locator  IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 5 and tmp:SingleManufacturerReturns = 1
BRW1::Sort14:Locator EntryLocatorClass                !Conditional Locator - choice(?CurrentTab) = 6 and tmp:SingleManufacturerLeaseReturns = 0
BRW1::Sort15:Locator EntryLocatorClass                !Conditional Locator - choice(?CurrentTab) = 6 and tmp:SingleManufacturerLeaseReturns = 1
BRW1::Sort10:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 7 and tmp:SingleManufacturerOtherReturns = 0
BRW1::Sort11:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 7 and tmp:SingleManufacturerOtherReturns = 1
BRW1::Sort12:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 8 and tmp:SingleManufacturer28Exchange = 0
BRW1::Sort13:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 8 and tmp:SingleManufacturer28Exchange = 1
BRW1::Sort16:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 9 and tmp:SingleManufacturerRouters = 0
BRW1::Sort17:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) = 9 and tmp:SingleManufacturerRouters = 1
BRW1::Sort18:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) =10 and tmp:SingleManufacturerTab10 = 0
BRW1::Sort19:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) =11 and tmp:SingleManufacturerTab11 = 0
BRW1::Sort20:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) =12 and tmp:SingleManufacturerTab12 = 0
BRW1::Sort21:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) =13 and tmp:SingleManufacturerTab13 = 0
BRW1::Sort22:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) =10 and tmp:SingleManufacturerTab10 = 1
BRW1::Sort23:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) =11 and tmp:SingleManufacturerTab11 = 1
BRW1::Sort24:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) =12 and tmp:SingleManufacturerTab12 = 1
BRW1::Sort25:Locator IncrementalLocatorClass          !Conditional Locator - choice(?CurrentTab) =13 and tmp:SingleManufacturerTab13 = 1
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

FDCB9                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

FDCB12               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

FDCB14               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

FDCB15               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

FDCB16               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:6         !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

FDCB17               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:7         !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

FDCB18               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:8         !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

FDCB19               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:9         !Reference to browse queue type
                     END

FDCB20               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:10        !Reference to browse queue type
                     END

FDCB21               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:11        !Reference to browse queue type
                     END

FDCB22               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:12        !Reference to browse queue type
                     END

FDCB23               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:13        !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1{prop:Fill} = 15066597

    ?ServiceCentre:Prompt{prop:FontColor} = -1
    ?ServiceCentre:Prompt{prop:Color} = 15066597
    ?tmp:SingleSC{prop:Font,3} = -1
    ?tmp:SingleSC{prop:Color} = 15066597
    ?tmp:SingleSC{prop:Trn} = 0
    ?Option2:Radio4{prop:Font,3} = -1
    ?Option2:Radio4{prop:Color} = 15066597
    ?Option2:Radio4{prop:Trn} = 0
    ?Option2:Radio3{prop:Font,3} = -1
    ?Option2:Radio3{prop:Color} = 15066597
    ?Option2:Radio3{prop:Trn} = 0
    If ?tmp:ServiceCentreNameFilter{prop:ReadOnly} = True
        ?tmp:ServiceCentreNameFilter{prop:FontColor} = 65793
        ?tmp:ServiceCentreNameFilter{prop:Color} = 15066597
    Elsif ?tmp:ServiceCentreNameFilter{prop:Req} = True
        ?tmp:ServiceCentreNameFilter{prop:FontColor} = 65793
        ?tmp:ServiceCentreNameFilter{prop:Color} = 8454143
    Else ! If ?tmp:ServiceCentreNameFilter{prop:Req} = True
        ?tmp:ServiceCentreNameFilter{prop:FontColor} = 65793
        ?tmp:ServiceCentreNameFilter{prop:Color} = 16777215
    End ! If ?tmp:ServiceCentreNameFilter{prop:Req} = True
    ?tmp:ServiceCentreNameFilter{prop:Trn} = 0
    ?tmp:ServiceCentreNameFilter{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?ManufacturerRetail:Prompt{prop:FontColor} = -1
    ?ManufacturerRetail:Prompt{prop:Color} = 15066597
    ?tmp:SingleManufacturerRetail{prop:Font,3} = -1
    ?tmp:SingleManufacturerRetail{prop:Color} = 15066597
    ?tmp:SingleManufacturerRetail{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    If ?tmp:ManufacturerFilterRetail{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterRetail{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterRetail{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterRetail{prop:Req} = True
        ?tmp:ManufacturerFilterRetail{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterRetail{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterRetail{prop:Req} = True
        ?tmp:ManufacturerFilterRetail{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterRetail{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterRetail{prop:Req} = True
    ?tmp:ManufacturerFilterRetail{prop:Trn} = 0
    ?tmp:ManufacturerFilterRetail{prop:FontStyle} = font:Bold
    If ?mod:Model_Number{prop:ReadOnly} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 15066597
    Elsif ?mod:Model_Number{prop:Req} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 8454143
    Else ! If ?mod:Model_Number{prop:Req} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 16777215
    End ! If ?mod:Model_Number{prop:Req} = True
    ?mod:Model_Number{prop:Trn} = 0
    ?mod:Model_Number{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?ManufacturerExchange:Prompt{prop:FontColor} = -1
    ?ManufacturerExchange:Prompt{prop:Color} = 15066597
    ?tmp:SingleManufacturerExchange{prop:Font,3} = -1
    ?tmp:SingleManufacturerExchange{prop:Color} = 15066597
    ?tmp:SingleManufacturerExchange{prop:Trn} = 0
    ?Option3:Radio5{prop:Font,3} = -1
    ?Option3:Radio5{prop:Color} = 15066597
    ?Option3:Radio5{prop:Trn} = 0
    ?Option3:Radio6{prop:Font,3} = -1
    ?Option3:Radio6{prop:Color} = 15066597
    ?Option3:Radio6{prop:Trn} = 0
    If ?tmp:ManufacturerFilterExchange{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterExchange{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterExchange{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterExchange{prop:Req} = True
        ?tmp:ManufacturerFilterExchange{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterExchange{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterExchange{prop:Req} = True
        ?tmp:ManufacturerFilterExchange{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterExchange{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterExchange{prop:Req} = True
    ?tmp:ManufacturerFilterExchange{prop:Trn} = 0
    ?tmp:ManufacturerFilterExchange{prop:FontStyle} = font:Bold
    If ?mod:Model_Number:2{prop:ReadOnly} = True
        ?mod:Model_Number:2{prop:FontColor} = 65793
        ?mod:Model_Number:2{prop:Color} = 15066597
    Elsif ?mod:Model_Number:2{prop:Req} = True
        ?mod:Model_Number:2{prop:FontColor} = 65793
        ?mod:Model_Number:2{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:2{prop:Req} = True
        ?mod:Model_Number:2{prop:FontColor} = 65793
        ?mod:Model_Number:2{prop:Color} = 16777215
    End ! If ?mod:Model_Number:2{prop:Req} = True
    ?mod:Model_Number:2{prop:Trn} = 0
    ?mod:Model_Number:2{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    ?ManufacturerBTB:Prompt{prop:FontColor} = -1
    ?ManufacturerBTB:Prompt{prop:Color} = 15066597
    ?tmp:SingleManufacturerBTB{prop:Font,3} = -1
    ?tmp:SingleManufacturerBTB{prop:Color} = 15066597
    ?tmp:SingleManufacturerBTB{prop:Trn} = 0
    ?Option4:Radio7{prop:Font,3} = -1
    ?Option4:Radio7{prop:Color} = 15066597
    ?Option4:Radio7{prop:Trn} = 0
    ?Option4:Radio8{prop:Font,3} = -1
    ?Option4:Radio8{prop:Color} = 15066597
    ?Option4:Radio8{prop:Trn} = 0
    If ?tmp:ManufacturerFilterBTB{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterBTB{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterBTB{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterBTB{prop:Req} = True
        ?tmp:ManufacturerFilterBTB{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterBTB{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterBTB{prop:Req} = True
        ?tmp:ManufacturerFilterBTB{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterBTB{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterBTB{prop:Req} = True
    ?tmp:ManufacturerFilterBTB{prop:Trn} = 0
    ?tmp:ManufacturerFilterBTB{prop:FontStyle} = font:Bold
    If ?mod:Model_Number:3{prop:ReadOnly} = True
        ?mod:Model_Number:3{prop:FontColor} = 65793
        ?mod:Model_Number:3{prop:Color} = 15066597
    Elsif ?mod:Model_Number:3{prop:Req} = True
        ?mod:Model_Number:3{prop:FontColor} = 65793
        ?mod:Model_Number:3{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:3{prop:Req} = True
        ?mod:Model_Number:3{prop:FontColor} = 65793
        ?mod:Model_Number:3{prop:Color} = 16777215
    End ! If ?mod:Model_Number:3{prop:Req} = True
    ?mod:Model_Number:3{prop:Trn} = 0
    ?mod:Model_Number:3{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    ?tmp:SingleManufacturer28{prop:Font,3} = -1
    ?tmp:SingleManufacturer28{prop:Color} = 15066597
    ?tmp:SingleManufacturer28{prop:Trn} = 0
    ?Option4:Radio7:2{prop:Font,3} = -1
    ?Option4:Radio7:2{prop:Color} = 15066597
    ?Option4:Radio7:2{prop:Trn} = 0
    ?Option4:Radio8:2{prop:Font,3} = -1
    ?Option4:Radio8:2{prop:Color} = 15066597
    ?Option4:Radio8:2{prop:Trn} = 0
    If ?tmp:ManufacturerFilter28{prop:ReadOnly} = True
        ?tmp:ManufacturerFilter28{prop:FontColor} = 65793
        ?tmp:ManufacturerFilter28{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilter28{prop:Req} = True
        ?tmp:ManufacturerFilter28{prop:FontColor} = 65793
        ?tmp:ManufacturerFilter28{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilter28{prop:Req} = True
        ?tmp:ManufacturerFilter28{prop:FontColor} = 65793
        ?tmp:ManufacturerFilter28{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilter28{prop:Req} = True
    ?tmp:ManufacturerFilter28{prop:Trn} = 0
    ?tmp:ManufacturerFilter28{prop:FontStyle} = font:Bold
    ?Manufacturer28:Prompt{prop:FontColor} = -1
    ?Manufacturer28:Prompt{prop:Color} = 15066597
    If ?mod:Model_Number:4{prop:ReadOnly} = True
        ?mod:Model_Number:4{prop:FontColor} = 65793
        ?mod:Model_Number:4{prop:Color} = 15066597
    Elsif ?mod:Model_Number:4{prop:Req} = True
        ?mod:Model_Number:4{prop:FontColor} = 65793
        ?mod:Model_Number:4{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:4{prop:Req} = True
        ?mod:Model_Number:4{prop:FontColor} = 65793
        ?mod:Model_Number:4{prop:Color} = 16777215
    End ! If ?mod:Model_Number:4{prop:Req} = True
    ?mod:Model_Number:4{prop:Trn} = 0
    ?mod:Model_Number:4{prop:FontStyle} = font:Bold
    ?Tab5{prop:Color} = 15066597
    ?tmp:SingleManufacturerReturns{prop:Font,3} = -1
    ?tmp:SingleManufacturerReturns{prop:Color} = 15066597
    ?tmp:SingleManufacturerReturns{prop:Trn} = 0
    ?Option3:Radio5:2{prop:Font,3} = -1
    ?Option3:Radio5:2{prop:Color} = 15066597
    ?Option3:Radio5:2{prop:Trn} = 0
    ?Option3:Radio6:2{prop:Font,3} = -1
    ?Option3:Radio6:2{prop:Color} = 15066597
    ?Option3:Radio6:2{prop:Trn} = 0
    If ?tmp:ManufacturerFilterReturns{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterReturns{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterReturns{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterReturns{prop:Req} = True
        ?tmp:ManufacturerFilterReturns{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterReturns{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterReturns{prop:Req} = True
        ?tmp:ManufacturerFilterReturns{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterReturns{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterReturns{prop:Req} = True
    ?tmp:ManufacturerFilterReturns{prop:Trn} = 0
    ?tmp:ManufacturerFilterReturns{prop:FontStyle} = font:Bold
    ?ManufacturerReturns:Prompt{prop:FontColor} = -1
    ?ManufacturerReturns:Prompt{prop:Color} = 15066597
    If ?mod:Model_Number:5{prop:ReadOnly} = True
        ?mod:Model_Number:5{prop:FontColor} = 65793
        ?mod:Model_Number:5{prop:Color} = 15066597
    Elsif ?mod:Model_Number:5{prop:Req} = True
        ?mod:Model_Number:5{prop:FontColor} = 65793
        ?mod:Model_Number:5{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:5{prop:Req} = True
        ?mod:Model_Number:5{prop:FontColor} = 65793
        ?mod:Model_Number:5{prop:Color} = 16777215
    End ! If ?mod:Model_Number:5{prop:Req} = True
    ?mod:Model_Number:5{prop:Trn} = 0
    ?mod:Model_Number:5{prop:FontStyle} = font:Bold
    ?Tab8{prop:Color} = 15066597
    ?ManufacturerReturns:Prompt:2{prop:FontColor} = -1
    ?ManufacturerReturns:Prompt:2{prop:Color} = 15066597
    If ?tmp:ManufacturerFilterLeaseReturns{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterLeaseReturns{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterLeaseReturns{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterLeaseReturns{prop:Req} = True
        ?tmp:ManufacturerFilterLeaseReturns{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterLeaseReturns{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterLeaseReturns{prop:Req} = True
        ?tmp:ManufacturerFilterLeaseReturns{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterLeaseReturns{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterLeaseReturns{prop:Req} = True
    ?tmp:ManufacturerFilterLeaseReturns{prop:Trn} = 0
    ?tmp:ManufacturerFilterLeaseReturns{prop:FontStyle} = font:Bold
    If ?mod:Model_Number:8{prop:ReadOnly} = True
        ?mod:Model_Number:8{prop:FontColor} = 65793
        ?mod:Model_Number:8{prop:Color} = 15066597
    Elsif ?mod:Model_Number:8{prop:Req} = True
        ?mod:Model_Number:8{prop:FontColor} = 65793
        ?mod:Model_Number:8{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:8{prop:Req} = True
        ?mod:Model_Number:8{prop:FontColor} = 65793
        ?mod:Model_Number:8{prop:Color} = 16777215
    End ! If ?mod:Model_Number:8{prop:Req} = True
    ?mod:Model_Number:8{prop:Trn} = 0
    ?mod:Model_Number:8{prop:FontStyle} = font:Bold
    ?tmp:SingleManufacturerLeaseReturns{prop:Font,3} = -1
    ?tmp:SingleManufacturerLeaseReturns{prop:Color} = 15066597
    ?tmp:SingleManufacturerLeaseReturns{prop:Trn} = 0
    ?Option9:Radio1{prop:Font,3} = -1
    ?Option9:Radio1{prop:Color} = 15066597
    ?Option9:Radio1{prop:Trn} = 0
    ?Option9:Radio2{prop:Font,3} = -1
    ?Option9:Radio2{prop:Color} = 15066597
    ?Option9:Radio2{prop:Trn} = 0
    ?Tab6{prop:Color} = 15066597
    ?tmp:SingleManufacturerOtherReturns{prop:Font,3} = -1
    ?tmp:SingleManufacturerOtherReturns{prop:Color} = 15066597
    ?tmp:SingleManufacturerOtherReturns{prop:Trn} = 0
    ?Option7:Radio2{prop:Font,3} = -1
    ?Option7:Radio2{prop:Color} = 15066597
    ?Option7:Radio2{prop:Trn} = 0
    ?Option7:Radio1{prop:Font,3} = -1
    ?Option7:Radio1{prop:Color} = 15066597
    ?Option7:Radio1{prop:Trn} = 0
    If ?tmp:ManufacturerFilterOtherReturns{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterOtherReturns{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterOtherReturns{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterOtherReturns{prop:Req} = True
        ?tmp:ManufacturerFilterOtherReturns{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterOtherReturns{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterOtherReturns{prop:Req} = True
        ?tmp:ManufacturerFilterOtherReturns{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterOtherReturns{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterOtherReturns{prop:Req} = True
    ?tmp:ManufacturerFilterOtherReturns{prop:Trn} = 0
    ?tmp:ManufacturerFilterOtherReturns{prop:FontStyle} = font:Bold
    If ?mod:Model_Number:6{prop:ReadOnly} = True
        ?mod:Model_Number:6{prop:FontColor} = 65793
        ?mod:Model_Number:6{prop:Color} = 15066597
    Elsif ?mod:Model_Number:6{prop:Req} = True
        ?mod:Model_Number:6{prop:FontColor} = 65793
        ?mod:Model_Number:6{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:6{prop:Req} = True
        ?mod:Model_Number:6{prop:FontColor} = 65793
        ?mod:Model_Number:6{prop:Color} = 16777215
    End ! If ?mod:Model_Number:6{prop:Req} = True
    ?mod:Model_Number:6{prop:Trn} = 0
    ?mod:Model_Number:6{prop:FontStyle} = font:Bold
    ?ManufacturerOtherReturns:Prompt{prop:FontColor} = -1
    ?ManufacturerOtherReturns:Prompt{prop:Color} = 15066597
    ?Tab7{prop:Color} = 15066597
    ?Manufacturer28Exchange:Prompt{prop:FontColor} = -1
    ?Manufacturer28Exchange:Prompt{prop:Color} = 15066597
    ?tmp:SingleManufacturer28Exchange{prop:Font,3} = -1
    ?tmp:SingleManufacturer28Exchange{prop:Color} = 15066597
    ?tmp:SingleManufacturer28Exchange{prop:Trn} = 0
    ?Option8:Radio1{prop:Font,3} = -1
    ?Option8:Radio1{prop:Color} = 15066597
    ?Option8:Radio1{prop:Trn} = 0
    ?Option8:Radio2{prop:Font,3} = -1
    ?Option8:Radio2{prop:Color} = 15066597
    ?Option8:Radio2{prop:Trn} = 0
    If ?tmp:ManufacturerFilter28Exchange{prop:ReadOnly} = True
        ?tmp:ManufacturerFilter28Exchange{prop:FontColor} = 65793
        ?tmp:ManufacturerFilter28Exchange{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilter28Exchange{prop:Req} = True
        ?tmp:ManufacturerFilter28Exchange{prop:FontColor} = 65793
        ?tmp:ManufacturerFilter28Exchange{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilter28Exchange{prop:Req} = True
        ?tmp:ManufacturerFilter28Exchange{prop:FontColor} = 65793
        ?tmp:ManufacturerFilter28Exchange{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilter28Exchange{prop:Req} = True
    ?tmp:ManufacturerFilter28Exchange{prop:Trn} = 0
    ?tmp:ManufacturerFilter28Exchange{prop:FontStyle} = font:Bold
    If ?mod:Model_Number:7{prop:ReadOnly} = True
        ?mod:Model_Number:7{prop:FontColor} = 65793
        ?mod:Model_Number:7{prop:Color} = 15066597
    Elsif ?mod:Model_Number:7{prop:Req} = True
        ?mod:Model_Number:7{prop:FontColor} = 65793
        ?mod:Model_Number:7{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:7{prop:Req} = True
        ?mod:Model_Number:7{prop:FontColor} = 65793
        ?mod:Model_Number:7{prop:Color} = 16777215
    End ! If ?mod:Model_Number:7{prop:Req} = True
    ?mod:Model_Number:7{prop:Trn} = 0
    ?mod:Model_Number:7{prop:FontStyle} = font:Bold
    ?Tab9{prop:Color} = 15066597
    ?tmp:SingleManufacturerRouters{prop:Font,3} = -1
    ?tmp:SingleManufacturerRouters{prop:Color} = 15066597
    ?tmp:SingleManufacturerRouters{prop:Trn} = 0
    ?tmp:SingleManufacturerRouters:Radio1{prop:Font,3} = -1
    ?tmp:SingleManufacturerRouters:Radio1{prop:Color} = 15066597
    ?tmp:SingleManufacturerRouters:Radio1{prop:Trn} = 0
    ?tmp:SingleManufacturerRouters:Radio2{prop:Font,3} = -1
    ?tmp:SingleManufacturerRouters:Radio2{prop:Color} = 15066597
    ?tmp:SingleManufacturerRouters:Radio2{prop:Trn} = 0
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    If ?tmp:ManufacturerFilterRouters{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterRouters{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterRouters{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterRouters{prop:Req} = True
        ?tmp:ManufacturerFilterRouters{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterRouters{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterRouters{prop:Req} = True
        ?tmp:ManufacturerFilterRouters{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterRouters{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterRouters{prop:Req} = True
    ?tmp:ManufacturerFilterRouters{prop:Trn} = 0
    ?tmp:ManufacturerFilterRouters{prop:FontStyle} = font:Bold
    If ?mod:Model_Number:9{prop:ReadOnly} = True
        ?mod:Model_Number:9{prop:FontColor} = 65793
        ?mod:Model_Number:9{prop:Color} = 15066597
    Elsif ?mod:Model_Number:9{prop:Req} = True
        ?mod:Model_Number:9{prop:FontColor} = 65793
        ?mod:Model_Number:9{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:9{prop:Req} = True
        ?mod:Model_Number:9{prop:FontColor} = 65793
        ?mod:Model_Number:9{prop:Color} = 16777215
    End ! If ?mod:Model_Number:9{prop:Req} = True
    ?mod:Model_Number:9{prop:Trn} = 0
    ?mod:Model_Number:9{prop:FontStyle} = font:Bold
    ?Tab10{prop:Color} = 15066597
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab10{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab10{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab10{prop:Trn} = 0
    ?tmp:SingleManufacturerTab10:Radio1{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab10:Radio1{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab10:Radio1{prop:Trn} = 0
    ?tmp:SingleManufacturerTab10:Radio2{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab10:Radio2{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab10:Radio2{prop:Trn} = 0
    If ?tmp:ManufacturerFilterTab10{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterTab10{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab10{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterTab10{prop:Req} = True
        ?tmp:ManufacturerFilterTab10{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab10{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterTab10{prop:Req} = True
        ?tmp:ManufacturerFilterTab10{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab10{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterTab10{prop:Req} = True
    ?tmp:ManufacturerFilterTab10{prop:Trn} = 0
    ?tmp:ManufacturerFilterTab10{prop:FontStyle} = font:Bold
    If ?mod:Model_Number:10{prop:ReadOnly} = True
        ?mod:Model_Number:10{prop:FontColor} = 65793
        ?mod:Model_Number:10{prop:Color} = 15066597
    Elsif ?mod:Model_Number:10{prop:Req} = True
        ?mod:Model_Number:10{prop:FontColor} = 65793
        ?mod:Model_Number:10{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:10{prop:Req} = True
        ?mod:Model_Number:10{prop:FontColor} = 65793
        ?mod:Model_Number:10{prop:Color} = 16777215
    End ! If ?mod:Model_Number:10{prop:Req} = True
    ?mod:Model_Number:10{prop:Trn} = 0
    ?mod:Model_Number:10{prop:FontStyle} = font:Bold
    ?Tab11{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab11{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab11{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab11{prop:Trn} = 0
    ?tmp:SingleManufacturerTab11:Radio1{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab11:Radio1{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab11:Radio1{prop:Trn} = 0
    ?tmp:SingleManufacturerTab11:Radio2{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab11:Radio2{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab11:Radio2{prop:Trn} = 0
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    If ?tmp:ManufacturerFilterTab11{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterTab11{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab11{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterTab11{prop:Req} = True
        ?tmp:ManufacturerFilterTab11{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab11{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterTab11{prop:Req} = True
        ?tmp:ManufacturerFilterTab11{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab11{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterTab11{prop:Req} = True
    ?tmp:ManufacturerFilterTab11{prop:Trn} = 0
    ?tmp:ManufacturerFilterTab11{prop:FontStyle} = font:Bold
    If ?mod:Model_Number:11{prop:ReadOnly} = True
        ?mod:Model_Number:11{prop:FontColor} = 65793
        ?mod:Model_Number:11{prop:Color} = 15066597
    Elsif ?mod:Model_Number:11{prop:Req} = True
        ?mod:Model_Number:11{prop:FontColor} = 65793
        ?mod:Model_Number:11{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:11{prop:Req} = True
        ?mod:Model_Number:11{prop:FontColor} = 65793
        ?mod:Model_Number:11{prop:Color} = 16777215
    End ! If ?mod:Model_Number:11{prop:Req} = True
    ?mod:Model_Number:11{prop:Trn} = 0
    ?mod:Model_Number:11{prop:FontStyle} = font:Bold
    ?Tab12{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab12{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab12{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab12{prop:Trn} = 0
    ?tmp:SingleManufacturerTab12:Radio2{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab12:Radio2{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab12:Radio2{prop:Trn} = 0
    ?tmp:SingleManufacturerTab12:Radio1{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab12:Radio1{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab12:Radio1{prop:Trn} = 0
    ?Prompt13{prop:FontColor} = -1
    ?Prompt13{prop:Color} = 15066597
    If ?tmp:ManufacturerFilterTab12{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterTab12{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab12{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterTab12{prop:Req} = True
        ?tmp:ManufacturerFilterTab12{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab12{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterTab12{prop:Req} = True
        ?tmp:ManufacturerFilterTab12{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab12{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterTab12{prop:Req} = True
    ?tmp:ManufacturerFilterTab12{prop:Trn} = 0
    ?tmp:ManufacturerFilterTab12{prop:FontStyle} = font:Bold
    If ?mod:Model_Number:12{prop:ReadOnly} = True
        ?mod:Model_Number:12{prop:FontColor} = 65793
        ?mod:Model_Number:12{prop:Color} = 15066597
    Elsif ?mod:Model_Number:12{prop:Req} = True
        ?mod:Model_Number:12{prop:FontColor} = 65793
        ?mod:Model_Number:12{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:12{prop:Req} = True
        ?mod:Model_Number:12{prop:FontColor} = 65793
        ?mod:Model_Number:12{prop:Color} = 16777215
    End ! If ?mod:Model_Number:12{prop:Req} = True
    ?mod:Model_Number:12{prop:Trn} = 0
    ?mod:Model_Number:12{prop:FontStyle} = font:Bold
    ?Tab13{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab13{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab13{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab13{prop:Trn} = 0
    ?tmp:SingleManufacturerTab13:Radio1{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab13:Radio1{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab13:Radio1{prop:Trn} = 0
    ?tmp:SingleManufacturerTab13:Radio2{prop:Font,3} = -1
    ?tmp:SingleManufacturerTab13:Radio2{prop:Color} = 15066597
    ?tmp:SingleManufacturerTab13:Radio2{prop:Trn} = 0
    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    If ?tmp:ManufacturerFilterTab13{prop:ReadOnly} = True
        ?tmp:ManufacturerFilterTab13{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab13{prop:Color} = 15066597
    Elsif ?tmp:ManufacturerFilterTab13{prop:Req} = True
        ?tmp:ManufacturerFilterTab13{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab13{prop:Color} = 8454143
    Else ! If ?tmp:ManufacturerFilterTab13{prop:Req} = True
        ?tmp:ManufacturerFilterTab13{prop:FontColor} = 65793
        ?tmp:ManufacturerFilterTab13{prop:Color} = 16777215
    End ! If ?tmp:ManufacturerFilterTab13{prop:Req} = True
    ?tmp:ManufacturerFilterTab13{prop:Trn} = 0
    ?tmp:ManufacturerFilterTab13{prop:FontStyle} = font:Bold
    If ?mod:Model_Number:13{prop:ReadOnly} = True
        ?mod:Model_Number:13{prop:FontColor} = 65793
        ?mod:Model_Number:13{prop:Color} = 15066597
    Elsif ?mod:Model_Number:13{prop:Req} = True
        ?mod:Model_Number:13{prop:FontColor} = 65793
        ?mod:Model_Number:13{prop:Color} = 8454143
    Else ! If ?mod:Model_Number:13{prop:Req} = True
        ?mod:Model_Number:13{prop:FontColor} = 65793
        ?mod:Model_Number:13{prop:Color} = 16777215
    End ! If ?mod:Model_Number:13{prop:Req} = True
    ?mod:Model_Number:13{prop:Trn} = 0
    ?mod:Model_Number:13{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::10:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
   GET(glo:Q_ModelNumber,glo:Q_ModelNumber.Model_Number_Pointer)
  IF ERRORCODE()
     glo:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     ADD(glo:Q_ModelNumber,glo:Q_ModelNumber.Model_Number_Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Q_ModelNumber)
    tmp:Tag = ''
  END
    Queue:Browse:1.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse:1.tmp:Tag_Icon = 1
  ELSE
    Queue:Browse:1.tmp:Tag_Icon = 0
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::10:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Q_ModelNumber)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     ADD(glo:Q_ModelNumber,glo:Q_ModelNumber.Model_Number_Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::10:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Q_ModelNumber)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::10:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::10:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Q_ModelNumber)
    GET(glo:Q_ModelNumber,QR#)
    DASBRW::10:QUEUE = glo:Q_ModelNumber
    ADD(DASBRW::10:QUEUE)
  END
  FREE(glo:Q_ModelNumber)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::10:QUEUE.Model_Number_Pointer = mod:Model_Number
     GET(DASBRW::10:QUEUE,DASBRW::10:QUEUE.Model_Number_Pointer)
    IF ERRORCODE()
       glo:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
       ADD(glo:Q_ModelNumber,glo:Q_ModelNumber.Model_Number_Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::10:DASSHOWTAG Routine
   CASE DASBRW::10:TAGDISPSTATUS
   OF 0
      DASBRW::10:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::10:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::10:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Display:SCFilter    routine

    if tmp:SingleSC = false
        tmp:ServiceCentreNameFilter = ''
        tmp:SCAccountIDFilter = 0
        ?tmp:ServiceCentreNameFilter{Prop:Disable} = true
    else
        ?tmp:ServiceCentreNameFilter{Prop:Disable} = false
    end

Display:ManRetailFilter    routine

    if tmp:SingleManufacturerRetail = false
        tmp:ManufacturerFilterRetail = ''
        ?tmp:ManufacturerFilterRetail{Prop:Disable} = true
    else
        ?tmp:ManufacturerFilterRetail{Prop:Disable} = false
    end

Display:ManExchangeFilter    routine

    if tmp:SingleManufacturerExchange = false
        tmp:ManufacturerFilterExchange = ''
        ?tmp:ManufacturerFilterExchange{Prop:Disable} = true
    else
        ?tmp:ManufacturerFilterExchange{Prop:Disable} = false
    end

Display:ManBTBFilter    routine

    if tmp:SingleManufacturerBTB = false
        tmp:ManufacturerFilterBTB = ''
        ?tmp:ManufacturerFilterBTB{Prop:Disable} = true
    else
        ?tmp:ManufacturerFilterBTB{Prop:Disable} = false
    end

Display:Man28Filter    routine

    if tmp:SingleManufacturer28 = false
        tmp:ManufacturerFilter28 = ''
        ?tmp:ManufacturerFilter28{Prop:Disable} = true
    else
        ?tmp:ManufacturerFilter28{Prop:Disable} = false
    end

Display:ManReturnsFilter    routine

    if tmp:SingleManufacturerReturns = false
        tmp:ManufacturerFilterReturns = ''
        ?tmp:ManufacturerFilterReturns{Prop:Disable} = true
    else
        ?tmp:ManufacturerFilterReturns{Prop:Disable} = false
    end

Display:ManLeaseReturnsFilter    routine

    if tmp:SingleManufacturerLeaseReturns = false
        tmp:ManufacturerFilterLeaseReturns = ''
        ?tmp:ManufacturerFilterLeaseReturns{Prop:Disable} = true
    else
        ?tmp:ManufacturerFilterLeaseReturns{Prop:Disable} = false
    end

Display:ManOtherReturnsFilter    routine

    if tmp:SingleManufacturerOtherReturns = false
        tmp:ManufacturerFilterOtherReturns = ''
        ?tmp:ManufacturerFilterOtherReturns{Prop:Disable} = true
    else
        ?tmp:ManufacturerFilterOtherReturns{Prop:Disable} = false
    end

Display:Man28ExchangeFilter     routine

    if tmp:SingleManufacturer28Exchange = false
        tmp:ManufacturerFilter28Exchange = ''
        ?tmp:ManufacturerFilter28Exchange{Prop:Disable} = true
    else
        ?tmp:ManufacturerFilter28Exchange{Prop:Disable} = false
    end
Display:ManRouterFilter ROUTINE
    IF (tmp:SingleManufacturerRouters = FALSE)
        tmp:ManufacturerFilterRouters = ''
        ?tmp:ManufacturerFilterRouters{prop:Disable} = TRUE
    ELSE ! IF
        ?tmp:ManufacturerFilterRouters{prop:Disable} = FALSE
    END ! IF
Display:Tab10Filter ROUTINE
    IF (tmp:SingleManufacturerTab10 = FALSE)
        tmp:ManufacturerFilterTab10 = ''
        ?tmp:ManufacturerFilterTab10{prop:Disable} = TRUE
    ELSE ! IF
        ?tmp:ManufacturerFilterTab10{prop:Disable} = FALSE
    END ! IF
Display:Tab11Filter ROUTINE
    IF (tmp:SingleManufacturerTab11 = FALSE)
        tmp:ManufacturerFilterTab11 = ''
        ?tmp:ManufacturerFilterTab11{prop:Disable} = TRUE
    ELSE ! IF
        ?tmp:ManufacturerFilterTab11{prop:Disable} = FALSE
    END ! IF
Display:Tab12Filter ROUTINE
    IF (tmp:SingleManufacturerTab12 = FALSE)
        tmp:ManufacturerFilterTab12 = ''
        ?tmp:ManufacturerFilterTab12{prop:Disable} = TRUE
    ELSE ! IF
        ?tmp:ManufacturerFilterTab12{prop:Disable} = FALSE
    END ! IF
Display:Tab13Filter ROUTINE
    IF (tmp:SingleManufacturerTab13 = FALSE)
        tmp:ManufacturerFilterTab13 = ''
        ?tmp:ManufacturerFilterTab13{prop:Disable} = TRUE
    ELSE ! IF
        ?tmp:ManufacturerFilterTab13{prop:Disable} = FALSE
    END ! IF


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BrowseSIDModelsToServiceCentre',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SCAccountID',tmp:SCAccountID,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:TurnaroundDaysRetail',tmp:TurnaroundDaysRetail,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:TurnaroundDaysExch',tmp:TurnaroundDaysExch,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:Retail',tmp:Retail,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:Exchange',tmp:Exchange,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:BookingType',tmp:BookingType,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ServiceCentreName',tmp:ServiceCentreName,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ServiceCentreNameFilter',tmp:ServiceCentreNameFilter,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SCAccountIDFilter',tmp:SCAccountIDFilter,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleSC',tmp:SingleSC,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:Tag',tmp:Tag,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerRetail',tmp:SingleManufacturerRetail,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterRetail',tmp:ManufacturerFilterRetail,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerExchange',tmp:SingleManufacturerExchange,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterExchange',tmp:ManufacturerFilterExchange,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerBTB',tmp:SingleManufacturerBTB,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterBTB',tmp:ManufacturerFilterBTB,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturer28',tmp:SingleManufacturer28,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilter28',tmp:ManufacturerFilter28,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerReturns',tmp:SingleManufacturerReturns,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterReturns',tmp:ManufacturerFilterReturns,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerOtherReturns',tmp:SingleManufacturerOtherReturns,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterOtherReturns',tmp:ManufacturerFilterOtherReturns,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturer28Exchange',tmp:SingleManufacturer28Exchange,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilter28Exchange',tmp:ManufacturerFilter28Exchange,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerLeaseReturns',tmp:SingleManufacturerLeaseReturns,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterLeaseReturns',tmp:ManufacturerFilterLeaseReturns,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerRouters',tmp:SingleManufacturerRouters,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterRouters',tmp:ManufacturerFilterRouters,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerTab10',tmp:SingleManufacturerTab10,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerTab11',tmp:SingleManufacturerTab11,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerTab12',tmp:SingleManufacturerTab12,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:SingleManufacturerTab13',tmp:SingleManufacturerTab13,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterTab10',tmp:ManufacturerFilterTab10,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterTab11',tmp:ManufacturerFilterTab11,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterTab12',tmp:ManufacturerFilterTab12,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('tmp:ManufacturerFilterTab13',tmp:ManufacturerFilterTab13,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('SCQueue:SCAccountID',SCQueue:SCAccountID,'BrowseSIDModelsToServiceCentre',1)
    SolaceViewVars('SCQueue:ServiceCentreName',SCQueue:ServiceCentreName,'BrowseSIDModelsToServiceCentre',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ServiceCentre:Prompt;  SolaceCtrlName = '?ServiceCentre:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleSC;  SolaceCtrlName = '?tmp:SingleSC';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option2:Radio4;  SolaceCtrlName = '?Option2:Radio4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option2:Radio3;  SolaceCtrlName = '?Option2:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ServiceCentreNameFilter;  SolaceCtrlName = '?tmp:ServiceCentreNameFilter';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BtnExceptions;  SolaceCtrlName = '?BtnExceptions';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ManufacturerRetail:Prompt;  SolaceCtrlName = '?ManufacturerRetail:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerRetail;  SolaceCtrlName = '?tmp:SingleManufacturerRetail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio1;  SolaceCtrlName = '?Option1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio2;  SolaceCtrlName = '?Option1:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterRetail;  SolaceCtrlName = '?tmp:ManufacturerFilterRetail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number;  SolaceCtrlName = '?mod:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ManufacturerExchange:Prompt;  SolaceCtrlName = '?ManufacturerExchange:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerExchange;  SolaceCtrlName = '?tmp:SingleManufacturerExchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option3:Radio5;  SolaceCtrlName = '?Option3:Radio5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option3:Radio6;  SolaceCtrlName = '?Option3:Radio6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterExchange;  SolaceCtrlName = '?tmp:ManufacturerFilterExchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:2;  SolaceCtrlName = '?mod:Model_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ManufacturerBTB:Prompt;  SolaceCtrlName = '?ManufacturerBTB:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerBTB;  SolaceCtrlName = '?tmp:SingleManufacturerBTB';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option4:Radio7;  SolaceCtrlName = '?Option4:Radio7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option4:Radio8;  SolaceCtrlName = '?Option4:Radio8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterBTB;  SolaceCtrlName = '?tmp:ManufacturerFilterBTB';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:3;  SolaceCtrlName = '?mod:Model_Number:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturer28;  SolaceCtrlName = '?tmp:SingleManufacturer28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option4:Radio7:2;  SolaceCtrlName = '?Option4:Radio7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option4:Radio8:2;  SolaceCtrlName = '?Option4:Radio8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilter28;  SolaceCtrlName = '?tmp:ManufacturerFilter28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer28:Prompt;  SolaceCtrlName = '?Manufacturer28:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:4;  SolaceCtrlName = '?mod:Model_Number:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerReturns;  SolaceCtrlName = '?tmp:SingleManufacturerReturns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option3:Radio5:2;  SolaceCtrlName = '?Option3:Radio5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option3:Radio6:2;  SolaceCtrlName = '?Option3:Radio6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterReturns;  SolaceCtrlName = '?tmp:ManufacturerFilterReturns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ManufacturerReturns:Prompt;  SolaceCtrlName = '?ManufacturerReturns:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:5;  SolaceCtrlName = '?mod:Model_Number:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab8;  SolaceCtrlName = '?Tab8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ManufacturerReturns:Prompt:2;  SolaceCtrlName = '?ManufacturerReturns:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterLeaseReturns;  SolaceCtrlName = '?tmp:ManufacturerFilterLeaseReturns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:8;  SolaceCtrlName = '?mod:Model_Number:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerLeaseReturns;  SolaceCtrlName = '?tmp:SingleManufacturerLeaseReturns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option9:Radio1;  SolaceCtrlName = '?Option9:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option9:Radio2;  SolaceCtrlName = '?Option9:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerOtherReturns;  SolaceCtrlName = '?tmp:SingleManufacturerOtherReturns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option7:Radio2;  SolaceCtrlName = '?Option7:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option7:Radio1;  SolaceCtrlName = '?Option7:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterOtherReturns;  SolaceCtrlName = '?tmp:ManufacturerFilterOtherReturns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:6;  SolaceCtrlName = '?mod:Model_Number:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ManufacturerOtherReturns:Prompt;  SolaceCtrlName = '?ManufacturerOtherReturns:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab7;  SolaceCtrlName = '?Tab7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer28Exchange:Prompt;  SolaceCtrlName = '?Manufacturer28Exchange:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturer28Exchange;  SolaceCtrlName = '?tmp:SingleManufacturer28Exchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option8:Radio1;  SolaceCtrlName = '?Option8:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option8:Radio2;  SolaceCtrlName = '?Option8:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilter28Exchange;  SolaceCtrlName = '?tmp:ManufacturerFilter28Exchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:7;  SolaceCtrlName = '?mod:Model_Number:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab9;  SolaceCtrlName = '?Tab9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerRouters;  SolaceCtrlName = '?tmp:SingleManufacturerRouters';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerRouters:Radio1;  SolaceCtrlName = '?tmp:SingleManufacturerRouters:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerRouters:Radio2;  SolaceCtrlName = '?tmp:SingleManufacturerRouters:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterRouters;  SolaceCtrlName = '?tmp:ManufacturerFilterRouters';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:9;  SolaceCtrlName = '?mod:Model_Number:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab10;  SolaceCtrlName = '?Tab10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab10;  SolaceCtrlName = '?tmp:SingleManufacturerTab10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab10:Radio1;  SolaceCtrlName = '?tmp:SingleManufacturerTab10:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab10:Radio2;  SolaceCtrlName = '?tmp:SingleManufacturerTab10:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterTab10;  SolaceCtrlName = '?tmp:ManufacturerFilterTab10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:10;  SolaceCtrlName = '?mod:Model_Number:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab11;  SolaceCtrlName = '?Tab11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab11;  SolaceCtrlName = '?tmp:SingleManufacturerTab11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab11:Radio1;  SolaceCtrlName = '?tmp:SingleManufacturerTab11:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab11:Radio2;  SolaceCtrlName = '?tmp:SingleManufacturerTab11:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12;  SolaceCtrlName = '?Prompt12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterTab11;  SolaceCtrlName = '?tmp:ManufacturerFilterTab11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:11;  SolaceCtrlName = '?mod:Model_Number:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab12;  SolaceCtrlName = '?Tab12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab12;  SolaceCtrlName = '?tmp:SingleManufacturerTab12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab12:Radio2;  SolaceCtrlName = '?tmp:SingleManufacturerTab12:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab12:Radio1;  SolaceCtrlName = '?tmp:SingleManufacturerTab12:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt13;  SolaceCtrlName = '?Prompt13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterTab12;  SolaceCtrlName = '?tmp:ManufacturerFilterTab12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:12;  SolaceCtrlName = '?mod:Model_Number:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab13;  SolaceCtrlName = '?Tab13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab13;  SolaceCtrlName = '?tmp:SingleManufacturerTab13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab13:Radio1;  SolaceCtrlName = '?tmp:SingleManufacturerTab13:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SingleManufacturerTab13:Radio2;  SolaceCtrlName = '?tmp:SingleManufacturerTab13:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14;  SolaceCtrlName = '?Prompt14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ManufacturerFilterTab13;  SolaceCtrlName = '?tmp:ManufacturerFilterTab13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number:13;  SolaceCtrlName = '?mod:Model_Number:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BtnProcess;  SolaceCtrlName = '?BtnProcess';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseSIDModelsToServiceCentre')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BrowseSIDModelsToServiceCentre')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  Relate:SIDMODSC.Open
  Relate:SIDSRVCN_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MODELNUM,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  tmp:SCAccountID = scAccountID
  tmp:Retail = 'R'
  tmp:Exchange = 'E'
  
  ! Generate queue of service centre names
  set(srv_ali:SCAccountIDKey)
  loop
      if Access:SIDSRVCN_ALIAS.Next() <> Level:Benign then break.
      SCQueue.scq:SCAccountID = srv_ali:SCAccountID
      SCQueue.scq:ServiceCentreName = srv_ali:ServiceCentreName
      add(SCQueue, SCQueue.scq:SCAccountID)
  end
  
  if tmp:SCAccountID > 0
      tmp:SingleSC = true
      tmp:SCAccountIDFilter = tmp:SCAccountID
  
      SCQueue.scq:SCAccountID = tmp:SCAccountID
      get(SCQueue, SCQueue.scq:SCAccountID)
      if not error()
          tmp:ServiceCentreNameFilter = SCQueue.scq:ServiceCentreName
      end
  end
  
  do Display:SCFilter
  do Display:ManRetailFilter
  do Display:ManExchangeFilter
  do Display:ManBTBFilter
  do Display:Man28Filter
  do Display:ManReturnsFilter
  do Display:ManLeaseReturnsFilter
  do Display:ManOtherReturnsFilter
  do Display:Man28ExchangeFilter
  do Display:ManRouterFilter
  DO Display:Tab10Filter
  DO Display:Tab11Filter
  DO Display:Tab12Filter
  DO Display:Tab13Filter
  If SecurityCheck('TURNAROUND EXCEPTIONS')
      ?BtnExceptions{prop:Disable} = 1
  End ! If SecurityCheck('TURNAROUND EXCEPTIONS')
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterRetail)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?mod:Model_Number,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?mod:Model_Number:2,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterExchange)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?mod:Model_Number:2,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?mod:Model_Number:3,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterBTB)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?mod:Model_Number:3,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?mod:Model_Number:4,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilter28)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?mod:Model_Number:4,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?mod:Model_Number:5,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterReturns)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?mod:Model_Number:5,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?mod:Model_Number:8,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterLeaseReturns)
  BRW1.AddLocator(BRW1::Sort15:Locator)
  BRW1::Sort15:Locator.Init(?mod:Model_Number:8,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?mod:Model_Number:6,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterOtherReturns)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?mod:Model_Number:6,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?mod:Model_Number:7,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilter28Exchange)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(?mod:Model_Number:7,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort16:Locator)
  BRW1::Sort16:Locator.Init(?mod:Model_Number:9,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterRouters)
  BRW1.AddLocator(BRW1::Sort17:Locator)
  BRW1::Sort17:Locator.Init(?mod:Model_Number:9,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort18:Locator)
  BRW1::Sort18:Locator.Init(?mod:Model_Number:10,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort19:Locator)
  BRW1::Sort19:Locator.Init(?mod:Model_Number:11,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort20:Locator)
  BRW1::Sort20:Locator.Init(?mod:Model_Number:12,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort21:Locator)
  BRW1::Sort21:Locator.Init(?mod:Model_Number:13,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterTab10)
  BRW1.AddLocator(BRW1::Sort22:Locator)
  BRW1::Sort22:Locator.Init(?mod:Model_Number:10,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterTab11)
  BRW1.AddLocator(BRW1::Sort23:Locator)
  BRW1::Sort23:Locator.Init(?mod:Model_Number:11,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterTab12)
  BRW1.AddLocator(BRW1::Sort24:Locator)
  BRW1::Sort24:Locator.Init(?mod:Model_Number:12,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,tmp:ManufacturerFilterTab13)
  BRW1.AddLocator(BRW1::Sort25:Locator)
  BRW1::Sort25:Locator.Init(?mod:Model_Number:13,mod:Model_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mod:Model_Number,mod:Model_Number,1,BRW1)
  BIND('tmp:Tag',tmp:Tag)
  BIND('tmp:ServiceCentreName',tmp:ServiceCentreName)
  ?Browse:1{PROP:IconList,1} = '~bluetick.ico'
  BRW1.AddField(tmp:Tag,BRW1.Q.tmp:Tag)
  BRW1.AddField(mod:Model_Number,BRW1.Q.mod:Model_Number)
  BRW1.AddField(mod:Manufacturer,BRW1.Q.mod:Manufacturer)
  BRW1.AddField(tmp:ServiceCentreName,BRW1.Q.tmp:ServiceCentreName)
  BRW1.AddField(mod:IsVodafoneSpecific,BRW1.Q.mod:IsVodafoneSpecific)
  BRW1.AddField(mod:CBUPostal,BRW1.Q.mod:CBUPostal)
  BRW1.AddField(mod:CBUExchange,BRW1.Q.mod:CBUExchange)
  BRW1.AddField(mod:ISPPostal,BRW1.Q.mod:ISPPostal)
  BRW1.AddField(mod:ISPExchange,BRW1.Q.mod:ISPExchange)
  BRW1.AddField(mod:EBUPostal,BRW1.Q.mod:EBUPostal)
  BRW1.AddField(mod:EBUExchange,BRW1.Q.mod:EBUExchange)
  BRW1.AddField(mod:TabletDevice,BRW1.Q.mod:TabletDevice)
  FDCB8.Init(tmp:ManufacturerFilterRetail,?tmp:ManufacturerFilterRetail,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder(man:Manufacturer_Key)
  FDCB8.AddField(man:Manufacturer,FDCB8.Q.man:Manufacturer)
  FDCB8.AddField(man:IsVodafoneSpecific,FDCB8.Q.man:IsVodafoneSpecific)
  FDCB8.AddField(man:RecordNumber,FDCB8.Q.man:RecordNumber)
  FDCB8.AddUpdateField(man:Manufacturer,tmp:ManufacturerFilterRetail)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  FDCB9.Init(tmp:ServiceCentreNameFilter,?tmp:ServiceCentreNameFilter,Queue:FileDropCombo:1.ViewPosition,FDCB9::View:FileDropCombo,Queue:FileDropCombo:1,Relate:SIDSRVCN_ALIAS,ThisWindow,GlobalErrors,0,1,0)
  FDCB9.Q &= Queue:FileDropCombo:1
  FDCB9.AddSortOrder(srv_ali:ServiceCentreNameKey)
  FDCB9.AddField(srv_ali:ServiceCentreName,FDCB9.Q.srv_ali:ServiceCentreName)
  FDCB9.AddUpdateField(srv_ali:ServiceCentreName,tmp:ServiceCentreNameFilter)
  FDCB9.AddUpdateField(srv_ali:SCAccountID,tmp:SCAccountIDFilter)
  ThisWindow.AddItem(FDCB9.WindowComponent)
  FDCB9.DefaultFill = 0
  FDCB11.Init(tmp:ManufacturerFilterExchange,?tmp:ManufacturerFilterExchange,Queue:FileDropCombo:2.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo:2,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo:2
  FDCB11.AddSortOrder(man:Manufacturer_Key)
  FDCB11.AddField(man:Manufacturer,FDCB11.Q.man:Manufacturer)
  FDCB11.AddField(man:IsCCentreSpecific,FDCB11.Q.man:IsCCentreSpecific)
  FDCB11.AddField(man:VodafoneISPAccount,FDCB11.Q.man:VodafoneISPAccount)
  FDCB11.AddField(man:RecordNumber,FDCB11.Q.man:RecordNumber)
  FDCB11.AddUpdateField(man:Manufacturer,tmp:ManufacturerFilterExchange)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  FDCB12.Init(tmp:ManufacturerFilterBTB,?tmp:ManufacturerFilterBTB,Queue:FileDropCombo:3.ViewPosition,FDCB12::View:FileDropCombo,Queue:FileDropCombo:3,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB12.Q &= Queue:FileDropCombo:3
  FDCB12.AddSortOrder(man:Manufacturer_Key)
  FDCB12.AddField(man:Manufacturer,FDCB12.Q.man:Manufacturer)
  FDCB12.AddField(man:IsCCentreSpecific,FDCB12.Q.man:IsCCentreSpecific)
  FDCB12.AddField(man:VodafoneISPAccount,FDCB12.Q.man:VodafoneISPAccount)
  FDCB12.AddField(man:RecordNumber,FDCB12.Q.man:RecordNumber)
  FDCB12.AddUpdateField(man:Manufacturer,tmp:ManufacturerFilterBTB)
  ThisWindow.AddItem(FDCB12.WindowComponent)
  FDCB12.DefaultFill = 0
  FDCB14.Init(tmp:ManufacturerFilter28,?tmp:ManufacturerFilter28,Queue:FileDropCombo:4.ViewPosition,FDCB14::View:FileDropCombo,Queue:FileDropCombo:4,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB14.Q &= Queue:FileDropCombo:4
  FDCB14.AddSortOrder(man:Manufacturer_Key)
  FDCB14.AddField(man:Manufacturer,FDCB14.Q.man:Manufacturer)
  FDCB14.AddField(man:IsVodafoneSpecific,FDCB14.Q.man:IsVodafoneSpecific)
  FDCB14.AddField(man:RecordNumber,FDCB14.Q.man:RecordNumber)
  FDCB14.AddUpdateField(man:Manufacturer,tmp:ManufacturerFilter28)
  ThisWindow.AddItem(FDCB14.WindowComponent)
  FDCB14.DefaultFill = 0
  FDCB15.Init(tmp:ManufacturerFilterReturns,?tmp:ManufacturerFilterReturns,Queue:FileDropCombo:5.ViewPosition,FDCB15::View:FileDropCombo,Queue:FileDropCombo:5,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB15.Q &= Queue:FileDropCombo:5
  FDCB15.AddSortOrder(man:Manufacturer_Key)
  FDCB15.AddField(man:Manufacturer,FDCB15.Q.man:Manufacturer)
  FDCB15.AddField(man:IsVodafoneSpecific,FDCB15.Q.man:IsVodafoneSpecific)
  FDCB15.AddField(man:RecordNumber,FDCB15.Q.man:RecordNumber)
  FDCB15.AddUpdateField(man:Manufacturer,tmp:ManufacturerFilterReturns)
  ThisWindow.AddItem(FDCB15.WindowComponent)
  FDCB15.DefaultFill = 0
  FDCB16.Init(tmp:ManufacturerFilterOtherReturns,?tmp:ManufacturerFilterOtherReturns,Queue:FileDropCombo:6.ViewPosition,FDCB16::View:FileDropCombo,Queue:FileDropCombo:6,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB16.Q &= Queue:FileDropCombo:6
  FDCB16.AddSortOrder(man:Manufacturer_Key)
  FDCB16.AddField(man:Manufacturer,FDCB16.Q.man:Manufacturer)
  FDCB16.AddField(man:IsVodafoneSpecific,FDCB16.Q.man:IsVodafoneSpecific)
  FDCB16.AddField(man:RecordNumber,FDCB16.Q.man:RecordNumber)
  FDCB16.AddUpdateField(man:Manufacturer,tmp:ManufacturerFilterOtherReturns)
  ThisWindow.AddItem(FDCB16.WindowComponent)
  FDCB16.DefaultFill = 0
  FDCB17.Init(tmp:ManufacturerFilter28Exchange,?tmp:ManufacturerFilter28Exchange,Queue:FileDropCombo:7.ViewPosition,FDCB17::View:FileDropCombo,Queue:FileDropCombo:7,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB17.Q &= Queue:FileDropCombo:7
  FDCB17.AddSortOrder(man:Manufacturer_Key)
  FDCB17.AddField(man:Manufacturer,FDCB17.Q.man:Manufacturer)
  FDCB17.AddField(man:IsVodafoneSpecific,FDCB17.Q.man:IsVodafoneSpecific)
  FDCB17.AddField(man:RecordNumber,FDCB17.Q.man:RecordNumber)
  FDCB17.AddUpdateField(man:Manufacturer,tmp:ManufacturerFilter28Exchange)
  ThisWindow.AddItem(FDCB17.WindowComponent)
  FDCB17.DefaultFill = 0
  FDCB18.Init(tmp:ManufacturerFilterLeaseReturns,?tmp:ManufacturerFilterLeaseReturns,Queue:FileDropCombo:8.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo:8,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB18.Q &= Queue:FileDropCombo:8
  FDCB18.AddSortOrder(man:Manufacturer_Key)
  FDCB18.AddField(man:Manufacturer,FDCB18.Q.man:Manufacturer)
  FDCB18.AddField(man:RecordNumber,FDCB18.Q.man:RecordNumber)
  FDCB18.AddUpdateField(man:Manufacturer,tmp:ManufacturerFilterLeaseReturns)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  FDCB19.Init(tmp:ManufacturerFilterRouters,?tmp:ManufacturerFilterRouters,Queue:FileDropCombo:9.ViewPosition,FDCB19::View:FileDropCombo,Queue:FileDropCombo:9,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB19.Q &= Queue:FileDropCombo:9
  FDCB19.AddSortOrder(man:Manufacturer_Key)
  FDCB19.AddField(man:Manufacturer,FDCB19.Q.man:Manufacturer)
  FDCB19.AddField(man:RecordNumber,FDCB19.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB19.WindowComponent)
  FDCB19.DefaultFill = 0
  FDCB20.Init(tmp:ManufacturerFilterTab10,?tmp:ManufacturerFilterTab10,Queue:FileDropCombo:10.ViewPosition,FDCB20::View:FileDropCombo,Queue:FileDropCombo:10,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB20.Q &= Queue:FileDropCombo:10
  FDCB20.AddSortOrder(man:Manufacturer_Key)
  FDCB20.AddField(man:Manufacturer,FDCB20.Q.man:Manufacturer)
  FDCB20.AddField(man:RecordNumber,FDCB20.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB20.WindowComponent)
  FDCB20.DefaultFill = 0
  FDCB21.Init(tmp:ManufacturerFilterTab11,?tmp:ManufacturerFilterTab11,Queue:FileDropCombo:11.ViewPosition,FDCB21::View:FileDropCombo,Queue:FileDropCombo:11,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB21.Q &= Queue:FileDropCombo:11
  FDCB21.AddSortOrder(man:Manufacturer_Key)
  FDCB21.AddField(man:Manufacturer,FDCB21.Q.man:Manufacturer)
  FDCB21.AddField(man:RecordNumber,FDCB21.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB21.WindowComponent)
  FDCB21.DefaultFill = 0
  FDCB22.Init(tmp:ManufacturerFilterTab12,?tmp:ManufacturerFilterTab12,Queue:FileDropCombo:12.ViewPosition,FDCB22::View:FileDropCombo,Queue:FileDropCombo:12,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB22.Q &= Queue:FileDropCombo:12
  FDCB22.AddSortOrder(man:Manufacturer_Key)
  FDCB22.AddField(man:Manufacturer,FDCB22.Q.man:Manufacturer)
  FDCB22.AddField(man:RecordNumber,FDCB22.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB22.WindowComponent)
  FDCB22.DefaultFill = 0
  FDCB23.Init(tmp:ManufacturerFilterTab13,?tmp:ManufacturerFilterTab13,Queue:FileDropCombo:13.ViewPosition,FDCB23::View:FileDropCombo,Queue:FileDropCombo:13,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB23.Q &= Queue:FileDropCombo:13
  FDCB23.AddSortOrder(man:Manufacturer_Key)
  FDCB23.AddField(man:Manufacturer,FDCB23.Q.man:Manufacturer)
  FDCB23.AddField(man:RecordNumber,FDCB23.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB23.WindowComponent)
  FDCB23.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Q_ModelNumber)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Q_ModelNumber)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:MANUFACT.Close
    Relate:SIDMODSC.Close
    Relate:SIDSRVCN_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BrowseSIDModelsToServiceCentre',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?BtnExceptions
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnExceptions, Accepted)
      BRW1.UpdateBuffer()
      
      tmp:TurnaroundDaysRetail = 0
      tmp:TurnaroundDaysExch = 0
      
      if tmp:SCAccountIDFilter = scAccountID
          tmp:TurnaroundDaysRetail = turnaroundDaysRetail
          tmp:TurnaroundDaysExch = turnaroundDaysExch
      else
          Access:SIDSRVCN_ALIAS.ClearKey(srv_ali:SCAccountIDKey)
          srv_ali:SCAccountID = tmp:SCAccountIDFilter
          if Access:SIDSRVCN_ALIAS.Fetch(srv_ali:SCAccountIDKey) = Level:Benign
              tmp:TurnaroundDaysRetail = srv_ali:TurnaroundDaysRetail
              tmp:TurnaroundDaysExch = srv_ali:TurnaroundDaysExch
          end
      end
      
      UpdateSIDModels(mod:Manufacturer, mod:Model_Number, tmp:TurnaroundDaysRetail, tmp:TurnaroundDaysExch)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnExceptions, Accepted)
    OF ?BtnProcess
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnProcess, Accepted)
      if records(glo:Q_ModelNumber) > 0
          case choice(?CurrentTab)
              of 1 ! Retail
                  tmp:BookingType = 'R'
              of 2 ! Exchange
                  tmp:BookingType = 'E'
              of 3 ! Back To Base
                  tmp:BookingType = 'B'
              of 4 ! 28 Day (COM)
                  tmp:BookingType = 'W'
              of 5 ! Returns (Booking types Q, S and T)
                  tmp:BookingType = 'Q'
              of 6
                  tmp:BookingType = 'K'  ! #3074 Add tab for Lease Returns (DBH: 01/07/2014)
              of 7 ! Other Returns
                  tmp:BookingType = 'O'
              of 8 ! 28 Day Exchange
                  tmp:BookingType = 'U'
              of 9 ! Routers
                  tmp:BookingType = '4' ! #5239 Add new booking type (DBH: 11/05/2015)
              of 10 ! Accessory Retail Repair
                  tmp:BookingType = 'Y'   ! #5505 New Booking Type (DBH: 19/05/2015)
              of 11 ! Accessory Contact Centre
                  tmp:BookingType = '0'   ! #5505 New Booking Type (DBH: 19/05/2015)
              of 12 ! Accessory Returns Retail
                  tmp:BookingType = 'Z'   ! #5505 New Booking Type (DBH: 19/05/2015)
              of 13 ! Accessory Returns Contact Centre
                  tmp:BookingType = '3'   ! #5505 New Booking Type (DBH: 19/05/2015)
      
          end
          if AttachModelsToSC(tmp:SCAccountIDFilter, tmp:BookingType) = true
              ! Success
              free(glo:Q_ModelNumber)
              BRW1.ResetSort(1)
          end
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BtnProcess, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:SingleSC
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleSC, Accepted)
      do Display:SCFilter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleSC, Accepted)
    OF ?tmp:ServiceCentreNameFilter
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ServiceCentreNameFilter, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ServiceCentreNameFilter, Accepted)
    OF ?tmp:SingleManufacturerRetail
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerRetail, Accepted)
      do Display:ManRetailFilter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerRetail, Accepted)
    OF ?tmp:ManufacturerFilterRetail
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterRetail, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterRetail, Accepted)
    OF ?tmp:SingleManufacturerExchange
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerExchange, Accepted)
      do Display:ManExchangeFilter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerExchange, Accepted)
    OF ?tmp:ManufacturerFilterExchange
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterExchange, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterExchange, Accepted)
    OF ?tmp:SingleManufacturerBTB
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerBTB, Accepted)
      do Display:ManBTBFilter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerBTB, Accepted)
    OF ?tmp:ManufacturerFilterBTB
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterBTB, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterBTB, Accepted)
    OF ?tmp:SingleManufacturer28
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturer28, Accepted)
      do Display:Man28Filter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturer28, Accepted)
    OF ?tmp:ManufacturerFilter28
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilter28, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilter28, Accepted)
    OF ?tmp:SingleManufacturerReturns
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerReturns, Accepted)
      do Display:ManReturnsFilter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerReturns, Accepted)
    OF ?tmp:ManufacturerFilterReturns
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterReturns, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterReturns, Accepted)
    OF ?tmp:ManufacturerFilterLeaseReturns
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterLeaseReturns, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterLeaseReturns, Accepted)
    OF ?tmp:SingleManufacturerLeaseReturns
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerLeaseReturns, Accepted)
      do Display:ManLeaseReturnsFilter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerLeaseReturns, Accepted)
    OF ?tmp:SingleManufacturerOtherReturns
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerOtherReturns, Accepted)
      do Display:ManOtherReturnsFilter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerOtherReturns, Accepted)
    OF ?tmp:ManufacturerFilterOtherReturns
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterOtherReturns, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterOtherReturns, Accepted)
    OF ?tmp:SingleManufacturer28Exchange
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturer28Exchange, Accepted)
      do Display:Man28ExchangeFilter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturer28Exchange, Accepted)
    OF ?tmp:ManufacturerFilter28Exchange
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilter28Exchange, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilter28Exchange, Accepted)
    OF ?tmp:SingleManufacturerRouters
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerRouters, Accepted)
      do Display:ManRouterFilter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerRouters, Accepted)
    OF ?tmp:ManufacturerFilterRouters
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterRouters, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterRouters, Accepted)
    OF ?tmp:SingleManufacturerTab10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerTab10, Accepted)
      do Display:Tab10Filter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerTab10, Accepted)
    OF ?tmp:ManufacturerFilterTab10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterTab10, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterTab10, Accepted)
    OF ?tmp:SingleManufacturerTab11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerTab11, Accepted)
      do Display:Tab11Filter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerTab11, Accepted)
    OF ?tmp:ManufacturerFilterTab11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterTab11, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterTab11, Accepted)
    OF ?tmp:SingleManufacturerTab12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerTab12, Accepted)
      do Display:Tab12Filter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerTab12, Accepted)
    OF ?tmp:ManufacturerFilterTab12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterTab12, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterTab12, Accepted)
    OF ?tmp:SingleManufacturerTab13
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerTab13, Accepted)
      do Display:Tab13Filter
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SingleManufacturerTab13, Accepted)
    OF ?tmp:ManufacturerFilterTab13
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterTab13, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ManufacturerFilterTab13, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BrowseSIDModelsToServiceCentre')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?CurrentTab
    CASE EVENT()
    OF EVENT:TabChanging
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, TabChanging)
      ! Clear tags if the tab changes
      free(glo:Q_ModelNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, TabChanging)
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::10:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mod:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Model_Number, Selected)
      select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Model_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF choice(?CurrentTab) = 1 and tmp:SingleManufacturerRetail = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF choice(?CurrentTab) = 2 and tmp:SingleManufacturerExchange = 0
    RETURN SELF.SetSort(2,Force)
  ELSIF choice(?CurrentTab) = 2 and tmp:SingleManufacturerExchange = 1
    RETURN SELF.SetSort(3,Force)
  ELSIF choice(?CurrentTab) = 3 and tmp:SingleManufacturerBTB = 0
    RETURN SELF.SetSort(4,Force)
  ELSIF choice(?CurrentTab) = 3 and tmp:SingleManufacturerBTB = 1
    RETURN SELF.SetSort(5,Force)
  ELSIF choice(?CurrentTab) = 4 and tmp:SingleManufacturer28 = 0
    RETURN SELF.SetSort(6,Force)
  ELSIF choice(?CurrentTab) = 4 and tmp:SingleManufacturer28 = 1
    RETURN SELF.SetSort(7,Force)
  ELSIF choice(?CurrentTab) = 5 and tmp:SingleManufacturerReturns = 0
    RETURN SELF.SetSort(8,Force)
  ELSIF choice(?CurrentTab) = 5 and tmp:SingleManufacturerReturns = 1
    RETURN SELF.SetSort(9,Force)
  ELSIF choice(?CurrentTab) = 6 and tmp:SingleManufacturerLeaseReturns = 0
    RETURN SELF.SetSort(10,Force)
  ELSIF choice(?CurrentTab) = 6 and tmp:SingleManufacturerLeaseReturns = 1
    RETURN SELF.SetSort(11,Force)
  ELSIF choice(?CurrentTab) = 7 and tmp:SingleManufacturerOtherReturns = 0
    RETURN SELF.SetSort(12,Force)
  ELSIF choice(?CurrentTab) = 7 and tmp:SingleManufacturerOtherReturns = 1
    RETURN SELF.SetSort(13,Force)
  ELSIF choice(?CurrentTab) = 8 and tmp:SingleManufacturer28Exchange = 0
    RETURN SELF.SetSort(14,Force)
  ELSIF choice(?CurrentTab) = 8 and tmp:SingleManufacturer28Exchange = 1
    RETURN SELF.SetSort(15,Force)
  ELSIF choice(?CurrentTab) = 9 and tmp:SingleManufacturerRouters = 0
    RETURN SELF.SetSort(16,Force)
  ELSIF choice(?CurrentTab) = 9 and tmp:SingleManufacturerRouters = 1
    RETURN SELF.SetSort(17,Force)
  ELSIF choice(?CurrentTab) =10 and tmp:SingleManufacturerTab10 = 0
    RETURN SELF.SetSort(18,Force)
  ELSIF choice(?CurrentTab) =11 and tmp:SingleManufacturerTab11 = 0
    RETURN SELF.SetSort(19,Force)
  ELSIF choice(?CurrentTab) =12 and tmp:SingleManufacturerTab12 = 0
    RETURN SELF.SetSort(20,Force)
  ELSIF choice(?CurrentTab) =13 and tmp:SingleManufacturerTab13 = 0
    RETURN SELF.SetSort(21,Force)
  ELSIF choice(?CurrentTab) =10 and tmp:SingleManufacturerTab10 = 1
    RETURN SELF.SetSort(22,Force)
  ELSIF choice(?CurrentTab) =11 and tmp:SingleManufacturerTab11 = 1
    RETURN SELF.SetSort(23,Force)
  ELSIF choice(?CurrentTab) =12 and tmp:SingleManufacturerTab12 = 1
    RETURN SELF.SetSort(24,Force)
  ELSIF choice(?CurrentTab) =13 and tmp:SingleManufacturerTab13 = 1
    RETURN SELF.SetSort(25,Force)
  ELSE
    RETURN SELF.SetSort(26,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     GET(glo:Q_ModelNumber,glo:Q_ModelNumber.Model_Number_Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 1
  ELSE
    SELF.Q.tmp:Tag_Icon = 0
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  case choice(?CurrentTab)
      of 1 ! Retail
          if mod:IsVodafoneSpecific <> 1 then return Record:Filtered.
      of 2 ! Exchange
          if mod:CBUExchange <> 1 and mod:EBUExchange <> 1 and mod:ISPExchange <> 1 and mod:TabletDevice <> 1 then return Record:Filtered.
      of 3 ! Back To Base
          if mod:CBUPostal <> 1 and mod:EBUPostal <> 1 and mod:ISPPostal <> 1 then return Record:Filtered.
      of 4 ! 28 Day Exchange (COM)
          if mod:IsVodafoneSpecific <> 1 then return Record:Filtered.
      of 5 ! Returns
          if mod:IsVodafoneSpecific <> 1 then return Record:Filtered.
      of 6 ! Lease Returns
          if mod:IsVodafoneSpecific <> 1 then return Record:Filtered.
      of 7 ! Other Returns
          if mod:IsVodafoneSpecific <> 1 then return Record:Filtered.
      of 8 ! 28 Day Exchange
          if mod:IsVodafoneSpecific <> 1 then return Record:Filtered.
      of 9 ! Routers
          if mod:Router <> 1 then return Record:Filtered.
      of 10 Orof 11 Orof 12 Orof 13 ! Accessory Retail Repair / Contact Center Repair / Returns
          if mod:AccessoryProduct <> 1 then return Record:Filtered.
  
  end
  
  tmp:ServiceCentreName = ''
  
  Access:SIDMODSC.ClearKey(msc:ManModelTypeKey)
  msc:Manufacturer = mod:Manufacturer
  msc:ModelNo = mod:Model_Number
  case choice(?CurrentTab)
      of 1 ! Retail
          msc:BookingType = 'R'
      of 2 ! Exchange
          msc:BookingType = 'E'
      of 3 ! Back To Base
          msc:BookingType = 'B'
      of 4 ! 28 Day Exchange (COM)
          msc:BookingType = 'W'
      of 5 ! Returns
          msc:BookingType = 'Q'
      of 6 ! Lease Returns       ! #3074 Add tab for Lease Returns (DBH: 01/07/2014)
          msc:BookingType = 'K'
      of 7 ! Other Returns
          msc:BookingType = 'O'
      of 8 ! 28 Day Exchange
          msc:BookingType = 'U'
      of 9 ! Routers
          msc:BookingType = '4' ! #5239 Add new booking type (DBH: 11/05/2015)
      of 10 ! Accessory Retail Repair
          msc:BookingType = 'Y'
      of 11 ! Accessory Contact Centre
          msc:BookingType = '0'
      of 12 ! Accessory Returns Retail
          msc:BookingType = 'Z'
      of 13 ! Accessory Returns Contact Centre
          msc:BookingType = '3'
  end
  if Access:SIDMODSC.Fetch(msc:ManModelTypeKey) = Level:Benign
  
      if tmp:SingleSC = true
          if tmp:SCAccountIDFilter <> msc:SCAccountID then return Record:Filtered.
      end
  
      SCQueue.scq:SCAccountID = msc:SCAccountID
      get(SCQueue, SCQueue.scq:SCAccountID)
      if not error()
          tmp:ServiceCentreName = SCQueue.scq:ServiceCentreName
      end
  !else
  !    if tmp:SingleSC = true then return Record:Filtered.
  end
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     GET(glo:Q_ModelNumber,glo:Q_ModelNumber.Model_Number_Pointer)
    EXECUTE DASBRW::10:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


FDCB8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(8, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  if man:IsVodafoneSpecific <> 1 then return Record:Filtered.
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(8, ValidateRecord, (),BYTE)
  RETURN ReturnValue


FDCB11.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(11, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  if man:IsCCentreSpecific <> 1 and man:VodafoneISPAccount <> 1 then return Record:Filtered.
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(11, ValidateRecord, (),BYTE)
  RETURN ReturnValue


FDCB12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(12, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  if man:IsCCentreSpecific <> 1 and man:VodafoneISPAccount <> 1 then return Record:Filtered.
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(12, ValidateRecord, (),BYTE)
  RETURN ReturnValue


FDCB14.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(14, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  if man:IsVodafoneSpecific <> 1 then return Record:Filtered.
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(14, ValidateRecord, (),BYTE)
  RETURN ReturnValue


FDCB15.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(15, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  if man:IsVodafoneSpecific <> 1 then return Record:Filtered.
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(15, ValidateRecord, (),BYTE)
  RETURN ReturnValue


FDCB16.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(16, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  if man:IsVodafoneSpecific <> 1 then return Record:Filtered.
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(16, ValidateRecord, (),BYTE)
  RETURN ReturnValue


FDCB17.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(17, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  if man:IsVodafoneSpecific <> 1 then return Record:Filtered.
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(17, ValidateRecord, (),BYTE)
  RETURN ReturnValue


FDCB18.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(18, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  if man:IsVodafoneSpecific <> 1 then return Record:Filtered.
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(18, ValidateRecord, (),BYTE)
  RETURN ReturnValue

