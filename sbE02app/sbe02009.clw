

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02009.INC'),ONCE        !Local module procedure declarations
                     END


UpdateLOAN PROCEDURE                                  !Generated from procedure template - Window

CurrentTab           STRING(80)
print_label_temp     STRING(3)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
status_temp          STRING(10)
yes_temp             STRING('YES')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?loa:Model_Number
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?loa:Location
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?loa:Shelf_Location
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?loa:Stock_Type
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?loa:Colour
moc:Colour             LIKE(moc:Colour)               !List box control field - type derived from field
moc:Record_Number      LIKE(moc:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
FDCB6::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB15::View:FileDropCombo VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:Site_Location)
                     END
FDCB11::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
FDCB14::View:FileDropCombo VIEW(MODELCOL)
                       PROJECT(moc:Colour)
                       PROJECT(moc:Record_Number)
                     END
BRW7::View:Browse    VIEW(LOANACC)
                       PROJECT(lac:Accessory)
                       PROJECT(lac:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
lac:Accessory          LIKE(lac:Accessory)            !List box control field - type derived from field
lac:Ref_Number         LIKE(lac:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::loa:Record  LIKE(loa:RECORD),STATIC
QuickWindow          WINDOW('Update the Loan File'),AT(,,384,215),FONT('Tahoma',8,,),CENTER,IMM,HLP('UpdateLOAN'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,216,180),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Loan Unit Number'),AT(8,20),USE(?LOA:Ref_Number:Prompt),TRN
                           ENTRY(@p<<<<<<<#pb),AT(80,20,64,10),USE(loa:Ref_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('E.S.N. / I.M.E.I.'),AT(8,56),USE(?LOA:ESN:Prompt),TRN
                           ENTRY(@s30),AT(80,56,124,10),USE(loa:ESN),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('M.S.N.'),AT(8,72),USE(?loa:MSN:Prompt),TRN,HIDE,FONT(,8,,)
                           ENTRY(@s30),AT(80,72,124,10),USE(loa:MSN),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Model Number'),AT(8,88),USE(?loa:model_number:prompt)
                           COMBO(@s30),AT(80,88,124,10),USE(loa:Model_Number),VSCROLL,FONT('Tahoma',8,,FONT:bold),REQ,FORMAT('281L(2)|M@s30@120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Manufacturer'),AT(8,104),USE(?LOA:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(80,104,124,10),USE(loa:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Colour'),AT(8,120),USE(?LOA:Manufacturer:Prompt:2),TRN
                           COMBO(@s30),AT(80,120,124,10),USE(loa:Colour),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4)
                           PROMPT('Location'),AT(8,136),USE(?loa:location:prompt)
                           COMBO(@s30),AT(80,136,124,10),USE(loa:Location),VSCROLL,FONT('Tahoma',8,,FONT:bold),REQ,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           PROMPT('Shelf Location'),AT(8,152),USE(?Prompt8)
                           COMBO(@s30),AT(80,152,124,10),USE(loa:Shelf_Location),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:2)
                           PROMPT('Stock Type'),AT(8,168),USE(?Prompt10)
                           COMBO(@s30),AT(80,168,124,10),USE(loa:Stock_Type),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:3)
                           ENTRY(@d6b),AT(152,20,52,10),USE(loa:Date_Booked),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Job Number'),AT(152,32),USE(?loa:job_number:prompt),HIDE,FONT(,7,,)
                           PROMPT('Status'),AT(7,40),USE(?status_temp:Prompt),TRN
                           ENTRY(@s10),AT(80,40,64,10),USE(status_temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@p<<<<<<<#pb),AT(152,40,52,10),USE(loa:Job_Number),SKIP,HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                         END
                       END
                       SHEET,AT(224,4,156,180),USE(?Sheet2),SPREAD
                         TAB('Accessories'),USE(?Tab2)
                           ENTRY(@s30),AT(228,20,125,10),USE(lac:Accessory)
                           LIST,AT(228,36,148,124),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)~Accessory~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(228,164,56,16),USE(?Button4),LEFT,ICON('insert.gif')
                           BUTTON('&Delete'),AT(320,164,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                         END
                       END
                       PANEL,AT(4,188,376,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Loan History'),AT(8,192,60,16),USE(?Button5),LEFT,ICON('history.gif')
                       BUTTON('&OK'),AT(264,192,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(320,192,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       BUTTON('Print Label'),AT(68,192,60,16),USE(?print_Label),LEFT,ICON('Print.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB15               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB14               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  IncrementalLocatorClass          !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_loa_ali_id   ushort,auto
save_esn_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?LOA:Ref_Number:Prompt{prop:FontColor} = -1
    ?LOA:Ref_Number:Prompt{prop:Color} = 15066597
    If ?loa:Ref_Number{prop:ReadOnly} = True
        ?loa:Ref_Number{prop:FontColor} = 65793
        ?loa:Ref_Number{prop:Color} = 15066597
    Elsif ?loa:Ref_Number{prop:Req} = True
        ?loa:Ref_Number{prop:FontColor} = 65793
        ?loa:Ref_Number{prop:Color} = 8454143
    Else ! If ?loa:Ref_Number{prop:Req} = True
        ?loa:Ref_Number{prop:FontColor} = 65793
        ?loa:Ref_Number{prop:Color} = 16777215
    End ! If ?loa:Ref_Number{prop:Req} = True
    ?loa:Ref_Number{prop:Trn} = 0
    ?loa:Ref_Number{prop:FontStyle} = font:Bold
    ?LOA:ESN:Prompt{prop:FontColor} = -1
    ?LOA:ESN:Prompt{prop:Color} = 15066597
    If ?loa:ESN{prop:ReadOnly} = True
        ?loa:ESN{prop:FontColor} = 65793
        ?loa:ESN{prop:Color} = 15066597
    Elsif ?loa:ESN{prop:Req} = True
        ?loa:ESN{prop:FontColor} = 65793
        ?loa:ESN{prop:Color} = 8454143
    Else ! If ?loa:ESN{prop:Req} = True
        ?loa:ESN{prop:FontColor} = 65793
        ?loa:ESN{prop:Color} = 16777215
    End ! If ?loa:ESN{prop:Req} = True
    ?loa:ESN{prop:Trn} = 0
    ?loa:ESN{prop:FontStyle} = font:Bold
    ?loa:MSN:Prompt{prop:FontColor} = -1
    ?loa:MSN:Prompt{prop:Color} = 15066597
    If ?loa:MSN{prop:ReadOnly} = True
        ?loa:MSN{prop:FontColor} = 65793
        ?loa:MSN{prop:Color} = 15066597
    Elsif ?loa:MSN{prop:Req} = True
        ?loa:MSN{prop:FontColor} = 65793
        ?loa:MSN{prop:Color} = 8454143
    Else ! If ?loa:MSN{prop:Req} = True
        ?loa:MSN{prop:FontColor} = 65793
        ?loa:MSN{prop:Color} = 16777215
    End ! If ?loa:MSN{prop:Req} = True
    ?loa:MSN{prop:Trn} = 0
    ?loa:MSN{prop:FontStyle} = font:Bold
    ?loa:model_number:prompt{prop:FontColor} = -1
    ?loa:model_number:prompt{prop:Color} = 15066597
    If ?loa:Model_Number{prop:ReadOnly} = True
        ?loa:Model_Number{prop:FontColor} = 65793
        ?loa:Model_Number{prop:Color} = 15066597
    Elsif ?loa:Model_Number{prop:Req} = True
        ?loa:Model_Number{prop:FontColor} = 65793
        ?loa:Model_Number{prop:Color} = 8454143
    Else ! If ?loa:Model_Number{prop:Req} = True
        ?loa:Model_Number{prop:FontColor} = 65793
        ?loa:Model_Number{prop:Color} = 16777215
    End ! If ?loa:Model_Number{prop:Req} = True
    ?loa:Model_Number{prop:Trn} = 0
    ?loa:Model_Number{prop:FontStyle} = font:Bold
    ?LOA:Manufacturer:Prompt{prop:FontColor} = -1
    ?LOA:Manufacturer:Prompt{prop:Color} = 15066597
    If ?loa:Manufacturer{prop:ReadOnly} = True
        ?loa:Manufacturer{prop:FontColor} = 65793
        ?loa:Manufacturer{prop:Color} = 15066597
    Elsif ?loa:Manufacturer{prop:Req} = True
        ?loa:Manufacturer{prop:FontColor} = 65793
        ?loa:Manufacturer{prop:Color} = 8454143
    Else ! If ?loa:Manufacturer{prop:Req} = True
        ?loa:Manufacturer{prop:FontColor} = 65793
        ?loa:Manufacturer{prop:Color} = 16777215
    End ! If ?loa:Manufacturer{prop:Req} = True
    ?loa:Manufacturer{prop:Trn} = 0
    ?loa:Manufacturer{prop:FontStyle} = font:Bold
    ?LOA:Manufacturer:Prompt:2{prop:FontColor} = -1
    ?LOA:Manufacturer:Prompt:2{prop:Color} = 15066597
    If ?loa:Colour{prop:ReadOnly} = True
        ?loa:Colour{prop:FontColor} = 65793
        ?loa:Colour{prop:Color} = 15066597
    Elsif ?loa:Colour{prop:Req} = True
        ?loa:Colour{prop:FontColor} = 65793
        ?loa:Colour{prop:Color} = 8454143
    Else ! If ?loa:Colour{prop:Req} = True
        ?loa:Colour{prop:FontColor} = 65793
        ?loa:Colour{prop:Color} = 16777215
    End ! If ?loa:Colour{prop:Req} = True
    ?loa:Colour{prop:Trn} = 0
    ?loa:Colour{prop:FontStyle} = font:Bold
    ?loa:location:prompt{prop:FontColor} = -1
    ?loa:location:prompt{prop:Color} = 15066597
    If ?loa:Location{prop:ReadOnly} = True
        ?loa:Location{prop:FontColor} = 65793
        ?loa:Location{prop:Color} = 15066597
    Elsif ?loa:Location{prop:Req} = True
        ?loa:Location{prop:FontColor} = 65793
        ?loa:Location{prop:Color} = 8454143
    Else ! If ?loa:Location{prop:Req} = True
        ?loa:Location{prop:FontColor} = 65793
        ?loa:Location{prop:Color} = 16777215
    End ! If ?loa:Location{prop:Req} = True
    ?loa:Location{prop:Trn} = 0
    ?loa:Location{prop:FontStyle} = font:Bold
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    If ?loa:Shelf_Location{prop:ReadOnly} = True
        ?loa:Shelf_Location{prop:FontColor} = 65793
        ?loa:Shelf_Location{prop:Color} = 15066597
    Elsif ?loa:Shelf_Location{prop:Req} = True
        ?loa:Shelf_Location{prop:FontColor} = 65793
        ?loa:Shelf_Location{prop:Color} = 8454143
    Else ! If ?loa:Shelf_Location{prop:Req} = True
        ?loa:Shelf_Location{prop:FontColor} = 65793
        ?loa:Shelf_Location{prop:Color} = 16777215
    End ! If ?loa:Shelf_Location{prop:Req} = True
    ?loa:Shelf_Location{prop:Trn} = 0
    ?loa:Shelf_Location{prop:FontStyle} = font:Bold
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    If ?loa:Stock_Type{prop:ReadOnly} = True
        ?loa:Stock_Type{prop:FontColor} = 65793
        ?loa:Stock_Type{prop:Color} = 15066597
    Elsif ?loa:Stock_Type{prop:Req} = True
        ?loa:Stock_Type{prop:FontColor} = 65793
        ?loa:Stock_Type{prop:Color} = 8454143
    Else ! If ?loa:Stock_Type{prop:Req} = True
        ?loa:Stock_Type{prop:FontColor} = 65793
        ?loa:Stock_Type{prop:Color} = 16777215
    End ! If ?loa:Stock_Type{prop:Req} = True
    ?loa:Stock_Type{prop:Trn} = 0
    ?loa:Stock_Type{prop:FontStyle} = font:Bold
    If ?loa:Date_Booked{prop:ReadOnly} = True
        ?loa:Date_Booked{prop:FontColor} = 65793
        ?loa:Date_Booked{prop:Color} = 15066597
    Elsif ?loa:Date_Booked{prop:Req} = True
        ?loa:Date_Booked{prop:FontColor} = 65793
        ?loa:Date_Booked{prop:Color} = 8454143
    Else ! If ?loa:Date_Booked{prop:Req} = True
        ?loa:Date_Booked{prop:FontColor} = 65793
        ?loa:Date_Booked{prop:Color} = 16777215
    End ! If ?loa:Date_Booked{prop:Req} = True
    ?loa:Date_Booked{prop:Trn} = 0
    ?loa:Date_Booked{prop:FontStyle} = font:Bold
    ?loa:job_number:prompt{prop:FontColor} = -1
    ?loa:job_number:prompt{prop:Color} = 15066597
    ?status_temp:Prompt{prop:FontColor} = -1
    ?status_temp:Prompt{prop:Color} = 15066597
    If ?status_temp{prop:ReadOnly} = True
        ?status_temp{prop:FontColor} = 65793
        ?status_temp{prop:Color} = 15066597
    Elsif ?status_temp{prop:Req} = True
        ?status_temp{prop:FontColor} = 65793
        ?status_temp{prop:Color} = 8454143
    Else ! If ?status_temp{prop:Req} = True
        ?status_temp{prop:FontColor} = 65793
        ?status_temp{prop:Color} = 16777215
    End ! If ?status_temp{prop:Req} = True
    ?status_temp{prop:Trn} = 0
    ?status_temp{prop:FontStyle} = font:Bold
    If ?loa:Job_Number{prop:ReadOnly} = True
        ?loa:Job_Number{prop:FontColor} = 65793
        ?loa:Job_Number{prop:Color} = 15066597
    Elsif ?loa:Job_Number{prop:Req} = True
        ?loa:Job_Number{prop:FontColor} = 65793
        ?loa:Job_Number{prop:Color} = 8454143
    Else ! If ?loa:Job_Number{prop:Req} = True
        ?loa:Job_Number{prop:FontColor} = 65793
        ?loa:Job_Number{prop:Color} = 16777215
    End ! If ?loa:Job_Number{prop:Req} = True
    ?loa:Job_Number{prop:Trn} = 0
    ?loa:Job_Number{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?lac:Accessory{prop:ReadOnly} = True
        ?lac:Accessory{prop:FontColor} = 65793
        ?lac:Accessory{prop:Color} = 15066597
    Elsif ?lac:Accessory{prop:Req} = True
        ?lac:Accessory{prop:FontColor} = 65793
        ?lac:Accessory{prop:Color} = 8454143
    Else ! If ?lac:Accessory{prop:Req} = True
        ?lac:Accessory{prop:FontColor} = 65793
        ?lac:Accessory{prop:Color} = 16777215
    End ! If ?lac:Accessory{prop:Req} = True
    ?lac:Accessory{prop:Trn} = 0
    ?lac:Accessory{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
msn_check       Routine
    Hide(?loa:msn)
    Hide(?loa:msn:prompt)
    access:manufact.clearkey(man:manufacturer_key)
    man:manufacturer    = loa:manufacturer
    If access:manufact.fetch(man:manufacturer_key) = Level:Benign
        If man:use_msn = 'YES'
            Unhide(?loa:msn)
            Unhide(?loa:msn:prompt)
        End
    End!If access:manufact.fetch(man:manufacturer_key)
    Display()
Check_Esn       Routine
    error# = 0

    setcursor(cursor:wait)
    
    save_loa_ali_id = access:loan_alias.savefile()
    access:loan_alias.clearkey(loa_ali:esn_only_key)
    loa_ali:esn = loa:esn
    set(loa_ali:esn_only_key,loa_ali:esn_only_key)
    loop
        if access:loan_alias.next()
           break
        end !if
        if loa_ali:esn <> loa:esn      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        If loa_ali:ref_number <> loa:ref_number
            Case MessageEx('This E.S.N. / I.M.E.I. has already been entered as an Exchange Stock Item.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
            error# = 1
        End!@If loa_ali:ref_number <> loa:ref_number
    end !loop
    access:loan_alias.restorefile(save_loa_ali_id)
    setcursor()

    If error# = 0
        access:jobs.clearkey(job:esn_key)
        job:esn = loa:esn
        if access:jobs.fetch(job:esn_key) = Level:Benign
           If job:date_completed = ''
                Case MessageEx('This E.S.N. / I.M.E.I. is alread attached to job number '&Clip(job:ref_number),'ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
           End!If job:date_completed <> ''
        end!if access:jobs.fetch(job:esn_key) = Level:Benign
    end!IF error# = 0
    access:exchange.clearkey(xch:esn_only_key)
    xch:esn = loa:esn
    if access:exchange.fetch(xch:esn_only_key) = Level:Benign
        Case MessageEx('This E.S.N. / I.M.E.I. has already been entered as an Exchange Stock Item.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
        error# = 1
    End!IF error# = 0
    If error# = 0
        access:jobs_alias.clearkey(job_ali:esn_key)
        job_ali:esn = loa:esn
        if access:jobs_alias.fetch(job_ali:esn_key) = Level:Benign
           If job_ali:date_completed = ''
                Case MessageEx('This E.S.N. / I.M.E.I. is alread attached to job number '&Clip(job_ali:ref_number),'ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
           End!If job:date_completed <> ''
        end!if access:jobs.fetch(job:esn_key) = Level:Benign
    end!IF error# = 0

    If error# = 0
        access:modelnum.clearkey(mod:model_number_key)
        mod:model_number = loa:model_number
        if access:modelnum.fetch(mod:model_number_key) = Level:benign
            If Len(Clip(loa:esn)) < mod:esn_length_from Or Len(Clip(loa:esn)) > mod:esn_length_to
                Case MessageEx('The E.S.N. / I.M.E.I. entered should be between '&clip(mod:esn_length_from)&' and '&clip(mod:esn_length_to)&' characters in length.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                error# = 1
            End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
            If man:use_msn = 'YES'
                If len(Clip(loa:msn)) < mod:msn_length_from Or len(Clip(loa:msn)) > mod:msn_length_to
                    Case MessageEx('The M.S.N. entered should be between '&clip(mod:msn_length_from)&' and '&clip(mod:msn_length_to)&' characters in length.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    error# = 1
                End!If len(job:msn) < mod:msn_length_from Or len(job:msn) > mod:msn_length_to
            End!If man:use_msn = 'YES'
        end !if access:modelnum.fetch(mod:model_number_key) = Level:benign
    End!IF error# = 0
    If error# = 1
        loa:esn = ''
        Select(?loa:esn)
    End


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateLOAN',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateLOAN',1)
    SolaceViewVars('print_label_temp',print_label_temp,'UpdateLOAN',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateLOAN',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateLOAN',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateLOAN',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateLOAN',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'UpdateLOAN',1)
    SolaceViewVars('status_temp',status_temp,'UpdateLOAN',1)
    SolaceViewVars('yes_temp',yes_temp,'UpdateLOAN',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOA:Ref_Number:Prompt;  SolaceCtrlName = '?LOA:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:Ref_Number;  SolaceCtrlName = '?loa:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOA:ESN:Prompt;  SolaceCtrlName = '?LOA:ESN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:ESN;  SolaceCtrlName = '?loa:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:MSN:Prompt;  SolaceCtrlName = '?loa:MSN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:MSN;  SolaceCtrlName = '?loa:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:model_number:prompt;  SolaceCtrlName = '?loa:model_number:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:Model_Number;  SolaceCtrlName = '?loa:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOA:Manufacturer:Prompt;  SolaceCtrlName = '?LOA:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:Manufacturer;  SolaceCtrlName = '?loa:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOA:Manufacturer:Prompt:2;  SolaceCtrlName = '?LOA:Manufacturer:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:Colour;  SolaceCtrlName = '?loa:Colour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:location:prompt;  SolaceCtrlName = '?loa:location:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:Location;  SolaceCtrlName = '?loa:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:Shelf_Location;  SolaceCtrlName = '?loa:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:Stock_Type;  SolaceCtrlName = '?loa:Stock_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:Date_Booked;  SolaceCtrlName = '?loa:Date_Booked';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:job_number:prompt;  SolaceCtrlName = '?loa:job_number:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_temp:Prompt;  SolaceCtrlName = '?status_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_temp;  SolaceCtrlName = '?status_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:Job_Number;  SolaceCtrlName = '?loa:Job_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lac:Accessory;  SolaceCtrlName = '?lac:Accessory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button5;  SolaceCtrlName = '?Button5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?print_Label;  SolaceCtrlName = '?print_Label';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Loan Unit'
  OF ChangeRecord
    ActionMessage = 'Changing A Loan Unit'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateLOAN')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateLOAN')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOA:Ref_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(loa:Record,History::loa:Record)
  SELF.AddHistoryField(?loa:Ref_Number,1)
  SELF.AddHistoryField(?loa:ESN,4)
  SELF.AddHistoryField(?loa:MSN,5)
  SELF.AddHistoryField(?loa:Model_Number,2)
  SELF.AddHistoryField(?loa:Manufacturer,3)
  SELF.AddHistoryField(?loa:Colour,6)
  SELF.AddHistoryField(?loa:Location,7)
  SELF.AddHistoryField(?loa:Shelf_Location,8)
  SELF.AddHistoryField(?loa:Stock_Type,13)
  SELF.AddHistoryField(?loa:Date_Booked,9)
  SELF.AddHistoryField(?loa:Job_Number,12)
  SELF.AddUpdateFile(Access:LOAN)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCESSOR.Open
  Relate:DEFAULTS.Open
  Relate:ESNMODEL.Open
  Relate:EXCHANGE.Open
  Relate:LOAN_ALIAS.Open
  Access:MANUFACT.UseFile
  Access:LOAN.UseFile
  Access:JOBS_ALIAS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOAN
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:LOANACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  !Status
  Case loa:available
      Of 'AVL'
          status_temp = 'AVAILABLE'
      Of 'LOA'
          status_temp = 'LOANED'
      Of 'REP'
          status_temp = 'IN REPAIR'
      Of 'DES'
          status_Temp = 'DESPATCHED'
  End!Case xch:status
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,lac:Ref_Number_Key)
  BRW7.AddRange(lac:Ref_Number,loa:Ref_Number)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(?lac:Accessory,lac:Accessory,1,BRW7)
  BRW7.AddField(lac:Accessory,BRW7.Q.lac:Accessory)
  BRW7.AddField(lac:Ref_Number,BRW7.Q.lac:Ref_Number)
  FDCB5.Init(loa:Model_Number,?loa:Model_Number,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.RemoveDuplicatesFlag = TRUE
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(mod:Model_Number_Key)
  FDCB5.AddField(mod:Model_Number,FDCB5.Q.mod:Model_Number)
  FDCB5.AddField(mod:Manufacturer,FDCB5.Q.mod:Manufacturer)
  FDCB5.AddUpdateField(mod:Model_Number,loa:Model_Number)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB6.Init(loa:Location,?loa:Location,Queue:FileDropCombo:1.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo:1,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo:1
  FDCB6.AddSortOrder(loc:Location_Key)
  FDCB6.AddField(loc:Location,FDCB6.Q.loc:Location)
  FDCB6.AddField(loc:RecordNumber,FDCB6.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB15.Init(loa:Shelf_Location,?loa:Shelf_Location,Queue:FileDropCombo:2.ViewPosition,FDCB15::View:FileDropCombo,Queue:FileDropCombo:2,Relate:LOCSHELF,ThisWindow,GlobalErrors,0,1,0)
  FDCB15.Q &= Queue:FileDropCombo:2
  FDCB15.AddSortOrder(los:Shelf_Location_Key)
  FDCB15.AddRange(los:Site_Location,loa:Location)
  FDCB15.AddField(los:Shelf_Location,FDCB15.Q.los:Shelf_Location)
  FDCB15.AddField(los:Site_Location,FDCB15.Q.los:Site_Location)
  ThisWindow.AddItem(FDCB15.WindowComponent)
  FDCB15.DefaultFill = 0
  FDCB11.Init(loa:Stock_Type,?loa:Stock_Type,Queue:FileDropCombo:3.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo:3,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo:3
  FDCB11.AddSortOrder(stp:Use_Loan_Key)
  FDCB11.AddRange(stp:Use_Loan,yes_temp)
  FDCB11.AddField(stp:Stock_Type,FDCB11.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  FDCB14.Init(loa:Colour,?loa:Colour,Queue:FileDropCombo:4.ViewPosition,FDCB14::View:FileDropCombo,Queue:FileDropCombo:4,Relate:MODELCOL,ThisWindow,GlobalErrors,0,1,0)
  FDCB14.Q &= Queue:FileDropCombo:4
  FDCB14.AddSortOrder(moc:Colour_Key)
  FDCB14.AddRange(moc:Model_Number,loa:Model_Number)
  FDCB14.AddField(moc:Colour,FDCB14.Q.moc:Colour)
  FDCB14.AddField(moc:Record_Number,FDCB14.Q.moc:Record_Number)
  ThisWindow.AddItem(FDCB14.WindowComponent)
  FDCB14.DefaultFill = 0
  BRW7.AskProcedure = 1
  BRW7.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCESSOR.Close
    Relate:DEFAULTS.Close
    Relate:ESNMODEL.Close
    Relate:EXCHANGE.Close
    Relate:LOAN_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateLOAN',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    loa:Date_Booked = Today()
    loa:Available = 'AVL'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Loan_Accessory
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Button5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      glo:select12 = loa:ref_number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?loa:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:ESN, Accepted)
      Esn_Model_Routine(loa:esn,loa:model_number,model",pass")
      If pass" = 1
          loa:model_number = model"
          access:modelnum.clearkey(mod:model_number_key)
          mod:model_number = loa:model_number
          if access:modelnum.fetch(mod:model_number_key) = Level:Benign
              loa:manufacturer = mod:manufacturer
          end
          Select(?loa:colour)
          Do msn_check
      End!If fill_in# = 1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:ESN, Accepted)
    OF ?loa:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:Model_Number, Accepted)
      access:modelnum.clearkey(mod:model_number_key)
      mod:model_number = loa:model_number
      If access:modelnum.fetch(mod:model_number_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browsemodelnum
          if globalresponse = requestcompleted
              loa:model_number = mod:model_number
              loa:manufacturer = mod:manufacturer
          else
              xch:model_number = ''
          end
          display(?loa:model_number)
          globalrequest     = saverequest#
      Else
          loa:manufacturer    = mod:manufacturer
      End!If access:modelnum.fetch(mod:model_number_key)
      Do msn_check
      FDCB14.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:Model_Number, Accepted)
    OF ?loa:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:Location, Accepted)
      FDCB15.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:Location, Accepted)
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      glo:select1 = loa:model_number
      Pick_Accessories
      IF Records(glo:Q_Accessory) <> ''
          Loop x# = 1 To Records(glo:Q_Accessory)
              Get(glo:Q_Accessory,x#)
              get(loanacc,0)
              if access:loanacc.primerecord() = level:benign
                  lac:ref_number  = loa:ref_number
                  lac:accessory   = glo:accessory_pointer
                  access:loanacc.tryinsert()
              end!if access:loanacc.primerecord() = level:benign
          End
          BRW7.ResetSort(1)
      End
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?Button5
      ThisWindow.Update
      Browse_Loan_History
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      glo:select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?print_Label
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?print_Label, Accepted)
      If thiswindow.request = Insertrecord
          Case MessageEx('Cannot print a label until the record has been saved.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If thiswindow.request = Insertrecord
          glo:select1 = loa:ref_number
          set(defaults)
          access:defaults.next()
          case def:label_printer_type
              of 'TEC B-440 / B-442'
                  loan_unit_label
              of 'TEC B-452'
                  loan_unit_label_b452
          end!case def:label_printer_type
          glo:select1 = ''
      End!If thiswindow.request = Insertrecord
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?print_Label, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ! Check ESN
      error# = 0
  
      setcursor(cursor:wait)
      
      save_loa_ali_id = access:loan_alias.savefile()
      access:loan_alias.clearkey(loa_ali:esn_only_key)
      loa_ali:esn = loa:esn
      set(loa_ali:esn_only_key,loa_ali:esn_only_key)
      loop
          if access:loan_alias.next()
             break
          end !if
          if loa_ali:esn <> loa:esn      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          If loa_ali:ref_number <> loa:ref_number
              Case MessageEx('This E.S.N. / I.M.E.I. has already been entered as an Exchange Stock Item.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
          End!@If loa_ali:ref_number <> loa:ref_number
      end !loop
      access:loan_alias.restorefile(save_loa_ali_id)
      setcursor()
  
      If error# = 0
          access:jobs.clearkey(job:esn_key)
          job:esn = loa:esn
          if access:jobs.fetch(job:esn_key) = Level:Benign
             If job:date_completed = ''
                  Case MessageEx('This E.S.N. / I.M.E.I. is already attached to job number '&clip(job:ref_number)&'.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
             End!If job:date_completed <> ''
          end!if access:jobs.fetch(job:esn_key) = Level:Benign
      end!IF error# = 0
      access:exchange.clearkey(xch:esn_only_key)
      xch:esn = loa:esn
      if access:exchange.fetch(xch:esn_only_key) = Level:Benign
          Case MessageEx('This E.S.N. / I.M.E.I. has already been entered as an Exchange Stock Item.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!IF error# = 0
      If error# = 0
          access:jobs_alias.clearkey(job_ali:esn_key)
          job_ali:esn = loa:esn
          if access:jobs_alias.fetch(job_ali:esn_key) = Level:Benign
             If job_ali:date_completed = ''
                  Case MessageEx('This E.S.N. / I.M.E.I. is already attached to job number '&clip(job_ali:ref_number)&'.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
             End!If job:date_completed <> ''
          end!if access:jobs.fetch(job:esn_key) = Level:Benign
      end!IF error# = 0
  
      If error# = 0
          access:modelnum.clearkey(mod:model_number_key)
          mod:model_number = loa:model_number
          if access:modelnum.fetch(mod:model_number_key) = Level:benign
              If Len(Clip(loa:esn)) < mod:esn_length_from Or Len(Clip(loa:esn)) > mod:esn_length_to
                  Case MessageEx('The E.S.N. / I.M.E.I. entered should be between '&clip(mod:esn_length_from)&' and '&clip(mod:esn_length_to)&' characters in length.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
              If man:use_msn = 'YES'
                  If len(Clip(loa:msn)) < mod:msn_length_from Or len(Clip(loa:msn)) > mod:msn_length_to
                      Case MessageEx('The M.S.N. entered should be between '&clip(mod:msn_length_from)&' and '&clip(mod:msn_length_to)&' characters in length.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End!If len(job:msn) < mod:msn_length_from Or len(job:msn) > mod:msn_length_to
              End!If man:use_msn = 'YES'
          end !if access:modelnum.fetch(mod:model_number_key) = Level:benign
      End!IF error# = 0
      If error# = 1
          Select(?loa:esn)
          Cycle
      End
  If thiswindow.request = Insertrecord
      Set(defaults)
      access:defaults.next()
      If def:use_loan_exchange_label = 'YES'
          Print_label_temp = 'YES'
      Else
          Print_label_temp = 'NO'
      End!If def:use_loan_exchange_label = 'YES'
  Else
      print_label_temp = 'NO'
  End!If thiswindow.request = Insertrecord
  
  ReturnValue = PARENT.TakeCompleted()
  If print_label_temp = 'YES' And thiswindow.request = insertrecord
      glo:select1 = loa:ref_number
      set(defaults)
      access:defaults.next()
      case def:label_printer_type
          of 'TEC B-440 / B-442'
              loan_Unit_label
          of 'TEC B-452'
              loan_unit_label_b452
      end!case def:label_printer_type
      glo:select1 = ''
  End!If print_label_temp = 'YES' And thiswindow.request = insertrecord
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateLOAN')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?lac:Accessory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lac:Accessory, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lac:Accessory, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do msn_check
      If loa:job_number <> ''
          Unhide(?loa:job_number)
          Unhide(?loa:job_number:prompt)
      Else!If xch:job_number <> ''
          Hide(?loa:job_number)
          Hide(?loa:job_number:prompt)
      End!If xch:job_number <> ''
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB15.ResetQueue(1)
      FDCB5.ResetQueue(1)
      FDCB6.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW7.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

