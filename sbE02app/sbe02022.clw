

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02022.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Exchange_By_Audit PROCEDURE (f_stock_type)     !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
all_temp             STRING('AVL')
available_temp       STRING('YES')
status_temp          STRING(30)
Stock_Type_Temp      STRING(30)
Model_Number_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Stock_Type_Temp
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(EXCAUDIT)
                       PROJECT(exa:Audit_Number)
                       PROJECT(exa:Stock_Unit_Number)
                       PROJECT(exa:Replacement_Unit_Number)
                       PROJECT(exa:Record_Number)
                       PROJECT(exa:Stock_Type)
                       JOIN(xch_ali:Ref_Number_Key,exa:Replacement_Unit_Number)
                         PROJECT(xch_ali:ESN)
                         PROJECT(xch_ali:Model_Number)
                         PROJECT(xch_ali:Ref_Number)
                       END
                       JOIN(xch:Ref_Number_Key,exa:Stock_Unit_Number)
                         PROJECT(xch:ESN)
                         PROJECT(xch:Model_Number)
                         PROJECT(xch:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
exa:Audit_Number       LIKE(exa:Audit_Number)         !List box control field - type derived from field
exa:Stock_Unit_Number  LIKE(exa:Stock_Unit_Number)    !List box control field - type derived from field
xch:ESN                LIKE(xch:ESN)                  !List box control field - type derived from field
xch:Model_Number       LIKE(xch:Model_Number)         !List box control field - type derived from field
exa:Replacement_Unit_Number LIKE(exa:Replacement_Unit_Number) !List box control field - type derived from field
xch_ali:ESN            LIKE(xch_ali:ESN)              !List box control field - type derived from field
xch_ali:Model_Number   LIKE(xch_ali:Model_Number)     !List box control field - type derived from field
exa:Record_Number      LIKE(exa:Record_Number)        !Primary key field - type derived from field
exa:Stock_Type         LIKE(exa:Stock_Type)           !Browse key field - type derived from field
xch_ali:Ref_Number     LIKE(xch_ali:Ref_Number)       !Related join file key field - type derived from field
xch:Ref_Number         LIKE(xch:Ref_Number)           !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB12::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
QuickWindow          WINDOW('Browse The Exchange Units File'),AT(,,589,294),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Loan'),SYSTEM,GRAY,MAX,DOUBLE
                       LIST,AT(8,64,492,224),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('52R(2)|M~Audit Number~L@s8b@53R(2)|M~Stock Unit No~L@s8b@69L(2)|M~E.S.N. / I.M.E' &|
   '.I.~@s16@84L(2)|M~Model Number~@s30@75R(2)|M~Replacement Unit No~L@s8b@69L(2)|M~' &|
   'E.S.N. / I.M.E.I.~@s16@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('Make Audit No. Available'),AT(512,32,76,20),USE(?Make_Available),LEFT,ICON(ICON:Cut)
                       BUTTON('&Change'),AT(512,216,76,20),USE(?Change),LEFT,ICON('edit.ico')
                       BUTTON('&Delete'),AT(512,240,76,20),USE(?Delete),LEFT,ICON('delete.ico')
                       PANEL,AT(4,4,500,24),USE(?Panel1),FILL(COLOR:Gray)
                       COMBO(@s30),AT(80,12,124,10),USE(Stock_Type_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       PROMPT('Stock Type'),AT(8,12),USE(?Prompt1),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                       SHEET,AT(4,32,500,260),USE(?CurrentTab),SPREAD
                         TAB('By Audit Number'),USE(?Tab:4)
                           ENTRY(@s8),AT(8,48,64,10),USE(exa:Stock_Unit_Number),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('Close'),AT(512,272,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB12               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    If ?Stock_Type_Temp{prop:ReadOnly} = True
        ?Stock_Type_Temp{prop:FontColor} = 65793
        ?Stock_Type_Temp{prop:Color} = 15066597
    Elsif ?Stock_Type_Temp{prop:Req} = True
        ?Stock_Type_Temp{prop:FontColor} = 65793
        ?Stock_Type_Temp{prop:Color} = 8454143
    Else ! If ?Stock_Type_Temp{prop:Req} = True
        ?Stock_Type_Temp{prop:FontColor} = 65793
        ?Stock_Type_Temp{prop:Color} = 16777215
    End ! If ?Stock_Type_Temp{prop:Req} = True
    ?Stock_Type_Temp{prop:Trn} = 0
    ?Stock_Type_Temp{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:4{prop:Color} = 15066597
    If ?exa:Stock_Unit_Number{prop:ReadOnly} = True
        ?exa:Stock_Unit_Number{prop:FontColor} = 65793
        ?exa:Stock_Unit_Number{prop:Color} = 15066597
    Elsif ?exa:Stock_Unit_Number{prop:Req} = True
        ?exa:Stock_Unit_Number{prop:FontColor} = 65793
        ?exa:Stock_Unit_Number{prop:Color} = 8454143
    Else ! If ?exa:Stock_Unit_Number{prop:Req} = True
        ?exa:Stock_Unit_Number{prop:FontColor} = 65793
        ?exa:Stock_Unit_Number{prop:Color} = 16777215
    End ! If ?exa:Stock_Unit_Number{prop:Req} = True
    ?exa:Stock_Unit_Number{prop:Trn} = 0
    ?exa:Stock_Unit_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Exchange_By_Audit',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Exchange_By_Audit',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Exchange_By_Audit',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Exchange_By_Audit',1)
    SolaceViewVars('all_temp',all_temp,'Browse_Exchange_By_Audit',1)
    SolaceViewVars('available_temp',available_temp,'Browse_Exchange_By_Audit',1)
    SolaceViewVars('status_temp',status_temp,'Browse_Exchange_By_Audit',1)
    SolaceViewVars('Stock_Type_Temp',Stock_Type_Temp,'Browse_Exchange_By_Audit',1)
    SolaceViewVars('Model_Number_Temp',Model_Number_Temp,'Browse_Exchange_By_Audit',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Make_Available;  SolaceCtrlName = '?Make_Available';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Stock_Type_Temp;  SolaceCtrlName = '?Stock_Type_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?exa:Stock_Unit_Number;  SolaceCtrlName = '?exa:Stock_Unit_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Exchange_By_Audit')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Exchange_By_Audit')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  If f_stock_Type <> ''
      stock_type_temp = f_stock_type
  End
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:EXCAUDIT.Open
  Relate:JOBS.Open
  Relate:USERS_ALIAS.Open
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EXCAUDIT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,exa:Audit_Number_Key)
  BRW1.AddRange(exa:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,exa:Audit_Number,1,BRW1)
  BRW1.AddResetField(all_temp)
  BIND('Stock_Type_Temp',Stock_Type_Temp)
  BRW1.AddField(exa:Audit_Number,BRW1.Q.exa:Audit_Number)
  BRW1.AddField(exa:Stock_Unit_Number,BRW1.Q.exa:Stock_Unit_Number)
  BRW1.AddField(xch:ESN,BRW1.Q.xch:ESN)
  BRW1.AddField(xch:Model_Number,BRW1.Q.xch:Model_Number)
  BRW1.AddField(exa:Replacement_Unit_Number,BRW1.Q.exa:Replacement_Unit_Number)
  BRW1.AddField(xch_ali:ESN,BRW1.Q.xch_ali:ESN)
  BRW1.AddField(xch_ali:Model_Number,BRW1.Q.xch_ali:Model_Number)
  BRW1.AddField(exa:Record_Number,BRW1.Q.exa:Record_Number)
  BRW1.AddField(exa:Stock_Type,BRW1.Q.exa:Stock_Type)
  BRW1.AddField(xch_ali:Ref_Number,BRW1.Q.xch_ali:Ref_Number)
  BRW1.AddField(xch:Ref_Number,BRW1.Q.xch:Ref_Number)
  QuickWindow{PROP:MinWidth}=530
  QuickWindow{PROP:MinHeight}=214
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB12.Init(Stock_Type_Temp,?Stock_Type_Temp,Queue:FileDropCombo.ViewPosition,FDCB12::View:FileDropCombo,Queue:FileDropCombo,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB12.Q &= Queue:FileDropCombo
  FDCB12.AddSortOrder(stp:Use_Loan_Key)
  FDCB12.AddField(stp:Stock_Type,FDCB12.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB12.WindowComponent)
  FDCB12.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:EXCAUDIT.Close
    Relate:JOBS.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Exchange_By_Audit',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            if securitycheck('AUDIT NUMBERS - INSERT')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                access:excaudit.cancelautoinc()
                do_update# = false
            end!if securitycheck('AUDIT NUMBERS - INSERT',x")
        of changerecord
            if securitycheck('AUDIT NUMBERS - CHANGE')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end!if securitycheck('AUDIT NUMBERS - CHANGE',x")
        of deleterecord
            if securitycheck('AUDIT NUMBERS - DELETE')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            Else
              If exa:stock_unit_number <> '' Or exa:replacement_unit_number <> ''
                  Case MessageEx('There are Exchange Units attached to this audit number!<13,10><13,10>You must make this Audit Number Available before you can delete it.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  do_update# = False
              End!If exc:stock_unit_number <> '' Or exc:replacement_unit_number <> ''
            end!if securitycheck('AUDIT NUMBERS - DELETE',x")
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Exchange_By_Audit
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Make_Available
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Make_Available, Accepted)
      If SecurityCheck('AUDIT NUMBERS - MAKE AVAILABLE')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('AUDIT NUMBERS - MAKE AVAILABLE)
          Case MessageEx('Warning! Any Exchange Units attached to this audit number will have it removed.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  thiswindow.reset(1)
                  access:excaudit.clearkey(exa:audit_number_key)
                  exa:stock_type   = Stock_Type_Temp
                  exa:audit_number = brw1.q.exa:audit_number
                  if access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
                      If exa:Stock_Unit_Number <> ''
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number = exa:stock_unit_number
                          if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                              xch:audit_number = ''
                              access:exchange.update()
                              get(exchhist,0)
                              if access:exchhist.primerecord() = level:benign
                                  exh:ref_number   = xch:ref_number
                                  exh:date          = today()
                                  exh:time          = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  exh:user = use:user_code
                                  exh:status        = 'MANUALLY REMOVED FROM AUDIT NO: ' & Clip(exa:stock_unit_number)
                                  access:exchhist.insert()
                              end!if access:exchhist.primerecord() = level:benign
                          End!if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                      End!If EXA:Stock_Unit_Number <> ''
                      If exa:Replacement_Unit_Number <> ''
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number = exa:replacement_unit_number
                          if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                              xch:audit_number = ''
                              access:exchange.update()
                              get(exchhist,0)
                              if access:exchhist.primerecord() = level:benign
                                  exh:ref_number   = xch:ref_number
                                  exh:date          = today()
                                  exh:time          = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  exh:user = use:user_code
                                  exh:status        = 'MANUALLY REMOVED FROM AUDIT NO: ' & Clip(exa:replacement_unit_number)
                                  access:exchhist.insert()
                              end!if access:exchhist.primerecord() = level:benign
                          End!if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                      End!If exa:Replacement_Unit_Number <> ''
                      exa:stock_unit_number = ''
                      exa:replacement_unit_number = ''
                      access:excaudit.update()
                  End!if access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
              Of 2 ! &No Button
          End!Case MessageEx
      End!If SecurityCheck('AUDIT NUMBERS - MAKE AVAILABLE)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Make_Available, Accepted)
    OF ?Stock_Type_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Exchange_By_Audit')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?exa:Stock_Unit_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?exa:Stock_Unit_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?exa:Stock_Unit_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  CASE (xch:Available)
  OF 'AVL'
    status_temp = 'AVAILABLE'
  OF 'LOA'
    status_temp = 'EXCHANGED - JOB NO: ' & FORMAT(xch:Job_Number,@p<<<<<<<#p)
  ELSE
    status_temp = 'IN REPAIR - JOB NO: ' & FORMAT(xch:Job_Number,@p<<<<<<<#p)
  END
  PARENT.SetQueueRecord


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?Stock_Type_Temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)

