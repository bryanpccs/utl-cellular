

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02003.INC'),ONCE        !Local module procedure declarations
                     END


Update_Exchange PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
save_moc_id          USHORT,AUTO
audit_number_temp    REAL
esn_temp             STRING(16)
print_label_temp     STRING('NO {1}')
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
status_temp          STRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW7::View:Browse    VIEW(EXCHACC)
                       PROJECT(xca:Accessory)
                       PROJECT(xca:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
xca:Accessory          LIKE(xca:Accessory)            !List box control field - type derived from field
xca:Ref_Number         LIKE(xca:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB9::View:FileDrop  VIEW(EXCAUDIT)
                       PROJECT(exa:Audit_Number)
                       PROJECT(exa:Record_Number)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?xch:Audit_Number
exa:Audit_Number       LIKE(exa:Audit_Number)         !List box control field - type derived from field
exa:Record_Number      LIKE(exa:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::xch:Record  LIKE(xch:RECORD),STATIC
QuickWindow          WINDOW('Update the Exchange File'),AT(,,384,231),FONT('Tahoma',8,,),CENTER,IMM,HLP('UpdateLOAN'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,216,196),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Exchange Unit No'),AT(8,20),USE(?LOA:Ref_Number:Prompt),TRN
                           ENTRY(@p<<<<<<<#pb),AT(80,20,64,10),USE(xch:Ref_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Unit Number')
                           ENTRY(@d6b),AT(152,20,52,10),USE(xch:Date_Booked),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Status'),AT(7,36),USE(?status_temp:Prompt),TRN
                           ENTRY(@s40),AT(80,36,124,10),USE(status_temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('E.S.N. / I.M.E.I.'),AT(8,52),USE(?LOA:ESN:Prompt),TRN
                           ENTRY(@s30),AT(80,52,124,10),USE(xch:ESN),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('M.S.N.'),AT(8,68),USE(?xch:MSN:Prompt),TRN,HIDE,FONT(,8,,)
                           ENTRY(@s30),AT(80,68,124,10),USE(xch:MSN),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Model Number'),AT(8,84),USE(?XCH:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(80,84,124,10),USE(xch:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(208,84,10,10),USE(?LookupModelNumber),SKIP,ICON('list3.ico')
                           PROMPT('Manufacturer'),AT(8,100),USE(?LOA:Manufacturer:Prompt),TRN
                           PROMPT('Colour'),AT(8,116),USE(?XCH:Colour:Prompt),TRN
                           ENTRY(@s30),AT(80,116,124,10),USE(xch:Colour),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(208,116,10,10),USE(?LookupModelColour),SKIP,ICON('list3.ico')
                           ENTRY(@s30),AT(80,100,124,10),USE(xch:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Location'),AT(8,132),USE(?XCH:Location:Prompt),TRN
                           ENTRY(@s30),AT(80,132,124,10),USE(xch:Location),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(208,132,10,10),USE(?LookupLocation),SKIP,ICON('list3.ico')
                           PROMPT('Shelf Location'),AT(8,148),USE(?XCH:Shelf_Location:Prompt),TRN
                           ENTRY(@s30),AT(80,148,124,10),USE(xch:Shelf_Location),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(208,148,10,10),USE(?LookupShelfLocation),SKIP,ICON('list3.ico')
                           PROMPT('Stock Type'),AT(8,164),USE(?XCH:Stock_Type:Prompt),TRN
                           ENTRY(@s30),AT(80,164,124,10),USE(xch:Stock_Type),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(208,164,10,10),USE(?LookupStockType),SKIP,ICON('list3.ico')
                           PROMPT('Audit Number'),AT(8,180),USE(?XCH:Stock_Type:Prompt:2),TRN
                           LIST,AT(80,180,64,10),USE(xch:Audit_Number),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('32L(2)|M@s8@'),DROP(10,64),FROM(Queue:FileDrop)
                           BUTTON('New Audit Nos'),AT(144,180,72,10),USE(?New_Audit_Number),FLAT,LEFT,ICON('Asterisk.ico')
                         END
                       END
                       SHEET,AT(224,4,156,196),USE(?Sheet2),SPREAD
                         TAB('Accessories'),USE(?Tab2)
                           ENTRY(@s30),AT(228,20,125,10),USE(xca:Accessory)
                           LIST,AT(228,36,148,140),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)@s30@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(228,180,56,16),USE(?Button4),LEFT,ICON('insert.gif')
                           BUTTON('&Delete'),AT(320,180,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                         END
                       END
                       PANEL,AT(4,204,376,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Exchange History'),AT(8,208,60,16),USE(?Button5),LEFT,ICON('history.gif')
                       BUTTON('Print Label'),AT(72,208,56,16),USE(?Print_Label),LEFT,ICON('Print.gif')
                       BUTTON('&OK'),AT(260,208,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(320,208,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  IncrementalLocatorClass          !Default Locator
FDB9                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_xch_ali_id   ushort,auto
!Save Entry Fields Incase Of Lookup
look:xch:Model_Number                Like(xch:Model_Number)
look:xch:Colour                Like(xch:Colour)
look:xch:Location                Like(xch:Location)
look:xch:Shelf_Location                Like(xch:Shelf_Location)
look:xch:Stock_Type                Like(xch:Stock_Type)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?LOA:Ref_Number:Prompt{prop:FontColor} = -1
    ?LOA:Ref_Number:Prompt{prop:Color} = 15066597
    If ?xch:Ref_Number{prop:ReadOnly} = True
        ?xch:Ref_Number{prop:FontColor} = 65793
        ?xch:Ref_Number{prop:Color} = 15066597
    Elsif ?xch:Ref_Number{prop:Req} = True
        ?xch:Ref_Number{prop:FontColor} = 65793
        ?xch:Ref_Number{prop:Color} = 8454143
    Else ! If ?xch:Ref_Number{prop:Req} = True
        ?xch:Ref_Number{prop:FontColor} = 65793
        ?xch:Ref_Number{prop:Color} = 16777215
    End ! If ?xch:Ref_Number{prop:Req} = True
    ?xch:Ref_Number{prop:Trn} = 0
    ?xch:Ref_Number{prop:FontStyle} = font:Bold
    If ?xch:Date_Booked{prop:ReadOnly} = True
        ?xch:Date_Booked{prop:FontColor} = 65793
        ?xch:Date_Booked{prop:Color} = 15066597
    Elsif ?xch:Date_Booked{prop:Req} = True
        ?xch:Date_Booked{prop:FontColor} = 65793
        ?xch:Date_Booked{prop:Color} = 8454143
    Else ! If ?xch:Date_Booked{prop:Req} = True
        ?xch:Date_Booked{prop:FontColor} = 65793
        ?xch:Date_Booked{prop:Color} = 16777215
    End ! If ?xch:Date_Booked{prop:Req} = True
    ?xch:Date_Booked{prop:Trn} = 0
    ?xch:Date_Booked{prop:FontStyle} = font:Bold
    ?status_temp:Prompt{prop:FontColor} = -1
    ?status_temp:Prompt{prop:Color} = 15066597
    If ?status_temp{prop:ReadOnly} = True
        ?status_temp{prop:FontColor} = 65793
        ?status_temp{prop:Color} = 15066597
    Elsif ?status_temp{prop:Req} = True
        ?status_temp{prop:FontColor} = 65793
        ?status_temp{prop:Color} = 8454143
    Else ! If ?status_temp{prop:Req} = True
        ?status_temp{prop:FontColor} = 65793
        ?status_temp{prop:Color} = 16777215
    End ! If ?status_temp{prop:Req} = True
    ?status_temp{prop:Trn} = 0
    ?status_temp{prop:FontStyle} = font:Bold
    ?LOA:ESN:Prompt{prop:FontColor} = -1
    ?LOA:ESN:Prompt{prop:Color} = 15066597
    If ?xch:ESN{prop:ReadOnly} = True
        ?xch:ESN{prop:FontColor} = 65793
        ?xch:ESN{prop:Color} = 15066597
    Elsif ?xch:ESN{prop:Req} = True
        ?xch:ESN{prop:FontColor} = 65793
        ?xch:ESN{prop:Color} = 8454143
    Else ! If ?xch:ESN{prop:Req} = True
        ?xch:ESN{prop:FontColor} = 65793
        ?xch:ESN{prop:Color} = 16777215
    End ! If ?xch:ESN{prop:Req} = True
    ?xch:ESN{prop:Trn} = 0
    ?xch:ESN{prop:FontStyle} = font:Bold
    ?xch:MSN:Prompt{prop:FontColor} = -1
    ?xch:MSN:Prompt{prop:Color} = 15066597
    If ?xch:MSN{prop:ReadOnly} = True
        ?xch:MSN{prop:FontColor} = 65793
        ?xch:MSN{prop:Color} = 15066597
    Elsif ?xch:MSN{prop:Req} = True
        ?xch:MSN{prop:FontColor} = 65793
        ?xch:MSN{prop:Color} = 8454143
    Else ! If ?xch:MSN{prop:Req} = True
        ?xch:MSN{prop:FontColor} = 65793
        ?xch:MSN{prop:Color} = 16777215
    End ! If ?xch:MSN{prop:Req} = True
    ?xch:MSN{prop:Trn} = 0
    ?xch:MSN{prop:FontStyle} = font:Bold
    ?XCH:Model_Number:Prompt{prop:FontColor} = -1
    ?XCH:Model_Number:Prompt{prop:Color} = 15066597
    If ?xch:Model_Number{prop:ReadOnly} = True
        ?xch:Model_Number{prop:FontColor} = 65793
        ?xch:Model_Number{prop:Color} = 15066597
    Elsif ?xch:Model_Number{prop:Req} = True
        ?xch:Model_Number{prop:FontColor} = 65793
        ?xch:Model_Number{prop:Color} = 8454143
    Else ! If ?xch:Model_Number{prop:Req} = True
        ?xch:Model_Number{prop:FontColor} = 65793
        ?xch:Model_Number{prop:Color} = 16777215
    End ! If ?xch:Model_Number{prop:Req} = True
    ?xch:Model_Number{prop:Trn} = 0
    ?xch:Model_Number{prop:FontStyle} = font:Bold
    ?LOA:Manufacturer:Prompt{prop:FontColor} = -1
    ?LOA:Manufacturer:Prompt{prop:Color} = 15066597
    ?XCH:Colour:Prompt{prop:FontColor} = -1
    ?XCH:Colour:Prompt{prop:Color} = 15066597
    If ?xch:Colour{prop:ReadOnly} = True
        ?xch:Colour{prop:FontColor} = 65793
        ?xch:Colour{prop:Color} = 15066597
    Elsif ?xch:Colour{prop:Req} = True
        ?xch:Colour{prop:FontColor} = 65793
        ?xch:Colour{prop:Color} = 8454143
    Else ! If ?xch:Colour{prop:Req} = True
        ?xch:Colour{prop:FontColor} = 65793
        ?xch:Colour{prop:Color} = 16777215
    End ! If ?xch:Colour{prop:Req} = True
    ?xch:Colour{prop:Trn} = 0
    ?xch:Colour{prop:FontStyle} = font:Bold
    If ?xch:Manufacturer{prop:ReadOnly} = True
        ?xch:Manufacturer{prop:FontColor} = 65793
        ?xch:Manufacturer{prop:Color} = 15066597
    Elsif ?xch:Manufacturer{prop:Req} = True
        ?xch:Manufacturer{prop:FontColor} = 65793
        ?xch:Manufacturer{prop:Color} = 8454143
    Else ! If ?xch:Manufacturer{prop:Req} = True
        ?xch:Manufacturer{prop:FontColor} = 65793
        ?xch:Manufacturer{prop:Color} = 16777215
    End ! If ?xch:Manufacturer{prop:Req} = True
    ?xch:Manufacturer{prop:Trn} = 0
    ?xch:Manufacturer{prop:FontStyle} = font:Bold
    ?XCH:Location:Prompt{prop:FontColor} = -1
    ?XCH:Location:Prompt{prop:Color} = 15066597
    If ?xch:Location{prop:ReadOnly} = True
        ?xch:Location{prop:FontColor} = 65793
        ?xch:Location{prop:Color} = 15066597
    Elsif ?xch:Location{prop:Req} = True
        ?xch:Location{prop:FontColor} = 65793
        ?xch:Location{prop:Color} = 8454143
    Else ! If ?xch:Location{prop:Req} = True
        ?xch:Location{prop:FontColor} = 65793
        ?xch:Location{prop:Color} = 16777215
    End ! If ?xch:Location{prop:Req} = True
    ?xch:Location{prop:Trn} = 0
    ?xch:Location{prop:FontStyle} = font:Bold
    ?XCH:Shelf_Location:Prompt{prop:FontColor} = -1
    ?XCH:Shelf_Location:Prompt{prop:Color} = 15066597
    If ?xch:Shelf_Location{prop:ReadOnly} = True
        ?xch:Shelf_Location{prop:FontColor} = 65793
        ?xch:Shelf_Location{prop:Color} = 15066597
    Elsif ?xch:Shelf_Location{prop:Req} = True
        ?xch:Shelf_Location{prop:FontColor} = 65793
        ?xch:Shelf_Location{prop:Color} = 8454143
    Else ! If ?xch:Shelf_Location{prop:Req} = True
        ?xch:Shelf_Location{prop:FontColor} = 65793
        ?xch:Shelf_Location{prop:Color} = 16777215
    End ! If ?xch:Shelf_Location{prop:Req} = True
    ?xch:Shelf_Location{prop:Trn} = 0
    ?xch:Shelf_Location{prop:FontStyle} = font:Bold
    ?XCH:Stock_Type:Prompt{prop:FontColor} = -1
    ?XCH:Stock_Type:Prompt{prop:Color} = 15066597
    If ?xch:Stock_Type{prop:ReadOnly} = True
        ?xch:Stock_Type{prop:FontColor} = 65793
        ?xch:Stock_Type{prop:Color} = 15066597
    Elsif ?xch:Stock_Type{prop:Req} = True
        ?xch:Stock_Type{prop:FontColor} = 65793
        ?xch:Stock_Type{prop:Color} = 8454143
    Else ! If ?xch:Stock_Type{prop:Req} = True
        ?xch:Stock_Type{prop:FontColor} = 65793
        ?xch:Stock_Type{prop:Color} = 16777215
    End ! If ?xch:Stock_Type{prop:Req} = True
    ?xch:Stock_Type{prop:Trn} = 0
    ?xch:Stock_Type{prop:FontStyle} = font:Bold
    ?XCH:Stock_Type:Prompt:2{prop:FontColor} = -1
    ?XCH:Stock_Type:Prompt:2{prop:Color} = 15066597
    ?xch:Audit_Number{prop:FontColor} = 65793
    ?xch:Audit_Number{prop:Color}= 16777215
    ?xch:Audit_Number{prop:Color,2} = 16777215
    ?xch:Audit_Number{prop:Color,3} = 12937777
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?xca:Accessory{prop:ReadOnly} = True
        ?xca:Accessory{prop:FontColor} = 65793
        ?xca:Accessory{prop:Color} = 15066597
    Elsif ?xca:Accessory{prop:Req} = True
        ?xca:Accessory{prop:FontColor} = 65793
        ?xca:Accessory{prop:Color} = 8454143
    Else ! If ?xca:Accessory{prop:Req} = True
        ?xca:Accessory{prop:FontColor} = 65793
        ?xca:Accessory{prop:Color} = 16777215
    End ! If ?xca:Accessory{prop:Req} = True
    ?xca:Accessory{prop:Trn} = 0
    ?xca:Accessory{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
msn_check       Routine
    Hide(?xch:msn)
    Hide(?xch:msn:prompt)
    access:modelnum.clearkey(mod:model_number_key)
    mod:model_number    = xch:model_number
    If access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
        access:manufact.clearkey(man:manufacturer_key)
        man:manufacturer    = mod:manufacturer
        If access:manufact.fetch(man:manufacturer_key) = Level:Benign
            If man:use_msn = 'YES'
                Unhide(?xch:msn)
                Unhide(?xch:msn:prompt)
            End
        End!If access:manufact.fetch(man:manufacturer_key)
    End!If access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
    Display()
Fill_Lists      Routine


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Exchange',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Exchange',1)
    SolaceViewVars('save_moc_id',save_moc_id,'Update_Exchange',1)
    SolaceViewVars('audit_number_temp',audit_number_temp,'Update_Exchange',1)
    SolaceViewVars('esn_temp',esn_temp,'Update_Exchange',1)
    SolaceViewVars('print_label_temp',print_label_temp,'Update_Exchange',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Update_Exchange',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Exchange',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Exchange',1)
    SolaceViewVars('RecordChanged',RecordChanged,'Update_Exchange',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'Update_Exchange',1)
    SolaceViewVars('status_temp',status_temp,'Update_Exchange',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOA:Ref_Number:Prompt;  SolaceCtrlName = '?LOA:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Ref_Number;  SolaceCtrlName = '?xch:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Date_Booked;  SolaceCtrlName = '?xch:Date_Booked';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_temp:Prompt;  SolaceCtrlName = '?status_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_temp;  SolaceCtrlName = '?status_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOA:ESN:Prompt;  SolaceCtrlName = '?LOA:ESN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:ESN;  SolaceCtrlName = '?xch:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:MSN:Prompt;  SolaceCtrlName = '?xch:MSN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:MSN;  SolaceCtrlName = '?xch:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Model_Number:Prompt;  SolaceCtrlName = '?XCH:Model_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Model_Number;  SolaceCtrlName = '?xch:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupModelNumber;  SolaceCtrlName = '?LookupModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOA:Manufacturer:Prompt;  SolaceCtrlName = '?LOA:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Colour:Prompt;  SolaceCtrlName = '?XCH:Colour:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Colour;  SolaceCtrlName = '?xch:Colour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupModelColour;  SolaceCtrlName = '?LookupModelColour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Manufacturer;  SolaceCtrlName = '?xch:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Location:Prompt;  SolaceCtrlName = '?XCH:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Location;  SolaceCtrlName = '?xch:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Shelf_Location:Prompt;  SolaceCtrlName = '?XCH:Shelf_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Shelf_Location;  SolaceCtrlName = '?xch:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupShelfLocation;  SolaceCtrlName = '?LookupShelfLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Stock_Type:Prompt;  SolaceCtrlName = '?XCH:Stock_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Stock_Type;  SolaceCtrlName = '?xch:Stock_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStockType;  SolaceCtrlName = '?LookupStockType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Stock_Type:Prompt:2;  SolaceCtrlName = '?XCH:Stock_Type:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Audit_Number;  SolaceCtrlName = '?xch:Audit_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?New_Audit_Number;  SolaceCtrlName = '?New_Audit_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xca:Accessory;  SolaceCtrlName = '?xca:Accessory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button5;  SolaceCtrlName = '?Button5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Label;  SolaceCtrlName = '?Print_Label';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Exchange Unit'
  OF ChangeRecord
    ActionMessage = 'Changing An Exchange Unit'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Exchange')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Exchange')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOA:Ref_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(xch:Record,History::xch:Record)
  SELF.AddHistoryField(?xch:Ref_Number,1)
  SELF.AddHistoryField(?xch:Date_Booked,9)
  SELF.AddHistoryField(?xch:ESN,4)
  SELF.AddHistoryField(?xch:MSN,5)
  SELF.AddHistoryField(?xch:Model_Number,2)
  SELF.AddHistoryField(?xch:Colour,6)
  SELF.AddHistoryField(?xch:Manufacturer,3)
  SELF.AddHistoryField(?xch:Location,7)
  SELF.AddHistoryField(?xch:Shelf_Location,8)
  SELF.AddHistoryField(?xch:Stock_Type,13)
  SELF.AddHistoryField(?xch:Audit_Number,14)
  SELF.AddUpdateFile(Access:EXCHANGE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCESSOR.Open
  Relate:DEFAULTS.Open
  Relate:ESNMODEL.Open
  Relate:EXCAUDIT.Open
  Relate:USERS_ALIAS.Open
  Access:MANUFACT.UseFile
  Access:LOAN.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:MODELNUM.UseFile
  Access:MODELCOL.UseFile
  Access:LOCATION.UseFile
  Access:LOCSHELF.UseFile
  Access:STOCKTYP.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:EXCHANGE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:EXCHACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  !Status
  CASE (XCH:Available)
  OF 'AVL'
      status_temp = 'AVAILABLE'
  OF 'EXC'
      status_temp = 'EXCHANGED - JOB NO: ' & XCH:Job_Number
  OF 'INC'
      status_temp = 'INCOMING TRANSIT - JOB NO: ' & XCH:Job_Number
  OF 'FAU'
      status_temp = 'FAULTY'
  Of 'NOA'
      Status_Temp = 'NOT AVAILABLE'
  OF 'REP'
      status_temp = 'IN REPAIR - JOB NO: ' & XCH:Job_Number
  OF 'SUS'
      status_temp = 'SUSPENDED'
  OF 'DES'
      status_temp = 'DESPATCHED - JOB NO: ' & XCH:Job_Number
  OF 'QA1'
      status_temp = 'ELECTRONIC QA REQUIRED - JOB NO: ' & XCH:Job_Number
  OF 'QA2'
      status_temp = 'MANUAL QA REQUIRED - JOB NO: ' & XCH:Job_Number
  OF 'QAF'
      status_temp = 'QA FAILED'
  OF 'RTS'
      status_temp = 'RETURN TO STOCK'
  Of 'INR'
      Status_Temp = 'EXCHANGE REPAIR'
  ELSE
    status_temp = 'IN REPAIR - JOB NO: ' & XCH:Job_Number
  END
  Do RecolourWindow
  ?xch:Audit_Number{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  IF ?xch:Model_Number{Prop:Tip} AND ~?LookupModelNumber{Prop:Tip}
     ?LookupModelNumber{Prop:Tip} = 'Select ' & ?xch:Model_Number{Prop:Tip}
  END
  IF ?xch:Model_Number{Prop:Msg} AND ~?LookupModelNumber{Prop:Msg}
     ?LookupModelNumber{Prop:Msg} = 'Select ' & ?xch:Model_Number{Prop:Msg}
  END
  IF ?xch:Colour{Prop:Tip} AND ~?LookupModelColour{Prop:Tip}
     ?LookupModelColour{Prop:Tip} = 'Select ' & ?xch:Colour{Prop:Tip}
  END
  IF ?xch:Colour{Prop:Msg} AND ~?LookupModelColour{Prop:Msg}
     ?LookupModelColour{Prop:Msg} = 'Select ' & ?xch:Colour{Prop:Msg}
  END
  IF ?xch:Location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?xch:Location{Prop:Tip}
  END
  IF ?xch:Location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?xch:Location{Prop:Msg}
  END
  IF ?xch:Shelf_Location{Prop:Tip} AND ~?LookupShelfLocation{Prop:Tip}
     ?LookupShelfLocation{Prop:Tip} = 'Select ' & ?xch:Shelf_Location{Prop:Tip}
  END
  IF ?xch:Shelf_Location{Prop:Msg} AND ~?LookupShelfLocation{Prop:Msg}
     ?LookupShelfLocation{Prop:Msg} = 'Select ' & ?xch:Shelf_Location{Prop:Msg}
  END
  IF ?xch:Stock_Type{Prop:Tip} AND ~?LookupStockType{Prop:Tip}
     ?LookupStockType{Prop:Tip} = 'Select ' & ?xch:Stock_Type{Prop:Tip}
  END
  IF ?xch:Stock_Type{Prop:Msg} AND ~?LookupStockType{Prop:Msg}
     ?LookupStockType{Prop:Msg} = 'Select ' & ?xch:Stock_Type{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,xca:Ref_Number_Key)
  BRW7.AddRange(xca:Ref_Number,xch:Ref_Number)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(?xca:Accessory,xca:Accessory,1,BRW7)
  BRW7.AddField(xca:Accessory,BRW7.Q.xca:Accessory)
  BRW7.AddField(xca:Ref_Number,BRW7.Q.xca:Ref_Number)
  BRW7.AskProcedure = 6
  FDB9.Init(?xch:Audit_Number,Queue:FileDrop.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop,Relate:EXCAUDIT,ThisWindow)
  FDB9.Q &= Queue:FileDrop
  FDB9.AddSortOrder(exa:TypeStockNumber)
  FDB9.AddRange(exa:Stock_Type,xch:Stock_Type)
  FDB9.SetFilter('Upper(exa:stock_unit_number) = 0')
  FDB9.AddField(exa:Audit_Number,FDB9.Q.exa:Audit_Number)
  FDB9.AddField(exa:Record_Number,FDB9.Q.exa:Record_Number)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCESSOR.Close
    Relate:DEFAULTS.Close
    Relate:ESNMODEL.Close
    Relate:EXCAUDIT.Close
    Relate:USERS_ALIAS.Close
  END
  If print_label_temp <> 'NO' And thiswindow.request = insertrecord
      glo:select1 = xch:ref_number
      case print_label_temp
          of '1'
              exchange_unit_label
          of '2'
              exchange_unit_label_b452
      end!case def:label_printer_type
      glo:select1 = ''
  End!If print_label_temp = 'YES' And thiswindow.request = insertrecord
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Exchange',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    xch:Date_Booked = Today()
    xch:Available = 'AVL'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      BrowseMODELNUM
      Browse_Colour_Model
      Select_Location
      Select_Shelf_Location
      Select_Stock_Type
      Update_Exchange_Accessory
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Button5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      glo:select12 = xch:ref_number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?xch:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:ESN, Accepted)
      Esn_Model_Routine(xch:esn,xch:model_number,model",pass")
      If pass" = 1
          xch:model_number = model"
          access:modelnum.clearkey(mod:model_number_key)
          mod:model_number = xch:model_number
          if access:modelnum.fetch(mod:model_number_key) = Level:Benign
              xch:manufacturer = mod:manufacturer
          end
          
          Post(event:accepted,?xch:model_number)
          Do msn_check
          If ?xch:msn{prop:hide} = 0
              Select(?xch:msn)
          Else!If ?xsn:msn{prop:hide} = 0
              If xch:colour <> '' and thiswindow.request  = Insertrecord
                  Select(?xch:location)
              Else!If xch:colour <> '' and thiswindow.request  = Insertrecord
                  Select(?xch:colour)
              End!If xch:colour <> '' and thiswindow.request  = Insertrecord
          End!If ?xsn:msn{prop:hide} = 0
      End!If fill_in# = 1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:ESN, Accepted)
    OF ?xch:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:MSN, Accepted)
      If xch:model_number <> '' And thiswindow.request = Insertrecord
          If xch:colour <> ''
              Select(?xch:location)
          Else!If xch:colour <> ''
              Select(?xch:colour)
          End!If xch:colour <> ''
      End!If xch:model_number <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:MSN, Accepted)
    OF ?xch:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Model_Number, Accepted)
      IF xch:Model_Number OR ?xch:Model_Number{Prop:Req}
        mod:Model_Number = xch:Model_Number
        !Save Lookup Field Incase Of error
        look:xch:Model_Number        = xch:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            xch:Model_Number = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            xch:Model_Number = look:xch:Model_Number
            SELECT(?xch:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      xch:manufacturer  = mod:manufacturer
      count# = 0
      colour" = ''
      save_moc_id = access:modelcol.savefile()
      access:modelcol.clearkey(moc:colour_key)
      moc:model_number = xch:model_number
      set(moc:colour_key,moc:colour_key)
      loop
          if access:modelcol.next()
             break
          end !if
          if moc:model_number <> xch:model_number      |
              then break.  ! end if
          count# += 1
          colour" = moc:colour
          If count# > 1
              Break
          End!If count# > 1
      end !loop
      access:modelcol.restorefile(save_moc_id)
      If count# = 1
          xch:colour  = colour"
      End!If count# = 1
      Do msn_check
      Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Model_Number, Accepted)
    OF ?LookupModelNumber
      ThisWindow.Update
      mod:Model_Number = xch:Model_Number
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          xch:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?xch:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?xch:Model_Number)
    OF ?xch:Colour
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Colour, Accepted)
      glo:select12   = xch:model_number
      moc:model_number  = xch:model_number
      IF xch:Colour OR ?xch:Colour{Prop:Req}
        moc:Colour = xch:Colour
        !Save Lookup Field Incase Of error
        look:xch:Colour        = xch:Colour
        IF Access:MODELCOL.TryFetch(moc:Colour_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            xch:Colour = moc:Colour
          ELSE
            !Restore Lookup On Error
            xch:Colour = look:xch:Colour
            SELECT(?xch:Colour)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:select12   = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Colour, Accepted)
    OF ?LookupModelColour
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupModelColour, Accepted)
      glo:select12   = xch:model_number
      moc:Colour = xch:Colour
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          xch:Colour = moc:Colour
          Select(?+1)
      ELSE
          Select(?xch:Colour)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?xch:Colour)
      glo:select12   = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupModelColour, Accepted)
    OF ?xch:Location
      IF xch:Location OR ?xch:Location{Prop:Req}
        loc:Location = xch:Location
        !Save Lookup Field Incase Of error
        look:xch:Location        = xch:Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            xch:Location = loc:Location
          ELSE
            !Restore Lookup On Error
            xch:Location = look:xch:Location
            SELECT(?xch:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loc:Location = xch:Location
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          xch:Location = loc:Location
          Select(?+1)
      ELSE
          Select(?xch:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?xch:Location)
    OF ?xch:Shelf_Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Shelf_Location, Accepted)
      glo:select2    = xch:location
      los:site_location = xch:location
      IF xch:Shelf_Location OR ?xch:Shelf_Location{Prop:Req}
        los:Shelf_Location = xch:Shelf_Location
        !Save Lookup Field Incase Of error
        look:xch:Shelf_Location        = xch:Shelf_Location
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            xch:Shelf_Location = los:Shelf_Location
          ELSE
            !Restore Lookup On Error
            xch:Shelf_Location = look:xch:Shelf_Location
            SELECT(?xch:Shelf_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:select2    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Shelf_Location, Accepted)
    OF ?LookupShelfLocation
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupShelfLocation, Accepted)
      glo:select2    = xch:location
      los:Shelf_Location = xch:Shelf_Location
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          xch:Shelf_Location = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?xch:Shelf_Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?xch:Shelf_Location)
      glo:select2    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupShelfLocation, Accepted)
    OF ?xch:Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Stock_Type, Accepted)
      glo:select1    = 'EXC'
      stp:use_exchange  = 'YES'
      IF xch:Stock_Type OR ?xch:Stock_Type{Prop:Req}
        stp:Stock_Type = xch:Stock_Type
        !Save Lookup Field Incase Of error
        look:xch:Stock_Type        = xch:Stock_Type
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            xch:Stock_Type = stp:Stock_Type
          ELSE
            !Restore Lookup On Error
            xch:Stock_Type = look:xch:Stock_Type
            SELECT(?xch:Stock_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Stock_Type, Accepted)
    OF ?LookupStockType
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupStockType, Accepted)
      glo:select1    = 'EXC'
      stp:Stock_Type = xch:Stock_Type
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          xch:Stock_Type = stp:Stock_Type
          Select(?+1)
      ELSE
          Select(?xch:Stock_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?xch:Stock_Type)
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupStockType, Accepted)
    OF ?New_Audit_Number
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?New_Audit_Number, Accepted)
      If SecurityCheck('EXCHANGE - NEW AUDIT NOS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
        do_update# = false
      Else!if x" = false
          If xch:stock_type = ''
              Case MessageEx('You must insert a Stock Type.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Else!If xch:stock_type = ''
              Add_Exchange_Audit_Numbers(xch:stock_type)
              Do fill_lists
          End!If xch:stock_type = ''
      end!if x" = false
      FDB9.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?New_Audit_Number, Accepted)
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      glo:select1 = xch:model_number
      Pick_Accessories
      IF Records(glo:Q_Accessory) <> ''
          Loop x# = 1 To Records(glo:Q_Accessory)
              get(glo:Q_Accessory,x#)
              get(exchacc,0)
              if access:exchacc.primerecord() = level:benign
                  xca:ref_number  = xch:ref_number
                  xca:accessory   = glo:accessory_pointer
                  access:exchacc.tryinsert()
              end!if access:exchacc.primerecord() = level:benign
          End
          BRW7.ResetSort(1)
      End
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?Button5
      ThisWindow.Update
      Browse_Exchange_History
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      glo:select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    OF ?Print_Label
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Label, Accepted)
      If thiswindow.request = Insertrecord
          Case MessageEx('Cannot print a label until this record has been saved.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If thiswindow.request = Insertrecord
          glo:select1 = xch:ref_number
          set(defaults)
          access:defaults.next()
          case def:label_printer_type
              of 'TEC B-440 / B-442'
                  exchange_unit_label
              of 'TEC B-452'
                  exchange_unit_label_b452
          end!case def:label_printer_type
          glo:select1 = ''
      End!If thiswindow.request = Insertrecord
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Label, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  error# = 0
  If esn_temp <> xch:esn
  
      setcursor(cursor:wait)
      save_xch_ali_id = access:exchange_alias.savefile()
      access:exchange_alias.clearkey(xch_ali:esn_only_key)
      xch_ali:esn = xch:esn
      set(xch_ali:esn_only_key,xch_ali:esn_only_key)
      loop
          if access:exchange_alias.next()
             break
          end !if
          if xch_ali:esn <> xch:esn      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          If xch_ali:ref_number <> xch:ref_number
              Case MessageEx('This E.S.N. / I.M.E.I. has already been entered as an Exchange Stock Item.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
              Break
          End!If xch_ali:ref_number <> xch:ref_number
      end !loop
      access:exchange_alias.restorefile(save_xch_ali_id)
      setcursor()
  
      If error# = 0
          access:loan.clearkey(loa:esn_key)
          loa:esn = xch:esn
          if access:loan.fetch(loa:esn_key) = Level:Benign
              Case MessageEx('This E.S.N. / I.M.E.I. has already been entered as a Loan Stock Item.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
          End!if access:loan.fetch(loa:esn_key) = Level:Benign
      End!If error# = 0
      If error# = 0
          access:jobs_alias.clearkey(job_ali:esn_key)
          job_ali:esn = xch:esn
          if access:jobs_alias.fetch(job_ali:esn_key) = Level:Benign
             If job_ali:date_completed = ''
                  error# = 1
                  Case MessageEx('You are attempting to insert an Exchange Unit with the same E.S.N. / I.M.E.I. as job number '&clip(job_ali:ref_number)&'.<13,10><13,10>You must use Rapid Job Update if you wish to return this unit to Exchange Stock.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
             End!If job:date_completed <> ''
          end!if access:jobs.fetch(job:esn_key) = Level:Benign
      end!IF error# = 0
      If error# = 0
          access:modelnum.clearkey(mod:model_number_key)
          mod:model_number = xch:model_number
          if access:modelnum.fetch(mod:model_number_key) = Level:benign
              If Len(Clip(xch:esn)) < mod:esn_length_from Or Len(Clip(xch:esn)) > mod:esn_length_to
                  Case MessageEx('The E.S.N. / I.M.E.I. entered should be between '&clip(mod:esn_length_from)&' and '&clip(mod:esn_length_to)&' characters in length.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
              If man:use_msn = 'YES'
                  If len(Clip(xch:msn)) < mod:msn_length_from Or len(Clip(xch:msn)) > mod:msn_length_to
                      Case MessageEx('The M.S.N. entered should be between '&clip(mod:msn_length_from)&' and '&Clip(mod:msn_length_to)&' characters in length.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End!If len(job:msn) < mod:msn_length_from Or len(job:msn) > mod:msn_length_to
              End!If man:use_msn = 'YES'
          end !if access:modelnum.fetch(mod:model_number_key) = Level:benign
      End!IF error# = 0
      If error# = 1
          Select(?xch:esn)
          Cycle
      End
  End!If esn_temp <> xch:esn
  !Audit Number Check
  If thiswindow.request <> Insertrecord
      If audit_number_temp <> xch:audit_number
          If xch:audit_number <> ''
              Case MessageEx('Are you sure you want to change the Audit Number of this Exchange Unit?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      access:excaudit.clearkey(exa:audit_number_key)
                      exa:stock_type   = xch:stock_type
                      exa:audit_number = xch:audit_number
                      if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                          !This should never happen, because used audit numbers aren't shown in the list
                      Else!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                          get(exchhist,0)
                          if access:exchhist.primerecord() = level:benign
                              exh:ref_number   = xch:ref_number
                              exh:date          = today()
                              exh:time          = clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              exh:user = use:user_code
                              exh:status        = 'AUDIT NUMBER CHANGED TO' & Clip(xch:audit_number)
                              access:exchhist.insert()
                          end!if access:exchhist.primerecord() = level:benign
                      end!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                  Of 2 ! &No Button
                      xch:audit_number    = audit_number_temp
              End!Case MessageEx
          Else!If xch:audit_number <> ''
  
          End!If xch:audit_number <> ''
      End!If audit_number_temp = xch:audit_number
  End!If thiswindow.request <> Insertrecord
  
  access:excaudit.clearkey(exa:audit_number_key)
  exa:stock_type   = xch:stock_type
  exa:audit_number = xch:audit_number
  if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
      If exa:stock_unit_number = 0
          exa:stock_unit_number = xch:ref_number
          access:excaudit.update()
          get(exchhist,0)
          if access:exchhist.primerecord() = level:benign
              exh:ref_number   = xch:ref_number
              exh:date          = today()
              exh:time          = clock()
              access:users.clearkey(use:password_key)
              use:password =glo:password
              access:users.fetch(use:password_key)
              exh:user = use:user_code
              exh:status        = 'ASSIGNED AUDIT NUMBER: ' & Clip(xch:audit_number)
              access:exchhist.insert()
          end!if access:exchhist.primerecord() = level:benign
      End!If exa:stock_unit_number = ''
  end!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
  
  
  If thiswindow.request = Insertrecord
      Set(defaults)
      access:defaults.next()
      If def:use_loan_exchange_label = 'YES'
          case def:label_printer_type
              of 'TEC B-440 / B-442'
                  print_label_temp = '1'
              of 'TEC B-452'
                  print_label_temp = '2'
          end!case def:label_printer_type
      Else
          Print_label_temp = 'NO'
      End!If def:use_loan_exchange_label = 'YES'
  Else
      print_label_temp = 'NO'
  End!If thiswindow.request = Insertrecord
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Exchange')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?xch:Model_Number
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Model_Number, AlertKey)
      Post(Event:Accepted,?LookupModelNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Model_Number, AlertKey)
    END
  OF ?xch:Colour
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Colour, AlertKey)
      Post(event:accepted,?LookupModelColour)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Colour, AlertKey)
    END
  OF ?xch:Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Location, AlertKey)
      Post(event:accepted,?LookupLocation)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Location, AlertKey)
    END
  OF ?xch:Shelf_Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Shelf_Location, AlertKey)
      Post(Event:accepted,?LookupShelfLocation)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Shelf_Location, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?xca:Accessory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xca:Accessory, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xca:Accessory, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do msn_check
      esn_temp = xch:esn
      audit_number_temp   = xch:audit_number
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW7.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

