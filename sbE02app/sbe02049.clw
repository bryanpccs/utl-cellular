

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02049.INC'),ONCE        !Local module procedure declarations
                     END


AttachModelsToSC PROCEDURE (scAccountID, bookingType) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
result               BYTE
tmp:ServiceCentreName STRING(30)
tmp:Exceptions       STRING(5000)
tmp:SCAccountID      LONG
i                    LONG
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:ServiceCentreName
srv_ali:ServiceCentreName LIKE(srv_ali:ServiceCentreName) !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(SIDSRVCN_ALIAS)
                       PROJECT(srv_ali:ServiceCentreName)
                     END
window               WINDOW('Attach Models To Service Centre'),AT(,,338,208),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,330,172),USE(?Sheet1),SPREAD
                         TAB('Process Details'),USE(?Tab1)
                           STRING('Service Centre'),AT(8,20),USE(?tmp:ServiceCentreName:Prompt),TRN
                           COMBO(@s30),AT(72,20,124,10),USE(tmp:ServiceCentreName),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Model Exceptions'),AT(8,36),USE(?tmp:Exceptions:Prompt),TRN
                           TEXT,AT(8,48,320,124),USE(tmp:Exceptions),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                         END
                       END
                       PANEL,AT(4,180,330,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(216,184,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(272,184,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(result)

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:ServiceCentreName:Prompt{prop:FontColor} = -1
    ?tmp:ServiceCentreName:Prompt{prop:Color} = 15066597
    If ?tmp:ServiceCentreName{prop:ReadOnly} = True
        ?tmp:ServiceCentreName{prop:FontColor} = 65793
        ?tmp:ServiceCentreName{prop:Color} = 15066597
    Elsif ?tmp:ServiceCentreName{prop:Req} = True
        ?tmp:ServiceCentreName{prop:FontColor} = 65793
        ?tmp:ServiceCentreName{prop:Color} = 8454143
    Else ! If ?tmp:ServiceCentreName{prop:Req} = True
        ?tmp:ServiceCentreName{prop:FontColor} = 65793
        ?tmp:ServiceCentreName{prop:Color} = 16777215
    End ! If ?tmp:ServiceCentreName{prop:Req} = True
    ?tmp:ServiceCentreName{prop:Trn} = 0
    ?tmp:ServiceCentreName{prop:FontStyle} = font:Bold
    ?tmp:Exceptions:Prompt{prop:FontColor} = -1
    ?tmp:Exceptions:Prompt{prop:Color} = 15066597
    If ?tmp:Exceptions{prop:ReadOnly} = True
        ?tmp:Exceptions{prop:FontColor} = 65793
        ?tmp:Exceptions{prop:Color} = 15066597
    Elsif ?tmp:Exceptions{prop:Req} = True
        ?tmp:Exceptions{prop:FontColor} = 65793
        ?tmp:Exceptions{prop:Color} = 8454143
    Else ! If ?tmp:Exceptions{prop:Req} = True
        ?tmp:Exceptions{prop:FontColor} = 65793
        ?tmp:Exceptions{prop:Color} = 16777215
    End ! If ?tmp:Exceptions{prop:Req} = True
    ?tmp:Exceptions{prop:Trn} = 0
    ?tmp:Exceptions{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Check:Exceptions routine

    ! Check for exceptions
    tmp:Exceptions = ''

    if tmp:SCAccountID = 0 then exit.

    loop i = 1 to records(glo:Q_ModelNumber)
        get(glo:Q_ModelNumber, i)
        if not error()
            Access:MODELNUM.ClearKey(mod:Model_Number_Key)
            mod:Model_Number = glo:Q_ModelNumber.glo:Model_Number_Pointer
            if Access:MODELNUM.Fetch(mod:Model_Number_Key) = Level:Benign
                Access:SIDMODSC.ClearKey(msc:ManModelTypeKey)
                msc:Manufacturer = mod:Manufacturer
                msc:ModelNo = mod:Model_Number
                msc:BookingType = bookingType
                if Access:SIDMODSC.Fetch(msc:ManModelTypeKey) = Level:Benign
                    if msc:SCAccountID <> tmp:SCAccountID
                        Access:SIDSRVCN_ALIAS.ClearKey(srv_ali:SCAccountIDKey)
                        srv_ali:SCAccountID = msc:SCAccountID
                        if Access:SIDSRVCN_ALIAS.Fetch(srv_ali:SCAccountIDKey) = Level:Benign
                            tmp:Exceptions = clip(tmp:Exceptions) & 'Model: ' & clip(mod:Manufacturer) & ' ' & clip(mod:Model_Number) & '   Attached To: ' & clip(srv_ali:ServiceCentreName) & '<13,10>'
                        end
                    end
                end
            end
        end
    end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AttachModelsToSC',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('result',result,'AttachModelsToSC',1)
    SolaceViewVars('tmp:ServiceCentreName',tmp:ServiceCentreName,'AttachModelsToSC',1)
    SolaceViewVars('tmp:Exceptions',tmp:Exceptions,'AttachModelsToSC',1)
    SolaceViewVars('tmp:SCAccountID',tmp:SCAccountID,'AttachModelsToSC',1)
    SolaceViewVars('i',i,'AttachModelsToSC',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ServiceCentreName:Prompt;  SolaceCtrlName = '?tmp:ServiceCentreName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ServiceCentreName;  SolaceCtrlName = '?tmp:ServiceCentreName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Exceptions:Prompt;  SolaceCtrlName = '?tmp:Exceptions:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Exceptions;  SolaceCtrlName = '?tmp:Exceptions';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AttachModelsToSC')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'AttachModelsToSC')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:ServiceCentreName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MODELNUM.Open
  Relate:SIDMODSC.Open
  Relate:SIDMODTT.Open
  Relate:SIDSRVCN_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ! After Opening Window
  
  result = false
  
  if scAccountID > 0
      Access:SIDSRVCN_ALIAS.ClearKey(srv_ali:SCAccountIDKey)
      srv_ali:SCAccountID = scAccountID
      if Access:SIDSRVCN_ALIAS.Fetch(srv_ali:SCAccountIDKey) = Level:Benign
          tmp:SCAccountID = srv_ali:SCAccountID
          tmp:ServiceCentreName = srv_ali:ServiceCentreName
      end
  end
  
  do Check:Exceptions
  
  
  Do RecolourWindow
  ! support for CPCS
  FDCB4.Init(tmp:ServiceCentreName,?tmp:ServiceCentreName,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:SIDSRVCN_ALIAS,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder(srv_ali:ServiceCentreNameKey)
  FDCB4.AddField(srv_ali:ServiceCentreName,FDCB4.Q.srv_ali:ServiceCentreName)
  FDCB4.AddUpdateField(srv_ali:ServiceCentreName,tmp:ServiceCentreName)
  FDCB4.AddUpdateField(srv_ali:SCAccountID,tmp:SCAccountID)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MODELNUM.Close
    Relate:SIDMODSC.Close
    Relate:SIDMODTT.Close
    Relate:SIDSRVCN_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'AttachModelsToSC',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      ! OK Button
      
      if tmp:Exceptions <> ''
          case MessageEx('There are models allocated to different Service Centres. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              of 1 ! &Yes Button
                        
              of 2 ! &No Button
                  cycle
          end !Case MessageEx
      end
      
      loop i = 1 to records(glo:Q_ModelNumber)
          get(glo:Q_ModelNumber, i)
          if not error()
              Access:MODELNUM.ClearKey(mod:Model_Number_Key)
              mod:Model_Number = glo:Q_ModelNumber.glo:Model_Number_Pointer
              if Access:MODELNUM.Fetch(mod:Model_Number_Key) = Level:Benign
      
                  Access:SIDMODSC.ClearKey(msc:ManModelTypeKey)
                  msc:Manufacturer = mod:Manufacturer
                  msc:ModelNo = mod:Model_Number
                  msc:BookingType = bookingType
                  if Access:SIDMODSC.Fetch(msc:ManModelTypeKey) = Level:Benign
                      msc:SCAccountID = tmp:SCAccountID
                      Access:SIDMODSC.TryUpdate()
                  else
                      if Access:SIDMODSC.PrimeRecord() = Level:Benign
                          msc:Manufacturer = mod:Manufacturer
                          msc:ModelNo = mod:Model_Number
                          msc:BookingType = bookingType
                          msc:SCAccountID = tmp:SCAccountID
                          if Access:SIDMODSC.TryUpdate() <> Level:Benign
                              Access:SIDMODSC.CancelAutoInc()
                          end
                      end
                  end
      
                  ! Commented out on 1st August 2012 - GK
                  ! This was blanking all the turnaround times for models and causing havoc.
                  ! Although it makes sense to do this if the model is moved to a different service
                  ! centre. UTL setup multiple internal service centres which are all really the same thing
                  ! so they don't want these fields cleared.
      
      !            Access:SIDMODTT.ClearKey(smt:ModelNoKey)
      !            smt:ModelNo = mod:Model_Number
      !            if Access:SIDMODTT.Fetch(smt:ModelNoKey) = Level:Benign
      !                smt:TurnaroundException = false
      !                smt:TurnaroundDaysRetail = 0
      !                smt:TurnaroundDaysExch = 0
      !                smt:ActiveTextRetail = false
      !                smt:TextRetail = ''
      !                smt:ActiveTextExch = false
      !                smt:TextExch = ''
      !                Access:SIDMODTT.TryUpdate()
      !            end
      
              end
          end
      end
      
      result = true
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:ServiceCentreName
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ServiceCentreName, Accepted)
      do Check:Exceptions
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ServiceCentreName, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'AttachModelsToSC')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

