

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02058.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSIDModelExceptionText PROCEDURE (bookingType, modelNo, active, guidanceText) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Text             STRING(250)
tmp:Active           BYTE
window               WINDOW('Model Exception Text'),AT(,,232,164),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,130),USE(?Sheet1),SPREAD
                         TAB('Exception Text'),USE(?Tab1)
                           PROMPT('The following text will be presented to SID users when booking retail repair job' &|
   's with the associated exception conditions attached to this model.'),AT(8,18,216,26),USE(?PromptTxt),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(8,46,216,68),USE(tmp:Text),FONT(,8,,FONT:bold,CHARSET:ANSI)
                           CHECK('Active'),AT(8,120),USE(tmp:Active),TRN,VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,138,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(112,142,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(168,142,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?PromptTxt{prop:FontColor} = -1
    ?PromptTxt{prop:Color} = 15066597
    If ?tmp:Text{prop:ReadOnly} = True
        ?tmp:Text{prop:FontColor} = 65793
        ?tmp:Text{prop:Color} = 15066597
    Elsif ?tmp:Text{prop:Req} = True
        ?tmp:Text{prop:FontColor} = 65793
        ?tmp:Text{prop:Color} = 8454143
    Else ! If ?tmp:Text{prop:Req} = True
        ?tmp:Text{prop:FontColor} = 65793
        ?tmp:Text{prop:Color} = 16777215
    End ! If ?tmp:Text{prop:Req} = True
    ?tmp:Text{prop:Trn} = 0
    ?tmp:Text{prop:FontStyle} = font:Bold
    ?tmp:Active{prop:Font,3} = -1
    ?tmp:Active{prop:Color} = 15066597
    ?tmp:Active{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSIDModelExceptionText',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:Text',tmp:Text,'UpdateSIDModelExceptionText',1)
    SolaceViewVars('tmp:Active',tmp:Active,'UpdateSIDModelExceptionText',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PromptTxt;  SolaceCtrlName = '?PromptTxt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Text;  SolaceCtrlName = '?tmp:Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Active;  SolaceCtrlName = '?tmp:Active';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSIDModelExceptionText')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSIDModelExceptionText')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PromptTxt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  ! After Opening Window
  
  if bookingType = 'R'
      0{Prop:Text} = 'Repair Model Exception Text - ' & modelNo
      ?PromptTxt{Prop:Text} = 'The following text will be presented to SID users when booking retail repair jobs with the associated exception conditions attached to this model.'
  else
      0{Prop:Text} = 'Exchange Model Exception Text - ' & modelNo
      ?PromptTxt{Prop:Text} = 'The following text will be presented to SID users when booking exchange jobs with the associated exception conditions attached to this model.'
  end
  
  tmp:Active = active
  tmp:Text = guidanceText
  
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSIDModelExceptionText',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      ! OK Button
      
      active = tmp:Active
      guidanceText = tmp:Text
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSIDModelExceptionText')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

BrowseRetailRepairChargeTiers PROCEDURE               !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Exceptions       STRING(5000)
BRW1::View:Browse    VIEW(SIDRRCT)
                       PROJECT(rrct:Tier)
                       PROJECT(rrct:ChargeValue)
                       PROJECT(rrct:SkuCode)
                       PROJECT(rrct:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
rrct:Tier              LIKE(rrct:Tier)                !List box control field - type derived from field
rrct:ChargeValue       LIKE(rrct:ChargeValue)         !List box control field - type derived from field
rrct:SkuCode           LIKE(rrct:SkuCode)             !List box control field - type derived from field
rrct:RecordNumber      LIKE(rrct:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Retail Repair Charge Tiers'),AT(,,248,188),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Stock_Type'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,36,148,144),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('20R(2)|M~Tier~L@n2@60R(2)|M~Charge Value (<163>)~L@n8.2@120L(2)|M~SKU Code~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(168,20,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       BUTTON('&Insert'),AT(168,88,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(168,112,76,20),USE(?Change:3),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(168,136,76,20),USE(?Delete:3),LEFT,ICON('delete.ico')
                       SHEET,AT(4,4,156,180),USE(?CurrentTab),SPREAD
                         TAB('By Tier'),USE(?Tab:3)
                         END
                       END
                       BUTTON('Close'),AT(168,164,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:3{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BrowseRetailRepairChargeTiers',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'BrowseRetailRepairChargeTiers',1)
    SolaceViewVars('FilesOpened',FilesOpened,'BrowseRetailRepairChargeTiers',1)
    SolaceViewVars('tmp:Exceptions',tmp:Exceptions,'BrowseRetailRepairChargeTiers',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseRetailRepairChargeTiers')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BrowseRetailRepairChargeTiers')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:SIDRRCT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,rrct:TierKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,rrct:Tier,1,BRW1)
  BRW1.AddField(rrct:Tier,BRW1.Q.rrct:Tier)
  BRW1.AddField(rrct:ChargeValue,BRW1.Q.rrct:ChargeValue)
  BRW1.AddField(rrct:SkuCode,BRW1.Q.rrct:SkuCode)
  BRW1.AddField(rrct:RecordNumber,BRW1.Q.rrct:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MODELNUM.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BrowseRetailRepairChargeTiers',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  if Request = DeleteRecord
      ! Check if any models are attached to this retail repair charge tier.
      BRW1.UpdateBuffer()
  
      tmp:Exceptions = ''
  
      Access:MODELNUM.ClearKey(mod:RetailRepairTierKey)
      mod:RetailRepairTier = rrct:Tier
      set(mod:RetailRepairTierKey, mod:RetailRepairTierKey)
      loop
          if Access:MODELNUM.Next() <> Level:Benign then break.
          if mod:RetailRepairTier <> rrct:Tier then break.
          tmp:Exceptions = clip(tmp:Exceptions) & 'Model: ' & clip(mod:Manufacturer) & ' ' & clip(mod:Model_Number) & '<13,10>'
          if len(clip(tmp:Exceptions)) > 1000 then break.
      end
  
      ! If model attached display message, prevent user from deleting the record
      if len(clip(tmp:Exceptions)) > 0
          if UserNotification('Unable To Delete Tier', 'You are unable to delete this tier because the following models are still associated to it. You must reallocate these models to a new tier before you can delete this entry.', tmp:Exceptions)
          end
          return RequestCancelled
      end
  
  end
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateRetailRepairChargeTier
    ReturnValue = GlobalResponse
  END
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BrowseRetailRepairChargeTiers')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateRetailRepairChargeTier PROCEDURE                !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::rrct:Record LIKE(rrct:RECORD),STATIC
QuickWindow          WINDOW('Update the SIDRRCT File'),AT(,,220,115),FONT('Tahoma',8,,),CENTER,IMM,HLP('UpdateSTOCKTYP'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,80),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Tier'),AT(8,20),USE(?rrct:Tier:Prompt),TRN
                           ENTRY(@n2),AT(70,20,50,10),USE(rrct:Tier),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Charge Value (<163>)'),AT(8,36),USE(?rrct:ChargeValue:Prompt),TRN
                           ENTRY(@n8.2),AT(70,36,100,10),USE(rrct:ChargeValue),DECIMAL(12),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('SKU Code'),AT(8,52),USE(?rrct:SkuCode:Prompt),TRN
                           ENTRY(@s30),AT(70,52,100,10),USE(rrct:SkuCode),REQ
                         END
                       END
                       BUTTON('&OK'),AT(100,92,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,92,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,88,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?rrct:Tier:Prompt{prop:FontColor} = -1
    ?rrct:Tier:Prompt{prop:Color} = 15066597
    If ?rrct:Tier{prop:ReadOnly} = True
        ?rrct:Tier{prop:FontColor} = 65793
        ?rrct:Tier{prop:Color} = 15066597
    Elsif ?rrct:Tier{prop:Req} = True
        ?rrct:Tier{prop:FontColor} = 65793
        ?rrct:Tier{prop:Color} = 8454143
    Else ! If ?rrct:Tier{prop:Req} = True
        ?rrct:Tier{prop:FontColor} = 65793
        ?rrct:Tier{prop:Color} = 16777215
    End ! If ?rrct:Tier{prop:Req} = True
    ?rrct:Tier{prop:Trn} = 0
    ?rrct:Tier{prop:FontStyle} = font:Bold
    ?rrct:ChargeValue:Prompt{prop:FontColor} = -1
    ?rrct:ChargeValue:Prompt{prop:Color} = 15066597
    If ?rrct:ChargeValue{prop:ReadOnly} = True
        ?rrct:ChargeValue{prop:FontColor} = 65793
        ?rrct:ChargeValue{prop:Color} = 15066597
    Elsif ?rrct:ChargeValue{prop:Req} = True
        ?rrct:ChargeValue{prop:FontColor} = 65793
        ?rrct:ChargeValue{prop:Color} = 8454143
    Else ! If ?rrct:ChargeValue{prop:Req} = True
        ?rrct:ChargeValue{prop:FontColor} = 65793
        ?rrct:ChargeValue{prop:Color} = 16777215
    End ! If ?rrct:ChargeValue{prop:Req} = True
    ?rrct:ChargeValue{prop:Trn} = 0
    ?rrct:ChargeValue{prop:FontStyle} = font:Bold
    ?rrct:SkuCode:Prompt{prop:FontColor} = -1
    ?rrct:SkuCode:Prompt{prop:Color} = 15066597
    If ?rrct:SkuCode{prop:ReadOnly} = True
        ?rrct:SkuCode{prop:FontColor} = 65793
        ?rrct:SkuCode{prop:Color} = 15066597
    Elsif ?rrct:SkuCode{prop:Req} = True
        ?rrct:SkuCode{prop:FontColor} = 65793
        ?rrct:SkuCode{prop:Color} = 8454143
    Else ! If ?rrct:SkuCode{prop:Req} = True
        ?rrct:SkuCode{prop:FontColor} = 65793
        ?rrct:SkuCode{prop:Color} = 16777215
    End ! If ?rrct:SkuCode{prop:Req} = True
    ?rrct:SkuCode{prop:Trn} = 0
    ?rrct:SkuCode{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateRetailRepairChargeTier',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateRetailRepairChargeTier',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateRetailRepairChargeTier',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateRetailRepairChargeTier',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rrct:Tier:Prompt;  SolaceCtrlName = '?rrct:Tier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rrct:Tier;  SolaceCtrlName = '?rrct:Tier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rrct:ChargeValue:Prompt;  SolaceCtrlName = '?rrct:ChargeValue:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rrct:ChargeValue;  SolaceCtrlName = '?rrct:ChargeValue';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rrct:SkuCode:Prompt;  SolaceCtrlName = '?rrct:SkuCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rrct:SkuCode;  SolaceCtrlName = '?rrct:SkuCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Retail Repair Charge Tier'
  OF ChangeRecord
    ActionMessage = 'Changing A Retail Repair Charge Tier'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRetailRepairChargeTier')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateRetailRepairChargeTier')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?rrct:Tier:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(rrct:Record,History::rrct:Record)
  SELF.AddHistoryField(?rrct:Tier,2)
  SELF.AddHistoryField(?rrct:ChargeValue,3)
  SELF.AddHistoryField(?rrct:SkuCode,4)
  SELF.AddUpdateFile(Access:SIDRRCT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:SIDRRCT.Open
  Relate:SIDRRCT_ALIAS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SIDRRCT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SIDRRCT.Close
    Relate:SIDRRCT_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateRetailRepairChargeTier',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      ! OK button validation
      Access:SIDRRCT_ALIAS.ClearKey(rrct_ali:TierKey)
      rrct_ali:Tier = rrct:Tier
      if Access:SIDRRCT_ALIAS.Fetch(rrct_ali:TierKey) = Level:Benign
          if rrct:RecordNumber <> rrct_ali:RecordNumber
              case messageex('This Tier already exists on the system.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  of 1 ! &ok button
              end!case messageex
              select(?rrct:Tier)
              cycle
          end
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateRetailRepairChargeTier')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UserNotification PROCEDURE (titleTxt, promptText, notificationTxt) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
result               BYTE
tmp:Exceptions       STRING(5000)
window               WINDOW('Title'),AT(,,338,208),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,330,172),USE(?Sheet1),SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('Text'),AT(8,20,320,22),USE(?Prompt:Text),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(8,48,320,124),USE(tmp:Exceptions),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                         END
                       END
                       PANEL,AT(4,180,330,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(272,184,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(result)

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt:Text{prop:FontColor} = -1
    ?Prompt:Text{prop:Color} = 15066597
    If ?tmp:Exceptions{prop:ReadOnly} = True
        ?tmp:Exceptions{prop:FontColor} = 65793
        ?tmp:Exceptions{prop:Color} = 15066597
    Elsif ?tmp:Exceptions{prop:Req} = True
        ?tmp:Exceptions{prop:FontColor} = 65793
        ?tmp:Exceptions{prop:Color} = 8454143
    Else ! If ?tmp:Exceptions{prop:Req} = True
        ?tmp:Exceptions{prop:FontColor} = 65793
        ?tmp:Exceptions{prop:Color} = 16777215
    End ! If ?tmp:Exceptions{prop:Req} = True
    ?tmp:Exceptions{prop:Trn} = 0
    ?tmp:Exceptions{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UserNotification',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('result',result,'UserNotification',1)
    SolaceViewVars('tmp:Exceptions',tmp:Exceptions,'UserNotification',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt:Text;  SolaceCtrlName = '?Prompt:Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Exceptions;  SolaceCtrlName = '?tmp:Exceptions';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UserNotification')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UserNotification')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt:Text
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  ! After Opening Window
  
  result = false
  
  0{Prop:Text} = titleTxt
  ?Prompt:Text{Prop:Text} = promptText
  tmp:Exceptions = notificationTxt
  
  
  
  
  
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UserNotification',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      ! OK Button
      
      result = true
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UserNotification')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

