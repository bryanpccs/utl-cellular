

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02012.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSubRecipients PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::sue:Record  LIKE(sue:RECORD),STATIC
QuickWindow          WINDOW('Update Sub Account Recipient Type'),AT(,,227,103),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('UpdateTradeRecipients'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,220,68),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Recipient Type'),AT(8,20),USE(?tre:RecipientType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(sue:RecipientType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Recipient Type'),TIP('Recipient Type'),ALRT(DownKey),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),REQ,UPR
                           BUTTON,AT(212,20,10,10),USE(?LookupRecipientTypes),SKIP,ICON('List3.ico')
                           PROMPT('Contact Name'),AT(8,36),USE(?tre:ContactName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(84,36,124,10),USE(sue:ContactName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Contact Name'),TIP('Contact Name'),REQ,UPR
                           PROMPT('Email Address'),AT(8,52),USE(?tre:EmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,52,124,10),USE(sue:EmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address'),REQ
                         END
                       END
                       PANEL,AT(4,76,220,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(108,80,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(164,80,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
look:sue:RecipientType                Like(sue:RecipientType)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tre:RecipientType:Prompt{prop:FontColor} = -1
    ?tre:RecipientType:Prompt{prop:Color} = 15066597
    If ?sue:RecipientType{prop:ReadOnly} = True
        ?sue:RecipientType{prop:FontColor} = 65793
        ?sue:RecipientType{prop:Color} = 15066597
    Elsif ?sue:RecipientType{prop:Req} = True
        ?sue:RecipientType{prop:FontColor} = 65793
        ?sue:RecipientType{prop:Color} = 8454143
    Else ! If ?sue:RecipientType{prop:Req} = True
        ?sue:RecipientType{prop:FontColor} = 65793
        ?sue:RecipientType{prop:Color} = 16777215
    End ! If ?sue:RecipientType{prop:Req} = True
    ?sue:RecipientType{prop:Trn} = 0
    ?sue:RecipientType{prop:FontStyle} = font:Bold
    ?tre:ContactName:Prompt{prop:FontColor} = -1
    ?tre:ContactName:Prompt{prop:Color} = 15066597
    If ?sue:ContactName{prop:ReadOnly} = True
        ?sue:ContactName{prop:FontColor} = 65793
        ?sue:ContactName{prop:Color} = 15066597
    Elsif ?sue:ContactName{prop:Req} = True
        ?sue:ContactName{prop:FontColor} = 65793
        ?sue:ContactName{prop:Color} = 8454143
    Else ! If ?sue:ContactName{prop:Req} = True
        ?sue:ContactName{prop:FontColor} = 65793
        ?sue:ContactName{prop:Color} = 16777215
    End ! If ?sue:ContactName{prop:Req} = True
    ?sue:ContactName{prop:Trn} = 0
    ?sue:ContactName{prop:FontStyle} = font:Bold
    ?tre:EmailAddress:Prompt{prop:FontColor} = -1
    ?tre:EmailAddress:Prompt{prop:Color} = 15066597
    If ?sue:EmailAddress{prop:ReadOnly} = True
        ?sue:EmailAddress{prop:FontColor} = 65793
        ?sue:EmailAddress{prop:Color} = 15066597
    Elsif ?sue:EmailAddress{prop:Req} = True
        ?sue:EmailAddress{prop:FontColor} = 65793
        ?sue:EmailAddress{prop:Color} = 8454143
    Else ! If ?sue:EmailAddress{prop:Req} = True
        ?sue:EmailAddress{prop:FontColor} = 65793
        ?sue:EmailAddress{prop:Color} = 16777215
    End ! If ?sue:EmailAddress{prop:Req} = True
    ?sue:EmailAddress{prop:Trn} = 0
    ?sue:EmailAddress{prop:FontStyle} = font:Bold
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSubRecipients',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateSubRecipients',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateSubRecipients',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tre:RecipientType:Prompt;  SolaceCtrlName = '?tre:RecipientType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sue:RecipientType;  SolaceCtrlName = '?sue:RecipientType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupRecipientTypes;  SolaceCtrlName = '?LookupRecipientTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tre:ContactName:Prompt;  SolaceCtrlName = '?tre:ContactName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sue:ContactName;  SolaceCtrlName = '?sue:ContactName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tre:EmailAddress:Prompt;  SolaceCtrlName = '?tre:EmailAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sue:EmailAddress;  SolaceCtrlName = '?sue:EmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Sub Account Recipient'
  OF ChangeRecord
    ActionMessage = 'Changing A Sub Account Recipient'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSubRecipients')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSubRecipients')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tre:RecipientType:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sue:Record,History::sue:Record)
  SELF.AddHistoryField(?sue:RecipientType,3)
  SELF.AddHistoryField(?sue:ContactName,4)
  SELF.AddHistoryField(?sue:EmailAddress,5)
  SELF.AddUpdateFile(Access:SUBEMAIL)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RECIPTYP.Open
  Relate:SUBEMAIL.Open
  Access:TRAEMAIL.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SUBEMAIL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  IF ?sue:RecipientType{Prop:Tip} AND ~?LookupRecipientTypes{Prop:Tip}
     ?LookupRecipientTypes{Prop:Tip} = 'Select ' & ?sue:RecipientType{Prop:Tip}
  END
  IF ?sue:RecipientType{Prop:Msg} AND ~?LookupRecipientTypes{Prop:Msg}
     ?LookupRecipientTypes{Prop:Msg} = 'Select ' & ?sue:RecipientType{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RECIPTYP.Close
    Relate:SUBEMAIL.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSubRecipients',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickRecipientTypes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sue:RecipientType
      IF sue:RecipientType OR ?sue:RecipientType{Prop:Req}
        rec:RecipientType = sue:RecipientType
        !Save Lookup Field Incase Of error
        look:sue:RecipientType        = sue:RecipientType
        IF Access:RECIPTYP.TryFetch(rec:RecipientTypeKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            sue:RecipientType = rec:RecipientType
          ELSE
            !Restore Lookup On Error
            sue:RecipientType = look:sue:RecipientType
            SELECT(?sue:RecipientType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupRecipientTypes
      ThisWindow.Update
      rec:RecipientType = sue:RecipientType
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          sue:RecipientType = rec:RecipientType
          Select(?+1)
      ELSE
          Select(?sue:RecipientType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sue:RecipientType)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSubRecipients')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

