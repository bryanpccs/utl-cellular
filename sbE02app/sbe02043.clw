

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02043.INC'),ONCE        !Local module procedure declarations
                     END


UpdateAccountRegion PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
save_accreg_id       USHORT,AUTO
ActionMessage        CSTRING(40)
tmp:AccountNumber    STRING(15)
tmp:AccRegID         LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::acg:Record  LIKE(acg:RECORD),STATIC
QuickWindow          WINDOW('Update the Account Regions'),AT(,,232,84),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('UpdateAccountRegion'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,224,50),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Account No'),AT(8,20),USE(?ACR:AccountNo:Prompt),TRN
                           ENTRY(@s15),AT(84,20,124,10),USE(acg:AccountNo),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(212,20,12,10),USE(?CallLookup:2),FLAT,ICON('list3.ico')
                           PROMPT('Region Name'),AT(8,36),USE(?ACR:RegionName:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(acg:RegionName),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(212,36,12,10),USE(?CallLookup),FLAT,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,58,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(112,62,56,16),USE(?OK),LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('Cancel'),AT(168,62,56,16),USE(?Cancel),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
look:acg:AccountNo                Like(acg:AccountNo)
look:acg:RegionName                Like(acg:RegionName)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?ACR:AccountNo:Prompt{prop:FontColor} = -1
    ?ACR:AccountNo:Prompt{prop:Color} = 15066597
    If ?acg:AccountNo{prop:ReadOnly} = True
        ?acg:AccountNo{prop:FontColor} = 65793
        ?acg:AccountNo{prop:Color} = 15066597
    Elsif ?acg:AccountNo{prop:Req} = True
        ?acg:AccountNo{prop:FontColor} = 65793
        ?acg:AccountNo{prop:Color} = 8454143
    Else ! If ?acg:AccountNo{prop:Req} = True
        ?acg:AccountNo{prop:FontColor} = 65793
        ?acg:AccountNo{prop:Color} = 16777215
    End ! If ?acg:AccountNo{prop:Req} = True
    ?acg:AccountNo{prop:Trn} = 0
    ?acg:AccountNo{prop:FontStyle} = font:Bold
    ?ACR:RegionName:Prompt{prop:FontColor} = -1
    ?ACR:RegionName:Prompt{prop:Color} = 15066597
    If ?acg:RegionName{prop:ReadOnly} = True
        ?acg:RegionName{prop:FontColor} = 65793
        ?acg:RegionName{prop:Color} = 15066597
    Elsif ?acg:RegionName{prop:Req} = True
        ?acg:RegionName{prop:FontColor} = 65793
        ?acg:RegionName{prop:Color} = 8454143
    Else ! If ?acg:RegionName{prop:Req} = True
        ?acg:RegionName{prop:FontColor} = 65793
        ?acg:RegionName{prop:Color} = 16777215
    End ! If ?acg:RegionName{prop:Req} = True
    ?acg:RegionName{prop:Trn} = 0
    ?acg:RegionName{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateAccountRegion',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateAccountRegion',1)
    SolaceViewVars('save_accreg_id',save_accreg_id,'UpdateAccountRegion',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateAccountRegion',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'UpdateAccountRegion',1)
    SolaceViewVars('tmp:AccRegID',tmp:AccRegID,'UpdateAccountRegion',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ACR:AccountNo:Prompt;  SolaceCtrlName = '?ACR:AccountNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?acg:AccountNo;  SolaceCtrlName = '?acg:AccountNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup:2;  SolaceCtrlName = '?CallLookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ACR:RegionName:Prompt;  SolaceCtrlName = '?ACR:RegionName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?acg:RegionName;  SolaceCtrlName = '?acg:RegionName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup;  SolaceCtrlName = '?CallLookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Region'
  OF ChangeRecord
    ActionMessage = 'Changing A Region'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateAccountRegion')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateAccountRegion')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ACR:AccountNo:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(acg:Record,History::acg:Record)
  SELF.AddHistoryField(?acg:AccountNo,2)
  SELF.AddHistoryField(?acg:RegionName,3)
  SELF.AddUpdateFile(Access:ACCREG)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCREG.Open
  Relate:SUBTRACC.Open
  Access:REGIONS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ACCREG
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  IF ?acg:AccountNo{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?acg:AccountNo{Prop:Tip}
  END
  IF ?acg:AccountNo{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?acg:AccountNo{Prop:Msg}
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCREG.Close
    Relate:SUBTRACC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateAccountRegion',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Sub_Accounts
      BrowseRegions
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?acg:AccountNo
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?acg:AccountNo, Accepted)
      IF acg:AccountNo OR ?acg:AccountNo{Prop:Req}
        sub:Account_Number = acg:AccountNo
        !Save Lookup Field Incase Of error
        look:acg:AccountNo        = acg:AccountNo
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            acg:AccountNo = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            acg:AccountNo = look:acg:AccountNo
            SELECT(?acg:AccountNo)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      !Start - Check for a duplicate account. This is just so it shows a pretty error message. - TrkBs: 4576 (DBH: 16-08-2004 48)
      tmp:AccountNumber   = acg:AccountNo
      tmp:AccRegID        = acg:AccRegID
      Error# = 0
      save_ACCREG_id = Access:ACCREG.SaveFile()
      Access:ACCREG.Clearkey(acg:AccountNoKey)
      acg:AccountNo   = tmp:AccountNumber
      If Access:ACCREG.Tryfetch(acg:AccountNoKey) = Level:Benign
          !Found
          If acg:AccRegID <> tmp:AccRegID
              Case MessageEx('A region is already allocated to this account.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Error# = 1
          End !If acg:AccRegID <> tmp:AccRegID
      Else ! If Access:ACCREG.Tryfetch(acg:AccountNoKey) = Level:Benign
          !Error
      End !If Access:ACCREG.Tryfetch(acg:AccountNoKey) = Level:Benign
      Access:ACCREG.RestoreFile(save_ACCREG_id)
      If Error#
          acg:AccountNo = ''
          Display()
      End !Error#
      !End   - Check for a duplicate account. This is just so it shows a pretty error message. - TrkBs: 4576 (DBH: 16-08-2004 48)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?acg:AccountNo, Accepted)
    OF ?CallLookup:2
      ThisWindow.Update
      sub:Account_Number = acg:AccountNo
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          acg:AccountNo = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?acg:AccountNo)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?acg:AccountNo)
    OF ?acg:RegionName
      IF acg:RegionName OR ?acg:RegionName{Prop:Req}
        reg:RegionName = acg:RegionName
        !Save Lookup Field Incase Of error
        look:acg:RegionName        = acg:RegionName
        IF Access:REGIONS.TryFetch(reg:RegionNameKey)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            acg:RegionName = reg:RegionName
          ELSE
            !Restore Lookup On Error
            acg:RegionName = look:acg:RegionName
            SELECT(?acg:RegionName)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      reg:RegionName = acg:RegionName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        acg:RegionName = reg:RegionName
      END
      ThisWindow.Reset(1)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateAccountRegion')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

