

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02054.INC'),ONCE        !Local module procedure declarations
                     END


UpdateUnAllocatedModels PROCEDURE                     !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:TransitDaysToSC  LONG
tmp:TurnaroundDays   LONG
tmp:TransitDaysToStore LONG
window               WINDOW('Unallocated Models Defaults'),AT(,,232,100),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,66),USE(?Sheet1),SPREAD
                         TAB('General Defaults'),USE(?Tab1)
                           PROMPT('Transit Days To SC'),AT(8,20),USE(?tmp:TransitDaysToSC:Prompt),TRN
                           ENTRY(@n3),AT(92,20,60,10),USE(tmp:TransitDaysToSC),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Turnaround Days'),AT(8,36),USE(?tmp:TurnaroundDays:Prompt),TRN
                           ENTRY(@n3),AT(92,36,60,10),USE(tmp:TurnaroundDays),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Transit Days To Store'),AT(8,52),USE(?tmp:TransitDaysToStore:Prompt),TRN
                           ENTRY(@n3),AT(92,52,60,10),USE(tmp:TransitDaysToStore),RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       PANEL,AT(4,74,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(112,78,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(168,78,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:TransitDaysToSC:Prompt{prop:FontColor} = -1
    ?tmp:TransitDaysToSC:Prompt{prop:Color} = 15066597
    If ?tmp:TransitDaysToSC{prop:ReadOnly} = True
        ?tmp:TransitDaysToSC{prop:FontColor} = 65793
        ?tmp:TransitDaysToSC{prop:Color} = 15066597
    Elsif ?tmp:TransitDaysToSC{prop:Req} = True
        ?tmp:TransitDaysToSC{prop:FontColor} = 65793
        ?tmp:TransitDaysToSC{prop:Color} = 8454143
    Else ! If ?tmp:TransitDaysToSC{prop:Req} = True
        ?tmp:TransitDaysToSC{prop:FontColor} = 65793
        ?tmp:TransitDaysToSC{prop:Color} = 16777215
    End ! If ?tmp:TransitDaysToSC{prop:Req} = True
    ?tmp:TransitDaysToSC{prop:Trn} = 0
    ?tmp:TransitDaysToSC{prop:FontStyle} = font:Bold
    ?tmp:TurnaroundDays:Prompt{prop:FontColor} = -1
    ?tmp:TurnaroundDays:Prompt{prop:Color} = 15066597
    If ?tmp:TurnaroundDays{prop:ReadOnly} = True
        ?tmp:TurnaroundDays{prop:FontColor} = 65793
        ?tmp:TurnaroundDays{prop:Color} = 15066597
    Elsif ?tmp:TurnaroundDays{prop:Req} = True
        ?tmp:TurnaroundDays{prop:FontColor} = 65793
        ?tmp:TurnaroundDays{prop:Color} = 8454143
    Else ! If ?tmp:TurnaroundDays{prop:Req} = True
        ?tmp:TurnaroundDays{prop:FontColor} = 65793
        ?tmp:TurnaroundDays{prop:Color} = 16777215
    End ! If ?tmp:TurnaroundDays{prop:Req} = True
    ?tmp:TurnaroundDays{prop:Trn} = 0
    ?tmp:TurnaroundDays{prop:FontStyle} = font:Bold
    ?tmp:TransitDaysToStore:Prompt{prop:FontColor} = -1
    ?tmp:TransitDaysToStore:Prompt{prop:Color} = 15066597
    If ?tmp:TransitDaysToStore{prop:ReadOnly} = True
        ?tmp:TransitDaysToStore{prop:FontColor} = 65793
        ?tmp:TransitDaysToStore{prop:Color} = 15066597
    Elsif ?tmp:TransitDaysToStore{prop:Req} = True
        ?tmp:TransitDaysToStore{prop:FontColor} = 65793
        ?tmp:TransitDaysToStore{prop:Color} = 8454143
    Else ! If ?tmp:TransitDaysToStore{prop:Req} = True
        ?tmp:TransitDaysToStore{prop:FontColor} = 65793
        ?tmp:TransitDaysToStore{prop:Color} = 16777215
    End ! If ?tmp:TransitDaysToStore{prop:Req} = True
    ?tmp:TransitDaysToStore{prop:Trn} = 0
    ?tmp:TransitDaysToStore{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateUnAllocatedModels',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:TransitDaysToSC',tmp:TransitDaysToSC,'UpdateUnAllocatedModels',1)
    SolaceViewVars('tmp:TurnaroundDays',tmp:TurnaroundDays,'UpdateUnAllocatedModels',1)
    SolaceViewVars('tmp:TransitDaysToStore',tmp:TransitDaysToStore,'UpdateUnAllocatedModels',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitDaysToSC:Prompt;  SolaceCtrlName = '?tmp:TransitDaysToSC:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitDaysToSC;  SolaceCtrlName = '?tmp:TransitDaysToSC';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TurnaroundDays:Prompt;  SolaceCtrlName = '?tmp:TurnaroundDays:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TurnaroundDays;  SolaceCtrlName = '?tmp:TurnaroundDays';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitDaysToStore:Prompt;  SolaceCtrlName = '?tmp:TransitDaysToStore:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:TransitDaysToStore;  SolaceCtrlName = '?tmp:TransitDaysToStore';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateUnAllocatedModels')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateUnAllocatedModels')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:TransitDaysToSC:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SIDDEF.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ! After Opening Window
  
  set(SIDDEF, 0)
  if Access:SIDDEF.Next() = Level:Benign
      tmp:TransitDaysToSC = SID:UnallocTransitToSC
      tmp:TurnaroundDays = SID:UnallocTurnaround
      tmp:TransitDaysToStore = SID:UnallocTransitStore
  else
      If Access:SIDDEF.PrimeRecord() = Level:Benign
          If Access:SIDDEF.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:SIDDEF.TryInsert() = Level:Benign
              ! Insert Failed
              Access:SIDDEF.CancelAutoInc()
          End ! If Access:SIDDEF.TryInsert() = Level:Benign
      End !If Access:SIDDEF.PrimeRecord() = Level:Benign
  end
  
  if tmp:TransitDaysToSC = 0 then tmp:TransitDaysToSC = 1.
  if tmp:TurnaroundDays = 0 then tmp:TurnaroundDays = 1.
  if tmp:TransitDaysToStore = 0 then tmp:TransitDaysToStore = 1.
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SIDDEF.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateUnAllocatedModels',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      ! OK Button
      
      if tmp:TransitDaysToSC = 0
          select(?tmp:TransitDaysToSC)
          cycle
      end
      
      if tmp:TurnaroundDays = 0
          select(?tmp:TurnaroundDays)
          cycle
      end
      
      if tmp:TransitDaysToStore = 0
          select(?tmp:TransitDaysToStore)
          cycle
      end
      
      SID:UnallocTransitToSC = tmp:TransitDaysToSC
      SID:UnallocTurnaround = tmp:TurnaroundDays
      SID:UnallocTransitStore = tmp:TransitDaysToStore
      Access:SIDDEF.Update()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateUnAllocatedModels')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

