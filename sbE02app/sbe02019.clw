

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02019.INC'),ONCE        !Local module procedure declarations
                     END


Confirm_Import PROCEDURE (Stock_Type_Local,Location_Local) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Stock_Type           STRING(30)
Location_Temp        STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Stock_Type
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Location_Temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
FDCB5::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
window               WINDOW('Confirm Import'),AT(,,184,84),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,176,48),USE(?Sheet1),SPREAD
                         TAB('Confirm Import'),USE(?Tab1)
                           STRING('Stock Type'),AT(8,20),USE(?String1)
                           COMBO(@s30),AT(52,21,124,10),USE(Stock_Type),IMM,FONT(,,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M~Stock Type~@s30@'),DROP(5),FROM(Queue:FileDropCombo)
                           STRING('Location'),AT(8,36),USE(?String2)
                           COMBO(@s30),AT(52,37,124,10),USE(Location_Temp),IMM,FONT(,,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M~Location~@s30@'),DROP(5),FROM(Queue:FileDropCombo:1)
                         END
                       END
                       PANEL,AT(4,56,176,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(120,60,56,16),USE(?OkButton),FLAT,LEFT,ICON('ok.gif'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    If ?Stock_Type{prop:ReadOnly} = True
        ?Stock_Type{prop:FontColor} = 65793
        ?Stock_Type{prop:Color} = 15066597
    Elsif ?Stock_Type{prop:Req} = True
        ?Stock_Type{prop:FontColor} = 65793
        ?Stock_Type{prop:Color} = 8454143
    Else ! If ?Stock_Type{prop:Req} = True
        ?Stock_Type{prop:FontColor} = 65793
        ?Stock_Type{prop:Color} = 16777215
    End ! If ?Stock_Type{prop:Req} = True
    ?Stock_Type{prop:Trn} = 0
    ?Stock_Type{prop:FontStyle} = font:Bold
    ?String2{prop:FontColor} = -1
    ?String2{prop:Color} = 15066597
    If ?Location_Temp{prop:ReadOnly} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 15066597
    Elsif ?Location_Temp{prop:Req} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 8454143
    Else ! If ?Location_Temp{prop:Req} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 16777215
    End ! If ?Location_Temp{prop:Req} = True
    ?Location_Temp{prop:Trn} = 0
    ?Location_Temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Confirm_Import',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Stock_Type',Stock_Type,'Confirm_Import',1)
    SolaceViewVars('Location_Temp',Location_Temp,'Confirm_Import',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Stock_Type;  SolaceCtrlName = '?Stock_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_Temp;  SolaceCtrlName = '?Location_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Confirm_Import')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Confirm_Import')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  Relate:STOCKTYP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FDCB4.Init(Stock_Type,?Stock_Type,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder()
  FDCB4.AddField(stp:Stock_Type,FDCB4.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB5.Init(Location_Temp,?Location_Temp,Queue:FileDropCombo:1.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:1,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:1
  FDCB5.AddSortOrder()
  FDCB5.AddField(loc:Location,FDCB5.Q.loc:Location)
  FDCB5.AddField(loc:RecordNumber,FDCB5.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:STOCKTYP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Confirm_Import',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      IF Stock_Type = ''
        SELECT(?Stock_Type)
        CYCLE
      END
      
      IF Location_Temp = ''
        SELECT(?Location_Temp)
        CYCLE
      END
      
      Stock_Type_Local = Stock_Type
      Location_Local = Location_Temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Confirm_Import')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

