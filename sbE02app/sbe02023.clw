

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02023.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBE02026.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateTRADEACC PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
pos                  STRING(255)
ok_pressed_temp      BYTE(0)
Account_Number_temp  STRING(15)
Company_Name_temp    STRING(30)
use_sub_account_temp STRING(3)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
discount_labour_temp REAL
discount_parts_temp  REAL
vat_labour_temp      REAL
vat_parts_temp       REAL
Discount_Retail_Temp REAL
vat_retail_temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tra:Labour_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?tra:Parts_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:6 QUEUE                           !Queue declaration for browse/combo box using ?tra:Loan_Stock_Type
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:7 QUEUE                           !Queue declaration for browse/combo box using ?tra:Exchange_Stock_Type
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:8 QUEUE                           !Queue declaration for browse/combo box using ?tra:Turnaround_Time
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:10 QUEUE                          !Queue declaration for browse/combo box using ?tra:Retail_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:11 QUEUE                          !Queue declaration for browse/combo box using ?tra:ChargeType
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:12 QUEUE                          !Queue declaration for browse/combo box using ?tra:WarChargeType
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Branch)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                       PROJECT(sub:Main_Account_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Branch             LIKE(sub:Branch)               !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
sub:Main_Account_Number LIKE(sub:Main_Account_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW23::View:Browse   VIEW(TRAEMAIL)
                       PROJECT(tre:RecipientType)
                       PROJECT(tre:ContactName)
                       PROJECT(tre:RecordNumber)
                       PROJECT(tre:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tre:RecipientType      LIKE(tre:RecipientType)        !List box control field - type derived from field
tre:ContactName        LIKE(tre:ContactName)          !List box control field - type derived from field
tre:RecordNumber       LIKE(tre:RecordNumber)         !Primary key field - type derived from field
tre:RefNumber          LIKE(tre:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB18::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB15::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB24::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
FDCB32::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
FDCB33::View:FileDropCombo VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                     END
FDCB36::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB5::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB16::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
History::tra:Record  LIKE(tra:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string String(255)
QuickWindow          WINDOW('Update Trade Account'),AT(,,643,348),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('PC.ICO'),HLP('UpdateTRADEACC'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,224,312),USE(?Sheet3),SPREAD
                         TAB('Main Invoice Account'),USE(?Tab7)
                           PROMPT('Account Number'),AT(8,20),USE(?TRA:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,20,64,10),USE(tra:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Company Name'),AT(8,36),USE(?TRA:Company_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,36,136,10),USE(tra:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Postcode'),AT(8,52),USE(?TRA:Postcode:Prompt),TRN
                           ENTRY(@s15),AT(84,52,64,10),USE(tra:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Address'),AT(8,64),USE(?TRA:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(84,64,136,10),USE(tra:Address_Line1),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('Clear'),AT(156,52,64,10),USE(?Clear_Address),SKIP,LEFT,ICON(ICON:Cut)
                           ENTRY(@s30),AT(84,76,136,10),USE(tra:Address_Line2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,88,136,10),USE(tra:Address_Line3),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Telephone Number'),AT(8,108),USE(?TRA:Telephone_Number:Prompt),TRN
                           STRING('Telephone Number'),AT(84,100),USE(?telephone_number),TRN,FONT(,7,,)
                           ENTRY(@s15),AT(84,108,64,10),USE(tra:Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           STRING('Fax Number'),AT(156,100),USE(?fax_number),TRN,FONT(,7,,)
                           ENTRY(@s15),AT(156,108,64,10),USE(tra:Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Email Address'),AT(8,156),USE(?tra:EmailAddress:Prompt),TRN,HIDE,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,156,136,10),USE(tra:EmailAddress),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           PROMPT('Contact Name'),AT(8,124),USE(?TRA:Contact_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,124,136,10),USE(tra:Contact_Name),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('V.A.T. Codes'),AT(8,176),USE(?vat_codes),TRN
                           PROMPT('Labour'),AT(84,168),USE(?labour:2),TRN,FONT(,7,,)
                           COMBO(@s2),AT(84,176,40,10),USE(tra:Labour_VAT_Code),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),REQ,FORMAT('14L(2)|M@s2@24L(2)@n6.2@'),DROP(10,64),FROM(Queue:FileDropCombo:1)
                           PROMPT('Parts'),AT(132,168),USE(?parts:2),TRN,FONT(,7,,)
                           COMBO(@s2),AT(132,176,40,10),USE(tra:Parts_VAT_Code),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,FORMAT('13L(2)|M@s2@24L(2)@n6.2@'),DROP(10,64),FROM(Queue:FileDropCombo:5)
                           PROMPT('Retail'),AT(180,168),USE(?retail:2),TRN,FONT(,7,,)
                           COMBO(@s2),AT(180,176,40,10),USE(tra:Retail_VAT_Code),IMM,VSCROLL,FONT(,,,FONT:bold),REQ,FORMAT('15L(2)|M@s2@24L(2)|M@n6.2@'),DROP(10,64),FROM(Queue:FileDropCombo:10)
                           PROMPT('Courier - Incoming'),AT(8,192),USE(?tra:Courier_Incoming:Prompt),TRN
                           ENTRY(@s30),AT(84,192,124,10),USE(tra:Courier_Incoming),FONT('Tahoma',8,,FONT:bold),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,192,10,10),USE(?LookupIncomingCourier),SKIP,ICON('List3.ico')
                           PROMPT('Courier - Outgoing'),AT(8,204),USE(?tra:Courier_Outgoing:Prompt),TRN
                           ENTRY(@s30),AT(84,204,124,10),USE(tra:Courier_Outgoing),FONT('Tahoma',8,,FONT:bold),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,204,10,10),USE(?LookupOutgoingCourier),SKIP,ICON('List3.ico')
                           PROMPT('Default Courier Cost'),AT(8,220),USE(?TRA:Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,220,64,10),USE(tra:Courier_Cost),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Loan Stock Type'),AT(8,236),USE(?tra:loan_stock_type:prompt),TRN
                           COMBO(@s30),AT(84,236,136,10),USE(tra:Loan_Stock_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:6)
                           PROMPT('Exchange Stock Type'),AT(8,248),USE(?tra:exchange_stock_type:prompt),TRN
                           COMBO(@s30),AT(84,248,136,10),USE(tra:Exchange_Stock_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:7)
                           PROMPT('Turnaround Time'),AT(8,264),USE(?turnaround_time),TRN
                           COMBO(@s30),AT(84,264,136,10),USE(tra:Turnaround_Time),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:8)
                           CHECK('Stop Account'),AT(84,280),USE(tra:Stop_Account),TRN,VALUE('YES','NO')
                         END
                       END
                       SHEET,AT(232,4,408,312),USE(?Sheet4),SPREAD
                         TAB('General Defaults'),USE(?Tab8)
                           CHECK('Use Contact Name'),AT(236,24),USE(tra:Use_Contact_Name),TRN,VALUE('YES','NO')
                           CHECK('Use Invoice As Delivery Address'),AT(236,36,136,10),USE(tra:Use_Delivery_Address),TRN,VALUE('YES','NO')
                           CHECK('Use Invoice As Collection Address'),AT(236,48),USE(tra:Use_Collection_Address),TRN,VALUE('YES','NO')
                           CHECK('Record End User Address'),AT(236,60),USE(tra:Use_Customer_Address),TRN,TIP('Do not use the Trade Account''s Address<13,10>as the Job''s ''Customer Address'''),VALUE('YES','NO')
                           CHECK('Invoice End User Address'),AT(236,72),USE(tra:Invoice_Customer_Address),TRN,TIP('Do not use the Trade Account''s Address as the<13,10>Invoice Address. Use the Job''s "C' &|
   'ustomer Address"'),VALUE('YES','NO')
                           GROUP,AT(392,32,160,112),USE(?EmailGroup),HIDE
                             LIST,AT(396,36,152,68),USE(?List:2),IMM,MSG('Browsing Records'),FORMAT('97L(2)|M~Recipient Type~@s30@240L(2)|M~Contact Name~@s60@'),FROM(Queue:Browse:1)
                             BUTTON('&Insert'),AT(396,108,42,12),USE(?Insert:2)
                             BUTTON('&Change'),AT(452,108,42,12),USE(?Change:2)
                             BUTTON('&Delete'),AT(508,108,42,12),USE(?Delete:2)
                             CHECK('Email Recipient List'),AT(396,120),USE(tra:EmailRecipientList),MSG('Email Recipient List'),TIP('Email Recipient List'),VALUE('1','0')
                             CHECK('Email End User'),AT(396,132),USE(tra:EmailEndUser),MSG('Email End User'),TIP('Email End User'),VALUE('1','0')
                           END
                           CHECK('Auto Send Status Emails'),AT(396,24),USE(tra:AutoSendStatusEmails),MSG('Auto Send Status Emails'),TIP('Auto Send Status Emails'),VALUE('1','0')
                           BUTTON('&Insert'),AT(376,292,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(432,292,56,16),USE(?Change),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(488,292,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                           PROMPT('Sub Accounts'),AT(488,180),USE(?Prompt28),TRN,FONT('Tahoma',10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           SHEET,AT(236,164,312,148),USE(?CurrentTab),SPREAD
                             TAB('By Account Number'),USE(?Tab3)
                               ENTRY(@s15),AT(240,180,64,10),USE(sub:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             END
                             TAB('By Branch'),USE(?Tab5)
                               ENTRY(@s30),AT(240,180,100,10),USE(sub:Branch),FONT('Tahoma',8,,FONT:bold),UPR
                             END
                             TAB('By Company Name'),USE(?Tab4)
                               ENTRY(@s30),AT(240,180,136,10),USE(sub:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             END
                           END
                           PANEL,AT(236,144,312,16),USE(?Panel2),FILL(COLOR:Silver)
                           CHECK('Use Sub Accounts'),AT(240,148),USE(tra:Use_Sub_Accounts),TRN,VALUE('YES','NO')
                           CHECK('Invoice Sub Accounts'),AT(428,148),USE(tra:Invoice_Sub_Accounts),TRN,VALUE('YES','NO')
                           LIST,AT(240,196,304,92),USE(?List),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('65L(2)|M~Account Number~@s15@100L(2)|M~Branch~@s30@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                         END
                         TAB('Service Defaults'),USE(?Tab9)
                           CHECK('Force Common Fault'),AT(236,132),USE(tra:ForceCommonFault),TRN,MSG('Force Common Fault'),TIP('Force Common Fault'),VALUE('1','0')
                           CHECK('Force Order Number'),AT(236,144),USE(tra:ForceOrderNumber),TRN,MSG('Force Order Number'),TIP('Force Order Number'),VALUE('1','0')
                           CHECK('Exchange Unit Account'),AT(236,228),USE(tra:ExchangeAcc),TRN,RIGHT,MSG('Account used for repairing exchange units'),TIP('Account used for repairing exchange units'),VALUE('YES','NO')
                           OPTION('Invoice Print Type'),AT(432,112,120,28),USE(tra:InvoiceType),BOXED,TRN,MSG('Invoice Type')
                             RADIO('Manual'),AT(440,124),USE(?Option4:Radio1),VALUE('0')
                             RADIO('Automatic'),AT(488,124),USE(?Option4:Radio1:2),VALUE('1')
                           END
                           PROMPT('Maximum Parts Cost'),AT(332,168),USE(?Prompt34),TRN,FONT(,7,,)
                           CHECK('Check Chargeable Parts'),AT(236,176),USE(tra:CheckChargeablePartsCost),TRN
                           ENTRY(@n14.2),AT(332,176,76,12),USE(tra:MaxChargeablePartsCost),FONT(,,,FONT:bold)
                           CHECK('Refurbishment Charge'),AT(236,32),USE(tra:RefurbCharge),TRN,RIGHT,VALUE('YES','NO')
                           GROUP('Default Charge Types'),AT(236,44,188,40),USE(?Charge_Type_Group),DISABLE,BOXED,TRN
                             COMBO(@s30),AT(284,56,124,10),USE(tra:ChargeType),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:11)
                             PROMPT('Chargeable'),AT(240,56),USE(?Prompt26),TRN
                             PROMPT('Warranty'),AT(240,68),USE(?Prompt26:2),TRN
                             COMBO(@s30),AT(284,68,124,10),USE(tra:WarChargeType),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:12)
                           END
                           CHECK('Zero Value Chargeable Parts'),AT(236,20),USE(tra:ZeroChargeable),TRN,MSG('Don''t show Chargeable Costs'),TIP('Don''t show Chargeable Costs'),VALUE('YES','NO')
                           OPTION('Invoice Type'),AT(432,144,124,64),USE(tra:MultiInvoice),BOXED,TRN,MSG('Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)')
                             RADIO('Single Invoice (No Summary)'),AT(440,156),USE(?TRA:MultiIn:Radio1),TRN,TIP('Will produce one invoice per job.'),VALUE('SIN')
                             RADIO('Single Invoice (Summary)'),AT(440,168),USE(?TRA:MultiIn:Radio2),TRN,TIP('Will produce one invoice per job. <13,10>During Batch Invoicing a summary <13,10>of all in' &|
   'voices created will be produced.'),VALUE('SIM')
                             RADIO('Multiple Invoice'),AT(440,180),USE(?TRA:MultiIn:Radio3),TRN,TIP('Many jobs will appear on the same invoice.'),VALUE('MUL')
                             RADIO('Batch Invoice'),AT(440,192),USE(?TRA:MultiIn:Radio4),TRN,TIP('All jobs in a batch will appear on the same invoice.'),VALUE('BAT')
                           END
                           CHECK('Allow Loan'),AT(236,88),USE(tra:Allow_Loan),TRN,VALUE('YES','NO')
                           CHECK('Use Sub Account Address On Estimate'),AT(236,156),USE(tra:Allow_Cash_Sales),TRN,VALUE('YES','NO')
                           CHECK('Stop Units From Being Sent To Third Paty'),AT(236,212),USE(tra:StopThirdParty),TRN,MSG('Stop Unit For Going To Third Party'),TIP('Stop Unit For Going To Third Party'),VALUE('1','0')
                           CHECK('Allow Exchange'),AT(236,100),USE(tra:Allow_Exchange),TRN,VALUE('YES','NO')
                           CHECK('Make Split Job On Exchange'),AT(432,20),USE(tra:MakeSplitJob),TRN,MSG('ak'),TIP('ak'),VALUE('1','0')
                           CHECK('Show Repair Types With Trade Price Structure'),AT(236,196),USE(tra:ShowRepairTypes),TRN,MSG('Repair Type Display'),TIP('Repair Type Display'),VALUE('1','0')
                           CHECK('Exclude From Bouncer Table'),AT(236,244),USE(tra:ExcludeBouncer),TRN,MSG('Exclude From Bouncer'),TIP('Exclude From Bouncer'),VALUE('1','0')
                           PROMPT('Estimate If Over'),AT(300,108),USE(?TRA:Estimate_If_Over:Prompt),TRN,FONT(,7,,)
                           GROUP('Split Job Details'),AT(432,32,200,76),USE(?SplitJobGroup),BOXED,TRN
                             PROMPT('Job Status'),AT(436,44),USE(?tra:SplitJobStatus:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(512,44,100,10),USE(tra:SplitJobStatus),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Status'),TIP('Job Status'),UPR
                             BUTTON,AT(616,44,10,10),USE(?LookupJobStatus),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ICON('List3.ico')
                             PROMPT('Exchange Status'),AT(436,60),USE(?tra:SplitExchangeStatus:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(512,60,100,10),USE(tra:SplitExchangeStatus),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Exchange Status'),TIP('Exchange Status'),UPR
                             BUTTON,AT(616,60,10,10),USE(?LookupExchangeStatus),SKIP,ICON('List3.ico')
                             PROMPT('Char Charge Type'),AT(436,76,72,15),USE(?tra:SplitCChargeType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(512,76,100,10),USE(tra:SplitCChargeType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Chargeable Charge Type'),TIP('Chargeable Charge Type'),UPR
                             BUTTON,AT(616,76,10,10),USE(?LookupCChargeType),SKIP,ICON('List3.ico')
                             PROMPT('Warranty Charge Type'),AT(436,92),USE(?tra:SplitWChargeType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(512,92,100,10),USE(tra:SplitWChargeType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Warranty Charge Type'),TIP('Warranty Charge Type'),UPR
                             BUTTON,AT(616,92,10,10),USE(?LookupWChargeType),SKIP,ICON('List3.ico')
                           END
                           CHECK('Force Estimate'),AT(236,116),USE(tra:Force_Estimate),TRN,VALUE('YES','NO')
                           ENTRY(@n14.2),AT(300,116,64,10),USE(tra:Estimate_If_Over),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('Retail Sales Defaults'),USE(?Tab6)
                           OPTION,AT(304,16,52,24),USE(tra:Retail_Payment_Type),TRN
                             RADIO('Account'),AT(308,20),USE(?TRA:Retail_Payment_Type:Radio1),TRN,VALUE('ACC')
                             RADIO('Cash'),AT(308,32),USE(?TRA:Retail_Payment_Type:Radio2),TRN,VALUE('CAS')
                           END
                           PROMPT('Payment Method'),AT(236,20),USE(?payment_Method),TRN
                           OPTION,AT(304,40,52,44),USE(tra:Retail_Price_Structure),TRN
                             RADIO('Trade'),AT(308,48),USE(?TRA:Retail_Price_Structure:Radio1),TRN,VALUE('TRA')
                             RADIO('Retail'),AT(308,60),USE(?TRA:Retail_Price_Structure:Radio2),TRN,VALUE('RET')
                             RADIO('Purchase'),AT(308,72),USE(?tra:Retail_Price_Structure:Radio3),VALUE('PUR')
                           END
                           PROMPT('Pricing Structure'),AT(236,48),USE(?pricing_structure),TRN
                           CHECK('Print Retail Picking Note'),AT(236,88),USE(tra:Print_Retail_Picking_Note),TRN,VALUE('YES','NO')
                           CHECK('Price Retail Despatch Notes'),AT(236,104),USE(tra:Price_Retail_Despatch),TRN,VALUE('YES','NO')
                           CHECK('Zero Value Parts'),AT(236,120),USE(tra:RetailZeroParts),MSG('Zero Value Parts'),TIP('Zero Value Parts'),VALUE('1','0')
                           CHECK('Charge In Euros'),AT(236,136),USE(tra:EuroApplies),VALUE('1','0'),MSG('Apply Euro')
                         END
                         TAB('Web Defaults'),USE(?Tab10)
                           PROMPT('General Password'),AT(236,20),USE(?TRA:Password:Prompt),TRN
                           ENTRY(@s20),AT(312,20,124,10),USE(tra:Password),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,PASSWORD
                           PROMPT('Order Password'),AT(236,36),USE(?tra:WebPassword1:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(312,36,124,10),USE(tra:WebPassword1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Web Password1'),TIP('Web Password1'),UPR,PASSWORD
                           PROMPT('Web Notes'),AT(236,52),USE(?TRA:Password:Prompt:2),TRN
                           TEXT,AT(312,52,208,56),USE(tra:WebMemo),VSCROLL,FONT(,,,FONT:bold),MSG('Web Memo')
                           GROUP('Stock Access Levels'),AT(312,112,124,32),USE(?StockAccessLevels),BOXED,HIDE
                             CHECK('E1'),AT(320,124),USE(tra:E1),MSG('E1'),TIP('E1'),VALUE('1','0')
                             CHECK('E2'),AT(364,124),USE(tra:E2),MSG('E2'),TIP('E2'),VALUE('1','0')
                             CHECK('E3'),AT(408,124),USE(tra:E3),MSG('E3'),TIP('E3'),VALUE('1','0')
                           END
                         END
                         TAB('Despatch Defaults'),USE(?Tab11)
                           CHECK('Print Service Despatch Notes'),AT(236,20),USE(tra:Print_Despatch_Notes),TRN,VALUE('YES','NO')
                           CHECK('Skip Despatch'),AT(468,20),USE(tra:IgnoreDespatch),TRN,VALUE('YES','NO'),MSG('Do not use Despatch Table')
                           CHECK('Print Despatch Note On Completion'),AT(244,32),USE(tra:Print_Despatch_Complete),DISABLE,TRN,MSG('Print Despatch Note At Completion'),TIP('Print Despatch Note At Completion'),VALUE('YES','NO')
                           CHECK('Print Despatch Note On Despatch'),AT(244,40),USE(tra:Print_Despatch_Despatch),DISABLE,TRN,MSG('Print Despatch Note At Completion'),TIP('Print Despatch Note At Completion'),VALUE('YES','NO')
                           CHECK('Hide Address On Despatch Note'),AT(244,48),USE(tra:HideDespAdd),DISABLE,MSG('Hide Address On Despatch Note'),TIP('Hide Address On Despatch Note'),VALUE('1','0')
                           GROUP('Despatch Note Type'),AT(236,56,172,64),USE(?Despatch_note_type),DISABLE,BOXED,TRN
                             CHECK('Despatch Note Per Item'),AT(244,68),USE(tra:Despatch_Note_Per_Item),TRN,VALUE('YES','NO')
                             CHECK('Despatch Note Summary'),AT(244,80),USE(tra:Summary_Despatch_Notes,,?TRA:Summary_Despatch_Notes:2),TRN,VALUE('YES','NO')
                             CHECK('Print Summary Note For Individual Despatch'),AT(244,92),USE(tra:IndividualSummary),MSG('Print Individual Summary Report At Despatch'),TIP('Print Individual Summary Report At Despatch'),VALUE('1','0')
                             CHECK('Use Alternate Despatch Note'),AT(244,104),USE(tra:UseAlternateDespatchNote)
                           END
                           CHECK('Price Service Despatch Notes'),AT(240,124),USE(tra:Price_Despatch),DISABLE,TRN,VALUE('YES','NO')
                           CHECK('Print Invoice At Despatch'),AT(240,156),USE(tra:InvoiceAtDespatch),MSG('Print Invoice At Despatch'),TIP('Print Invoice At Despatch'),VALUE('1','0')
                           CHECK('Despatch Paid Jobs Only'),AT(240,168),USE(tra:Despatch_Paid_Jobs),TRN,VALUE('YES','NO')
                           CHECK('Despatch Invoiced Jobs Only'),AT(240,180),USE(tra:Despatch_Invoiced_Jobs),TRN,VALUE('YES','NO')
                           CHECK('Batch Despatch'),AT(240,192),USE(tra:Skip_Despatch),TRN,VALUE('YES','NO'),MSG('Use Batch Despatch')
                           CHECK('Use Alternative Contact Nos On Despatch Note'),AT(240,208),USE(tra:UseDespatchDetails),MSG('Use Alternative Contact Nos On Despatch Note#'),TIP('Use Alternative Contact Nos On Despatch Note#'),VALUE('1','0')
                           GROUP('Alternative Contact Numbers'),AT(240,220,292,60),USE(?AlternativeContactNumbers),DISABLE,BOXED
                             PROMPT('Telephone Number'),AT(248,232),USE(?tra:AltTelephoneNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(324,232,64,10),USE(tra:AltTelephoneNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Telephone Number'),TIP('Telephone Number'),UPR
                             PROMPT('Fax Number'),AT(248,248),USE(?tra:AltFaxNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(324,248,64,10),USE(tra:AltFaxNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fax Number'),TIP('Fax Number'),UPR
                             PROMPT('Email Address'),AT(248,264),USE(?tra:AltEmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s255),AT(324,264,200,10),USE(tra:AltEmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           END
                           CHECK('Use End User For Despatch'),AT(240,284),USE(tra:UseCustDespAdd),TRN,RIGHT,MSG('Use Customer Address As Despatch Address'),TIP('Do not use the Trade Account''s Address as the<13,10>Despatch Address for a Job. Use t' &|
   'he "Customer Address"'),VALUE('YES','NO')
                           CHECK('Use Trade Contact No On Despatch Note'),AT(240,136),USE(tra:UseTradeContactNo),DISABLE,MSG('Use Trade Contact Name'),TIP('Use Trade Contact Name'),VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,320,636,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Standard Charge Exceptions'),AT(8,324,88,16),USE(?Button6),LEFT,ICON('History.gif')
                       BUTTON('&OK'),AT(524,324,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(580,324,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       BUTTON('&Fault Codes'),AT(100,324,56,16),USE(?FaultCodes),LEFT,ICON('FauCode.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW2::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW2::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW23                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW23::Sort0:Locator StepLocatorClass                 !Default Locator
FDCB18               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB15               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

FDCB24               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:6         !Reference to browse queue type
                     END

FDCB32               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:7         !Reference to browse queue type
                     END

FDCB33               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:8         !Reference to browse queue type
                     END

FDCB36               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:10        !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:11        !Reference to browse queue type
                     END

FDCB16               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:12        !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_sub_id   ushort,auto
save_tch_id   ushort,auto
!Save Entry Fields Incase Of Lookup
look:tra:Courier_Incoming                Like(tra:Courier_Incoming)
look:tra:Courier_Outgoing                Like(tra:Courier_Outgoing)
look:tra:SplitJobStatus                Like(tra:SplitJobStatus)
look:tra:SplitExchangeStatus                Like(tra:SplitExchangeStatus)
look:tra:SplitCChargeType                Like(tra:SplitCChargeType)
look:tra:SplitWChargeType                Like(tra:SplitWChargeType)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet3{prop:Color} = 15066597
    ?Tab7{prop:Color} = 15066597
    ?TRA:Account_Number:Prompt{prop:FontColor} = -1
    ?TRA:Account_Number:Prompt{prop:Color} = 15066597
    If ?tra:Account_Number{prop:ReadOnly} = True
        ?tra:Account_Number{prop:FontColor} = 65793
        ?tra:Account_Number{prop:Color} = 15066597
    Elsif ?tra:Account_Number{prop:Req} = True
        ?tra:Account_Number{prop:FontColor} = 65793
        ?tra:Account_Number{prop:Color} = 8454143
    Else ! If ?tra:Account_Number{prop:Req} = True
        ?tra:Account_Number{prop:FontColor} = 65793
        ?tra:Account_Number{prop:Color} = 16777215
    End ! If ?tra:Account_Number{prop:Req} = True
    ?tra:Account_Number{prop:Trn} = 0
    ?tra:Account_Number{prop:FontStyle} = font:Bold
    ?TRA:Company_Name:Prompt{prop:FontColor} = -1
    ?TRA:Company_Name:Prompt{prop:Color} = 15066597
    If ?tra:Company_Name{prop:ReadOnly} = True
        ?tra:Company_Name{prop:FontColor} = 65793
        ?tra:Company_Name{prop:Color} = 15066597
    Elsif ?tra:Company_Name{prop:Req} = True
        ?tra:Company_Name{prop:FontColor} = 65793
        ?tra:Company_Name{prop:Color} = 8454143
    Else ! If ?tra:Company_Name{prop:Req} = True
        ?tra:Company_Name{prop:FontColor} = 65793
        ?tra:Company_Name{prop:Color} = 16777215
    End ! If ?tra:Company_Name{prop:Req} = True
    ?tra:Company_Name{prop:Trn} = 0
    ?tra:Company_Name{prop:FontStyle} = font:Bold
    ?TRA:Postcode:Prompt{prop:FontColor} = -1
    ?TRA:Postcode:Prompt{prop:Color} = 15066597
    If ?tra:Postcode{prop:ReadOnly} = True
        ?tra:Postcode{prop:FontColor} = 65793
        ?tra:Postcode{prop:Color} = 15066597
    Elsif ?tra:Postcode{prop:Req} = True
        ?tra:Postcode{prop:FontColor} = 65793
        ?tra:Postcode{prop:Color} = 8454143
    Else ! If ?tra:Postcode{prop:Req} = True
        ?tra:Postcode{prop:FontColor} = 65793
        ?tra:Postcode{prop:Color} = 16777215
    End ! If ?tra:Postcode{prop:Req} = True
    ?tra:Postcode{prop:Trn} = 0
    ?tra:Postcode{prop:FontStyle} = font:Bold
    ?TRA:Address_Line1:Prompt{prop:FontColor} = -1
    ?TRA:Address_Line1:Prompt{prop:Color} = 15066597
    If ?tra:Address_Line1{prop:ReadOnly} = True
        ?tra:Address_Line1{prop:FontColor} = 65793
        ?tra:Address_Line1{prop:Color} = 15066597
    Elsif ?tra:Address_Line1{prop:Req} = True
        ?tra:Address_Line1{prop:FontColor} = 65793
        ?tra:Address_Line1{prop:Color} = 8454143
    Else ! If ?tra:Address_Line1{prop:Req} = True
        ?tra:Address_Line1{prop:FontColor} = 65793
        ?tra:Address_Line1{prop:Color} = 16777215
    End ! If ?tra:Address_Line1{prop:Req} = True
    ?tra:Address_Line1{prop:Trn} = 0
    ?tra:Address_Line1{prop:FontStyle} = font:Bold
    If ?tra:Address_Line2{prop:ReadOnly} = True
        ?tra:Address_Line2{prop:FontColor} = 65793
        ?tra:Address_Line2{prop:Color} = 15066597
    Elsif ?tra:Address_Line2{prop:Req} = True
        ?tra:Address_Line2{prop:FontColor} = 65793
        ?tra:Address_Line2{prop:Color} = 8454143
    Else ! If ?tra:Address_Line2{prop:Req} = True
        ?tra:Address_Line2{prop:FontColor} = 65793
        ?tra:Address_Line2{prop:Color} = 16777215
    End ! If ?tra:Address_Line2{prop:Req} = True
    ?tra:Address_Line2{prop:Trn} = 0
    ?tra:Address_Line2{prop:FontStyle} = font:Bold
    If ?tra:Address_Line3{prop:ReadOnly} = True
        ?tra:Address_Line3{prop:FontColor} = 65793
        ?tra:Address_Line3{prop:Color} = 15066597
    Elsif ?tra:Address_Line3{prop:Req} = True
        ?tra:Address_Line3{prop:FontColor} = 65793
        ?tra:Address_Line3{prop:Color} = 8454143
    Else ! If ?tra:Address_Line3{prop:Req} = True
        ?tra:Address_Line3{prop:FontColor} = 65793
        ?tra:Address_Line3{prop:Color} = 16777215
    End ! If ?tra:Address_Line3{prop:Req} = True
    ?tra:Address_Line3{prop:Trn} = 0
    ?tra:Address_Line3{prop:FontStyle} = font:Bold
    ?TRA:Telephone_Number:Prompt{prop:FontColor} = -1
    ?TRA:Telephone_Number:Prompt{prop:Color} = 15066597
    ?telephone_number{prop:FontColor} = -1
    ?telephone_number{prop:Color} = 15066597
    If ?tra:Telephone_Number{prop:ReadOnly} = True
        ?tra:Telephone_Number{prop:FontColor} = 65793
        ?tra:Telephone_Number{prop:Color} = 15066597
    Elsif ?tra:Telephone_Number{prop:Req} = True
        ?tra:Telephone_Number{prop:FontColor} = 65793
        ?tra:Telephone_Number{prop:Color} = 8454143
    Else ! If ?tra:Telephone_Number{prop:Req} = True
        ?tra:Telephone_Number{prop:FontColor} = 65793
        ?tra:Telephone_Number{prop:Color} = 16777215
    End ! If ?tra:Telephone_Number{prop:Req} = True
    ?tra:Telephone_Number{prop:Trn} = 0
    ?tra:Telephone_Number{prop:FontStyle} = font:Bold
    ?fax_number{prop:FontColor} = -1
    ?fax_number{prop:Color} = 15066597
    If ?tra:Fax_Number{prop:ReadOnly} = True
        ?tra:Fax_Number{prop:FontColor} = 65793
        ?tra:Fax_Number{prop:Color} = 15066597
    Elsif ?tra:Fax_Number{prop:Req} = True
        ?tra:Fax_Number{prop:FontColor} = 65793
        ?tra:Fax_Number{prop:Color} = 8454143
    Else ! If ?tra:Fax_Number{prop:Req} = True
        ?tra:Fax_Number{prop:FontColor} = 65793
        ?tra:Fax_Number{prop:Color} = 16777215
    End ! If ?tra:Fax_Number{prop:Req} = True
    ?tra:Fax_Number{prop:Trn} = 0
    ?tra:Fax_Number{prop:FontStyle} = font:Bold
    ?tra:EmailAddress:Prompt{prop:FontColor} = -1
    ?tra:EmailAddress:Prompt{prop:Color} = 15066597
    If ?tra:EmailAddress{prop:ReadOnly} = True
        ?tra:EmailAddress{prop:FontColor} = 65793
        ?tra:EmailAddress{prop:Color} = 15066597
    Elsif ?tra:EmailAddress{prop:Req} = True
        ?tra:EmailAddress{prop:FontColor} = 65793
        ?tra:EmailAddress{prop:Color} = 8454143
    Else ! If ?tra:EmailAddress{prop:Req} = True
        ?tra:EmailAddress{prop:FontColor} = 65793
        ?tra:EmailAddress{prop:Color} = 16777215
    End ! If ?tra:EmailAddress{prop:Req} = True
    ?tra:EmailAddress{prop:Trn} = 0
    ?tra:EmailAddress{prop:FontStyle} = font:Bold
    ?TRA:Contact_Name:Prompt{prop:FontColor} = -1
    ?TRA:Contact_Name:Prompt{prop:Color} = 15066597
    If ?tra:Contact_Name{prop:ReadOnly} = True
        ?tra:Contact_Name{prop:FontColor} = 65793
        ?tra:Contact_Name{prop:Color} = 15066597
    Elsif ?tra:Contact_Name{prop:Req} = True
        ?tra:Contact_Name{prop:FontColor} = 65793
        ?tra:Contact_Name{prop:Color} = 8454143
    Else ! If ?tra:Contact_Name{prop:Req} = True
        ?tra:Contact_Name{prop:FontColor} = 65793
        ?tra:Contact_Name{prop:Color} = 16777215
    End ! If ?tra:Contact_Name{prop:Req} = True
    ?tra:Contact_Name{prop:Trn} = 0
    ?tra:Contact_Name{prop:FontStyle} = font:Bold
    ?vat_codes{prop:FontColor} = -1
    ?vat_codes{prop:Color} = 15066597
    ?labour:2{prop:FontColor} = -1
    ?labour:2{prop:Color} = 15066597
    If ?tra:Labour_VAT_Code{prop:ReadOnly} = True
        ?tra:Labour_VAT_Code{prop:FontColor} = 65793
        ?tra:Labour_VAT_Code{prop:Color} = 15066597
    Elsif ?tra:Labour_VAT_Code{prop:Req} = True
        ?tra:Labour_VAT_Code{prop:FontColor} = 65793
        ?tra:Labour_VAT_Code{prop:Color} = 8454143
    Else ! If ?tra:Labour_VAT_Code{prop:Req} = True
        ?tra:Labour_VAT_Code{prop:FontColor} = 65793
        ?tra:Labour_VAT_Code{prop:Color} = 16777215
    End ! If ?tra:Labour_VAT_Code{prop:Req} = True
    ?tra:Labour_VAT_Code{prop:Trn} = 0
    ?tra:Labour_VAT_Code{prop:FontStyle} = font:Bold
    ?parts:2{prop:FontColor} = -1
    ?parts:2{prop:Color} = 15066597
    If ?tra:Parts_VAT_Code{prop:ReadOnly} = True
        ?tra:Parts_VAT_Code{prop:FontColor} = 65793
        ?tra:Parts_VAT_Code{prop:Color} = 15066597
    Elsif ?tra:Parts_VAT_Code{prop:Req} = True
        ?tra:Parts_VAT_Code{prop:FontColor} = 65793
        ?tra:Parts_VAT_Code{prop:Color} = 8454143
    Else ! If ?tra:Parts_VAT_Code{prop:Req} = True
        ?tra:Parts_VAT_Code{prop:FontColor} = 65793
        ?tra:Parts_VAT_Code{prop:Color} = 16777215
    End ! If ?tra:Parts_VAT_Code{prop:Req} = True
    ?tra:Parts_VAT_Code{prop:Trn} = 0
    ?tra:Parts_VAT_Code{prop:FontStyle} = font:Bold
    ?retail:2{prop:FontColor} = -1
    ?retail:2{prop:Color} = 15066597
    If ?tra:Retail_VAT_Code{prop:ReadOnly} = True
        ?tra:Retail_VAT_Code{prop:FontColor} = 65793
        ?tra:Retail_VAT_Code{prop:Color} = 15066597
    Elsif ?tra:Retail_VAT_Code{prop:Req} = True
        ?tra:Retail_VAT_Code{prop:FontColor} = 65793
        ?tra:Retail_VAT_Code{prop:Color} = 8454143
    Else ! If ?tra:Retail_VAT_Code{prop:Req} = True
        ?tra:Retail_VAT_Code{prop:FontColor} = 65793
        ?tra:Retail_VAT_Code{prop:Color} = 16777215
    End ! If ?tra:Retail_VAT_Code{prop:Req} = True
    ?tra:Retail_VAT_Code{prop:Trn} = 0
    ?tra:Retail_VAT_Code{prop:FontStyle} = font:Bold
    ?tra:Courier_Incoming:Prompt{prop:FontColor} = -1
    ?tra:Courier_Incoming:Prompt{prop:Color} = 15066597
    If ?tra:Courier_Incoming{prop:ReadOnly} = True
        ?tra:Courier_Incoming{prop:FontColor} = 65793
        ?tra:Courier_Incoming{prop:Color} = 15066597
    Elsif ?tra:Courier_Incoming{prop:Req} = True
        ?tra:Courier_Incoming{prop:FontColor} = 65793
        ?tra:Courier_Incoming{prop:Color} = 8454143
    Else ! If ?tra:Courier_Incoming{prop:Req} = True
        ?tra:Courier_Incoming{prop:FontColor} = 65793
        ?tra:Courier_Incoming{prop:Color} = 16777215
    End ! If ?tra:Courier_Incoming{prop:Req} = True
    ?tra:Courier_Incoming{prop:Trn} = 0
    ?tra:Courier_Incoming{prop:FontStyle} = font:Bold
    ?tra:Courier_Outgoing:Prompt{prop:FontColor} = -1
    ?tra:Courier_Outgoing:Prompt{prop:Color} = 15066597
    If ?tra:Courier_Outgoing{prop:ReadOnly} = True
        ?tra:Courier_Outgoing{prop:FontColor} = 65793
        ?tra:Courier_Outgoing{prop:Color} = 15066597
    Elsif ?tra:Courier_Outgoing{prop:Req} = True
        ?tra:Courier_Outgoing{prop:FontColor} = 65793
        ?tra:Courier_Outgoing{prop:Color} = 8454143
    Else ! If ?tra:Courier_Outgoing{prop:Req} = True
        ?tra:Courier_Outgoing{prop:FontColor} = 65793
        ?tra:Courier_Outgoing{prop:Color} = 16777215
    End ! If ?tra:Courier_Outgoing{prop:Req} = True
    ?tra:Courier_Outgoing{prop:Trn} = 0
    ?tra:Courier_Outgoing{prop:FontStyle} = font:Bold
    ?TRA:Courier_Cost:Prompt{prop:FontColor} = -1
    ?TRA:Courier_Cost:Prompt{prop:Color} = 15066597
    If ?tra:Courier_Cost{prop:ReadOnly} = True
        ?tra:Courier_Cost{prop:FontColor} = 65793
        ?tra:Courier_Cost{prop:Color} = 15066597
    Elsif ?tra:Courier_Cost{prop:Req} = True
        ?tra:Courier_Cost{prop:FontColor} = 65793
        ?tra:Courier_Cost{prop:Color} = 8454143
    Else ! If ?tra:Courier_Cost{prop:Req} = True
        ?tra:Courier_Cost{prop:FontColor} = 65793
        ?tra:Courier_Cost{prop:Color} = 16777215
    End ! If ?tra:Courier_Cost{prop:Req} = True
    ?tra:Courier_Cost{prop:Trn} = 0
    ?tra:Courier_Cost{prop:FontStyle} = font:Bold
    ?tra:loan_stock_type:prompt{prop:FontColor} = -1
    ?tra:loan_stock_type:prompt{prop:Color} = 15066597
    If ?tra:Loan_Stock_Type{prop:ReadOnly} = True
        ?tra:Loan_Stock_Type{prop:FontColor} = 65793
        ?tra:Loan_Stock_Type{prop:Color} = 15066597
    Elsif ?tra:Loan_Stock_Type{prop:Req} = True
        ?tra:Loan_Stock_Type{prop:FontColor} = 65793
        ?tra:Loan_Stock_Type{prop:Color} = 8454143
    Else ! If ?tra:Loan_Stock_Type{prop:Req} = True
        ?tra:Loan_Stock_Type{prop:FontColor} = 65793
        ?tra:Loan_Stock_Type{prop:Color} = 16777215
    End ! If ?tra:Loan_Stock_Type{prop:Req} = True
    ?tra:Loan_Stock_Type{prop:Trn} = 0
    ?tra:Loan_Stock_Type{prop:FontStyle} = font:Bold
    ?tra:exchange_stock_type:prompt{prop:FontColor} = -1
    ?tra:exchange_stock_type:prompt{prop:Color} = 15066597
    If ?tra:Exchange_Stock_Type{prop:ReadOnly} = True
        ?tra:Exchange_Stock_Type{prop:FontColor} = 65793
        ?tra:Exchange_Stock_Type{prop:Color} = 15066597
    Elsif ?tra:Exchange_Stock_Type{prop:Req} = True
        ?tra:Exchange_Stock_Type{prop:FontColor} = 65793
        ?tra:Exchange_Stock_Type{prop:Color} = 8454143
    Else ! If ?tra:Exchange_Stock_Type{prop:Req} = True
        ?tra:Exchange_Stock_Type{prop:FontColor} = 65793
        ?tra:Exchange_Stock_Type{prop:Color} = 16777215
    End ! If ?tra:Exchange_Stock_Type{prop:Req} = True
    ?tra:Exchange_Stock_Type{prop:Trn} = 0
    ?tra:Exchange_Stock_Type{prop:FontStyle} = font:Bold
    ?turnaround_time{prop:FontColor} = -1
    ?turnaround_time{prop:Color} = 15066597
    If ?tra:Turnaround_Time{prop:ReadOnly} = True
        ?tra:Turnaround_Time{prop:FontColor} = 65793
        ?tra:Turnaround_Time{prop:Color} = 15066597
    Elsif ?tra:Turnaround_Time{prop:Req} = True
        ?tra:Turnaround_Time{prop:FontColor} = 65793
        ?tra:Turnaround_Time{prop:Color} = 8454143
    Else ! If ?tra:Turnaround_Time{prop:Req} = True
        ?tra:Turnaround_Time{prop:FontColor} = 65793
        ?tra:Turnaround_Time{prop:Color} = 16777215
    End ! If ?tra:Turnaround_Time{prop:Req} = True
    ?tra:Turnaround_Time{prop:Trn} = 0
    ?tra:Turnaround_Time{prop:FontStyle} = font:Bold
    ?tra:Stop_Account{prop:Font,3} = -1
    ?tra:Stop_Account{prop:Color} = 15066597
    ?tra:Stop_Account{prop:Trn} = 0
    ?Sheet4{prop:Color} = 15066597
    ?Tab8{prop:Color} = 15066597
    ?tra:Use_Contact_Name{prop:Font,3} = -1
    ?tra:Use_Contact_Name{prop:Color} = 15066597
    ?tra:Use_Contact_Name{prop:Trn} = 0
    ?tra:Use_Delivery_Address{prop:Font,3} = -1
    ?tra:Use_Delivery_Address{prop:Color} = 15066597
    ?tra:Use_Delivery_Address{prop:Trn} = 0
    ?tra:Use_Collection_Address{prop:Font,3} = -1
    ?tra:Use_Collection_Address{prop:Color} = 15066597
    ?tra:Use_Collection_Address{prop:Trn} = 0
    ?tra:Use_Customer_Address{prop:Font,3} = -1
    ?tra:Use_Customer_Address{prop:Color} = 15066597
    ?tra:Use_Customer_Address{prop:Trn} = 0
    ?tra:Invoice_Customer_Address{prop:Font,3} = -1
    ?tra:Invoice_Customer_Address{prop:Color} = 15066597
    ?tra:Invoice_Customer_Address{prop:Trn} = 0
    ?EmailGroup{prop:Font,3} = -1
    ?EmailGroup{prop:Color} = 15066597
    ?EmailGroup{prop:Trn} = 0
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?tra:EmailRecipientList{prop:Font,3} = -1
    ?tra:EmailRecipientList{prop:Color} = 15066597
    ?tra:EmailRecipientList{prop:Trn} = 0
    ?tra:EmailEndUser{prop:Font,3} = -1
    ?tra:EmailEndUser{prop:Color} = 15066597
    ?tra:EmailEndUser{prop:Trn} = 0
    ?tra:AutoSendStatusEmails{prop:Font,3} = -1
    ?tra:AutoSendStatusEmails{prop:Color} = 15066597
    ?tra:AutoSendStatusEmails{prop:Trn} = 0
    ?Prompt28{prop:FontColor} = -1
    ?Prompt28{prop:Color} = 15066597
    ?CurrentTab{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    If ?sub:Account_Number{prop:ReadOnly} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 15066597
    Elsif ?sub:Account_Number{prop:Req} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 8454143
    Else ! If ?sub:Account_Number{prop:Req} = True
        ?sub:Account_Number{prop:FontColor} = 65793
        ?sub:Account_Number{prop:Color} = 16777215
    End ! If ?sub:Account_Number{prop:Req} = True
    ?sub:Account_Number{prop:Trn} = 0
    ?sub:Account_Number{prop:FontStyle} = font:Bold
    ?Tab5{prop:Color} = 15066597
    If ?sub:Branch{prop:ReadOnly} = True
        ?sub:Branch{prop:FontColor} = 65793
        ?sub:Branch{prop:Color} = 15066597
    Elsif ?sub:Branch{prop:Req} = True
        ?sub:Branch{prop:FontColor} = 65793
        ?sub:Branch{prop:Color} = 8454143
    Else ! If ?sub:Branch{prop:Req} = True
        ?sub:Branch{prop:FontColor} = 65793
        ?sub:Branch{prop:Color} = 16777215
    End ! If ?sub:Branch{prop:Req} = True
    ?sub:Branch{prop:Trn} = 0
    ?sub:Branch{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    If ?sub:Company_Name{prop:ReadOnly} = True
        ?sub:Company_Name{prop:FontColor} = 65793
        ?sub:Company_Name{prop:Color} = 15066597
    Elsif ?sub:Company_Name{prop:Req} = True
        ?sub:Company_Name{prop:FontColor} = 65793
        ?sub:Company_Name{prop:Color} = 8454143
    Else ! If ?sub:Company_Name{prop:Req} = True
        ?sub:Company_Name{prop:FontColor} = 65793
        ?sub:Company_Name{prop:Color} = 16777215
    End ! If ?sub:Company_Name{prop:Req} = True
    ?sub:Company_Name{prop:Trn} = 0
    ?sub:Company_Name{prop:FontStyle} = font:Bold
    ?Panel2{prop:Fill} = 15066597

    ?tra:Use_Sub_Accounts{prop:Font,3} = -1
    ?tra:Use_Sub_Accounts{prop:Color} = 15066597
    ?tra:Use_Sub_Accounts{prop:Trn} = 0
    ?tra:Invoice_Sub_Accounts{prop:Font,3} = -1
    ?tra:Invoice_Sub_Accounts{prop:Color} = 15066597
    ?tra:Invoice_Sub_Accounts{prop:Trn} = 0
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Tab9{prop:Color} = 15066597
    ?tra:ForceCommonFault{prop:Font,3} = -1
    ?tra:ForceCommonFault{prop:Color} = 15066597
    ?tra:ForceCommonFault{prop:Trn} = 0
    ?tra:ForceOrderNumber{prop:Font,3} = -1
    ?tra:ForceOrderNumber{prop:Color} = 15066597
    ?tra:ForceOrderNumber{prop:Trn} = 0
    ?tra:ExchangeAcc{prop:Font,3} = -1
    ?tra:ExchangeAcc{prop:Color} = 15066597
    ?tra:ExchangeAcc{prop:Trn} = 0
    ?tra:InvoiceType{prop:Font,3} = -1
    ?tra:InvoiceType{prop:Color} = 15066597
    ?tra:InvoiceType{prop:Trn} = 0
    ?Option4:Radio1{prop:Font,3} = -1
    ?Option4:Radio1{prop:Color} = 15066597
    ?Option4:Radio1{prop:Trn} = 0
    ?Option4:Radio1:2{prop:Font,3} = -1
    ?Option4:Radio1:2{prop:Color} = 15066597
    ?Option4:Radio1:2{prop:Trn} = 0
    ?Prompt34{prop:FontColor} = -1
    ?Prompt34{prop:Color} = 15066597
    ?tra:CheckChargeablePartsCost{prop:Font,3} = -1
    ?tra:CheckChargeablePartsCost{prop:Color} = 15066597
    ?tra:CheckChargeablePartsCost{prop:Trn} = 0
    If ?tra:MaxChargeablePartsCost{prop:ReadOnly} = True
        ?tra:MaxChargeablePartsCost{prop:FontColor} = 65793
        ?tra:MaxChargeablePartsCost{prop:Color} = 15066597
    Elsif ?tra:MaxChargeablePartsCost{prop:Req} = True
        ?tra:MaxChargeablePartsCost{prop:FontColor} = 65793
        ?tra:MaxChargeablePartsCost{prop:Color} = 8454143
    Else ! If ?tra:MaxChargeablePartsCost{prop:Req} = True
        ?tra:MaxChargeablePartsCost{prop:FontColor} = 65793
        ?tra:MaxChargeablePartsCost{prop:Color} = 16777215
    End ! If ?tra:MaxChargeablePartsCost{prop:Req} = True
    ?tra:MaxChargeablePartsCost{prop:Trn} = 0
    ?tra:MaxChargeablePartsCost{prop:FontStyle} = font:Bold
    ?tra:RefurbCharge{prop:Font,3} = -1
    ?tra:RefurbCharge{prop:Color} = 15066597
    ?tra:RefurbCharge{prop:Trn} = 0
    ?Charge_Type_Group{prop:Font,3} = -1
    ?Charge_Type_Group{prop:Color} = 15066597
    ?Charge_Type_Group{prop:Trn} = 0
    If ?tra:ChargeType{prop:ReadOnly} = True
        ?tra:ChargeType{prop:FontColor} = 65793
        ?tra:ChargeType{prop:Color} = 15066597
    Elsif ?tra:ChargeType{prop:Req} = True
        ?tra:ChargeType{prop:FontColor} = 65793
        ?tra:ChargeType{prop:Color} = 8454143
    Else ! If ?tra:ChargeType{prop:Req} = True
        ?tra:ChargeType{prop:FontColor} = 65793
        ?tra:ChargeType{prop:Color} = 16777215
    End ! If ?tra:ChargeType{prop:Req} = True
    ?tra:ChargeType{prop:Trn} = 0
    ?tra:ChargeType{prop:FontStyle} = font:Bold
    ?Prompt26{prop:FontColor} = -1
    ?Prompt26{prop:Color} = 15066597
    ?Prompt26:2{prop:FontColor} = -1
    ?Prompt26:2{prop:Color} = 15066597
    If ?tra:WarChargeType{prop:ReadOnly} = True
        ?tra:WarChargeType{prop:FontColor} = 65793
        ?tra:WarChargeType{prop:Color} = 15066597
    Elsif ?tra:WarChargeType{prop:Req} = True
        ?tra:WarChargeType{prop:FontColor} = 65793
        ?tra:WarChargeType{prop:Color} = 8454143
    Else ! If ?tra:WarChargeType{prop:Req} = True
        ?tra:WarChargeType{prop:FontColor} = 65793
        ?tra:WarChargeType{prop:Color} = 16777215
    End ! If ?tra:WarChargeType{prop:Req} = True
    ?tra:WarChargeType{prop:Trn} = 0
    ?tra:WarChargeType{prop:FontStyle} = font:Bold
    ?tra:ZeroChargeable{prop:Font,3} = -1
    ?tra:ZeroChargeable{prop:Color} = 15066597
    ?tra:ZeroChargeable{prop:Trn} = 0
    ?tra:MultiInvoice{prop:Font,3} = -1
    ?tra:MultiInvoice{prop:Color} = 15066597
    ?tra:MultiInvoice{prop:Trn} = 0
    ?TRA:MultiIn:Radio1{prop:Font,3} = -1
    ?TRA:MultiIn:Radio1{prop:Color} = 15066597
    ?TRA:MultiIn:Radio1{prop:Trn} = 0
    ?TRA:MultiIn:Radio2{prop:Font,3} = -1
    ?TRA:MultiIn:Radio2{prop:Color} = 15066597
    ?TRA:MultiIn:Radio2{prop:Trn} = 0
    ?TRA:MultiIn:Radio3{prop:Font,3} = -1
    ?TRA:MultiIn:Radio3{prop:Color} = 15066597
    ?TRA:MultiIn:Radio3{prop:Trn} = 0
    ?TRA:MultiIn:Radio4{prop:Font,3} = -1
    ?TRA:MultiIn:Radio4{prop:Color} = 15066597
    ?TRA:MultiIn:Radio4{prop:Trn} = 0
    ?tra:Allow_Loan{prop:Font,3} = -1
    ?tra:Allow_Loan{prop:Color} = 15066597
    ?tra:Allow_Loan{prop:Trn} = 0
    ?tra:Allow_Cash_Sales{prop:Font,3} = -1
    ?tra:Allow_Cash_Sales{prop:Color} = 15066597
    ?tra:Allow_Cash_Sales{prop:Trn} = 0
    ?tra:StopThirdParty{prop:Font,3} = -1
    ?tra:StopThirdParty{prop:Color} = 15066597
    ?tra:StopThirdParty{prop:Trn} = 0
    ?tra:Allow_Exchange{prop:Font,3} = -1
    ?tra:Allow_Exchange{prop:Color} = 15066597
    ?tra:Allow_Exchange{prop:Trn} = 0
    ?tra:MakeSplitJob{prop:Font,3} = -1
    ?tra:MakeSplitJob{prop:Color} = 15066597
    ?tra:MakeSplitJob{prop:Trn} = 0
    ?tra:ShowRepairTypes{prop:Font,3} = -1
    ?tra:ShowRepairTypes{prop:Color} = 15066597
    ?tra:ShowRepairTypes{prop:Trn} = 0
    ?tra:ExcludeBouncer{prop:Font,3} = -1
    ?tra:ExcludeBouncer{prop:Color} = 15066597
    ?tra:ExcludeBouncer{prop:Trn} = 0
    ?TRA:Estimate_If_Over:Prompt{prop:FontColor} = -1
    ?TRA:Estimate_If_Over:Prompt{prop:Color} = 15066597
    ?SplitJobGroup{prop:Font,3} = -1
    ?SplitJobGroup{prop:Color} = 15066597
    ?SplitJobGroup{prop:Trn} = 0
    ?tra:SplitJobStatus:Prompt{prop:FontColor} = -1
    ?tra:SplitJobStatus:Prompt{prop:Color} = 15066597
    If ?tra:SplitJobStatus{prop:ReadOnly} = True
        ?tra:SplitJobStatus{prop:FontColor} = 65793
        ?tra:SplitJobStatus{prop:Color} = 15066597
    Elsif ?tra:SplitJobStatus{prop:Req} = True
        ?tra:SplitJobStatus{prop:FontColor} = 65793
        ?tra:SplitJobStatus{prop:Color} = 8454143
    Else ! If ?tra:SplitJobStatus{prop:Req} = True
        ?tra:SplitJobStatus{prop:FontColor} = 65793
        ?tra:SplitJobStatus{prop:Color} = 16777215
    End ! If ?tra:SplitJobStatus{prop:Req} = True
    ?tra:SplitJobStatus{prop:Trn} = 0
    ?tra:SplitJobStatus{prop:FontStyle} = font:Bold
    ?tra:SplitExchangeStatus:Prompt{prop:FontColor} = -1
    ?tra:SplitExchangeStatus:Prompt{prop:Color} = 15066597
    If ?tra:SplitExchangeStatus{prop:ReadOnly} = True
        ?tra:SplitExchangeStatus{prop:FontColor} = 65793
        ?tra:SplitExchangeStatus{prop:Color} = 15066597
    Elsif ?tra:SplitExchangeStatus{prop:Req} = True
        ?tra:SplitExchangeStatus{prop:FontColor} = 65793
        ?tra:SplitExchangeStatus{prop:Color} = 8454143
    Else ! If ?tra:SplitExchangeStatus{prop:Req} = True
        ?tra:SplitExchangeStatus{prop:FontColor} = 65793
        ?tra:SplitExchangeStatus{prop:Color} = 16777215
    End ! If ?tra:SplitExchangeStatus{prop:Req} = True
    ?tra:SplitExchangeStatus{prop:Trn} = 0
    ?tra:SplitExchangeStatus{prop:FontStyle} = font:Bold
    ?tra:SplitCChargeType:Prompt{prop:FontColor} = -1
    ?tra:SplitCChargeType:Prompt{prop:Color} = 15066597
    If ?tra:SplitCChargeType{prop:ReadOnly} = True
        ?tra:SplitCChargeType{prop:FontColor} = 65793
        ?tra:SplitCChargeType{prop:Color} = 15066597
    Elsif ?tra:SplitCChargeType{prop:Req} = True
        ?tra:SplitCChargeType{prop:FontColor} = 65793
        ?tra:SplitCChargeType{prop:Color} = 8454143
    Else ! If ?tra:SplitCChargeType{prop:Req} = True
        ?tra:SplitCChargeType{prop:FontColor} = 65793
        ?tra:SplitCChargeType{prop:Color} = 16777215
    End ! If ?tra:SplitCChargeType{prop:Req} = True
    ?tra:SplitCChargeType{prop:Trn} = 0
    ?tra:SplitCChargeType{prop:FontStyle} = font:Bold
    ?tra:SplitWChargeType:Prompt{prop:FontColor} = -1
    ?tra:SplitWChargeType:Prompt{prop:Color} = 15066597
    If ?tra:SplitWChargeType{prop:ReadOnly} = True
        ?tra:SplitWChargeType{prop:FontColor} = 65793
        ?tra:SplitWChargeType{prop:Color} = 15066597
    Elsif ?tra:SplitWChargeType{prop:Req} = True
        ?tra:SplitWChargeType{prop:FontColor} = 65793
        ?tra:SplitWChargeType{prop:Color} = 8454143
    Else ! If ?tra:SplitWChargeType{prop:Req} = True
        ?tra:SplitWChargeType{prop:FontColor} = 65793
        ?tra:SplitWChargeType{prop:Color} = 16777215
    End ! If ?tra:SplitWChargeType{prop:Req} = True
    ?tra:SplitWChargeType{prop:Trn} = 0
    ?tra:SplitWChargeType{prop:FontStyle} = font:Bold
    ?tra:Force_Estimate{prop:Font,3} = -1
    ?tra:Force_Estimate{prop:Color} = 15066597
    ?tra:Force_Estimate{prop:Trn} = 0
    If ?tra:Estimate_If_Over{prop:ReadOnly} = True
        ?tra:Estimate_If_Over{prop:FontColor} = 65793
        ?tra:Estimate_If_Over{prop:Color} = 15066597
    Elsif ?tra:Estimate_If_Over{prop:Req} = True
        ?tra:Estimate_If_Over{prop:FontColor} = 65793
        ?tra:Estimate_If_Over{prop:Color} = 8454143
    Else ! If ?tra:Estimate_If_Over{prop:Req} = True
        ?tra:Estimate_If_Over{prop:FontColor} = 65793
        ?tra:Estimate_If_Over{prop:Color} = 16777215
    End ! If ?tra:Estimate_If_Over{prop:Req} = True
    ?tra:Estimate_If_Over{prop:Trn} = 0
    ?tra:Estimate_If_Over{prop:FontStyle} = font:Bold
    ?Tab6{prop:Color} = 15066597
    ?tra:Retail_Payment_Type{prop:Font,3} = -1
    ?tra:Retail_Payment_Type{prop:Color} = 15066597
    ?tra:Retail_Payment_Type{prop:Trn} = 0
    ?TRA:Retail_Payment_Type:Radio1{prop:Font,3} = -1
    ?TRA:Retail_Payment_Type:Radio1{prop:Color} = 15066597
    ?TRA:Retail_Payment_Type:Radio1{prop:Trn} = 0
    ?TRA:Retail_Payment_Type:Radio2{prop:Font,3} = -1
    ?TRA:Retail_Payment_Type:Radio2{prop:Color} = 15066597
    ?TRA:Retail_Payment_Type:Radio2{prop:Trn} = 0
    ?payment_Method{prop:FontColor} = -1
    ?payment_Method{prop:Color} = 15066597
    ?tra:Retail_Price_Structure{prop:Font,3} = -1
    ?tra:Retail_Price_Structure{prop:Color} = 15066597
    ?tra:Retail_Price_Structure{prop:Trn} = 0
    ?TRA:Retail_Price_Structure:Radio1{prop:Font,3} = -1
    ?TRA:Retail_Price_Structure:Radio1{prop:Color} = 15066597
    ?TRA:Retail_Price_Structure:Radio1{prop:Trn} = 0
    ?TRA:Retail_Price_Structure:Radio2{prop:Font,3} = -1
    ?TRA:Retail_Price_Structure:Radio2{prop:Color} = 15066597
    ?TRA:Retail_Price_Structure:Radio2{prop:Trn} = 0
    ?tra:Retail_Price_Structure:Radio3{prop:Font,3} = -1
    ?tra:Retail_Price_Structure:Radio3{prop:Color} = 15066597
    ?tra:Retail_Price_Structure:Radio3{prop:Trn} = 0
    ?pricing_structure{prop:FontColor} = -1
    ?pricing_structure{prop:Color} = 15066597
    ?tra:Print_Retail_Picking_Note{prop:Font,3} = -1
    ?tra:Print_Retail_Picking_Note{prop:Color} = 15066597
    ?tra:Print_Retail_Picking_Note{prop:Trn} = 0
    ?tra:Price_Retail_Despatch{prop:Font,3} = -1
    ?tra:Price_Retail_Despatch{prop:Color} = 15066597
    ?tra:Price_Retail_Despatch{prop:Trn} = 0
    ?tra:RetailZeroParts{prop:Font,3} = -1
    ?tra:RetailZeroParts{prop:Color} = 15066597
    ?tra:RetailZeroParts{prop:Trn} = 0
    ?tra:EuroApplies{prop:Font,3} = -1
    ?tra:EuroApplies{prop:Color} = 15066597
    ?tra:EuroApplies{prop:Trn} = 0
    ?Tab10{prop:Color} = 15066597
    ?TRA:Password:Prompt{prop:FontColor} = -1
    ?TRA:Password:Prompt{prop:Color} = 15066597
    If ?tra:Password{prop:ReadOnly} = True
        ?tra:Password{prop:FontColor} = 65793
        ?tra:Password{prop:Color} = 15066597
    Elsif ?tra:Password{prop:Req} = True
        ?tra:Password{prop:FontColor} = 65793
        ?tra:Password{prop:Color} = 8454143
    Else ! If ?tra:Password{prop:Req} = True
        ?tra:Password{prop:FontColor} = 65793
        ?tra:Password{prop:Color} = 16777215
    End ! If ?tra:Password{prop:Req} = True
    ?tra:Password{prop:Trn} = 0
    ?tra:Password{prop:FontStyle} = font:Bold
    ?tra:WebPassword1:Prompt{prop:FontColor} = -1
    ?tra:WebPassword1:Prompt{prop:Color} = 15066597
    If ?tra:WebPassword1{prop:ReadOnly} = True
        ?tra:WebPassword1{prop:FontColor} = 65793
        ?tra:WebPassword1{prop:Color} = 15066597
    Elsif ?tra:WebPassword1{prop:Req} = True
        ?tra:WebPassword1{prop:FontColor} = 65793
        ?tra:WebPassword1{prop:Color} = 8454143
    Else ! If ?tra:WebPassword1{prop:Req} = True
        ?tra:WebPassword1{prop:FontColor} = 65793
        ?tra:WebPassword1{prop:Color} = 16777215
    End ! If ?tra:WebPassword1{prop:Req} = True
    ?tra:WebPassword1{prop:Trn} = 0
    ?tra:WebPassword1{prop:FontStyle} = font:Bold
    ?TRA:Password:Prompt:2{prop:FontColor} = -1
    ?TRA:Password:Prompt:2{prop:Color} = 15066597
    If ?tra:WebMemo{prop:ReadOnly} = True
        ?tra:WebMemo{prop:FontColor} = 65793
        ?tra:WebMemo{prop:Color} = 15066597
    Elsif ?tra:WebMemo{prop:Req} = True
        ?tra:WebMemo{prop:FontColor} = 65793
        ?tra:WebMemo{prop:Color} = 8454143
    Else ! If ?tra:WebMemo{prop:Req} = True
        ?tra:WebMemo{prop:FontColor} = 65793
        ?tra:WebMemo{prop:Color} = 16777215
    End ! If ?tra:WebMemo{prop:Req} = True
    ?tra:WebMemo{prop:Trn} = 0
    ?tra:WebMemo{prop:FontStyle} = font:Bold
    ?StockAccessLevels{prop:Font,3} = -1
    ?StockAccessLevels{prop:Color} = 15066597
    ?StockAccessLevels{prop:Trn} = 0
    ?tra:E1{prop:Font,3} = -1
    ?tra:E1{prop:Color} = 15066597
    ?tra:E1{prop:Trn} = 0
    ?tra:E2{prop:Font,3} = -1
    ?tra:E2{prop:Color} = 15066597
    ?tra:E2{prop:Trn} = 0
    ?tra:E3{prop:Font,3} = -1
    ?tra:E3{prop:Color} = 15066597
    ?tra:E3{prop:Trn} = 0
    ?Tab11{prop:Color} = 15066597
    ?tra:Print_Despatch_Notes{prop:Font,3} = -1
    ?tra:Print_Despatch_Notes{prop:Color} = 15066597
    ?tra:Print_Despatch_Notes{prop:Trn} = 0
    ?tra:IgnoreDespatch{prop:Font,3} = -1
    ?tra:IgnoreDespatch{prop:Color} = 15066597
    ?tra:IgnoreDespatch{prop:Trn} = 0
    ?tra:Print_Despatch_Complete{prop:Font,3} = -1
    ?tra:Print_Despatch_Complete{prop:Color} = 15066597
    ?tra:Print_Despatch_Complete{prop:Trn} = 0
    ?tra:Print_Despatch_Despatch{prop:Font,3} = -1
    ?tra:Print_Despatch_Despatch{prop:Color} = 15066597
    ?tra:Print_Despatch_Despatch{prop:Trn} = 0
    ?tra:HideDespAdd{prop:Font,3} = -1
    ?tra:HideDespAdd{prop:Color} = 15066597
    ?tra:HideDespAdd{prop:Trn} = 0
    ?Despatch_note_type{prop:Font,3} = -1
    ?Despatch_note_type{prop:Color} = 15066597
    ?Despatch_note_type{prop:Trn} = 0
    ?tra:Despatch_Note_Per_Item{prop:Font,3} = -1
    ?tra:Despatch_Note_Per_Item{prop:Color} = 15066597
    ?tra:Despatch_Note_Per_Item{prop:Trn} = 0
    ?TRA:Summary_Despatch_Notes:2{prop:Font,3} = -1
    ?TRA:Summary_Despatch_Notes:2{prop:Color} = 15066597
    ?TRA:Summary_Despatch_Notes:2{prop:Trn} = 0
    ?tra:IndividualSummary{prop:Font,3} = -1
    ?tra:IndividualSummary{prop:Color} = 15066597
    ?tra:IndividualSummary{prop:Trn} = 0
    ?tra:UseAlternateDespatchNote{prop:Font,3} = -1
    ?tra:UseAlternateDespatchNote{prop:Color} = 15066597
    ?tra:UseAlternateDespatchNote{prop:Trn} = 0
    ?tra:Price_Despatch{prop:Font,3} = -1
    ?tra:Price_Despatch{prop:Color} = 15066597
    ?tra:Price_Despatch{prop:Trn} = 0
    ?tra:InvoiceAtDespatch{prop:Font,3} = -1
    ?tra:InvoiceAtDespatch{prop:Color} = 15066597
    ?tra:InvoiceAtDespatch{prop:Trn} = 0
    ?tra:Despatch_Paid_Jobs{prop:Font,3} = -1
    ?tra:Despatch_Paid_Jobs{prop:Color} = 15066597
    ?tra:Despatch_Paid_Jobs{prop:Trn} = 0
    ?tra:Despatch_Invoiced_Jobs{prop:Font,3} = -1
    ?tra:Despatch_Invoiced_Jobs{prop:Color} = 15066597
    ?tra:Despatch_Invoiced_Jobs{prop:Trn} = 0
    ?tra:Skip_Despatch{prop:Font,3} = -1
    ?tra:Skip_Despatch{prop:Color} = 15066597
    ?tra:Skip_Despatch{prop:Trn} = 0
    ?tra:UseDespatchDetails{prop:Font,3} = -1
    ?tra:UseDespatchDetails{prop:Color} = 15066597
    ?tra:UseDespatchDetails{prop:Trn} = 0
    ?AlternativeContactNumbers{prop:Font,3} = -1
    ?AlternativeContactNumbers{prop:Color} = 15066597
    ?AlternativeContactNumbers{prop:Trn} = 0
    ?tra:AltTelephoneNumber:Prompt{prop:FontColor} = -1
    ?tra:AltTelephoneNumber:Prompt{prop:Color} = 15066597
    If ?tra:AltTelephoneNumber{prop:ReadOnly} = True
        ?tra:AltTelephoneNumber{prop:FontColor} = 65793
        ?tra:AltTelephoneNumber{prop:Color} = 15066597
    Elsif ?tra:AltTelephoneNumber{prop:Req} = True
        ?tra:AltTelephoneNumber{prop:FontColor} = 65793
        ?tra:AltTelephoneNumber{prop:Color} = 8454143
    Else ! If ?tra:AltTelephoneNumber{prop:Req} = True
        ?tra:AltTelephoneNumber{prop:FontColor} = 65793
        ?tra:AltTelephoneNumber{prop:Color} = 16777215
    End ! If ?tra:AltTelephoneNumber{prop:Req} = True
    ?tra:AltTelephoneNumber{prop:Trn} = 0
    ?tra:AltTelephoneNumber{prop:FontStyle} = font:Bold
    ?tra:AltFaxNumber:Prompt{prop:FontColor} = -1
    ?tra:AltFaxNumber:Prompt{prop:Color} = 15066597
    If ?tra:AltFaxNumber{prop:ReadOnly} = True
        ?tra:AltFaxNumber{prop:FontColor} = 65793
        ?tra:AltFaxNumber{prop:Color} = 15066597
    Elsif ?tra:AltFaxNumber{prop:Req} = True
        ?tra:AltFaxNumber{prop:FontColor} = 65793
        ?tra:AltFaxNumber{prop:Color} = 8454143
    Else ! If ?tra:AltFaxNumber{prop:Req} = True
        ?tra:AltFaxNumber{prop:FontColor} = 65793
        ?tra:AltFaxNumber{prop:Color} = 16777215
    End ! If ?tra:AltFaxNumber{prop:Req} = True
    ?tra:AltFaxNumber{prop:Trn} = 0
    ?tra:AltFaxNumber{prop:FontStyle} = font:Bold
    ?tra:AltEmailAddress:Prompt{prop:FontColor} = -1
    ?tra:AltEmailAddress:Prompt{prop:Color} = 15066597
    If ?tra:AltEmailAddress{prop:ReadOnly} = True
        ?tra:AltEmailAddress{prop:FontColor} = 65793
        ?tra:AltEmailAddress{prop:Color} = 15066597
    Elsif ?tra:AltEmailAddress{prop:Req} = True
        ?tra:AltEmailAddress{prop:FontColor} = 65793
        ?tra:AltEmailAddress{prop:Color} = 8454143
    Else ! If ?tra:AltEmailAddress{prop:Req} = True
        ?tra:AltEmailAddress{prop:FontColor} = 65793
        ?tra:AltEmailAddress{prop:Color} = 16777215
    End ! If ?tra:AltEmailAddress{prop:Req} = True
    ?tra:AltEmailAddress{prop:Trn} = 0
    ?tra:AltEmailAddress{prop:FontStyle} = font:Bold
    ?tra:UseCustDespAdd{prop:Font,3} = -1
    ?tra:UseCustDespAdd{prop:Color} = 15066597
    ?tra:UseCustDespAdd{prop:Trn} = 0
    ?tra:UseTradeContactNo{prop:Font,3} = -1
    ?tra:UseTradeContactNo{prop:Color} = 15066597
    ?tra:UseTradeContactNo{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Hide_Fields     Routine
    If tra:account_type <> 'CREDIT'
!        Disable(?tra:credit_limit)
!        Disable(?tra:credit_limit:prompt)
    Else
!        Enable(?tra:credit_limit)
!        Enable(?tra:credit_limit:prompt)
    End
    If tra:use_sub_accounts <> 'YES'
        BRW2.InsertControl = 0
        BRW2.ChangeControl = 0
        BRW2.DeleteControl = 0
        Hide(?Insert)
        Hide(?Change)
        Hide(?Delete)
    Else
        BRW2.InsertControl = ?Insert
        BRW2.ChangeControl = ?Change
        BRW2.DeleteControl = ?Delete
        Unhide(?Insert)
        Unhide(?Change)
        Unhide(?Delete)
    End
    Display()
Copy_Main_Account           Routine

    setcursor(cursor:wait)
    save_sub_id = access:subtracc.savefile()
    access:subtracc.clearkey(sub:main_account_key)
    sub:main_account_number = tra:account_number
    set(sub:main_account_key,sub:main_account_key)
    loop
        if access:subtracc.next()
           break
        end !if
        if sub:main_account_number <> tra:account_number      |
            then break.  ! end if
        Delete(subtracc)
    end !loop
    access:subtracc.restorefile(save_sub_id)
    setcursor()

    Clear(sub:record)
    sub:main_account_number = tra:account_number
    sub:account_number      = tra:account_number
    SUB:Company_Name        = tra:company_Name
    sub:branch              = 'MAIN ACCOUNT'
    TRA:Postcode            =TRA:Postcode
    TRA:Company_Name        =TRA:Company_Name
    TRA:Address_Line1       =TRA:Address_Line1
    TRA:Address_Line2       =TRA:Address_Line2
    TRA:Address_Line3       =TRA:Address_Line3
    TRA:Telephone_Number    =TRA:Telephone_Number
    TRA:Fax_Number          =TRA:Fax_Number
    Access:subtracc.Insert()

Fill_Rates      Routine
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = tra:labour_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_labour_temp  = vat:vat_rate
    end
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = tra:parts_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_parts_temp  = vat:vat_rate
    end
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = tra:retail_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_retail_temp  = vat:vat_rate
    end

    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateTRADEACC',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateTRADEACC',1)
    SolaceViewVars('pos',pos,'UpdateTRADEACC',1)
    SolaceViewVars('ok_pressed_temp',ok_pressed_temp,'UpdateTRADEACC',1)
    SolaceViewVars('Account_Number_temp',Account_Number_temp,'UpdateTRADEACC',1)
    SolaceViewVars('Company_Name_temp',Company_Name_temp,'UpdateTRADEACC',1)
    SolaceViewVars('use_sub_account_temp',use_sub_account_temp,'UpdateTRADEACC',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateTRADEACC',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateTRADEACC',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateTRADEACC',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateTRADEACC',1)
    SolaceViewVars('discount_labour_temp',discount_labour_temp,'UpdateTRADEACC',1)
    SolaceViewVars('discount_parts_temp',discount_parts_temp,'UpdateTRADEACC',1)
    SolaceViewVars('vat_labour_temp',vat_labour_temp,'UpdateTRADEACC',1)
    SolaceViewVars('vat_parts_temp',vat_parts_temp,'UpdateTRADEACC',1)
    SolaceViewVars('Discount_Retail_Temp',Discount_Retail_Temp,'UpdateTRADEACC',1)
    SolaceViewVars('vat_retail_temp',vat_retail_temp,'UpdateTRADEACC',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab7;  SolaceCtrlName = '?Tab7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Account_Number:Prompt;  SolaceCtrlName = '?TRA:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Account_Number;  SolaceCtrlName = '?tra:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Company_Name:Prompt;  SolaceCtrlName = '?TRA:Company_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Company_Name;  SolaceCtrlName = '?tra:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Postcode:Prompt;  SolaceCtrlName = '?TRA:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Postcode;  SolaceCtrlName = '?tra:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Address_Line1:Prompt;  SolaceCtrlName = '?TRA:Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Address_Line1;  SolaceCtrlName = '?tra:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Clear_Address;  SolaceCtrlName = '?Clear_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Address_Line2;  SolaceCtrlName = '?tra:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Address_Line3;  SolaceCtrlName = '?tra:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Telephone_Number:Prompt;  SolaceCtrlName = '?TRA:Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?telephone_number;  SolaceCtrlName = '?telephone_number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Telephone_Number;  SolaceCtrlName = '?tra:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?fax_number;  SolaceCtrlName = '?fax_number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Fax_Number;  SolaceCtrlName = '?tra:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:EmailAddress:Prompt;  SolaceCtrlName = '?tra:EmailAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:EmailAddress;  SolaceCtrlName = '?tra:EmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Contact_Name:Prompt;  SolaceCtrlName = '?TRA:Contact_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Contact_Name;  SolaceCtrlName = '?tra:Contact_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_codes;  SolaceCtrlName = '?vat_codes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?labour:2;  SolaceCtrlName = '?labour:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Labour_VAT_Code;  SolaceCtrlName = '?tra:Labour_VAT_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?parts:2;  SolaceCtrlName = '?parts:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Parts_VAT_Code;  SolaceCtrlName = '?tra:Parts_VAT_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retail:2;  SolaceCtrlName = '?retail:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Retail_VAT_Code;  SolaceCtrlName = '?tra:Retail_VAT_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Courier_Incoming:Prompt;  SolaceCtrlName = '?tra:Courier_Incoming:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Courier_Incoming;  SolaceCtrlName = '?tra:Courier_Incoming';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupIncomingCourier;  SolaceCtrlName = '?LookupIncomingCourier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Courier_Outgoing:Prompt;  SolaceCtrlName = '?tra:Courier_Outgoing:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Courier_Outgoing;  SolaceCtrlName = '?tra:Courier_Outgoing';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupOutgoingCourier;  SolaceCtrlName = '?LookupOutgoingCourier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Courier_Cost:Prompt;  SolaceCtrlName = '?TRA:Courier_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Courier_Cost;  SolaceCtrlName = '?tra:Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:loan_stock_type:prompt;  SolaceCtrlName = '?tra:loan_stock_type:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Loan_Stock_Type;  SolaceCtrlName = '?tra:Loan_Stock_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:exchange_stock_type:prompt;  SolaceCtrlName = '?tra:exchange_stock_type:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Exchange_Stock_Type;  SolaceCtrlName = '?tra:Exchange_Stock_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?turnaround_time;  SolaceCtrlName = '?turnaround_time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Turnaround_Time;  SolaceCtrlName = '?tra:Turnaround_Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Stop_Account;  SolaceCtrlName = '?tra:Stop_Account';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab8;  SolaceCtrlName = '?Tab8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Use_Contact_Name;  SolaceCtrlName = '?tra:Use_Contact_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Use_Delivery_Address;  SolaceCtrlName = '?tra:Use_Delivery_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Use_Collection_Address;  SolaceCtrlName = '?tra:Use_Collection_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Use_Customer_Address;  SolaceCtrlName = '?tra:Use_Customer_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Invoice_Customer_Address;  SolaceCtrlName = '?tra:Invoice_Customer_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EmailGroup;  SolaceCtrlName = '?EmailGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:EmailRecipientList;  SolaceCtrlName = '?tra:EmailRecipientList';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:EmailEndUser;  SolaceCtrlName = '?tra:EmailEndUser';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:AutoSendStatusEmails;  SolaceCtrlName = '?tra:AutoSendStatusEmails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt28;  SolaceCtrlName = '?Prompt28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Account_Number;  SolaceCtrlName = '?sub:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Branch;  SolaceCtrlName = '?sub:Branch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Company_Name;  SolaceCtrlName = '?sub:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Use_Sub_Accounts;  SolaceCtrlName = '?tra:Use_Sub_Accounts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Invoice_Sub_Accounts;  SolaceCtrlName = '?tra:Invoice_Sub_Accounts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab9;  SolaceCtrlName = '?Tab9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:ForceCommonFault;  SolaceCtrlName = '?tra:ForceCommonFault';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:ForceOrderNumber;  SolaceCtrlName = '?tra:ForceOrderNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:ExchangeAcc;  SolaceCtrlName = '?tra:ExchangeAcc';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:InvoiceType;  SolaceCtrlName = '?tra:InvoiceType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option4:Radio1;  SolaceCtrlName = '?Option4:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option4:Radio1:2;  SolaceCtrlName = '?Option4:Radio1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt34;  SolaceCtrlName = '?Prompt34';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:CheckChargeablePartsCost;  SolaceCtrlName = '?tra:CheckChargeablePartsCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:MaxChargeablePartsCost;  SolaceCtrlName = '?tra:MaxChargeablePartsCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:RefurbCharge;  SolaceCtrlName = '?tra:RefurbCharge';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Charge_Type_Group;  SolaceCtrlName = '?Charge_Type_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:ChargeType;  SolaceCtrlName = '?tra:ChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt26;  SolaceCtrlName = '?Prompt26';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt26:2;  SolaceCtrlName = '?Prompt26:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:WarChargeType;  SolaceCtrlName = '?tra:WarChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:ZeroChargeable;  SolaceCtrlName = '?tra:ZeroChargeable';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:MultiInvoice;  SolaceCtrlName = '?tra:MultiInvoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:MultiIn:Radio1;  SolaceCtrlName = '?TRA:MultiIn:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:MultiIn:Radio2;  SolaceCtrlName = '?TRA:MultiIn:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:MultiIn:Radio3;  SolaceCtrlName = '?TRA:MultiIn:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:MultiIn:Radio4;  SolaceCtrlName = '?TRA:MultiIn:Radio4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Allow_Loan;  SolaceCtrlName = '?tra:Allow_Loan';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Allow_Cash_Sales;  SolaceCtrlName = '?tra:Allow_Cash_Sales';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:StopThirdParty;  SolaceCtrlName = '?tra:StopThirdParty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Allow_Exchange;  SolaceCtrlName = '?tra:Allow_Exchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:MakeSplitJob;  SolaceCtrlName = '?tra:MakeSplitJob';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:ShowRepairTypes;  SolaceCtrlName = '?tra:ShowRepairTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:ExcludeBouncer;  SolaceCtrlName = '?tra:ExcludeBouncer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Estimate_If_Over:Prompt;  SolaceCtrlName = '?TRA:Estimate_If_Over:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SplitJobGroup;  SolaceCtrlName = '?SplitJobGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:SplitJobStatus:Prompt;  SolaceCtrlName = '?tra:SplitJobStatus:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:SplitJobStatus;  SolaceCtrlName = '?tra:SplitJobStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupJobStatus;  SolaceCtrlName = '?LookupJobStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:SplitExchangeStatus:Prompt;  SolaceCtrlName = '?tra:SplitExchangeStatus:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:SplitExchangeStatus;  SolaceCtrlName = '?tra:SplitExchangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupExchangeStatus;  SolaceCtrlName = '?LookupExchangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:SplitCChargeType:Prompt;  SolaceCtrlName = '?tra:SplitCChargeType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:SplitCChargeType;  SolaceCtrlName = '?tra:SplitCChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupCChargeType;  SolaceCtrlName = '?LookupCChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:SplitWChargeType:Prompt;  SolaceCtrlName = '?tra:SplitWChargeType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:SplitWChargeType;  SolaceCtrlName = '?tra:SplitWChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupWChargeType;  SolaceCtrlName = '?LookupWChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Force_Estimate;  SolaceCtrlName = '?tra:Force_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Estimate_If_Over;  SolaceCtrlName = '?tra:Estimate_If_Over';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Retail_Payment_Type;  SolaceCtrlName = '?tra:Retail_Payment_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Retail_Payment_Type:Radio1;  SolaceCtrlName = '?TRA:Retail_Payment_Type:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Retail_Payment_Type:Radio2;  SolaceCtrlName = '?TRA:Retail_Payment_Type:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?payment_Method;  SolaceCtrlName = '?payment_Method';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Retail_Price_Structure;  SolaceCtrlName = '?tra:Retail_Price_Structure';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Retail_Price_Structure:Radio1;  SolaceCtrlName = '?TRA:Retail_Price_Structure:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Retail_Price_Structure:Radio2;  SolaceCtrlName = '?TRA:Retail_Price_Structure:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Retail_Price_Structure:Radio3;  SolaceCtrlName = '?tra:Retail_Price_Structure:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pricing_structure;  SolaceCtrlName = '?pricing_structure';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Print_Retail_Picking_Note;  SolaceCtrlName = '?tra:Print_Retail_Picking_Note';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Price_Retail_Despatch;  SolaceCtrlName = '?tra:Price_Retail_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:RetailZeroParts;  SolaceCtrlName = '?tra:RetailZeroParts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:EuroApplies;  SolaceCtrlName = '?tra:EuroApplies';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab10;  SolaceCtrlName = '?Tab10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Password:Prompt;  SolaceCtrlName = '?TRA:Password:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Password;  SolaceCtrlName = '?tra:Password';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:WebPassword1:Prompt;  SolaceCtrlName = '?tra:WebPassword1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:WebPassword1;  SolaceCtrlName = '?tra:WebPassword1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Password:Prompt:2;  SolaceCtrlName = '?TRA:Password:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:WebMemo;  SolaceCtrlName = '?tra:WebMemo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockAccessLevels;  SolaceCtrlName = '?StockAccessLevels';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:E1;  SolaceCtrlName = '?tra:E1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:E2;  SolaceCtrlName = '?tra:E2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:E3;  SolaceCtrlName = '?tra:E3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab11;  SolaceCtrlName = '?Tab11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Print_Despatch_Notes;  SolaceCtrlName = '?tra:Print_Despatch_Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:IgnoreDespatch;  SolaceCtrlName = '?tra:IgnoreDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Print_Despatch_Complete;  SolaceCtrlName = '?tra:Print_Despatch_Complete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Print_Despatch_Despatch;  SolaceCtrlName = '?tra:Print_Despatch_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:HideDespAdd;  SolaceCtrlName = '?tra:HideDespAdd';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Despatch_note_type;  SolaceCtrlName = '?Despatch_note_type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Despatch_Note_Per_Item;  SolaceCtrlName = '?tra:Despatch_Note_Per_Item';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRA:Summary_Despatch_Notes:2;  SolaceCtrlName = '?TRA:Summary_Despatch_Notes:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:IndividualSummary;  SolaceCtrlName = '?tra:IndividualSummary';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:UseAlternateDespatchNote;  SolaceCtrlName = '?tra:UseAlternateDespatchNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Price_Despatch;  SolaceCtrlName = '?tra:Price_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:InvoiceAtDespatch;  SolaceCtrlName = '?tra:InvoiceAtDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Despatch_Paid_Jobs;  SolaceCtrlName = '?tra:Despatch_Paid_Jobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Despatch_Invoiced_Jobs;  SolaceCtrlName = '?tra:Despatch_Invoiced_Jobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:Skip_Despatch;  SolaceCtrlName = '?tra:Skip_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:UseDespatchDetails;  SolaceCtrlName = '?tra:UseDespatchDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AlternativeContactNumbers;  SolaceCtrlName = '?AlternativeContactNumbers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:AltTelephoneNumber:Prompt;  SolaceCtrlName = '?tra:AltTelephoneNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:AltTelephoneNumber;  SolaceCtrlName = '?tra:AltTelephoneNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:AltFaxNumber:Prompt;  SolaceCtrlName = '?tra:AltFaxNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:AltFaxNumber;  SolaceCtrlName = '?tra:AltFaxNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:AltEmailAddress:Prompt;  SolaceCtrlName = '?tra:AltEmailAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:AltEmailAddress;  SolaceCtrlName = '?tra:AltEmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:UseCustDespAdd;  SolaceCtrlName = '?tra:UseCustDespAdd';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tra:UseTradeContactNo;  SolaceCtrlName = '?tra:UseTradeContactNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button6;  SolaceCtrlName = '?Button6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FaultCodes;  SolaceCtrlName = '?FaultCodes';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Trade Account'
  OF ChangeRecord
    ActionMessage = 'Changing A Trade Account'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateTRADEACC')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateTRADEACC')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRA:Account_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(tra:Record,History::tra:Record)
  SELF.AddHistoryField(?tra:Account_Number,2)
  SELF.AddHistoryField(?tra:Company_Name,4)
  SELF.AddHistoryField(?tra:Postcode,3)
  SELF.AddHistoryField(?tra:Address_Line1,5)
  SELF.AddHistoryField(?tra:Address_Line2,6)
  SELF.AddHistoryField(?tra:Address_Line3,7)
  SELF.AddHistoryField(?tra:Telephone_Number,8)
  SELF.AddHistoryField(?tra:Fax_Number,9)
  SELF.AddHistoryField(?tra:EmailAddress,10)
  SELF.AddHistoryField(?tra:Contact_Name,11)
  SELF.AddHistoryField(?tra:Labour_VAT_Code,15)
  SELF.AddHistoryField(?tra:Parts_VAT_Code,16)
  SELF.AddHistoryField(?tra:Retail_VAT_Code,17)
  SELF.AddHistoryField(?tra:Courier_Incoming,30)
  SELF.AddHistoryField(?tra:Courier_Outgoing,31)
  SELF.AddHistoryField(?tra:Courier_Cost,51)
  SELF.AddHistoryField(?tra:Loan_Stock_Type,50)
  SELF.AddHistoryField(?tra:Exchange_Stock_Type,49)
  SELF.AddHistoryField(?tra:Turnaround_Time,54)
  SELF.AddHistoryField(?tra:Stop_Account,23)
  SELF.AddHistoryField(?tra:Use_Contact_Name,32)
  SELF.AddHistoryField(?tra:Use_Delivery_Address,34)
  SELF.AddHistoryField(?tra:Use_Collection_Address,35)
  SELF.AddHistoryField(?tra:Use_Customer_Address,58)
  SELF.AddHistoryField(?tra:Invoice_Customer_Address,59)
  SELF.AddHistoryField(?tra:EmailRecipientList,93)
  SELF.AddHistoryField(?tra:EmailEndUser,94)
  SELF.AddHistoryField(?tra:AutoSendStatusEmails,92)
  SELF.AddHistoryField(?tra:Use_Sub_Accounts,21)
  SELF.AddHistoryField(?tra:Invoice_Sub_Accounts,22)
  SELF.AddHistoryField(?tra:ForceCommonFault,74)
  SELF.AddHistoryField(?tra:ForceOrderNumber,75)
  SELF.AddHistoryField(?tra:ExchangeAcc,64)
  SELF.AddHistoryField(?tra:InvoiceType,81)
  SELF.AddHistoryField(?tra:CheckChargeablePartsCost,100)
  SELF.AddHistoryField(?tra:MaxChargeablePartsCost,101)
  SELF.AddHistoryField(?tra:RefurbCharge,61)
  SELF.AddHistoryField(?tra:ChargeType,62)
  SELF.AddHistoryField(?tra:WarChargeType,63)
  SELF.AddHistoryField(?tra:ZeroChargeable,60)
  SELF.AddHistoryField(?tra:MultiInvoice,65)
  SELF.AddHistoryField(?tra:Allow_Loan,37)
  SELF.AddHistoryField(?tra:Allow_Cash_Sales,24)
  SELF.AddHistoryField(?tra:StopThirdParty,84)
  SELF.AddHistoryField(?tra:Allow_Exchange,38)
  SELF.AddHistoryField(?tra:MakeSplitJob,95)
  SELF.AddHistoryField(?tra:ShowRepairTypes,76)
  SELF.AddHistoryField(?tra:ExcludeBouncer,70)
  SELF.AddHistoryField(?tra:SplitJobStatus,96)
  SELF.AddHistoryField(?tra:SplitExchangeStatus,97)
  SELF.AddHistoryField(?tra:SplitCChargeType,98)
  SELF.AddHistoryField(?tra:SplitWChargeType,99)
  SELF.AddHistoryField(?tra:Force_Estimate,52)
  SELF.AddHistoryField(?tra:Estimate_If_Over,53)
  SELF.AddHistoryField(?tra:Retail_Payment_Type,56)
  SELF.AddHistoryField(?tra:Retail_Price_Structure,57)
  SELF.AddHistoryField(?tra:Print_Retail_Picking_Note,44)
  SELF.AddHistoryField(?tra:Price_Retail_Despatch,26)
  SELF.AddHistoryField(?tra:RetailZeroParts,71)
  SELF.AddHistoryField(?tra:EuroApplies,73)
  SELF.AddHistoryField(?tra:Password,55)
  SELF.AddHistoryField(?tra:WebPassword1,78)
  SELF.AddHistoryField(?tra:WebMemo,77)
  SELF.AddHistoryField(?tra:E1,85)
  SELF.AddHistoryField(?tra:E2,86)
  SELF.AddHistoryField(?tra:E3,87)
  SELF.AddHistoryField(?tra:Print_Despatch_Notes,42)
  SELF.AddHistoryField(?tra:IgnoreDespatch,47)
  SELF.AddHistoryField(?tra:Print_Despatch_Complete,27)
  SELF.AddHistoryField(?tra:Print_Despatch_Despatch,28)
  SELF.AddHistoryField(?tra:HideDespAdd,72)
  SELF.AddHistoryField(?tra:Despatch_Note_Per_Item,45)
  SELF.AddHistoryField(?TRA:Summary_Despatch_Notes:2,48)
  SELF.AddHistoryField(?tra:IndividualSummary,82)
  SELF.AddHistoryField(?tra:UseAlternateDespatchNote,102)
  SELF.AddHistoryField(?tra:Price_Despatch,25)
  SELF.AddHistoryField(?tra:InvoiceAtDespatch,80)
  SELF.AddHistoryField(?tra:Despatch_Paid_Jobs,41)
  SELF.AddHistoryField(?tra:Despatch_Invoiced_Jobs,40)
  SELF.AddHistoryField(?tra:Skip_Despatch,46)
  SELF.AddHistoryField(?tra:UseDespatchDetails,88)
  SELF.AddHistoryField(?tra:AltTelephoneNumber,89)
  SELF.AddHistoryField(?tra:AltFaxNumber,90)
  SELF.AddHistoryField(?tra:AltEmailAddress,91)
  SELF.AddHistoryField(?tra:UseCustDespAdd,36)
  SELF.AddHistoryField(?tra:UseTradeContactNo,83)
  SELF.AddUpdateFile(Access:TRADEACC)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:STATUS.Open
  Relate:STOCKTYP.Open
  Relate:TURNARND.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Access:COURIER.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRADEACC
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW23.Init(?List:2,Queue:Browse:1.ViewPosition,BRW23::View:Browse,Queue:Browse:1,Relate:TRAEMAIL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Clip(GETINI('WEBMASTER','StockLevels',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      ?StockAccessLevels{prop:Hide} = 0
  End !Clip(GETINI('WEBMASTER','StockLevels',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
  
  !Security Check
  If SecurityCheck('USE SUB ACCOUNTS')
      ?tra:Use_Sub_Accounts{prop:Disable} = 1
  Else !SecurityCheck('USE SUB ACCOUNTS')
      ?tra:Use_Sub_Accounts{prop:Disable} = 0
  End !SecurityCheck('USE SUB ACCOUNTS')
  
  If SecurityCheck('INVOICE SUB ACCOUNTS')
      ?tra:Invoice_Sub_Accounts{prop:Disable} = 1
  Else !SecurityCheck('INVOICE SUB ACCOUNTS')
      ?tra:Invoice_Sub_Accounts{prop:Disable} = 0
  End !SecurityCheck('INVOICE SUB ACCOUNTS')
  Do RecolourWindow
  ?List:2{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
    ?Tab3{PROP:TEXT} = 'By Account Number'
    ?Tab5{PROP:TEXT} = 'By Branch'
    ?Tab4{PROP:TEXT} = 'By Company Name'
    ?List{PROP:FORMAT} ='67L(2)|M~Account Number~@s15@#1#100L(2)|M~Branch~@s30@#2#120L(2)|M~Company Name~@s40@#3#'
  ! support for CPCS
  IF ?tra:Courier_Incoming{Prop:Tip} AND ~?LookupIncomingCourier{Prop:Tip}
     ?LookupIncomingCourier{Prop:Tip} = 'Select ' & ?tra:Courier_Incoming{Prop:Tip}
  END
  IF ?tra:Courier_Incoming{Prop:Msg} AND ~?LookupIncomingCourier{Prop:Msg}
     ?LookupIncomingCourier{Prop:Msg} = 'Select ' & ?tra:Courier_Incoming{Prop:Msg}
  END
  IF ?tra:Courier_Outgoing{Prop:Tip} AND ~?LookupOutgoingCourier{Prop:Tip}
     ?LookupOutgoingCourier{Prop:Tip} = 'Select ' & ?tra:Courier_Outgoing{Prop:Tip}
  END
  IF ?tra:Courier_Outgoing{Prop:Msg} AND ~?LookupOutgoingCourier{Prop:Msg}
     ?LookupOutgoingCourier{Prop:Msg} = 'Select ' & ?tra:Courier_Outgoing{Prop:Msg}
  END
  IF ?tra:SplitExchangeStatus{Prop:Tip} AND ~?LookupExchangeStatus{Prop:Tip}
     ?LookupExchangeStatus{Prop:Tip} = 'Select ' & ?tra:SplitExchangeStatus{Prop:Tip}
  END
  IF ?tra:SplitExchangeStatus{Prop:Msg} AND ~?LookupExchangeStatus{Prop:Msg}
     ?LookupExchangeStatus{Prop:Msg} = 'Select ' & ?tra:SplitExchangeStatus{Prop:Msg}
  END
  IF ?tra:SplitCChargeType{Prop:Tip} AND ~?LookupCChargeType{Prop:Tip}
     ?LookupCChargeType{Prop:Tip} = 'Select ' & ?tra:SplitCChargeType{Prop:Tip}
  END
  IF ?tra:SplitCChargeType{Prop:Msg} AND ~?LookupCChargeType{Prop:Msg}
     ?LookupCChargeType{Prop:Msg} = 'Select ' & ?tra:SplitCChargeType{Prop:Msg}
  END
  IF ?tra:SplitWChargeType{Prop:Tip} AND ~?LookupWChargeType{Prop:Tip}
     ?LookupWChargeType{Prop:Tip} = 'Select ' & ?tra:SplitWChargeType{Prop:Tip}
  END
  IF ?tra:SplitWChargeType{Prop:Msg} AND ~?LookupWChargeType{Prop:Msg}
     ?LookupWChargeType{Prop:Msg} = 'Select ' & ?tra:SplitWChargeType{Prop:Msg}
  END
  IF ?tra:SplitJobStatus{Prop:Tip} AND ~?LookupJobStatus{Prop:Tip}
     ?LookupJobStatus{Prop:Tip} = 'Select ' & ?tra:SplitJobStatus{Prop:Tip}
  END
  IF ?tra:SplitJobStatus{Prop:Msg} AND ~?LookupJobStatus{Prop:Msg}
     ?LookupJobStatus{Prop:Msg} = 'Select ' & ?tra:SplitJobStatus{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,sub:Main_Branch_Key)
  BRW2.AddRange(sub:Main_Account_Number,Relate:SUBTRACC,Relate:TRADEACC)
  BRW2.AddLocator(BRW2::Sort2:Locator)
  BRW2::Sort2:Locator.Init(?sub:Branch,sub:Branch,1,BRW2)
  BRW2.AddSortOrder(,sub:Main_Name_Key)
  BRW2.AddRange(sub:Main_Account_Number,Relate:SUBTRACC,Relate:TRADEACC)
  BRW2.AddLocator(BRW2::Sort1:Locator)
  BRW2::Sort1:Locator.Init(?SUB:Company_Name,sub:Company_Name,1,BRW2)
  BRW2.AddSortOrder(,sub:Main_Account_Key)
  BRW2.AddRange(sub:Main_Account_Number,Relate:SUBTRACC,Relate:TRADEACC)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(?SUB:Account_Number,sub:Account_Number,1,BRW2)
  BRW2.AddField(sub:Account_Number,BRW2.Q.sub:Account_Number)
  BRW2.AddField(sub:Branch,BRW2.Q.sub:Branch)
  BRW2.AddField(sub:Company_Name,BRW2.Q.sub:Company_Name)
  BRW2.AddField(sub:RecordNumber,BRW2.Q.sub:RecordNumber)
  BRW2.AddField(sub:Main_Account_Number,BRW2.Q.sub:Main_Account_Number)
  BRW23.Q &= Queue:Browse:1
  BRW23.AddSortOrder(,tre:RecipientKey)
  BRW23.AddLocator(BRW23::Sort0:Locator)
  BRW23::Sort0:Locator.Init(,tre:RefNumber,1,BRW23)
  BRW23.AddField(tre:RecipientType,BRW23.Q.tre:RecipientType)
  BRW23.AddField(tre:ContactName,BRW23.Q.tre:ContactName)
  BRW23.AddField(tre:RecordNumber,BRW23.Q.tre:RecordNumber)
  BRW23.AddField(tre:RefNumber,BRW23.Q.tre:RefNumber)
  IF ?tra:AutoSendStatusEmails{Prop:Checked} = True
    UNHIDE(?EmailGroup)
  END
  IF ?tra:AutoSendStatusEmails{Prop:Checked} = False
    HIDE(?EmailGroup)
  END
  IF ?tra:Use_Sub_Accounts{Prop:Checked} = True
    UNHIDE(?TRA:Invoice_Sub_Accounts)
    UNHIDE(?tra:Allow_Cash_Sales)
  END
  IF ?tra:Use_Sub_Accounts{Prop:Checked} = False
    HIDE(?TRA:Invoice_Sub_Accounts)
    HIDE(?tra:Allow_Cash_Sales)
  END
  IF ?tra:RefurbCharge{Prop:Checked} = True
    ENABLE(?Charge_Type_Group)
  END
  IF ?tra:RefurbCharge{Prop:Checked} = False
    DISABLE(?Charge_Type_Group)
  END
  IF ?tra:MakeSplitJob{Prop:Checked} = True
    ENABLE(?SplitJobGroup)
  END
  IF ?tra:MakeSplitJob{Prop:Checked} = False
    DISABLE(?SplitJobGroup)
  END
  IF ?tra:Print_Despatch_Notes{Prop:Checked} = True
    ENABLE(?TRA:Print_Despatch_Complete)
    ENABLE(?TRA:Print_Despatch_Despatch)
    ENABLE(?TRA:Price_Despatch)
    ENABLE(?tra:HideDespAdd)
    ENABLE(?tra:UseTradeContactNo)
  END
  IF ?tra:Print_Despatch_Notes{Prop:Checked} = False
    tra:Summary_Despatch_Notes = 'NO'
    tra:Despatch_Note_Per_Item = 'NO'
    tra:Print_Despatch_Despatch = 'NO'
    tra:Print_Despatch_Complete = 'NO'
    DISABLE(?TRA:Print_Despatch_Complete)
    DISABLE(?TRA:Print_Despatch_Despatch)
    DISABLE(?TRA:Price_Despatch)
    DISABLE(?tra:HideDespAdd)
    DISABLE(?tra:UseTradeContactNo)
  END
  IF ?tra:Print_Despatch_Despatch{Prop:Checked} = True
    ENABLE(?Despatch_note_type)
  END
  IF ?tra:Print_Despatch_Despatch{Prop:Checked} = False
    DISABLE(?Despatch_note_type)
  END
  IF ?TRA:Summary_Despatch_Notes:2{Prop:Checked} = True
    UNHIDE(?tra:IndividualSummary)
  END
  IF ?TRA:Summary_Despatch_Notes:2{Prop:Checked} = False
    HIDE(?tra:IndividualSummary)
  END
  IF ?tra:InvoiceAtDespatch{Prop:Checked} = True
    ENABLE(?tra:InvoiceType)
  END
  IF ?tra:InvoiceAtDespatch{Prop:Checked} = False
    DISABLE(?tra:InvoiceType)
  END
  IF ?tra:UseDespatchDetails{Prop:Checked} = True
    ENABLE(?AlternativeContactNumbers)
  END
  IF ?tra:UseDespatchDetails{Prop:Checked} = False
    DISABLE(?AlternativeContactNumbers)
  END
  BRW2.AskProcedure = 7
  BRW23.AskProcedure = 8
  FDCB18.Init(tra:Labour_VAT_Code,?tra:Labour_VAT_Code,Queue:FileDropCombo:1.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo:1,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB18.Q &= Queue:FileDropCombo:1
  FDCB18.AddSortOrder(vat:Vat_code_Key)
  FDCB18.AddField(vat:VAT_Code,FDCB18.Q.vat:VAT_Code)
  FDCB18.AddField(vat:VAT_Rate,FDCB18.Q.vat:VAT_Rate)
  FDCB18.AddUpdateField(vat:VAT_Code,tra:Labour_VAT_Code)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  FDCB15.Init(tra:Parts_VAT_Code,?tra:Parts_VAT_Code,Queue:FileDropCombo:5.ViewPosition,FDCB15::View:FileDropCombo,Queue:FileDropCombo:5,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB15.Q &= Queue:FileDropCombo:5
  FDCB15.AddSortOrder(vat:Vat_code_Key)
  FDCB15.AddField(vat:VAT_Code,FDCB15.Q.vat:VAT_Code)
  FDCB15.AddField(vat:VAT_Rate,FDCB15.Q.vat:VAT_Rate)
  ThisWindow.AddItem(FDCB15.WindowComponent)
  FDCB15.DefaultFill = 0
  FDCB24.Init(tra:Loan_Stock_Type,?tra:Loan_Stock_Type,Queue:FileDropCombo:6.ViewPosition,FDCB24::View:FileDropCombo,Queue:FileDropCombo:6,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB24.Q &= Queue:FileDropCombo:6
  FDCB24.AddSortOrder(stp:Stock_Type_Key)
  FDCB24.SetFilter('Upper(stp:use_loan) = ''YES''')
  FDCB24.AddField(stp:Stock_Type,FDCB24.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB24.WindowComponent)
  FDCB24.DefaultFill = 0
  FDCB32.Init(tra:Exchange_Stock_Type,?tra:Exchange_Stock_Type,Queue:FileDropCombo:7.ViewPosition,FDCB32::View:FileDropCombo,Queue:FileDropCombo:7,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB32.Q &= Queue:FileDropCombo:7
  FDCB32.AddSortOrder(stp:Use_Exchange_Key)
  FDCB32.SetFilter('Upper(stp:use_exchange) = ''YES''')
  FDCB32.AddField(stp:Stock_Type,FDCB32.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB32.WindowComponent)
  FDCB32.DefaultFill = 0
  FDCB33.Init(tra:Turnaround_Time,?tra:Turnaround_Time,Queue:FileDropCombo:8.ViewPosition,FDCB33::View:FileDropCombo,Queue:FileDropCombo:8,Relate:TURNARND,ThisWindow,GlobalErrors,0,1,0)
  FDCB33.Q &= Queue:FileDropCombo:8
  FDCB33.AddSortOrder(tur:Turnaround_Time_Key)
  FDCB33.AddField(tur:Turnaround_Time,FDCB33.Q.tur:Turnaround_Time)
  ThisWindow.AddItem(FDCB33.WindowComponent)
  FDCB33.DefaultFill = 0
  FDCB36.Init(tra:Retail_VAT_Code,?tra:Retail_VAT_Code,Queue:FileDropCombo:10.ViewPosition,FDCB36::View:FileDropCombo,Queue:FileDropCombo:10,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB36.Q &= Queue:FileDropCombo:10
  FDCB36.AddSortOrder(vat:Vat_code_Key)
  FDCB36.AddField(vat:VAT_Code,FDCB36.Q.vat:VAT_Code)
  FDCB36.AddField(vat:VAT_Rate,FDCB36.Q.vat:VAT_Rate)
  ThisWindow.AddItem(FDCB36.WindowComponent)
  FDCB36.DefaultFill = 0
  FDCB5.Init(tra:ChargeType,?tra:ChargeType,Queue:FileDropCombo:11.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:11,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:11
  FDCB5.AddSortOrder(cha:Warranty_Key)
  FDCB5.SetFilter('Upper(cha:warranty) = ''NO''')
  FDCB5.AddField(cha:Charge_Type,FDCB5.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB16.Init(tra:WarChargeType,?tra:WarChargeType,Queue:FileDropCombo:12.ViewPosition,FDCB16::View:FileDropCombo,Queue:FileDropCombo:12,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB16.Q &= Queue:FileDropCombo:12
  FDCB16.AddSortOrder(cha:Warranty_Key)
  FDCB16.SetFilter('Upper(cha:warranty) = ''YES''')
  FDCB16.AddField(cha:Charge_Type,FDCB16.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB16.WindowComponent)
  FDCB16.DefaultFill = 0
  BRW2.AddToolbarTarget(Toolbar)
  BRW23.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  brw2.popup.kill
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:STATUS.Close
    Relate:STOCKTYP.Close
    Relate:TURNARND.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateTRADEACC',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    tra:Invoice_Sub_Accounts = 'NO'
    tra:Stop_Account = 'NO'
    tra:Allow_Cash_Sales = 'NO'
    tra:Use_Sub_Accounts = 'NO'
    tra:Price_Despatch = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If Field() = ?Insert Or Field() = ?Change Or Field() = ?Delete
    If tra:use_sub_accounts <> 'YES'
        do_update# = 0
    Else!If tra:use_sub_account <> 'YES'
        glo:Select12   = tra:account_number
        If tra:invoice_sub_accounts = 'YES'
            glo:select2  = 'FINANCIAL'
        End!If tra:invoice_sub_accounts = 'YES'
    End!If tra:use_sub_account <> 'YES'
  
  End !If number <> 3
  
  If do_update# = 1
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickCouriers
      PickCouriers
      PickJobStatus
      PickExchangeStatus
      PickChargeableChargeTypes
      PickWarrantyChargeTypes
      UpdateSUBTRACC
      UpdateTradeRecipients
    END
    ReturnValue = GlobalResponse
  END
     glo:Select1 = ''
     glo:select12 = ''
  End !If do_update# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?tra:Use_Sub_Accounts
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Use_Sub_Accounts, Accepted)
      If tra:use_sub_accounts <> use_sub_account_temp
          error# = 0
          If tra:account_number = ''
              Case MessageEx('You have not entered an Account Number.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
              tra:use_sub_accounts = use_sub_account_temp
          End!If tra:account_number = ''
          If tra:company_name = '' And error# = 0
              Case MessageEx('You have not entered a Company Name.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
              tra:use_sub_accounts = use_sub_account_temp
          End!If tra:company_name = '' And error# = 0
          If error# = 0
              If tra:use_sub_accounts = 'YES'
                  Case MessageEx('Sub Accounts are used when invoiced are sent to one location and units to another.<13,10><13,10>Are you sure you want to use Sub Accounts?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          use_sub_account_temp = tra:use_sub_accounts
                          setcursor(cursor:wait)
                          
                          save_sub_id = access:subtracc.savefile()
                          access:subtracc.clearkey(sub:main_account_key)
                          sub:main_account_number = tra:account_number
                          set(sub:main_account_key,sub:main_account_key)
                          loop
                              if access:subtracc.next()
                                 break
                              end !if
                              if sub:main_account_number <> tra:account_number      |
                                  then break.  ! end if
                              Delete(subtracc)
                          end !loop
                          access:subtracc.restorefile(save_sub_id)
                          setcursor()
      
                      Of 2 ! &No Button
                          tra:use_sub_accounts = use_sub_account_temp
                  End!Case MessageEx
              Else
                  found# = 0
      
                  setcursor(cursor:wait)
                  save_sub_id = access:subtracc.savefile()
                  access:subtracc.clearkey(sub:main_account_key)
                  sub:main_account_number = tra:account_number
                  set(sub:main_account_key,sub:main_account_key)
                  loop
                      if access:subtracc.next()
                         break
                      end !if
                      if sub:main_account_number <> tra:account_number      |
                          then break.  ! end if
                      found# = 1
                      Break
                  end !loop
                  access:subtracc.restorefile(save_sub_id)
                  setcursor()
      
                  delete_all# = 0
                  If found# = 1
                      Case MessageEx('You are about to delete all the Sub Accounts attached to this Trade Account.<13,10><13,10>If you continue all the Sub Account information will be lost.<13,10>                Are you sure?','ServiceBase 2000',|
                                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              Case MessageEx('Are you sure you want to delete all the Sub Account information for this Trade Account?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                      delete_all# = 1
                                      use_sub_account_temp = tra:use_sub_accounts
          
                                  Of 2 ! &No Button
                                      delete_all# = 0
                                      tra:use_sub_accounts = use_sub_account_temp
          
                              End!Case MessageEx
                          Of 2 ! &No Button
                              tra:use_sub_accounts = use_sub_account_temp
                              delete_all# = 0
                      End!Case MessageEx
                  Else
                      use_sub_account_temp    = tra:use_sub_accounts
                      delete_all# = 1
                  End
                  If delete_all# = 1 and ThisWindow.Request <> Insertrecord
                      Do Copy_Main_Account
                  End
              End
          End!End!If error# = 0
      End
      Do Hide_fields
      brw2.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Use_Sub_Accounts, Accepted)
    OF ?Button6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      glo:Select11 = tra:account_number
      glo:Select12 = 'TRADE'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      ok_pressed_temp = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      ok_pressed_temp = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tra:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Account_Number, Accepted)
      If tra:account_number <> account_number_temp
          If account_number_temp <> ''
              Case MessageEx('Are you sure you want to change the Account Number?','ServiceBase 2000','Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      setcursor(cursor:wait)
                      save_sub_id = access:subtracc.savefile()
                      access:subtracc.clearkey(sub:main_account_key)
                      sub:main_account_number = account_number_temp
                      set(sub:main_account_key,sub:main_account_key)
                      loop
                          if access:subtracc.next()
                             break
                          end !if
                          if sub:main_account_number <> account_number_temp      |
                              then break.  ! end if
                          pos = Position(sub:main_account_key)
                          If tra:use_sub_accounts <> 'YES'
                              sub:account_number  = tra:account_number
                          End!If tra:use_sub_accounts <> 'YES'
                          sub:main_account_number = tra:account_number
                          access:subtracc.update()
                          reset(sub:main_account_key,pos)
                      end !loop
                      access:subtracc.restorefile(save_sub_id)
                      setcursor()
                      account_number_temp = tra:account_number
                  Of 2 ! &No Button
                      tra:account_number = account_number_temp
              End!Case MessageEx
          End!If account_number_temp <> ''
          account_number_temp = tra:account_number
      End!If tra:account_number <> account_number_temp
      brw2.resetsort(1)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Account_Number, Accepted)
    OF ?tra:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Postcode, Accepted)
      If ~quickwindow{prop:acceptall}
          postcode_routine(tra:postcode,tra:address_line1,tra:address_line2,tra:address_line3)
          Select(?tra:address_line1,1)
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Postcode, Accepted)
    OF ?Clear_Address
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
      Case MessageEx('Are you sure you want to clear the Address?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              tra:postcode      = ''
              tra:company_name  = ''
              tra:address_line1 = ''
              tra:address_line2 = ''
              tra:address_line3 = ''
              Display()
              Select(?tra:postcode)
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
    OF ?tra:Telephone_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Telephone_Number, Accepted)
      
          temp_string = Clip(left(tra:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          tra:Telephone_Number    = temp_string
          Display(?tra:Telephone_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Telephone_Number, Accepted)
    OF ?tra:Fax_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Fax_Number, Accepted)
      
          temp_string = Clip(left(tra:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          tra:Fax_Number    = temp_string
          Display(?tra:Fax_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Fax_Number, Accepted)
    OF ?tra:Labour_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Labour_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Labour_VAT_Code, Accepted)
    OF ?tra:Parts_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Parts_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Parts_VAT_Code, Accepted)
    OF ?tra:Retail_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Retail_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Retail_VAT_Code, Accepted)
    OF ?tra:Courier_Incoming
      IF tra:Courier_Incoming OR ?tra:Courier_Incoming{Prop:Req}
        cou:Courier = tra:Courier_Incoming
        !Save Lookup Field Incase Of error
        look:tra:Courier_Incoming        = tra:Courier_Incoming
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tra:Courier_Incoming = cou:Courier
          ELSE
            !Restore Lookup On Error
            tra:Courier_Incoming = look:tra:Courier_Incoming
            SELECT(?tra:Courier_Incoming)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupIncomingCourier
      ThisWindow.Update
      cou:Courier = tra:Courier_Incoming
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tra:Courier_Incoming = cou:Courier
          Select(?+1)
      ELSE
          Select(?tra:Courier_Incoming)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:Courier_Incoming)
    OF ?tra:Courier_Outgoing
      IF tra:Courier_Outgoing OR ?tra:Courier_Outgoing{Prop:Req}
        cou:Courier = tra:Courier_Outgoing
        !Save Lookup Field Incase Of error
        look:tra:Courier_Outgoing        = tra:Courier_Outgoing
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tra:Courier_Outgoing = cou:Courier
          ELSE
            !Restore Lookup On Error
            tra:Courier_Outgoing = look:tra:Courier_Outgoing
            SELECT(?tra:Courier_Outgoing)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupOutgoingCourier
      ThisWindow.Update
      cou:Courier = tra:Courier_Outgoing
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tra:Courier_Outgoing = cou:Courier
          Select(?+1)
      ELSE
          Select(?tra:Courier_Outgoing)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:Courier_Outgoing)
    OF ?tra:AutoSendStatusEmails
      IF ?tra:AutoSendStatusEmails{Prop:Checked} = True
        UNHIDE(?EmailGroup)
      END
      IF ?tra:AutoSendStatusEmails{Prop:Checked} = False
        HIDE(?EmailGroup)
      END
      ThisWindow.Reset
    OF ?tra:Use_Sub_Accounts
      IF ?tra:Use_Sub_Accounts{Prop:Checked} = True
        UNHIDE(?TRA:Invoice_Sub_Accounts)
        UNHIDE(?tra:Allow_Cash_Sales)
      END
      IF ?tra:Use_Sub_Accounts{Prop:Checked} = False
        HIDE(?TRA:Invoice_Sub_Accounts)
        HIDE(?tra:Allow_Cash_Sales)
      END
      ThisWindow.Reset
    OF ?tra:RefurbCharge
      IF ?tra:RefurbCharge{Prop:Checked} = True
        ENABLE(?Charge_Type_Group)
      END
      IF ?tra:RefurbCharge{Prop:Checked} = False
        DISABLE(?Charge_Type_Group)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:RefurbCharge, Accepted)
      ! Start Change 1895 BE(14/05/03)
      ! Commented out because it causes screen to flash
      ! and as far as I can see is superfluous after initial Window Constructor
      !thismakeover.setwindow(win:form)
      ! End Change 1895 BE(14/05/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:RefurbCharge, Accepted)
    OF ?tra:MakeSplitJob
      IF ?tra:MakeSplitJob{Prop:Checked} = True
        ENABLE(?SplitJobGroup)
      END
      IF ?tra:MakeSplitJob{Prop:Checked} = False
        DISABLE(?SplitJobGroup)
      END
      ThisWindow.Reset
    OF ?tra:SplitJobStatus
      IF tra:SplitJobStatus OR ?tra:SplitJobStatus{Prop:Req}
        sts:Status = tra:SplitJobStatus
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tra:SplitJobStatus        = tra:SplitJobStatus
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tra:SplitJobStatus = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tra:SplitJobStatus = look:tra:SplitJobStatus
            SELECT(?tra:SplitJobStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupJobStatus
      ThisWindow.Update
      sts:Status = tra:SplitJobStatus
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tra:SplitJobStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?tra:SplitJobStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:SplitJobStatus)
    OF ?tra:SplitExchangeStatus
      IF tra:SplitExchangeStatus OR ?tra:SplitExchangeStatus{Prop:Req}
        sts:Status = tra:SplitExchangeStatus
        sts:Exchange = 'YES'
        !Save Lookup Field Incase Of error
        look:tra:SplitExchangeStatus        = tra:SplitExchangeStatus
        IF Access:STATUS.TryFetch(sts:ExchangeKey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            tra:SplitExchangeStatus = sts:Status
          ELSE
            CLEAR(sts:Exchange)
            !Restore Lookup On Error
            tra:SplitExchangeStatus = look:tra:SplitExchangeStatus
            SELECT(?tra:SplitExchangeStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupExchangeStatus
      ThisWindow.Update
      sts:Status = tra:SplitExchangeStatus
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          tra:SplitExchangeStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?tra:SplitExchangeStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:SplitExchangeStatus)
    OF ?tra:SplitCChargeType
      IF tra:SplitCChargeType OR ?tra:SplitCChargeType{Prop:Req}
        cha:Charge_Type = tra:SplitCChargeType
        cha:Warranty = 'NO'
        !Save Lookup Field Incase Of error
        look:tra:SplitCChargeType        = tra:SplitCChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            tra:SplitCChargeType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            tra:SplitCChargeType = look:tra:SplitCChargeType
            SELECT(?tra:SplitCChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupCChargeType
      ThisWindow.Update
      cha:Charge_Type = tra:SplitCChargeType
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          tra:SplitCChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?tra:SplitCChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:SplitCChargeType)
    OF ?tra:SplitWChargeType
      IF tra:SplitWChargeType OR ?tra:SplitWChargeType{Prop:Req}
        cha:Charge_Type = tra:SplitWChargeType
        cha:Warranty = 'YES'
        !Save Lookup Field Incase Of error
        look:tra:SplitWChargeType        = tra:SplitWChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            tra:SplitWChargeType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            tra:SplitWChargeType = look:tra:SplitWChargeType
            SELECT(?tra:SplitWChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWChargeType
      ThisWindow.Update
      cha:Charge_Type = tra:SplitWChargeType
      
      IF SELF.RUN(6,Selectrecord)  = RequestCompleted
          tra:SplitWChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?tra:SplitWChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tra:SplitWChargeType)
    OF ?tra:Print_Despatch_Notes
      IF ?tra:Print_Despatch_Notes{Prop:Checked} = True
        ENABLE(?TRA:Print_Despatch_Complete)
        ENABLE(?TRA:Print_Despatch_Despatch)
        ENABLE(?TRA:Price_Despatch)
        ENABLE(?tra:HideDespAdd)
        ENABLE(?tra:UseTradeContactNo)
      END
      IF ?tra:Print_Despatch_Notes{Prop:Checked} = False
        tra:Summary_Despatch_Notes = 'NO'
        tra:Despatch_Note_Per_Item = 'NO'
        tra:Print_Despatch_Despatch = 'NO'
        tra:Print_Despatch_Complete = 'NO'
        DISABLE(?TRA:Print_Despatch_Complete)
        DISABLE(?TRA:Print_Despatch_Despatch)
        DISABLE(?TRA:Price_Despatch)
        DISABLE(?tra:HideDespAdd)
        DISABLE(?tra:UseTradeContactNo)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Print_Despatch_Notes, Accepted)
      Post(Event:Accepted,?tra:Print_Despatch_Despatch)
      !thismakeover.setwindow(win:form)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Print_Despatch_Notes, Accepted)
    OF ?tra:Print_Despatch_Despatch
      IF ?tra:Print_Despatch_Despatch{Prop:Checked} = True
        ENABLE(?Despatch_note_type)
      END
      IF ?tra:Print_Despatch_Despatch{Prop:Checked} = False
        DISABLE(?Despatch_note_type)
      END
      ThisWindow.Reset
    OF ?TRA:Summary_Despatch_Notes:2
      IF ?TRA:Summary_Despatch_Notes:2{Prop:Checked} = True
        UNHIDE(?tra:IndividualSummary)
      END
      IF ?TRA:Summary_Despatch_Notes:2{Prop:Checked} = False
        HIDE(?tra:IndividualSummary)
      END
      ThisWindow.Reset
    OF ?tra:Price_Despatch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Price_Despatch, Accepted)
      If tra:price_despatch = 'NO'
          setcursor(cursor:wait)
          save_tch_id = access:trachar.savefile()
          access:trachar.clearkey(tch:account_number_key)
          tch:account_number = tra:account_number
          set(tch:account_number_key,tch:account_number_key)
          loop
              if access:trachar.next()
                 break
              end !if
              if tch:account_number <> tra:account_number |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              Delete(trachar)
          end !loop
          access:trachar.restorefile(save_tch_id)
          setcursor()
      End!If tra:price_despatch_notes = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Price_Despatch, Accepted)
    OF ?tra:InvoiceAtDespatch
      IF ?tra:InvoiceAtDespatch{Prop:Checked} = True
        ENABLE(?tra:InvoiceType)
      END
      IF ?tra:InvoiceAtDespatch{Prop:Checked} = False
        DISABLE(?tra:InvoiceType)
      END
      ThisWindow.Reset
    OF ?tra:UseDespatchDetails
      IF ?tra:UseDespatchDetails{Prop:Checked} = True
        ENABLE(?AlternativeContactNumbers)
      END
      IF ?tra:UseDespatchDetails{Prop:Checked} = False
        DISABLE(?AlternativeContactNumbers)
      END
      ThisWindow.Reset
    OF ?tra:AltTelephoneNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:AltTelephoneNumber, Accepted)
      
          temp_string = Clip(left(tra:AltTelephoneNumber))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          tra:AltTelephoneNumber    = temp_string
          Display(?tra:AltTelephoneNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:AltTelephoneNumber, Accepted)
    OF ?tra:AltFaxNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:AltFaxNumber, Accepted)
      
          temp_string = Clip(left(tra:AltFaxNumber))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          tra:AltFaxNumber    = temp_string
          Display(?tra:AltFaxNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:AltFaxNumber, Accepted)
    OF ?Button6
      ThisWindow.Update
      START(Insert_Stardard_Charges, 25000)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      glo:Select1 = ''
      glo:Select2 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?FaultCodes
      ThisWindow.Update
      BrowseTradeFaultCodes
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If tra:Print_Despatch_Notes = 'YES'
      If tra:Print_Despatch_Despatch = 'YES'
          If tra:Despatch_Note_Per_Item <> 'YES' And tra:Summary_Despatch_Notes <> 'YES'
              Case MessageEx('You have selected to print a Despatch Note at Despatch but have not selected a Despatch Note Type!','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Select(?tra:Print_Despatch_Notes)
              Cycle
          End !If tra:Despatch_Note_Per_Item <> 'YES' And tra:Summary_Despatch_Notes <> 'YES'
      End !If tra:Print_Despatch_Despatch = 'YES'
  End !tra:Print_Despatch_Notes = 'YES'
  
  
  !If New Account
  If tra:use_sub_accounts = 'NO'
      If ThisWindow.Request = Insertrecord
  
          found# = 0
  
  !            setcursor(cursor:wait)
  !            save_sub_id = access:subtracc.savefile()
  !            access:subtracc.clearkey(sub:main_account_key)
  !            sub:main_account_number = tra:account_number
  !            set(sub:main_account_key,sub:main_account_key)
  !            loop
  !                if access:subtracc.next()
  !                   break
  !                end !if
  !                if sub:main_account_number <> tra:account_number      |
  !                    then break.  ! end if
  !                found# = 1
  !                Break
  !            end !loop
  !            access:subtracc.restorefile(save_sub_id)
  !            setcursor()
  !
  !            If found# = 1
              Do copy_main_account
  !            End
      End !If ThisWindow.Request = Insertrecord
  Else !If tra:use_sub_accounts = 'NO'
      found# = 0
      setcursor(cursor:wait)
      save_sub_id = access:subtracc.savefile()
      access:subtracc.clearkey(sub:main_account_key)
      sub:main_account_number = tra:account_number
      set(sub:main_account_key,sub:main_account_key)
      loop
          if access:subtracc.next()
             break
          end !if
          if sub:main_account_number <> tra:account_number      |
              then break.  ! end if
          found# = 1
          Break
      end !loop
      access:subtracc.restorefile(save_sub_id)
      setcursor()
      If found# = 0
          Case MessageEx('You have not entered a Sub Account.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Select(?tra:account_number,1)
          Cycle
      End
  End !If tra:use_sub_accounts = 'NO'
  
  
  
  
  
  
  If tra:company_name <> company_name_temp Or tra:account_number <> account_number_temp
      If tra:use_sub_accounts <> 'YES'
          Do Copy_Main_Account
      End!If tra:use_sub_accounts <> 'YES'
  End!If tra:company_name <> company_name_temp Or tra:account_number <> account_number_temp
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateTRADEACC')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tra:Courier_Incoming
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Courier_Incoming, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Incoming Courier')
             Post(Event:Accepted,?LookupIncomingCourier)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupIncomingCourier)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Courier_Incoming, AlertKey)
    END
  OF ?tra:Courier_Outgoing
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Courier_Outgoing, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Outgoing Courier')
             Post(Event:Accepted,?LookupOutgoingCourier)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupOutgoingCourier)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tra:Courier_Outgoing, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
      CASE CHOICE(?CurrentTab)
        OF 1
          ?List{PROP:FORMAT} ='67L(2)|M~Account Number~@s15@#1#100L(2)|M~Branch~@s30@#2#120L(2)|M~Company Name~@s40@#3#'
          ?Tab3{PROP:TEXT} = 'By Account Number'
        OF 2
          ?List{PROP:FORMAT} ='100L(2)|M~Branch~@s30@#2#67L(2)|M~Account Number~@s15@#1#120L(2)|M~Company Name~@s40@#3#'
          ?Tab5{PROP:TEXT} = 'By Branch'
        OF 3
          ?List{PROP:FORMAT} ='120L(2)|M~Company Name~@s40@#3#67L(2)|M~Account Number~@s15@#1#100L(2)|M~Branch~@s30@#2#'
          ?Tab4{PROP:TEXT} = 'By Company Name'
      END
    END
  ReturnValue = PARENT.TakeNewSelection()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sub:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Account_Number, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Account_Number, Selected)
    OF ?sub:Branch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Branch, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Branch, Selected)
    OF ?sub:Company_Name
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Company_Name, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Company_Name, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      If ok_pressed_temp = 0
          setcursor(cursor:wait)
          found# = 0
          save_sub_id = access:subtracc.savefile()
          access:subtracc.clearkey(sub:main_account_key)
          sub:main_account_number = tra:account_number
          set(sub:main_account_key,sub:main_account_key)
          loop
              if access:subtracc.next()
                 break
              end !if
              if sub:main_account_number <> tra:account_number      |
                  then break.  ! end if
              found# = 1
              Break
          end !loop
          access:subtracc.restorefile(save_sub_id)
          setcursor()
          If found# = 0
              If tra:use_sub_accounts = 'YES'
                  Case MessageEx('You must either insert at least one Sub Account or untick ''Use Sub Accounts'' before you can cancel.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Cycle
              Else!If tra:use_sub_accounts = 'YES'
                  If ThisWindow.Request <> Insertrecord
                      Do Copy_Main_Account
                  End
              End!If tra:use_sub_accounts = 'YES'
          End!If found# = 0
      End!If ok_pressed_temp = 0
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      
          temp_string = Clip(left(tra:WarChargeType))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          tra:WarChargeType    = temp_string
          Display(?tra:WarChargeType)
      Do Hide_Fields
      Do Fill_Rates
      Post(Event:accepted,?tra:print_despatch_notes)
      Post(Event:accepted,?tra:RefurbCharge)
      Select(?tra:account_number)
      
      !Use Sub Account
      use_sub_account_temp    = tra:use_sub_accounts
      account_number_temp     = tra:account_number
      company_name_temp       = tra:company_name
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW2.ResetSort(1)
      FDCB15.ResetQueue(1)
      FDCB18.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW2.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW23.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW23.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

