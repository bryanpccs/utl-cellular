

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01009.INC'),ONCE        !Local module procedure declarations
                     END








Retail_Despatch_Note PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
Back_Title           BYTE
hide_temp            BYTE
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
vat_rate_parts_temp  REAL
vat_total_temp       REAL
total_temp           REAL
vat_rate_temp        REAL
Delivery_Name_Temp   STRING(30)
Delivery_Address_Line_1_Temp STRING(60)
save_res_id          USHORT,AUTO
Invoice_Name_temp    STRING(30)
Invoice_address_Line_1 STRING(60)
items_total_temp     REAL
vat_temp             REAL
item_cost_temp       REAL
line_cost_temp       REAL
despatch_note_number_temp STRING(10)
courier_cost_temp    REAL
sub_total_temp       REAL
line_cost_ex_temp    REAL
PartsList            QUEUE,PRE(tmpque)
RecordNumber         LONG,NAME('RecordNumber')
OrderNumber          STRING(30),NAME('OrderNumber')
PartNumber           STRING(30),NAME('PartNumber')
Quantity             LONG,NAME('Quantity')
                     END
tmp:DefaultEmailAddress STRING(255)
tmp:CountLines       LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(RETDESNO)
                     END
report               REPORT,AT(396,4573,7521,4260),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(354,760,7521,9469),USE(?unnamed)
                         STRING('Date Printed:'),AT(5010,979),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6),AT(6000,979),USE(ReportRunDate),TRN,LEFT,FONT('Arial',8,,FONT:bold)
                         STRING('Tel:'),AT(208,2240),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('Despatch Number:'),AT(5000,156),USE(?string22:3),TRN,FONT(,8,,)
                         STRING(@s10),AT(5990,156),USE(despatch_note_number_temp),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s8),AT(5990,365),USE(ret:Ref_Number),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Sale Number:'),AT(5000,365),USE(?string22:10),TRN,FONT(,8,,)
                         STRING('Account No'),AT(208,2917),USE(?string22:6),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(208,3125),USE(ret:Account_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Contact Name'),AT(3125,2917),USE(?string22:11),TRN,FONT(,8,,FONT:bold)
                         STRING('Purchase Order No'),AT(1667,2917),USE(?string22:7),TRN,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(3125,3125),USE(ret:Contact_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s20),AT(1667,3125),USE(ret:Purchase_Order_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(260,8490,2448,781),USE(ret:Delivery_Text),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(208,1458),USE(ret:Company_Name_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1458),USE(ret:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(208,1615,3438,208),USE(Delivery_Address_Line_1_Temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(4063,1615,3281,208),USE(Invoice_address_Line_1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1771),USE(ret:Address_Line2_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1771),USE(ret:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(500,2240),USE(ret:Telephone_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(4063,2240),USE(?string22:9),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2240),USE(ret:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(208,2396),USE(?string22:5),TRN,FONT(,8,,)
                         STRING(@s10),AT(208,2083),USE(ret:Postcode_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s10),AT(4063,2083),USE(ret:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1927),USE(ret:Address_Line3_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1927),USE(ret:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(500,2396),USE(ret:Fax_Number_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(4063,2396),USE(?string22:8),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2396),USE(ret:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break2                 BREAK(endofreport),USE(?unnamed:2)
detail2                  DETAIL,AT(,,,208),USE(?unnamed:5)
                           STRING(@n10.2),AT(5833,0),USE(res:Item_Cost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n10.2b),AT(6719,0),USE(line_cost_ex_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s15),AT(156,0),USE(res:Purchase_Order_Number),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s6),AT(1354,0),USE(tmpque:Quantity),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s25),AT(2240,0),USE(res:Part_Number),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s25),AT(4010,0),USE(res:Description),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                         END
BackTitle                DETAIL,AT(,,,531),USE(?BackTitle)
                           LINE,AT(156,52,7188,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING('BACK ORDER PARTS'),AT(104,104),USE(?string44:5),TRN,FONT(,9,,FONT:bold)
                         END
Continued                DETAIL,PAGEBEFORE(-1),AT(,,,323),USE(?unnamed:3)
                           STRING('Continued...'),AT(104,52),USE(?ContinuedText),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,9167,7521,2000),USE(?unnamed:4)
                         STRING('Courier Cost:'),AT(4948,104),USE(?Courier_Cost),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6563,104),USE(courier_cost_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Total Line Cost (Ex VAT)'),AT(4948,313),USE(?Sub_Total),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6542,313),USE(sub_total_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('V.A.T.:'),AT(4948,521),USE(?Vat),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6542,521),USE(vat_total_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(6354,729,1042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total (Inc VAT):'),AT(4948,781),USE(?Total),TRN,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6542,781),USE(total_temp),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(104,1146,7292,781),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(354,479,7521,11198),USE(?String70:2)
                         IMAGE('rinvdet.gif'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('DESPATCH NOTE'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,958),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,958),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1115),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1115),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1250,3844,198),USE(tmp:DefaultEmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1271),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Qty'),AT(1635,3854),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('(Ex. V.A.T.)'),AT(6906,3927),USE(?Line_Cost:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Part Number'),AT(2281,3854),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(4052,3854),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6927,3844),USE(?line_cost:1),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Order No'),AT(198,3854),USE(?string24:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost'),AT(5927,3854),USE(?item_Cost),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('DELIVERY ADDRESS'),AT(208,1490),USE(?string44:2),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(4063,1490),USE(?string44:3),TRN,FONT(,9,,FONT:bold)
                         STRING('String 70'),AT(198,3594,5833,208),USE(?String70),TRN,FONT(,,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4740,8490),USE(?Charge_Details),TRN,FONT(,9,,FONT:bold)
                         STRING('(Despatched Items Only)'),AT(5938,8510),USE(?despatched_items_only),TRN,FONT(,7,,FONT:bold)
                         STRING('DELIVERY TEXT'),AT(104,8490),USE(?Charge_Details:2),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Retail_Despatch_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RETDESNO.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:INVOICE.Open
  Relate:STANTEXT.Open
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:RETSALES.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(RETDESNO)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(RETDESNO,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(rdn:Despatch_Number_Key)
      Process:View{Prop:Filter} = |
      'rdn:Despatch_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
    Clear(PartsList)
    Free(PartsList)

    setcursor(cursor:wait)
    save_res_id = access:retstock.savefile()
    access:retstock.clearkey(res:despatched_only_key)
    res:ref_number = ret:ref_number
    res:despatched = 'YES'
    set(res:despatched_only_key,res:despatched_only_key)
    loop
        if access:retstock.next()
           break
        end !if
        if res:ref_number <> ret:ref_number      |
        or res:despatched <> 'YES'      |
            then break.  ! end if
        tmp:RecordsCount += 1
        If hide_temp = 1
            Settarget(report)
            Hide(?res:item_cost)
            Hide(?line_cost:1)
            Hide(?line_cost:2)
            HIde(?line_cost_ex_temp)
            Settarget()
        End!If hide_temp = 1
        tmpque:OrderNumber  = res:Purchase_Order_Number
        tmpque:PartNumber   = res:Part_number
        Get(PartsList,'OrderNumber,PartNumber')
        If Error()
            tmpque:RecordNumber = res:Record_Number
            tmpque:OrderNumber  = res:Purchase_Order_Number
            tmpque:PartNumber   = res:Part_Number
            tmpque:Quantity     = res:Quantity
            Add(PartsList)
        Else !If Error()
            tmpque:Quantity     += res:Quantity
            Put(PartsList)
        End !If Error()
        
    end !loop
    access:retstock.restorefile(save_res_id)
    setcursor()

    tmp:CountLines = 0
    Sort(PartsList,tmpque:OrderNumber,tmpque:PartNumber)
    Loop x# = 1 To Records(PartsList)
        Get(PartsList,x#)
        Access:RETSTOCK.ClearKey(res:Record_Number_Key)
        res:Record_Number = tmpque:RecordNumber
        If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
            line_cost_ex_temp = Round(res:item_cost * tmpque:quantity,.01)
            sub_total_temp += line_cost_ex_temp
            tmp:CountLines += 1
            If tmp:CountLines > 20
                SetTarget(Report)
                ?ContinuedText{prop:Text} = 'Despatched Parts Continued...'
                SetTarget()
                Print(rpt:Continued)
                tmp:CountLines = 1
            End !If tmp:CountLines > 15
            Print(rpt:Detail2)
            
        End !If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
    End !x# = 1 To Records(PartsList)

    vat_total_temp = Round((ret:courier_cost + sub_total_temp) * vat_rate_temp/100,.01)
    total_temp  = ret:courier_cost + Round(vat_total_temp,.01) + Round(sub_total_temp,.01)

        Clear(PartsList)
        Free(PartsList)
        first# = 1
        save_res_id = access:retstock.savefile()
        access:retstock.clearkey(res:despatched_key)
        res:ref_number           = ret:ref_number
        res:despatched           = 'PEN'
        set(res:despatched_key,res:despatched_key)
        loop
            if access:retstock.next()
               break
            end !if
            if res:ref_number           <> ret:ref_number      |
            or res:despatched           <> 'PEN'      |
                then break.  ! end if
            Settarget(report)
            Hide(?res:item_cost)
            Hide(?line_cost_ex_temp)
            Settarget()
            vat_temp     = ''
            line_cost_temp  = ''
            item_cost_temp  = ''
            tmpque:OrderNumber  = res:Purchase_Order_Number
            tmpque:PartNumber   = res:Part_number
            Get(PartsList,'OrderNumber,PartNumber')
            If Error()
                tmpque:RecordNumber = res:Record_Number
                tmpque:OrderNumber  = res:Purchase_Order_Number
                tmpque:PartNumber   = res:Part_Number
                tmpque:Quantity     = res:Quantity
                Add(PartsList)
            Else !If Error()
                tmpque:Quantity     += res:Quantity
                Put(PartsList)
            End !If Error()
            
        end !loop
        access:retstock.restorefile(save_res_id)
        
        If Records(PartsList)
            Print(rpt:backtitle)
            If tmp:CountLines > 16
                tmp:CountLines = 3
            Else
                tmp:CountLines += 3
            End !If tmp:CountLines > 17
            
        End
        
        Sort(PartsList,tmpque:OrderNumber,tmpque:PartNumber)
        Loop x# = 1 To Records(PartsList)
            Get(PartsList,x#)
            Access:RETSTOCK.ClearKey(res:Record_Number_Key)
            res:Record_Number = tmpque:RecordNumber
            If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
                tmp:CountLines += 1
                If tmp:CountLines > 20
                    SetTarget(Report)
                    ?ContinuedText{prop:Text} = 'Backorder Parts Continued...'
                    SetTarget()
                    Print(rpt:Continued)
                    tmp:CountLines = 1
                End !If tmp:CountLines > 15
        
                Print(rpt:Detail2)
            End !If Access:RETSTOCK.TryFetch(res:Record_Number_Key) = Level:Benign
        End !x# = 1 To Records(PartsList)
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RETDESNO,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:RETDESNO.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'RETDESNO')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','DESPATCH NOTE - RETAIL')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'DESPATCH NOTE - RETAIL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  access:retsales.clearkey(ret:ref_number_key)
  ret:ref_number = RDN:Sale_Number
  access:retsales.tryfetch(ret:ref_number_key)
  
  
  despatch_note_number_temp   = 'D-' & glo:select1
  If ret:payment_method = 'CAS'
      Delivery_Name_Temp  = RET:Contact_Name
      Invoice_Name_Temp   = ret:contact_name
  Else!If ret:payment_method = 'CAS'
      Delivery_Name_Temp  = RET:Company_Name_Delivery
      invoice_name_temp   = ret:company_name
  End!If ret:payment_method = 'CAS'
  If ret:building_name    = ''
      Invoice_address_Line_1     = Clip(RET:Address_Line1)
  Else!    If ret:building_name    = ''
      Invoice_address_Line_1     = Clip(RET:Building_Name) & ' ' & Clip(RET:Address_Line1)
  End!
  If ret:building_name_delivery   = ''
      Delivery_Address_Line_1_Temp    = Clip(RET:Address_Line1_Delivery)
  Else!    If ret:building_name_delivery   = ''
      Delivery_Address_Line_1_Temp    = Clip(RET:Building_Name_Delivery) & ' ' & Clip(RET:Address_Line1_Delivery)
  End!    If ret:building_name_delivery   = ''
  
  courier_cost_temp   = ret:courier_cost
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = ret:account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key) 
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          If tra:price_retail_despatch <> 'YES'
              hide_temp = 1
              Settarget(Report)
              Hide(?courier_cost)
              Hide(?sub_total)
              Hide(?vat)
              Hide(?total)
              Hide(?line1)
              Hide(?charge_details)
              Hide(?vat_total_temp)
              Hide(?total_temp)
              Hide(?courier_cost_temp)
              Hide(?sub_total_temp)
              Hide(?item_cost)
              Hide(?line_cost:1)
              Hide(?line_cost:2)
              Hide(?despatched_items_only)
              Settarget()
          End!If tra:price_retail_despatch <> 'YES'
          if tra:invoice_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = sub:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          else!if tra:use_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = tra:retail_vat_code
              if access:vatcode.fetch(vat:vat_code_key) = level:benign
                  vat_rate_temp = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign 
  SetTarget(Report)
  ?String70{prop:text} = ''
  SetTarget()
  !Alternative Contact Nos
  Case UseAlternativeContactNos(ret:Account_Number)
      Of 1
          tmp:DefaultTelephone    = tra:AltTelephoneNumber
          tmp:DefaultFax          = tra:AltFaxNumber
          tmp:DefaultEmailAddress = tra:AltEmailAddress
      Of 2
          tmp:DefaultTelephone    = sub:AltTelephoneNumber
          tmp:DefaultFax          = sub:AltFaxNumber
          tmp:DefaultEmailAddress = sub:AltEmailAddress
      Else !UseAlternativeContactNos(ret:Account_Number)
          tmp:DefaultEmailAddress = def:EmailAddress
  End !UseAlternativeContactNos(ret:Account_Number)
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE - RETAIL'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'DESPATCH NOTE - RETAIL'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Retail_Despatch_Note'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Retail_Despatch_Note',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Retail_Despatch_Note',1)
    SolaceViewVars('Back_Title',Back_Title,'Retail_Despatch_Note',1)
    SolaceViewVars('hide_temp',hide_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Retail_Despatch_Note',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Retail_Despatch_Note',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Retail_Despatch_Note',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Retail_Despatch_Note',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Retail_Despatch_Note',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Retail_Despatch_Note',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Retail_Despatch_Note',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Retail_Despatch_Note',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Retail_Despatch_Note',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Retail_Despatch_Note',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Retail_Despatch_Note',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Retail_Despatch_Note',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Retail_Despatch_Note',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Retail_Despatch_Note',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Retail_Despatch_Note',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Retail_Despatch_Note',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Retail_Despatch_Note',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Retail_Despatch_Note',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Retail_Despatch_Note',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Retail_Despatch_Note',1)
    SolaceViewVars('InitialPath',InitialPath,'Retail_Despatch_Note',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Retail_Despatch_Note',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Retail_Despatch_Note',1)
    SolaceViewVars('vat_rate_parts_temp',vat_rate_parts_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('vat_total_temp',vat_total_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('total_temp',total_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('vat_rate_temp',vat_rate_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('Delivery_Name_Temp',Delivery_Name_Temp,'Retail_Despatch_Note',1)
    SolaceViewVars('Delivery_Address_Line_1_Temp',Delivery_Address_Line_1_Temp,'Retail_Despatch_Note',1)
    SolaceViewVars('save_res_id',save_res_id,'Retail_Despatch_Note',1)
    SolaceViewVars('Invoice_Name_temp',Invoice_Name_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('Invoice_address_Line_1',Invoice_address_Line_1,'Retail_Despatch_Note',1)
    SolaceViewVars('items_total_temp',items_total_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('vat_temp',vat_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('item_cost_temp',item_cost_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('despatch_note_number_temp',despatch_note_number_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('courier_cost_temp',courier_cost_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('sub_total_temp',sub_total_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('line_cost_ex_temp',line_cost_ex_temp,'Retail_Despatch_Note',1)
    SolaceViewVars('PartsList:RecordNumber',PartsList:RecordNumber,'Retail_Despatch_Note',1)
    SolaceViewVars('PartsList:OrderNumber',PartsList:OrderNumber,'Retail_Despatch_Note',1)
    SolaceViewVars('PartsList:PartNumber',PartsList:PartNumber,'Retail_Despatch_Note',1)
    SolaceViewVars('PartsList:Quantity',PartsList:Quantity,'Retail_Despatch_Note',1)
    SolaceViewVars('tmp:DefaultEmailAddress',tmp:DefaultEmailAddress,'Retail_Despatch_Note',1)
    SolaceViewVars('tmp:CountLines',tmp:CountLines,'Retail_Despatch_Note',1)


BuildCtrlQueue      Routine







