

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01014.INC'),ONCE        !Local module procedure declarations
                     END








Single_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
save_par_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
customer_name_temp   STRING(60)
sub_total_temp       REAL
payment_date_temp    DATE,DIM(4)
other_amount_temp    REAL
total_paid_temp      REAL
payment_type_temp    STRING(20),DIM(4)
payment_amount_temp  REAL,DIM(4)
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Invoice_Number)
                     END
Report               REPORT,AT(396,6521,7521,2323),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,5844),USE(?unnamed)
                         STRING('Job Number: '),AT(4896,313),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s12),AT(5917,313),USE(job:Ref_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Of Invoice:'),AT(4896,469),USE(?String86),TRN,FONT(,8,,)
                         STRING('Invoice Number: '),AT(4896,625),USE(?String87),TRN,FONT(,8,,)
                         STRING(@s10),AT(5917,625),USE(inv:Invoice_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5917,781),USE(job:Order_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Order Number:'),AT(4896,781),USE(?String87:3),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5917,938),USE(job:date_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Date Booked:'),AT(4896,938),USE(?String87:2),TRN,FONT(,8,,)
                         STRING('INVOICE ADDRESS'),AT(156,1302),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('V.A.T. No: '),AT(1615,1302),USE(?String102),TRN,FONT(,9,,)
                         STRING(@s20),AT(2240,1302),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1302,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING(@s60),AT(156,1563),USE(customer_name_temp),TRN,FONT(,8,,)
                         STRING(@s60),AT(4115,1563),USE(customer_name_temp,,?customer_name_temp:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5917,469),USE(inv:Date_Created),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@s22),AT(1604,3240),USE(job:Order_Number,,?JOB:Order_Number:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4604,3240),USE(job:Charge_Type,,?JOB:Charge_Type:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s22),AT(3156,3240),USE(job:Authority_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(6083,3240),USE(job:Repair_Type,,?JOB:Repair_Type:2),TRN,FONT(,8,,)
                         STRING(@s25),AT(156,3917,1000,156),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(6250,3906),USE(job:Mobile_Number),TRN,FONT(,8,,)
                         STRING(@s25),AT(1250,3906,1323,156),USE(job:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s25),AT(2656,3906,1396,156),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s16),AT(4167,3906,990,156),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                         STRING(@s16),AT(5208,3906),USE(job:MSN),TRN,FONT(,8,,)
                         STRING('REPORTED FAULT'),AT(156,4323),USE(?String64),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1823,4323,5417,469),USE(jbn:Fault_Description),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('ENGINEERS REPORT'),AT(156,5000),USE(?String88),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1823,5000,5417,469),USE(jbn:Invoice_Text),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('PARTS USED'),AT(156,5604),USE(?String79),TRN,FONT(,9,,FONT:bold)
                         STRING('Qty'),AT(1521,5604),USE(?String80),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1917,5604),USE(?String81),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3677,5604),USE(?String82),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Cost'),AT(5719,5604),USE(?String83),TRN,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(6677,5604),USE(?String89),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1396,5760,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4115,2500),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4375,2500),USE(job:Telephone_Delivery),FONT(,8,,)
                         STRING('Tel:'),AT(156,2500),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(417,2500),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(1927,2500),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2188,2500),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3240),USE(job:Account_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1719),USE(job:Company_Name_Delivery),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1875,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2031),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2188),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2344),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@n8b),AT(1156,0),USE(par:Quantity),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,8,,)
                           STRING(@s25b),AT(3677,0),USE(par:Description),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(5396,0),USE(par:Sale_Cost),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,0),USE(line_cost_temp),TRN,RIGHT,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,9146,7521,2406),USE(?unnamed:4)
                         STRING('Date'),AT(156,0),USE(?String66),TRN,FONT(,7,,FONT:bold)
                         STRING('Type'),AT(677,0),USE(?String67),TRN,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1771,156),USE(payment_amount_temp[1]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('Labour:'),AT(4844,0),USE(?String90),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6406,0),USE(job:Invoice_Labour_Cost),TRN,RIGHT,FONT(,8,,)
                         STRING(@s20),AT(677,156),USE(payment_type_temp[1]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('Amount'),AT(1938,0),USE(?String70),TRN,FONT(,7,,FONT:bold)
                         STRING(@s20),AT(3125,281),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(104,260),USE(payment_date_temp[2]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s8),AT(2875,83,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                         STRING(@d6b),AT(104,156),USE(payment_date_temp[1]),TRN,RIGHT,FONT(,7,,)
                         STRING('Carriage:'),AT(4844,313),USE(?String93),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6406,156),USE(job:Invoice_Parts_Cost),TRN,RIGHT,FONT(,8,,)
                         STRING(@n10.2b),AT(1771,260),USE(payment_amount_temp[2]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1771,365),USE(payment_amount_temp[3]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('V.A.T.'),AT(4844,625),USE(?String94),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6406,313),USE(job:Invoice_Courier_Cost),TRN,RIGHT,FONT(,8,,)
                         STRING('Total:'),AT(4844,781),USE(?String95),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(6302,729,1000,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING(@d6b),AT(104,469),USE(payment_date_temp[4]),TRN,RIGHT,FONT(,7,,)
                         STRING(@n14.2),AT(6406,781),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Others:'),AT(156,625),USE(?Others),TRN,FONT(,7,,FONT:bold)
                         STRING(@s20),AT(677,365),USE(payment_type_temp[3]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1771,469),USE(payment_amount_temp[4]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         LINE,AT(6302,417,1000,0),USE(?Line2:2),COLOR(COLOR:Black)
                         STRING(@n14.2),AT(6406,469),USE(sub_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING(@d6b),AT(104,365),USE(payment_date_temp[3]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(677,260),USE(payment_type_temp[2]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('Sub Total:'),AT(4844,469),USE(?String106),TRN,FONT(,8,,)
                         STRING(@s16),AT(2875,521,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                         LINE,AT(156,781,1979,0),USE(?Line4),COLOR(COLOR:Black)
                         STRING('Total:'),AT(156,833),USE(?String109),TRN,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1771,625),USE(other_amount_temp),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n14.2),AT(6406,625),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                         STRING(@s20),AT(677,469),USE(payment_type_temp[4]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@s24),AT(2875,719,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                         STRING(@n10.2b),AT(1771,833),USE(total_paid_temp),TRN,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('<128>'),AT(6344,781),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Parts:'),AT(4844,156),USE(?String92),TRN,FONT(,8,,)
                         TEXT,AT(104,1302,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('rinvdet.gif'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('INVOICE'),AT(5604,0),USE(?String20),TRN,FONT(,20,,FONT:bold)
                         STRING(@s30),AT(156,52,3844,240),USE(def:Invoice_Company_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,323,2240,156),USE(def:Invoice_Address_Line1),TRN
                         STRING(@s30),AT(156,479,2240,156),USE(def:Invoice_Address_Line2),TRN
                         STRING(@s30),AT(156,635,2240,156),USE(def:Invoice_Address_Line3),TRN
                         STRING(@s15),AT(156,802,1156,156),USE(def:Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s15),AT(677,958),USE(def:Invoice_Telephone_Number)
                         STRING('Fax: '),AT(156,1094,313,208),USE(?String19),TRN
                         STRING(@s15),AT(677,1094),USE(def:Invoice_Fax_Number)
                         STRING('Email:'),AT(156,1250,417,208),USE(?String19:2),TRN
                         STRING(@s255),AT(677,1250),USE(def:InvoiceEmailAddress),TRN
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('PAYMENT DETAILS'),AT(156,8479),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),TRN,FONT(,9,,FONT:bold)
                         STRING('Model'),AT(156,3854),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(156,3177),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(1604,3177),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Authority Number'),AT(3073,3177),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(4531,3177),USE(?String40:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(6000,3177),USE(?String40:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1250,3854),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(2656,3854),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('E.S.N. / I.M.E.I.'),AT(4167,3854),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(5208,3854),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('Mobile No'),AT(6250,3854,625,208),USE(?String44:2),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3635),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Single_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBPAYMT.Open
  Relate:STANTEXT.Open
  Access:JOBACC.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        count# = 0
        setcursor(cursor:wait)
        save_par_id = access:parts.savefile()
        access:parts.clearkey(par:part_number_key)
        par:ref_number  = job:ref_number
        set(par:part_number_key,par:part_number_key)
        loop
            if access:parts.next()
               break
            end !if
            if par:ref_number  <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            count# += 1
            part_number_temp = par:part_number
            line_cost_temp = par:quantity * par:sale_cost
            Print(rpt:detail)
        end !loop
        access:parts.restorefile(save_par_id)
        setcursor()
        If count# = 0
            part_number_temp = 'No Parts Attached'
            Print(rpt:detail)
        End!If count# = 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:JOBPAYMT.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  access:jobs.clearkey(job:InvoiceNumberKey)
  job:invoice_number = inv:invoice_number
  access:jobs.fetch(job:InvoiceNumberKey)
  
  access:jobnotes.clearkey(jbn:RefNumberKey)
  jbn:RefNumber   = job:Ref_number
  access:jobnotes.tryfetch(jbn:RefNumberKey)
  
  delivery_address1_temp     = job:address_line1_delivery
  delivery_address2_temp     = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp  = job:address_line3_delivery
      delivery_address4_temp  = job:postcode_delivery
  End
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job:account_number
  access:subtracc.fetch(sub:account_number_key)
  access:tradeacc.clearkey(tra:account_number_key) 
  tra:account_number = sub:main_account_number
  access:tradeacc.fetch(tra:account_number_key)
  if tra:invoice_sub_accounts = 'YES'
      If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = job:address_line1
          invoice_address2_temp     = job:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = job:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job:address_line3
              invoice_address4_temp  = job:postcode
          End
          invoice_company_name_temp   = job:company_name
          invoice_telephone_number_temp   = job:telephone_number
          invoice_fax_number_temp = job:fax_number
          if job:title = '' and job:initial = ''
              customer_name_temp = clip(job:surname)
          elsif job:title = '' and job:initial <> ''
              customer_name_temp = clip(job:initial) & ' ' & clip(job:surname)
          elsif job:title <> '' and job:initial = ''
              customer_name_temp = clip(job:title) & ' ' & clip(job:surname)
          elsif job:title <> '' and job:initial <> ''
              customer_name_temp = clip(job:title) & ' ' & clip(job:initial) & ' ' & clip(job:surname)
          else
              customer_name_temp = ''
          end
  
      Else!If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = sub:address_line1
          invoice_address2_temp     = sub:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = sub:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = sub:address_line3
              invoice_address4_temp  = sub:postcode
          End
          invoice_company_name_temp   = sub:company_name
          invoice_telephone_number_temp   = sub:telephone_number
          invoice_fax_number_temp = sub:fax_number
          customer_name_temp = sub:contact_name
  
      End!If sub:invoice_customer_address = 'YES'
      
  else!if tra:use_sub_accounts = 'YES'
      If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = job:address_line1
          invoice_address2_temp     = job:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = job:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job:address_line3
              invoice_address4_temp  = job:postcode
          End
          invoice_company_name_temp   = job:company_name
          invoice_telephone_number_temp   = job:telephone_number
          invoice_fax_number_temp = job:fax_number
          if job:title = '' and job:initial = ''
              customer_name_temp = clip(job:surname)
          elsif job:title = '' and job:initial <> ''
              customer_name_temp = clip(job:initial) & ' ' & clip(job:surname)
          elsif job:title <> '' and job:initial = ''
              customer_name_temp = clip(job:title) & ' ' & clip(job:surname)
          elsif job:title <> '' and job:initial <> ''
              customer_name_temp = clip(job:title) & ' ' & clip(job:initial) & ' ' & clip(job:surname)
          else
              customer_name_temp = ''
          end
  
      Else!If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = tra:address_line1
          invoice_address2_temp     = tra:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = tra:address_line3
              invoice_address4_temp  = tra:postcode
          End
          invoice_company_name_temp   = tra:company_name
          invoice_telephone_number_temp   = tra:telephone_number
          invoice_fax_number_temp = tra:fax_number
          customer_name_temp = tra:contact_name
      End!If tra:invoice_customer_address = 'YES'
  End!!if tra:use_sub_accounts = 'YES'
  
  
  
  Total_Price('I',vat",total",balance")
  vat_temp = vat"
  total_temp = total"
  sub_total_temp = job:invoice_labour_cost + job:invoice_parts_cost + job:invoice_courier_cost
  
  access:users.clearkey(use:user_code_key)
  use:user_code = job:despatch_user
  If access:users.fetch(use:user_code_key) = Level:Benign
      despatched_user_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  End!If access:users.fetch(use:user_code_key) = Level:Benign
  
  !Payments
  x# = 0
  save_jpt_id = access:jobpaymt.savefile()
  access:jobpaymt.clearkey(jpt:all_date_key)
  jpt:ref_number = job:ref_number
  set(jpt:all_date_key,jpt:all_date_key)
  loop
      if access:jobpaymt.next()
         break
      end !if
      if jpt:ref_number <> job:Ref_number      |
          then break.  ! end if
      x# += 1
      If x# > 0 And x# < 5
          payment_date_temp[x#] = jpt:date
          payment_type_temp[x#] = jpt:payment_type
          payment_amount_temp[x#] = jpt:amount
      Else
          other_amount_temp += jpt:amount
      End!If x# < 5
      total_paid_temp += jpt:amount
  end !loop
  access:jobpaymt.restorefile(save_jpt_id)
  
  settarget(report)
  If other_amount_temp <> 0
      Unhide(?others)
  Else!If other_amount_temp <> 0
      Hide(?others)
  End!If other_amount_temp <> 0
  settarget()
  
  
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job:ref_number)
  job_number_temp = 'Job No: ' & Clip(job:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job:esn)
  esn_temp = 'E.S.N.: ' & Clip(job:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Single_Invoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Single_Invoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Default_Invoice_Company_Name_Temp',Default_Invoice_Company_Name_Temp,'Single_Invoice',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Single_Invoice',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Single_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line1_Temp',Default_Invoice_Address_Line1_Temp,'Single_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line2_Temp',Default_Invoice_Address_Line2_Temp,'Single_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line3_Temp',Default_Invoice_Address_Line3_Temp,'Single_Invoice',1)
    SolaceViewVars('Default_Invoice_Postcode_Temp',Default_Invoice_Postcode_Temp,'Single_Invoice',1)
    SolaceViewVars('Default_Invoice_Telephone_Number_Temp',Default_Invoice_Telephone_Number_Temp,'Single_Invoice',1)
    SolaceViewVars('Default_Invoice_Fax_Number_Temp',Default_Invoice_Fax_Number_Temp,'Single_Invoice',1)
    SolaceViewVars('Default_Invoice_VAT_Number_Temp',Default_Invoice_VAT_Number_Temp,'Single_Invoice',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Single_Invoice',1)
    SolaceViewVars('save_par_id',save_par_id,'Single_Invoice',1)
    SolaceViewVars('save_jpt_id',save_jpt_id,'Single_Invoice',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Single_Invoice',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Single_Invoice',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Single_Invoice',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Single_Invoice',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Single_Invoice',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Single_Invoice',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Single_Invoice',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Single_Invoice',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Single_Invoice',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Single_Invoice',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Single_Invoice',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Single_Invoice',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Single_Invoice',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Single_Invoice',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Single_Invoice',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Single_Invoice',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Single_Invoice',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Single_Invoice',1)
    SolaceViewVars('InitialPath',InitialPath,'Single_Invoice',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Single_Invoice',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Single_Invoice',1)
    SolaceViewVars('code_temp',code_temp,'Single_Invoice',1)
    SolaceViewVars('option_temp',option_temp,'Single_Invoice',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Single_Invoice',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Single_Invoice',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Single_Invoice',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Single_Invoice',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Single_Invoice',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Single_Invoice',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Single_Invoice',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Single_Invoice',1)
    SolaceViewVars('Delivery_Address1_Temp',Delivery_Address1_Temp,'Single_Invoice',1)
    SolaceViewVars('Delivery_address2_temp',Delivery_address2_temp,'Single_Invoice',1)
    SolaceViewVars('Delivery_address3_temp',Delivery_address3_temp,'Single_Invoice',1)
    SolaceViewVars('Delivery_address4_temp',Delivery_address4_temp,'Single_Invoice',1)
    SolaceViewVars('Invoice_Company_Temp',Invoice_Company_Temp,'Single_Invoice',1)
    SolaceViewVars('Invoice_address1_temp',Invoice_address1_temp,'Single_Invoice',1)
    SolaceViewVars('invoice_address2_temp',invoice_address2_temp,'Single_Invoice',1)
    SolaceViewVars('invoice_address3_temp',invoice_address3_temp,'Single_Invoice',1)
    SolaceViewVars('invoice_address4_temp',invoice_address4_temp,'Single_Invoice',1)
    
      Loop SolaceDim1# = 1 to 6
        SolaceFieldName" = 'accessories_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",accessories_temp[SolaceDim1#],'Single_Invoice',1)
      End
    
    
    SolaceViewVars('estimate_value_temp',estimate_value_temp,'Single_Invoice',1)
    SolaceViewVars('despatched_user_temp',despatched_user_temp,'Single_Invoice',1)
    SolaceViewVars('vat_temp',vat_temp,'Single_Invoice',1)
    SolaceViewVars('total_temp',total_temp,'Single_Invoice',1)
    SolaceViewVars('part_number_temp',part_number_temp,'Single_Invoice',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Single_Invoice',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Single_Invoice',1)
    SolaceViewVars('esn_temp',esn_temp,'Single_Invoice',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Single_Invoice',1)
    SolaceViewVars('sub_total_temp',sub_total_temp,'Single_Invoice',1)
    
      Loop SolaceDim1# = 1 to 4
        SolaceFieldName" = 'payment_date_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",payment_date_temp[SolaceDim1#],'Single_Invoice',1)
      End
    
    
    SolaceViewVars('other_amount_temp',other_amount_temp,'Single_Invoice',1)
    SolaceViewVars('total_paid_temp',total_paid_temp,'Single_Invoice',1)
    
      Loop SolaceDim1# = 1 to 4
        SolaceFieldName" = 'payment_type_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",payment_type_temp[SolaceDim1#],'Single_Invoice',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 4
        SolaceFieldName" = 'payment_amount_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",payment_amount_temp[SolaceDim1#],'Single_Invoice',1)
      End
    
    
    SolaceViewVars('invoice_company_name_temp',invoice_company_name_temp,'Single_Invoice',1)
    SolaceViewVars('invoice_telephone_number_temp',invoice_telephone_number_temp,'Single_Invoice',1)
    SolaceViewVars('invoice_fax_number_temp',invoice_fax_number_temp,'Single_Invoice',1)


BuildCtrlQueue      Routine







