

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01005.INC'),ONCE        !Local module procedure declarations
                     END








Job_Card PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
tmp:PrintedBy        STRING(60)
invoice_text_temp    STRING(255)
engineers_notes_temp STRING(255)
save_job2_id         USHORT,AUTO
tmp:accessories      STRING(255)
RejectRecord         LONG,AUTO
save_maf_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
save_jea_id          USHORT,AUTO
fault_code_field_temp STRING(20),DIM(12)
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(70)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
engineer_temp        STRING(30)
part_type_temp       STRING(4)
customer_name_temp   STRING(40)
delivery_name_temp   STRING(40)
exchange_unit_number_temp STRING(20)
exchange_model_number STRING(30)
exchange_manufacturer_temp STRING(30)
exchange_unit_type_temp STRING(30)
exchange_esn_temp    STRING(16)
exchange_msn_temp    STRING(15)
exchange_unit_title_temp STRING(15)
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:bouncers         STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Account_Number)
                       PROJECT(job_ali:Charge_Type)
                       PROJECT(job_ali:Company_Name_Delivery)
                       PROJECT(job_ali:DOP)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Date_QA_Passed)
                       PROJECT(job_ali:Date_QA_Rejected)
                       PROJECT(job_ali:Date_QA_Second_Passed)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Fault_Code1)
                       PROJECT(job_ali:Fault_Code10)
                       PROJECT(job_ali:Fault_Code11)
                       PROJECT(job_ali:Fault_Code12)
                       PROJECT(job_ali:Fault_Code2)
                       PROJECT(job_ali:Fault_Code3)
                       PROJECT(job_ali:Fault_Code4)
                       PROJECT(job_ali:Fault_Code5)
                       PROJECT(job_ali:Fault_Code6)
                       PROJECT(job_ali:Fault_Code7)
                       PROJECT(job_ali:Fault_Code8)
                       PROJECT(job_ali:Fault_Code9)
                       PROJECT(job_ali:Location)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Order_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Repair_Type)
                       PROJECT(job_ali:Repair_Type_Warranty)
                       PROJECT(job_ali:Telephone_Delivery)
                       PROJECT(job_ali:Time_Completed)
                       PROJECT(job_ali:Time_QA_Passed)
                       PROJECT(job_ali:Time_QA_Rejected)
                       PROJECT(job_ali:Time_QA_Second_Passed)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:Warranty_Charge_Type)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:time_booked)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                       END
                     END
Report               REPORT,AT(385,7031,7531,1510),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,635,7521,8292)
                         STRING('Job Number: '),AT(4896,365),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s12),AT(5938,365),USE(job_ali:Ref_Number),TRN,LEFT,FONT(,12,,FONT:bold)
                         STRING('Date Booked: '),AT(4896,604),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5938,604),USE(job_ali:date_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6563,604),USE(job_ali:time_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(5938,990),USE(job_ali:DOP),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Engineer:'),AT(4896,760),USE(?String96),TRN,FONT(,8,,)
                         STRING(@s30),AT(5938,760),USE(engineer_temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Date Of Purchase:'),AT(4896,990),USE(?String96:2),TRN,FONT(,8,,)
                         STRING(@s40),AT(156,1563),USE(customer_name_temp),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s40),AT(4083,1563),USE(delivery_name_temp),TRN,LEFT,FONT(,8,,,CHARSET:ANSI)
                         STRING(@s20),AT(1615,3229),USE(job_ali:Charge_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3073,3229),USE(job_ali:Repair_Type),TRN,FONT(,8,,)
                         STRING(@s20),AT(156,3229),USE(job_ali:Location),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,3917,1000,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(1615,3917,1323,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3125,3917,1396,156),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(4740,3917,1396,156),USE(job_ali:ESN),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(6083,3917),USE(job_ali:MSN),TRN,FONT(,8,,)
                         STRING(@s15),AT(6083,4271),USE(exchange_msn_temp),TRN,FONT(,8,,)
                         STRING(@s16),AT(4740,4271,1396,156),USE(exchange_esn_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3125,4271,1396,156),USE(exchange_unit_type_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(1615,4271,1323,156),USE(exchange_manufacturer_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(156,4271,1000,156),USE(exchange_model_number),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,4115),USE(exchange_unit_title_temp),TRN,LEFT,FONT(,9,,FONT:bold)
                         STRING(@s20),AT(1406,4115),USE(exchange_unit_number_temp),TRN,LEFT
                         STRING('REPORTED FAULT'),AT(156,4479),USE(?String64),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,4479,5417,417),USE(jbn_ali:Fault_Description),TRN,FONT(,8,,)
                         STRING('ESTIMATE'),AT(156,4948),USE(?Estimate),TRN,HIDE,FONT(,9,,FONT:bold)
                         STRING(@s70),AT(1615,4948,5625,208),USE(estimate_value_temp),TRN,FONT(,8,,)
                         STRING('ENGINEERS REPORT'),AT(156,5156),USE(?String88),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,5156,1979,417),USE(invoice_text_temp),TRN,FONT(,7,,)
                         STRING('WORK CARRIED OUT'),AT(3750,5156),USE(?String88:2),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(5104,5156,1927,417),USE(engineers_notes_temp),TRN,FONT(,7,,)
                         TEXT,AT(1615,5625,5417,313),USE(tmp:accessories),TRN,FONT(,8,,)
                         STRING('ACCESSORIES'),AT(156,5625),USE(?String100),TRN,FONT(,9,,FONT:bold)
                         STRING('Type'),AT(521,6198),USE(?String98),TRN,FONT(,8,,FONT:bold)
                         STRING('PARTS REQUIRED'),AT(156,5990),USE(?String79),TRN,FONT(,9,,FONT:bold)
                         STRING('Qty'),AT(156,6198),USE(?String80),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(885,6198),USE(?String81),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(2292,6198),USE(?String82),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Cost'),AT(4063,6198),USE(?String83),TRN,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(4948,6198),USE(?String89),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(208,6354,7083,0),USE(?Line1),COLOR(COLOR:Black)
                         LINE,AT(208,7969,7083,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('BOUNCER'),AT(156,8021,1354,156),USE(?BouncerTitle),TRN,HIDE,FONT('Arial',9,,FONT:bold)
                         TEXT,AT(1615,8021,5417,208),USE(tmp:bouncers),TRN,HIDE,FONT(,8,,)
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4083,2500),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2500),USE(job_ali:Telephone_Delivery),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2500),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(625,2500),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(2135,2500),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2552,2500),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(2708,1719),USE(job_ali:Account_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(4531,3229),USE(job_ali:Warranty_Charge_Type),TRN,FONT(,8,,)
                         STRING(@s20),AT(5990,3229),USE(job_ali:Repair_Type_Warranty),TRN,FONT(,8,,)
                         STRING('Acc No:'),AT(2135,1719),USE(?String94),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1719),USE(job_ali:Company_Name_Delivery),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1875,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s20),AT(2708,1875),USE(job_ali:Order_Number),TRN,FONT(,8,,)
                         STRING('Order No:'),AT(2135,1875),USE(?String140),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2031),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2188),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2344),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:3)
DETAIL                   DETAIL,AT(,,,115),USE(?DetailBand)
                           STRING(@n8b),AT(-104,0),USE(Quantity_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s25),AT(885,0),USE(part_number_temp),TRN,FONT(,7,,)
                           STRING(@s25),AT(2292,0),USE(Description_temp),TRN,FONT(,7,,)
                           STRING(@n14.2b),AT(3802,0),USE(Cost_Temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n14.2b),AT(4740,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s4),AT(521,0),USE(part_type_temp),TRN,FONT(,7,,)
                         END
                         FOOTER,AT(396,9198,,2406),USE(?unnamed:2),TOGETHER,ABSOLUTE
                           STRING('Completed:'),AT(156,52),USE(?String66),TRN,FONT(,8,,)
                           STRING(@D6b),AT(1146,52),USE(job_ali:Date_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@t1b),AT(1771,52),USE(job_ali:Time_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('QA Passed:'),AT(156,208),USE(?qa_passed),TRN,FONT(,8,,)
                           STRING('Labour:'),AT(4844,52),USE(?labour_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,52),USE(labour_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('QA Rejected:'),AT(156,365),USE(?qa_rejected),TRN,FONT(,8,,)
                           STRING(@s20),AT(3125,281),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(2875,83,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING('QA 2nd Check:'),AT(156,521),USE(?qa_2nd_check),TRN,FONT(,8,,)
                           STRING('Carriage:'),AT(4844,365),USE(?carriage_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,208),USE(parts_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(4844,521),USE(?vat_String),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,365),USE(courier_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@d6b),AT(1146,365),USE(job_ali:Date_QA_Rejected),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@d6b),AT(1146,208),USE(job_ali:Date_QA_Passed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('Total:'),AT(4844,781),USE(?total_string),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(6281,719,1000,0),USE(?line),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6406,781),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('<128>'),AT(6250,781),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                           STRING(@d6b),AT(1146,521),USE(job_ali:Date_QA_Second_Passed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@t1b),AT(1771,365),USE(job_ali:Time_QA_Rejected),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@t1b),AT(1771,521),USE(job_ali:Time_QA_Second_Passed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('FAULT CODES'),AT(156,1042),USE(?String107),TRN,FONT(,9,,FONT:bold)
                           STRING(@s20),AT(208,1250),USE(fault_code_field_temp[1]),TRN,FONT(,8,,)
                           STRING(@s25),AT(1510,1250),USE(job_ali:Fault_Code1),TRN,FONT(,8,,)
                           STRING(@s20),AT(4417,1250),USE(fault_code_field_temp[7]),TRN,FONT(,8,,)
                           STRING(@s25),AT(5719,1250),USE(job_ali:Fault_Code7),FONT(,8,,)
                           STRING(@s20),AT(208,1406),USE(fault_code_field_temp[2]),TRN,FONT(,8,,)
                           STRING(@s25),AT(1510,1406),USE(job_ali:Fault_Code2),FONT(,8,,)
                           STRING(@s20),AT(4417,1406),USE(fault_code_field_temp[8]),TRN,FONT(,8,,)
                           STRING(@s25),AT(5719,1406),USE(job_ali:Fault_Code8),FONT(,8,,)
                           STRING(@s20),AT(208,1563),USE(fault_code_field_temp[3]),TRN,FONT(,8,,)
                           STRING(@s25),AT(1500,1563),USE(job_ali:Fault_Code3),FONT(,8,,)
                           STRING(@s20),AT(4417,1563),USE(fault_code_field_temp[9]),TRN,FONT(,8,,)
                           STRING(@s25),AT(5719,1563),USE(job_ali:Fault_Code9),FONT(,8,,)
                           STRING(@s20),AT(208,1719),USE(fault_code_field_temp[4]),TRN,FONT(,8,,)
                           STRING(@s25),AT(1510,1719),USE(job_ali:Fault_Code4),FONT(,8,,)
                           STRING(@s20),AT(4417,1719),USE(fault_code_field_temp[10]),TRN,FONT(,8,,)
                           STRING(@s25),AT(5719,1719),USE(job_ali:Fault_Code10),FONT(,8,,)
                           STRING(@s20),AT(208,1875),USE(fault_code_field_temp[5]),TRN,FONT(,8,,)
                           STRING(@s25),AT(1500,1875),USE(job_ali:Fault_Code5),FONT(,8,,)
                           STRING(@s20),AT(4417,1875),USE(fault_code_field_temp[11]),TRN,FONT(,8,,)
                           STRING(@s25),AT(5719,1875),USE(job_ali:Fault_Code11),FONT(,8,,)
                           STRING(@s20),AT(208,2031),USE(fault_code_field_temp[6]),TRN,FONT(,8,,)
                           STRING(@s25),AT(1500,2031),USE(job_ali:Fault_Code6),FONT(,8,,)
                           STRING(@s20),AT(4417,2031),USE(fault_code_field_temp[12]),TRN,FONT(,8,,)
                           STRING(@s25),AT(5719,2031),USE(job_ali:Fault_Code12),FONT(,8,,)
                           STRING(@s16),AT(2875,521,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@n14.2b),AT(6396,521),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@t1b),AT(1771,208),USE(job_ali:Time_QA_Passed),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s24),AT(2875,719,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Parts:'),AT(4844,208),USE(?parts_string),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10271,7521,1156),USE(?Fault_Code9:2)
                       END
                       FORM,AT(396,479,7521,11198)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('JOB CARD'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),TRN,FONT(,9,,FONT:bold)
                         STRING(@s30),AT(156,240,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,396,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,563,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,719,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,917),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,917),USE(tmp:DefaultTelephone),FONT(,9,,)
                         STRING('Fax: '),AT(156,1083),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1083),USE(tmp:DefaultFax),FONT(,9,,)
                         STRING(@s255),AT(521,1250,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('INVOICE ADDRESS'),AT(156,1458),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4083,1510,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('COMPLETION DETAILS'),AT(156,8479),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),TRN,FONT(,9,,FONT:bold)
                         STRING('Model'),AT(156,3844),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Location'),AT(156,3177),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Type'),AT(4531,3177),USE(?warranty_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Repair Type'),AT(5990,3177),USE(?warranty_repair_type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(1615,3177),USE(?Chargeable_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(3073,3177),USE(?Repair_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1635,3844),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3125,3844),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('E.S.N. / I.M.E.I.'),AT(4740,3854),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6094,3854),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3635),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Job_Card')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBEXACC.Open
  Relate:JOBS2_ALIAS.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:JOBNOTES_ALIAS.UseFile
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        count# = 0
        
        If job_ali:warranty_job = 'YES' 
            setcursor(cursor:wait)
            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = job_ali:ref_number
            set(wpr:part_number_key,wpr:part_number_key)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:ref_number  <> job_ali:ref_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                count# += 1
                part_number_temp = wpr:part_number
                part_type_temp = 'Warr'
                description_temp = wpr:description
                quantity_temp = wpr:quantity
                cost_temp = ''
                line_cost_temp = ''
                Print(rpt:detail)
            end !loop
            access:warparts.restorefile(save_wpr_id)
            setcursor()
        
        End!If job_ali:warranty_job = 'YES' 
        
        If job_ali:chargeable_job = 'YES'
            setcursor(cursor:wait)
            save_par_id = access:parts.savefile()
            access:parts.clearkey(par:part_number_key)
            par:ref_number  = job_ali:ref_number
            set(par:part_number_key,par:part_number_key)
            loop
                if access:parts.next()
                   break
                end !if
                if par:ref_number  <> job_ali:ref_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                count# += 1
                part_Type_temp = 'Char'
                description_temp = par:description
                quantity_temp = par:quantity
                cost_temp = par:sale_cost
                part_number_temp = par:part_number
                line_cost_temp = par:quantity * par:sale_cost
                Print(rpt:detail)
            end !loop
            access:parts.restorefile(save_par_id)
            setcursor()
        
        End!If job_ali:chargeable_job = 'YES'
        If count# = 0
            part_number_temp = ''
            Print(rpt:detail)
        End!If count# = 0
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:JOBEXACC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','JOB CARD')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'JOB CARD'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'JOB CARD'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'JOB CARD'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  !Barcode Bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'Job No: ' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'E.S.N.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  set(defaults)
  access:defaults.next()
  
  Settarget(Report)
  
  If job_ali:warranty_job <> 'YES'
      Hide(?job_ali:warranty_charge_type)
      Hide(?job_ali:repair_type_warranty)
      Hide(?warranty_type)
      Hide(?Warranty_repair_Type)
  End!If job_ali:warranty_job <> 'YES'
  
  If job_ali:chargeable_job <> 'YES'
      Hide(?job_ali:charge_type)
      Hide(?job_ali:repair_type)
      Hide(?Chargeable_type)
      Hide(?repair_Type)
  End!If job_ali:chargeable_job <> 'YES'
  
  
  If def:qa_required <> 'YES'
      Hide(?qa_passed)
      Hide(?qa_rejected)
      Hide(?qa_2nd_check)
  End!If def:qa_required <> 'YES'
  
  If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
      Hide(?labour_string)
      Hide(?parts_string)
      Hide(?Carriage_string)
      Hide(?vat_string)
      Hide(?total_string)
      Hide(?line)
      Hide(?Euro)
  End!If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
  
  If job_ali:estimate = 'YES' and job_ali:Chargeable_Job = 'YES'
      Unhide(?estimate)
      estimate_value_temp = 'ESTIMATE REQUIRED IF REPAIR COST EXCEEDS ' & Clip(Format(job_ali:estimate_if_over,@n14.2)) & ' (Plus V.A.T.)'
  End
  Settarget()
  
  ! Change 2399 BE(27/03/03)
  IF (INSTRING('CELLECTRONIC', UPPER(def:User_Name), 1, 1)) THEN
      invoice_text_temp = CLIP(jbn_ali:Engineers_Notes)
      engineers_notes_temp = CLIP(jbn_ali:Invoice_Text)
  ELSE
      invoice_text_temp = CLIP(jbn_ali:Invoice_Text)
      engineers_notes_temp = CLIP(jbn_ali:Engineers_Notes)
  END
  ! Change 2399 BE(27/03/03)
  
  access:users.clearkey(use:user_code_key)
  use:user_code   = job:engineer
  access:users.fetch(use:user_code_Key)
  engineer_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  
  if job_ali:title = '' and job_ali:initial = ''
      customer_name_temp = clip(job_ali:surname)
  elsif job_ali:title = '' and job_ali:initial <> ''
      customer_name_temp = clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial = ''
      customer_name_temp = clip(job_ali:title) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial <> ''
      customer_name_temp = clip(job_ali:title) & ' ' & clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  else
      customer_name_temp = ''
  end
  
  delivery_name_temp = customer_name_temp
  
  delivery_address1_temp     = job_ali:address_line1_delivery
  delivery_address2_temp     = job_ali:address_line2_delivery
  If job_ali:address_line3_delivery   = ''
      delivery_address3_temp = job_ali:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp  = job_ali:address_line3_delivery
      delivery_address4_temp  = job_ali:postcode_delivery
  End
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job_ali:account_number
  access:subtracc.fetch(sub:account_number_key)
  access:tradeacc.clearkey(tra:account_number_key) 
  tra:account_number = sub:main_account_number
  access:tradeacc.fetch(tra:account_number_key)
  if tra:invoice_sub_accounts = 'YES'
      If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = job_ali:address_line1
          invoice_address2_temp     = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job_ali:address_line3
              invoice_address4_temp  = job_ali:postcode
          End
          invoice_company_name_temp   = job_ali:company_name
          invoice_telephone_number_temp   = job_ali:telephone_number
          invoice_fax_number_temp = job_ali:fax_number
      Else!If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = sub:address_line1
          invoice_address2_temp     = sub:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = sub:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = sub:address_line3
              invoice_address4_temp  = sub:postcode
          End
          invoice_company_name_temp   = sub:company_name
          invoice_telephone_number_temp   = sub:telephone_number
          invoice_fax_number_temp = sub:fax_number
  
  
      End!If sub:invoice_customer_address = 'YES'
      
  else!if tra:use_sub_accounts = 'YES'
      If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = job_ali:address_line1
          invoice_address2_temp     = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job_ali:address_line3
              invoice_address4_temp  = job_ali:postcode
          End
          invoice_company_name_temp   = job_ali:company_name
          invoice_telephone_number_temp   = job_ali:telephone_number
          invoice_fax_number_temp = job_ali:fax_number
  
      Else!If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = tra:address_line1
          invoice_address2_temp     = tra:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = tra:address_line3
              invoice_address4_temp  = tra:postcode
          End
          invoice_company_name_temp   = tra:company_name
          invoice_telephone_number_temp   = tra:telephone_number
          invoice_fax_number_temp = tra:fax_number
  
      End!If tra:invoice_customer_address = 'YES'
  End!!if tra:use_sub_accounts = 'YES'
  
  
  If job_ali:chargeable_job <> 'YES'
      labour_temp = ''
      parts_temp = ''
      courier_cost_temp = ''
      vat_temp = ''
      total_temp = ''
  Else!If job_ali:chargeable_job <> 'YES'
  
      If job_ali:invoice_number <> ''
          Total_Price('I',vat",total",balance")
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job_ali:invoice_number
          access:invoice.fetch(inv:invoice_number_key)
          labour_temp       = job_ali:invoice_labour_cost
          parts_temp        = job_ali:invoice_parts_cost
          courier_cost_temp = job_ali:invoice_courier_cost
      Else!If job_ali:invoice_number <> ''
          Total_Price('C',vat",total",balance")
          labour_temp = job_ali:labour_cost
          parts_temp  = job_ali:parts_cost
          courier_cost_temp = job_ali:courier_cost
      End!If job_ali:invoice_number <> ''
      vat_temp = vat"
      total_temp = total"
  
  End!If job_ali:chargeable_job <> 'YES'
  
  
  access:users.clearkey(use:user_code_key)
  use:user_code = job_ali:despatch_user
  If access:users.fetch(use:user_code_key) = Level:Benign
      despatched_user_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  End!If access:users.fetch(use:user_code_key) = Level:Benign
  
  
  setcursor(cursor:wait)
  
  save_maf_id = access:manfault.savefile()
  access:manfault.clearkey(maf:field_number_key)
  maf:manufacturer = job_ali:manufacturer
  set(maf:field_number_key,maf:field_number_key)
  loop
      if access:manfault.next()
         break
      end !if
      if maf:manufacturer <> job_ali:manufacturer      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      ! Inserting (DBH 24/01/2006) #7063 - There is only room for 12 fault codes
      If maf:Field_Number > 12
          Break
      End ! If maf:Field_Number > 12b
      ! End (DBH 24/01/2006) #7063
      fault_code_field_temp[maf:field_number] = maf:field_name
  end !loop
  access:manfault.restorefile(save_maf_id)
  setcursor()
  
  tmp:accessories = ''
  setcursor(cursor:wait)
  x# = 0
  save_jac_id = access:jobacc.savefile()
  access:jobacc.clearkey(jac:ref_number_key)
  jac:ref_number = job_ali:ref_number
  set(jac:ref_number_key,jac:ref_number_key)
  loop
      if access:jobacc.next()
         break
      end !if
      if jac:ref_number <> job_ali:ref_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      x# += 1
      If tmp:accessories = ''
          tmp:accessories = jac:accessory
      Else!If tmp:accessories = ''
          tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
      End!If tmp:accessories = ''
  end !loop
  access:jobacc.restorefile(save_jac_id)
  setcursor()
  
  
  If job_ali:exchange_unit_number <> ''
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job_ali:exchange_unit_number
      if access:exchange.fetch(xch:ref_number_key) = Level:Benign
          exchange_unit_number_temp  = 'Unit: ' & Clip(job_ali:exchange_unit_number)
          exchange_model_number      = xch:model_number
          exchange_manufacturer_temp = xch:manufacturer
          exchange_unit_type_temp    = 'N/A'
          exchange_esn_temp          = xch:esn
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = xch:msn
      end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
  Else!If job_ali:exchange_unit_number <> ''
      x# = 0
      save_jea_id = access:jobexacc.savefile()
      access:jobexacc.clearkey(jea:part_number_key)
      jea:job_ref_number = job_ali:ref_number
      set(jea:part_number_key,jea:part_number_key)
      loop
          if access:jobexacc.next()
             break
          end !if
          if jea:job_ref_number <> job_ali:ref_number      |
              then break.  ! end if
          x# = 1
          Break
      end !loop
      access:jobexacc.restorefile(save_jea_id)
      If x# = 1
          exchange_unit_number_temp  = ''
          exchange_model_number      = 'N/A'
          exchange_manufacturer_temp = job_ali:manufacturer
          exchange_unit_type_temp    = 'ACCESSORY'
          exchange_esn_temp          = 'N/A'
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = 'N/A'
      End!If x# = 1
  End!If job_ali:exchange_unit_number <> ''
  
  !Check For Bouncer
  access:manufact.clearkey(man:manufacturer_key)
  man:manufacturer = job_ali:manufacturer
  access:manufact.tryfetch(man:manufacturer_key)
  
  tmp:bouncers = ''
  If job_ali:esn <> 'N/A' And job_ali:ESN <> ''
      setcursor(cursor:wait)
      save_job2_id = access:jobs2_alias.savefile()
      access:jobs2_alias.clearkey(job2:esn_key)
      job2:esn = job_ali:esn
      set(job2:esn_key,job2:esn_key)
      loop
          if access:jobs2_alias.next()
             break
          end !if
          if job2:esn <> job_ali:esn      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          If job2:Cancelled = 'YES'
              Cycle
          End !If job2:Cancelled = 'YES'
          If job2:ref_number <> job_ali:ref_number
              If job2:date_booked + man:warranty_period > job_ali:date_booked And job2:date_booked <= job_ali:date_booked
                  If tmp:bouncers = ''
                      tmp:bouncers = 'Previous Job Number(s): ' & job2:ref_number
                  Else!If tmp:bouncer = ''
                      tmp:bouncers = CLip(tmp:bouncers) & ', ' & job2:ref_number
                  End!If tmp:bouncer = ''
              End!If job2:date_booked + man:warranty_period < Today()
          End!If job2:esn <> job2:ref_number
      end !loop
      access:jobs2_alias.restorefile(save_job2_id)
      setcursor()
  End !job_ali:esn <> 'N/A' or job_ali:ESN <> ''
  
  If tmp:bouncers <> ''
      Settarget(report)
      Unhide(?bouncertitle)
      Unhide(?tmp:bouncers)
      Settarget()
  End!If CheckBouncer(job_ali:ref_number)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Job_Card'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Job_Card',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Job_Card',1)
    SolaceViewVars('invoice_text_temp',invoice_text_temp,'Job_Card',1)
    SolaceViewVars('engineers_notes_temp',engineers_notes_temp,'Job_Card',1)
    SolaceViewVars('save_job2_id',save_job2_id,'Job_Card',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'Job_Card',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Job_Card',1)
    SolaceViewVars('save_maf_id',save_maf_id,'Job_Card',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Job_Card',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Job_Card',1)
    SolaceViewVars('save_par_id',save_par_id,'Job_Card',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Job_Card',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Job_Card',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Job_Card',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Job_Card',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Job_Card',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Job_Card',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Job_Card',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Job_Card',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Job_Card',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Job_Card',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Job_Card',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Job_Card',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Job_Card',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Job_Card',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Job_Card',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Job_Card',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Job_Card',1)
    SolaceViewVars('InitialPath',InitialPath,'Job_Card',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Job_Card',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Job_Card',1)
    SolaceViewVars('code_temp',code_temp,'Job_Card',1)
    SolaceViewVars('save_jea_id',save_jea_id,'Job_Card',1)
    
      Loop SolaceDim1# = 1 to 12
        SolaceFieldName" = 'fault_code_field_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",fault_code_field_temp[SolaceDim1#],'Job_Card',1)
      End
    
    
    SolaceViewVars('option_temp',option_temp,'Job_Card',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Job_Card',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Job_Card',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Job_Card',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Job_Card',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Job_Card',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Job_Card',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Job_Card',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Job_Card',1)
    SolaceViewVars('Delivery_Address1_Temp',Delivery_Address1_Temp,'Job_Card',1)
    SolaceViewVars('Delivery_address2_temp',Delivery_address2_temp,'Job_Card',1)
    SolaceViewVars('Delivery_address3_temp',Delivery_address3_temp,'Job_Card',1)
    SolaceViewVars('Delivery_address4_temp',Delivery_address4_temp,'Job_Card',1)
    SolaceViewVars('Invoice_Company_Temp',Invoice_Company_Temp,'Job_Card',1)
    SolaceViewVars('Invoice_address1_temp',Invoice_address1_temp,'Job_Card',1)
    SolaceViewVars('invoice_address2_temp',invoice_address2_temp,'Job_Card',1)
    SolaceViewVars('invoice_address3_temp',invoice_address3_temp,'Job_Card',1)
    SolaceViewVars('invoice_address4_temp',invoice_address4_temp,'Job_Card',1)
    
      Loop SolaceDim1# = 1 to 6
        SolaceFieldName" = 'accessories_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",accessories_temp[SolaceDim1#],'Job_Card',1)
      End
    
    
    SolaceViewVars('estimate_value_temp',estimate_value_temp,'Job_Card',1)
    SolaceViewVars('despatched_user_temp',despatched_user_temp,'Job_Card',1)
    SolaceViewVars('vat_temp',vat_temp,'Job_Card',1)
    SolaceViewVars('total_temp',total_temp,'Job_Card',1)
    SolaceViewVars('part_number_temp',part_number_temp,'Job_Card',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Job_Card',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Job_Card',1)
    SolaceViewVars('esn_temp',esn_temp,'Job_Card',1)
    SolaceViewVars('charge_type_temp',charge_type_temp,'Job_Card',1)
    SolaceViewVars('repair_type_temp',repair_type_temp,'Job_Card',1)
    SolaceViewVars('labour_temp',labour_temp,'Job_Card',1)
    SolaceViewVars('parts_temp',parts_temp,'Job_Card',1)
    SolaceViewVars('courier_cost_temp',courier_cost_temp,'Job_Card',1)
    SolaceViewVars('Quantity_temp',Quantity_temp,'Job_Card',1)
    SolaceViewVars('Description_temp',Description_temp,'Job_Card',1)
    SolaceViewVars('Cost_Temp',Cost_Temp,'Job_Card',1)
    SolaceViewVars('engineer_temp',engineer_temp,'Job_Card',1)
    SolaceViewVars('part_type_temp',part_type_temp,'Job_Card',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Job_Card',1)
    SolaceViewVars('delivery_name_temp',delivery_name_temp,'Job_Card',1)
    SolaceViewVars('exchange_unit_number_temp',exchange_unit_number_temp,'Job_Card',1)
    SolaceViewVars('exchange_model_number',exchange_model_number,'Job_Card',1)
    SolaceViewVars('exchange_manufacturer_temp',exchange_manufacturer_temp,'Job_Card',1)
    SolaceViewVars('exchange_unit_type_temp',exchange_unit_type_temp,'Job_Card',1)
    SolaceViewVars('exchange_esn_temp',exchange_esn_temp,'Job_Card',1)
    SolaceViewVars('exchange_msn_temp',exchange_msn_temp,'Job_Card',1)
    SolaceViewVars('exchange_unit_title_temp',exchange_unit_title_temp,'Job_Card',1)
    SolaceViewVars('invoice_company_name_temp',invoice_company_name_temp,'Job_Card',1)
    SolaceViewVars('invoice_telephone_number_temp',invoice_telephone_number_temp,'Job_Card',1)
    SolaceViewVars('invoice_fax_number_temp',invoice_fax_number_temp,'Job_Card',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Job_Card',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Job_Card',1)
    SolaceViewVars('tmp:bouncers',tmp:bouncers,'Job_Card',1)


BuildCtrlQueue      Routine







