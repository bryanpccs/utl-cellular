

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01019.INC'),ONCE        !Local module procedure declarations
                     END








Batch_Despatch_Note PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
save_jea_id          USHORT,AUTO
save_job_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
save_jpt_id          USHORT,AUTO
print_group          QUEUE,PRE(prngrp)
esn                  STRING(30)
msn                  STRING(30)
job_number           REAL
                     END
Invoice_Address_Group GROUP,PRE(INVGRP)
Postcode             STRING(15)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Delivery_Address_Group GROUP,PRE(DELGRP)
Postcode             STRING(15)
Company_Name         STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Unit_Details_Group   GROUP,PRE(UNIGRP)
Model_Number         STRING(30)
Manufacturer         STRING(30)
ESN                  STRING(16)
MSN                  STRING(16)
Unit_Type            STRING(30)
                     END
Unit_Ref_Number_Temp REAL
Despatch_Type_Temp   STRING(3)
Invoice_Account_Number_Temp STRING(15)
Delivery_Account_Number_Temp STRING(15)
count_temp           REAL
model_number_temp    STRING(50)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(SUBTRACC)
                     END
report               REPORT,AT(396,3865,7521,6490),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,823,7521,2542),USE(?unnamed)
                         STRING('Courier:'),AT(4896,104),USE(?String23),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,104),USE(GLO:Select4),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Consignment No:'),AT(4896,260),USE(?String27),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,260),USE(GLO:Select3,,?glo:select3:2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Despatch Date:'),AT(4896,417),USE(?String27:2),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6042,417),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING('Despatched By:'),AT(4896,573),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s60),AT(6042,573),USE(tmp:PrintedBy),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Despatch Batch No:'),AT(4896,729,990,208),USE(?String28:2),TRN,FONT(,8,,)
                         STRING(@s40),AT(6042,729),USE(GLO:Select5),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(4896,885),USE(?String62),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@n3),AT(6042,885),PAGENO,USE(?ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(208,1354),USE(INVGRP:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1354),USE(DELGRP:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(208,1510),USE(INVGRP:Address_Line1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1510),USE(DELGRP:Address_Line1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Account No:'),AT(208,2292),USE(?String38),TRN,FONT(,8,,)
                         STRING(@s15),AT(885,2292),USE(GLO:Select1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(4073,2135),USE(?String36:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(5521,2135),USE(?String37:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4375,2135),USE(DELGRP:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4688,2292),USE(GLO:Select1,,?glo:select1:2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Account No:'),AT(4073,2292),USE(?String38:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(5833,2135),USE(DELGRP:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(208,1667),USE(INVGRP:Address_Line2),LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1667),USE(DELGRP:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(208,1823),USE(INVGRP:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s30),AT(4073,1823),USE(DELGRP:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(208,1979),USE(INVGRP:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2135),USE(?String36),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(573,2135),USE(INVGRP:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Fax:'),AT(1719,2135),USE(?String37),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(2031,2135),USE(INVGRP:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING(@s15),AT(4073,1979),USE(DELGRP:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s16),AT(146,0),USE(UNIGRP:ESN),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(3594,0),USE(UNIGRP:Unit_Type),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s16),AT(1250,0),USE(UNIGRP:MSN),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s3),AT(5521,0),USE(Despatch_Type_Temp),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(5875,0),USE(job:Order_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s20),AT(2292,0),USE(UNIGRP:Model_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s8),AT(4948,0),USE(job:Ref_Number),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         END
Totals                   DETAIL,AT(,,,354),USE(?Totals)
                           LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(260,104,677,156),USE(?String59),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(1042,104),USE(count_temp),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,10344,7521,854),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,677),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(385,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('BATCH DESPATCH NOTE'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,990),USE(?Telephone),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,990),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax:'),AT(156,1146),USE(?fax),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1146),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1302,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1302),USE(?Email),TRN,FONT(,9,,)
                         STRING('INVOICE ADDRESS'),AT(208,1510),USE(?String29),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4063,1510),USE(?String29:2),TRN,FONT(,9,,FONT:bold)
                         STRING('Model Number'),AT(2302,3177),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('E.S.N. / I.M.E.I.'),AT(156,3177),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('M.S.N.'),AT(1260,3177),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Type'),AT(5521,3177),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Order Number'),AT(5885,3177),USE(?string25:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Unit Type'),AT(3604,3177),USE(?string44:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Job No'),AT(4958,3177),USE(?string25:4),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Batch_Despatch_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:SUBTRACC.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:STANTEXT.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:LOAN.UseFile
  
  
  RecordsToProcess = RECORDS(SUBTRACC)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(SUBTRACC,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      recordstoprocess    = Records(jobs)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Free(print_group)
        Clear(print_group)
        
        setcursor(cursor:wait)
        
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:batch_number_key)
        job:batch_number = glo:select2
        set(job:batch_number_key,job:batch_number_key)
        loop
            if access:jobs.next()
               break
            end !if
            if job:batch_number <> glo:select2  |
                then break.  ! end if
            recordsprocessed += 1
            Do DisplayProgress
        
            If job:exchange_unit_number <> ''
                If job:exchange_consignment_number <> glo:select3
                    Cycle
                End!If job:exchange_consignment_number <> glo:select2
            Else!If job:exchange_unit_number <> ''
                If job:consignment_number <> glo:select3
                    Cycle
                End!If job:consignment_number <> glo:select2
            End!If job:exchange_unit_number <> ''
            prngrp:esn  = job:esn
            prngrp:msn  = job:msn
            prngrp:job_number = job:ref_number
            Add(print_group)
        
        end !loop
        access:jobs.restorefile(save_job_id)
        
        RecordsToProcess = Records(Print_Group)
        
        Sort(print_group,prngrp:esn,prngrp:msn)
        
        Loop x# = 1 To Records(print_group)
            Get(print_group,x#)
            recordsprocessed += 1
            Do DisplayProgress
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = prngrp:job_number
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                If job:exchange_unit_number <> ''
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number = job:exchange_unit_number
                    if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                        UNIGRP:Model_Number = XCH:Model_Number
                        UNIGRP:Manufacturer = xch:manufacturer
                        UNIGRP:ESN          = xch:esn
                        UNIGRP:MSN          = xch:msn
                        UNIGRP:Unit_Type    = 'N/A'
                    end
                    despatch_Type_temp  = 'EXC'
                Else!If job:exchange_unit_number <> ''
                    UNIGRP:Model_Number = job:Model_Number
                    UNIGRP:Manufacturer = job:Manufacturer
                    UNIGRP:ESN          = job:ESN
                    UNIGRP:MSN          = job:MSN
                    UNIGRP:Unit_Type    = job:Unit_Type
                    despatch_Type_temp  = 'JOB'
                End!If job:exchange_unit_number <> ''
                Print(Rpt:detail)
            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        
        End!Loop x# = 1 To Records(print_group)
        count_temp = Records(print_group)
        Print(rpt:totals)
        
        setcursor()
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(SUBTRACC,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','DESPATCH NOTE - BATCH')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'DESPATCH NOTE - BATCH'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  HideDespAdd# = 0
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = glo:select1
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key) 
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          If tra:Invoice_Sub_Accounts = 'YES'
              If sub:HideDespAdd = 1
                  HideDespAdd# = 1
              End!If sub:HideDespAdd = 1
          Else!If tra:Invoice_Sub_Accounts = 'YES'
              If tra:HideDespAdd = 1
                  HideDespAdd# = 1
              End!If tra:HideDespAdd = 1
          End!If tra:Invoice_Sub_Accounts = 'YES'
          if tra:use_sub_accounts = 'YES'
              DELGRP:Postcode         = sub:Postcode
              DELGRP:Company_Name     = sub:Company_Name
              DELGRP:Address_Line1    = sub:Address_Line1
              DELGRP:Address_Line2    = sub:Address_Line2
              DELGRP:Address_Line3    = sub:Address_Line3
              DELGRP:Telephone_Number = sub:Telephone_Number
              DELGRP:Fax_Number       = sub:Fax_Number
              If tra:invoice_sub_accounts = 'YES'
                  INVGRP:Postcode         = sub:Postcode
                  INVGRP:Company_Name     = sub:Company_Name
                  INVGRP:Address_Line1    = sub:Address_Line1
                  INVGRP:Address_Line2    = sub:Address_Line2
                  INVGRP:Address_Line3    = sub:Address_Line3
                  INVGRP:Telephone_Number = sub:Telephone_Number
                  INVGRP:Fax_Number       = sub:Fax_Number
              Else
                  INVGRP:Postcode         = tra:Postcode
                  INVGRP:Company_Name     = tra:Company_Name
                  INVGRP:Address_Line1    = tra:Address_Line1
                  INVGRP:Address_Line2    = tra:Address_Line2
                  INVGRP:Address_Line3    = tra:Address_Line3
                  INVGRP:Telephone_Number = tra:Telephone_Number
                  INVGRP:Fax_Number       = tra:Fax_Number
              End!If tra:invoice_sub_accounts = 'YES'
          else!if tra:use_sub_accounts = 'YES'
              INVGRP:Postcode         = tra:Postcode
              INVGRP:Company_Name     = tra:Company_Name
              INVGRP:Address_Line1    = tra:Address_Line1
              INVGRP:Address_Line2    = tra:Address_Line2
              INVGRP:Address_Line3    = tra:Address_Line3
              INVGRP:Telephone_Number = tra:Telephone_Number
              INVGRP:Fax_Number       = tra:Fax_Number
              DELGRP:Postcode         = tra:Postcode
              DELGRP:Company_Name     = tra:Company_Name
              DELGRP:Address_Line1    = tra:Address_Line1
              DELGRP:Address_Line2    = tra:Address_Line2
              DELGRP:Address_Line3    = tra:Address_Line3
              DELGRP:Telephone_Number = tra:Telephone_Number
              DELGRP:Fax_Number       = tra:Fax_Number
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  !Hide Despatch Address
  If HideDespAdd# = 1
      Settarget(Report)
      ?def:User_Name{prop:Hide} = 1
      ?def:Address_Line1{prop:Hide} = 1
      ?def:Address_Line2{prop:Hide} = 1
      ?def:Address_Line3{prop:Hide} = 1
      ?def:Postcode{prop:hide} = 1
      ?tmp:DefaultTelephone{prop:hide} = 1
      ?tmp:DefaultFax{prop:hide} = 1
      ?telephone{prop:hide} = 1
      ?fax{prop:hide} = 1
      ?email{prop:hide} = 1
      ?def:EmailAddress{prop:Hide} = 1
      Settarget()
  End!If HideDespAdd# = 1
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE - BATCH'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'DESPATCH NOTE - BATCH'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Batch_Despatch_Note'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Batch_Despatch_Note',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_jea_id',save_jea_id,'Batch_Despatch_Note',1)
    SolaceViewVars('save_job_id',save_job_id,'Batch_Despatch_Note',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Batch_Despatch_Note',1)
    SolaceViewVars('save_jpt_id',save_jpt_id,'Batch_Despatch_Note',1)
    SolaceViewVars('print_group:esn',print_group:esn,'Batch_Despatch_Note',1)
    SolaceViewVars('print_group:msn',print_group:msn,'Batch_Despatch_Note',1)
    SolaceViewVars('print_group:job_number',print_group:job_number,'Batch_Despatch_Note',1)
    SolaceViewVars('Invoice_Address_Group:Postcode',Invoice_Address_Group:Postcode,'Batch_Despatch_Note',1)
    SolaceViewVars('Invoice_Address_Group:Company_Name',Invoice_Address_Group:Company_Name,'Batch_Despatch_Note',1)
    SolaceViewVars('Invoice_Address_Group:Address_Line1',Invoice_Address_Group:Address_Line1,'Batch_Despatch_Note',1)
    SolaceViewVars('Invoice_Address_Group:Address_Line2',Invoice_Address_Group:Address_Line2,'Batch_Despatch_Note',1)
    SolaceViewVars('Invoice_Address_Group:Address_Line3',Invoice_Address_Group:Address_Line3,'Batch_Despatch_Note',1)
    SolaceViewVars('Invoice_Address_Group:Telephone_Number',Invoice_Address_Group:Telephone_Number,'Batch_Despatch_Note',1)
    SolaceViewVars('Invoice_Address_Group:Fax_Number',Invoice_Address_Group:Fax_Number,'Batch_Despatch_Note',1)
    SolaceViewVars('Delivery_Address_Group:Postcode',Delivery_Address_Group:Postcode,'Batch_Despatch_Note',1)
    SolaceViewVars('Delivery_Address_Group:Company_Name',Delivery_Address_Group:Company_Name,'Batch_Despatch_Note',1)
    SolaceViewVars('Delivery_Address_Group:Address_Line1',Delivery_Address_Group:Address_Line1,'Batch_Despatch_Note',1)
    SolaceViewVars('Delivery_Address_Group:Address_Line2',Delivery_Address_Group:Address_Line2,'Batch_Despatch_Note',1)
    SolaceViewVars('Delivery_Address_Group:Address_Line3',Delivery_Address_Group:Address_Line3,'Batch_Despatch_Note',1)
    SolaceViewVars('Delivery_Address_Group:Telephone_Number',Delivery_Address_Group:Telephone_Number,'Batch_Despatch_Note',1)
    SolaceViewVars('Delivery_Address_Group:Fax_Number',Delivery_Address_Group:Fax_Number,'Batch_Despatch_Note',1)
    SolaceViewVars('Unit_Details_Group:Model_Number',Unit_Details_Group:Model_Number,'Batch_Despatch_Note',1)
    SolaceViewVars('Unit_Details_Group:Manufacturer',Unit_Details_Group:Manufacturer,'Batch_Despatch_Note',1)
    SolaceViewVars('Unit_Details_Group:ESN',Unit_Details_Group:ESN,'Batch_Despatch_Note',1)
    SolaceViewVars('Unit_Details_Group:MSN',Unit_Details_Group:MSN,'Batch_Despatch_Note',1)
    SolaceViewVars('Unit_Details_Group:Unit_Type',Unit_Details_Group:Unit_Type,'Batch_Despatch_Note',1)
    SolaceViewVars('Unit_Ref_Number_Temp',Unit_Ref_Number_Temp,'Batch_Despatch_Note',1)
    SolaceViewVars('Despatch_Type_Temp',Despatch_Type_Temp,'Batch_Despatch_Note',1)
    SolaceViewVars('Invoice_Account_Number_Temp',Invoice_Account_Number_Temp,'Batch_Despatch_Note',1)
    SolaceViewVars('Delivery_Account_Number_Temp',Delivery_Account_Number_Temp,'Batch_Despatch_Note',1)
    SolaceViewVars('count_temp',count_temp,'Batch_Despatch_Note',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Batch_Despatch_Note',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Batch_Despatch_Note',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Batch_Despatch_Note',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Batch_Despatch_Note',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Batch_Despatch_Note',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Batch_Despatch_Note',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Batch_Despatch_Note',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Batch_Despatch_Note',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Batch_Despatch_Note',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Batch_Despatch_Note',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Batch_Despatch_Note',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Batch_Despatch_Note',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Batch_Despatch_Note',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Batch_Despatch_Note',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Batch_Despatch_Note',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Batch_Despatch_Note',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Batch_Despatch_Note',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Batch_Despatch_Note',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Batch_Despatch_Note',1)
    SolaceViewVars('InitialPath',InitialPath,'Batch_Despatch_Note',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Batch_Despatch_Note',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Batch_Despatch_Note',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Batch_Despatch_Note',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Batch_Despatch_Note',1)


BuildCtrlQueue      Routine







