

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01006.INC'),ONCE        !Local module procedure declarations
                     END








Estimate PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:PrintedBy        STRING(255)
save_epr_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
customer_name_temp   STRING(60)
sub_total_temp       REAL
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Authority_Number)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:Company_Name_Delivery)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Telephone_Delivery)
                       PROJECT(job:Unit_Type)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                         PROJECT(jbn:Fault_Description)
                         PROJECT(jbn:Invoice_Text)
                       END
                     END
Report               REPORT,AT(396,6521,7521,2323),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,5844),USE(?unnamed)
                         STRING('Job Number: '),AT(4896,365),USE(?String25),TRN,FONT(,12,,)
                         STRING(@s12),AT(6198,365),USE(job:Ref_Number),TRN,LEFT,FONT(,12,,FONT:bold)
                         STRING('Estimate Date:'),AT(4896,573),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6198,573),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(156,1563),USE(customer_name_temp),TRN,FONT(,8,,)
                         STRING(@s60),AT(4063,1563),USE(customer_name_temp,,?customer_name_temp:2),TRN,FONT(,8,,)
                         STRING(@s22),AT(1604,3240),USE(job:Order_Number,,?JOB:Order_Number:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4531,3240),USE(job:Charge_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3156,3240),USE(job:Authority_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(6042,3229),USE(job:Repair_Type),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,3917,1000,156),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(1604,3917,1323,156),USE(job:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3156,3917,1396,156),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(4604,3917,1396,156),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(6083,3917),USE(job:MSN),TRN,FONT(,8,,)
                         STRING('Reported Fault: '),AT(156,4323),USE(?String64),TRN,FONT(,8,,FONT:bold)
                         TEXT,AT(1615,4323,5625,417),USE(jbn:Fault_Description),TRN,FONT(,8,,,CHARSET:ANSI)
                         TEXT,AT(1615,5052,5625,417),USE(jbn:Invoice_Text),TRN,FONT(,8,,,CHARSET:ANSI)
                         STRING('Engineers Report:'),AT(156,5000),USE(?String88),TRN,FONT(,8,,FONT:bold)
                         GROUP,AT(104,5469,7500,677),USE(?PartsHeadingGroup),FONT('Arial',,,)
                           STRING('PARTS REQUIRED'),AT(156,5604),USE(?String79),TRN,FONT(,9,,FONT:bold)
                           STRING('Qty'),AT(1521,5604),USE(?String80),TRN,FONT(,8,,FONT:bold)
                           STRING('Part Number'),AT(1917,5604),USE(?String81),TRN,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(3677,5604),USE(?String82),TRN,FONT(,8,,FONT:bold)
                           STRING('Unit Cost'),AT(5719,5604),USE(?String83),TRN,FONT(,8,,FONT:bold)
                           STRING('Line Cost'),AT(6677,5604),USE(?String89),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(1396,5760,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         END
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4083,2500),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2500),USE(job:Telephone_Delivery),FONT(,8,,)
                         STRING('Tel:'),AT(156,2500),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(417,2500),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(1927,2500),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2240,2500),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3240),USE(job:Account_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,1719),USE(job:Company_Name_Delivery),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,1875,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2031),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2188),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2344),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@n8b),AT(1156,0),USE(Quantity_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,8,,)
                           STRING(@s25),AT(3677,0),USE(Description_temp),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(5396,0),USE(Cost_Temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,0),USE(line_cost_temp),TRN,RIGHT,FONT(,8,,)
                         END
                         FOOTER,AT(396,9198,,1479),USE(?unnamed:2),TOGETHER,ABSOLUTE
                           STRING('Labour:'),AT(4844,52),USE(?labour_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6406,52),USE(labour_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s20),AT(3125,281),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(2875,83,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Carriage:'),AT(4844,365),USE(?carriage_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6406,208),USE(parts_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(4844,677),USE(?vat_String),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6406,365),USE(courier_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Total:'),AT(4844,833),USE(?total_string),TRN,FONT(,8,,FONT:bold)
                           STRING('<128>'),AT(6302,833,156,208),USE(?Euro),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(6302,781,1000,0),USE(?line),COLOR(COLOR:Black)
                           LINE,AT(6302,469,1000,0),USE(?line:2),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6406,833),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('Sub Total:'),AT(4844,521),USE(?String92),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6406,521),USE(sub_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s16),AT(2875,521,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@n14.2b),AT(6406,677),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@s24),AT(2875,719,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Parts:'),AT(4844,208),USE(?parts_string),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1156)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7552,11198),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('ESTIMATE'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),TRN,FONT(,9,,FONT:bold)
                         STRING(@s30),AT(156,240,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,396,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,563,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,719,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,938),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,938),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1094),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1094),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1250,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('INVOICE ADDRESS'),AT(156,1458),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1510,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('AUTHORISATION DETAILS'),AT(156,8479),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),TRN,FONT(,9,,FONT:bold)
                         STRING('Estimate Accepted'),AT(208,8750),USE(?String94),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(1406,8750,156,156),USE(?Box1),COLOR(COLOR:Black)
                         STRING('Estimate Refused'),AT(208,8958),USE(?String95),TRN,FONT(,8,,FONT:bold)
                         BOX,AT(1406,8958,156,156),USE(?Box2),COLOR(COLOR:Black)
                         STRING('Name (Capitals)'),AT(208,9219),USE(?String96),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1198,9323,1354,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Signature'),AT(208,9479),USE(?String97),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(833,9583,1719,0),USE(?Line4),COLOR(COLOR:Black)
                         STRING('Model'),AT(156,3844),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(156,3156),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(1604,3156),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Authority Number'),AT(3083,3156),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(4531,3156),USE(?Chargeable_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(6000,3156),USE(?Repair_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1635,3844),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3125,3844),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('E.S.N. / I.M.E.I.'),AT(4563,3844),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6083,3844),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3635),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Estimate')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:JOBNOTES.UseFile
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:AUDIT.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        count# = 0
        
        setcursor(cursor:wait)
        
        save_epr_id = access:estparts.savefile()
        access:estparts.clearkey(epr:part_number_key)
        epr:ref_number  = job:ref_number
        set(epr:part_number_key,epr:part_number_key)
        loop
            if access:estparts.next()
               break
            end !if
            if epr:ref_number  <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            count# += 1
            part_number_temp = epr:part_number
            description_temp = epr:description
            quantity_temp = epr:quantity
            cost_temp = epr:sale_cost
            line_cost_temp = epr:quantity * epr:sale_cost
            Print(rpt:detail)
        
        end !loop
        access:estparts.restorefile(save_epr_id)
        setcursor()
        
        If count# = 0
            SetTarget(Report)
            ?PartsHeadingGroup{Prop:Hide} = 1
            Part_Number_Temp = ''
            !Print Blank Line
            Print(Rpt:Detail)
            SetTarget()
        End!If count# = 0
        
        
        If job:estimate = 'YES' And job:estimate_accepted <> 'YES' and job:estimate_rejected <> 'YES'
            GetStatus(520,1,'JOB') !estimate sent
            If access:jobs.update() = Level:Benign
                If Access:AUDIT.PrimeRecord() = Level:Benign
                    aud:Notes         = 'ESTIMATE VALUE: ' & Format(Total_Temp,@n14.2)
                    aud:Ref_Number    = job:ref_number
                    aud:Date          = Today()
                    aud:Time          = Clock()
                    aud:Type          = 'JOB'
                    Access:USERS.ClearKey(use:Password_Key)
                    use:Password      = glo:Password
                    Access:USERS.Fetch(use:Password_Key)
                    aud:User          = use:User_Code
                    aud:Action        = 'ESTIMATE SENT'
                    Access:AUDIT.Insert()
                End!If Access:AUDIT.PrimeRecord() = Level:Benign
            End !If access:jobs.update() = Level:Benign
        End!If job:estimate = 'YES' And job:estimate_accepted <> 'YES' and job:estimate_refused <> 'YES'
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(Report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(Report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','ESTIMATE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'ESTIMATE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  set(defaults)
  access:defaults.next()
  
  delivery_address1_temp     = job:address_line1_delivery
  delivery_address2_temp     = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp  = job:address_line3_delivery
      delivery_address4_temp  = job:postcode_delivery
  End
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job:account_number
  access:subtracc.fetch(sub:account_number_key)
  access:tradeacc.clearkey(tra:account_number_key) 
  tra:account_number = sub:main_account_number
  access:tradeacc.fetch(tra:account_number_key)
  
  If tra:Use_Sub_Accounts = 'YES'
      If tra:Invoice_Sub_Accounts = 'YES'
          !If invoice sub accounts is selected, then use the sub account address
          !unless "use customer address" is ticked on the sub account
          If sub:invoice_customer_address = 'YES'
              invoice_address1_temp     = job:address_line1
              invoice_address2_temp     = job:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = job:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = job:address_line3
                  invoice_address4_temp  = job:postcode
              End
              invoice_company_name_temp   = job:company_name
              invoice_telephone_number_temp   = job:telephone_number
              invoice_fax_number_temp = job:fax_number
          Else!If sub:invoice_customer_address = 'YES'
              invoice_address1_temp     = sub:address_line1
              invoice_address2_temp     = sub:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = sub:address_line3
                  invoice_address4_temp  = sub:postcode
              End
              invoice_company_name_temp   = sub:company_name
              invoice_telephone_number_temp   = sub:telephone_number
              invoice_fax_number_temp = sub:fax_number
          End!If sub:invoice_customer_address = 'YES'
      Else !If tra:Invoice_Sub_Accounts = 'YES'
          !Use Sub Account address as Invoice Address
          If tra:Allow_Cash_Sales = 'YES'
              invoice_address1_temp     = sub:address_line1
              invoice_address2_temp     = sub:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = sub:address_line3
                  invoice_address4_temp  = sub:postcode
              End
              invoice_company_name_temp   = sub:company_name
              invoice_telephone_number_temp   = sub:telephone_number
              invoice_fax_number_temp = sub:fax_number
          Else !If tra:Allow_Cash_Sales = 'YES'
              !Use the head account address, unless the "use customer" is ticked
              If tra:invoice_customer_address = 'YES'
                  invoice_address1_temp     = job:address_line1
                  invoice_address2_temp     = job:address_line2
                  If job:address_line3_delivery   = ''
                      invoice_address3_temp = job:postcode
                      invoice_address4_temp = ''
                  Else
                      invoice_address3_temp  = job:address_line3
                      invoice_address4_temp  = job:postcode
                  End
                  invoice_company_name_temp   = job:company_name
                  invoice_telephone_number_temp   = job:telephone_number
                  invoice_fax_number_temp = job:fax_number
  
              Else!If tra:invoice_customer_address = 'YES'
                  invoice_address1_temp     = tra:address_line1
                  invoice_address2_temp     = tra:address_line2
                  If job:address_line3_delivery   = ''
                      invoice_address3_temp = tra:postcode
                      invoice_address4_temp = ''
                  Else
                      invoice_address3_temp  = tra:address_line3
                      invoice_address4_temp  = tra:postcode
                  End
                  invoice_company_name_temp   = tra:company_name
                  invoice_telephone_number_temp   = tra:telephone_number
                  invoice_fax_number_temp = tra:fax_number
  
              End!If tra:invoice_customer_address = 'YES'
          End !If tra:Allow_Cash_Sales = 'YES'
      End !If tra:Invoice_Sub_Accounts = 'YES'
  Else !tra:Use_Sub_Accounts = 'YES'
      !Not using sub accounts? Invoice the trade account unless
      !"use customer is ticked"
      If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = job:address_line1
          invoice_address2_temp     = job:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = job:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job:address_line3
              invoice_address4_temp  = job:postcode
          End
          invoice_company_name_temp   = job:company_name
          invoice_telephone_number_temp   = job:telephone_number
          invoice_fax_number_temp = job:fax_number
  
      Else!If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = tra:address_line1
          invoice_address2_temp     = tra:address_line2
          If job:address_line3_delivery   = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = tra:address_line3
              invoice_address4_temp  = tra:postcode
          End
          invoice_company_name_temp   = tra:company_name
          invoice_telephone_number_temp   = tra:telephone_number
          invoice_fax_number_temp = tra:fax_number
  
      End!If tra:invoice_customer_address = 'YES'
  
  End !tra:Use_Sub_Accounts = 'YES'
  
  if job:title = '' and job:initial = ''
      customer_name_temp = clip(job:surname)
  elsif job:title = '' and job:initial <> ''
      customer_name_temp = clip(job:initial) & ' ' & clip(job:surname)
  elsif job:title <> '' and job:initial = ''
      customer_name_temp = clip(job:title) & ' ' & clip(job:surname)
  elsif job:title <> '' and job:initial <> ''
      customer_name_temp = clip(job:title) & ' ' & clip(job:initial) & ' ' & clip(job:surname)
  else
      customer_name_temp = ''
  end
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'ESTIMATE'
  access:stantext.fetch(stt:description_key)
  
  labour_temp = job:labour_cost_estimate
  parts_temp  = job:parts_cost_estimate
  courier_cost_temp = job:courier_cost_estimate
  
  Total_Price('E',vat",total",balance")
  vat_temp = vat"
  total_temp = total"
  sub_total_temp  = job:labour_cost_estimate + job:parts_cost_estimate + job:courier_cost_estimate
  
  
  !Barcode Bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job:ref_number)
  job_number_temp = 'Job No: ' & Clip(job:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job:esn)
  esn_temp = 'E.S.N.: ' & Clip(job:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'ESTIMATE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'ESTIMATE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Estimate'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Estimate',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Estimate',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Estimate',1)
    SolaceViewVars('save_epr_id',save_epr_id,'Estimate',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Estimate',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Estimate',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Estimate',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Estimate',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Estimate',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Estimate',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Estimate',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Estimate',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Estimate',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Estimate',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Estimate',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Estimate',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Estimate',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Estimate',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Estimate',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Estimate',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Estimate',1)
    SolaceViewVars('InitialPath',InitialPath,'Estimate',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Estimate',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Estimate',1)
    SolaceViewVars('code_temp',code_temp,'Estimate',1)
    SolaceViewVars('option_temp',option_temp,'Estimate',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Estimate',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Estimate',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Estimate',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Estimate',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Estimate',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Estimate',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Estimate',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Estimate',1)
    SolaceViewVars('Delivery_Address1_Temp',Delivery_Address1_Temp,'Estimate',1)
    SolaceViewVars('Delivery_address2_temp',Delivery_address2_temp,'Estimate',1)
    SolaceViewVars('Delivery_address3_temp',Delivery_address3_temp,'Estimate',1)
    SolaceViewVars('Delivery_address4_temp',Delivery_address4_temp,'Estimate',1)
    SolaceViewVars('Invoice_Company_Temp',Invoice_Company_Temp,'Estimate',1)
    SolaceViewVars('Invoice_address1_temp',Invoice_address1_temp,'Estimate',1)
    SolaceViewVars('invoice_address2_temp',invoice_address2_temp,'Estimate',1)
    SolaceViewVars('invoice_address3_temp',invoice_address3_temp,'Estimate',1)
    SolaceViewVars('invoice_address4_temp',invoice_address4_temp,'Estimate',1)
    
      Loop SolaceDim1# = 1 to 6
        SolaceFieldName" = 'accessories_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",accessories_temp[SolaceDim1#],'Estimate',1)
      End
    
    
    SolaceViewVars('estimate_value_temp',estimate_value_temp,'Estimate',1)
    SolaceViewVars('despatched_user_temp',despatched_user_temp,'Estimate',1)
    SolaceViewVars('vat_temp',vat_temp,'Estimate',1)
    SolaceViewVars('total_temp',total_temp,'Estimate',1)
    SolaceViewVars('part_number_temp',part_number_temp,'Estimate',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Estimate',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Estimate',1)
    SolaceViewVars('esn_temp',esn_temp,'Estimate',1)
    SolaceViewVars('charge_type_temp',charge_type_temp,'Estimate',1)
    SolaceViewVars('repair_type_temp',repair_type_temp,'Estimate',1)
    SolaceViewVars('labour_temp',labour_temp,'Estimate',1)
    SolaceViewVars('parts_temp',parts_temp,'Estimate',1)
    SolaceViewVars('courier_cost_temp',courier_cost_temp,'Estimate',1)
    SolaceViewVars('Quantity_temp',Quantity_temp,'Estimate',1)
    SolaceViewVars('Description_temp',Description_temp,'Estimate',1)
    SolaceViewVars('Cost_Temp',Cost_Temp,'Estimate',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Estimate',1)
    SolaceViewVars('sub_total_temp',sub_total_temp,'Estimate',1)
    SolaceViewVars('invoice_company_name_temp',invoice_company_name_temp,'Estimate',1)
    SolaceViewVars('invoice_telephone_number_temp',invoice_telephone_number_temp,'Estimate',1)
    SolaceViewVars('invoice_fax_number_temp',invoice_fax_number_temp,'Estimate',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Estimate',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Estimate',1)


BuildCtrlQueue      Routine







