

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01021.INC'),ONCE        !Local module procedure declarations
                     END








Chargeable_Summary_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
count_temp           LONG
tmp:PrintedBy        STRING(255)
save_job_id          USHORT,AUTO
first_page_temp      LONG(1)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
account_number_temp  STRING(15)
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
labour_total_temp    REAL
parts_total_temp     REAL
carriage_total_temp  REAL
vat_total_temp       REAL
grand_total_temp     REAL
total_lines_temp     STRING(20)
invoice_telephone    STRING(20)
invoice_Fax_number   STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Account_Number)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Invoice_Number)
                     END
Report               REPORT,AT(458,2083,10938,4448),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),LANDSCAPE,THOUS
                       HEADER,AT(438,323,10917,1719),USE(?unnamed)
                         STRING(@s30),AT(104,104,3438,260),USE(def:Invoice_Company_Name),LEFT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(3750,260),USE(Invoice_Name_Temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,365,3438,260),USE(def:Invoice_Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(3750,417),USE(Address_Line1_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,521,3438,260),USE(def:Invoice_Address_Line2),TRN,FONT(,9,,)
                         STRING('Date Of Invoice:'),AT(7448,729),USE(?String44),TRN,FONT(,8,,)
                         STRING(@d6b),AT(8594,729),USE(inv:Date_Created),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(104,677,3438,260),USE(def:Invoice_Address_Line3),TRN,FONT(,9,,)
                         STRING('Account Number:'),AT(7448,885),USE(?String45),TRN,FONT(,8,,)
                         STRING(@s15),AT(8594,885),USE(inv:Account_Number),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(104,833,1156,156),USE(def:Invoice_Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(104,1042),USE(?String16),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,1042),USE(def:Invoice_Telephone_Number),FONT(,9,,)
                         STRING(@s30),AT(3750,729),USE(Address_Line3_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Fax: '),AT(104,1198),USE(?String19),TRN,FONT(,8,,)
                         STRING(@s15),AT(521,1198),USE(def:Invoice_Fax_Number),FONT(,9,,)
                         STRING('Tel:'),AT(3750,1042),USE(?String47),TRN,FONT(,8,,)
                         STRING(@s20),AT(4010,1042),USE(invoice_telephone),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('V.A.T. Number:'),AT(7448,1042),USE(?String46),TRN,FONT(,8,,)
                         STRING(@s30),AT(8594,1042),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(7448,1198),USE(?String46:2),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(3750,1198),USE(?String48),TRN,FONT(,8,,)
                         STRING(@s20),AT(4010,1198),USE(invoice_Fax_number),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(9063,1198,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s255),AT(521,1354,3438,198),USE(def:InvoiceEmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1354),USE(?String19:2),TRN,FONT(,8,,)
                         STRING(@s3),AT(8594,1198),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(8854,1198,156,260),USE(?String46:3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(3750,885),USE(Address_Line4_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('Invoice Number:'),AT(7448,573),USE(?String43),TRN,FONT(,8,,)
                         STRING(@s10),AT(8594,573),USE(inv:Invoice_Number),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(3750,573),USE(Address_Line2_Temp),TRN,FONT(,8,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,167),USE(?DetailBand)
                           STRING(@s8),AT(156,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,7,,)
                           STRING(@s20),AT(729,0),USE(job:Order_Number),TRN,FONT(,7,,)
                           STRING(@s20),AT(1875,0),USE(job:Model_Number),TRN,LEFT,FONT(,7,,)
                           STRING(@s18),AT(2969,0),USE(job:Manufacturer),TRN,FONT(,7,,)
                           STRING(@s16),AT(3958,0),USE(job:ESN),TRN,FONT(,7,,)
                           STRING(@s16),AT(4844,0),USE(job:MSN),TRN,FONT(,7,,)
                           STRING(@n10.2),AT(8698,0),USE(labour_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(9167,0),USE(parts_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(9635,0),USE(courier_cost_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n10.2),AT(10208,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s20),AT(7656,0),USE(job:Repair_Type),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s20),AT(6563,0),USE(job:Charge_Type),TRN,LEFT,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s15),AT(5729,0),USE(job:Mobile_Number),TRN,FONT(,7,,)
                         END
                         FOOTER,AT(0,0,,1594),USE(?unnamed:2),ABSOLUTE
                         END
                       END
                       FOOTER,AT(448,6927,10917,844),USE(?unnamed:3)
                         TEXT,AT(104,52,7135,729),USE(stt:Text),FONT(,8,,)
                         STRING('Labour:'),AT(7490,52),USE(?String49),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(9896,52),USE(labour_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('Parts:'),AT(7490,208),USE(?String50),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(9896,208),USE(parts_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('Carriage:'),AT(7490,365),USE(?String51),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(9896,365),USE(carriage_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('V.A.T.'),AT(7490,521),USE(?String52),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(9896,521),USE(vat_total_temp),TRN,RIGHT,FONT(,8,,)
                         STRING('Total:'),AT(7490,729),USE(?String53),TRN,FONT(,8,,FONT:bold)
                         STRING('<128>'),AT(9833,729),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(9896,729),USE(grand_total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                       END
                       FORM,AT(438,417,10917,7417),USE(?unnamed:4)
                         IMAGE('rinvlan.gif'),AT(0,0,10938,7448),USE(?Image1)
                         STRING('Number Of Lines:'),AT(104,6198),USE(?String54),TRN,FONT(,,,FONT:bold)
                         STRING(@s9),AT(1354,6198),USE(count_temp),TRN,LEFT
                         STRING('SUMMARY INVOICE'),AT(8906,104),USE(?String42),TRN,FONT(,14,,FONT:bold)
                         STRING('Order Number'),AT(750,1458),USE(?String17),TRN,FONT(,7,,FONT:bold)
                         STRING('Job No'),AT(104,1458),USE(?String18),TRN,FONT(,7,,FONT:bold)
                         STRING('Make'),AT(3021,1458),USE(?String20),TRN,FONT(,7,,FONT:bold)
                         STRING('I.M.E.I. No'),AT(4083,1458),USE(?String21),TRN,FONT(,7,,FONT:bold)
                         STRING('M.S.N.'),AT(4865,1458),USE(?String37),TRN,FONT(,7,,FONT:bold)
                         STRING('Labour'),AT(8854,1458),USE(?String38),TRN,FONT(,7,,FONT:bold)
                         STRING('Parts'),AT(9375,1458),USE(?String39),TRN,FONT(,7,,FONT:bold)
                         STRING('Carriage'),AT(9688,1458),USE(?String40),TRN,FONT(,7,,FONT:bold)
                         STRING('Line Cost'),AT(10208,1458),USE(?String41),TRN,FONT(,7,,FONT:bold)
                         STRING('Repair Type'),AT(7677,1458),USE(?RepairType),TRN,FONT(,7,,FONT:bold)
                         STRING('Charge Type'),AT(6583,1458),USE(?ChargeType),TRN,FONT(,7,,FONT:bold)
                         STRING('Mobile No'),AT(5750,1458),USE(?String37:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Model'),AT(1896,1458),USE(?String35),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Chargeable_Summary_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:STANTEXT.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        parts_total_temp = 0
        labour_total_temp = 0
        carriage_total_temp = 0
        vat_total_temp = 0
        grand_total_temp = 0
        
        parts_temp = ''
        labour_temp = ''
        courier_cost_temp = ''
        vat_temp = ''
        line_cost_temp = ''
        
        count_temp = 0
        setcursor(cursor:wait)
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:InvoiceNumberKey)
        job:invoice_number = inv:invoice_number
        set(job:InvoiceNumberKey,job:InvoiceNumberKey)
        loop
            if access:jobs.next()
               break
            end !if
            if job:invoice_number <> inv:invoice_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
        
        
            parts_temp = job:invoice_parts_cost
            labour_temp = job:invoice_labour_cost
            courier_cost_temp = job:invoice_courier_cost
        !        vat_temp = Round((job:invoice_parts_cost * inv:vat_rate_labour/100),.01) + |
        !                    Round((job:invoice_labour_cost * inv:vat_rate_labour/100),.01) +|
        !                    Round((job:invoice_courier_cost * inv:vat_rate_labour/100),.01)
            vat_temp = (job:invoice_parts_cost * inv:vat_rate_labour/100) + |
                        (job:invoice_labour_cost * inv:vat_rate_labour/100) +|
                        (job:invoice_courier_cost * inv:vat_rate_labour/100)
        
            line_cost_temp = parts_temp + labour_temp + courier_cost_temp
        
            parts_total_temp += job:invoice_parts_cost
            labour_total_temp += job:invoice_labour_cost
            carriage_total_temp += job:invoice_courier_cost
            
        !        vat_total_temp = Round((parts_total_temp * inv:vat_rate_labour/100),.01) + |
        !                    Round((labour_total_temp * inv:vat_rate_labour/100),.01) +|
        !                    Round((carriage_total_temp * inv:vat_rate_labour/100),.01)
            vat_total_temp = (parts_total_temp * inv:vat_rate_labour/100) + |
                        (labour_total_temp * inv:vat_rate_labour/100) +|
                        (carriage_total_temp * inv:vat_rate_labour/100)
        
            grand_total_temp = vat_total_temp + (parts_total_temp + labour_total_temp + carriage_total_temp)
        
            count_temp += 1
            Print(rpt:detail)
        
        end !loop
        access:jobs.restorefile(save_job_id)
        setcursor()        
            
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvlan.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  access:invoice.clearkey(inv:invoice_Number_key)
  inv:invoice_number  = glo:select1
  access:invoice.fetch(inv:invoice_number_key)
  case inv:accounttype
      Of 'MAI'
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number  = inv:account_number
          if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
              invoice_name_temp   = tra:company_name
              address_line1_temp  = tra:address_line1
              address_line2_temp  = tra:address_line2
              If sup:address_line3 = ''
                  address_line3_temp = tra:postcode
                  address_line4_temp = ''
              Else
                  address_line3_temp  = tra:address_line3
                  address_line4_temp  = tra:postcode
              End
              invoice_telephone   = tra:telephone_number
              invoice_fax_number         = tra:fax_number
          end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
      Else
          access:subtracc.clearkey(sub:account_number_key)
          sub:account_number  = inv:account_number
          access:subtracc.fetch(sub:account_number_key)
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = sub:main_account_number
          if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
              if tra:invoice_sub_accounts = 'YES'
                  invoice_name_temp   = sub:company_name
                  address_line1_temp  = sub:address_line1
                  address_line2_temp  = sub:address_line2
                  If sup:address_line3 = ''
                      address_line3_temp = sub:postcode
                      address_line4_temp = ''
                  Else
                      address_line3_temp  = sub:address_line3
                      address_line4_temp  = sub:postcode
                  End
                  invoice_telephone   = sub:telephone_number
                  invoice_fax_number         = sub:fax_number
  
              else!if tra:use_sub_accounts = 'YES'
                  invoice_name_temp   = tra:company_name
                  address_line1_temp  = tra:address_line1
                  address_line2_temp  = tra:address_line2
                  If sup:address_line3 = ''
                      address_line3_temp = tra:postcode
                      address_line4_temp = ''
                  Else
                      address_line3_temp  = tra:address_line3
                      address_line4_temp  = tra:postcode
                  End
                  invoice_telephone   = tra:telephone_number
                  invoice_fax_number         = tra:fax_number
  
              end!if tra:use_sub_accounts = 'YES'
          end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
  
  End!case inv:accounttype
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Chargeable_Summary_Invoice'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Chargeable_Summary_Invoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Default_Invoice_Company_Name_Temp',Default_Invoice_Company_Name_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line1_Temp',Default_Invoice_Address_Line1_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line2_Temp',Default_Invoice_Address_Line2_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line3_Temp',Default_Invoice_Address_Line3_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Default_Invoice_Postcode_Temp',Default_Invoice_Postcode_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Default_Invoice_Telephone_Number_Temp',Default_Invoice_Telephone_Number_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Default_Invoice_Fax_Number_Temp',Default_Invoice_Fax_Number_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Default_Invoice_VAT_Number_Temp',Default_Invoice_VAT_Number_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('count_temp',count_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('save_job_id',save_job_id,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('first_page_temp',first_page_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('InitialPath',InitialPath,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('code_temp',code_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('account_number_temp',account_number_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('option_temp',option_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Delivery_Address1_Temp',Delivery_Address1_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Delivery_address2_temp',Delivery_address2_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Delivery_address3_temp',Delivery_address3_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Delivery_address4_temp',Delivery_address4_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Invoice_Company_Temp',Invoice_Company_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Invoice_address1_temp',Invoice_address1_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('invoice_address2_temp',invoice_address2_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('invoice_address3_temp',invoice_address3_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('invoice_address4_temp',invoice_address4_temp,'Chargeable_Summary_Invoice',1)
    
      Loop SolaceDim1# = 1 to 6
        SolaceFieldName" = 'accessories_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",accessories_temp[SolaceDim1#],'Chargeable_Summary_Invoice',1)
      End
    
    
    SolaceViewVars('estimate_value_temp',estimate_value_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('despatched_user_temp',despatched_user_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('vat_temp',vat_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('total_temp',total_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('part_number_temp',part_number_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('esn_temp',esn_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('charge_type_temp',charge_type_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('repair_type_temp',repair_type_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('labour_temp',labour_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('parts_temp',parts_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('courier_cost_temp',courier_cost_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Quantity_temp',Quantity_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Description_temp',Description_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('Cost_Temp',Cost_Temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('labour_total_temp',labour_total_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('parts_total_temp',parts_total_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('carriage_total_temp',carriage_total_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('vat_total_temp',vat_total_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('grand_total_temp',grand_total_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('total_lines_temp',total_lines_temp,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('invoice_telephone',invoice_telephone,'Chargeable_Summary_Invoice',1)
    SolaceViewVars('invoice_Fax_number',invoice_Fax_number,'Chargeable_Summary_Invoice',1)


BuildCtrlQueue      Routine







