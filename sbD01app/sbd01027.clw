

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01027.INC'),ONCE        !Local module procedure declarations
                     END








Despatch_Note_2 PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:PrintedBy        STRING(255)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
customer_name_temp   STRING(60)
sub_total_temp       REAL
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:LoanExchangeUnit STRING(100)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:accessories      STRING(255)
tmp:OriginalIMEI     STRING(30)
tmp:FinalIMEI        STRING(20)
tmp:DefaultEmailAddress STRING(255)
tmp:ChargeTypes      STRING(63)
tmp:ReportedFault    STRING(255)
tmp:resolution       STRING(255)
tmp:CustomerNameFull STRING(60)
tmp:dd               LONG
tmp:mmyyyy           STRING(20)
tmp:card             STRING(2)
tmp:ImagePath        STRING(255)
tmp:letterText       STRING(1000)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Company_Name_Delivery)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Telephone_Number)
                       PROJECT(job_ali:Unit_Type)
                     END
Report               REPORT,AT(0,0,210,297),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),MM
                       HEADER,AT(0,0,210,297),USE(?unnamed:2)
                         IMAGE('DESP_3.gif'),AT(0,0,210,228),USE(?BackgroundImage)
                         IMAGE,AT(2,228,205,65),USE(?AdImage)
                       END
detail1                DETAIL,AT(,,210,238),USE(?unnamed)
                         STRING('Delivery Address'),AT(13,46),USE(?String25),TRN,FONT('Arial',10,01700FFH,)
                         STRING(@s30),AT(13,57,96,4),USE(job_ali:Company_Name_Delivery),TRN,FONT('Arial',10,,)
                         STRING(@s30),AT(13,61,96,4),USE(Delivery_Address1_Temp),TRN,FONT('Arial',10,,)
                         STRING(@s30),AT(13,65,96,4),USE(Delivery_address2_temp),TRN,FONT('Arial',10,,)
                         STRING(@s30),AT(13,69,92,4),USE(Delivery_address3_temp),TRN,FONT('Arial',10,,)
                         STRING(@s30),AT(13,73,60,5),USE(Delivery_address4_temp),TRN,FONT('Arial',10,,)
                         STRING('Customer Details'),AT(13,83),USE(?String26),TRN,FONT('Arial',10,01700FFH,)
                         STRING('Customer Name'),AT(13,91),USE(?String11),TRN,FONT('Arial',10,,)
                         STRING('Contact Telephone'),AT(60,91),USE(?String12),TRN,FONT('Arial',10,,)
                         STRING('Mobile Number'),AT(108,91),USE(?String13),TRN,FONT('Arial',10,,)
                         STRING(@s60),AT(13,95,46,5),USE(tmp:CustomerNameFull),TRN,FONT('Arial',10,,)
                         STRING(@s15),AT(60,95),USE(job_ali:Telephone_Number),TRN,FONT('Arial',10,,)
                         STRING(@s15),AT(108,95),USE(job_ali:Mobile_Number),TRN,FONT('Arial',10,,)
                         STRING(@s60),AT(13,108,124,5),USE(customer_name_temp),TRN,FONT('Arial',10,,)
                         TEXT,AT(13,115,181,56),USE(tmp:letterText),FONT('Arial',10,,)
                         TEXT,AT(49,186,143,9),USE(tmp:ReportedFault),FONT('Arial',10,,)
                         STRING('Model'),AT(27,173),USE(?String40),TRN,FONT('Arial',10,,)
                         STRING('Make'),AT(61,173),USE(?String41),TRN,FONT('Arial',10,,)
                         STRING('Unit Type'),AT(93,173),USE(?String42),TRN,FONT('Arial',10,,)
                         STRING('I.M.E.I. Number'),AT(127,173),USE(?IMEINo),TRN,FONT('Arial',10,,)
                         STRING('M.S.N.'),AT(171,173),USE(?String44),TRN,FONT('Arial',10,,)
                         STRING(@s30),AT(13,178,38,4),USE(job_ali:Model_Number),TRN,CENTER,FONT('Arial',10,,)
                         STRING(@s30),AT(48,178,38,4),USE(job_ali:Manufacturer),TRN,CENTER,FONT('Arial',10,,)
                         STRING(@s30),AT(85,178,35,4),USE(job_ali:Unit_Type),TRN,CENTER,FONT('Arial',10,,)
                         STRING(@s30),AT(121,178,40,4),USE(tmp:OriginalIMEI),TRN,CENTER,FONT('Arial',10,,)
                         STRING(@s20),AT(160,178,38,4),USE(job_ali:MSN),TRN,CENTER,FONT('Arial',10,,)
                         STRING('Job Number'),AT(15,182),USE(?String21),TRN,FONT('Arial',10,,)
                         STRING(@s8),AT(49,182),USE(job_ali:Ref_Number),TRN,FONT('Arial',10,,)
                         STRING('Reported Fault'),AT(15,186),USE(?String22),TRN,FONT('Arial',10,,)
                         STRING('Resolution'),AT(16,195),USE(?String23),TRN,FONT('Arial',10,,)
                         TEXT,AT(49,196,143,9),USE(tmp:resolution),FONT('Arial',10,,)
                         STRING('Type'),AT(15,205),USE(?String24),TRN,FONT('Arial',10,,)
                         STRING(@s63),AT(49,205,132,5),USE(tmp:ChargeTypes),TRN,FONT('Arial',10,,)
                         IMAGE('DESP_3.gif'),AT(0,0,210,228),USE(?HiddenImage),HIDE
                         STRING(@s2),AT(17,32),USE(tmp:card),TRN,FONT('Arial',8,,)
                         STRING(@n2),AT(11,33),USE(tmp:dd),TRN,RIGHT,FONT('Arial',10,,)
                         STRING(@s20),AT(21,33),USE(tmp:mmyyyy),TRN,FONT('Arial',10,,)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Despatch_Note_2')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:STOCK.UseFile
  Access:WARPARTS.UseFile
  Access:LOAN.UseFile
  Access:JOBTHIRD.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        Print(rpt:detail1)
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','DESPATCH NOTE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'DESPATCH NOTE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  If glo:Select2 > 1
      Printer{PropPrint:Copies} = glo:Select2
  End!If glo:Select2 > 1
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'VODAFONE DESPATCH NOTE 1'
  IF (access:stantext.fetch(stt:description_key) = Level:Benign) THEN
      tmp:LetterText = stt:Text
  ELSE
      tmp:LetterText = ''
  END
  
  stt:description = 'VODAFONE DESPATCH NOTE 2'
  IF (access:stantext.fetch(stt:description_key) = Level:Benign) THEN
      tmp:LetterText = CLIP(tmp:LetterText) & stt:Text
  END
  
  tmp:ImagePath = CLIP(PATH()) & '\despatch.gif'
  
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  delivery_address1_temp     = job_ali:address_line1_delivery
  delivery_address2_temp     = job_ali:address_line2_delivery
  If job_ali:address_line3_delivery   = ''
      delivery_address3_temp = job_ali:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp  = job_ali:address_line3_delivery
      delivery_address4_temp  = job_ali:postcode_delivery
  End
  
  dt# = TODAY()
  tmp:dd = DAY(dt#)
  month# = MONTH(dt#)
  
  CASE tmp:dd
      OF 1
          tmp:card = 'st'
      OF 2
          tmp:card = 'nd'
      OF 3
          tmp:card = 'rd'
      ELSE
          tmp:card = 'th'
  END
  
  CASE month#
      OF 1
          tmp:mmyyyy = 'January ' & FORMAT(YEAR(dt#), @n04)
      OF 2
          tmp:mmyyyy = 'February ' & FORMAT(YEAR(dt#), @n04)
      OF 3
          tmp:mmyyyy = 'March ' & FORMAT(YEAR(dt#), @n04)
      OF 4
          tmp:mmyyyy = 'April ' & FORMAT(YEAR(dt#), @n04)
      OF 5
          tmp:mmyyyy = 'May ' & FORMAT(YEAR(dt#), @n04)
      OF 6
          tmp:mmyyyy = 'June ' & FORMAT(YEAR(dt#), @n04)
      OF 7
          tmp:mmyyyy = 'July ' & FORMAT(YEAR(dt#), @n04)
      OF 8
          tmp:mmyyyy = 'August ' & FORMAT(YEAR(dt#), @n04)
      OF 9
          tmp:mmyyyy = 'September ' & FORMAT(YEAR(dt#), @n04)
      OF 10
          tmp:mmyyyy = 'October ' & FORMAT(YEAR(dt#), @n04)
      OF 11
          tmp:mmyyyy = 'November ' & FORMAT(YEAR(dt#), @n04)
      OF 12
          tmp:mmyyyy = 'December ' & FORMAT(YEAR(dt#), @n04)
  END
  
  IF (job_ali:title = '') THEN
      IF ((job_ali:initial = '') OR (job_ali:surname = '')) THEN
          customer_name_temp = ''
      ELSE
          customer_name_temp = CLIP(job_ali:initial) &  ' ' & CLIP(job_ali:surname) & ','
      END
  ELSE
      IF (job_ali:surname = '') THEN
          customer_name_temp = ''
      ELSE
          customer_name_temp = CLIP(job_ali:title) & ' ' &  CLIP(job_ali:surname) & ','
      END
  END
  
  tmp:CustomerNameFull = ''
  IF (job_ali:title <> '') THEN
      tmp:CustomerNameFull = CLIP(job_ali:title)
  END
  
  IF (job_ali:initial <> '') THEN
      IF (tmp:CustomerNameFull <> '') THEN
          tmp:CustomerNameFull = CLIP(tmp:CustomerNameFull) & ' '  & CLIP(job_ali:initial)
      ELSE
          tmp:CustomerNameFull = CLIP(job_ali:initial)
      END
  END
  
  IF (job_ali:surname <> '') THEN
      IF (tmp:CustomerNameFull <> '') THEN
          tmp:CustomerNameFull = CLIP(tmp:CustomerNameFull) & ' ' & CLIP(job_ali:surname)
      ELSE
          tmp:CustomerNameFull = CLIP(job_ali:surname)
      END
  END
  
  
  !Get Job Notes!
  Access:JobNotes_Alias.ClearKey(jbn_ali:RefNumberKey)
  jbn_ali:RefNumber = job_ali:Ref_Number
  Access:JobNotes_Alias.Fetch(jbn_ali:RefNumberKey)
  
  
  tmp:ReportedFault = jbn_ali:Fault_Description
  ct# = LEN(CLIP(tmp:ReportedFault))
  LOOP ix# = 1 TO ct#
      IF (tmp:ReportedFault[ix# : ix#] < ' ') THEN
          tmp:ReportedFault[ix# : ix#] = ' '
      END
  END
  
  tmp:Resolution = jbn_ali:Invoice_Text
  ct# = LEN(CLIP(tmp:Resolution))
  LOOP ix# = 1 TO ct#
      IF (tmp:Resolution[ix# : ix#] < ' ') THEN
          tmp:Resolution[ix# : ix#] = ' '
      END
  END
  
  tmp:OriginalIMEI = job_ali:ESN
  
  !Was the unit exchanged at Third Party?
  IF (job_ali:Third_Party_Site <> '') THEN
      IF (job_ali:Third_Party_Site <> '') THEN
          Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
          jot:RefNumber = job_ali:Ref_Number
          SET(jot:RefNumberKey,jot:RefNumberKey)
          IF (Access:JOBTHIRD.NEXT() = Level:Benign) THEN
              IF (jot:RefNumber = job_ali:Ref_Number) THEN
                  IF (jot:OriginalIMEI <> job_ali:esn) THEN
                       tmp:OriginalIMEI = jot:OriginalIMEI
                  END
              END
          END
      END
  END
  
  tmp:ChargeTypes = ''
  
  IF (job_ali:chargeable_job = 'YES') THEN
      tmp:ChargeTypes = job_ali:charge_type
  END
  
  IF (job_ali:warranty_job = 'YES') THEN
      IF (tmp:ChargeTypes <> '') THEN
          tmp:ChargeTypes = CLIP(tmp:ChargeTypes) & ' - ' & job_ali:warranty_charge_type
      ELSE
          tmp:ChargeTypes = job_ali:warranty_charge_type
      END
  END
  
  SETTARGET(Report)
  !?HiddenImage{PROP:HIDE}=True
  ?AdImage{PROP:TEXT}=CLIP(tmp:ImagePath)
  !?HiddenImage{PROP:TEXT}=tmp:ImagePath
  SETPOSITION(?AdImage,,,205,65)
  SETTARGET
  
  
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Despatch_Note_2'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Despatch_Note_2',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Despatch_Note_2',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Despatch_Note_2',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Despatch_Note_2',1)
    SolaceViewVars('save_par_id',save_par_id,'Despatch_Note_2',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Despatch_Note_2',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Despatch_Note_2',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Despatch_Note_2',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Despatch_Note_2',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Despatch_Note_2',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Despatch_Note_2',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Despatch_Note_2',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Despatch_Note_2',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Despatch_Note_2',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Despatch_Note_2',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Despatch_Note_2',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Despatch_Note_2',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Despatch_Note_2',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Despatch_Note_2',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Despatch_Note_2',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Despatch_Note_2',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Despatch_Note_2',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Despatch_Note_2',1)
    SolaceViewVars('InitialPath',InitialPath,'Despatch_Note_2',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Despatch_Note_2',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Despatch_Note_2',1)
    SolaceViewVars('code_temp',code_temp,'Despatch_Note_2',1)
    SolaceViewVars('option_temp',option_temp,'Despatch_Note_2',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Despatch_Note_2',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Despatch_Note_2',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Despatch_Note_2',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Despatch_Note_2',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Despatch_Note_2',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Despatch_Note_2',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Despatch_Note_2',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Despatch_Note_2',1)
    SolaceViewVars('Delivery_Address1_Temp',Delivery_Address1_Temp,'Despatch_Note_2',1)
    SolaceViewVars('Delivery_address2_temp',Delivery_address2_temp,'Despatch_Note_2',1)
    SolaceViewVars('Delivery_address3_temp',Delivery_address3_temp,'Despatch_Note_2',1)
    SolaceViewVars('Delivery_address4_temp',Delivery_address4_temp,'Despatch_Note_2',1)
    SolaceViewVars('Invoice_Company_Temp',Invoice_Company_Temp,'Despatch_Note_2',1)
    SolaceViewVars('Invoice_address1_temp',Invoice_address1_temp,'Despatch_Note_2',1)
    SolaceViewVars('invoice_address2_temp',invoice_address2_temp,'Despatch_Note_2',1)
    SolaceViewVars('invoice_address3_temp',invoice_address3_temp,'Despatch_Note_2',1)
    SolaceViewVars('invoice_address4_temp',invoice_address4_temp,'Despatch_Note_2',1)
    
      Loop SolaceDim1# = 1 to 6
        SolaceFieldName" = 'accessories_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",accessories_temp[SolaceDim1#],'Despatch_Note_2',1)
      End
    
    
    SolaceViewVars('estimate_value_temp',estimate_value_temp,'Despatch_Note_2',1)
    SolaceViewVars('despatched_user_temp',despatched_user_temp,'Despatch_Note_2',1)
    SolaceViewVars('vat_temp',vat_temp,'Despatch_Note_2',1)
    SolaceViewVars('total_temp',total_temp,'Despatch_Note_2',1)
    SolaceViewVars('part_number_temp',part_number_temp,'Despatch_Note_2',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Despatch_Note_2',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Despatch_Note_2',1)
    SolaceViewVars('esn_temp',esn_temp,'Despatch_Note_2',1)
    SolaceViewVars('charge_type_temp',charge_type_temp,'Despatch_Note_2',1)
    SolaceViewVars('repair_type_temp',repair_type_temp,'Despatch_Note_2',1)
    SolaceViewVars('labour_temp',labour_temp,'Despatch_Note_2',1)
    SolaceViewVars('parts_temp',parts_temp,'Despatch_Note_2',1)
    SolaceViewVars('courier_cost_temp',courier_cost_temp,'Despatch_Note_2',1)
    SolaceViewVars('Quantity_temp',Quantity_temp,'Despatch_Note_2',1)
    SolaceViewVars('Description_temp',Description_temp,'Despatch_Note_2',1)
    SolaceViewVars('Cost_Temp',Cost_Temp,'Despatch_Note_2',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Despatch_Note_2',1)
    SolaceViewVars('sub_total_temp',sub_total_temp,'Despatch_Note_2',1)
    SolaceViewVars('invoice_company_name_temp',invoice_company_name_temp,'Despatch_Note_2',1)
    SolaceViewVars('invoice_telephone_number_temp',invoice_telephone_number_temp,'Despatch_Note_2',1)
    SolaceViewVars('invoice_fax_number_temp',invoice_fax_number_temp,'Despatch_Note_2',1)
    SolaceViewVars('tmp:LoanExchangeUnit',tmp:LoanExchangeUnit,'Despatch_Note_2',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Despatch_Note_2',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Despatch_Note_2',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'Despatch_Note_2',1)
    SolaceViewVars('tmp:OriginalIMEI',tmp:OriginalIMEI,'Despatch_Note_2',1)
    SolaceViewVars('tmp:FinalIMEI',tmp:FinalIMEI,'Despatch_Note_2',1)
    SolaceViewVars('tmp:DefaultEmailAddress',tmp:DefaultEmailAddress,'Despatch_Note_2',1)
    SolaceViewVars('tmp:ChargeTypes',tmp:ChargeTypes,'Despatch_Note_2',1)
    SolaceViewVars('tmp:ReportedFault',tmp:ReportedFault,'Despatch_Note_2',1)
    SolaceViewVars('tmp:resolution',tmp:resolution,'Despatch_Note_2',1)
    SolaceViewVars('tmp:CustomerNameFull',tmp:CustomerNameFull,'Despatch_Note_2',1)
    SolaceViewVars('tmp:dd',tmp:dd,'Despatch_Note_2',1)
    SolaceViewVars('tmp:mmyyyy',tmp:mmyyyy,'Despatch_Note_2',1)
    SolaceViewVars('tmp:card',tmp:card,'Despatch_Note_2',1)
    SolaceViewVars('tmp:ImagePath',tmp:ImagePath,'Despatch_Note_2',1)
    SolaceViewVars('tmp:letterText',tmp:letterText,'Despatch_Note_2',1)


BuildCtrlQueue      Routine







