

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01011.INC'),ONCE        !Local module procedure declarations
                     END








Retail_Single_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Use_Euro             BYTE
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
RejectRecord         LONG,AUTO
tmp:CompanyName      STRING(30)
tmp:AddressLine1     STRING(30)
tmp:AddressLine2     STRING(30)
tmp:AddressLine3     STRING(30)
tmp:Postcode         STRING(30)
tmp:TelephoneNumber  STRING(30)
tmp:FaxNumber        STRING(30)
tmp:EmailAddress     STRING(255)
tmp:VATNumber        STRING(30)
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
save_rtp_id          USHORT,AUTO
vat_rate_temp        REAL
Delivery_Name_Temp   STRING(30)
Delivery_Address_Line_1_Temp STRING(60)
save_res_id          USHORT,AUTO
Invoice_Name_temp    STRING(30)
Invoice_address_Line_1 STRING(60)
items_total_temp     REAL
vat_temp             REAL
item_cost_temp       REAL
line_cost_temp       REAL
despatch_note_number_temp STRING(10)
line_cost_total_temp REAL
vat_total_temp       REAL
Total_temp           REAL
payment_date_temp    DATE,DIM(4)
other_amount_temp    REAL
total_paid_temp      REAL
payment_type_temp    STRING(20),DIM(4)
payment_amount_temp  REAL,DIM(4)
sub_total_temp       REAL
courier_cost_temp    REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:TotalExchangeValue REAL
tmp:ItemCost         REAL
Accessory_Cost       REAL
PartsList            QUEUE,PRE(tmpque)
OrderNumber          STRING(30),NAME('OrderNumber')
Quantity             LONG
PartNumber           STRING(30),NAME('PartNumber')
Description          STRING(30)
ExchangeValue        REAL
ItemCost             REAL
LineCost             REAL
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Invoice_Number)
                     END
report               REPORT,AT(396,4573,7521,4292),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(354,490,7521,10896),USE(?unnamed)
                         STRING('Date Printed:'),AT(5000,938),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5990,938),USE(ReportRunDate),TRN,LEFT,FONT('Arial',8,,FONT:bold)
                         STRING('Tel:'),AT(4063,2500),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,781),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@s255),AT(5990,781),USE(tmp:PrintedBy),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('INVOICE:'),AT(4896,52),USE(?string22:3),TRN,FONT(,16,,FONT:bold)
                         STRING(@s8),AT(5990,52),USE(inv:Invoice_Number),TRN,LEFT,FONT('Arial',16,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5990,469),USE(tmp:VATNumber),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T. Number:'),AT(5000,469),USE(?string22:12),TRN,FONT(,8,,)
                         STRING(@n08),AT(5990,1094),USE(ret:Ref_Number),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n3),AT(5990,1250),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6250,1250),USE(?string22:14),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6406,1250,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,1250),USE(?string22:13),TRN,FONT(,8,,)
                         STRING('Date Of Invoice:'),AT(5000,625),USE(?String65),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5990,625),USE(inv:Date_Created),TRN,FONT(,8,,FONT:bold)
                         STRING('Sale Number:'),AT(5000,1094),USE(?string22:10),TRN,FONT(,8,,)
                         STRING('Account No'),AT(208,3177),USE(?string22:6),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(208,3385),USE(ret:Account_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Purch Order No'),AT(1615,3177),USE(?string22:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name'),AT(3125,3177),USE(?string22:11),TRN,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(1615,3385),USE(ret:Purchase_Order_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s20),AT(3125,3385),USE(ret:Contact_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date'),AT(208,8698),USE(?String66),TRN,FONT(,7,,FONT:bold)
                         STRING('Type'),AT(708,8698),USE(?String67),TRN,FONT(,7,,FONT:bold)
                         STRING('Amount'),AT(1969,8698),USE(?String70:5),TRN,FONT(,7,,FONT:bold)
                         STRING(@d6b),AT(156,8854),USE(payment_date_temp[1]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,8854),USE(payment_type_temp[1]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,8854),USE(payment_amount_temp[1]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@d6b),AT(156,9167),USE(payment_date_temp[4]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,9167),USE(payment_type_temp[4]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,9167),USE(payment_amount_temp[4]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         TEXT,AT(2917,8750,1719,781),USE(ret:Invoice_Text),FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(156,8958),USE(payment_date_temp[2]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,8958),USE(payment_type_temp[2]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,8958),USE(payment_amount_temp[2]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@d6b),AT(156,9063),USE(payment_date_temp[3]),TRN,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,9063),USE(payment_type_temp[3]),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,9063),USE(payment_amount_temp[3]),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('Others:'),AT(208,9323),USE(?Others),TRN,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1823,9323),USE(other_amount_temp),TRN,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         LINE,AT(208,9479,1979,0),USE(?Line4),COLOR(COLOR:Black)
                         STRING('Total:'),AT(208,9531),USE(?String109),TRN,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1823,9531),USE(total_paid_temp),TRN,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1719),USE(ret:Company_Name_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1719),USE(ret:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(4063,1875,3281,208),USE(Delivery_Address_Line_1_Temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(208,1875,3646,208),USE(Invoice_address_Line_1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2031),USE(ret:Address_Line2_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2031),USE(ret:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(4271,2500),USE(ret:Telephone_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2500),USE(?string22:9),TRN,FONT(,8,,)
                         STRING(@s15),AT(469,2500),USE(ret:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(4063,2656),USE(?string22:5),TRN,FONT(,8,,)
                         STRING(@s10),AT(4063,2344),USE(ret:Postcode_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s10),AT(208,2344),USE(ret:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2188),USE(ret:Address_Line3_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2188),USE(ret:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(4271,2656),USE(ret:Fax_Number_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(208,2656),USE(?string22:8),TRN,FONT(,8,,)
                         STRING(@s15),AT(469,2656),USE(ret:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break2                 BREAK(endofreport),USE(?unnamed:2)
detail2                  DETAIL,AT(,,,208),USE(?unnamed:5)
                           STRING(@s4),AT(1510,0),USE(tmpque:Quantity),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6510,0),USE(tmpque:LineCost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s22),AT(52,0,1406,208),USE(tmpque:OrderNumber),TRN,LEFT,FONT(,8,,)
                           STRING(@n14.2),AT(4844,0),USE(tmpque:ExchangeValue),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(5677,0),USE(tmpque:ItemCost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s22),AT(2135,0),USE(tmpque:PartNumber),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s20),AT(3542,0),USE(tmpque:Description),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                         END
                         FOOTER,USE(?unnamed:6)
                           STRING('Total Exchange Value:'),AT(2240,104),USE(?TotalExchangeValue),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(4844,104),USE(tmp:TotalExchangeValue),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(2052,313,3427,0),USE(?TotalExchangeLine),COLOR(COLOR:Black)
                           STRING('Warranty Note: You may raise a warranty claim to a value equal to the total exch' &|
   'ange value detailed above'),AT(156,365,7240,208),USE(?WarrantyNote),TRN,CENTER
                           LINE,AT(156,52,7188,0),USE(?TotalExchangeLine2),COLOR(COLOR:Black)
                         END
                       END
detail1                DETAIL,AT(,,,313),USE(?unnamed:7)
                         STRING('There are no items attached to this sale'),AT(73,52,7344,208),USE(?String106),TRN,CENTER,FONT(,12,,FONT:bold)
                       END
                       FOOTER,AT(365,9219,7521,1938),USE(?unnamed:4)
                         STRING('Courier Cost:'),AT(4948,0),USE(?CourierCost),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6531,0),USE(courier_cost_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('<128>'),AT(6354,0),USE(?String100),TRN,HIDE,FONT('Arial',8,,)
                         STRING('Total Line Cost (Ex VAT)'),AT(4948,208),USE(?TotalLineCost),TRN,FONT(,8,,)
                         STRING('<128>'),AT(6354,375),USE(?String100:3),TRN,HIDE,FONT('Arial',8,,)
                         STRING(@n14.2),AT(6531,167),USE(sub_total_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('V.A.T.:'),AT(4948,417),USE(?VAT),TRN,FONT(,8,,)
                         STRING('<128>'),AT(6354,167),USE(?String100:2),TRN,HIDE,FONT('Arial',8,,)
                         STRING(@n14.2),AT(6531,375),USE(vat_total_temp),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(6354,573,1042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('<128>'),AT(6354,635),USE(?String100:4),TRN,HIDE,FONT('Arial',8,,)
                         STRING('Total (Inc VAT):'),AT(4948,635),USE(?TotalVat),TRN,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6531,635),USE(Total_temp),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(104,1094,7292,781),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(354,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,83,3844,240),USE(tmp:CompanyName),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,323,3844,156),USE(tmp:AddressLine1),TRN
                         STRING(@s30),AT(156,479,3844,156),USE(tmp:AddressLine2),TRN
                         STRING(@s30),AT(156,635,3844,156),USE(tmp:AddressLine3),TRN
                         STRING(@s30),AT(156,802,1156,156),USE(tmp:Postcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s30),AT(677,958),USE(tmp:TelephoneNumber),TRN
                         STRING('Fax: '),AT(156,1094),USE(?String19),TRN
                         STRING(@s30),AT(677,1094),USE(tmp:FaxNumber),TRN
                         STRING(@s255),AT(677,1250,3844,208),USE(tmp:EmailAddress),TRN
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN
                         STRING('Qty'),AT(1719,3854),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(2177,3854),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3583,3854),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6917,3823),USE(?LineCost1),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost'),AT(5990,3854),USE(?ItemCost),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('(Ex.V.A.T.)'),AT(6896,3917),USE(?LineCost2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Order No'),AT(104,3854),USE(?string24:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Exchange Value'),AT(4823,3854),USE(?ExchangeValue),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('INVOICE ADDRESS'),AT(208,1469),USE(?string44:2),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4063,1469),USE(?string44:3),TRN,FONT(,9,,FONT:bold)
                         STRING('PAYMENT DETAILS'),AT(104,8490),USE(?string44:4),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4740,8490),USE(?string44:5),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE TEXT'),AT(2906,8490),USE(?string44:6),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Retail_Single_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:RETDESNO.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:RETSALES.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:RETPAY.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        sub_total_temp = 0
        
        Save_res_ID = Access:RETSTOCK.SaveFile()
        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
        res:Ref_Number  = ret:Ref_Number
        Set(res:Part_Number_Key,res:Part_Number_Key)
        Loop
            If Access:RETSTOCK.NEXT()
               Break
            End !If
            If res:Ref_Number  <> ret:Ref_Number      |
                Then Break.  ! End If
            If res:Despatched <> 'YES'
                Cycle
            End !If res:Despatched <> 'YES'
        
            If Use_Euro = False
                Accessory_Cost  = res:AccessoryCost
                tmp:TotalExchangeValue += res:AccessoryCost * res:Quantity
                Line_Cost_Temp = Round(res:Item_Cost * res:Quantity,.01)
                Case ret:Payment_Method
                    Of 'EXC'
                        tmp:ItemCost    = res:AccessoryCost * res:Quantity
                    Else
                        tmp:ItemCost    = res:Item_Cost
                End !Case ret:Payment_Method
                sub_total_temp  += line_cost_temp
        
                Courier_Cost_Temp   = inv:Courier_Paid
                Vat_Total_Temp = Round((inv:Courier_Paid + Sub_Total_Temp) * Vat_Rate_Temp/100,.01)
                total_temp  = inv:courier_paid + Round(vat_total_temp,.01) + (sub_total_temp)
            Else !If Use_Euro = False
                Case ret:Payment_Method
                    Of 'EXC'
                        tmp:ItemCost    = res:AccessoryCost * res:Quantity * inv:EuroExhangeRate
                    Else
                        tmp:ItemCost    = res:Item_Cost * inv:EuroExhangeRate
                End !Case ret:Payment_Method
                Accessory_Cost  = res:AccessoryCost * inv:EuroExhangeRate
                tmp:TotalExchangeValue += ((res:AccessoryCost * inv:EuroExhangeRate) * res:Quantity)
                Line_Cost_Temp  = Round((res:Item_Cost * inv:EuroExhangeRate) * res:Quantity,.01)
                sub_total_temp  += line_cost_temp
                Courier_Cost_temp   = inv:Courier_Paid * inv:EuroExhangeRate
                vat_total_temp = (Round(((inv:courier_paid * inv:EuroExhangeRate) + sub_total_temp) * vat_rate_temp/100,.01))
                total_temp  = (inv:courier_paid * inv:EuroExhangeRate) + Round(vat_total_temp,.01) + (sub_total_temp)
            End !If Use_Euro = False
        
            tmpque:OrderNumber  = res:Purchase_Order_Number
            tmpque:PartNumber   = res:Part_number
            Get(PartsList,'OrderNumber,PartNumber')
            If Error()
                tmpque:OrderNumber  = res:Purchase_Order_Number
                tmpque:PartNumber   = res:Part_Number
                tmpque:Description  = res:Description
                tmpque:Quantity     = res:Quantity
                tmpque:ExchangeValue    = Accessory_Cost
                tmpque:ItemCost     = tmp:ItemCost
                tmpque:LineCost     = Line_Cost_Temp
                Add(PartsList,tmpque:OrderNumber,tmpque:PartNumber)
            Else !If Error()
                tmpque:Quantity     += res:Quantity
                tmpque:LineCost     += Line_Cost_Temp
                Put(PartsList,tmpque:OrderNumber,tmpque:PartNumber)
            End !If Error()
        End !Loop
        Access:RETSTOCK.RestoreFile(Save_res_ID)
        
        Loop x# = 1 To Records(PartsList)
            Get(PartsList,x#)
            Print(rpt:detail2)
        End !x# = 1 To Records(PartsList)
        IF Records(PartsList) = 0
          Print(rpt:Detail1)
        END
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:RETDESNO.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE - RETAIL')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE - RETAIL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  !printed by
  set(defaults)
  access:defaults.next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  access:retsales.clearkey(ret:invoice_number_key)
  ret:invoice_number = inv:invoice_number
  access:retsales.tryfetch(ret:invoice_number_key)
  
  If inv:UseAlternativeAddress
      Use_Euro = True
  Else !inv:UseAlternativeAddress
      Use_Euro = FALSE
  End !inv:UseAlternativeAddress
  
  
  IF inv:AccountType = 'MAI'
    Access:TradeAcc.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = inv:Account_Number
    IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
      !Error!
    ELSE
    END
  ELSE
    Access:SubTracc.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = inv:Account_Number
    IF Access:SubTracc.Fetch(sub:Account_Number_Key)
      !Error!
    ELSE
    END
  END
  
  despatch_note_number_temp   = 'D-' & glo:select1
  If ret:payment_method = 'CAS'
      Delivery_Name_Temp  = RET:Contact_Name
      Invoice_Name_Temp   = ret:contact_name
  Else!If ret:payment_method = 'CAS'
      Delivery_Name_Temp  = RET:Company_Name_Delivery
      invoice_name_temp   = ret:company_name
  End!If ret:payment_method = 'CAS'
  If ret:building_name    = ''
      Invoice_address_Line_1     = Clip(RET:Address_Line1)
  Else!    If ret:building_name    = ''
      Invoice_address_Line_1     = Clip(RET:Building_Name) & ' ' & Clip(RET:Address_Line1)
  End!
  If ret:building_name_delivery   = ''
      Delivery_Address_Line_1_Temp    = Clip(RET:Address_Line1_Delivery)
  Else!    If ret:building_name_delivery   = ''
      Delivery_Address_Line_1_Temp    = Clip(RET:Building_Name_Delivery) & ' ' & Clip(RET:Address_Line1_Delivery)
  End!    If ret:building_name_delivery   = ''
  
  vat_rate_temp   = inv:vat_rate_retail
  
  IF Use_Euro = FALSE
      sub_total_temp  = inv:parts_paid
      courier_cost_temp   = inv:courier_paid
      vat_total_temp = Round((inv:courier_paid + inv:parts_paid) * vat_rate_temp/100,.01)
      total_temp  = inv:courier_paid + Round(vat_total_temp,.01) + inv:parts_paid
      tmp:CompanyName       = def:Invoice_Company_Name
      tmp:AddressLine1      = def:Invoice_Address_Line1
      tmp:AddressLine2      = def:Invoice_Address_Line2
      tmp:AddressLine3      = def:Invoice_Address_Line3
      tmp:Postcode          = def:Invoice_Postcode
      tmp:TelephoneNumber   = def:Invoice_Telephone_Number
      tmp:FaxNumber         = def:Invoice_Fax_Number
      tmp:EmailAddress      = def:InvoiceEmailAddress
      tmp:VatNumber         = def:Invoice_VAT_Number
  ELSE
     !Line removed as is now in loop.
  
      !sub_total_temp  = inv:parts_paid * inv:EuroExhangeRate
  
      !courier_cost_temp   = inv:courier_paid * inv:EuroExhangeRate
      !vat_total_temp = (Round(((inv:courier_paid * inv:EuroExhangeRate) + sub_total_temp) * vat_rate_temp/100,.01))
      !total_temp  = (inv:courier_paid * inv:EuroExhangeRate) + Round(vat_total_temp,.01) + (sub_total_temp)
  
      tmp:CompanyName       = DE2:Inv2CompanyName
      tmp:AddressLine1      = DE2:Inv2AddressLine1
      tmp:AddressLine2      = DE2:Inv2AddressLine2
      tmp:AddressLine3      = DE2:Inv2AddressLine3
      tmp:Postcode          = DE2:Inv2Postcode
      tmp:TelephoneNumber   = DE2:Inv2TelephoneNumber
      tmp:FaxNumber         = DE2:Inv2FaxNumber
      tmp:EmailAddress      = DE2:Inv2EmailAddress
      tmp:VatNumber         = DE2:Inv2VATNumber
  END
  !Payments
  
  x# = 0
  save_rtp_id = access:retpay.savefile()
  access:retpay.clearkey(rtp:date_key)
  rtp:ref_number   = ret:ref_number
  set(rtp:date_key,rtp:date_key)
  loop
      if access:retpay.next()
         break
      end !if
      if rtp:ref_number   <> ret:ref_number      |
          then break.  ! end if
  
      x# += 1
      If x# > 0 And x# < 5
          payment_date_temp[x#] = rtp:date
          payment_type_temp[x#] = rtp:payment_type
          IF Use_Euro = FALSE
            payment_amount_temp[x#] = rtp:amount
          ELSE
            payment_amount_temp[x#] = rtp:amount * inv:EuroExhangeRate
          END
      Else
          IF Use_Euro = FALSE
            other_amount_temp += rtp:amount
          ELSE
            other_amount_temp += rtp:amount * inv:EuroExhangeRate
          END
      End!If x# < 5
      IF Use_Euro = FALSE
        total_paid_temp += rtp:amount
      ELSE
        total_paid_temp += rtp:amount * inv:EuroExhangeRate
      END
  end !loop
  access:retpay.restorefile(save_rtp_id)
  
  settarget(report)
  If other_amount_temp <> 0
      Unhide(?others)
  Else!If other_amount_temp <> 0
      Hide(?others)
  End!If other_amount_temp <> 0
  If ret:Payment_Method = 'EXC'
      ?couriercost{prop:Hide} = 1
      ?totallinecost{prop:Hide} = 1
      ?vat{prop:Hide} = 1
      ?totalvat{prop:Hide} = 1
  
      ?courier_cost_temp{prop:Hide} = 1
      ?sub_total_temp{prop:Hide} = 1
      ?vat_total_temp{prop:Hide} = 1
      ?total_temp{prop:Hide} = 1
  
      ?tmpque:ExchangeValue{prop:Hide} = 0
      ?tmpque:LineCost{prop:Hide} = 1
  
      ?ExchangeValue{prop:Hide} = 0
      ?ItemCost{prop:Text} = 'Total'
      ?LineCost1{prop:Hide} = 1
      ?LineCost2{prop:Hide} = 1
      ?tmp:TotalExchangeValue{prop:Hide} = 0
      ?TotalExchangeValue{prop:Hide} = 0
      ?TotalExchangeLine{prop:Hide} = 0
      ?TotalExchangeLine2{prop:Hide} = 0
      ?WarrantyNote{prop:Hide} = 0
  Else !ret:Payment_Method = 'EXC'
      ?couriercost{prop:Hide} = 0
      ?totallinecost{prop:Hide} = 0
      ?vat{prop:Hide} = 0
      ?totalvat{prop:Hide} = 0
  
      ?courier_cost_temp{prop:Hide} = 0
      ?sub_total_temp{prop:Hide} = 0
      ?vat_total_temp{prop:Hide} = 0
      ?total_temp{prop:Hide} = 0
  
      ?tmpque:ExchangeValue{prop:Hide} = 1
      ?tmpque:LineCost{prop:Hide} = 0
  
      ?ExchangeValue{prop:Hide} = 1
      ?LineCost1{prop:Hide} = 0
      ?LineCost2{prop:Hide} = 0
      ?tmp:TotalExchangeValue{prop:Hide} = 1
      ?TotalExchangeValue{prop:Hide} = 1
      ?TotalExchangeLine{prop:Hide} = 1
      ?TotalExchangeLine2{prop:Hide} = 1
      ?WarrantyNote{prop:Hide} = 1
  End !ret:Payment_Method = 'EXC'
  IF Use_Euro = TRUE
      ?String100{prop:Hide} = FALSE
      ?String100:2{prop:Hide} = FALSE
      ?String100:3{prop:Hide} = FALSE
      ?String100:4{prop:Hide} = FALSE
  END
  
  settarget()
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE - RETAIL'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE - RETAIL'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Retail_Single_Invoice'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Retail_Single_Invoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Use_Euro',Use_Euro,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Retail_Single_Invoice',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:CompanyName',tmp:CompanyName,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:AddressLine1',tmp:AddressLine1,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:AddressLine2',tmp:AddressLine2,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:AddressLine3',tmp:AddressLine3,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:Postcode',tmp:Postcode,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:TelephoneNumber',tmp:TelephoneNumber,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:FaxNumber',tmp:FaxNumber,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:EmailAddress',tmp:EmailAddress,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:VATNumber',tmp:VATNumber,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Retail_Single_Invoice',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Retail_Single_Invoice',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Retail_Single_Invoice',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Retail_Single_Invoice',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Retail_Single_Invoice',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Retail_Single_Invoice',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Retail_Single_Invoice',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Retail_Single_Invoice',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Retail_Single_Invoice',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Retail_Single_Invoice',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Retail_Single_Invoice',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Retail_Single_Invoice',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Retail_Single_Invoice',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Retail_Single_Invoice',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Retail_Single_Invoice',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Retail_Single_Invoice',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Retail_Single_Invoice',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Retail_Single_Invoice',1)
    SolaceViewVars('InitialPath',InitialPath,'Retail_Single_Invoice',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Retail_Single_Invoice',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Retail_Single_Invoice',1)
    SolaceViewVars('save_rtp_id',save_rtp_id,'Retail_Single_Invoice',1)
    SolaceViewVars('vat_rate_temp',vat_rate_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('Delivery_Name_Temp',Delivery_Name_Temp,'Retail_Single_Invoice',1)
    SolaceViewVars('Delivery_Address_Line_1_Temp',Delivery_Address_Line_1_Temp,'Retail_Single_Invoice',1)
    SolaceViewVars('save_res_id',save_res_id,'Retail_Single_Invoice',1)
    SolaceViewVars('Invoice_Name_temp',Invoice_Name_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('Invoice_address_Line_1',Invoice_address_Line_1,'Retail_Single_Invoice',1)
    SolaceViewVars('items_total_temp',items_total_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('vat_temp',vat_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('item_cost_temp',item_cost_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('despatch_note_number_temp',despatch_note_number_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('line_cost_total_temp',line_cost_total_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('vat_total_temp',vat_total_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('Total_temp',Total_temp,'Retail_Single_Invoice',1)
    
      Loop SolaceDim1# = 1 to 4
        SolaceFieldName" = 'payment_date_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",payment_date_temp[SolaceDim1#],'Retail_Single_Invoice',1)
      End
    
    
    SolaceViewVars('other_amount_temp',other_amount_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('total_paid_temp',total_paid_temp,'Retail_Single_Invoice',1)
    
      Loop SolaceDim1# = 1 to 4
        SolaceFieldName" = 'payment_type_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",payment_type_temp[SolaceDim1#],'Retail_Single_Invoice',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 4
        SolaceFieldName" = 'payment_amount_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",payment_amount_temp[SolaceDim1#],'Retail_Single_Invoice',1)
      End
    
    
    SolaceViewVars('sub_total_temp',sub_total_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('courier_cost_temp',courier_cost_temp,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:TotalExchangeValue',tmp:TotalExchangeValue,'Retail_Single_Invoice',1)
    SolaceViewVars('tmp:ItemCost',tmp:ItemCost,'Retail_Single_Invoice',1)
    SolaceViewVars('Accessory_Cost',Accessory_Cost,'Retail_Single_Invoice',1)
    SolaceViewVars('PartsList:OrderNumber',PartsList:OrderNumber,'Retail_Single_Invoice',1)
    SolaceViewVars('PartsList:Quantity',PartsList:Quantity,'Retail_Single_Invoice',1)
    SolaceViewVars('PartsList:PartNumber',PartsList:PartNumber,'Retail_Single_Invoice',1)
    SolaceViewVars('PartsList:Description',PartsList:Description,'Retail_Single_Invoice',1)
    SolaceViewVars('PartsList:ExchangeValue',PartsList:ExchangeValue,'Retail_Single_Invoice',1)
    SolaceViewVars('PartsList:ItemCost',PartsList:ItemCost,'Retail_Single_Invoice',1)
    SolaceViewVars('PartsList:LineCost',PartsList:LineCost,'Retail_Single_Invoice',1)


BuildCtrlQueue      Routine







