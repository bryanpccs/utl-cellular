

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01016.INC'),ONCE        !Local module procedure declarations
                     END








Summary_Warranty_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Invoice_Company_Temp STRING(30)
courier_vat_temp     REAL
total_temp           REAL
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
Invoice_Number_Temp  STRING(28)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Labour_vat_info_temp STRING(15)
Parts_vat_info_temp  STRING(15)
batch_number_temp    STRING(28)
labour_vat_temp      REAL
parts_vat_temp       REAL
balance_temp         REAL
vat_temp             REAL
vat_total_temp       REAL
sub_total_temp       REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Account_Number)
                       PROJECT(inv:Batch_Number)
                       PROJECT(inv:Courier_Paid)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Invoice_Number)
                       PROJECT(inv:Labour_Paid)
                       PROJECT(inv:Parts_Paid)
                       PROJECT(inv:jobs_count)
                     END
Report               REPORT,AT(396,4573,7521,4271),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,3604),USE(?unnamed:3)
                         STRING('Date Of Invoice:'),AT(4948,781),USE(?String86),TRN,FONT(,8,,)
                         STRING('Invoice Number: '),AT(4948,365),USE(?String87),TRN,FONT(,8,,)
                         STRING(@s10),AT(5885,365),USE(inv:Invoice_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(156,1302),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('V.A.T. No: '),AT(4948,573),USE(?String102),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,573),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1302,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING(@d6b),AT(5885,781),USE(inv:Date_Created),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(1771,3229),USE(inv:Date_Created,,?INV:Date_Created:2),TRN,LEFT,FONT(,8,,)
                         STRING(@p<<<<<<#p),AT(4844,3229,469,208),USE(inv:Batch_Number),TRN,RIGHT,FONT(,8,,)
                         STRING('WARRANTY'),AT(6042,3229),USE(?String71),TRN,FONT(,8,,)
                         STRING(@p<<<<<<<<#p),AT(3385,3229),USE(inv:Invoice_Number,,?INV:Invoice_Number:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(208,2240),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(208,2396),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(469,2396),USE(job:Telephone_Number),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(1917,2396),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2198,2396),USE(job:Fax_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3229),USE(inv:Account_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(208,1615),USE(Invoice_Company_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(208,1760),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(208,2083),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(208,1917),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,198),USE(?DetailBand)
                           STRING(@n14.2),AT(3854,0),USE(inv:Labour_Paid),TRN,RIGHT
                           STRING(@n14.2),AT(5052,0),USE(inv:Parts_Paid),TRN,RIGHT
                           STRING(@n14.2),AT(6146,0),USE(vat_total_temp,,?vat_total_temp:2),TRN,RIGHT
                           STRING(@n14.2),AT(2708,0),USE(inv:Courier_Paid,,?INV:Courier_Paid:2),TRN,RIGHT
                           STRING('WARRANTY'),AT(1635,0),USE(?String72),TRN
                           STRING(@s8),AT(208,0),USE(inv:jobs_count),TRN,RIGHT
                         END
                         FOOTER,AT(396,9125,,1458),USE(?unnamed:2),TOGETHER,ABSOLUTE
                           STRING('Despatch Date:'),AT(208,208),USE(?String66),TRN,FONT(,8,,)
                           STRING('Courier: '),AT(208,365),USE(?String67),TRN,FONT(,8,,)
                           STRING('Labour Cost:'),AT(4844,208,885,208),USE(?String90),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6406,52,833,188),USE(inv:Courier_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6406,208),USE(inv:Labour_Paid,,?INV:Labour_Paid:2),TRN,RIGHT,FONT(,8,,)
                           STRING('Consignment No:'),AT(208,521),USE(?String70),TRN,FONT(,8,,)
                           STRING('Despatched By:'),AT(208,677),USE(?String68),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6406,365),USE(inv:Parts_Paid,,?INV:Parts_Paid:2),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(4844,677),USE(?String94),TRN,FONT(,8,,)
                           STRING('Total:'),AT(4844,833),USE(?String95),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(6302,781,1000,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING(@n14.2),AT(6406,833),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING('<128>'),AT(6344,833),USE(?Euro),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(6302,469,1000,0),USE(?Line2:2),COLOR(COLOR:Black)
                           STRING(@n14.2),AT(6406,677),USE(vat_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Sub Total:'),AT(4844,521),USE(?String75),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6406,521),USE(sub_total_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Courier Cost'),AT(4844,52),USE(?String65),TRN,FONT(,8,,)
                           STRING('Parts Cost:'),AT(4844,365),USE(?String92),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1156)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(406,469,7521,11198),USE(?unnamed)
                         IMAGE('rinvdet.gif'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,83,3844,240),USE(def:Invoice_Company_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('INVOICE'),AT(5604,0),USE(?String20),TRN,FONT(,20,,FONT:bold)
                         STRING(@s30),AT(156,323,3844,156),USE(def:Invoice_Address_Line1),TRN
                         STRING(@s30),AT(156,479,3844,156),USE(def:Invoice_Address_Line2),TRN
                         STRING(@s30),AT(156,635,3844,156),USE(def:Invoice_Address_Line3),TRN
                         STRING(@s15),AT(156,802,1156,156),USE(def:Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s15),AT(677,958),USE(def:Invoice_Telephone_Number)
                         STRING('Fax: '),AT(156,1094),USE(?String19),TRN
                         STRING(@s15),AT(677,1094),USE(def:Invoice_Fax_Number)
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN
                         STRING(@s255),AT(677,1250,3844,208),USE(def:InvoiceEmailAddress),TRN
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY DETAILS'),AT(156,8479),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),TRN,FONT(,9,,FONT:bold)
                         STRING('No Of Jobs'),AT(156,3854),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(208,3177),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Invoice Date'),AT(1667,3177),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Invoice Number'),AT(3125,3177),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Batch Number'),AT(4583,3177),USE(?String40:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(6042,3177),USE(?String40:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Charge Type'),AT(1635,3854),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Courier Cost'),AT(3031,3854),USE(?String69),TRN,FONT(,8,,FONT:bold)
                         STRING('Labour Cost'),AT(4208,3854),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('Parts Cost'),AT(5510,3854),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('V.A.T.'),AT(6875,3854),USE(?String44),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Summary_Warranty_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBS.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:DETAIL)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = inv:account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key) 
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          if tra:invoice_sub_accounts = 'YES'
              invoice_company_temp   = sub:company_name
              invoice_address1_temp  = sub:address_line1
              invoice_address2_temp  = sub:address_line2
              If sup:address_line3 = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = sub:address_line3
                  invoice_address4_temp  = sub:postcode
              End
          else!if tra:use_sub_accounts = 'YES'
              invoice_company_temp   = tra:company_name
              invoice_address1_temp  = tra:address_line1
              invoice_address2_temp  = tra:address_line2
              If sup:address_line3 = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = tra:address_line3
                  invoice_address4_temp  = tra:postcode
              End
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
  
  
  labour_vat_temp      = Round(inv:labour_paid * (INV:Vat_Rate_labour/100),.01)
  parts_vat_temp      = Round(inv:parts_paid * (INV:Vat_Rate_Parts/100),.01)
  courier_vat_temp     = Round(inv:courier_paid * (inv:vat_rate_labour/100),.01)
  sub_total_temp       = Round(inv:labour_paid,.01) + Round(inv:parts_paid,.01) + Round(inv:courier_paid,.01)
  vat_total_temp      = Round(labour_vat_temp,.01) + Round(parts_vat_temp,.01) + Round(courier_vat_temp,.01)
  total_temp           = Round(inv:labour_paid,.01) + Round(inv:parts_paid,.01) + Round(vat_total_temp,.01) + Round(inv:courier_paid,.01)
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Summary_Warranty_Invoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Summary_Warranty_Invoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Default_Invoice_Company_Name_Temp',Default_Invoice_Company_Name_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Summary_Warranty_Invoice',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line1_Temp',Default_Invoice_Address_Line1_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line2_Temp',Default_Invoice_Address_Line2_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line3_Temp',Default_Invoice_Address_Line3_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Default_Invoice_Postcode_Temp',Default_Invoice_Postcode_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Default_Invoice_Telephone_Number_Temp',Default_Invoice_Telephone_Number_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Default_Invoice_Fax_Number_Temp',Default_Invoice_Fax_Number_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Default_Invoice_VAT_Number_Temp',Default_Invoice_VAT_Number_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Summary_Warranty_Invoice',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Summary_Warranty_Invoice',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Summary_Warranty_Invoice',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Summary_Warranty_Invoice',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Summary_Warranty_Invoice',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Summary_Warranty_Invoice',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Summary_Warranty_Invoice',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Summary_Warranty_Invoice',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Summary_Warranty_Invoice',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Summary_Warranty_Invoice',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Summary_Warranty_Invoice',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Summary_Warranty_Invoice',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Summary_Warranty_Invoice',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Summary_Warranty_Invoice',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Summary_Warranty_Invoice',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Summary_Warranty_Invoice',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Summary_Warranty_Invoice',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Summary_Warranty_Invoice',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Summary_Warranty_Invoice',1)
    SolaceViewVars('InitialPath',InitialPath,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Summary_Warranty_Invoice',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Invoice_Company_Temp',Invoice_Company_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('courier_vat_temp',courier_vat_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('total_temp',total_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Invoice_address1_temp',Invoice_address1_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('invoice_address2_temp',invoice_address2_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('invoice_address3_temp',invoice_address3_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('invoice_address4_temp',invoice_address4_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Invoice_Number_Temp',Invoice_Number_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Labour_vat_info_temp',Labour_vat_info_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('Parts_vat_info_temp',Parts_vat_info_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('batch_number_temp',batch_number_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('labour_vat_temp',labour_vat_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('parts_vat_temp',parts_vat_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('balance_temp',balance_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('vat_temp',vat_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('vat_total_temp',vat_total_temp,'Summary_Warranty_Invoice',1)
    SolaceViewVars('sub_total_temp',sub_total_temp,'Summary_Warranty_Invoice',1)


BuildCtrlQueue      Routine







