

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01008.INC'),ONCE        !Local module procedure declarations
                     END








Job_Receipt PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:accessories      STRING(255)
tmp:PrintedBy        STRING(255)
save_jea_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
received_by_temp     STRING(60)
fault_code_field_temp STRING(20),DIM(12)
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(70)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
engineer_temp        STRING(30)
part_type_temp       STRING(4)
amount_received_temp REAL
customer_name_temp   STRING(40)
exchange_unit_number_temp STRING(20)
exchange_model_number STRING(30)
exchange_manufacturer_temp STRING(30)
exchange_unit_type_temp STRING(30)
exchange_esn_temp    STRING(16)
exchange_msn_temp    STRING(15)
exchange_unit_title_temp STRING(15)
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Account_Number)
                       PROJECT(job_ali:Charge_Type)
                       PROJECT(job_ali:Company_Name_Delivery)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Incoming_Consignment_Number)
                       PROJECT(job_ali:Incoming_Courier)
                       PROJECT(job_ali:Incoming_Date)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Order_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Repair_Type)
                       PROJECT(job_ali:Repair_Type_Warranty)
                       PROJECT(job_ali:Telephone_Delivery)
                       PROJECT(job_ali:Transit_Type)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:Warranty_Charge_Type)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:time_booked)
                     END
Report               REPORT,AT(396,4521,7521,6708),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,3438),USE(?unnamed)
                         STRING('Job Number: '),AT(4917,396),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s12),AT(5917,396),USE(job_ali:Ref_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Booked: '),AT(4917,760),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5917,760),USE(job_ali:date_booked),TRN,FONT(,8,,FONT:bold)
                         STRING(@t1),AT(6604,760),USE(job_ali:time_booked),TRN,FONT(,8,,FONT:bold)
                         STRING(@s40),AT(156,1563),USE(customer_name_temp),TRN,FONT(,8,,)
                         STRING(@s40),AT(4083,1563),USE(customer_name_temp,,?customer_name_temp:2),TRN,FONT(,8,,)
                         STRING(@s20),AT(1604,3240),USE(job_ali:Charge_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4604,3240),USE(job_ali:Warranty_Charge_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3083,3240),USE(job_ali:Repair_Type),TRN,FONT(,8,,)
                         STRING(@s20),AT(6000,3240),USE(job_ali:Repair_Type_Warranty),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4083,2500),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2500),USE(job_ali:Telephone_Delivery),FONT(,8,,)
                         STRING('Acc No:'),AT(2188,1719),USE(?String78),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(156,2500),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(417,2500),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(2188,2500),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2656,2500),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(2656,1719),USE(job_ali:Account_Number,,?job_ali:Account_Number:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3240),USE(job_ali:Order_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1719),USE(job_ali:Company_Name_Delivery),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,1875,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2031),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2188),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4083,2344),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,6198),USE(?DetailBand)
                           STRING(@s30),AT(156,83,1000,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(1635,83,1323,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(3125,83,1396,156),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(4563,83,1396,156),USE(job_ali:ESN),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(6083,83),USE(job_ali:MSN),TRN,FONT(,8,,)
                           STRING(@s15),AT(6094,729),USE(exchange_msn_temp),TRN,FONT(,8,,)
                           STRING(@s16),AT(4583,729,1396,156),USE(exchange_esn_temp),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(3125,729,1396,156),USE(exchange_unit_type_temp),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(1615,729,1323,156),USE(exchange_manufacturer_temp),TRN,LEFT,FONT(,8,,)
                           STRING(@s30),AT(156,729,1000,156),USE(exchange_model_number),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(156,469),USE(exchange_unit_title_temp),TRN,LEFT,FONT(,9,,FONT:bold)
                           STRING(@s20),AT(1354,469),USE(exchange_unit_number_temp),TRN,LEFT
                           STRING('ACCESSORIES'),AT(156,2656),USE(?String63),TRN,FONT(,9,,FONT:bold)
                           TEXT,AT(1927,2656,5313,469),USE(tmp:accessories),TRN,FONT(,8,,)
                           STRING('Estimate Required If Repair Costs Exceed:'),AT(156,3333),USE(?estimate_text),TRN,HIDE,FONT(,8,,)
                           STRING(@s70),AT(2344,3281,3917,240),USE(estimate_value_temp),TRN,HIDE,FONT(,8,,)
                           STRING('Customer Signature'),AT(156,3760),USE(?String82),TRN
                           STRING('Date: {13}/ {14}/'),AT(5156,3760),USE(?String83),TRN
                           LINE,AT(1396,4000,3677,0),USE(?Line1),COLOR(COLOR:Black)
                           LINE,AT(5604,4000,1521,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING('Initial Transit Type: '),AT(156,4740),USE(?String66),TRN,FONT(,8,,)
                           STRING(@s20),AT(1250,4740),USE(job_ali:Transit_Type),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING('Courier: '),AT(156,4896),USE(?String67),TRN,FONT(,8,,)
                           STRING(@s30),AT(1240,4896,1302,188),USE(job_ali:Incoming_Courier),TRN,FONT(,8,,FONT:bold)
                           STRING('Consignment No:'),AT(156,5052),USE(?String70),TRN,FONT(,8,,)
                           STRING(@s30),AT(1240,5052,1302,188),USE(job_ali:Incoming_Consignment_Number),TRN,FONT(,8,,FONT:bold)
                           STRING('Date Received: '),AT(156,5208),USE(?String68),TRN,FONT(,8,,)
                           STRING(@d6b),AT(1240,5208),USE(job_ali:Incoming_Date),TRN,FONT(,8,,FONT:bold)
                           STRING('Received By: '),AT(156,5365),USE(?String69),TRN,FONT(,8,,)
                           STRING(@s22),AT(1250,5365),USE(received_by_temp),TRN,FONT(,8,,FONT:bold)
                           STRING('REPORTED FAULT'),AT(156,1250),USE(?String64),TRN,FONT(,9,,FONT:bold)
                           TEXT,AT(1927,1250,5313,625),USE(jbn_ali:Fault_Description),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING('ENGINEER REPORT'),AT(156,1927),USE(?String64:2),TRN,FONT(,9,,FONT:bold)
                           TEXT,AT(1927,1927,5313,625),USE(jbn_ali:Invoice_Text),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s16),AT(2885,5177,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@s24),AT(2885,5375,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING(@s20),AT(3146,4948),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Amount:'),AT(4792,5052),USE(?amount_text),TRN,FONT(,8,,)
                           STRING('<128>'),AT(5625,5052,52,208),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                           STRING(@n-14.2b),AT(5729,5052),USE(amount_received_temp),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(2885,4740,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1156)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198)
                         IMAGE('rinvdet.gif'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('JOB RECEIPT'),AT(5917,0,1521,240),USE(?String20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),TRN,FONT(,9,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,990),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,990),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1146),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1146),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1302,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1302),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('INVOICE ADDRESS'),AT(156,1479),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4000,1521,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING('GOODS RECEIVED DETAILS'),AT(156,8479),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('PAYMENT RECEIVED (INC. V.A.T.)'),AT(4792,8490),USE(?String73:2),TRN,FONT(,9,,FONT:bold)
                         STRING('Model'),AT(156,3844),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(156,3156),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(1604,3156),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(3083,3156),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Type'),AT(4604,3156),USE(?String40:5),TRN,FONT(,8,,FONT:bold)
                         STRING('Warranty Repair Type'),AT(6000,3156),USE(?String40:6),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1635,3844),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3125,3844),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4563,3844),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6083,3844),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3677),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Job_Receipt')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBEXACC.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:JOBPAYMT.Open
  Relate:STANTEXT.Open
  Access:JOBACC.UseFile
  Access:JOBS.UseFile
  Access:JOBNOTES.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:DETAIL)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
    Relate:JOBEXACC.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:JOBPAYMT.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','JOB RECEIPT')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'JOB RECEIPT'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  set(defaults)
  access:defaults.next()
  
  if job_ali:title = '' and job_ali:initial = ''
      customer_name_temp = clip(job_ali:surname)
  elsif job_ali:title = '' and job_ali:initial <> ''
      customer_name_temp = clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial = ''
      customer_name_temp = clip(job_ali:title) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial <> ''
      customer_name_temp = clip(job_ali:title) & ' ' & clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  else
      customer_name_temp = ''
  end
  
  Access:JobNotes_ALias.ClearKey(jbn_ali:RefNumberKey)
  jbn_ali:RefNumber = job_ali:Ref_Number
  IF Access:JobNotes_Alias.Fetch(jbn_ali:RefNumberKey)
    !Error!
  END
  
  delivery_address1_temp     = job_ali:address_line1_delivery
  delivery_address2_temp     = job_ali:address_line2_delivery
  If job_ali:address_line3_delivery   = ''
      delivery_address3_temp = job_ali:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp  = job_ali:address_line3_delivery
      delivery_address4_temp  = job_ali:postcode_delivery
  End
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job_ali:account_number
  access:subtracc.fetch(sub:account_number_key)
  access:tradeacc.clearkey(tra:account_number_key) 
  tra:account_number = sub:main_account_number
  access:tradeacc.fetch(tra:account_number_key)
  if tra:invoice_sub_accounts = 'YES'
      If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = job_ali:address_line1
          invoice_address2_temp     = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job_ali:address_line3
              invoice_address4_temp  = job_ali:postcode
          End
          invoice_company_name_temp   = job_ali:company_name
          invoice_telephone_number_temp   = job_ali:telephone_number
          invoice_fax_number_temp = job_ali:fax_number
      Else!If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = sub:address_line1
          invoice_address2_temp     = sub:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = sub:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = sub:address_line3
              invoice_address4_temp  = sub:postcode
          End
          invoice_company_name_temp   = sub:company_name
          invoice_telephone_number_temp   = sub:telephone_number
          invoice_fax_number_temp = sub:fax_number
  
  
      End!If sub:invoice_customer_address = 'YES'
      
  else!if tra:use_sub_accounts = 'YES'
      If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = job_ali:address_line1
          invoice_address2_temp     = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job_ali:address_line3
              invoice_address4_temp  = job_ali:postcode
          End
          invoice_company_name_temp   = job_ali:company_name
          invoice_telephone_number_temp   = job_ali:telephone_number
          invoice_fax_number_temp = job_ali:fax_number
  
      Else!If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = tra:address_line1
          invoice_address2_temp     = tra:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = tra:address_line3
              invoice_address4_temp  = tra:postcode
          End
          invoice_company_name_temp   = tra:company_name
          invoice_telephone_number_temp   = tra:telephone_number
          invoice_fax_number_temp = tra:fax_number
  
      End!If tra:invoice_customer_address = 'YES'
  End!!if tra:use_sub_accounts = 'YES'
  
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'JOB RECEIPT'
  access:stantext.fetch(stt:description_key)
  
  If job_ali:exchange_unit_number <> ''
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job_ali:exchange_unit_number
      if access:exchange.fetch(xch:ref_number_key) = Level:Benign
          exchange_unit_number_temp  = 'Unit: ' & Clip(job_ali:exchange_unit_number)
          exchange_model_number      = xch:model_number
          exchange_manufacturer_temp = xch:manufacturer
          exchange_unit_type_temp    = 'N/A'
          exchange_esn_temp          = xch:esn
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = xch:msn
      end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
  Else!If job_ali:exchange_unit_number <> ''
      x# = 0
      save_jea_id = access:jobexacc.savefile()
      access:jobexacc.clearkey(jea:part_number_key)
      jea:job_ref_number = job_ali:ref_number
      set(jea:part_number_key,jea:part_number_key)
      loop
          if access:jobexacc.next()
             break
          end !if
          if jea:job_ref_number <> job_ali:ref_number      |
              then break.  ! end if
          x# = 1
          Break
      end !loop
      access:jobexacc.restorefile(save_jea_id)
      If x# = 1
          exchange_unit_number_temp  = ''
          exchange_model_number      = 'N/A'
          exchange_manufacturer_temp = job_ali:manufacturer
          exchange_unit_type_temp    = 'ACCESSORY'
          exchange_esn_temp          = 'N/A'
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = 'N/A'
      End!If x# = 1
  End!If job_ali:exchange_unit_number <> ''
  
  
  x# = 1
  setcursor(cursor:wait)
  tmp:accessories= ''
  save_jac_id = access:jobacc.savefile()
  access:jobacc.clearkey(jac:ref_number_key)
  jac:ref_number = job_ali:ref_number
  set(jac:ref_number_key,jac:ref_number_key)
  loop
      if access:jobacc.next()
         break
      end !if
      if jac:ref_number <> job_ali:ref_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      If tmp:accessories = ''
          tmp:accessories = jac:accessory
      Else!If tmp:accessories = ''
          tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
      End!If tmp:accessories = ''
  end !loop
  access:jobacc.restorefile(save_jac_id)
  setcursor()
  
  
  
  If job_ali:estimate = 'YES'
      Settarget(report)
      Unhide(?estimate_text)
      Unhide(?estimate_value_temp)
      estimate_value_temp = Clip(Format(job_ali:Estimate_If_Over,@n14.2)) & ' (Plus VAT)'
      Settarget()
  End!If job_ali:estimate = 'YES'
  
  access:users.clearkey(use:password_key)
  use:password = job_ali:despatch_user
  access:users.fetch(use:password_key)
  received_by_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  
  If job_ali:chargeable_job = 'YES'
      setcursor(cursor:wait)
      save_jpt_id = access:jobpaymt.savefile()
      access:jobpaymt.clearkey(jpt:all_date_key)
      jpt:ref_number = job_ali:ref_number
      set(jpt:all_date_key,jpt:all_date_key)
      loop
          if access:jobpaymt.next()
             break
          end !if
          if jpt:ref_number <> job_ali:ref_number      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          amount_received_temp += jpt:amount
      end !loop
      access:jobpaymt.restorefile(save_jpt_id)
      setcursor()
  Else!If job_ali:chargeable_job = 'YES'
      Settarget(report)
      Hide(?amount_text)
      Settarget()
  End!If job_ali:chargeable_job = 'YES'
  
  !Added by Neil 03/07/2001!
  
  IF job_ali:Chargeable_Job <> 'YES'
    Settarget(report)
    Hide(?job_ali:Charge_Type)
    Hide(?String40:3)
    Settarget()
  END
  !Barcode Bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'Job No: ' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'E.S.N.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'JOB RECEIPT'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'JOB RECEIPT'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Job_Receipt'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Job_Receipt',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Job_Receipt',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'Job_Receipt',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Job_Receipt',1)
    SolaceViewVars('save_jea_id',save_jea_id,'Job_Receipt',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Job_Receipt',1)
    SolaceViewVars('save_jpt_id',save_jpt_id,'Job_Receipt',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Job_Receipt',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Job_Receipt',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Job_Receipt',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Job_Receipt',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Job_Receipt',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Job_Receipt',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Job_Receipt',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Job_Receipt',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Job_Receipt',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Job_Receipt',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Job_Receipt',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Job_Receipt',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Job_Receipt',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Job_Receipt',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Job_Receipt',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Job_Receipt',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Job_Receipt',1)
    SolaceViewVars('InitialPath',InitialPath,'Job_Receipt',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Job_Receipt',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Job_Receipt',1)
    SolaceViewVars('code_temp',code_temp,'Job_Receipt',1)
    SolaceViewVars('received_by_temp',received_by_temp,'Job_Receipt',1)
    
      Loop SolaceDim1# = 1 to 12
        SolaceFieldName" = 'fault_code_field_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",fault_code_field_temp[SolaceDim1#],'Job_Receipt',1)
      End
    
    
    SolaceViewVars('option_temp',option_temp,'Job_Receipt',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Job_Receipt',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Job_Receipt',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Job_Receipt',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Job_Receipt',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Job_Receipt',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Job_Receipt',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Job_Receipt',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Job_Receipt',1)
    SolaceViewVars('Delivery_Address1_Temp',Delivery_Address1_Temp,'Job_Receipt',1)
    SolaceViewVars('Delivery_address2_temp',Delivery_address2_temp,'Job_Receipt',1)
    SolaceViewVars('Delivery_address3_temp',Delivery_address3_temp,'Job_Receipt',1)
    SolaceViewVars('Delivery_address4_temp',Delivery_address4_temp,'Job_Receipt',1)
    SolaceViewVars('Invoice_Company_Temp',Invoice_Company_Temp,'Job_Receipt',1)
    SolaceViewVars('Invoice_address1_temp',Invoice_address1_temp,'Job_Receipt',1)
    SolaceViewVars('invoice_address2_temp',invoice_address2_temp,'Job_Receipt',1)
    SolaceViewVars('invoice_address3_temp',invoice_address3_temp,'Job_Receipt',1)
    SolaceViewVars('invoice_address4_temp',invoice_address4_temp,'Job_Receipt',1)
    
      Loop SolaceDim1# = 1 to 6
        SolaceFieldName" = 'accessories_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",accessories_temp[SolaceDim1#],'Job_Receipt',1)
      End
    
    
    SolaceViewVars('estimate_value_temp',estimate_value_temp,'Job_Receipt',1)
    SolaceViewVars('despatched_user_temp',despatched_user_temp,'Job_Receipt',1)
    SolaceViewVars('vat_temp',vat_temp,'Job_Receipt',1)
    SolaceViewVars('total_temp',total_temp,'Job_Receipt',1)
    SolaceViewVars('part_number_temp',part_number_temp,'Job_Receipt',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Job_Receipt',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Job_Receipt',1)
    SolaceViewVars('esn_temp',esn_temp,'Job_Receipt',1)
    SolaceViewVars('charge_type_temp',charge_type_temp,'Job_Receipt',1)
    SolaceViewVars('repair_type_temp',repair_type_temp,'Job_Receipt',1)
    SolaceViewVars('labour_temp',labour_temp,'Job_Receipt',1)
    SolaceViewVars('parts_temp',parts_temp,'Job_Receipt',1)
    SolaceViewVars('courier_cost_temp',courier_cost_temp,'Job_Receipt',1)
    SolaceViewVars('Quantity_temp',Quantity_temp,'Job_Receipt',1)
    SolaceViewVars('Description_temp',Description_temp,'Job_Receipt',1)
    SolaceViewVars('Cost_Temp',Cost_Temp,'Job_Receipt',1)
    SolaceViewVars('engineer_temp',engineer_temp,'Job_Receipt',1)
    SolaceViewVars('part_type_temp',part_type_temp,'Job_Receipt',1)
    SolaceViewVars('amount_received_temp',amount_received_temp,'Job_Receipt',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Job_Receipt',1)
    SolaceViewVars('exchange_unit_number_temp',exchange_unit_number_temp,'Job_Receipt',1)
    SolaceViewVars('exchange_model_number',exchange_model_number,'Job_Receipt',1)
    SolaceViewVars('exchange_manufacturer_temp',exchange_manufacturer_temp,'Job_Receipt',1)
    SolaceViewVars('exchange_unit_type_temp',exchange_unit_type_temp,'Job_Receipt',1)
    SolaceViewVars('exchange_esn_temp',exchange_esn_temp,'Job_Receipt',1)
    SolaceViewVars('exchange_msn_temp',exchange_msn_temp,'Job_Receipt',1)
    SolaceViewVars('exchange_unit_title_temp',exchange_unit_title_temp,'Job_Receipt',1)
    SolaceViewVars('invoice_company_name_temp',invoice_company_name_temp,'Job_Receipt',1)
    SolaceViewVars('invoice_telephone_number_temp',invoice_telephone_number_temp,'Job_Receipt',1)
    SolaceViewVars('invoice_fax_number_temp',invoice_fax_number_temp,'Job_Receipt',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Job_Receipt',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Job_Receipt',1)


BuildCtrlQueue      Routine







