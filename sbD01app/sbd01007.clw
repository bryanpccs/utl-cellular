

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01007.INC'),ONCE        !Local module procedure declarations
                     END








Despatch_Note PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:PrintedBy        STRING(255)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Delivery_Address1_Temp STRING(30)
Delivery_address2_temp STRING(30)
Delivery_address3_temp STRING(30)
Delivery_address4_temp STRING(30)
Invoice_Company_Temp STRING(30)
Invoice_address1_temp STRING(30)
invoice_address2_temp STRING(30)
invoice_address3_temp STRING(30)
invoice_address4_temp STRING(30)
accessories_temp     STRING(30),DIM(6)
estimate_value_temp  STRING(40)
despatched_user_temp STRING(40)
vat_temp             REAL
total_temp           REAL
part_number_temp     STRING(30)
line_cost_temp       REAL
job_number_temp      STRING(20)
esn_temp             STRING(24)
charge_type_temp     STRING(22)
repair_type_temp     STRING(22)
labour_temp          REAL
parts_temp           REAL
courier_cost_temp    REAL
Quantity_temp        REAL
Description_temp     STRING(30)
Cost_Temp            REAL
customer_name_temp   STRING(60)
sub_total_temp       REAL
invoice_company_name_temp STRING(30)
invoice_telephone_number_temp STRING(15)
invoice_fax_number_temp STRING(15)
tmp:LoanExchangeUnit STRING(100)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:accessories      STRING(255)
tmp:OriginalIMEI     STRING(30)
tmp:FinalIMEI        STRING(20)
tmp:DefaultEmailAddress STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Account_Number)
                       PROJECT(job_ali:Authority_Number)
                       PROJECT(job_ali:Company_Name_Delivery)
                       PROJECT(job_ali:Consignment_Number)
                       PROJECT(job_ali:Courier)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Date_Despatched)
                       PROJECT(job_ali:Despatch_Number)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Order_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Telephone_Delivery)
                       PROJECT(job_ali:Time_Completed)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:time_booked)
                     END
Report               REPORT,AT(396,6938,7521,1948),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,6521),USE(?unnamed)
                         STRING('Job Number: '),AT(4917,396),USE(?String25),TRN,FONT(,8,,)
                         STRING(@s12),AT(6156,396),USE(job_ali:Ref_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Booked: '),AT(4917,719),USE(?String58),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6156,719),USE(job_ali:date_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6760,719),USE(job_ali:time_booked),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING('Date Completed:'),AT(4917,875),USE(?String85),TRN,FONT(,8,,)
                         STRING('Despatch Batch No:'),AT(4917,563),USE(?String86),TRN,FONT(,8,,)
                         STRING(@d6b),AT(6156,875,635,198),USE(job_ali:Date_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6760,875),USE(job_ali:Time_Completed),TRN,RIGHT,FONT(,8,,FONT:bold)
                         STRING(@s60),AT(156,1563),USE(customer_name_temp),TRN,FONT(,8,,)
                         STRING(@s60),AT(4115,1563),USE(customer_name_temp,,?customer_name_temp:2),TRN,FONT(,8,,)
                         STRING(@s10b),AT(6156,563),USE(job_ali:Despatch_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s22),AT(1604,3240),USE(job_ali:Order_Number,,?job_ali:Order_Number:2),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(4531,3229),USE(charge_type_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(3156,3240),USE(job_ali:Authority_Number),TRN,FONT(,8,,)
                         STRING(@s20),AT(6000,3240),USE(repair_type_temp),TRN,FONT(,8,,)
                         STRING(@s20),AT(156,3917,1000,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(5052,3906,1094,156),USE(tmp:FinalIMEI),TRN,HIDE,LEFT,FONT(,8,,)
                         STRING(@s20),AT(1250,3906,1323,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(2604,3906,1396,156),USE(job_ali:Unit_Type),TRN,LEFT,FONT(,8,,)
                         STRING(@s30),AT(3906,3906,1094,156),USE(tmp:OriginalIMEI),TRN,LEFT,FONT(,8,,)
                         STRING(@s20),AT(6198,3906),USE(job_ali:MSN),TRN,FONT(,8,,)
                         STRING('REPORTED FAULT'),AT(156,4167),USE(?String64),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,4167,5573,469),USE(jbn_ali:Fault_Description),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('ENGINEER REPORT'),AT(156,4740),USE(?String88),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,4740,5573,677),USE(jbn_ali:Invoice_Text),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('EXCHANGE UNIT'),AT(156,5885),USE(?Exchange_Unit),TRN,HIDE,FONT(,9,,FONT:bold)
                         STRING(@s100),AT(1615,5885,5781,208),USE(tmp:LoanExchangeUnit),TRN,HIDE,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('ACCESSORIES'),AT(156,5521),USE(?String105),TRN,FONT(,9,,FONT:bold)
                         TEXT,AT(1615,5521,5573,313),USE(tmp:accessories),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('PARTS USED'),AT(156,6094),USE(?Parts_Used),TRN,FONT(,9,,FONT:bold)
                         STRING('Qty'),AT(1521,6094),USE(?Qty),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(1917,6094),USE(?Part_Number),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(3698,6094),USE(?description),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Cost'),AT(5719,6094),USE(?Unit_Cost),TRN,FONT(,8,,FONT:bold)
                         STRING('Line Cost'),AT(6677,6094),USE(?Line_Cost),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(1406,6250,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),TRN,FONT(,8,,)
                         STRING('Tel: '),AT(4115,2500),USE(?String32:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2500),USE(job_ali:Telephone_Delivery),FONT(,8,,)
                         STRING(@s15),AT(2500,2188),USE(job_ali:Mobile_Number),TRN,LEFT,FONT(,8,,)
                         STRING('Mobile:'),AT(2135,2188),USE(?String35:3),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(2135,1875),USE(?String34:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2500,1875),USE(invoice_telephone_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING('Fax: '),AT(2135,2031),USE(?String35:2),TRN,FONT(,8,,)
                         STRING(@s15),AT(2500,2031),USE(invoice_fax_number_temp),TRN,LEFT,FONT(,8,,)
                         STRING(@s15),AT(156,3240),USE(job_ali:Account_Number),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1719),USE(job_ali:Company_Name_Delivery),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,1875,1917,156),USE(Delivery_Address1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2031),USE(Delivery_address2_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2188),USE(Delivery_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(4115,2344),USE(Delivery_address4_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,115),USE(?DetailBand)
                           STRING(@n8b),AT(1198,0),USE(Quantity_temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@s25),AT(1917,0),USE(part_number_temp),TRN,FONT(,7,,)
                           STRING(@s25),AT(3677,0),USE(Description_temp),TRN,FONT(,7,,)
                           STRING(@n14.2b),AT(5531,0),USE(Cost_Temp),TRN,RIGHT,FONT(,7,,)
                           STRING(@n14.2b),AT(6521,0),USE(line_cost_temp),TRN,RIGHT,FONT(,7,,)
                         END
                         FOOTER,AT(396,9198,,1479),USE(?unnamed:2),TOGETHER,ABSOLUTE
                           STRING('Despatch Date:'),AT(156,83),USE(?String66),TRN,FONT(,8,,)
                           STRING(@d6b),AT(1156,83),USE(job_ali:Date_Despatched),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING('Courier: '),AT(156,240),USE(?String67),TRN,FONT(,8,,)
                           STRING(@s20),AT(1156,240),USE(job_ali:Courier),TRN,FONT(,8,,FONT:bold)
                           STRING('Labour:'),AT(4844,83),USE(?labour_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,83),USE(labour_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Consignment No:'),AT(156,396),USE(?String70),TRN,FONT(,8,,)
                           STRING(@s20),AT(1156,396),USE(job_ali:Consignment_Number),TRN,FONT(,8,,FONT:bold)
                           STRING(@s20),AT(3125,281),USE(job_number_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(2875,83,1760,198),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Carriage:'),AT(4844,396),USE(?carriage_string),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,240),USE(parts_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('V.A.T.'),AT(4844,563),USE(?vat_String),TRN,FONT(,8,,)
                           STRING(@n14.2b),AT(6396,396),USE(courier_cost_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Total:'),AT(4844,729),USE(?total_string),TRN,FONT(,8,,FONT:bold)
                           LINE,AT(6281,719,1000,0),USE(?line),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6396,729),USE(total_temp),TRN,RIGHT,FONT(,8,,FONT:bold)
                           STRING(@s16),AT(2875,521,1760,198),USE(Bar_Code2_Temp),CENTER,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@n14.2b),AT(6396,563),USE(vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('<128>'),AT(6250,729,156,208),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                           STRING(@s24),AT(2875,719,1760,240),USE(esn_temp),TRN,CENTER,FONT(,8,,FONT:bold)
                           STRING('Parts:'),AT(4844,240),USE(?parts_string),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10156,7521,1156)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('DESPATCH NOTE'),AT(4167,0,3281,260),USE(?Title),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),TRN,FONT(,9,,FONT:bold)
                         STRING(@s30),AT(156,240,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,396,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,563,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,719,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,938),USE(?Telephone),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,917),USE(tmp:DefaultTelephone),FONT(,9,,)
                         STRING('Fax:'),AT(156,1094,260,208),USE(?Fax),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1083),USE(tmp:DefaultFax),FONT(,9,,)
                         STRING(@s255),AT(521,1250,3844,198),USE(tmp:DefaultEmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1250),USE(?Email),TRN,FONT(,9,,)
                         STRING('CUSTOMER ADDRESS'),AT(156,1458),USE(?String24),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4010,1510,1677,156),USE(?String28),TRN,FONT(,9,,FONT:bold)
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY DETAILS'),AT(156,8479),USE(?String73),TRN,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?charge_details),TRN,FONT(,9,,FONT:bold)
                         STRING('Model'),AT(156,3844),USE(?String40),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number'),AT(156,3156),USE(?String40:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Order Number'),AT(1604,3156),USE(?String40:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Authority Number'),AT(3083,3156),USE(?String40:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Type'),AT(4531,3156),USE(?Chargeable_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Chargeable Repair Type'),AT(6000,3156),USE(?Repair_Type),TRN,FONT(,8,,FONT:bold)
                         STRING('Make'),AT(1250,3854),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Exch I.M.E.I. No'),AT(5052,3844),USE(?IMEINo:2),TRN,HIDE,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(2604,3844),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(3906,3844),USE(?IMEINo),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(6198,3844),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('REPAIR DETAILS'),AT(156,3635),USE(?String50),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Despatch_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:STOCK.UseFile
  Access:WARPARTS.UseFile
  Access:LOAN.UseFile
  Access:JOBTHIRD.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        If job_ali:exchange_unit_number <> ''
            part_number_temp = ''
            Print(rpt:detail)
        Else!If job_ali:exchange_unit_number <> ''
            count# = 0
        
            If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
                setcursor(cursor:wait)
                save_wpr_id = access:warparts.savefile()
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = job_ali:ref_number
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> job_ali:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    part_number_temp = wpr:part_number
                    description_temp = wpr:description
                    quantity_temp = wpr:quantity
                    cost_temp = wpr:purchase_cost
                    line_cost_temp = wpr:quantity * wpr:purchase_cost
                    Print(rpt:detail)
                end !loop
                access:warparts.restorefile(save_wpr_id)
                setcursor()
        
            Else!If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
                setcursor(cursor:wait)
                save_par_id = access:parts.savefile()
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = job_ali:ref_number
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> job_ali:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    description_temp = par:description
                    quantity_temp = par:quantity
                    cost_temp = par:sale_cost
                    part_number_temp = par:part_number
                    line_cost_temp = par:quantity * par:sale_cost
                    Print(rpt:detail)
                end !loop
                access:parts.restorefile(save_par_id)
                setcursor()
            End!If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
            If count# = 0
                part_number_temp = ''
                Print(rpt:detail)
            End!If count# = 0
        End!If job_ali:exchange_unit_number <> ''
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  ! Before Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','DESPATCH NOTE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'DESPATCH NOTE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  ! Start Change 3778 BE(19/02/04)
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job:account_number
  IF (access:subtracc.fetch(sub:account_number_key) = Level:benign) THEN
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      IF (access:tradeacc.fetch(tra:account_number_key) = Level:Benign) THEN
          !IF (GETINI(CLIP(tra:Account_Number), 'UseAlternateDespatchNote',, CLIP(PATH())&'\TRADEFS.INI') = 1) THEN
          IF (tra:UseAlternateDespatchNote) THEN
              Despatch_Note_2()
              LocalResponse = GlobalResponse
              DO ProcedureReturn
          END
      END
  END
  ! End Change 3778 BE(19/02/04)
  
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  If glo:Select2 > 1
      Printer{PropPrint:Copies} = glo:Select2
  End!If glo:Select2 > 1
  ! After Embed Point: %BeforeOpeningReport) DESC(Before Opening Report) ARG()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:printbarcode <> 1
      Hide(?Bar_Code2_Temp)
  End!If def:printbarcode <> 'YES'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'DESPATCH NOTE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  !Barcode Bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'Job No: ' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'I.M.E.I. No.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
      Settarget(Report)
      ?chargeable_type{prop:text} = 'Warranty Type'
      ?repair_type{prop:text} = 'Warranty Repair Type'
      Hide(?labour_string)
      Hide(?parts_string)
      Hide(?Carriage_string)
      Hide(?vat_string)
      Hide(?total_string)
      Hide(?line)
      Settarget()
      charge_type_temp = job_ali:warranty_charge_type
      repair_type_temp = job_ali:repair_type_warranty
  Else
      charge_type_temp = job_ali:charge_type
      repair_type_temp = job_ali:repair_type
  End!If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job_ali:account_number
  IF access:subtracc.fetch(sub:account_number_key)
    !Error!
  END
  access:tradeacc.clearkey(tra:account_number_key) 
  tra:account_number = sub:main_account_number
  IF access:tradeacc.fetch(tra:account_number_key)
    !Error!
  END
  If tra:price_despatch <> 'YES'
      Settarget(report)
      hide(?unit_Cost)
      hide(?line_cost)
      hide(?cost_temp)
      hide(?line_cost_temp)
      hide(?labour_string)
      hide(?parts_string)
      hide(?carriage_String)
      hide(?vat_string)
      hide(?total_string)
      hide(?labour_temp)
      hide(?parts_temp)
      hide(?courier_cost_temp)
      hide(?vat_temp)
      hide(?total_temp)
      hide(?charge_details)
      ?Euro{prop:Hide} = 1
      Settarget()
  End!If tra:price_despatch <> 'YES'
  IF sub:PriceDespatchNotes = TRUE
    Settarget(report)
    unhide(?unit_Cost)
    unhide(?line_cost)
    unhide(?cost_temp)
    unhide(?line_cost_temp)
    unhide(?labour_string)
    unhide(?parts_string)
    unhide(?carriage_String)
    unhide(?vat_string)
    unhide(?total_string)
    unhide(?labour_temp)
    unhide(?parts_temp)
    unhide(?courier_cost_temp)
    unhide(?vat_temp)
    unhide(?total_temp)
    unhide(?charge_details)
    Settarget()
  END
  
  HideDespAdd# = 0
  if tra:invoice_sub_accounts = 'YES'
      If sub:HideDespAdd = 1
          HideDespAdd# = 1
      End!If sub:HideDespAdd = 1
  
      If SUB:UseCustDespAdd = 'YES'
          invoice_address1_temp     = job_ali:address_line1
          invoice_address2_temp     = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job_ali:address_line3
              invoice_address4_temp  = job_ali:postcode
          End
          invoice_company_name_temp   = job_ali:company_name
          invoice_telephone_number_temp   = job_ali:telephone_number
          invoice_fax_number_temp = job_ali:fax_number
      Else!If sub:invoice_customer_address = 'YES'
          invoice_address1_temp     = sub:address_line1
          invoice_address2_temp     = sub:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = sub:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = sub:address_line3
              invoice_address4_temp  = sub:postcode
          End
          invoice_company_name_temp   = sub:company_name
          invoice_telephone_number_temp   = sub:telephone_number
          invoice_fax_number_temp = sub:fax_number
  
  
      End!If sub:invoice_customer_address = 'YES'
      
  else!if tra:use_sub_accounts = 'YES'
      If tra:HideDespAdd = 1
          HideDespAdd# = 1
      End!If tra:HideDespAdd = 'YES'
  
      If TRA:UseCustDespAdd = 'YES'
          invoice_address1_temp     = job_ali:address_line1
          invoice_address2_temp     = job_ali:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = job_ali:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = job_ali:address_line3
              invoice_address4_temp  = job_ali:postcode
          End
          invoice_company_name_temp   = job_ali:company_name
          invoice_telephone_number_temp   = job_ali:telephone_number
          invoice_fax_number_temp = job_ali:fax_number
  
      Else!If tra:invoice_customer_address = 'YES'
          invoice_address1_temp     = tra:address_line1
          invoice_address2_temp     = tra:address_line2
          If job_ali:address_line3_delivery   = ''
              invoice_address3_temp = tra:postcode
              invoice_address4_temp = ''
          Else
              invoice_address3_temp  = tra:address_line3
              invoice_address4_temp  = tra:postcode
          End
          invoice_company_name_temp   = tra:company_name
          invoice_telephone_number_temp   = tra:telephone_number
          invoice_fax_number_temp = tra:fax_number
  
      End!If tra:invoice_customer_address = 'YES'
  End!!if tra:use_sub_accounts = 'YES'
  
  !Hide Despatch Address
  If HideDespAdd# = 1
      Settarget(Report)
      ?def:User_Name{prop:Hide} = 1
      ?def:Address_Line1{prop:Hide} = 1
      ?def:Address_Line2{prop:Hide} = 1
      ?def:Address_Line3{prop:Hide} = 1
      ?def:Postcode{prop:hide} = 1
      ?tmp:DefaultTelephone{prop:hide} = 1
      ?tmp:DefaultFax{prop:hide} = 1
      ?telephone{prop:hide} = 1
      ?fax{prop:hide} = 1
      ?email{prop:hide} = 1
      ?tmp:DefaultEmailAddress{prop:Hide} = 1
      Settarget()
  End!If HideDespAdd# = 1
  
  delivery_address1_temp     = job_ali:address_line1_delivery
  delivery_address2_temp     = job_ali:address_line2_delivery
  If job_ali:address_line3_delivery   = ''
      delivery_address3_temp = job_ali:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp  = job_ali:address_line3_delivery
      delivery_address4_temp  = job_ali:postcode_delivery
  End
  
  
  if job_ali:title = '' and job_ali:initial = ''
      customer_name_temp = clip(job_ali:surname)
  elsif job_ali:title = '' and job_ali:initial <> ''
      customer_name_temp = clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial = ''
      customer_name_temp = clip(job_ali:title) & ' ' & clip(job_ali:surname)
  elsif job_ali:title <> '' and job_ali:initial <> ''
      customer_name_temp = clip(job_ali:title) & ' ' & clip(job_ali:initial) & ' ' & clip(job_ali:surname)
  else
      customer_name_temp = ''
  end
  
  
  If job_ali:chargeable_job <> 'YES'
      labour_temp = ''
      parts_temp = ''
      courier_cost_temp = ''
      vat_temp = ''
      total_temp = ''
  Else!If job_ali:chargeable_job <> 'YES'
  
      If job_ali:invoice_number <> ''
          Total_Price('I',vat",total",balance")
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job_ali:invoice_number
          access:invoice.fetch(inv:invoice_number_key)
          labour_temp       = job_ali:invoice_labour_cost
          parts_temp        = job_ali:invoice_parts_cost
          courier_cost_temp = job_ali:invoice_courier_cost
      Else!If job_ali:invoice_number <> ''
          Total_Price('C',vat",total",balance")
          labour_temp = job_ali:labour_cost
          parts_temp  = job_ali:parts_cost
          courier_cost_temp = job_ali:courier_cost
      End!If job_ali:invoice_number <> ''
      vat_temp = vat"
      total_temp = total"
  
  End!If job_ali:chargeable_job <> 'YES'
  
  access:users.clearkey(use:user_code_key)
  use:user_code = job_ali:despatch_user
  If access:users.fetch(use:user_code_key) = Level:Benign
      despatched_user_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  End!If access:users.fetch(use:user_code_key) = Level:Benign
  
  setcursor(cursor:wait)
  tmp:accessories= ''
  save_jac_id = access:jobacc.savefile()
  access:jobacc.clearkey(jac:ref_number_key)
  jac:ref_number = job_ali:ref_number
  set(jac:ref_number_key,jac:ref_number_key)
  loop
      if access:jobacc.next()
         break
      end !if
      if jac:ref_number <> job_ali:ref_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      If tmp:accessories = ''
          tmp:accessories = jac:accessory
      Else!If tmp:accessories = ''
          tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
      End!If tmp:accessories = ''
  end !loop
  access:jobacc.restorefile(save_jac_id)
  setcursor()
  
  exchange_note# = 0
  
  !Exchange Loan
  If job_ali:exchange_unit_number <> ''
      exchange_note# = 1
  End!If job_ali:exchange_unit_number <> ''
  
  !Check Chargeable Parts for an "Exchange" part
  
  save_par_id = access:parts.savefile()
  access:parts.clearkey(par:part_number_key)
  par:ref_number  = job_ali:ref_number
  set(par:part_number_key,par:part_number_key)
  loop
      if access:parts.next()
         break
      end !if
      if par:ref_number  <> job_ali:ref_number      |
          then break.  ! end if
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number = par:part_ref_number
      if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
          If sto:ExchangeUnit = 'YES'
              exchange_note# = 1
              Break
          End!If sto:ExchangeUnit = 'YES'
      end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
  end !loop
  access:parts.restorefile(save_par_id)
  
  If exchange_note# = 0
  !Check Warranty Parts for an "Exchange" part
      save_wpr_id = access:warparts.savefile()
      access:warparts.clearkey(wpr:part_number_key)
      wpr:ref_number  = job_ali:ref_number
      set(wpr:part_number_key,wpr:part_number_key)
      loop
          if access:warparts.next()
             break
          end !if
          if wpr:ref_number  <> job_ali:ref_number      |
              then break.  ! end if
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = wpr:part_ref_number
          if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
              If sto:ExchangeUnit = 'YES'
                  exchange_note# = 1
                  Break
              End!If sto:ExchangeUnit = 'YES'
          end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
      end !loop
      access:warparts.restorefile(save_wpr_id)
  End!If exchange_note# = 0
  
  If job_ali:loan_unit_number <> ''
      Settarget(report)
      ?exchange_unit{prop:text} = 'LOAN UNIT'
      If job_ali:despatch_Type = 'LOA'
          ?title{prop:text} = 'LOAN DESPATCH NOTE'
      End!If job_ali:despatch_Type = 'EXC'
      Unhide(?exchange_unit)
      Unhide(?tmp:LoanExchangeUnit)
      access:loan.clearkey(loa:ref_number_key)
      loa:ref_number = job_ali:loan_unit_number
      if access:loan.tryfetch(loa:ref_number_key) = Level:Benign
          tmp:LoanExchangeUnit = '(' & Clip(loa:Ref_number) & ') ' & CLip(loa:manufacturer) & ' ' & CLip(loa:model_number) &|
                                 ' - ' & CLip(loa:esn)
      end
      Settarget()
  
  End!If job_ali:exchange_unit_number <> ''
  
  tmp:OriginalIMEI = job_ali:ESN
  
  !Was the unit exchanged at Third Party?
  If job_ali:Third_Party_Site <> ''
      IMEIError# = 0
      If job_ali:Third_Party_Site <> ''
          Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
          jot:RefNumber = job_ali:Ref_Number
          Set(jot:RefNumberKey,jot:RefNumberKey)
          If Access:JOBTHIRD.NEXT()
              IMEIError# = 1
          Else !If Access:JOBTHIRD.NEXT()
              If jot:RefNumber <> job_ali:Ref_Number            
                  IMEIError# = 1
              Else !If jot:RefNumber <> job_ali:Ref_Number
                  If jot:OriginalIMEI <> job_ali:esn
                      IMEIError# = 0
                  Else
                      IMEIError# = 1
                  End !If jot:OriginalIMEI <> job_ali:esn
  
              End !If jot:RefNumber <> job_ali:Ref_Number            
          End !If Access:JOBTHIRD.NEXT()
      Else !job_ali:Third_Party_Site <> ''
          IMEIError# = 1    
      End !job_ali:Third_Party_Site <> ''
  
      If IMEIError# = 1
          tmp:OriginalIMEI     = job_ali:esn
      Else !IMEIError# = 1
          Settarget(Report)
          tmp:OriginalIMEI     = jot:OriginalIMEI
          tmp:FinalIMEI        = job_ali:esn
          ?IMEINo{prop:text}   = 'Orig I.M.E.I. No'
          ?IMEINo:2{prop:hide} = 0
          ?tmp:FinalIMEI{prop:hide} = 0
          Settarget()
      End !IMEIError# = 1
  End !job_ali:Third_Party_Site <> ''
  
  If exchange_note# = 1
      Settarget(report)
      ?title{prop:text} = 'EXCHANGE DESPATCH NOTE'
      Unhide(?exchange_unit)
      Unhide(?tmp:LoanExchangeUnit)
      Hide(?parts_used)
      Hide(?qty)
      Hide(?part_number)
      Hide(?Description)
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job_ali:exchange_unit_number
      if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
          tmp:LoanExchangeUnit = '(' & Clip(xch:Ref_number) & ') ' & CLip(xch:manufacturer) & ' ' & CLip(xch:model_number) &|
                                  ' - ' & CLip(xch:esn)
          tmp:OriginalIMEI    = job_ali:ESN
          tmp:FinalIMEI       = xch:ESN
          ?tmp:FinalIMEI{prop:Hide} = 0
          ?IMEINo:2{prop:Hide} = 0
      end
      Settarget()
  End!If exchange_note# = 1
  
  !Get Job Notes!
  Access:JobNotes_Alias.ClearKey(jbn_ali:RefNumberKey)
  jbn_ali:RefNumber = job_ali:Ref_Number
  IF Access:JobNotes_Alias.Fetch(jbn_ali:RefNumberKey)
    !Error!
  END
  !Use Alternative Contact Nos
  Case UseAlternativeContactNos(job_ali:Account_Number)
      Of 1
          tmp:DefaultEmailAddress = tra:AltEmailAddress
          tmp:DefaultTelephone    = tra:AltTelephoneNumber
          tmp:DefaultFax          = tra:AltFaxNumber
      Of 2
          tmp:DefaultEmailAddress = sub:AltEmailAddress
          tmp:DefaultTelephone    = sub:AltTelephoneNumber
          tmp:DefaultFax          = sub:AltFaxNumber
      Else
          tmp:DefaultEmailAddress = def:EmailAddress
  End !UseAlternativeContactNos(job_ali:Account_Number)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Despatch_Note'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Despatch_Note',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Despatch_Note',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Despatch_Note',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Despatch_Note',1)
    SolaceViewVars('save_par_id',save_par_id,'Despatch_Note',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Despatch_Note',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Despatch_Note',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Despatch_Note',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Despatch_Note',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Despatch_Note',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Despatch_Note',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Despatch_Note',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Despatch_Note',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Despatch_Note',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Despatch_Note',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Despatch_Note',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Despatch_Note',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Despatch_Note',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Despatch_Note',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Despatch_Note',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Despatch_Note',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Despatch_Note',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Despatch_Note',1)
    SolaceViewVars('InitialPath',InitialPath,'Despatch_Note',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Despatch_Note',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Despatch_Note',1)
    SolaceViewVars('code_temp',code_temp,'Despatch_Note',1)
    SolaceViewVars('option_temp',option_temp,'Despatch_Note',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Despatch_Note',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Despatch_Note',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Despatch_Note',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Despatch_Note',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Despatch_Note',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Despatch_Note',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Despatch_Note',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Despatch_Note',1)
    SolaceViewVars('Delivery_Address1_Temp',Delivery_Address1_Temp,'Despatch_Note',1)
    SolaceViewVars('Delivery_address2_temp',Delivery_address2_temp,'Despatch_Note',1)
    SolaceViewVars('Delivery_address3_temp',Delivery_address3_temp,'Despatch_Note',1)
    SolaceViewVars('Delivery_address4_temp',Delivery_address4_temp,'Despatch_Note',1)
    SolaceViewVars('Invoice_Company_Temp',Invoice_Company_Temp,'Despatch_Note',1)
    SolaceViewVars('Invoice_address1_temp',Invoice_address1_temp,'Despatch_Note',1)
    SolaceViewVars('invoice_address2_temp',invoice_address2_temp,'Despatch_Note',1)
    SolaceViewVars('invoice_address3_temp',invoice_address3_temp,'Despatch_Note',1)
    SolaceViewVars('invoice_address4_temp',invoice_address4_temp,'Despatch_Note',1)
    
      Loop SolaceDim1# = 1 to 6
        SolaceFieldName" = 'accessories_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",accessories_temp[SolaceDim1#],'Despatch_Note',1)
      End
    
    
    SolaceViewVars('estimate_value_temp',estimate_value_temp,'Despatch_Note',1)
    SolaceViewVars('despatched_user_temp',despatched_user_temp,'Despatch_Note',1)
    SolaceViewVars('vat_temp',vat_temp,'Despatch_Note',1)
    SolaceViewVars('total_temp',total_temp,'Despatch_Note',1)
    SolaceViewVars('part_number_temp',part_number_temp,'Despatch_Note',1)
    SolaceViewVars('line_cost_temp',line_cost_temp,'Despatch_Note',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Despatch_Note',1)
    SolaceViewVars('esn_temp',esn_temp,'Despatch_Note',1)
    SolaceViewVars('charge_type_temp',charge_type_temp,'Despatch_Note',1)
    SolaceViewVars('repair_type_temp',repair_type_temp,'Despatch_Note',1)
    SolaceViewVars('labour_temp',labour_temp,'Despatch_Note',1)
    SolaceViewVars('parts_temp',parts_temp,'Despatch_Note',1)
    SolaceViewVars('courier_cost_temp',courier_cost_temp,'Despatch_Note',1)
    SolaceViewVars('Quantity_temp',Quantity_temp,'Despatch_Note',1)
    SolaceViewVars('Description_temp',Description_temp,'Despatch_Note',1)
    SolaceViewVars('Cost_Temp',Cost_Temp,'Despatch_Note',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Despatch_Note',1)
    SolaceViewVars('sub_total_temp',sub_total_temp,'Despatch_Note',1)
    SolaceViewVars('invoice_company_name_temp',invoice_company_name_temp,'Despatch_Note',1)
    SolaceViewVars('invoice_telephone_number_temp',invoice_telephone_number_temp,'Despatch_Note',1)
    SolaceViewVars('invoice_fax_number_temp',invoice_fax_number_temp,'Despatch_Note',1)
    SolaceViewVars('tmp:LoanExchangeUnit',tmp:LoanExchangeUnit,'Despatch_Note',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Despatch_Note',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Despatch_Note',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'Despatch_Note',1)
    SolaceViewVars('tmp:OriginalIMEI',tmp:OriginalIMEI,'Despatch_Note',1)
    SolaceViewVars('tmp:FinalIMEI',tmp:FinalIMEI,'Despatch_Note',1)
    SolaceViewVars('tmp:DefaultEmailAddress',tmp:DefaultEmailAddress,'Despatch_Note',1)


BuildCtrlQueue      Routine







