

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01020.INC'),ONCE        !Local module procedure declarations
                     END








Third_Party_Single_Despatch PROCEDURE(f_type)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
save_trb_id          USHORT,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_jac_id          USHORT,AUTO
tmp:accessoryque     QUEUE,PRE(accque)
accessory            STRING(30)
                     END
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
code_temp            BYTE
option_temp          BYTE
bar_code_string_temp CSTRING(21)
bar_code_temp        CSTRING(21)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:DOP)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:ThirdPartyDateDesp)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:date_booked)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                         PROJECT(jbn:Fault_Description)
                         PROJECT(jbn:Invoice_Text)
                       END
                     END
report               REPORT,AT(396,2792,7521,7406),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?header)
                         STRING('3rd Party Agent:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(GLO:Select3),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,4240),USE(?detailband)
                           STRING('Job Number:'),AT(313,260),USE(?String2),TRN,FONT(,16,,FONT:bold,CHARSET:ANSI)
                           STRING(@s20),AT(2917,313,1771,208),USE(bar_code_temp),LEFT,FONT('C128 High 12pt LJ3',12,,FONT:regular,CHARSET:ANSI)
                           STRING(@s8),AT(4792,313),USE(job:Ref_Number,,?job:Ref_Number:2),TRN,LEFT,FONT(,12,,FONT:bold)
                           STRING('Unit Details:'),AT(313,625),USE(?String2:2),TRN,FONT(,16,,FONT:bold,CHARSET:ANSI)
                           STRING(@d6b),AT(5625,313),USE(job:date_booked),LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Manufacturer:'),AT(2917,615),USE(?String6:2),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,615),USE(job:Manufacturer),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Model Number:'),AT(2917,927),USE(?String6:3),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,927),USE(job:Model_Number),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Unit Type:'),AT(2917,1240),USE(?String6:4),TRN,FONT(,12,,FONT:bold)
                           STRING(@s30),AT(4792,1240),USE(job:Unit_Type),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('E.S.N. / I.M.E.I.:'),AT(2917,1552),USE(?String6:5),TRN,FONT(,12,,FONT:bold)
                           STRING(@s16),AT(4792,1552),USE(job:ESN),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('M.S.N.:'),AT(2917,1875),USE(?String6:6),TRN,FONT(,12,,FONT:bold)
                           STRING(@s16),AT(4792,1865),USE(job:MSN),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING(@d6),AT(4750,2167),USE(job:DOP),TRN,LEFT,FONT(,12,,)
                           STRING('Date Of Purchase:'),AT(2917,2167),USE(?String46),TRN,FONT(,12,,FONT:bold)
                           STRING('Reported Fault:'),AT(2917,2479),USE(?String6:14),TRN,FONT(,12,,FONT:bold)
                           TEXT,AT(4792,2500,2552,885),USE(jbn:Fault_Description),TRN,FONT('Arial',10,,,CHARSET:ANSI)
                           STRING('Date Despatched:'),AT(2917,3438),USE(?String6:7),TRN,FONT(,12,,FONT:bold)
                           STRING(@d6b),AT(4792,3438),USE(job:ThirdPartyDateDesp),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING(@s40),AT(4792,3698),USE(GLO:Select2),TRN,LEFT,FONT(,12,,,CHARSET:ANSI)
                           STRING('Exchange'),AT(2917,3958,4427,208),USE(?Exchange),TRN,HIDE,FONT(,12,,FONT:bold,CHARSET:ANSI)
                           STRING('Batch Number:'),AT(2917,3698),USE(?String6:8),TRN,FONT(,12,,FONT:bold)
                           STRING('Despatch Details:'),AT(260,3385),USE(?String2:3),TRN,FONT(,16,,FONT:bold,CHARSET:ANSI)
                         END
detail2                  DETAIL,AT(,,,1125),USE(?InvoiceText)
                           STRING('Invoice Text:'),AT(260,52),USE(?String2:5),TRN,FONT(,16,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(2917,104,4479,885),USE(jbn:Invoice_Text),TRN,FONT('Arial',10,,,CHARSET:ANSI)
                         END
detail3                  DETAIL,USE(?Accessories2:2)
                           STRING('Accessories:'),AT(260,52),USE(?Accessories),TRN,HIDE,FONT(,16,,FONT:bold,CHARSET:ANSI)
                           STRING('Retained'),AT(2917,104,4427,260),USE(?DespatchText),TRN,FONT(,12,,FONT:bold)
                         END
                       END
detail1                DETAIL,AT(,,,219),USE(?unnamed)
                         STRING(@s30),AT(4792,0),USE(jac:Accessory),TRN,FONT(,12,,)
                       END
                       FOOTER,AT(385,10260,7521,1156),USE(?unnamed:2),FONT('Arial',,,)
                         STRING('BOUNCER REPORT:'),AT(208,52),USE(?bouncer1),TRN,HIDE,FONT(,10,,FONT:bold)
                         STRING('This unit has previously been repaired by your workshop'),AT(1563,52),USE(?bouncer2),TRN,HIDE,FONT(,10,,FONT:bold)
                         LINE,AT(365,260,6802,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(156,313,7188,833),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('rlistsim.gif'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('3RD PARTY DESPATCH NOTE'),AT(4010,0,3438,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Third_Party_Single_Despatch')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Access:JOBNOTES.UseFile
  Access:JOBACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:TRDBATCH.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        PrintSkipDetails = FALSE
        
        
        
        
        ! Before Embed Point: %AfterPrint) DESC(After Printing Detail Section) ARG()
        Print(rpt:detail)
        first# = 1
        ret# = 0
        save_jac_id = access:jobacc.savefile()
        access:jobacc.clearkey(jac:ref_number_key)
        jac:ref_number = job:ref_number
        set(jac:ref_number_key,jac:ref_number_key)
        loop
            if access:jobacc.next()
               break
            end !if
            if jac:ref_number <> job:ref_number      |
                then break.  ! end if
            Case f_Type
                Of 'RET'
                    If first# = 1
                        SetTarget(Report)
                        ?Accessories{prop:Hide} = 0
                        ?DespatchText{prop:Text} = 'Retained By ' & Clip(Capitalize(def:User_Name)) & ' :-'
                        SetTarget()
                        Print(Rpt:Detail3)
                        first# = 0
                    End !If first# = 1
                    Print(rpt:Detail1)
                Of 'DES'
                    Sort(glo:Queue,glo:Pointer)
                    glo:Pointer = jac:Accessory
                    Get(glo:Queue,glo:Pointer)
                    IF Error()
                        If first# = 1
                            SetTarget(Report)
                            ?Accessories{prop:Hide} = 0
                            ?DespatchText{prop:Text} = 'Retained By ' & Clip(Capitalize(def:user_name)) & ' :-'
                            Print(rpt:Detail3)
                            SetTarget()
                            first# = 0
                            ret# = 1
                        End !If first# = 1
                        print(rpt:detail1)
                    End !IF Error()
            End !Case f_Type
        end !loop
        access:jobacc.restorefile(save_jac_id)
        
        If f_Type = 'DES'
            first# = 1
            save_jac_id = access:jobacc.savefile()
            access:jobacc.clearkey(jac:ref_number_key)
            jac:ref_number = job:ref_number
            set(jac:ref_number_key,jac:ref_number_key)
            loop
                if access:jobacc.next()
                   break
                end !if
                if jac:ref_number <> job:ref_number      |
                    then break.  ! end if
                Sort(glo:Queue,glo:Pointer)
                glo:Pointer = jac:Accessory
                Get(glo:Queue,glo:Pointer)
                IF ~Error()
                    If first# = 1
                        SetTarget(Report)
                        If ret# = 0
                            ?Accessories{Prop:Hide} = 0
                        Else
                            ?Accessories{Prop:Hide} = 1
                        End !If ret# = 0
                        ?DespatchText{prop:Text} = 'Despatched With Unit:-'
                        SetTarget()
                        Print(rpt:Detail3)
                        first# = 0
                    End !If first# = 1
                    print(rpt:detail1)
                End !IF Error()
            end !loop
            access:jobacc.restorefile(save_jac_id)
        End !f_Type = 'DET'
        Print(rpt:Detail2)
        ! After Embed Point: %AfterPrint) DESC(After Printing Detail Section) ARG()
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','DESPATCH NOTE - THIRD PARTY')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'DESPATCH NOTE - THIRD PARTY'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  count# = 0
  save_jac_id = access:jobacc.savefile()
  access:jobacc.clearkey(jac:ref_number_key)
  jac:ref_number = job:ref_number
  set(jac:ref_number_key,jac:ref_number_key)
  loop
      if access:jobacc.next()
         break
      end !if
      if jac:ref_number <> job:ref_number      |
          then break.  ! end if
      count# = 1
      Break
  end !loop
  access:jobacc.restorefile(save_jac_id)
  
!  If count# = 1
!      Settarget(report)
!      ?retain_text{prop:text} = 'Retained By ' & Clip(def:user_name) & ' :-'
!      ?DespatchText{prop:Text} = 'Despatched With Unit:-'
!      Settarget()
!  Else!If records(tmp:accessoryque)
!      Settarget(report)
!      ?retain_text{prop:text} = 'No Accessories'
!      Settarget()
!  End!If records(tmp:accessoryque)
  !barcode bit
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job:ref_number)
  sequence2(code_temp,option_temp,bar_code_string_temp,bar_code_temp)
  
  If job:exchange_unit_number <> ''
      Settarget(Report)
      Unhide(?exchange)
      ?exchange{prop:text} = 'Exchange Unit ' & Clip(job:exchange_unit_number) & ' issued.'
      Settarget()
  End!If job:exchange_unit_number <> ''
  
  !Bouncer?
  found# = 0
  save_trb_id = access:trdbatch.savefile()
  clear(trb:record, -1)
  trb:esn = job:esn
  set(trb:esn_only_key,trb:esn_only_key)
  loop
      next(trdbatch)
      if errorcode()         |
         or trb:esn <> job:esn      |
         then break.  ! end if
      If trb:company_name = job:third_party_site
          access:jobs_alias.clearkey(job_ali:ref_number_key)
          job_ali:ref_number  = trb:ref_number
          If access:jobs_alias.tryfetch(job_ali:ref_number_key) = Level:Benign
              If job_ali:ThirdPartyDateDesp <> Today()
                  If job_ali:ThirdPartyDateDesp > Today() - 90
                      found# = 1
                      Break
                  End!If job:third_party_despatch_date > Today() - 90
              End!If job:third_party_despatch_date <> Today()
          End!If access:jobs_alias.tryfetch(job_ali:ref_number_key) = Level:Benign
      End!If trb:company_name = job:third_party_site
  end !loop
  access:trdbatch.restorefile(save_trb_id)
  If found# = 1
      Settarget(Report)
      Unhide(?bouncer1)
      Unhide(?bouncer2)
      Settarget()
  End!If found# = 1
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE - THIRD PARTY'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'DESPATCH NOTE - THIRD PARTY'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Third_Party_Single_Despatch'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Third_Party_Single_Despatch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Third_Party_Single_Despatch',1)
    SolaceViewVars('save_trb_id',save_trb_id,'Third_Party_Single_Despatch',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Third_Party_Single_Despatch',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Third_Party_Single_Despatch',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Third_Party_Single_Despatch',1)
    SolaceViewVars('tmp:accessoryque:accessory',tmp:accessoryque:accessory,'Third_Party_Single_Despatch',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Third_Party_Single_Despatch',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Third_Party_Single_Despatch',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Third_Party_Single_Despatch',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Third_Party_Single_Despatch',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Third_Party_Single_Despatch',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Third_Party_Single_Despatch',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Third_Party_Single_Despatch',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Third_Party_Single_Despatch',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Third_Party_Single_Despatch',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Third_Party_Single_Despatch',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Third_Party_Single_Despatch',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Third_Party_Single_Despatch',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Third_Party_Single_Despatch',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Third_Party_Single_Despatch',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Third_Party_Single_Despatch',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Third_Party_Single_Despatch',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Third_Party_Single_Despatch',1)
    SolaceViewVars('InitialPath',InitialPath,'Third_Party_Single_Despatch',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Third_Party_Single_Despatch',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Third_Party_Single_Despatch',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'Third_Party_Single_Despatch',1)
    SolaceViewVars('code_temp',code_temp,'Third_Party_Single_Despatch',1)
    SolaceViewVars('option_temp',option_temp,'Third_Party_Single_Despatch',1)
    SolaceViewVars('bar_code_string_temp',bar_code_string_temp,'Third_Party_Single_Despatch',1)
    SolaceViewVars('bar_code_temp',bar_code_temp,'Third_Party_Single_Despatch',1)


BuildCtrlQueue      Routine







