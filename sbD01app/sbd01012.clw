

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01012.INC'),ONCE        !Local module procedure declarations
                     END








Exception_Report PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
count_temp           LONG
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Unit_Type)
                     END
Report               REPORT('Exceptions Report'),AT(396,2771,7521,8531),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,521,7521,1760),USE(?unnamed)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('WARRANTY EXCEPTIONS REPORT'),AT(4063,0,3385,260),USE(?String20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,313,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,469,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,625,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING('Manufacturer:'),AT(4896,417),USE(?String35),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,417),USE(GLO:Select1),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(156,781,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1042),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING(@s30),AT(5885,1042),USE(SUB:Company_Name),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1198),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN,FONT(,9,,)
                         STRING(@s4),AT(5885,1458),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Batch Number'),AT(4896,625),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s9),AT(5885,625,1760,240),USE(GLO:Select2),TRN,FONT(,8,,FONT:bold)
                         STRING('Date'),AT(4896,1250),USE(?String31),TRN,FONT(,8,,)
                         STRING(@D6),AT(5885,1250),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@T3),AT(6563,1250),USE(ReportRunTime),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s255),AT(573,1354,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Account Number: '),AT(4896,833),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s15),AT(5885,833,1000,240),USE(SUB:Account_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Account Name: '),AT(4896,1042),USE(?String36),TRN,FONT(,8,,)
                         STRING('Page Number'),AT(4896,1458,833,208),USE(?String34),TRN,FONT(,8,,)
                       END
break1                 BREAK(EndOfReport),USE(?unnamed:4)
detail1                  DETAIL
                           STRING(@s20),AT(1083,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s25),AT(2396,0),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                           STRING(@s16),AT(5625,0),USE(job:MSN),TRN,LEFT,FONT(,8,,)
                           STRING(@s8),AT(104,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s16),AT(4083,0),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,646),USE(?unnamed:6)
                           LINE,AT(271,208,6979,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total:'),AT(313,313),USE(?String30),TRN,FONT(,9,,FONT:bold)
                           STRING(@n8),AT(729,313),USE(count_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(365,11417,7552,260),USE(?unnamed:2),ABSOLUTE
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('rlistsim.gif'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('Job Number'),AT(104,2083),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(1083,2083),USE(?String51),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(2396,2083),USE(?String52),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4083,2083),USE(?String53),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(5625,2083),USE(?String54),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Exception_Report')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select2',GLO:Select2)
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job:EDI_Key)
      Process:View{Prop:Filter} = |
      'UPPER(job:Manufacturer) = UPPER(GLO:Select1) AND (Upper(job:edi) = ''YE' & |
      'S'' And Upper(job:edi_batch_number) = Upper(glo:select2))'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        count_temp += 1
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:detail1)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  set(defaults)
  access:defaults.next()
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = glo:select3
  access:subtracc.fetch(sub:account_number_key) 
  count_temp = 0
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Exception_Report'
  END
  Report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Exception_Report',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Exception_Report',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Exception_Report',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Exception_Report',1)
    SolaceViewVars('count_temp',count_temp,'Exception_Report',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Exception_Report',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Exception_Report',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Exception_Report',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Exception_Report',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Exception_Report',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Exception_Report',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Exception_Report',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Exception_Report',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Exception_Report',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Exception_Report',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Exception_Report',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Exception_Report',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Exception_Report',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Exception_Report',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Exception_Report',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Exception_Report',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Exception_Report',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Exception_Report',1)
    SolaceViewVars('InitialPath',InitialPath,'Exception_Report',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Exception_Report',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Exception_Report',1)


BuildCtrlQueue      Routine







