

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01018.INC'),ONCE        !Local module procedure declarations
                     END








Credit_Note PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
tmp:Default_Invoice_Company_Name STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
save_ivp_id          USHORT,AUTO
tmp:Default_Invoice_Address_Line1 STRING(30)
tmp:Default_Invoice_Address_Line2 STRING(30)
tmp:Default_Invoice_Address_Line3 STRING(30)
tmp:Default_Invoice_Postcode STRING(15)
tmp:Default_Invoice_Telephone_Number STRING(15)
tmp:Default_Invoice_Fax_Number STRING(15)
tmp:Default_Invoice_VAT_Number STRING(30)
RejectRecord         LONG,AUTO
tmp:printedby        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
save_rtp_id          USHORT,AUTO
tmp:vat_rate         REAL
tmp:Delivery_Name    STRING(30)
tmp:Delivery_Address_Line_1 STRING(30)
save_res_id          USHORT,AUTO
tmp:Invoice_Name     STRING(30)
tmp:Invoice_address_L STRING(30)
tmp:items_total      REAL
tmp:vat              REAL
tmp:item_cost        REAL
tmp:line_cost        REAL
tmp:despatch_note_number STRING(10)
tmp:line_cost_total  REAL
tmp:vat_total        REAL
tmp:Total            REAL
tmp:payment_date     DATE,DIM(4)
tmp:other_amount     REAL
tmp:total_paid       REAL
tmp:payment_type     STRING(20),DIM(4)
tmp:payment_amount   REAL,DIM(4)
tmp:sub_total        REAL
tmp:courier_cost     REAL
tmp:PartsCost        REAL
tmp:labour_cost      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Invoice_Number)
                       PROJECT(inv:PrevInvoiceNo)
                     END
report               REPORT,AT(396,4573,7521,4260),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(344,427,7521,10896),USE(?unnamed)
                         STRING('Date Printed:'),AT(5000,938),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5990,938),USE(ReportRunDate),TRN,LEFT,FONT('Arial',8,,FONT:bold)
                         STRING('Tel:'),AT(4063,2500),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('Printed By:'),AT(5000,781),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@s255),AT(5990,781),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('CREDIT NOTE:'),AT(4792,52),USE(?string22:3),TRN,FONT(,16,,FONT:bold)
                         STRING(@s8),AT(6354,52),USE(inv:Invoice_Number),TRN,LEFT,FONT('Arial',16,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5990,469),USE(tmp:Default_Invoice_VAT_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T. Number:'),AT(5000,469),USE(?string22:12),TRN,FONT(,8,,)
                         STRING(@s8),AT(5990,1094),USE(inv:PrevInvoiceNo),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@n3),AT(5990,1250),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6250,1250),USE(?string22:14),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6406,1250,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold)
                         STRING('Page Number:'),AT(5000,1250),USE(?string22:13),TRN,FONT(,8,,)
                         STRING('Date Of Credit:'),AT(5000,625),USE(?String65),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5990,625),USE(inv:Date_Created),TRN,FONT(,8,,FONT:bold)
                         STRING('Original Invoice No:'),AT(5000,1094),USE(?string22:10),TRN,FONT(,8,,)
                         STRING('Account No'),AT(208,3177),USE(?string22:6),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(208,3385),USE(ret:Account_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Purch Order No'),AT(1615,3177),USE(?string22:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name'),AT(3125,3177),USE(?string22:11),TRN,FONT(,8,,FONT:bold)
                         STRING(@s20),AT(1615,3385),USE(ret:Purchase_Order_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s20),AT(3125,3385),USE(ret:Contact_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date'),AT(208,8698),USE(?String66),TRN,HIDE,FONT(,7,,FONT:bold)
                         STRING('Type'),AT(708,8698),USE(?String67),TRN,HIDE,FONT(,7,,FONT:bold)
                         STRING('Amount'),AT(1969,8698),USE(?String70:5),TRN,HIDE,FONT(,7,,FONT:bold)
                         STRING(@d6b),AT(156,8854),USE(tmp:payment_date[1]),TRN,HIDE,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,8854),USE(tmp:payment_type[1]),TRN,HIDE,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,8854),USE(tmp:payment_amount[1]),TRN,HIDE,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@d6b),AT(156,9167),USE(tmp:payment_date[4]),TRN,HIDE,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,9167),USE(tmp:payment_type[4]),TRN,HIDE,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,9167),USE(tmp:payment_amount[4]),TRN,HIDE,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         TEXT,AT(2917,8750,1719,781),USE(ret:Invoice_Text),FONT(,8,,FONT:bold)
                         STRING(@d6b),AT(156,8958),USE(tmp:payment_date[2]),TRN,HIDE,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,8958),USE(tmp:payment_type[2]),TRN,HIDE,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,8958),USE(tmp:payment_amount[2]),TRN,HIDE,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@d6b),AT(156,9063),USE(tmp:payment_date[3]),TRN,HIDE,RIGHT,FONT(,7,,)
                         STRING(@s20),AT(729,9063),USE(tmp:payment_type[3]),TRN,HIDE,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING(@n10.2b),AT(1823,9063),USE(tmp:payment_amount[3]),TRN,HIDE,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         STRING('Others:'),AT(208,9323),USE(?Others),TRN,HIDE,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1823,9323),USE(tmp:other_amount),TRN,HIDE,RIGHT,FONT('Arial',7,,,CHARSET:ANSI)
                         LINE,AT(208,9479,1979,0),USE(?Line4),HIDE,COLOR(COLOR:Black)
                         STRING('Total:'),AT(208,9531),USE(?String109),TRN,HIDE,FONT(,7,,FONT:bold)
                         STRING(@n10.2b),AT(1823,9531),USE(tmp:total_paid),TRN,HIDE,RIGHT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1719),USE(ret:Company_Name_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1719),USE(ret:Company_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1875),USE(tmp:Delivery_Address_Line_1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1875),USE(tmp:Invoice_address_L),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2031),USE(ret:Address_Line2_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2031),USE(ret:Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(4271,2500),USE(ret:Telephone_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2500),USE(?string22:9),TRN,FONT(,8,,)
                         STRING(@s15),AT(469,2500),USE(ret:Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(4063,2656),USE(?string22:5),TRN,FONT(,8,,)
                         STRING(@s10),AT(4063,2344),USE(ret:Postcode_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s10),AT(208,2344),USE(ret:Postcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2188),USE(ret:Address_Line3_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2188),USE(ret:Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(4271,2656),USE(ret:Fax_Number_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(208,2656),USE(?string22:8),TRN,FONT(,8,,)
                         STRING(@s15),AT(469,2656),USE(ret:Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break2                 BREAK(endofreport),USE(?unnamed:2)
detail2                  DETAIL,AT(,,,208),USE(?unnamed:5)
                           STRING(@n-14),AT(5573,0),USE(ivp:CreditQuantity),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6531,0),USE(tmp:line_cost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(4167,0),USE(ivp:RetailCost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s25),AT(156,0),USE(ivp:PartNumber),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(1771,0),USE(ivp:Description),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(365,9219,7521,1938),USE(?unnamed:4)
                         STRING('Courier Cost:'),AT(4948,0),USE(?String70:4),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6563,0),USE(tmp:courier_cost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@n14.2),AT(6563,156),USE(tmp:PartsCost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Labour Cost:'),AT(4948,313),USE(?String70:7),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6563,313),USE(tmp:labour_cost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Parts Cost:'),AT(4948,156),USE(?String70:6),TRN,FONT(,8,,)
                         STRING('Total (Ex VAT)'),AT(4948,469),USE(?String70),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6563,469),USE(tmp:sub_total),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('V.A.T.:'),AT(4948,625),USE(?String70:2),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6563,625),USE(tmp:vat_total),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(6354,729,1042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total (Inc VAT):'),AT(4948,781),USE(?String70:3),TRN,FONT(,8,,FONT:bold)
                         LINE,AT(6354,417,1042,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING(@n14.2),AT(6563,781),USE(tmp:Total),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s3),AT(6354,781),USE(de2:CurrencySymbol),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(104,1094,7292,781),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(354,479,7521,11198),USE(?unnamed:3)
                         IMAGE('rinvdet.gif'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,83,3844,240),USE(tmp:Default_Invoice_Company_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,323,3844,156),USE(tmp:Default_Invoice_Address_Line1),TRN
                         STRING(@s30),AT(156,479,3844,156),USE(tmp:Default_Invoice_Address_Line2),TRN
                         STRING(@s30),AT(156,635,3844,156),USE(tmp:Default_Invoice_Address_Line3),TRN
                         STRING(@s15),AT(156,802,1156,156),USE(tmp:Default_Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s15),AT(677,958),USE(tmp:Default_Invoice_Telephone_Number)
                         STRING('Fax: '),AT(156,1094),USE(?String19),TRN
                         STRING(@s15),AT(677,1094),USE(tmp:Default_Invoice_Fax_Number)
                         STRING(@s255),AT(677,1250,3844,208),USE(def:EmailAddress),TRN
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN
                         STRING('Quantity'),AT(5698,3854),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(156,3854),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(1792,3854),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6917,3823),USE(?string25:4),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost'),AT(4479,3854),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('(Ex.V.A.T.)'),AT(6896,3917),USE(?string25:5),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('INVOICE ADDRESS'),AT(208,1469),USE(?string44:2),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4063,1469),USE(?string44:3),TRN,FONT(,9,,FONT:bold)
                         STRING('PAYMENT DETAILS'),AT(104,8490),USE(?string44:4),TRN,HIDE,FONT(,9,,FONT:bold)
                         STRING('CREDIT DETAILS'),AT(4740,8490),USE(?string44:5),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE TEXT'),AT(2906,8490),USE(?string44:6),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Credit_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:INVPARTS.Open
  Relate:RETDESNO.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:RETSALES.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:RETPAY.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = |
      'inv:Invoice_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        save_ivp_id = access:invparts.savefile()
        access:invparts.clearkey(ivp:invoicenokey)
        ivp:invoicenumber = inv:invoice_number
        set(ivp:invoicenokey,ivp:invoicenokey)
        loop
            if access:invparts.next()
               break
            end !if
            if ivp:invoicenumber <> inv:invoice_number      |
                then break.  ! end if
            Case inv:invoice_Type
                Of 'RET'
                    tmp:line_cost  = Round(ivp:retailcost * ivp:creditquantity,.01)
                Of 'SIN'
                    tmp:line_cost  = Round(ivp:salecost * ivp:creditquantity,.01)
                Of 'CHA'
                    tmp:line_cost  = Round(ivp:salecost * ivp:creditquantity,.01)
                Of 'WAR'
                    tmp:line_cost  = Round(ivp:purchasecost * ivp:creditquantity,.01)
            End!Case inv:invoice_Type
            tmp:recordscount += 1
            print(rpt:detail2)
        
        end !loop
        access:invparts.restorefile(save_ivp_id)
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:INVPARTS.Close
    Relate:RETDESNO.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','CREDIT NOTE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'CREDIT NOTE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      tmp:Default_Invoice_Company_Name       = DEF:Invoice_Company_Name
      tmp:Default_Invoice_Address_Line1      = DEF:Invoice_Address_Line1
      tmp:Default_Invoice_Address_Line2      = DEF:Invoice_Address_Line2
      tmp:Default_Invoice_Address_Line3      = DEF:Invoice_Address_Line3
      tmp:Default_Invoice_Postcode           = DEF:Invoice_Postcode
      tmp:Default_Invoice_Telephone_Number   = DEF:Invoice_Telephone_Number
      tmp:Default_Invoice_Fax_Number         = DEF:Invoice_Fax_Number
      tmp:Default_Invoice_VAT_Number         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      tmp:Default_Invoice_Company_Name       = DEF:User_Name
      tmp:Default_Invoice_Address_Line1      = DEF:Address_Line1
      tmp:Default_Invoice_Address_Line2      = DEF:Address_Line2
      tmp:Default_Invoice_Address_Line3      = DEF:Address_Line3
      tmp:Default_Invoice_Postcode             = DEF:Postcode
      tmp:Default_Invoice_Telephone_Number   = DEF:Telephone_Number
      tmp:Default_Invoice_Fax_Number    = DEF:Fax_Number
      tmp:Default_Invoice_VAT_Number         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  
  case inv:invoice_type
      of 'RET' !retail invoice
          access:retsales.clearkey(ret:invoice_number_key)
          ret:invoice_number = inv:previnvoiceno
          access:retsales.tryfetch(ret:invoice_number_key)
          tmp:despatch_note_number   = 'D-' & glo:select1
          If ret:payment_method = 'CAS'
              tmp:Delivery_Name  = RET:Contact_Name
              tmp:Invoice_Name   = ret:contact_name
          Else!If ret:payment_method = 'CAS'
              tmp:Delivery_Name  = RET:Company_Name_Delivery
              tmp:invoice_name   = ret:company_name
          End!If ret:payment_method = 'CAS'
          If ret:building_name    = ''
              tmp:Invoice_address_L     = Clip(RET:Address_Line1)
          Else!    If ret:building_name    = ''
              tmp:Invoice_Address_L     = Clip(RET:Building_Name) & ' ' & Clip(RET:Address_Line1)
          End!
          If ret:building_name_delivery   = ''
              tmp:Delivery_Address_Line_1    = Clip(RET:Address_Line1_Delivery)
          Else!    If ret:building_name_delivery   = ''
              tmp:Delivery_Address_Line_1    = Clip(RET:Building_Name_Delivery) & ' ' & Clip(RET:Address_Line1_Delivery)
          End!    If ret:building_name_delivery   = ''
  
      of 'CHA' !multiple chargeable invoice
  
      of 'SIN' !single chargeable invoice
  
      of 'WAR' !warranty invoice
  
  end!case inv:invoice_type
  
  tmp:vat_rate   = inv:vat_rate_retail
  
  
  
  tmp:courier_cost   = inv:courier_paid
  tmp:partsCost  = inv:parts_paid
  tmp:Labour_Cost = inv:labour_paid
  tmp:sub_total = tmp:courier_Cost + tmp:partsCost + tmp:labour_cost
  tmp:vat_total = Round((inv:courier_paid + inv:parts_paid) * tmp:vat_total/100,.01)
  tmp:Total  = inv:courier_paid + Round(tmp:vat_total,.01) + inv:parts_paid
  
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'CREDIT NOTE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'CREDIT NOTE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Credit_Note'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Credit_Note',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:Default_Invoice_Company_Name',tmp:Default_Invoice_Company_Name,'Credit_Note',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Credit_Note',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Credit_Note',1)
    SolaceViewVars('save_ivp_id',save_ivp_id,'Credit_Note',1)
    SolaceViewVars('tmp:Default_Invoice_Address_Line1',tmp:Default_Invoice_Address_Line1,'Credit_Note',1)
    SolaceViewVars('tmp:Default_Invoice_Address_Line2',tmp:Default_Invoice_Address_Line2,'Credit_Note',1)
    SolaceViewVars('tmp:Default_Invoice_Address_Line3',tmp:Default_Invoice_Address_Line3,'Credit_Note',1)
    SolaceViewVars('tmp:Default_Invoice_Postcode',tmp:Default_Invoice_Postcode,'Credit_Note',1)
    SolaceViewVars('tmp:Default_Invoice_Telephone_Number',tmp:Default_Invoice_Telephone_Number,'Credit_Note',1)
    SolaceViewVars('tmp:Default_Invoice_Fax_Number',tmp:Default_Invoice_Fax_Number,'Credit_Note',1)
    SolaceViewVars('tmp:Default_Invoice_VAT_Number',tmp:Default_Invoice_VAT_Number,'Credit_Note',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Credit_Note',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'Credit_Note',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Credit_Note',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Credit_Note',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Credit_Note',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Credit_Note',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Credit_Note',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Credit_Note',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Credit_Note',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Credit_Note',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Credit_Note',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Credit_Note',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Credit_Note',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Credit_Note',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Credit_Note',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Credit_Note',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Credit_Note',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Credit_Note',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Credit_Note',1)
    SolaceViewVars('InitialPath',InitialPath,'Credit_Note',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Credit_Note',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Credit_Note',1)
    SolaceViewVars('save_rtp_id',save_rtp_id,'Credit_Note',1)
    SolaceViewVars('tmp:vat_rate',tmp:vat_rate,'Credit_Note',1)
    SolaceViewVars('tmp:Delivery_Name',tmp:Delivery_Name,'Credit_Note',1)
    SolaceViewVars('tmp:Delivery_Address_Line_1',tmp:Delivery_Address_Line_1,'Credit_Note',1)
    SolaceViewVars('save_res_id',save_res_id,'Credit_Note',1)
    SolaceViewVars('tmp:Invoice_Name',tmp:Invoice_Name,'Credit_Note',1)
    SolaceViewVars('tmp:Invoice_address_L',tmp:Invoice_address_L,'Credit_Note',1)
    SolaceViewVars('tmp:items_total',tmp:items_total,'Credit_Note',1)
    SolaceViewVars('tmp:vat',tmp:vat,'Credit_Note',1)
    SolaceViewVars('tmp:item_cost',tmp:item_cost,'Credit_Note',1)
    SolaceViewVars('tmp:line_cost',tmp:line_cost,'Credit_Note',1)
    SolaceViewVars('tmp:despatch_note_number',tmp:despatch_note_number,'Credit_Note',1)
    SolaceViewVars('tmp:line_cost_total',tmp:line_cost_total,'Credit_Note',1)
    SolaceViewVars('tmp:vat_total',tmp:vat_total,'Credit_Note',1)
    SolaceViewVars('tmp:Total',tmp:Total,'Credit_Note',1)
    
      Loop SolaceDim1# = 1 to 4
        SolaceFieldName" = 'tmp:payment_date' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:payment_date[SolaceDim1#],'Credit_Note',1)
      End
    
    
    SolaceViewVars('tmp:other_amount',tmp:other_amount,'Credit_Note',1)
    SolaceViewVars('tmp:total_paid',tmp:total_paid,'Credit_Note',1)
    
      Loop SolaceDim1# = 1 to 4
        SolaceFieldName" = 'tmp:payment_type' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:payment_type[SolaceDim1#],'Credit_Note',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 4
        SolaceFieldName" = 'tmp:payment_amount' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:payment_amount[SolaceDim1#],'Credit_Note',1)
      End
    
    
    SolaceViewVars('tmp:sub_total',tmp:sub_total,'Credit_Note',1)
    SolaceViewVars('tmp:courier_cost',tmp:courier_cost,'Credit_Note',1)
    SolaceViewVars('tmp:PartsCost',tmp:PartsCost,'Credit_Note',1)
    SolaceViewVars('tmp:labour_cost',tmp:labour_cost,'Credit_Note',1)


BuildCtrlQueue      Routine







