

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01013.INC'),ONCE        !Local module procedure declarations
                     END








Multiple_Chargeable_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
save_job_id          USHORT,AUTO
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Invoice_Number_Temp  STRING(28)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Labour_vat_info_temp STRING(15)
Parts_vat_info_temp  STRING(15)
batch_number_temp    STRING(28)
labour_vat_temp      REAL
parts_vat_temp       REAL
balance_temp         REAL
job_total_temp       REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Invoice_VAT_Number)
                       PROJECT(inv:Labour_Paid)
                       PROJECT(inv:Parts_Paid)
                     END
Report               REPORT,AT(396,4563,7521,4281),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,521,7521,4042)
                         STRING(@s28),AT(4917,917),USE(batch_number_temp),TRN,CENTER,FONT(,,,FONT:bold)
                         STRING('Page Number: '),AT(4083,2396),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@N4),AT(5521,2396),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s28),AT(4917,521),USE(Invoice_Number_Temp),TRN,CENTER,FONT(,,,FONT:bold)
                         STRING(@s30),AT(156,1677),USE(Invoice_Name_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('V.A.T. Number: '),AT(4083,1677),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,1844,1917,156),USE(Address_Line1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(5521,1677,1760,240),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2000),USE(Address_Line2_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2156),USE(Address_Line3_Temp),TRN,FONT(,8,,)
                         STRING('Date Of Invoice:'),AT(4083,1917),USE(?String31),TRN,FONT(,8,,)
                         STRING(@D6),AT(5521,1917),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@T3),AT(6156,1917),USE(ReportRunTime),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2323),USE(Address_Line4_Temp),TRN,FONT(,8,,)
                         STRING('Account Number: '),AT(4083,2156),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s15),AT(5521,2156,1000,240),USE(SUB:Account_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Job Number'),AT(323,3844),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(1240,3844),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3000,3844),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4396,3844),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(5521,3844),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING('Value'),AT(6677,3844),USE(?String48),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2521,1323,156),USE(inv:Invoice_VAT_Number),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@p<<<<<<<#p),AT(240,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(1240,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s20),AT(3000,0),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(4396,0),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                           STRING(@s10),AT(5521,0),USE(job:MSN),TRN,LEFT,FONT(,8,,)
                           STRING(@n14.2),AT(6156,0),USE(job:Invoice_Sub_Total),RIGHT,FONT(,8,,)
                         END
                         FOOTER,AT(396,8802,,2281),USE(?unnamed),ABSOLUTE
                           STRING('Parts Sub Total'),AT(4802,719),USE(?String37),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6323,719),USE(inv:Parts_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6323,875),USE(parts_vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6323,563),USE(labour_vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@p<<<<<<#p),AT(1042,719),USE(job_total_temp),TRN,RIGHT
                           STRING('Labour Sub Total'),AT(4802,396),USE(?String39),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6323,396),USE(inv:Labour_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING('Total Jobs: '),AT(396,719),USE(?String50),TRN
                           STRING(@s15),AT(4802,563),USE(Labour_vat_info_temp),TRN,LEFT,FONT(,8,,)
                           STRING('Balance Due'),AT(4802,1042),USE(?String38),TRN,FONT(,10,,FONT:bold)
                           STRING('<128>'),AT(6042,1042),USE(?Euro),TRN,HIDE,FONT(,10,,FONT:bold)
                           STRING(@n14.2),AT(6094,1042),USE(balance_temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING(@s15),AT(4802,875),USE(Parts_vat_info_temp),TRN,FONT(,8,,)
                         END
                       END
NewPage                DETAIL,PAGEAFTER(1),AT(,,,42),USE(?NewPage)
                       END
                       FOOTER,AT(396,10323,7521,1156)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198)
                         IMAGE('rinvdet.gif'),AT(0,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,83,3844,240),USE(def:Invoice_Company_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('INVOICE'),AT(5604,0),USE(?String20),TRN,FONT(,20,,FONT:bold)
                         STRING(@s30),AT(156,323,3844,156),USE(def:Invoice_Address_Line1),TRN
                         STRING(@s30),AT(156,479,3844,156),USE(def:Invoice_Address_Line2),TRN
                         STRING(@s30),AT(156,635,3844,156),USE(def:Invoice_Address_Line3),TRN
                         STRING(@s15),AT(156,802,1156,156),USE(def:Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s15),AT(677,958,3844,208),USE(def:Invoice_Telephone_Number)
                         STRING('Fax: '),AT(156,1094),USE(?String19),TRN
                         STRING(@s15),AT(677,1094,3844,208),USE(def:Invoice_Fax_Number)
                         STRING(@s255),AT(677,1250,3844,208),USE(def:InvoiceEmailAddress),TRN
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Multiple_Chargeable_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:JOBS.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(inv:Invoice_Number_Key)
      Process:View{Prop:Filter} = ''
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
            access:subtracc.clearkey(sub:account_number_key)
            sub:account_number = inv:account_number
            if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                invoice_name_temp      = sub:company_name
                address_line1_temp     = sub:address_line1
                address_line2_temp     = sub:address_line2
                If sup:address_line3   = ''
                    address_line3_temp = sub:postcode
                    address_line4_temp = ''
                Else
                    address_line3_temp  = sub:address_line3
                    address_line4_temp  = sub:postcode
                End
            end
            invoice_number_temp     = 'Invoice Number: ' & Format(glo:Queue2,@p<<<<<<<#p)
            labour_vat_info_temp    = 'V.A.T. @ ' & Format(inv:vat_rate_labour,@n6.2)
            labour_vat_temp         = inv:labour_paid * (inv:vat_rate_labour/100)
            parts_vat_info_temp     = 'V.A.T. @ ' & Format(inv:vat_rate_parts,@n6.2)
            parts_vat_temp          = inv:parts_paid * (inv:vat_rate_parts/100)
            balance_temp            = inv:labour_paid + labour_vat_temp + inv:parts_paid + parts_vat_temp
        !    end!if access:invoice.fetch(inv:invoice_number_key)
            job_total_temp = 0
            setcursor(cursor:wait)
            save_job_id = access:jobs.savefile()
            access:jobs.clearkey(job:InvoiceNumberKey)
            job:invoice_number = inv:invoice_number
            set(job:InvoiceNumberKey,job:InvoiceNumberKey)
            loop
                if access:jobs.next()
                   break
                end !if
                if job:invoice_number <> inv:invoice_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                job_total_temp += 1
                Print(rpt:detail)
            end !loop
            access:jobs.restorefile(save_job_id)
            setcursor()
            Print(rpt:newpage)
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:DETAIL)
          PRINT(RPT:NewPage)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'INVOICE')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  access:vatcode.clearkey(vat:vat_code_key)
  vat:vat_code = def:vat_rate_labour
  if access:vatcode.fetch(vat:vat_code_key) = level:benign
      labour_vat_info_temp = 'V.A.T. @ ' & Format(vat:vat_rate,@n6.2)
  end
  access:vatcode.clearkey(vat:vat_code_key)
  vat:vat_code = def:vat_rate_labour
  if access:vatcode.fetch(vat:vat_code_key) = level:benign
      parts_vat_info_temp = 'V.A.T. @ ' & Format(vat:vat_rate,@n6.2)
  end
  
  access:vatcode.clearkey(vat:vat_code_key)
  vat:vat_code = def:vat_rate_labour
  access:vatcode.tryfetch(vat:vat_code_key)
  labour_vat_info_temp = 'V.A.T. @ ' & Format(vat:vat_rate,@n6.2)
  labour_vat_temp      = inv:labour_paid * (vat:vat_rate/100)
  
  access:vatcode.clearkey(vat:vat_code_key)
  vat:vat_code = def:vat_rate_parts
  access:vatcode.tryfetch(vat:vat_code_key)
  parts_vat_info_temp = 'V.A.T. @ ' & Format(vat:vat_rate,@n6.2)
  parts_vat_temp      = inv:parts_paid * (vat:vat_rate/100)
  
  balance_temp            = inv:labour_paid + labour_vat_temp + inv:parts_paid + parts_vat_temp
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE'
  access:stantext.fetch(stt:description_key)
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Multiple_Chargeable_Invoice'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Multiple_Chargeable_Invoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Default_Invoice_Company_Name_Temp',Default_Invoice_Company_Name_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line1_Temp',Default_Invoice_Address_Line1_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line2_Temp',Default_Invoice_Address_Line2_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line3_Temp',Default_Invoice_Address_Line3_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Default_Invoice_Postcode_Temp',Default_Invoice_Postcode_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Default_Invoice_Telephone_Number_Temp',Default_Invoice_Telephone_Number_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Default_Invoice_Fax_Number_Temp',Default_Invoice_Fax_Number_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Default_Invoice_VAT_Number_Temp',Default_Invoice_VAT_Number_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('save_job_id',save_job_id,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('InitialPath',InitialPath,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Invoice_Number_Temp',Invoice_Number_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Labour_vat_info_temp',Labour_vat_info_temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('Parts_vat_info_temp',Parts_vat_info_temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('batch_number_temp',batch_number_temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('labour_vat_temp',labour_vat_temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('parts_vat_temp',parts_vat_temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('balance_temp',balance_temp,'Multiple_Chargeable_Invoice',1)
    SolaceViewVars('job_total_temp',job_total_temp,'Multiple_Chargeable_Invoice',1)


BuildCtrlQueue      Routine







