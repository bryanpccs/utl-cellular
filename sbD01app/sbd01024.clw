

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01024.INC'),ONCE        !Local module procedure declarations
                     END








Chargeable_Batch_Invoice PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:defaultfax       STRING(20)
save_job_id          USHORT,AUTO
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:AuthorisedQueue  QUEUE,PRE(autque)
AuthQty              LONG
AuthJobType          STRING(30)
AuthRepairType       STRING(30)
AuthItemPrice        REAL
AuthLinePrice        REAL
AuthVatAmount        REAL
                     END
Invoice_Number_Temp  STRING(28)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
tmp:AuthCount        LONG
tmp:LineTotal        REAL
tmp:VatTotal         REAL
tmp:QueryCount       LONG
tmp:ValueTotal       REAL
tmp:InvoiceTotal     REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:InvoiceQuery)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Sub_Total)
                     END
report               REPORT,AT(396,3854,7521,6500),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(396,521,7521,2948),USE(?unnamed)
                         STRING(@s30),AT(156,83,3844,240),USE(def:Invoice_Company_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,365,3844,156),USE(def:Invoice_Address_Line1),TRN
                         STRING('Invoice Date:'),AT(4896,521),USE(?String31:6),TRN,FONT(,8,,)
                         STRING(@s8),AT(5729,885),USE(inv:Batch_Number),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Invoice Number:'),AT(4896,365),USE(?String31:5),TRN,FONT(,8,,)
                         STRING(@s8),AT(5729,365),USE(inv:Invoice_Number),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(156,521,3844,156),USE(def:Invoice_Address_Line2),TRN
                         STRING(@d6b),AT(5729,521),USE(inv:Date_Created),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Account No:'),AT(2083,1667),USE(?String31:4),TRN,FONT(,8,,)
                         STRING('Batch Number:'),AT(4896,885),USE(?String31:2),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,677,3844,156),USE(def:Invoice_Address_Line3),TRN
                         STRING(@s15),AT(2760,1667),USE(inv:Account_Number),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(156,833,1156,156),USE(def:Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN
                         STRING(@s15),AT(573,1042),USE(def:Invoice_Telephone_Number),TRN
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN
                         STRING(@s15),AT(573,1198),USE(def:Invoice_Fax_Number),TRN
                         STRING('Page Number: '),AT(4896,1198),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@N4),AT(5729,1198),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Of '),AT(6042,1198),USE(?PagePrompt:2),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6198,1198,375,208),USE(?CPCSPgOfPgStr),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN
                         STRING(@s255),AT(573,1354,3844,208),USE(def:InvoiceEmailAddress),TRN
                         STRING(@s30),AT(156,1667),USE(Invoice_Name_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('V.A.T. Number: '),AT(156,2656),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s30),AT(990,2656),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,1844,1917,156),USE(Address_Line1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2000),USE(Address_Line2_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2156),USE(Address_Line3_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2323),USE(Address_Line4_Temp),TRN,FONT(,8,,)
                         STRING('Printed:'),AT(4896,1042),USE(?String46),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5729,1042),USE(ReportRunDate),TRN,FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6354,1042),USE(ReportRunTime),TRN,FONT(,8,,FONT:bold)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@s8),AT(156,0),USE(autque:AuthQty),TRN,RIGHT(1),FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(833,0),USE(autque:AuthJobType),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s30),AT(2760,0),USE(autque:AuthRepairType),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(5573,0),USE(autque:AuthLinePrice),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6458,0),USE(autque:AuthVatAmount),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
AuthTotal                DETAIL,AT(,,,583),USE(?AuthTotal)
                           LINE,AT(156,52,1042,0),USE(?Line5),COLOR(COLOR:Black)
                           LINE,AT(5625,52,1667,0),USE(?Line6),COLOR(COLOR:Black)
                           STRING('Totals:'),AT(156,104),USE(?String57:4),TRN,FONT('Arial',8,,FONT:bold)
                           STRING(@s8),AT(573,104),USE(tmp:AuthCount),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n14.2),AT(5573,104),USE(tmp:LineTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n14.2),AT(6458,104),USE(tmp:VatTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
QueryTitle               DETAIL,USE(?QueryTitle)
                           STRING('QUERY REPAIR DETAILS'),AT(156,52),USE(?String56),TRN,FONT('Arial',9,,FONT:bold)
                           LINE,AT(156,417,7135,0),USE(?Line1:3),COLOR(COLOR:Black)
                           STRING('Job No'),AT(156,260),USE(?String57),TRN,FONT('Arial',8,,FONT:bold)
                           STRING('Job Value (Net)'),AT(1146,260),USE(?String57:2),TRN,FONT('Arial',8,,FONT:bold)
                           STRING('Failure Reason'),AT(2292,260),USE(?String57:3),TRN,FONT('Arial',8,,FONT:bold)
                         END
Query                    DETAIL,AT(,,,427),USE(?Query)
                           TEXT,AT(2292,0,5052,365),USE(job:InvoiceQuery),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@s8),AT(156,0),USE(job:Ref_Number),TRN,RIGHT(1),FONT(,8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(1146,0),USE(job:Sub_Total),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                         END
QueryTotal               DETAIL,AT(,,,583),USE(?QueryTotal)
                           LINE,AT(156,52,1823,0),USE(?Line7),COLOR(COLOR:Black)
                           STRING('Totals:'),AT(156,104),USE(?String57:5),TRN,FONT('Arial',8,,FONT:bold)
                           STRING(@n14.2),AT(1146,104),USE(tmp:ValueTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(573,104),USE(tmp:QueryCount),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
                         FOOTER,AT(198,9000,,1198),USE(?unnamed:2),ABSOLUTE
                           STRING('Line Total:'),AT(5000,260),USE(?String55),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6458,260),USE(tmp:LineTotal,,?tmp:LineTotal:2),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING('V.A.T. Total:'),AT(5000,469),USE(?String55:2),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6458,469),USE(tmp:VatTotal,,?tmp:VatTotal:2),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(5000,677,2292,0),USE(?Line9),COLOR(COLOR:Black)
                           STRING(@n14.2),AT(6458,729),USE(tmp:InvoiceTotal),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING('<128>'),AT(6406,729),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                           STRING('Proforma Inv Total:'),AT(5000,729),USE(?String55:3),TRN,FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(385,10323,7521,1156),USE(?unnamed:4)
                         LINE,AT(156,52,7240,0),USE(?Line8),COLOR(COLOR:Black)
                         TEXT,AT(156,156,7240,781),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('rinvsim.gif'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('BATCH INVOICE'),AT(5885,104),USE(?String49),TRN,FONT('Arial',14,,FONT:bold)
                         STRING('PROFORMA INVOICE TOTAL'),AT(4740,8490),USE(?String56:2),TRN,FONT('Arial',9,,FONT:bold)
                         STRING('Qty'),AT(417,3177),USE(?String47),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Job Type'),AT(833,3177),USE(?String47:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Repair Type'),AT(2760,3177),USE(?String47:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Price'),AT(5938,3177),USE(?String47:5),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T. Amount'),AT(6573,3177),USE(?String47:6),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Chargeable_Batch_Invoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:PROINV.Open
  Relate:STANTEXT.Open
  Access:USERS.UseFile
  Access:INVOICE.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        !Alternative Invoice Address
        If def:use_invoice_address = 'YES'
            Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
            Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
            Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
            Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
            Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
            Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
            Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
            Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
        Else!If def:use_invoice_address = 'YES'
            Default_Invoice_Company_Name_Temp       = DEF:User_Name
            Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
            Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
            Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
            Default_Invoice_Postcode_Temp           = DEF:Postcode
            Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
            Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
            Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
        End!If def:use_invoice_address = 'YES'
        Set(DEFAULT2)
        Access:DEFAULT2.Next()
        
        access:invoice.clearkey(inv:invoice_number_key)
        inv:invoice_number  = glo:select1
        If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
            Case inv:accounttype
                Of 'MAI'
                    access:tradeacc.clearkey(tra:account_number_key)
                    tra:account_number  = inv:Account_Number
                    If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                        Invoice_Name_Temp   = tra:company_name
                        Address_Line1_Temp  = tra:address_line1
                        Address_Line2_Temp  = tra:address_line2
                        Address_Line3_Temp  = tra:address_line3
                        Address_Line4_Temp  = tra:postcode
                    End!If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
        
                Of 'SUB'
                    access:subtracc.clearkey(sub:account_number_key)
                    sub:account_number  = inv:Account_Number
                    If access:subtracc.tryfetch(tra:account_number_key) = Level:Benign
                        Invoice_Name_Temp   = sub:company_name
                        Address_Line1_Temp  = sub:address_line1
                        Address_Line2_Temp  = sub:address_line2
                        Address_Line3_Temp  = sub:address_line3
                        Address_Line4_Temp  = sub:postcode
                    End!If access:subtracc.tryfetch(tra:account_number_key) = Level:Benign
        
            End!Case inv:accounttype
        
            save_job_id = access:jobs.savefile()
            access:jobs.clearkey(job:batchstatuskey)
            job:invoiceaccount = inv:Account_Number
            job:invoicebatch   = inv:Batch_Number
            set(job:batchstatuskey,job:batchstatuskey)
            loop
                if access:jobs.next()
                   break
                end !if
                if job:invoiceaccount <> inv:Account_Number      |
                or job:invoicebatch   <> inv:Batch_Number      |
                    then break.  ! end if
                tmp:AuthCount += 1
                Sort(tmp:AuthorisedQueue,autque:AuthJobType,autque:AuthRepairType)
                autque:authrepairtype   = job:repair_type
                Get(tmp:AuthorisedQueue,autque:AuthRepairType)
                If ~Error()
                    autque:Authitemprice += ''
                    autque:AuthLinePrice += job:invoice_sub_total
                    autque:AuthVatAmount   += Round(job:invoice_labour_cost * inv:vat_rate_labour/100,.01) + |
                                                Round(job:invoice_parts_cost * inv:vat_rate_parts/100,.01) + |
                                                Round(job:invoice_courier_cost * inv:vat_rate_labour/100,.01)
                    autque:Authqty += 1
                    Put(tmp:AuthorisedQueue)
                Else!If Error()
                    autque:AuthQty          = 1
                    autque:AuthJobType      = job:charge_type
                    autque:AuthRepairType   = job:repair_type
                    autque:AuthItemPrice    = ''
                    autque:AuthLinePrice    = job:invoice_sub_total
                    autque:AuthVatAmount    = Round(job:invoice_labour_cost * inv:vat_rate_labour/100,.01) + |
                                                Round(job:invoice_parts_cost * inv:vat_rate_parts/100,.01) + |
                                                Round(job:invoice_courier_cost * inv:vat_rate_labour/100,.01)
                    Add(tmp:AuthorisedQueue)
                End!If Error()
                tmp:LineTotal += job:invoice_sub_total
                tmp:VatTotal  += Round(job:invoice_labour_cost * inv:vat_rate_labour/100,.01) + |
                                                Round(job:invoice_parts_cost * inv:vat_rate_parts/100,.01) + |
                                                Round(job:invoice_courier_cost * inv:vat_rate_labour/100,.01)
            end !loop
            access:jobs.restorefile(save_job_id)
        
            If Records(tmp:AuthorisedQueue)
                Loop x# = 1 To Records(tmp:AuthorisedQueue)
                    Get(tmp:AuthorisedQueue,x#)
                    Print(rpt:detail)
                End!Loop x# = 1 To Records(tmp:AuthorisedQueue)
                tmp:InvoiceTotal    = tmp:LineTotal + tmp:VatTotal
                Print(rpt:AuthTotal)
            End!If Records(tmp:AuthorisedQueue)
        End!If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:PROINV.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','INVOICE - PROFORMA')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE - PROFORMA'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rinvsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'INVOICE - PROFORMA'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'INVOICE - PROFORMA'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Chargeable_Batch_Invoice'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Chargeable_Batch_Invoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Default_Invoice_Company_Name_Temp',Default_Invoice_Company_Name_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:defaultfax',tmp:defaultfax,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('save_job_id',save_job_id,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line1_Temp',Default_Invoice_Address_Line1_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line2_Temp',Default_Invoice_Address_Line2_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line3_Temp',Default_Invoice_Address_Line3_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Default_Invoice_Postcode_Temp',Default_Invoice_Postcode_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Default_Invoice_Telephone_Number_Temp',Default_Invoice_Telephone_Number_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Default_Invoice_Fax_Number_Temp',Default_Invoice_Fax_Number_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Default_Invoice_VAT_Number_Temp',Default_Invoice_VAT_Number_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('InitialPath',InitialPath,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:telephonenumber',tmp:telephonenumber,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:faxnumber',tmp:faxnumber,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:AuthorisedQueue:AuthQty',tmp:AuthorisedQueue:AuthQty,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:AuthorisedQueue:AuthJobType',tmp:AuthorisedQueue:AuthJobType,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:AuthorisedQueue:AuthRepairType',tmp:AuthorisedQueue:AuthRepairType,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:AuthorisedQueue:AuthItemPrice',tmp:AuthorisedQueue:AuthItemPrice,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:AuthorisedQueue:AuthLinePrice',tmp:AuthorisedQueue:AuthLinePrice,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:AuthorisedQueue:AuthVatAmount',tmp:AuthorisedQueue:AuthVatAmount,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Invoice_Number_Temp',Invoice_Number_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:AuthCount',tmp:AuthCount,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:LineTotal',tmp:LineTotal,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:VatTotal',tmp:VatTotal,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:QueryCount',tmp:QueryCount,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:ValueTotal',tmp:ValueTotal,'Chargeable_Batch_Invoice',1)
    SolaceViewVars('tmp:InvoiceTotal',tmp:InvoiceTotal,'Chargeable_Batch_Invoice',1)


BuildCtrlQueue      Routine







