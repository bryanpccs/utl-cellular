

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01003.INC'),ONCE        !Local module procedure declarations
                     END








Third_Party_Despatch_Note PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
tmp:PrintedBy        STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
printed_temp         BYTE(0)
pos                  STRING(255)
count_temp           REAL
user_name_temp       STRING(40)
job_count_temp       REAL
total_jobs_temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:DefaultEmail     STRING(255)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                       PROJECT(job:DOP)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Unit_Type)
                     END
Report               REPORT,AT(396,2740,7521,7458),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,1677),USE(?unnamed)
                         STRING('Service Centre:'),AT(4906,566),USE(?String35),TRN,FONT(,8,,)
                         STRING('Despatch Date:'),AT(4906,943),USE(?String36),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5849,943),USE(GLO:Select3),TRN,FONT(,8,,FONT:bold)
                         STRING('Raised By:'),AT(4906,1132),USE(?String31),TRN,FONT(,8,,)
                         STRING(@s40),AT(5849,1132),USE(user_name_temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s3),AT(5849,1321),USE(ReportPageNo),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(5849,566),USE(trd:Company_Name),TRN,FONT(,8,,FONT:bold)
                         STRING('Account Number: '),AT(4906,755),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s30),AT(5849,755),USE(trd:Account_Number),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('Page Number'),AT(4906,1321,917,198),USE(?String34),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
detail1                  DETAIL,USE(?unnamed:2)
                           STRING(@s16),AT(188,0),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                           STRING(@s25),AT(3740,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s16),AT(1229,0),USE(job:MSN),TRN,LEFT,FONT(,8,,)
                           STRING(@s9),AT(2313,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s30),AT(5385,0),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                           STRING(@d6),AT(3021,0),USE(job:DOP),TRN,LEFT,FONT(,8,,)
                         END
                         FOOTER,AT(0,0,,719),USE(?unnamed:5)
                           LINE,AT(365,83,6927,1),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(5760,156),USE(?String30),TRN,FONT(,10,,FONT:bold)
                           STRING(@n8),AT(6677,156),USE(count_temp),TRN,LEFT,FONT(,10,,FONT:bold)
                         END
                       END
                       FOOTER,AT(385,10260,7521,1156),USE(?unnamed:3),FONT('Arial',,,)
                         LINE,AT(365,52,6802,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(156,156,7198,958),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(396,469,7521,11198),USE(?unnamed:4)
                         IMAGE('rlistsim.gif'),AT(0,0,7521,11198),USE(?Image1)
                         STRING(@s30),AT(156,52,3844,240),USE(def:User_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING('THIRD PARTY DESPATCH NOTE'),AT(4323,52),USE(?title),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,313,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,469,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,625,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,781,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,990),USE(?String16),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,990),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1146),USE(?String19),TRN,FONT(,9,,)
                         STRING(@s20),AT(573,1146),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1302,3844,198),USE(tmp:DefaultEmail),TRN,FONT(,9,,)
                         STRING('D.O.P.'),AT(3313,2083),USE(?String37),TRN,FONT(,8,,FONT:bold)
                         STRING('Email:'),AT(156,1302),USE(?String19:2),TRN,FONT(,9,,)
                         STRING('I.M.E.I. Number'),AT(156,2083),USE(?String29),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(1229,2083),USE(?String29:2),TRN,FONT(,8,,FONT:bold)
                         STRING('Job Number'),AT(2229,2083),USE(?String29:3),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Nuber'),AT(3740,2083),USE(?String29:4),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(5354,2083),USE(?String29:5),TRN,FONT(,8,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Third_Party_Despatch_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Relate:STANTEXT.Open
  Access:TRDPARTY.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job:ThirdEsnKey)
      Process:View{Prop:Filter} = |
      'UPPER(job:Third_Party_Site) = UPPER(GLO:Select1) AND (Upper(job:third_' & |
      'party_despatch_printed) = Upper(glo:select2))'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        printed_temp = 1
        If glo:select2 = 'YES'
            If job:ThirdPartyDateDesp = glo:select3
                count_temp += 1
                Print(rpt:detail1)
            End!If job:third_party_despatch_date < glo:select3 Or job:third_party_despatch_date > glo:select4
        Else!If glo:select2 = 'YES'
            pos = Position(job:thirdesnkey)
            job:Third_Party_Printed = 'YES'
            job:ThirdPartyDateDesp = Today()
            access:jobs.update()
            Reset(job:thirdesnkey,pos)
            count_temp += 1
            Print(rpt:detail1)
        End!If glo:select2 = 'YES'
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','DESPATCH NOTE - THIRD PARTY')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'DESPATCH NOTE - THIRD PARTY'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE - THIRD PARTY'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'DESPATCH NOTE - THIRD PARTY'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='Third_Party_Despatch_Note'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Third_Party_Despatch_Note',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Third_Party_Despatch_Note',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Third_Party_Despatch_Note',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Third_Party_Despatch_Note',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Third_Party_Despatch_Note',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Third_Party_Despatch_Note',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Third_Party_Despatch_Note',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Third_Party_Despatch_Note',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Third_Party_Despatch_Note',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Third_Party_Despatch_Note',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Third_Party_Despatch_Note',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Third_Party_Despatch_Note',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Third_Party_Despatch_Note',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Third_Party_Despatch_Note',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Third_Party_Despatch_Note',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Third_Party_Despatch_Note',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Third_Party_Despatch_Note',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Third_Party_Despatch_Note',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Third_Party_Despatch_Note',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Third_Party_Despatch_Note',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Third_Party_Despatch_Note',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Third_Party_Despatch_Note',1)
    SolaceViewVars('InitialPath',InitialPath,'Third_Party_Despatch_Note',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Third_Party_Despatch_Note',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Third_Party_Despatch_Note',1)
    SolaceViewVars('printed_temp',printed_temp,'Third_Party_Despatch_Note',1)
    SolaceViewVars('pos',pos,'Third_Party_Despatch_Note',1)
    SolaceViewVars('count_temp',count_temp,'Third_Party_Despatch_Note',1)
    SolaceViewVars('user_name_temp',user_name_temp,'Third_Party_Despatch_Note',1)
    SolaceViewVars('job_count_temp',job_count_temp,'Third_Party_Despatch_Note',1)
    SolaceViewVars('total_jobs_temp',total_jobs_temp,'Third_Party_Despatch_Note',1)
    SolaceViewVars('tmp:DefaultEmail',tmp:DefaultEmail,'Third_Party_Despatch_Note',1)


BuildCtrlQueue      Routine







