

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01017.INC'),ONCE        !Local module procedure declarations
                     END








Retail_Picking_Note PROCEDURE(func:type)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
save_res_id          USHORT,AUTO
tmp:PrintedBy        STRING(60)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Address_Group        GROUP,PRE()
Postcode             STRING(10)
Company_Name         STRING(30)
Building_Name        STRING(30)
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Telephone_Number     STRING(15)
Fax_Number           STRING(15)
                     END
Address_Delivery_Group GROUP,PRE()
Postcode_Delivery    STRING(10)
Company_Name_Delivery STRING(30)
Building_Name_Delivery STRING(30)
Address_Line1_Delivery STRING(30)
Address_Line2_Delivery STRING(30)
Address_Line3_Delivery STRING(30)
Telephone_Delivery   STRING(15)
Fax_Number_Delivery  STRING(15)
                     END
Purchase_Order_Number STRING(30)
Account_Number       STRING(15)
Stock_queue          QUEUE,PRE(STOQUE)
Ref_Number           REAL
Location             STRING(30)
Shelf_Location       STRING(30)
Second_Location      STRING(30)
Quantity             REAL
                     END
Delivery_Name_Temp   STRING(30)
Delivery_Address_Line_1_Temp STRING(60)
Invoice_Name_temp    STRING(30)
Invoice_address_Line_1 STRING(60)
items_total_temp     REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Type             STRING(4)
!-----------------------------------------------------------------------------
Process:View         VIEW(RETSALES)
                       PROJECT(ret:Contact_Name)
                       PROJECT(ret:Ref_Number)
                       JOIN(res:Part_Number_Key,ret:Ref_Number)
                       END
                     END
report               REPORT,AT(396,3875,7521,6010),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(354,760,7521,2740),USE(?unnamed)
                         STRING('Date Printed:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,156),USE(ReportRunDate),TRN,LEFT,FONT('Arial',8,,FONT:bold)
                         STRING(@t1),AT(6615,156),USE(ReportRunTime),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2240),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('Sales Number:'),AT(5000,365),USE(?string22:2),TRN,FONT(,8,,)
                         STRING('Account No:'),AT(5000,573),USE(?string22:3),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,365),USE(GLO:Select1),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Purch Order No:'),AT(5000,781),USE(?string22:6),TRN,FONT(,8,,)
                         STRING(@s15),AT(5885,573),USE(Account_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Contact Name:'),AT(5000,990),USE(?string22:7),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,781),USE(Purchase_Order_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5885,990),USE(ret:Contact_Name),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1458),USE(Delivery_Name_Temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1458),USE(Invoice_Name_temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(208,1615,3438,208),USE(Delivery_Address_Line_1_Temp),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s60),AT(4063,1615,3281,208),USE(Invoice_address_Line_1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1771),USE(Address_Line2_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1771),USE(Address_Line2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(521,2240),USE(Telephone_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(4063,2240),USE(?string22:9),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2240),USE(Telephone_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(208,2396),USE(?string22:5),TRN,FONT(,8,,)
                         STRING(@s10),AT(208,2083),USE(Postcode_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s10),AT(4063,2083),USE(Postcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1927),USE(Address_Line3_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1927),USE(Address_Line3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s15),AT(521,2396),USE(Fax_Number_Delivery),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(4063,2396),USE(?string22:8),TRN,FONT(,8,,)
                         STRING(@s15),AT(4323,2396),USE(Fax_Number),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break1                 BREAK(RES:Ref_Number),USE(?unnamed:5)
detail1                  DETAIL,AT(,,,188),USE(?unnamed:6)
                           STRING(@s18),AT(167,0),USE(sto:Location),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s18),AT(1354,0),USE(sto:Shelf_Location),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s14),AT(2552,0),USE(sto:Second_Location),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s6),AT(6771,0),USE(STOQUE:Quantity),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           BOX,AT(7240,0,156,156),USE(?Box1),COLOR(COLOR:Black)
                           STRING(@s4),AT(6458,0),USE(tmp:Type),FONT(,8,,)
                           STRING(@s20),AT(3490,0),USE(sto:Part_Number),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s25),AT(4792,0),USE(sto:Description),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,510),USE(?unnamed:7)
                           LINE,AT(208,208,7083,0),USE(?line1),COLOR(COLOR:Black)
                           STRING('Total No Of Lines:'),AT(208,260),USE(?string26),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(1302,260),USE(tmp:RecordsCount),TRN,FONT(,8,,FONT:bold)
                           STRING('Total No Of Items:'),AT(2396,260),USE(?string26:2),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(3438,260),USE(items_total_temp),TRN,DECIMAL(14),FONT(,8,,FONT:bold)
                         END
                       END
                       FOOTER,AT(385,10198,7521,958),USE(?unnamed:4)
                         TEXT,AT(208,52,7083,781),USE(stt:Text),TRN,FONT(,8,,)
                       END
                       FORM,AT(354,479,7521,11198),USE(?unnamed:3)
                         IMAGE('rlistdet.gif'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('PICKING NOTE'),AT(4271,0,3177,260),USE(?string20),TRN,RIGHT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,990),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,990),USE(tmp:DefaultTelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1146),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1146),USE(tmp:DefaultFax),TRN,FONT(,9,,)
                         STRING(@s255),AT(521,1302,3844,198),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1302),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Qty'),AT(6979,3177),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Type'),AT(6500,3177),USE(?string24:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Part Number'),AT(3583,3177),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(4885,3177),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Site Location'),AT(208,3177),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Shelf Location'),AT(1396,3177),USE(?string25:2),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('2nd Location'),AT(2542,3177),USE(?string25:3),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Picked By:'),AT(240,9510),USE(?String54),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Name (Capitals):'),AT(2792,9510),USE(?String55),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Picked:'),AT(5813,9510),USE(?String56),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(260,9688,7031,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING('DELIVERY ADDRESS'),AT(208,1521),USE(?string44:2),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE ADDRESS'),AT(4063,1521),USE(?string44:3),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Retail_Picking_Note')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:RETSALES.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:STANTEXT.Open
  Relate:STOCK.Open
  Access:RETSTOCK.UseFile
  
  
  RecordsToProcess = RECORDS(RETSALES)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(RETSALES,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      ! Before Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      RecordsToProcess = Records(retstock)
      ! After Embed Point: %HandCodeEventOpnWin) DESC(*HandCode* Top of EVENT:OpenWindow) ARG()
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        access:retsales.clearkey(ret:ref_number_key)
        ret:ref_number = glo:select1
        if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
        
            If func:Type = 'ALL'
        
            End!If func:Type = 'ALL'
            save_res_id = access:retstock.savefile()
        
            If func:Type = 'ALL'
                access:retstock.clearkey(res:Part_Number_key)
                res:ref_number = ret:ref_number
                Set(res:Part_Number_Key,res:Part_Number_Key)
            Else
                access:retstock.clearkey(res:Despatched_Only_Key)
                res:ref_number = ret:ref_number
                res:despatched = func:type
                Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
            End!If func:Type = 'ALL'
            
            loop
                if access:retstock.next()
                   break
                end !if
                if res:ref_number <> ret:ref_number
                    Break
                End!if res:ref_number <> ret:ref_number
                If func:Type <> 'ALL'
                    If res:despatched <> func:Type
                        Break
                    End!If res:despatched <> func:Type
                End!If func:Type <> 'ALL'
                tmp:RecordsCount += 1
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = res:part_ref_number
                access:stock.tryfetch(sto:ref_number_key)
        
                Sort(Stock_Queue,stoque:Ref_Number)
                stoque:Ref_Number   = sto:Ref_Number
                Get(Stock_Queue,stoque:Ref_Number)
                If Error()
                    stoque:ref_number   = sto:ref_number
                    stoque:location     = sto:location
                    stoque:shelf_location   = sto:shelf_location
                    stoque:second_location  = sto:second_location
                    stoque:quantity = res:quantity
                    Add(Stock_Queue)
                Else !If Error()
                    stoque:Quantity += res:Quantity
                    Put(Stock_Queue)
                End !If Error()
            end !loop
            access:retstock.restorefile(save_res_id)
        
            RecordsToProcess += Records(Stock_queue)
        
            Sort(Stock_queue,stoque:location,stoque:shelf_location,stoque:second_location)
            Loop x# = 1 To Records(Stock_queue)
                Get(stock_queue,x#)
                RecordsProcessed += 1
                Do Displayprogress
        
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number  = stoque:ref_number
                If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                    items_total_temp    += stoque:quantity
                    Print(rpt:detail1)
                End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            End!Loop x# = 1 To Records(Stock_queue)
        end!if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(RETSALES,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:RETSALES.Close
    Relate:STANTEXT.Close
    Relate:STOCK.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','PICKING NOTE - RETAIL')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'PICKING NOTE - RETAIL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  access:retsales.clearkey(ret:ref_number_key)
  ret:ref_number = glo:select1
  if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      If ret:Payment_Method = 'EXC'
          tmp:Type = 'EXCH'
      Else !If ret:Payment_Method = 'EXC'
          tmp:Type = 'SALE'
      End !If ret:Payment_Method = 'EXC'
  
      If ret:payment_method = 'CAS'
          Delivery_Name_Temp  = RET:Contact_Name
          Invoice_Name_Temp   = ret:contact_name
      Else!If ret:payment_method = 'CAS'
          Delivery_Name_Temp  = RET:Company_Name_Delivery
          invoice_name_temp   = ret:company_name
      End!If ret:payment_method = 'CAS'
      If ret:building_name    = ''
          Invoice_address_Line_1     = Clip(RET:Address_Line1)
      Else!    If ret:building_name    = ''
          Invoice_address_Line_1     = Clip(RET:Building_Name) & ' ' & Clip(RET:Address_Line1)
      End!
      If ret:building_name_delivery   = ''
          Delivery_Address_Line_1_Temp    = Clip(RET:Address_Line1_Delivery)
      Else!    If ret:building_name_delivery   = ''
          Delivery_Address_Line_1_Temp    = Clip(RET:Building_Name_Delivery) & ' ' & Clip(RET:Address_Line1_Delivery)
      End!    If ret:building_name_delivery   = ''
  
  
      Postcode= ret:Postcode
      Company_Name=ret:Company_Name
      Building_Name=ret:Building_Name
      Address_Line1=ret:Address_Line1
      Address_Line2=ret:Address_Line2
      Address_Line3=ret:Address_Line3
      Telephone_Number=ret:Telephone_Number
      Fax_Number=ret:Fax_Number
  
  
      Postcode_Delivery= ret:Postcode_Delivery
      Company_Name_Delivery=ret:Company_Name_Delivery
      Building_Name_Delivery=ret:Building_Name_Delivery
      Address_Line1_Delivery=ret:Address_Line1_Delivery
      Address_Line2_Delivery=ret:Address_Line2_Delivery
      Address_Line3_Delivery=ret:Address_Line3_Delivery
      Telephone_Delivery=ret:Telephone_Delivery
      Fax_Number_Delivery=ret:Fax_Number_Delivery
      Purchase_Order_Number = ret:purchase_order_number
      Account_Number = ret:account_number
  End !access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'PICKING NOTE - RETAIL'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'PICKING NOTE - RETAIL'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Retail_Picking_Note'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Retail_Picking_Note',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Retail_Picking_Note',1)
    SolaceViewVars('save_res_id',save_res_id,'Retail_Picking_Note',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'Retail_Picking_Note',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Retail_Picking_Note',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Retail_Picking_Note',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Retail_Picking_Note',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Retail_Picking_Note',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Retail_Picking_Note',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Retail_Picking_Note',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Retail_Picking_Note',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Retail_Picking_Note',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Retail_Picking_Note',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Retail_Picking_Note',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Retail_Picking_Note',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Retail_Picking_Note',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Retail_Picking_Note',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Retail_Picking_Note',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Retail_Picking_Note',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Retail_Picking_Note',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Retail_Picking_Note',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Retail_Picking_Note',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Retail_Picking_Note',1)
    SolaceViewVars('InitialPath',InitialPath,'Retail_Picking_Note',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Retail_Picking_Note',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Group:Postcode',Address_Group:Postcode,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Group:Company_Name',Address_Group:Company_Name,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Group:Building_Name',Address_Group:Building_Name,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Group:Address_Line1',Address_Group:Address_Line1,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Group:Address_Line2',Address_Group:Address_Line2,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Group:Address_Line3',Address_Group:Address_Line3,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Group:Telephone_Number',Address_Group:Telephone_Number,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Group:Fax_Number',Address_Group:Fax_Number,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Delivery_Group:Postcode_Delivery',Address_Delivery_Group:Postcode_Delivery,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Delivery_Group:Company_Name_Delivery',Address_Delivery_Group:Company_Name_Delivery,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Delivery_Group:Building_Name_Delivery',Address_Delivery_Group:Building_Name_Delivery,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Delivery_Group:Address_Line1_Delivery',Address_Delivery_Group:Address_Line1_Delivery,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Delivery_Group:Address_Line2_Delivery',Address_Delivery_Group:Address_Line2_Delivery,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Delivery_Group:Address_Line3_Delivery',Address_Delivery_Group:Address_Line3_Delivery,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Delivery_Group:Telephone_Delivery',Address_Delivery_Group:Telephone_Delivery,'Retail_Picking_Note',1)
    SolaceViewVars('Address_Delivery_Group:Fax_Number_Delivery',Address_Delivery_Group:Fax_Number_Delivery,'Retail_Picking_Note',1)
    SolaceViewVars('Purchase_Order_Number',Purchase_Order_Number,'Retail_Picking_Note',1)
    SolaceViewVars('Account_Number',Account_Number,'Retail_Picking_Note',1)
    SolaceViewVars('Stock_queue:Ref_Number',Stock_queue:Ref_Number,'Retail_Picking_Note',1)
    SolaceViewVars('Stock_queue:Location',Stock_queue:Location,'Retail_Picking_Note',1)
    SolaceViewVars('Stock_queue:Shelf_Location',Stock_queue:Shelf_Location,'Retail_Picking_Note',1)
    SolaceViewVars('Stock_queue:Second_Location',Stock_queue:Second_Location,'Retail_Picking_Note',1)
    SolaceViewVars('Stock_queue:Quantity',Stock_queue:Quantity,'Retail_Picking_Note',1)
    SolaceViewVars('Delivery_Name_Temp',Delivery_Name_Temp,'Retail_Picking_Note',1)
    SolaceViewVars('Delivery_Address_Line_1_Temp',Delivery_Address_Line_1_Temp,'Retail_Picking_Note',1)
    SolaceViewVars('Invoice_Name_temp',Invoice_Name_temp,'Retail_Picking_Note',1)
    SolaceViewVars('Invoice_address_Line_1',Invoice_address_Line_1,'Retail_Picking_Note',1)
    SolaceViewVars('items_total_temp',items_total_temp,'Retail_Picking_Note',1)
    SolaceViewVars('tmp:Type',tmp:Type,'Retail_Picking_Note',1)


BuildCtrlQueue      Routine







