

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD01022.INC'),ONCE        !Local module procedure declarations
                     END








Single_Invoice_Summary PROCEDURE
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
TMP:Account_No       STRING(20)
tmp:totalcourier     REAL
tmp:totalparts       REAL
tmp:totallabour      REAL
tmp:totalexvat       REAL
tmp:totalinvat       REAL
tmp:totalvat         REAL
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:printedby        STRING(60)
tmp:telephonenumber  STRING(20)
tmp:faxnumber        STRING(20)
tmp:total            REAL
tmp:count            LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Invoice_Number)
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,927,7521,1000),USE(?unnamed)
                         STRING(@s20),AT(5885,167),USE(TMP:Account_No),TRN,FONT('Arial',8,,FONT:bold)
                         STRING('Account No.'),AT(5000,167),USE(?String64),TRN,FONT('Arial',8,,)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,781),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,781),USE(?string26),TRN,FONT(,8,,FONT:bold)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,167),USE(?detailband)
                           STRING(@s8),AT(156,0),USE(inv:Invoice_Number),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(5042,0),USE(job:Invoice_Labour_Cost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s8),AT(729,0),USE(job:Ref_Number),TRN,RIGHT(1),FONT(,7,,,CHARSET:ANSI)
                           STRING(@s30),AT(1250,0,1042,156),USE(job:Order_Number),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(4469,0),USE(job:Invoice_Courier_Cost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(5615,0),USE(job:Invoice_Parts_Cost),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(6188,0),USE(job:Invoice_Sub_Total),TRN,RIGHT,FONT('Arial',7,,FONT:regular,CHARSET:ANSI)
                           STRING(@n10.2),AT(6771,0),USE(tmp:total),TRN,RIGHT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(3438,0,1094,156),USE(job:Repair_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                           STRING(@s20),AT(2333,0,1146,156),USE(job:Charge_Type),TRN,LEFT,FONT(,7,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,1813),USE(?unnamed:2)
                           STRING(@n10.2),AT(4375,52),USE(tmp:totalcourier),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(4948,52),USE(tmp:totallabour),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(5521,52),USE(tmp:totalparts),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           LINE,AT(156,0,7240,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING(@n10.2),AT(6094,52),USE(tmp:totalexvat),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n10.2),AT(6677,52),USE(tmp:totalinvat),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Summary Analysis for'),AT(208,469),USE(?String62),TRN,FONT('Arial',8,,)
                           STRING(@s8),AT(1323,469),USE(tmp:count,,?tmp:count:2),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING('Jobs'),AT(1938,469),USE(?String63),TRN,FONT('Arial',8,,)
                           STRING('Courier'),AT(208,677),USE(?String57),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2),AT(990,1208),USE(tmp:totalexvat,,?tmp:totalexvat:2),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING('VAT'),AT(208,1385),USE(?String61),TRN,FONT('Arial',8,,)
                           STRING(@n-14.2),AT(990,1385),USE(tmp:totalvat),TRN,RIGHT,FONT('Arial',8,,)
                           LINE,AT(1042,1563,729,0),USE(?Line2),COLOR(COLOR:Black)
                           STRING('Sub Total'),AT(208,1208),USE(?String56),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING('Total'),AT(208,1594),USE(?String58),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2),AT(990,1031),USE(tmp:totalparts,,?tmp:totalparts:2),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2),AT(990,854),USE(tmp:totallabour,,?tmp:totallabour:2),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Parts'),AT(208,1031),USE(?String55),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2),AT(990,1594),USE(tmp:totalinvat,,?tmp:totalinvat:2),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING(@n-14.2),AT(990,677),USE(tmp:totalcourier,,?tmp:totalcourier:2),TRN,RIGHT,FONT(,8,,,CHARSET:ANSI)
                           STRING('Labour'),AT(208,854),USE(?String54),TRN,FONT(,8,,,CHARSET:ANSI)
                           STRING('<128>'),AT(4323,52),USE(?Euro),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING('Total: '),AT(208,52,313,156),USE(?String40),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(781,52),USE(tmp:count),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:Invoice_Company_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('INVOICE SUMMARY'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,3844,156),USE(def:Invoice_Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,3844,156),USE(def:Invoice_Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,3844,156),USE(def:Invoice_Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Invoice_Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s15),AT(573,1042),USE(def:Invoice_Telephone_Number),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s15),AT(573,1198),USE(def:Invoice_Fax_Number),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1354,3844,198),USE(def:InvoiceEmailAddress),TRN,FONT(,9,,)
                         STRING('Email:'),AT(156,1354),USE(?string19:2),TRN,FONT(,9,,)
                         STRING('Inv No'),AT(208,2083),USE(?string44),TRN,FONT(,7,,FONT:bold)
                         STRING('Order No'),AT(1250,2083),USE(?string45),TRN,FONT(,7,,FONT:bold)
                         STRING('Labour'),AT(5156,2083),USE(?string24),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Parts'),AT(5781,2083),USE(?string25),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Total (Ex VAT)'),AT(6042,2083),USE(?string25:3),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Total (Inc VAT)'),AT(6719,2083),USE(?string25:4),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Repair Type'),AT(3438,2083),USE(?string25:6),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Charge Type'),AT(2333,2083),USE(?string25:5),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Courier'),AT(4531,2083),USE(?string25:2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Job No'),AT(729,2083),USE(?string44:2),TRN,FONT(,7,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('Single_Invoice_Summary')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 0
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:INVOICE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Access:JOBS.UseFile
  
  
  RecordsToProcess = RECORDS(INVOICE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(INVOICE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        Loop x# = 1 To Records(glo:Queue3)
            Get(glo:Queue3,x#)
            access:invoice.clearkey(inv:invoice_number_key)
            inv:invoice_number  = glo:pointer3
            If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
                access:jobs.clearkey(job:ref_number_key)
                job:invoice_number  = glo:pointer3
                If access:jobs.tryfetch(job:InvoiceNumberKey) = Level:Benign
                    Total_Price('I',vat",total",balance")
                    tmp:total           = total"
                    tmp:totalcourier    += JOB:Invoice_Courier_Cost
                    tmp:totalparts      += job:invoice_parts_cost
                    tmp:totallabour     += job:invoice_labour_cost
                    tmp:totalexvat      += job:invoice_sub_total
                    tmp:totalinvat      += tmp:total
                    tmp:totalvat        += tmp:total - job:invoice_sub_total
                    tmp:count           += 1
                    Print(rpt:detail)
                End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
            End!If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
        End!Loop x# = 1 To Records(glo:Queue3)
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(INVOICE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        Wmf2AsciiName = 'C:\REPORT.TXT'
        Wmf2AsciiName = '~' & Wmf2AsciiName
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,Wmf2AsciiName,AsciiLineOption,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  Get(glo:Queue3,1)
  access:invoice.clearkey(inv:invoice_number_key)
  inv:invoice_number  = glo:pointer3
  If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
      access:jobs.clearkey(job:ref_number_key)
      job:invoice_number  = glo:pointer3
      If access:jobs.tryfetch(job:InvoiceNumberKey) = Level:Benign
        TMP:Account_No = job:Account_Number
      End
  End
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistsim.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='Single_Invoice_Summary'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Single_Invoice_Summary',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Single_Invoice_Summary',1)
    SolaceViewVars('TMP:Account_No',TMP:Account_No,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:totalcourier',tmp:totalcourier,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:totalparts',tmp:totalparts,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:totallabour',tmp:totallabour,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:totalexvat',tmp:totalexvat,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:totalinvat',tmp:totalinvat,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:totalvat',tmp:totalvat,'Single_Invoice_Summary',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Single_Invoice_Summary',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Single_Invoice_Summary',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Single_Invoice_Summary',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Single_Invoice_Summary',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Single_Invoice_Summary',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Single_Invoice_Summary',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Single_Invoice_Summary',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Single_Invoice_Summary',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Single_Invoice_Summary',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Single_Invoice_Summary',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Single_Invoice_Summary',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Single_Invoice_Summary',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Single_Invoice_Summary',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Single_Invoice_Summary',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Single_Invoice_Summary',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Single_Invoice_Summary',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Single_Invoice_Summary',1)
    SolaceViewVars('InitialPath',InitialPath,'Single_Invoice_Summary',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Single_Invoice_Summary',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:telephonenumber',tmp:telephonenumber,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:faxnumber',tmp:faxnumber,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:total',tmp:total,'Single_Invoice_Summary',1)
    SolaceViewVars('tmp:count',tmp:count,'Single_Invoice_Summary',1)


BuildCtrlQueue      Routine







