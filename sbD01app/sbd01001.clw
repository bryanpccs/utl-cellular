

   MEMBER('sbd01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD01001.INC'),ONCE        !Local module procedure declarations
                     END


Multiple_Invoice PROCEDURE (f_batch_number,f_account_number) !Generated from procedure template - Report

Default_Invoice_Company_Name_Temp STRING(30)
tmp:DefaultTelephone STRING(20)
tmp:DefaultFax       STRING(20)
Default_Invoice_Address_Line1_Temp STRING(30)
Default_Invoice_Address_Line2_Temp STRING(30)
Default_Invoice_Address_Line3_Temp STRING(30)
Default_Invoice_Postcode_Temp STRING(15)
Default_Invoice_Telephone_Number_Temp STRING(15)
Default_Invoice_Fax_Number_Temp STRING(15)
Default_Invoice_VAT_Number_Temp STRING(30)
RejectRecord         LONG,AUTO
tmp:printer          STRING(255)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
Invoice_Number_Temp  STRING(28)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Address_Line4_Temp   STRING(30)
Invoice_Name_Temp    STRING(30)
Labour_vat_info_temp STRING(15)
Parts_vat_info_temp  STRING(15)
batch_number_temp    STRING(28)
labour_vat_temp      REAL
parts_vat_temp       REAL
balance_temp         REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS)
                       PROJECT(job:ESN)
                       PROJECT(job:Invoice_Number_Warranty)
                       PROJECT(job:MSN)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Unit_Type)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(43,42,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

report               REPORT,AT(396,4604,7521,4240),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(396,521,7521,4042),USE(?unnamed)
                         STRING(@s30),AT(156,83,3844,240),USE(def:Invoice_Company_Name),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,365,3844,156),USE(def:Invoice_Address_Line1),TRN
                         STRING(@s30),AT(156,521,3844,156),USE(def:Invoice_Address_Line2),TRN
                         STRING(@s30),AT(156,677,3844,156),USE(def:Invoice_Address_Line3),TRN
                         STRING(@s15),AT(156,833,1156,156),USE(def:Invoice_Postcode),TRN
                         STRING('Tel:'),AT(156,1042),USE(?String16),TRN
                         STRING(@s15),AT(625,1042),USE(def:Invoice_Telephone_Number),TRN
                         STRING('Fax: '),AT(156,1198),USE(?String19),TRN
                         STRING(@s15),AT(625,1198),USE(def:Invoice_Fax_Number),TRN
                         STRING(@s255),AT(625,1354,3844,208),USE(def:InvoiceEmailAddress),TRN
                         STRING('Email:'),AT(156,1354),USE(?String19:2),TRN
                         STRING('INVOICE'),AT(5521,0),USE(?String20),TRN,FONT(,20,,FONT:bold)
                         STRING(@s28),AT(4917,917),USE(batch_number_temp),TRN,CENTER,FONT(,,,FONT:bold)
                         STRING('Page Number: '),AT(4083,2604),USE(?PagePrompt),TRN,FONT(,8,,)
                         STRING(@N4),AT(5521,2604),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s28),AT(4917,521),USE(Invoice_Number_Temp),TRN,CENTER,FONT(,,,FONT:bold)
                         STRING(@s30),AT(156,1677),USE(Invoice_Name_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING('V.A.T. Number: '),AT(4083,1677),USE(?String29),TRN,FONT(,8,,)
                         STRING(@s30),AT(5521,1677),USE(Default_Invoice_VAT_Number_Temp),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,1844,1917,156),USE(Address_Line1_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2000),USE(Address_Line2_Temp),TRN,FONT(,8,,)
                         STRING(@s30),AT(156,2156),USE(Address_Line3_Temp),TRN,FONT(,8,,)
                         STRING('Date Of Invoice:'),AT(4083,1917),USE(?String31),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5521,1917),USE(inv:Date_Created),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2323),USE(Address_Line4_Temp),TRN,FONT(,8,,)
                         STRING('Printed:'),AT(4083,2396),USE(?String46),TRN,FONT(,8,,)
                         STRING(@d6b),AT(5521,2396),USE(ReportRunDate),FONT(,8,,FONT:bold)
                         STRING(@t1b),AT(6156,2396),USE(ReportRunTime),FONT(,8,,FONT:bold)
                         STRING('Account Number: '),AT(4083,2156),USE(?String32),TRN,FONT(,8,,)
                         STRING(@s15),AT(5521,2156,1000,240),USE(sub:Account_Number),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Job Number'),AT(323,3844),USE(?String33),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(1240,3844),USE(?String41),TRN,FONT(,8,,FONT:bold)
                         STRING('Unit Type'),AT(3000,3844),USE(?String44),TRN,FONT(,8,,FONT:bold)
                         STRING('I.M.E.I. Number'),AT(4396,3844),USE(?String42),TRN,FONT(,8,,FONT:bold)
                         STRING('M.S.N.'),AT(5521,3844),USE(?String43),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(156,2521,1323,156),USE(inv:Invoice_VAT_Number),TRN,FONT(,8,,)
                       END
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,,156),USE(?DetailBand)
                           STRING(@n12),AT(240,0),USE(job:Ref_Number),TRN,RIGHT,FONT(,8,,)
                           STRING(@s25),AT(1240,0),USE(job:Model_Number),TRN,LEFT,FONT(,8,,)
                           STRING(@s20),AT(3000,0),USE(job:Unit_Type),TRN,LEFT,FONT(,8,,)
                           STRING(@s15),AT(4396,0),USE(job:ESN),TRN,LEFT,FONT(,8,,)
                           STRING(@s10),AT(5521,0),USE(job:MSN),TRN,LEFT,FONT(,8,,)
                         END
                         FOOTER,AT(396,8802,,2281),USE(?unnamed:2),ABSOLUTE
                           STRING('Parts Sub Total'),AT(4802,719),USE(?String37),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6323,719),USE(inv:Parts_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6323,875),USE(parts_vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING(@n14.2),AT(6323,563),USE(labour_vat_temp),TRN,RIGHT,FONT(,8,,)
                           STRING('Labour Sub Total'),AT(4802,396),USE(?String39),TRN,FONT(,8,,)
                           STRING(@n14.2),AT(6323,396),USE(inv:Labour_Paid),TRN,RIGHT,FONT(,8,,)
                           STRING(@s15),AT(4802,563),USE(Labour_vat_info_temp),TRN,LEFT,FONT(,8,,)
                           STRING('Balance Due'),AT(4802,1146),USE(?String38),TRN,FONT(,10,,FONT:bold)
                           STRING(@n14.2),AT(6083,1146),USE(balance_temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           STRING('<128>'),AT(6021,1146),USE(?Euro),TRN,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                           STRING(@s15),AT(4802,875),USE(Parts_vat_info_temp),TRN,FONT(,8,,)
                         END
                       END
                       FOOTER,AT(396,10323,7521,1156)
                         TEXT,AT(83,83,7365,958),USE(stt:Text),FONT(,8,,)
                       END
                       FORM,AT(396,479,7521,11198)
                         IMAGE,AT(0,0,7521,11156),USE(?Image1)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
AskPreview             PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
TempNameFunc_Flag    SHORT(0)                         !---ClarioNET 22
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Progress:UserString{prop:FontColor} = -1
    ?Progress:UserString{prop:Color} = 15066597
    ?Progress:PctText{prop:FontColor} = -1
    ?Progress:PctText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Multiple_Invoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Default_Invoice_Company_Name_Temp',Default_Invoice_Company_Name_Temp,'Multiple_Invoice',1)
    SolaceViewVars('tmp:DefaultTelephone',tmp:DefaultTelephone,'Multiple_Invoice',1)
    SolaceViewVars('tmp:DefaultFax',tmp:DefaultFax,'Multiple_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line1_Temp',Default_Invoice_Address_Line1_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line2_Temp',Default_Invoice_Address_Line2_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Default_Invoice_Address_Line3_Temp',Default_Invoice_Address_Line3_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Default_Invoice_Postcode_Temp',Default_Invoice_Postcode_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Default_Invoice_Telephone_Number_Temp',Default_Invoice_Telephone_Number_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Default_Invoice_Fax_Number_Temp',Default_Invoice_Fax_Number_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Default_Invoice_VAT_Number_Temp',Default_Invoice_VAT_Number_Temp,'Multiple_Invoice',1)
    SolaceViewVars('RejectRecord',RejectRecord,'Multiple_Invoice',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Multiple_Invoice',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Multiple_Invoice',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Multiple_Invoice',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Multiple_Invoice',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Multiple_Invoice',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Multiple_Invoice',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Multiple_Invoice',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Multiple_Invoice',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Multiple_Invoice',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Multiple_Invoice',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Multiple_Invoice',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Multiple_Invoice',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Multiple_Invoice',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Multiple_Invoice',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Multiple_Invoice',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Multiple_Invoice',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Multiple_Invoice',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Multiple_Invoice',1)
    SolaceViewVars('InitialPath',InitialPath,'Multiple_Invoice',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Multiple_Invoice',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Multiple_Invoice',1)
    SolaceViewVars('Invoice_Number_Temp',Invoice_Number_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Address_Line4_Temp',Address_Line4_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Invoice_Name_Temp',Invoice_Name_Temp,'Multiple_Invoice',1)
    SolaceViewVars('Labour_vat_info_temp',Labour_vat_info_temp,'Multiple_Invoice',1)
    SolaceViewVars('Parts_vat_info_temp',Parts_vat_info_temp,'Multiple_Invoice',1)
    SolaceViewVars('batch_number_temp',batch_number_temp,'Multiple_Invoice',1)
    SolaceViewVars('labour_vat_temp',labour_vat_temp,'Multiple_Invoice',1)
    SolaceViewVars('parts_vat_temp',parts_vat_temp,'Multiple_Invoice',1)
    SolaceViewVars('balance_temp',balance_temp,'Multiple_Invoice',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 34
  PARENT.Ask


ThisWindow.AskPreview PROCEDURE

  CODE
  IF ClarioNETServer:Active()                         !---ClarioNET 46
    IF ~SELF.Preview &= NULL AND SELF.Response = RequestCompleted
      ENDPAGE(SELF.Report)
      LOOP TempNameFunc_Flag = 1 TO RECORDS(SELF.PreviewQueue)
        GET(SELF.PreviewQueue, TempNameFunc_Flag)
        ClarioNET:AddMetafileName(SELF.PreviewQueue.Filename)
      END
      ClarioNET:SetReportProperties(report)
      FREE(SELF.PreviewQueue)
      TempNameFunc_Flag = 0
    END
  ELSE
    PARENT.AskPreview
  END                                                 !---ClarioNET 48


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Multiple_Invoice')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Multiple_Invoice')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  BIND('GLO:Select1',GLO:Select1)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:INVOICE.Open
  Relate:STANTEXT.Open
  Relate:VATCODE.Open
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Invoice_Number_Warranty)
  ThisReport.AddSortOrder(job:WarInvoiceNoKey)
  ThisReport.AddRange(job:Invoice_Number_Warranty,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  Do RecolourWindow


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Multiple_Invoice',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'INVOICE'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  set(defaults)
  access:defaults.next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  
  !Alternative Invoice Address
  If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  Else!If def:use_invoice_address = 'YES'
      Default_Invoice_Company_Name_Temp       = DEF:User_Name
      Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
      Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
      Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
      Default_Invoice_Postcode_Temp           = DEF:Postcode
      Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
      Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
      Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  End!If def:use_invoice_address = 'YES'
  
  
  reportrundate = Today()
  reportruntime = Clock()
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = f_account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key) 
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          if tra:invoice_sub_accounts = 'YES'
              invoice_name_temp   = sub:company_name
              address_line1_temp  = sub:address_line1
              address_line2_temp  = sub:address_line2
              If sup:address_line3 = ''
                  address_line3_temp = sub:postcode
                  address_line4_temp = ''
              Else
                  address_line3_temp  = sub:address_line3
                  address_line4_temp  = sub:postcode
              End
          else!if tra:use_sub_accounts = 'YES'
              invoice_name_temp   = tra:company_name
              address_line1_temp  = tra:address_line1
              address_line2_temp  = tra:address_line2
              If sup:address_line3 = ''
                  address_line3_temp = tra:postcode
                  address_line4_temp = ''
              Else
                  address_line3_temp  = tra:address_line3
                  address_line4_temp  = tra:postcode
              End
          end!if tra:use_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
  
  
  invoice_number_temp = 'Invoice Number: ' & Format(glo:select1,@p<<<<<<<#p)
  batch_number_temp   = 'Batch Number  : ' & Format(f_batch_number,@p<<<<<<<#p)
  
  access:invoice.clearkey(inv:invoice_type_key)
  inv:invoice_type   = 'WAR'
  inv:invoice_number = glo:select1
  If access:invoice.fetch(inv:invoice_type_key) = Level:Benign
      labour_vat_info_temp = 'V.A.T. @ ' & Format(INV:Vat_Rate_labour,@n6.2)
      labour_vat_temp      = inv:labour_paid * (INV:Vat_Rate_labour/100)
      parts_vat_info_temp = 'V.A.T. @ ' & Format(INV:Vat_Rate_Parts,@n6.2)
      parts_vat_temp      = inv:parts_paid * (INV:Vat_Rate_Parts/100)
      balance_temp            = inv:labour_paid + labour_vat_temp + inv:parts_paid + parts_vat_temp
  End!If access:invoice.fetch(inv:invoice_type_key) = Level:Benign
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  report{Prop:Text} = 'Invoice'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  set(DEFAULT2)
  Access:DEFAULT2.Next()
  Settarget(Report)
  If de2:CurrencySymbol <> 'YES'
      ?Euro{prop:Hide} = 1
  Else !de2:CurrentSymbol = 'YES'
      ?Euro{prop:Hide} = 0
      ?Euro{prop:Text} = '�'
  End !de2:CurrentSymbol = 'YES'
  Settarget()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF ClarioNETServer:Active()
    ClarioNET:EndReport                               !---ClarioNET 101
  END                                                 !---ClarioNET 102
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Multiple_Invoice')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:OpenWindow
      ClarioNET:StartReport                           !---ClarioNET 91
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
    IF ClarioNETServer:Active()                       !---ClarioNET 44
      IF TempNameFunc_Flag = 0
        TempNameFunc_Flag = 1
        report{PROP:TempNameFunc} = ADDRESS(ClarioNET:GetPrintFileName)
      END
    END
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue

