

   MEMBER('sbcr0084.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBCR0001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBCR0002.INC'),ONCE        !Req'd for module callout resolution
                     END


TeamPerformanceReport PROCEDURE                       !Generated from procedure template - Window

Pathname             STRING(255)
PrintReport          BYTE
ExportCsv            BYTE
DateRange            STRING(30)
tmp:printer          STRING(255)
Field_1              STRING(30)
Field_2              STRING(30)
Field_3              STRING(10)
Field_4              STRING(30)
Field_5              STRING(61)
Field_6              STRING(30)
ReportType           STRING(1)
window               WINDOW('Team/Engineer Performance Report'),AT(,,263,154),FONT('Tahoma',8,,),CENTER,ICON('PC.ICO'),SYSTEM,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,256,120),USE(?Sheet1),WIZARD,SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('Start Completion Date'),AT(8,16),USE(?Prompt3)
                           ENTRY(@d17),AT(88,16,72,10),USE(TheStartDate)
                           BUTTON('...'),AT(164,16,10,10),USE(?PopCalendar),LEFT,ICON('CALENDA2.ICO')
                           PROMPT('End Completion Date'),AT(8,32),USE(?Prompt4)
                           ENTRY(@d17),AT(88,32,72,10),USE(TheEndDate)
                           BUTTON('...'),AT(164,32,10,10),USE(?PopCalendar:2),LEFT,ICON('CALENDA2.ICO')
                           PROMPT('Export Filename'),AT(8,48),USE(?Prompt2)
                           ENTRY(@s255),AT(88,48,148,10),USE(Pathname)
                           BUTTON('...'),AT(240,48,10,10),USE(?FileButton),LEFT,ICON('list3.ico')
                           CHECK('Print Report'),AT(168,84),USE(PrintReport),LEFT,VALUE('1','0')
                           OPTION('Report Type'),AT(12,68,124,48),USE(ReportType),BOXED
                             RADIO('Job Completed Date'),AT(28,84),USE(?ReportType:Radio1),VALUE('1')
                             RADIO('RepairCompleted Date'),AT(28,96),USE(?ReportType:Radio2),VALUE('2')
                           END
                           CHECK('Export CSV'),AT(168,96),USE(ExportCsv),LEFT,VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,128,256,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Export'),AT(128,132,60,16),USE(?ExportButton),LEFT,ICON('ok.ico')
                       BUTTON('Cancel'),AT(192,132,60,16),USE(?CancelButton),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! ProgressWindow Declarations

RecordsToProcess        LONG,AUTO
RecordsProcessed        LONG,AUTO
RecordsPerCycle         LONG,AUTO
RecordsThisCycle        LONG,AUTO
PercentProgress         LONG,AUTO
ProgressThermometer     LONG,AUTO
AppName                 STRING(30)
ProgressWindow          WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
                            PROGRESS,USE(ProgressThermometer),AT(25,15,111,12),RANGE(0,100)
                            STRING(''),AT(0,3,161,10),USE(?ProgressUserString),CENTER,FONT('Arial',8,,)
                            STRING(''),AT(0,30,161,10),USE(?ProgressText),TRN,CENTER,FONT('Arial',8,,)
                            BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
                        END

               MAP
ProgressBarShow         PROCEDURE(STRING arg:AppNameString, LONG arg:RecordCount=1000,  |
                                  LONG arg:CycleCount=25,   <STRING arg:UserText>)
ProgressBarReset        PROCEDURE(LONG arg:RecordCount=1000, <LONG arg:CycleCount>, <STRING arg:UserText>)
ProgressBarUpdate       PROCEDURE(),LONG
ProgressBarCancelled    PROCEDURE(),LONG
ProgressBarClose        PROCEDURE()

              END
LocalVariables      GROUP,PRE(LOC)
ApplicationName         STRING('ServiceBase 2000')
ProgramName             STRING('Charge Structure Export')
UserName                STRING(100)
UserCode                STRING(3)
                    END

IniFilepath          STRING(255)

EXFILE              FILE,DRIVER('BASIC'),PRE(expfile),CREATE,BINDABLE,THREAD
EXRECORD                RECORD,PRE(exprec)
Team                    STRING(40)
Engineer                STRING(30)
JobNumber               STRING(30)
Model                   STRING(30)
ChargeChargeType        STRING(30)
WarrantyChargeType      STRING(30)
TradeAccount            STRING(30)
                        END
                    END

                     MAP
ExportCSV            PROCEDURE(),LONG
BuildQ               PROCEDURE(),LONG
BuildQ2              PROCEDURE(),LONG
GetTradeAccount      PROCEDURE(STRING arg:AccountNumber),STRING
GetTeam              PROCEDURE(STRING arg:EngineerCode),STRING
GetEngineer          PROCEDURE(STRING arg:EngineerCode),STRING
GetDailyAverage      PROCEDURE(LONG arg:Default=2000),LONG
IsStatusSet          PROCEDURE(LONG arg:JobNumber, STRING arg:StatusString),LONG
GetStatusString      PROCEDURE(LONG arg:statusCode),STRING
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?TheStartDate{prop:ReadOnly} = True
        ?TheStartDate{prop:FontColor} = 65793
        ?TheStartDate{prop:Color} = 15066597
    Elsif ?TheStartDate{prop:Req} = True
        ?TheStartDate{prop:FontColor} = 65793
        ?TheStartDate{prop:Color} = 8454143
    Else ! If ?TheStartDate{prop:Req} = True
        ?TheStartDate{prop:FontColor} = 65793
        ?TheStartDate{prop:Color} = 16777215
    End ! If ?TheStartDate{prop:Req} = True
    ?TheStartDate{prop:Trn} = 0
    ?TheStartDate{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?TheEndDate{prop:ReadOnly} = True
        ?TheEndDate{prop:FontColor} = 65793
        ?TheEndDate{prop:Color} = 15066597
    Elsif ?TheEndDate{prop:Req} = True
        ?TheEndDate{prop:FontColor} = 65793
        ?TheEndDate{prop:Color} = 8454143
    Else ! If ?TheEndDate{prop:Req} = True
        ?TheEndDate{prop:FontColor} = 65793
        ?TheEndDate{prop:Color} = 16777215
    End ! If ?TheEndDate{prop:Req} = True
    ?TheEndDate{prop:Trn} = 0
    ?TheEndDate{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?Pathname{prop:ReadOnly} = True
        ?Pathname{prop:FontColor} = 65793
        ?Pathname{prop:Color} = 15066597
    Elsif ?Pathname{prop:Req} = True
        ?Pathname{prop:FontColor} = 65793
        ?Pathname{prop:Color} = 8454143
    Else ! If ?Pathname{prop:Req} = True
        ?Pathname{prop:FontColor} = 65793
        ?Pathname{prop:Color} = 16777215
    End ! If ?Pathname{prop:Req} = True
    ?Pathname{prop:Trn} = 0
    ?Pathname{prop:FontStyle} = font:Bold
    ?PrintReport{prop:Font,3} = -1
    ?PrintReport{prop:Color} = 15066597
    ?PrintReport{prop:Trn} = 0
    ?ReportType{prop:Font,3} = -1
    ?ReportType{prop:Color} = 15066597
    ?ReportType{prop:Trn} = 0
    ?ReportType:Radio1{prop:Font,3} = -1
    ?ReportType:Radio1{prop:Color} = 15066597
    ?ReportType:Radio1{prop:Trn} = 0
    ?ReportType:Radio2{prop:Font,3} = -1
    ?ReportType:Radio2{prop:Color} = 15066597
    ?ReportType:Radio2{prop:Trn} = 0
    ?ExportCsv{prop:Font,3} = -1
    ?ExportCsv{prop:Color} = 15066597
    ?ExportCsv{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
GetUserName         ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('%', CommandLine)
        IF (NOT tmpPos) THEN
            MessageEx('Attempting to use ' & LOC:ProgramName & '<10,13>'          & |
                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
                LOC:ApplicationName,                                                           |
                'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                beep:systemhand,msgex:samewidths,84,26,0)
           HALT
        END

        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        IF (Access:USERS.Tryfetch(use:Password_Key) <> Level:Benign) THEN
            MessageEx('Unable to find your logged in user details.', |
                    LOC:ApplicationName,                                  |
                    'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
            HALT
        END

        LOC:UserCode = use:User_Code

        LOC:UserName = use:Forename
        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TeamPerformanceReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDSTATS.Open
  Relate:STATUS.Open
  Access:SUBTRACC.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:TEAMS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  DO GetUserName
  
  IniFilepath =  CLIP(PATH()) & '\SBCR0084.INI'
  Pathname = GETINI('DEFAULTS','Pathname',,CLIP(IniFilepath))
  
  TheStartDate   = TODAY() -1
  TheEndDate = TheStartDate
  
  ! Start Change xxx BE(01/09/2004)
  ! Repair Complete Date
  ReportType = 2
  ! End Change xxx BE(01/09/2004)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?TheStartDate{Prop:Alrt,255} = MouseLeft2
  ?TheEndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDSTATS.Close
    Relate:STATUS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          TheStartDate = TINCALENDARStyle1(TheStartDate)
          Display(?TheStartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          TheEndDate = TINCALENDARStyle1(TheEndDate)
          Display(?TheEndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?FileButton
      ThisWindow.Update
      FILEDIALOG('Choose Export File',pathname,'CSV Files|*.csv', |
                        file:save + file:keepdir + file:noerror + file:longname)
      strlen# = LEN(CLIP(Pathname))
      IF ((strlen# < 4) OR (UPPER(Pathname[strlen#-3 : strlen#]) <> '.CSV')) THEN
          Pathname = CLIP(Pathname) & '.csv'
      END
      DISPLAY(?pathname)
    OF ?ExportButton
      ThisWindow.Update
      IF ((NOT ExportCsv) AND (NOT PrintReport)) THEN
          MESSAGE('Please Select Either Export CSV or Print Report')
          CYCLE
      END
      
      IF (ExportCsv) THEN
          IF (Pathname = '') THEN
              MESSAGE('Please Select a Filename')
              SELECT(?Pathname)
              CYCLE
          END
      
          EXFILE{Prop:NAME} = CLIP(Pathname)
      
          IF (EXISTS(CLIP(Pathname))) THEN
              REMOVE(EXFILE)
          END
          CREATE(EXFILE)
      
          OPEN(EXFILE, 42h)
          IF (ERRORCODE()) THEN
              MESSAGE('Error Opening ' & CLIP(Pathname))
              SELECT(?Pathname)
              CYCLE
          END
      END
      
      max# = ((TheEndDate - TheStartDate) + 1) * GetDailyAverage()
      
      ProgressBarShow(LOC:ApplicationName, max#)
      ! Start Change xxx BE(01/09/2004)
      !count# = BuildQ()
      CASE ReportType
          OF 1
              ! Job Complete Date
              count# = BuildQ()
          ELSE
              ! Repair Complete Date
              count# = BuildQ2()
      END
      ! End Change xxx BE(01/09/2004)
      IF ((ExportCsv) AND (count# > 0)) THEN
          ProgressBarReset(RECORDS(ExportQueue),,'Building CSV File...')
          count# = ExportCsv()
          IF (count# > 0) THEN
              PUTINI('DEFAULTS','Pathname',CLIP(Pathname),CLIP(IniFilepath))
          END
      END
      ProgressBarClose()
      IF ((PrintReport) AND (count# > 0)) THEN
          PrintedReport(LOC:Username)
      END
      
      FREE(ExportQueue)
      
      IF (ExportCsv) THEN
          CLOSE(EXFILE)
          IF (count# <= 0) THEN
              REMOVE(EXFILE)
          END
      END
      
      IF (count# = 0) THEN
          MESSAGE('No Records Found')
      ELSE
          MESSAGE('Export Complete (' & CLIP(LEFT(count#)) & ' Records)')
      END
    OF ?CancelButton
      ThisWindow.Update
      POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?TheStartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?TheEndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Progress Bar Window
ProgressBarShow    PROCEDURE(arg:AppNameString, arg:RecordCount, arg:CycleCount, arg:UserText)
    CODE
    AppName              = arg:AppNameString
    RecordsPerCycle      = arg:CycleCount
    RecordsThisCycle     = 0
    RecordsProcessed     = 0
    RecordsToProcess     = arg:RecordCount
    PercentProgress      = 0
    ProgressThermometer  = 0

    !thiswindow.reset(1) !This may error, if so, remove this line.
    open(ProgressWindow)
    !ProgressWindow{PROP:Timer} = 1
         
    IF (OMITTED(4)) THEN
        ?ProgressUserString{PROP:Text} = 'Running...'
    ELSE
        ?ProgressUserString{PROP:Text} = CLIP(arg:UserText)
    END
    ?progressText{prop:text}    = '0% Completed'
    DISPLAY()

    RETURN

ProgressBarReset    PROCEDURE(arg:RecordCount, arg:CycleCount, arg:UserText)
    CODE
    IF (NOT OMITTED(2)) THEN
        RecordsPerCycle      = arg:CycleCount
    END
    RecordsThisCycle     = 0
    RecordsProcessed     = 0
    RecordsToProcess     = arg:RecordCount
    PercentProgress      = 0
    ProgressThermometer  = 0

    IF (OMITTED(3)) THEN
        ?ProgressUserString{PROP:Text} = 'Running...'
    ELSE
        ?ProgressUserString{PROP:Text} = CLIP(arg:UserText)
    END
    ?progressText{prop:text}    = '0% Completed'
    DISPLAY()

    RETURN

ProgressBarUpdate     PROCEDURE()
result  LONG
    CODE
    result = -1
    RecordsProcessed += 1
    RecordsThisCycle += 1
    IF (RecordsThisCycle > RecordsPerCycle) THEN
        RecordsThisCycle = 0
        IF (ProgressBarCancelled()) THEN
            !CLOSE(ProgressWindow)
            result = 0
        ELSE
            IF (PercentProgress < 100) THEN
                PercentProgress = ROUND((RecordsProcessed / RecordsToProcess) * 100.0, 1)
                IF (PercentProgress > 100) THEN
                    PercentProgress = 100
                END
                IF (PercentProgress <> ProgressThermometer) THEN
                    ProgressThermometer = PercentProgress
                    ?ProgressText{prop:text} = format(PercentProgress,@n3) & '% Completed'
                    DISPLAY()
                END
            END
        END
    END
    RETURN result

ProgressBarCancelled       PROCEDURE()
result  LONG
    CODE
    result = 0
    cancel# = 0

    ACCEPT
        CASE EVENT()
        OF Event:Timer
            BREAK
        OF Event:CloseWindow
            cancel# = 1
            BREAK
        OF Event:accepted
            IF (FIELD() = ?ProgressCancel) THEN
                cancel# = 1
                BREAK
            END
        END
    END

    IF (cancel# = 1) THEN
        BEEP(BEEP:SystemAsterisk)
        CASE MessageEx('Are you sure you want to cancel?', CLIP(AppName),|
                       'Styles\question.ico','&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                       beep:systemquestion,msgex:samewidths,84,26,0)
            OF 1 ! &Yes Button
                result = -1
            OF 2 ! &No Button
        END
    END
    RETURN result

ProgressBarClose       PROCEDURE()
    CODE
    CLOSE(ProgressWindow)
    RETURN
ExportCSV      PROCEDURE()
ResultValue      LONG
    CODE
    ResultValue = 0

    CLEAR(EXFILE)
    exprec:Team = 'Team/Engineer Performance Report'
    ADD(EXFILE)

    CLEAR(EXFILE)
    exprec:Team = 'Date Range: ' & FORMAT(TheStartDate, @D05) & ' - ' & FORMAT(TheEndDate, @D05)
    ADD(EXFILE)

    CLEAR(EXFILE)
    ADD(EXFILE)

    CLEAR(EXFILE)
    exprec:Team               =    'Team'
    exprec:Engineer           =    'Engineer'
    exprec:JobNumber          =    'Job Number'
    exprec:Model              =    'Model'
    exprec:ChargeChargeType   =    'Chargeable Charge Type'
    exprec:WarrantyChargeType =    'Warranty Charge Type'
    exprec:TradeAccount       =    'Trade Account'
    ADD(EXFILE)

    LOOP ix# = 1 TO RECORDS(ExportQueue)
        GET(ExportQueue, ix#)
        IF (NOT ProgressBarUpdate()) THEN
            ResultValue = -1
            BREAK
        END
        access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number = EXQ:JobNumber
        IF (access:JOBS.fetch(job:Ref_Number_Key) = Level:Benign) THEN
            CLEAR(EXFILE)
            exprec:Team               =    EXQ:Team
            exprec:Engineer           =    EXQ:Engineer
            exprec:JobNumber          =    FORMAT(EXQ:JobNumber, @s8)
            exprec:Model              =    job:Model_Number
            IF (job:Chargeable_Job = 'YES') THEN
                exprec:ChargeChargeType    = job:Charge_Type
            ELSE
                exprec:ChargeChargeType    = ''
            END
            IF (job:Warranty_Job = 'YES') THEN
                exprec:WarrantyChargeType  = job:Warranty_Charge_Type
            ELSE
                exprec:WarrantyChargeType  = ''
            END
            exprec:TradeAccount       =    GetTradeAccount(job:Account_Number)
            ADD(EXFILE)
            ResultValue += 1
        END
    END
    RETURN ResultValue
BuildQ      PROCEDURE()
ResultValue         LONG
ThirdPartyDespatch  STRING(30)
    CODE
    ResultValue = 0
    FREE(ExportQueue)
    ThirdPartyDespatch = GetStatusString(410)

    access:JOBS.CLEARKEY(job:DateCompletedKey)
    job:Date_Completed = TheStartDate
    SET(job:DateCompletedKey, job:DateCompletedKey)
    LOOP
        IF ((access:JOBS.next() <> Level:Benign) OR |
            (job:Date_Completed > TheEndDate)) THEN
            BREAK
        END

!    access:JOBSE.CLEARKEY(jobe:CompleteRepairKey)
!    jobe:CompleteRepairDate = TheStartDate
!    SET(jobe:CompleteRepairKey, jobe:CompleteRepairKey)
!    LOOP
!        IF ((access:JOBSE.next() <> Level:Benign) OR |
!            (jobe:CompleteRepairDate > TheEndDate)) THEN
!            BREAK
!        END
!
!        ! Get Master Record
!        access:JOBS.CLEARKEY(job:Ref_Number_Key)
!        job:Ref_Number = jobe:RefNumber
!        IF (access:JOBS.fetch(job:Ref_Number_Key) <> Level:Benign) THEN
!            CYCLE
!        END

        IF (NOT ProgressBarUpdate()) THEN
            ResultValue = -1
            BREAK
        END

        IF (IsStatusSet(job:Ref_Number, ThirdPartyDespatch)) THEN
            CYCLE
        END

        CLEAR(ExportQueue)
        EXQ:Engineer            = GetEngineer(job:Engineer)
        EXQ:Team                = GetTeam(job:Engineer)
        EXQ:JobNumber           = job:Ref_Number
        ADD(ExportQueue, +EXQ:Team, +EXQ:Engineer, +EXQ:JobNumber)
        ResultValue += 1
    END
    RETURN ResultValue
BuildQ2      PROCEDURE()
ResultValue         LONG
ThirdPartyDespatch  STRING(30)
    CODE
    ResultValue = 0
    FREE(ExportQueue)
    ThirdPartyDespatch = GetStatusString(410)
!    access:JOBS.CLEARKEY(job:DateCompletedKey)
!    job:Date_Completed = TheStartDate
!    SET(job:DateCompletedKey, job:DateCompletedKey)
!    LOOP
!        IF ((access:JOBS.next() <> Level:Benign) OR |
!            (job:Date_Completed > TheEndDate)) THEN
!            BREAK
!        END
    access:JOBSE.CLEARKEY(jobe:CompleteRepairKey)
    jobe:CompleteRepairDate = TheStartDate
    SET(jobe:CompleteRepairKey, jobe:CompleteRepairKey)
    LOOP
        IF ((access:JOBSE.next() <> Level:Benign) OR |
            (jobe:CompleteRepairDate > TheEndDate)) THEN
            BREAK
        END

        ! Get Master Record
        access:JOBS.CLEARKEY(job:Ref_Number_Key)
        job:Ref_Number = jobe:RefNumber
        IF (access:JOBS.fetch(job:Ref_Number_Key) <> Level:Benign) THEN
            CYCLE
        END

        IF (NOT ProgressBarUpdate()) THEN
            ResultValue = -1
            BREAK
        END

        IF (IsStatusSet(job:Ref_Number, ThirdPartyDespatch)) THEN
            CYCLE
        END

        CLEAR(ExportQueue)
        EXQ:Engineer            = GetEngineer(job:Engineer)
        EXQ:Team                = GetTeam(job:Engineer)
        EXQ:JobNumber           = job:Ref_Number
        ADD(ExportQueue, +EXQ:Team, +EXQ:Engineer, +EXQ:JobNumber)
        ResultValue += 1
    END
    RETURN ResultValue
GetTradeAccount     PROCEDURE(arg:AccountNumber)
ResultValue     STRING(30)
        CODE
        ResultValue = ''
        access:subtracc.clearkey(sub:Account_Number_Key)
        sub:account_number = arg:AccountNumber
        IF (access:SUBTRACC.fetch(sub:Account_Number_Key) = Level:Benign) THEN
            access:TRADEACC.clearkey(tra:Account_Number_Key)
            tra:account_number = sub:main_account_number
            IF (access:TRADEACC.fetch(tra:Account_Number_Key) = Level:Benign) THEN
                ResultValue = tra:Company_Name
            END
        END
        RETURN ResultValue
GetTeam             PROCEDURE(arg:EngineerCode)
ResultValue     STRING(30)
    CODE
    ResultValue = ''
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code = arg:EngineerCode
    IF (Access:USERS.fetch(use:User_Code_Key) = Level:Benign) THEN
            ResultValue = use:Team
    END
    RETURN ResultValue

GetEngineer         PROCEDURE(arg:EngineerCode)
ResultValue STRING(30)
    CODE
    ResultValue = ''
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code = arg:EngineerCode
    IF (Access:USERS.fetch(use:User_Code_Key) = Level:Benign) THEN
        ResultValue = CLIP(use:Forename) & ' ' &  CLIP(use:Surname)
        IF (ResultValue = '') THEN
            ResultValue = use:user_Code
        END
    END
    RETURN ResultValue
GetDailyAverage     PROCEDURE(arg:Default)
ResultValue     LONG
        CODE
        ResultValue = 0
        access:JOBS.CLEARKEY(job:Ref_Number_Key)
        SET(job:Ref_Number_Key)
        IF (access:JOBS.next() = Level:benign) THEN
            day# = job:Date_Booked
            access:JOBS.CLEARKEY(job:Ref_Number_Key,,1)
            SET(job:Ref_Number_Key)
            IF (access:JOBS.previous() = Level:benign) THEN
                ResultValue = ROUND((RECORDS(JOBS) / (job:Date_Booked - day#)) * 1.4 , 1)
            END
        END
        IF (ResultValue < arg:Default) THEN
            ResultValue = arg:Default
        END
        Return ResultValue
IsStatusSet    PROCEDURE(arg:JobNumber, arg:StatusString)
ResultValue LONG
        CODE
        ResultValue = 0
        access:AUDSTATS.Clearkey(aus:NewStatusKey)
        aus:RefNumber = arg:JobNumber
        aus:type = 'JOB'
        aus:NewStatus = arg:StatusString  
        IF (access:AUDSTATS.fetch(aus:NewStatusKey) = Level:Benign) THEN
            ResultValue = 1
        END
        RETURN ResultValue
GetStatusString      PROCEDURE(arg:statusCode)
ResultValue     STRING(30)
        CODE
        ResultValue = ''
        access:STATUS.Clearkey(sts:Ref_Number_Only_Key)
        sts:Ref_Number = arg:StatusCode
        IF (access:STATUS.fetch(sts:Ref_Number_Only_Key) = Level:Benign) THEN
            ResultValue = sts:Status
        END
        RETURN ResultValue
