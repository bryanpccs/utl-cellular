

   MEMBER('sbcr0084.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBCR0002.INC'),ONCE        !Local module procedure declarations
                     END








PrintedReport PROCEDURE(arg:Username)
    MAP
CountJobs                   PROCEDURE(STRING arg:team, STRING arg:engineer),LONG
GetTradeAccount             PROCEDURE(STRING arg:AccountNumber),STRING
ProgressBarCancelled        PROCEDURE(),LONG
    END
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
DateRange            STRING(30)
Field_1              STRING(30)
Field_2              STRING(30)
Field_3              STRING(10)
Field_4              STRING(20)
Field_5              STRING(61)
Field_6              STRING(30)
CurrentTeam          STRING(30)
CurrentEngineer      STRING(30)
PrintedBy            STRING(60)
ChargeTypeString     STRING(61)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
Report               REPORT,AT(396,2864,7521,8500),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,2385),USE(?unnamed:3)
                         STRING(@s30),AT(94,52),USE(def:User_Name),TRN,FONT(,14,,FONT:bold)
                         STRING('Date Range:'),AT(4896,469),USE(?String6),TRN,FONT(,8,,)
                         STRING(@s30),AT(94,469,3531,),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(94,313,3531,),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(5938,469,1510,208),USE(DateRange),TRN,FONT(,8,,FONT:bold)
                         STRING(@s15),AT(94,781,3531,),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Email:'),AT(104,1406),USE(?String23),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(104,1094),USE(?String21),TRN,FONT(,9,,)
                         STRING(@s255),AT(573,1406,3531,),USE(def:EmailAddress),TRN,FONT(,9,,)
                         STRING('Page:'),AT(4896,1510),USE(?String29),TRN,FONT(,8,,)
                         STRING(@N4),AT(5938,1510),PAGENO,USE(?ReportPageNo),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Date Printed:'),AT(4896,1354),USE(?String28),TRN,FONT(,8,,)
                         STRING(@s15),AT(573,1094,3531,),USE(def:Telephone_Number),TRN,FONT(,9,,)
                         STRING(@s60),AT(5938,1198,1563,),USE(PrintedBy),TRN,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(4896,1198),USE(?String27),TRN,FONT(,8,,)
                         STRING('Fax:'),AT(104,1250),USE(?String22),TRN,FONT(,9,,)
                         STRING(@s15),AT(573,1250,3531,),USE(def:Fax_Number),TRN,FONT(,9,,)
                         STRING(@D06),AT(5938,1354),USE(ReportRunDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(94,625,3531,),USE(def:Address_Line3),TRN,FONT(,9,,)
                       END
DETAIL_1               DETAIL,AT(,,,177),USE(?DetailBand)
                         STRING(@s30),AT(156,0,2240,208),USE(Field_1),TRN
                         STRING(@s30),AT(2604,0,2552,),USE(Field_2),TRN
                       END
DETAIL_2               DETAIL,PAGEBEFORE(-1),AT(,,,177),USE(?unnamed:4)
                         STRING(@s30),AT(156,0),USE(Field_1,,?Field_1:2),TRN
                         STRING(@s30),AT(2604,0),USE(Field_2,,?Field_2:2),TRN
                       END
DETAIL_3               DETAIL,AT(,,,177),USE(?unnamed)
                         STRING(@s10),AT(156,0),USE(Field_3),TRN
                         STRING(@s20),AT(917,0),USE(Field_4),TRN
                         STRING(@s61),AT(2396,0,3240,240),USE(Field_5),TRN
                         STRING(@s30),AT(5760,0,1677,240),USE(Field_6),TRN
                       END
                       FORM,AT(396,479,7521,10802)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11198),USE(?Image1)
                         STRING('Team/Engineer Performance Report'),AT(3990,52),USE(?String30),TRN,FONT(,14,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('PrintedReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
            ! Initialise
            SET(defaults)
            access:defaults.next()
            PrintedBy = arg:Username
            DateRange = FORMAT(TheStartDate, @D05) & ' - ' & Format(TheEndDate, @D05)
            recs# = RECORDS(ExportQueue)
            RecordsToProcess = recs#  * 3
            cancelled# = 0
        
            ! Team Summary Report
        
            Field_1 = 'Teams:'
            PRINT(RPT:Detail_1)
            Field_1 = ''
            CurrentTeam = ''
            CurrentTotal# = 0
            GrandTotal# = 0
            LOOP ix# = 1 TO recs#
                GET(ExportQueue, ix#)
                DO DisplayProgress
                IF (ProgressBarCancelled()) THEN
                    cancelled# = 1
                    BREAK
                END
                IF (EXQ:Team = CurrentTeam) THEN
                    CurrentTotal# += 1
                ELSE
                    IF (CurrentTotal# > 0) THEN
                        Field_1 = CurrentTeam
                        Field_2 = CLIP(LEFT(CurrentTotal#))
                        PRINT(RPT:Detail_1)
                        GrandTotal# += CurrentTotal#
                    END
                    CurrentTeam = EXQ:Team
                    CurrentTotal# = 1
                END
            END
            IF (CurrentTotal# > 0) THEN
                Field_1 = CurrentTeam
                Field_2 = CLIP(LEFT(CurrentTotal#))
                PRINT(RPT:Detail_1)
                GrandTotal# += CurrentTotal#
            END
            Field_1 = ''
            Field_2 = ''
            PRINT(RPT:Detail_1)
            Field_1 = 'Total Completed Repairs:'
            Field_2 = CLIP(LEFT(GrandTotal#))
            PRINT(RPT:Detail_1)
        
            ! Engineer Summary Report
        
            GET(ExportQueue, 1)
            CurrentTeam = EXQ:Team
            CurrentEngineer = EXQ:Engineer
            CurrentTotal# = 0
            GrandTotal# = 0
        
            LOOP ix# = 1 TO recs#
                IF (cancelled# = 1) THEN
                    BREAK
                END
                GET(ExportQueue, ix#)
                DO DisplayProgress
                IF (ProgressBarCancelled()) THEN
                    cancelled# = 1
                    BREAK
                END
                IF ((EXQ:Team = CurrentTeam) AND (ix# > 1)) THEN
                    IF (EXQ:Engineer = CurrentEngineer) THEN
                        CurrentTotal# += 1
                    ELSE
                        Field_1 = CurrentEngineer
                        Field_2 = CLIP(LEFT(CurrentTotal#))
                        PRINT(RPT:Detail_1)
                        GrandTotal# += CurrentTotal#
                        CurrentEngineer = EXQ:Engineer
                        CurrentTotal# = 1
                    END
                ELSE
                    IF (CurrentTotal# > 0) THEN
                        Field_1 = CurrentEngineer
                        Field_2 = CLIP(LEFT(CurrentTotal#))
                        PRINT(RPT:Detail_1)
                        GrandTotal# += CurrentTotal#
                        Field_1 = ''
                        Field_2 = ''
                        PRINT(RPT:Detail_1)
                        Field_1 = 'Team Completed Repairs:'
                        Field_2 = CLIP(LEFT(GrandTotal#))
                        PRINT(RPT:Detail_1)
                    END
        
                    Field_1 = 'Team:'
                    Field_2 = EXQ:Team
                    PRINT(RPT:Detail_2)
                    Field_1 = ''
                    Field_2 = ''
                    PRINT(RPT:Detail_1)
                    Field_1 = 'Engineers:'
                    Field_2 = ''
                    PRINT(RPT:Detail_1)
                    CurrentTeam = EXQ:Team
                    CurrentEngineer = EXQ:Engineer
                    CurrentTotal# = 1
                    GrandTotal# = 0
                END
            END
            IF (CurrentTotal# > 0) THEN
                Field_1 = CurrentEngineer
                Field_2 = CLIP(LEFT(CurrentTotal#))
                PRINT(RPT:Detail_1)
                GrandTotal# += CurrentTotal#
            END
            Field_1 = ''
            Field_2 = ''
            PRINT(RPT:Detail_1)
            Field_1 = 'Team Completed Repairs:'
            Field_2 = CLIP(LEFT(GrandTotal#))
            PRINT(RPT:Detail_1)
        
            ! Detail Report
        
            CurrentTeam = ''
            CurrentEngineer = ''
        
            LOOP ix# = 1 TO recs#
                IF (cancelled# = 1) THEN
                    BREAK
                END
                GET(ExportQueue, ix#)
                DO DisplayProgress
                IF (ProgressBarCancelled()) THEN
                    cancelled# = 1
                    BREAK
                END
                access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number = EXQ:JobNumber
                IF (access:JOBS.fetch(job:Ref_Number_Key) = Level:Benign) THEN
                    ChargeTypeString = ''
                    IF (job:Chargeable_Job = 'YES') THEN
                        ChargeTypeString = job:Charge_Type
                    END
                    IF (job:Warranty_Job = 'YES') THEN
                        IF (ChargeTypeString <> '') THEN
                            ChargeTypeString = CLIP(ChargeTypeString) & '/' & job:Warranty_Charge_Type
                        ELSE
                            ChargeTypeString = job:Warranty_Charge_Type
                        END
                    END
                    IF ((EXQ:Team = CurrentTeam) AND (EXQ:Engineer = CurrentEngineer) AND (ix# > 1))  THEN
                        Field_3 = EXQ:JobNumber
                        Field_4 = job:Model_Number
                        Field_5 = CLIP(ChargeTypeString)
                        Field_6 = GetTradeAccount(job:Account_Number)
                        PRINT(RPT:Detail_3)
                    ELSE
                        Field_1 = 'Team:'
                        Field_2 = EXQ:Team
                        PRINT(RPT:Detail_2)
                        Field_1 = 'Engineer:'
                        Field_2 = EXQ:Engineer
                        PRINT(RPT:Detail_1)
                        Field_1 = 'Engineer Completed Repairs:'
                        Field_2 = CountJobs(EXQ:Team, EXQ:Engineer)
                        PRINT(RPT:Detail_1)
                        Field_1 = ''
                        Field_2 = ''
                        PRINT(RPT:Detail_1)
                        Field_3 = 'Job Number'
                        Field_4 = 'Model'
                        Field_5 = 'Charge type'
                        Field_6 = 'Account'
                        PRINT(RPT:Detail_3)
                        Field_3 = EXQ:JobNumber
                        Field_4 = job:Model_Number
                        Field_5 = CLIP(ChargeTypeString)
                        Field_6 = GetTradeAccount(job:Account_Number)
                        PRINT(RPT:Detail_3)
                        CurrentTeam = EXQ:Team
                        CurrentEngineer = EXQ:Engineer
                    END
                END
            END
        
            IF (cancelled# = 1) THEN
                LocalResponse = RequestCancelled
                PartialPreviewReq = False
            ELSE
                LocalResponse = RequestCompleted
            END
            BREAK
        
        
        
        
        
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        Report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),Report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
          Report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(Report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
        IF Report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
        END
        Report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(Report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='PrintedReport'
  END
  IF PreviewReq = True OR (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END


CountJobs   PROCEDURE(arg:team, arg:engineer)
ResultValue LONG
SavePos     LONG
        CODE
        ResultValue = 0
        SavePos = POSITION(ExportQueue)
        LOOP p# = 1 TO RECORDS(ExportQueue)
            GET(ExportQueue, p#)
            IF ((EXQ:Team = arg:Team) AND (EXQ:Engineer = arg:Engineer)) THEN
                ResultValue += 1
            END
        END
        GET(ExportQueue, SavePos)
        RETURN ResultValue
GetTradeAccount     PROCEDURE(arg:AccountNumber)
ResultValue     STRING(30)
    CODE
    ResultValue = ''
    access:subtracc.clearkey(sub:Account_Number_Key)
    sub:account_number = arg:AccountNumber
    IF (access:SUBTRACC.fetch(sub:Account_Number_Key) = Level:Benign) THEN
        access:TRADEACC.clearkey(tra:Account_Number_Key)
        tra:account_number = sub:main_account_number
        IF (access:TRADEACC.fetch(tra:Account_Number_Key) = Level:Benign) THEN
            ResultValue = tra:Company_Name
        END
    END
    RETURN ResultValue
ProgressBarCancelled       PROCEDURE()
result  LONG
    CODE
    result = 0
    cancel# = 0

    ACCEPT
        CASE EVENT()
        OF Event:Timer
            BREAK
        OF Event:CloseWindow
            cancel# = 1
            BREAK
        OF Event:accepted
            IF (FIELD() = ?Progress:Cancel) THEN
                cancel# = 1
                BREAK
            END
        END
    END

    IF (cancel# = 1) THEN
        BEEP(BEEP:SystemAsterisk)
        IF (Message('Are you sure you want to cancel?','ServiceBase 2000',|
                     ICON:Question,Button:Yes+Button:No,Button:Yes,0) = Button:Yes) THEN
                ProgressWindow{PROP:Timer}=0
                ProgressWindow{PROP:HIDE}=True
                result = -1
        END
    END
    RETURN result
RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674





