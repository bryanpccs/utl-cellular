    If check_for_bouncers_temp = 1 And saved_esn_temp <> ''
        access:defaults.open()
        access:defaults.usefile()
        access:jobs.open()
        access:jobs.usefile()
        access:bouncer.open()
        access:bouncer.usefile()
        access:audit.open()
        access:audit.usefile()
        access:status.open()
        access:status.usefile()
        access:jobstage.open()
        access:jobstage.usefile()
    
        Set(defaults)
        access:defaults.next()


        found# = 0
        setcursor(cursor:wait)
        save_job_id = access:jobs.savefile()
        access:jobs.clearkey(job:esn_key)
        job:esn = saved_esn_temp
        set(job:esn_key,job:esn_key)
        loop
            if access:jobs.next()
               break
            end !if
            if job:esn <> saved_esn_temp      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            If job:ref_number <> saved_ref_number_temp
                If job:cancelled = 'YES'
                    Cycle
                End!If job:cancelled = 'YES'
                If job:date_booked + def:bouncertime > Today()
                    get(bouncer,0)
                    if access:bouncer.primerecord() = level:benign
                        bou:original_ref_number = saved_ref_number_temp
                        bou:bouncer_job_number  = job:ref_number
                        found# += 1
                        If access:bouncer.tryinsert()
                            access:bouncer.cancelautoinc()
                        End!If access:bouncer.tryinsert()
                    End!if access:bouncer.primerecord() = level:benign
                End!If job:date_booked < Today() - def:warranty_period
            End!If job:ref_number <> ref_number_temp
        end !loop
        access:jobs.restorefile(save_job_id)
        setcursor()
        
        access:jobs.clearkey(job:ref_number_key)
        job:ref_number  = saved_ref_number_temp
        If access:jobs.fetch(job:ref_number_key) = Level:Benign
            If found# <> 0
                If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
                    job:bouncer_type    = 'BOT'
                End!If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
                If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
                    job:bouncer_type    = 'CHA'
                End!If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
                IF job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
                    job:bouncer_type    = 'WAR'
                End!IF job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
                job:bouncer = 'X'
                
                If job:edi  = 'YES'
                    job:edi = 'YEB'
                End!If job:edi  = 'YES'

                job:EDI = PendingJob(job:Manufacturer)
                access:jobs.update()
                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'JOB MARKED AS BOUNCER'
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:benign
                beep(beep:systemexclamation)  ;  yield()
                case message('This job has been found to be a Bouncer.'&|
                        '||This job can now only be Invoiced after it has been authorised '&|
                        'from the Bouncer List.', |
                        'ServiceBase 2000', icon:exclamation, |
                         button:ok, button:ok, 0)
                of button:ok
                end !case
            Else
                job:bouncer = ''
                access:jobs.update()                        
            End!If found# <> 0
        End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
        access:jobstage.close()
        access:status.close()
        access:audit.close()
        access:bouncer.close()
        access:jobs.close()
        access:defaults.close()
    End!If check_for_bouncer_temp = 1
