        If ExcludeFromInvoice(job:charge_type)
            Cycle
        End!If ExcludeFromInvoice(job:charge_type)

        If job:date_completed = ''
            Cycle
        End!If job:date_completed = ''

        If job:invoice_exception = 'YES'
            Cycle
        End!If job:invoice_exception = 'YES'

        If job:bouncer = 'X'
            ! Start Change 4776 BE(22/10/2004)
            !job:invoice_exception      = 'YES'
            !job:invoice_failure_reason = 'BOUNCER'
            !access:jobs.update()
            !count# += 1
            ! End Change 4776 BE(22/10/2004)
            Cycle
        End!If job:bouncer <> ''

        If def:qa_required = 'YES'
            If job:qa_passed <> 'YES'
                job:invoice_exception   = 'YES'
                job:invoice_failure_reason   = 'QA NOT PASSED'
                access:jobs.update()
                count# += 1
                Cycle
            End!If job:qa_passed <> 'YES'
        End!If def:qa_required = 'YES'

        IF job:ignore_chargeable_charges = 'YES'
        	job:sub_total	= job:labour_cost + job:parts_cost + job:courier_cost
        	access:jobs.update()
            If job:sub_total = 0
                job:invoice_exception   = 'YES'
                job:invoice_failure_reason  = 'ZERO VALUE'
                access:jobs.update()
                count# += 1
                Cycle
            End!If job:sub_total = 0
        Else!IF job:ignore_chargeable_charges = 'YES'
            Pricing_Routine('C',labour",parts",pass",a")
            If pass"    = False
                job:invoice_exception   = 'YES'
                job:invoice_failure_reason  = 'ZERO VALUE: NO PRICING STRUCTURE'
                access:jobs.update()
                count# += 1
                Cycle
            Else!If pass"    = False
                job:labour_cost = labour"
                job:parts_cost  = parts"
                job:sub_total   = job:labour_cost + job:parts_cost + job:courier_cost
            End!If pass"    = False
        End!IF job:ignore_chargeable_charges = 'YES'
