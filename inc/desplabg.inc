
SECTION('CheckOld')
    save_cou_ali_id = access:courier_alias.savefile()
    access:courier_alias.clearkey(cou_ali:courier_type_key)
    cou_ali:courier_type = 'LABEL G'
    set(cou_ali:courier_type_key,cou_ali:courier_type_key)
    loop
        if access:courier_alias.next()
           break
        end !if
        if cou_ali:courier_type <> 'LABEL G'      |
            then break.  ! end if
        tmp:OldConsignNo = ''

        save_job_ali_id = access:jobs_alias.savefile()
        access:jobs_alias.clearkey(job_ali:DateDespatchKey)
        job_ali:courier         = cou_ali:courier
        If Today() %7 = 6
            thedate#    = Today() + 2
        Else
            thedate#    = Today()                
        End!If Today() %7 = 6
        job_ali:date_despatched = thedate#
        set(job_ali:DateDespatchKey,job_ali:DateDespatchKey)
        loop
            if access:jobs_alias.next()
               break
            end !if
            if job_ali:courier         <> cou_ali:courier      |
            or job_ali:date_despatched <> thedate#      |
                then break.  ! end if
            If job_ali:consignment_number = 'N/A'    
                Cycle                        
            End!If job_ali:consignment_number = 'N/A'
    Compile('***',Debug=1)
      Message('Found job despatched today (' & tmp:accountnumber &|
              '|job:account: ' & job_ali:account_number &|
              '|job_ali:postcode: ' & job_ali:postcode_delivery &|
              '|job:postcode: ' & job:postcode_delivery,'Debug Message',icon:exclamation)
    ***                        
            If job_ali:account_number   = tmp:accountnumber and job_ali:postcode_Delivery = job:postcode_delivery
                If job_ali:consignment_number <> ''
                    tmp:OldConsignNo = job_ali:consignment_number
    Compile('***',Debug=1)
      Message('Found Old Consignment Number on Job: ' & job_ali:ref_number,'Debug Message',icon:exclamation)
    ***                            
                    Break
                End!If job_ali:consignment_number <> ''
            End!If job_ali:account_number   = job:account_number
        end !loop
        access:jobs_alias.restorefile(save_job_ali_id)
        If tmp:OldConsignNo <> ''
            Break
        Else!If tmp:OldConsignNo <> ''
            save_job_ali_id = access:jobs_alias.savefile()
            access:jobs_alias.clearkey(job_ali:DateDespLoaKey)
            job_ali:loan_courier    = cou_ali:courier
            If Today() %7 = 6
                thedate#    = Today() + 2
            Else
                thedate#    = Today()                
            End!If Today() %7 = 6            
            job_ali:loan_despatched = thedate#
            set(job_ali:DateDespLoaKey,job_ali:DateDespLoaKey)
            loop
                if access:jobs_alias.next()
                   break
                end !if
                if job_ali:loan_courier    <> cou_ali:courier      |
                or job_ali:loan_despatched <> thedate#      |
                    then break.  ! end if
                If job:loan_consignment_number = 'N/A'    
                    Cycle                        
                End!If job:loan_consignment_number = 'N/A'
    Compile('***',Debug=1)
      Message('Found loan despatched today (' & tmp:accountnumber &|
              '|job:account: ' & job_ali:account_number &|
              '|job_ali:postcode: ' & job_ali:postcode_delivery &|
              '|job:postcode: ' & job:postcode_delivery,'Debug Message',icon:exclamation)
    ***                                              
                If job_ali:account_number   = tmp:accountnumber and job_ali:postcode_Delivery = job:postcode_delivery
                     If job_ali:loan_consignment_number <> ''
                        tmp:OldConsignNo = job_ali:loan_consignment_number
    Compile('***',Debug=1)
      Message('Found Old Consignment Number on Job(Loan): ' & job_ali:ref_number,'Debug Message',icon:exclamation)
    ***                                                    
                        Break
                    End!If job_ali:loan_consignment_number <> ''
                End!If job_ali:account_number   = job:account_number
            end !loop
            access:jobs_alias.restorefile(save_job_ali_id)
            IF tmp:OldConsignNo <> ''
                Break
            Else!IF tmp:OldConsignNo <> ''
                save_job_ali_id = access:jobs_alias.savefile()
                access:jobs_alias.clearkey(job_ali:DateDespExcKey)
                job_ali:exchange_courier    = cou_ali:courier
                If Today() %7 = 6
                    thedate#    = Today() + 2
                Else
                    thedate#    = Today()                
                End!If Today() %7 = 6                
                job_ali:exchange_despatched = thedate#
                set(job_ali:DateDespExcKey,job_ali:DateDespExcKey)
                loop
                    if access:jobs_alias.next()
                       break
                    end !if
                    if job_ali:exchange_courier    <> cou_ali:courier      |
                    or job_ali:exchange_despatched <> thedate#      |
                        then break.  ! end if
                    If job_ali:exchange_consignment_number = 'N/A'    
                        Cycle    
                    End!If job_ali:exchange_consignment_number = 'N/A'
    Compile('***',Debug=1)
      Message('Found job exchange despatched today (' & tmp:accountnumber &|
              '|job:account: ' & job_ali:account_number &|
              '|job_ali:postcode: ' & job_ali:postcode_delivery &|
              '|job:postcode: ' & job:postcode_delivery,'Debug Message',icon:exclamation)
    ***                                                  
                    If job_ali:account_number   = tmp:accountnumber and job_ali:postcode_Delivery = job:postcode_delivery
                        If job_ali:exchange_consignment_number <> ''
                            tmp:OldConsignNo = job_ali:exchange_consignment_number
    Compile('***',Debug=1)
      Message('Found Old Consignment Number on Job(Exchange): ' & job_ali:ref_number,'Debug Message',icon:exclamation)
    ***                                                        
                            Break
                        End!If job_ali:exchange_consignment_number <> ''
                    End!If job_ali:account_number   = job:account_number
                end !loop
                access:jobs_alias.restorefile(save_job_ali_id)
                If tmp:OldConsignNo <> ''
                    Break
                End!If tmp:OldConsignNo <> ''
            End!IF tmp:OldConsignNo <> ''
        End!If tmp:OldConsignNo <> ''
    end !loop
    access:courier_alias.restorefile(save_cou_ali_id)

SECTION('Export')
    Remove(explabg)
    access:explabg.open()
    access:explabg.usefile()

    Clear(gen:record)
    epg:account_number  = Format(cou:account_number,@s8)
    If multi# = 1   
        epg:ref_number        = '|DB' & Clip(Format(dbt:batch_number,@s9))
        epg:customer_name     = '|' & Format(account_number2_temp,@s30)
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = account_number2_temp
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key) 
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(sub:contact_name)
                    epc:address_line1 = '|' & Left(sub:company_name)
                    epc:address_line2 = '|' & Left(sub:address_line1)
                    epc:town = '|' & Left(sub:address_line2)
                    epc:county = '|' & Left(sub:address_line3)
                    epc:postcode = '|' & Left(sub:postcode)
                else!if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(tra:contact_name)
                    epc:address_line1 = '|' & Left(tra:company_name)
                    epc:address_line1 = '|' & Left(tra:address_line1)
                    epc:town = '|' & Left(tra:address_line2)
                    epc:county = '|' & Left(tra:address_line3)
                    epc:postcode = '|' & Left(tra:postcode)
                end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
    Else!If multi# = 1
        epg:ref_number      = '|' & Format('J' & Clip(job:Ref_number) & '/DB' & Clip(dbt:batch_number) & '/' |
                        & Clip(job:order_number),@s30)
        If job:surname = ''                         
            epg:contact_Name    = '|' & Format('N/A',@s30)
        Else!If job:surname = ''
            epg:contact_Name    = '|' & Format(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & |
                                        CLip(job:surname),@s30)
        End!If job:surname = ''
        epg:customer_name   = '|' & Format(job:account_number,@s30)                                             
        epg:Address_Line1   = '|' & Format(job:company_name_delivery,@s30)              
        epg:Address_Line2   = '|' & Format(job:address_line1_delivery,@s30)
        epg:Address_Line3   = '|' & Format(job:address_line2_delivery,@s30)
        epg:Address_Line4   = '|' & Format(job:address_line3_delivery,@s30)
        epg:Postcode        = '|' & Format(job:postcode_delivery,@s4)
        
    End!If multi# = 1
    epg:city_service      = '|' & Format(cou:service,@s2)
    Case job:despatch_type    
        Of 'JOB'
            If job:jobservice <> ''
                epg:city_service      = '|' & Format(job:jobservice,@s2)
            End!            If job:jobservice <> ''
        Of 'EXC'
            If job:excservice <> ''
                epg:city_service      = '|' & Format(job:excservice,@s2)            
            End!If job:excservice <> ''
        Of 'LOA'
            If job:loaservice <> ''
                epg:city_service      = '|' & Format(job:loaservice,@s2)
            End!If job:loaservice <> ''
    End!Case job:despatch_type
    access:jobnotes.clearkey(jbn:refNumberKey)
    jbn:RefNumber   = job:ref_number
    access:jobnotes.tryfetch(jbn:refnumberkey)
    epg:City_Instructions   = '|' & Format(jbn:delivery_text,@s30)
    epg:Pudamt          = '|' & Format(0.00,@s10)
    return# = 1
    If job:despatch_type = 'EXC' Or job:despatch_type = 'LOA'
        If job:workshop = 'YES'
            return# = 0
        Else!If job:workshop = 'YES'
            If job:third_party_site <> ''
                return# = 0
            End!If job:third_party_site <> ''
        End!If job:workshop = 'YES'
    End!If job:despatch_type = 'EXC'
    If job:despatch_type = 'JOB'
        If job:loan_unit_number = ''
            return# = 0
        End!If job:loan_unit_number = ''
    End!If job:despatch_type = 'JOB'
    
    If return# = 0
        epg:Return_it   = '|N'
    Else!If return# = 0
        epg:Return_it   = '|Y'
    End!If return# = 0
    
    epg:Saturday    = '|N'
    epg:dog         = '|' & Format('Mobile Phone Goods',@s30)
    epg:nol         = '|' & Format('01',@s8)
    If tmp:OldConsignNo <> ''
        Compile('***',Debug=1)
            message('Previous Consignment Number: ' & tmp:OldConsignNo,'Debug Message', icon:exclamation)
        ***
        epg:JobNo       = '|' & Clip(Format(tmp:OldConsignNo,@s8))      
    Else!   If tmp:OldConsignNo <> ''
        epg:JobNo       = '|        '       
    End!    If tmp:OldConsignNo <> ''
    epg:Weight      = '|00.00'

    access:explabg.insert()
    
    filename3   = Clip(Cou:export_Path) & '\SB' & Format(job:Ref_number,@n06) & '.TXT'    
    Remove(filename3)
    Open(outfile)
    If Error()
        Create(outfile)
        Open(outfile)
    End!Open(outfile)
    ouf:line1   = 'Customer Name,' & CLip(epg:customer_name)
    Add(outfile)
    ouf:line1   = 'Date,' & Clip(Format(Today(),@d6))
    Add(outfile)
    ouf:line1   = 'No Passed,' & CLip(epg:JobNo)    
    add(outfile)
    Close(outfile)
    
!    Set(explabg,0)
!    access:explabg.next()
!    If Sub(epg:account_number,1,2) = '10'
!        epg:account_number = Format(Sub(epg:account_number,3,Len(epg:account_number)) & '  ',@s8)
!        access:explabg.update()
!    End!If Sub(epg:account_number,1,2) = '10'

    access:explabg.close()

SECTION('Error')
    access:explabg.open()
    access:explabg.usefile()
    Set(explabg)
    access:explabg.next()
    tmp:labelerror = 0
    Setcursor()
    Compile('***',Debug=1)
        message('Consignment Number Returned: ' & Clip(epg:account_number),'Debug Message', icon:exclamation)
    ***    
    Case Sub(epg:account_number,1,2)
        Of '01'
            tmp:labelerror = 1
            Case MessageEx('Job Number: ' & Clip(job:ref_number) & ' - Export File Error: 01<13,10><13,10>Incorrect Format.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Of '02'
            tmp:labelerror = 1
            Case MessageEx('Job Number: ' & Clip(job:ref_number) & ' - Export File Error: 02<13,10><13,10>Missing City Link Account Number.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Of '03'
            tmp:labelerror = 1
            Case MessageEx('Job Number: ' & Clip(job:ref_number) & ' - Export File Error: 03<13,10><13,10>No Job Number.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Of '04'
            tmp:labelerror = 1
            Case MessageEx('Job Number: ' & Clip(job:ref_number) & ' - Export File Error: 04<13,10><13,10>No Customer Name.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Of '05'
            tmp:labelerror = 1
            Case MessageEx('Job Number: ' & Clip(job:ref_number) & ' - Export File Error: 05<13,10><13,10>No Contact Name.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Of '06'
            tmp:labelerror = 1
            Case MessageEx('Job Number: ' & Clip(job:ref_number) & ' - Export File Error: 06<13,10><13,10>Invalid City Link Service Code.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Of '07'
            tmp:labelerror = 1
            Case MessageEx('Job Number: ' & Clip(job:ref_number) & ' - Export File Error: 07<13,10><13,10>Service not available to destination.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Of '08'
            tmp:labelerror = 1
            Case MessageEx('Job Number: ' & Clip(job:ref_number) & ' - Export File Error: 08<13,10><13,10>Description Of Goods Required.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Of '09'
            tmp:labelerror = 1
            Case MessageEx('Job Number: ' & Clip(job:ref_number) & ' - Export File Error: 09<13,10><13,10>Number Of Labels Required.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Of '10'
            tmp:labelerror = 1
            Case MessageEx('Job Number: ' & Clip(job:ref_number) & ' - Export File Error: 10<13,10><13,10>Invalid Postcode.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Else 
            tmp:labelerror = epg:account_number              
    End!Case epg:account_number
    Setcursor(cursor:wait)
    filename3   = Clip(Cou:export_Path) & '\SB' & Format(job:Ref_number,@n06) & '.TXT'    
    Open(outfile)
    ouf:line1   = 'Returned,' & CLip(epg:account_number)    
    Add(outfile)
    Close(outfile)
    
    access:explabg.close()
                