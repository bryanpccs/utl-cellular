    !Accessory Check
    pass# = 1
	exchange_required# = 0             
	loan_required# = 0
	refurb# = 0
	error# = 0
	
	Case type#
		Of 1
			access:trantype.clearkey(trt:transit_type_key)
			trt:transit_type = job:transit_type
			if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
				If trt:exchange_unit = 'YES' or job:exchange_unit_number <> ''	
					exchange_required# = 1
				End!If trt:exchange_unit = 'YES'
				If exchange_required# = 1 or loan_required# = 1
					access:subtracc.clearkey(sub:account_number_key)
					sub:account_number = job:account_number
					if access:subtracc.fetch(sub:account_number_key) = level:benign
						access:tradeacc.clearkey(tra:account_number_key) 
						tra:account_number = sub:main_account_number
						if access:tradeacc.fetch(tra:account_number_key) = level:benign
							If tra:refurbcharge = 'YES'
								refurb# = 1
							End!If tra:refurbcharge = 'YES'
						end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
					end!if access:subtracc.fetch(sub:account_number_key) = level:benign	
				End!If exchange_required# = 1 or loan_required# = 1					
			end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
		Of 2
			exchange_required# = 1				
		Of 3
			loan_required# = 1						
	End!If type# = 1					
	
	If refurb# = 1
		Case MessageEx('This unit is authorised for Refurbishment.<13,10><13,10>Is the Refurbishment Complete?','ServiceBase 2000',|
		               'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
			Of 1 ! &Yes Button
			
			Of 2 ! &No Button
				pass# = 0
                job:qa_rejected      = 'YES'
                job:date_qa_rejected = Today()
                job:time_qa_rejected = Clock()
                If preliminary# = 1
                	GetStatus(625,1,'JOB') !Electronic QA Rejected

				Else                        		
                	GetStatus(615,1,'JOB') !Manual QA Rejected

                End                            	
                access:jobs.update()
                reason" = ''
                Qa_Failure_Reason(Reason")                                          !Get the failure reason and write
                get(audit,0)                                                        !the incoming and outgoing accessories
                if access:audit.primerecord() = level:benign                        !into the Audit trail
                    aud:notes		  = 'REFURBISHMENT NOT COMPLETE'
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'QA REJECTION: ' & CLip(Reason")
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:benign

                beep(beep:systemasterisk)
				glo:notes_global	= Clip(Reason")                        
                If def:qa_failed_label = 'YES'                                      !Print the failure report/label
					glo:select1	= job:ref_number
					QA_Failed_Label
                   glo:select1  = ''
                End!If def:qa_failed_label = 'YES'
                If def:qa_failed_report = 'YES'
                    glo:select1 = job:ref_number
                    QA_Failed
                    glo:select1 = ''
                End!If def:qa_failed_report = 'YES'
                glo:notes_global = ''
			
		End!Case MessageEx				
	End!If tra:refurbcharge = 'YES'
	If loan_required# = 1 And pass# = 1
	    found# = 0
	    save_lac_id = access:loanacc.savefile()
	    access:loanacc.clearkey(lac:ref_number_key)
	    lac:ref_number = job:loan_unit_number
	    set(lac:ref_number_key,lac:ref_number_key)
	    loop
	        if access:loanacc.next()
	           break
	        end !if
	        if lac:ref_number <> job:loan_unit_number      |
	            then break.  ! end if
	        found# = 1
	        Break
	    end !loop
	    access:loanacc.restorefile(save_lac_id)
	    If found# = 1
            glo:select1 = job:ref_number
            glo:select2 = ''
            glo:select12 = job:model_number
            Validate_Job_Accessories                                                    !Bring up the list of all accessories
	        error# = 0
		    save_lac_id = access:loanacc.savefile()
		    access:loanacc.clearkey(lac:ref_number_key)
		    lac:ref_number = job:loan_unit_number
		    set(lac:ref_number_key,lac:ref_number_key)
		    loop
		        if access:loanacc.next()
		           break
		        end !if
		        if lac:ref_number <> job:loan_unit_number      |
		            then break.  ! end if
				Sort(glo:queue,glo:pointer)
				glo:pointer	= lac:accessory
				Get(glo:queue,glo:pointer)
				If Error()	            	
					error# = 1
					Break
				End!If Error()
		    end !loop
		    access:loanacc.restorefile(save_lac_id)
								        
		End!If found# = 1						
	End!If loan_required# = 1 And pass# = 1

	If exchange_required# = 0 and pass# = 1 and loan_required# = 0
        found# = 0
        save_jac_id = access:jobacc.savefile()                                          !Check to see if there are any
        clear(jac:record, -1)                                                           !accessories attached to this job.
        jac:ref_number = job:ref_number
        set(jac:ref_number_key,jac:ref_number_key)
        loop
            next(jobacc)
            if errorcode()                |
               or jac:ref_number <> job:Ref_number      |
               then break.  ! end if
               
            found# = 1
            Break
        end !loop
        access:jobacc.restorefile(save_jac_id)
        If found# = 1                                                                   !Some accessories have been found.
            glo:select1 = job:ref_number
            glo:select2 = ''
            glo:select12 = job:model_number
            Validate_Job_Accessories                                                    !Bring up the list of all accessories
            error# = 0                                                                  !for this model
            setcursor(cursor:wait)
            save_jac_id = access:jobacc.savefile()
            access:jobacc.clearkey(jac:ref_number_key)
            jac:ref_number = job:ref_number
            set(jac:ref_number_key,jac:ref_number_key)
            loop                                                                        !Loop through the accessories attached
                if access:jobacc.next()                                                 !to the job.
                   break
                end !if
                if jac:ref_number <> job:ref_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if

                Sort(glo:queue,glo:pointer)                                       !See if the accessories can be found in
                glo:pointer  = jac:accessory                                         !the tagged entries.
                Get(glo:queue,glo:pointer)
                If Error()
                    error# = 1                                                          !If not, then it's a Missing Accessory
                    Break
                End!If Error()
            end !loop
            access:jobacc.restorefile(save_jac_id)
            setcursor()
        End!If found# = 1
	End!If exchange_required# = 0 and pass# = 1	                                                                                            !Missing Accessory Error
    If error# = 1
        pass# = 0
        If def:qa_required = 'YES'                                              !If QA after completion
			Case MessageEx('This unit has a missing accessory.<13,10><13,10>It will now FAIL QA.','ServiceBase 2000',|
			               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
				Of 1 ! &OK Button
			End!Case MessageEx

			
			status_audit# = 1
            
			Case type#
				Of 1!Job
                    If preliminary# = 1						
                        GetStatus(625,1,'JOB') !Electronic QA Rejected
                    Else!If preliminary# = 1
                        GetStatus(615,1,'JOB') !Manual QA Rejected
                    End!If preliminary# = 1
					job:despatch_Type = 'JHL'
				Of 2!Exchange
                    If preliminary# = 1						
                        GetStatus(625,1,'EXC') !Electronic QA Rejected
                    Else!If preliminary# = 1
                        GetStatus(615,1,'EXC') !Manual QA Rejected
                    End!If preliminary# = 1

					job:despatch_Type = 'EHL'
				Of 3!Loan							
                    If preliminary# = 1						
                        GetStatus(625,1,'LOA') !Electronic QA Rejected
                    Else!If preliminary# = 1
                        GetStatus(615,1,'LOA') !Manual QA Rejected
                    End!If preliminary# = 1

					job:despatch_Type = 'LHL'
			End!Case type#
            
            access:jobs.update()
            get(audit,0)
            if access:audit.primerecord() = level:benign
                aud:notes         = 'UNIT HELD FROM DESPATCH'
                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                access:users.clearkey(use:password_key)
                use:password = glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'MISSING ACCESSORY'
                Case type#
                	Of 1
                		aud:type = 'JOB'
                	Of 2
                		aud:type = 'EXC'
                	Of 3
                		aud:type = 'LOA'
                End!Case type#
                
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign

        End!If def:qa_required = 'YES'

        If def:qa_before_complete = 'YES'
			Case MessageEx('This unit has a missing accessory.<13,10><13,10>It will now FAIL QA.','ServiceBase 2000',|
			               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
				Of 1 ! &OK Button
			End!Case MessageEx

			
			status_audit# = 1
            
			Case type#
				Of 1!Job
                    If preliminary# = 1						
                        GetStatus(625,1,'JOB') !Electronic QA Rejected
                    Else!If preliminary# = 1
                        GetStatus(615,1,'JOB') !Manual QA Rejected
                    End!If preliminary# = 1

					job:despatch_Type = 'JHL'
				Of 2!Exchange
                    If preliminary# = 1						
                        GetStatus(625,1,'EXC') !Electronic QA Rejected
                    Else!If preliminary# = 1
                        GetStatus(615,1,'EXC') !Manual QA Rejected
                    End!If preliminary# = 1
					job:despatch_Type = 'EHL'
				Of 3!Loan							
                    If preliminary# = 1						
                        GetStatus(625,1,'LOA') !Electronic QA Rejected
                    Else!If preliminary# = 1
                        GetStatus(615,1,'LOA') !Manual QA Rejected
                    End!If preliminary# = 1
					job:despatch_Type = 'LHL'
			End!Case type#
            access:jobs.update()

            reason" = ''
            QA_failure_reason(reason") 
            get(audit,0)
            if access:audit.primerecord() = level:benign                        !Put the accessoried booked in, and
                aud:notes         = 'ACCESSORIES BOOKED IN:-'                   !booked out into the Audit Trail
	            Case type#
					Of 3
					    save_lac_id = access:loanacc.savefile()
					    access:loanacc.clearkey(lac:ref_number_key)
					    lac:ref_number = job:loan_unit_number
					    set(lac:ref_number_key,lac:ref_number_key)
					    loop
					        if access:loanacc.next()
					           break
					        end !if
					        if lac:ref_number <> job:loan_unit_number      |
					            then break.  ! end if
		                    aud:notes = CLip(aud:notes) & '<13,10>' & Clip(lac:accessory)				    
		                end !loop	                	
					Else	                										
		                save_jac_id = access:jobacc.savefile()
		                access:jobacc.clearkey(jac:ref_number_key)
		                jac:ref_number = glo:select1
		                set(jac:ref_number_key,jac:ref_number_key)
		                loop
		                    if access:jobacc.next()
		                       break
		                    end !if
		                    if jac:ref_number <> glo:select1      |
		                        then break.  ! end if
		                    aud:notes = CLip(aud:notes) & '<13,10>' & Clip(jac:accessory)
		                end !loop
		                access:jobacc.restorefile(save_jac_id)
	            End!Case type#	                
                
                aud:notes = Clip(aud:notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                Loop x# = 1 To Records(glo:queue)
                    Get(glo:queue,x#)
                    aud:notes = Clip(aud:notes) & '<13,10>' & CLip(glo:pointer)
                End!Loop x# = 1 To Records(glo:queue)
                glo:notes_global    = Clip(aud:notes)

                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                access:users.clearkey(use:password_key)
                use:password = glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'QA REJECTION: ' & Clip(reason")
                Case type#
                	Of 1
                		aud:type = 'JOB'
                	Of 2
                		aud:type = 'EXC'
                	Of 3
                		aud:type = 'LOA'
                End!Case type#
                
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign
            beep(beep:systemasterisk)
            If def:qa_failed_label = 'YES'                                      !Print the failure report/label
				Case type#
					Of 1!Job
						glo:select1	= job:ref_number
						QA_Failed_Label
					Of 2!Exchange
						glo:select1	= job:exchange_unit_number
						QA_Failed_Label_Exchange
					Of 3!Loan							
						glo:select1	= job:loan_unit_number
						QA_Failed_Label_Loan
				End!Case type#
                glo:select1  = ''
            End!If def:qa_failed_label = 'YES'
            If def:qa_failed_report = 'YES'
				Case type#
					Of 1!Job
                        glo:select1 = job:ref_number
                        QA_Failed
					Of 2!Exchange
                        glo:select1 = job:exchange_unit_number
                        QA_Failed_Exchange
					Of 3!Loan							
                        glo:select1 = job:loan_unit_number
                        QA_Failed_Loan
				End!Case type#
            
                glo:select1 = ''
            End!If def:qa_failed_report = 'YES'
            glo:notes_global = ''

        End!If def:qa_before_completed = 'YES'

    Else!If error# = 1
        mismatch# = 0
		Case type#
			Of 3
		        Loop x# = 1 To Records(glo:queue)                                    !Check to see if more accessories were
		            Get(glo:queue,x#) 
					access:loanacc.clearkey(lac:ref_number_key)
					lac:ref_number = job:loan_unit_number
					lac:accessory  = glo:pointer
					if access:loanacc.tryfetch(lac:ref_number_key)
		                mismatch# = 1
		                Break
		            End!if access:jobacc.tryfetch(jac:ref_number_key)
		        End!Loop x# = 1 To Records(glo:queue)
			
			Else
		        Loop x# = 1 To Records(glo:queue)                                    !Check to see if more accessories were
		            Get(glo:queue,x#)                                                !tagged than were on the job
		            access:jobacc.clearkey(jac:ref_number_key)
		            jac:ref_number = job:ref_number
		            jac:accessory  = glo:pointer
		            if access:jobacc.tryfetch(jac:ref_number_key)
		                mismatch# = 1
		                Break
		            End!if access:jobacc.tryfetch(jac:ref_number_key)
		        End!Loop x# = 1 To Records(glo:queue)
			
		End!Case type#
        If mismatch# = 1                                                        !If yes fail the QA
            pass# = 0
			Case MessageEx('There is a Mismatch between the selected Accessories and the Accessories booked in with the job.<13,10><13,10>This job will now be marked as a QA Failure. The Failure Document/Label will now be printed.','ServiceBase 2000',|
			               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
				Of 1 ! &OK Button
			End!Case MessageEx
	
            job:qa_rejected      = 'YES'
            job:date_qa_rejected = Today()
            job:time_qa_rejected = Clock()
            If preliminary# = 1
            	status_audit# = 1
            	Case type#
            		Of 1 !Job
                    	GetStatus(625,1,'JOB') !Electronic QA Rejected
            		Of 2 !Exchange
                    	GetStatus(625,1,'EXC') !Electronic QA Rejected
            		Of 3 !Loan
                    	GetStatus(625,1,'LOA') !Electronic QA Rejected
            	End!Case type#
			Else                        		
            	status_number# = 615 !Manual QA Rejected
            	status_audit# = 1
            	Case type#
            		Of 1 !Job
                    	GetStatus(615,1,'JOB') !Electronic QA Rejected
            		Of 2 !Exchange
                    	GetStatus(615,1,'EXC') !Electronic QA Rejected
            		Of 3 !Loan
                    	GetStatus(615,1,'LOA') !Electronic QA Rejected
            	End!Case type#
            End                            	
            access:jobs.update()
            reason" = ''
            Qa_Failure_Reason(Reason")                                          !Get the failure reason and write
            get(audit,0)                                                        !the incoming and outgoing accessories
            if access:audit.primerecord() = level:benign                        !into the Audit trail
                aud:notes         = 'ACCESSORIES BOOKED IN:-'
                Case type#
                	Of 3
					    save_lac_id = access:loanacc.savefile()
					    access:loanacc.clearkey(lac:ref_number_key)
					    lac:ref_number = job:loan_unit_number
					    set(lac:ref_number_key,lac:ref_number_key)
					    loop
					        if access:loanacc.next()
					           break
					        end !if
					        if lac:ref_number <> job:loan_unit_number      |
					            then break.  ! end if
		                    aud:notes = CLip(aud:notes) & '<13,10>' & Clip(lac:accessory)				    
		                end !loop	                	
                	
                	Else
		                save_jac_id = access:jobacc.savefile()
		                access:jobacc.clearkey(jac:ref_number_key)
		                jac:ref_number = glo:select1
		                set(jac:ref_number_key,jac:ref_number_key)
		                loop
		                    if access:jobacc.next()
		                       break
		                    end !if
		                    if jac:ref_number <> glo:select1      |
		                        then break.  ! end if
		                    aud:notes = CLip(aud:notes) & '<13,10>' & Clip(jac:accessory)
		                end !loop
		                access:jobacc.restorefile(save_jac_id)
                	
                End!Case type#
                
                aud:notes = Clip(aud:notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                Loop x# = 1 To Records(glo:queue)
                    Get(glo:queue,x#)
                    aud:notes = Clip(aud:notes) & '<13,10>' & CLip(glo:pointer)
                End!Loop x# = 1 To Records(glo:queue)
                glo:notes_global    = Clip(aud:notes)

                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                access:users.clearkey(use:password_key)
                use:password = glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'QA REJECTION: ' & CLip(reason")
                Case type#
                	Of 1
                		aud:type = 'JOB'
                	Of 2
                		aud:type = 'EXC'
                	Of 3
                		aud:type = 'LOA'
                End!Case type#
                
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign

            beep(beep:systemasterisk)
            
            If def:qa_failed_label = 'YES'                                      !Print the failure report/label
				Case type#
					Of 1!Job
						glo:select1	= job:ref_number
						QA_Failed_Label
					Of 2!Exchange
						glo:select1	= job:exchange_unit_number
						QA_Failed_Label_Exchange
					Of 3!Loan							
						glo:select1	= job:loan_unit_number
						QA_Failed_Label_Loan
				End!Case type#
                glo:select1  = ''
            End!If def:qa_failed_label = 'YES'
            If def:qa_failed_report = 'YES'
				Case type#
					Of 1!Job
                        glo:select1 = job:ref_number
                        QA_Failed
					Of 2!Exchange
                        glo:select1 = job:exchange_unit_number
                        QA_Failed_Exchange
					Of 3!Loan							
                        glo:select1 = job:loan_unit_number
                        QA_Failed_Loan
				End!Case type#
            
                glo:select1 = ''
            End!If def:qa_failed_report = 'YES'
            glo:notes_global = ''

        Else!If mismatch                                                        !If everything ok allow the job to
            job:despatch_type = 'JOB'                                           !be despatched although if it is a
            access:jobs.update()                                                !QA before completion this bit
        End!If mismatch# = 1                                                    !shouldn't matter

    End!If error# = 1
    If pass# = 1
        get(audit,0)
        if access:audit.primerecord() = level:benign                        !Put the accessoried booked in, and
            aud:notes         = 'ACCESSORIES VALIDATED:-'                   !booked out into the Audit Trail
            Case type#
            	Of 3
				    save_lac_id = access:loanacc.savefile()
				    access:loanacc.clearkey(lac:ref_number_key)
				    lac:ref_number = job:loan_unit_number
				    set(lac:ref_number_key,lac:ref_number_key)
				    loop
				        if access:loanacc.next()
				           break
				        end !if
				        if lac:ref_number <> job:loan_unit_number      |
				            then break.  ! end if
	                    aud:notes = CLip(aud:notes) & '<13,10>' & Clip(lac:accessory)				    
	                end !loop	                	
                    access:loanacc.restorefile(save_lac_id)
				Else!Case type#
		            save_jac_id = access:jobacc.savefile()
		            access:jobacc.clearkey(jac:ref_number_key)
		            jac:ref_number = glo:select1
		            set(jac:ref_number_key,jac:ref_number_key)
		            loop
		                if access:jobacc.next()
		                   break
		                end !if
		                if jac:ref_number <> glo:select1      |
		                    then break.  ! end if
		                aud:notes = CLip(aud:notes) & '<13,10>' & Clip(jac:accessory)
		            end !loop
		            access:jobacc.restorefile(save_jac_id)
				
			End!Case type#						                	
            	
            aud:ref_number    = job:ref_number
            aud:date          = today()
            aud:time          = clock()
            access:users.clearkey(use:password_key)
            use:password = glo:password
            access:users.fetch(use:password_key)
            aud:user = use:user_code
            aud:action        = 'QA PASSED - ACCESSORIES VALIDATED'
                Case type#
                	Of 1
                		aud:type = 'JOB'
                	Of 2
                		aud:type = 'EXC'
                	Of 3
                		aud:type = 'LOA'
                End!Case type#
            
            access:audit.insert()
        end!�if access:audit.primerecord() = level:benign
    	
    End!If pass# = 1
    
    glo:select1 = ''
    glo:select2 = ''
    glo:select12 = ''
