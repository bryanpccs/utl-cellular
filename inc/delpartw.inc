    do_delete#  = 0
    Scrap# = 0
Compile('***',Debug=1)
	message('Adjustment?: ' & wpr:adjustment,'Debug Message', icon:exclamation)
***
    If wpr:adjustment = 'YES'
		Case MessageEx('Are you sure you want to delete this part?','ServiceBase 2000',|
		               'Styles\Trash.ico','|&Yes|&No',2,0,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
			Of 1 ! &Yes Button
				do_delete# = 1
			Of 2 ! &No Button
		End!Case MessageEx    	
    Else!If wpr:adjustment = 'YES'
    
	    stock_part# = 1
	    If wpr:part_ref_number <> ''
	        access:stock.clearkey(sto:ref_number_key)
	        sto:ref_number  = wpr:part_ref_number
	        IF access:stock.tryfetch(sto:ref_number_key)
	            stock_part# = 2
	        Else!IF access:stock.tryfetch(sto:ref_number_key)
	            If sto:sundry_item = 'YES'
	                stock_part# = 3
	            End!If sto:sundry = 'YES'
	        End!IF access:stock.tryfetch(sto:ref_number_key) = Level:Benign
	    Else!IF wpr:part_ref_number <> ''
	        stock_part# = 0
	    End!IF wpr:part_ref_number <> ''
	    If wpr:order_number <> ''
	        If wpr:date_received <> ''
	            If stock_part# = 3 !Sundry Part
	                do_delete# = 1
	            Else!If stock_part# = 3
	                Case MessageEx('This part exists on order number '&clip(wpr:order_number)&' and has been RECEIVED.<13,10><13,10>Do you wish to Restock this part, or scrap it?','ServiceBase 2000',|
	                               'Styles\Trash.ico','|&Restock Part|&Scrap Part|&Cancel',3,3,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
	                    Of 1 ! &Restock Part Button
	                        Case stock_part#
	                            Of 0 Orof 2!Not originally from stock
	                                glo:select1  = ''
	                                glo:select2  = ''
	                                glo:select3  = ''
	                                Pick_Locations
	                                If glo:select1 <> ''
	                                    do_delete# = 1
	                                    Get(stock,0)
	                                    If access:stock.primerecord() = Level:Benign
	                                        sto:part_number = wpr:part_number
	                                        sto:description = wpr:description
	                                        sto:supplier    = wpr:supplier
	                                        sto:purchase_cost   = wpr:purchase_cost
	                                        sto:sale_cost   = wpr:sale_cost
	                                        sto:shelf_location  = glo:select2
	                                        sto:manufacturer    = job:manufacturer
	                                        sto:location    = glo:select1
	                                        sto:second_location = glo:select3
	                                        If access:stock.insert() = Level:Benign
	                                            get(stohist,0)
	                                            if access:stohist.primerecord() = level:benign
	                                                shi:ref_number           = sto:ref_number
	                                                access:users.clearkey(use:password_key)
	                                                use:password              = glo:password
	                                                access:users.fetch(use:password_key)
	                                                shi:user                  = use:user_code    
	                                                shi:date                 = today()
	                                                shi:transaction_type     = 'ADD'
	                                                shi:despatch_note_number = wpr:despatch_note_number
	                                                shi:job_number           = job:Ref_number
	                                                shi:quantity             = wpr:quantity
	                                                shi:purchase_cost        = wpr:purchase_cost
	                                                shi:sale_cost            = wpr:sale_cost
	                                                shi:retail_cost          = wpr:retail_cost
	                                                shi:information          = 'WARRANTY PART REMOVED FROM JOB'
	                                                shi:notes                = 'PART EXISTS ON ORDER NUMBER: ' & wpr:order_number
	                                                if access:stohist.insert()
	                                                   access:stohist.cancelautoinc()
	                                                end
	                                            end!if access:stohist.primerecord() = level:benign
	                                        End!If access:stock.insert() = Level:Benign
	                                    End!If access:stock.primerecord() = Level:Benign
	                                End!If glo:select1 = ''
	                            Of 1 !Originally from stock
	                                Case MessageEx('This item was originally taken from location: '&clip(sto:location)&'.<13,10><13,10>Do you wish to return it to it''s Original Location, or to a New Location?','ServiceBase 2000',|
	                                               'Styles\Trash.ico','|&Original Location|&New Location|&Cancel',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
	                                    Of 1 ! &Original Location Button
	                                        do_delete# = 1
	                                        sto:quantity_stock += wpr:quantity
	                                        If access:stock.update() = Level:Benign
	                                            get(stohist,0)
	                                            if access:stohist.primerecord() = level:benign
	                                                shi:ref_number           = sto:ref_number
	                                                access:users.clearkey(use:password_key)
	                                                use:password              = glo:password
	                                                access:users.fetch(use:password_key)
	                                                shi:user                  = use:user_code    
	                                                shi:date                 = today()
	                                                shi:transaction_type     = 'ADD'
	                                                shi:despatch_note_number = wpr:despatch_note_number
	                                                shi:job_number           = job:Ref_number
	                                                shi:quantity             = wpr:quantity
	                                                shi:purchase_cost        = wpr:purchase_cost
	                                                shi:sale_cost            = wpr:sale_cost
	                                                shi:retail_cost          = wpr:retail_cost
	                                                shi:information          = 'WARRANTY PART REMOVED FROM JOB'
	                                                shi:notes                = 'PART EXISTS ON ORDER NUMBER: ' & wpr:order_number
	                                                if access:stohist.insert()
	                                                   access:stohist.cancelautoinc()
	                                                end
	                                            end!if access:stohist.primerecord() = level:benign
	                                        End!If access:stock.update = Level:Benign
	                    
	                                    Of 2 ! &New Location Button
	                                        glo:select1 = ''
	                                        Pick_New_Location
	                                        If glo:select1 <> ''
	                                            access:stock.clearkey(sto:location_part_description_key)
	                                            sto:location    = glo:select1
	                                            sto:part_number = wpr:part_number
	                                            sto:description = wpr:description
	                                            If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
	                                                sto:quantity_stock   += wpr:quantity
	                                                access:stock.update()
	                                                do_delete# = 1
	                                                get(stohist,0)
	                                                if access:stohist.primerecord() = level:benign
	                                                    shi:ref_number           = sto:ref_number
	                                                    access:users.clearkey(use:password_key)
	                                                    use:password              = glo:password
	                                                    access:users.fetch(use:password_key)
	                                                    shi:user                  = use:user_code    
	                                                    shi:date                 = today()
	                                                    shi:transaction_type     = 'ADD'
	                                                    shi:despatch_note_number = wpr:despatch_note_number
	                                                    shi:job_number           = job:Ref_number
	                                                    shi:quantity             = wpr:quantity
	                                                    shi:purchase_cost        = wpr:purchase_cost
	                                                    shi:sale_cost            = wpr:sale_cost
	                                                    shi:retail_cost          = wpr:retail_cost
	                                                    shi:information          = 'WARRANTY PART REMOVED FROM JOB'
	                                                    shi:notes                = 'PART EXISTS ON ORDER NUMBER: ' & wpr:order_number
	                                                    if access:stohist.insert()
	                                                       access:stohist.cancelautoinc()
	                                                    end
	                                                end!if access:stohist.primerecord() = level:benign
	                                            Else!If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
	                                                Case MessageEx('Cannot find the selected part in location: '&Clip(glo:select1)&'.<13,10><13,10>Do you wish to add it as a NEW item in that location, or scrap it?','ServiceBase 2000',|
	                                                               'Styles\warn.ico','|&Create New Item|&Scrap Part|&Cancel',3,3,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
	                                                    Of 1 ! &Create New Item Button
	                    
	                                                        glo:select2  = ''
	                                                        glo:select3  = ''
	                                                        Pick_Locations
	                                                        If glo:select1 <> ''
	                                                            do_delete# = 1
	                                                            Get(stock,0)
	                                                            If access:stock.primerecord() = Level:Benign
	                                                                sto:part_number = wpr:part_number
	                                                                sto:description = wpr:description
	                                                                sto:supplier    = wpr:supplier
	                                                                sto:purchase_cost   = wpr:purchase_cost
	                                                                sto:sale_cost   = wpr:sale_cost
	                                                                sto:shelf_location  = glo:select2
	                                                                sto:manufacturer    = job:manufacturer
	                                                                sto:location    = glo:select1
	                                                                sto:second_location = glo:select3
	                                                                If access:stock.insert() = Level:Benign
	                                                                    get(stohist,0)
	                                                                    if access:stohist.primerecord() = level:benign
	                                                                        shi:ref_number           = sto:ref_number
	                                                                        access:users.clearkey(use:password_key)
	                                                                        use:password              = glo:password
	                                                                        access:users.fetch(use:password_key)
	                                                                        shi:user                  = use:user_code    
	                                                                        shi:date                 = today()
	                                                                        shi:transaction_type     = 'ADD'
	                                                                        shi:despatch_note_number = wpr:despatch_note_number
	                                                                        shi:job_number           = job:Ref_number
	                                                                        shi:quantity             = wpr:quantity
	                                                                        shi:purchase_cost        = wpr:purchase_cost
	                                                                        shi:sale_cost            = wpr:sale_cost
	                                                                        shi:retail_cost          = wpr:retail_cost
	                                                                        shi:information          = 'WARRANTY PART REMOVED FROM JOB'
	                                                                        shi:notes                = 'PART EXISTS ON ORDER NUMBER: ' & wpr:order_number
	                                                                        if access:stohist.insert()
	                                                                           access:stohist.cancelautoinc()
	                                                                        end
	                                                                    end!if access:stohist.primerecord() = level:benign
	                                                                End!If access:stock.insert() = Level:Benign
	                                                            End!If access:stock.primerecord() = Level:Benign
	                                                        End!If glo:select1 = ''
	                                                    Of 2 ! &Scrap Part Button
	                                                        do_delete# = 1
	                                                    Of 3 ! &Cancel Button
	                                                End!Case MessageEx
	                                            End!If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
	                                        End!If glo:select1 <> ''
	                                    Of 3 ! &Cancel Button
	                                End!Case MessageEx
	                        End!Case stock_part#
	                    Of 2 ! &Scrap Part Button
	                        do_delete# = 1
                            Scrap# = 1
	                    Of 3 ! &Cancel Button
	                End!Case MessageEx
	            End!If stock_part# = 3
	        Else!If wpr:date_received <> ''
	            Case MessageEx('This part is on Order '&clip(wpr:order_number)&' but has NOT been received.<13,10><13,10>If you choose to delete it, the part on the order with be marked as received.<13,10><13,10>Are you sure you want to delete this part?','ServiceBase 2000',|
	                           'Styles\Trash.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
	                Of 1 ! &Yes Button
	                    do_delete# = 1
	                    access:ordparts.clearkey(orp:record_number_key)
	                    orp:record_number   = wpr:order_part_number
	                    If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
	                        orp:quantity    = 0
	                        orp:date_received   = Today()
	                        orp:number_received = 0
	                        orp:all_Received    = 'YES'
	                        access:ordparts.update()
	
	                        !Check if all received
	                        found# = 0
	                        Setcursor(cursor:wait)
	                        save_orp_id = access:ordparts.savefile()
	                        access:ordparts.clearkey(orp:order_Number_key)
	                        orp:order_number    = wpr:order_number
	                        Set(orp:order_number_key,orp:order_number_key)
	                        Loop
	                            If access:ordparts.next()
	                                Break
	                            End!If access:ordparts.next()
	                            If orp:order_Number <> wpr:order_Number
	                                Break
	                            End!If orp:order_Number <> wpr:order_Number
	                            If orp:all_received <> 'YES'
	                                found# = 1
	                                Break
	                            End!If orp:all_received <> 'YES'
	                        End!Loop
	                        access:ordparts.restorefile(save_orp_id)
	                        setcursor()
	
	                        If found# = 0
	                            access:orders.clearkey(ord:order_Number_key)
	                            ord:order_number    = wpr:order_number
	                            If access:orders.tryfetch(ord:order_number_key) = Level:Benign
	                                ord:all_received = 'YES'
	                                access:orders.update()
	                            End!If access:orders.tryfetch(ord:order_number_key) = Level:Benign
	                        End!If found# = 0
	                    End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
	                Of 2 ! &No Button
	            End!Case MessageEx
	        End!If wpr:date_received <> ''
	    Else!If wpr:date_ordered <> ''
	        If wpr:pending_Ref_number <> ''
	            Case MessageEx('This part has been marked to appear on the NEXT Parts Order Generation.<13,10><13,10>Are you sure you want to delete this part?','ServiceBase 2000',|
	                           'Styles\Trash.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
	                Of 1 ! &Yes Button
	                    do_delete# = 1
	                    access:ordpend.clearkey(ope:ref_number_key)
	                    ope:ref_number  = wpr:pending_ref_number
	                    If access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
	                        Delete(ordpend)
	                    End!If access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
	                Of 2 ! &No Button
	            End!Case MessageEx
	        Else!If wpr:pending_Ref_number <> ''
	            Case MessageEx('You have selected to delete this part.<13,10><13,10>Do you wish to Restock this item, or Scrap it?','ServiceBase 2000',|
	                           'Styles\Trash.ico','|&Restock Part|&Scrap Part|&Cancel',3,3,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
	                Of 1 ! &Restock Part Button
	                    Case stock_part#
	                        Of 0 Orof 2!Not originally from stock
	                            glo:select1  = ''
	                            glo:select2  = ''
	                            glo:select3  = ''
	                            Pick_Locations
	                            If glo:select1 <> ''
	                                do_delete# = 1
	                                Get(stock,0)
	                                If access:stock.primerecord() = Level:Benign
	                                    sto:part_number = wpr:part_number
	                                    sto:description = wpr:description
	                                    sto:supplier    = wpr:supplier
	                                    sto:purchase_cost   = wpr:purchase_cost
	                                    sto:sale_cost   = wpr:sale_cost
	                                    sto:shelf_location  = glo:select2
	                                    sto:manufacturer    = job:manufacturer
	                                    sto:location    = glo:select1
	                                    sto:second_location = glo:select3
	                                    If access:stock.insert() = Level:Benign
	                                        get(stohist,0)
	                                        if access:stohist.primerecord() = level:benign
	                                            shi:ref_number           = sto:ref_number
	                                            access:users.clearkey(use:password_key)
	                                            use:password              = glo:password
	                                            access:users.fetch(use:password_key)
	                                            shi:user                  = use:user_code    
	                                            shi:date                 = today()
	                                            shi:transaction_type     = 'ADD'
	                                            shi:despatch_note_number = wpr:despatch_note_number
	                                            shi:job_number           = job:Ref_number
	                                            shi:quantity             = wpr:quantity
	                                            shi:purchase_cost        = wpr:purchase_cost
	                                            shi:sale_cost            = wpr:sale_cost
	                                            shi:retail_cost          = wpr:retail_cost
	                                            shi:information          = 'WARRANTY PART REMOVED FROM JOB'
	                                            shi:notes                = ''
	                                            if access:stohist.insert()
	                                               access:stohist.cancelautoinc()
	                                            end
	                                        end!if access:stohist.primerecord() = level:benign
	                                    End!If access:stock.insert() = Level:Benign
	                                End!If access:stock.primerecord() = Level:Benign
	                            End!If glo:select1 = ''
	                        Of 1 !Originally from stock
	                            Case MessageEx('This item was originally taken from location: '&clip(sto:location)&'.<13,10><13,10>Do you wish to return it to it''s Original Location, or to a New Location?','ServiceBase 2000',|
	                                           'Styles\Trash.ico','|&Original Location|&New Location|&Cancel',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
	                                Of 1 ! &Original Location Button
	                                    do_delete# = 1
	                                    sto:quantity_stock += wpr:quantity
	                                    If access:stock.update() = Level:Benign
	                                        get(stohist,0)
	                                        if access:stohist.primerecord() = level:benign
	                                            shi:ref_number           = sto:ref_number
	                                            access:users.clearkey(use:password_key)
	                                            use:password              = glo:password
	                                            access:users.fetch(use:password_key)
	                                            shi:user                  = use:user_code    
	                                            shi:date                 = today()
	                                            shi:transaction_type     = 'ADD'
	                                            shi:despatch_note_number = wpr:despatch_note_number
	                                            shi:job_number           = job:Ref_number
	                                            shi:quantity             = wpr:quantity
	                                            shi:purchase_cost        = wpr:purchase_cost
	                                            shi:sale_cost            = wpr:sale_cost
	                                            shi:retail_cost          = wpr:retail_cost
	                                            shi:information          = 'WARRANTY PART REMOVED FROM JOB'
	                                            shi:notes                = ''
	                                            if access:stohist.insert()
	                                               access:stohist.cancelautoinc()
	                                            end
	                                        end!if access:stohist.primerecord() = level:benign
	                                    End!If access:stock.update = Level:Benign
	                
	                                Of 2 ! &New Location Button
	                                    glo:select1 = ''
	                                    Pick_New_Location
	                                    If glo:select1 <> ''
	                                        access:stock.clearkey(sto:location_part_description_key)
	                                        sto:location    = glo:select1
	                                        sto:part_number = wpr:part_number
	                                        sto:description = wpr:description
	                                        If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
	                                            sto:quantity_stock   += wpr:quantity
	                                            access:stock.update()
	                                            do_delete# = 1
	                                            get(stohist,0)
	                                            if access:stohist.primerecord() = level:benign
	                                                shi:ref_number           = sto:ref_number
	                                                access:users.clearkey(use:password_key)
	                                                use:password              = glo:password
	                                                access:users.fetch(use:password_key)
	                                                shi:user                  = use:user_code    
	                                                shi:date                 = today()
	                                                shi:transaction_type     = 'ADD'
	                                                shi:despatch_note_number = wpr:despatch_note_number
	                                                shi:job_number           = job:Ref_number
	                                                shi:quantity             = wpr:quantity
	                                                shi:purchase_cost        = wpr:purchase_cost
	                                                shi:sale_cost            = wpr:sale_cost
	                                                shi:retail_cost          = wpr:retail_cost
	                                                shi:information          = 'WARRANTY PART REMOVED FROM JOB'
	                                                shi:notes                = ''
	                                                if access:stohist.insert()
	                                                   access:stohist.cancelautoinc()
	                                                end
	                                            end!if access:stohist.primerecord() = level:benign
	                                        Else!If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
	                                            Case MessageEx('Cannot find the selected part in location: '&Clip(glo:select1)&'.<13,10><13,10>Do you wish to add it as a NEW item in that location, or scrap it?','ServiceBase 2000',|
	                                                           'Styles\warn.ico','|&Create New Item|&Scrap Part|&Cancel',3,3,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
	                                                Of 1 ! &Create New Item Button
	                
	                                                    glo:select2  = ''
	                                                    glo:select3  = ''
	                                                    Pick_Locations
	                                                    If glo:select1 <> ''
	                                                        do_delete# = 1
	                                                        Get(stock,0)
	                                                        If access:stock.primerecord() = Level:Benign
	                                                            sto:part_number = wpr:part_number
	                                                            sto:description = wpr:description
	                                                            sto:supplier    = wpr:supplier
	                                                            sto:purchase_cost   = wpr:purchase_cost
	                                                            sto:sale_cost   = wpr:sale_cost
	                                                            sto:shelf_location  = glo:select2
	                                                            sto:manufacturer    = job:manufacturer
	                                                            sto:location    = glo:select1
	                                                            sto:second_location = glo:select3
	                                                            If access:stock.insert() = Level:Benign
	                                                                get(stohist,0)
	                                                                if access:stohist.primerecord() = level:benign
	                                                                    shi:ref_number           = sto:ref_number
	                                                                    access:users.clearkey(use:password_key)
	                                                                    use:password              = glo:password
	                                                                    access:users.fetch(use:password_key)
	                                                                    shi:user                  = use:user_code    
	                                                                    shi:date                 = today()
	                                                                    shi:transaction_type     = 'ADD'
	                                                                    shi:despatch_note_number = wpr:despatch_note_number
	                                                                    shi:job_number           = job:Ref_number
	                                                                    shi:quantity             = wpr:quantity
	                                                                    shi:purchase_cost        = wpr:purchase_cost
	                                                                    shi:sale_cost            = wpr:sale_cost
	                                                                    shi:retail_cost          = wpr:retail_cost
	                                                                    shi:information          = 'WARRANTY PART REMOVED FROM JOB'
	                                                                    shi:notes                = ''
	                                                                    if access:stohist.insert()
	                                                                       access:stohist.cancelautoinc()
	                                                                    end
	                                                                end!if access:stohist.primerecord() = level:benign
	                                                            End!If access:stock.insert() = Level:Benign
	                                                        End!If access:stock.primerecord() = Level:Benign
	                                                    End!If glo:select1 = ''
	                                                Of 2 ! &Scrap Part Button
	                                                    do_delete# = 1
	                                                Of 3 ! &Cancel Button
	                                            End!Case MessageEx
	                                        End!If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
	                                    End!If glo:select1 <> ''
	                                Of 3 ! &Cancel Button
	                            End!Case MessageEx
	                    End!Case stock_part#
	                Of 2 ! &Scrap Part Button
	                    do_delete# = 1
                        Scrap#  = 1
	                Of 3 ! &Cancel Button
	            End!Case MessageEx
	        End!If wpr:pending_Ref_number <> ''
	    End!If wpr:date_ordered <> ''
    End!If wpr:adjustment = 'YES'

    If Scrap# = 1
        If Access:AUDIT.PrimeRecord() = Level:Benign
            aud:Notes         = 'DESCRIPTION: ' & Clip(wpr:Description)
            aud:Ref_Number    = job:ref_number
            aud:Date          = Today()
            aud:Time          = Clock()
            aud:Type          = 'JOB'
            Access:USERS.ClearKey(use:Password_Key)
            use:Password      = glo:Password
            Access:USERS.Fetch(use:Password_Key)
            aud:User          = use:User_Code
            aud:Action        = 'SCRAP WARRANTY PART: ' & Clip(wpr:Part_Number)
            Access:AUDIT.Insert()
        End!If Access:AUDIT.PrimeRecord() = Level:Benign
    End !If Scrap# = 1
