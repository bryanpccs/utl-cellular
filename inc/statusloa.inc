	!status_number# is the number of the status
	!status_audit# is whether an entry is written in the audi trail
	
	previous_status"	= job:loan_status
	access:status.clearkey(sts:ref_number_only_key)
	sts:ref_number	= status_number#
	IF access:status.tryfetch(sts:ref_number_only_key)	
		job:loan_status	= 'ERROR'
	Else!IF access:status.tryfetch(sts:ref_number_only_key)
		job:loan_status	= sts:status
		If previous_status" <> job:loan_status
			If status_audit# = 0
			    Get(audit,0)
				If access:audit.primerecord() = Level:Benign			
					aud:notes	= 'PREVIOUS STATUS: ' & Clip(previous_status") & |
									'<13,10>NEW STATUS: ' & Clip(job:loan_status)
					aud:ref_number	= job:ref_number
					aud:date		= Today()
					aud:time		= Clock()
					aud:user		= use:user_code
					aud:type		= 'LOA'
					aud:action		= 'STATUS (LOAN) CHANGED TO: ' & CLip(job:loan_status)
					access:audit.tryinsert()								
				End!If access:audit.primerecord() = Level:Benign
			End!If status_audit# = 1
		End!If job:PreviousStatus <> job:Current_Status		
				
	End!IF access:status.tryfetch(sts:ref_number_only_key)
	
	
