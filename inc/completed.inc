    Set(defaults)
    access:defaults.next()
    
    exchange_update# = 0
    completed# = 0
    glo:errortext = ''
    If job:date_completed = ''
        CompulsoryFieldCheck('C')
        If glo:ErrorText <> ''
            glo:errortext = 'You cannot complete this job due to the following error(s): <13,10>' & Clip(glo:errortext)
            Error_Text
            glo:errortext = ''
            !thismakeover.SetWindow(win:form)
        Else
        
            found_requested# = 0
            found_ordered# = 0
            found_received# = 0
            setcursor(cursor:wait)
            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = job:ref_number
            set(wpr:part_number_key,wpr:part_number_key)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:ref_number  <> job:ref_number      |
                    then break.  ! end if
                If wpr:pending_ref_number <> ''
                    found_requested# = 1
                    Break
                End                                     
                If wpr:order_number <> '' And wpr:date_received = ''
                    found_ordered# = 1
                    Break
                End
                found_received# = 1                 
            end !loop
            access:warparts.restorefile(save_wpr_id)
            setcursor() 

            setcursor(cursor:wait)
            
            save_par_id = access:parts.savefile()
            access:parts.clearkey(par:part_number_key)
            par:ref_number  = job:ref_number
            set(par:part_number_key,par:part_number_key)
            loop
                if access:parts.next()
                   break
                end !if
                if par:ref_number  <> job:ref_number      |
                    then break.  ! end if
                If par:pending_ref_number <> ''
                    found_requested# = 1
                    Break
                End                                     
                If par:order_number <> '' And par:date_received = ''
                    found_ordered# = 1
                    Break
                End
                found_received# = 1                 
            end !loop
            access:parts.restorefile(save_par_id)
            setcursor()

            If found_requested# = 1
                error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>There are Unordered spares attached'                                
            Elsif found_ordered# = 1
                error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>There are Unreceived spares attached'                                
            End
            
            
            If error# = 0
                If date_error_temp = 1
                    date_error_temp = 0
                Else!If date_error_temp = 1
                    exchange_update# = 1
                    completed# = 1
                    
                    If def:qa_required = 'YES' And def:qa_before_complete <> 'YES'
                        Status_Routine(605,job:current_status,end_date",end_time",thiswindow.request)
                    Else
                        Do Check_For_Despatch
                        If job:loan_unit_number <> ''
                            Status_routine(811,job:current_status,end_Date",end_time",thiswindow.request)
                        Else!If job:loan_unit_number <> ''
                            If job:exchange_unit_number <> ''   
                                Status_Routine(707,job:current_status,end_date",end_time",thiswindow.request)                           
                            Else!If job:exchange_unit_number <> ''
                                Status_Routine(705,job:current_status,end_date",end_time",thiswindow.request)
                            End!If job:exchange_unit_number <> ''
                        End!If job:loan_unit_number <> ''
                                
                    End
                    job:status_end_date = end_date"
                    job:status_end_time = end_time"
                End !If date_error_temp = 1
            Else
                job:date_completed = ''
                job:time_completed = ''
                job:completed = 'NO'
            End!If error# = 0
        End !If error# = 0
    Else!If date_completed_temp = ''
        Case MessageEx('Are you sure you want to change the completion date?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                If date_error_temp = 1
                    date_error_temp = 0
                Else!If date_error_temp = 1
                    exchange_update# = 1
                    completed# = 1
                    If job:loan_unit_number <> ''
                        GetStatus(811,0,'JOB') ! Ready To Despatch Loan
                    Else!If job:loan_unit_number <> ''
                        If ToBeExchanged()
                            GetStatus(707,0,'JOB') !Return To Exchange Stock
                        Else!If ToBeExchange()
                            GetStatus(705,0,'JOB') !Completed
                        End!If ToBeExchange()
                    End!!If job:loan_unit_number <> ''
                    job:status_end_date = end_date"
                    job:status_end_time = end_time"
                    If def:qa_required <> 'YES'
                        Do Check_For_Despatch
                    End!If def:qa_required <> 'YES'
                end!If date_error_temp = 1
                Display()
            Of 2 ! &No Button
                job:date_completed = date_completed_temp
                job:time_completed = time_completed_temp
                job:completed = 'YES'
                Display()
        End!Case MessageEx
    End !If date_completed_temp = ''
    check_for_bouncers_temp = 0
    If completed# = 1
        print_despatch_note_temp = ''
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key) 
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                If tra:excludebouncer
                    check_for_bouncers_temp = 0                                
                Else!If tra:excludebouncer
                    If ToBeLoaned() = Level:Benign
                        check_for_bouncers_temp = 1                        
                    End!If ToBeLoaned() = Level:Benign
                End!If tra:excludebouncer
!               if tra:use_sub_accounts = 'YES'
!                   print_Despatch_note_temp = sub:print_despatch_complete
!               else!if tra:use_sub_accounts = 'YES'
                    print_despatch_note_temp = tra:print_despatch_complete
!               end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign     
        
        saved_ref_number_temp   = job:ref_number
        saved_esn_temp  = job:esn
        saved_msn_temp  = job:msn
        job:date_completed = Today()
        job:time_completed = Clock()
        date_completed_temp = job:date_completed
        time_completed_temp = job:time_completed
        job:completed = 'YES'
        If job:edi = 'EDI'
            access:chartype.clearkey(cha:charge_type_key)
            cha:charge_type = job:warranty_charge_type
            if access:chartype.fetch(cha:charge_type_key)
                job:edi = 'NO'
            Else!if access:chartype.fetch(cha:charge_type_key)
                If cha:exclude_edi <> 'YES'
                    job:edi = 'NO'
                End
            end!if access:chartype.fetch(cha:charge_type_key)
        End!If job:edi = 'EDI'
        
        access:exchange_alias.clearkey(xch_ali:ref_number_key)
        xch_ali:ref_number  = job:exchange_unit_number
        If access:Exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign
            If xch_ali:audit_number <> ''                                               !If this has an audit number
                access:excaudit.clearkey(exa:audit_number_key)                          !Mark the replacement as ready
                exa:stock_type  = xch_ali:stock_type                                    !To be returned to stock
                exa:audit_number    = xch_ali:audit_number
                If access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number  = exa:stock_unit_number
                    If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                        get(exchhist,0)
                        if access:exchhist.primerecord() = level:benign
                            exh:ref_number   = xch:ref_number
                            exh:date          = today()
                            exh:time          = clock()
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            exh:user = use:user_code
                            exh:status        = 'REMOVED FROM AUDIT NUMBER: ' & Clip(xch:audit_number)
                            access:exchhist.insert()
                        end!if access:exchhist.primerecord() = level:benign
                        xch:audit_number    = ''
                        access:exchange.update()
                    End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number  = exa:replacement_unit_number
                    If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                        get(exchhist,0)
                        if access:exchhist.primerecord() = level:benign
                            exh:ref_number   = xch:ref_number
                            exh:date          = today()
                            exh:time          = clock()
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            exh:user = use:user_code
                            exh:status        = 'UNIT REPAIRED. (AUDIT:' &|
                                                Clip(exa:audit_number) & ')'
                            access:exchhist.insert()
                        end!if access:exchhist.primerecord() = level:benign
                        xch:audit_number    = exa:audit_number
                        xch:available   = 'RTS'
                        access:exchange.update()
                    End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                End!If access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
            Else!If xch_ali:audit_number <> ''
                access:exchange.clearkey(xch:esn_available_key)
                xch:available  = 'REP'
                xch:stock_type = xch_ali:stock_type
                xch:esn        = job:esn
                if access:exchange.tryfetch(xch:esn_available_key) = Level:Benign
                    get(exchhist,0)
                    if access:exchhist.primerecord() = level:benign
                        exh:ref_number   = xch:ref_number
                        exh:date          = today()
                        exh:time          = clock()
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        exh:user = use:user_code
                        exh:status        = 'REPAIR COMPLETED'
                        access:exchhist.insert()
                    end!if access:exchhist.primerecord() = level:benign
                    xch:available = 'RTS'
                    access:exchange.update()
                end!if access:exchange.tryfetch(xch:esn_available_key)
            End!If xch:audit_number <> ''
        End!If access:Exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign

    End!If completed# = 1
    Do QA_Group

!START Removing Exchange Unit From Stock, and putting replacement unit as stock item.
