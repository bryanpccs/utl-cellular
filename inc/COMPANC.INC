        normal_despatch# = 1
        restock# = 0
        IF job:exchange_unit_number <> ''
        	normal_despatch# = 0
        	restock# = 1
		Else!IF job:exchange_unit_number <> ''        		
			If Sub(job:exchange_status,1,3) = '108'
				normal_despatch# = 0
				restock# = 1
			End!If Sub(job:exchange_status,1,3) = '108'
            access:trantype.clearkey(trt:transit_type_key)          !In case hasn't been attached yet!
            trt:transit_type = job:transit_type
            if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                If trt:Exchange_Unit = 'YES'    
                    normal_despatch# = 0
                    restock# = 1
                End!If trt:Exchange_Unit = 'YES'
            end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
		End!!IF job:exchange_unit_number <> ''        		
		
        If normal_despatch# = 0 and job:loan_unit_number <> ''
            Case MessageEx('A Loan Unit has been issued to this job but the Initial Transit Type states that an Exchange Unit is required.<13,10><13,10>Do you wish to Restock This Unit, or mark it for Despatch back to the customer?','ServiceBase 2000',|
                           'Styles\question.ico','|&Restock Unit|&Despatch Back',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                Of 1 ! &Restock Unit Button
                    normal_despatch# = 0
                    restock# = 1
                Of 2 ! &Despatch Back Button
                    normal_despatch# = 1
            End!Case MessageEx              
        End!If normal_despatch# = 0 and job:loan_unit_number <> ''
                
		If normal_despatch# = 1

            access:courier.clearkey(cou:courier_key)
            cou:courier = job:courier
            if access:courier.tryfetch(cou:courier_key) = Level:Benign
                If cou:courier_type = 'ANC'
                    Case Today() % 7
                        Of 6 !Saturday
                            If def:include_saturday <> 'YES' And def:include_sunday <> 'YES'
                                job:date_despatched = Today() + 2
                            End!If def:include_saturday <> 'YES' And def:include_sunday <> 'YES'
                            If def:include_saturday <> 'YES' and def:include_sunday = 'YES'
                                job:date_despatched = Today() + 1
                            End!If def:include_saturday <> 'YES' and def:include_sunday = 'YES'
                            IF def:include_saturday = 'YES' and def:include_sunday <> 'YES'
                                If clock() > cou:last_despatch_time
                                    job:date_despatched = Today() + 2
                                Else!If clock() > cou:last_despatch_time
                                    job:date_despatched = Today()
                                End!If clock() > cou:last_despatch_time
                            End!IF def:include_saturday = 'YES' and def:include_sunday <> 'YES'
                            If def:include_saturday = 'YES' and def:include_sunday = 'YES'
                                If clock() > cou:last_despatch_time
                                    job:date_despatched = Today() + 1
                                Else!If clock() > cou:last_despatch_time
                                    job:date_despatched = Today()
                                End!If clock() > cou:last_despatch_time
                            End!If def:include_saturday = 'YES' and def:include_sunday = 'YES'
                        Of 0 !Sunday
                            If def:include_sunday = 'YES'
                                If clock() > cou:last_despatch_time
                                    job:date_despatched = Today() + 1
                                Else!If clock() > cou:last_despatch_time
                                    job:date_despatched = Today()
                                End!If clock() > cou:last_despatch_time
                            Else!If def:include_sunday = 'YES'
                                job:date_despatched = Today() + 1
                            End!If def:include_sunday = 'YES'
                        Of 5 !Friday
                            If def:include_saturday <> 'YES' And def:include_sunday <> 'YES'
                                If clock() > cou:last_despatch_time
                                    job:date_despatched = Today() + 3
                                Else!If clock() > cou:last_despatch_time
                                    job:date_despatched = Today()
                                End!If clock() > cou:last_despatch_time
                            End!If def:include_saturday <> 'YES' And def:include_sunday <> 'YES'
                            If def:include_saturday <> 'YES' and def:include_sunday = 'YES'
                                If clock() > cou:last_despatch_time
                                    job:date_despatched = Today() + 2
                                Else!If clock() > cou:last_despatch_time
                                    job:date_despatched = Today()
                                End!If clock() > cou:last_despatch_time
                            End!If def:include_saturday <> 'YES' and def:include_sunday = 'YES'
                            IF def:include_saturday = 'YES' 
                                If clock() > cou:last_despatch_time
                                    job:date_despatched = Today() + 1
                                Else!If clock() > cou:last_despatch_time
                                    job:date_despatched = Today()
                                End!If clock() > cou:last_despatch_time
                            End!IF def:include_saturday = 'YES' and def:include_sunday <> 'YES'
                        Else
                            If clock() > cou:last_despatch_time
                                job:date_despatched = Today() + 1
                            Else!If clock() > cou:last_despatch_time
                                job:date_despatched = Today()
                            End!If clock() > cou:last_despatch_time
                    End!Case Today() % 7
                End!If cou:courier_type = 'ANC'
            end!if access:courier.tryfetch(cou:courier_key) = Level:Benign

			access:subtracc.clearkey(sub:account_number_key)
			sub:account_number = job:account_number
			if access:subtracc.fetch(sub:account_number_key) = level:benign
				access:tradeacc.clearkey(tra:account_number_key) 
				tra:account_number = sub:main_account_number
				if access:tradeacc.fetch(tra:account_number_key) = level:benign
					If tra:IgnoreDespatch = 'YES'
						ignore_despatch# = 1
					End!If tra:IgnoreDespatch = 'YES'
				end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
			end!if access:subtracc.fetch(sub:account_number_key) = level:benign		            
        	job:despatched = 'REA'
        	job:despatch_type = 'JOB'
        	job:current_courier = job:courier
Compile('***',Debug=1)
	message('Not Stock Bit: despatched: ' & job:despatched,'Debug Message', icon:exclamation)
***		
        	
		Else		!If normal_despatch# = 1
			If restock# = 1
				!job:despatched = 'RTS'						
			Else!If restock# = 1
				job:despatched = 'YES'
			End!If restock# = 1
Compile('***',Debug=1)
	message('despatched: ' & job:despatched,'Debug Message', icon:exclamation)
***		
			
			job:date_despatched = Today()
			job:consignment_number = 'N/A'
			access:users.clearkey(use:password_key)
			use:password	= glo:password
			access:users.tryfetch(use:password_key)
			job:despatch_user = use:user_code
			get(desbatch,0)
			if access:desbatch.primerecord() =  Level:Benign
				if access:desbatch.insert()
				   access:desbatch.cancelautoinc()
				end										
			end!if access:desbatch.primerecord() =  Level:Benign
			job:despatch_number = dbt:batch_number
        End!If normal_despatch# = 1
		