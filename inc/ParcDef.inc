ParcelLineExport    File,Driver('ASCII'),Pre(Parcel),Name(tmp:ParcelLineName),CREATE,THREAD,BINDABLE
Record                  Record
OrderNO                 String(25)
Labels                  String(3)
Service                 String(2)
CompanyName             String(35)
AddressLine1            String(35)
AddressLine2            String(35)
Town                    String(35)
County                  String(35)
Postcode                String(8)
Instructions            String(25)
Instructions2           String(25)
AccountCode             String(10)
Type                    String(1)
Workstation             String(2)
                        End
                    End

! Start Change 3813 BE(19/01/04)
ToteParcelLineExport    File,Driver('ASCII'),Pre(TPL),Name(tmp:ParcelLineName),CREATE,THREAD,BINDABLE
Record                  Record
OrderNO                 String(25)
Labels                  String(3)
Service                 String(2)
CompanyName             String(35)
AddressLine1            String(35)
AddressLine2            String(35)
Town                    String(35)
County                  String(35)
Postcode                String(8)
Instructions            String(25)
Instructions2           String(25)
AccountCode             String(10)
Type                    String(1)
Workstation             String(2)
ConsignmentNo           String(23)
                        End
                    End
! End Change 3813 BE(19/01/04)