SECTION('Job')
    job:consignment_number  = tmp:ConsignNo
    If Today() %7 = 6
        thedate#    = Today() + 2
    Else
        thedate#    = Today()                
    End!If Today() %7 = 6            
    job:date_despatched = thedate#
    job:despatch_number = dbt:Batch_Number
    job:despatched  = 'YES'
    access:users.clearkey(use:password_key)
    use:password    = glo:password
    access:users.fetch(use:password_key)
    job:despatch_user   = use:user_code

    Access:COURIER.ClearKey(cou:Courier_Key)
    cou:Courier = job:Courier
    If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
        If cou:CustomerCollection = 1         
            If cou:NewStatus <> ''
                GetStatus(Sub(cou:NewStatus,1,3),1,'JOB')                            
            End!If cou:NewStatus <> ''
        Else!If cou:CustomerCollection = 1
            If job:Paid = 'YES'            
                GetStatus(910,1,'JOB') !Despatch Paid
            Else!If job:Paid = 'YES'
                GetStatus(905,1,'JOB') !Despatch Unpaid
            End!If job:Paid = 'YES'
        End!If cou:CustomerCollection = 1
    End!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign

    If def:RemoveWorkshopDespatch = 1        
        job:Workshop = 'NO'
        job:Location = 'DESPATCHED'
    End!If def:RemoveWorkshopDespatch = 1
        
    access:jobs.update()

    get(audit,0)
    if access:audit.primerecord() = level:benign
        aud:notes         = 'CONSIGNMENT NUMBER: ' & Clip(job:Consignment_number)
        aud:ref_number    = job:ref_number
        aud:date          = today()
        aud:time          = clock()
        aud:type          = 'JOB'
        access:users.clearkey(use:password_key)
        use:password = glo:password
        access:users.fetch(use:password_key)
        aud:user = use:user_code
        aud:action        = 'JOB DESPATCHED VIA ' & Clip(job:courier)
        access:audit.insert()
    end!�if access:audit.primerecord() = level:benign

    !Return Location
    access:locinter.clearkey(loi:location_key)
    loi:location    = job:location
    If access:locinter.fetch(loi:location_key) = Level:Benign
        If loi:allocate_spaces  = 'YES'
            loi:current_spaces  += 1
            loi:location_available  = 'YES'
            access:locinter.update()
        End!If loi:allocate_spaces  = 'YES'
    End!If access:locinter.fetch(loi:location_key) = Level:Benign

    print_despatch# = 0
    If tra:use_sub_accounts = 'YES'
        If sub:print_despatch_despatch  = 'YES'
            If multi# = 1
                If sub:Despatch_Note_Per_Item = 'YES'                                
                    print_Despatch# = 1
                End !If sub:Despatch_Note_Per_Item = 'YES'                                
            Else !If multi# = 1
                print_despatch# = 1    
            End !If multi# = 1
            
        End!If sub:print_despatch_despatch  = 'YES'
    Else!If tra:use_sub_accounts = 'YES'
        If tra:print_despatch_despatch  = 'YES'
            If multi# = 1
                If tra:Despatch_Note_Per_Item = 'YES'                                
                    print_Despatch# = 1
                End !If sub:Despatch_Note_Per_Item = 'YES'                                
            Else !If multi# = 1
                print_despatch# = 1    
            End !If multi# = 1        
        End!If tra:print_despatch_despatch  = 'YES'
    End!If tra:use_sub_accounts = 'YES'
    If print_despatch# = 1
        glo:select1  = job:ref_number
        If cou:CustomerCollection = 1
            glo:Select2 = cou:NoOfDespatchNotes
        Else!If cou:CustomerCollection = 1
            glo:Select2 = 1
        End!If cou:CustomerCollection = 1
        setcursor()
        Despatch_Note
        setcursor(cursor:wait)
        glo:select1 = ''
        glo:Select2 = ''
    End!If print_despatch# = 1

SECTION('Exchange')
	job:exchange_consignment_number	= tmp:ConsignNo
	job:exchange_despatched	= Today()
	job:exchange_despatch_number	= dbt:batch_number	
	job:despatched	= ''
	access:users.clearkey(use:password_key)
	use:password	= glo:password
	access:users.tryfetch(use:password_key)
	job:exchange_despatched_user	= use:user_code
	If job:workshop	<> 'YES' and job:third_party_site = ''
		GetStatus(116,1,'JOB')
	End!If job:workshop	<> 'YES'
	GetStatus(901,1,'EXC')

	access:jobs.update()	
	get(audit,0)
	if access:audit.primerecord() = level:benign
	    aud:notes         = 'CONSIGNMENT NOTE NUMBER: ' & Clip(job:exchange_consignment_number)
	    aud:ref_number    = job:ref_number
	    aud:date          = today()
	    aud:time          = clock()
	    aud:type          = 'EXC'
	    access:users.clearkey(use:password_key)
	    use:password = glo:password
	    access:users.fetch(use:password_key)
	    aud:user = use:user_code
	    aud:action        = 'EXCHANGE UNIT DESPATCHED VIA ' & CLip(job:exchange_courier)
	    access:audit.insert()
	end!�if access:audit.primerecord() = level:benign

	access:exchange.clearkey(xch:ref_number_key)
	xch:ref_number	= job:exchange_unit_number
	If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
		xch:available	= 'DES'
		access:exchange.update()
	
		get(exchhist,0)
		if access:exchhist.primerecord() = level:benign
		    exh:ref_number   = xch:ref_number
		    exh:date          = today()
		    exh:time          = clock()
		    access:users.clearkey(use:password_key)
		    use:password = glo:password
		    access:users.fetch(use:password_key)
		    exh:user = use:user_code
		    exh:status        = 'UNIT DESPATCHED ON JOB: ' & Clip(job:ref_number)
		    access:exchhist.insert()
		end!if access:exchhist.primerecord() = level:benign
	End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
    print_despatch# = 0
    If tra:use_sub_accounts = 'YES'
        If sub:print_despatch_despatch  = 'YES'
            print_despatch# = 1
        End!If sub:print_despatch_despatch  = 'YES'
    Else!If tra:use_sub_accounts = 'YES'
        If tra:print_despatch_despatch  = 'YES'
            print_despatch# = 1
        End!If tra:print_despatch_despatch  = 'YES'
    End!If tra:use_sub_accounts = 'YES'
    If print_despatch# = 1
        glo:select1  = job:ref_number
        setcursor()
        Despatch_Note
        setcursor(cursor:wait)
        glo:select1  = ''
    End!If print_despatch# = 1


SECTION('Loan')
	job:loan_consignment_number	= tmp:ConsignNo
	job:loan_despatched	= Today()
	job:loan_despatch_number	= dbt:batch_number
	job:despatched	= ''
	access:users.clearkey(use:password_key)
	use:password	= glo:password
	access:users.tryfetch(use:password_key)
	job:loan_despatched_user	= use:user_code
	If job:workshop <> 'YES' and job:third_party_site	= ''		
		GetStatus(117,1,'JOB')

	End!If job:workshop <> 'YES' and job:third_party_site	= ''
	GetStatus(901,1,'LOA')

	access:jobs.update()	
	get(audit,0)
	if access:audit.primerecord() = level:benign
	    aud:notes         = 'CONSIGNMENT NOTE NUMBER: ' & Clip(job:loan_consignment_number)
	    aud:ref_number    = job:ref_number
	    aud:date          = today()
	    aud:time          = clock()
	    aud:type          = 'LOA'
	    access:users.clearkey(use:password_key)
	    use:password = glo:password
	    access:users.fetch(use:password_key)
	    aud:user = use:user_code
	    aud:action        = 'LOAN UNIT DESPATCHED VIA ' & CLip(job:loan_courier)
	    access:audit.insert()
	end!�if access:audit.primerecord() = level:benign
	
	access:loan.clearkey(loa:ref_number_key)
	loa:ref_number	= job:loan_unit_number
	If access:loan.tryfetch(loa:ref_number_key)	= Level:Benign
		loa:available	= 'DES'
		access:loan.update()
		get(loanhist,0)
		if access:loanhist.primerecord() = level:benign
		    loh:ref_number    = loa:ref_number
		    loh:date          = today()
		    loh:time          = clock()
		    access:users.clearkey(use:password_key)
		    use:password = glo:password
		    access:users.fetch(use:password_key)
		    loh:user = use:user_code
		    loh:status        = 'UNIT DESPATCHED ON JOB: ' & CLip(job:ref_number)
		    access:loanhist.insert()
		end!if access:loanhist.primerecord() = level:benign
	
	End!	If access:loan.tryfetch(loa:ref_number_key)	= Level:Benign
    print_despatch# = 0
    If tra:use_sub_accounts = 'YES'
        If sub:print_despatch_despatch  = 'YES'
            print_despatch# = 1
        End!If sub:print_despatch_despatch  = 'YES'
    Else!If tra:use_sub_accounts = 'YES'
        If tra:print_despatch_despatch  = 'YES'
            print_despatch# = 1
        End!If tra:print_despatch_despatch  = 'YES'
    End!If tra:use_sub_accounts = 'YES'
    If print_despatch# = 1
        glo:select1  = job:ref_number
        setcursor()
        Despatch_Note
        setcursor(cursor:wait)
        glo:select1  = ''
    End!If print_despatch# = 1
