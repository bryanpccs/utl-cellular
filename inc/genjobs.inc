SECTION('ChargeableJobAccepted')
    If ~0{prop:acceptall}
        If job:chargeable_job <> chargeable_job_temp
            If job:chargeable_job = 'YES'
                If job:warranty_job = 'YES'
                    Case MessageEx('This job has already been marked as a Warranty Job.<13,10><13,10>This will change it to a split Warranty / Chargeable job.<13,10>Are you sure?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Yes Button
                            chargeable_job_temp  = job:chargeable_job
                        Of 2 ! &No Button
                            job:chargeable_job  = 'NO'
                    End!Case MessageEx
                Else !!If job:warranty_job = 'YES'
                    chargeable_job_temp = job:chargeable_job
                End !If job:warranty_job = 'YES'

            Else !If job:chargeable_job = 'YES'

                found# = 0
                setcursor(cursor:wait)
                save_par_id = access:parts.savefile()
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = job:ref_number
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                       break
                    end !if
                    if par:ref_number  <> job:ref_number      |
                        then break.  ! end if
                    found# = 1
                    Break
                end !loop
                access:parts.restorefile(save_par_id)
                setcursor()

                If job:estimate = 'YES'
                    found# = 2
                End

                Case found#
                    Of 1    !Parts
                        Case MessageEx('Chargeable parts have been added to this job.<13,10><13,10>You must remove these before you can mark this job as Warranty Only.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        job:chargeable_job  = chargeable_job_temp

                    Of 2    !Estimate
                        Case MessageEx('This job has been marked as an Estimate.<13,10><13,10>You cannot make this a Warranty Only job.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        job:chargeable_job  = chargeable_job_temp
                Else !Case found#
                    chargeable_job_temp = job:chargeable_job
                End !If found# <> 0
            End !If job:chargeable_job = 'YES'
        End !If job:chargeable_job <> chargeable_job_temp
    End !If ~0{prop:acceptall}
    Post(Event:accepted,?job:charge_type)


SECTION('WarrantyJobAccepted')
    If ~0{prop:acceptall}
        If job:warranty_job <> warranty_job_temp
            If job:warranty_job = 'YES'
                error# = 0
                If job:chargeable_job = 'YES'
                    Case MessageEx('This job has already been marked as a Chargeable Job.<13,10><13,10>This will change it to a split Warranty / Chargeable job.<13,10>Are you sure?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Yes Button
                            warranty_job_temp  = job:warranty_job
                        Of 2 ! &No Button
                            job:warranty_job  = 'NO'
                            error# = 1
                    End!Case MessageEx
                Else !!If job:chargeable_job = 'YES'
                    warranty_job_temp = job:warranty_job
                End !If job:chargeable_job = 'YES'
                charge_type"    = ''
                found# = 0
                save_cha_id = access:chartype.savefile()
                access:chartype.clearkey(cha:warranty_key)
                cha:warranty    = 'YES'
                set(cha:warranty_key,cha:warranty_key)
                loop
                    if access:chartype.next()
                       break
                    end !if
                    if cha:warranty    <> 'YES'      |
                        then break.  ! end if
                    found# += 1
                    charge_type"    = cha:charge_type
                    If found# > 1
                        Break
                    End!If found# > 1

                end !loop
                access:chartype.restorefile(save_cha_id)
                If found# = 1
                    job:warranty_Charge_type = charge_type"
                End!If found# = 1


            Else !If job:chargeable_job = 'YES'

                found# = 0
                setcursor(cursor:wait)
                save_wpr_id = access:warparts.savefile()
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = job:ref_number
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> job:ref_number      |
                        then break.  ! end if
                    found# = 1
                    Break
                end !loop
                access:warparts.restorefile(save_wpr_id)
                setcursor()

                If found# <> 0
                    Case MessageEx('Warranty parts have been added to this job.<13,10><13,10>You must remove these before you can mark this job as Chargeable Only.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx

                    job:warranty_job  = warranty_job_temp
                Else !!If found# <> 0
                    warranty_job_temp   = job:warranty_job
                End !If found# <> 0
            End !If job:warranty_job = 'YES'
        End !If job:warranty_job <> warranty_job_temp
    End !If ~0{prop:acceptall}
    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = job:Model_Number
    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Found
        job:EDI = PendingJob(mod:Manufacturer)
        job:Manufacturer    = mod:Manufacturer
    Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

SECTION('ChargeableAdjustment')
	Case MessageEx('Are you sure you want to add a Chargeable Adjustment?','ServiceBase 2000',|
	             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
	  Of 1 ! &Yes Button
	    get(parts,0)
	    if access:parts.primerecord() = level:benign
	        par:ref_number            = job:ref_number
	        par:adjustment            = 'YES'
	        par:part_number           = 'ADJUSTMENT'
	        par:description           = 'ADJUSTMENT'
	        par:quantity              = 1
	        par:warranty_part         = 'NO'
	        par:exclude_from_order    = 'YES'
	        if access:parts.insert()
	            access:parts.cancelautoinc()
	        end
	    end!if access:parts.primerecord() = level:benign
	  Of 2 ! &No Button
	End!Case MessageEx

SECTION('WarrantyAdjustment')
	Case MessageEx('Are you sure you want to add a Warranty Adjustment?','ServiceBase 2000',|
	           'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
	Of 1 ! &Yes Button
	    found# = 0
	    access:manufact.clearkey(man:manufacturer_key)
	    man:manufacturer    = job:manufacturer
	    If access:manufact.tryfetch(man:manufacturer_key) = Level:benign
	        If man:includeadjustment = 'YES' and man:adjustpart = 1
	            found# = 1
	        End!If job:skip_adjustment <> 'YES'
	    End!If access:manufacturer.tryfetch(man:manufacturer_key)
	    If found# = 0
	        access:chartype.clearkey(cha:charge_type_key)
	        cha:charge_type = job:warranty_charge_type
	        if access:chartype.fetch(cha:charge_type_key) = Level:Benign
	            If cha:force_warranty = 'YES'
	                setcursor(cursor:wait)
	                save_map_id = access:manfaupa.savefile()
	                access:manfaupa.clearkey(map:field_number_key)
	                map:manufacturer = job:manufacturer
	                set(map:field_number_key,map:field_number_key)
	                loop
	                    if access:manfaupa.next()
	                       break
	                    end !if
	                    if map:manufacturer <> job:manufacturer      |
	                        then break.  ! end if
	                    if map:compulsory = 'YES'
	                        found# = 1
	                        Break
	                    End
	                end !loop
	                access:manfaupa.restorefile(save_map_id)
	                setcursor()
	            End!If cha:force_warranty = 'YES'
	        end!if access:chartype.fetch(cha:charge_type_key) = Level:Benign
	    End!If found# = 0
	    If found# = 1
	        Clear(wpr:Record)
	        Globalrequest = Insertrecord
	        glo:select1 = 'ADJUSTMENT'
	        glo:select2 = job:ref_number
	        Update_Warranty_Part
	        glo:select1 = ''
	        glo:select2 = ''
	    End!If found# = 1
	    If found# = 0
	        get(warparts,0)
	        if access:warparts.primerecord() = level:benign
	            wpr:ref_number           = job:ref_number
	            wpr:adjustment           = 'YES'
	            wpr:part_number          = 'ADJUSTMENT'
	            wpr:description          = 'ADJUSTMENT'
	            wpr:quantity             = 1
	            wpr:warranty_part        = 'YES'
	            wpr:exclude_from_order   = 'YES'
	            if access:warparts.insert()
	                access:warparts.cancelautoinc()
	            end
	        end!if access:warparts.primerecord() = level:benign
	    End!!If found# = 0
	Of 2 ! &No Button
	End!Case MessageEx
