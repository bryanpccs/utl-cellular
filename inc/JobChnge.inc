SECTION('Save')
!SaveGroup          group,Pre(sav)
!sav:ChargeType             String(30)
!sav:WarrantyChargeType     String(30)
!sav:RepairType             String(30)
!sav:WarrantyRepairType     String(30)
!                   End

!ChangedGroup       group,Pre(sav1)
!sav1:cct                   Byte
!sav1:wct                   Byte
!sav1:crt                   Byte
!sav1:wrt                   Byte
!                   End             

    sav:ChargeType          = job:Charge_Type
    sav:WarrantyChargeType  = job:Warranty_Charge_Type
    sav:RepairType          = job:Repair_Type
    sav:WarrantyRepairType  = job:Repair_Type_Warranty
    sav:ChargeableJob       = job:Chargeable_Job
    sav:WarrantyJob         = job:Warranty_Job

SECTION('Audit')
    If thiswindow.request <> Insertrecord
        Clear(ChangedGroup)
        If sav:ChargeType <> job:charge_type
            sav1:cct = 1
        End!If sav:ChargeType <> job:charge_type
        If sav:WarrantyChargeType <> job:warranty_Charge_type
            sav1:wct = 1
        End!If sav:WarrantyChargeType <> job:warranty_Charge_type
        If sav:RepairType <> job:Repair_Type
            sav1:crt = 1
        End!If sav:RepairType <> job:Repair_Type
        If sav:WarrantyRepairType   <> job:Repair_Type_Warranty
            sav1:wrt = 1
        End!If sav:WarrantyRepairType   = job:Repair_Type_Warranty
        If sav:ChargeableJob <> job:Chargeable_Job
            sav1:chj = 1
        End!If sav:ChargeableJob <> job:Chargeable_Job
        If sav:WarrantyJob <> job:Warranty_Job
            sav1:wrj = 1
        End!If sav:WarrantyJob <> job:Warranty_Job
    
        If sav1:cct Or sav1:wct Or sav1:crt Or sav1:wrt Or sav1:chj Or sav1:wrj
            !Something has changed, so add an audit entry
            get(audit,0)
            if access:audit.primerecord() = level:benign
                aud:notes         = 'JOB DETAILS CHANGED: '
    
                If sav1:cct = 1
                !Chargeable Charge Type
                    aud:notes = Clip(aud:notes) & '<13,10,13,10>CHAR. CHARGE TYPE: ' & CLip(job:Charge_Type)
                End!If sav1:cct
    
                If sav1:wct = 1
                !Warranty Charge Type
                    aud:notes = Clip(aud:notes) & '<13,10,13,10>WARR. CHARGE TYPE: ' & CLip(job:Warranty_Charge_Type)
                End!If sav1:cct
    
                If sav1:crt = 1
                !Chargeable Repair Type
                    aud:notes = Clip(aud:notes) & '<13,10,13,10>CHAR. REPAIR TYPE: ' & CLip(job:Repair_Type)
                End!If sav1:cct
    
                If sav1:wrt = 1
                !Warranty Repair Type
                    aud:notes = Clip(aud:notes) & '<13,10,13,10>WARR. REPAIR TYPE: ' & CLip(job:Repair_Type_Warranty)
                End!If sav1:cct

                If sav1:chj = 1
                !Chargable Job
                    aud:notes = Clip(aud:notes) & '<13,10,13,10>CHARGEABLE JOB: ' &  Clip(job:Chargeable_Job)
                End!If sav1:chj = 1

                If sav1:wrj = 1
                !Warranty Job    
                    aud:notes = Clip(aud:notes) & '<13,10,13,10>WARRANTY JOB: ' & Clip(job:Warranty_Job)
                End!If sav1:wrj = 1
    
                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                aud:type          = 'JOB'
                access:users.clearkey(use:password_key)
                use:password = glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'JOB UPDATED'
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign
    
        End!If ChangedGroup
    End!If thiswindow.request <> Insertrecord
