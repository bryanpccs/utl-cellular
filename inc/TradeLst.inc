	recordspercycle     = 25
	recordsprocessed    = 0
	percentprogress     = 0
	setcursor(cursor:wait)
	open(progresswindow)
	progress:thermometer    = 0
	?progress:pcttext{prop:text} = '0% Completed'
	
	recordstoprocess    = Records(tradeacc)
	
	
	save_tra_id = access:tradeacc.savefile()
	set(tra:account_number_key)
	loop
	    if access:tradeacc.next()
	       break
	    end !if
	    Do GetNextRecord2
	    If tra:invoice_sub_accounts <> 'YES'
	        get(tradetmp,0)
	        if access:tradetmp.primerecord() = Level:Benign
	            tratmp:account_number = tra:account_number
	            tratmp:company_name   = tra:company_name
	            tratmp:type           = 'MAI'
	            if access:tradetmp.insert()
	               access:tradetmp.cancelautoinc()
	            end
	        end!if access:tradetmp.primerecord() = Level:Benign
	    Else!If tra:invoice_sub_accounts = 'YES'
	        save_sub_id = access:subtracc.savefile()
	        access:subtracc.clearkey(sub:main_account_key)
	        sub:main_account_number = tra:account_number
	        set(sub:main_account_key,sub:main_account_key)
	        loop
	            if access:subtracc.next()
	               break
	            end !if
	            if sub:main_account_number <> tra:account_number      |
	                then break.  ! end if
	            get(tradetmp,0)
	            if access:tradetmp.primerecord() = Level:Benign
	                tratmp:account_number = sub:account_number
	                tratmp:company_name   = sub:company_name
	                tratmp:type           = 'SUB'
	                if access:tradetmp.insert()
	                   access:tradetmp.cancelautoinc()
	                end
	            end!if access:tradetmp.primerecord() = Level:Benign
	        end !loop
	        access:subtracc.restorefile(save_sub_id)
	    End!If tra:invoice_sub_accounts = 'YES'
	end !loop
	access:tradeacc.restorefile(save_tra_id)
	
	setcursor()
	close(progresswindow)
