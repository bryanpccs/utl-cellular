                If job:date_completed = ''
                    Cycle
                End!If job:date_completed = ''

				If job:date_completed > tmp:BeforeDate
					Cycle					
				End!If job:date_completed > tmp:BeforeDate
                If job:invoice_exception = 'YES'
                    Cycle
                End!If job:invoice_exception = 'YES'
                If job:invoicebatch <> ''
                    Cycle
                End!If job:invoicebatch <> ''
                If job:bouncer <> ''
                    Cycle
                End!If job:bouncer <> ''  
                If job:invoicestatus <> ''
                	Cycle
                End!If job:invoicestatus <> ''

		! Start Change 2905 BE(18/07/03)
                IF (job:cancelled = 'YES') THEN
                	Cycle
                END
		! End Change 2905 BE(18/07/03)

                If def:qa_required = 'YES'
                    If job:qa_passed <> 'YES'
                        Cycle
                    End!If job:qa_passed <> 'YES'
                End!If def:qa_required = 'YES'
                access:chartype.clearkey(cha:charge_type_key)
                cha:charge_type = job:charge_type
                If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
                    !Found
                    If cha:excludeinvoice 
                        Cycle
                    End!If cha:excludeinvoice
                Else! If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
                    !Error
                End! If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
                Access:REPTYDEF.ClearKey(rtd:Chargeable_Key)
                rtd:Chargeable  = 'YES'
                rtd:Repair_Type = job:Repair_Type
                If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                    !Found
                    If rtd:ExcludeFromInvoicing
                        Cycle                                
                    End !If rtd:ExcludeFromInvoicing
                Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End !If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign

                IF job:ignore_chargeable_charges = 'YES'
                    If job:sub_total = 0
                        Cycle
                    End!If job:sub_total = 0
                Else!IF job:ignore_chargeable_charges = 'YES'
                    Pricing_Routine('C',labour",parts",pass",a")
                    If pass"    = False
                        Cycle
                    Else!If pass"    = False
                        job:labour_cost = labour"
                        job:parts_cost  = parts"
                        job:sub_total   = job:labour_cost + job:parts_cost + job:courier_cost
					End!If pass"    = False
				End!IF job:ignore_chargeable_charges = 'YES'						
                JOB:InvoiceStatus = 'UNA'
                job:invoicebatch    = prv:BatchNumber
                job:invoiceaccount  = prv:accountnumber
                access:jobs.update()
                job_count# += 1
                job_value$ += job:sub_total
                If job_count# = 1
                    prv:noofjobs = 1
                    prv:Batchvalue = job:sub_total
                    prv:accounttype = tratmp:type
                    access:proinv.insert()
                End!If job_count# = 1
                    
                
