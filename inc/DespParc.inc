    Parcel:Labels  = 1
    If multi# = 1
        Parcel:OrderNo = 'DB' & dbt:Batch_Number
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = Account_Number2_Temp
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:Use_Sub_Accounts = 'YES'
                    Parcel:CompanyName  = sub:Company_Name
                    Parcel:AddressLine1 = sub:Address_Line1
                    Parcel:AddressLine2 = sub:Address_Line2
                    Parcel:Town         = sub:Address_Line3
                    Parcel:Postcode     = sub:Postcode
                Else !If tra:Use_Sub_Accounts = 'YES'
                    Parcel:CompanyName  = tra:Company_Name
                    Parcel:AddressLine1 = tra:Address_Line1
                    Parcel:AddressLine2 = tra:Address_Line2
                    Parcel:Town         = tra:Address_Line3
                    Parcel:Postcode     = tra:Postcode
                 
                End !If tra:Use_Sub_Accounts = 'YES'
            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            
        Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        Parcel:AccountCode  = Account_Number2_Temp
    Else !If multi# = 1
        Parcel:OrderNo = 'J' & job:Ref_Number & ' ' & job:Despatch_Type
        Parcel:Instructions = 'DB' & dbt:Batch_Number
        Parcel:CompanyName  = job:Company_Name_Delivery
        Parcel:AddressLine1 = job:Address_Line1_Delivery
        Parcel:AddressLine2 = job:Address_Line2_Delivery
        Parcel:Town         = job:Address_Line3_Delivery
        Parcel:Postcode     = job:Postcode_Delivery 
        Parcel:AccountCode  = job:Account_Number            
    End !If multi# = 1
    Parcel:Service      = cou:Service
    Parcel:Type         = 1
    Parcel:Workstation  = tmp:WorkstationName
