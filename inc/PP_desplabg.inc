
SECTION('CheckOld')
    save_cou_ali_id = access:courier_alias.savefile()
    access:courier_alias.clearkey(cou_ali:courier_type_key)
    cou_ali:courier_type = 'LABEL G'
    set(cou_ali:courier_type_key,cou_ali:courier_type_key)
    loop
        if access:courier_alias.next()
           break
        end !if
        if cou_ali:courier_type <> 'LABEL G'      |
            then break.  ! end if
        tmp:OldConsignNo = ''

        save_job_ali_id = access:jobs_alias.savefile()
        access:jobs_alias.clearkey(job_ali:date_despatch_key)
        job_ali:courier         = cou_ali:courier
        job_ali:date_despatched = Today()
        set(job_ali:date_despatch_key,job_ali:date_despatch_key)
        loop
            if access:jobs_alias.next()
               break
            end !if
            if job_ali:courier         <> cou_ali:courier      |
            or job_ali:date_despatched <> Today()      |
                then break.  ! end if
    Compile('***',Debug=1)
      Message('Found job despatched today (' & tmp:accountnumber &|
              '|job:account: ' & job_ali:account_number &|
              '|job_ali:postcode: ' & job_ali:postcode_delivery &|
              '|job:postcode: ' & job:postcode_delivery,'Debug Message',icon:exclamation)
    ***
            If job_ali:account_number   = tmp:accountnumber and job_ali:postcode_Delivery = job:postcode_delivery
                If job_ali:consignment_number <> ''
                    tmp:OldConsignNo = job_ali:consignment_number
    Compile('***',Debug=1)
      Message('Found Old Consignment Number on Job: ' & job_ali:ref_number,'Debug Message',icon:exclamation)
    ***
                    Break
                End!If job_ali:consignment_number <> ''
            End!If job_ali:account_number   = job:account_number
        end !loop
        access:jobs_alias.restorefile(save_job_ali_id)
        If tmp:OldConsignNo <> ''
            Break
        Else!If tmp:OldConsignNo <> ''
            save_job_ali_id = access:jobs_alias.savefile()
            access:jobs_alias.clearkey(job_ali:date_despatch_loan_key)
            job_ali:loan_courier    = cou_ali:courier
            job_ali:loan_despatched = Today()
            set(job_ali:date_despatch_loan_key,job_ali:date_despatch_loan_key)
            loop
                if access:jobs_alias.next()
                   break
                end !if
                if job_ali:loan_courier    <> cou_ali:courier      |
                or job_ali:loan_despatched <> Today()      |
                    then break.  ! end if
    Compile('***',Debug=1)
      Message('Found loan despatched today (' & tmp:accountnumber &|
              '|job:account: ' & job_ali:account_number &|
              '|job_ali:postcode: ' & job_ali:postcode_delivery &|
              '|job:postcode: ' & job:postcode_delivery,'Debug Message',icon:exclamation)
    ***
                If job_ali:account_number   = tmp:accountnumber and job_ali:postcode_Delivery = job:postcode_delivery
                     If job_ali:loan_consignment_number <> ''
                        tmp:OldConsignNo = job_ali:loan_consignment_number
    Compile('***',Debug=1)
      Message('Found Old Consignment Number on Job(Loan): ' & job_ali:ref_number,'Debug Message',icon:exclamation)
    ***
                        Break
                    End!If job_ali:loan_consignment_number <> ''
                End!If job_ali:account_number   = job:account_number
            end !loop
            access:jobs_alias.restorefile(save_job_ali_id)
            IF tmp:OldConsignNo <> ''
                Break
            Else!IF tmp:OldConsignNo <> ''
                save_job_ali_id = access:jobs_alias.savefile()
                access:jobs_alias.clearkey(job_ali:date_despatch_exchange_key)
                job_ali:exchange_courier    = cou_ali:courier
                job_ali:exchange_despatched = Today()
                set(job_ali:date_despatch_exchange_key,job_ali:date_despatch_exchange_key)
                loop
                    if access:jobs_alias.next()
                       break
                    end !if
                    if job_ali:exchange_courier    <> cou_ali:courier      |
                    or job_ali:exchange_despatched <> Today()      |
                        then break.  ! end if
    Compile('***',Debug=1)
      Message('Found job exchange despatched today (' & tmp:accountnumber &|
              '|job:account: ' & job_ali:account_number &|
              '|job_ali:postcode: ' & job_ali:postcode_delivery &|
              '|job:postcode: ' & job:postcode_delivery,'Debug Message',icon:exclamation)
    ***
                    If job_ali:account_number   = tmp:accountnumber and job_ali:postcode_Delivery = job:postcode_delivery
                        If job_ali:exchange_consignment_number <> ''
                            tmp:OldConsignNo = job_ali:exchange_consignment_number
    Compile('***',Debug=1)
      Message('Found Old Consignment Number on Job(Exchange): ' & job_ali:ref_number,'Debug Message',icon:exclamation)
    ***
                            Break
                        End!If job_ali:exchange_consignment_number <> ''
                    End!If job_ali:account_number   = job:account_number
                end !loop
                access:jobs_alias.restorefile(save_job_ali_id)
                If tmp:OldConsignNo <> ''
                    Break
                End!If tmp:OldConsignNo <> ''
            End!IF tmp:OldConsignNo <> ''
        End!If tmp:OldConsignNo <> ''
    end !loop
    access:courier_alias.restorefile(save_cou_ali_id)

SECTION('Export')
    Remove(explabg)
    access:explabg.open()
    access:explabg.usefile()

    Clear(gen:record)
    epg:account_number  = Format(cou:account_number,@s8)
    If multi# = 1
        epg:ref_number        = '|DB' & Clip(Format(dbt:batch_number,@s9))
        epg:customer_name     = '|' & Format(account_number2_temp,@s30)
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = account_number2_temp
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(sub:contact_name)
                    epc:address_line1 = '|' & Left(sub:company_name)
                    epc:address_line2 = '|' & Left(sub:address_line1)
                    epc:town = '|' & Left(sub:address_line2)
                    epc:county = '|' & Left(sub:address_line3)
                    epc:postcode = '|' & Left(sub:postcode)
                else!if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(tra:contact_name)
                    epc:address_line1 = '|' & Left(tra:company_name)
                    epc:address_line1 = '|' & Left(tra:address_line1)
                    epc:town = '|' & Left(tra:address_line2)
                    epc:county = '|' & Left(tra:address_line3)
                    epc:postcode = '|' & Left(tra:postcode)
                end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
    Else!If multi# = 1
        epg:ref_number      = '|' & Format('J' & Clip(job:Ref_number) & '/DB' & Clip(dbt:batch_number) & '/' |
                        & Clip(job:order_number),@s30)
        If job:surname = ''
            epg:contact_Name    = '|' & Format('N/A',@s30)
        Else!If job:surname = ''
            epg:contact_Name    = '|' & Format(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & |
                                        CLip(job:surname),@s30)
        End!If job:surname = ''
        epg:customer_name   = '|' & Format(job:account_number,@s30)
        epg:Address_Line1   = '|' & Format(job:company_name_delivery,@s30)
        epg:Address_Line2   = '|' & Format(job:address_line1_delivery,@s30)
        epg:Address_Line3   = '|' & Format(job:address_line2_delivery,@s30)
        epg:Address_Line4   = '|' & Format(job:address_line3_delivery,@s30)
        epg:Postcode        = '|' & Format(job:postcode_delivery,@s4)

    End!If multi# = 1
    epg:city_service      = '|' & Format(cou:service,@s2)
    Case job:despatch_type
        Of 'JOB'
