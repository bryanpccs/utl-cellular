            failed# = 0
            Access:Failed.ClearKey(fai:ref_number_key)                                                          !Delete all the entries from
            fai:ref_number = war:ref_number                                                     !the failure file, should be
            SET(fai:ref_number_key,fai:ref_number_key)                                          !blank anyway.
            LOOP
              IF Access:Failed.Next()
                BREAK
              END
              IF fai:ref_number <> war:ref_number    
                BREAK
              END
              Relate:Failed.Delete(0)
              Access:Failed.ClearKey(fai:ref_number_key)                                                          !Delete all the entries from
              fai:ref_number = war:ref_number                                                     !the failure file, should be
              SET(fai:ref_number_key,fai:ref_number_key)   
            END !loop
            ignore# = 0
        
            faultlog# = 0
        
            !Validate Service Centre
            Access:Centre_Codes.ClearKey(cen:service_centre_code_key)
            cen:service_centre_code = war:service_centre_code
            IF Access:Centre_Codes.Fetch(cen:service_centre_code_key)
              CLEAR(cen:record)
              AddFailed(war:service_centre_code,'Invalid Service Cen. Code')
              failed# = 1
            ELSE                
              IF CEN:Ignore_Warranty_Period = 'YES'
                ignore# = 1
              END!If CEN:Ignore_Warranty_Period = 'YES'
              IF cen:include_fault_logger = 'YES'
                faultlog# = 1
              END!If cen:include_fault_logger = 'YES'
            END
        
            !Has this reference number been used before
            found# = 0
            Access:Warranty_Alias.ClearKey(war_ali:job_reference_key)
            war_ali:service_centre_code = war:service_centre_Code
            war_ali:job_reference       = war:job_reference
            SET(war_ali:job_reference_key,war_ali:job_reference_key)
            LOOP
              IF Access:Warranty_Alias.Next()
                BREAK
              END
              IF war_ali:service_centre_code <> war:service_centre_code
                BREAK
              END
              IF war_ali:job_reference <> war:job_reference
                BREAK
              END
              IF war_ali:ref_number <> war:ref_number
                found# = 1
                BREAK
              END!If war_ali:ref_number <> war:ref_number
            END !loop
        
            IF found# = 1
              AddFailed(war:Job_Reference,'Job Ref. Already Exists')
              failed# = 1
            END!If found# = 1
        
            war:labour_total    = 0
        
            valprod# = 1
            valhard# = 1
            valsoft# = 1
            valmand# = 1
            valclub# = 1
            valoldi# = 1
            valpart# = 1
            valcent# = 1
            useoldserial# = 0
        
            novalidate# = 0
            !Validate Purchase Order And Price Category
            Access:Purchase_File.ClearKey(pur:purchase_order_no_key)
            pur:purchase_order_no = war:purchase_order_no
            IF Access:Purchase_File.Fetch(pur:purchase_order_no_key)
              CLEAR(pur:record)
              AddFailed(war:purchase_order_no,'CANNOT VALIDATE: Invalid Purch. Ord. No.')
              novalidate# = 1        
              failed# = 1
            ELSE!if err = 1orcode()
              Access:Price_Category.ClearKey(pri:purchase_order_key)
              pri:purchase_order_no = war:purchase_order_no
              pri:price_category    = war:price_category
              IF Access:Price_Category.Fetch(pri:purchase_order_key)
                CLEAR(pri:record)
                AddFailed(war:price_category,'CANNOT VALIDATE: Invalid Price Cat.')
                novalidate# = 1
                failed# = 1
              ELSE!if errorcode()
                If PRI:ValProductionSerialNO = 0
                  valprod# = 0
        End!If PRI:ValProductionSerialNO = 0
                If PRI:ValHardwareID = 0
          valhard# = 0
                End!If PRI:ValHardwareID = 0
                If PRI:ValSoftwareVersion = 0
          valsoft# = 0
                End!If PRI:ValSoftwareVersion = 0
                IF PRI:ValManufacturingDate = 0
          valmand# = 0
                End!If PRI:ValManufacturingDate = 0
                IF PRI:ValClubNokiaID = 0
          valclub# = 0
                End!IF PRI:ValClubNokiaID = 0
                If PRI:OldIMEI = 0
          valoldi# = 0
                End!If PRI:OldIMEI = 0
                If PRI:PartsRequired = 0
          valpart# = 0
                End!If PRI:PartsRequired = 0
                If PRI:IgnoreWarrantyPeriod = 1
          valcent# = 0
                End!If PRI:IgnoreWarrantyPeriod = 1
                If pri:ValidateOldIMEI = 1
          useoldserial# = 1                        
                End!If pri:ValidateOldIMEI = 1
                
                If pur:max_jobs <> 0
          If pur:jobs_to_date >= pur:max_jobs
            AddFailed(war:purchase_order_no,'Exceeded Max Jobs')
            failed# = 1
          End!If pur:jobs_to_date >= pur:max_jobs
                Else!If pur:max_jobs <> 0
          war:labour_total    = pri:cost
                End!If pur:max_jobs <> 0
              End!if errorcode()
            End!if errorcode()
        
            If novalidate# = 1
                valprod# = 0
                valhard# = 0
                valsoft# = 0
                valmand# = 0
                valclub# = 0
                valoldi# = 0
                valpart# = 0
                valcent# = 0
            End!If novalidate# <> 1
                
            
            !Validate Equipment Type
            Access:Equipment.ClearKey(equ:equipment_type_key)
            equ:equipment_type = war:equipment_type
            IF Access:Equipment.Fetch(equ:equipment_type_key)
              CLEAR(equ:record)
              AddFailed(war:equipment_type,'Invalid Equipment Type')
        
              failed# = 1
            ELSE!if
              If EQU:ValProdSerNo = 0
                  valprod# = 0
              End!If EQU:ValProdSerNo = 0
              If EQU:ValHardwareID = 0
                  valhard# = 0
              End!If EQU:ValHardwareID = 0
              If EQU:ValSoftwareVer = 0
                  valsoft# = 0
              End!If EQU:ValSoftwareVer = 0
              If EQU:ValManufactDate = 0
                  valmand# = 0
              End!If EQU:ValManufactDate = 0
            END !if
        
            If Len(Clip(war:serial_number)) <> 15
                AddFailed(war:serial_number,'Serial Number Invalid Length')
                failed# = 1
            End!If Len(war:serial_number) <> 15
        
            If NumberCheck(war:serial_number)
                AddFailed(war:serial_number,'Serial Number Invalid')
                failed# = 1
            End!If NumberCheck(war:serial_number)
        
        
            If ignore# = 0
        
                If UseOldSerial# = 1
                    Access:Warranty_Alias.ClearKey(war_ali:OldSerialNumberKey)
                    war_ali:old_serial_number = war:serial_number
                    SET(war_ali:OldSerialNumberKey,war_ali:OldSerialNumberKey)
                    loop
                        IF Access:warranty_alias.Next()
                          BREAK
                        END
                        IF war_ali:Old_serial_number <> war:serial_number     
                          BREAK
                        END
                        If war_ali:ref_number <> war:ref_number
                            If war_ali:service_centre_code  = war:service_centre_code
                                If Abs(war_ali:date_booked - war:date_booked) < def:days_overdue
                                    AddFailed(war:serial_number,'Old Serial No. Already Exists')
            
                                    failed# = 1
                                End!If Abs(war_ali:date_booked - war:date_booked) < def:days_overdue
                            Else!If war_ali:service_centre_code  = war:service_centre_code
                                If valcent# = 1
                                    If Abs(war_ali:date_booked - war:date_booked) < def:days_overdue
                                        AddFailed(war:serial_number,'Old Serial No. Exists In Serv. Cen.: ' & war_ali:service_centre_code)
            
                                        failed# = 1
                                    End!If Abs(war_ali:date_booked - war:date_booked) < def:days_overdue
                                End!If valcent# = 1
                            End!If war_ali:service_centre_code  = war:service_centre_code
                        End!If war_ali:ref_number <> war:ref_number
                    end !loop
                    
                Else!If UseOldSerial# = 1
                    Access:Warranty_Alias.ClearKey(war_ali:serial_number_key2)
                    war_ali:serial_number = war:serial_number
                    SET(war_ali:serial_number_key2,war_ali:serial_number_key2)
                    loop
                        IF Access:warranty_alias.Next()
                          BREAK
                        END
                        IF war_ali:serial_number <> war:serial_number     
                          BREAK
                        END
                        If war_ali:ref_number <> war:ref_number
                            If war_ali:service_centre_code  = war:service_centre_code
                                If Abs(war_ali:date_booked - war:date_booked) < def:days_overdue
                                    AddFailed(war:serial_number,'Serial No. Already Exists')
            
                                    failed# = 1
                                End!If Abs(war_ali:date_booked - war:date_booked) < def:days_overdue
                            Else!If war_ali:service_centre_code  = war:service_centre_code
                                If valcent# = 1
                                    If Abs(war_ali:date_booked - war:date_booked) < def:days_overdue
                                        AddFailed(war:serial_number,'Serial No. Exists In Serv. Cen.: ' & war_ali:service_centre_code)
            
                                        failed# = 1
                                    End!If Abs(war_ali:date_booked - war:date_booked) < def:days_overdue
                                End!If valcent# = 1
                            End!If war_ali:service_centre_code  = war:service_centre_code
                        End!If war_ali:ref_number <> war:ref_number
                    end !loop
                    
                End!If UseOldSerial# = 1
            End!If ignore# = 0
        
            !Validate Old Serial Number
            If valoldi# = 1
                If war:old_serial_number = ''
                    AddFailed('*NO OLD SERIAL NO*','Old Serial Number Missing')
                    failed# = 1
                Else!If war:old_serial_number = ''
                    If NumberCheck(war:old_serial_number)
                        AddFailed(war:old_serial_number,'Old Serial Number Incorrect Format')
                        failed# = 1
                    Else!If NumberCheck(war:old_serial_number)
                        If Len(Clip(war:old_serial_number)) <> 15
                            AddFailed(war:old_serial_number,'Old Serial Number Invalid Length')
                            failed# = 1
                        End!If Len(Clip(war:old_serial_number)) <> 15
                    End!If NumberCheck(war:old_serial_number)
                End!If war:old_serial_number = ''
            End!If valoldi# = 1
        
            !Validate Product Code
            If faultlog# = 1
        
                If war:product_code = '' or war:Product_Code = '0'
                    AddFailed('*NO PROD CODE*','Product Code Missing')
        
                    failed# = 1
                Else!If war:product_code = ''
                    If Len(Clip(war:product_code)) <> 7
                        AddFailed(war:product_code,'Product Code Invalid Format')
        
                        failed# = 1
                    Else
                        If NumberCheck(war:product_code)
                            AddFailed(war:product_code,'Product Code Invalid Format')
                            failed# = 1
                        Else
                            If DEF:LeadingZero = 'YES'
                                If val(sub(war:product_code,1,1)) <> 48
                                    AddFailed(war:product_code,'Invalid Product Code')
                
                                    failed# = 1
                                Else
                                    If val(sub(war:product_code,2,1)) = 48
                                        AddFailed(war:product_code,'Invalid Product Code')
                                        
                                        failed# = 1
                
                                    End!If val(sub(war:product_code,1,1)) = 48
                                End
                
                            End!If DEF:LeadingZero = 'YES'
                        End!If NumberCheck(war:product_code)
                    End!If Len(war:product_code) > 7        

                End!If war:product_code = ''
            End!If faultlog# = 1
        
            !Validate Repair Date
            If war:repair_date = ''
                AddFailed('*NO REP DATE*','No Repair Date Entered')
                
                failed# = 1
            End!If war:repair_date = ''
        
            If war:repair_date > Today()
                AddFailed(war:repair_date,'Invalid Repair Date')
                
                failed# = 1
            End!If war:repair_date > Today()
            If ignore# = 0
                If war:repair_date < Today() - def:days_overdue
                    AddFailed(Format(war:repair_date,@d6),'Repair Over ' & Clip(def:days_overdue) & ' Days Old')
                    
                    failed# = 1
                End!If war:repair_date < Today() - def
            End!If ignore# = 0
        
            !Validate Date-In
            If war:date_in = ''
                AddFailed('*NO REP DATE*','No Date-In Entered')
                
                failed# = 1
            End!If war: = 1date_in = ''
        
            If war:date_in > Today()
                AddFailed(Format(war:date_in,@d6),'Invalid Date-In')
                
                failed# = 1
            End!If war:date_in > Today()
        
            If Year(war:date_in) < Year(Today()) - 10
                AddFailed(Format(war:date_in,@d6),'Date-In Out Of Date')
                
                failed# = 1
            End!If Year(war:date_in) < Year(Today() - 100
        
            !Validate Authorisation Code
            Access:Auth_Codes.ClearKey(aut:authorisation_code_key)
            aut:authorisation_code = war:authorisation_code
            IF Access:Auth_codes.Fetch(aut:authorisation_code_key)
                clear(aut:record)
                AddFailed(war:authorisation_code,'Invalid Authorisation Code')
                
                failed# = 1
            end !if
        
            !Validate Customer Fault Text
            If war:customer_fault_text = '' or war:Customer_fault_Text = '0'
                AddFailed('*NO SYMP CODE*','Symptom Code Missing')
                
                failed# = 1
            Else!If war:customer_fault_text = ''
                If Len(Clip(war:customer_fault_text)) <> 4
                    AddFailed(war:customer_fault_text,'Symptom Code Invalid Length')
                    
                    failed# = 1
                Else!If Len(Clip(war:customer_fault_text)) <> 4
                    Access:Sympcode.ClearKey(sym:symptomcodekey)
                    sym:symptomcode = war:customer_fault_text
                    IF Access:Sympcode.Fetch(sym:symptomcodekey)
                        clear(sym:record)
                        AddFailed(war:customer_fault_text,'Invalid Symptom Code')
                        
                        failed# = 1
                    end !if
                End!If Len(Clip(war:customer_fault_text)) <> 4
            End!If war:customer_fault_text = ''
        
            !Validate Production Serial Number
            If valprod# = 1
                If war:production_serial_number = '' or war:Production_Serial_Number = '0'
                    AddFailed('*NO PROD SER NO*','Production Serial Number Missing')
                    
                    failed# = 1
                Else!If war:production_serial_number = ''
                    If Len(Clip(war:production_serial_number)) <> 9
                        AddFailed(war:production_serial_number,'Prod Serial No Invalid Length')
                        
                        failed# = 1
                    End!If Len(Clip(war:production_serial_number)) <> 9
                End!If war:production_serial_number = ''
        
            End!If valprod# = 1
        
            !Validate Hardware ID
            If valhard# = 1
                If war:hardware_id   = '' or war:hardware_ID = '0'
                    AddFailed('*NO HARD ID*','Hardware I.D. Missing')
                    
                    failed# = 1
                Else!If war:hardware_id   = ''
                    If Len(Clip(war:hardware_id)) <> 4
                        AddFailed(war:hardware_id,'Hardware I.D. Invalid Length')
                        
                        failed# = 1
                    Else
                        Loop z# = 1 To Len(Clip(war:hardware_id))
                            If NumberCheck(war:hardware_id)
                                AddFailed(war:hardware_id,'Hardware I.D. Invalid Format')
                                failed# = 1
                            End!If NumberCheck(war:hardware_id)
                        End!Loop z# = 1 To Len(war:product_code)
        
                    End!If Len(Clip(war:hardware_id)) <> 4
                End!If war:harware_id   = ''
        
            End!If valhard# = 1
        
            !Validate Software Version
            If valsoft# = 1
                If war:software_version = '' or war:software_version = '0'
                    AddFailed('*NO SOFT VER*','Software Version Missing')
                    
                    failed# = 1
                Else!!If war:software_version = ''

                    If Len(Clip(war:software_version)) <> 5
                        AddFailed(war:software_version,'Software Version Invalid Format')
                        failed# = 1
                    Else!If Len(Clip(war:software_version)) > 5
                        If Sub(war:software_version,3,1) <> '.'
                            AddFailed(war:software_version,'Software Version Invalid Format')
                            
                            failed# = 1
                        Else!If Sub(war:software_version,3,1) <> '.'
                            If NumberCheck(Sub(war:Software_Version,1,2)) Or NumberCheck(Sub(war:Software_Version,4,2))
                                AddFailed(war:Software_Version,'Software Version Invalid Format')
                                failed# = 1
                            End!NumberCheck(Clip(war:Software_Version,4,2))
                        End!If ~Instring('.',war:software_version,1,1)
                    End!If Len(Clip(war:software_version)) > 5
                End!If war:software_version = ''
            End!If valsoft# = 1
        
            !Validate Manufacturing Date
            If valmand# = 1
                If war:manufacturing_date = '' or war:Manufacturing_date = '0'
                    AddFailed('*NO MAN DATE*','Manufacturing Date Missing')
                    
                    failed# = 1
                Else!If war:manufacturing_date = ''
                    If Len(Format(war:manufacturing_date,@n06)) <> 6
                        AddFailed(Format(war:manufacturing_date,@n06),'Manufacturing Date Invalid Length')
                        
                        failed# = 1
                    Else!If Len(Clip(war:manufacturing_date)) <> 6
                        If DEF:ManDateValidation <> 0
                            If (Sub(Format(war:manufacturing_date,@n06),5,2)) > 12 Or (Sub(Format(war:manufacturing_date,@n06),5,2)) < 1
                                AddFailed(Format(war:manufacturing_date,@n06),'Manufacturing Date Invalid Format')
                                
                                failed# = 1
                            Else
                                If (Sub(Format(war:manufacturing_date,@n06),1,4)) < (Sub(Format(def:mandate,@n06),1,4))
                                    AddFailed(Format(war:manufacturing_date,@n06),'Manufacturing Date Invalid Format')
                                    
                                    failed# = 1
                                Else!If (Sub(Format(war:manufacturing_date,@n06),3,4) < (Sub(Format(def:mandate,@n06),3,4)
                                    If (Sub(Format(war:manufacturing_date,@n06),1,4) = Sub(Format(def:mandate,@n06),1,4)) And |
                                        (Sub(Format(war:manufacturing_date,@n06),5,2) < Sub(Format(def:mandate,@n06),5,2))
                                        AddFailed(Format(war:manufacturing_date,@n06),'Manufacturing Date Invalid Format')
                                        
                                        failed# = 1
                                    End
                                End!If (Sub(Format(war:manufacturing_date,@n06),3,4) < (Sub(Format(def:mandate,@n06),3,4)
                            End!If (Sub(Format(war:manufacturing_date,@n06),3,4)) < (Sub(Format(def:mandate,@n06),3,4))
                        End!If DEF:ManDateValidation <> 0
                    End
                End!If war:manufacturing_date = ''
        
            End!If valmand# = 1
        
            !Validate Club Nokia ID
            If valclub# = 1
                If Len(Clip(war:clubnokiaid)) < 10 Or Len(Clip(war:clubnokiaid)) > 11
                    AddFailed(war:clubnokiaid,'Club Nokia I.D. Invalid Length')
                    
                    failed# = 1
                End!If Len(Clip(war:clubnokiaid)) < 10 Or Len(Clip(war:clubnokiaid)) > 11
            End!If valclub# = 1
        
            !Validate Parts
            CLEAR(Duplicate_Parts_Queue)      
            FREE(Duplicate_Parts_Queue)
            parts_cost$ = 0
            found# = 0
            Access:Parts.ClearKey(pts:ref_number_key)
            pts:ref_number = war:ref_number
            SET(pts:ref_number_key,pts:ref_number_key)
            loop
                IF Access:Parts.Next()
                  BREAK
                END
                IF pts:ref_number <> war:ref_number    
                  BREAK
                END
                found# = 1
                Access:Stock.ClearKey(sto:location_key)
                sto:location    = 'MAIN STORE'
                sto:part_number = pts:part_number
                IF Access:Stock.Fetch(sto:location_key)
                    clear(sto:record)
                    AddFailed(pts:part_number,'Invalid Part Number')
                    
                    failed# = 1
                Else!if errorcode()
                    
                    If pts:part_number = 'ADJUSTMENT'
                        pts:description = 'ADJUSTMENT'
                        pts:cost    = 0
                    ELSIF pts:part_number = 'N/A'
                       !Do nothing!
                    ELSIF pts:part_number = ''
                       !Do nothing
                    Else!If pts:part_number = 'ADJUSTMENT'
                    
                        !Test to see if duplicated!
            
                        SORT(Duplicate_Parts_Queue,Duplicate_Parts_Queue.Part_Number)
                        Duplicate_Parts_Queue.Part_Number = pts:part_number
                        GET(Duplicate_Parts_Queue,Duplicate_Parts_Queue.Part_Number)
                        IF ~ERROR()
                          !Failed!
                          AddFailed(pts:part_number,'Duplicated Part Number')
                          failed# = 1
                        ELSE
                          Duplicate_Parts_Queue.Part_Number = pts:part_number
                          ADD(Duplicate_Parts_Queue)
                        END
                        !Continue!
                    
                        If Len(Clip(pts:part_number)) < 6 Or Len(Clip(pts:part_number)) > 7
                            AddFailed(pts:part_number,'Invalid Part Number Length')
                            
                            failed# = 1
                        Else!If Len(Clip(pts:part_number)) <> 7
                            pts:description = sto:description
                            pts:cost    = sto:sale_cost
                            parts_cost$ += pts:cost * pts:quantity
                            Access:Parts.Update()
                        End!!If Len(Clip(pts:part_number)) <> 7
                    End!If pts:part_number = 'ADJUSTMENT'
        
                end !if
        
                !Validate Fault Type
                Access:Faults.ClearKey(fau:fault_type_key)
                fau:fault_type = pts:fault_type
                IF Access:Faults.Fetch(fau:fault_type_key)
                    clear(fau:record)
                    AddFailed(pts:fault_type,'Invalid Fault Type')
                    
                    failed# = 1
                Else!if errorcode()
                    If FAU:FailClaim = 1
                        AddFailed(pts:fault_type,'Fault Type - Automatic Fail')
                        
                        failed# = 1
                    End!If FAU:FailClaim = 1
        
                    If fau:part <> 'Y' And (pts:part_number <> 'ADJUSTMENT' And pts:part_number <> '')
                        AddFailed(pts:part_number,'Fault Type / Part No. Mismatch')
                        
                        failed# = 1
                    End!If fau:part <> 'Y' And pts:part_number <> 'ADJUSTMENT'
        
                    If fau:part = 'Y' and pts:part_number = ''
                        Addfailed('*NO PART NO*','Fault Type / Part No. Missing')
                        
                        failed# = 1
                    End!If fau:part = 'Y' and pts:part_number <> 'ADJUSTMENT' Or pts:part_number = ''
        
                    If fau:module <> 'Y' And pts:module <> 'N/A' And pts:module <> ''
                        AddFailed(pts:module,'Fault Type / Module Mismatch')
                        
                        failed# = 1
                    End!If fau:modue <> 'Y' And pts:module <> 'N/A' And pts:module <> ''
        
                    If fau:circuit <> 'Y' And pts:circuit <> 'N/A' And pts:circuit <> ''
                        AddFailed(pts:circuit,'Fault Type / Circuit Mismatch')
                        
                        failed# = 1
                    End!If fau:module <> 'Y' And pts:module <> 'N/A' And pts:module <> ''
        
        
                    !Validate Circuit
                    If fau:circuit = 'Y'
                    !Circuit is required according to the Fault Type
                        If pts:circuit = '' or pts:Circuit = '0'
                            !Is the Circuit Missing?
                            AddFailed('*NO CIRCUIT*','Fault Type / Circuit Missing')
                            
                            failed# = 1
                        Else!If pts:circuit = ''
                            !Does the Circuit match with the list of circuits?
                            Access:Circuits.ClearKey(cir:circuit_key)
                            cir:circuit = pts:circuit
                            IF Access:Circuits.Fetch(cir:circuit_key)
                                clear(cir:record)
                                AddFailed(pts:circuit,'Invalid Circuit')
                                
                                failed# = 1
                            Else!if errorcode()
                                !Is the Circuit Format correct?
                                If Len(Clip(pts:circuit)) <> 4
                                    AddFailed(pts:circuit,'Circuit Invalid Length')
                                    
                                    failed# = 1
                                End!If Len(Clip(pts:circuit)) <> 4
                                If val(sub(pts:circuit,1,1)) < 65 Or val(sub(pts:circuit,1,1)) > 90
                                    AddFailed(pts:circuit,'Circuit Invalid Format')
                                    
                                    failed# = 1
                                Else
                                    If NumberCheck(Sub(pts:circuit,2,Len(pts:circuit)))
                                        AddFailed(pts:circuit,'Circuit Invalid Format')
                                        failed# = 1
                                    End!If NumberCheck(pts:circuit)
                                End!If val(sub(pts:circuit,1,1)) < 65 Or val(sub(pts:circuit,1,1)) > 90
                            End!if errorcode()
        
                        End!If pts:circuit = ''
        
                    End!If fau:circuit = 'Y' and (pts:circuit <> 'N/A' or pts:circuit = '')
                end !if
        
                !Validate Module
                If pts:module <> ''
                    Access:Modules.ClearKey(mod:module_key)
                    mod:module = pts:module
                    IF AccesS:Modules.Fetch(mod:module_key)
                        clear(mod:record)
                        AddFailed(pts:module,'Invalid Module')
                        
                        failed# = 1
                    end !if
        
                End!If pts:module <> ''
        
        
            end !loop
        
            !Validate Parts
            If valpart# = 1
                If found# = 0
                    AddFailed('*NO PARTS*','No Parts Used')
                    
                    failed# = 1
                End!If found# = 0
            End!War:purchase_order_no <> 'FAULTY S/S'
