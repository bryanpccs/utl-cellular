        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                despatch# = 0
                If tra:use_sub_accounts = 'YES'
                    If sub:despatch_paid_jobs = 'YES'
                        If sub:despatch_invoiced_jobs = 'YES'
                            If job:invoice_number <> ''
                                despatch# = 1
                            End!If job:invoice_number <> ''
                        Else!If sub:despatch_invoiced_jobs = 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs = 'YES' And job:invoice_number <> ''
                    End!If sub:despatch_paid_jobs = 'YES'
                Else!If tra:use_sub_accounts = 'YES'
                    If tra:despatch_paid_jobs = 'YES'
                        If tra:despatch_invoiced_jobs = 'YES'
                            If job:invoice_number <> ''
                                despatch# = 1
                            End!If job:invoice_number <> ''
                        Else!If tra:despatch_invoiced_jobs = 'YES' 
                            despatch# = 1
                        End!If tra:despatch_invoiced_jobs = 'YES' and job:invoice_number <> ''
                    End!If sub:despatch_paid_jobs = 'YES'
                End!If tra:use_sub_accounts = 'YES'
            end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign

        If job:date_completed = ''
            job:paid = 'NO'
            job:paid_warranty = 'NO'
        Else!If job:date_completed = ''
            If job:chargeable_job = 'YES'
                Total_Price('C',vat",total",balance")
                If total" = 0
                    job:paid = 'NO'
                Else!If total" = 0
                    If balance" <= 0
                        If job:paid <> 'YES'
                            job:paid = 'YES'
                        End
                        If despatch# = 1
                            If job:despatched <> 'YES'
                                job:despatched = 'REA'
                                job:despatch_type = 'JOB'
                                job:current_courier = job:courier
                                GetStatus(810,1,'JOB') !ready to despatch
                            Else!If job:despatched <> 'YES'
                                GetStatus(910,1,'JOB') !despatched unpaid
                            End!If job:despatched <> 'YES'
                        End!If despatch# = 1
                        If despatch# = 0
			    ! Start Change 4012 BE(11/03/04)
			    !GetStatus(925,1,'JOB') !paid awaiting collection
			    access:COURIER.Clearkey(cou:Courier_Key)
			    cou:Courier = job:Courier
			    IF (access:COURIER.fetch(cou:Courier_Key) = Level:Benign AND cou:CustomerCollection) THEN
			    	GetStatus(925,1,'JOB') !paid awaiting collection
			    END
			    ! End Change 4012 BE(11/03/04)
                        End!If despatch# = 0
                    End!If balance" = 0
                    If balance" > 0
                        If job:paid = 'YES'
                            job:paid = 'NO'
                        End
                    End!If balance" < 0
                End!If total" = 0
            End!If job:chargeable_job = 'YES'
            If job:warranty_job = 'YES'
                If job:invoice_number_warranty <> ''
                    job:paid_warranty = 'YES'
                Else
                    job:paid_warranty = 'NO'
                End!If job:invoice_number_warranty <> ''
            End!If job:warranty_job = 'YES'
        End!If job:date_completed = ''
        access:jobs.update()

