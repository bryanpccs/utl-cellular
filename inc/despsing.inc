    cont# = 1
    If job:chargeable_job = 'YES'
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number  = job:account_number
        If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
            !Found
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number  = sub:main_account_number
            If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                !Found
                setcursor()
                If tra:Use_sub_accounts = 'YES' and tra:invoice_sub_accounts = 'YES'  !Changed from Invoice_Sub_Account by N.Brownlee 4/7/01
                    If sub:stop_account = 'YES'
                        Case MessageEx('Cannot despatch job number '&Clip(job:ref_number)&'.<13,10><13,10>Trade account is on stop.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx                    
                        cont# = 0
                    End!If tra:stop_account = 'YES'                    
                Else!If tra:invoice_sub_accounts = 'YES'
                    If tra:stop_account = 'YES'
                        Case MessageEx('Cannot despatch job number '&Clip(job:ref_number)&'.<13,10><13,10>Trade account is on stop.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx                    
                        cont# = 0
                    End!If tra:stop_account = 'YES'                    
                End!If tra:invoice_sub_accounts = 'YES'

                setcursor(cursor:wait)
            Else! If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                !Error
            End! If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
            
        Else! If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
            !Error
        End! If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign        
    End!If job:chargeable_job = 'YES'

    If cont# = 1
    
        Case cou:courier_type
            Of 'CITY LINK'
                glo:file_name   = 'C:\CITYOUT.TXT'
                Remove(expcity)
                access:expcity.open()
                access:expcity.usefile()
                epc:ref_number  = '|' & Left(Sub(Format(job:ref_number,@n_30),1,30))
                Include('DespCity.inc')
                access:expcity.insert()
                access:expcity.close()
                Copy(expcity,Clip(cou:export_path))
                tmp:ConsignNo   =   ''
                setcursor()
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,'')
                setcursor(cursor:wait)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
!                    Case job:despatch_type
!                        Of 'JOB'
!                            Include('despatch.inc','JOB')
!                        Of 'EXC'
!                            Include('despatch.inc','Exchange')
!                        Of 'LOA'
!                            Include('despatch.inc','Loan')
!                    End!Case job:despatch_type
                End!If consignment_number"  = ''
        
            Of 'LABEL G'
                tmp:accountnumber   = job:account_number
                tmp:OldConsignNo    = ''
                Include('desplabg.inc','CheckOld')
        
                glo:file_name   = Clip(cou:export_path) & '\CL' & Format(job:ref_number,@n06) & '.TXT'
                Remove(glo:file_name)
                Include('desplabg.inc','Export')
        
                sav:path    = Path()
                Setpath(cou:export_path)
                Run('LABELG.EXE ' & Clip(glo:file_name) & ' ' & Clip(COU:LabGOptions),1)
                Setpath(sav:path)
        
                include('desplabg.inc','Error')
                
                Compile('***',Debug=1)
                    Message('tmp:labelerror: ' & tmp:labelerror,'Debug Message',icon:exclamation)
                ***                
                If tmp:labelerror <> 1
                    tmp:ConsignNo   =   ''
                    Setcursor()
                    Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:labelerror)
                    Setcursor(cursor:wait)
                    If tmp:ConsignNo  = ''
                        get(audit,0)
                        if access:audit.primerecord() = level:benign
                            aud:ref_number    = job:ref_number
                            aud:date          = today()
                            aud:time          = clock()
                            aud:type          = job:despatch_type
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            aud:user = use:user_code
                            aud:action        = 'DESPATCH ROUTINE CANCELLED'
                            access:audit.insert()
                        end!�if access:audit.primerecord() = level:benign
        
                    Else!If consignment_number"  = ''
                        Despatch(job:Despatch_Type,tmp:ConsignNo,0)                                      
!                        Case job:despatch_type
!                            Of 'JOB'
!                                Include('despatch.inc','JOB')
!                            Of 'EXC'
!                                Include('despatch.inc','Exchange')
!                            Of 'LOA'
!                                Include('despatch.inc','Loan')
!                        End!Case job:despatch_type
                    End!If consignment_number"  = ''
        
                End!If label_error# = 0
            Of 'UPS ALTERNATIVE'
                glo:file_name   = 'C:\UPS.TXT'
                Remove(expcity)
                access:expcity.open()
                access:expcity.usefile()
                epc:ref_number  = '|' & Left(Sub(Format(job:ref_number,@n_30),1,30))
                Include('DespCity.inc')
                access:expcity.insert()
                access:expcity.close()
                Copy(expcity,Clip(cou:export_path))
                tmp:ConsignNo   =   ''
                setcursor()
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,'')
                setcursor(cursor:wait)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
!                    Case job:despatch_type
!                        Of 'JOB'
!                            Include('despatch.inc','JOB')
!                        Of 'EXC'
!                            Include('despatch.inc','Exchange')
!                        Of 'LOA'
!                            Include('despatch.inc','Loan')
!                    End!Case job:despatch_type
                End!If consignment_number"  = ''

            Of 'ANC'
                !Do Nothing             
            Of 'PARCELINE LASERNET'
                !Parceline
                tmp:ParcelLineName = Clip(cou:Export_Path) & '\J' & Format(Job:Ref_Number,@n07) &  '.TXA'
                Remove(tmp:ParcelLineName)
                Open(ParcelLineExport)
                If Error()
                    Create(ParcelLineExport)
                    Open(ParcelLineExport)
                End !If Error()
                Clear(Parcel:Record)
                Include('DespParc.inc')
                Add(ParcelLineExport)

                CLose(ParcelLineExport)
                RENAME(Clip(tmp:ParcelLineName),Clip(Clip(cou:Export_Path) & '\J' & Format(Job:Ref_Number,@n07) &  '.TXT'))
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,'')
                setcursor(cursor:wait)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
!                    Case job:despatch_type
!                        Of 'JOB'
!                            Include('despatch.inc','JOB')
!                        Of 'EXC'
!                            Include('despatch.inc','Exchange')
!                        Of 'LOA'
!                            Include('despatch.inc','Loan')
!                    End!Case job:despatch_type
                End!If consignment_number"  = ''
            Else
                Setcursor()
                If cou:AutoConsignmentNo 
                    cou:LastConsignmentNo += 1
                    Access:COURIER.Update()
                    tmp:ConsignNo   = cou:LastConsignmentNo

                Else !If cou:AutoConsignmentNo 
                    Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,'')    
                End !If cou:AutoConsignmentNo 
                
                Setcursor(cursor:wait)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
!                    Case job:despatch_type
!                        Of 'JOB'
!                            Include('despatch.inc','JOB')
!                        Of 'EXC'
!                            Include('despatch.inc','Exchange')
!                        Of 'LOA'
!                            Include('despatch.inc','Loan')
!                    End!Case job:despatch_type
                End!If consignment_number"  = ''
        
        End!Case cou:courier_type
    End!If cont# = 1