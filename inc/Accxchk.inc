    found# = 0
    save_xca_id = access:exchacc.savefile()
    access:exchacc.clearkey(xca:ref_number_key)
    xca:ref_number = job:exchange_unit_number
    set(xca:ref_number_key,xca:ref_number_key)
    loop
        if access:exchacc.next()
           break
        end !if
        if xca:ref_number <> job:exchange_unit_number      |
            then break.  ! end if
        found# = 1
        Break
    end !loop
    access:exchacc.restorefile(save_xca_id)
    If found# = 1
        glo:select1 = job:exchange_unit_number
        glo:select2 = ''
        Validate_Exchange_Accessories
        If glo:select2 = 'FAIL'
            beep(beep:systemhand)  ;  yield()
            message('You are attempting to despatch a unit with a missing accessory.'&|
                    '||The following will now be actioned:'&|
                    '||1) The status of the job will be changed to: '&|
                    '|    804 HELD - MISSING ACCESSORY.'&|
                    '||2) An entry will be made in the Audit Trail.'&|
                    '||You will NOT be able to despatch this unit all the accessories '&|
                    'are present.', |
                    'ServiceBase 2000', icon:hand)
            Status_Routine(804,job:current_status,end_date",end_time",a")
            job:status_end_date = end_date"
            job:status_end_time = end_time"
            job:despatch_Type = 'EHL'
            access:jobs.update()
            get(audit,0)
            if access:audit.primerecord() = level:benign
                aud:notes         = 'UNIT HELD FROM DESPATCH'
                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                access:users.clearkey(use:password_key)
                use:password = glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'MISSING ACCESSORY ON EXCHANGE UNIT'
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign

            brw1.resetsort(1)
        Else!If glo:select1 = 'FAIL'
            job:despatch_type = 'EXC'
            access:jobs.update()
        End!If glo:select1 = 'FAIL'
        glo:select1 = ''
        glo:select2 = ''
    End!If found# = 1
