

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01037.INC'),ONCE        !Local module procedure declarations
                     END


MSNRequired          PROCEDURE  (func:Manufacturer)   ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'MSNRequired')      !Add Procedure to Log
  end


    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:Use_MSN = 'YES'
            Return Level:Benign
        End!If man:Use_MSN = 'YES'
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    Return Level:Fatal


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'MSNRequired',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
