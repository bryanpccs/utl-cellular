

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01064.INC'),ONCE        !Local module procedure declarations
                     END


IMEIModelRoutine     PROCEDURE  (func:IMEI,func:ModelNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:IMEI             STRING(6)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_esn_id     ushort,auto
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'IMEIModelRoutine')      !Add Procedure to Log
  end


    If func:IMEI <> 'N/A' And func:IMEI <> ''
        tmp:IMEI    = Sub(func:IMEI,1,6)
        If func:ModelNumber <> ''
            !Does the IMEI match the Model?
            Access:ESNMODEL.Clearkey(esn:ESN_Key)
            esn:ESN             = tmp:IMEI
            esn:Model_Number    = func:ModelNumber
            If Access:ESNMODEL.Tryfetch(esn:ESN_Key) = Level:Benign
                !Found
                Return func:ModelNumber
            Else! If Access:ESNMODEL.Tryfetch(esn:ESN_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
                !If not, count how many other entries for that IMEI Number
                Count# = 0
                Save_esn_ID = Access:ESNMODEL.SaveFile()
                Access:ESNMODEL.ClearKey(esn:ESN_Only_Key)
                esn:ESN = tmp:IMEI
                Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
                Loop
                    If Access:ESNMODEL.NEXT()
                       Break
                    End !If
                    If esn:ESN <> tmp:IMEI      |
                        Then Break.  ! End If
                    Count# += 1
                    If Count# > 1
                        Break
                    End !If Count# > 1
                End !Loop
                Access:ESNMODEL.RestoreFile(Save_esn_ID)

                Case Count#
                    Of 0
                        !There are no other Models' for this IMEI
                        get(esnmodel,0)
                        if access:esnmodel.primerecord() = Level:Benign
                            esn:esn           = tmp:IMEI
                            esn:model_number  = func:ModelNumber
                            access:esnmodel.insert()
                        end!if access:esnmodel.primerecord() = Level:Benign
                        Return func:ModelNumber

                    Of 1
                        !There is only one Model for this IMEI
                        Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
                        esn:ESN = tmp:IMEI
                        If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
                            !Found
                            Case IMEIModelMismatch(func:IMEI,func:ModelNumber,esn:Model_Number)
                                Of 1 !Use Existing Number
                                    !There are no other Models' for this IMEI
                                    get(esnmodel,0)
                                    if access:esnmodel.primerecord() = Level:Benign
                                        esn:esn           = tmp:IMEI
                                        esn:model_number  = func:ModelNumber
                                        access:esnmodel.insert()
                                    end!if access:esnmodel.primerecord() = Level:Benign

                                    Return func:ModelNumber
                                Of 2 !Use New Model Number
                                    Return esn:Model_Number
                                Else
                                    Return func:ModelNumber
                            End !If IMEIModelMismatch(func:IMEI,func:ModelNumber,esn:Model_Number)
                        Else! If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End! If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
                        
                    Else
                        !There are more than one IMEI for this model
                        Case MessageEx('There is a mismatch between the selected Model Number and I.M.E.I. Number.'&|
                          '<13,10>'&|
                          '<13,10>The selected I.M.E.I. Number corresponds to more than one Model Number.'&|
                          '<13,10>Do you wish to:'&|
                          '<13,10>'&|
                          '<13,10>1) Select a NEW Model Number for this unit?'&|
                          '<13,10>2) Ignore mismatch and allocate the Model Number to this I.M.E.I. Number','ServiceBase 2000',|
                                       'Styles\warn.ico','|&Select New Model|&Ignore Mismatch|&Cancel',3,3,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                            Of 1 ! &Select New Model Button
                                saverequest#      = globalrequest
                                globalresponse    = requestcancelled
                                globalrequest     = selectrecord
                                glo:select7 = func:IMEI
                                select_esn_model
                                glo:select7 = ''
                                if globalresponse = requestcompleted
                                    Return esn:Model_Number
                                Else
                                    Return func:ModelNumber
                                end
                                globalrequest     = saverequest#
                            Of 2 ! &Ignore Mismatch Button
                                get(esnmodel,0)
                                if access:esnmodel.primerecord() = Level:Benign
                                    esn:esn           = tmp:IMEI
                                    esn:model_number  = func:ModelNumber
                                    access:esnmodel.insert()
                                end!if access:esnmodel.primerecord() = Level:Benign
                                Return func:ModelNumber
                            Of 3 ! &Cancel Button
                                Return func:ModelNumber
                        End!Case MessageEx
                End !Case Count#
            End! If Access:ESNMODEL.Tryfetch(esn:ESN_Key) = Level:Benign
        Else !If func:ModelNumber <> ''
            !If there is no model number already, go off and find one
            Count# = 0
            Save_esn_ID = Access:ESNMODEL.SaveFile()
            Access:ESNMODEL.ClearKey(esn:ESN_Only_Key)
            esn:ESN = tmp:IMEI
            Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
            Loop
                If Access:ESNMODEL.NEXT()
                   Break
                End !If
                If esn:ESN <> tmp:IMEI      |
                    Then Break.  ! End If
                Count# += 1
                If Count# > 1
                    Break
                End !If Count# > 1
            End !Loop
            Access:ESNMODEL.RestoreFile(Save_esn_ID)

            Case Count#
                Of 0
                    !There are no corresponding Model Numbers
                    Return ''
                Of 1
                    !There is only ONE corresponding Model Number
                    Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
                    esn:ESN = tmp:IMEI
                    If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
                        !Found
                        Return esn:Model_Number
                    Else! If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        Return ''
                    End! If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
                Else
                    !There are many corresponding Model Numbers
                    saverequest#      = globalrequest
                    globalresponse    = requestcancelled
                    globalrequest     = selectrecord
                    glo:select7 = func:IMEI
                    select_esn_model
                    glo:select7 = ''
                    if globalresponse = requestcompleted
                        Return esn:Model_Number
                    Else
                        Return func:ModelNumber
                    end
                    globalrequest     = saverequest#
            End !Case Count#
        End !If func:ModelNumber <> ''
    End !If func:IMEI <> 'N/A' And func:IMEI <> ''

    Return func:ModelNumber
!    f_pass = 0
!    add_model# = 0
!    If f_esn <> 'N/A'
!        If f_model_number <> ''
!            found# = 1
!            access:esnmodel.clearkey(esn:esn_key)
!            esn:esn          = Sub(f_esn,1,6)
!            esn:model_number = f_model_number
!            if access:esnmodel.fetch(esn:esn_key)
!                found# = 0
!            end!if access:esnmodel.fetch(esn:esn_key)
!
!            If found# = 0
!                setcursor(cursor:wait)
!                count# = 0
!                save_esn_id = access:esnmodel.savefile()
!                access:esnmodel.clearkey(esn:esn_only_key)
!                esn:esn = Sub(f_esn,1,6)
!                set(esn:esn_only_key,esn:esn_only_key)
!                loop
!                    if access:esnmodel.next()
!                       break
!                    end !if
!                    if esn:esn <> Sub(f_esn,1,6)      |
!                        then break.  ! end if
!                    yldcnt# += 1
!                    if yldcnt# > 25
!                       yield() ; yldcnt# = 0
!                    end !if
!                    count# += 1
!                    If count# > 1
!                        Break
!                    End!If count# > 1
!                end !loop
!                access:esnmodel.restorefile(save_esn_id)
!                setcursor()
!                If count# = 0
!                    f_new_model_number = f_model_number
!                    add_model# = 1
!                End!If count# = 0
!
!                If count# = 1
!                    access:esnmodel.clearkey(esn:esn_only_key)
!                    esn:esn = Sub(f_esn,1,6)
!                    if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign
!                        Case MessageEx('There is a mismatch between the selected Model Number and the entered E.S.N. / I.M.E.I.<13,10><13,10>This E.S.N. corresponse to a '&clip(esn:model_number)&'.<13,10><13,10>Do you wish to :<13,10><13,10>1) Change the Model Number of this unit to '&clip(esn:model_number)&', 2) Ignore the mismatch?','ServiceBase 2000',|
!                                       'Styles\warn.ico','|&Change Model|&Ignore Mismatch|&Cancel',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                            Of 1 ! &Change Model Button
!                                f_new_model_number = esn:model_number
!                                f_pass = 1
!                            Of 2 ! &Ignore Mismatch Button
!                                f_pass = 1
!                                f_new_model_number = f_model_number
!                                add_model# = 1
!                            Of 3 ! &Cancel Button
!                        End!Case MessageEx
!                    end!if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign            
!                End!If count# = 1
!                If count# > 1
!                    Case MessageEx('There is a mismatch between the selected Model Number and the entered E.S.N. / I.M.E.I.<13,10><13,10>This E.S.N. corresponse to more than one Model Number.<13,10>Do you wish to :<13,10><13,10>1) Select a new Model Number for this unit,<13,10>2) Ignore the mismatch?','ServiceBase 2000',|
!                                   'Styles\warn.ico','|&Select Model|&Ignore Mismatch|&Cancel',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                        Of 1 ! &Select Model Button
!                            saverequest#      = globalrequest
!                            globalresponse    = requestcancelled
!                            globalrequest     = selectrecord
!                            glo:select7 = f_esn
!                            select_esn_model
!                            glo:select7 = ''
!                            if globalresponse = requestcompleted
!                                f_new_model_number = esn:model_number
!                                f_pass = 1
!                            end
!                            globalrequest     = saverequest#
!                        Of 2 ! &Ignore Mismatch Button
!                            f_new_model_number = f_model_number
!                            f_pass = 1
!                           add_model# = 1
!                        Of 3 ! &Cancel Button
!                    End!Case MessageEx
!                End!If count# > 1
!            End!If found# = 0
!        Else!If job:model_number <> ''
!            count# = 0
!            setcursor(cursor:wait)
!            save_esn_id = access:esnmodel.savefile()
!            access:esnmodel.clearkey(esn:esn_only_key)
!            esn:esn = Sub(f_esn,1,6)
!            set(esn:esn_only_key,esn:esn_only_key)
!            loop
!                if access:esnmodel.next()
!                   break
!                end !if
!                if esn:esn <> Sub(f_esn,1,6)      |
!                    then break.  ! end if
!                yldcnt# += 1
!                if yldcnt# > 25
!                   yield() ; yldcnt# = 0
!                end !if
!                count# += 1
!                If count# > 1
!                    Break
!                End!If count# > 1
!            end !loop
!            access:esnmodel.restorefile(save_esn_id)
!            setcursor()    
!            
!            If count# = 1
!                access:esnmodel.clearkey(esn:esn_only_key)
!                esn:esn = Sub(f_esn,1,6)
!                if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign
!                    f_new_model_number    = esn:model_number
!                    f_pass = 1
!                end!if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign
!            End!If count# = 1
!            
!            If count# > 1
!
!                saverequest#      = globalrequest
!                globalresponse    = requestcancelled
!                globalrequest     = selectrecord
!                glo:select7 = f_esn
!                select_esn_model
!                glo:select7 = ''
!                if globalresponse = requestcompleted
!                    f_new_model_number = esn:model_number
!                    f_pass = 1
!                end
!                globalrequest     = saverequest#
!
!            End!If count# > 1
!        End!If job:model_number <> ''
!
!        If add_model# = 1
!            get(esnmodel,0)
!            if access:esnmodel.primerecord() = Level:Benign
!                esn:esn           = f_esn
!                esn:model_number  = f_new_model_number
!                access:esnmodel.insert()
!            end!if access:esnmodel.primerecord() = Level:Benign
!        End!If add_model# = 1
!    End!If f_esn <> 'N/A'
!    


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'IMEIModelRoutine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:IMEI',tmp:IMEI,'IMEIModelRoutine',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
