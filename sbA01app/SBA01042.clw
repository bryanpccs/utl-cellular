

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01042.INC'),ONCE        !Local module procedure declarations
                     END


PricingStructure     PROCEDURE  (func:AccountNumber,func:ModelNumber,func:UnitType,func:ChargeType,func:RepairType) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'PricingStructure')      !Add Procedure to Log
  end


!Is there a pricing structure setup?
    Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
    sta:Model_Number = func:ModelNumber
    sta:Charge_Type  = func:ChargeType
    sta:Unit_Type    = func:UnitType
    Set(sta:Model_Number_Charge_Key,sta:Model_Number_Charge_Key)
    If Access:STDCHRGE.NEXT()

    Else !If Access:STDCHRGE.NEXT()
        If sta:Model_Number = func:ModelNumber      |
        And sta:Charge_Type  = func:ChargeType      |
        And sta:Unit_Type    = func:UnitType
            Return Level:Benign
        End !Or sta:Unit_Type    <> func:UnitType

    End !If Access:STDCHRGE.NEXT()

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            Access:TRACHRGE.ClearKey(trc:Account_Charge_Key)
            trc:Account_Number = tra:Account_Number
            trc:Model_Number   = func:ModelNumber
            trc:Charge_Type    = func:ChargeType
            trc:Unit_Type      = func:UnitType
            Set(trc:Account_Charge_Key,trc:Account_Charge_Key)
            If Access:TRACHRGE.NEXT()

            Else !If Access:TRACHRGE.NEXT()
                If trc:Account_Number = tra:Account_Number      |
                And trc:Model_Number   = func:ModelNumber      |
                And trc:Charge_Type    = func:ChargeType      |
                And trc:Unit_Type      = func:UnitType
                    Return Level:Benign
                End !Or trc:Unit_Type      <> func:UnitType
            End !If Access:TRACHRGE.NEXT()

            If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                suc:Account_Number = func:AccountNumber
                suc:Model_Number   = func:ModelNumber
                suc:Charge_Type    = func:ChargeType
                suc:Unit_Type      = func:UnitType
                Set(suc:Model_Repair_Type_Key,suc:Model_Repair_Type_Key)
                If Access:SUBCHRGE.NEXT()

                Else !If Access:SUBCHRGE.NEXT()
                    If suc:Account_Number = func:AccountNumber      |
                    And suc:Model_Number   = func:ModelNumber      |
                    And suc:Charge_Type    = func:ChargeType      |
                    And suc:Unit_Type      = func:UnitType
                        Return Level:Benign
                    End !Or suc:Unit_Type      <> func:UnitType

                End !Loop

            End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Fatal


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PricingStructure',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
