

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01038.INC'),ONCE        !Local module procedure declarations
                     END


MakePartsRequest     PROCEDURE  (func:Supplier,func:PartNumber,func:Description,func:Quantity) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'MakePartsRequest')      !Add Procedure to Log
  end


    access:ORDPEND.clearkey(ope:Supplier_Key)
    ope:Supplier    = func:Supplier
    ope:Part_Number = func:PartNumber
    If access:ORDPEND.tryfetch(ope:Supplier_Key) = Level:Benign
        !Found
        ope:Quantity    += func:Quantity
        Access:ORDPEND.Update()
    Else! If access:ORDPEND.tryfetch(ope:Supplier_Key) = Level:Benign
        !Error
        !Assert(0,'Fetch Error')
        If Access:ORDPEND.PrimeRecord() = Level:Benign
            !ope:Part_Ref_Number = sto:Ref_Number
            ope:Part_Type       = 'STO'
            ope:Supplier        = func:Supplier
            ope:Part_Number     = func:PartNumber
            ope:Description     = func:Description
            !ope:Job_Number      = ''
            ope:Quantity        = func:Quantity

            If Access:ORDPEND.Tryinsert()
                Access:ORDPEND.CancelAutoInc()
            End!If access:ORDPEND.tryinsert()
        End!If access:ORDPEND.primerecord() = Level:Benign
    End! If access:ORDPEND.tryfetch(ope:Supplier_Key) = Level:Benign
    


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'MakePartsRequest',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
