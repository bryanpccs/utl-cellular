

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01007.INC'),ONCE        !Local module procedure declarations
                     END


Weekend_Routine      PROCEDURE  (f_date,f_days,f_end_date) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Weekend_Routine')      !Add Procedure to Log
  end


    Set(defaults)
    access:defaults.next()
    f_end_date = f_date
    count# = 0
    Loop
        f_end_date += 1
        If def:include_saturday <> 'YES'
            If (f_end_date) % 7 = 6
                Cycle
            End!If (ord:date + f_end_date) % 7 = 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If (f_end_date) % 7 = 0
                Cycle
            End!If (ord:date + f_end_date) % 7 = 6
        End!If def:include_saturday <> 'YES'
        count# += 1
        If count# >= f_days
            Break
        End
    End!Loop


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Weekend_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
