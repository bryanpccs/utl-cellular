

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01012.INC'),ONCE        !Local module procedure declarations
                     END


CheckPricing         PROCEDURE  (f_type)              ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CheckPricing')      !Add Procedure to Log
  end


    If job:chargeable_job = 'YES' and f_type = 'C'
        If job:account_number <> '' And job:charge_type <> '' And |
            job:model_number <> '' And job:unit_type <> '' And job:repair_type <> ''
                Pricing_Routine('C',clab",cpar",cpass",a")
                If cpass" = 0
                    Case MessageEx('Error! No Pricing Structure.<13,10><13,10>The selected combination of Trade Account, Charge Type, Model Number, Unit Type and Repair Types does not have a Pricing Structure setup.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return(1)
                Else!If cpass" = 0
                    If (job:labour_cost <> clab") Or (job:parts_cost <> cpar")
                        Return(2)
                    End!If (job:labour_cost <> clab") Or (job:parts_cost <> wpar")
                End!If cpass" = 0
        End!If job:account_number <> '' And job:charge_type <> '' And |
    End!If job:chargeable_job = 'YES'

    If job:warranty_job = 'YES' and f_type = 'W'
        If job:account_number <> '' And job:warranty_charge_type <> '' And |
            job:model_number <> '' And job:unit_type <> '' And job:repair_type_warranty <> ''
                Pricing_Routine('W',wlab",wpar",wpass",a")
                If wpass" = 0
                    Case MessageEx('Error! No Pricing Structure.<13,10><13,10>The selected combination of Trade Account, Charge Type, Model Number, Unit Type and Repair Types does not have a Pricing Structure setup.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return(1)
                Else!If cpass" = 0
                    If (job:labour_cost_warranty <> wlab") Or (job:parts_cost_warranty <> wpar")
                        Return(2)
                    End!If (job:labour_cost <> clab") Or (job:parts_cost <> wpar")
                End!If cpass" = 0
        End!If job:account_number <> '' And job:charge_type <> '' And |
    End!If job:chargeable_job = 'YES'

    Return(0)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CheckPricing',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
