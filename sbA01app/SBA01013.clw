

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01013.INC'),ONCE        !Local module procedure declarations
                     END


Set_EDI              PROCEDURE  (f_manufacturer)      ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Set_EDI')      !Add Procedure to Log
  end


    !This Function is no longer used.
    !It has been superceeded by PendingJob function
    !Any reference to set_EDI will need to be removed
    Return job:EDI
!    !Is this an EDI manufacturer
!    Case f_manufacturer
!        Of 'ALCATEL'
!            Return('EDI')
!        Of 'BOSCH'
!            Return('EDI')
!        Of 'ERICSSON'
!            Return('EDI')
!        Of 'MAXON'
!            Return('EDI')
!        Of 'MOTOROLA'
!            Return('EDI')
!        Of 'NEC'
!            Return('EDI')
!        Of 'NOKIA'
!            Return('EDI')
!        Of 'SIEMENS'
!            Return('EDI')
!        Of 'SAMSUNG'
!            Return('EDI')
!        Of 'MITSUBISHI'
!            Return('EDI')
!        Of 'TELITAL'
!            Return('EDI')
!        Of 'SAGEM'
!            Return('EDI')
!        Of 'SONY'
!            Return('EDI')
!        Of 'PANASONIC'
!            Return('EDI')
!        Else
!            Return('XXX')
!    End



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Set_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
