

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01061.INC'),ONCE        !Local module procedure declarations
                     END


CheckFaultFormat     PROCEDURE  (func:FaultCode,func:FieldFormat) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:MonthStart       LONG
tmp:MonthValue       LONG
tmp:YearStart        LONG
tmp:YearValue        LONG
tmp:StartQuotes      LONG
tmp:FaultYearStart   LONG
tmp:FaultMonthStart  LONG
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CheckFaultFormat')      !Add Procedure to Log
  end


!    If Len(Clip(StripQuotes(func:FieldFormat))) <> Len(Clip(func:FaultCode))
!        Return Level:Fatal
!    End !If Len(Clip(StripQuote(func:FieldFormat))) <> Len(Clip(func:FaultCode))

    y# = 1
    tmp:MonthStart = 0
    tmp:YearStart = 0
    Loop x# = 1 To Len(Clip(func:FieldFormat))
        !If this is in a quotes just do a straight comparison of characters
        If tmp:StartQuotes
            If Sub(func:FieldFormat,x#,1) = '"'
                !End Of The Quotes
                tmp:StartQuotes = 0
                Cycle
            End !If Sub(func:FieldFormat,x#,1) = '"'
            If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,y#,1)
                Return Level:Fatal
            Else! !If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,x#,1)
                y# += 1
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,x#,1)

        End !If tmp:StartQuotes

        If tmp:YearStart
            If Sub(func:FieldFormat,x#,1) <> 'Y'
                If x# - tmp:YearStart = 4
                    If Sub(func:FaultCode,tmp:FaultYearStart,4) < 1990 Or Sub(func:FaultCode,tmp:FaultYearStart,4) > 2010
                        !Failed Year
                        Return Level:Fatal
                    End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
                End !If x# - tmp:YearStart = 4

                If x# - tmp:YearStart = 2
                    If Sub(func:FaultCode,tmp:FaultYearStart,2) > 10 And Sub(func:FaultCode,tmp:FaultYearStart,2) < 90
                        !Failed Year
                        Return Level:Fatal
                    End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
                End !If x# - tmp:YearStart = 2
                tmp:YearStart = 0
                ! Start Change 4239 BE(1/6/2004)
                !y# += 1
                ! End Change 4239 BE(1/6/2004)
            Else !If Sub(func:FieldFormat,x#,1) <> 'Y'
                ! Start Change 4239 BE(1/6/2004)
                y# += 1
                ! End Change 4239 BE(1/6/2004)
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> 'Y'
        End !If tmp:YearStart

        If tmp:MonthStart
            If Sub(func:FieldFormat,x#,1) <> 'M'
                If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
                    !Month Fail
                    Return Level:Fatal
                End !If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
                tmp:MonthStart = 0
                ! Start Change 4239 BE(1/6/2004)
                !y# += 1
                ! End Change 4239 BE(1/6/2004)
            Else !If Sub(func:FieldFormat,x#,1) <> 'M'
                ! Start Change 4239 BE(1/6/2004)
                y# += 1
                ! End Change 4239 BE(1/6/2004)
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> 'M'
        End !If tmp:MonthStart

        Case Sub(func:FieldFormat,x#,1)
            Of 'A'
                If ~((Val(Sub(func:FaultCode,y#,1)) >= 65 And Val(Sub(func:FaultCode,y#,1)) <= 90) Or |
                    (Val(Sub(func:FaultCode,y#,1)) >= 92 And Val(Sub(func:FaultCode,y#,1)) <= 122))
                    !Alpha Error
                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 65 Or Val(Sub(func:FaultCode,x#,1)) > 90
            Of '0'
                If Val(Sub(func:FaultCode,y#,1)) < 48 Or Val(Sub(func:FaultCode,y#,1)) > 57
                    !Numeric Error

                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 48 Or Val(Sub(func:FaultCode,x#,1)) > 57
            Of 'X'
                If ~((Val(Sub(func:FaultCode,y#,1)) >=48 And Val(Sub(func:FaultCode,y#,1)) <= 57) Or |
                    (Val(Sub(func:FaultCode,y#,1)) >=65 And Val(Sub(func:FaultCode,y#,1)) <=90) Or |
                    (Val(Sub(func:FaultCode,y#,1)) >= 92 And Val(Sub(func:FaultCode,y#,1)) <= 122))
                    !Alphanumeric Error
                    Return Level:Fatal
                End !(Val(Sub(func:FaultCode,x#,1)) >=65 And Val(Sub(func:FaultCode,x#,1)) <=90))
            Of '"'
                tmp:StartQuotes = 1
                Cycle
            Of 'Y'
                SolaceViewVars('Found A Y','x#: ' & x#,'y#: ' & y#,6)
                tmp:YearStart   = x#
                tmp:FaultYearStart  = y#
            Of 'M'
                SolaceViewVars('Found A M','x#: ' & x#,'y#: ' & y#,6)
                tmp:MonthStart  = x#
                tmp:FaultMonthStart = y#
        End !Case Sub(func:FaultCode,x#,1)
        y# += 1
    End !Loop x# = 1 To Len(func:FaultCode)

    If tmp:YearStart
        
        If x# - tmp:YearStart = 4
            If Sub(func:FaultCode,tmp:FaultYearStart,4) < 1990 Or Sub(func:FaultCode,tmp:FaultYearStart,4) > 2010
                !Failed Year
                Return Level:Fatal
            End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
        End !If x# - tmp:YearStart = 4

        If x# - tmp:YearStart = 2
            If Sub(func:FaultCode,tmp:FaultYearStart,2) < 90 And Sub(func:FaultCode,tmp:FaultYearStart,2) > 10
                !Failed Year
                Return Level:Fatal
            End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
        End !If x# - tmp:YearStart = 2
    End !If tmp:YearStart

    If tmp:MonthStart
        If Sub(func:FaultCode,tmp:FaultMonthStart,2) <1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
            !Month Fail
            Return Level:Fatal
        End !If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
    End !If tmp:MonthStart

    Return Level:Benign

        



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CheckFaultFormat',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:MonthStart',tmp:MonthStart,'CheckFaultFormat',1)
    SolaceViewVars('tmp:MonthValue',tmp:MonthValue,'CheckFaultFormat',1)
    SolaceViewVars('tmp:YearStart',tmp:YearStart,'CheckFaultFormat',1)
    SolaceViewVars('tmp:YearValue',tmp:YearValue,'CheckFaultFormat',1)
    SolaceViewVars('tmp:StartQuotes',tmp:StartQuotes,'CheckFaultFormat',1)
    SolaceViewVars('tmp:FaultYearStart',tmp:FaultYearStart,'CheckFaultFormat',1)
    SolaceViewVars('tmp:FaultMonthStart',tmp:FaultMonthStart,'CheckFaultFormat',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
