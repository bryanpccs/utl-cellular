

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01054.INC'),ONCE        !Local module procedure declarations
                     END


AccountAllowedToDespatch PROCEDURE                    ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'AccountAllowedToDespatch')      !Add Procedure to Log
  end


    If job:Chargeable_Job <> 'YES'
        Return Level:Fatal
    Else
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = job:Account_Number
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:Use_Sub_Accounts = 'YES'
                    ! Start Change 4223 BE(29/04/04)
                    !If sub:Despatch_Invoiced_Jobs <> 'YES' And sub:Despatch_Paid_Jobs <> 'YES'
                    IF (((sub:Despatch_Invoiced_Jobs <> 'YES') AND (sub:Despatch_Paid_Jobs <> 'YES')) OR |
                        ((sub:Despatch_Paid_Jobs = 'YES') AND (job:labour_cost +job:parts_cost + job:courier_cost = 0))) THEN
                    ! End Change 4223 BE(29/04/04)
                        Return Level:Fatal
                    End !If sub:Despatch_Invoiced_Jobs <> 'YES' And sub:Despatch_Paid_Jobs <> 
                Else !If tra:Use_Sub_Accounts = 'YES'
                    ! Start Change 4223 BE(29/04/04)
                    !If tra:Despatch_Invoiced_Jobs <> 'YES' And tra:Despatch_Paid_Jobs <> 'YES'
                    IF (((tra:Despatch_Invoiced_Jobs <> 'YES') AND (tra:Despatch_Paid_Jobs <> 'YES')) OR |
                        ((tra:Despatch_Paid_Jobs = 'YES') AND (job:labour_cost +job:parts_cost + job:courier_cost = 0))) THEN
                    ! End Change 4223 BE(29/04/04)
                        Return Level:Fatal
                    End !If tra:Despatch_Invoiced_Jobs <> 'YES' And tra:Despatch_Paid_Jobs <> 'YES'                
                End !If tra:Use_Sub_Accounts = 'YES'
            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    End !job:Chargeable_Job <> 'YES'
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AccountAllowedToDespatch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
