

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01051.INC'),ONCE        !Local module procedure declarations
                     END


AddBouncers          PROCEDURE  (func:RefNumber,func:DateBooked,func:IMEINumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_job_id          USHORT,AUTO
tmp:Count            LONG
save_job2_id         USHORT,AUTO
save_bou_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'AddBouncers')      !Add Procedure to Log
  end


    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    !Clear Blank Entries From Bouncer Table
    Save_bou_ID = Access:BOUNCER.SaveFile()
    Access:BOUNCER.ClearKey(bou:Bouncer_Job_Number_Key)
    bou:Original_Ref_Number = 0
    bou:Bouncer_Job_Number  = 0
    Set(bou:Bouncer_Job_Number_Key,bou:Bouncer_Job_Number_Key)
    Loop
        If Access:BOUNCER.NEXT()
           Break
        End !If
        If bou:Original_Ref_Number <> 0      |
        Or bou:Bouncer_Job_Number  <> 0      |
            Then Break.  ! End If
        Delete(BOUNCER)
    End !Loop
    Access:BOUNCER.RestoreFile(Save_bou_ID)

    !Only check for bouncers if the account number isn't ticked "Exclude From Bouncer"
    If CompleteCheckForBouncer(job:Account_Number)
        job:Bouncer = ''
    Else
        tmp:Count = 0
        If func:IMEINumber <> 'N/A' And func:IMEINumber <> ''
            Setcursor(Cursor:Wait)
            Save_job2_ID = Access:JOBS2_ALIAS.SaveFile()
            Access:JOBS2_ALIAS.ClearKey(job2:ESN_Key)
            job2:ESN = func:IMEINumber
            Set(job2:ESN_Key,job2:ESN_Key)
            Loop
                If Access:JOBS2_ALIAS.NEXT()
                   Break
                End !If
                If job2:ESN <> func:IMEINumber      |
                    Then Break.  ! End If
                If job2:Cancelled = 'YES'
                    Cycle
                End !If job2:Cancelled = 'YES'
                If job2:Exchange_Unit_Number <> ''
                    Cycle
                End !If job:Exchange_Unit_Number <> ''

                If job2:ref_number <> func:RefNumber and job2:Cancelled <> 'YES'
                    If job2:Date_Booked + def:BouncerTime > func:DateBooked And job2:date_booked <= func:DateBooked
                        If Access:BOUNCER.PrimeRecord() = Level:Benign
                            bou:Original_Ref_Number = func:RefNumber
                            bou:Bouncer_Job_Number  = job2:Ref_Number
                            
                            If Access:BOUNCER.TryInsert() = Level:Benign
                                !Insert Successful
                                tmp:Count += 1
                            Else !If Access:BOUNCER.TryInsert() = Level:Benign
                                !Insert Failed
                                ! Start Change BE008 BE(02/07/03)
                                Access:BOUNCER.Cancelautoinc()
                                ! End Change BE008 BE(02/07/03)
                            End !If Access:BOUNCER.TryInsert() = Level:Benign
                        End !If Access:BOUNCER.PrimeRecord() = Level:Benign
                    End!If job2:date_booked + man:warranty_period < Today()
                End!If job2:esn <> job2:ref_number

            End !Loop
            Access:JOBS2_ALIAS.RestoreFile(Save_job2_ID)
            Setcursor()

            If tmp:Count
                If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
                    job:bouncer_type    = 'BOT'
                End!If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
                If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
                    job:bouncer_type    = 'CHA'
                End!If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
                IF job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
                    job:bouncer_type    = 'WAR'
                End!IF job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
                job:bouncer = 'X'
                
                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'JOB MARKED AS BOUNCER'
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:benign
                Case MessageEx('This job has been found to be a Bouncer.'&|
                  '<13,10>'&|
                  '<13,10>It can now only be Invoiced after it has been authorised from the Bouncer List.','ServiceBase 2000',|
                               'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            Else !If tmp:Count
                job:Bouncer = ''
            End !If tmp:Count
        End !If func:IMEINumber <> 'N/A' And func:IMEINumber <> ''

    End !If CompleteCheckForBouncer(job:Account_Number) = Level:Benign

    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = job:Model_Number
    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Found
        job:EDI = PendingJob(mod:Manufacturer)
        job:Manufacturer    = mod:Manufacturer
    Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AddBouncers',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_id',save_job_id,'AddBouncers',1)
    SolaceViewVars('tmp:Count',tmp:Count,'AddBouncers',1)
    SolaceViewVars('save_job2_id',save_job2_id,'AddBouncers',1)
    SolaceViewVars('save_bou_id',save_bou_id,'AddBouncers',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
