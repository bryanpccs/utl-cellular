

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01025.INC'),ONCE        !Local module procedure declarations
                     END


CommonFaults         PROCEDURE  (f_RefNumber)         ! Declare Procedure
save_wpr_id          USHORT,AUTO
tmp:EngineersLocation STRING(30)
save_map_id          USHORT,AUTO
save_par_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CommonFaults')      !Add Procedure to Log
  end


!Get The Engineer's Location

    tmp:EngineersLocation = ''
    access:users.clearkey(use:password_key)
    use:password    = glo:password
    access:users.fetch(use:password_key)
    If use:user_type = 'ENGINEER'
        If job:engineer <> ''
            If use:user_code <> job:engineer                                                    !Check if the user adding the
                beep(beep:systemquestion)  ;  yield()                                           !common fault is the same as
                case message('You are not marked an the Engineer allocated to this job. '&|     !on the job. And ask to change
                        ''&|                                                                    !if not.
                        '||Do you wish to change the Engineer?', |
                        'ServiceBase 2000', icon:question, |
                         button:yes+button:no, button:no, 0)
                of button:yes
                    job:engineer    = use:user_code
                of button:no
                end !case
            Else!If use:user_code <> job:engineer

            End!If use:user_code <> job:engineer
        Else!If job:engineer = ''
            job:engineer = use:user_code
        End!If job:engineer = ''

    End!If use:user_type = 'ENGINEER'
    access:users.clearkey(use:user_code_key)
    use:user_code   = job:engineer
    access:users.tryfetch(use:user_code_key)
    tmp:EngineersLocation   = use:location

!Get The Common Fault
    access:commonfa.clearkey(com:ref_number_Key)
    com:ref_number  = f_refnumber
    If access:commonfa.tryfetch(com:ref_number_key) = Level:Benign
        If com:Chargeable_Job = 'YES'
            If com:Chargeable_Charge_Type <> '' And com:Chargeable_Repair_Type <> ''
                If PricingStructureRepairType(job:Account_Number,job:Model_Number,job:Unit_Type,com:Chargeable_Charge_Type,com:Chargeable_Repair_Type)
                    Case MessageEx('Error! There is no charge structure setup for the selected Common Fault.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return
                End !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,com:Chargeable_Charge_Type,com:Chargeable_Repair_Type)
            End !If com:Chargeable_Charge_Type <> '' And com:Chargeable_Repair_Type <> ''
        End !If com:Chargeable_Job = 'YES'

        If com:Warranty_Job = 'YES'
            If com:Warranty_Charge_Type <> '' And com:Warranty_Repair_Type <> ''
                If PricingStructureRepairType(job:Account_Number,job:Model_Number,job:Unit_Type,com:Warranty_Charge_Type,com:Warranty_Repair_Type)
                    Case MessageEx('Error! There is no charge structure setup for the selected Common Fault.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return
                End !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,com:Chargeable_Charge_Type,com:Chargeable_Repair_Type)
            End !If com:Chargeable_Charge_Type <> '' And com:Chargeable_Repair_Type <> ''
        End !If com:Chargeable_Job = 'YES'


!Add the notes
        access:jobnotes.clearkey(jbn:RefNumberKey)
        jbn:RefNumber   = job:Ref_Number
        If access:jobnotes.tryfetch(jbn:RefNumberKey)
            Get(jobnotes,0)
            If access:jobnotes.primerecord() = Level:Benign
                jbn:refnumber   = job:ref_number
                access:jobnotes.tryinsert()
            End!If access:jobnotes.primerecord() = Level:Benign
        End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
        jbn:engineers_notes     = Clip(jbn:Engineers_Notes) & '<13,10>' & com:engineers_notes
        jbn:invoice_text        = Clip(jbn:Invoice_Text) & '<13,10>' & com:invoice_text
        access:jobnotes.update()

        If job:Chargeable_Job = 'YES' and com:Chargeable_Job = 'NO'
            !Are there any parts attached?
            FoundParts# = 0
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_number
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                FoundParts# = 1
                Break
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
            If FoundParts# = 1
                Case MessageEx('Warning! The selected Common Fault is trying to make this job Warranty Only but you have Chargeable Parts attached.'&|
                  '<13,10>You will need to remove these parts manually.','ServiceBase 2000',|
                               'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            Else
                job:Chargeable_Job = 'NO'
            End !If FoundParts# = 1
        Else  !If job:Chargeable_Job = 'YES' and com:Chargeable_Job = 'NO'
            job:Chargeable_Job = com:Chargeable_Job
        End !If job:Chargeable_Job = 'YES' and com:Chargeable_Job = 'NO'

        If job:Warranty_Job = 'YES' and com:Warranty_Job = 'NO'
            !Are there any parts attached?
            FoundParts# = 0
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                FoundParts# = 1
                Break
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
            If FoundParts# = 1
                Case MessageEx('Warning! The selected Common Fault is trying to make this job Chargeable Only but you have Warranty Parts attached.'&|
                  '<13,10>You will need to remove these parts manually.','ServiceBase 2000',|
                               'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            Else
                job:Warranty_Job = 'NO'
            End !If FoundParts# = 1
        Else  !If job:Chargeable_Job = 'YES' and com:Chargeable_Job = 'NO'
            job:Warranty_Job = com:Warranty_Job
        End !If job:Chargeable_Job = 'YES' and com:Chargeable_Job = 'NO'

        job:repair_type     = com:chargeable_repair_type
        job:repair_type_warranty    = com:warranty_repair_type
        job:charge_type     = com:chargeable_charge_type
        job:warranty_charge_type    = com:warranty_charge_type
        If job:fault_code1  = ''
            job:fault_code1 = com:fault_code1
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 1,  job:Fault_Code1)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code2  = ''
            job:fault_code2 = com:fault_code2
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 2,  job:Fault_Code2)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code3  = ''
            job:fault_code3 = com:fault_code3
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 3,  job:Fault_Code3)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code4  = ''
            job:fault_code4 = com:fault_code4
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 4,  job:Fault_Code4)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code5  = ''
            job:fault_code5 = com:fault_code5
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 5,  job:Fault_Code5)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code6  = ''
            job:fault_code6 = com:fault_code6
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 6,  job:Fault_Code6)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code7  = ''
            job:fault_code7 = com:fault_code7
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 7,  job:Fault_Code7)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code8  = ''
            job:fault_code8 = com:fault_code8
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 8,  job:Fault_Code8)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code9  = ''
            job:fault_code9 = com:fault_code9
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 9,  job:Fault_Code9)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code10 = ''
            job:fault_code10 = com:fault_code10
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 10,  job:Fault_Code10)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code11  = ''
            job:fault_code11 = com:fault_code11
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 11,  job:Fault_Code11)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        If job:fault_code12  = ''
            job:fault_code12 = com:fault_code12
            ! Start Change 3336 BE(10/10/03)
            CommonFaultNotes(job:ref_number, job:manufacturer, 12,  job:Fault_Code12)
            ! End Change 3336 BE(10/10/03)
        End!If job:fault_code1  = ''
        nostockc# = 0
        nostockw# = 0

        nostockc# = CommonFaultsChar(f_refnumber,tmp:EngineersLocation)
        nostockw# = CommonFaultsWar(f_refnumber,tmp:EngineersLocation)

        If nostockc# Or nostockw#
            Case MessageEx('One or more of the Parts you have just attached to this job are not in Stock and will appear on the next Order Generation.','ServiceBase 2000',|
                           'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        End !If nostockc# Or nostockw#

        get(audit,0)
        If access:audit.primerecord() = level:benign
            aud:notes         = 'CATEGORY: ' & Clip(com:category) & |
                                '<13,10>DESCRIPTION: ' & Clip(com:description)
            aud:ref_number    = job:ref_number
            aud:date          = today()
            aud:time          = clock()
            aud:type          = 'JOB'
            access:users.clearkey(use:password_key)
            use:password = glo:password
            access:users.fetch(use:password_key)
            aud:user = use:user_code
            aud:action        = 'INSERTED COMMON FAULT'
            access:audit.insert()
        End!if access:audit.primerecord() = level:benign

!Check The Fault Codes
        glo:errortext = ''
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number  <> job:ref_number      |
                then break.  ! end if
            save_map_id = access:manfaupa.savefile()
            access:manfaupa.clearkey(map:field_number_key)
            map:manufacturer = job:manufacturer
            set(map:field_number_key,map:field_number_key)
            loop
                if access:manfaupa.next()
                   break
                end !if
                if map:manufacturer <> job:manufacturer      |
                    then break.  ! end if
                Case map:field_number
                    Of 1
                        If map:compulsory = 'YES' and wpr:fault_code1 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''
                    Of 2
                        If map:compulsory = 'YES' and wpr:fault_code2 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''
                    Of 3
                        If map:compulsory = 'YES' and wpr:fault_code3 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 4
                        If map:compulsory = 'YES' and wpr:fault_code4 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 5
                        If map:compulsory = 'YES' and wpr:fault_code5 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 6
                        If map:compulsory = 'YES' and wpr:fault_code6 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 7
                        If map:compulsory = 'YES' and wpr:fault_code7 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 8
                        If map:compulsory = 'YES' and wpr:fault_code8 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 9
                        If map:compulsory = 'YES' and wpr:fault_code9 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 10
                        If map:compulsory = 'YES' and wpr:fault_code10 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 11
                        If map:compulsory = 'YES' and wpr:fault_code11 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 12
                        If map:compulsory = 'YES' and wpr:fault_code12 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                End!Case map:field_number
            end !loop
            access:manfaupa.restorefile(save_map_id)
        end !loop
        access:warparts.restorefile(save_wpr_id)

        If glo:ErrorText <> ''
            glo:ErrorText = 'Common Faults Warning!:<13,10>' & glo:ErrorText
            Error_Text
            glo:ErrorText = ''
        End!If glo:ErrorText <> ''

        If job:ignore_warranty_charges <> 'YES'
            Pricing_Routine('W',labour",parts",pass",a")
            If pass" = True
                job:labour_cost_warranty = labour"
                job:parts_cost_warranty  = parts"
                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
            End!If pass" = False
        End!If job:ignore_warranty_charges <> 'YES'

        If job:ignore_Chargeable_charges <> 'YES'
            Pricing_Routine('C',labour",parts",pass",a")
            If pass" = True
                job:labour_cost = labour"
                job:parts_cost  = parts"
                job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
            End!If pass" = False
        End!If job:ignore_warranty_charges <> 'YES'

    Else!If access:commonfa.tryfetch(com:ref_number_key) = Level:Benign
        Case MessageEx('Unable to find the selected Common Fault.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
    End!If access:commonfa.tryfetch(com:ref_number_key) = Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CommonFaults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_wpr_id',save_wpr_id,'CommonFaults',1)
    SolaceViewVars('tmp:EngineersLocation',tmp:EngineersLocation,'CommonFaults',1)
    SolaceViewVars('save_map_id',save_map_id,'CommonFaults',1)
    SolaceViewVars('save_par_id',save_par_id,'CommonFaults',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
