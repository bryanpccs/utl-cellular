

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01047.INC'),ONCE        !Local module procedure declarations
                     END


StripReturnToComma   PROCEDURE  (in_string)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'StripReturnToComma')      !Add Procedure to Log
  end


    in_string = CLIP(LEFT(in_string))

    STR_LEN#  = LEN(in_string)
    STR_POS#  = 1

    in_string = UPPER(SUB(in_string,STR_POS#,1)) & SUB(in_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF VAL(SUB(in_string,STR_POS#,1)) < 32 Or VAL(SUB(in_string,STR_POS#,1)) > 126
        in_string = SUB(in_string,1,STR_POS#-1) & ',' & SUB(in_string,STR_POS#+1,STR_LEN#-1)
     .
    .

    RETURN(in_string)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'StripReturnToComma',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
