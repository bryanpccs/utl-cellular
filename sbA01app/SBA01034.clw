

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01034.INC'),ONCE        !Local module procedure declarations
                     END


PartsChanging        PROCEDURE  (func:OldPartNumber,func:NewPartNumber,func:OldDescription,func:NewDescription,func:OldSaleCost,func:NewSaleCost,func:OldPurchaseCost,func:NewPurchaseCost,func:OldRetailCost,func:NewRetailCost,func:OShelfLoc,func:NShelfLoc,func:OSecLoc,func:NSecLoc) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_loc_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
save_ccp_id          USHORT,AUTO
save_stm_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
pos                  STRING(255)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! Moving Bar Window
RejectRecord         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO

Progress:Thermometer BYTE
ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER,FONT('Arial',8,,)
     END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'PartsChanging')      !Add Procedure to Log
  end


?    Message('Part Changing Routine','Debug Message',icon:exclamation)


    RecordsPerCycle     = 25
    RecordsProcessed    = 0
    PercentProgress     = 0
    Setcursor(cursor:wait)
    Open(ProgressWindow)
    Progress:Thermometer    = 0
    ?Progress:PctText{prop:text} = '0% Completed'
    ?Progress:UserString{prop:text} = 'Updating Stock'

    RecordsToProcess = Records(LOCATION)

    Save_loc_ID = Access:LOCATION.SaveFile()
    Set(loc:Location_Key)
    Loop
        If Access:LOCATION.NEXT()
           Break
        End !If

        Do GetNextRecord2

        Save_sto_ID = Access:STOCK.SaveFile()
        Access:STOCK.ClearKey(sto:Location_Key)
        sto:Location    = loc:Location
        sto:Part_Number = func:OldPartNumber
        Set(sto:Location_Key,sto:Location_Key)
        Loop
            If Access:STOCK.NEXT()
               Break
            End !If
            If sto:Location    <> loc:Location      |
            Or sto:Part_Number <> func:OldPartNumber      |
                Then Break.  ! End If

            If func:OldPartNumber <> func:NewPartNumber
                pos = Position(sto:Location_Key)
                sto:Part_Number = func:NewPartNumber            
                Access:STOCK.Update()
                Reset(sto:Location_Key,pos)
            End!If func:OldPartNumber <> func:NewPartNumber

            If func:OldDescription <> func:NewDescription
                sto:Description = func:NewDescription            
            End!If func:OldDescription <> func:NewDescription


            If func:OldSaleCost <> func:NewSaleCost            
                sto:Sale_Cost   = func:NewSaleCost
            End!If func:OldSaleCost <> func:NewSaleCost

            If func:OldPurchaseCost <> func:NewPurchaseCost            
                sto:Purchase_Cost   = func:NewPurchaseCost            
            End!If func:OldPurchaseCost <> func:NewPurchaseCost

            If func:OldRetailCost <> func:NewRetailCost
                sto:Retail_Cost     = func:NewRetailCost            
            End!If func:OldRetailCost <> func:NewRetailCost

            If func:OShelfLoc <> func:NShelfLoc
                sto:Shelf_Location  = func:NShelfLoc
            End !If func:OShelfLoc <> func:NShelfLoc

            If func:OSecLoc <> func:NSecLoc
                sto:Second_Location = func:NSecLoc
            End !If func:OSecLoc <> func:NSecLoc

            Access:STOCK.Update()

            get(stohist,0)
            if access:stohist.primerecord() = level:benign
                shi:ref_number           = sto:ref_number
                access:users.clearkey(use:password_key)
                use:password              = glo:password
                access:users.fetch(use:password_key)
                shi:user                  = use:user_code    
                shi:date                 = today()
                shi:transaction_type     = 'ADD'
                shi:despatch_note_number = ''
                shi:job_number           = ''
                shi:quantity             = 0
                shi:purchase_cost        = STO:PURCHASE_COST
                shi:sale_cost            = STO:SALE_COST
                shi:retail_cost          = STO:RETAIL_COST
                shi:information          = 'PREVIOUS DETAILS:-'
                If func:OldPartNumber <> func:NewPartNumber
                    shi:Information = Clip(shi:Information) & '<13,10>PART NUMBER: ' & CLip(func:OldPartNumber)
                End!If func:OldPartNumber <> func:NewPartNumber
                If func:OldDescription <> func:NewDescription
                    shi:Information = Clip(shi:Information) & '<13,10>DESCRIPTION: ' & CLip(func:OldDescription)
                End!If func:OldDescription <> func:NewDescription
                If func:OldPurchaseCost <> func:NewPurchaseCost
                    shi:Information = Clip(shi:Information) & '<13,10>PURCHASE COST: ' & CLip(func:OldPurchaseCost)
                End!If func:OldPurchaseCost <> func:NewPurchaseCost
                If func:OldSaleCost <> func:NewSaleCost
                    shi:Information = Clip(shi:Information) & '<13,10>TRADE PRICE: ' & CLip(func:OldSaleCost)
                End!If func:OldSaleCost <> func:NewSaleCost
                If func:OldRetailCost <> func:NewRetailCost
                    shi:Information = Clip(shi:Information) & '<13,10>RETAIL PRICE: ' & CLip(func:OldRetailCost)
                End!If func:OldRetailCost <> func:NewRetailCost
                If func:OShelfLoc <> func:NShelfLoc
                    shi:Information = Clip(shi:Information) & '<13,10>RETAIL PRICE: ' & CLip(func:OShelfLoc)
                End !If func:OShelfLoc <> func:NShelfLoc
                If func:OSecLoc <> func:NSecLoc
                    shi:Information = Clip(shi:Information) & '<13,10>RETAIL PRICE: ' & CLip(func:OSecLoc)
                End !If func:OSecLoc <> func:NSecLoc

                shi:notes                = 'DETAILS CHANGED'
                if access:stohist.insert()
                   access:stohist.cancelautoinc()
                end
            end!if access:stohist.primerecord() = level:benign

            Save_stm_ID = Access:STOMODEL.SaveFile()
            Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
            stm:Ref_Number   = sto:Ref_Number
            Set(stm:Mode_Number_Only_Key,stm:Mode_Number_Only_Key)
            Loop
                If Access:STOMODEL.NEXT()
                   Break
                End !If
                If stm:Ref_Number   <> sto:Ref_Number      |
                    Then Break.  ! End If
                If func:OldPartNumber <> func:NewPartNumber
                    stm:Part_Number = func:NewPartNumber            
                End!If func:OldPartNumber <> func:NewPartNumber

                If func:OldDescription <> func:NewDescription
                    stm:Description = func:NewDescription            
                End!If func:OldDescription <> func:NewDescription

                Access:STOMODEL.Update()
            End !Loop
            Access:STOMODEL.RestoreFile(Save_stm_ID)


        End !Loop
        Access:STOCK.RestoreFile(Save_sto_ID)

    End !Loop
    Access:LOCATION.RestoreFile(Save_loc_ID)

    Progress:Thermometer    = 0
    RecordsPerCycle     = 25
    RecordsProcessed    = 0
    PercentProgress     = 0

    ?Progress:UserString{prop:text} = 'Updating Chargeable Common Faults'

    RecordsToProcess = Records(COMMONCP)

    Save_ccp_ID = Access:COMMONCP.SaveFile()
    Access:COMMONCP.ClearKey(ccp:PartNumberKey)
    ccp:Part_Number = func:OldPartNumber
    Set(ccp:PartNumberKey,ccp:PartNumberKey)
    Loop
        If Access:COMMONCP.NEXT()
           Break
        End !If
        Do GetNextRecord2
        If ccp:Part_Number <> func:OldPartNumber      |
            Then Break.  ! End If
        If func:OldPartNumber <> func:NewPartNumber
            pos = Position(ccp:PartNumberKey)
            ccp:Part_Number = func:NewPartNumber            
            Access:COMMONCP.Update()
            Reset(ccp:PartNumberKey,pos)
        End!If func:OldPartNumber <> func:NewPartNumber

        If func:OldDescription <> func:NewDescription
            ccp:Description = func:NewDescription            
        End!If func:OldDescription <> func:NewDescription

        If func:OldSaleCost <> func:NewSaleCost            
            ccp:Sale_Cost   = func:NewSaleCost
        End!If func:OldSaleCost <> func:NewSaleCost

        If func:OldPurchaseCost <> func:NewPurchaseCost            
            ccp:Purchase_Cost   = func:NewPurchaseCost            
        End!If func:OldPurchaseCost <> func:NewPurchaseCost

        Access:COMMONCP.Update()
    End !Loop
    Access:COMMONCP.RestoreFile(Save_ccp_ID)

    ?Progress:UserString{prop:text} = 'Updating Warranty Common Faults'
    Progress:Thermometer    = 0
    RecordsPerCycle     = 25
    RecordsProcessed    = 0
    PercentProgress     = 0

    RecordsToProcess = Records(COMMONWP)

    Save_cwp_ID = Access:COMMONWP.SaveFile()
    Access:COMMONWP.ClearKey(cwp:PartNumberKey)
    cwp:Part_Number = func:OldPartNumber
    Set(cwp:PartNumberKey,cwp:PartNumberKey)
    Loop
        If Access:COMMONWP.NEXT()
           Break
        End !If
        If cwp:Part_Number <> func:OldPartNumber      |
            Then Break.  ! End If
        Do GetNextRecord2
        If func:OldPartNumber <> func:NewPartNumber
            pos = Position(cwp:PartNumberKey)
            cwp:Part_Number = func:NewPartNumber            
            Access:COMMONWP.Update()
            Reset(cwp:PartNumberKey,pos)
        End!If func:OldPartNumber <> func:NewPartNumber

        If func:OldDescription <> func:NewDescription
            cwp:Description = func:NewDescription            
        End!If func:OldDescription <> func:NewDescription

        If func:OldSaleCost <> func:NewSaleCost            
            cwp:Sale_Cost   = func:NewSaleCost
        End!If func:OldSaleCost <> func:NewSaleCost

        If func:OldPurchaseCost <> func:NewPurchaseCost            
            cwp:Purchase_Cost   = func:NewPurchaseCost            
        End!If func:OldPurchaseCost <> func:NewPurchaseCost

        Access:COMMONWP.Update()
    End !Loop
    Access:COMMONWP.RestoreFile(Save_cwp_ID)



    Setcursor()
    Close(ProgressWindow)
GetNextRecord2      Routine
  RecordsProcessed += 1
  RecordsThisCycle += 1
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
    IF PercentProgress <> Progress:Thermometer THEN
      Progress:Thermometer = PercentProgress
      ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & '% Completed'
      DISPLAY()
    END
  END
EndPrintRun         Routine
    Progress:Thermometer = 100
    ?Progress:PctText{Prop:Text} = '100% Completed'
    Close(ProgressWindow)
    DISPLAY()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PartsChanging',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_loc_id',save_loc_id,'PartsChanging',1)
    SolaceViewVars('save_cwp_id',save_cwp_id,'PartsChanging',1)
    SolaceViewVars('save_ccp_id',save_ccp_id,'PartsChanging',1)
    SolaceViewVars('save_stm_id',save_stm_id,'PartsChanging',1)
    SolaceViewVars('save_sto_id',save_sto_id,'PartsChanging',1)
    SolaceViewVars('pos',pos,'PartsChanging',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
