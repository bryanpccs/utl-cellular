

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01004.INC'),ONCE        !Local module procedure declarations
                     END


Check_Access         PROCEDURE  (f_area,f_access)     ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Check_Access')      !Add Procedure to Log
  end


   Relate:USERS_ALIAS.Open
   Relate:ACCAREAS_ALIAS.Open
! security check
    f_access    = False
    If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
        f_access = True
    Else!If password = 'JOB:ENTER'
        access:users_alias.clearkey(use_ali:password_key)
        use_ali:password    = glo:password
        If access:users_alias.fetch(use_ali:password_key) = Level:Benign
            access:accareas_alias.clearkey(acc_ali:access_level_key)
            acc_ali:user_level = use_ali:user_level
            acc_ali:access_area = f_area
            If access:accareas_alias.fetch(acc_ali:access_level_key) = Level:Benign
                f_access = True
            End
        End
    End!If password = 'JOB:ENTER'
   Relate:USERS_ALIAS.Close
   Relate:ACCAREAS_ALIAS.Close


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Check_Access',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
