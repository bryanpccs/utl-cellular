

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01053.INC'),ONCE        !Local module procedure declarations
                     END


CheckLengthNoError   PROCEDURE  (f_type,f_ModelNumber,f_number) ! Declare Procedure
save_man_id          USHORT
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CheckLengthNoError')      !Add Procedure to Log
  end


!Return The Correct Length Of A Model Number
    access:modelnum.clearkey(mod:model_number_key)
    mod:model_number = f_ModelNumber
    if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
        If f_number <> 'N/A'
            Case f_type
                Of 'IMEI'
                    If Len(Clip(f_number)) < mod:esn_length_from Or Len(Clip(f_number)) > mod:esn_length_to
                        Return Level:Fatal
                    End!If Clip(Len(f_number)) < mod:esn_length_from Or Clip(Len(f_number)) > mod:esn_length_to


                Of 'MSN'
                    If Len(Clip(f_number)) < mod:msn_length_from Or len(clip(f_number)) > mod:msn_length_to
                        Return Level:Fatal
                    End!If Clip(Len(f_number)) < mod:esn_length_from
            End!Case f_type
        ! Start Change 2651 BE(02/06/03)
        ELSE !If f_number <> 'N/A'
            IF (f_type = 'MSN') THEN
                save_man_id = access:manufact.savefile()
                access:manufact.clearkey(man:manufacturer_key)
                man:manufacturer = mod:manufacturer
                IF (access:manufact.tryfetch(man:manufacturer_key) = Level:Benign) THEN
                    IF (man:DisallowNA) THEN
                            RETURN Level:Fatal
                    END
                END
                access:manufact.restorefile(save_man_id)
            END
        ! End Change 2651 BE(02/06/03)
        End!If f_number <> 'N/A'

    End!if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CheckLengthNoError',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_man_id',save_man_id,'CheckLengthNoError',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
