

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01048.INC'),ONCE        !Local module procedure declarations
                     END


ValidatePostcode     PROCEDURE  (f_Postcode)          ! Declare Procedure
tmp:os               STRING(20)
szpostcode           CSTRING(20)
szpath               CSTRING(255)
szaddress            CSTRING(255)
tmp:high             BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
full_postcode       String(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'ValidatePostcode')      !Add Procedure to Log
  end


!Postcode bit
    Setclipboard(f_postcode)
    Set(Defaults)
    access:defaults.next()
    tmp:os  = GetVersion()
    tmp:high    = Bshift(tmp:os,-8)

    If def:use_postcode = 'YES'

        If def:postcodedll <> 'YES'

            If tmp:high = 0
                Run(Clip(def:postcode_path) & '\addressc.exe',0)
            Else!If tmp:high = 0
                Run(Clip(def:postcode_path) & '\addressc.exe',1)
            End!If tmp:high = 0
            if error()
                Return Level:Fatal
            Else
                IF Sub(Clipboard(),1,1) = '#'
                    Return Level:Fatal
                end!if error
            End
        Else!If def:postcodedll <> 'YES'
            szpath  = Clip(def:postcode_path)
            If Initaddress(szpath)
                szpostcode  = f_postcode
                If getaddress(szpostcode,szaddress)
                    full_postcode   = szaddress
                Else!
                    Return Level:Fatal
                End!If getaddress(szpostcode,szaddress)
            Else!If Initaddress(szpath)
                Return Level:Fatal
            End!If Initaddress(szpath)
        End!If def:postcodedll <> 'YES'
    End
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ValidatePostcode',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:os',tmp:os,'ValidatePostcode',1)
    SolaceViewVars('szpostcode',szpostcode,'ValidatePostcode',1)
    SolaceViewVars('szpath',szpath,'ValidatePostcode',1)
    SolaceViewVars('szaddress',szaddress,'ValidatePostcode',1)
    SolaceViewVars('tmp:high',tmp:high,'ValidatePostcode',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
