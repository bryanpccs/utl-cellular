

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01073.INC'),ONCE        !Local module procedure declarations
                     END


CountMobileBouncer   PROCEDURE  (f_RefNumber,f_DateBooked,f_MobileNumber) ! Declare Procedure
tmp:count            LONG
save_job2_id         USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CountMobileBouncer')      !Add Procedure to Log
  end


! Start Code CountMobileNumber BE(23/06/03)

    tmp:count    = 0
    len# = LEN(CLIP(f_MobileNumber))
    mobile_error# = 0
    IF (len# = 0) THEN
        mobile_error# = 1
    ELSE
        LOOP ix# = 1 TO len#
            IF ((f_MobileNumber[ix#] < '0') OR (f_MobileNumber[ix#] > '9')) THEN
                mobile_error# = 1
                BREAK
            END
        END
    END

    IF ((mobile_error# = 0) AND (f_MobileNumber <> 0)) THEN
        SET(defaults)
        Access:Defaults.Next()

        SETCURSOR(cursor:wait)
        save_job2_id = access:jobs2_alias.savefile()
        access:jobs2_alias.clearkey(job2:MobileNumberKey)
        job2:Mobile_Number = f_MobileNumber
        SET(job2:MobileNumberKey,job2:MobileNumberKey)
        LOOP
            IF ((access:jobs2_alias.next() <> Level:benign) OR (job2:Mobile_Number <> f_MobileNumber)) THEN
                BREAK
            END

            ! start Change BE010 BE(02/07/03)
            !IF ((job2:Cancelled = 'YES') OR (job2:Exchange_Unit_NUmber <> '')) THEN
            IF (job2:Cancelled = 'YES') THEN
            ! End Change BE010 BE(02/07/03)
                CYCLE
            END

            IF ((job2:ref_number <> f_refnumber) AND |
                (job2:date_booked + def:bouncertime > f_datebooked) AND |
                (job2:date_booked <= f_datebooked)) THEN
                tmp:count += 1
                IF (tmp:count = 99) THEN
                    BREAK
                END
            END
        END
        access:jobs2_alias.restorefile(save_job2_id)
        SETCURSOR()
    END
    RETURN tmp:count
! End Code CountMobileNumber BE(23/06/03)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CountMobileBouncer',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:count',tmp:count,'CountMobileBouncer',1)
    SolaceViewVars('save_job2_id',save_job2_id,'CountMobileBouncer',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
