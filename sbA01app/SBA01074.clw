

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01074.INC'),ONCE        !Local module procedure declarations
                     END


CommonFaultNotes     PROCEDURE  (f_refnumber, f_manufacturer, f_fieldno, f_field) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CommonFaultNotes')      !Add Procedure to Log
  end


    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = f_manufacturer
    maf:field_number = f_fieldno
    IF (access:manfault.fetch(maf:field_number_key) = Level:Benign) THEN
         IF ((Maf:ReplicateFault = 'YES') OR (Maf:ReplicateInvoice = 'YES')) THEN
            access:manfaulo.clearkey(mfo:field_key)
            mfo:manufacturer = maf:manufacturer
            mfo:field_number = maf:field_number
            mfo:field = f_field
            IF (access:manfaulo.fetch(mfo:field_key) = Level:Benign) THEN
                access:jobnotes.clearkey(jbn:RefNumberKey)
                jbn:RefNumber = f_refnumber
                IF (access:jobnotes.fetch(jbn:RefNumberKey) <> Level:Benign) THEN
                    access:jobnotes.primerecord()
                    jbn:RefNumber = f_refnumber
                    access:jobnotes.tryinsert()
                    access:jobnotes.clearkey(jbn:RefNUmberKey)
                    jbn:RefNumber = f_refnumber
                    access:jobnotes.tryfetch(jbn:RefNumberKey)
                END
                IF (Maf:ReplicateFault = 'YES') THEN
                    IF (jbn:fault_description = '') THEN
                        jbn:fault_description = CLIP(mfo:description)
                    ELSE
                        jbn:fault_description = CLIP(jbn:fault_description) & '<13,10>' & CLIP(mfo:description)
                    END
                END
                IF (Maf:ReplicateInvoice = 'YES') THEN
                    IF (jbn:invoice_text = '') THEN
                        jbn:invoice_Text = CLIP(mfo:description)
                    ELSE
                        jbn:invoice_text = CLIP(jbn:invoice_Text) & '<13,10>' & CLIP(mfo:description)
                    END
                END
                access:jobnotes.update()
            END
        END
    END


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CommonFaultNotes',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
