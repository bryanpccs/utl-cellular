

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01010.INC'),ONCE        !Local module procedure declarations
                     END


CheckRefurb          PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CheckRefurb')      !Add Procedure to Log
  end


    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type    = job:transit_type
    If access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
        If trt:exchange_unit    = 'YES' Or job:exchange_unit_number <> '' Or Sub(job:exchange_status,1,3) = '108'
            access:subtracc.clearkey(sub:account_number_key)
            sub:account_number  = job:account_number
            If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                access:tradeacc.clearkey(tra:account_number_key)
                tra:account_number  = sub:main_account_number
                If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                    If tra:refurbcharge = 'YES'
                        Return Level:Fatal
                    End!If tra:refurbcharge = 'YES'
                End!If access:tradeacc.tryfetch(tra:account_number_key)
            End!If access:subtracc.tryfetch(sub:account_number_key)
        End!If trt:exchange_unit    = 'YES' Or job:exchange_unit_number <> '' Or Sub(job:exchange_status,1,3) = '108'
    End!If access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CheckRefurb',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
