

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01024.INC'),ONCE        !Local module procedure declarations
                     END


TradeList            PROCEDURE  (f_type)              ! Declare Procedure
save_inv_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'TradeList')      !Add Procedure to Log
  end


    Case f_type
        Of 'INVOICE'
            recordspercycle     = 25
            recordsprocessed    = 0
            percentprogress     = 0
            setcursor(cursor:wait)
            open(progresswindow)
            progress:thermometer    = 0
            ?progress:pcttext{prop:text} = '0% Completed'
            ?progress:userstring{prop:text} = 'Building Account List'
            
            recordstoprocess    = Records(invoice)

            set(tradetmp)
            loop
                if access:tradetmp.next()
                   break
                end !if
                Delete(tradetmp)
            end !loop
            
            setcursor(cursor:wait)

            save_inv_id = access:invoice.savefile()
            access:invoice.clearkey(inv:Account_Number_Key)
            set(inv:Account_Number_Key)
            loop
                if access:invoice.next()
                   break
                end !if
                Do getNextRecord2

                access:tradetmp.clearkey(tratmp:accountnokey)
                tratmp:account_number = inv:Account_Number
                if access:tradetmp.tryfetch(tratmp:accountnokey)
                    get(tradetmp,0)
                    if access:tradetmp.primerecord() = Level:Benign
                        tratmp:account_number = inv:Account_Number
                        if access:tradetmp.insert()
                           access:tradetmp.cancelautoinc()
                        end
                    end!if access:tradetmp.primerecord() = Level:Benign
                end!if access:tradetmp.tryfetch(tratmp:accountnokey)
                
            end !loop
            access:invoice.restorefile(save_inv_id)
            
            setcursor()
            close(progresswindow)
    End!Case f_type
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'TradeList',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_inv_id',save_inv_id,'TradeList',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
