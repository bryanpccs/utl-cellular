

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01075.INC'),ONCE        !Local module procedure declarations
                     END


RemoveBlankRecords PROCEDURE                          !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('File Maintenance'),AT(,,220,90),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       PROMPT('Blank records in files can produced the following error:'),AT(8,6),USE(?Prompt1),FONT(,,,FONT:bold)
                       PROMPT('"Attempts to automatically number this record have failed"'),AT(17,22),USE(?Prompt2),FONT(,,,FONT:italic)
                       PROMPT('This routine will remove blanks from the following files: AUDIT, AUDSTATS, JOBS,' &|
   ' TRADEACC and SUBTRACC.'),AT(16,36,188,26),USE(?Prompt3),CENTER,FONT(,,COLOR:Navy,FONT:bold)
                       SHEET,AT(4,2,212,84),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                         END
                       END
                       BUTTON('Remove Blanks'),AT(20,66,56,16),USE(?Button1),LEFT,ICON('ok.ico')
                       BUTTON('Cancel'),AT(144,66,56,16),USE(?Button2),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RemoveBlankRecords',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button1;  SolaceCtrlName = '?Button1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button2;  SolaceCtrlName = '?Button2';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RemoveBlankRecords')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'RemoveBlankRecords')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Access:AUDSTATS.UseFile
  Access:JOBS.UseFile
  Access:MODELNUM.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'RemoveBlankRecords',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
      Case MessageEx('Are you sure you want to begin?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,14084079,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Audit# = 0
              Access:AUDIT.ClearKey(aud:Record_Number_Key)
              aud:record_number = 0
              If Access:AUDIT.TryFetch(aud:Record_Number_Key) = Level:Benign
                  !Found
                  Delete(AUDIT)
                  Audit# = 1
              Else !If Access:AUDIT.TryFetch(aud:Record_Number_Key) = Level:Benign
                  !Error
              End !If Access:AUDIT.TryFetch(aud:Record_Number_Key) = Level:Benign
      
              AudStats# = 0
              Access:AUDSTATS.ClearKey(aus:RecordNumberKey)
              aus:RecordNumber = 0
              If Access:AUDSTATS.TryFetch(aus:RecordNumberKey) = Level:Benign
                  !Found
                  Delete(AUDSTATS)
                  AudStats# = 1
              Else !If Access:AUDSTATS.TryFetch(aus:RecordNumberKey) = Level:Benign
                  !Error
              End !If Access:AUDSTATS.TryFetch(aus:RecordNumberKey) = Level:Benign
      
              Jobs# = 0
              Access:JOBS.ClearKey(job:Ref_Number_Key)
              job:Ref_Number = 0
              If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                  !Found
                  Delete(JOBS)
                  Jobs# = 1
              Else !If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      
              TradeAcc# = 0
              Access:TRADEACC.ClearKey(tra:RecordNumberKey)
              tra:RecordNumber = 0
              If Access:TRADEACC.TryFetch(tra:RecordNumberKey) = Level:Benign
                  !Found
                  Delete(TRADEACC)
                  TradeAcc# = 1
              Else !If Access:TRADEACC.TryFetch(tra:RecordNumberKey) = Level:Benign
                  !Error
              End !If Access:TRADEACC.TryFetch(tra:RecordNumberKey) = Level:Benign
      
              SubTracc# = 0
              Access:SUBTRACC.ClearKey(sub:RecordNumberKey)
              sub:RecordNumber = 0
              If Access:SUBTRACC.TryFetch(sub:RecordNumberKey) = Level:Benign
                  !Found
                  Delete(SUBTRACC)
                  SubTracc# = 1
              Else !If Access:SUBTRACC.TryFetch(sub:RecordNumberKey) = Level:Benign
                  !Error           
              End !If Access:SUBTRACC.TryFetch(sub:RecordNumberKey) = Level:Benign
      
              Case MessageEx('Records Deleted:'&|
                '<13,10>'&|
                '<13,10>AUDIT: '&Audit#&|
                '<13,10>AUDSTATS: '&AudStats#&|
                '<13,10>JOBS: '&Jobs#&|
                '<13,10>TRADEACC: '&TradeAcc#&|
                '<13,10>SUBTRACC: '&SubTracc#,'ServiceBase 2000',|
                             'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,14084079,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'RemoveBlankRecords')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

