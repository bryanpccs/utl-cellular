

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01050.INC'),ONCE        !Local module procedure declarations
                     END


PendingJob           PROCEDURE  (func:Manufacturer)   ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'PendingJob')      !Add Procedure to Log
  end


    ! Use the Manufacturer's EDI Type - TrkBs: 5855 (DBH: 28-09-2005)
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = func:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        ! Found
        If man:EDIFileType    = 'ALCATEL' Or  |
            man:EDIFileType   = 'BOSCH' Or    |
            man:EDIFileType   = 'ERICSSON' Or |
            man:EDIFileType   = 'MAXON' Or    |
            man:EDIFileType   = 'MOTOROLA' Or |
            man:EDIFileType   = 'NEC' Or      |
            man:EDIFileType   = 'NOKIA' Or    |
            man:EDIFileType   = 'SHARP' Or  |
            man:EDIFileType   = 'SIEMENS' Or  |
            man:EDIFileType   = 'SIEMENS DECT' Or  |
            man:EDIFileType   = 'SAMSUNG' Or  |
            man:EDIFileType   = 'MITSUBISHI' Or   |
            man:EDIFileType   = 'TELITAL' Or  |
            man:EDIFileType   = 'SAGEM' Or    |
            man:EDIFileType   = 'SONY' Or     |
            man:EDIFileType   = 'SENDO' Or     |  ! Change 3524 BE(15/01/04)
            man:EDIFileType   = 'VK' Or       |  ! Change 4018 BE(06/04/04)
            man:EDIFileType   = 'LG' Or       |  ! Change 3516 BE(27/09/04)
            man:EDIFileType   = 'PANASONIC' Or |
            man:EDIFileType   = 'STANDARD FORMAT'

            !Job should never become an EDI exception by mistake,
            !So no need to doublt check if edi is set to exceptinon.

            If job:EDI = 'EXC'
                Return 'EXC'
            End !If job:EDI_Batch_Number = 'EXC'

            !Do the calculation to check if a job should appear in the
            !Pending Claims Table, i.e. job:EDI = 'NO'

            !Job must be completed, a warranty job, not already have a batch number
            !not be an exception, not be a bouncer
            !and not be excluded (by Charge Type or Repair Type)

            If job:Date_Completed <> ''
                If job:Warranty_Job = 'YES'
                    If job:EDI_Batch_Number = 0
                        If job:EDI <> 'EXC'
                            If ~(job:Bouncer = 'R' Or (job:Bouncer <> '' And (job:Bouncer_Type = 'BOT' Or job:Bouncer_Type = 'WAR')))
                                Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
                                cha:Charge_Type = job:Warranty_Charge_Type
                                If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                                    !Found
                                    If cha:Exclude_EDI <> 'YES'
                                        Access:REPTYDEF.Clearkey(rtd:Warranty_Key)
                                        rtd:Warranty = 'YES'
                                        rtd:Repair_Type = job:Repair_Type_Warranty
                                        If Access:REPTYDEF.Tryfetch(rtd:Warranty_Key) = Level:Benign
                                            !Found
                                            If ~rtd:ExcludeFromEDI
                                                !Message('Return NO')

                                                ! Start Change 2905 BE(15/07/03)
                                                !Return 'NO'
                                                IF ((job:Cancelled = 'YES') AND |
                                                     GETINI('VALIDATE','ExcludeCancelledJobs',,CLIP(PATH())&'\SB2KDEF.INI')) THEN
                                                    RETURN 'XXX'
                                                ELSE
                                                    RETURN 'NO'
                                                END
                                                ! End Change 2905 BE(15/07/03)

                                            End !If ~rtd:ExcludeFromEDI
                                        Else! If Access:REPTYDEF.Tryfetch(rtd:Warranty_Key) = Level:Benign
                                            !Error
                                            !Assert(0,'<13,10>Fetch Error<13,10>')
                                        End! If Access:REPTYDEF.Tryfetch(rtd:Warranty_Key) = Level:Benign
                                    End !If cha:Exclude_EDI = 'YES'
                                Else! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                            End !If job:Bouncer = ''
                        End !If job:EDI <> 'EXC'
                    Else !If job:EDI_Batch_Number = 0
                        !This is an error check.
                        !If a job has a batch number then there are only 3 things it can be:
                        !FIN = Invoiced, 'YES' = Reconciled, or 'EXC: Exception.
                        If job:Invoice_Number_Warranty <> ''
                            Return 'FIN'
                        Else !If job:Invoice_Number_Warranty <> ''
                            Return 'YES'
                        End !If job:Invoice_Number_Warranty <> ''
                    End !If job:EDI_Batch_Number = 0
                End !If job:Warranty_Job = 'YES'
            End !If job:Date_Completed <> ''
        End !If man:EDIFileType    = 'ALCATEL' Or  |

    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        ! Error
    End ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign


    !This is to make sure that the EDI value doesn't change
    !once it's set.

    !Reconciled
    If job:EDI = 'YES'
        Return 'YES'
    End !If job:EDI_Batch_Number = 'YES'

    !Invoiced
    If job:EDI = 'FIN'
        Return 'FIN'
    End !If job:EDI_Batch_Number = 'FIN'

    !If all else fails then this should be a warranty claim
    Return 'XXX'



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PendingJob',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
