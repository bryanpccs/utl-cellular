

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01065.INC'),ONCE        !Local module procedure declarations
                     END


IMEIModelMismatch PROCEDURE (func:IMEI,func:ModelNumber,func:ActualModel) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:IMEI             STRING(30)
tmp:ModelNumber      STRING(30)
tmp:ActualModelNumber STRING(30)
tmp:Return           BYTE(0)
window               WINDOW('I.M.E.I. / Model Number Mismatch'),AT(,,231,158),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,124),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('There is a mismatch between the selected I.M.E.I. Number and Model Number.'),AT(36,8,188,20),USE(?Prompt1),FONT(,,,FONT:bold)
                           IMAGE('Stop.ico'),AT(8,52),USE(?Image1)
                           PROMPT('I.M.E.I. Number:'),AT(36,32),USE(?Prompt2)
                           STRING(@s30),AT(36,40),USE(tmp:IMEI),FONT(,,,FONT:bold)
                           STRING(@s15),AT(148,72),USE(tmp:ModelNumber),LEFT,FONT(,10,COLOR:Green,FONT:bold)
                           STRING(@s30),AT(148,100,74,11),USE(tmp:ActualModelNumber),LEFT,FONT(,10,COLOR:Navy,FONT:bold)
                           BUTTON,AT(144,68,80,16),USE(?Button3:2),TRN
                           PROMPT('Do you wish to:?'),AT(36,52,164,12),USE(?Prompt5),FONT(,,,FONT:bold)
                           PROMPT('a) Ignore Mismatch - Use Entered Model Number:'),AT(36,68,104,20),USE(?Prompt2:2),LEFT
                           PROMPT('b) Change Model Number - Use Corresponding Model:'),AT(36,96,104,24),USE(?Prompt4),LEFT
                           BUTTON,AT(144,96,80,16),USE(?Button3),TRN
                         END
                       END
                       PANEL,AT(4,132,224,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(168,136,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?tmp:IMEI{prop:FontColor} = -1
    ?tmp:IMEI{prop:Color} = 15066597
    ?tmp:ModelNumber{prop:FontColor} = -1
    ?tmp:ModelNumber{prop:Color} = 15066597
    ?tmp:ActualModelNumber{prop:FontColor} = -1
    ?tmp:ActualModelNumber{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt2:2{prop:FontColor} = -1
    ?Prompt2:2{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'IMEIModelMismatch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:IMEI',tmp:IMEI,'IMEIModelMismatch',1)
    SolaceViewVars('tmp:ModelNumber',tmp:ModelNumber,'IMEIModelMismatch',1)
    SolaceViewVars('tmp:ActualModelNumber',tmp:ActualModelNumber,'IMEIModelMismatch',1)
    SolaceViewVars('tmp:Return',tmp:Return,'IMEIModelMismatch',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:IMEI;  SolaceCtrlName = '?tmp:IMEI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber;  SolaceCtrlName = '?tmp:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ActualModelNumber;  SolaceCtrlName = '?tmp:ActualModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3:2;  SolaceCtrlName = '?Button3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('IMEIModelMismatch')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'IMEIModelMismatch')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  tmp:IMEI    = func:IMEI
  tmp:ModelNumber = func:ModelNumber
  tmp:ActualModelNumber = func:ActualModel
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'IMEIModelMismatch',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button3:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3:2, Accepted)
      tmp:Return = 1
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3:2, Accepted)
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      tmp:Return = 2
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Return = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'IMEIModelMismatch')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

