

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01026.INC'),ONCE        !Local module procedure declarations
                     END


CommonFaultsChar     PROCEDURE  (f_refnumber,f_location) ! Declare Procedure
Local                CLASS
PartAlreadyAttached  Procedure(Long func:RefNumber,String func:PartNumber),Byte
                     END
tmp_RF_Board_IMEI    STRING(20)
tmp:engstockrefnumber LONG
tmp:engstockquantity LONG
tmp:mainstockrefnumber LONG
tmp:mainstockquantity LONG
tmp:engstocksundry   BYTE(0)
tmp:mainstocksundry  BYTE(0)
tmp:mainstore        STRING(30)
tmp:ordered          BYTE(0)
save_ccp_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_par_ali_id      USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CommonFaultsChar')      !Add Procedure to Log
  end


!Add Chargeable Common Fault Parts
    setcursor(cursor:wait)
    save_ccp_id = access:commoncp.savefile()
    access:commoncp.clearkey(ccp:ref_number_key)
    ccp:ref_number = f_refnumber
    set(ccp:ref_number_key,ccp:ref_number_key)
    loop
        if access:commoncp.next()
           break
        end !if
        if ccp:ref_number <> f_refnumber      |
            then break.  ! end if

        !TH 09/02/04
        Access:LOCATION.ClearKey(loc:ActiveLocationKey)
        loc:Active   = 1
        loc:Location = f_location
        If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.

        ! Start Change 4652 BE(2/09/2004)
        !if loc:PickNoteEnable then
        tmp_RF_Board_IMEI = ''
        if NOT loc:PickNoteEnable then
        ! End Change 4652 BE(2/09/2004)
            ! Start Change 2116 BE(15/07/03)
            tmp_RF_Board_IMEI = ''
            access:stock.clearkey(sto:location_key)
            sto:location    = f_location
            sto:part_number = ccp:part_number
            IF (access:stock.tryfetch(sto:location_key) = Level:Benign) AND |
                (sto:RF_BOARD) THEN
                glo:select2 = ''
                glo:select3 = job:model_number
                glo:select4 = cwp:part_number
                SETCURSOR()
                EnterNewIMEI
                !SETCURSOR(cursor:wait)
                tmp_RF_Board_IMEI = CLIP(glo:select2)
                glo:select2 = ''
                glo:select3 = ''
                glo:select4 = ''
                IF (tmp_RF_Board_IMEI = '') THEN
                    CYCLE
                END
            END
            ! End Change 2116 BE(15/07/03)
        end

        If ccp:exclude_from_order = 'YES'
            If access:parts.primerecord() = Level:Benign
                par:ref_number           = job:ref_number
                par:adjustment           = ccp:adjustment
                par:part_number          = CCP:Part_Number
                par:description          = CCP:Description
                par:supplier             = CCP:Supplier
                par:purchase_cost        = CCP:Purchase_Cost
                par:sale_cost            = CCP:Sale_Cost
                par:quantity             = CCP:Quantity
                par:fault_codes_checked  = 'YES'
                par:fault_code1    = ccp:fault_code1
                par:fault_code2    = ccp:fault_code2
                par:fault_code3    = ccp:fault_code3
                par:fault_code4    = ccp:fault_code4
                par:fault_code5    = ccp:fault_code5
                par:fault_code6    = ccp:fault_code6
                par:fault_code7    = ccp:fault_code7
                par:fault_code8    = ccp:fault_code8
                par:fault_code9    = ccp:fault_code9
                par:fault_code10   = ccp:fault_code10
                par:fault_code11   = ccp:fault_code11
                par:fault_code12   = ccp:fault_code12
                par:exclude_from_order   = ccp:exclude_from_order
                par:date_ordered         = Today()
                access:PARTS.tryinsert()
            End!If access:parts.primerecord() = Level:Benign
        Else!If ccp:exclude_from_order = 'YES'
            !Clear the variables
            tmp:engstockrefnumber   = 0
            tmp:engstockquantity    = 0
            tmp:engstocksundry      = 0
            tmp:mainstockrefnumber  = 0
            tmp:mainstockquantity   = 0
            tmp:mainstocksundry     = 0

            !Find the part at the Engineers Location
            access:stock.clearkey(sto:location_key)
            sto:location    = f_location
            sto:part_number = ccp:part_number
            if access:stock.tryfetch(sto:location_key) = Level:Benign
                !Part Number has been found in the Engineers' Location
                !Try and match Model Number
                access:stomodel.clearkey(stm:model_number_key)
                stm:ref_number   = sto:ref_number
                stm:manufacturer = job:manufacturer
                stm:model_number = job:model_number
                if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
                    !Found Part At Engineers Location
                    If sto:Suspend
                        tmp:engstockrefnumber   = 0
                        tmp:engstockquantity    = 0
                    Else !If sto:Suspend
                        tmp:engstockrefnumber   = sto:ref_number
                        tmp:engstockquantity    = sto:quantity_stock
                        If sto:sundry_item = 'YES'
                            tmp:engstocksundry = 1
                        Else!If sto:sundry_item = 'YES'
                            tmp:engstocksundry = 0
                        End!If sto:sundry_item = 'YES'

                    End !If sto:Suspend
                Else!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
                    !Part Number does not match Model Number
                End!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
            Else!if access:stock.tryfetch(sto:location_key) = Level:Benign
                !Part Number NOT found at engineer's location
            End!if access:stock.tryfetch(sto:location_key) = Level:Benign

            If tmp:engstockrefnumber = 0 !And tmp:mainstockrefnumber = 0
                Case MessageEx('Cannot add the following part to this job. It has been suspended or cannot be found in the Engineer''s Loation:<13,10><13,10>'&Clip(ccp:part_number)&'<13,10>'&Clip(ccp:description),'ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            Else
                tmp:ordered = 0
                Do AddParts
                
            End!If tmp:engstockrefnumber = 0 And tmp:mainstockrefnumber = 0


        End!If ccp:exclude_from_order = 'YES'

        ! Start Change 2116 BE(15/07/03)
        IF (tmp_RF_Board_IMEI <> '') THEN
            GET(audit,0)
            IF (access:audit.primerecord() = level:benign) THEN
                aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                    '<13,10>Old IMEI ' & Clip(job:esn)
                aud:ref_number    = job:ref_number
                aud:date          = Today()
                aud:time          = Clock()
                access:users.clearkey(use:password_key)
                use:password =glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                IF (access:audit.insert() <> Level:benign) THEN
                    access:audit.cancelautoinc()
                END
            END
            access:JOBSE.clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                IF (access:JOBSE.primerecord() = Level:Benign) THEN
                    jobe:RefNumber  = job:Ref_Number
                    IF (access:JOBSE.tryinsert()) THEN
                        access:JOBSE.cancelautoinc()
                    END
                END
            END
            IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                jobe:PRE_RF_BOARD_IMEI = job:ESN
                access:jobse.update()
            END
            job:ESN = tmp_RF_Board_IMEI
            access:jobs.update()
        END
        ! End Change 2116 BE(15/07/03)

    end !loop
    access:commoncp.restorefile(save_ccp_id)
    setcursor()
    Return tmp:ordered
AddParts        Routine

    Set(Defaults)
    access:Defaults.next()

    !Is this part already attached?

    !Only check if the part exists in stock
    Found# = 0
    If tmp:EngStockRefNumber
        If local.PartAlreadyAttached(tmp:EngStockRefNumber,ccp:Part_Number)
            Found# = 1
        End !If local.PartAlreadyAttached(tmp:EngStockRefNumber,ccp:Part_Number)
    End !If tmp:EngStockRefNumber

    If Found# = 0

        
        If access:parts.primerecord() = Level:Benign
            !Insert a New Part
            par:quantity             = CCP:Quantity
            Do AddPartDetails

            If tmp:engstockrefnumber <> 0
                !Engineers Part Exists
                If tmp:engstocksundry
                    !If part is a sundry, don't take from stock and just add it.
                    par:part_ref_number = tmp:engstockrefnumber
                    par:date_ordered    = Today()
                    access:parts.tryinsert()
                Else!If tmp:engstocksundry
                    If ccp:quantity > tmp:engstockquantity
                        !The Quantity Required is more than in the Engineers Location
                        If tmp:engstockquantity = 0
                            !If there is NO engineers stock
                            If tmp:mainstockrefnumber <> 0
                                !If there is stock in Main Stock
                                If par:quantity > tmp:mainstockquantity
                                    !If Stock required is more than Main Stock Quantity
                                    If access:ordpend.primerecord() = Level:Benign
                                        ope:part_ref_number = tmp:engstockrefnumber
                                        ope:job_number      = job:ref_number
                                        ope:part_type       = 'JOB'
                                        ope:supplier        = par:supplier
                                        ope:part_number     = par:part_number
                                        If tmp:mainstockquantity = 0
                                            !If there is NO Main Store stock
                                            !Order all the stock for the engineer part
                                            ope:quantity        = ccp:quantity
                                            access:ordpend.insert()
                                            tmp:ordered = 1
                                            par:pending_ref_number  = ope:ref_number
                                            par:part_ref_number = tmp:engstockrefnumber
                                            access:parts.insert()
                                            if def:PickNoteNormal or def:PickNoteMainStore then
                                                CreatePickingNote (sto:Location)
                                                InsertPickingPart ('chargeable')
                                            end
                                        Else!If tmp:mainstockquantity = 0
                                            !If there is some Main Store Stock, but not enough
                                            !make an order for the difference, and change the
                                            !part's quantity to match what was taken from Main Store
                                            ope:quantity    = ccp:quantity - tmp:mainstockquantity
                                            access:ordpend.insert()
                                            tmp:ordered = 1
                                            par:quantity        = tmp:mainstockquantity
                                            par:date_ordered    = Today()
                                            par:part_ref_number = tmp:mainstockquantity
                                            access:parts.insert()
                                            if def:PickNoteNormal or def:PickNoteMainStore then
                                                CreatePickingNote (sto:Location)
                                                InsertPickingPart ('chargeable')
                                            end

                                            !Take the item from Stock
                                            access:stock.clearkey(sto:ref_number_key)
                                            sto:ref_number      = tmp:mainstockrefnumber
                                            If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                                !Take The quantity from stock
                                                sto:quantity_stock  = 0
                                                access:stock.update()

                                                If access:stohist.primerecord() = Level:Benign
                                                    shi:quantity    = tmp:mainstockquantity
                                                    Do AddStockHistory
                                                End!If access:stohist.primerecord() = Level:Benign
                                            End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign

                                            !Create a new part for the Pending Order
                                            If access:parts.primerecord() = Level:benign
                                                par:quantity    = ccp:quantity - tmp:mainstockquantity
                                                par:pending_ref_number  = ope:ref_number
                                                access:parts.insert()
                                                if def:PickNoteNormal or def:PickNoteMainStore then
                                                    CreatePickingNote (sto:Location)
                                                    InsertPickingPart ('chargeable')
                                                end
                                            End!If access:parts.primerecord() = Level:benign
                                        End!If tmp:mainstockquantity = 0

                                        ! Start Change 4652 BE(02/09/2004)
                                        ! Status 330 Spares Requested
                                        GetStatus(330,2,'JOB')
                                        ! End Change 4652 BE(02/09/2004)

                                     End!If access:ordpend.primerecord() = Level:Benign
                                Else!If par:quantity > tmp:mainstockquantity
                                    !There is sufficent Stock In the Main Store
                                    !Take the item from stock
                                    par:quantity    = ccp:quantity
                                    par:date_ordered    = Today()
                                    par:part_ref_number = tmp:mainstockrefnumber
                                    access:parts.insert()
                                    if def:PickNoteNormal or def:PickNoteMainStore then
                                        CreatePickingNote (sto:Location)
                                        InsertPickingPart ('chargeable')
                                    end

                                    access:stock.clearkey(sto:ref_number_key)
                                    sto:ref_number  = tmp:mainstockrefnumber
                                    If access:stock.tryfetch(sto:ref_number_key) = level:benign
                                        sto:quantity_stock -= ccp:quantity
                                        If sto:quantity_stock < 0
                                            sto:quantity_stock = 0
                                        End!If sto:quantity_stock < 0
                                        access:stock.update()

                                        If access:stohist.primerecord() = Level:benign
                                            shi:quantity    = ccp:quantity
                                            Do AddStockHistory
                                        End!If access:stohist.primerecord() = Level:benign
                                    End!If access:stock.tryfetch(sto:ref_number_key) = level:benign
                                End!If par:quantity > tmp:mainstockquantity
                            Else!If tmp:mainstockrefnumber <> 0
                                !There is No Main Store Stock Item
                                !Create an order from the Engineers Part
                                If access:ordpend.primerecord() = Level:Benign
                                    ope:part_ref_number = tmp:engstockrefnumber
                                    ope:job_number      = job:ref_number
                                    ope:part_type       = 'JOB'
                                    ope:supplier        = par:supplier
                                    ope:part_number     = par:part_number
                                    ope:Description     = par:Description
                                    ope:quantity        = ccp:quantity
                                    access:ordpend.insert()
                                    tmp:ordered = 1
                                    par:quantity        = ccp:quantity
                                    par:pending_ref_number  = ope:ref_number
                                    par:Part_ref_Number = tmp:EngStockRefNumber
                                    access:parts.insert()
                                    if def:PickNoteNormal or def:PickNoteMainStore then
                                        CreatePickingNote (sto:Location)
                                        InsertPickingPart ('chargeable')
                                    end

                                    ! Start Change 4652 BE(02/09/2004)
                                    ! Status 330 Spares Requested
                                    GetStatus(330,2,'JOB')
                                    ! End Change 4652 BE(02/09/2004)

                                End!If access:ordpend.primerecord() = Level:Benign
                            End!If tmp:mainstockrefnumber <> 0
                        Else!If tmp:engstockquantity = 0
                            !There is some stock in the Engineer's Location, but not enough
                            !Take the quantity left from engineers stock
                            par:quantity        = tmp:engstockquantity
                            par:date_ordered    = Today()
                            par:part_ref_number = tmp:engstockrefnumber
                            access:parts.insert()
                            if def:PickNoteNormal or def:PickNoteMainStore then
                                CreatePickingNote (sto:Location)
                                InsertPickingPart ('chargeable')
                            end

                            access:stock.clearkey(sto:ref_number_key)
                            sto:ref_number  = tmp:engstockrefnumber
                            If access:stock.tryfetch(sto:ref_number_key) = level:Benign
                                sto:quantity_stock = 0
                                access:stock.update()

                                If access:stohist.primerecord() = Level:Benign
                                    shi:quantity    = tmp:engstockquantity
                                    Do AddStockHistory
                                End!If access:stohist.primerecord() = Level:Benign
                            End!If access:stock.tryfetch(sto:ref_number_key) = level:Benign

                            !Is there a main store item as well?
                            If tmp:mainstockrefnumber <> 0
                                !There is a main store item, is there enough in stock?
                                If (ccp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                                    !Quantity requested is higher than main store and engineer put together
                                    !Create an order
                                    If access:ordpend.primerecord() = Level:benign
                                        ope:part_ref_number = tmp:engstockrefnumber
                                        ope:job_number      = job:ref_number
                                        ope:part_type       = 'JOB'
                                        ope:supplier        = par:supplier

                                        If access:parts.primerecord() = Level:Benign
                                            Do AddPartDetails
                                            par:part_ref_number = tmp:engstockrefnumber
                                            If tmp:mainstockquantity = 0
                                                !There is no stock in Main Store
                                                par:quantity    = ccp:quantity - tmp:engstockquantity
                                                par:pending_ref_number = ope:ref_number
                                                access:parts.insert()
                                                if def:PickNoteNormal or def:PickNoteMainStore then
                                                    CreatePickingNote (sto:Location)
                                                    InsertPickingPart ('chargeable')
                                                end
                                                ope:quantity    = par:quantity
                                                access:ordpend.insert()
                                                tmp:ordered = 1
                                            Else!If tmp:mainstockquantity = 0
                                                !Take the stock from main store.
                                                !Create an order pending for the rest
                                                par:quantity    = tmp:mainstockquantity
                                                par:date_ordered    = Today()
                                                par:part_ref_number = tmp:mainstockrefnumber
                                                access:parts.insert()
                                                if def:PickNoteNormal or def:PickNoteMainStore then
                                                    CreatePickingNote (sto:Location)
                                                    InsertPickingPart ('chargeable')
                                                end

                                                If access:parts.primerecord() = Level:benign
                                                    Do AddPartDetails
                                                    par:part_ref_number = tmp:engstockrefnumber
                                                    par:quantity        = ccp:quantity - (tmp:engstockquantity + tmp:mainstockquantity)
                                                    par:pending_ref_number  = ope:ref_number
                                                    access:parts.insert()
                                                    if def:PickNoteNormal or def:PickNoteMainStore then
                                                        CreatePickingNote (sto:Location)
                                                        InsertPickingPart ('chargeable')
                                                    end
                                                    ope:quantity        = par:quantity
                                                    access:ordpend.insert()
                                                    tmp:ordered = 1
                                                End!If access:parts.primerecord() = Level:benign

                                                Access:stock.clearkey(sto:ref_number_key)
                                                sto:ref_number  = tmp:mainstockrefnumber
                                                If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                                    sto:quantity_stock = 0
                                                    access:stock.update()
                                                End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                                If access:stohist.primerecord() = Level:Benign
                                                    shi:quantity    = tmp:mainstockquantity
                                                    Do AddStockHistory
                                                End!If access:stohist.primerecord() = Level:Benign
                                            End!If tmp:mainstockquantity = 0
                                        End!If access:parts.primerecord() = Level:Benign

                                        ! Start Change 4652 BE(02/09/2004)
                                        ! Status 330 Spares Requested
                                        GetStatus(330,2,'JOB')
                                        ! End Change 4652 BE(02/09/2004)

                                    End!If access:ordpend.primerecord(0 = Level:benign
                                Else!If (ccp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                                    !The is sufficient stock left in the Main Store
                                    par:quantity    = ccp:quantity - tmp:engstockquantity
                                    par:part_ref_number = tmp:mainstockrefnumber
                                    par:date_ordered    = Today()
                                    access:parts.insert()
                                    if def:PickNoteNormal or def:PickNoteMainStore then
                                        CreatePickingNote (sto:Location)
                                        InsertPickingPart ('chargeable')
                                    end

                                    access:stock.clearkey(sto:ref_number_key)
                                    sto:ref_number  = tmp:mainstockrefnumber
                                    If Access:stock.tryfetch(sto:ref_number_key) = level:Benign
                                        sto:quantity_stock  -= (ccp:quantity - tmp:engstockquantity)
                                        If sto:quantity_stock < 0
                                            sto:quantity_stock = 0
                                        End!If sto:quantity_stock < 0
                                        access:stock.update()
                                    End!If Access:stock.tryfetch(sto:ref_number_key) = level:Benign

                                    If access:stohist.primerecord() = Level:Benign
                                        shi:quantity    = ccp:quantity - tmp:engstockquantity
                                        Do AddStockHistory
                                    End!If access:stohist.primerecord() = Level:Benign
                                End!If (ccp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                            Else!If tmp:mainstockrefnumber <> 0
                                !If there is no Main Store Item
                                !Create an order for the remaining
                                If access:ordpend.primerecord() = Level:Benign
                                    ope:part_ref_number = tmp:engstockrefnumber
                                    ope:job_number      = job:ref_number
                                    ope:part_type       = 'JOB'
                                    ope:supplier        = par:supplier
                                    ope:part_number     = par:part_number
                                    ope:quantity        = ccp:quantity - tmp:engstockquantity
                                    access:ordpend.insert()
                                    tmp:ordered = 1
                                    If access:parts.primerecord() = Level:Benign
                                        Do AddPartDetails
                                        par:pending_ref_number  = ope:ref_number
                                        par:part_ref_number     = tmp:engstockrefnumber
                                        par:quantity            = ccp:quantity - tmp:engstockquantity
                                        access:parts.insert()
                                        if def:PickNoteNormal or def:PickNoteMainStore then
                                            CreatePickingNote (sto:Location)
                                            InsertPickingPart ('chargeable')
                                        end
                                    End!If access:parts.primerecord() = Level:Benign

                                    ! Start Change 4652 BE(02/09/2004)
                                    ! Status 330 Spares Requested
                                    GetStatus(330,2,'JOB')
                                    ! End Change 4652 BE(02/09/2004)

                                End!If access:ordpend.primerecord() = Level:Benign
                            End!If tmp:mainstockrefnumber <> 0
                        End!If tmp:engstockquantity = 0
                    Else!If ccp:quantity > tmp:engstockquantity
                        !If there is enough in the engineer's location
                        par:quantity   = ccp:quantity
                        par:date_ordered    = Today()
                        par:part_ref_number = tmp:engstockrefnumber
                        access:parts.insert()
                        if def:PickNoteNormal or def:PickNoteMainStore then
                            CreatePickingNote (sto:Location)
                            InsertPickingPart ('chargeable')
                        end
                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number  = tmp:engstockrefnumber
                        If access:stock.tryfetch(sto:ref_number_key) = Level:benign
                            sto:quantity_stock -= ccp:quantity
                            If sto:quantity_stock < 0
                                sto:quantity_stock = 0
                            End!If sto:quantity_stock < 0
                            access:stock.update()
                            If access:stohist.primerecord() = level:Benign
                                shi:quantity    = ccp:quantity
                                Do AddStockHistory
                            End!If access:stohist.primerecord() = level:Benign
                        End!If access:stock.tryfetch(sto:ref_number_key) = Level:benign
                    End!If ccp:quantity > tmp:engstockquantity
                End!If tmp:engstocksundry
            Else!If tmp:engstockrefnumber <> 0
            End!If tmp:engstockrefnumber <> 0
        End!If access:parts.primerecord() = Level:Benign
    End !If Found# = 0
AddPartDetails      Routine
    par:ref_number           = job:ref_number
    par:adjustment           = ccp:adjustment
    par:part_number          = CCP:Part_Number
    par:description          = CCP:Description
    par:supplier             = CCP:Supplier
    par:purchase_cost        = CCP:Purchase_Cost
    par:sale_cost            = CCP:Sale_Cost
    
    par:fault_codes_checked  = 'YES'
    If par:fault_Code1 = ''
        par:Fault_Code1         = ccp:fault_Code1
    End!If par:FaultCode1           = ''            
    If par:Fault_Code2          = ''
        par:Fault_Code2         = ccp:fault_Code2
    End!If par:FaultCode2           = ''            
    If par:Fault_Code3          = ''
        par:Fault_Code3         = ccp:fault_Code3
    End!If par:FaultCode3           = ''            
    If par:Fault_Code4          = ''
        par:Fault_Code4         = ccp:fault_Code4
    End!If par:FaultCode4           = ''            
    If par:Fault_Code5          = ''
        par:Fault_Code5         = ccp:fault_Code5
    End!If par:FaultCode5           = ''            
    If par:Fault_Code6          = ''
        par:Fault_Code6         = ccp:fault_Code6
    End!If par:FaultCode6           = ''            
    If par:Fault_Code7          = ''
        par:Fault_Code7         = ccp:fault_Code7
    End!If par:FaultCode7           = ''            
    If par:Fault_Code8          = ''
        par:Fault_Code8         = ccp:fault_Code8
    End!If par:FaultCode8           = ''            
    If par:Fault_Code9          = ''
        par:Fault_Code9         = ccp:fault_Code9
    End!If par:FaultCode9           = ''            
    If par:Fault_Code10         = ''
        par:Fault_Code10        = ccp:fault_Code10
    End!If par:FaultCode10      = ''            
    If par:Fault_Code11         = ''
        par:Fault_Code11        = ccp:fault_Code11
    End!If par:FaultCode11      = ''            
    If par:Fault_Code12         = ''
        par:Fault_Code12        = ccp:fault_Code12
    End
    par:exclude_from_order   = ccp:exclude_from_order
AddStockHistory     Routine
    shi:ref_number           = sto:ref_number
    access:users.clearkey(use:password_key)
    use:password              = glo:password
    access:users.fetch(use:password_key)
    shi:user                  = use:user_code    
    shi:date                 = today()
    shi:transaction_type     = 'DEC'
    shi:despatch_note_number = par:despatch_note_number
    shi:job_number           = job:ref_number
    shi:purchase_cost        = par:purchase_cost
    shi:sale_cost            = par:sale_cost
    shi:retail_cost          = par:retail_cost
    shi:notes                = 'STOCK DECREMENTED'
    if access:stohist.insert()
       access:stohist.cancelautoinc()
    end
!NOT USED
    !            !If there is no engineer's stock
    !            !Is there a main stock item?
    !            If tmp:mainstockrefnumber <> 0
    !                !There is a main store item
    !                If tmp:mainstocksundry
    !                    par:quantity    = tmp:mainstockquantity
    !                    par:date_ordered = Today()
    !                    par:part_ref_number  = tmp:mainstockrefnumber
    !                    access:parts.insert()
    !                Else!If tmp:mainstocksundry
    !                    If ccp:quantity > tmp:mainstockquantity
    !                        !Quantity Required is more than main store quantity
    !                        If access:ordpend.primerecord() = Level:Benign
    !                            ope:part_ref_number     = tmp:mainstockrefnumber
    !                            ope:job_number          = job:ref_number
    !                            ope:part_type           = 'JOB'
    !                            ope:supplier            = par:supplier
    !                            ope:part_number         = par:part_number
    !                            If tmp:mainstockquantity = 0
    !                                !If there is no stock in the main store
    !                                ope:quantity    = ccp:quantity
    !                                access:ordpend.insert()
    !                                tmp:ordered = 1
    !                                par:quantity    = ccp:quantity
    !                                par:pending_ref_number  = ope:ref_number
    !                                par:part_ref_number     = tmp:mainstockrefnumber
    !                                access:parts.insert()
    !                            Else!If tmp:mainstockquantity = 0
    !                                !take the remaining items from main store, and order the rest
    !                                ope:quantity    = ccp:quantity - tmp:mainstockquantity
    !                                access:ordpend.insert()
    !                                tmp:ordered = 1
    !                                par:quantity        = tmp:mainstockquantity
    !                                par:date_ordered    = Today()
    !                                par:part_ref_number = tmp:mainstockrefnumber
    !                                access:parts.insert()
    !                                access:stock.clearkey(sto:ref_number_key)
    !                                sto:ref_number      = tmp:mainstockrefnumber
    !                                If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
    !                                    sto:quantity_stock = 0
    !                                    access:stock.update()
    !                                    If access:stohist.primerecord() = Level:Benign
    !                                        shi:quantity    = tmp:mainstockquantity
    !                                        If access:stohist.tryinsert()
    !                                            access:stohist.cancelautoinc()
    !                                        End!If access:stohist.tryinsert()
    !                                    End!If access:stohist.primerecord() = Level:Benign
    !                                End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
    !                                If access:parts.primerecord() = Level:Benign
    !                                    Do AddPartDetails
    !                                    par:quantity            = ccp:quantity - tmp:mainstockquantity
    !                                    par:pending_ref_number  = ope:ref_number
    !                                    par:part_ref_number     = tmp:mainstockrefnumber
    !                                    If access:parts.tryinsert()
    !                                        access:parts.cancelautoinc()
    !                                    End!If access:parts.tryinsert()
    !                                End!If access:parts.primerecord() = Level:Benign
    !                            End!If tmp:mainstockquantity = 0
    !                        End!If access:ordpend.primerecord() = Level:Benign
    !                    Else!If ccp:quantity > tmp:mainstockquantity
    !                        !If there is enough quantity in the main store
    !                        par:quantity        = ccp:quantity
    !                        par:date_ordered    = Today()
    !                        par:part_ref_number = tmp:mainstockrefnumber
    !                        access:parts.insert()
    !                        access:stock.clearkey(sto:ref_number_key)
    !                        If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
    !                            !Found
    !                            sto:quantity_stock -= ccp:quantity
    !                            If sto:quantity_stock < 0
    !                                sto:quantity_stock = 0
    !                            End!If sto:quantity_stock < 0
    !                            access:stock.update()
    !                        Else! If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
    !                            !Error
    !                        End! If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
    !                        If access:stohist.primerecord() = Level:Benign
    !                            shi:quantity    = ccp:quantity
    !                            Do AddStockHistory
    !                        End!If access:stohist.primerecord() = Level:Benign
    !                    End!If ccp:quantity > tmp:mainstockquantity
    !                End!If tmp:mainstocksundry
    !            Else!If tmp:mainstockrefnumber <> 0
    !                !If there is no main store item
    !                If access:ordpend.primerecord() = Level:Benign
    !                    ope:part_ref_number     = ''
    !                    ope:job_number          = job:ref_number
    !                    ope:part_type           = 'JOB'
    !                    ope:supplier            = par:supplier
    !                    ope:part_number         = par:part_number
    !                    ope:quantity            = par:quantity
    !                    If access:ordpend.tryinsert()
    !                        access:ordpend.cancelautoinc()
    !                    End!If access:ordpend.tryinsert()
    !                    par:pending_ref_number  = ope:ref_number
    !                    access:parts.insert()
    !                End!If access:ordpend.primerecord() = Level:Benign
    !            End!If tmp:mainstockrefnumber <> 0
Local.PartAlreadyAttached       Procedure(Long func:RefNumber,String func:PartNumber)
Code
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = func:RefNumber
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
            If sto:AllowDuplicate = 0
                Save_par_ali_ID = Access:PARTS_ALIAS.SaveFile()
                Access:PARTS_ALIAS.ClearKey(par_ali:Part_Number_Key)
                par_ali:Ref_Number  = job:Ref_Number
                par_ali:Part_Number = func:PartNumber
                Set(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
                Loop
                    If Access:PARTS_ALIAS.NEXT()
                       Break
                    End !If
                    If par_ali:Ref_Number  <> job:Ref_Number      |
                    Or par_ali:Part_Number <> func:PartNumber     |
                        Then Break.  ! End If
                    If par_ali:Date_Received = ''
                        Case MessageEx('Chargeable Part ' & Clip(ccp:Part_Number) & ' is already attached to this job.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Return Level:Fatal
                        Break
                    End !If par:Record_Number <> par_ali:Record_Number
                End !Loop
                Access:PARTS_ALIAS.RestoreFile(Save_par_ali_ID)
            End !If sto:AllowDuplicate = 0
        Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CommonFaultsChar',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Local',Local,'CommonFaultsChar',1)
    SolaceViewVars('tmp_RF_Board_IMEI',tmp_RF_Board_IMEI,'CommonFaultsChar',1)
    SolaceViewVars('tmp:engstockrefnumber',tmp:engstockrefnumber,'CommonFaultsChar',1)
    SolaceViewVars('tmp:engstockquantity',tmp:engstockquantity,'CommonFaultsChar',1)
    SolaceViewVars('tmp:mainstockrefnumber',tmp:mainstockrefnumber,'CommonFaultsChar',1)
    SolaceViewVars('tmp:mainstockquantity',tmp:mainstockquantity,'CommonFaultsChar',1)
    SolaceViewVars('tmp:engstocksundry',tmp:engstocksundry,'CommonFaultsChar',1)
    SolaceViewVars('tmp:mainstocksundry',tmp:mainstocksundry,'CommonFaultsChar',1)
    SolaceViewVars('tmp:mainstore',tmp:mainstore,'CommonFaultsChar',1)
    SolaceViewVars('tmp:ordered',tmp:ordered,'CommonFaultsChar',1)
    SolaceViewVars('save_ccp_id',save_ccp_id,'CommonFaultsChar',1)
    SolaceViewVars('save_loc_id',save_loc_id,'CommonFaultsChar',1)
    SolaceViewVars('save_par_ali_id',save_par_ali_id,'CommonFaultsChar',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
