

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01033.INC'),ONCE        !Local module procedure declarations
                     END


FullAccess           PROCEDURE  (func:AccessNumber,func:Password) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'FullAccess')      !Add Procedure to Log
  end


    If func:AccessNumber = 0
        Return Level:Fatal
    End !If func:AccessNumber = 0

    day#    = Sub(func:AccessNumber,1,5)
    time#   = Sub(func:AccessNumber,6,20)

    ! Start Change 4780 BE(26/10/2004)
    !If func:Password <> INT(((day# * 3)/23) + ((time# * 2)/7))
    If func:Password <> INT(((day# * 3)/19) + ((time# * 2)/9))
    ! End Change 4780 BE(26/10/2004)
        Return Level:Fatal
    End!If func:Password <> INT((((day#*3/23) + ((time#*2)/7)))

    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'FullAccess',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
