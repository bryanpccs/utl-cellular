

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01005.INC'),ONCE        !Local module procedure declarations
                     END


Total_Price          PROCEDURE  (f_type,f_vat,f_total,f_balance) ! Declare Procedure
paid_chargeable_temp REAL
paid_warranty_temp   REAL
labour_rate_temp     REAL
parts_rate_temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_jpt_ali_id   ushort,auto
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Total_Price')      !Add Procedure to Log
  end


    paid_warranty_temp = 0
    paid_chargeable_temp = 0
    save_jpt_ali_id = access:jobpaymt_alias.savefile()
    access:jobpaymt_alias.clearkey(jpt_ali:all_date_key)
    jpt_ali:ref_number = job:ref_number
    set(jpt_ali:all_date_key,jpt_ali:all_date_key)
    loop
        if access:jobpaymt_alias.next()
           break
        end !if
        if jpt_ali:ref_number <> job:ref_number      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        paid_chargeable_temp += jpt_ali:amount
    end !loop
    access:jobpaymt_alias.restorefile(save_jpt_ali_id)

    labour_rate_temp = 0
    parts_rate_temp = 0

    access:subtracc.clearkey(sub:account_number_key)
    sub:account_number = job:account_number
    if access:subtracc.fetch(sub:account_number_key) = Level:Benign
        access:tradeacc.clearkey(tra:account_number_key)
        tra:account_number = sub:main_account_number
        if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
            If tra:invoice_sub_accounts = 'YES'
                access:vatcode.clearkey(vat:vat_code_key)
                vat:vat_code = sub:labour_vat_code
                if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                    labour_rate_temp = vat:vat_rate
                end
                access:vatcode.clearkey(vat:vat_code_key)
                vat:vat_code = sub:parts_vat_code
                if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                    parts_rate_temp = vat:vat_rate
                end
            Else
                access:vatcode.clearkey(vat:vat_code_key)
                vat:vat_code = tra:labour_vat_code
                if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                    labour_rate_temp = vat:vat_rate
                end
                access:vatcode.clearkey(vat:vat_code_key)
                vat:vat_code = tra:parts_vat_code
                if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                    parts_rate_temp = vat:vat_rate
                end

            End!If tra:use_sub_accounts = 'YES'
        end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
    end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign

    Case f_type
        Of 'C' ! Chargeable
            f_vat    = Round(job:labour_cost * (labour_rate_temp/100),.01) + |
                        Round(job:parts_cost * (parts_rate_temp/100),.01) + |
                        Round(job:courier_cost * (labour_rate_temp/100),.01)

            f_total = job:labour_cost + job:parts_cost + job:courier_cost + f_vat
            
            f_balance = f_total - paid_chargeable_temp
        Of 'W' ! Warranty
            f_vat    = Round(job:labour_cost_warranty * (labour_rate_temp/100),.01) + |
                        Round(job:parts_cost_warranty * (parts_rate_temp/100),.01) + |
                        Round(job:courier_cost_warranty * (labour_rate_temp/100),.01)
                        
            f_total = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty + f_vat
            
!            f_balance = f_total - paid_warranty_temp

        Of 'E' ! estimate
            f_vat    = Round(job:labour_cost_estimate * (labour_rate_temp/100),.01) + |
                        Round(job:parts_cost_estimate * (parts_rate_temp/100),.01) + |
                        Round(job:courier_cost_estimate * (labour_rate_temp/100),.01)
                        
            f_total = job:labour_cost_estimate + job:parts_cost_estimate + job:courier_cost_estimate + f_vat
            
            f_balance = 0

        Of 'I' ! Chargeable Invoice

            access:invoice.clearkey(inv:invoice_number_key)
            inv:invoice_number = job:invoice_number
            if access:invoice.fetch(inv:invoice_number_key) = Level:Benign
                f_vat    = Round(job:invoice_labour_cost * (inv:vat_rate_labour/100),.01) + |
                            Round(job:invoice_parts_cost * (inv:vat_rate_parts/100),.01) + |
                            Round(job:invoice_courier_cost * (inv:vat_rate_labour/100),.01)
                            
                f_total = job:invoice_labour_cost + job:invoice_parts_cost + job:invoice_courier_cost + f_vat
                
                f_balance = f_total - paid_chargeable_temp
            end!if access:invoice.fetch(inv:invoice_number_key) = Level:Benign    
        Of 'V' ! Warranty Invoice
            access:invoice.clearkey(inv:invoice_number_key)
            inv:invoice_number = job:invoice_number_warranty
            if access:invoice.fetch(inv:invoice_number_key) = Level:Benign
                f_vat    = Round(job:Winvoice_labour_cost * (inv:vat_rate_labour/100),.01) + |
                            Round(job:Winvoice_parts_cost * (inv:vat_rate_parts/100),.01) + |
                            Round(job:Winvoice_courier_cost * (inv:vat_rate_labour/100),.01)
                f_total = job:Winvoice_labour_cost + job:Winvoice_parts_cost + |
                            job:Winvoice_courier_cost + f_vat
                
                f_balance = f_total - paid_chargeable_temp
            end!if access:invoice.fetch(inv:invoice_number_key) = Level:Benign        
    End!Case f_type
    f_vat = Round(f_vat,.01)
    f_total = Round(f_total,.01)
    f_balance = Round(f_balance,.01)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Total_Price',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('paid_chargeable_temp',paid_chargeable_temp,'Total_Price',1)
    SolaceViewVars('paid_warranty_temp',paid_warranty_temp,'Total_Price',1)
    SolaceViewVars('labour_rate_temp',labour_rate_temp,'Total_Price',1)
    SolaceViewVars('parts_rate_temp',parts_rate_temp,'Total_Price',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
