

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01003.INC'),ONCE        !Local module procedure declarations
                     END


Pricing_Routine      PROCEDURE  (f_type,f_labour,f_parts,f_pass,f_discount) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_par_id   ushort,auto
save_wpr_id   ushort,auto
save_epr_id   ushort,auto
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Pricing_Routine')      !Add Procedure to Log
  end


! Source Bit
    f_labour = 0
    f_parts = 0
    f_pass = 0
    f_discount = ''
! Chargeable Job?
    SolaceViewVars('Account Number: ' & job:Account_Number,'Job Number: ' & job:Ref_Number,,6)
    If job:account_number = ''
        Return
    End
    Case f_type
        Of 'C'
            If job:chargeable_job = 'YES' 
                labour_discount# = 0
                parts_discount#  = 0
                f_pass  = 1
            ! No Charge?
                If job:ignore_chargeable_charges = 'YES'
                    Return
                End
                access:chartype.clearkey(cha:charge_type_key)
                cha:charge_type = job:charge_type
                If access:chartype.fetch(cha:charge_type_key)
                    f_labour    = 0
                    f_parts     = 0
                    f_pass      = 0
                Else !If access:chartype.fetch(cha:charge_type_key)
                    If cha:no_charge = 'YES'
                        f_labour = ''
                        f_parts  = ''

                    Else !If cha:no_charge = 'YES'
                        If cha:zero_parts <> 'YES'
                            parts_total$ = 0
                            save_par_id = access:parts.savefile()
                            access:parts.clearkey(par:part_number_key)
                            par:ref_number  = job:ref_number
                            set(par:part_number_key,par:part_number_key)
                            loop
                                if access:parts.next()
                                   break
                                end !if
                                if par:ref_number  <> job:ref_number      |
                                    then break.  ! end if
                                parts_total$ += par:sale_cost * par:quantity
                            end !loop
                            access:parts.restorefile(save_par_id)
                            f_parts = parts_total$
                        Else !If cha:zero_parts <> 'YES'
                            f_parts = 0

                        End !If cha:zero_parts <> 'YES'

                        access:subtracc.clearkey(sub:account_number_key)
                        sub:account_number = job:account_number
                        if access:subtracc.fetch(sub:account_number_key)
                            Case MessageEx('Error! Cannot find Sub Account Details.','ServiceBase 2000',|
                                           'Styles\stop.ico','',0,0,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            End!Case MessageEx
                        Else!if access:subtracc.fetch(sub:account_number_key)
                            access:tradeacc.clearkey(tra:account_number_key)
                            tra:account_number = sub:main_account_number
                            if access:tradeacc.fetch(tra:account_number_key)
                                Case MessageEx('Error! Cannot find Trade Account Details.','ServiceBase 2000',|
                                               'Styles\stop.ico','',0,0,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                End!Case MessageEx
                            end!if access:tradeacc.fetch(tra:account_number_key)
                        end!if access:subtracc.fetch(sub:account_number_key)
                        use_standard# = 1
                        If TRA:ZeroChargeable = 'YES'
                            f_parts = 0

                        End!If TRA:ZeroChargeable = 'YES'
                        If tra:invoice_sub_accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
                            access:subchrge.clearkey(suc:model_repair_type_key)
                            suc:account_number = job:account_number
                            suc:model_number   = job:model_number
                            suc:charge_type    = job:charge_type
                            suc:unit_type      = job:unit_type
                            suc:repair_type    = job:repair_type
                            if access:subchrge.fetch(suc:model_repair_type_key)
                                access:trachrge.clearkey(trc:account_charge_key)
                                trc:account_number = sub:main_account_number
                                trc:model_number   = job:model_number
                                trc:charge_type    = job:charge_type
                                trc:unit_type      = job:unit_type
                                trc:repair_type    = job:repair_type
                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                                    f_labour    = trc:cost
                                    use_standard# = 0
                                End!if access:trachrge.fetch(trc:account_charge_key)
                            Else
                                f_labour    = suc:cost
                                use_standard# = 0
                            End!if access:subchrge.fetch(suc:model_repair_type_key)
                        Else!If tra:invoice_sub_accounts = 'YES'
                            access:trachrge.clearkey(trc:account_charge_key)
                            trc:account_number = sub:main_account_number
                            trc:model_number   = job:model_number
                            trc:charge_type    = job:charge_type
                            trc:unit_type      = job:unit_type
                            trc:repair_type    = job:repair_type
                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                                f_labour    = trc:cost
                                use_standard# = 0

                            End!if access:trachrge.fetch(trc:account_charge_key)
                        End!If tra:invoice_sub_accounts = 'YES'

                        If use_standard# = 1

                            access:stdchrge.clearkey(sta:model_number_charge_key)
                            sta:model_number = job:model_number
                            sta:charge_type  = job:charge_type
                            sta:unit_type    = job:unit_type
                            sta:repair_type  = job:repair_type
                            if access:stdchrge.fetch(sta:model_number_charge_key)
                                f_labour     = 0
                                f_pass       = 0
                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                                f_labour    = sta:cost
                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
                        End!If use_standard# = 1
                    End !If cha:no_charge = 'YES'
                End !If access:chartype.clearkey(cha:charge_type_key)
            End !If job:chargeable_job = 'YES'

        Of 'W'

        !Warranty Job
            If job:warranty_job = 'YES' 
                parts_discount# = 0
                labour_discount# = 0
                f_pass  = 1
                If job:ignore_warranty_charges = 'YES'
                    Return
                End
            ! No Charge?
                access:chartype.clearkey(cha:charge_type_key)
                cha:charge_type = job:warranty_charge_type
                If access:chartype.fetch(cha:charge_type_key)
                    f_labour    = 0
                    f_parts     = 0
                    f_pass      = 0
                Else !If access:chartype.fetch(cha:charge_type_key)
                    If cha:no_charge = 'YES'
                        f_labour = ''
                        f_parts  = ''
                    Else !If cha:no_charge = 'YES'

                        If cha:zero_parts <> 'YES'
                            parts_total$ = 0
                            save_wpr_id = access:warparts.savefile()
                            access:warparts.clearkey(wpr:part_number_key)
                            wpr:ref_number  = job:ref_number
                            set(wpr:part_number_key,wpr:part_number_key)
                            loop
                                if access:warparts.next()
                                   break
                                end !if
                                if wpr:ref_number  <> job:ref_number      |
                                    then break.  ! end if
                                parts_total$ += wpr:purchase_cost * wpr:quantity
                            end !loop
                            access:warparts.restorefile(save_wpr_id)
                            f_parts = parts_total$
                        Else !If cha:zero_parts <> 'YES'
                            f_parts = 0
                        End !If cha:zero_parts <> 'YES'

                        access:subtracc.clearkey(sub:account_number_key)
                        sub:account_number = job:account_number
                        if access:subtracc.fetch(sub:account_number_key)
                            Case MessageEx('Error! Cannot find Sub Account Details.','ServiceBase 2000',|
                                           'Styles\stop.ico','',0,0,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            End!Case MessageEx
                        Else!if access:subtracc.fetch(sub:account_number_key)
                            access:tradeacc.clearkey(tra:account_number_key)
                            tra:account_number = sub:main_account_number
                            if access:tradeacc.fetch(tra:account_number_key)
                                Case MessageEx('Error! Cannot find Trade Account Details.','ServiceBase 2000',|
                                               'Styles\stop.ico','',0,0,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                End!Case MessageEx
                            end!if access:tradeacc.fetch(tra:account_number_key)
                        end!if access:subtracc.fetch(sub:account_number_key)
                        use_standard# = 1
!                        If TRA:ZeroChargeable = 'YES'
!                            f_parts = 0
!                        End!If TRA:ZeroChargeable = 'YES'
                        If tra:invoice_sub_accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                            access:subchrge.clearkey(suc:model_repair_type_key)
                            suc:account_number = job:account_number
                            suc:model_number   = job:model_number
                            suc:charge_type    = job:Warranty_charge_type
                            suc:unit_type      = job:unit_type
                            suc:repair_type    = job:repair_type_warranty
                            if access:subchrge.fetch(suc:model_repair_type_key)
                                access:trachrge.clearkey(trc:account_charge_key)
                                trc:account_number = sub:main_account_number
                                trc:model_number   = job:model_number
                                trc:charge_type    = job:warranty_charge_type
                                trc:unit_type      = job:unit_type
                                trc:repair_type    = job:repair_type_warranty
                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                                    f_labour    = trc:cost
                                    use_standard# = 0
                                End!if access:trachrge.fetch(trc:account_charge_key)
                            Else
                                f_labour    = suc:cost
                                use_standard# = 0
                            End!if access:subchrge.fetch(suc:model_repair_type_key)
                        Else!If tra:use_sub_accounts = 'YES'
                            access:trachrge.clearkey(trc:account_charge_key)
                            trc:account_number = tra:account_number
                            trc:model_number   = job:model_number
                            trc:charge_type    = job:warranty_charge_type
                            trc:unit_type      = job:unit_type
                            trc:repair_type    = job:repair_type_warranty
                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                                f_labour    = trc:cost
                                use_standard# = 0
                            End!if access:trachrge.fetch(trc:account_charge_key)
                        End!If tra:use_sub_accounts = 'YES'

                        If use_standard# = 1
                            access:stdchrge.clearkey(sta:model_number_charge_key)
                            sta:model_number = job:model_number
                            sta:charge_type  = job:warranty_charge_type
                            sta:unit_type    = job:unit_type
                            sta:repair_type  = job:repair_type_warranty
                            if access:stdchrge.fetch(sta:model_number_charge_key)
                                f_labour     = 0
                                f_pass       = 0
                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                                f_labour    = sta:cost
                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
                        End!If use_standard# = 1

                    End !If cha:no_charge = 'YES'
                End !If access:chartype.clearkey(cha:charge_type_key)
            End !If job:chargeable_job = 'YES'

        Of 'E'
            If job:estimate = 'YES' 
                labour_discount# = 0
                parts_discount#  = 0
                f_pass  = 1
                If job:ignore_estimate_charges = 'YES'
                    Return
                End!If job:ignore_estimate_charges = 'YES'
            ! No Charge?
                access:chartype.clearkey(cha:charge_type_key)
                cha:charge_type = job:charge_type
                If access:chartype.fetch(cha:charge_type_key)
                    f_labour    = 0
                    f_parts     = 0
                    f_pass      = 0
                Else !If access:chartype.fetch(cha:charge_type_key)
                    If cha:no_charge = 'YES'
                        f_labour = ''
                        f_parts  = ''
                    Else !If cha:no_charge = 'YES'

                        If cha:zero_parts <> 'YES'
                            parts_total$ = 0
                            save_epr_id = access:estparts.savefile()
                            access:estparts.clearkey(epr:part_number_key)
                            epr:ref_number  = job:ref_number
                            set(epr:part_number_key,epr:part_number_key)
                            loop
                                if access:estparts.next()
                                   break
                                end !if
                                if epr:ref_number  <> job:ref_number      |
                                    then break.  ! end if
                                parts_total$ += epr:sale_cost * epr:quantity
                            end !loop
                            access:estparts.restorefile(save_epr_id)
                            f_parts = parts_total$
                        Else !If cha:zero_parts <> 'YES'
                            f_parts = 0
                        End !If cha:zero_parts <> 'YES'

                        access:subtracc.clearkey(sub:account_number_key)
                        sub:account_number = job:account_number
                        if access:subtracc.fetch(sub:account_number_key)
                            Case MessageEx('Error! Cannot find Sub Account Details.','ServiceBase 2000',|
                                           'Styles\stop.ico','',0,0,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            End!Case MessageEx
                        Else!if access:subtracc.fetch(sub:account_number_key)
                            access:tradeacc.clearkey(tra:account_number_key)
                            tra:account_number = sub:main_account_number
                            if access:tradeacc.fetch(tra:account_number_key)
                                Case MessageEx('Error! Cannot find Trade Account Details.','ServiceBase 2000',|
                                           'Styles\stop.ico','',0,0,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                End!Case MessageEx
                            end!if access:tradeacc.fetch(tra:account_number_key)
                        end!if access:subtracc.fetch(sub:account_number_key)
                        use_standard# = 1
                        If TRA:ZeroChargeable = 'YES'
                            f_parts = 0
                        End!If TRA:ZeroChargeable = 'YES'

                        If tra:invoice_sub_accounts = 'YES'
                            access:subchrge.clearkey(suc:model_repair_type_key)
                            suc:account_number = job:account_number
                            suc:model_number   = job:model_number
                            suc:charge_type    = job:charge_type
                            suc:unit_type      = job:unit_type
                            suc:repair_type    = job:repair_type
                            if access:subchrge.fetch(suc:model_repair_type_key)
                                access:trachrge.clearkey(trc:account_charge_key)
                                trc:account_number = sub:main_account_number
                                trc:model_number   = job:model_number
                                trc:charge_type    = job:charge_type
                                trc:unit_type      = job:unit_type
                                trc:repair_type    = job:repair_type
                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                                    f_labour    = trc:cost
                                    use_standard# = 0
                                End!if access:trachrge.fetch(trc:account_charge_key)
                            Else
                                f_labour    = suc:cost
                                use_standard# = 0
                            End!if access:subchrge.fetch(suc:model_repair_type_key)
                        Else!If tra:use_sub_accounts = 'YES'
                            access:trachrge.clearkey(trc:account_charge_key)
                            trc:account_number = sub:main_account_number
                            trc:model_number   = job:model_number
                            trc:charge_type    = job:charge_type
                            trc:unit_type      = job:unit_type
                            trc:repair_type    = job:repair_type
                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                                f_labour    = trc:cost
                                use_standard# = 0
                            End!if access:trachrge.fetch(trc:account_charge_key)
                        End!If tra:use_sub_accounts = 'YES'

                        If use_standard# = 1
                            access:stdchrge.clearkey(sta:model_number_charge_key)
                            sta:model_number = job:model_number
                            sta:charge_type  = job:charge_type
                            sta:unit_type    = job:unit_type
                            sta:repair_type  = job:repair_type
                            if access:stdchrge.fetch(sta:model_number_charge_key)
                                f_labour     = 0
                                f_pass       = 0
                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                                f_labour    = sta:cost
                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
                        End!If use_standard# = 1

                    End !If cha:no_charge = 'YES'
                End !If access:chartype.clearkey(cha:charge_type_key)
            End !If job:estimate = 'YES'
    End!Case f_type



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Pricing_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
