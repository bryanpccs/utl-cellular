

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01057.INC'),ONCE        !Local module procedure declarations
                     END


CanInvoiceBePrinted  PROCEDURE  (func:JobNumber,func:ShowMessages) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CanInvoiceBePrinted')      !Add Procedure to Log
  end


!Can the invoice of the selected job be printed/created?
?   Message('Can Invoice Be Printed?','Debug Message', Icon:Hand)
    Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = func:JobNumber
    If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Found
        If job_ali:Chargeable_Job <> 'YES'
            If func:ShowMessages
                Case MessageEx('Cannot invoice! The selected job is NOT a Chargeable Job.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            End !If func:ShowMessages
?   Message('Not Chargeable','Debug Message', Icon:Hand)
            Return Level:Fatal
        End !If job:Chargeable_Job <> 'YES'
        If job_ali:Bouncer <> ''
            If func:ShowMessages
                Case MessageEx('Cannot invoice! The selected job has been marked as a Bouncer.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            End !If func:ShowMessages
?   Message('Bouncer','Debug Message', Icon:Hand)
            Return Level:Fatal
        End !If job_ali:Bouncer <> ''

        If job_ali:Date_Completed = ''
            If func:ShowMessages
                Case MessageEx('Cannot invoice! The selected job has not been completed.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            End !If func:ShowMessages
?   Message('Not Completed','Debug Message', Icon:Hand)
            Return Level:Fatal
        End !If job_ali:Date_Completed = ''

        !There should now be now way to complete a job without a default structure

        If job_ali:Ignore_Chargeable_Charges = 'YES' and job:Sub_Total = 0
            If func:ShowMessages
                Case MessageEx('Cannot invoice! The selected job has not been priced.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            End !If func:ShowMessages
?   Message('No Cost','Debug Message', Icon:Hand)
            Return Level:Fatal
        End !If job_ali:Ignore_Chargeable_Charges = 'YES' and job:Sub_Total = 0

       ! Start Change 2905 BE(15/07/03)
        IF ((job_ali:Cancelled = 'YES') AND (GETINI('VALIDATE','ExcludeCancelledJobs',,CLIP(PATH())&'\SB2KDEF.INI'))) THEN
            IF (func:ShowMessages) THEN
                MessageEx('Cannot invoice! The selected job has been cancelled.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0, |
                               beep:systemhand,msgex:samewidths,84,26,0)
            END
            RETURN Level:Fatal
        END
        ! End Change 2905 BE(15/07/03)

        !Is this job a single invoice, and not part of a multiple invoice?
        If job_ali:Invoice_Number <> ''
            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
            inv:Invoice_Number =job_ali:Invoice_Number
            If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
               !Found
                If inv:Invoice_Type <> 'SIN'
                    If func:ShowMessages
                        Case MessageEx('Cannot print invoice! The selected job is NOT a Single Invoice, it may be part of a Multiple Invoice.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    End !If func:ShowMessages
?   Message('Already Invoiced, not Single','Debug Message', Icon:Hand)
                    Return Level:Fatal
                End !If inv:Invoice_Type <> 'SIN'
            Else! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
                Assert(0,'<13,10>Fetch Error<13,10>')
                Return Level:Fatal
            End! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        End !If job_ali:Invoice_Number <> ''
         
    Else!If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Error
        Assert(0,'<13,10>Fetch Error<13,10>')
        Return Level:Fatal
    End!If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
?   Message('a-ok','Debug Message', Icon:Hand)
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CanInvoiceBePrinted',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
