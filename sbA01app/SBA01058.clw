

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01058.INC'),ONCE        !Local module procedure declarations
                     END


CreateInvoice        PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:LabourVATRate    REAL
tmp:PartsVATRate     REAL
Invoice_Number_Temp  LONG
tmp:PrintDespatch    BYTE(0)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
WindowsDir      CSTRING(255)
SystemDir       CSTRING(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CreateInvoice')      !Add Procedure to Log
  end


    !Create a Chargeable Invoice
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    SystemDir   = GetSystemDirectory(WindowsDir,Size(WindowsDir))

    IF job:Invoice_Number > 0
      RETURN Level:Benign
    END

    If InvoiceSubAccounts(job:Account_Number)
        Access:VATCODE.Clearkey(vat:VAT_Code_Key)
        vat:VAT_Code    = sub:Labour_Vat_Code
        If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Found
            tmp:LabourVATRate   = vat:VAT_Rate
        Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign

        Access:VATCODE.Clearkey(vat:VAT_Code_Key)
        vat:VAT_Code    = sub:Parts_Vat_Code
        If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Found
            tmp:PartsVATRate   = vat:VAT_Rate
        Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign

        !tmp:PrintDespatch = 1 - Print Despatch Note
        !tmp:PrintDespatch = 2 - Do not print, but change status to READY TO DESPATCH
        If sub:Despatch_Invoiced_Jobs = 'YES'
            If sub:Despatch_Paid_Jobs = 'YES'
                If job:Paid = 'YES'
                    tmp:PrintDespatch = 1
                Else !If job:Paid = 'YES'
                    tmp:PrintDespatch = 2
                End !If job:Paid = 'YES'
            Else !If sub:Despatch_Paid_Jobs = 'YES'
                tmp:PrintDespatch = 1
            End !If sub:Despatch_Paid_Jobs = 'YES'
        End !If sub:Despatch_Invoiced_Jobs = 'YES'

    Else !If InvoiceSubAccounts(job:Account_Number)
        Access:VATCODE.Clearkey(vat:VAT_Code_Key)
        vat:VAT_Code    = tra:Labour_Vat_Code
        If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Found
            tmp:LabourVATRate   = vat:VAT_Rate
        Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign

        Access:VATCODE.Clearkey(vat:VAT_Code_Key)
        vat:VAT_Code    = tra:Parts_Vat_Code
        If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Found
            tmp:PartsVATRate   = vat:VAT_Rate
        Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign

        If tra:Despatch_Invoiced_Jobs = 'YES'
            If tra:Despatch_Paid_Jobs = 'YES'
                If job:Paid = 'YES'
                    tmp:PrintDespatch = 1
                End !If job:Paid = 'YES'
            Else !If sub:Despatch_Paid_Jobs = 'YES'
                tmp:PrintDespatch = 1
            End !If sub:Despatch_Paid_Jobs = 'YES'
        End !If sub:Despatch_Invoiced_Jobs = 'YES'
    End !If InvoiceSubAccounts(job:Account_Number)

    Sage_Error# = 0
    If def:Use_Sage = 'YES'
        glo:file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
        Remove(paramss)
        access:paramss.open()
        access:paramss.usefile()
!LABOUR
        get(paramss,0)
        if access:paramss.primerecord() = Level:Benign
            prm:account_ref       = Stripcomma(job:Account_number)
            If tra:invoice_sub_accounts = 'YES'
                If sub:invoice_customer_address <> 'YES'
                    prm:name              = Stripcomma(sub:company_name)
                    prm:address_1         = Stripcomma(sub:address_line1)
                    prm:address_2         = Stripcomma(sub:address_line2)
                    prm:address_3         = Stripcomma(sub:address_line3)
                    prm:address_5         = Stripcomma(sub:postcode)
                Else!If tra:invoice_customer_address = 'YES'
                    prm:name              = Stripcomma(job:company_name)
                    prm:address_1         = Stripcomma(job:address_line1)
                    prm:address_2         = Stripcomma(job:address_line2)
                    prm:address_3         = Stripcomma(job:address_line3)
                    prm:address_5         = Stripcomma(job:postcode)

                End!If tra:invoice_customer_address = 'YES'
            Else!If tra:invoice_sub_accounts = 'YES'
                If tra:invoice_customer_address <> 'YES'
                    prm:name              = Stripcomma(tra:company_name)
                    prm:address_1         = Stripcomma(tra:address_line1)
                    prm:address_2         = Stripcomma(tra:address_line2)
                    prm:address_3         = Stripcomma(tra:address_line3)
                    prm:address_5         = Stripcomma(tra:postcode)
                Else!If tra:invoice_customer_address = 'YES'
                    prm:name              = Stripcomma(job:company_name)
                    prm:address_1         = Stripcomma(job:address_line1)
                    prm:address_2         = Stripcomma(job:address_line2)
                    prm:address_3         = Stripcomma(job:address_line3)
                    prm:address_5         = Stripcomma(job:postcode)
                End!If tra:invoice_customer_address = 'YES'
            End!If tra:invoice_sub_accounts = 'YES'
            prm:address_4         = ''
            prm:del_address_1     = Stripcomma(job:address_line1_delivery)
            prm:del_address_2     = Stripcomma(job:address_line2_delivery)
            prm:del_address_3     = Stripcomma(job:address_line3_delivery)
            prm:del_address_4     = ''
            prm:del_address_5     = Stripcomma(job:postcode_delivery)
            prm:cust_tel_number   = Stripcomma(job:telephone_number)
            If job:surname <> ''
                if job:title = '' and job:initial = '' 
                    prm:contact_name = Stripcomma(clip(job:surname))
                elsif job:title = '' and job:initial <> ''
                    prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                elsif job:title <> '' and job:initial = ''
                    prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                elsif job:title <> '' and job:initial <> ''
                    prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                else
                    prm:contact_name = ''
                end
            else
                prm:contact_name = ''
            end
            prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
            prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
            prm:notes_3           = ''
            prm:taken_by          = Stripcomma(job:who_booked)
            prm:order_number      = Stripcomma(job:order_number)
            prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
            prm:payment_ref       = ''
            prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
            prm:global_details    = ''
            prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
            prm:items_tax         = Stripcomma(Round(job:labour_cost * (tmp:LabourVATRate/100),.01) + |
                                        Round(job:parts_cost * (tmp:PartsVATRate/100),.01) + |
                                        Round(job:courier_cost * (tmp:LabourVATRate/100),.01))
            prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
            prm:description       = Stripcomma(DEF:Labour_Description)
            prm:nominal_code      = Stripcomma(DEF:Labour_Code)
            prm:qty_order         = '1'
            prm:unit_price        = '0'
            prm:net_amount        = Stripcomma(job:labour_cost)
            prm:tax_amount        = Stripcomma(Round(job:labour_cost * (tmp:LabourVATRate/100),.01))
            prm:comment_1         = Stripcomma(job:charge_type)
            prm:comment_2         = Stripcomma(job:warranty_charge_type)
            prm:unit_of_sale      = '0'
            prm:full_net_amount   = '0'
            prm:invoice_date      = '*'
            prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
            prm:user_name         = Clip(DEF:User_Name_Sage)
            prm:password          = Clip(DEF:Password_Sage)
            prm:set_invoice_number= '*'
            prm:invoice_no        = '*'
            access:paramss.insert()
        end!if access:paramss.primerecord() = Level:Benign
!PARTS
        get(paramss,0)
        if access:paramss.primerecord() = Level:Benign
            prm:account_ref       = Stripcomma(job:Account_number)
            If tra:invoice_sub_accounts = 'YES'
                If sub:invoice_customer_address <> 'YES'
                    prm:name              = Stripcomma(sub:company_name)
                    prm:address_1         = Stripcomma(sub:address_line1)
                    prm:address_2         = Stripcomma(sub:address_line2)
                    prm:address_3         = Stripcomma(sub:address_line3)
                    prm:address_5         = Stripcomma(sub:postcode)
                Else!If tra:invoice_customer_address = 'YES'
                    prm:name              = Stripcomma(job:company_name)
                    prm:address_1         = Stripcomma(job:address_line1)
                    prm:address_2         = Stripcomma(job:address_line2)
                    prm:address_3         = Stripcomma(job:address_line3)
                    prm:address_5         = Stripcomma(job:postcode)

                End!If tra:invoice_customer_address = 'YES'
            Else!If tra:invoice_sub_accounts = 'YES'
                If tra:invoice_customer_address <> 'YES'
                    prm:name              = Stripcomma(tra:company_name)
                    prm:address_1         = Stripcomma(tra:address_line1)
                    prm:address_2         = Stripcomma(tra:address_line2)
                    prm:address_3         = Stripcomma(tra:address_line3)
                    prm:address_5         = Stripcomma(tra:postcode)
                Else!If tra:invoice_customer_address = 'YES'
                    prm:name              = Stripcomma(job:company_name)
                    prm:address_1         = Stripcomma(job:address_line1)
                    prm:address_2         = Stripcomma(job:address_line2)
                    prm:address_3         = Stripcomma(job:address_line3)
                    prm:address_5         = Stripcomma(job:postcode)
                End!If tra:invoice_customer_address = 'YES'
            End!If tra:invoice_sub_accounts = 'YES'
            prm:address_4         = ''
            prm:del_address_1     = Stripcomma(job:address_line1_delivery)
            prm:del_address_2     = Stripcomma(job:address_line2_delivery)
            prm:del_address_3     = Stripcomma(job:address_line3_delivery)
            prm:del_address_4     = ''
            prm:del_address_5     = Stripcomma(job:postcode_delivery)
            prm:cust_tel_number   = Stripcomma(job:telephone_number)
            If job:surname <> ''
                if job:title = '' and job:initial = '' 
                    prm:contact_name = Stripcomma(clip(job:surname))
                elsif job:title = '' and job:initial <> ''
                    prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                elsif job:title <> '' and job:initial = ''
                    prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                elsif job:title <> '' and job:initial <> ''
                    prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                else
                    prm:contact_name = ''
                end
            else
                prm:contact_name = ''
            end
            prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
            prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
            prm:notes_3           = ''
            prm:taken_by          = Stripcomma(job:who_booked)
            prm:order_number      = Stripcomma(job:order_number)
            prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
            prm:payment_ref       = ''
            prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
            prm:global_details    = ''
            prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
            prm:items_tax         = Stripcomma(Round(job:labour_cost * (tmp:LabourVATRate/100),.01) + |
                                        Round(job:parts_cost * (tmp:PartsVATRate/100),.01) + |
                                        Round(job:courier_cost * (tmp:LabourVATRate/100),.01))
            prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
            prm:description       = Stripcomma(DEF:Parts_Description)
            prm:nominal_code      = Stripcomma(DEF:Parts_Code)
            prm:qty_order         = '1'
            prm:unit_price        = '0'
            prm:net_amount        = Stripcomma(job:parts_cost)
            prm:tax_amount        = Stripcomma(Round(job:parts_cost * (tmp:PartsVATRate/100),.01))
            prm:comment_1         = Stripcomma(job:charge_type)
            prm:comment_2         = Stripcomma(job:warranty_charge_type)
            prm:unit_of_sale      = '0'
            prm:full_net_amount   = '0'
            prm:invoice_date      = '*'
            prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
            prm:user_name         = Clip(DEF:User_Name_Sage)
            prm:password          = Clip(DEF:Password_Sage)
            prm:set_invoice_number= '*'
            prm:invoice_no        = '*'
            access:paramss.insert()
        end!if access:paramss.primerecord() = Level:Benign
!COURIER
        get(paramss,0)
        if access:paramss.primerecord() = Level:Benign
            prm:account_ref       = Stripcomma(job:Account_number)
            If tra:invoice_sub_accounts = 'YES'
                If sub:invoice_customer_address <> 'YES'
                    prm:name              = Stripcomma(sub:company_name)
                    prm:address_1         = Stripcomma(sub:address_line1)
                    prm:address_2         = Stripcomma(sub:address_line2)
                    prm:address_3         = Stripcomma(sub:address_line3)
                    prm:address_5         = Stripcomma(sub:postcode)
                Else!If tra:invoice_customer_address = 'YES'
                    prm:name              = Stripcomma(job:company_name)
                    prm:address_1         = Stripcomma(job:address_line1)
                    prm:address_2         = Stripcomma(job:address_line2)
                    prm:address_3         = Stripcomma(job:address_line3)
                    prm:address_5         = Stripcomma(job:postcode)

                End!If tra:invoice_customer_address = 'YES'
            Else!If tra:invoice_sub_accounts = 'YES'
                If tra:invoice_customer_address <> 'YES'
                    prm:name              = Stripcomma(tra:company_name)
                    prm:address_1         = Stripcomma(tra:address_line1)
                    prm:address_2         = Stripcomma(tra:address_line2)
                    prm:address_3         = Stripcomma(tra:address_line3)
                    prm:address_5         = Stripcomma(tra:postcode)
                Else!If tra:invoice_customer_address = 'YES'
                    prm:name              = Stripcomma(job:company_name)
                    prm:address_1         = Stripcomma(job:address_line1)
                    prm:address_2         = Stripcomma(job:address_line2)
                    prm:address_3         = Stripcomma(job:address_line3)
                    prm:address_5         = Stripcomma(job:postcode)
                End!If tra:invoice_customer_address = 'YES'
            End!If tra:invoice_sub_accounts = 'YES'

            prm:address_4         = ''
            prm:del_address_1     = Stripcomma(job:address_line1_delivery)
            prm:del_address_2     = Stripcomma(job:address_line2_delivery)
            prm:del_address_3     = Stripcomma(job:address_line3_delivery)
            prm:del_address_4     = ''
            prm:del_address_5     = Stripcomma(job:postcode_delivery)
            prm:cust_tel_number   = Stripcomma(job:telephone_number)
            If job:surname <> ''
                if job:title = '' and job:initial = '' 
                    prm:contact_name = Stripcomma(clip(job:surname))
                elsif job:title = '' and job:initial <> ''
                    prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                elsif job:title <> '' and job:initial = ''
                    prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                elsif job:title <> '' and job:initial <> ''
                    prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                else
                    prm:contact_name = ''
                end
            else
                prm:contact_name = ''
            end
            prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
            prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
            prm:notes_3           = ''
            prm:taken_by          = Stripcomma(job:who_booked)
            prm:order_number      = Stripcomma(job:order_number)
            prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
            prm:payment_ref       = ''
            prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
            prm:global_details    = ''
            prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
            prm:items_tax         = Stripcomma(Round(job:labour_cost * (tmp:LabourVATRate/100),.01) + |
                                        Round(job:parts_cost * (tmp:PartsVATRate/100),.01) + |
                                        Round(job:courier_cost * (tmp:LabourVATRate/100),.01))
            prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
            prm:description       = Stripcomma(DEF:Courier_Description)
            prm:nominal_code      = Stripcomma(DEF:Courier_Code)
            prm:qty_order         = '1'
            prm:unit_price        = '0'
            prm:net_amount        = Stripcomma(job:courier_cost)
            prm:tax_amount        = Stripcomma(Round(job:courier_cost * (tmp:LabourVATRate/100),.01))
            prm:comment_1         = Stripcomma(job:charge_type)
            prm:comment_2         = Stripcomma(job:warranty_charge_type)
            prm:unit_of_sale      = '0'
            prm:full_net_amount   = '0'
            prm:invoice_date      = '*'
            prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
            prm:user_name         = Clip(DEF:User_Name_Sage)
            prm:password          = Clip(DEF:Password_Sage)
            prm:set_invoice_number= '*'
            prm:invoice_no        = '*'
            access:paramss.insert()
        end!if access:paramss.primerecord() = Level:Benign
        access:paramss.close()
        Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
        sage_error# = 0
        access:paramss.open()
        access:paramss.usefile()
        Set(paramss,0)
        If access:paramss.next()
            sage_error# = 1
        Else!If access:paramss.next()
            If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
                sage_error# = 1
            Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
                invoice_number_temp = prm:invoice_no
                If invoice_number_temp = 0
                    sage_error# = 1
                End!If invoice_number_temp = 0
            End!If prm:invoice_no = '-1'
        End!If access:paramss.next()
        access:paramss.close()
    End !If def:Use_Sage = 'YES'

    If sage_error# = 0
        invoice_error# = 1
        If Access:INVOICE.PrimeRecord() = Level:Benign
            If def:use_sage = 'YES'
                inv:invoice_number = invoice_number_temp
            End!If def:use_sage = 'YES'
            inv:invoice_type       = 'SIN'
            inv:job_number         = job:ref_number
            inv:date_created       = Today()
            inv:account_number     = job:account_number
            If tra:invoice_sub_accounts = 'YES'
                inv:AccountType = 'SUB'
            Else!If tra:invoice_sub_accounts = 'YES'
                inv:AccountType = 'MAI'
            End!If tra:invoice_sub_accounts = 'YES'
            inv:total              = job:sub_total
            inv:vat_rate_labour    = tmp:LabourVATRate
            inv:vat_rate_parts     = tmp:PartsVATRate
            inv:vat_rate_retail    = tmp:LabourVATRate
            inv:vat_number         = def:vat_number
            INV:Courier_Paid       = job:courier_cost
            inv:parts_paid         = job:parts_cost
            inv:labour_paid        = job:labour_cost
            inv:invoice_vat_number = vat_number"

            If Access:INVOICE.TryInsert() = Level:Benign
                !Insert Successful
                Case tmp:PrintDespatch
                    Of 1
                        If job:Despatched = 'YES'
                            If job:Paid = 'YES'
                                GetStatus(910,0,'JOB')
                            Else !If job:Paid = 'YES'
                                GetStatus(905,0,'JOB')
                            End !If job:Paid = 'YES'
                        Else !If job:Despatch = 'YES'
                            GetStatus(810,0,'JOB')
                            job:Despatched = 'REA'
                            job:Despatch_Type = 'JOB'
                            job:Current_Courier = job:Courier
                        End !If job:Despatch = 'YES'
                    Of 2
                End !Case tmp:PrintDespatch
                job:invoice_number        = inv:invoice_number
                job:invoice_date          = Today()
                job:invoice_courier_cost  = job:courier_cost
                job:invoice_labour_cost   = job:labour_cost
                job:invoice_parts_cost    = job:parts_cost
                job:invoice_sub_total     = job:sub_total
                access:jobs.update()
                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:notes         = 'INVOICE NUMBER: ' & inv:invoice_number
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    aud:type          = 'JOB'
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'INVOICE CREATED'
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:benign

                Return Level:Benign

            Else !If Access:INVOICE.TryInsert() = Level:Benign
                !Insert Failed
                Return Level:Fatal
            End !If Access:INVOICE.TryInsert() = Level:Benign
        End !If Access:INVOICE.PrimeRecord() = Level:Benign
    Else !sage_Error# = 1
        Return Level:Fatal
    End!sage_error# = 1


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CreateInvoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:LabourVATRate',tmp:LabourVATRate,'CreateInvoice',1)
    SolaceViewVars('tmp:PartsVATRate',tmp:PartsVATRate,'CreateInvoice',1)
    SolaceViewVars('Invoice_Number_Temp',Invoice_Number_Temp,'CreateInvoice',1)
    SolaceViewVars('tmp:PrintDespatch',tmp:PrintDespatch,'CreateInvoice',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
