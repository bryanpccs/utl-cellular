

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01031.INC'),ONCE        !Local module procedure declarations
                     END


ExcludeFromInvoice   PROCEDURE  (f_chargetype)        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'ExcludeFromInvoice')      !Add Procedure to Log
  end


    access:chartype.clearkey(cha:charge_type_key)
    cha:charge_type = f_chargetype
    If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
        !Found
        If CHA:ExcludeInvoice
            Return Level:Fatal
        End!If CHA:ExcludeInvoice
    Else! If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
        !Error
    End! If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ExcludeFromInvoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
