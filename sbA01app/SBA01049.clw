

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01049.INC'),ONCE        !Local module procedure declarations
                     END


NormalDespatchCourier PROCEDURE  (func:Courier)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'NormalDespatchCourier')      !Add Procedure to Log
  end


    Access:COURIER_ALIAS.ClearKey(cou_ali:Courier_Key)
    cou_ali:Courier = func:Courier
    If Access:COURIER_ALIAS.TryFetch(cou_ali:Courier_Key) = Level:Benign
        !Found
        If cou_ali:Courier_Type     = 'ANC' Or |
            cou_ali:Courier_Type    = 'ROYAL MAIL' Or |
            cou_ali:Courier_Type    = 'PARCELINE' Or |
            cou_ali:Courier_Type    = 'UPS'
            Return Level:Fatal
        End !cou_ali:Courier_Type    = 'UPS'
    Else!If Access:COURIER_ALIAS.TryFetch(cou_ali:Courier_Key)e = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:COURIER_ALIAS.TryFetch(cou_ali:Courier_Key)e = Level:Benign
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'NormalDespatchCourier',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
