

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01063.INC'),ONCE        !Local module procedure declarations
                     END


BERRepairType        PROCEDURE  (func:RepairType)     ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'BERRepairType')      !Add Procedure to Log
  end


!Is the selected Repair Type a BER?
    Access:REPTYDEF.ClearKey(rtd:Repair_Type_Key)
    rtd:Repair_Type = func:RepairType
    If Access:REPTYDEF.TryFetch(rtd:Repair_Type_Key) = Level:Benign
        !Found
        If rtd:BER
            Return Level:Benign
        End !If rtd:BER
    Else!If Access:REPTYDEF.TryFetch(rtd:Repair_Type_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:REPTYDEF.TryFetch(rtd:Repair_Type_Key) = Level:Benign
    Return Level:Fatal


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BERRepairType',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
