

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01039.INC'),ONCE        !Local module procedure declarations
                     END


MainStoreLocation    PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'MainStoreLocation')      !Add Procedure to Log
  end


    Access:LOCATION_ALIAS.ClearKey(loc_ali:Main_Store_Key)
    loc_ali:Main_Store = 'YES'
    Set(loc_ali:Main_Store_Key,loc_ali:Main_Store_Key)
    If Access:LOCATION_ALIAS.NEXT() = Level:Benign
        If loc_ali:Main_Store = 'YES'
            Return loc_ali:Location
        End!If loc_ali:Main_Store = 'YES'
    End!If Access:LOCATION_ALIAS.NEXT() = Level:Benign

    Return ''


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'MainStoreLocation',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
