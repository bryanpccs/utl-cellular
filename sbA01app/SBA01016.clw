

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01016.INC'),ONCE        !Local module procedure declarations
                     END


TurnaroundTime       PROCEDURE  (f_number,f_type)     ! Declare Procedure
clock_temp           TIME
tmp:end_days         LONG
tmp:end_hours        LONG
start_time_temp      TIME
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'TurnaroundTime')      !Add Procedure to Log
  end


    Set(Defaults)
    access:defaults.next()
    tmp:end_days = Today()
    tmp:end_hours = Clock()

    Case f_type
        Of 'D' !Return Days

            x# = 0
            count# = 0
            If f_number <> 0
                Loop
                    count# += 1
                    day_number# = (Today() + count#) % 7
                    If def:include_saturday <> 'YES'
                        If day_number# = 6
                            tmp:end_days += 1
                            Cycle
                        End
                    End
                    If def:include_sunday <> 'YES'
                        If day_number# = 0
                            tmp:end_days += 1
                            Cycle
                        End
                    End
                    tmp:end_days += 1
                    x# += 1
                    If x# >= f_number
                        Break
                    End!If x# >= sts:turnaround_days
                End!Loop
            End!If f_days <> ''
            Return(tmp:end_days)
        Of 'T' !Return Time
            If f_number <> 0
                start_time_temp = Clock()
                new_day# = 0
                Loop x# = 1 To f_number
                    clock_temp = start_time_temp + (x# * 360000)
                    If def:start_work_hours <> '' And def:end_work_hours <> ''
                        If clock_temp < def:start_work_hours Or clock_temp > def:end_work_hours
                            tmp:end_hours = def:start_work_hours
                            start_time_temp = def:start_work_hours
                            tmp:end_days += 1
                        End!If clock_temp > def:start_work_hours And clock_temp < def:end_work_hours
                    End!If def:start_work_hours <> '' And def:end_work_hours <> ''
                    tmp:end_hours += 360000
                End!Loop x# = 1 To Abs(sts:turnaround_hours/6000)
            End!If f_hours <> ''
            Return(tmp:end_hours)
    End!Case f_type



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'TurnaroundTime',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('clock_temp',clock_temp,'TurnaroundTime',1)
    SolaceViewVars('tmp:end_days',tmp:end_days,'TurnaroundTime',1)
    SolaceViewVars('tmp:end_hours',tmp:end_hours,'TurnaroundTime',1)
    SolaceViewVars('start_time_temp',start_time_temp,'TurnaroundTime',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
