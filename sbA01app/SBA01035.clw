

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01035.INC'),ONCE        !Local module procedure declarations
                     END


CompulsoryFieldCheck PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_taf_id          USHORT,AUTO
save_maf_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
tmp:ForceFaultCodes  BYTE(0)
tmp:FoundRequestedParts BYTE(0)
tmp:FoundOrderedParts BYTE(0)
tmp:FoundCommonFault BYTE(0)
save_aud_id          USHORT,AUTO
save_map_id          USHORT
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CompulsoryFieldCheck')      !Add Procedure to Log
  end


    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    error# = 0
    glo:ErrorText = ''

    If ForceTransitType(func:Type)
        If job:Transit_Type = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Transit Type Missing.'
        End!If job:Transit_Type = ''
    End !If ForceTransitType(func:Type)

    If ForceMobileNumber(func:Type)
        If job:Mobile_Number = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Mobile Number Missing.'
        End!If job:Mobile_Number = ''
    End !If ForceMobileNumber(func:Type)

    If ForceModelNumber(func:Type)
        If job:Model_Number = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Model Number Missing.'
        End!If job:Model_Number = ''
    End !If ForceModelNumber(func:Type)

    If ForceUnitType(func:Type)
        If job:Unit_Type = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Unit Type Type Missing.'
        End!If job:Unit_Type = ''
    End !If ForceUnitType(func:Type)

    If ForceColour(func:Type)
        If job:Colour = ''
            error# = 1
            If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                glo:ErrorText = Clip(glo:ErrorText) & '<13,10>' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & ' Missing.'
            Else !If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Colour Missing'
            End !Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
        End!If job:Unit_Type = ''
    End !If ForceColour(func:Type)

    Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
    jbn:RefNumber = job:Ref_Number
    Access:JOBNOTES.TryFetch(jbn:RefNumberKey)

    If ForceFaultDescription(func:Type)
        If jbn:Fault_Description = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Description Missing.'
        End!If jbn:Fault_Description = ''
    End !If ForceFaultDescription(func:Type)

    If ForceInvoiceText(func:Type)
        IF jbn:Invoice_Text = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Invoice Text Missing.'
        End!IF jbn:Invoice_Text = ''
    End !If ForceInvoiceText(func:Type)

    If ForceIMEI(func:Type)
        If job:ESN = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>I.M.E.I. Number Missing.'
        End!If job:ESN = ''
    End !If ForceIMEI(func:Type)

    ! Start Change 2941 BE(06/11/03)
    !If ForceMSN(job:Manufacturer,func:Type)
    If ForceMSN(job:Manufacturer,job:Workshop,func:Type)
    ! End Change 2941 BE(06/11/03)
        If job:MSN = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>M.S.N. Missing.'
        End!If job:MSN = ''
    End !If ForceMSN(func:Type)

    !Check I.M.E.I. Number Length
    If CheckLengthNoError(job:ESN,job:Model_Number,'IMEI')
        Error# = 1
        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>I.M.E.I. Number Incorrect Length.'
    End !If CheckLength(job:ESN,job:Model_Number,'IMEI')

    If MSNRequired(job:Manufacturer) = Level:Benign
        If CheckLengthNoError(job:MSN,job:Model_Number,'MSN')
            Error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>M.S.N. Incorrect Length.'
        End !If CheckLength(job:MSN,job:Model_Number,'MSN')
    End !If MSNRequired(job:Manufacturer) = Level:Benign

    If (def:Force_Job_Type = 'B' And func:Type = 'B') Or |
        (def:Force_Job_Type <> 'I' And func:Type = 'C')
        If job:Chargeable_Job <> 'YES' And job:Warranty_Job <> 'YES'
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Job Type(s) Missing.'
        End!If job:Chargeable_Job <> 'YES' And job:Warranty_Job <> 'YES'
        If job:Chargeable_Job = 'YES' And job:Charge_Type = ''
            Error# = 1
            glo:ErrorText   = Clip(glo:ErrorText) & '<13,10>Chargeable Charge Type Missing.'
        End !If job:Chargeable_Job = 'YES' And job:Charge_Type = ''
        If job:Warranty_Job = 'YES' and job:Warranty_Charge_Type = ''
            Error# = 1
            glo:ErrorText   = Clip(glo:ErrorText) & '<13,10>Warranty Charge Type Missing.'
        End !If job:Warranty_Job = 'YES' and job:Warranty_Charge_Type = ''
    End!If def:Force_Job_Type = 'B'

    If (def:Force_Engineer = 'B' And func:Type = 'B') Or |
        (def:Force_Engineer <> 'I' And func:Type = 'C')
        If job:Engineer = '' And job:Workshop = 'YES'
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Engineer Missing.'
        End!If job:Engineer = ''
    End!If def:Force_Engineer = 'B'

    If ForceRepairType(func:Type)
        If (job:Chargeable_Job = 'YES' And job:Repair_Type = '') Or |
            (job:Warranty_Job = 'YES' And job:Repair_Type_Warranty = '')
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Repair Type(s) Missing.'
        End
    End!If def:Force_Repair_Type = 'B'

    If ForceAuthorityNumber(func:Type)
        If job:Authority_Number = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Authority Number Missing.'
        End!If job:Authority_Number = ''
    End !If ForceAuthorityNumber(func:Type)

    If ForceIncomingCourier(func:Type)
        If job:Incoming_Courier = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Incoming Courier Missing.'
        End!If job:Incoming_Courier = ''
    End !If ForceIncomingCourier(func:Type)

    If ForceCourier(func:Type)
        If job:Courier = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Courier Missing.'
        End!If job:Courier = ''
    End !If ForceCourier(func:Type)

    If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,func:Type)
        If job:DOP = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>DOP Missing, required by Transit Type.'
        End !If job:DOP = ''
    End !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,func:Type)

    If ForceLocation(job:Transit_Type,job:Workshop,func:Type)
        If job:Location = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Location Missing, required by Transit Type.'
        End !If job:Location = ''
    End !If ForceLocation(job:Transit_Type,job:Workshop,func:Type)

    If ForceCustomerName(job:Account_Number,func:Type)
         If job:Initial = '' And job:Title = '' And job:Surname = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Customer Name Missing.'
         End!If job:Initial = '' And job:Title = '' And job:Surname = ''
    End !If ForceCustomerName(job:Account_Number,func:Type)

    If ForcePostcode(func:type)
        If job:Postcode = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Postcode Missing.'
        End!If job:Postcode = ''
    End !If ForcePostcode(func:type)

    If ForceDeliveryPostcode(func:Type)
        If job:Postcode_Delivery = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Delivery Postcode Missing.'
        End!If job:Postcode_Delivery = ''
    End !If ForceDeliveryPostcode(func:Type)

    tmp:ForceFaultCodes = 0
    If job:chargeable_job = 'YES'
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                If job:Repair_Type <> ''
                    Access:REPTYDEF.ClearKey(rtd:Chargeable_Key)
                    rtd:Chargeable  = 'YES'
                    rtd:Repair_Type = job:Repair_Type
                    If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                        !Found
                        If rtd:CompFaultCoding
                            tmp:ForceFaultCodes = 1
                        End !If rtd:CompFaultCoding

                    Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                Else !If job:Repair_Type <> ''
                    tmp:ForceFaultCodes = 1
                End !If job:Repair_Type <> ''
            End
        end !if
    End!If job:chargeable = 'YES'
    If job:warranty_job = 'YES' And tmp:ForceFaultCodes = 0
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:warranty_charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                If job:Repair_Type_Warranty <> ''
                    Access:REPTYDEF.ClearKey(rtd:Warranty_Key)
                    rtd:Warranty    = 'YES'
                    rtd:Repair_Type = job:Repair_Type_Warranty
                    If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                        !Found
                        If rtd:CompFaultCoding
                            tmp:ForceFaultCodes = 1
                        End !If rtd:CompFaultCoding
                    Else!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign

                Else !If job:Repair_Type_Warranty <> ''
                    tmp:ForceFaultCodes = 1
                End !If job:Repair_Type_Warranty <> ''
            End
        end !if
    End!If job:warranty_job = 'YES'

    If tmp:ForceFaultCodes = 1
!        If (def:Force_Fault_Coding = 'B' And func:Type = 'B') Or |
!            (def:Force_Fault_Coding <> 'I' and func:Type = 'C')

            setcursor(cursor:wait)
            save_maf_id = access:manfault.savefile()
            access:manfault.clearkey(maf:field_number_key)
            maf:manufacturer = job:manufacturer
            set(maf:field_number_key,maf:field_number_key)
            loop
                if access:manfault.next()
                   break
                end !if
                if maf:manufacturer <> job:manufacturer      |
                    then break.  ! end if

                If (maf:Compulsory_At_Booking = 'YES' ) Or (maf:Compulsory = 'YES' And func:Type = 'C')
                    Case maf:Field_Number
                        Of 1
                            If maf:restrictlength
                                If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength

                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat


                        Of 2
                            If maf:restrictlength
                                If Len(Clip(job:fault_code2)) < maf:lengthfrom or Len(Clip(job:fault_code2)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code2,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat

                        Of 3
                            If maf:restrictlength
                                If Len(Clip(job:fault_code3)) < maf:lengthfrom or Len(Clip(job:fault_code3)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code3,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat


                        Of 4
                            If maf:restrictlength
                                If Len(Clip(job:fault_code4)) < maf:lengthfrom or Len(Clip(job:fault_code4)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code4,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat


                        Of 5
                            If maf:restrictlength
                                If Len(Clip(job:fault_code5)) < maf:lengthfrom or Len(Clip(job:fault_code5)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code5,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat


                        Of 6
                            If maf:restrictlength
                                If Len(Clip(job:fault_code6)) < maf:lengthfrom or Len(Clip(job:fault_code6)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code6,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat


                        Of 7
                            If maf:restrictlength
                                If Len(Clip(job:fault_code7)) < maf:lengthfrom or Len(Clip(job:fault_code7)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code7,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat


                        Of 8
                            If maf:restrictlength
                                If Len(Clip(job:fault_code8)) < maf:lengthfrom or Len(Clip(job:fault_code8)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code8,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat


                        Of 9
                            If maf:restrictlength
                                If Len(Clip(job:fault_code9)) < maf:lengthfrom or Len(Clip(job:fault_code9)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code9,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat


                        Of 10
                            If maf:restrictlength
                                If Len(Clip(job:fault_code10)) < maf:lengthfrom or Len(Clip(job:fault_code10)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code10,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat



                        Of 11
                            If maf:restrictlength
                                If Len(Clip(job:fault_code11)) < maf:lengthfrom or Len(Clip(job:fault_code11)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code11,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat


                        Of 12
                            If maf:restrictlength
                                If Len(Clip(job:fault_code12)) < maf:lengthfrom or Len(Clip(job:fault_code12)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(job:fault_code1)) < maf:lengthfrom or Len(Clip(job:fault_code1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(job:Fault_Code12,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(job:Fault_Code1,maf:FieldFormat)
                            End !If maf:ForceFormat
                        Of 13
                            If maf:restrictlength
                                If Len(Clip(jobe:FaultCode13)) < maf:lengthfrom or Len(Clip(jobe:FaultCode13)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(jobe:FaultCode1)) < maf:lengthfrom or Len(Clip(jobe:FaultCode1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(jobe:FaultCode13,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(jobe:FaultCode1,maf:FieldFormat)
                            End !If maf:ForceFormat
                        Of 14
                            If maf:restrictlength
                                If Len(Clip(jobe:FaultCode14)) < maf:lengthfrom or Len(Clip(jobe:FaultCode14)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(jobe:FaultCode1)) < maf:lengthfrom or Len(Clip(jobe:FaultCode1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(jobe:FaultCode14,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(jobe:FaultCode1,maf:FieldFormat)
                            End !If maf:ForceFormat
                        Of 15
                            If maf:restrictlength
                                If Len(Clip(jobe:FaultCode15)) < maf:lengthfrom or Len(Clip(jobe:FaultCode15)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(jobe:FaultCode1)) < maf:lengthfrom or Len(Clip(jobe:FaultCode1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(jobe:FaultCode15,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(jobe:FaultCode1,maf:FieldFormat)
                            End !If maf:ForceFormat
                        Of 16
                            If maf:restrictlength
                                If Len(Clip(jobe:FaultCode16)) < maf:lengthfrom or Len(Clip(jobe:FaultCode16)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(jobe:FaultCode1)) < maf:lengthfrom or Len(Clip(jobe:FaultCode1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(jobe:FaultCode16,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(jobe:FaultCode1,maf:FieldFormat)
                            End !If maf:ForceFormat
                        Of 17
                            If maf:restrictlength
                                If Len(Clip(jobe:FaultCode17)) < maf:lengthfrom or Len(Clip(jobe:FaultCode17)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(jobe:FaultCode1)) < maf:lengthfrom or Len(Clip(jobe:FaultCode1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(jobe:FaultCode17,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(jobe:FaultCode1,maf:FieldFormat)
                            End !If maf:ForceFormat
                        Of 18
                            If maf:restrictlength
                                If Len(Clip(jobe:FaultCode18)) < maf:lengthfrom or Len(Clip(jobe:FaultCode18)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(jobe:FaultCode1)) < maf:lengthfrom or Len(Clip(jobe:FaultCode1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(jobe:FaultCode18,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(jobe:FaultCode1,maf:FieldFormat)
                            End !If maf:ForceFormat
                        Of 19
                            If maf:restrictlength
                                If Len(Clip(jobe:FaultCode19)) < maf:lengthfrom or Len(Clip(jobe:FaultCode19)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(jobe:FaultCode1)) < maf:lengthfrom or Len(Clip(jobe:FaultCode1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(jobe:FaultCode19,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(jobe:FaultCode1,maf:FieldFormat)
                            End !If maf:ForceFormat
                        Of 20
                            If maf:restrictlength
                                If Len(Clip(jobe:FaultCode20)) < maf:lengthfrom or Len(Clip(jobe:FaultCode20)) > maf:lengthto
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
                                End!If Len(Clip(jobe:FaultCode1)) < maf:lengthfrom or Len(Clip(jobe:FaultCode1)) > maf:lengthto
                            End!If maf:restrictlength
                            If maf:ForceFormat
                                If CheckFaultFormat(jobe:FaultCode20,maf:FieldFormat)
                                    error# = 1
                                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
                                End !If CheckFaultFormat(jobe:FaultCode1,maf:FieldFormat)
                            End !If maf:ForceFormat


                    End !Case maf:Field_Number
                End !If (maf:Compulsory_At_Booking = 'YES' And func:Type = 'B') Or maf:Compulsory = 'YES'
                If (maf:Compulsory_At_Booking = 'YES' ) Or (maf:Compulsory = 'YES' And func:Type = 'C')

                    Case maf:field_number
                        Of 1
                            If job:fault_code1 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End

                        Of 2
                            If job:fault_code2 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End
                        Of 3
                            If job:fault_code3 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End

                        Of 4
                            If job:fault_code4 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End

                        Of 5
                            If job:fault_code5 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End

                        Of 6
                            If job:fault_code6 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End

                        Of 7
                            If job:fault_code7 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End

                        Of 8
                            If job:fault_code8 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End

                        Of 9
                            If job:fault_code9 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End

                        Of 10
                            If job:fault_code10 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End

                        Of 11
                            If job:fault_code11 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End

                        Of 12
                            If job:fault_code12 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End
                        Of 13
                            If jobe:FaultCode13 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End
                        Of 14
                            If jobe:FaultCode14 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End
                        Of 15
                            If jobe:FaultCode15 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End
                        Of 16
                            If jobe:FaultCode16 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End
                        Of 17
                            If jobe:FaultCode17 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End
                        Of 18
                            If jobe:FaultCode18 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End
                        Of 19
                            If jobe:FaultCode19 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End
                        Of 20
                            If jobe:FaultCode20 = ''
                                error# = 1
                                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
                            End


                    End !Case maf:field_number
                End!If maf:compulsory = 'YES'
            End !loop
            access:manfault.restorefile(save_maf_id)
            setcursor()
!        End!If def:Force_Fault_Coding = 'B'
    End!If tmp:ForceFaultCodes = 1

    !The Trade Fault codes, should be independent of Charge Types,
    !therefore, they are compulsory if the boxes are ticked.
    access:jobse.clearkey(jobe:refnumberkey)
    jobe:refnumber  = job:Ref_number
    If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
      !Found

        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            If access:tradeacc.tryfetch(tra:account_number_Key) = Level:Benign
                setcursor(cursor:wait)
                save_taf_id = access:trafault.savefile()
                access:trafault.clearkey(taf:field_number_key)
                taf:accountnumber = tra:account_number
                set(taf:field_number_key,taf:field_number_key)
                loop
                    if access:trafault.next()
                       break
                    end !if
                    if taf:accountnumber <> tra:account_number      |
                        then break.  ! end if
                    If (taf:Compulsory_At_Booking = 'YES' ) Or (taf:Compulsory = 'YES' And func:Type = 'C')
                         Case taf:Field_Number
                            Of 1
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            Of 2
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode2)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode2)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            Of 3
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode3)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode3)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            Of 4
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode4)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode4)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            Of 5
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode5)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode5)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            Of 6
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode6)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode6)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            Of 7
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode7)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode7)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            Of 8
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode8)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode8)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            Of 9
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode9)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode9)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            Of 10
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode10)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode10)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            OF 11
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode11)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode11)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                            OF 12
                               If taf:restrictlength
                                    If Len(Clip(jobe:TraFaultCode12)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode12)) > taf:lengthto
                                        error# = 1
                                        glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Invalid Length: ' & Clip(taf:Field_name)
                                    End!If Len(Clip(jobe:TraFaultCode1)) < taf:lengthfrom or Len(Clip(jobe:TraFaultCode1)) > taf:lengthto
                                End!If taf:restrictlength

                        End !Case taf:Field_Number
                    End !If (ta:Compulsory_At_Booking = 'YES' and func:Type = 'B') Or func:Type = 'C'
                    If (taf:Compulsory_At_Booking = 'YES' ) Or (taf:Compulsory = 'YES' And func:Type = 'C')

                        Case taf:field_number
                            Of 1
                                If jobe:TraFaultCode1 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End
                            Of 2
                                If jobe:TraFaultCode2 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End
                            Of 3
                                If jobe:TraFaultCode3 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End

                            Of 4
                                If jobe:TraFaultCode4 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End

                            Of 5
                                If jobe:TraFaultCode5 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End

                            Of 6
                                If jobe:TraFaultCode6 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End

                            Of 7
                                If jobe:TraFaultCode7 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End

                            Of 8
                                If jobe:TraFaultCode8 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End

                            Of 9
                                If jobe:TraFaultCode9 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End

                            Of 10
                                If jobe:TraFaultCode10 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End

                            Of 11
                                If jobe:TraFaultCode11 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End

                            Of 12
                                If jobe:TraFaultCode12 = ''
                                    error# = 1
                                    glo:errortext = Clip(glo:errortext) & '<13,10>Trade Fault Code Missing: ' & Clip(taf:field_name)
                                End

                        End !Case taf:field_number
                    End!If taf:compulsory = 'YES'
                end !loop
                access:trafault.restorefile(save_taf_id)
                setcursor()
            End!If access:tradeacc.tryfetch(tra:account_number_Key) = Level:Benign
        End!if access:subtracc.fetch(sub:account_number_key) = level:benign
    Else! If access:jobse.tryfetch(jobe:refnumberkey                ) = Level:Benign
      !Error
    End! If access:jobse.tryfetch(jobe:refnumberkey                ) = Level:Benign


    !Check For Adjustments only at completion
    If job:warranty_job = 'YES' And func:Type = 'C' and tmp:ForceFaultCodes = 1
        access:manufact.clearkey(man:manufacturer_key)
        man:manufacturer  = job:manufacturer
        If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
          !Found
            If man:forceparts
                setcursor(cursor:wait)
                parts# = 0
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = job:ref_number
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> job:ref_number      |
                        then break.  ! end if
                    parts# = 1
                    Break
                end !loop
                setcursor()
                If parts# = 0
                    error# = 1
                    glo:errortext = Clip(glo:errortext) & '<13,10>Warranty Spares Missing'
                End !If found# = 0

            End!If man:forceparts

        Else! If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
          !Error
        End! If access:.tryfetch(man:manufacturer_key) = Level:Benign
    End!If job:warranty_job = 'YES'

    If (def:Force_Spares = 'B' And func:Type = 'B') Or |
        (def:Force_Spares <> 'I' And func:Type = 'C')
        If job:chargeable_job    = 'YES'
            setcursor(cursor:wait)
            parts# = 0
            access:parts.clearkey(par:part_number_key)
            par:ref_number  = job:ref_number
            set(par:part_number_key,par:part_number_key)
            loop
                if access:parts.next()
                   break
                end !if
                if par:ref_number  <> job:ref_number      |
                    then break.  ! end if
                parts# = 1
                Break
            end !loop
            setcursor()
            If parts# = 0
                error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>Chargeable Spares Missing'
            End !If found# = 0
        End !If job:job_Type = 'CHARGEABLE'
    End!If def:Force_Spares = 'B'

    !Qa before Completion
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = job:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:UseQA And func:Type = 'C'
            If ~man:QAAtCompletion And job:QA_Passed <> 'YES'
                error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>QA Has Not Been Passed'

            End !If man:QAAtCompletion And job:QA_Passed <> 'YES'
        End !If man:UseQA
    Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    !Parts Remaining
    tmp:FoundRequestedParts = 0
    tmp:FoundOrderedParts = 0
    setcursor(cursor:wait)
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        If tmp:ForceFaultCodes
            Do WarrantyPartFaultCodes
        End!If tmp:ForceFaultCodes

        If wpr:pending_ref_number <> '' and wpr:Order_Number = ''
            tmp:FoundRequestedParts = 1
            Break
        End
        If wpr:order_number <> '' And wpr:date_received = ''
            tmp:FoundOrderedParts = 1
            Break
        End
    end !loop
    access:warparts.restorefile(save_wpr_id)
    setcursor()

    If func:Type = 'C'
        setcursor(cursor:wait)
        save_par_id = access:parts.savefile()
        access:parts.clearkey(par:part_number_key)
        par:ref_number  = job:ref_number
        set(par:part_number_key,par:part_number_key)
        loop
            if access:parts.next()
               break
            end !if
            if par:ref_number  <> job:ref_number      |
                then break.  ! end if
            If par:pending_ref_number <> '' and par:Order_Number = ''
                tmp:FoundRequestedParts = 1
                Break
            End
            If par:order_number <> '' And par:date_received = ''
                tmp:FoundOrderedParts = 1
                Break
            End
        end !loop
        access:parts.restorefile(save_par_id)
        setcursor()

        If tmp:FoundRequestedParts = 1
            error# = 1
            If def:SummaryOrders
                glo:errortext = Clip(glo:errortext) & '<13,10>There are Unreceived spares attached'
            Else !If def:SummaryOrders
                glo:errortext = Clip(glo:errortext) & '<13,10>There are Unordered spares attached'
            End !If def:SummaryOrders

        Elsif tmp:FoundOrderedParts = 1
            error# = 1
            glo:errortext = Clip(glo:errortext) & '<13,10>There are Unreceived spares attached'
        End
    End!If func:Type = 'C'


    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:ForceOrderNumber and job:Order_Number = ''
                error# = 1
                glo:errorText   = Clip(glo:ErrorText) & '<13,10>Order Number Missing.'
            End !If tra:ForceOrderNumber
            If func:Type = 'C'
                If tra:ForceCommonFault
                    tmp:FoundCommonFault = 0
                    Save_aud_ID = Access:AUDIT.SaveFile()
                    Access:AUDIT.ClearKey(aud:Action_Key)
                    aud:Ref_Number = job:Ref_Number
                    aud:Action     = 'INSERTED COMMON FAULT'
                    Set(aud:Action_Key,aud:Action_Key)
                    Loop
                        If Access:AUDIT.NEXT()
                           Break
                        End !If
                        If aud:Ref_Number <> job:Ref_Number      |
                        Or aud:Action     <> 'INSERTED COMMON FAULT'      |
                            Then Break.  ! End If
                        tmp:FoundCommonFault = 1
                        Break
                    End !Loop
                    Access:AUDIT.RestoreFile(Save_aud_ID)
                    If tmp:FoundCommonFault = 0
                        error# = 1
                        glo:errortext = Clip(glo:errortext) & '<13,10>Common Fault Not Been Applied.'

                    End!If tmp:FoundCommonFault = 1
                End !If tra:ForceCommonFault
            End !If func:Type = 'C'

        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    If (Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) = 'B' And func:Type = 'B') Or |
        (Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) <> 'I' And func:Type = 'C')
        AccError# = 0
        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        If Access:JOBACC.Next()
            AccError# = 1
        Else !If Access:JOBACC.Next()
            If jac:Ref_Number <> job:Ref_Number
                AccError# = 1
            End !If jac:Ref_Number <> job:Ref_Number
        End !If Access:JOBACC.Next()
        If AccError# = 1
            error# = 1
            glo:errortext = Clip(glo:errortext) & '<13,10>Job Accessories Missing.'
        End !If AccError# = 1
    End !Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) = 'B'

    If GETINI('ESTIMATE','ForceAccepted',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        If job:Chargeable_Job = 'YES' and func:Type = 'C'
            If job:Estimate = 'YES' and job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                Error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>Estimate has not been Accepted/Rejected.'
            End !If job:Estimate = 'YES' and (job:Estiamte_Accepted <> 'YES' Or job:Estimate_Rejected <> 'YES')
        End !If job:Chargeable_Job = 'YES'
    End !If GETINI('ESTIMATE','ForceAccepted',,CLIP(PATH())&'\SB2KDEF.INI') = 1

    If ForceNetwork(func:Type)
        If jobe:Network = ''
            Error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Network Missing.'
        End !If jobe:Network = ''
    End !If ForceNetwork(func:Type)

WarrantyPartFaultCodes      Routine
    !Warranty Parts Fault Codes
    Save_map_ID = Access:MANFAUPA.SaveFile()
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = job:Manufacturer
    Set(map:Field_Number_Key,map:Field_Number_Key)
    Loop
        If Access:MANFAUPA.NEXT()
           Break
        End !If
        If map:Manufacturer <> job:Manufacturer      |
            Then Break.  ! End If
        Case map:Field_Number
            Of 1
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code1 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'
                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto  
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength

                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 2
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code2 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code2)) < map:lengthfrom or Len(Clip(wpr:Fault_Code2)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code2,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat

            Of 3
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code3 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code3)) < map:lengthfrom or Len(Clip(wpr:Fault_Code3)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code3,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 4
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code4 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code4)) < map:lengthfrom or Len(Clip(wpr:Fault_Code4)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code4,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 5
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code5 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code5)) < map:lengthfrom or Len(Clip(wpr:Fault_Code5)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code5,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 6
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code6 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code6)) < map:lengthfrom or Len(Clip(wpr:Fault_Code6)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code6,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 7
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code7 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code7)) < map:lengthfrom or Len(Clip(wpr:Fault_Code7)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code7,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 8
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code8 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code8)) < map:lengthfrom or Len(Clip(wpr:Fault_Code8)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code8,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 9
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code9 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code9)) < map:lengthfrom or Len(Clip(wpr:Fault_Code9)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code9,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 10
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code10 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code10)) < map:lengthfrom or Len(Clip(wpr:Fault_Code10)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code10,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat



            Of 11
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code11 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code11)) < map:lengthfrom or Len(Clip(wpr:Fault_Code11)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code11,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 12
                If map:Compulsory = 'YES'
                    If wpr:Fault_Code12 = ''
                        Error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
                    End !If wpr:Fault_Code1 = ''
                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(wpr:Fault_Code12)) < map:lengthfrom or Len(Clip(wpr:Fault_Code12)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(wpr:Fault_Code12,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat
            
        End !Case map:Field_Number
    End !Loop
    Access:MANFAUPA.RestoreFile(Save_map_ID)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CompulsoryFieldCheck',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_taf_id',save_taf_id,'CompulsoryFieldCheck',1)
    SolaceViewVars('save_maf_id',save_maf_id,'CompulsoryFieldCheck',1)
    SolaceViewVars('save_par_id',save_par_id,'CompulsoryFieldCheck',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'CompulsoryFieldCheck',1)
    SolaceViewVars('tmp:ForceFaultCodes',tmp:ForceFaultCodes,'CompulsoryFieldCheck',1)
    SolaceViewVars('tmp:FoundRequestedParts',tmp:FoundRequestedParts,'CompulsoryFieldCheck',1)
    SolaceViewVars('tmp:FoundOrderedParts',tmp:FoundOrderedParts,'CompulsoryFieldCheck',1)
    SolaceViewVars('tmp:FoundCommonFault',tmp:FoundCommonFault,'CompulsoryFieldCheck',1)
    SolaceViewVars('save_aud_id',save_aud_id,'CompulsoryFieldCheck',1)
    SolaceViewVars('save_map_id',save_map_id,'CompulsoryFieldCheck',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
