

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01002.INC'),ONCE        !Local module procedure declarations
                     END


Postcode_Routine     PROCEDURE  (f_postcode,f_add1,f_add2,f_add3) ! Declare Procedure
tmp:os               STRING(20)
szpostcode           CSTRING(20)
szpath               CSTRING(255)
szaddress            CSTRING(255)
tmp:high             BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
full_postcode       String(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Postcode_Routine')      !Add Procedure to Log
  end


!Postcode bit
    Setclipboard(f_postcode)
    Set(Defaults)
    access:defaults.next()
    tmp:os  = GetVersion()
    tmp:high    = Bshift(tmp:os,-8)


    If def:use_postcode = 'YES'

        If def:postcodedll <> 'YES'

            If tmp:high = 0
                Run(Clip(def:postcode_path) & '\addressc.exe',0)
            Else!If tmp:high = 0
                Run(Clip(def:postcode_path) & '\addressc.exe',1)
            End!If tmp:high = 0
            if error()
                error# = 1
            Else
                IF Sub(Clipboard(),1,1) = '#'
                    Case MessageEx('Invalid Postcode.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,500) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    error# = 1
                    f_postcode = ''
                    f_add1 = ''
                    f_add2 = ''
                    f_add3 = ''
                Else!if error
                    full_postcode = Clipboard()
                end!if error
            End
        Else!If def:postcodedll <> 'YES'
            szpath  = Clip(def:postcode_path)
            If Initaddress(szpath)
                szpostcode  = f_postcode
                If getaddress(szpostcode,szaddress)
                    full_postcode   = szaddress
                Else!
                    error# = 1
                End!If getaddress(szpostcode,szaddress)
            Else!If Initaddress(szpath)
                error# = 1
            End!If Initaddress(szpath)
        End!If def:postcodedll <> 'YES'

        If error# = 0
            Loop x# = 1 to len(full_postcode)
                If Sub(full_postcode,x#,1) = ';'
                    f_add1 = Sub(full_postcode,1,(x#-1))
                    Break
                End
            End
            Loop y# = (x#+1) to len(full_postcode)
                If sub(full_postcode,y#,1) = ';'
                    f_add2 = Sub(full_postcode,(x#+1),(y#-1)-(x#))
                    Break
                End
            End
            Loop z# = (y#+1) to len(full_postcode)
                If sub(full_postcode,z#,1) = ';'
                    f_add3 = Sub(full_postcode,(y#+1),(z#-1)-(y#))
                    f_postcode = Sub(full_postcode,(z#+1),len(full_postcode)-z#)
                    Break
                Else
                    f_add3 = ''
                    f_postcode = Sub(full_postcode,(y#+1),len(full_postcode)-y#)
                End
            End
            f_add1  = Upper(f_add1)
            f_add2  = Upper(f_add2)
            f_add3  = Upper(f_add3)
            f_postcode  = Upper(f_postcode)
        Else!If error# = 0
            Case MessageEx('A problem has occured with the Postcode Program.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        End!If error# = 0
    End


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Postcode_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:os',tmp:os,'Postcode_Routine',1)
    SolaceViewVars('szpostcode',szpostcode,'Postcode_Routine',1)
    SolaceViewVars('szpath',szpath,'Postcode_Routine',1)
    SolaceViewVars('szaddress',szaddress,'Postcode_Routine',1)
    SolaceViewVars('tmp:high',tmp:high,'Postcode_Routine',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
