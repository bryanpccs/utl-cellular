

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01045.INC'),ONCE        !Local module procedure declarations
                     END


CompleteDoExchange   PROCEDURE  (func:ExchangeUnitNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CompleteDoExchange')      !Add Procedure to Log
  end


!Do Replacement Exchange Unit Bit At Completion
    If func:ExchangeUnitNumber <> 0
        access:exchange_alias.clearkey(xch_ali:ref_number_key)
        xch_ali:ref_number  = func:ExchangeUnitNUmber
        If access:Exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign
            If xch_ali:audit_number <> ''                                               !If this has an audit number
                access:excaudit.clearkey(exa:audit_number_key)                          !Mark the replacement as ready
                exa:stock_type  = xch_ali:stock_type                                    !To be returned to stock
                exa:audit_number    = xch_ali:audit_number
                If access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number  = exa:stock_unit_number
                    If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                        get(exchhist,0)
                        if access:exchhist.primerecord() = level:benign
                            exh:ref_number   = xch:ref_number
                            exh:date          = today()
                            exh:time          = clock()
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            exh:user = use:user_code
                            exh:status        = 'REMOVED FROM AUDIT NUMBER: ' & Clip(xch:audit_number)
                            access:exchhist.insert()
                        end!if access:exchhist.primerecord() = level:benign
                        xch:audit_number    = ''
                        access:exchange.update()
                    End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number  = exa:replacement_unit_number
                    If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                        get(exchhist,0)
                        if access:exchhist.primerecord() = level:benign
                            exh:ref_number   = xch:ref_number
                            exh:date          = today()
                            exh:time          = clock()
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            exh:user = use:user_code
                            exh:status        = 'UNIT REPAIRED. (AUDIT:' &|
                                                Clip(exa:audit_number) & ')'
                            access:exchhist.insert()
                        end!if access:exchhist.primerecord() = level:benign
                        xch:audit_number    = exa:audit_number
                        xch:available   = 'RTS'
                        access:exchange.update()
                    End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                End!If access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
            Else!If xch_ali:audit_number <> ''
                access:exchange.clearkey(xch:esn_available_key)
                xch:available  = 'REP'
                xch:stock_type = xch_ali:stock_type
                xch:esn        = job:esn
                if access:exchange.tryfetch(xch:esn_available_key) = Level:Benign
                    get(exchhist,0)
                    if access:exchhist.primerecord() = level:benign
                        exh:ref_number   = xch:ref_number
                        exh:date          = today()
                        exh:time          = clock()
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        exh:user = use:user_code
                        exh:status        = 'REPAIR COMPLETED'
                        access:exchhist.insert()
                    end!if access:exchhist.primerecord() = level:benign
                    xch:available = 'RTS'
                    access:exchange.update()
                end!if access:exchange.tryfetch(xch:esn_available_key)
            End!If xch:audit_number <> ''
        End!If access:Exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign

    End !If func:ExchangeUnitNumber <> 0


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CompleteDoExchange',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
