

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01067.INC'),ONCE        !Local module procedure declarations
                     END


DeleteChargeablePart PROCEDURE                        ! Declare Procedure
Local                CLASS
CreateStockHistory   PROCEDURE(String func:Information)
                     END
Result               BYTE
Msg1                 STRING(256)
Msg2                 STRING(256)
save_orp_id          USHORT,AUTO
tmp:Delete           BYTE(0)
tmp:Scrap            BYTE(0)
tmp:StockNumber      LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'DeleteChargeablePart')      !Add Procedure to Log
  end


    tmp:Delete  = 0
    tmp:Scrap   = 0
    ! Start Change 2466 BE(28/11/03)
    !If par:Adjustment = 'YES'
    IF ((par:Adjustment = 'YES') OR (par:Exclude_From_Order = 'YES')) THEN
    ! End Change 2466 BE(28/11/03)
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to delete this part?', |
                'ServiceBase 2000', Icon:Question, |
                 '&Yes|&No', 2, 0)
        Of 1  ! Name: &Yes
            tmp:Delete = 1
        Of 2  ! Name: &No  (Default)
        End !CASE
    Else !If par:Adjustment = 'YES'
        tmp:StockNumber = 0

        If par:Part_Ref_Number <> ''
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number  = par:Part_Ref_Number
            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found
                tmp:StockNumber = sto:Ref_Number
            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            
        Else !If par:Part_Ref_Number <>

        End !If par:Part_Ref_Number <>

        If par:Order_Number <> ''
            If par:Date_Received <> ''
                If tmp:StockNumber and sto:Sundry_Item = 'YES'
                    tmp:Delete = 1
                Else !If tmp:StockNumber and sto:Sundry_Item = 'YES'
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    ! Start 2632 BE(20/05/03)
                    !     Case Message('This part exists on Order Number '&Clip(par:Order_Number)&' '&|
                    !             'and has been Received.'&|
                    !             '||Do you wish to RESTOCK this part, or SCRAP it?', |
                    !             'ServiceBase 2000', Icon:Question, |
                    !              'Restock|Scrap|Cancel', 3, 0)
                    msg1 = 'This part exists on Order Number ' & Clip(par:Order_Number) & ' and has been Received.'
                    msg2 = 'Do you wish to RESTOCK this part, or SCRAP it?'
                    ScrapDialog(msg1, msg2, 'Restock', 'Scrap', 'Cancel', result)
                    CASE result
                    ! End 2632 BE(20/05/03)
                    Of 1  ! Name: Restock
                        If tmp:StockNumber
                            Beep(Beep:SystemQuestion)  ;  Yield()
                            Case Message('This item was originally taken from location: '&|
                                    Clip(sto:Location)&'.'&|
                                    '||Do you wish to return it to it''s ORIGINAL location, or '&|
                                    'to a NEW Location?', |
                                    'ServiceBase 2000', Icon:Question, |
                                     'Original|New|Cancel', 3, 0)
                            Of 1  ! Name: Original
                                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                sto:Ref_Number  = tmp:StockNumber
                                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Found
                                    sto:Quantity_Stock += par:Quantity
                                    If Access:STOCK.Update() = Level:Benign
                                        Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & par:order_number)
                                        tmp:Delete = 1
                                    End !If Access:STOCK.Update() = Level:Benign
                                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                
                            Of 2  ! Name: New
                                glo:Select1 = ''
                                Pick_New_Location
                                If glo:Select1 <> ''
                                    Access:STOCK.Clearkey(sto:Location_Part_Description_Key)
                                    sto:Location    = glo:Select1
                                    sto:Part_Number = par:Part_Number
                                    sto:Description = par:Description
                                    If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                        !Found
                                        sto:Quantity_Stock += par:Quantity
                                        If Access:STOCK.Update() = Level:Benign
                                            Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & par:order_number)
                                            tmp:Delete = 1
                                        End !If Access:STOCK.Update() = Level:Benign
                                    Else! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                        Beep(Beep:SystemQuestion)  ;  Yield()
                                        ! Start 2632 BE(20/05/03)
                                        !Case Message('Cannot find the selected part in location: '&|
                                        !        Clip(glo:Select1)&'.'&|
                                        !        '||Do you wish to add it as a NEW item, or SCRAP it?', |
                                        !        'ServiceBase 2000', Icon:Question, |
                                        !         'New|Scrap|Cancel', 3, 0)
                                        msg1 = 'Cannot find the selected part in location: ' & Clip(glo:Select1) & '.'
                                        msg2 = 'Do you wish to add it as a NEW item, or SCRAP it?'
                                        ScrapDialog(msg1, msg2, 'New', 'Scrap', 'Cancel', result)
                                        CASE result
                                        ! End 2632 BE(20/05/03)
                                        Of 1  ! Name: New
                                            glo:Select2 = ''
                                            glo:Select3 = ''
                                            Pick_Locations
                                            If glo:Select1 <> ''
                                                If Access:STOCK.PrimeRecord() = Level:Benign
                                                    sto:Part_Number     = par:Part_Number
                                                    sto:Description     = par:Description
                                                    sto:Supplier        = par:Supplier
                                                    sto:Purchase_Cost   = par:Purchase_Cost
                                                    sto:Sale_Cost       = par:Sale_Cost
                                                    sto:Shelf_Location  = glo:Select2
                                                    sto:Manufacturer    = job:Manufacturer
                                                    sto:Location        = glo:Select1
                                                    sto:Second_Location = glo:Select3
                                                    If Access:STOCK.TryInsert() = Level:Benign
                                                        !Insert Successful
                                                        Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & par:order_number)
                                                        tmp:Delete = 1
                                                    Else !If Access:STOCK.TryInsert() = Level:Benign
                                                        !Insert Failed
                                                    End !If Access:STOCK.TryInsert() = Level:Benign
                                                End !If Access:STOCK.PrimeRecord() = Level:Benign
                                               
                                            End !If glo:Select1 <> ''
                                        Of 2  ! Name: Scrap
                                            tmp:Delete  = 1
                                            tmp:Scrap   = 1
                                        Of 3  ! Name: Cancel  (Default)
                                        End !CASE
                                    End! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                    
                                End !If glo:Select1 <> ''
                            Of 3  ! Name: Cancel  (Default)
                            End !CASE
                        Else !If tmp:StockNumber
                            glo:Select1 = ''
                            glo:Select2 = ''
                            glo:Select3 = ''
                            Pick_Locations
                            If glo:Select1 <> ''
                                If Access:STOCK.PrimeRecord() = Level:Benign
                                    sto:Part_Number     = par:Part_Number
                                    sto:Description     = par:Description
                                    sto:Supplier        = par:Supplier
                                    sto:Purchase_Cost   = par:Purchase_Cost
                                    sto:Sale_Cost       = par:Sale_Cost
                                    sto:Shelf_Location  = glo:Select2
                                    sto:Manufacturer    = job:Manufacturer
                                    sto:Location        = glo:Select1
                                    sto:Second_Location = glo:Select3
                                    If Access:STOCK.TryInsert() = Level:Benign
                                        !Insert Successful
                                        Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & par:order_number)
                                        tmp:Delete = 1
                                    Else !If Access:STOCK.TryInsert() = Level:Benign
                                        !Insert Failed
                                    End !If Access:STOCK.TryInsert() = Level:Benign
                                End !If Access:STOCK.PrimeRecord() = Level:Benign
                            End !If glo:Select1 <> ''
                        End !If tmp:StockNumber
                    Of 2  ! Name: Scrap
                        ! Start Change 2466 BE(28/11/03)
                        tmp:Delete  = 1
                        tmp:Scrap   = 1
                        ! End Change 2466 BE(28/11/03)
                    Of 3  ! Name: Cancel  (Default)
                    
                    End !CASE                        
                End !If tmp:StockNumber and sto:Sundry_Item = 'YES'
            Else !If par:Date_Recevied <> ''
                Beep(Beep:SystemQuestion)  ;  Yield()
                Case Message('The selected part is on Order '&Clip(par:Order_Number)&' '&|
                        'but has NOT been received.'&|
                        '||If you choose to delete it, the part on the order will be '&|
                        'marked as RECEIVED.'&|
                        '||Are you sure you want to delete this part?', |
                        'ServiceBase 2000', Icon:Question, |
                         '&Yes|&No', 2, 0)
                Of 1  ! Name: &Yes
                    Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
                    orp:Record_Number   = par:Order_Part_Number
                    If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
                        !Found
                        orp:Quantity    = 0
                        orp:Date_Received   = Today()
                        orp:Number_Received = 0
                        orp:All_Received    = 'YES'
                        If Access:ORDPARTS.Update() = Level:Benign
                            !Check if all received
                            found# = 0
                            Setcursor(cursor:wait)
                            save_orp_id = access:ordparts.savefile()
                            access:ordparts.clearkey(orp:order_Number_key)
                            orp:order_number    = par:order_number
                            Set(orp:order_number_key,orp:order_number_key)
                            Loop
                                If access:ordparts.next()
                                    Break
                                End!If access:ordparts.next()
                                If orp:order_Number <> par:order_Number
                                    Break
                                End!If orp:order_Number <> par:order_Number
                                If orp:all_received <> 'YES'
                                    found# = 1
                                    Break
                                End!If orp:all_received <> 'YES'
                            End!Loop
                            access:ordparts.restorefile(save_orp_id)
                            setcursor()
    
                            If found# = 0
                                access:orders.clearkey(ord:order_Number_key)
                                ord:order_number    = par:order_number
                                If access:orders.tryfetch(ord:order_number_key) = Level:Benign
                                    ord:all_received = 'YES'
                                    access:orders.update()
                                End!If access:orders.tryfetch(ord:order_number_key) = Level:Benign
                            End!If found# = 0
                            tmp:Delete = 1
                        End !If Access:ORDPARTS.Update() = Level:Benign
                    Else! If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
                    
                Of 2  ! Name: &No  (Default)
                End !CASE
            End !If par:Date_Recevied <> ''
        Else  !If par:Order_Number <> ''
            If par:Pending_Ref_Number <> ''
                Beep(Beep:SystemQuestion)  ;  Yield()
                Case Message('This part has been marked to appear on the NEXT Parts '&|
                        'Order Generation.'&|
                        '||Are you sure you want to delete it?', |
                        'ServiceBase 2000', Icon:Question, |
                         '&Yes|&No', 2, 0)
                Of 1  ! Name: &Yes
                    Case def:SummaryOrders
                        Of 0!Old Way
                            Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                            ope:Ref_Number  = par:Pending_Ref_Number
                            If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                !Found
                                Delete(ORDPEND)
                            Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                        Of 1!New Way
                            Access:ORDPEND.Clearkey(ope:Supplier_Key)
                            ope:Supplier    = par:Supplier
                            ope:Part_Number = par:Part_Number
                            If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                !Found
                                ope:Quantity -= par:Quantity
                                If ope:Quantity <=0
                                    Delete(ORDPEND)
                                Else !If ope:Quantity <=0
                                    Access:ORDPEND.Update()
                                End !If ope:Quantity <=0
                            Else! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                       
                    End !Case def:SummaryOrders
                    tmp:Delete = 1                   
                Of 2  ! Name: &No  (Default)
                End !CASE                
            Else !If par:Pending_Ref_Number <> ''
                Beep(Beep:SystemQuestion)  ;  Yield()
                ! Start 2632 BE(20/05/03)
                !Case Message('You have selected to delete this part.'&|
                !        '||Do you wish to RESTOCK it, or SCRAP it?', |
                !        'ServiceBase 2000', Icon:Question, |
                !         'Restock|Scrap|Cancel', 3, 0)
                msg1 = 'You have selected to delete this part.'
                msg2 = 'Do you wish to RESTOCK it, or SCRAP it?'
                ScrapDialog(msg1, msg2, 'Restock', 'Scrap', 'Cancel', result)
                CASE result
                ! End 2632 BE(20/05/03)
                Of 1  ! Name: Restock
                    If tmp:StockNumber
                        Beep(Beep:SystemQuestion)  ;  Yield()
                        Case Message('The selected part was originally taken from location: '&|
                                ''&clip(sto:Location)&'.'&|
                                '||Do you wish to return it to it''s ORIGINAL location, or '&|
                                'to a NEW location?', |
                                'ServiceBase 2000', Icon:Question, |
                                 'Original|New|Cancel', 3, 0)
                        Of 1  ! Name: Original
                            Access:STOCK.Clearkey(sto:Ref_Number_Key)
                            sto:Ref_Number  = tmp:StockNumber
                            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                !Found
                                sto:Quantity_stock += par:Quantity
                                If Access:STOCK.Update() = Level:Benign
                                    Local.CreateStockHistory('')
                                    tmp:Delete = 1
                                End !If Access:STOCK.Update() = Level:Benign
                            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        Of 2  ! Name: New
                            glo:Select1 = ''
                            Pick_New_Location
                            If glo:Select1 <> ''
                                Access:STOCK.Clearkey(sto:Location_Part_Description_Key)
                                sto:Location    = glo:Select1
                                sto:Part_Number = par:Part_Number
                                sto:Description = par:Description
                                If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                    !Found
                                    sto:Quantity_Stock += par:Quantity
                                    If Access:STOCK.Update() = Level:Benign
                                        Local.CreateStockHistory('')
                                        tmp:Delete = 1
                                    End !If Access:STOCK.Update() = Level:Benign
                                Else! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                    Beep(Beep:SystemQuestion)  ;  Yield()
                                    ! Start 2632 BE(20/05/03)
                                    !Case Message('Cannot find the selected part in location: '&|
                                    !        Clip(glo:Select1)&'.'&|
                                    !        '||Do you wish to add it as a NEW part, or SCRAP it?', |
                                    !        'ServiceBase 2000', Icon:Question, |
                                    !         'New|Scrap|Cancel', 3, 0)
                                    msg1 = 'Cannot find the selected part in location: ' & Clip(glo:Select1) & '.'
                                    msg2 = 'Do you wish to add it as a NEW part, or SCRAP it?'
                                    ScrapDialog(msg1, msg2, 'New', 'Scrap', 'Cancel', result)
                                    CASE result
                                    ! End 2632 BE(20/05/03)
                                    Of 1  ! Name: New
                                        glo:Select2 = ''
                                        glo:Select3 = ''
                                        Pick_Locations
                                        If glo:Select1 <> ''
                                            If Access:STOCK.PrimeRecord() = Level:Benign
                                                sto:Part_Number     = par:Part_Number
                                                sto:Description     = par:Description
                                                sto:Supplier        = par:Supplier
                                                sto:Purchase_Cost   = par:Purchase_Cost
                                                sto:Sale_Cost       = par:Sale_Cost
                                                sto:Shelf_Location  = glo:select2
                                                sto:Manufacturer    = job:Manufacturer
                                                sto:Location        = glo:Select1
                                                sto:Second_Location = glo:Select3
                                                If Access:STOCK.TryInsert() = Level:Benign
                                                    !Insert Successful
                                                    Local.CreateStockHistory('')
                                                    tmp:Delete = 1
                                                Else !If Access:STOCK.TryInsert() = Level:Benign
                                                    !Insert Failed
                                                End !If Access:STOCK.TryInsert() = Level:Benign
                                            End !If Access:STOCK.PrimeRecord() = Level:Benign
                                        End !If glo:Select1 <> ''
                                    Of 2  ! Name: Scrap
                                        tmp:Delete  = 1
                                        tmp:Scrap   = 1
                                    Of 3  ! Name: Cancel  (Default)
                                    End !CASE
                                End! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                
                            End !If glo:Select1 <> ''
                        Of 3  ! Name: Cancel  (Default)

                        End !CASE
                        
                    Else !If tmp:StockNumber
                        glo:Select1 = ''
                        glo:Select2 = ''
                        glo:Select3 = ''
                        Pick_Locations
                        If glo:Select1 <> ''
                            If Access:STOCK.PrimeRecord() = Level:Benign
                                sto:Part_Number     = par:Part_Number
                                sto:Description     = par:Description
                                sto:Supplier        = par:Supplier
                                sto:Purchase_Cost   = par:Purchase_Cost
                                sto:Sale_Cost       = par:Sale_Cost
                                sto:Shelf_Location  = glo:Select2
                                sto:Manufacturer    = job:Manufacturer
                                sto:Location        = glo:Select1
                                sto:Second_Location = glo:Select3
                                If Access:STOCK.TryInsert() = Level:Benign
                                    !Insert Successful
                                    Local.CreateStockHistory('')
                                    tmp:Delete = 1
                                Else !If Access:STOCK.TryInsert() = Level:Benign
                                    !Insert Failed
                                End !If Access:STOCK.TryInsert() = Level:Benign
                            End !If Access:STOCK.PrimeRecord() = Level:Benign
                        End !If glo:Select1 <> ''
                    End !If tmp:StockNumber
                Of 2  ! Name: Scrap
                    tmp:Delete  = 1
                    tmp:Scrap   = 1
                Of 3  ! Name: Cancel  (Default)
                End !CASE                                
            End !If par:Pending_Ref_Number <> ''
        End !If par:Order_Number <> ''
    End !If par:Adjustment = 'YES'
    If tmp:Scrap
        If Access:AUDIT.PrimeRecord() = Level:Benign
            ! Start 2632/3 BE(02/06/03)
            !aud:Notes         = 'DESCRIPTION: ' & Clip(par:Description)
            aud:Notes         = 'DESCRIPTION: ' & CLIP(par:Description)  & '<13,10>' & |
                                'QUANTITY = ' & CLIP(LEFT(FORMAT((par:Quantity), @n_8)))
            ! End 2632/3 BE(02/06/03)
            aud:Ref_Number    = job:ref_number
            aud:Date          = Today()
            aud:Time          = Clock()
            aud:Type          = 'JOB'
            Access:USERS.ClearKey(use:Password_Key)
            use:Password      = glo:Password
            Access:USERS.Fetch(use:Password_Key)
            aud:User          = use:User_Code
            aud:Action        = 'SCRAP CHARGEABLE PART: ' & Clip(par:Part_Number)
            Access:AUDIT.Insert()
        End!If Access:AUDIT.PrimeRecord() = Level:Benign
    End !If Scrap# = 1

    Return tmp:Delete
Local.CreateStockHistory          Procedure(String    func:Information)
Code
    if access:stohist.primerecord() = level:benign
        shi:ref_number           = sto:ref_number
        access:users.clearkey(use:password_key)
        use:password              = glo:password
        access:users.fetch(use:password_key)
        shi:user                  = use:user_code    
        shi:date                 = today()
        shi:transaction_type     = 'REC'
        shi:despatch_note_number = par:despatch_note_number
        shi:job_number           = job:Ref_number
        shi:quantity             = paR:quantity
        shi:purchase_cost        = par:purchase_cost
        shi:sale_cost            = par:sale_cost
        shi:retail_cost          = par:retail_cost
        shi:information          = 'CHARGEABLE PART REMOVED FROM JOB'
        shi:notes                = func:Information
        if access:stohist.insert()
           access:stohist.cancelautoinc()
        end
        tmp:Delete = 1
    end!if access:stohist.primerecord() = level:benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DeleteChargeablePart',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Local',Local,'DeleteChargeablePart',1)
    SolaceViewVars('Result',Result,'DeleteChargeablePart',1)
    SolaceViewVars('Msg1',Msg1,'DeleteChargeablePart',1)
    SolaceViewVars('Msg2',Msg2,'DeleteChargeablePart',1)
    SolaceViewVars('save_orp_id',save_orp_id,'DeleteChargeablePart',1)
    SolaceViewVars('tmp:Delete',tmp:Delete,'DeleteChargeablePart',1)
    SolaceViewVars('tmp:Scrap',tmp:Scrap,'DeleteChargeablePart',1)
    SolaceViewVars('tmp:StockNumber',tmp:StockNumber,'DeleteChargeablePart',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
