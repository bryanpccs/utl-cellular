

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01066.INC'),ONCE        !Local module procedure declarations
                     END


RemovePartsAndInvoiceText PROCEDURE                   ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'RemovePartsAndInvoiceText')      !Add Procedure to Log
  end


    If SecurityCheck('REMOVE PARTS AND INVOICE TEXT')
        Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
    Else !SecurityCheck('REMOVE PARTS AND INVOICE TEXT')
        FoundWar# = 0
        FoundChar# = 0
        Error# = 0

        If job:Chargeable_Job = 'YES'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                FoundChar# = 1
                Break
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If job:Chargeable_Job = 'YES'

        If job:Warranty_Job = 'YES'
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                FoundWar# = 1
                Break
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
        End !If job:Warranty_Job = 'YES'

        If FoundWar# And FoundChar#
            Case MessageEx('There are Warranty and Chargeable Parts attached to this job.'&|
              '<13,10>'&|
              '<13,10>Which parts do you want to remove?','ServiceBase 2000',|
                           'Styles\question.ico','|&All Parts|&Warranty Only|&Chargeable Only|&Cancel',4,4,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                Of 1 ! &All Parts Button
                Of 2 ! &Warranty Only Button
                    FoundChar# = 0
                Of 3 ! &Chargeable Only Button
                    FoundWar# = 0
                Of 4 ! &Cancel Button
                    Error# = 1
            End!Case MessageEx
        End !If FoundWar# And FoundChar#
        If Error# = 0
            Case MessageEx('Are you sure you want to RESTOCK the parts and DELETE the invoice text from this job?','ServiceBase 2000',|
                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                Of 1 ! &Yes Button
                    If FoundChar#
                        Save_par_ID = Access:PARTS.SaveFile()
                        Access:PARTS.ClearKey(par:Part_Number_Key)
                        par:Ref_Number  = job:Ref_Number
                        Set(par:Part_Number_Key,par:Part_Number_Key)
                        Loop
                            If Access:PARTS.NEXT()
                               Break
                            End !If
                            If par:Ref_Number  <> job:Ref_Number      |
                                Then Break.  ! End If
                            If par:Order_Number <> ''
                                Case MessageEx('Cannot delete Chargeable Part ' & clip(par:Part_Number) & '. It exists on an order.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                Cycle
                            End !If par:Order_Number <> ''
                            StockError# = 0
                            If par:Part_Ref_Number <> '' and par:Pending_Ref_number = 0
                                !From Stock
                                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                sto:Ref_Number  = par:Part_Ref_Number
                                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Found
                                    If sto:Sundry_Item <> 'YES'
                                        sto:quantity_stock += par:quantity
                                        If access:stock.update() = Level:Benign
                                            get(stohist,0)
                                            if access:stohist.primerecord() = level:benign
                                                shi:ref_number           = sto:ref_number
                                                access:users.clearkey(use:password_key)
                                                use:password              = glo:password
                                                access:users.fetch(use:password_key)
                                                shi:user                  = use:user_code    
                                                shi:date                 = today()
                                                shi:transaction_type     = 'ADD'
                                                shi:despatch_note_number = par:despatch_note_number
                                                shi:job_number           = job:Ref_number
                                                shi:quantity             = par:quantity
                                                shi:purchase_cost        = par:purchase_cost
                                                shi:sale_cost            = par:sale_cost
                                                shi:retail_cost          = par:retail_cost
                                                shi:information          = 'CHARGEABLE PART REMOVED FROM JOB'
                                                if access:stohist.insert()
                                                   access:stohist.cancelautoinc()
                                                end
                                            end!if access:stohist.primerecord() = level:benign
                                        End!If access:stock.update = Level:Benign
                                        !Begin deletion of pick detail entry TH
                                        par:Quantity = 0 !reset since it is about to be deleted - can utilise
                                        if def:PickNoteNormal or def:PickNoteMainStore then
                                            ChangePickingPart('chargeable')
                                            DeletePickingParts('chargeable',0)
                                        end
                                    End !If sto:Sundry_Item = 'YES'
                                    Delete(PARTS)
                                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                    StockError# = 1
                                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            Else !If par:Part_Ref_Number <> ''
                                StockError# = 1
                            End !If par:Part_Ref_Number <> ''

                            If StockError#
                                Case MessageEx('Cannot Restock Chargeable Part ' & clip(par:Part_Number) & '. Unable to find Stock Item.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                Cycle
                            End !If StockError#

                        End !Loop
                        Access:PARTS.RestoreFile(Save_par_ID)
                    End !If FoundChar#
                    If FoundWar#
                        Save_wpr_ID = Access:WARPARTS.SaveFile()
                        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                        wpr:Ref_Number  = job:Ref_Number
                        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        Loop
                            If Access:WARPARTS.NEXT()
                               Break
                            End !If
                            If wpr:Ref_Number  <> job:Ref_Number      |
                                Then Break.  ! End If
                            If wpr:Order_Number <> ''
                                Case MessageEx('Cannot delete Warranty Part ' & clip(wpr:Part_Number) & '. It exists on an order.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                Cycle
                            End !If wpr:Order_Number <> ''
                            StockError# = 0
                            If wpr:Part_Ref_Number <> '' and wpr:Pending_Ref_Number = 0
                                !From Stock
                                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                sto:Ref_Number  = wpr:Part_Ref_Number
                                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Found
                                    If sto:Sundry_Item <> 'YES'
                                        sto:quantity_stock += wpr:quantity
                                        If access:stock.update() = Level:Benign
                                            get(stohist,0)
                                            if access:stohist.primerecord() = level:benign
                                                shi:ref_number           = sto:ref_number
                                                access:users.clearkey(use:password_key)
                                                use:password              = glo:password
                                                access:users.fetch(use:password_key)
                                                shi:user                  = use:user_code    
                                                shi:date                 = today()
                                                shi:transaction_type     = 'ADD'
                                                shi:despatch_note_number = wpr:despatch_note_number
                                                shi:job_number           = job:Ref_number
                                                shi:quantity             = wpr:quantity
                                                shi:purchase_cost        = wpr:purchase_cost
                                                shi:sale_cost            = wpr:sale_cost
                                                shi:retail_cost          = wpr:retail_cost
                                                shi:information          = 'WARRANTY PART REMOVED FROM JOB'
                                                if access:stohist.insert()
                                                   access:stohist.cancelautoinc()
                                                end
                                            end!if access:stohist.primerecord() = level:benign
                                        End!If access:stock.update = Level:Benign
                                        !Begin deletion of pick detail entry TH
                                        wpr:Quantity = 0 !reset since it is about to be deleted - can utilise
                                        if def:PickNoteNormal or def:PickNoteMainStore then
                                            ChangePickingPart('warranty')
                                            DeletePickingParts('warranty',0)
                                        end
                                    End !If sto:Sundry_Item = 'YES'
                                    Delete(WARPARTS)
                                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                    StockError# = 1
                                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            Else !If wpr:Part_Ref_Number <> ''
                                StockError# = 1
                            End !If wpr:Part_Ref_Number <> ''

                            If StockError#
                                Case MessageEx('Cannot Restock Chargeable Part ' & clip(wpr:Part_Number) & '. Unable to find Stock Item.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                Cycle
                            End !If StockError#

                        End !Loop
                        Access:WARPARTS.RestoreFile(Save_wpr_ID)
                    End !If FoundChar#

                    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                    jbn:RefNumber   = job:Ref_Number
                    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        !Found
                        jbn:Invoice_Text = ''
                        Access:JOBNOTES.Update()
                    Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign

                    If Access:AUDIT.PrimeRecord() = Level:Benign
                        aud:Ref_Number    = job:ref_number
                        aud:Date          = Today()
                        aud:Time          = Clock()
                        aud:Type          = 'JOB'
                        Access:USERS.ClearKey(use:Password_Key)
                        use:Password      = glo:Password
                        Access:USERS.Fetch(use:Password_Key)
                        aud:User          = use:User_Code
                        aud:Action        = 'PARTS AND INVOICE TEXT REMOVED'
                        Access:AUDIT.Insert()
                    End!If Access:AUDIT.PrimeRecord() = Level:Benign

                Of 2 ! &No Button
            End!Case MessageEx
        End !If Error# = 0
    End !SecurityCheck('REMOVE PARTS AND INVOICE TEXT')


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RemovePartsAndInvoiceText',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_par_id',save_par_id,'RemovePartsAndInvoiceText',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'RemovePartsAndInvoiceText',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
