

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01018.INC'),ONCE        !Local module procedure declarations
                     END


ToBeExchanged        PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'ToBeExchanged')      !Add Procedure to Log
  end


!Will this or does this job have an exchange unit on it?
!1 = Exchange unit is already attached to the job
!2 = Exchange unit is NOT attached, but the transit type is marked 'Exchange Unit Required'
!3 = Exchange unit is NOT attached, but the Exchange Status is 108 Exchange Unit Required
    If job:exchange_unit_number <> ''
        Return 1
    End!If job:exchange_unit_number <> ''

    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = job:transit_type
    if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
        If trt:exchange_unit = 'YES'
            Return 2
        End!If trt:exchange = 'YES'
    End!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign

    If Sub(job:exchange_status,1,3) = '108'
        Return 3
    End!If Sub(job:exchange_status,1,3) = '108'

    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ToBeExchanged',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
