

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01021.INC'),ONCE        !Local module procedure declarations
                     END


CountBouncer         PROCEDURE  (f_RefNumber,f_DateBooked,f_IMEI) ! Declare Procedure
tmp:DateBooked       DATE
tmp:count            LONG
save_job2_id         USHORT,AUTO
tmp:IMEI             STRING(30)
tmp:Manufacturer     STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CountBouncer')      !Add Procedure to Log
  end


    !Pass the Job Number. Use this to get the job's IMEI Number, Date Booked and Manufacturer
    !From that count how many times the IMEI number has been booked in before

    tmp:count    = 0
    If f_IMEI <> 'N/A' And f_IMEI <> ''
        Set(defaults)
        Access:Defaults.Next()

        setcursor(cursor:wait)
        save_job2_id = access:jobs2_alias.savefile()
        access:jobs2_alias.clearkey(job2:esn_key)
        job2:esn = f_imei
        set(job2:esn_key,job2:esn_key)
        loop
            if access:jobs2_alias.next()
               break
            end !if
            if job2:esn <> f_imei      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0                                            
            end !if

            If job2:Cancelled = 'YES'
                Cycle
            End !If job2:Cancelled = 'YES'

            If job2:Exchange_Unit_NUmber <> ''
                Cycle
            End !If job2:Exchange_Unit_NUmber <> ''

            ! In mutiple job booking, no job number is passed - TrkBs: 6503 (DBH: 21-10-2005)
            If f_RefNumber > 0
                ! Check you haven't found the job you've just passed - TrkBs: 6503 (DBH: 21-10-2005)
                If f_RefNumber = job2:Ref_Number
                    Cycle
                End ! If f_RefNumber = job2:Ref_Number
            End ! If f_RefNumber > 0

            If job2:date_booked + def:bouncertime > f_datebooked And job2:date_booked <= f_datebooked
                tmp:count += 1
            End!If job2:date_booked + man:warranty_period < Today()
        end !loop
        access:jobs2_alias.restorefile(save_job2_id)
        setcursor()
    End!If access:jobs2_alias.clearkey(job2:RefNumberKey) = Level:Benign
    Return tmp:count


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CountBouncer',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:DateBooked',tmp:DateBooked,'CountBouncer',1)
    SolaceViewVars('tmp:count',tmp:count,'CountBouncer',1)
    SolaceViewVars('save_job2_id',save_job2_id,'CountBouncer',1)
    SolaceViewVars('tmp:IMEI',tmp:IMEI,'CountBouncer',1)
    SolaceViewVars('tmp:Manufacturer',tmp:Manufacturer,'CountBouncer',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
