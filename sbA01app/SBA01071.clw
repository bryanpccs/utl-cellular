

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01071.INC'),ONCE        !Local module procedure declarations
                     END


PricingStructureRepairType PROCEDURE  (func:AccountNumber,func:ModelNumber,func:UnitType,func:ChargeType,func:RepairType) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'PricingStructureRepairType')      !Add Procedure to Log
  end


!Is there a pricing structure setup?
    Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
    sta:Model_Number = func:ModelNumber
    sta:Charge_Type  = func:ChargeType
    sta:Unit_Type    = func:UnitType
    sta:Repair_Type  = func:RepairType
    If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
        !Found
        Return Level:Benign
    Else!If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            Access:TRACHRGE.ClearKey(trc:Account_Charge_Key)
            trc:Account_Number = tra:Account_Number
            trc:Model_Number   = func:ModelNumber
            trc:Charge_Type    = func:ChargeType
            trc:Unit_Type      = func:UnitType
            trc:Repair_Type    = func:RepairType
            If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                !Found
                Return Level:Benign
            Else!If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign

            If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                suc:Account_Number = func:AccountNumber
                suc:Model_Number   = func:ModelNumber
                suc:Charge_Type    = func:ChargeType
                suc:Unit_Type      = func:UnitType
                suc:Repair_Type    = func:RepairType
                If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                    !Found
                    Return Level:Benign
                Else!If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
            End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Fatal


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PricingStructureRepairType',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
