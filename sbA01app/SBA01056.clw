

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01056.INC'),ONCE        !Local module procedure declarations
                     END


InvoiceSubAccounts   PROCEDURE  (func:AccountNumber)  ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'InvoiceSubAccounts')      !Add Procedure to Log
  end


    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                Return Level:Fatal
            End !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'InvoiceSubAccounts',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
