

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01043.INC'),ONCE        !Local module procedure declarations
                     END


UpdateComputerNames PROCEDURE                         !Generated from procedure template - Window

ComputerName         STRING(20)
DefaultLocation      STRING(30)
LocationQueue        QUEUE,PRE(LCQ)
Location             STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Update Computer location'),AT(,,211,79),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,204,44),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Computer Name'),AT(12,12),USE(?Prompt1),FONT(,,,FONT:bold)
                           ENTRY(@s20),AT(76,12,124,10),USE(ComputerName),UPR
                           PROMPT('Default Location'),AT(12,28),USE(?Prompt2),FONT(,,,FONT:bold)
                           LIST,AT(76,28,124,10),USE(?List1),VSCROLL,FORMAT('120L(2)|M@s30@'),DROP(7),FROM(LocationQueue)
                         END
                       END
                       PANEL,AT(4,52,204,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(84,56,56,16),USE(?OkButton),LEFT,ICON('ok.ico')
                       BUTTON('&Cancel'),AT(144,56,56,16),USE(?CancelButton),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?ComputerName{prop:ReadOnly} = True
        ?ComputerName{prop:FontColor} = 65793
        ?ComputerName{prop:Color} = 15066597
    Elsif ?ComputerName{prop:Req} = True
        ?ComputerName{prop:FontColor} = 65793
        ?ComputerName{prop:Color} = 8454143
    Else ! If ?ComputerName{prop:Req} = True
        ?ComputerName{prop:FontColor} = 65793
        ?ComputerName{prop:Color} = 16777215
    End ! If ?ComputerName{prop:Req} = True
    ?ComputerName{prop:Trn} = 0
    ?ComputerName{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateComputerNames')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  GLO:Select22 = 'CANCEL'
  ComputerName = GLO:Select20
  DefaultLocation = GLO:Select21
  
  FREE(LocationQueue)
  access:LOCATION.clearkey(loc:ActiveLocationKey)
  loc:active = 1
  SET(loc:ActiveLocationKey, loc:ActiveLocationKey)
  LOOP
      IF ((access:LOCATION.next() <> Level:Benign) OR (loc:active <> 1)) THEN
          BREAK
      END
  
      CLEAR(LocationQueue)
      LCQ:Location = loc:location
      ADD(LocationQueue)
  END
  
  SORT(LocationQueue, LCQ:Location)
  
  OPEN(window)
  SELF.Opened=True
  IF (RECORDS(LocationQueue) > 0) THEN
      CLEAR(LocationQueue)
      LCQ:Location = DefaultLocation
      p# = POSITION(LocationQueue)
      IF (ERRORCODE()) THEN
          p# = 1
      END
      SELECT(?List1, p#)
      GET(LocationQueue, CHOICE(?List1))
      DefaultLocation = LCQ:Location
  ELSE
      DefaultLocation = ''
  END
  DISPLAY()
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  ! support for CPCS
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      GLO:Select20 = UPPER(ComputerName)
      GLO:Select21 = UPPER(DefaultLocation)
      GLO:Select22 = 'OK'
      POST(EVENT:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      GLO:Select22 = 'CANCEL'
      POST(EVENT:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1, NewSelection)
      GET(LocationQueue, CHOICE(?List1))
      DefaultLocation = LCQ:Location
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

