

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01021.INC'),ONCE        !Local module procedure declarations
                     END


UpdateESNMODEL PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?esn:Model_Number
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
History::esn:Record  LIKE(esn:RECORD),STATIC
QuickWindow          WINDOW('Update the ESNMODEL File'),AT(,,220,84),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('UpdateESNMODEL'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,48),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('I.M.E.I. Number'),AT(8,20),USE(?ESN:ESN:Prompt),TRN
                           ENTRY(@s8),AT(84,20,64,10),USE(esn:ESN),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           COMBO(@s30),AT(84,36,124,10),USE(esn:Model_Number),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Model Number'),AT(8,36),USE(?esn:model_number:prompt)
                         END
                       END
                       BUTTON('&OK'),AT(100,60,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,60,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       PANEL,AT(4,56,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?ESN:ESN:Prompt{prop:FontColor} = -1
    ?ESN:ESN:Prompt{prop:Color} = 15066597
    If ?esn:ESN{prop:ReadOnly} = True
        ?esn:ESN{prop:FontColor} = 65793
        ?esn:ESN{prop:Color} = 15066597
    Elsif ?esn:ESN{prop:Req} = True
        ?esn:ESN{prop:FontColor} = 65793
        ?esn:ESN{prop:Color} = 8454143
    Else ! If ?esn:ESN{prop:Req} = True
        ?esn:ESN{prop:FontColor} = 65793
        ?esn:ESN{prop:Color} = 16777215
    End ! If ?esn:ESN{prop:Req} = True
    ?esn:ESN{prop:Trn} = 0
    ?esn:ESN{prop:FontStyle} = font:Bold
    If ?esn:Model_Number{prop:ReadOnly} = True
        ?esn:Model_Number{prop:FontColor} = 65793
        ?esn:Model_Number{prop:Color} = 15066597
    Elsif ?esn:Model_Number{prop:Req} = True
        ?esn:Model_Number{prop:FontColor} = 65793
        ?esn:Model_Number{prop:Color} = 8454143
    Else ! If ?esn:Model_Number{prop:Req} = True
        ?esn:Model_Number{prop:FontColor} = 65793
        ?esn:Model_Number{prop:Color} = 16777215
    End ! If ?esn:Model_Number{prop:Req} = True
    ?esn:Model_Number{prop:Trn} = 0
    ?esn:Model_Number{prop:FontStyle} = font:Bold
    ?esn:model_number:prompt{prop:FontColor} = -1
    ?esn:model_number:prompt{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Model Number'
  OF ChangeRecord
    ActionMessage = 'Changing A Model Number'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateESNMODEL')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ESN:ESN:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(esn:Record,History::esn:Record)
  SELF.AddHistoryField(?esn:ESN,2)
  SELF.AddHistoryField(?esn:Model_Number,3)
  SELF.AddUpdateFile(Access:ESNMODEL)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ESNMODEL.Open
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ESNMODEL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(esn:Model_Number,?esn:Model_Number,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(mod:Model_Number_Key)
  FDCB6.AddField(mod:Model_Number,FDCB6.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESNMODEL.Close
    Relate:MODELNUM.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

