

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01001.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSTANTEXT PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
text_types_queue     QUEUE,PRE(TTQ)
text_types           STRING(30)
                     END
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::stt:Record  LIKE(stt:RECORD),STATIC
QuickWindow          WINDOW('Update the STANTEXT File'),AT(,,456,191),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('UpdateSTANTEXT'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,448,156),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           LIST,AT(84,20,124,10),USE(stt:Description),VSCROLL,LEFT(2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('20L(2)'),DROP(10,124),FROM(text_types_queue)
                           PROMPT('Alternative Tel No'),AT(8,36),USE(?STT:TelephoneNumber:Prompt)
                           ENTRY(@s30),AT(84,36,64,10),USE(stt:TelephoneNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Alternative Fax No'),AT(8,52),USE(?STT:FaxNumber:Prompt)
                           ENTRY(@s30),AT(84,52,64,10),USE(stt:FaxNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Standard Text Type'),AT(8,20),USE(?Prompt2)
                           PROMPT('Standard Text'),AT(8,68),USE(?STT:Text:Prompt),TRN
                           TEXT,AT(84,68,360,88),USE(stt:Text),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON('&OK'),AT(336,168,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(392,168,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       PANEL,AT(4,164,448,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?stt:Description{prop:FontColor} = 65793
    ?stt:Description{prop:Color}= 16777215
    ?stt:Description{prop:Color,2} = 16777215
    ?stt:Description{prop:Color,3} = 12937777
    ?STT:TelephoneNumber:Prompt{prop:FontColor} = -1
    ?STT:TelephoneNumber:Prompt{prop:Color} = 15066597
    If ?stt:TelephoneNumber{prop:ReadOnly} = True
        ?stt:TelephoneNumber{prop:FontColor} = 65793
        ?stt:TelephoneNumber{prop:Color} = 15066597
    Elsif ?stt:TelephoneNumber{prop:Req} = True
        ?stt:TelephoneNumber{prop:FontColor} = 65793
        ?stt:TelephoneNumber{prop:Color} = 8454143
    Else ! If ?stt:TelephoneNumber{prop:Req} = True
        ?stt:TelephoneNumber{prop:FontColor} = 65793
        ?stt:TelephoneNumber{prop:Color} = 16777215
    End ! If ?stt:TelephoneNumber{prop:Req} = True
    ?stt:TelephoneNumber{prop:Trn} = 0
    ?stt:TelephoneNumber{prop:FontStyle} = font:Bold
    ?STT:FaxNumber:Prompt{prop:FontColor} = -1
    ?STT:FaxNumber:Prompt{prop:Color} = 15066597
    If ?stt:FaxNumber{prop:ReadOnly} = True
        ?stt:FaxNumber{prop:FontColor} = 65793
        ?stt:FaxNumber{prop:Color} = 15066597
    Elsif ?stt:FaxNumber{prop:Req} = True
        ?stt:FaxNumber{prop:FontColor} = 65793
        ?stt:FaxNumber{prop:Color} = 8454143
    Else ! If ?stt:FaxNumber{prop:Req} = True
        ?stt:FaxNumber{prop:FontColor} = 65793
        ?stt:FaxNumber{prop:Color} = 16777215
    End ! If ?stt:FaxNumber{prop:Req} = True
    ?stt:FaxNumber{prop:Trn} = 0
    ?stt:FaxNumber{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?STT:Text:Prompt{prop:FontColor} = -1
    ?STT:Text:Prompt{prop:Color} = 15066597
    If ?stt:Text{prop:ReadOnly} = True
        ?stt:Text{prop:FontColor} = 65793
        ?stt:Text{prop:Color} = 15066597
    Elsif ?stt:Text{prop:Req} = True
        ?stt:Text{prop:FontColor} = 65793
        ?stt:Text{prop:Color} = 8454143
    Else ! If ?stt:Text{prop:Req} = True
        ?stt:Text{prop:FontColor} = 65793
        ?stt:Text{prop:Color} = 16777215
    End ! If ?stt:Text{prop:Req} = True
    ?stt:Text{prop:Trn} = 0
    ?stt:Text{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Standard Text'
  OF ChangeRecord
    ActionMessage = 'Changing A Standard Text'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSTANTEXT')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?stt:Description
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Free(text_types_queue)
  Clear(text_types_queue)
  TTQ:text_types = '3RD PARTY COURIER DIRECT NOTE'
  ADD(text_types_queue)
  TTQ:text_types = '3RD PARTY EXCEPTIONS'
  ADD(text_types_queue)
  TTQ:text_types = '3RD PARTY FAILED RETURNS'
  ADD(text_types_queue)
  TTQ:text_types = '3RD PARTY RETURNS NOTE'
  ADD(text_types_queue)
  TTQ:text_types = 'CONSIGNMENT NOTE - 3RD PARTY'
  ADD(text_types_queue)
  TTQ:text_types = 'COURIER COLLECTION REPORT'
  ADD(text_types_queue)
  TTQ:text_types = 'CREDIT NOTE'
  ADD(text_types_queue)
  TTQ:text_types = 'DESPATCH NOTE'
  ADD(text_types_queue)
  TTQ:text_types = 'DESPATCH NOTE - RETAIL'
  ADD(text_types_queue)
  TTQ:text_types = 'DESPATCH NOTE - BATCH'
  ADD(text_types_queue)
  TTQ:text_types = 'DESPATCH NOTE - THIRD PARTY'
  ADD(text_types_queue)
  TTQ:text_types = 'ESTIMATE'
  ADD(text_types_queue)
  TTQ:text_types = 'EXCHANGE CLAIMS REPORT'
  ADD(text_types_queue)
  TTQ:text_types = 'EXCHANGE EXCEPTIONS REPORT'
  ADD(text_types_queue)
  TTQ:text_types = 'INVOICE'
  ADD(text_types_queue)
  TTQ:text_types = 'INVOICE - RETAIL'
  ADD(text_types_queue)
  TTQ:text_types = 'INVOICE - PROFORMA'
  ADD(text_types_queue)
  TTQ:text_types = 'JOB CARD'
  ADD(text_types_queue)
  TTQ:text_types = 'JOB RECEIPT'
  ADD(text_types_queue)
  TTQ:text_types = 'PARTS ORDER'
  ADD(text_types_queue)
  TTQ:text_types = 'PICKING NOTE - RETAIL'
  ADD(text_types_queue)
  TTQ:text_types = 'RETURNS NOTE - 3RD PARTY'
  ADD(text_types_queue)
  TTQ:text_types = 'SHORTAGES - 3RD PARTY'
  ADD(text_types_queue)
  TTQ:text_types = 'SPECIAL DELIVERY REPORT'
  ADD(text_types_queue)
  ! Start Change 3778 BE(25/02/04)
  TTQ:text_types = 'VODAFONE DESPATCH NOTE 1'
  ADD(text_types_queue)
  TTQ:text_types = 'VODAFONE DESPATCH NOTE 2'
  ADD(text_types_queue)
  ! End Change 3778 BE(25/02/04)
  
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(stt:Record,History::stt:Record)
  SELF.AddHistoryField(?stt:Description,1)
  SELF.AddHistoryField(?stt:TelephoneNumber,2)
  SELF.AddHistoryField(?stt:FaxNumber,3)
  SELF.AddHistoryField(?stt:Text,5)
  SELF.AddUpdateFile(Access:STANTEXT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STANTEXT.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STANTEXT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?stt:Description{prop:vcr} = TRUE
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STANTEXT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If stt:description = ''
      Select(?stt:description)
      Cycle
  End
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

