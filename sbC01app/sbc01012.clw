

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01012.INC'),ONCE        !Local module procedure declarations
                     END


Update_Repair_Type PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::rep:Record  LIKE(rep:RECORD),STATIC
QuickWindow          WINDOW('Update the REPAIRTY File'),AT(,,220,99),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('Browse_Repair_Type'),TILED,SYSTEM,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,64),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(8,20),USE(?REP:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(rep:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(8,36),USE(?REP:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(rep:Model_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Repair Type'),AT(8,52),USE(?REP:Repair_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(rep:Repair_Type),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('&OK'),AT(100,76,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,76,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,72,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?REP:Manufacturer:Prompt{prop:FontColor} = -1
    ?REP:Manufacturer:Prompt{prop:Color} = 15066597
    If ?rep:Manufacturer{prop:ReadOnly} = True
        ?rep:Manufacturer{prop:FontColor} = 65793
        ?rep:Manufacturer{prop:Color} = 15066597
    Elsif ?rep:Manufacturer{prop:Req} = True
        ?rep:Manufacturer{prop:FontColor} = 65793
        ?rep:Manufacturer{prop:Color} = 8454143
    Else ! If ?rep:Manufacturer{prop:Req} = True
        ?rep:Manufacturer{prop:FontColor} = 65793
        ?rep:Manufacturer{prop:Color} = 16777215
    End ! If ?rep:Manufacturer{prop:Req} = True
    ?rep:Manufacturer{prop:Trn} = 0
    ?rep:Manufacturer{prop:FontStyle} = font:Bold
    ?REP:Model_Number:Prompt{prop:FontColor} = -1
    ?REP:Model_Number:Prompt{prop:Color} = 15066597
    If ?rep:Model_Number{prop:ReadOnly} = True
        ?rep:Model_Number{prop:FontColor} = 65793
        ?rep:Model_Number{prop:Color} = 15066597
    Elsif ?rep:Model_Number{prop:Req} = True
        ?rep:Model_Number{prop:FontColor} = 65793
        ?rep:Model_Number{prop:Color} = 8454143
    Else ! If ?rep:Model_Number{prop:Req} = True
        ?rep:Model_Number{prop:FontColor} = 65793
        ?rep:Model_Number{prop:Color} = 16777215
    End ! If ?rep:Model_Number{prop:Req} = True
    ?rep:Model_Number{prop:Trn} = 0
    ?rep:Model_Number{prop:FontStyle} = font:Bold
    ?REP:Repair_Type:Prompt{prop:FontColor} = -1
    ?REP:Repair_Type:Prompt{prop:Color} = 15066597
    If ?rep:Repair_Type{prop:ReadOnly} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 15066597
    Elsif ?rep:Repair_Type{prop:Req} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 8454143
    Else ! If ?rep:Repair_Type{prop:Req} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 16777215
    End ! If ?rep:Repair_Type{prop:Req} = True
    ?rep:Repair_Type{prop:Trn} = 0
    ?rep:Repair_Type{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Repair Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Repair Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Repair_Type')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REP:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(rep:Record,History::rep:Record)
  SELF.AddHistoryField(?rep:Manufacturer,1)
  SELF.AddHistoryField(?rep:Model_Number,2)
  SELF.AddHistoryField(?rep:Repair_Type,3)
  SELF.AddUpdateFile(Access:REPAIRTY)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:REPAIRTY.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:REPAIRTY
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  IF SELF.Request = ViewRecord
    ?rep:Manufacturer{PROP:ReadOnly} = True
    ?rep:Model_Number{PROP:ReadOnly} = True
    DISABLE(?rep:Repair_Type)
    HIDE(?OK)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPAIRTY.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    rep:Manufacturer = glo:select13
    rep:Model_Number = glo:select14
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

