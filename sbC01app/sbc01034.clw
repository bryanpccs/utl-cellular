

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01034.INC'),ONCE        !Local module procedure declarations
                     END


PrintingDefaults PROCEDURE                            !Generated from procedure template - Window

ThisThreadActive BYTE
LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
SDS_Thermal          STRING(3)
tmp:ThirdPartyDetails BYTE(0)
tmp:ThirdPartyJobLabel BYTE(0)
tmp:ExchangeRepairLabel BYTE(0)
tmp:InWorkshopDate   BYTE(0)
tmp:SuspendPrintJobcard BYTE(0)
tmp:HideIMEIBarcode  BYTE
tmp:NoDespatchNote4IncompleteJob BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW8::View:Browse    VIEW(DEFPRINT)
                       PROJECT(dep:Printer_Name)
                       PROJECT(dep:Printer_Path)
                       PROJECT(dep:Copies)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
dep:Printer_Name       LIKE(dep:Printer_Name)         !List box control field - type derived from field
dep:Printer_Path       LIKE(dep:Printer_Path)         !List box control field - type derived from field
dep:Copies             LIKE(dep:Copies)               !List box control field - type derived from field
tmp:Background         LIKE(tmp:Background)           !List box control field - type derived from local data
tmp:Background_Icon    LONG                           !Entry's icon ID
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('General Defaults'),AT(,,467,329),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),TILED,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,460,296),USE(?Sheet1),SPREAD
                         TAB('Printer Defaults'),USE(?printerPreferences)
                           PROMPT('Default Printers'),AT(12,20),USE(?Prompt2)
                           LIST,AT(12,32,388,92),USE(?List:2),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('127L(2)|M~Report Name~@s60@161L(2)|M~Printer Name~@s255@28R(2)|M~Copies~@n3@11L(' &|
   '2)J~Background~@s1@'),FROM(Queue:Browse:1)
                           BUTTON('&Insert'),AT(404,32,56,16),USE(?Insert:2),TRN,LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(404,68,56,16),USE(?Change:2),TRN,LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(404,108,56,16),USE(?Delete:2),TRN,LEFT,ICON('delete.gif')
                           GROUP('Label Printing Defaults'),AT(240,128,220,168),USE(?LabelPrintingDefaults),BOXED
                             PROMPT('Label Printer Type'),AT(248,140),USE(?Prompt44),TRN
                             LIST,AT(324,140,124,10),USE(def:Label_Printer_Type),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(label_queue_temp)
                             CHECK('Print Job Label'),AT(324,156),USE(def:Use_Job_Label),VALUE('YES','NO')
                             CHECK('Use Small Label'),AT(392,156),USE(def:UseSmallLabel),MSG('Use Small Label'),TIP('Use Small Label'),VALUE('1','0')
                             CHECK('Print Job Part Label'),AT(324,168),USE(def:add_stock_label),VALUE('YES','NO')
                             CHECK('Print Loan/Exchange Label'),AT(324,180),USE(def:Use_Loan_Exchange_Label),VALUE('YES','NO')
                             CHECK('Print Exchange Repair Label'),AT(324,192),USE(tmp:ExchangeRepairLabel),MSG('Print Exchange Repair Label'),TIP('Print Exchange Repair Label'),VALUE('1','0')
                             CHECK('Print Stock Label'),AT(324,204),USE(def:receive_stock_label),VALUE('YES','NO')
                             CHECK('Print QA Failed Label'),AT(324,216),USE(def:QA_Failed_Label),VALUE('YES','NO')
                             CHECK('Print QA Pass Label'),AT(324,228),USE(def:QAPassLabel),MSG('Print QA Pass Label'),TIP('Print QA Pass Label'),VALUE('1','0')
                             CHECK('Print Retained Accessory Label'),AT(324,240),USE(de2:PrintRetainedAccLabel),MSG('Print Retained Accessory Label'),TIP('Print Retained Accessory Label'),VALUE('1','0')
                             CHECK('Print Thermal SDS Labels'),AT(324,252),USE(SDS_Thermal),VALUE('YES','NO')
                             CHECK('Print 3rd Party Returns Label'),AT(324,264),USE(tmp:ThirdPartyJobLabel),MSG('Print'),TIP('Print'),VALUE('1','0')
                             CHECK('Job Label - In Workshop Date'),AT(324,276),USE(tmp:InWorkshopDate)
                           END
                           GROUP('General Printing Defaults'),AT(12,128,220,168),USE(?Group2),BOXED
                             CHECK('Remove Report Background From All Reports'),AT(20,140),USE(def:Remove_Backgrounds),VALUE('YES','NO')
                             CHECK('Print I.M.E.I. Number Barcodes On Reports'),AT(20,152),USE(def:PrintBarcode),MSG('Print Barcode'),TIP('Print Barcode'),VALUE('1','0')
                             CHECK('Print Third Party Despatch Note'),AT(20,176),USE(def:ThirdPartyNote),RIGHT,MSG('Print Single 3rd Party Despatch Note'),TIP('Print Single 3rd Party Despatch Note'),VALUE('YES','NO')
                             CHECK('Print QA Failed Report'),AT(20,188),USE(def:QA_Failed_Report),VALUE('YES','NO')
                             CHECK('Print Accessories On Job Label '),AT(20,200),USE(def:Job_Label_Accessories),VALUE('YES','NO')
                             CHECK('Show Euro Symbol on Financial Paperwork'),AT(20,212),USE(de2:CurrencySymbol),VALUE('YES','NO'),MSG('Currency Symbol')
                             CHECK('Hide 3rd Party Consignment Note Details'),AT(20,224),USE(tmp:ThirdPartyDetails),MSG('Hide Third Party Consignment Details'),TIP('Hide Third Party Consignment Details'),VALUE('1','0')
                             CHECK('Suspend Printing Job Card until In Workshop'),AT(20,236),USE(tmp:SuspendPrintJobcard)
                             CHECK('No Despatch Note For Incomplete Jobs'),AT(20,248),USE(tmp:NoDespatchNote4IncompleteJob)
                             CHECK('Hide IMEI Barcode On Job label'),AT(20,164),USE(tmp:HideIMEIBarcode)
                           END
                         END
                       END
                       BUTTON('&OK'),AT(348,308,56,16),USE(?OkButton),TRN,LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(404,308,56,16),USE(?CancelButton),TRN,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,304,460,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?printerPreferences{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?LabelPrintingDefaults{prop:Font,3} = -1
    ?LabelPrintingDefaults{prop:Color} = 15066597
    ?LabelPrintingDefaults{prop:Trn} = 0
    ?Prompt44{prop:FontColor} = -1
    ?Prompt44{prop:Color} = 15066597
    ?def:Label_Printer_Type{prop:FontColor} = 65793
    ?def:Label_Printer_Type{prop:Color}= 16777215
    ?def:Label_Printer_Type{prop:Color,2} = 16777215
    ?def:Label_Printer_Type{prop:Color,3} = 12937777
    ?def:Use_Job_Label{prop:Font,3} = -1
    ?def:Use_Job_Label{prop:Color} = 15066597
    ?def:Use_Job_Label{prop:Trn} = 0
    ?def:UseSmallLabel{prop:Font,3} = -1
    ?def:UseSmallLabel{prop:Color} = 15066597
    ?def:UseSmallLabel{prop:Trn} = 0
    ?def:add_stock_label{prop:Font,3} = -1
    ?def:add_stock_label{prop:Color} = 15066597
    ?def:add_stock_label{prop:Trn} = 0
    ?def:Use_Loan_Exchange_Label{prop:Font,3} = -1
    ?def:Use_Loan_Exchange_Label{prop:Color} = 15066597
    ?def:Use_Loan_Exchange_Label{prop:Trn} = 0
    ?tmp:ExchangeRepairLabel{prop:Font,3} = -1
    ?tmp:ExchangeRepairLabel{prop:Color} = 15066597
    ?tmp:ExchangeRepairLabel{prop:Trn} = 0
    ?def:receive_stock_label{prop:Font,3} = -1
    ?def:receive_stock_label{prop:Color} = 15066597
    ?def:receive_stock_label{prop:Trn} = 0
    ?def:QA_Failed_Label{prop:Font,3} = -1
    ?def:QA_Failed_Label{prop:Color} = 15066597
    ?def:QA_Failed_Label{prop:Trn} = 0
    ?def:QAPassLabel{prop:Font,3} = -1
    ?def:QAPassLabel{prop:Color} = 15066597
    ?def:QAPassLabel{prop:Trn} = 0
    ?de2:PrintRetainedAccLabel{prop:Font,3} = -1
    ?de2:PrintRetainedAccLabel{prop:Color} = 15066597
    ?de2:PrintRetainedAccLabel{prop:Trn} = 0
    ?SDS_Thermal{prop:Font,3} = -1
    ?SDS_Thermal{prop:Color} = 15066597
    ?SDS_Thermal{prop:Trn} = 0
    ?tmp:ThirdPartyJobLabel{prop:Font,3} = -1
    ?tmp:ThirdPartyJobLabel{prop:Color} = 15066597
    ?tmp:ThirdPartyJobLabel{prop:Trn} = 0
    ?tmp:InWorkshopDate{prop:Font,3} = -1
    ?tmp:InWorkshopDate{prop:Color} = 15066597
    ?tmp:InWorkshopDate{prop:Trn} = 0
    ?Group2{prop:Font,3} = -1
    ?Group2{prop:Color} = 15066597
    ?Group2{prop:Trn} = 0
    ?def:Remove_Backgrounds{prop:Font,3} = -1
    ?def:Remove_Backgrounds{prop:Color} = 15066597
    ?def:Remove_Backgrounds{prop:Trn} = 0
    ?def:PrintBarcode{prop:Font,3} = -1
    ?def:PrintBarcode{prop:Color} = 15066597
    ?def:PrintBarcode{prop:Trn} = 0
    ?def:ThirdPartyNote{prop:Font,3} = -1
    ?def:ThirdPartyNote{prop:Color} = 15066597
    ?def:ThirdPartyNote{prop:Trn} = 0
    ?def:QA_Failed_Report{prop:Font,3} = -1
    ?def:QA_Failed_Report{prop:Color} = 15066597
    ?def:QA_Failed_Report{prop:Trn} = 0
    ?def:Job_Label_Accessories{prop:Font,3} = -1
    ?def:Job_Label_Accessories{prop:Color} = 15066597
    ?def:Job_Label_Accessories{prop:Trn} = 0
    ?de2:CurrencySymbol{prop:Font,3} = -1
    ?de2:CurrencySymbol{prop:Color} = 15066597
    ?de2:CurrencySymbol{prop:Trn} = 0
    ?tmp:ThirdPartyDetails{prop:Font,3} = -1
    ?tmp:ThirdPartyDetails{prop:Color} = 15066597
    ?tmp:ThirdPartyDetails{prop:Trn} = 0
    ?tmp:SuspendPrintJobcard{prop:Font,3} = -1
    ?tmp:SuspendPrintJobcard{prop:Color} = 15066597
    ?tmp:SuspendPrintJobcard{prop:Trn} = 0
    ?tmp:NoDespatchNote4IncompleteJob{prop:Font,3} = -1
    ?tmp:NoDespatchNote4IncompleteJob{prop:Color} = 15066597
    ?tmp:NoDespatchNote4IncompleteJob{prop:Trn} = 0
    ?tmp:HideIMEIBarcode{prop:Font,3} = -1
    ?tmp:HideIMEIBarcode{prop:Color} = 15066597
    ?tmp:HideIMEIBarcode{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('PrintingDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='PrintingDefaults'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  Clear(label_queue_temp)
  Free(label_queue_temp)
  label_printer_temp = 'TEC B-440 / B-442'
  Add(label_queue_temp)
  label_printer_temp = 'TEC B-452'
  Add(label_queue_Temp)
  BRW8.Init(?List:2,Queue:Browse:1.ViewPosition,BRW8::View:Browse,Queue:Browse:1,Relate:DEFPRINT,SELF)
  OPEN(window)
  SELF.Opened=True
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  Set(DEFAULT2)
  If Access:DEFAULT2.Next()
      If Access:DEFAULT2.PrimeRecord() = Level:Benign
  
          If Access:DEFAULT2.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:DEFAULT2.TryInsert() = Level:Benign
              !Insert Failed
          End !If Access:DEFAULT2.TryInsert() = Level:Benign
      End !If Access:DEFAULT2.PrimeRecord() = Level:Benign
  End !Access:DEFAULT2.Next()
  SDS_Thermal = GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ThirdPartyJobLabel = GETINI('PRINTING','ThirdPartyReturnsLabel',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ExchangeRepairLabel = GETINI('PRINTING','ExchangeRepairLabel',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ThirdPartyDetails = GETINI('HIDE','ThirdPartyDetails',,CLIP(PATH())&'\SB2KDEF.INI')
  ! Start Change 2648 BE(22/05/2003)
  tmp:InWorkshopDate = GETINI('PRINTING','InWorkshopDate',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2648 BE(22/05/2003)
  ! Start Change 2851 BE(02/07/2003)
  tmp:SuspendPrintJobcard = GETINI('PRINTING','SuspendPrintingJobCard',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2851 BE(02/07/2003)
  ! Start Change 3701 BE(15/12/2003)
  tmp:HideIMEIBarcode = GETINI('PRINTING','HideIMEIBarcode',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 3701 BE(15/12/2003)
  ! Start Change 3779 BE(29/01/04)
  tmp:NoDespatchNote4IncompleteJob = GETINI('PRINTING','NoDespatchNote4IncompleteJob',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 3779 BE(29/01/04)
  
  Do RecolourWindow
  ?List:2{prop:vcr} = TRUE
  ?def:Label_Printer_Type{prop:vcr} = TRUE
  ?def:Label_Printer_Type{prop:vcr} = False
  ! support for CPCS
  BRW8.Q &= Queue:Browse:1
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,)
  BIND('tmp:Background',tmp:Background)
  ?List:2{PROP:IconList,1} = '~bluetick.ico'
  BRW8.AddField(dep:Printer_Name,BRW8.Q.dep:Printer_Name)
  BRW8.AddField(dep:Printer_Path,BRW8.Q.dep:Printer_Path)
  BRW8.AddField(dep:Copies,BRW8.Q.dep:Copies)
  BRW8.AddField(tmp:Background,BRW8.Q.tmp:Background)
  IF ?def:Use_Job_Label{Prop:Checked} = True
    UNHIDE(?def:UseSmallLabel)
  END
  IF ?def:Use_Job_Label{Prop:Checked} = False
    HIDE(?def:UseSmallLabel)
  END
  IF ?def:Use_Loan_Exchange_Label{Prop:Checked} = True
    ENABLE(?tmp:ExchangeRepairLabel)
  END
  IF ?def:Use_Loan_Exchange_Label{Prop:Checked} = False
    DISABLE(?tmp:ExchangeRepairLabel)
  END
  BRW8.AskProcedure = 1
  BRW8.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:DEFSTOCK.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='PrintingDefaults'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Printer_Defaults
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?def:Use_Job_Label
      IF ?def:Use_Job_Label{Prop:Checked} = True
        UNHIDE(?def:UseSmallLabel)
      END
      IF ?def:Use_Job_Label{Prop:Checked} = False
        HIDE(?def:UseSmallLabel)
      END
      ThisWindow.Reset
    OF ?def:Use_Loan_Exchange_Label
      IF ?def:Use_Loan_Exchange_Label{Prop:Checked} = True
        ENABLE(?tmp:ExchangeRepairLabel)
      END
      IF ?def:Use_Loan_Exchange_Label{Prop:Checked} = False
        DISABLE(?tmp:ExchangeRepairLabel)
      END
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defaults.update()
      Access:DEFAULT2.Update()
      PUTINI('PRINTING','SDS',SDS_Thermal,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('HIDE','ThirdPartyDetails',tmp:ThirdPartyDetails,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('PRINTING','ThirdPartyReturnsLabel',tmp:ThirdPartyJobLabel,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('PRINTING','ExchangeRepairLabel',tmp:ExchangeRepairLabel,CLIP(PATH()) & '\SB2KDEF.INI')
      ! Start Change 2648 BE(22/05/2003)
      PUTINI('PRINTING','InWorkshopDate',tmp:InWorkshopDate,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 2648 BE(22/05/2003)
      ! Start Change 2851 BE(02/07/2003)
      PUTINI('PRINTING','SuspendPrintingJobCard',tmp:SuspendPrintJobcard,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 2851 BE(02/07/2003)
      ! Start Change 3701 BE(15/12/2003)
      PUTINI('PRINTING','HideIMEIBarcode',tmp:HideIMEIBarcode,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 3701 BE(15/12/2003)
      ! Start Change 3779 BE(29/01/04)
      PUTINI('PRINTING','NoDespatchNote4IncompleteJob',tmp:NoDespatchNote4IncompleteJob,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 3779 BE(29/01/04)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      CONVERTOEMTOANSI(de2:CurrencySymbol)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW8.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (dep:background <> 'YES')
    SELF.Q.tmp:Background_Icon = 1
  ELSE
    SELF.Q.tmp:Background_Icon = 0
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

