

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01014.INC'),ONCE        !Local module procedure declarations
                     END


Update_Trade_Account_Charges PROCEDURE                !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?trc:Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?trc:Unit_Type
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?trc:Repair_Type
rep:Repair_Type        LIKE(rep:Repair_Type)          !List box control field - type derived from field
rep:Manufacturer       LIKE(rep:Manufacturer)         !Primary key field - type derived from field
rep:Model_Number       LIKE(rep:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB6::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
FDCB7::View:FileDropCombo VIEW(REPAIRTY)
                       PROJECT(rep:Repair_Type)
                       PROJECT(rep:Manufacturer)
                       PROJECT(rep:Model_Number)
                     END
History::trc:Record  LIKE(trc:RECORD),STATIC
QuickWindow          WINDOW('Update the TRACHRGE File'),AT(,,220,152),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Update_Trade_Account_Charges'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,116),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Trade Account Number'),AT(8,20),USE(?TRC:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,20,124,10),USE(trc:Account_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Model Number'),AT(8,36),USE(?TRC:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(trc:Model_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           COMBO(@s30),AT(84,52,124,10),USE(trc:Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Unit Type'),AT(8,68),USE(?Prompt4)
                           COMBO(@s30),AT(84,68,124,10),USE(trc:Unit_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           PROMPT('Repair Type'),AT(7,85),USE(?Prompt5)
                           COMBO(@s30),AT(84,84,124,10),USE(trc:Repair_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:2)
                           PROMPT('Cost'),AT(8,100),USE(?TRC:Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,100,64,10),USE(trc:Cost),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Charge Type'),AT(8,52),USE(?Prompt3)
                         END
                       END
                       PANEL,AT(4,124,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,128,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,128,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?TRC:Account_Number:Prompt{prop:FontColor} = -1
    ?TRC:Account_Number:Prompt{prop:Color} = 15066597
    If ?trc:Account_Number{prop:ReadOnly} = True
        ?trc:Account_Number{prop:FontColor} = 65793
        ?trc:Account_Number{prop:Color} = 15066597
    Elsif ?trc:Account_Number{prop:Req} = True
        ?trc:Account_Number{prop:FontColor} = 65793
        ?trc:Account_Number{prop:Color} = 8454143
    Else ! If ?trc:Account_Number{prop:Req} = True
        ?trc:Account_Number{prop:FontColor} = 65793
        ?trc:Account_Number{prop:Color} = 16777215
    End ! If ?trc:Account_Number{prop:Req} = True
    ?trc:Account_Number{prop:Trn} = 0
    ?trc:Account_Number{prop:FontStyle} = font:Bold
    ?TRC:Model_Number:Prompt{prop:FontColor} = -1
    ?TRC:Model_Number:Prompt{prop:Color} = 15066597
    If ?trc:Model_Number{prop:ReadOnly} = True
        ?trc:Model_Number{prop:FontColor} = 65793
        ?trc:Model_Number{prop:Color} = 15066597
    Elsif ?trc:Model_Number{prop:Req} = True
        ?trc:Model_Number{prop:FontColor} = 65793
        ?trc:Model_Number{prop:Color} = 8454143
    Else ! If ?trc:Model_Number{prop:Req} = True
        ?trc:Model_Number{prop:FontColor} = 65793
        ?trc:Model_Number{prop:Color} = 16777215
    End ! If ?trc:Model_Number{prop:Req} = True
    ?trc:Model_Number{prop:Trn} = 0
    ?trc:Model_Number{prop:FontStyle} = font:Bold
    If ?trc:Charge_Type{prop:ReadOnly} = True
        ?trc:Charge_Type{prop:FontColor} = 65793
        ?trc:Charge_Type{prop:Color} = 15066597
    Elsif ?trc:Charge_Type{prop:Req} = True
        ?trc:Charge_Type{prop:FontColor} = 65793
        ?trc:Charge_Type{prop:Color} = 8454143
    Else ! If ?trc:Charge_Type{prop:Req} = True
        ?trc:Charge_Type{prop:FontColor} = 65793
        ?trc:Charge_Type{prop:Color} = 16777215
    End ! If ?trc:Charge_Type{prop:Req} = True
    ?trc:Charge_Type{prop:Trn} = 0
    ?trc:Charge_Type{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?trc:Unit_Type{prop:ReadOnly} = True
        ?trc:Unit_Type{prop:FontColor} = 65793
        ?trc:Unit_Type{prop:Color} = 15066597
    Elsif ?trc:Unit_Type{prop:Req} = True
        ?trc:Unit_Type{prop:FontColor} = 65793
        ?trc:Unit_Type{prop:Color} = 8454143
    Else ! If ?trc:Unit_Type{prop:Req} = True
        ?trc:Unit_Type{prop:FontColor} = 65793
        ?trc:Unit_Type{prop:Color} = 16777215
    End ! If ?trc:Unit_Type{prop:Req} = True
    ?trc:Unit_Type{prop:Trn} = 0
    ?trc:Unit_Type{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?trc:Repair_Type{prop:ReadOnly} = True
        ?trc:Repair_Type{prop:FontColor} = 65793
        ?trc:Repair_Type{prop:Color} = 15066597
    Elsif ?trc:Repair_Type{prop:Req} = True
        ?trc:Repair_Type{prop:FontColor} = 65793
        ?trc:Repair_Type{prop:Color} = 8454143
    Else ! If ?trc:Repair_Type{prop:Req} = True
        ?trc:Repair_Type{prop:FontColor} = 65793
        ?trc:Repair_Type{prop:Color} = 16777215
    End ! If ?trc:Repair_Type{prop:Req} = True
    ?trc:Repair_Type{prop:Trn} = 0
    ?trc:Repair_Type{prop:FontStyle} = font:Bold
    ?TRC:Cost:Prompt{prop:FontColor} = -1
    ?TRC:Cost:Prompt{prop:Color} = 15066597
    If ?trc:Cost{prop:ReadOnly} = True
        ?trc:Cost{prop:FontColor} = 65793
        ?trc:Cost{prop:Color} = 15066597
    Elsif ?trc:Cost{prop:Req} = True
        ?trc:Cost{prop:FontColor} = 65793
        ?trc:Cost{prop:Color} = 8454143
    Else ! If ?trc:Cost{prop:Req} = True
        ?trc:Cost{prop:FontColor} = 65793
        ?trc:Cost{prop:Color} = 16777215
    End ! If ?trc:Cost{prop:Req} = True
    ?trc:Cost{prop:Trn} = 0
    ?trc:Cost{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Trade Account Charge'
  OF ChangeRecord
    ActionMessage = 'Changing A Trade Account Charge'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Trade_Account_Charges')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRC:Account_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(trc:Record,History::trc:Record)
  SELF.AddHistoryField(?trc:Account_Number,1)
  SELF.AddHistoryField(?trc:Model_Number,4)
  SELF.AddHistoryField(?trc:Charge_Type,2)
  SELF.AddHistoryField(?trc:Unit_Type,3)
  SELF.AddHistoryField(?trc:Repair_Type,5)
  SELF.AddHistoryField(?trc:Cost,6)
  SELF.AddUpdateFile(Access:TRACHRGE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRACHRGE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB4.Init(trc:Charge_Type,?trc:Charge_Type,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder(cha:Charge_Type_Key)
  FDCB4.AddField(cha:Charge_Type,FDCB4.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB6.Init(trc:Unit_Type,?trc:Unit_Type,Queue:FileDropCombo:1.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo:1,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo:1
  FDCB6.AddSortOrder(uni:Unit_Type_Key)
  FDCB6.AddField(uni:Unit_Type,FDCB6.Q.uni:Unit_Type)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB7.Init(trc:Repair_Type,?trc:Repair_Type,Queue:FileDropCombo:2.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:2,Relate:REPAIRTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:2
  FDCB7.AddSortOrder(rep:Model_Number_Key)
  FDCB7.AddRange(rep:Model_Number,trc:Model_Number)
  FDCB7.AddField(rep:Repair_Type,FDCB7.Q.rep:Repair_Type)
  FDCB7.AddField(rep:Manufacturer,FDCB7.Q.rep:Manufacturer)
  FDCB7.AddField(rep:Model_Number,FDCB7.Q.rep:Model_Number)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

