

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01008.INC'),ONCE        !Local module procedure declarations
                     END


Clear_Address_Window PROCEDURE                        !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
address_option_temp  STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Clear / Replicate'),AT(,,155,139),FONT('Tahoma',8,,),CENTER,ICON('PC.ICO'),TILED,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,148,104),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           STRING('String 1'),AT(8,8,140,12),USE(?String1),TRN,FONT(,,COLOR:Navy,)
                           OPTION,AT(8,16,133,90),USE(address_option_temp)
                             RADIO('Clear Address'),AT(36,24),USE(?Option1),VALUE('1')
                             RADIO('Customer Address'),AT(36,52),USE(?Option2),VALUE('2')
                             RADIO('Collection Address'),AT(36,68),USE(?Option3),VALUE('3')
                             RADIO('Delivery Address'),AT(36,84),USE(?Option4),TRN,VALUE('4')
                           END
                           GROUP('Replicate Address From'),AT(12,44,133,62),USE(?Group1),BOXED
                           END
                         END
                       END
                       PANEL,AT(4,112,148,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(36,116,56,16),USE(?OkButton),LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(92,116,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?address_option_temp{prop:Font,3} = -1
    ?address_option_temp{prop:Color} = 15066597
    ?address_option_temp{prop:Trn} = 0
    ?Option1{prop:Font,3} = -1
    ?Option1{prop:Color} = 15066597
    ?Option1{prop:Trn} = 0
    ?Option2{prop:Font,3} = -1
    ?Option2{prop:Color} = 15066597
    ?Option2{prop:Trn} = 0
    ?Option3{prop:Font,3} = -1
    ?Option3{prop:Color} = 15066597
    ?Option3{prop:Trn} = 0
    ?Option4{prop:Font,3} = -1
    ?Option4{prop:Color} = 15066597
    ?Option4{prop:Trn} = 0
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Clear_Address_Window')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! Before Embed Point: %AfterWindowOpening) DESC(Legacy: After Opening the Window) ARG()
  Case glo:select1
      Of 'INVOICE'
          ?string1{prop:text} = 'Invoice Address'
          Disable(?Option2)
      Of 'COLLECTION'
          ?string1{prop:text} = 'Collection Address'
          Disable(?Option3)
      Of 'DELIVERY'
          ?string1{prop:text} = 'Delivery Address'
          Disable(?Option4)
  End
  Case glo:select3
      Of 'INVOICE'
          Disable(?Option2)
      Of 'COLLECTION'
          Disable(?Option3)
      Of 'DELIVERY'
          Disable(?Option4)
  End
  address_option_temp = glo:select2
  ! After Embed Point: %AfterWindowOpening) DESC(Legacy: After Opening the Window) ARG()
  ! support for CPCS
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlPreEventHandling) DESC(Legacy: Control Event Handling, before generated code) ARG(?OkButton, Accepted)
      glo:select1 = address_option_temp
      ! After Embed Point: %ControlPreEventHandling) DESC(Legacy: Control Event Handling, before generated code) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ! Before Embed Point: %ControlPreEventHandling) DESC(Legacy: Control Event Handling, before generated code) ARG(?CancelButton, Accepted)
      glo:select1 = 'CANCEL'
      ! After Embed Point: %ControlPreEventHandling) DESC(Legacy: Control Event Handling, before generated code) ARG(?CancelButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

