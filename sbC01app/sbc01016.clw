

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01016.INC'),ONCE        !Local module procedure declarations
                     END


Update_Sub_Account_Charges PROCEDURE                  !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?suc:Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Ref_Number         LIKE(cha:Ref_Number)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?suc:Unit_Type
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?suc:Repair_Type
rep:Repair_Type        LIKE(rep:Repair_Type)          !List box control field - type derived from field
rep:Manufacturer       LIKE(rep:Manufacturer)         !Primary key field - type derived from field
rep:Model_Number       LIKE(rep:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Ref_Number)
                     END
FDCB6::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
FDCB7::View:FileDropCombo VIEW(REPAIRTY)
                       PROJECT(rep:Repair_Type)
                       PROJECT(rep:Manufacturer)
                       PROJECT(rep:Model_Number)
                     END
History::suc:Record  LIKE(suc:RECORD),STATIC
QuickWindow          WINDOW('Update the TRACHRGE File'),AT(,,220,152),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Update_Trade_Account_Charges'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,116),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Trade Account Number'),AT(8,20),USE(?TRC:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,20,124,10),USE(suc:Account_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(8,36),USE(?TRC:Model_Number:Prompt),TRN
                           PROMPT('Charge Type'),AT(8,52),USE(?Prompt3)
                           COMBO(@s30),AT(84,52,124,10),USE(suc:Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('114L(2)|M@s30@12L(2)@n3@'),DROP(10,144),FROM(Queue:FileDropCombo)
                           ENTRY(@s30),AT(84,36,124,10),USE(suc:Model_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Unit Type'),AT(8,68),USE(?Prompt4)
                           COMBO(@s30),AT(84,68,124,10),USE(suc:Unit_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           PROMPT('Repair Type'),AT(7,85),USE(?Prompt5)
                           COMBO(@s30),AT(84,84,124,10),USE(suc:Repair_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:2)
                           PROMPT('Cost'),AT(8,100),USE(?TRC:Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,100,64,10),USE(suc:Cost),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       PANEL,AT(4,124,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,128,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,128,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?TRC:Account_Number:Prompt{prop:FontColor} = -1
    ?TRC:Account_Number:Prompt{prop:Color} = 15066597
    If ?suc:Account_Number{prop:ReadOnly} = True
        ?suc:Account_Number{prop:FontColor} = 65793
        ?suc:Account_Number{prop:Color} = 15066597
    Elsif ?suc:Account_Number{prop:Req} = True
        ?suc:Account_Number{prop:FontColor} = 65793
        ?suc:Account_Number{prop:Color} = 8454143
    Else ! If ?suc:Account_Number{prop:Req} = True
        ?suc:Account_Number{prop:FontColor} = 65793
        ?suc:Account_Number{prop:Color} = 16777215
    End ! If ?suc:Account_Number{prop:Req} = True
    ?suc:Account_Number{prop:Trn} = 0
    ?suc:Account_Number{prop:FontStyle} = font:Bold
    ?TRC:Model_Number:Prompt{prop:FontColor} = -1
    ?TRC:Model_Number:Prompt{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?suc:Charge_Type{prop:ReadOnly} = True
        ?suc:Charge_Type{prop:FontColor} = 65793
        ?suc:Charge_Type{prop:Color} = 15066597
    Elsif ?suc:Charge_Type{prop:Req} = True
        ?suc:Charge_Type{prop:FontColor} = 65793
        ?suc:Charge_Type{prop:Color} = 8454143
    Else ! If ?suc:Charge_Type{prop:Req} = True
        ?suc:Charge_Type{prop:FontColor} = 65793
        ?suc:Charge_Type{prop:Color} = 16777215
    End ! If ?suc:Charge_Type{prop:Req} = True
    ?suc:Charge_Type{prop:Trn} = 0
    ?suc:Charge_Type{prop:FontStyle} = font:Bold
    If ?suc:Model_Number{prop:ReadOnly} = True
        ?suc:Model_Number{prop:FontColor} = 65793
        ?suc:Model_Number{prop:Color} = 15066597
    Elsif ?suc:Model_Number{prop:Req} = True
        ?suc:Model_Number{prop:FontColor} = 65793
        ?suc:Model_Number{prop:Color} = 8454143
    Else ! If ?suc:Model_Number{prop:Req} = True
        ?suc:Model_Number{prop:FontColor} = 65793
        ?suc:Model_Number{prop:Color} = 16777215
    End ! If ?suc:Model_Number{prop:Req} = True
    ?suc:Model_Number{prop:Trn} = 0
    ?suc:Model_Number{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?suc:Unit_Type{prop:ReadOnly} = True
        ?suc:Unit_Type{prop:FontColor} = 65793
        ?suc:Unit_Type{prop:Color} = 15066597
    Elsif ?suc:Unit_Type{prop:Req} = True
        ?suc:Unit_Type{prop:FontColor} = 65793
        ?suc:Unit_Type{prop:Color} = 8454143
    Else ! If ?suc:Unit_Type{prop:Req} = True
        ?suc:Unit_Type{prop:FontColor} = 65793
        ?suc:Unit_Type{prop:Color} = 16777215
    End ! If ?suc:Unit_Type{prop:Req} = True
    ?suc:Unit_Type{prop:Trn} = 0
    ?suc:Unit_Type{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?suc:Repair_Type{prop:ReadOnly} = True
        ?suc:Repair_Type{prop:FontColor} = 65793
        ?suc:Repair_Type{prop:Color} = 15066597
    Elsif ?suc:Repair_Type{prop:Req} = True
        ?suc:Repair_Type{prop:FontColor} = 65793
        ?suc:Repair_Type{prop:Color} = 8454143
    Else ! If ?suc:Repair_Type{prop:Req} = True
        ?suc:Repair_Type{prop:FontColor} = 65793
        ?suc:Repair_Type{prop:Color} = 16777215
    End ! If ?suc:Repair_Type{prop:Req} = True
    ?suc:Repair_Type{prop:Trn} = 0
    ?suc:Repair_Type{prop:FontStyle} = font:Bold
    ?TRC:Cost:Prompt{prop:FontColor} = -1
    ?TRC:Cost:Prompt{prop:Color} = 15066597
    If ?suc:Cost{prop:ReadOnly} = True
        ?suc:Cost{prop:FontColor} = 65793
        ?suc:Cost{prop:Color} = 15066597
    Elsif ?suc:Cost{prop:Req} = True
        ?suc:Cost{prop:FontColor} = 65793
        ?suc:Cost{prop:Color} = 8454143
    Else ! If ?suc:Cost{prop:Req} = True
        ?suc:Cost{prop:FontColor} = 65793
        ?suc:Cost{prop:Color} = 16777215
    End ! If ?suc:Cost{prop:Req} = True
    ?suc:Cost{prop:Trn} = 0
    ?suc:Cost{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Sub Account Charge'
  OF ChangeRecord
    ActionMessage = 'Changing A Sub Account Charge'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Sub_Account_Charges')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRC:Account_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(suc:Record,History::suc:Record)
  SELF.AddHistoryField(?suc:Account_Number,1)
  SELF.AddHistoryField(?suc:Charge_Type,2)
  SELF.AddHistoryField(?suc:Model_Number,4)
  SELF.AddHistoryField(?suc:Unit_Type,3)
  SELF.AddHistoryField(?suc:Repair_Type,5)
  SELF.AddHistoryField(?suc:Cost,6)
  SELF.AddUpdateFile(Access:SUBCHRGE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SUBCHRGE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB4.Init(suc:Charge_Type,?suc:Charge_Type,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder(cha:Ref_Number_Key)
  FDCB4.AddField(cha:Charge_Type,FDCB4.Q.cha:Charge_Type)
  FDCB4.AddField(cha:Ref_Number,FDCB4.Q.cha:Ref_Number)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB6.Init(suc:Unit_Type,?suc:Unit_Type,Queue:FileDropCombo:1.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo:1,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo:1
  FDCB6.AddSortOrder(uni:Unit_Type_Key)
  FDCB6.AddField(uni:Unit_Type,FDCB6.Q.uni:Unit_Type)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB7.Init(suc:Repair_Type,?suc:Repair_Type,Queue:FileDropCombo:2.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:2,Relate:REPAIRTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:2
  FDCB7.AddSortOrder(rep:Model_Number_Key)
  FDCB7.AddRange(rep:Model_Number,suc:Model_Number)
  FDCB7.AddField(rep:Repair_Type,FDCB7.Q.rep:Repair_Type)
  FDCB7.AddField(rep:Manufacturer,FDCB7.Q.rep:Manufacturer)
  FDCB7.AddField(rep:Model_Number,FDCB7.Q.rep:Model_Number)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

