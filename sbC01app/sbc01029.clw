

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01029.INC'),ONCE        !Local module procedure declarations
                     END


StockDefaults PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
tmp:NokiaEDIRate     REAL
tmp:UseStockLevels   BYTE(0)
History::dst:Record  LIKE(dst:RECORD),STATIC
QuickWindow          WINDOW('Stock Control Defaults'),AT(,,232,128),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('StockDefaults'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,224,92),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           CHECK('Use Default Site Location'),AT(84,20,112,8),USE(dst:Use_Site_Location),VALUE('YES','NO')
                           GROUP,AT(8,24,216,24),USE(?Group1),DISABLE
                             PROMPT('Site Location'),AT(8,32),USE(?dst:Site_Location:Prompt),TRN
                             ENTRY(@s30),AT(84,32,124,10),USE(dst:Site_Location),FONT('Tahoma',8,,FONT:bold),UPR
                             BUTTON,AT(212,32,10,10),USE(?LookupSiteLocation),SKIP,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),ICON('List3.ico')
                           END
                           CHECK(' Force Requisition No.'),AT(84,49),USE(de2:UseRequisitionNumber),VALUE('1','0'),MSG('Use Requisition Number')
                           CHECK('Use Stock Access Levels (Webmaster)'),AT(84,64),USE(tmp:UseStockLevels),MSG('Use Stock Levels (Webmaster)'),TIP('Use Stock Levels (Webmaster)'),VALUE('1','0')
                           PROMPT('EDI Euro Rate'),AT(8,80),USE(?tmp:NokiaEDIRate:Prompt),TRN,HIDE
                           ENTRY(@n23.11),AT(84,80,64,10),USE(tmp:NokiaEDIRate),HIDE,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       BUTTON('&OK'),AT(112,104,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(168,104,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,100,224,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
look:dst:Site_Location                Like(dst:Site_Location)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?dst:Use_Site_Location{prop:Font,3} = -1
    ?dst:Use_Site_Location{prop:Color} = 15066597
    ?dst:Use_Site_Location{prop:Trn} = 0
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?dst:Site_Location:Prompt{prop:FontColor} = -1
    ?dst:Site_Location:Prompt{prop:Color} = 15066597
    If ?dst:Site_Location{prop:ReadOnly} = True
        ?dst:Site_Location{prop:FontColor} = 65793
        ?dst:Site_Location{prop:Color} = 15066597
    Elsif ?dst:Site_Location{prop:Req} = True
        ?dst:Site_Location{prop:FontColor} = 65793
        ?dst:Site_Location{prop:Color} = 8454143
    Else ! If ?dst:Site_Location{prop:Req} = True
        ?dst:Site_Location{prop:FontColor} = 65793
        ?dst:Site_Location{prop:Color} = 16777215
    End ! If ?dst:Site_Location{prop:Req} = True
    ?dst:Site_Location{prop:Trn} = 0
    ?dst:Site_Location{prop:FontStyle} = font:Bold
    ?de2:UseRequisitionNumber{prop:Font,3} = -1
    ?de2:UseRequisitionNumber{prop:Color} = 15066597
    ?de2:UseRequisitionNumber{prop:Trn} = 0
    ?tmp:UseStockLevels{prop:Font,3} = -1
    ?tmp:UseStockLevels{prop:Color} = 15066597
    ?tmp:UseStockLevels{prop:Trn} = 0
    ?tmp:NokiaEDIRate:Prompt{prop:FontColor} = -1
    ?tmp:NokiaEDIRate:Prompt{prop:Color} = 15066597
    If ?tmp:NokiaEDIRate{prop:ReadOnly} = True
        ?tmp:NokiaEDIRate{prop:FontColor} = 65793
        ?tmp:NokiaEDIRate{prop:Color} = 15066597
    Elsif ?tmp:NokiaEDIRate{prop:Req} = True
        ?tmp:NokiaEDIRate{prop:FontColor} = 65793
        ?tmp:NokiaEDIRate{prop:Color} = 8454143
    Else ! If ?tmp:NokiaEDIRate{prop:Req} = True
        ?tmp:NokiaEDIRate{prop:FontColor} = 65793
        ?tmp:NokiaEDIRate{prop:Color} = 16777215
    End ! If ?tmp:NokiaEDIRate{prop:Req} = True
    ?tmp:NokiaEDIRate{prop:Trn} = 0
    ?tmp:NokiaEDIRate{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting Stock Defaults'
  OF ChangeRecord
    ActionMessage = 'Stock Control Defaults'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:DEFSTOCK.Open
    SET(DEFSTOCK)
    CASE Access:DEFSTOCK.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:DEFSTOCK.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('StockDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?dst:Use_Site_Location
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(dst:Record,History::dst:Record)
  SELF.AddHistoryField(?dst:Use_Site_Location,1)
  SELF.AddHistoryField(?dst:Site_Location,3)
  SELF.AddUpdateFile(Access:DEFSTOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SET(Default2)
  Access:Default2.Next()
  OPEN(QuickWindow)
  SELF.Opened=True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If Instring('COMMUNICAID',def:User_Name,1,1)
      ?tmp:NokiaEDIRate{prop:Hide} = 0
      ?tmp:NokiaEDIRate:Prompt{prop:Hide} = 0
  End !Instring('COMMUNICAID',def:User_Name,1,1)
  tmp:NokiaEDIRate = GETINI('EDI','PartsOrderEuroRate',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:UseStockLevels = GETINI('WEBMASTER','StockLevels',,CLIP(PATH())&'\SB2KDEF.INI')
  Do RecolourWindow
  ! support for CPCS
  IF ?dst:Site_Location{Prop:Tip} AND ~?LookupSiteLocation{Prop:Tip}
     ?LookupSiteLocation{Prop:Tip} = 'Select ' & ?dst:Site_Location{Prop:Tip}
  END
  IF ?dst:Site_Location{Prop:Msg} AND ~?LookupSiteLocation{Prop:Msg}
     ?LookupSiteLocation{Prop:Msg} = 'Select ' & ?dst:Site_Location{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?dst:Use_Site_Location{Prop:Checked} = True
    ENABLE(?Group1)
  END
  IF ?dst:Use_Site_Location{Prop:Checked} = False
    DISABLE(?LookupSiteLocation)
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(DEFSTOCK)
      Access:DEFSTOCK.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Location
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?dst:Use_Site_Location
      IF ?dst:Use_Site_Location{Prop:Checked} = True
        ENABLE(?Group1)
      END
      IF ?dst:Use_Site_Location{Prop:Checked} = False
        DISABLE(?LookupSiteLocation)
      END
      ThisWindow.Reset
    OF ?dst:Site_Location
      IF dst:Site_Location OR ?dst:Site_Location{Prop:Req}
        loc:Location = dst:Site_Location
        !Save Lookup Field Incase Of error
        look:dst:Site_Location        = dst:Site_Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            dst:Site_Location = loc:Location
          ELSE
            !Restore Lookup On Error
            dst:Site_Location = look:dst:Site_Location
            SELECT(?dst:Site_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSiteLocation
      ThisWindow.Update
      loc:Location = dst:Site_Location
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          dst:Site_Location = loc:Location
          Select(?+1)
      ELSE
          Select(?dst:Site_Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?dst:Site_Location)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Access:Default2.Update()
  PUTINI('EDI','PartsOrderEuroRate',tmp:NokiaEDIRate,CLIP(PATH()) & '\SB2KDEF.INI')
  PUTINI('WEBMASTER','StockLevels',tmp:UseStockLevels,CLIP(PATH()) & '\SB2KDEF.INI')
  
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

