

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01042.INC'),ONCE        !Local module procedure declarations
                     END


ComputerNameDefaults PROCEDURE                        !Generated from procedure template - Window

ComputerNameQueue    QUEUE,PRE(CNQ)
ComputerName         STRING(20)
DefaultLocation      STRING(30)
                     END
IniPath              STRING(255)
IsModified           BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Computer Name Defaults'),AT(,,341,176),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,272,168),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           LIST,AT(8,8,260,160),USE(?List1),ALRT(MouseLeft2),FORMAT('85L(2)|M~Computer Name~@s20@120L(2)|M~Default Location~@s30@'),FROM(ComputerNameQueue)
                         END
                         TAB('Tab 2'),USE(?Tab2)
                         END
                       END
                       BUTTON('&insert'),AT(280,96,56,16),USE(?InsetButton),LEFT,ICON('insert.ico')
                       BUTTON('&Change'),AT(280,116,56,16),USE(?ChangeButton),LEFT,ICON('edit.ico')
                       BUTTON('&Delete'),AT(280,136,56,16),USE(?DeleteButton),LEFT,ICON('delete.ico')
                       BUTTON('&Close'),AT(280,156,56,16),USE(?CloseButton),LEFT,ICON('ok.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Tab2{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ComputerNameDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  IniPath = CLIP(PATH()) & '\Computer.ini'
  
  Count# = GETINI('COMPUTERNAMES', 'COUNT', 0, IniPath)
  FREE(ComputerNameQueue)
  
  LOOP ix#=1 TO Count#
      CLEAR(ComputerNameQueue)
      CNQ:ComputerName = GETINI('COMPUTERNAMES', 'NAME_' & FORMAT(ix#, @n06),, IniPath)
      CNQ:DefaultLocation = GETINI(CNQ:ComputerName, 'LOCATION',, IniPath)
      ADD(ComputerNameQueue)
  END
  
  SORT(ComputerNameQueue, CNQ:ComputerName)
  OPEN(window)
  SELF.Opened=True
  SELECT(?List1, 1, 1)
  GET(ComputerNameQueue, CHOICE(?List1))
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  ! support for CPCS
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?InsetButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InsetButton, Accepted)
      GLO:Select20 = ''
      GLO:Select21 = ''
      UpdateComputerNames()
      IF (GLO:Select22 = 'OK') THEN
          CLEAR(ComputerNameQueue)
          CNQ:ComputerName = GLO:Select20
          CNQ:DefaultLocation = GLO:Select21
          ADD(ComputerNameQueue, CNQ:ComputerName)
          IF (ERRORCODE()) THEN
              MESSAGE('Duplicate Computer Name Found')
          ELSE
              pos# = RECORDS(ComputerNameQueue)
              SELECT(?List1, pos#, pos#)
              IsModified = 1
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InsetButton, Accepted)
    OF ?ChangeButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChangeButton, Accepted)
      IF (RECORDS(ComputerNameQueue) > 0) THEN
          GLO:Select20 = CNQ:ComputerName
          GLO:Select21 = CNQ:DefaultLocation
          UpdateComputerNames()
          IF (GLO:Select22 = 'OK') THEN
              CNQ:ComputerName = GLO:Select20
              CNQ:DefaultLocation = GLO:Select21
              PUT(ComputerNameQueue, CNQ:ComputerName)
              IF (ERRORCODE()) THEN
                  MESSAGE('Duplicate Computer Name Found')
              ELSE
                  IsModified = 1
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChangeButton, Accepted)
    OF ?DeleteButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DeleteButton, Accepted)
      IF (RECORDS(ComputerNameQueue) > 0) THEN
          CASE MESSAGE('About to delete ' & CLIP(CNQ:ComputerName) & ', Are you sure ?',|
                       'Delete', ICON:QUESTION, BUTTON:YES+BUTTON:NO,BUTTON:NO,1)
          OF BUTTON:NO
              CYCLE
          OF BUTTON:YES
              p# = POINTER(ComputerNameQueue)
              DELETE(ComputerNameQueue)
              IF (p# > RECORDS(ComputerNameQueue)) THEN
                  p# = RECORDS(ComputerNameQueue)
              END
              IF (p# > 0) THEN
                  SELECT(?List1, p#, p#)
                  GET(ComputerNameQueue, CHOICE(?List1))
              END
              IsModified = 1
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DeleteButton, Accepted)
    OF ?CloseButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CloseButton, Accepted)
      IF (IsModified = 1) THEN
          REMOVE(CLIP(PATH()) & '\Computer.bak')
          RENAME(IniPath, CLIP(PATH()) & '\Computer.bak')
      
          PUTINI('COMPUTERNAMES', 'COUNT', FORMAT(RECORDS(ComputerNameQueue), @n06), IniPath)
          LOOP ix#=1 TO RECORDS(ComputerNameQueue)
              GET(ComputerNameQueue, ix#)
              PUTINI('COMPUTERNAMES', 'NAME_' & FORMAT(ix#, @n06), CNQ:ComputerName, IniPath)
              PUTINI(CNQ:ComputerName, 'LOCATION', CNQ:DefaultLocation, IniPath)
          END
      END
      POST(EVENT:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CloseButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1, AlertKey)
      IF (KEYCODE() = MouseLeft2) THEN
           POST(EVENT:Accepted, ?ChangeButton)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1, NewSelection)
      GET(ComputerNameQueue, CHOICE(?List1))
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

