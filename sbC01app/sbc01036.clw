

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01036.INC'),ONCE        !Local module procedure declarations
                     END


AddChargesOption PROCEDURE                            !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Selection        BYTE(1)
tmp:Return           BYTE(0)
window               WINDOW('Add Charges'),AT(,,223,167),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,216,132),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Add Standard Charges'),AT(8,8),USE(?Prompt1)
                           OPTION,AT(12,28,200,88),USE(tmp:Selection),BOXED
                             RADIO,AT(24,40,176,16),USE(?tmp:Selection:Radio1),VALUE('1')
                             RADIO,AT(24,64,176,20),USE(?tmp:Selection:Radio2),VALUE('2')
                             RADIO,AT(24,88,176,20),USE(?tmp:Selection:Radio3),VALUE('3')
                           END
                           PROMPT('Add charges to the selected Model Number ONLY.'),AT(40,40,160,16),USE(?Prompt2)
                           PROMPT('Add charges to ALL Model Numbers to the selected Manufacturer ONLY.'),AT(40,64,160,24),USE(?Prompt3)
                           PROMPT('Add charges to ALL Model Numbers AND ALL Manufacturers.'),AT(40,88,160,24),USE(?Prompt3:2)
                         END
                       END
                       PANEL,AT(4,140,216,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(104,144,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(160,144,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:Selection{prop:Font,3} = -1
    ?tmp:Selection{prop:Color} = 15066597
    ?tmp:Selection{prop:Trn} = 0
    ?tmp:Selection:Radio1{prop:Font,3} = -1
    ?tmp:Selection:Radio1{prop:Color} = 15066597
    ?tmp:Selection:Radio1{prop:Trn} = 0
    ?tmp:Selection:Radio2{prop:Font,3} = -1
    ?tmp:Selection:Radio2{prop:Color} = 15066597
    ?tmp:Selection:Radio2{prop:Trn} = 0
    ?tmp:Selection:Radio3{prop:Font,3} = -1
    ?tmp:Selection:Radio3{prop:Color} = 15066597
    ?tmp:Selection:Radio3{prop:Trn} = 0
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt3:2{prop:FontColor} = -1
    ?Prompt3:2{prop:Color} = 15066597
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AddChargesOption')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      tmp:Return  = tmp:Selection
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Return = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

