

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01023.INC'),ONCE        !Local module procedure declarations
                     END


UpdateDEFWEB PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?dew:Account_Number
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?dew:Warranty_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?dew:Transit_Type
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?dew:Job_Priority
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?dew:Status_Type
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?dew:Insurance_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:8 QUEUE                           !Queue declaration for browse/combo box using ?dew:Chargeable_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:6 QUEUE                           !Queue declaration for browse/combo box using ?dew:HandSet
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB7::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB8::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB9::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB10::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB11::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB14::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB12::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
History::dew:Record  LIKE(dew:RECORD),STATIC
QuickWindow          WINDOW('Update the DEFWEB File'),AT(,,219,315),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateDEFWEB'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,280),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           COMBO(@s15),AT(84,20,124,10),USE(dew:Account_Number),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('65L(2)|M@s15@120L(2)|M@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                           PROMPT('Handset Type'),AT(8,36),USE(?dew:handset:prompt)
                           COMBO(@s30),AT(84,36,124,10),USE(dew:HandSet),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:6),MSG('Handset Type')
                           GROUP('Transit Type'),AT(8,48,204,60),USE(?Delivery),BOXED
                             PROMPT('Delivery'),AT(12,60),USE(?Prompt3)
                             COMBO(@s30),AT(84,60,124,10),USE(dew:Transit_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                             PROMPT('Collection'),AT(12,76),USE(?Prompt4)
                             COMBO(@s30),AT(84,76,124,10),USE(dew:Job_Priority),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                             PROMPT('Exchange'),AT(12,92),USE(?Prompt5)
                             COMBO(@s30),AT(84,92,124,10),USE(dew:Status_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4)
                           END
                           GROUP('Charge Type'),AT(8,112,204,60),USE(?Warranty),BOXED
                             PROMPT('Warranty'),AT(12,124),USE(?Prompt2)
                             COMBO(@s30),AT(84,124,124,10),USE(dew:Warranty_Charge_Type),IMM,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                             PROMPT('Insurance'),AT(12,140),USE(?Prompt7)
                             COMBO(@s30),AT(84,140,124,10),USE(dew:Insurance_Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:5)
                             PROMPT('Chargeable'),AT(12,156),USE(?Prompt9)
                             COMBO(@s30),AT(84,156,124,10),USE(dew:Chargeable_Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:8)
                           END
                           GROUP('Allow Access To'),AT(8,176,204,104),USE(?Group3),BOXED
                             CHECK('New Job Booking'),AT(16,188),USE(dew:AllowJobBooking),MSG('New Job Booking'),TIP('New Job Booking'),VALUE('1','0')
                             CHECK('Job Reports'),AT(120,188),USE(dew:Option6),MSG('Job Reports'),TIP('Job Reports'),VALUE('1','0')
                             CHECK('Job Progress'),AT(16,204),USE(dew:AllowJobProgress),MSG('Job Progress'),TIP('Job Progress'),VALUE('1','0')
                             CHECK('Order Reports'),AT(120,220),USE(dew:Option7),MSG('Order Reports'),TIP('Order Reports'),VALUE('1','0')
                             CHECK('Order Parts'),AT(16,220),USE(dew:AllowOrderParts),MSG('Order Parts'),TIP('Order Parts'),VALUE('1','0')
                             CHECK('Use I.M.E.I. No. Validation'),AT(16,268),USE(dew:Option8),MSG('Use I.M.E.I. No. Validation'),TIP('Use I.M.E.I. No. Validation'),VALUE('1','0')
                             CHECK('Order Progress'),AT(16,236),USE(dew:AllowOrderProgress),MSG('Order Progress'),TIP('Order Progress'),VALUE('1','0')
                             CHECK('Use I.M.E.I. No. Lookup'),AT(120,268),USE(dew:Option9),MSG('Use I.M.E.I. No. Lookup'),TIP('Use I.M.E.I. No. Lookup'),VALUE('1','0')
                             CHECK('Accept/Reject Estimates'),AT(16,252),USE(dew:Option5),MSG('Accept/Reject Estimates'),TIP('Accept/Reject Estimates'),VALUE('1','0')
                             CHECK('Option 10'),AT(120,252),USE(dew:Option10),HIDE,MSG('Option 10'),TIP('Option 10'),VALUE('1','0')
                           END
                           PROMPT('Account Number'),AT(8,20),USE(?Prompt1)
                         END
                       END
                       PANEL,AT(4,288,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,292,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,292,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB9                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

FDCB14               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:8         !Reference to browse queue type
                     END

FDCB12               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:6         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?dew:Account_Number{prop:ReadOnly} = True
        ?dew:Account_Number{prop:FontColor} = 65793
        ?dew:Account_Number{prop:Color} = 15066597
    Elsif ?dew:Account_Number{prop:Req} = True
        ?dew:Account_Number{prop:FontColor} = 65793
        ?dew:Account_Number{prop:Color} = 8454143
    Else ! If ?dew:Account_Number{prop:Req} = True
        ?dew:Account_Number{prop:FontColor} = 65793
        ?dew:Account_Number{prop:Color} = 16777215
    End ! If ?dew:Account_Number{prop:Req} = True
    ?dew:Account_Number{prop:Trn} = 0
    ?dew:Account_Number{prop:FontStyle} = font:Bold
    ?dew:handset:prompt{prop:FontColor} = -1
    ?dew:handset:prompt{prop:Color} = 15066597
    If ?dew:HandSet{prop:ReadOnly} = True
        ?dew:HandSet{prop:FontColor} = 65793
        ?dew:HandSet{prop:Color} = 15066597
    Elsif ?dew:HandSet{prop:Req} = True
        ?dew:HandSet{prop:FontColor} = 65793
        ?dew:HandSet{prop:Color} = 8454143
    Else ! If ?dew:HandSet{prop:Req} = True
        ?dew:HandSet{prop:FontColor} = 65793
        ?dew:HandSet{prop:Color} = 16777215
    End ! If ?dew:HandSet{prop:Req} = True
    ?dew:HandSet{prop:Trn} = 0
    ?dew:HandSet{prop:FontStyle} = font:Bold
    ?Delivery{prop:Font,3} = -1
    ?Delivery{prop:Color} = 15066597
    ?Delivery{prop:Trn} = 0
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?dew:Transit_Type{prop:ReadOnly} = True
        ?dew:Transit_Type{prop:FontColor} = 65793
        ?dew:Transit_Type{prop:Color} = 15066597
    Elsif ?dew:Transit_Type{prop:Req} = True
        ?dew:Transit_Type{prop:FontColor} = 65793
        ?dew:Transit_Type{prop:Color} = 8454143
    Else ! If ?dew:Transit_Type{prop:Req} = True
        ?dew:Transit_Type{prop:FontColor} = 65793
        ?dew:Transit_Type{prop:Color} = 16777215
    End ! If ?dew:Transit_Type{prop:Req} = True
    ?dew:Transit_Type{prop:Trn} = 0
    ?dew:Transit_Type{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?dew:Job_Priority{prop:ReadOnly} = True
        ?dew:Job_Priority{prop:FontColor} = 65793
        ?dew:Job_Priority{prop:Color} = 15066597
    Elsif ?dew:Job_Priority{prop:Req} = True
        ?dew:Job_Priority{prop:FontColor} = 65793
        ?dew:Job_Priority{prop:Color} = 8454143
    Else ! If ?dew:Job_Priority{prop:Req} = True
        ?dew:Job_Priority{prop:FontColor} = 65793
        ?dew:Job_Priority{prop:Color} = 16777215
    End ! If ?dew:Job_Priority{prop:Req} = True
    ?dew:Job_Priority{prop:Trn} = 0
    ?dew:Job_Priority{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?dew:Status_Type{prop:ReadOnly} = True
        ?dew:Status_Type{prop:FontColor} = 65793
        ?dew:Status_Type{prop:Color} = 15066597
    Elsif ?dew:Status_Type{prop:Req} = True
        ?dew:Status_Type{prop:FontColor} = 65793
        ?dew:Status_Type{prop:Color} = 8454143
    Else ! If ?dew:Status_Type{prop:Req} = True
        ?dew:Status_Type{prop:FontColor} = 65793
        ?dew:Status_Type{prop:Color} = 16777215
    End ! If ?dew:Status_Type{prop:Req} = True
    ?dew:Status_Type{prop:Trn} = 0
    ?dew:Status_Type{prop:FontStyle} = font:Bold
    ?Warranty{prop:Font,3} = -1
    ?Warranty{prop:Color} = 15066597
    ?Warranty{prop:Trn} = 0
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?dew:Warranty_Charge_Type{prop:ReadOnly} = True
        ?dew:Warranty_Charge_Type{prop:FontColor} = 65793
        ?dew:Warranty_Charge_Type{prop:Color} = 15066597
    Elsif ?dew:Warranty_Charge_Type{prop:Req} = True
        ?dew:Warranty_Charge_Type{prop:FontColor} = 65793
        ?dew:Warranty_Charge_Type{prop:Color} = 8454143
    Else ! If ?dew:Warranty_Charge_Type{prop:Req} = True
        ?dew:Warranty_Charge_Type{prop:FontColor} = 65793
        ?dew:Warranty_Charge_Type{prop:Color} = 16777215
    End ! If ?dew:Warranty_Charge_Type{prop:Req} = True
    ?dew:Warranty_Charge_Type{prop:Trn} = 0
    ?dew:Warranty_Charge_Type{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?dew:Insurance_Charge_Type{prop:ReadOnly} = True
        ?dew:Insurance_Charge_Type{prop:FontColor} = 65793
        ?dew:Insurance_Charge_Type{prop:Color} = 15066597
    Elsif ?dew:Insurance_Charge_Type{prop:Req} = True
        ?dew:Insurance_Charge_Type{prop:FontColor} = 65793
        ?dew:Insurance_Charge_Type{prop:Color} = 8454143
    Else ! If ?dew:Insurance_Charge_Type{prop:Req} = True
        ?dew:Insurance_Charge_Type{prop:FontColor} = 65793
        ?dew:Insurance_Charge_Type{prop:Color} = 16777215
    End ! If ?dew:Insurance_Charge_Type{prop:Req} = True
    ?dew:Insurance_Charge_Type{prop:Trn} = 0
    ?dew:Insurance_Charge_Type{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    If ?dew:Chargeable_Charge_Type{prop:ReadOnly} = True
        ?dew:Chargeable_Charge_Type{prop:FontColor} = 65793
        ?dew:Chargeable_Charge_Type{prop:Color} = 15066597
    Elsif ?dew:Chargeable_Charge_Type{prop:Req} = True
        ?dew:Chargeable_Charge_Type{prop:FontColor} = 65793
        ?dew:Chargeable_Charge_Type{prop:Color} = 8454143
    Else ! If ?dew:Chargeable_Charge_Type{prop:Req} = True
        ?dew:Chargeable_Charge_Type{prop:FontColor} = 65793
        ?dew:Chargeable_Charge_Type{prop:Color} = 16777215
    End ! If ?dew:Chargeable_Charge_Type{prop:Req} = True
    ?dew:Chargeable_Charge_Type{prop:Trn} = 0
    ?dew:Chargeable_Charge_Type{prop:FontStyle} = font:Bold
    ?Group3{prop:Font,3} = -1
    ?Group3{prop:Color} = 15066597
    ?Group3{prop:Trn} = 0
    ?dew:AllowJobBooking{prop:Font,3} = -1
    ?dew:AllowJobBooking{prop:Color} = 15066597
    ?dew:AllowJobBooking{prop:Trn} = 0
    ?dew:Option6{prop:Font,3} = -1
    ?dew:Option6{prop:Color} = 15066597
    ?dew:Option6{prop:Trn} = 0
    ?dew:AllowJobProgress{prop:Font,3} = -1
    ?dew:AllowJobProgress{prop:Color} = 15066597
    ?dew:AllowJobProgress{prop:Trn} = 0
    ?dew:Option7{prop:Font,3} = -1
    ?dew:Option7{prop:Color} = 15066597
    ?dew:Option7{prop:Trn} = 0
    ?dew:AllowOrderParts{prop:Font,3} = -1
    ?dew:AllowOrderParts{prop:Color} = 15066597
    ?dew:AllowOrderParts{prop:Trn} = 0
    ?dew:Option8{prop:Font,3} = -1
    ?dew:Option8{prop:Color} = 15066597
    ?dew:Option8{prop:Trn} = 0
    ?dew:AllowOrderProgress{prop:Font,3} = -1
    ?dew:AllowOrderProgress{prop:Color} = 15066597
    ?dew:AllowOrderProgress{prop:Trn} = 0
    ?dew:Option9{prop:Font,3} = -1
    ?dew:Option9{prop:Color} = 15066597
    ?dew:Option9{prop:Trn} = 0
    ?dew:Option5{prop:Font,3} = -1
    ?dew:Option5{prop:Color} = 15066597
    ?dew:Option5{prop:Trn} = 0
    ?dew:Option10{prop:Font,3} = -1
    ?dew:Option10{prop:Color} = 15066597
    ?dew:Option10{prop:Trn} = 0
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting The Web Defaults'
  OF ChangeRecord
    ActionMessage = 'Changing The Web Defaults'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateDEFWEB')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?dew:Account_Number
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(dew:Record,History::dew:Record)
  SELF.AddHistoryField(?dew:Account_Number,2)
  SELF.AddHistoryField(?dew:HandSet,9)
  SELF.AddHistoryField(?dew:Transit_Type,5)
  SELF.AddHistoryField(?dew:Job_Priority,6)
  SELF.AddHistoryField(?dew:Status_Type,8)
  SELF.AddHistoryField(?dew:Warranty_Charge_Type,3)
  SELF.AddHistoryField(?dew:Insurance_Charge_Type,11)
  SELF.AddHistoryField(?dew:Chargeable_Charge_Type,10)
  SELF.AddHistoryField(?dew:AllowJobBooking,12)
  SELF.AddHistoryField(?dew:Option6,17)
  SELF.AddHistoryField(?dew:AllowJobProgress,13)
  SELF.AddHistoryField(?dew:Option7,18)
  SELF.AddHistoryField(?dew:AllowOrderParts,14)
  SELF.AddHistoryField(?dew:Option8,19)
  SELF.AddHistoryField(?dew:AllowOrderProgress,15)
  SELF.AddHistoryField(?dew:Option9,20)
  SELF.AddHistoryField(?dew:Option5,16)
  SELF.AddHistoryField(?dew:Option10,21)
  SELF.AddUpdateFile(Access:DEFWEB)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:DEFWEB.Open
  Relate:TRANTYPE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFWEB
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(dew:Account_Number,?dew:Account_Number,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sub:Account_Number_Key)
  FDCB6.AddField(sub:Account_Number,FDCB6.Q.sub:Account_Number)
  FDCB6.AddField(sub:Company_Name,FDCB6.Q.sub:Company_Name)
  FDCB6.AddField(sub:RecordNumber,FDCB6.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB7.Init(dew:Warranty_Charge_Type,?dew:Warranty_Charge_Type,Queue:FileDropCombo:1.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:1,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:1
  FDCB7.AddSortOrder(cha:Warranty_Ref_Number_Key)
  FDCB7.SetFilter('Upper(cha:warranty) = ''YES''')
  FDCB7.AddField(cha:Charge_Type,FDCB7.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  FDCB8.Init(dew:Transit_Type,?dew:Transit_Type,Queue:FileDropCombo:2.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:2,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:2
  FDCB8.AddSortOrder(trt:Transit_Type_Key)
  FDCB8.AddField(trt:Transit_Type,FDCB8.Q.trt:Transit_Type)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  FDCB9.Init(dew:Job_Priority,?dew:Job_Priority,Queue:FileDropCombo:3.ViewPosition,FDCB9::View:FileDropCombo,Queue:FileDropCombo:3,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB9.Q &= Queue:FileDropCombo:3
  FDCB9.AddSortOrder(trt:Transit_Type_Key)
  FDCB9.AddField(trt:Transit_Type,FDCB9.Q.trt:Transit_Type)
  ThisWindow.AddItem(FDCB9.WindowComponent)
  FDCB9.DefaultFill = 0
  FDCB10.Init(dew:Status_Type,?dew:Status_Type,Queue:FileDropCombo:4.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:4,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:4
  FDCB10.AddSortOrder(trt:Transit_Type_Key)
  FDCB10.AddField(trt:Transit_Type,FDCB10.Q.trt:Transit_Type)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  FDCB11.Init(dew:Insurance_Charge_Type,?dew:Insurance_Charge_Type,Queue:FileDropCombo:5.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo:5,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo:5
  FDCB11.AddSortOrder(cha:Warranty_Ref_Number_Key)
  FDCB11.SetFilter('Upper(cha:warranty) = ''NO''')
  FDCB11.AddField(cha:Charge_Type,FDCB11.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  FDCB14.Init(dew:Chargeable_Charge_Type,?dew:Chargeable_Charge_Type,Queue:FileDropCombo:8.ViewPosition,FDCB14::View:FileDropCombo,Queue:FileDropCombo:8,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB14.Q &= Queue:FileDropCombo:8
  FDCB14.AddSortOrder(cha:Warranty_Ref_Number_Key)
  FDCB14.SetFilter('Upper(cha:warranty) = ''NO''')
  FDCB14.AddField(cha:Charge_Type,FDCB14.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB14.WindowComponent)
  FDCB14.DefaultFill = 0
  FDCB12.Init(dew:HandSet,?dew:HandSet,Queue:FileDropCombo:6.ViewPosition,FDCB12::View:FileDropCombo,Queue:FileDropCombo:6,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB12.Q &= Queue:FileDropCombo:6
  FDCB12.AddSortOrder(uni:Unit_Type_Key)
  FDCB12.AddField(uni:Unit_Type,FDCB12.Q.uni:Unit_Type)
  ThisWindow.AddItem(FDCB12.WindowComponent)
  FDCB12.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFWEB.Close
    Relate:TRANTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

