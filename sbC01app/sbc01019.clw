

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01019.INC'),ONCE        !Local module procedure declarations
                     END


Vodafone_Defaults PROCEDURE                           !Generated from procedure template - Window

FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Vodafone Defaults'),AT(,,272,80),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,264,44),USE(?Sheet1),SPREAD
                         TAB('Vodafone Default'),USE(?Tab1)
                           PROMPT('Vodafone Import Path'),AT(8,20),USE(?DEF:Vodafone_Import_Path:Prompt),TRN
                           ENTRY(@s255),AT(84,20,164,10),USE(def:Vodafone_Import_Path),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(252,20,10,10),USE(?Directory),ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,52,264,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(152,56,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(208,56,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?DEF:Vodafone_Import_Path:Prompt{prop:FontColor} = -1
    ?DEF:Vodafone_Import_Path:Prompt{prop:Color} = 15066597
    If ?def:Vodafone_Import_Path{prop:ReadOnly} = True
        ?def:Vodafone_Import_Path{prop:FontColor} = 65793
        ?def:Vodafone_Import_Path{prop:Color} = 15066597
    Elsif ?def:Vodafone_Import_Path{prop:Req} = True
        ?def:Vodafone_Import_Path{prop:FontColor} = 65793
        ?def:Vodafone_Import_Path{prop:Color} = 8454143
    Else ! If ?def:Vodafone_Import_Path{prop:Req} = True
        ?def:Vodafone_Import_Path{prop:FontColor} = 65793
        ?def:Vodafone_Import_Path{prop:Color} = 16777215
    End ! If ?def:Vodafone_Import_Path{prop:Req} = True
    ?def:Vodafone_Import_Path{prop:Trn} = 0
    ?def:Vodafone_Import_Path{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Vodafone_Defaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DEF:Vodafone_Import_Path:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Set(defaults)
  access:defaults.next()
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defaults.update
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

