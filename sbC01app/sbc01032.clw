

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01032.INC'),ONCE        !Local module procedure declarations
                     END


MainDefaults PROCEDURE                                !Generated from procedure template - Window

LocalRequest         LONG
JobStatusColours     GROUP,PRE(JSC)
Maroon               STRING(30)
Green                STRING(30)
Olive                STRING(30)
Navy                 STRING(30)
Purple               STRING(30)
Teal                 STRING(30)
Gray                 STRING(30)
Blue                 STRING(30)
MaroonAction         BYTE(0)
GreenAction          BYTE(0)
OliveAction          BYTE(0)
NavyAction           BYTE(0)
PurpleAction         BYTE(0)
TealAction           BYTE(0)
GrayAction           BYTE(0)
BlueAction           BYTE(0)
                     END
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
tmp:RenameColour     BYTE(0)
tmp:ColourFieldName  STRING(30)
tmp:AccCheckAtComp   BYTE(0)
tmp:StopLiveBouncers BYTE(0)
tmp:HideSkillLevel   BYTE(0)
tmp:ForceEstimateAccepted BYTE(0)
tmp:HideNetwork      BYTE(0)
tmp:ShowPaymentDespatch BYTE(0)
tmp:force_fault_desc BYTE
tmp:RecordExchangeReason BYTE(0)
tmp:LockInWorkshopDate BYTE(0)
tmp:ValidateMobileNumber BYTE(0)
tmp:ExcludeCancelledJobs BYTE(0)
tmp:RepairTypeCancelledJob BYTE
tmp:Disable_MSN_If_Not_InWorkshop BYTE
tmp:ReplicateModelsToLocations BYTE
tmp:CreateMultipleDespatchCSV BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:RemoveUnlock     BYTE(0)
FDB3::View:FileDrop  VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?def:PickNoteJobStatus
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('General Defaults'),AT(,,665,336),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,656,300),USE(?Sheet1),WIZARD,SPREAD
                         TAB('&Main Defaults'),USE(?Tab1)
                           PROMPT('Main Defaults'),AT(8,8),USE(?MainDefaults),TRN
                           PANEL,AT(236,24,4,260),USE(?Panel2)
                           CHECK('Use Postcode Program'),AT(84,20),USE(def:Use_Postcode),VALUE('YES','NO')
                           CHECK('DLL Version'),AT(180,20),USE(def:PostcodeDll),HIDE,RIGHT,MSG('Use DLL Version Of Postcode'),TIP('Use DLL Version Of Postcode'),VALUE('YES','NO')
                           PROMPT('Postcode Path'),AT(8,36),USE(?DEF:Postcode_Path:Prompt),TRN
                           ENTRY(@s255),AT(84,36,124,10),USE(def:Postcode_Path),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,36,10,10),USE(?Lookup_Postcode_Path),SKIP,ICON('list3.ico')
                           PROMPT('Estimate If Over'),AT(8,52),USE(?DEF:Estimate_If_Over:Prompt),TRN
                           ENTRY(@n14.2),AT(84,52,64,10),USE(def:Estimate_If_Over),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Default Export Path'),AT(8,68),USE(?DEF:ExportPath:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,68,124,10),USE(def:ExportPath),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Default Export Path'),TIP('Default Path for all export files'),UPR
                           BUTTON,AT(212,68,10,10),USE(?LookupExportPath),SKIP,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('List3.ico')
                           GROUP('Autosearch'),AT(8,80,216,48),USE(?Autosearchgroup),BOXED
                             CHECK('Default Autosearch'),AT(84,88),USE(def:Automatic_Replicate),VALUE('YES','NO')
                             PROMPT('Replicate To Field'),AT(12,104),USE(?DEF:Automatic_Replicate_Field:Prompt),TRN
                             COMBO(@s15),AT(84,104,124,10),USE(def:Automatic_Replicate_Field),LEFT(2),FONT('Tahoma',8,,FONT:bold),UPR,DROP(10),FROM('SURNAME|MOBILE NUMBER|E.S.N./I.M.E.I.|M.S.N.|POSTCODE')
                           END
                           GROUP('Job Status Colours'),AT(480,16,172,152),USE(?Group6),BOXED,TRN
                             PROMPT('Action'),AT(628,24),USE(?Prompt8),TRN
                             PANEL,AT(488,35,12,12),USE(?Panel3),FILL(COLOR:Maroon),BEVEL(-1)
                             ENTRY(@s30),AT(504,36,100,10),USE(JSC:Maroon),FONT(,,,FONT:bold),READONLY
                             BUTTON('...'),AT(607,36,10,10),USE(?PickMaroonButton),LEFT,TIP('Pick Job Status'),ICON('list3.ico')
                             BUTTON('...'),AT(620,36,10,10),USE(?ClearMaroonButton),LEFT,TIP('Clear Job Status'),ICON(ICON:Cut)
                             CHECK,AT(636,36),USE(JSC:MaroonAction),TRN,COLOR(COLOR:Black),TIP('Stop Processing Action')
                             PANEL,AT(488,51,12,12),USE(?Panel4),FILL(COLOR:Green),BEVEL(-1)
                             ENTRY(@s30),AT(504,52,100,10),USE(JSC:Green),FONT(,,,FONT:bold),READONLY
                             BUTTON('...'),AT(607,52,10,10),USE(?PickGreenButton),LEFT,TIP('Pick Job Status'),ICON('list3.ico')
                             BUTTON('...'),AT(620,52,10,10),USE(?ClearGreenButton),LEFT,TIP('Clear Job Status'),ICON(ICON:Cut)
                             CHECK,AT(636,52),USE(JSC:GreenAction),TRN,TIP('Stop Processing Action')
                             PANEL,AT(488,67,12,12),USE(?Panel5),FILL(COLOR:Olive),BEVEL(-1)
                             ENTRY(@s30),AT(504,68,100,10),USE(JSC:Olive),FONT(,,,FONT:bold),READONLY
                             BUTTON('...'),AT(607,68,10,10),USE(?PickOliveButton),LEFT,TIP('Pick Job Status'),ICON('list3.ico')
                             BUTTON('...'),AT(620,68,10,10),USE(?ClearOliveButton),LEFT,TIP('Clear Job Status'),ICON(ICON:Cut)
                             CHECK,AT(636,68),USE(JSC:OliveAction),TRN,TIP('Stop Processing Action')
                             PANEL,AT(488,83,12,12),USE(?Panel6),FILL(COLOR:Navy),BEVEL(-1)
                             ENTRY(@s30),AT(504,84,100,10),USE(JSC:Navy),FONT(,,,FONT:bold),READONLY
                             BUTTON('...'),AT(607,84,10,10),USE(?PickNavyButton),LEFT,TIP('Pick Job Status'),ICON('list3.ico')
                             BUTTON('...'),AT(620,84,10,10),USE(?ClearNavyButton),LEFT,TIP('Clear Job Status'),ICON(ICON:Cut)
                             CHECK,AT(636,84),USE(JSC:NavyAction),TRN,TIP('Stop Processing Action')
                             PANEL,AT(488,99,12,12),USE(?Panel7),FILL(COLOR:Purple),BEVEL(-1)
                             ENTRY(@s30),AT(504,100,100,10),USE(JSC:Purple),FONT(,,,FONT:bold),READONLY
                             BUTTON('...'),AT(607,100,10,10),USE(?PickPurpleButton),LEFT,TIP('Pick Job Status'),ICON('list3.ico')
                             BUTTON('...'),AT(620,100,10,10),USE(?ClearPurpleButton),LEFT,TIP('Clear Job Status'),ICON(ICON:Cut)
                             CHECK,AT(636,100),USE(JSC:PurpleAction),TRN,TIP('Stop Processing Action')
                             PANEL,AT(488,115,12,12),USE(?Panel8),FILL(COLOR:Teal),BEVEL(-1)
                             CHECK,AT(636,116),USE(JSC:TealAction),TRN,TIP('Stop Processing Action')
                             ENTRY(@s30),AT(504,116,100,10),USE(JSC:Teal),FONT(,,,FONT:bold),READONLY
                             BUTTON('...'),AT(607,116,10,10),USE(?PickTealButton),LEFT,TIP('Pick Job Status'),ICON('list3.ico')
                             BUTTON('...'),AT(620,116,10,10),USE(?ClearTealButton),LEFT,TIP('Clear Job Status'),ICON(ICON:Cut)
                             PANEL,AT(488,131,12,12),USE(?Panel9),FILL(COLOR:Gray),BEVEL(-1)
                             ENTRY(@s30),AT(504,132,100,10),USE(JSC:Gray),FONT(,,,FONT:bold),READONLY
                             BUTTON('...'),AT(607,132,10,10),USE(?PickGrayButton),LEFT,TIP('Pick Job Status'),ICON('list3.ico')
                             BUTTON('...'),AT(620,132,10,10),USE(?ClearGrayButton),LEFT,TIP('Clear Job Status'),ICON(ICON:Cut)
                             CHECK,AT(636,132),USE(JSC:GrayAction),TRN,TIP('Stop Processing Action')
                             PANEL,AT(488,147,12,12),USE(?Panel10),FILL(COLOR:Blue),BEVEL(-1)
                             ENTRY(@s30),AT(504,148,100,10),USE(JSC:Blue),FONT(,,,FONT:bold),READONLY
                             BUTTON('..'),AT(607,148,10,10),USE(?PickBlueButton),LEFT,TIP('Pick Job Status'),ICON('list3.ico')
                             BUTTON('...'),AT(620,148,10,10),USE(?ClearBlueButton),LEFT,TIP('Clear Job Status'),ICON(ICON:Cut)
                             CHECK,AT(636,148),USE(JSC:BlueAction),TRN,TIP('Stop Processing Action')
                           END
                           GROUP('Hide Fields On Job Screen'),AT(8,132,216,104),USE(?HideFieldsGroup),BOXED
                             CHECK('Mobile Number'),AT(24,148),USE(def:Show_Mobile_Number),VALUE('YES','NO')
                             CHECK('Loan / Exchange Details'),AT(24,160),USE(def:Show_Loan_Exchange_Details),VALUE('YES','NO')
                             CHECK('Physical Damage'),AT(24,172),USE(def:Hide_Physical_Damage),VALUE('YES','NO')
                             CHECK('Insurance'),AT(24,184),USE(def:Hide_Insurance),VALUE('YES','NO')
                             CHECK('Authority Number'),AT(24,196),USE(def:Hide_Authority_Number),VALUE('YES','NO')
                             CHECK('Colour'),AT(24,208),USE(def:HideColour),RIGHT,VALUE('YES','NO')
                             CHECK('Incoming Courier'),AT(24,220),USE(def:HideInCourier),RIGHT,MSG('Hide Incoming Courier'),TIP('Hide Incoming Courier'),VALUE('YES','NO')
                             CHECK('Intermittant Fault'),AT(132,148),USE(def:HideIntFault),RIGHT,MSG('Hide Intermittant Fault'),TIP('Hide Intermittant Fault'),VALUE('YES','NO')
                             CHECK('Product Code'),AT(132,160),USE(def:HideProduct),RIGHT,MSG('Hide Product Code'),TIP('Hide Product Code'),VALUE('YES','NO')
                             CHECK('Hide Internal Location'),AT(132,172),USE(def:HideLocation),MSG('Hide Internal Location'),TIP('Hide Internal Location'),VALUE('1','0')
                             CHECK('Hide Electronic QA'),AT(132,184),USE(def:QAPreliminary),MSG('Use Preliminary QA Check'),TIP('Use Preliminary QA Check'),VALUE('YES','NO')
                             CHECK('Job Skill Level'),AT(132,196),USE(tmp:HideSkillLevel),MSG('Job Skill Level'),TIP('Job Skill Level'),VALUE('1','0')
                             CHECK('Network'),AT(132,208),USE(tmp:HideNetwork),MSG('s'),TIP('s'),VALUE('1','0')
                           END
                           GROUP('Completion Defaults'),AT(252,160,216,32),USE(?Group5),BOXED
                             CHECK('Validate I.M.E.I. Number At Closing'),AT(328,168),USE(def:ValidateESN),TRN,RIGHT,MSG('Validate ESN Before Rapid Job Update'),TIP('Validate ESN Before Rapid Job Update'),VALUE('YES','NO')
                             CHECK('Validate Accessories At Completion'),AT(328,180),USE(tmp:AccCheckAtComp),TRN,MSG('Force Accessory Check At Completion'),TIP('Force Accessory Check At Completion'),VALUE('1','0')
                           END
                           CHECK('Replicate Models to All Stock Locations'),AT(508,176),USE(tmp:ReplicateModelsToLocations)
                           GROUP('Despatch Defaults'),AT(252,86,216,72),USE(?Group4),BOXED
                             CHECK('Validate Accessories At Despatch'),AT(328,94),USE(def:Force_Accessory_Check),TRN,VALUE('YES','NO')
                             CHECK('Validate I.M.E.I. Number At Despatch'),AT(328,106),USE(def:ValidateDesp),TRN,RIGHT,MSG('Validate ESN At Despatch'),TIP('Validate ESN At Despatch'),VALUE('YES','NO')
                             CHECK('Remove From Workshop On Despatch'),AT(328,118),USE(def:RemoveWorkshopDespatch),TRN,MSG('Remove Location On'),TIP('Remove Location On'),VALUE('1','0')
                             CHECK('Create Multiple Despatch CSV'),AT(328,142),USE(tmp:CreateMultipleDespatchCSV),TRN,MSG('Create Multiple Despatch CSV'),TIP('Create Multiple Despatch CSV'),VALUE('1','0')
                             CHECK('Show Payment Details On Despatch'),AT(328,130),USE(tmp:ShowPaymentDespatch),TRN,MSG('Show Payment Details On Despatch'),TIP('Show Payment Details On Despatch'),VALUE('1','0')
                           END
                           GROUP('Bouncers'),AT(252,16,216,40),USE(?BouncersGroup),BOXED
                             CHECK('Allow Bouncers'),AT(260,24,72,12),USE(def:Allow_Bouncer),VALUE('YES','NO')
                             CHECK('Do Not Allow "Live" Bouncers'),AT(352,24),USE(tmp:StopLiveBouncers),MSG('Do Not Allow "Live" Bouncers'),TIP('Stop booking of a job <13,10>when the bouncer <13,10>has not been completed.'),VALUE('1','0')
                             PROMPT('Bouncer Period'),AT(260,40),USE(?DEF:BouncerTime:Prompt),TRN
                             ENTRY(@n3),AT(332,40,64,10),USE(def:BouncerTime),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Bouncer Time'),TIP('Period, in days, for a job to be a bouncer.'),UPR
                             PROMPT('(Days)'),AT(400,40),USE(?Days)
                           END
                           OPTION('Job Progress Address Display Type'),AT(252,56,216,28),USE(def:Browse_Option),BOXED,MSG('Which Address To Show On Main Browse')
                             RADIO('Invoice Add.'),AT(256,68),USE(?DEF:Browse_Option:Radio1),TRN,VALUE('INV')
                             RADIO('Collection Add.'),AT(328,68),USE(?DEF:Browse_Option:Radio2),TRN,VALUE('COL')
                             RADIO('Delivery Add.'),AT(404,68),USE(?DEF:Browse_Option:Radio3),TRN,VALUE('DEL')
                           END
                           CHECK('Summary Parts Orders'),AT(328,292),USE(def:SummaryOrders),HIDE,MSG('Summary Parts Orders'),TIP('Summary Parts Orders'),VALUE('1','0')
                           CHECK('Force Skill Levels For Engineers'),AT(328,208),USE(de2:UserSkillLevel),TRN,MSG('Allocate Skill Level To Users'),TIP('Allocate Skill Level To Users'),VALUE('1','0')
                           CHECK('Normal'),AT(508,204),USE(def:PickNoteNormal),VALUE('1','0')
                           CHECK('Password Required To Allocate Engineer'),AT(328,220),USE(de2:AllocateEngPassword),TRN,MSG('Password Required To Allocate Engineer'),TIP('Password Required To Allocate Engineer'),VALUE('1','0')
                           CHECK('Extended(Main Store)'),AT(508,216),USE(def:PickNoteMainStore),VALUE('1','0')
                           CHECK('Force Estimates To Be Accepted/Rejected'),AT(328,232),USE(tmp:ForceEstimateAccepted),TRN,MSG('Force Estimate To Be Accepted/Rejected'),TIP('Force Estimate To Be Accepted/Rejected'),VALUE('1','0')
                           CHECK('Force Fault Look-up'),AT(328,244),USE(tmp:force_fault_desc),TRN,VALUE('1','0')
                           CHECK('Pick Note Change Job Status to :-'),AT(508,240),USE(def:PickNoteChangeStatus),VALUE('1','0')
                           OPTION('Picking Notes'),AT(480,192,172,40),USE(?Picking),BOXED
                           END
                           CHECK('Rename Colour Field'),AT(328,256),USE(tmp:RenameColour),TRN,MSG('Rename Colour'),TIP('Rename Colour'),VALUE('1','0')
                           LIST,AT(508,252,124,10),USE(def:PickNoteJobStatus),DISABLE,FORMAT('120L|F@s30@'),DROP(15),FROM(Queue:FileDrop)
                           CHECK('Validate Mobile Number'),AT(24,252),USE(tmp:ValidateMobileNumber),TRN
                           CHECK('Lock InWorkshop'),AT(24,240),USE(tmp:LockInWorkshopDate),TRN,VALUE('1','0')
                           PROMPT('Colour Field Name'),AT(252,268),USE(?tmp:ColourFieldName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(328,268,124,10),USE(tmp:ColourFieldName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Colour Field Name'),TIP('Colour Field Name'),CAP
                           CHECK('Move "Job Cancelled" To Repair Type'),AT(24,276),USE(tmp:RepairTypeCancelledJob),TRN
                           CHECK('Exclude Cancelled Jobs from EDI / Invoicing'),AT(24,264),USE(tmp:ExcludeCancelledJobs),TRN
                           CHECK('Team Performance Ticker'),AT(508,272),USE(def:TeamPerformanceTicker),VALUE('1','0')
                           PROMPT('Refresh Rate'),AT(508,284),USE(?Prompt9)
                           SPIN(@n-7),AT(560,284,40,12),USE(def:TickerRefreshRate),RANGE(1,999),STEP(1)
                           CHECK('Record Exchange/Loan Reason'),AT(327,280),USE(tmp:RecordExchangeReason),MSG('Record Exchange/Loan Reason'),TIP('Record Exchange/Loan Reason'),VALUE('1','0')
                           CHECK('Disable MSN Requirement If Not In Workshop'),AT(24,288),USE(tmp:Disable_MSN_If_Not_InWorkshop),TRN
                           CHECK('Show Repair Type Costs'),AT(328,196),USE(def:ShowRepTypeCosts),TRN,MSG('Show Repair Type Costs'),TIP('Show Repair Type Costs'),VALUE('1','0')
                         END
                       END
                       BUTTON('&OK'),AT(544,312,56,16),USE(?OkButton),TRN,LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(600,312,56,16),USE(?CancelButton),TRN,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       CHECK('Remove Job Locking And Make Job Progress Screen View Only'),AT(24,314),USE(tmp:RemoveUnlock),TRN,HIDE,MSG('Remove Job Locking And Make Job Progress Screen View Only'),TIP('Remove Job Locking And Make Job Progress Screen View Only'),VALUE('1','0')
                       PANEL,AT(4,308,656,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup10         SelectFileClass
FileLookup13         SelectFileClass
FDB3                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?MainDefaults{prop:FontColor} = -1
    ?MainDefaults{prop:Color} = 15066597
    ?Panel2{prop:Fill} = 15066597

    ?def:Use_Postcode{prop:Font,3} = -1
    ?def:Use_Postcode{prop:Color} = 15066597
    ?def:Use_Postcode{prop:Trn} = 0
    ?def:PostcodeDll{prop:Font,3} = -1
    ?def:PostcodeDll{prop:Color} = 15066597
    ?def:PostcodeDll{prop:Trn} = 0
    ?DEF:Postcode_Path:Prompt{prop:FontColor} = -1
    ?DEF:Postcode_Path:Prompt{prop:Color} = 15066597
    If ?def:Postcode_Path{prop:ReadOnly} = True
        ?def:Postcode_Path{prop:FontColor} = 65793
        ?def:Postcode_Path{prop:Color} = 15066597
    Elsif ?def:Postcode_Path{prop:Req} = True
        ?def:Postcode_Path{prop:FontColor} = 65793
        ?def:Postcode_Path{prop:Color} = 8454143
    Else ! If ?def:Postcode_Path{prop:Req} = True
        ?def:Postcode_Path{prop:FontColor} = 65793
        ?def:Postcode_Path{prop:Color} = 16777215
    End ! If ?def:Postcode_Path{prop:Req} = True
    ?def:Postcode_Path{prop:Trn} = 0
    ?def:Postcode_Path{prop:FontStyle} = font:Bold
    ?DEF:Estimate_If_Over:Prompt{prop:FontColor} = -1
    ?DEF:Estimate_If_Over:Prompt{prop:Color} = 15066597
    If ?def:Estimate_If_Over{prop:ReadOnly} = True
        ?def:Estimate_If_Over{prop:FontColor} = 65793
        ?def:Estimate_If_Over{prop:Color} = 15066597
    Elsif ?def:Estimate_If_Over{prop:Req} = True
        ?def:Estimate_If_Over{prop:FontColor} = 65793
        ?def:Estimate_If_Over{prop:Color} = 8454143
    Else ! If ?def:Estimate_If_Over{prop:Req} = True
        ?def:Estimate_If_Over{prop:FontColor} = 65793
        ?def:Estimate_If_Over{prop:Color} = 16777215
    End ! If ?def:Estimate_If_Over{prop:Req} = True
    ?def:Estimate_If_Over{prop:Trn} = 0
    ?def:Estimate_If_Over{prop:FontStyle} = font:Bold
    ?DEF:ExportPath:Prompt{prop:FontColor} = -1
    ?DEF:ExportPath:Prompt{prop:Color} = 15066597
    If ?def:ExportPath{prop:ReadOnly} = True
        ?def:ExportPath{prop:FontColor} = 65793
        ?def:ExportPath{prop:Color} = 15066597
    Elsif ?def:ExportPath{prop:Req} = True
        ?def:ExportPath{prop:FontColor} = 65793
        ?def:ExportPath{prop:Color} = 8454143
    Else ! If ?def:ExportPath{prop:Req} = True
        ?def:ExportPath{prop:FontColor} = 65793
        ?def:ExportPath{prop:Color} = 16777215
    End ! If ?def:ExportPath{prop:Req} = True
    ?def:ExportPath{prop:Trn} = 0
    ?def:ExportPath{prop:FontStyle} = font:Bold
    ?Autosearchgroup{prop:Font,3} = -1
    ?Autosearchgroup{prop:Color} = 15066597
    ?Autosearchgroup{prop:Trn} = 0
    ?def:Automatic_Replicate{prop:Font,3} = -1
    ?def:Automatic_Replicate{prop:Color} = 15066597
    ?def:Automatic_Replicate{prop:Trn} = 0
    ?DEF:Automatic_Replicate_Field:Prompt{prop:FontColor} = -1
    ?DEF:Automatic_Replicate_Field:Prompt{prop:Color} = 15066597
    If ?def:Automatic_Replicate_Field{prop:ReadOnly} = True
        ?def:Automatic_Replicate_Field{prop:FontColor} = 65793
        ?def:Automatic_Replicate_Field{prop:Color} = 15066597
    Elsif ?def:Automatic_Replicate_Field{prop:Req} = True
        ?def:Automatic_Replicate_Field{prop:FontColor} = 65793
        ?def:Automatic_Replicate_Field{prop:Color} = 8454143
    Else ! If ?def:Automatic_Replicate_Field{prop:Req} = True
        ?def:Automatic_Replicate_Field{prop:FontColor} = 65793
        ?def:Automatic_Replicate_Field{prop:Color} = 16777215
    End ! If ?def:Automatic_Replicate_Field{prop:Req} = True
    ?def:Automatic_Replicate_Field{prop:Trn} = 0
    ?def:Automatic_Replicate_Field{prop:FontStyle} = font:Bold
    ?Group6{prop:Font,3} = -1
    ?Group6{prop:Color} = 15066597
    ?Group6{prop:Trn} = 0
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?Panel3{prop:Fill} = 15066597

    If ?JSC:Maroon{prop:ReadOnly} = True
        ?JSC:Maroon{prop:FontColor} = 65793
        ?JSC:Maroon{prop:Color} = 15066597
    Elsif ?JSC:Maroon{prop:Req} = True
        ?JSC:Maroon{prop:FontColor} = 65793
        ?JSC:Maroon{prop:Color} = 8454143
    Else ! If ?JSC:Maroon{prop:Req} = True
        ?JSC:Maroon{prop:FontColor} = 65793
        ?JSC:Maroon{prop:Color} = 16777215
    End ! If ?JSC:Maroon{prop:Req} = True
    ?JSC:Maroon{prop:Trn} = 0
    ?JSC:Maroon{prop:FontStyle} = font:Bold
    ?JSC:MaroonAction{prop:Font,3} = -1
    ?JSC:MaroonAction{prop:Color} = 15066597
    ?JSC:MaroonAction{prop:Trn} = 0
    ?Panel4{prop:Fill} = 15066597

    If ?JSC:Green{prop:ReadOnly} = True
        ?JSC:Green{prop:FontColor} = 65793
        ?JSC:Green{prop:Color} = 15066597
    Elsif ?JSC:Green{prop:Req} = True
        ?JSC:Green{prop:FontColor} = 65793
        ?JSC:Green{prop:Color} = 8454143
    Else ! If ?JSC:Green{prop:Req} = True
        ?JSC:Green{prop:FontColor} = 65793
        ?JSC:Green{prop:Color} = 16777215
    End ! If ?JSC:Green{prop:Req} = True
    ?JSC:Green{prop:Trn} = 0
    ?JSC:Green{prop:FontStyle} = font:Bold
    ?JSC:GreenAction{prop:Font,3} = -1
    ?JSC:GreenAction{prop:Color} = 15066597
    ?JSC:GreenAction{prop:Trn} = 0
    ?Panel5{prop:Fill} = 15066597

    If ?JSC:Olive{prop:ReadOnly} = True
        ?JSC:Olive{prop:FontColor} = 65793
        ?JSC:Olive{prop:Color} = 15066597
    Elsif ?JSC:Olive{prop:Req} = True
        ?JSC:Olive{prop:FontColor} = 65793
        ?JSC:Olive{prop:Color} = 8454143
    Else ! If ?JSC:Olive{prop:Req} = True
        ?JSC:Olive{prop:FontColor} = 65793
        ?JSC:Olive{prop:Color} = 16777215
    End ! If ?JSC:Olive{prop:Req} = True
    ?JSC:Olive{prop:Trn} = 0
    ?JSC:Olive{prop:FontStyle} = font:Bold
    ?JSC:OliveAction{prop:Font,3} = -1
    ?JSC:OliveAction{prop:Color} = 15066597
    ?JSC:OliveAction{prop:Trn} = 0
    ?Panel6{prop:Fill} = 15066597

    If ?JSC:Navy{prop:ReadOnly} = True
        ?JSC:Navy{prop:FontColor} = 65793
        ?JSC:Navy{prop:Color} = 15066597
    Elsif ?JSC:Navy{prop:Req} = True
        ?JSC:Navy{prop:FontColor} = 65793
        ?JSC:Navy{prop:Color} = 8454143
    Else ! If ?JSC:Navy{prop:Req} = True
        ?JSC:Navy{prop:FontColor} = 65793
        ?JSC:Navy{prop:Color} = 16777215
    End ! If ?JSC:Navy{prop:Req} = True
    ?JSC:Navy{prop:Trn} = 0
    ?JSC:Navy{prop:FontStyle} = font:Bold
    ?JSC:NavyAction{prop:Font,3} = -1
    ?JSC:NavyAction{prop:Color} = 15066597
    ?JSC:NavyAction{prop:Trn} = 0
    ?Panel7{prop:Fill} = 15066597

    If ?JSC:Purple{prop:ReadOnly} = True
        ?JSC:Purple{prop:FontColor} = 65793
        ?JSC:Purple{prop:Color} = 15066597
    Elsif ?JSC:Purple{prop:Req} = True
        ?JSC:Purple{prop:FontColor} = 65793
        ?JSC:Purple{prop:Color} = 8454143
    Else ! If ?JSC:Purple{prop:Req} = True
        ?JSC:Purple{prop:FontColor} = 65793
        ?JSC:Purple{prop:Color} = 16777215
    End ! If ?JSC:Purple{prop:Req} = True
    ?JSC:Purple{prop:Trn} = 0
    ?JSC:Purple{prop:FontStyle} = font:Bold
    ?JSC:PurpleAction{prop:Font,3} = -1
    ?JSC:PurpleAction{prop:Color} = 15066597
    ?JSC:PurpleAction{prop:Trn} = 0
    ?Panel8{prop:Fill} = 15066597

    ?JSC:TealAction{prop:Font,3} = -1
    ?JSC:TealAction{prop:Color} = 15066597
    ?JSC:TealAction{prop:Trn} = 0
    If ?JSC:Teal{prop:ReadOnly} = True
        ?JSC:Teal{prop:FontColor} = 65793
        ?JSC:Teal{prop:Color} = 15066597
    Elsif ?JSC:Teal{prop:Req} = True
        ?JSC:Teal{prop:FontColor} = 65793
        ?JSC:Teal{prop:Color} = 8454143
    Else ! If ?JSC:Teal{prop:Req} = True
        ?JSC:Teal{prop:FontColor} = 65793
        ?JSC:Teal{prop:Color} = 16777215
    End ! If ?JSC:Teal{prop:Req} = True
    ?JSC:Teal{prop:Trn} = 0
    ?JSC:Teal{prop:FontStyle} = font:Bold
    ?Panel9{prop:Fill} = 15066597

    If ?JSC:Gray{prop:ReadOnly} = True
        ?JSC:Gray{prop:FontColor} = 65793
        ?JSC:Gray{prop:Color} = 15066597
    Elsif ?JSC:Gray{prop:Req} = True
        ?JSC:Gray{prop:FontColor} = 65793
        ?JSC:Gray{prop:Color} = 8454143
    Else ! If ?JSC:Gray{prop:Req} = True
        ?JSC:Gray{prop:FontColor} = 65793
        ?JSC:Gray{prop:Color} = 16777215
    End ! If ?JSC:Gray{prop:Req} = True
    ?JSC:Gray{prop:Trn} = 0
    ?JSC:Gray{prop:FontStyle} = font:Bold
    ?JSC:GrayAction{prop:Font,3} = -1
    ?JSC:GrayAction{prop:Color} = 15066597
    ?JSC:GrayAction{prop:Trn} = 0
    ?Panel10{prop:Fill} = 15066597

    If ?JSC:Blue{prop:ReadOnly} = True
        ?JSC:Blue{prop:FontColor} = 65793
        ?JSC:Blue{prop:Color} = 15066597
    Elsif ?JSC:Blue{prop:Req} = True
        ?JSC:Blue{prop:FontColor} = 65793
        ?JSC:Blue{prop:Color} = 8454143
    Else ! If ?JSC:Blue{prop:Req} = True
        ?JSC:Blue{prop:FontColor} = 65793
        ?JSC:Blue{prop:Color} = 16777215
    End ! If ?JSC:Blue{prop:Req} = True
    ?JSC:Blue{prop:Trn} = 0
    ?JSC:Blue{prop:FontStyle} = font:Bold
    ?JSC:BlueAction{prop:Font,3} = -1
    ?JSC:BlueAction{prop:Color} = 15066597
    ?JSC:BlueAction{prop:Trn} = 0
    ?HideFieldsGroup{prop:Font,3} = -1
    ?HideFieldsGroup{prop:Color} = 15066597
    ?HideFieldsGroup{prop:Trn} = 0
    ?def:Show_Mobile_Number{prop:Font,3} = -1
    ?def:Show_Mobile_Number{prop:Color} = 15066597
    ?def:Show_Mobile_Number{prop:Trn} = 0
    ?def:Show_Loan_Exchange_Details{prop:Font,3} = -1
    ?def:Show_Loan_Exchange_Details{prop:Color} = 15066597
    ?def:Show_Loan_Exchange_Details{prop:Trn} = 0
    ?def:Hide_Physical_Damage{prop:Font,3} = -1
    ?def:Hide_Physical_Damage{prop:Color} = 15066597
    ?def:Hide_Physical_Damage{prop:Trn} = 0
    ?def:Hide_Insurance{prop:Font,3} = -1
    ?def:Hide_Insurance{prop:Color} = 15066597
    ?def:Hide_Insurance{prop:Trn} = 0
    ?def:Hide_Authority_Number{prop:Font,3} = -1
    ?def:Hide_Authority_Number{prop:Color} = 15066597
    ?def:Hide_Authority_Number{prop:Trn} = 0
    ?def:HideColour{prop:Font,3} = -1
    ?def:HideColour{prop:Color} = 15066597
    ?def:HideColour{prop:Trn} = 0
    ?def:HideInCourier{prop:Font,3} = -1
    ?def:HideInCourier{prop:Color} = 15066597
    ?def:HideInCourier{prop:Trn} = 0
    ?def:HideIntFault{prop:Font,3} = -1
    ?def:HideIntFault{prop:Color} = 15066597
    ?def:HideIntFault{prop:Trn} = 0
    ?def:HideProduct{prop:Font,3} = -1
    ?def:HideProduct{prop:Color} = 15066597
    ?def:HideProduct{prop:Trn} = 0
    ?def:HideLocation{prop:Font,3} = -1
    ?def:HideLocation{prop:Color} = 15066597
    ?def:HideLocation{prop:Trn} = 0
    ?def:QAPreliminary{prop:Font,3} = -1
    ?def:QAPreliminary{prop:Color} = 15066597
    ?def:QAPreliminary{prop:Trn} = 0
    ?tmp:HideSkillLevel{prop:Font,3} = -1
    ?tmp:HideSkillLevel{prop:Color} = 15066597
    ?tmp:HideSkillLevel{prop:Trn} = 0
    ?tmp:HideNetwork{prop:Font,3} = -1
    ?tmp:HideNetwork{prop:Color} = 15066597
    ?tmp:HideNetwork{prop:Trn} = 0
    ?Group5{prop:Font,3} = -1
    ?Group5{prop:Color} = 15066597
    ?Group5{prop:Trn} = 0
    ?def:ValidateESN{prop:Font,3} = -1
    ?def:ValidateESN{prop:Color} = 15066597
    ?def:ValidateESN{prop:Trn} = 0
    ?tmp:AccCheckAtComp{prop:Font,3} = -1
    ?tmp:AccCheckAtComp{prop:Color} = 15066597
    ?tmp:AccCheckAtComp{prop:Trn} = 0
    ?tmp:ReplicateModelsToLocations{prop:Font,3} = -1
    ?tmp:ReplicateModelsToLocations{prop:Color} = 15066597
    ?tmp:ReplicateModelsToLocations{prop:Trn} = 0
    ?Group4{prop:Font,3} = -1
    ?Group4{prop:Color} = 15066597
    ?Group4{prop:Trn} = 0
    ?def:Force_Accessory_Check{prop:Font,3} = -1
    ?def:Force_Accessory_Check{prop:Color} = 15066597
    ?def:Force_Accessory_Check{prop:Trn} = 0
    ?def:ValidateDesp{prop:Font,3} = -1
    ?def:ValidateDesp{prop:Color} = 15066597
    ?def:ValidateDesp{prop:Trn} = 0
    ?def:RemoveWorkshopDespatch{prop:Font,3} = -1
    ?def:RemoveWorkshopDespatch{prop:Color} = 15066597
    ?def:RemoveWorkshopDespatch{prop:Trn} = 0
    ?tmp:CreateMultipleDespatchCSV{prop:Font,3} = -1
    ?tmp:CreateMultipleDespatchCSV{prop:Color} = 15066597
    ?tmp:CreateMultipleDespatchCSV{prop:Trn} = 0
    ?tmp:ShowPaymentDespatch{prop:Font,3} = -1
    ?tmp:ShowPaymentDespatch{prop:Color} = 15066597
    ?tmp:ShowPaymentDespatch{prop:Trn} = 0
    ?BouncersGroup{prop:Font,3} = -1
    ?BouncersGroup{prop:Color} = 15066597
    ?BouncersGroup{prop:Trn} = 0
    ?def:Allow_Bouncer{prop:Font,3} = -1
    ?def:Allow_Bouncer{prop:Color} = 15066597
    ?def:Allow_Bouncer{prop:Trn} = 0
    ?tmp:StopLiveBouncers{prop:Font,3} = -1
    ?tmp:StopLiveBouncers{prop:Color} = 15066597
    ?tmp:StopLiveBouncers{prop:Trn} = 0
    ?DEF:BouncerTime:Prompt{prop:FontColor} = -1
    ?DEF:BouncerTime:Prompt{prop:Color} = 15066597
    If ?def:BouncerTime{prop:ReadOnly} = True
        ?def:BouncerTime{prop:FontColor} = 65793
        ?def:BouncerTime{prop:Color} = 15066597
    Elsif ?def:BouncerTime{prop:Req} = True
        ?def:BouncerTime{prop:FontColor} = 65793
        ?def:BouncerTime{prop:Color} = 8454143
    Else ! If ?def:BouncerTime{prop:Req} = True
        ?def:BouncerTime{prop:FontColor} = 65793
        ?def:BouncerTime{prop:Color} = 16777215
    End ! If ?def:BouncerTime{prop:Req} = True
    ?def:BouncerTime{prop:Trn} = 0
    ?def:BouncerTime{prop:FontStyle} = font:Bold
    ?Days{prop:FontColor} = -1
    ?Days{prop:Color} = 15066597
    ?def:Browse_Option{prop:Font,3} = -1
    ?def:Browse_Option{prop:Color} = 15066597
    ?def:Browse_Option{prop:Trn} = 0
    ?DEF:Browse_Option:Radio1{prop:Font,3} = -1
    ?DEF:Browse_Option:Radio1{prop:Color} = 15066597
    ?DEF:Browse_Option:Radio1{prop:Trn} = 0
    ?DEF:Browse_Option:Radio2{prop:Font,3} = -1
    ?DEF:Browse_Option:Radio2{prop:Color} = 15066597
    ?DEF:Browse_Option:Radio2{prop:Trn} = 0
    ?DEF:Browse_Option:Radio3{prop:Font,3} = -1
    ?DEF:Browse_Option:Radio3{prop:Color} = 15066597
    ?DEF:Browse_Option:Radio3{prop:Trn} = 0
    ?def:SummaryOrders{prop:Font,3} = -1
    ?def:SummaryOrders{prop:Color} = 15066597
    ?def:SummaryOrders{prop:Trn} = 0
    ?de2:UserSkillLevel{prop:Font,3} = -1
    ?de2:UserSkillLevel{prop:Color} = 15066597
    ?de2:UserSkillLevel{prop:Trn} = 0
    ?def:PickNoteNormal{prop:Font,3} = -1
    ?def:PickNoteNormal{prop:Color} = 15066597
    ?def:PickNoteNormal{prop:Trn} = 0
    ?de2:AllocateEngPassword{prop:Font,3} = -1
    ?de2:AllocateEngPassword{prop:Color} = 15066597
    ?de2:AllocateEngPassword{prop:Trn} = 0
    ?def:PickNoteMainStore{prop:Font,3} = -1
    ?def:PickNoteMainStore{prop:Color} = 15066597
    ?def:PickNoteMainStore{prop:Trn} = 0
    ?tmp:ForceEstimateAccepted{prop:Font,3} = -1
    ?tmp:ForceEstimateAccepted{prop:Color} = 15066597
    ?tmp:ForceEstimateAccepted{prop:Trn} = 0
    ?tmp:force_fault_desc{prop:Font,3} = -1
    ?tmp:force_fault_desc{prop:Color} = 15066597
    ?tmp:force_fault_desc{prop:Trn} = 0
    ?def:PickNoteChangeStatus{prop:Font,3} = -1
    ?def:PickNoteChangeStatus{prop:Color} = 15066597
    ?def:PickNoteChangeStatus{prop:Trn} = 0
    ?Picking{prop:Font,3} = -1
    ?Picking{prop:Color} = 15066597
    ?Picking{prop:Trn} = 0
    ?tmp:RenameColour{prop:Font,3} = -1
    ?tmp:RenameColour{prop:Color} = 15066597
    ?tmp:RenameColour{prop:Trn} = 0
    ?def:PickNoteJobStatus{prop:FontColor} = 65793
    ?def:PickNoteJobStatus{prop:Color}= 16777215
    ?def:PickNoteJobStatus{prop:Color,2} = 16777215
    ?def:PickNoteJobStatus{prop:Color,3} = 12937777
    ?tmp:ValidateMobileNumber{prop:Font,3} = -1
    ?tmp:ValidateMobileNumber{prop:Color} = 15066597
    ?tmp:ValidateMobileNumber{prop:Trn} = 0
    ?tmp:LockInWorkshopDate{prop:Font,3} = -1
    ?tmp:LockInWorkshopDate{prop:Color} = 15066597
    ?tmp:LockInWorkshopDate{prop:Trn} = 0
    ?tmp:ColourFieldName:Prompt{prop:FontColor} = -1
    ?tmp:ColourFieldName:Prompt{prop:Color} = 15066597
    If ?tmp:ColourFieldName{prop:ReadOnly} = True
        ?tmp:ColourFieldName{prop:FontColor} = 65793
        ?tmp:ColourFieldName{prop:Color} = 15066597
    Elsif ?tmp:ColourFieldName{prop:Req} = True
        ?tmp:ColourFieldName{prop:FontColor} = 65793
        ?tmp:ColourFieldName{prop:Color} = 8454143
    Else ! If ?tmp:ColourFieldName{prop:Req} = True
        ?tmp:ColourFieldName{prop:FontColor} = 65793
        ?tmp:ColourFieldName{prop:Color} = 16777215
    End ! If ?tmp:ColourFieldName{prop:Req} = True
    ?tmp:ColourFieldName{prop:Trn} = 0
    ?tmp:ColourFieldName{prop:FontStyle} = font:Bold
    ?tmp:RepairTypeCancelledJob{prop:Font,3} = -1
    ?tmp:RepairTypeCancelledJob{prop:Color} = 15066597
    ?tmp:RepairTypeCancelledJob{prop:Trn} = 0
    ?tmp:ExcludeCancelledJobs{prop:Font,3} = -1
    ?tmp:ExcludeCancelledJobs{prop:Color} = 15066597
    ?tmp:ExcludeCancelledJobs{prop:Trn} = 0
    ?def:TeamPerformanceTicker{prop:Font,3} = -1
    ?def:TeamPerformanceTicker{prop:Color} = 15066597
    ?def:TeamPerformanceTicker{prop:Trn} = 0
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    If ?def:TickerRefreshRate{prop:ReadOnly} = True
        ?def:TickerRefreshRate{prop:FontColor} = 65793
        ?def:TickerRefreshRate{prop:Color} = 15066597
    Elsif ?def:TickerRefreshRate{prop:Req} = True
        ?def:TickerRefreshRate{prop:FontColor} = 65793
        ?def:TickerRefreshRate{prop:Color} = 8454143
    Else ! If ?def:TickerRefreshRate{prop:Req} = True
        ?def:TickerRefreshRate{prop:FontColor} = 65793
        ?def:TickerRefreshRate{prop:Color} = 16777215
    End ! If ?def:TickerRefreshRate{prop:Req} = True
    ?def:TickerRefreshRate{prop:Trn} = 0
    ?def:TickerRefreshRate{prop:FontStyle} = font:Bold
    ?tmp:RecordExchangeReason{prop:Font,3} = -1
    ?tmp:RecordExchangeReason{prop:Color} = 15066597
    ?tmp:RecordExchangeReason{prop:Trn} = 0
    ?tmp:Disable_MSN_If_Not_InWorkshop{prop:Font,3} = -1
    ?tmp:Disable_MSN_If_Not_InWorkshop{prop:Color} = 15066597
    ?tmp:Disable_MSN_If_Not_InWorkshop{prop:Trn} = 0
    ?def:ShowRepTypeCosts{prop:Font,3} = -1
    ?def:ShowRepTypeCosts{prop:Color} = 15066597
    ?def:ShowRepTypeCosts{prop:Trn} = 0
    ?tmp:RemoveUnlock{prop:Font,3} = -1
    ?tmp:RemoveUnlock{prop:Color} = 15066597
    ?tmp:RemoveUnlock{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('MainDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MainDefaults
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:STATUS.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  
  
  if securitycheck('PICK PART DEFAULTS') = level:benign
      enable(?def:PickNoteNormal)
      enable(?def:PickNoteMainStore)
      enable(?def:PickNoteChangeStatus)
      unhide(?def:PickNoteJobStatus)
  else!if securitycheck('PICK PART DEFAULTS') = level:benign
      disable(?def:PickNoteNormal)
      disable(?def:PickNoteMainStore)
      disable(?def:PickNoteChangeStatus)
      hide(?def:PickNoteJobStatus)
  end!if securitycheck('PICK PART DEFAULTS') = level:benign
  
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  
  Set(DEFAULT2)
  If Access:DEFAULT2.Next()
      If Access:DEFAULT2.PrimeRecord() = Level:Benign
          If Access:DEFAULT2.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:DEFAULT2.TryInsert() = Level:Benign
              !Insert Failed
          End !If Access:DEFAULT2.TryInsert() = Level:Benign
      End !If Access:DEFAULT2.PrimeRecord() = Level:Benign
  End !Access:DEFAULT2.Next()
  
  tmp:RenameColour = Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:ColourFieldName = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:AccCheckAtComp = Clip(GETINI('VALIDATE','ForceAccCheckComp',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:StopLiveBouncers = Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:HideSkillLevel = Clip(GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:ForceEstimateAccepted = Clip(GETINI('ESTIMATE','ForceAccepted',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:HideNetwork = GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ShowPaymentDespatch = GETINI('DESPATCH','ShowPaymentDetails',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:force_fault_desc = Clip(GETINI('FAULT_DESC','ForceFaultDesc',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:RecordExchangeReason = GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI')
  ! Start Change 2471 BE(14/04/03)
  tmp:LockInWorkshopDate = GETINI('THIRDPARTY','LockInWorkshopDate',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2471 BE(14/04/03)
  
  If tmp:RenameColour and tmp:ColourFieldName <> ''
      ?def:HideColour{prop:Text} = Clip(tmp:ColourFieldName)
  End !tmp:RenameColour and tmp:ColourFieldName <> ''
  
  ! Start Change 2821 BE(24/06/03)
  tmp:ValidateMobileNumber = GETINI('VALIDATE','MobileNumber',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2821 BE(24/06/03)
  
  ! Start Change 2905 BE(15/07/03)
  tmp:ExcludeCancelledJobs = GETINI('VALIDATE','ExcludeCancelledJobs',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2905 BE(15/07/03)
  
  ! Start Change 2941 BE(05/11/03)
  tmp:Disable_MSN_If_Not_InWorkshop = GETINI('VALIDATE','DisableMSNIfNotInWorkshop',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2941 BE(05/11/03)
  
  ! Start Change 2618 BE(05/08/03)
  JSC:Maroon = GETINI('JobStatusColours','Maroon',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Green = GETINI('JobStatusColours','Green',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Olive = GETINI('JobStatusColours','Olive',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Navy = GETINI('JobStatusColours','Navy',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Purple = GETINI('JobStatusColours','Purple',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Teal = GETINI('JobStatusColours','Teal',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Gray = GETINI('JobStatusColours','Gray',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Blue = GETINI('JobStatusColours','Blue',,CLIP(PATH())&'\SB2KDEF.INI')
  
  JSC:MaroonAction = GETINI('JobStatusColours','MaroonAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:GreenAction = GETINI('JobStatusColours','GreenAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:OliveAction = GETINI('JobStatusColours','OliveAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:NavyAction = GETINI('JobStatusColours','NavyAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:PurpleAction = GETINI('JobStatusColours','PurpleAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:TealAction = GETINI('JobStatusColours','TealAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:GrayAction = GETINI('JobStatusColours','GrayAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:BlueAction = GETINI('JobStatusColours','BlueAction',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2618 BE(05/08/03)
  
  ! Start Change 3240 BE(23/10/03)
  tmp:RepairTypeCancelledJob = GETINI('CancelledJobs','RepairType',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 3240 BE(23/10/03)
  
  ! Start Change 3317 BE(12/11/03)
  tmp:ReplicateModelsToLocations = GETINI('Stock','ReplicateModels',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 3317 BE(12/11/03)
  !Turn Multiple Despatch CSV on/off - TrkBs: 4996 (DBH: 07-12-2004)
  tmp:CreateMultipleDespatchCSV = GETINI('DESPATCH','CreateMultipleDespatchCSV',,CLIP(PATH())&'\SB2KDEF.INI')
  
  ! UTL: Remove Job Locking and make job view only (DBH: 02-11-2005)
  If FullAccess(glo:PassAccount,glo:Password) = 0
      ?tmp:RemoveUnlock{Prop:Hide} = False
  End ! If FullAccess(glo:PassAccount,glo:Password) = 0
  tmp:RemoveUnlock = GETINI('DEFAULTS','View',,Clip(Path()) & '\SB2KDEF.INI')
  
  
  
  Do RecolourWindow
  ?def:PickNoteJobStatus{prop:vcr} = TRUE
  ! support for CPCS
  IF ?def:Use_Postcode{Prop:Checked} = True
    UNHIDE(?DEF:PostcodeDll)
  END
  IF ?def:Use_Postcode{Prop:Checked} = False
    HIDE(?DEF:PostcodeDll)
  END
  IF ?def:Allow_Bouncer{Prop:Checked} = True
    UNHIDE(?tmp:StopLiveBouncers)
  END
  IF ?def:Allow_Bouncer{Prop:Checked} = False
    HIDE(?tmp:StopLiveBouncers)
  END
  IF ?def:PickNoteChangeStatus{Prop:Checked} = True
    ENABLE(?def:PickNoteJobStatus)
  END
  IF ?def:PickNoteChangeStatus{Prop:Checked} = False
    DISABLE(?def:PickNoteJobStatus)
  END
  IF ?tmp:RenameColour{Prop:Checked} = True
    UNHIDE(?tmp:ColourFieldName:Prompt)
    UNHIDE(?tmp:ColourFieldName)
  END
  IF ?tmp:RenameColour{Prop:Checked} = False
    HIDE(?tmp:ColourFieldName:Prompt)
    HIDE(?tmp:ColourFieldName)
  END
  FileLookup10.Init
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:LongName)
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:Directory)
  FileLookup10.SetMask('All Files','*.*')
  FileLookup10.WindowTitle='Select Folder For Postcode Program'
  FileLookup13.Init
  FileLookup13.Flags=BOR(FileLookup13.Flags,FILE:LongName)
  FileLookup13.Flags=BOR(FileLookup13.Flags,FILE:Directory)
  FileLookup13.SetMask('All Files','*.*')
  FileLookup13.WindowTitle='Default Export Path'
  FDB3.Init(?def:PickNoteJobStatus,Queue:FileDrop.ViewPosition,FDB3::View:FileDrop,Queue:FileDrop,Relate:STATUS,ThisWindow)
  FDB3.Q &= Queue:FileDrop
  FDB3.AddSortOrder(sts:Status_Key)
  FDB3.AddField(sts:Status,FDB3.Q.sts:Status)
  FDB3.AddField(sts:Ref_Number,FDB3.Q.sts:Ref_Number)
  FDB3.AddUpdateField(sts:Status,def:PickNoteJobStatus)
  ThisWindow.AddItem(FDB3.WindowComponent)
  FDB3.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:STATUS.Close
    Relate:USERS_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?def:Use_Postcode
      IF ?def:Use_Postcode{Prop:Checked} = True
        UNHIDE(?DEF:PostcodeDll)
      END
      IF ?def:Use_Postcode{Prop:Checked} = False
        HIDE(?DEF:PostcodeDll)
      END
      ThisWindow.Reset
    OF ?Lookup_Postcode_Path
      ThisWindow.Update
      def:Postcode_Path = Upper(FileLookup10.Ask(1)  )
      DISPLAY
    OF ?def:ExportPath
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:ExportPath, Accepted)
      pl# = LEN(CLIP(def:ExportPath))
      IF (def:ExportPath[pl# : pl#] = '\') THEN
          def:ExportPath =  def:ExportPath[1 : pl#-1]
          DISPLAY(?def:ExportPath)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:ExportPath, Accepted)
    OF ?LookupExportPath
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupExportPath, Accepted)
      def:ExportPath = Upper(FileLookup13.Ask(1)  )
      DISPLAY
      ! Start Change BE023 BE(16/12//2003)
      POST(Event:Accepted, ?def:ExportPath)
      ! End Change BE023 BE(16/12//2003)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupExportPath, Accepted)
    OF ?PickMaroonButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickMaroonButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      GlobalRequest = SelectRecord                   ! Set Action for Lookup
      PickJobStatus                                  ! Call the Lookup Procedure
      IF (GlobalResponse = RequestCompleted) THEN           ! IF Lookup completed
          JSC:Maroon = sts:Status                      ! Source on Completion
          DISPLAY(?JSC:Maroon)
      END                                            ! END (IF Lookup completed)
      GlobalResponse = RequestCancelled              ! Clear Result
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickMaroonButton, Accepted)
    OF ?ClearMaroonButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearMaroonButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      JSC:Maroon = ''
      DISPLAY(?JSC:Maroon)
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearMaroonButton, Accepted)
    OF ?PickGreenButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickGreenButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      GlobalRequest = SelectRecord                   ! Set Action for Lookup
      PickJobStatus                                  ! Call the Lookup Procedure
      IF (GlobalResponse = RequestCompleted) THEN           ! IF Lookup completed
          JSC:Green = sts:Status                      ! Source on Completion
          DISPLAY(?JSC:Green)
      END                                            ! END (IF Lookup completed)
      GlobalResponse = RequestCancelled              ! Clear Result
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickGreenButton, Accepted)
    OF ?ClearGreenButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearGreenButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      JSC:Green = ''
      DISPLAY(?JSC:Green)
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearGreenButton, Accepted)
    OF ?PickOliveButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickOliveButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      GlobalRequest = SelectRecord                   ! Set Action for Lookup
      PickJobStatus                                  ! Call the Lookup Procedure
      IF (GlobalResponse = RequestCompleted) THEN           ! IF Lookup completed
          JSC:Olive = sts:Status                      ! Source on Completion
          DISPLAY(?JSC:Olive)
      END                                            ! END (IF Lookup completed)
      GlobalResponse = RequestCancelled              ! Clear Result
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickOliveButton, Accepted)
    OF ?ClearOliveButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearOliveButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      JSC:Olive = ''
      DISPLAY(?JSC:Olive)
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearOliveButton, Accepted)
    OF ?PickNavyButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickNavyButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      GlobalRequest = SelectRecord                   ! Set Action for Lookup
      PickJobStatus                                  ! Call the Lookup Procedure
      IF (GlobalResponse = RequestCompleted) THEN           ! IF Lookup completed
          JSC:Navy = sts:Status                      ! Source on Completion
          DISPLAY(?JSC:Navy)
      END                                            ! END (IF Lookup completed)
      GlobalResponse = RequestCancelled              ! Clear Result
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickNavyButton, Accepted)
    OF ?ClearNavyButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearNavyButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      JSC:Navy = ''
      DISPLAY(?JSC:Navy)
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearNavyButton, Accepted)
    OF ?PickPurpleButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickPurpleButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      GlobalRequest = SelectRecord                   ! Set Action for Lookup
      PickJobStatus                                  ! Call the Lookup Procedure
      IF (GlobalResponse = RequestCompleted) THEN           ! IF Lookup completed
          JSC:Purple = sts:Status                      ! Source on Completion
          DISPLAY(?JSC:Purple)
      END                                            ! END (IF Lookup completed)
      GlobalResponse = RequestCancelled              ! Clear Result
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickPurpleButton, Accepted)
    OF ?ClearPurpleButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearPurpleButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      JSC:Purple = ''
      DISPLAY(?JSC:Purple)
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearPurpleButton, Accepted)
    OF ?PickTealButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickTealButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      GlobalRequest = SelectRecord                   ! Set Action for Lookup
      PickJobStatus                                  ! Call the Lookup Procedure
      IF (GlobalResponse = RequestCompleted) THEN           ! IF Lookup completed
          JSC:Teal = sts:Status                      ! Source on Completion
          DISPLAY(?JSC:Teal)
      END                                            ! END (IF Lookup completed)
      GlobalResponse = RequestCancelled              ! Clear Result
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickTealButton, Accepted)
    OF ?ClearTealButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearTealButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      JSC:Teal = ''
      DISPLAY(?JSC:Teal)
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearTealButton, Accepted)
    OF ?PickGrayButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickGrayButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      GlobalRequest = SelectRecord                   ! Set Action for Lookup
      PickJobStatus                                  ! Call the Lookup Procedure
      IF (GlobalResponse = RequestCompleted) THEN           ! IF Lookup completed
          JSC:Gray = sts:Status                      ! Source on Completion
          DISPLAY(?JSC:Gray)
      END                                            ! END (IF Lookup completed)
      GlobalResponse = RequestCancelled              ! Clear Result
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickGrayButton, Accepted)
    OF ?ClearGrayButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearGrayButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      JSC:Gray = ''
      DISPLAY(?JSC:Gray)
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearGrayButton, Accepted)
    OF ?PickBlueButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickBlueButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      GlobalRequest = SelectRecord                   ! Set Action for Lookup
      PickJobStatus                                  ! Call the Lookup Procedure
      IF (GlobalResponse = RequestCompleted) THEN           ! IF Lookup completed
          JSC:Blue = sts:Status                      ! Source on Completion
          DISPLAY(?JSC:Blue)
      END                                            ! END (IF Lookup completed)
      GlobalResponse = RequestCancelled              ! Clear Result
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickBlueButton, Accepted)
    OF ?ClearBlueButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearBlueButton, Accepted)
      ! Start Change 2618 BE (05/08/03)
      JSC:Blue = ''
      DISPLAY(?JSC:Blue)
      ! End Change 2618 BE (05/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearBlueButton, Accepted)
    OF ?def:Allow_Bouncer
      IF ?def:Allow_Bouncer{Prop:Checked} = True
        UNHIDE(?tmp:StopLiveBouncers)
      END
      IF ?def:Allow_Bouncer{Prop:Checked} = False
        HIDE(?tmp:StopLiveBouncers)
      END
      ThisWindow.Reset
    OF ?def:PickNoteChangeStatus
      IF ?def:PickNoteChangeStatus{Prop:Checked} = True
        ENABLE(?def:PickNoteJobStatus)
      END
      IF ?def:PickNoteChangeStatus{Prop:Checked} = False
        DISABLE(?def:PickNoteJobStatus)
      END
      ThisWindow.Reset
    OF ?tmp:RenameColour
      IF ?tmp:RenameColour{Prop:Checked} = True
        UNHIDE(?tmp:ColourFieldName:Prompt)
        UNHIDE(?tmp:ColourFieldName)
      END
      IF ?tmp:RenameColour{Prop:Checked} = False
        HIDE(?tmp:ColourFieldName:Prompt)
        HIDE(?tmp:ColourFieldName)
      END
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defaults.update()
      access:default2.update()
      PUTINI('RENAME','RenameColour',tmp:RenameColour,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('RENAME','ColourName',tmp:ColourFieldName,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('VALIDATE','ForceAccCheckComp',tmp:AccCheckAtComp,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('BOUNCER','StopLive',tmp:StopLiveBouncers,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('HIDE','SkillLevel',tmp:HideSkillLevel,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('HIDE','HideNetwork',tmp:HideNetwork,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('ESTIMATE','ForceAccepted',tmp:ForceEstimateAccepted,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('DESPATCH','ShowPaymentDetails',tmp:ShowPaymentDespatch,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('FAULT_DESC','ForceFaultDesc',tmp:force_fault_desc,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('EXCHANGE','RecordReason',tmp:RecordExchangeReason,CLIP(PATH()) & '\SB2KDEF.INI')
      ! Start Change 2471 BE(14/04/03)
      PUTINI('THIRDPARTY','LockInWorkshopDate',tmp:LockInWorkshopDate,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 2471 BE(14/04/03)
      ! Start Change 2821 BE(24/06/03)
      PUTINI('VALIDATE','MobileNumber',tmp:ValidateMobileNumber,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 2821 BE(24/06/03)
      
      ! Start Change 2905 BE(15/07/03)
      PUTINI('VALIDATE','ExcludeCancelledJobs',tmp:ExcludeCancelledJobs,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 2905 BE(15/07/03)
      
      ! Start Change 2941 BE(05/11/03)
      PUTINI('VALIDATE','DisableMSNIfNotInWorkshop',tmp:Disable_MSN_If_Not_InWorkshop,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 2941 BE(05/11/03)
      
      ! Start Change 2618 BE(05/08/03)
      PUTINI('JobStatusColours','Maroon',JSC:Maroon,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','Green',JSC:Green,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','Olive',JSC:Olive,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','Navy',JSC:Navy,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','Purple',JSC:Purple,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','Teal',JSC:Teal,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','Gray',JSC:Gray,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','Blue',JSC:Blue,CLIP(PATH())&'\SB2KDEF.INI')
      
      PUTINI('JobStatusColours','MaroonAction',JSC:MaroonAction,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','GreenAction',JSC:GreenAction,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','OliveAction',JSC:OliveAction,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','NavyAction',JSC:NavyAction,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','PurpleAction',JSC:PurpleAction,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','TealAction',JSC:TealAction,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','GrayAction',JSC:GrayAction,CLIP(PATH())&'\SB2KDEF.INI')
      PUTINI('JobStatusColours','BlueAction',JSC:BlueAction,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 2618 BE(05/08/03)
      
      ! Start Change 3240 BE(23/10/03)
      PUTINI('CancelledJobs','RepairType',tmp:RepairTypeCancelledJob,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 3240 BE(23/10/03)
      
      ! Start Change 3317 BE(12/11/03)
      PUTINI('Stock','ReplicateModels',tmp:ReplicateModelsToLocations,CLIP(PATH())&'\SB2KDEF.INI')
      ! End Change 3317 BE(12/11/03)
      PUTINI('DESPATCH','CreateMultipleDespatchCSV',tmp:CreateMultipleDespatchCSV,CLIP(PATH())&'\SB2KDEF.INI')
      ! UTL: Remove job locking and make job read only (DBH: 02-11-2005)
      PUTINI('DEFAULTS','View',tmp:RemoveUnlock,Clip(Path()) & '\SB2KDEF.INI')
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
          ?def:SummaryOrders{prop:hide} = 0
      End !FullAccess(glo:PassAccount,glo:Password)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

