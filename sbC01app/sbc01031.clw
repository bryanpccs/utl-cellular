

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01031.INC'),ONCE        !Local module procedure declarations
                     END


CompulsoryFields PROCEDURE                            !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ForceAccessories STRING('0')
tmp:ForceNetwork     STRING(1)
window               WINDOW('General Defaults'),AT(,,239,339),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,232,304),USE(?Sheet1),SPREAD
                         TAB('&Compulsory Fields'),USE(?Tab5)
                           PROMPT('Ignore'),AT(88,24),USE(?Prompt26),TRN
                           PROMPT('Booking'),AT(140,24),USE(?Prompt27),TRN
                           PROMPT('Completion'),AT(192,24),USE(?Prompt28),TRN
                           OPTION,AT(80,192,148,12),USE(def:Force_Repair_Type),TRN
                             RADIO,AT(92,192),USE(?DEF:Force_Repair_Type:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,192),USE(?DEF:Force_Repair_Type:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,192),USE(?DEF:Force_Repair_Type:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Incoming Courier'),AT(8,216),USE(?Prompt43),TRN
                           OPTION,AT(80,204,148,12),USE(def:Force_Authority_Number),TRN
                             RADIO,AT(92,204),USE(?DEF:Force_Authority_Number:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,204),USE(?DEF:Force_Authority_Number:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,204),USE(?DEF:Force_Authority_Number:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(80,216,148,12),USE(def:Force_Incoming_Courier),TRN
                             RADIO,AT(92,216),USE(?DEF:Force_Incoming_Courier:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,216),USE(?DEF:Force_Incoming_Courier:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,216),USE(?DEF:Force_Incoming_Courier:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Outgoing Courier'),AT(8,228),USE(?Prompt43:2),TRN
                           OPTION,AT(80,144,148,12),USE(def:Customer_Name),TRN
                             RADIO,AT(92,144),USE(?DEF:Customer_Name:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,144),USE(?DEF:Customer_Name:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,144),USE(?DEF:Customer_Name:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(80,36,148,12),USE(def:Force_Initial_Transit_Type),TRN
                             RADIO,AT(92,36),USE(?DEF:Force_Initial_Transit_Type:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,36),USE(?DEF:Force_Initial_Transit_Type:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,36),USE(?DEF:Force_Initial_Transit_Type:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(80,264,148,12),USE(def:ForceDelPostcode),TRN
                             RADIO,AT(92,264),USE(?DEF:Force_Initial_Transit_Type:Radio1:2),TRN,VALUE('I')
                             RADIO,AT(144,264),USE(?DEF:Force_Initial_Transit_Type:Radio2:2),TRN,VALUE('B')
                             RADIO,AT(200,264),USE(?DEF:Force_Initial_Transit_Type:Radio3:2),TRN,VALUE('C')
                           END
                           PROMPT('Job Accessories'),AT(8,276),USE(?Prompt25:4),TRN
                           OPTION,AT(80,276,148,12),USE(tmp:ForceAccessories)
                             RADIO,AT(92,276),USE(?tmp:ForceAccessories:Radio46),VALUE('I')
                             RADIO,AT(144,276),USE(?tmp:ForceAccessories:Radio47),VALUE('B')
                             RADIO,AT(200,276),USE(?tmp:ForceAccessories:Radio48),VALUE('C')
                           END
                           PROMPT('Network'),AT(8,288),USE(?Prompt25:5),TRN
                           OPTION,AT(80,288,148,12),USE(tmp:ForceNetwork)
                             RADIO,AT(92,288),USE(?tmp:ForceAccessories:Radio46:2),VALUE('I')
                             RADIO,AT(144,288),USE(?tmp:ForceAccessories:Radio47:2),VALUE('B')
                             RADIO,AT(200,288),USE(?tmp:ForceAccessories:Radio48:2),VALUE('C')
                           END
                           PROMPT('Initial Transit Type'),AT(8,36),USE(?Prompt25),TRN
                           PROMPT('Delivery Postcode'),AT(8,264),USE(?Prompt25:3),TRN
                           PROMPT('Mobile Number'),AT(8,48),USE(?Prompt29),TRN
                           PROMPT('Model Number'),AT(8,60),USE(?Prompt30),TRN
                           OPTION,AT(80,60,148,12),USE(def:Force_Model_Number),TRN
                             RADIO,AT(92,60),USE(?DEF:Force_Model_Number:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,60),USE(?DEF:Force_Model_Number:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,60),USE(?DEF:Force_Model_Number:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(80,84,148,12),USE(def:Force_Fault_Description),TRN
                             RADIO,AT(92,84),USE(?DEF:Force_Fault_Description:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,84),USE(?DEF:Force_Fault_Description:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,84),USE(?DEF:Force_Fault_Description:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(80,240,148,12),USE(def:Force_Spares),TRN
                             RADIO,AT(92,240),USE(?DEF:Force_Spares:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,240),USE(?DEF:Force_Spares:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,240),USE(?DEF:Force_Spares:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Unit Type'),AT(8,72),USE(?Prompt31),TRN
                           PROMPT('Fault Description'),AT(8,84),USE(?Prompt32),TRN
                           PROMPT('I.M.E.I. Number'),AT(8,96),USE(?Prompt33),TRN
                           PROMPT('M.S.N.'),AT(8,108),USE(?Prompt34),TRN
                           PROMPT('Colour'),AT(8,120),USE(?Colour),TRN
                           OPTION,AT(80,48,148,12),USE(def:Force_Mobile_Number),TRN
                             RADIO,AT(92,48),USE(?DEF:Force_Mobile_Number:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,48),USE(?DEF:Force_Mobile_Number:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,48),USE(?DEF:Force_Mobile_Number:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Customer Postcode'),AT(8,252),USE(?Prompt25:2),TRN
                           OPTION,AT(80,252,148,12),USE(def:ForcePostcode)
                             RADIO,AT(92,252),USE(?DEF:ForcePostcode:Radio31),VALUE('I')
                             RADIO,AT(144,252),USE(?DEF:ForcePostcode:Radio32),VALUE('B')
                             RADIO,AT(200,252),USE(?DEF:ForcePostcode:Radio33),VALUE('C')
                           END
                           PROMPT('Job Type'),AT(8,156),USE(?Prompt35),TRN
                           OPTION,AT(80,96,148,12),USE(def:Force_ESN),TRN
                             RADIO,AT(92,96),USE(?DEF:Force_ESN:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,96),USE(?DEF:Force_ESN:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,96),USE(?DEF:Force_ESN:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Engineer'),AT(8,168),USE(?Prompt36),TRN
                           OPTION,AT(80,168,148,12),USE(def:Force_Engineer),TRN
                             RADIO,AT(92,168),USE(?DEF:Force_Engineer:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,168),USE(?DEF:Force_Engineer:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,168),USE(?DEF:Force_Engineer:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(80,228,148,12),USE(def:Force_Outoing_Courier)
                             RADIO,AT(92,228),USE(?DEF:Force_Outoing_Courier:Radio22),VALUE('I')
                             RADIO,AT(144,228),USE(?DEF:Force_Outoing_Courier:Radio23),VALUE('B')
                             RADIO,AT(200,228),USE(?DEF:Force_Outoing_Courier:Radio24),VALUE('C')
                           END
                           OPTION,AT(80,156,148,12),USE(def:Force_Job_Type),TRN
                             RADIO,AT(92,156),USE(?DEF:Force_Job_Type:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,156),USE(?DEF:Force_Job_Type:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,156),USE(?DEF:Force_Job_Type:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(80,72,148,12),USE(def:Force_Unit_Type),TRN
                             RADIO,AT(92,72),USE(?DEF:Force_Unit_Type:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,72),USE(?DEF:Force_Unit_Type:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,72),USE(?DEF:Force_Unit_Type:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Invoice Text'),AT(8,180),USE(?Prompt37),TRN
                           OPTION,AT(80,108,148,12),USE(def:Force_MSN),TRN
                             RADIO,AT(92,108),USE(?DEF:Force_MSN:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,108),USE(?DEF:Force_MSN:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,108),USE(?DEF:Force_MSN:Radio3),TRN,VALUE('C')
                           END
                           OPTION,AT(80,120,148,12),USE(def:ForceCommonFault),MSG('Force Common Fault')
                             RADIO,AT(92,120),USE(?def:ForceCommonFault:Radio10),VALUE('I')
                             RADIO,AT(144,120),USE(?def:ForceCommonFault:Radio11),VALUE('B')
                             RADIO,AT(200,120),USE(?def:ForceCommonFault:Radio12),VALUE('C')
                           END
                           PROMPT('Order Number'),AT(8,132),USE(?order_number:prompt),TRN
                           OPTION,AT(80,132,148,12),USE(def:Order_Number),TRN
                             RADIO,AT(92,132),USE(?DEF:Order_Number:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,132),USE(?DEF:Order_Number:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,132),USE(?DEF:Order_Number:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Customer Name'),AT(8,144),USE(?order_number:prompt:2),TRN
                           PROMPT('Repair Type'),AT(8,192),USE(?Prompt38),TRN
                           OPTION,AT(80,180,148,12),USE(def:Force_Invoice_Text),TRN
                             RADIO,AT(92,180),USE(?DEF:Force_Invoice_Text:Radio1),TRN,VALUE('I')
                             RADIO,AT(144,180),USE(?DEF:Force_Invoice_Text:Radio2),TRN,VALUE('B')
                             RADIO,AT(200,180),USE(?DEF:Force_Invoice_Text:Radio3),TRN,VALUE('C')
                           END
                           PROMPT('Authority Number'),AT(8,204),USE(?Prompt39),TRN
                           PROMPT('Spares'),AT(8,240),USE(?Prompt41),TRN
                         END
                       END
                       BUTTON('&OK'),AT(120,316,56,16),USE(?OkButton),TRN,LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(176,316,56,16),USE(?CancelButton),TRN,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,312,232,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab5{prop:Color} = 15066597
    ?Prompt26{prop:FontColor} = -1
    ?Prompt26{prop:Color} = 15066597
    ?Prompt27{prop:FontColor} = -1
    ?Prompt27{prop:Color} = 15066597
    ?Prompt28{prop:FontColor} = -1
    ?Prompt28{prop:Color} = 15066597
    ?def:Force_Repair_Type{prop:Font,3} = -1
    ?def:Force_Repair_Type{prop:Color} = 15066597
    ?def:Force_Repair_Type{prop:Trn} = 0
    ?DEF:Force_Repair_Type:Radio1{prop:Font,3} = -1
    ?DEF:Force_Repair_Type:Radio1{prop:Color} = 15066597
    ?DEF:Force_Repair_Type:Radio1{prop:Trn} = 0
    ?DEF:Force_Repair_Type:Radio2{prop:Font,3} = -1
    ?DEF:Force_Repair_Type:Radio2{prop:Color} = 15066597
    ?DEF:Force_Repair_Type:Radio2{prop:Trn} = 0
    ?DEF:Force_Repair_Type:Radio3{prop:Font,3} = -1
    ?DEF:Force_Repair_Type:Radio3{prop:Color} = 15066597
    ?DEF:Force_Repair_Type:Radio3{prop:Trn} = 0
    ?Prompt43{prop:FontColor} = -1
    ?Prompt43{prop:Color} = 15066597
    ?def:Force_Authority_Number{prop:Font,3} = -1
    ?def:Force_Authority_Number{prop:Color} = 15066597
    ?def:Force_Authority_Number{prop:Trn} = 0
    ?DEF:Force_Authority_Number:Radio1{prop:Font,3} = -1
    ?DEF:Force_Authority_Number:Radio1{prop:Color} = 15066597
    ?DEF:Force_Authority_Number:Radio1{prop:Trn} = 0
    ?DEF:Force_Authority_Number:Radio2{prop:Font,3} = -1
    ?DEF:Force_Authority_Number:Radio2{prop:Color} = 15066597
    ?DEF:Force_Authority_Number:Radio2{prop:Trn} = 0
    ?DEF:Force_Authority_Number:Radio3{prop:Font,3} = -1
    ?DEF:Force_Authority_Number:Radio3{prop:Color} = 15066597
    ?DEF:Force_Authority_Number:Radio3{prop:Trn} = 0
    ?def:Force_Incoming_Courier{prop:Font,3} = -1
    ?def:Force_Incoming_Courier{prop:Color} = 15066597
    ?def:Force_Incoming_Courier{prop:Trn} = 0
    ?DEF:Force_Incoming_Courier:Radio1{prop:Font,3} = -1
    ?DEF:Force_Incoming_Courier:Radio1{prop:Color} = 15066597
    ?DEF:Force_Incoming_Courier:Radio1{prop:Trn} = 0
    ?DEF:Force_Incoming_Courier:Radio2{prop:Font,3} = -1
    ?DEF:Force_Incoming_Courier:Radio2{prop:Color} = 15066597
    ?DEF:Force_Incoming_Courier:Radio2{prop:Trn} = 0
    ?DEF:Force_Incoming_Courier:Radio3{prop:Font,3} = -1
    ?DEF:Force_Incoming_Courier:Radio3{prop:Color} = 15066597
    ?DEF:Force_Incoming_Courier:Radio3{prop:Trn} = 0
    ?Prompt43:2{prop:FontColor} = -1
    ?Prompt43:2{prop:Color} = 15066597
    ?def:Customer_Name{prop:Font,3} = -1
    ?def:Customer_Name{prop:Color} = 15066597
    ?def:Customer_Name{prop:Trn} = 0
    ?DEF:Customer_Name:Radio1{prop:Font,3} = -1
    ?DEF:Customer_Name:Radio1{prop:Color} = 15066597
    ?DEF:Customer_Name:Radio1{prop:Trn} = 0
    ?DEF:Customer_Name:Radio2{prop:Font,3} = -1
    ?DEF:Customer_Name:Radio2{prop:Color} = 15066597
    ?DEF:Customer_Name:Radio2{prop:Trn} = 0
    ?DEF:Customer_Name:Radio3{prop:Font,3} = -1
    ?DEF:Customer_Name:Radio3{prop:Color} = 15066597
    ?DEF:Customer_Name:Radio3{prop:Trn} = 0
    ?def:Force_Initial_Transit_Type{prop:Font,3} = -1
    ?def:Force_Initial_Transit_Type{prop:Color} = 15066597
    ?def:Force_Initial_Transit_Type{prop:Trn} = 0
    ?DEF:Force_Initial_Transit_Type:Radio1{prop:Font,3} = -1
    ?DEF:Force_Initial_Transit_Type:Radio1{prop:Color} = 15066597
    ?DEF:Force_Initial_Transit_Type:Radio1{prop:Trn} = 0
    ?DEF:Force_Initial_Transit_Type:Radio2{prop:Font,3} = -1
    ?DEF:Force_Initial_Transit_Type:Radio2{prop:Color} = 15066597
    ?DEF:Force_Initial_Transit_Type:Radio2{prop:Trn} = 0
    ?DEF:Force_Initial_Transit_Type:Radio3{prop:Font,3} = -1
    ?DEF:Force_Initial_Transit_Type:Radio3{prop:Color} = 15066597
    ?DEF:Force_Initial_Transit_Type:Radio3{prop:Trn} = 0
    ?def:ForceDelPostcode{prop:Font,3} = -1
    ?def:ForceDelPostcode{prop:Color} = 15066597
    ?def:ForceDelPostcode{prop:Trn} = 0
    ?DEF:Force_Initial_Transit_Type:Radio1:2{prop:Font,3} = -1
    ?DEF:Force_Initial_Transit_Type:Radio1:2{prop:Color} = 15066597
    ?DEF:Force_Initial_Transit_Type:Radio1:2{prop:Trn} = 0
    ?DEF:Force_Initial_Transit_Type:Radio2:2{prop:Font,3} = -1
    ?DEF:Force_Initial_Transit_Type:Radio2:2{prop:Color} = 15066597
    ?DEF:Force_Initial_Transit_Type:Radio2:2{prop:Trn} = 0
    ?DEF:Force_Initial_Transit_Type:Radio3:2{prop:Font,3} = -1
    ?DEF:Force_Initial_Transit_Type:Radio3:2{prop:Color} = 15066597
    ?DEF:Force_Initial_Transit_Type:Radio3:2{prop:Trn} = 0
    ?Prompt25:4{prop:FontColor} = -1
    ?Prompt25:4{prop:Color} = 15066597
    ?tmp:ForceAccessories{prop:Font,3} = -1
    ?tmp:ForceAccessories{prop:Color} = 15066597
    ?tmp:ForceAccessories{prop:Trn} = 0
    ?tmp:ForceAccessories:Radio46{prop:Font,3} = -1
    ?tmp:ForceAccessories:Radio46{prop:Color} = 15066597
    ?tmp:ForceAccessories:Radio46{prop:Trn} = 0
    ?tmp:ForceAccessories:Radio47{prop:Font,3} = -1
    ?tmp:ForceAccessories:Radio47{prop:Color} = 15066597
    ?tmp:ForceAccessories:Radio47{prop:Trn} = 0
    ?tmp:ForceAccessories:Radio48{prop:Font,3} = -1
    ?tmp:ForceAccessories:Radio48{prop:Color} = 15066597
    ?tmp:ForceAccessories:Radio48{prop:Trn} = 0
    ?Prompt25:5{prop:FontColor} = -1
    ?Prompt25:5{prop:Color} = 15066597
    ?tmp:ForceNetwork{prop:Font,3} = -1
    ?tmp:ForceNetwork{prop:Color} = 15066597
    ?tmp:ForceNetwork{prop:Trn} = 0
    ?tmp:ForceAccessories:Radio46:2{prop:Font,3} = -1
    ?tmp:ForceAccessories:Radio46:2{prop:Color} = 15066597
    ?tmp:ForceAccessories:Radio46:2{prop:Trn} = 0
    ?tmp:ForceAccessories:Radio47:2{prop:Font,3} = -1
    ?tmp:ForceAccessories:Radio47:2{prop:Color} = 15066597
    ?tmp:ForceAccessories:Radio47:2{prop:Trn} = 0
    ?tmp:ForceAccessories:Radio48:2{prop:Font,3} = -1
    ?tmp:ForceAccessories:Radio48:2{prop:Color} = 15066597
    ?tmp:ForceAccessories:Radio48:2{prop:Trn} = 0
    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    ?Prompt25:3{prop:FontColor} = -1
    ?Prompt25:3{prop:Color} = 15066597
    ?Prompt29{prop:FontColor} = -1
    ?Prompt29{prop:Color} = 15066597
    ?Prompt30{prop:FontColor} = -1
    ?Prompt30{prop:Color} = 15066597
    ?def:Force_Model_Number{prop:Font,3} = -1
    ?def:Force_Model_Number{prop:Color} = 15066597
    ?def:Force_Model_Number{prop:Trn} = 0
    ?DEF:Force_Model_Number:Radio1{prop:Font,3} = -1
    ?DEF:Force_Model_Number:Radio1{prop:Color} = 15066597
    ?DEF:Force_Model_Number:Radio1{prop:Trn} = 0
    ?DEF:Force_Model_Number:Radio2{prop:Font,3} = -1
    ?DEF:Force_Model_Number:Radio2{prop:Color} = 15066597
    ?DEF:Force_Model_Number:Radio2{prop:Trn} = 0
    ?DEF:Force_Model_Number:Radio3{prop:Font,3} = -1
    ?DEF:Force_Model_Number:Radio3{prop:Color} = 15066597
    ?DEF:Force_Model_Number:Radio3{prop:Trn} = 0
    ?def:Force_Fault_Description{prop:Font,3} = -1
    ?def:Force_Fault_Description{prop:Color} = 15066597
    ?def:Force_Fault_Description{prop:Trn} = 0
    ?DEF:Force_Fault_Description:Radio1{prop:Font,3} = -1
    ?DEF:Force_Fault_Description:Radio1{prop:Color} = 15066597
    ?DEF:Force_Fault_Description:Radio1{prop:Trn} = 0
    ?DEF:Force_Fault_Description:Radio2{prop:Font,3} = -1
    ?DEF:Force_Fault_Description:Radio2{prop:Color} = 15066597
    ?DEF:Force_Fault_Description:Radio2{prop:Trn} = 0
    ?DEF:Force_Fault_Description:Radio3{prop:Font,3} = -1
    ?DEF:Force_Fault_Description:Radio3{prop:Color} = 15066597
    ?DEF:Force_Fault_Description:Radio3{prop:Trn} = 0
    ?def:Force_Spares{prop:Font,3} = -1
    ?def:Force_Spares{prop:Color} = 15066597
    ?def:Force_Spares{prop:Trn} = 0
    ?DEF:Force_Spares:Radio1{prop:Font,3} = -1
    ?DEF:Force_Spares:Radio1{prop:Color} = 15066597
    ?DEF:Force_Spares:Radio1{prop:Trn} = 0
    ?DEF:Force_Spares:Radio2{prop:Font,3} = -1
    ?DEF:Force_Spares:Radio2{prop:Color} = 15066597
    ?DEF:Force_Spares:Radio2{prop:Trn} = 0
    ?DEF:Force_Spares:Radio3{prop:Font,3} = -1
    ?DEF:Force_Spares:Radio3{prop:Color} = 15066597
    ?DEF:Force_Spares:Radio3{prop:Trn} = 0
    ?Prompt31{prop:FontColor} = -1
    ?Prompt31{prop:Color} = 15066597
    ?Prompt32{prop:FontColor} = -1
    ?Prompt32{prop:Color} = 15066597
    ?Prompt33{prop:FontColor} = -1
    ?Prompt33{prop:Color} = 15066597
    ?Prompt34{prop:FontColor} = -1
    ?Prompt34{prop:Color} = 15066597
    ?Colour{prop:FontColor} = -1
    ?Colour{prop:Color} = 15066597
    ?def:Force_Mobile_Number{prop:Font,3} = -1
    ?def:Force_Mobile_Number{prop:Color} = 15066597
    ?def:Force_Mobile_Number{prop:Trn} = 0
    ?DEF:Force_Mobile_Number:Radio1{prop:Font,3} = -1
    ?DEF:Force_Mobile_Number:Radio1{prop:Color} = 15066597
    ?DEF:Force_Mobile_Number:Radio1{prop:Trn} = 0
    ?DEF:Force_Mobile_Number:Radio2{prop:Font,3} = -1
    ?DEF:Force_Mobile_Number:Radio2{prop:Color} = 15066597
    ?DEF:Force_Mobile_Number:Radio2{prop:Trn} = 0
    ?DEF:Force_Mobile_Number:Radio3{prop:Font,3} = -1
    ?DEF:Force_Mobile_Number:Radio3{prop:Color} = 15066597
    ?DEF:Force_Mobile_Number:Radio3{prop:Trn} = 0
    ?Prompt25:2{prop:FontColor} = -1
    ?Prompt25:2{prop:Color} = 15066597
    ?def:ForcePostcode{prop:Font,3} = -1
    ?def:ForcePostcode{prop:Color} = 15066597
    ?def:ForcePostcode{prop:Trn} = 0
    ?DEF:ForcePostcode:Radio31{prop:Font,3} = -1
    ?DEF:ForcePostcode:Radio31{prop:Color} = 15066597
    ?DEF:ForcePostcode:Radio31{prop:Trn} = 0
    ?DEF:ForcePostcode:Radio32{prop:Font,3} = -1
    ?DEF:ForcePostcode:Radio32{prop:Color} = 15066597
    ?DEF:ForcePostcode:Radio32{prop:Trn} = 0
    ?DEF:ForcePostcode:Radio33{prop:Font,3} = -1
    ?DEF:ForcePostcode:Radio33{prop:Color} = 15066597
    ?DEF:ForcePostcode:Radio33{prop:Trn} = 0
    ?Prompt35{prop:FontColor} = -1
    ?Prompt35{prop:Color} = 15066597
    ?def:Force_ESN{prop:Font,3} = -1
    ?def:Force_ESN{prop:Color} = 15066597
    ?def:Force_ESN{prop:Trn} = 0
    ?DEF:Force_ESN:Radio1{prop:Font,3} = -1
    ?DEF:Force_ESN:Radio1{prop:Color} = 15066597
    ?DEF:Force_ESN:Radio1{prop:Trn} = 0
    ?DEF:Force_ESN:Radio2{prop:Font,3} = -1
    ?DEF:Force_ESN:Radio2{prop:Color} = 15066597
    ?DEF:Force_ESN:Radio2{prop:Trn} = 0
    ?DEF:Force_ESN:Radio3{prop:Font,3} = -1
    ?DEF:Force_ESN:Radio3{prop:Color} = 15066597
    ?DEF:Force_ESN:Radio3{prop:Trn} = 0
    ?Prompt36{prop:FontColor} = -1
    ?Prompt36{prop:Color} = 15066597
    ?def:Force_Engineer{prop:Font,3} = -1
    ?def:Force_Engineer{prop:Color} = 15066597
    ?def:Force_Engineer{prop:Trn} = 0
    ?DEF:Force_Engineer:Radio1{prop:Font,3} = -1
    ?DEF:Force_Engineer:Radio1{prop:Color} = 15066597
    ?DEF:Force_Engineer:Radio1{prop:Trn} = 0
    ?DEF:Force_Engineer:Radio2{prop:Font,3} = -1
    ?DEF:Force_Engineer:Radio2{prop:Color} = 15066597
    ?DEF:Force_Engineer:Radio2{prop:Trn} = 0
    ?DEF:Force_Engineer:Radio3{prop:Font,3} = -1
    ?DEF:Force_Engineer:Radio3{prop:Color} = 15066597
    ?DEF:Force_Engineer:Radio3{prop:Trn} = 0
    ?def:Force_Outoing_Courier{prop:Font,3} = -1
    ?def:Force_Outoing_Courier{prop:Color} = 15066597
    ?def:Force_Outoing_Courier{prop:Trn} = 0
    ?DEF:Force_Outoing_Courier:Radio22{prop:Font,3} = -1
    ?DEF:Force_Outoing_Courier:Radio22{prop:Color} = 15066597
    ?DEF:Force_Outoing_Courier:Radio22{prop:Trn} = 0
    ?DEF:Force_Outoing_Courier:Radio23{prop:Font,3} = -1
    ?DEF:Force_Outoing_Courier:Radio23{prop:Color} = 15066597
    ?DEF:Force_Outoing_Courier:Radio23{prop:Trn} = 0
    ?DEF:Force_Outoing_Courier:Radio24{prop:Font,3} = -1
    ?DEF:Force_Outoing_Courier:Radio24{prop:Color} = 15066597
    ?DEF:Force_Outoing_Courier:Radio24{prop:Trn} = 0
    ?def:Force_Job_Type{prop:Font,3} = -1
    ?def:Force_Job_Type{prop:Color} = 15066597
    ?def:Force_Job_Type{prop:Trn} = 0
    ?DEF:Force_Job_Type:Radio1{prop:Font,3} = -1
    ?DEF:Force_Job_Type:Radio1{prop:Color} = 15066597
    ?DEF:Force_Job_Type:Radio1{prop:Trn} = 0
    ?DEF:Force_Job_Type:Radio2{prop:Font,3} = -1
    ?DEF:Force_Job_Type:Radio2{prop:Color} = 15066597
    ?DEF:Force_Job_Type:Radio2{prop:Trn} = 0
    ?DEF:Force_Job_Type:Radio3{prop:Font,3} = -1
    ?DEF:Force_Job_Type:Radio3{prop:Color} = 15066597
    ?DEF:Force_Job_Type:Radio3{prop:Trn} = 0
    ?def:Force_Unit_Type{prop:Font,3} = -1
    ?def:Force_Unit_Type{prop:Color} = 15066597
    ?def:Force_Unit_Type{prop:Trn} = 0
    ?DEF:Force_Unit_Type:Radio1{prop:Font,3} = -1
    ?DEF:Force_Unit_Type:Radio1{prop:Color} = 15066597
    ?DEF:Force_Unit_Type:Radio1{prop:Trn} = 0
    ?DEF:Force_Unit_Type:Radio2{prop:Font,3} = -1
    ?DEF:Force_Unit_Type:Radio2{prop:Color} = 15066597
    ?DEF:Force_Unit_Type:Radio2{prop:Trn} = 0
    ?DEF:Force_Unit_Type:Radio3{prop:Font,3} = -1
    ?DEF:Force_Unit_Type:Radio3{prop:Color} = 15066597
    ?DEF:Force_Unit_Type:Radio3{prop:Trn} = 0
    ?Prompt37{prop:FontColor} = -1
    ?Prompt37{prop:Color} = 15066597
    ?def:Force_MSN{prop:Font,3} = -1
    ?def:Force_MSN{prop:Color} = 15066597
    ?def:Force_MSN{prop:Trn} = 0
    ?DEF:Force_MSN:Radio1{prop:Font,3} = -1
    ?DEF:Force_MSN:Radio1{prop:Color} = 15066597
    ?DEF:Force_MSN:Radio1{prop:Trn} = 0
    ?DEF:Force_MSN:Radio2{prop:Font,3} = -1
    ?DEF:Force_MSN:Radio2{prop:Color} = 15066597
    ?DEF:Force_MSN:Radio2{prop:Trn} = 0
    ?DEF:Force_MSN:Radio3{prop:Font,3} = -1
    ?DEF:Force_MSN:Radio3{prop:Color} = 15066597
    ?DEF:Force_MSN:Radio3{prop:Trn} = 0
    ?def:ForceCommonFault{prop:Font,3} = -1
    ?def:ForceCommonFault{prop:Color} = 15066597
    ?def:ForceCommonFault{prop:Trn} = 0
    ?def:ForceCommonFault:Radio10{prop:Font,3} = -1
    ?def:ForceCommonFault:Radio10{prop:Color} = 15066597
    ?def:ForceCommonFault:Radio10{prop:Trn} = 0
    ?def:ForceCommonFault:Radio11{prop:Font,3} = -1
    ?def:ForceCommonFault:Radio11{prop:Color} = 15066597
    ?def:ForceCommonFault:Radio11{prop:Trn} = 0
    ?def:ForceCommonFault:Radio12{prop:Font,3} = -1
    ?def:ForceCommonFault:Radio12{prop:Color} = 15066597
    ?def:ForceCommonFault:Radio12{prop:Trn} = 0
    ?order_number:prompt{prop:FontColor} = -1
    ?order_number:prompt{prop:Color} = 15066597
    ?def:Order_Number{prop:Font,3} = -1
    ?def:Order_Number{prop:Color} = 15066597
    ?def:Order_Number{prop:Trn} = 0
    ?DEF:Order_Number:Radio1{prop:Font,3} = -1
    ?DEF:Order_Number:Radio1{prop:Color} = 15066597
    ?DEF:Order_Number:Radio1{prop:Trn} = 0
    ?DEF:Order_Number:Radio2{prop:Font,3} = -1
    ?DEF:Order_Number:Radio2{prop:Color} = 15066597
    ?DEF:Order_Number:Radio2{prop:Trn} = 0
    ?DEF:Order_Number:Radio3{prop:Font,3} = -1
    ?DEF:Order_Number:Radio3{prop:Color} = 15066597
    ?DEF:Order_Number:Radio3{prop:Trn} = 0
    ?order_number:prompt:2{prop:FontColor} = -1
    ?order_number:prompt:2{prop:Color} = 15066597
    ?Prompt38{prop:FontColor} = -1
    ?Prompt38{prop:Color} = 15066597
    ?def:Force_Invoice_Text{prop:Font,3} = -1
    ?def:Force_Invoice_Text{prop:Color} = 15066597
    ?def:Force_Invoice_Text{prop:Trn} = 0
    ?DEF:Force_Invoice_Text:Radio1{prop:Font,3} = -1
    ?DEF:Force_Invoice_Text:Radio1{prop:Color} = 15066597
    ?DEF:Force_Invoice_Text:Radio1{prop:Trn} = 0
    ?DEF:Force_Invoice_Text:Radio2{prop:Font,3} = -1
    ?DEF:Force_Invoice_Text:Radio2{prop:Color} = 15066597
    ?DEF:Force_Invoice_Text:Radio2{prop:Trn} = 0
    ?DEF:Force_Invoice_Text:Radio3{prop:Font,3} = -1
    ?DEF:Force_Invoice_Text:Radio3{prop:Color} = 15066597
    ?DEF:Force_Invoice_Text:Radio3{prop:Trn} = 0
    ?Prompt39{prop:FontColor} = -1
    ?Prompt39{prop:Color} = 15066597
    ?Prompt41{prop:FontColor} = -1
    ?Prompt41{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CompulsoryFields')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt26
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  
  tmp:ForceAccessories = Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:ForceNetwork    =Clip(GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI'))
  
  If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      ?Colour{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
  End !Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
  
  
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defaults.update()
      PUTINI('COMPULSORY','JobAccessories',tmp:ForceAccessories,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('COMPULSORY','Network',tmp:ForceNetwork,CLIP(PATH()) & '\SB2KDEF.INI')
      
      
      
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

