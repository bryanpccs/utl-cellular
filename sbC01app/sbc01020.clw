

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01020.INC'),ONCE        !Local module procedure declarations
                     END


Sage_Defaults PROCEDURE                               !Generated from procedure template - Window

FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Sage Defaults'),AT(,,232,332),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('PC.ICO'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,296),USE(?Sheet1),SPREAD
                         TAB('Sage Defaults'),USE(?Tab1)
                           CHECK('Use Sage'),AT(84,20),USE(def:Use_Sage),VALUE('YES','NO')
                           GROUP,AT(4,28,220,268),USE(?Sage_Group)
                             PROMPT('User Name'),AT(8,36),USE(?DEF:User_Name_Sage:Prompt),TRN
                             ENTRY(@s30),AT(84,36,124,10),USE(def:User_Name_Sage),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Password'),AT(8,52),USE(?DEF:Password_Sage:Prompt),TRN
                             ENTRY(@s30),AT(84,52,124,10),USE(def:Password_Sage),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,PASSWORD
                             PROMPT('Sage Path'),AT(8,68),USE(?DEF:Path_Sage:Prompt)
                             ENTRY(@s255),AT(84,68,124,10),USE(def:Path_Sage),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             BUTTON,AT(212,68,10,10),USE(?Lookup_Sage_Path),LEFT,ICON('list3.ico')
                             PROMPT('Global Nominal Code'),AT(8,84),USE(?DEF:Global_Nominal_Code:Prompt)
                             ENTRY(@s30),AT(84,84,124,10),USE(def:Global_Nominal_Code),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             GROUP('Labour Details'),AT(8,96,212,64),USE(?Group1),BOXED
                               PROMPT('Labour Stock Code'),AT(16,108),USE(?DEF:Labour_Stock_Code:Prompt)
                               ENTRY(@s30),AT(84,108,124,10),USE(def:Labour_Stock_Code),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                               PROMPT('Labour Description'),AT(16,124),USE(?DEF:Labour_Description:Prompt)
                               ENTRY(@s60),AT(84,124,124,10),USE(def:Labour_Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                               PROMPT('Labour Code'),AT(16,140),USE(?DEF:Labour_Code:Prompt)
                               ENTRY(@s30),AT(84,140,124,10),USE(def:Labour_Code),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             END
                             GROUP('Parts Details'),AT(8,164,212,64),USE(?Group1:2),BOXED
                               PROMPT('Parts Stock Code'),AT(16,176),USE(?DEF:Parts_Stock_Code:Prompt)
                               ENTRY(@s30),AT(84,176,124,10),USE(def:Parts_Stock_Code),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                               PROMPT('Parts Description'),AT(16,192),USE(?DEF:Parts_Description:Prompt)
                               ENTRY(@s60),AT(84,192,124,10),USE(def:Parts_Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                               PROMPT('Parts Code'),AT(16,208),USE(?DEF:Parts_Code:Prompt)
                               ENTRY(@s30),AT(84,208,124,10),USE(def:Parts_Code),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             END
                             GROUP('Courier Details'),AT(8,232,212,64),USE(?Group1:3),BOXED
                               PROMPT('Courier Stock Code'),AT(16,244),USE(?DEF:Courier_Stock_Code:Prompt)
                               ENTRY(@s30),AT(84,244,124,10),USE(def:Courier_Stock_Code),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                               PROMPT('Courier Description'),AT(16,260),USE(?DEF:Courier_Description:Prompt)
                               ENTRY(@s60),AT(84,260,124,10),USE(def:Courier_Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                               PROMPT('Courier Code'),AT(16,276),USE(?DEF:Courier_Code:Prompt)
                               ENTRY(@s30),AT(84,276,124,10),USE(def:Courier_Code),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             END
                           END
                         END
                       END
                       PANEL,AT(4,304,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(112,308,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(168,308,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?def:Use_Sage{prop:Font,3} = -1
    ?def:Use_Sage{prop:Color} = 15066597
    ?def:Use_Sage{prop:Trn} = 0
    ?Sage_Group{prop:Font,3} = -1
    ?Sage_Group{prop:Color} = 15066597
    ?Sage_Group{prop:Trn} = 0
    ?DEF:User_Name_Sage:Prompt{prop:FontColor} = -1
    ?DEF:User_Name_Sage:Prompt{prop:Color} = 15066597
    If ?def:User_Name_Sage{prop:ReadOnly} = True
        ?def:User_Name_Sage{prop:FontColor} = 65793
        ?def:User_Name_Sage{prop:Color} = 15066597
    Elsif ?def:User_Name_Sage{prop:Req} = True
        ?def:User_Name_Sage{prop:FontColor} = 65793
        ?def:User_Name_Sage{prop:Color} = 8454143
    Else ! If ?def:User_Name_Sage{prop:Req} = True
        ?def:User_Name_Sage{prop:FontColor} = 65793
        ?def:User_Name_Sage{prop:Color} = 16777215
    End ! If ?def:User_Name_Sage{prop:Req} = True
    ?def:User_Name_Sage{prop:Trn} = 0
    ?def:User_Name_Sage{prop:FontStyle} = font:Bold
    ?DEF:Password_Sage:Prompt{prop:FontColor} = -1
    ?DEF:Password_Sage:Prompt{prop:Color} = 15066597
    If ?def:Password_Sage{prop:ReadOnly} = True
        ?def:Password_Sage{prop:FontColor} = 65793
        ?def:Password_Sage{prop:Color} = 15066597
    Elsif ?def:Password_Sage{prop:Req} = True
        ?def:Password_Sage{prop:FontColor} = 65793
        ?def:Password_Sage{prop:Color} = 8454143
    Else ! If ?def:Password_Sage{prop:Req} = True
        ?def:Password_Sage{prop:FontColor} = 65793
        ?def:Password_Sage{prop:Color} = 16777215
    End ! If ?def:Password_Sage{prop:Req} = True
    ?def:Password_Sage{prop:Trn} = 0
    ?def:Password_Sage{prop:FontStyle} = font:Bold
    ?DEF:Path_Sage:Prompt{prop:FontColor} = -1
    ?DEF:Path_Sage:Prompt{prop:Color} = 15066597
    If ?def:Path_Sage{prop:ReadOnly} = True
        ?def:Path_Sage{prop:FontColor} = 65793
        ?def:Path_Sage{prop:Color} = 15066597
    Elsif ?def:Path_Sage{prop:Req} = True
        ?def:Path_Sage{prop:FontColor} = 65793
        ?def:Path_Sage{prop:Color} = 8454143
    Else ! If ?def:Path_Sage{prop:Req} = True
        ?def:Path_Sage{prop:FontColor} = 65793
        ?def:Path_Sage{prop:Color} = 16777215
    End ! If ?def:Path_Sage{prop:Req} = True
    ?def:Path_Sage{prop:Trn} = 0
    ?def:Path_Sage{prop:FontStyle} = font:Bold
    ?DEF:Global_Nominal_Code:Prompt{prop:FontColor} = -1
    ?DEF:Global_Nominal_Code:Prompt{prop:Color} = 15066597
    If ?def:Global_Nominal_Code{prop:ReadOnly} = True
        ?def:Global_Nominal_Code{prop:FontColor} = 65793
        ?def:Global_Nominal_Code{prop:Color} = 15066597
    Elsif ?def:Global_Nominal_Code{prop:Req} = True
        ?def:Global_Nominal_Code{prop:FontColor} = 65793
        ?def:Global_Nominal_Code{prop:Color} = 8454143
    Else ! If ?def:Global_Nominal_Code{prop:Req} = True
        ?def:Global_Nominal_Code{prop:FontColor} = 65793
        ?def:Global_Nominal_Code{prop:Color} = 16777215
    End ! If ?def:Global_Nominal_Code{prop:Req} = True
    ?def:Global_Nominal_Code{prop:Trn} = 0
    ?def:Global_Nominal_Code{prop:FontStyle} = font:Bold
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?DEF:Labour_Stock_Code:Prompt{prop:FontColor} = -1
    ?DEF:Labour_Stock_Code:Prompt{prop:Color} = 15066597
    If ?def:Labour_Stock_Code{prop:ReadOnly} = True
        ?def:Labour_Stock_Code{prop:FontColor} = 65793
        ?def:Labour_Stock_Code{prop:Color} = 15066597
    Elsif ?def:Labour_Stock_Code{prop:Req} = True
        ?def:Labour_Stock_Code{prop:FontColor} = 65793
        ?def:Labour_Stock_Code{prop:Color} = 8454143
    Else ! If ?def:Labour_Stock_Code{prop:Req} = True
        ?def:Labour_Stock_Code{prop:FontColor} = 65793
        ?def:Labour_Stock_Code{prop:Color} = 16777215
    End ! If ?def:Labour_Stock_Code{prop:Req} = True
    ?def:Labour_Stock_Code{prop:Trn} = 0
    ?def:Labour_Stock_Code{prop:FontStyle} = font:Bold
    ?DEF:Labour_Description:Prompt{prop:FontColor} = -1
    ?DEF:Labour_Description:Prompt{prop:Color} = 15066597
    If ?def:Labour_Description{prop:ReadOnly} = True
        ?def:Labour_Description{prop:FontColor} = 65793
        ?def:Labour_Description{prop:Color} = 15066597
    Elsif ?def:Labour_Description{prop:Req} = True
        ?def:Labour_Description{prop:FontColor} = 65793
        ?def:Labour_Description{prop:Color} = 8454143
    Else ! If ?def:Labour_Description{prop:Req} = True
        ?def:Labour_Description{prop:FontColor} = 65793
        ?def:Labour_Description{prop:Color} = 16777215
    End ! If ?def:Labour_Description{prop:Req} = True
    ?def:Labour_Description{prop:Trn} = 0
    ?def:Labour_Description{prop:FontStyle} = font:Bold
    ?DEF:Labour_Code:Prompt{prop:FontColor} = -1
    ?DEF:Labour_Code:Prompt{prop:Color} = 15066597
    If ?def:Labour_Code{prop:ReadOnly} = True
        ?def:Labour_Code{prop:FontColor} = 65793
        ?def:Labour_Code{prop:Color} = 15066597
    Elsif ?def:Labour_Code{prop:Req} = True
        ?def:Labour_Code{prop:FontColor} = 65793
        ?def:Labour_Code{prop:Color} = 8454143
    Else ! If ?def:Labour_Code{prop:Req} = True
        ?def:Labour_Code{prop:FontColor} = 65793
        ?def:Labour_Code{prop:Color} = 16777215
    End ! If ?def:Labour_Code{prop:Req} = True
    ?def:Labour_Code{prop:Trn} = 0
    ?def:Labour_Code{prop:FontStyle} = font:Bold
    ?Group1:2{prop:Font,3} = -1
    ?Group1:2{prop:Color} = 15066597
    ?Group1:2{prop:Trn} = 0
    ?DEF:Parts_Stock_Code:Prompt{prop:FontColor} = -1
    ?DEF:Parts_Stock_Code:Prompt{prop:Color} = 15066597
    If ?def:Parts_Stock_Code{prop:ReadOnly} = True
        ?def:Parts_Stock_Code{prop:FontColor} = 65793
        ?def:Parts_Stock_Code{prop:Color} = 15066597
    Elsif ?def:Parts_Stock_Code{prop:Req} = True
        ?def:Parts_Stock_Code{prop:FontColor} = 65793
        ?def:Parts_Stock_Code{prop:Color} = 8454143
    Else ! If ?def:Parts_Stock_Code{prop:Req} = True
        ?def:Parts_Stock_Code{prop:FontColor} = 65793
        ?def:Parts_Stock_Code{prop:Color} = 16777215
    End ! If ?def:Parts_Stock_Code{prop:Req} = True
    ?def:Parts_Stock_Code{prop:Trn} = 0
    ?def:Parts_Stock_Code{prop:FontStyle} = font:Bold
    ?DEF:Parts_Description:Prompt{prop:FontColor} = -1
    ?DEF:Parts_Description:Prompt{prop:Color} = 15066597
    If ?def:Parts_Description{prop:ReadOnly} = True
        ?def:Parts_Description{prop:FontColor} = 65793
        ?def:Parts_Description{prop:Color} = 15066597
    Elsif ?def:Parts_Description{prop:Req} = True
        ?def:Parts_Description{prop:FontColor} = 65793
        ?def:Parts_Description{prop:Color} = 8454143
    Else ! If ?def:Parts_Description{prop:Req} = True
        ?def:Parts_Description{prop:FontColor} = 65793
        ?def:Parts_Description{prop:Color} = 16777215
    End ! If ?def:Parts_Description{prop:Req} = True
    ?def:Parts_Description{prop:Trn} = 0
    ?def:Parts_Description{prop:FontStyle} = font:Bold
    ?DEF:Parts_Code:Prompt{prop:FontColor} = -1
    ?DEF:Parts_Code:Prompt{prop:Color} = 15066597
    If ?def:Parts_Code{prop:ReadOnly} = True
        ?def:Parts_Code{prop:FontColor} = 65793
        ?def:Parts_Code{prop:Color} = 15066597
    Elsif ?def:Parts_Code{prop:Req} = True
        ?def:Parts_Code{prop:FontColor} = 65793
        ?def:Parts_Code{prop:Color} = 8454143
    Else ! If ?def:Parts_Code{prop:Req} = True
        ?def:Parts_Code{prop:FontColor} = 65793
        ?def:Parts_Code{prop:Color} = 16777215
    End ! If ?def:Parts_Code{prop:Req} = True
    ?def:Parts_Code{prop:Trn} = 0
    ?def:Parts_Code{prop:FontStyle} = font:Bold
    ?Group1:3{prop:Font,3} = -1
    ?Group1:3{prop:Color} = 15066597
    ?Group1:3{prop:Trn} = 0
    ?DEF:Courier_Stock_Code:Prompt{prop:FontColor} = -1
    ?DEF:Courier_Stock_Code:Prompt{prop:Color} = 15066597
    If ?def:Courier_Stock_Code{prop:ReadOnly} = True
        ?def:Courier_Stock_Code{prop:FontColor} = 65793
        ?def:Courier_Stock_Code{prop:Color} = 15066597
    Elsif ?def:Courier_Stock_Code{prop:Req} = True
        ?def:Courier_Stock_Code{prop:FontColor} = 65793
        ?def:Courier_Stock_Code{prop:Color} = 8454143
    Else ! If ?def:Courier_Stock_Code{prop:Req} = True
        ?def:Courier_Stock_Code{prop:FontColor} = 65793
        ?def:Courier_Stock_Code{prop:Color} = 16777215
    End ! If ?def:Courier_Stock_Code{prop:Req} = True
    ?def:Courier_Stock_Code{prop:Trn} = 0
    ?def:Courier_Stock_Code{prop:FontStyle} = font:Bold
    ?DEF:Courier_Description:Prompt{prop:FontColor} = -1
    ?DEF:Courier_Description:Prompt{prop:Color} = 15066597
    If ?def:Courier_Description{prop:ReadOnly} = True
        ?def:Courier_Description{prop:FontColor} = 65793
        ?def:Courier_Description{prop:Color} = 15066597
    Elsif ?def:Courier_Description{prop:Req} = True
        ?def:Courier_Description{prop:FontColor} = 65793
        ?def:Courier_Description{prop:Color} = 8454143
    Else ! If ?def:Courier_Description{prop:Req} = True
        ?def:Courier_Description{prop:FontColor} = 65793
        ?def:Courier_Description{prop:Color} = 16777215
    End ! If ?def:Courier_Description{prop:Req} = True
    ?def:Courier_Description{prop:Trn} = 0
    ?def:Courier_Description{prop:FontStyle} = font:Bold
    ?DEF:Courier_Code:Prompt{prop:FontColor} = -1
    ?DEF:Courier_Code:Prompt{prop:Color} = 15066597
    If ?def:Courier_Code{prop:ReadOnly} = True
        ?def:Courier_Code{prop:FontColor} = 65793
        ?def:Courier_Code{prop:Color} = 15066597
    Elsif ?def:Courier_Code{prop:Req} = True
        ?def:Courier_Code{prop:FontColor} = 65793
        ?def:Courier_Code{prop:Color} = 8454143
    Else ! If ?def:Courier_Code{prop:Req} = True
        ?def:Courier_Code{prop:FontColor} = 65793
        ?def:Courier_Code{prop:Color} = 16777215
    End ! If ?def:Courier_Code{prop:Req} = True
    ?def:Courier_Code{prop:Trn} = 0
    ?def:Courier_Code{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Show_Hide       Routine
    If def:use_sage = 'YES'
        Enable(?Sage_Group)
    Else!If def:use_sage = 'YES'
        Disable(?Sage_Group)
    End!If def:use_sage = 'YES'
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Sage_Defaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?def:Use_Sage
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  Set(Defaults)
  access:defaults.next()
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?def:Use_Sage
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:Use_Sage, Accepted)
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:Use_Sage, Accepted)
    OF ?Lookup_Sage_Path
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Sage_Path, Accepted)
      filedialog ('Choose Directory',def:path_sage,'All Directories|*.*', |
                  file:save+file:keepdir + file:noerror + file:longname + file:directory)
      def:path_sage = upper(def:path_sage)
      display(?def:path_sage)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Sage_Path, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:Defaults.update()
      Post(Event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do show_hide
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

