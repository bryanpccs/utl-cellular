

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01041.INC'),ONCE        !Local module procedure declarations
                     END


EmailDefaults PROCEDURE                               !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('General Defaults'),AT(,,235,90),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,228,56),USE(?Sheet1),SPREAD
                         TAB('Email Defaults'),USE(?BookingDefaultsTab)
                           PROMPT('Email Server Address'),AT(8,20),USE(?def:EmailServerAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,20,124,10),USE(def:EmailServerAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Server Address'),TIP('Email Server Address'),UPR
                           PROMPT('Email Server Port Number'),AT(8,36,68,16),USE(?def:EmailServerPort:Prompt),TRN
                           ENTRY(@s8),AT(84,36,64,10),USE(def:EmailServerPort),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Service Port Number'),TIP('Email Service Port Number'),UPR
                         END
                       END
                       BUTTON('&OK'),AT(116,68,56,16),USE(?OkButton),TRN,LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(172,68,56,16),USE(?CancelButton),TRN,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,64,228,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?BookingDefaultsTab{prop:Color} = 15066597
    ?def:EmailServerAddress:Prompt{prop:FontColor} = -1
    ?def:EmailServerAddress:Prompt{prop:Color} = 15066597
    If ?def:EmailServerAddress{prop:ReadOnly} = True
        ?def:EmailServerAddress{prop:FontColor} = 65793
        ?def:EmailServerAddress{prop:Color} = 15066597
    Elsif ?def:EmailServerAddress{prop:Req} = True
        ?def:EmailServerAddress{prop:FontColor} = 65793
        ?def:EmailServerAddress{prop:Color} = 8454143
    Else ! If ?def:EmailServerAddress{prop:Req} = True
        ?def:EmailServerAddress{prop:FontColor} = 65793
        ?def:EmailServerAddress{prop:Color} = 16777215
    End ! If ?def:EmailServerAddress{prop:Req} = True
    ?def:EmailServerAddress{prop:Trn} = 0
    ?def:EmailServerAddress{prop:FontStyle} = font:Bold
    ?def:EmailServerPort:Prompt{prop:FontColor} = -1
    ?def:EmailServerPort:Prompt{prop:Color} = 15066597
    If ?def:EmailServerPort{prop:ReadOnly} = True
        ?def:EmailServerPort{prop:FontColor} = 65793
        ?def:EmailServerPort{prop:Color} = 15066597
    Elsif ?def:EmailServerPort{prop:Req} = True
        ?def:EmailServerPort{prop:FontColor} = 65793
        ?def:EmailServerPort{prop:Color} = 8454143
    Else ! If ?def:EmailServerPort{prop:Req} = True
        ?def:EmailServerPort{prop:FontColor} = 65793
        ?def:EmailServerPort{prop:Color} = 16777215
    End ! If ?def:EmailServerPort{prop:Req} = True
    ?def:EmailServerPort{prop:Trn} = 0
    ?def:EmailServerPort{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('EmailDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?def:EmailServerAddress:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  
  
  
  
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !If def:auto_logout <> 'NO'
      !    If def:auto_logout_time < 60 Or def:auto_logout_time > 3600
      !        beep(beep:systemhand)  ;  yield()
      !        message('The logout time must be between 60 seconds (1 minute) and '&|
      !               '|3,600 seconds (1 hour).', |
      !               'ServiceBase 2000', icon:hand)
      !        def:auto_logout_time = ''
      !        Select(?def:auto_logout_time)
      !        Cycle
      !    End
      !End
      access:defaults.update()
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

