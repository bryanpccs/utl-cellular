

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01035.INC'),ONCE        !Local module procedure declarations
                     END


BookingDefaults PROCEDURE                             !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('General Defaults'),AT(,,235,168),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,228,132),USE(?Sheet1),SPREAD
                         TAB('Booking Defaults'),USE(?BookingDefaultsTab)
                           GROUP('Default Charge Types'),AT(8,20,216,68),USE(?DefaultChargeTypes),BOXED
                             PROMPT('The following Charge Types will be automatically filled in when Chargeable/Warra' &|
   'nty Job is selected on a New Job'),AT(12,32,208,20),USE(?Prompt42),FONT(,8,COLOR:Navy,)
                             PROMPT('Chargeable'),AT(12,52),USE(?def:ChaChargeType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(84,52,124,10),USE(def:ChaChargeType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Chargeable Charge Type'),TIP('Chargeable Charge Type'),ALRT(MouseRight),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                             BUTTON,AT(212,52,10,10),USE(?LookupChargeableChargeType),SKIP,ICON('List3.ico')
                             PROMPT('Warranty'),AT(12,68),USE(?def:WarChargeType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(84,68,124,10),USE(def:WarChargeType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Warranty Charge Type'),TIP('Warranty Charge Type'),ALRT(EnterKey),ALRT(MouseRight),ALRT(MouseLeft2),UPR
                             BUTTON,AT(212,68,10,10),USE(?LookupWarrantyChargeType),SKIP,ICON('List3.ico')
                           END
                           CHECK('Use Original Booking Template'),AT(8,92),USE(def:UseOriginalBookingTemplate),RIGHT,VALUE('1','0')
                         END
                       END
                       BUTTON('&OK'),AT(116,144,56,16),USE(?OkButton),TRN,LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(172,144,56,16),USE(?CancelButton),TRN,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,140,228,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
look:def:ChaChargeType                Like(def:ChaChargeType)
look:def:WarChargeType                Like(def:WarChargeType)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?BookingDefaultsTab{prop:Color} = 15066597
    ?DefaultChargeTypes{prop:Font,3} = -1
    ?DefaultChargeTypes{prop:Color} = 15066597
    ?DefaultChargeTypes{prop:Trn} = 0
    ?Prompt42{prop:FontColor} = -1
    ?Prompt42{prop:Color} = 15066597
    ?def:ChaChargeType:Prompt{prop:FontColor} = -1
    ?def:ChaChargeType:Prompt{prop:Color} = 15066597
    If ?def:ChaChargeType{prop:ReadOnly} = True
        ?def:ChaChargeType{prop:FontColor} = 65793
        ?def:ChaChargeType{prop:Color} = 15066597
    Elsif ?def:ChaChargeType{prop:Req} = True
        ?def:ChaChargeType{prop:FontColor} = 65793
        ?def:ChaChargeType{prop:Color} = 8454143
    Else ! If ?def:ChaChargeType{prop:Req} = True
        ?def:ChaChargeType{prop:FontColor} = 65793
        ?def:ChaChargeType{prop:Color} = 16777215
    End ! If ?def:ChaChargeType{prop:Req} = True
    ?def:ChaChargeType{prop:Trn} = 0
    ?def:ChaChargeType{prop:FontStyle} = font:Bold
    ?def:WarChargeType:Prompt{prop:FontColor} = -1
    ?def:WarChargeType:Prompt{prop:Color} = 15066597
    If ?def:WarChargeType{prop:ReadOnly} = True
        ?def:WarChargeType{prop:FontColor} = 65793
        ?def:WarChargeType{prop:Color} = 15066597
    Elsif ?def:WarChargeType{prop:Req} = True
        ?def:WarChargeType{prop:FontColor} = 65793
        ?def:WarChargeType{prop:Color} = 8454143
    Else ! If ?def:WarChargeType{prop:Req} = True
        ?def:WarChargeType{prop:FontColor} = 65793
        ?def:WarChargeType{prop:Color} = 16777215
    End ! If ?def:WarChargeType{prop:Req} = True
    ?def:WarChargeType{prop:Trn} = 0
    ?def:WarChargeType{prop:FontStyle} = font:Bold
    ?def:UseOriginalBookingTemplate{prop:Font,3} = -1
    ?def:UseOriginalBookingTemplate{prop:Color} = 15066597
    ?def:UseOriginalBookingTemplate{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BookingDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt42
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  
  
  
  
  Do RecolourWindow
  ! support for CPCS
  IF ?def:ChaChargeType{Prop:Tip} AND ~?LookupChargeableChargeType{Prop:Tip}
     ?LookupChargeableChargeType{Prop:Tip} = 'Select ' & ?def:ChaChargeType{Prop:Tip}
  END
  IF ?def:ChaChargeType{Prop:Msg} AND ~?LookupChargeableChargeType{Prop:Msg}
     ?LookupChargeableChargeType{Prop:Msg} = 'Select ' & ?def:ChaChargeType{Prop:Msg}
  END
  IF ?def:WarChargeType{Prop:Tip} AND ~?LookupWarrantyChargeType{Prop:Tip}
     ?LookupWarrantyChargeType{Prop:Tip} = 'Select ' & ?def:WarChargeType{Prop:Tip}
  END
  IF ?def:WarChargeType{Prop:Msg} AND ~?LookupWarrantyChargeType{Prop:Msg}
     ?LookupWarrantyChargeType{Prop:Msg} = 'Select ' & ?def:WarChargeType{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Warranty_Charge_Types
      Browse_Warranty_Charge_Types
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?def:ChaChargeType
      IF def:ChaChargeType OR ?def:ChaChargeType{Prop:Req}
        cha:Charge_Type = def:ChaChargeType
        cha:Warranty = 'NO'
        GLO:Select1 = 'NO'
        !Save Lookup Field Incase Of error
        look:def:ChaChargeType        = def:ChaChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            def:ChaChargeType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            def:ChaChargeType = look:def:ChaChargeType
            SELECT(?def:ChaChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupChargeableChargeType
      ThisWindow.Update
      cha:Charge_Type = def:ChaChargeType
      GLO:Select1 = 'NO'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          def:ChaChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?def:ChaChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?def:ChaChargeType)
    OF ?def:WarChargeType
      IF def:WarChargeType OR ?def:WarChargeType{Prop:Req}
        cha:Charge_Type = def:WarChargeType
        cha:Warranty = 'YES'
        GLO:Select1 = 'YES'
        !Save Lookup Field Incase Of error
        look:def:WarChargeType        = def:WarChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            def:WarChargeType = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            def:WarChargeType = look:def:WarChargeType
            SELECT(?def:WarChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWarrantyChargeType
      ThisWindow.Update
      cha:Charge_Type = def:WarChargeType
      GLO:Select1 = 'YES'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          def:WarChargeType = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?def:WarChargeType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?def:WarChargeType)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !If def:auto_logout <> 'NO'
      !    If def:auto_logout_time < 60 Or def:auto_logout_time > 3600
      !        beep(beep:systemhand)  ;  yield()
      !        message('The logout time must be between 60 seconds (1 minute) and '&|
      !               '|3,600 seconds (1 hour).', |
      !               'ServiceBase 2000', icon:hand)
      !        def:auto_logout_time = ''
      !        Select(?def:auto_logout_time)
      !        Cycle
      !    End
      !End
      access:defaults.update()
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?def:ChaChargeType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:ChaChargeType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Charge Type')
             Post(Event:Accepted,?LookupChargeableChargeType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupChargeableChargeType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:ChaChargeType, AlertKey)
    END
  OF ?def:WarChargeType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:WarChargeType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Charge Type')
             Post(Event:Accepted,?def:WarChargeType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?def:WarChargeType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:WarChargeType, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

