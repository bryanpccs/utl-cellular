

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01004.INC'),ONCE        !Local module procedure declarations
                     END


New_Job_Screen PROCEDURE                              !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
name_address_temp    STRING(1)
notes_temp           STRING(1)
Invoice_Text_Temp    STRING(1)
repair_details_temp  STRING(1)
model_temp           STRING(1)
title_temp           STRING(80)
field_type_temp      STRING(1)
select_all_temp      STRING(1)
Trade_Account_temp   STRING(1)
Collection_Delivery_Address STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Create New Job'),AT(,,188,180),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,180,144),USE(?Sheet1),SPREAD
                         TAB('Replicate Job'),USE(?Tab1),HIDE
                           STRING('Create New Job From Job Number:'),AT(8,20),USE(?String1),TRN
                           STRING(@p<<<<<<#p),AT(124,20),USE(GLO:Select3),TRN,FONT(,,,FONT:bold)
                           GROUP('Replicate Fields'),AT(23,36,142,108),USE(?Group1),BOXED
                             CHECK('Select All'),AT(36,48),USE(select_all_temp),VALUE('Y','N')
                             CHECK('Trade Account'),AT(36,60),USE(Trade_Account_temp),VALUE('Y','N')
                             CHECK('Contact Name'),AT(36,72),USE(name_address_temp),TRN,VALUE('Y','N')
                             CHECK('Collection / Delivery Addresses'),AT(36,84),USE(Collection_Delivery_Address),VALUE('Y','N')
                             CHECK('Model Details'),AT(36,96),USE(model_temp),VALUE('Y','N')
                             CHECK('Engineer''s Notes'),AT(36,108),USE(notes_temp),TRN,VALUE('Y','N')
                             CHECK('Invoice Text'),AT(36,120),USE(Invoice_Text_Temp),TRN,VALUE('Y','N')
                             CHECK('Repair Details'),AT(36,132),USE(repair_details_temp),VALUE('Y','N')
                           END
                         END
                         TAB('Create New Job'),USE(?Tab2),HIDE
                           STRING(@s80),AT(8,20,158,10),USE(title_temp),TRN,FONT(,,,FONT:bold)
                           OPTION('Select A Field'),AT(30,36,128,104),USE(field_type_temp),BOXED
                             RADIO('Surname'),AT(56,64),USE(?Option1:Radio1),VALUE('2')
                             RADIO('Mobile Number'),AT(56,76),USE(?Option1:Radio2),VALUE('3')
                             RADIO('E.S.N. / I.M.E.I.'),AT(56,88),USE(?Option1:Radio3),VALUE('4')
                             RADIO('M.S.N.'),AT(56,100),USE(?Option1:Radio4),VALUE('5')
                             RADIO('Postcode'),AT(56,112),USE(?Option1:Radio5),VALUE('6')
                             RADIO('Search Again'),AT(56,124),USE(?field_type_temp:Radio7),VALUE('7')
                             RADIO('Don''t Replicate'),AT(56,52),USE(?field_type_temp:Radio6),VALUE('1')
                           END
                         END
                       END
                       PANEL,AT(4,152,180,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(68,156,56,16),USE(?OkButton),LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(124,156,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?GLO:Select3{prop:FontColor} = -1
    ?GLO:Select3{prop:Color} = 15066597
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?select_all_temp{prop:Font,3} = -1
    ?select_all_temp{prop:Color} = 15066597
    ?select_all_temp{prop:Trn} = 0
    ?Trade_Account_temp{prop:Font,3} = -1
    ?Trade_Account_temp{prop:Color} = 15066597
    ?Trade_Account_temp{prop:Trn} = 0
    ?name_address_temp{prop:Font,3} = -1
    ?name_address_temp{prop:Color} = 15066597
    ?name_address_temp{prop:Trn} = 0
    ?Collection_Delivery_Address{prop:Font,3} = -1
    ?Collection_Delivery_Address{prop:Color} = 15066597
    ?Collection_Delivery_Address{prop:Trn} = 0
    ?model_temp{prop:Font,3} = -1
    ?model_temp{prop:Color} = 15066597
    ?model_temp{prop:Trn} = 0
    ?notes_temp{prop:Font,3} = -1
    ?notes_temp{prop:Color} = 15066597
    ?notes_temp{prop:Trn} = 0
    ?Invoice_Text_Temp{prop:Font,3} = -1
    ?Invoice_Text_Temp{prop:Color} = 15066597
    ?Invoice_Text_Temp{prop:Trn} = 0
    ?repair_details_temp{prop:Font,3} = -1
    ?repair_details_temp{prop:Color} = 15066597
    ?repair_details_temp{prop:Trn} = 0
    ?Tab2{prop:Color} = 15066597
    ?title_temp{prop:FontColor} = -1
    ?title_temp{prop:Color} = 15066597
    ?field_type_temp{prop:Font,3} = -1
    ?field_type_temp{prop:Color} = 15066597
    ?field_type_temp{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    ?Option1:Radio3{prop:Font,3} = -1
    ?Option1:Radio3{prop:Color} = 15066597
    ?Option1:Radio3{prop:Trn} = 0
    ?Option1:Radio4{prop:Font,3} = -1
    ?Option1:Radio4{prop:Color} = 15066597
    ?Option1:Radio4{prop:Trn} = 0
    ?Option1:Radio5{prop:Font,3} = -1
    ?Option1:Radio5{prop:Color} = 15066597
    ?Option1:Radio5{prop:Trn} = 0
    ?field_type_temp:Radio7{prop:Font,3} = -1
    ?field_type_temp:Radio7{prop:Color} = 15066597
    ?field_type_temp:Radio7{prop:Trn} = 0
    ?field_type_temp:Radio6{prop:Font,3} = -1
    ?field_type_temp:Radio6{prop:Color} = 15066597
    ?field_type_temp:Radio6{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('New_Job_Screen')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! Before Embed Point: %AfterWindowOpening) DESC(Legacy: After Opening the Window) ARG()
  If glo:select1 = 'NO MATCH'
      Hide(?Tab1)
      Unhide(?Tab2)
      Title_temp = 'Replicate ' & Clip(glo:select2) & ' to tagged field: '
      field_type_temp = 1
  Else
      If glo:select4 = 'NOBOUNCER'
          disable(?model_temp)
      End
      Hide(?Tab2)
      Unhide(?Tab1)
  End
  ! After Embed Point: %AfterWindowOpening) DESC(Legacy: After Opening the Window) ARG()
  ! support for CPCS
  IF ?select_all_temp{Prop:Checked} = True
    model_temp = 'Y'
    name_address_temp = 'Y'
    notes_temp = 'Y'
    Invoice_Text_Temp = 'Y'
    repair_details_temp = 'Y'
    Trade_Account_temp = 'Y'
    Collection_Delivery_Address = 'Y'
  END
  IF ?select_all_temp{Prop:Checked} = False
    name_address_temp = 'N'
    notes_temp = 'N'
    Invoice_Text_Temp = 'N'
    repair_details_temp = 'N'
    model_temp = 'N'
    Trade_Account_temp = 'N'
    Collection_Delivery_Address = 'N'
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CancelButton
      ! Before Embed Point: %ControlPreEventHandling) DESC(Legacy: Control Event Handling, before generated code) ARG(?CancelButton, Accepted)
      glo:select1 = ''
      ! After Embed Point: %ControlPreEventHandling) DESC(Legacy: Control Event Handling, before generated code) ARG(?CancelButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?select_all_temp
      IF ?select_all_temp{Prop:Checked} = True
        model_temp = 'Y'
        name_address_temp = 'Y'
        notes_temp = 'Y'
        Invoice_Text_Temp = 'Y'
        repair_details_temp = 'Y'
        Trade_Account_temp = 'Y'
        Collection_Delivery_Address = 'Y'
      END
      IF ?select_all_temp{Prop:Checked} = False
        name_address_temp = 'N'
        notes_temp = 'N'
        Invoice_Text_Temp = 'N'
        repair_details_temp = 'N'
        model_temp = 'N'
        Trade_Account_temp = 'N'
        Collection_Delivery_Address = 'N'
      END
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?OkButton, Accepted)
      If glo:select1 <> 'NO MATCH'
          If glo:select4 = 'NOBOUNCER'
              model_temp = ''
          End
          glo:select5 = Trade_Account_temp
          glo:select6 = Collection_Delivery_Address
          glo:select7 = model_temp
          glo:select8 = name_address_temp
          glo:select9 = notes_temp
          glo:select10 = Invoice_Text_Temp
          glo:select11 = repair_details_temp
          glo:select12 = select_all_temp
      Else
          Case(field_type_temp)
              Of 1
                  ! Do Nowt
              Of 2
                  glo:select2 = 'SURNAME'
              Of 3
                  glo:select2 = 'MOBILE NUMBER'
              Of 4
                  glo:select2 = 'ESN'
              Of 5
                  glo:select2 = 'MSN'
              Of 6
                  glo:select2 = 'POSTCODE'
              Of 7
                  glo:select2 = 'SEARCH AGAIN'
          End
      End
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

