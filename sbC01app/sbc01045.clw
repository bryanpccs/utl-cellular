

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01045.INC'),ONCE        !Local module procedure declarations
                     END


SIDJobExtractDefaults PROCEDURE                       !Generated from procedure template - Form

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
window               WINDOW('SID Job Extract Defaults'),AT(,,227,280),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,2,220,246),USE(?Sheet1),SPREAD
                         TAB('SID Job Extract Defaults'),USE(?Tab1)
                           GROUP('Email Defaults'),AT(8,16,212,108),USE(?Group2),BOXED
                             PROMPT('SMTP Server'),AT(12,28),USE(?defe:SMTPServer:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s50),AT(88,28,124,10),USE(defe:SMTPServer),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Server'),TIP('SMTP Server'),REQ
                             PROMPT('SMTP Port Number'),AT(12,42),USE(?defe:SMTPPortNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s6),AT(88,42,64,10),USE(defe:SMTPPortNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Port Number'),TIP('SMTP Port Number'),REQ,UPR
                             PROMPT('SMTP Username'),AT(12,54),USE(?defe:SMTPUserName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s60),AT(88,56,124,10),USE(defe:SMTPUserName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Username'),TIP('SMTP Username')
                             PROMPT('SMTP Password'),AT(12,70),USE(?defe:SMTPPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s60),AT(88,70,124,10),USE(defe:SMTPPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Password'),TIP('SMTP Password'),PASSWORD
                             PROMPT('From Email Address'),AT(12,84),USE(?defe:FromEmailAddress:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s255),AT(88,84,124,10),USE(defe:FromEmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('From Email Address'),TIP('From Email Address'),REQ
                             PROMPT('Email Subject'),AT(12,98),USE(?defe:EmailSubject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s100),AT(88,98,124,10),USE(defe:EmailSubject),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Subject'),TIP('Email Subject'),REQ
                             PROMPT('Email Recipient List'),AT(12,110),USE(?defe:EmailRecipientList:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s255),AT(88,110,124,10),USE(defe:EmailRecipientList),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Recipient List'),TIP('Email Recipient List'),REQ
                           END
                           GROUP('FTP Defaults'),AT(8,126,212,70),USE(?Group3),BOXED
                             PROMPT('FTP Server'),AT(12,136),USE(?defe:FTPServer:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s60),AT(88,136,124,10),USE(defe:FTPServer),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Server'),TIP('FTP Server')
                             PROMPT('FTP Username'),AT(12,150),USE(?defe:FTPUsername:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s60),AT(88,150,124,10),USE(defe:FTPUsername),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Username'),TIP('FTP Username')
                             PROMPT('Password'),AT(12,164),USE(?defe:FTPPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s60),AT(88,164,124,10),USE(defe:FTPPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Password'),TIP('Password'),PASSWORD
                             PROMPT('FTP Export Path'),AT(12,178),USE(?defe:FTPExportPath:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s255),AT(88,178,124,10),USE(defe:FTPExportPath),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Export Path'),TIP('FTP Export Path')
                           END
                           GROUP('Export Criteria'),AT(8,200,212,42),USE(?Group1),BOXED
                             PROMPT('Export Jobs From The Last'),AT(12,210,72,16),USE(?defe:DateFrom:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             PROMPT('Days'),AT(156,210),USE(?Prompt13)
                             ENTRY(@s8),AT(88,210,64,10),USE(defe:ExportDays),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Export Days'),TIP('Export Days'),UPR
                           END
                         END
                       END
                       PANEL,AT(4,252,220,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(104,256,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(164,256,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Group2{prop:Font,3} = -1
    ?Group2{prop:Color} = 15066597
    ?Group2{prop:Trn} = 0
    ?defe:SMTPServer:Prompt{prop:FontColor} = -1
    ?defe:SMTPServer:Prompt{prop:Color} = 15066597
    If ?defe:SMTPServer{prop:ReadOnly} = True
        ?defe:SMTPServer{prop:FontColor} = 65793
        ?defe:SMTPServer{prop:Color} = 15066597
    Elsif ?defe:SMTPServer{prop:Req} = True
        ?defe:SMTPServer{prop:FontColor} = 65793
        ?defe:SMTPServer{prop:Color} = 8454143
    Else ! If ?defe:SMTPServer{prop:Req} = True
        ?defe:SMTPServer{prop:FontColor} = 65793
        ?defe:SMTPServer{prop:Color} = 16777215
    End ! If ?defe:SMTPServer{prop:Req} = True
    ?defe:SMTPServer{prop:Trn} = 0
    ?defe:SMTPServer{prop:FontStyle} = font:Bold
    ?defe:SMTPPortNumber:Prompt{prop:FontColor} = -1
    ?defe:SMTPPortNumber:Prompt{prop:Color} = 15066597
    If ?defe:SMTPPortNumber{prop:ReadOnly} = True
        ?defe:SMTPPortNumber{prop:FontColor} = 65793
        ?defe:SMTPPortNumber{prop:Color} = 15066597
    Elsif ?defe:SMTPPortNumber{prop:Req} = True
        ?defe:SMTPPortNumber{prop:FontColor} = 65793
        ?defe:SMTPPortNumber{prop:Color} = 8454143
    Else ! If ?defe:SMTPPortNumber{prop:Req} = True
        ?defe:SMTPPortNumber{prop:FontColor} = 65793
        ?defe:SMTPPortNumber{prop:Color} = 16777215
    End ! If ?defe:SMTPPortNumber{prop:Req} = True
    ?defe:SMTPPortNumber{prop:Trn} = 0
    ?defe:SMTPPortNumber{prop:FontStyle} = font:Bold
    ?defe:SMTPUserName:Prompt{prop:FontColor} = -1
    ?defe:SMTPUserName:Prompt{prop:Color} = 15066597
    If ?defe:SMTPUserName{prop:ReadOnly} = True
        ?defe:SMTPUserName{prop:FontColor} = 65793
        ?defe:SMTPUserName{prop:Color} = 15066597
    Elsif ?defe:SMTPUserName{prop:Req} = True
        ?defe:SMTPUserName{prop:FontColor} = 65793
        ?defe:SMTPUserName{prop:Color} = 8454143
    Else ! If ?defe:SMTPUserName{prop:Req} = True
        ?defe:SMTPUserName{prop:FontColor} = 65793
        ?defe:SMTPUserName{prop:Color} = 16777215
    End ! If ?defe:SMTPUserName{prop:Req} = True
    ?defe:SMTPUserName{prop:Trn} = 0
    ?defe:SMTPUserName{prop:FontStyle} = font:Bold
    ?defe:SMTPPassword:Prompt{prop:FontColor} = -1
    ?defe:SMTPPassword:Prompt{prop:Color} = 15066597
    If ?defe:SMTPPassword{prop:ReadOnly} = True
        ?defe:SMTPPassword{prop:FontColor} = 65793
        ?defe:SMTPPassword{prop:Color} = 15066597
    Elsif ?defe:SMTPPassword{prop:Req} = True
        ?defe:SMTPPassword{prop:FontColor} = 65793
        ?defe:SMTPPassword{prop:Color} = 8454143
    Else ! If ?defe:SMTPPassword{prop:Req} = True
        ?defe:SMTPPassword{prop:FontColor} = 65793
        ?defe:SMTPPassword{prop:Color} = 16777215
    End ! If ?defe:SMTPPassword{prop:Req} = True
    ?defe:SMTPPassword{prop:Trn} = 0
    ?defe:SMTPPassword{prop:FontStyle} = font:Bold
    ?defe:FromEmailAddress:Prompt{prop:FontColor} = -1
    ?defe:FromEmailAddress:Prompt{prop:Color} = 15066597
    If ?defe:FromEmailAddress{prop:ReadOnly} = True
        ?defe:FromEmailAddress{prop:FontColor} = 65793
        ?defe:FromEmailAddress{prop:Color} = 15066597
    Elsif ?defe:FromEmailAddress{prop:Req} = True
        ?defe:FromEmailAddress{prop:FontColor} = 65793
        ?defe:FromEmailAddress{prop:Color} = 8454143
    Else ! If ?defe:FromEmailAddress{prop:Req} = True
        ?defe:FromEmailAddress{prop:FontColor} = 65793
        ?defe:FromEmailAddress{prop:Color} = 16777215
    End ! If ?defe:FromEmailAddress{prop:Req} = True
    ?defe:FromEmailAddress{prop:Trn} = 0
    ?defe:FromEmailAddress{prop:FontStyle} = font:Bold
    ?defe:EmailSubject:Prompt{prop:FontColor} = -1
    ?defe:EmailSubject:Prompt{prop:Color} = 15066597
    If ?defe:EmailSubject{prop:ReadOnly} = True
        ?defe:EmailSubject{prop:FontColor} = 65793
        ?defe:EmailSubject{prop:Color} = 15066597
    Elsif ?defe:EmailSubject{prop:Req} = True
        ?defe:EmailSubject{prop:FontColor} = 65793
        ?defe:EmailSubject{prop:Color} = 8454143
    Else ! If ?defe:EmailSubject{prop:Req} = True
        ?defe:EmailSubject{prop:FontColor} = 65793
        ?defe:EmailSubject{prop:Color} = 16777215
    End ! If ?defe:EmailSubject{prop:Req} = True
    ?defe:EmailSubject{prop:Trn} = 0
    ?defe:EmailSubject{prop:FontStyle} = font:Bold
    ?defe:EmailRecipientList:Prompt{prop:FontColor} = -1
    ?defe:EmailRecipientList:Prompt{prop:Color} = 15066597
    If ?defe:EmailRecipientList{prop:ReadOnly} = True
        ?defe:EmailRecipientList{prop:FontColor} = 65793
        ?defe:EmailRecipientList{prop:Color} = 15066597
    Elsif ?defe:EmailRecipientList{prop:Req} = True
        ?defe:EmailRecipientList{prop:FontColor} = 65793
        ?defe:EmailRecipientList{prop:Color} = 8454143
    Else ! If ?defe:EmailRecipientList{prop:Req} = True
        ?defe:EmailRecipientList{prop:FontColor} = 65793
        ?defe:EmailRecipientList{prop:Color} = 16777215
    End ! If ?defe:EmailRecipientList{prop:Req} = True
    ?defe:EmailRecipientList{prop:Trn} = 0
    ?defe:EmailRecipientList{prop:FontStyle} = font:Bold
    ?Group3{prop:Font,3} = -1
    ?Group3{prop:Color} = 15066597
    ?Group3{prop:Trn} = 0
    ?defe:FTPServer:Prompt{prop:FontColor} = -1
    ?defe:FTPServer:Prompt{prop:Color} = 15066597
    If ?defe:FTPServer{prop:ReadOnly} = True
        ?defe:FTPServer{prop:FontColor} = 65793
        ?defe:FTPServer{prop:Color} = 15066597
    Elsif ?defe:FTPServer{prop:Req} = True
        ?defe:FTPServer{prop:FontColor} = 65793
        ?defe:FTPServer{prop:Color} = 8454143
    Else ! If ?defe:FTPServer{prop:Req} = True
        ?defe:FTPServer{prop:FontColor} = 65793
        ?defe:FTPServer{prop:Color} = 16777215
    End ! If ?defe:FTPServer{prop:Req} = True
    ?defe:FTPServer{prop:Trn} = 0
    ?defe:FTPServer{prop:FontStyle} = font:Bold
    ?defe:FTPUsername:Prompt{prop:FontColor} = -1
    ?defe:FTPUsername:Prompt{prop:Color} = 15066597
    If ?defe:FTPUsername{prop:ReadOnly} = True
        ?defe:FTPUsername{prop:FontColor} = 65793
        ?defe:FTPUsername{prop:Color} = 15066597
    Elsif ?defe:FTPUsername{prop:Req} = True
        ?defe:FTPUsername{prop:FontColor} = 65793
        ?defe:FTPUsername{prop:Color} = 8454143
    Else ! If ?defe:FTPUsername{prop:Req} = True
        ?defe:FTPUsername{prop:FontColor} = 65793
        ?defe:FTPUsername{prop:Color} = 16777215
    End ! If ?defe:FTPUsername{prop:Req} = True
    ?defe:FTPUsername{prop:Trn} = 0
    ?defe:FTPUsername{prop:FontStyle} = font:Bold
    ?defe:FTPPassword:Prompt{prop:FontColor} = -1
    ?defe:FTPPassword:Prompt{prop:Color} = 15066597
    If ?defe:FTPPassword{prop:ReadOnly} = True
        ?defe:FTPPassword{prop:FontColor} = 65793
        ?defe:FTPPassword{prop:Color} = 15066597
    Elsif ?defe:FTPPassword{prop:Req} = True
        ?defe:FTPPassword{prop:FontColor} = 65793
        ?defe:FTPPassword{prop:Color} = 8454143
    Else ! If ?defe:FTPPassword{prop:Req} = True
        ?defe:FTPPassword{prop:FontColor} = 65793
        ?defe:FTPPassword{prop:Color} = 16777215
    End ! If ?defe:FTPPassword{prop:Req} = True
    ?defe:FTPPassword{prop:Trn} = 0
    ?defe:FTPPassword{prop:FontStyle} = font:Bold
    ?defe:FTPExportPath:Prompt{prop:FontColor} = -1
    ?defe:FTPExportPath:Prompt{prop:Color} = 15066597
    If ?defe:FTPExportPath{prop:ReadOnly} = True
        ?defe:FTPExportPath{prop:FontColor} = 65793
        ?defe:FTPExportPath{prop:Color} = 15066597
    Elsif ?defe:FTPExportPath{prop:Req} = True
        ?defe:FTPExportPath{prop:FontColor} = 65793
        ?defe:FTPExportPath{prop:Color} = 8454143
    Else ! If ?defe:FTPExportPath{prop:Req} = True
        ?defe:FTPExportPath{prop:FontColor} = 65793
        ?defe:FTPExportPath{prop:Color} = 16777215
    End ! If ?defe:FTPExportPath{prop:Req} = True
    ?defe:FTPExportPath{prop:Trn} = 0
    ?defe:FTPExportPath{prop:FontStyle} = font:Bold
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?defe:DateFrom:Prompt{prop:FontColor} = -1
    ?defe:DateFrom:Prompt{prop:Color} = 15066597
    ?Prompt13{prop:FontColor} = -1
    ?Prompt13{prop:Color} = 15066597
    If ?defe:ExportDays{prop:ReadOnly} = True
        ?defe:ExportDays{prop:FontColor} = 65793
        ?defe:ExportDays{prop:Color} = 15066597
    Elsif ?defe:ExportDays{prop:Req} = True
        ?defe:ExportDays{prop:FontColor} = 65793
        ?defe:ExportDays{prop:Color} = 8454143
    Else ! If ?defe:ExportDays{prop:Req} = True
        ?defe:ExportDays{prop:FontColor} = 65793
        ?defe:ExportDays{prop:Color} = 16777215
    End ! If ?defe:ExportDays{prop:Req} = True
    ?defe:ExportDays{prop:Trn} = 0
    ?defe:ExportDays{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:DEFSIDEX.Open
    SET(DEFSIDEX)
    CASE Access:DEFSIDEX.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:DEFSIDEX.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('SIDJobExtractDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?defe:SMTPServer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:DEFSIDEX)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFSIDEX.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFSIDEX
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(DEFSIDEX)
      Access:DEFSIDEX.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSIDEX.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

