

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01009.INC'),ONCE        !Local module procedure declarations
                     END


New_Stock_Level PROCEDURE                             !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
sale_cost_temp       REAL
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
supplier_temp        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?supplier_temp
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
window               WINDOW('Stock Order'),AT(,,219,119),FONT('Tahoma',8,,),CENTER,ICON('PC.ICO'),TILED,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,84),USE(?Sheet1),SPREAD
                         TAB('Stock Order'),USE(?Tab1)
                           PROMPT('Quantity'),AT(9,20),USE(?Prompt7)
                           SPIN(@p<<<<<<<#p),AT(85,20,64,10),USE(quantity_temp),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR,RANGE(1,99999999)
                           PROMPT('Supplier'),AT(9,36),USE(?Prompt3)
                           COMBO(@s30),AT(85,36,124,10),USE(supplier_temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Reason'),AT(9,52),USE(?Prompt6)
                           TEXT,AT(85,52,124,28),USE(additional_notes_temp),FONT(,,,FONT:bold),UPR
                         END
                       END
                       PANEL,AT(4,92,212,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,96,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,96,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?quantity_temp{prop:ReadOnly} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 15066597
    Elsif ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 8454143
    Else ! If ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 16777215
    End ! If ?quantity_temp{prop:Req} = True
    ?quantity_temp{prop:Trn} = 0
    ?quantity_temp{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?supplier_temp{prop:ReadOnly} = True
        ?supplier_temp{prop:FontColor} = 65793
        ?supplier_temp{prop:Color} = 15066597
    Elsif ?supplier_temp{prop:Req} = True
        ?supplier_temp{prop:FontColor} = 65793
        ?supplier_temp{prop:Color} = 8454143
    Else ! If ?supplier_temp{prop:Req} = True
        ?supplier_temp{prop:FontColor} = 65793
        ?supplier_temp{prop:Color} = 16777215
    End ! If ?supplier_temp{prop:Req} = True
    ?supplier_temp{prop:Trn} = 0
    ?supplier_temp{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    If ?additional_notes_temp{prop:ReadOnly} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 15066597
    Elsif ?additional_notes_temp{prop:Req} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 8454143
    Else ! If ?additional_notes_temp{prop:Req} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 16777215
    End ! If ?additional_notes_temp{prop:Req} = True
    ?additional_notes_temp{prop:Trn} = 0
    ?additional_notes_temp{prop:FontStyle} = font:Bold
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('New_Stock_Level')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt7
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDPEND.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  access:stock.clearkey(sto:ref_number_key)
  sto:ref_number = glo:select1
  If access:stock.tryfetch(sto:ref_number_key)
      Return(level:fatal)
  End!If access:stock.tryfetch(sto:ref_number_key)
  quantity_temp = 1
  date_received_temp = Today()
  purchase_cost_Temp = STO:Purchase_Cost
  sale_cost_temp = STO:Sale_Cost
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FDCB5.Init(supplier_temp,?supplier_temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(sup:Company_Name_Key)
  FDCB5.AddField(sup:Company_Name,FDCB5.Q.sup:Company_Name)
  FDCB5.AddField(sup:RecordNumber,FDCB5.Q.sup:RecordNumber)
  FDCB5.AddUpdateField(sup:Company_Name,supplier_temp)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDPEND.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If access:ordpend.primerecord() = Level:Benign
          ope:part_ref_number = sto:ref_number
          ope:job_number      = ''
          ope:part_type       = 'STO'
          ope:supplier        = sto:supplier
          ope:part_number     = sto:part_number
          ope:description     = sto:description
          ope:quantity        = quantity_temp
          access:ordpend.insert()
      End!If ~access:ordpend.primteautoinc()
      sto:quantity_to_order += quantity_temp
      sto:pending_ref_number   = ope:ref_number
      access:stock.update()
      Post(Event:closeWindow)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

