

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01040.INC'),ONCE        !Local module procedure declarations
                     END


BouncerDefaults PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:CRepairType      STRING(30)
tmp:WRepairType      STRING(30)
window               WINDOW('Bouncer Defaults'),AT(,,227,107),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,220,72),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('When a Bouncer is "Submitted", please select the Repair Type that the job will a' &|
   'utomatically change to.'),AT(8,16,200,20),USE(?Prompt3),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Charge. Repair Type'),AT(9,44),USE(?tmp:CRepairType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,44,124,10),USE(tmp:CRepairType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Chargeable Repair Type'),TIP('Chargeable Repair Type'),ALRT(DownKey),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                           BUTTON,AT(211,44,10,10),USE(?LookupChargeableRepairType),SKIP,ICON('List3.ico')
                           PROMPT('Warranty Repair Type'),AT(8,60),USE(?tmp:WRepairType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,60,124,10),USE(tmp:WRepairType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Warranty Repair Type'),TIP('Warranty Repair Type'),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,60,10,10),USE(?LookupWarrantyRepairType),SKIP,ICON('List3.ico')
                         END
                       END
                       PANEL,AT(4,80,220,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(108,84,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(164,84,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
look:tmp:CRepairType                Like(tmp:CRepairType)
look:tmp:WRepairType                Like(tmp:WRepairType)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?tmp:CRepairType:Prompt{prop:FontColor} = -1
    ?tmp:CRepairType:Prompt{prop:Color} = 15066597
    If ?tmp:CRepairType{prop:ReadOnly} = True
        ?tmp:CRepairType{prop:FontColor} = 65793
        ?tmp:CRepairType{prop:Color} = 15066597
    Elsif ?tmp:CRepairType{prop:Req} = True
        ?tmp:CRepairType{prop:FontColor} = 65793
        ?tmp:CRepairType{prop:Color} = 8454143
    Else ! If ?tmp:CRepairType{prop:Req} = True
        ?tmp:CRepairType{prop:FontColor} = 65793
        ?tmp:CRepairType{prop:Color} = 16777215
    End ! If ?tmp:CRepairType{prop:Req} = True
    ?tmp:CRepairType{prop:Trn} = 0
    ?tmp:CRepairType{prop:FontStyle} = font:Bold
    ?tmp:WRepairType:Prompt{prop:FontColor} = -1
    ?tmp:WRepairType:Prompt{prop:Color} = 15066597
    If ?tmp:WRepairType{prop:ReadOnly} = True
        ?tmp:WRepairType{prop:FontColor} = 65793
        ?tmp:WRepairType{prop:Color} = 15066597
    Elsif ?tmp:WRepairType{prop:Req} = True
        ?tmp:WRepairType{prop:FontColor} = 65793
        ?tmp:WRepairType{prop:Color} = 8454143
    Else ! If ?tmp:WRepairType{prop:Req} = True
        ?tmp:WRepairType{prop:FontColor} = 65793
        ?tmp:WRepairType{prop:Color} = 16777215
    End ! If ?tmp:WRepairType{prop:Req} = True
    ?tmp:WRepairType{prop:Trn} = 0
    ?tmp:WRepairType{prop:FontStyle} = font:Bold
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BouncerDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:REPTYDEF.Open
  SELF.FilesOpened = True
  tmp:CRepairType = GETINI('BOUNCER','ChargeableRepairType',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:WRepairType = GETINI('BOUNCER','WarrantyRepairType',,CLIP(PATH())&'\SB2KDEF.INI')
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  IF ?tmp:CRepairType{Prop:Tip} AND ~?LookupChargeableRepairType{Prop:Tip}
     ?LookupChargeableRepairType{Prop:Tip} = 'Select ' & ?tmp:CRepairType{Prop:Tip}
  END
  IF ?tmp:CRepairType{Prop:Msg} AND ~?LookupChargeableRepairType{Prop:Msg}
     ?LookupChargeableRepairType{Prop:Msg} = 'Select ' & ?tmp:CRepairType{Prop:Msg}
  END
  IF ?tmp:WRepairType{Prop:Tip} AND ~?LookupWarrantyRepairType{Prop:Tip}
     ?LookupWarrantyRepairType{Prop:Tip} = 'Select ' & ?tmp:WRepairType{Prop:Tip}
  END
  IF ?tmp:WRepairType{Prop:Msg} AND ~?LookupWarrantyRepairType{Prop:Msg}
     ?LookupWarrantyRepairType{Prop:Msg} = 'Select ' & ?tmp:WRepairType{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPTYDEF.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickChargeableRepairTypes
      PickWarrantyRepairTypes
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:CRepairType
      IF tmp:CRepairType OR ?tmp:CRepairType{Prop:Req}
        rtd:Repair_Type = tmp:CRepairType
        rtd:Chargeable = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:CRepairType        = tmp:CRepairType
        IF Access:REPTYDEF.TryFetch(rtd:Chargeable_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:CRepairType = rtd:Repair_Type
          ELSE
            CLEAR(rtd:Chargeable)
            !Restore Lookup On Error
            tmp:CRepairType = look:tmp:CRepairType
            SELECT(?tmp:CRepairType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupChargeableRepairType
      ThisWindow.Update
      rtd:Repair_Type = tmp:CRepairType
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:CRepairType = rtd:Repair_Type
          Select(?+1)
      ELSE
          Select(?tmp:CRepairType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:CRepairType)
    OF ?tmp:WRepairType
      IF tmp:WRepairType OR ?tmp:WRepairType{Prop:Req}
        rtd:Repair_Type = tmp:WRepairType
        rtd:Warranty = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:WRepairType        = tmp:WRepairType
        IF Access:REPTYDEF.TryFetch(rtd:Warranty_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:WRepairType = rtd:Repair_Type
          ELSE
            CLEAR(rtd:Warranty)
            !Restore Lookup On Error
            tmp:WRepairType = look:tmp:WRepairType
            SELECT(?tmp:WRepairType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWarrantyRepairType
      ThisWindow.Update
      rtd:Repair_Type = tmp:WRepairType
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:WRepairType = rtd:Repair_Type
          Select(?+1)
      ELSE
          Select(?tmp:WRepairType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:WRepairType)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      PUTINI('BOUNCER','ChargeableRepairType',tmp:CRepairType,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI('BOUNCER','WarrantyRepairType',tmp:WRepairType,CLIP(PATH()) & '\SB2KDEF.INI')
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:CRepairType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:CRepairType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Chargeable Repair Type')
             Post(Event:Accepted,?LookupChargeableRepairType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupChargeableRepairType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:CRepairType, AlertKey)
    END
  OF ?tmp:WRepairType
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:WRepairType, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Warranty Repair Type')
             Post(Event:Accepted,?LookupWarrantyRepairType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupWarrantyRepairType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:WRepairType, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

