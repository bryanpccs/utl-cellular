

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01047.INC'),ONCE        !Local module procedure declarations
                     END


StorePerfReportDefaults PROCEDURE                     !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
FTPServer            STRING(80)
FTPUsername          STRING(30)
FTPPassword          STRING(30)
FTPPath              STRING(254)
window               WINDOW('SID Store Performance Report Defaults'),AT(,,308,128),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,2,300,98),USE(?Sheet1),SPREAD
                         TAB('SID Store Performance Report Defaults'),USE(?SIDStorePerfReportDefaultsTab)
                           GROUP('FTP Defaults'),AT(8,18,292,76),USE(?FTPGroup),BOXED
                           END
                           PROMPT('FTP Server'),AT(16,28),USE(?FTPServer:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s80),AT(92,28,124,10),USE(FTPServer),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Server'),TIP('FTP Server')
                           PROMPT('FTP Username'),AT(16,44),USE(?FTPUsername:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(92,44,124,10),USE(FTPUsername),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Username'),TIP('FTP Username')
                           PROMPT('FTP Password'),AT(16,60),USE(?FTPPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(92,60,124,10),USE(FTPPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Password'),TIP('FTP Password'),PASSWORD
                           PROMPT('FTP Path'),AT(16,76),USE(?FTPPath:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s254),AT(92,76,124,10),USE(FTPPath),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Path'),TIP('FTP Path')
                         END
                       END
                       BUTTON('&OK'),AT(188,106,56,16),USE(?OkButton),TRN,LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(244,106,56,16),USE(?CancelButton),TRN,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,102,300,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
insert               byte

SQLDefaults             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('DEFAULTS'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD
PerfRepFTPServer                cstring(80), Name('PerfRepFTPServer')
PerfRepFTPUserName              cstring(30), Name('PerfRepFTPUserName')
PerfRepFTPPassword              cstring(30), Name('PerfRepFTPPassword')
PerfRepFTPPath                  cstring(254), Name('PerfRepFTPPath')
                        END
                    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?SIDStorePerfReportDefaultsTab{prop:Color} = 15066597
    ?FTPGroup{prop:Font,3} = -1
    ?FTPGroup{prop:Color} = 15066597
    ?FTPGroup{prop:Trn} = 0
    ?FTPServer:Prompt{prop:FontColor} = -1
    ?FTPServer:Prompt{prop:Color} = 15066597
    If ?FTPServer{prop:ReadOnly} = True
        ?FTPServer{prop:FontColor} = 65793
        ?FTPServer{prop:Color} = 15066597
    Elsif ?FTPServer{prop:Req} = True
        ?FTPServer{prop:FontColor} = 65793
        ?FTPServer{prop:Color} = 8454143
    Else ! If ?FTPServer{prop:Req} = True
        ?FTPServer{prop:FontColor} = 65793
        ?FTPServer{prop:Color} = 16777215
    End ! If ?FTPServer{prop:Req} = True
    ?FTPServer{prop:Trn} = 0
    ?FTPServer{prop:FontStyle} = font:Bold
    ?FTPUsername:Prompt{prop:FontColor} = -1
    ?FTPUsername:Prompt{prop:Color} = 15066597
    If ?FTPUsername{prop:ReadOnly} = True
        ?FTPUsername{prop:FontColor} = 65793
        ?FTPUsername{prop:Color} = 15066597
    Elsif ?FTPUsername{prop:Req} = True
        ?FTPUsername{prop:FontColor} = 65793
        ?FTPUsername{prop:Color} = 8454143
    Else ! If ?FTPUsername{prop:Req} = True
        ?FTPUsername{prop:FontColor} = 65793
        ?FTPUsername{prop:Color} = 16777215
    End ! If ?FTPUsername{prop:Req} = True
    ?FTPUsername{prop:Trn} = 0
    ?FTPUsername{prop:FontStyle} = font:Bold
    ?FTPPassword:Prompt{prop:FontColor} = -1
    ?FTPPassword:Prompt{prop:Color} = 15066597
    If ?FTPPassword{prop:ReadOnly} = True
        ?FTPPassword{prop:FontColor} = 65793
        ?FTPPassword{prop:Color} = 15066597
    Elsif ?FTPPassword{prop:Req} = True
        ?FTPPassword{prop:FontColor} = 65793
        ?FTPPassword{prop:Color} = 8454143
    Else ! If ?FTPPassword{prop:Req} = True
        ?FTPPassword{prop:FontColor} = 65793
        ?FTPPassword{prop:Color} = 16777215
    End ! If ?FTPPassword{prop:Req} = True
    ?FTPPassword{prop:Trn} = 0
    ?FTPPassword{prop:FontStyle} = font:Bold
    ?FTPPath:Prompt{prop:FontColor} = -1
    ?FTPPath:Prompt{prop:Color} = 15066597
    If ?FTPPath{prop:ReadOnly} = True
        ?FTPPath{prop:FontColor} = 65793
        ?FTPPath{prop:Color} = 15066597
    Elsif ?FTPPath{prop:Req} = True
        ?FTPPath{prop:FontColor} = 65793
        ?FTPPath{prop:Color} = 8454143
    Else ! If ?FTPPath{prop:Req} = True
        ?FTPPath{prop:FontColor} = 65793
        ?FTPPath{prop:Color} = 16777215
    End ! If ?FTPPath{prop:Req} = True
    ?FTPPath{prop:Trn} = 0
    ?FTPPath{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('StorePerfReportDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?FTPServer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  iniFilePath     = clip(path()) & '\Webimp.ini'
  ConnectionStr   = getini('Defaults', 'Connection', '', clip(iniFilePath))
  
  open(SQLDefaults)
  
  SQLDefaults{Prop:SQL} = 'SELECT PerfRepFTPServer, PerfRepFTPUserName, PerfRepFTPPassword, PerfRepFTPPath FROM Defaults WHERE DefaultID = 1'
  
  next(SQLDefaults)
  if error()
      insert = true
  else
      FTPServer = sql:PerfRepFTPServer
      FTPUsername = sql:PerfRepFTPUserName
      FTPPassword = sql:PerfRepFTPPassword
      FTPPath = sql:PerfRepFTPPath
  end
  
  close(SQLDefaults)
  
      
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      open(SQLDefaults)
      
      if insert
          SQLDefaults{Prop:SQL} = 'INSERT INTO Defaults (PerfRepFTPServer, PerfRepFTPUserName, PerfRepFTPPassword, PerfRepFTPPath) VALUES(''' & clip(FTPServer) & ''',''' & clip(FTPUsername) & ''',''' & clip(FTPPassword) & ''',''' & clip(FTPPath) & ''')'
      else
          SQLDefaults{Prop:SQL} = 'UPDATE Defaults SET PerfRepFTPServer = ''' & clip(FTPServer) & ''', PerfRepFTPUserName = ''' & clip(FTPUsername) & ''', PerfRepFTPPassword = ''' & clip(FTPPassword) & ''', PerfRepFTPPath = ''' & clip(FTPPath) & ''' WHERE DefaultID = 1'
      end
      
      close(SQLDefaults)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

