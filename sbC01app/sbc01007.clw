

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01007.INC'),ONCE        !Local module procedure declarations
                     END


Defaults_Window PROCEDURE                             !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
RecipAddress         STRING(10000)
MsgSubject           STRING(255)
MsgNoteText          STRING(10000)
AttachList           STRING(1000)
CCAddress            STRING(10000)
BCCAddress           STRING(10000)
Connection           STRING('Default {20}')
Disconnect           BYTE(0)
RtnVal               BYTE(0)
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
window               WINDOW('Licence Defaults'),AT(,,283,271),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,276,236),USE(?Sheet1),IMM,SPREAD
                         TAB('License Address'),USE(?Tab1),REQ
                           GROUP('Hours Of Business'),AT(8,164,216,72),USE(?Group1),BOXED
                             PROMPT('Start Time'),AT(16,176),USE(?DEF:Start_Work_Hours:Prompt),TRN
                             ENTRY(@t1),AT(84,176,67,10),USE(def:Start_Work_Hours),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('(HH:MM)'),AT(156,176),USE(?Prompt9)
                             PROMPT('End Time'),AT(16,192),USE(?DEF:End_Work_Hours:Prompt),TRN
                             ENTRY(@t1),AT(84,192,67,10),USE(def:End_Work_Hours),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('(HH:MM)'),AT(156,192),USE(?Prompt10)
                             CHECK('Include Saturday'),AT(84,208),USE(def:Include_Saturday),VALUE('YES','NO')
                             CHECK('Include Sunday'),AT(84,220),USE(def:Include_Sunday),VALUE('YES','NO')
                           END
                           GROUP('License Address'),AT(4,20,228,144),USE(?LicenseAddress)
                             PROMPT('Company Name'),AT(8,20),USE(?DEF:User_Name:Prompt),TRN
                             ENTRY(@s30),AT(80,20,136,10),USE(def:User_Name),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Address'),AT(8,36),USE(?DEF:Address_Line1:Prompt),TRN
                             ENTRY(@s30),AT(80,36,136,10),USE(def:Address_Line1),FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s30),AT(80,52,136,10),USE(def:Address_Line2),FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s30),AT(80,68,136,10),USE(def:Address_Line3),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Postcode'),AT(8,84),USE(?DEF:Postcode:Prompt),TRN
                             ENTRY(@s15),AT(80,84,67,10),USE(def:Postcode),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Telephone Number'),AT(8,100),USE(?DEF:Telephone_Number:Prompt),TRN
                             ENTRY(@s15),AT(80,100,67,10),USE(def:Telephone_Number),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Fax Number'),AT(8,116),USE(?DEF:Fax_Number:Prompt),TRN
                             ENTRY(@s15),AT(80,116,67,10),USE(def:Fax_Number),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('V.A.T. Number'),AT(8,132),USE(?DEF:VAT_Number:Prompt),TRN
                             ENTRY(@s30),AT(80,132,67,10),USE(def:VAT_Number),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Email Address'),AT(8,148),USE(?def:EmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s255),AT(80,148,136,10),USE(def:EmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           END
                         END
                         TAB('Invoice Address'),USE(?InvoiceAddressTab)
                           GROUP('Invoice Address'),AT(4,20,228,144),USE(?InvoiceAddress)
                             PROMPT('Company Name'),AT(8,21),USE(?DEF:Invoice_Company_Name:Prompt)
                             ENTRY(@s30),AT(80,21,136,10),USE(def:Invoice_Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Address'),AT(8,37),USE(?DEF:Invoice_Address_Line1:Prompt)
                             ENTRY(@s30),AT(80,37,136,10),USE(def:Invoice_Address_Line1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             ENTRY(@s30),AT(80,53,136,10),USE(def:Invoice_Address_Line2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             ENTRY(@s30),AT(80,69,136,10),USE(def:Invoice_Address_Line3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Postcode'),AT(8,84),USE(?DEF:Invoice_Postcode:Prompt),TRN
                             ENTRY(@s15),AT(80,84,64,10),USE(def:Invoice_Postcode),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             PROMPT('Telephone Number'),AT(8,100),USE(?DEF:Invoice_Telephone_Number:Prompt),TRN
                             ENTRY(@s15),AT(80,100,64,10),USE(def:Invoice_Telephone_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             PROMPT('Fax Number'),AT(8,116),USE(?DEF:Invoice_Fax_Number:Prompt),TRN
                             ENTRY(@s15),AT(80,116,64,10),USE(def:Invoice_Fax_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             PROMPT('V.A.T. Number'),AT(8,132),USE(?DEF:Invoice_VAT_Number:Prompt),TRN
                             ENTRY(@s30),AT(80,132,64,10),USE(def:Invoice_VAT_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             PROMPT('Email Address'),AT(8,148),USE(?def:InvoiceEmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s255),AT(80,148,136,10),USE(def:InvoiceEmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           END
                         END
                         TAB('Parts Order Address'),USE(?PartsOrderTab)
                           GROUP('Parts Order Address'),AT(4,20,228,144),USE(?PartsOrderAddress:2)
                             PROMPT('Company Name'),AT(8,21),USE(?def:OrderCompanyName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(80,21,136,10),USE(def:OrderCompanyName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Company Name'),TIP('Company Name'),UPR
                             PROMPT('Address'),AT(8,37),USE(?def:OrderAddressLine1:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(80,37,136,10),USE(def:OrderAddressLine1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Address'),TIP('Address'),UPR
                             ENTRY(@s30),AT(80,53,136,10),USE(def:OrderAddressLine2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Address'),TIP('Address'),UPR
                             ENTRY(@s30),AT(80,69,136,10),USE(def:OrderAddressLine3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Address'),TIP('Address'),UPR
                             PROMPT('Postcode'),AT(8,85),USE(?def:OrderPostcode:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(80,85,64,10),USE(def:OrderPostcode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Postcode'),TIP('Postcode'),UPR
                             PROMPT('Telephone Number'),AT(8,101),USE(?def:OrderTelephoneNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(80,101,64,10),USE(def:OrderTelephoneNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Telephone Number'),TIP('Telephone Number'),UPR
                             PROMPT('Fax Number'),AT(8,117),USE(?def:OrderFaxNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(80,117,64,10),USE(def:OrderFaxNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fax Number'),TIP('Fax Number'),UPR
                             PROMPT('Email Address'),AT(8,148),USE(?def:OrderEmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s255),AT(80,148,136,10),USE(def:OrderEmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           END
                         END
                         TAB('Euro Address'),USE(?EuroAddressTab)
                           GROUP('Euro Address'),AT(4,20,228,144),USE(?EuroAddressDetails)
                             PROMPT('Company Name'),AT(8,22),USE(?def:OrderCompanyName:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(80,20,136,10),USE(de2:Inv2CompanyName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Company Name'),TIP('Company Name'),UPR
                             PROMPT('Address'),AT(8,36),USE(?def:OrderAddressLine1:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(80,36,136,10),USE(de2:Inv2AddressLine1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Address'),TIP('Address'),UPR
                             ENTRY(@s30),AT(80,52,136,10),USE(de2:Inv2AddressLine2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Address'),TIP('Address'),UPR
                             ENTRY(@s30),AT(80,68,136,10),USE(de2:Inv2AddressLine3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Address'),TIP('Address'),UPR
                             PROMPT('Postcode'),AT(8,84),USE(?def:OrderPostcode:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s15),AT(80,84,64,10),USE(de2:Inv2Postcode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Postcode'),TIP('Postcode'),UPR
                             PROMPT('Telephone Number'),AT(8,100),USE(?def:OrderTelephoneNumber:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s15),AT(80,100,64,10),USE(de2:Inv2TelephoneNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Telephone Number'),TIP('Telephone Number'),UPR
                             PROMPT('Fax Number'),AT(8,116),USE(?def:OrderFaxNumber:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s15),AT(80,116,64,10),USE(de2:Inv2FaxNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fax Number'),TIP('Fax Number'),UPR
                             PROMPT('V.A.T. Number'),AT(8,132),USE(?DE2:Inv2VATNumber:Prompt),TRN
                             ENTRY(@s30),AT(80,132,124,10),USE(de2:Inv2VATNumber),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             PROMPT('Email Address'),AT(8,148),USE(?def:OrderEmailAddress:Prompt:2),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s255),AT(80,148,136,10),USE(de2:Inv2EmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                           END
                         END
                         TAB('?'),USE(?BackgroundTab),HIDE
                           PROMPT('Current Version'),AT(8,20),USE(?DEF:Version_Number:Prompt),TRN
                           ENTRY(@s10),AT(84,20,124,10),USE(def:Version_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('License Number'),AT(8,36),USE(?DEF:License_Number:Prompt),TRN
                           ENTRY(@s10),AT(84,36,124,10),USE(def:License_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Maximum Users'),AT(8,52),USE(?DEF:Maximum_Users:Prompt),TRN
                           ENTRY(@n4),AT(84,52,64,10),USE(def:Maximum_Users),DECIMAL(14),FONT('Tahoma',8,,FONT:bold),UPR
                           CHECK('Use Alternative Invoice Address'),AT(84,80),USE(def:Use_Invoice_Address),HIDE,VALUE('YES','NO')
                           CHECK('Use Address For Parts Orders'),AT(84,88),USE(def:Use_For_Order),HIDE,VALUE('YES','NO')
                         END
                       END
                       BUTTON('&OK'),AT(164,248,56,16),USE(?OkButton),LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(220,248,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,244,276,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?DEF:Start_Work_Hours:Prompt{prop:FontColor} = -1
    ?DEF:Start_Work_Hours:Prompt{prop:Color} = 15066597
    If ?def:Start_Work_Hours{prop:ReadOnly} = True
        ?def:Start_Work_Hours{prop:FontColor} = 65793
        ?def:Start_Work_Hours{prop:Color} = 15066597
    Elsif ?def:Start_Work_Hours{prop:Req} = True
        ?def:Start_Work_Hours{prop:FontColor} = 65793
        ?def:Start_Work_Hours{prop:Color} = 8454143
    Else ! If ?def:Start_Work_Hours{prop:Req} = True
        ?def:Start_Work_Hours{prop:FontColor} = 65793
        ?def:Start_Work_Hours{prop:Color} = 16777215
    End ! If ?def:Start_Work_Hours{prop:Req} = True
    ?def:Start_Work_Hours{prop:Trn} = 0
    ?def:Start_Work_Hours{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?DEF:End_Work_Hours:Prompt{prop:FontColor} = -1
    ?DEF:End_Work_Hours:Prompt{prop:Color} = 15066597
    If ?def:End_Work_Hours{prop:ReadOnly} = True
        ?def:End_Work_Hours{prop:FontColor} = 65793
        ?def:End_Work_Hours{prop:Color} = 15066597
    Elsif ?def:End_Work_Hours{prop:Req} = True
        ?def:End_Work_Hours{prop:FontColor} = 65793
        ?def:End_Work_Hours{prop:Color} = 8454143
    Else ! If ?def:End_Work_Hours{prop:Req} = True
        ?def:End_Work_Hours{prop:FontColor} = 65793
        ?def:End_Work_Hours{prop:Color} = 16777215
    End ! If ?def:End_Work_Hours{prop:Req} = True
    ?def:End_Work_Hours{prop:Trn} = 0
    ?def:End_Work_Hours{prop:FontStyle} = font:Bold
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    ?def:Include_Saturday{prop:Font,3} = -1
    ?def:Include_Saturday{prop:Color} = 15066597
    ?def:Include_Saturday{prop:Trn} = 0
    ?def:Include_Sunday{prop:Font,3} = -1
    ?def:Include_Sunday{prop:Color} = 15066597
    ?def:Include_Sunday{prop:Trn} = 0
    ?LicenseAddress{prop:Font,3} = -1
    ?LicenseAddress{prop:Color} = 15066597
    ?LicenseAddress{prop:Trn} = 0
    ?DEF:User_Name:Prompt{prop:FontColor} = -1
    ?DEF:User_Name:Prompt{prop:Color} = 15066597
    If ?def:User_Name{prop:ReadOnly} = True
        ?def:User_Name{prop:FontColor} = 65793
        ?def:User_Name{prop:Color} = 15066597
    Elsif ?def:User_Name{prop:Req} = True
        ?def:User_Name{prop:FontColor} = 65793
        ?def:User_Name{prop:Color} = 8454143
    Else ! If ?def:User_Name{prop:Req} = True
        ?def:User_Name{prop:FontColor} = 65793
        ?def:User_Name{prop:Color} = 16777215
    End ! If ?def:User_Name{prop:Req} = True
    ?def:User_Name{prop:Trn} = 0
    ?def:User_Name{prop:FontStyle} = font:Bold
    ?DEF:Address_Line1:Prompt{prop:FontColor} = -1
    ?DEF:Address_Line1:Prompt{prop:Color} = 15066597
    If ?def:Address_Line1{prop:ReadOnly} = True
        ?def:Address_Line1{prop:FontColor} = 65793
        ?def:Address_Line1{prop:Color} = 15066597
    Elsif ?def:Address_Line1{prop:Req} = True
        ?def:Address_Line1{prop:FontColor} = 65793
        ?def:Address_Line1{prop:Color} = 8454143
    Else ! If ?def:Address_Line1{prop:Req} = True
        ?def:Address_Line1{prop:FontColor} = 65793
        ?def:Address_Line1{prop:Color} = 16777215
    End ! If ?def:Address_Line1{prop:Req} = True
    ?def:Address_Line1{prop:Trn} = 0
    ?def:Address_Line1{prop:FontStyle} = font:Bold
    If ?def:Address_Line2{prop:ReadOnly} = True
        ?def:Address_Line2{prop:FontColor} = 65793
        ?def:Address_Line2{prop:Color} = 15066597
    Elsif ?def:Address_Line2{prop:Req} = True
        ?def:Address_Line2{prop:FontColor} = 65793
        ?def:Address_Line2{prop:Color} = 8454143
    Else ! If ?def:Address_Line2{prop:Req} = True
        ?def:Address_Line2{prop:FontColor} = 65793
        ?def:Address_Line2{prop:Color} = 16777215
    End ! If ?def:Address_Line2{prop:Req} = True
    ?def:Address_Line2{prop:Trn} = 0
    ?def:Address_Line2{prop:FontStyle} = font:Bold
    If ?def:Address_Line3{prop:ReadOnly} = True
        ?def:Address_Line3{prop:FontColor} = 65793
        ?def:Address_Line3{prop:Color} = 15066597
    Elsif ?def:Address_Line3{prop:Req} = True
        ?def:Address_Line3{prop:FontColor} = 65793
        ?def:Address_Line3{prop:Color} = 8454143
    Else ! If ?def:Address_Line3{prop:Req} = True
        ?def:Address_Line3{prop:FontColor} = 65793
        ?def:Address_Line3{prop:Color} = 16777215
    End ! If ?def:Address_Line3{prop:Req} = True
    ?def:Address_Line3{prop:Trn} = 0
    ?def:Address_Line3{prop:FontStyle} = font:Bold
    ?DEF:Postcode:Prompt{prop:FontColor} = -1
    ?DEF:Postcode:Prompt{prop:Color} = 15066597
    If ?def:Postcode{prop:ReadOnly} = True
        ?def:Postcode{prop:FontColor} = 65793
        ?def:Postcode{prop:Color} = 15066597
    Elsif ?def:Postcode{prop:Req} = True
        ?def:Postcode{prop:FontColor} = 65793
        ?def:Postcode{prop:Color} = 8454143
    Else ! If ?def:Postcode{prop:Req} = True
        ?def:Postcode{prop:FontColor} = 65793
        ?def:Postcode{prop:Color} = 16777215
    End ! If ?def:Postcode{prop:Req} = True
    ?def:Postcode{prop:Trn} = 0
    ?def:Postcode{prop:FontStyle} = font:Bold
    ?DEF:Telephone_Number:Prompt{prop:FontColor} = -1
    ?DEF:Telephone_Number:Prompt{prop:Color} = 15066597
    If ?def:Telephone_Number{prop:ReadOnly} = True
        ?def:Telephone_Number{prop:FontColor} = 65793
        ?def:Telephone_Number{prop:Color} = 15066597
    Elsif ?def:Telephone_Number{prop:Req} = True
        ?def:Telephone_Number{prop:FontColor} = 65793
        ?def:Telephone_Number{prop:Color} = 8454143
    Else ! If ?def:Telephone_Number{prop:Req} = True
        ?def:Telephone_Number{prop:FontColor} = 65793
        ?def:Telephone_Number{prop:Color} = 16777215
    End ! If ?def:Telephone_Number{prop:Req} = True
    ?def:Telephone_Number{prop:Trn} = 0
    ?def:Telephone_Number{prop:FontStyle} = font:Bold
    ?DEF:Fax_Number:Prompt{prop:FontColor} = -1
    ?DEF:Fax_Number:Prompt{prop:Color} = 15066597
    If ?def:Fax_Number{prop:ReadOnly} = True
        ?def:Fax_Number{prop:FontColor} = 65793
        ?def:Fax_Number{prop:Color} = 15066597
    Elsif ?def:Fax_Number{prop:Req} = True
        ?def:Fax_Number{prop:FontColor} = 65793
        ?def:Fax_Number{prop:Color} = 8454143
    Else ! If ?def:Fax_Number{prop:Req} = True
        ?def:Fax_Number{prop:FontColor} = 65793
        ?def:Fax_Number{prop:Color} = 16777215
    End ! If ?def:Fax_Number{prop:Req} = True
    ?def:Fax_Number{prop:Trn} = 0
    ?def:Fax_Number{prop:FontStyle} = font:Bold
    ?DEF:VAT_Number:Prompt{prop:FontColor} = -1
    ?DEF:VAT_Number:Prompt{prop:Color} = 15066597
    If ?def:VAT_Number{prop:ReadOnly} = True
        ?def:VAT_Number{prop:FontColor} = 65793
        ?def:VAT_Number{prop:Color} = 15066597
    Elsif ?def:VAT_Number{prop:Req} = True
        ?def:VAT_Number{prop:FontColor} = 65793
        ?def:VAT_Number{prop:Color} = 8454143
    Else ! If ?def:VAT_Number{prop:Req} = True
        ?def:VAT_Number{prop:FontColor} = 65793
        ?def:VAT_Number{prop:Color} = 16777215
    End ! If ?def:VAT_Number{prop:Req} = True
    ?def:VAT_Number{prop:Trn} = 0
    ?def:VAT_Number{prop:FontStyle} = font:Bold
    ?def:EmailAddress:Prompt{prop:FontColor} = -1
    ?def:EmailAddress:Prompt{prop:Color} = 15066597
    If ?def:EmailAddress{prop:ReadOnly} = True
        ?def:EmailAddress{prop:FontColor} = 65793
        ?def:EmailAddress{prop:Color} = 15066597
    Elsif ?def:EmailAddress{prop:Req} = True
        ?def:EmailAddress{prop:FontColor} = 65793
        ?def:EmailAddress{prop:Color} = 8454143
    Else ! If ?def:EmailAddress{prop:Req} = True
        ?def:EmailAddress{prop:FontColor} = 65793
        ?def:EmailAddress{prop:Color} = 16777215
    End ! If ?def:EmailAddress{prop:Req} = True
    ?def:EmailAddress{prop:Trn} = 0
    ?def:EmailAddress{prop:FontStyle} = font:Bold
    ?InvoiceAddressTab{prop:Color} = 15066597
    ?InvoiceAddress{prop:Font,3} = -1
    ?InvoiceAddress{prop:Color} = 15066597
    ?InvoiceAddress{prop:Trn} = 0
    ?DEF:Invoice_Company_Name:Prompt{prop:FontColor} = -1
    ?DEF:Invoice_Company_Name:Prompt{prop:Color} = 15066597
    If ?def:Invoice_Company_Name{prop:ReadOnly} = True
        ?def:Invoice_Company_Name{prop:FontColor} = 65793
        ?def:Invoice_Company_Name{prop:Color} = 15066597
    Elsif ?def:Invoice_Company_Name{prop:Req} = True
        ?def:Invoice_Company_Name{prop:FontColor} = 65793
        ?def:Invoice_Company_Name{prop:Color} = 8454143
    Else ! If ?def:Invoice_Company_Name{prop:Req} = True
        ?def:Invoice_Company_Name{prop:FontColor} = 65793
        ?def:Invoice_Company_Name{prop:Color} = 16777215
    End ! If ?def:Invoice_Company_Name{prop:Req} = True
    ?def:Invoice_Company_Name{prop:Trn} = 0
    ?def:Invoice_Company_Name{prop:FontStyle} = font:Bold
    ?DEF:Invoice_Address_Line1:Prompt{prop:FontColor} = -1
    ?DEF:Invoice_Address_Line1:Prompt{prop:Color} = 15066597
    If ?def:Invoice_Address_Line1{prop:ReadOnly} = True
        ?def:Invoice_Address_Line1{prop:FontColor} = 65793
        ?def:Invoice_Address_Line1{prop:Color} = 15066597
    Elsif ?def:Invoice_Address_Line1{prop:Req} = True
        ?def:Invoice_Address_Line1{prop:FontColor} = 65793
        ?def:Invoice_Address_Line1{prop:Color} = 8454143
    Else ! If ?def:Invoice_Address_Line1{prop:Req} = True
        ?def:Invoice_Address_Line1{prop:FontColor} = 65793
        ?def:Invoice_Address_Line1{prop:Color} = 16777215
    End ! If ?def:Invoice_Address_Line1{prop:Req} = True
    ?def:Invoice_Address_Line1{prop:Trn} = 0
    ?def:Invoice_Address_Line1{prop:FontStyle} = font:Bold
    If ?def:Invoice_Address_Line2{prop:ReadOnly} = True
        ?def:Invoice_Address_Line2{prop:FontColor} = 65793
        ?def:Invoice_Address_Line2{prop:Color} = 15066597
    Elsif ?def:Invoice_Address_Line2{prop:Req} = True
        ?def:Invoice_Address_Line2{prop:FontColor} = 65793
        ?def:Invoice_Address_Line2{prop:Color} = 8454143
    Else ! If ?def:Invoice_Address_Line2{prop:Req} = True
        ?def:Invoice_Address_Line2{prop:FontColor} = 65793
        ?def:Invoice_Address_Line2{prop:Color} = 16777215
    End ! If ?def:Invoice_Address_Line2{prop:Req} = True
    ?def:Invoice_Address_Line2{prop:Trn} = 0
    ?def:Invoice_Address_Line2{prop:FontStyle} = font:Bold
    If ?def:Invoice_Address_Line3{prop:ReadOnly} = True
        ?def:Invoice_Address_Line3{prop:FontColor} = 65793
        ?def:Invoice_Address_Line3{prop:Color} = 15066597
    Elsif ?def:Invoice_Address_Line3{prop:Req} = True
        ?def:Invoice_Address_Line3{prop:FontColor} = 65793
        ?def:Invoice_Address_Line3{prop:Color} = 8454143
    Else ! If ?def:Invoice_Address_Line3{prop:Req} = True
        ?def:Invoice_Address_Line3{prop:FontColor} = 65793
        ?def:Invoice_Address_Line3{prop:Color} = 16777215
    End ! If ?def:Invoice_Address_Line3{prop:Req} = True
    ?def:Invoice_Address_Line3{prop:Trn} = 0
    ?def:Invoice_Address_Line3{prop:FontStyle} = font:Bold
    ?DEF:Invoice_Postcode:Prompt{prop:FontColor} = -1
    ?DEF:Invoice_Postcode:Prompt{prop:Color} = 15066597
    If ?def:Invoice_Postcode{prop:ReadOnly} = True
        ?def:Invoice_Postcode{prop:FontColor} = 65793
        ?def:Invoice_Postcode{prop:Color} = 15066597
    Elsif ?def:Invoice_Postcode{prop:Req} = True
        ?def:Invoice_Postcode{prop:FontColor} = 65793
        ?def:Invoice_Postcode{prop:Color} = 8454143
    Else ! If ?def:Invoice_Postcode{prop:Req} = True
        ?def:Invoice_Postcode{prop:FontColor} = 65793
        ?def:Invoice_Postcode{prop:Color} = 16777215
    End ! If ?def:Invoice_Postcode{prop:Req} = True
    ?def:Invoice_Postcode{prop:Trn} = 0
    ?def:Invoice_Postcode{prop:FontStyle} = font:Bold
    ?DEF:Invoice_Telephone_Number:Prompt{prop:FontColor} = -1
    ?DEF:Invoice_Telephone_Number:Prompt{prop:Color} = 15066597
    If ?def:Invoice_Telephone_Number{prop:ReadOnly} = True
        ?def:Invoice_Telephone_Number{prop:FontColor} = 65793
        ?def:Invoice_Telephone_Number{prop:Color} = 15066597
    Elsif ?def:Invoice_Telephone_Number{prop:Req} = True
        ?def:Invoice_Telephone_Number{prop:FontColor} = 65793
        ?def:Invoice_Telephone_Number{prop:Color} = 8454143
    Else ! If ?def:Invoice_Telephone_Number{prop:Req} = True
        ?def:Invoice_Telephone_Number{prop:FontColor} = 65793
        ?def:Invoice_Telephone_Number{prop:Color} = 16777215
    End ! If ?def:Invoice_Telephone_Number{prop:Req} = True
    ?def:Invoice_Telephone_Number{prop:Trn} = 0
    ?def:Invoice_Telephone_Number{prop:FontStyle} = font:Bold
    ?DEF:Invoice_Fax_Number:Prompt{prop:FontColor} = -1
    ?DEF:Invoice_Fax_Number:Prompt{prop:Color} = 15066597
    If ?def:Invoice_Fax_Number{prop:ReadOnly} = True
        ?def:Invoice_Fax_Number{prop:FontColor} = 65793
        ?def:Invoice_Fax_Number{prop:Color} = 15066597
    Elsif ?def:Invoice_Fax_Number{prop:Req} = True
        ?def:Invoice_Fax_Number{prop:FontColor} = 65793
        ?def:Invoice_Fax_Number{prop:Color} = 8454143
    Else ! If ?def:Invoice_Fax_Number{prop:Req} = True
        ?def:Invoice_Fax_Number{prop:FontColor} = 65793
        ?def:Invoice_Fax_Number{prop:Color} = 16777215
    End ! If ?def:Invoice_Fax_Number{prop:Req} = True
    ?def:Invoice_Fax_Number{prop:Trn} = 0
    ?def:Invoice_Fax_Number{prop:FontStyle} = font:Bold
    ?DEF:Invoice_VAT_Number:Prompt{prop:FontColor} = -1
    ?DEF:Invoice_VAT_Number:Prompt{prop:Color} = 15066597
    If ?def:Invoice_VAT_Number{prop:ReadOnly} = True
        ?def:Invoice_VAT_Number{prop:FontColor} = 65793
        ?def:Invoice_VAT_Number{prop:Color} = 15066597
    Elsif ?def:Invoice_VAT_Number{prop:Req} = True
        ?def:Invoice_VAT_Number{prop:FontColor} = 65793
        ?def:Invoice_VAT_Number{prop:Color} = 8454143
    Else ! If ?def:Invoice_VAT_Number{prop:Req} = True
        ?def:Invoice_VAT_Number{prop:FontColor} = 65793
        ?def:Invoice_VAT_Number{prop:Color} = 16777215
    End ! If ?def:Invoice_VAT_Number{prop:Req} = True
    ?def:Invoice_VAT_Number{prop:Trn} = 0
    ?def:Invoice_VAT_Number{prop:FontStyle} = font:Bold
    ?def:InvoiceEmailAddress:Prompt{prop:FontColor} = -1
    ?def:InvoiceEmailAddress:Prompt{prop:Color} = 15066597
    If ?def:InvoiceEmailAddress{prop:ReadOnly} = True
        ?def:InvoiceEmailAddress{prop:FontColor} = 65793
        ?def:InvoiceEmailAddress{prop:Color} = 15066597
    Elsif ?def:InvoiceEmailAddress{prop:Req} = True
        ?def:InvoiceEmailAddress{prop:FontColor} = 65793
        ?def:InvoiceEmailAddress{prop:Color} = 8454143
    Else ! If ?def:InvoiceEmailAddress{prop:Req} = True
        ?def:InvoiceEmailAddress{prop:FontColor} = 65793
        ?def:InvoiceEmailAddress{prop:Color} = 16777215
    End ! If ?def:InvoiceEmailAddress{prop:Req} = True
    ?def:InvoiceEmailAddress{prop:Trn} = 0
    ?def:InvoiceEmailAddress{prop:FontStyle} = font:Bold
    ?PartsOrderTab{prop:Color} = 15066597
    ?PartsOrderAddress:2{prop:Font,3} = -1
    ?PartsOrderAddress:2{prop:Color} = 15066597
    ?PartsOrderAddress:2{prop:Trn} = 0
    ?def:OrderCompanyName:Prompt{prop:FontColor} = -1
    ?def:OrderCompanyName:Prompt{prop:Color} = 15066597
    If ?def:OrderCompanyName{prop:ReadOnly} = True
        ?def:OrderCompanyName{prop:FontColor} = 65793
        ?def:OrderCompanyName{prop:Color} = 15066597
    Elsif ?def:OrderCompanyName{prop:Req} = True
        ?def:OrderCompanyName{prop:FontColor} = 65793
        ?def:OrderCompanyName{prop:Color} = 8454143
    Else ! If ?def:OrderCompanyName{prop:Req} = True
        ?def:OrderCompanyName{prop:FontColor} = 65793
        ?def:OrderCompanyName{prop:Color} = 16777215
    End ! If ?def:OrderCompanyName{prop:Req} = True
    ?def:OrderCompanyName{prop:Trn} = 0
    ?def:OrderCompanyName{prop:FontStyle} = font:Bold
    ?def:OrderAddressLine1:Prompt{prop:FontColor} = -1
    ?def:OrderAddressLine1:Prompt{prop:Color} = 15066597
    If ?def:OrderAddressLine1{prop:ReadOnly} = True
        ?def:OrderAddressLine1{prop:FontColor} = 65793
        ?def:OrderAddressLine1{prop:Color} = 15066597
    Elsif ?def:OrderAddressLine1{prop:Req} = True
        ?def:OrderAddressLine1{prop:FontColor} = 65793
        ?def:OrderAddressLine1{prop:Color} = 8454143
    Else ! If ?def:OrderAddressLine1{prop:Req} = True
        ?def:OrderAddressLine1{prop:FontColor} = 65793
        ?def:OrderAddressLine1{prop:Color} = 16777215
    End ! If ?def:OrderAddressLine1{prop:Req} = True
    ?def:OrderAddressLine1{prop:Trn} = 0
    ?def:OrderAddressLine1{prop:FontStyle} = font:Bold
    If ?def:OrderAddressLine2{prop:ReadOnly} = True
        ?def:OrderAddressLine2{prop:FontColor} = 65793
        ?def:OrderAddressLine2{prop:Color} = 15066597
    Elsif ?def:OrderAddressLine2{prop:Req} = True
        ?def:OrderAddressLine2{prop:FontColor} = 65793
        ?def:OrderAddressLine2{prop:Color} = 8454143
    Else ! If ?def:OrderAddressLine2{prop:Req} = True
        ?def:OrderAddressLine2{prop:FontColor} = 65793
        ?def:OrderAddressLine2{prop:Color} = 16777215
    End ! If ?def:OrderAddressLine2{prop:Req} = True
    ?def:OrderAddressLine2{prop:Trn} = 0
    ?def:OrderAddressLine2{prop:FontStyle} = font:Bold
    If ?def:OrderAddressLine3{prop:ReadOnly} = True
        ?def:OrderAddressLine3{prop:FontColor} = 65793
        ?def:OrderAddressLine3{prop:Color} = 15066597
    Elsif ?def:OrderAddressLine3{prop:Req} = True
        ?def:OrderAddressLine3{prop:FontColor} = 65793
        ?def:OrderAddressLine3{prop:Color} = 8454143
    Else ! If ?def:OrderAddressLine3{prop:Req} = True
        ?def:OrderAddressLine3{prop:FontColor} = 65793
        ?def:OrderAddressLine3{prop:Color} = 16777215
    End ! If ?def:OrderAddressLine3{prop:Req} = True
    ?def:OrderAddressLine3{prop:Trn} = 0
    ?def:OrderAddressLine3{prop:FontStyle} = font:Bold
    ?def:OrderPostcode:Prompt{prop:FontColor} = -1
    ?def:OrderPostcode:Prompt{prop:Color} = 15066597
    If ?def:OrderPostcode{prop:ReadOnly} = True
        ?def:OrderPostcode{prop:FontColor} = 65793
        ?def:OrderPostcode{prop:Color} = 15066597
    Elsif ?def:OrderPostcode{prop:Req} = True
        ?def:OrderPostcode{prop:FontColor} = 65793
        ?def:OrderPostcode{prop:Color} = 8454143
    Else ! If ?def:OrderPostcode{prop:Req} = True
        ?def:OrderPostcode{prop:FontColor} = 65793
        ?def:OrderPostcode{prop:Color} = 16777215
    End ! If ?def:OrderPostcode{prop:Req} = True
    ?def:OrderPostcode{prop:Trn} = 0
    ?def:OrderPostcode{prop:FontStyle} = font:Bold
    ?def:OrderTelephoneNumber:Prompt{prop:FontColor} = -1
    ?def:OrderTelephoneNumber:Prompt{prop:Color} = 15066597
    If ?def:OrderTelephoneNumber{prop:ReadOnly} = True
        ?def:OrderTelephoneNumber{prop:FontColor} = 65793
        ?def:OrderTelephoneNumber{prop:Color} = 15066597
    Elsif ?def:OrderTelephoneNumber{prop:Req} = True
        ?def:OrderTelephoneNumber{prop:FontColor} = 65793
        ?def:OrderTelephoneNumber{prop:Color} = 8454143
    Else ! If ?def:OrderTelephoneNumber{prop:Req} = True
        ?def:OrderTelephoneNumber{prop:FontColor} = 65793
        ?def:OrderTelephoneNumber{prop:Color} = 16777215
    End ! If ?def:OrderTelephoneNumber{prop:Req} = True
    ?def:OrderTelephoneNumber{prop:Trn} = 0
    ?def:OrderTelephoneNumber{prop:FontStyle} = font:Bold
    ?def:OrderFaxNumber:Prompt{prop:FontColor} = -1
    ?def:OrderFaxNumber:Prompt{prop:Color} = 15066597
    If ?def:OrderFaxNumber{prop:ReadOnly} = True
        ?def:OrderFaxNumber{prop:FontColor} = 65793
        ?def:OrderFaxNumber{prop:Color} = 15066597
    Elsif ?def:OrderFaxNumber{prop:Req} = True
        ?def:OrderFaxNumber{prop:FontColor} = 65793
        ?def:OrderFaxNumber{prop:Color} = 8454143
    Else ! If ?def:OrderFaxNumber{prop:Req} = True
        ?def:OrderFaxNumber{prop:FontColor} = 65793
        ?def:OrderFaxNumber{prop:Color} = 16777215
    End ! If ?def:OrderFaxNumber{prop:Req} = True
    ?def:OrderFaxNumber{prop:Trn} = 0
    ?def:OrderFaxNumber{prop:FontStyle} = font:Bold
    ?def:OrderEmailAddress:Prompt{prop:FontColor} = -1
    ?def:OrderEmailAddress:Prompt{prop:Color} = 15066597
    If ?def:OrderEmailAddress{prop:ReadOnly} = True
        ?def:OrderEmailAddress{prop:FontColor} = 65793
        ?def:OrderEmailAddress{prop:Color} = 15066597
    Elsif ?def:OrderEmailAddress{prop:Req} = True
        ?def:OrderEmailAddress{prop:FontColor} = 65793
        ?def:OrderEmailAddress{prop:Color} = 8454143
    Else ! If ?def:OrderEmailAddress{prop:Req} = True
        ?def:OrderEmailAddress{prop:FontColor} = 65793
        ?def:OrderEmailAddress{prop:Color} = 16777215
    End ! If ?def:OrderEmailAddress{prop:Req} = True
    ?def:OrderEmailAddress{prop:Trn} = 0
    ?def:OrderEmailAddress{prop:FontStyle} = font:Bold
    ?EuroAddressTab{prop:Color} = 15066597
    ?EuroAddressDetails{prop:Font,3} = -1
    ?EuroAddressDetails{prop:Color} = 15066597
    ?EuroAddressDetails{prop:Trn} = 0
    ?def:OrderCompanyName:Prompt:2{prop:FontColor} = -1
    ?def:OrderCompanyName:Prompt:2{prop:Color} = 15066597
    If ?de2:Inv2CompanyName{prop:ReadOnly} = True
        ?de2:Inv2CompanyName{prop:FontColor} = 65793
        ?de2:Inv2CompanyName{prop:Color} = 15066597
    Elsif ?de2:Inv2CompanyName{prop:Req} = True
        ?de2:Inv2CompanyName{prop:FontColor} = 65793
        ?de2:Inv2CompanyName{prop:Color} = 8454143
    Else ! If ?de2:Inv2CompanyName{prop:Req} = True
        ?de2:Inv2CompanyName{prop:FontColor} = 65793
        ?de2:Inv2CompanyName{prop:Color} = 16777215
    End ! If ?de2:Inv2CompanyName{prop:Req} = True
    ?de2:Inv2CompanyName{prop:Trn} = 0
    ?de2:Inv2CompanyName{prop:FontStyle} = font:Bold
    ?def:OrderAddressLine1:Prompt:2{prop:FontColor} = -1
    ?def:OrderAddressLine1:Prompt:2{prop:Color} = 15066597
    If ?de2:Inv2AddressLine1{prop:ReadOnly} = True
        ?de2:Inv2AddressLine1{prop:FontColor} = 65793
        ?de2:Inv2AddressLine1{prop:Color} = 15066597
    Elsif ?de2:Inv2AddressLine1{prop:Req} = True
        ?de2:Inv2AddressLine1{prop:FontColor} = 65793
        ?de2:Inv2AddressLine1{prop:Color} = 8454143
    Else ! If ?de2:Inv2AddressLine1{prop:Req} = True
        ?de2:Inv2AddressLine1{prop:FontColor} = 65793
        ?de2:Inv2AddressLine1{prop:Color} = 16777215
    End ! If ?de2:Inv2AddressLine1{prop:Req} = True
    ?de2:Inv2AddressLine1{prop:Trn} = 0
    ?de2:Inv2AddressLine1{prop:FontStyle} = font:Bold
    If ?de2:Inv2AddressLine2{prop:ReadOnly} = True
        ?de2:Inv2AddressLine2{prop:FontColor} = 65793
        ?de2:Inv2AddressLine2{prop:Color} = 15066597
    Elsif ?de2:Inv2AddressLine2{prop:Req} = True
        ?de2:Inv2AddressLine2{prop:FontColor} = 65793
        ?de2:Inv2AddressLine2{prop:Color} = 8454143
    Else ! If ?de2:Inv2AddressLine2{prop:Req} = True
        ?de2:Inv2AddressLine2{prop:FontColor} = 65793
        ?de2:Inv2AddressLine2{prop:Color} = 16777215
    End ! If ?de2:Inv2AddressLine2{prop:Req} = True
    ?de2:Inv2AddressLine2{prop:Trn} = 0
    ?de2:Inv2AddressLine2{prop:FontStyle} = font:Bold
    If ?de2:Inv2AddressLine3{prop:ReadOnly} = True
        ?de2:Inv2AddressLine3{prop:FontColor} = 65793
        ?de2:Inv2AddressLine3{prop:Color} = 15066597
    Elsif ?de2:Inv2AddressLine3{prop:Req} = True
        ?de2:Inv2AddressLine3{prop:FontColor} = 65793
        ?de2:Inv2AddressLine3{prop:Color} = 8454143
    Else ! If ?de2:Inv2AddressLine3{prop:Req} = True
        ?de2:Inv2AddressLine3{prop:FontColor} = 65793
        ?de2:Inv2AddressLine3{prop:Color} = 16777215
    End ! If ?de2:Inv2AddressLine3{prop:Req} = True
    ?de2:Inv2AddressLine3{prop:Trn} = 0
    ?de2:Inv2AddressLine3{prop:FontStyle} = font:Bold
    ?def:OrderPostcode:Prompt:2{prop:FontColor} = -1
    ?def:OrderPostcode:Prompt:2{prop:Color} = 15066597
    If ?de2:Inv2Postcode{prop:ReadOnly} = True
        ?de2:Inv2Postcode{prop:FontColor} = 65793
        ?de2:Inv2Postcode{prop:Color} = 15066597
    Elsif ?de2:Inv2Postcode{prop:Req} = True
        ?de2:Inv2Postcode{prop:FontColor} = 65793
        ?de2:Inv2Postcode{prop:Color} = 8454143
    Else ! If ?de2:Inv2Postcode{prop:Req} = True
        ?de2:Inv2Postcode{prop:FontColor} = 65793
        ?de2:Inv2Postcode{prop:Color} = 16777215
    End ! If ?de2:Inv2Postcode{prop:Req} = True
    ?de2:Inv2Postcode{prop:Trn} = 0
    ?de2:Inv2Postcode{prop:FontStyle} = font:Bold
    ?def:OrderTelephoneNumber:Prompt:2{prop:FontColor} = -1
    ?def:OrderTelephoneNumber:Prompt:2{prop:Color} = 15066597
    If ?de2:Inv2TelephoneNumber{prop:ReadOnly} = True
        ?de2:Inv2TelephoneNumber{prop:FontColor} = 65793
        ?de2:Inv2TelephoneNumber{prop:Color} = 15066597
    Elsif ?de2:Inv2TelephoneNumber{prop:Req} = True
        ?de2:Inv2TelephoneNumber{prop:FontColor} = 65793
        ?de2:Inv2TelephoneNumber{prop:Color} = 8454143
    Else ! If ?de2:Inv2TelephoneNumber{prop:Req} = True
        ?de2:Inv2TelephoneNumber{prop:FontColor} = 65793
        ?de2:Inv2TelephoneNumber{prop:Color} = 16777215
    End ! If ?de2:Inv2TelephoneNumber{prop:Req} = True
    ?de2:Inv2TelephoneNumber{prop:Trn} = 0
    ?de2:Inv2TelephoneNumber{prop:FontStyle} = font:Bold
    ?def:OrderFaxNumber:Prompt:2{prop:FontColor} = -1
    ?def:OrderFaxNumber:Prompt:2{prop:Color} = 15066597
    If ?de2:Inv2FaxNumber{prop:ReadOnly} = True
        ?de2:Inv2FaxNumber{prop:FontColor} = 65793
        ?de2:Inv2FaxNumber{prop:Color} = 15066597
    Elsif ?de2:Inv2FaxNumber{prop:Req} = True
        ?de2:Inv2FaxNumber{prop:FontColor} = 65793
        ?de2:Inv2FaxNumber{prop:Color} = 8454143
    Else ! If ?de2:Inv2FaxNumber{prop:Req} = True
        ?de2:Inv2FaxNumber{prop:FontColor} = 65793
        ?de2:Inv2FaxNumber{prop:Color} = 16777215
    End ! If ?de2:Inv2FaxNumber{prop:Req} = True
    ?de2:Inv2FaxNumber{prop:Trn} = 0
    ?de2:Inv2FaxNumber{prop:FontStyle} = font:Bold
    ?DE2:Inv2VATNumber:Prompt{prop:FontColor} = -1
    ?DE2:Inv2VATNumber:Prompt{prop:Color} = 15066597
    If ?de2:Inv2VATNumber{prop:ReadOnly} = True
        ?de2:Inv2VATNumber{prop:FontColor} = 65793
        ?de2:Inv2VATNumber{prop:Color} = 15066597
    Elsif ?de2:Inv2VATNumber{prop:Req} = True
        ?de2:Inv2VATNumber{prop:FontColor} = 65793
        ?de2:Inv2VATNumber{prop:Color} = 8454143
    Else ! If ?de2:Inv2VATNumber{prop:Req} = True
        ?de2:Inv2VATNumber{prop:FontColor} = 65793
        ?de2:Inv2VATNumber{prop:Color} = 16777215
    End ! If ?de2:Inv2VATNumber{prop:Req} = True
    ?de2:Inv2VATNumber{prop:Trn} = 0
    ?de2:Inv2VATNumber{prop:FontStyle} = font:Bold
    ?def:OrderEmailAddress:Prompt:2{prop:FontColor} = -1
    ?def:OrderEmailAddress:Prompt:2{prop:Color} = 15066597
    If ?de2:Inv2EmailAddress{prop:ReadOnly} = True
        ?de2:Inv2EmailAddress{prop:FontColor} = 65793
        ?de2:Inv2EmailAddress{prop:Color} = 15066597
    Elsif ?de2:Inv2EmailAddress{prop:Req} = True
        ?de2:Inv2EmailAddress{prop:FontColor} = 65793
        ?de2:Inv2EmailAddress{prop:Color} = 8454143
    Else ! If ?de2:Inv2EmailAddress{prop:Req} = True
        ?de2:Inv2EmailAddress{prop:FontColor} = 65793
        ?de2:Inv2EmailAddress{prop:Color} = 16777215
    End ! If ?de2:Inv2EmailAddress{prop:Req} = True
    ?de2:Inv2EmailAddress{prop:Trn} = 0
    ?de2:Inv2EmailAddress{prop:FontStyle} = font:Bold
    ?BackgroundTab{prop:Color} = 15066597
    ?DEF:Version_Number:Prompt{prop:FontColor} = -1
    ?DEF:Version_Number:Prompt{prop:Color} = 15066597
    If ?def:Version_Number{prop:ReadOnly} = True
        ?def:Version_Number{prop:FontColor} = 65793
        ?def:Version_Number{prop:Color} = 15066597
    Elsif ?def:Version_Number{prop:Req} = True
        ?def:Version_Number{prop:FontColor} = 65793
        ?def:Version_Number{prop:Color} = 8454143
    Else ! If ?def:Version_Number{prop:Req} = True
        ?def:Version_Number{prop:FontColor} = 65793
        ?def:Version_Number{prop:Color} = 16777215
    End ! If ?def:Version_Number{prop:Req} = True
    ?def:Version_Number{prop:Trn} = 0
    ?def:Version_Number{prop:FontStyle} = font:Bold
    ?DEF:License_Number:Prompt{prop:FontColor} = -1
    ?DEF:License_Number:Prompt{prop:Color} = 15066597
    If ?def:License_Number{prop:ReadOnly} = True
        ?def:License_Number{prop:FontColor} = 65793
        ?def:License_Number{prop:Color} = 15066597
    Elsif ?def:License_Number{prop:Req} = True
        ?def:License_Number{prop:FontColor} = 65793
        ?def:License_Number{prop:Color} = 8454143
    Else ! If ?def:License_Number{prop:Req} = True
        ?def:License_Number{prop:FontColor} = 65793
        ?def:License_Number{prop:Color} = 16777215
    End ! If ?def:License_Number{prop:Req} = True
    ?def:License_Number{prop:Trn} = 0
    ?def:License_Number{prop:FontStyle} = font:Bold
    ?DEF:Maximum_Users:Prompt{prop:FontColor} = -1
    ?DEF:Maximum_Users:Prompt{prop:Color} = 15066597
    If ?def:Maximum_Users{prop:ReadOnly} = True
        ?def:Maximum_Users{prop:FontColor} = 65793
        ?def:Maximum_Users{prop:Color} = 15066597
    Elsif ?def:Maximum_Users{prop:Req} = True
        ?def:Maximum_Users{prop:FontColor} = 65793
        ?def:Maximum_Users{prop:Color} = 8454143
    Else ! If ?def:Maximum_Users{prop:Req} = True
        ?def:Maximum_Users{prop:FontColor} = 65793
        ?def:Maximum_Users{prop:Color} = 16777215
    End ! If ?def:Maximum_Users{prop:Req} = True
    ?def:Maximum_Users{prop:Trn} = 0
    ?def:Maximum_Users{prop:FontStyle} = font:Bold
    ?def:Use_Invoice_Address{prop:Font,3} = -1
    ?def:Use_Invoice_Address{prop:Color} = 15066597
    ?def:Use_Invoice_Address{prop:Trn} = 0
    ?def:Use_For_Order{prop:Font,3} = -1
    ?def:Use_For_Order{prop:Color} = 15066597
    ?def:Use_For_Order{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Defaults_Window')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DEF:Start_Work_Hours:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      Access:DEFAULTS.Update()
      Access:DEFAULT2.Update()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?def:Telephone_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:Telephone_Number, Accepted)
      
          temp_string = Clip(left(def:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          def:Telephone_Number    = temp_string
          Display(?def:Telephone_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:Telephone_Number, Accepted)
    OF ?def:Fax_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:Fax_Number, Accepted)
      
          temp_string = Clip(left(def:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          def:Fax_Number    = temp_string
          Display(?def:Fax_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?def:Fax_Number, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Set(Defaults)
      If access:defaults.next()
          get(defaults,0)
          if access:defaults.primerecord() = level:benign
              if access:defaults.insert()
                  access:defaults.cancelautoinc()
              end
          end!if access:defaults.primerecord() = level:benign
      End!If access:defaults.next()
      
      Set(defstock)
      If access:defstock.next()
          get(defstock,0)
          if access:defstock.primerecord() = level:benign
              if access:defstock.insert()
                  access:defstock.cancelautoinc()
              end
          end!if access:defstock.primerecord() = level:benign
      End!If access:defstock.next()
      
      Set(DEFAULT2)
      If Access:DEFAULT2.Next()
          If Access:DEFAULT2.PrimeRecord() = Level:Benign
              If Access:DEFAULT2.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:DEFAULT2.TryInsert() = Level:Benign
                  !Insert Failed
                  Access:DEFAULT2.CancelAutoInc()
              End !If Access:DEFAULT2.TryInsert() = Level:Benign
          End !If Access:DEFAULT2.PrimeRecord() = Level:Benign
      End !Access:DEFAULT2.Next()
      
      If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
          Unhide(?backgroundtab)
      End!If glo:password = int((today()/2.5)*9.7)
      Post(event:accepted,?def:use_invoice_address)
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

