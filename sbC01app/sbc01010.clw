

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01010.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSTDCHRGE PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?sta:Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Ref_Number         LIKE(cha:Ref_Number)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?sta:Unit_Type
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?sta:Repair_Type
rep:Repair_Type        LIKE(rep:Repair_Type)          !List box control field - type derived from field
rep:Manufacturer       LIKE(rep:Manufacturer)         !Primary key field - type derived from field
rep:Model_Number       LIKE(rep:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Ref_Number)
                     END
FDCB7::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
FDCB8::View:FileDropCombo VIEW(REPAIRTY)
                       PROJECT(rep:Repair_Type)
                       PROJECT(rep:Manufacturer)
                       PROJECT(rep:Model_Number)
                     END
History::sta:Record  LIKE(sta:RECORD),STATIC
QuickWindow          WINDOW('Update the STDCHRGE File'),AT(,,220,136),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('UpdateSTDCHRGE'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,100),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Model Number'),AT(8,20),USE(?STA:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(sta:Model_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Charge Type'),AT(7,36),USE(?Prompt2)
                           COMBO(@s30),AT(84,36,124,10),USE(sta:Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('114L(2)|M@s30@12L(2)@n3@'),DROP(10,144),FROM(Queue:FileDropCombo)
                           PROMPT('Unit Type'),AT(7,52),USE(?Prompt3)
                           COMBO(@s30),AT(84,52,124,10),USE(sta:Unit_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           PROMPT('Repair Type'),AT(8,68),USE(?Prompt4)
                           COMBO(@s30),AT(84,68,124,10),USE(sta:Repair_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:2)
                           PROMPT('Cost'),AT(8,84),USE(?STA:Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,84,64,10),USE(sta:Cost),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       PANEL,AT(4,108,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,112,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,112,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?STA:Model_Number:Prompt{prop:FontColor} = -1
    ?STA:Model_Number:Prompt{prop:Color} = 15066597
    If ?sta:Model_Number{prop:ReadOnly} = True
        ?sta:Model_Number{prop:FontColor} = 65793
        ?sta:Model_Number{prop:Color} = 15066597
    Elsif ?sta:Model_Number{prop:Req} = True
        ?sta:Model_Number{prop:FontColor} = 65793
        ?sta:Model_Number{prop:Color} = 8454143
    Else ! If ?sta:Model_Number{prop:Req} = True
        ?sta:Model_Number{prop:FontColor} = 65793
        ?sta:Model_Number{prop:Color} = 16777215
    End ! If ?sta:Model_Number{prop:Req} = True
    ?sta:Model_Number{prop:Trn} = 0
    ?sta:Model_Number{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?sta:Charge_Type{prop:ReadOnly} = True
        ?sta:Charge_Type{prop:FontColor} = 65793
        ?sta:Charge_Type{prop:Color} = 15066597
    Elsif ?sta:Charge_Type{prop:Req} = True
        ?sta:Charge_Type{prop:FontColor} = 65793
        ?sta:Charge_Type{prop:Color} = 8454143
    Else ! If ?sta:Charge_Type{prop:Req} = True
        ?sta:Charge_Type{prop:FontColor} = 65793
        ?sta:Charge_Type{prop:Color} = 16777215
    End ! If ?sta:Charge_Type{prop:Req} = True
    ?sta:Charge_Type{prop:Trn} = 0
    ?sta:Charge_Type{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?sta:Unit_Type{prop:ReadOnly} = True
        ?sta:Unit_Type{prop:FontColor} = 65793
        ?sta:Unit_Type{prop:Color} = 15066597
    Elsif ?sta:Unit_Type{prop:Req} = True
        ?sta:Unit_Type{prop:FontColor} = 65793
        ?sta:Unit_Type{prop:Color} = 8454143
    Else ! If ?sta:Unit_Type{prop:Req} = True
        ?sta:Unit_Type{prop:FontColor} = 65793
        ?sta:Unit_Type{prop:Color} = 16777215
    End ! If ?sta:Unit_Type{prop:Req} = True
    ?sta:Unit_Type{prop:Trn} = 0
    ?sta:Unit_Type{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?sta:Repair_Type{prop:ReadOnly} = True
        ?sta:Repair_Type{prop:FontColor} = 65793
        ?sta:Repair_Type{prop:Color} = 15066597
    Elsif ?sta:Repair_Type{prop:Req} = True
        ?sta:Repair_Type{prop:FontColor} = 65793
        ?sta:Repair_Type{prop:Color} = 8454143
    Else ! If ?sta:Repair_Type{prop:Req} = True
        ?sta:Repair_Type{prop:FontColor} = 65793
        ?sta:Repair_Type{prop:Color} = 16777215
    End ! If ?sta:Repair_Type{prop:Req} = True
    ?sta:Repair_Type{prop:Trn} = 0
    ?sta:Repair_Type{prop:FontStyle} = font:Bold
    ?STA:Cost:Prompt{prop:FontColor} = -1
    ?STA:Cost:Prompt{prop:Color} = 15066597
    If ?sta:Cost{prop:ReadOnly} = True
        ?sta:Cost{prop:FontColor} = 65793
        ?sta:Cost{prop:Color} = 15066597
    Elsif ?sta:Cost{prop:Req} = True
        ?sta:Cost{prop:FontColor} = 65793
        ?sta:Cost{prop:Color} = 8454143
    Else ! If ?sta:Cost{prop:Req} = True
        ?sta:Cost{prop:FontColor} = 65793
        ?sta:Cost{prop:Color} = 16777215
    End ! If ?sta:Cost{prop:Req} = True
    ?sta:Cost{prop:Trn} = 0
    ?sta:Cost{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Standard Charge'
  OF ChangeRecord
    ActionMessage = 'Changing A Standard Charge'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSTDCHRGE')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STA:Model_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sta:Record,History::sta:Record)
  SELF.AddHistoryField(?sta:Model_Number,2)
  SELF.AddHistoryField(?sta:Charge_Type,1)
  SELF.AddHistoryField(?sta:Unit_Type,3)
  SELF.AddHistoryField(?sta:Repair_Type,4)
  SELF.AddHistoryField(?sta:Cost,5)
  SELF.AddUpdateFile(Access:STDCHRGE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STDCHRGE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.AddItem(ToolbarForm)
  FDCB6.Init(sta:Charge_Type,?sta:Charge_Type,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(cha:Ref_Number_Key)
  FDCB6.AddField(cha:Charge_Type,FDCB6.Q.cha:Charge_Type)
  FDCB6.AddField(cha:Ref_Number,FDCB6.Q.cha:Ref_Number)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB7.Init(sta:Unit_Type,?sta:Unit_Type,Queue:FileDropCombo:1.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:1,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:1
  FDCB7.AddSortOrder(uni:Unit_Type_Key)
  FDCB7.AddField(uni:Unit_Type,FDCB7.Q.uni:Unit_Type)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  FDCB8.Init(sta:Repair_Type,?sta:Repair_Type,Queue:FileDropCombo:2.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:2,Relate:REPAIRTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:2
  FDCB8.AddSortOrder(rep:Model_Number_Key)
  FDCB8.AddRange(rep:Model_Number,sta:Model_Number)
  FDCB8.AddField(rep:Repair_Type,FDCB8.Q.rep:Repair_Type)
  FDCB8.AddField(rep:Manufacturer,FDCB8.Q.rep:Manufacturer)
  FDCB8.AddField(rep:Model_Number,FDCB8.Q.rep:Model_Number)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

