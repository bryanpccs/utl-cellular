

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_eprn.inc'),ONCE

                     MAP
                       INCLUDE('SBC01002.INC'),ONCE        !Local module procedure declarations
                     END


Update_Printer_Defaults PROCEDURE                     !Generated from procedure template - Window

CurrentTab           STRING(80)
printer_queue_temp   QUEUE,PRE(PRNQUE)
default_printer      STRING(255)
                     END
defaultprinter       CSTRING(256)
printer_name_temp    CSTRING(256)
printer_temp         STRING(255)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
tmp:CurrentPrinter   STRING(255)
Printer_Name_Queue   QUEUE,PRE(PRNQUE)
Printer_Name         STRING(60)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::dep:Record  LIKE(dep:RECORD),STATIC
QuickWindow          WINDOW('Update the DEFPRINT File'),AT(,,287,115),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Update_Printer_Defaults'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,280,80),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Report Name'),AT(8,20),USE(?Prompt2)
                           LIST,AT(84,20,124,10),USE(dep:Printer_Name),VSCROLL,LEFT(2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),DROP(10),FROM(Printer_Name_Queue)
                           PROMPT('Printer Name'),AT(8,36),USE(?dep:Printer_Path:Prompt),TRN
                           ENTRY(@s255),AT(84,36,124,10),USE(dep:Printer_Path),FONT('Tahoma',,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON('Select Printer'),AT(212,36,68,10),USE(?LookupPrinter),SKIP,LEFT,ICON('List3.ico')
                           PROMPT('Copies'),AT(8,52),USE(?dep:Copies:Prompt),TRN
                           SPIN(@n8),AT(84,52,64,10),USE(dep:Copies),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Copies'),TIP('Copies'),UPR,STEP(1)
                           CHECK('Remove Background'),AT(84,68),USE(dep:Background),RIGHT,VALUE('YES','NO')
                         END
                       END
                       BUTTON('&OK'),AT(168,92,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(224,92,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       PANEL,AT(4,88,280,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

aePrn apiPrinterClass
aePrn:Ptr LONG
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?dep:Printer_Name{prop:FontColor} = 65793
    ?dep:Printer_Name{prop:Color}= 16777215
    ?dep:Printer_Name{prop:Color,2} = 16777215
    ?dep:Printer_Name{prop:Color,3} = 12937777
    ?dep:Printer_Path:Prompt{prop:FontColor} = -1
    ?dep:Printer_Path:Prompt{prop:Color} = 15066597
    If ?dep:Printer_Path{prop:ReadOnly} = True
        ?dep:Printer_Path{prop:FontColor} = 65793
        ?dep:Printer_Path{prop:Color} = 15066597
    Elsif ?dep:Printer_Path{prop:Req} = True
        ?dep:Printer_Path{prop:FontColor} = 65793
        ?dep:Printer_Path{prop:Color} = 8454143
    Else ! If ?dep:Printer_Path{prop:Req} = True
        ?dep:Printer_Path{prop:FontColor} = 65793
        ?dep:Printer_Path{prop:Color} = 16777215
    End ! If ?dep:Printer_Path{prop:Req} = True
    ?dep:Printer_Path{prop:Trn} = 0
    ?dep:Printer_Path{prop:FontStyle} = font:Bold
    ?dep:Copies:Prompt{prop:FontColor} = -1
    ?dep:Copies:Prompt{prop:Color} = 15066597
    If ?dep:Copies{prop:ReadOnly} = True
        ?dep:Copies{prop:FontColor} = 65793
        ?dep:Copies{prop:Color} = 15066597
    Elsif ?dep:Copies{prop:Req} = True
        ?dep:Copies{prop:FontColor} = 65793
        ?dep:Copies{prop:Color} = 8454143
    Else ! If ?dep:Copies{prop:Req} = True
        ?dep:Copies{prop:FontColor} = 65793
        ?dep:Copies{prop:Color} = 16777215
    End ! If ?dep:Copies{prop:Req} = True
    ?dep:Copies{prop:Trn} = 0
    ?dep:Copies{prop:FontStyle} = font:Bold
    ?dep:Background{prop:Font,3} = -1
    ?dep:Background{prop:Color} = 15066597
    ?dep:Background{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Printer_Defaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  Free(printer_name_queue)
  Clear(printer_name_queue)
  prnque:printer_name = '3RD PARTY RETURNS NOTE'
  Add(printer_name_queue)
  prnque:printer_name = '3RD PARTY FAILED RETURNS'
  Add(printer_name_queue)
  prnque:printer_name = 'BOUNCER HISTORY REPORT'
  Add(printer_name_queue)
  prnque:printer_name = 'CONSIGNMENT NOTE - 3RD PARTY'
  Add(printer_name_queue)
  prnque:printer_name = 'CREDIT NOTE'
  Add(printer_name_queue)
  prnque:printer_name = 'JOB CARD'
  Add(printer_name_queue)
  prnque:printer_name = 'JOB RECEIPT'
  Add(printer_name_queue)
  prnque:printer_name = 'DESPATCH NOTE'
  Add(printer_name_queue)
  prnque:printer_name = 'DESPATCH NOTE - BATCH'
  Add(printer_name_queue)
  prnque:printer_name = 'DESPATCH NOTE - MULTIPLE'
  Add(printer_name_queue)
  prnque:printer_name = 'DESPATCH NOTE - RETAIL'
  Add(printer_name_queue)
  prnque:printer_name = 'DESPATCH NOTE - THIRD PARTY'
  Add(printer_name_queue)
  prnque:printer_name = 'ESTIMATE'
  Add(printer_name_queue)
  prnque:printer_name = 'INVOICE'
  Add(printer_name_queue)
  prnque:printer_name = 'INVOICE - RETAIL'
  Add(printer_name_queue)
  prnque:printer_name = 'INVOICE - PROFORMA'
  Add(printer_name_queue)
  prnque:printer_name = 'PARTS ORDER'
  Add(printer_name_queue)
  prnque:printer_name = 'LABEL'
  Add(printer_name_queue)
  prnque:printer_name = 'LABEL - RETAIL'
  Add(printer_name_queue)
  prnque:printer_name = 'LABEL - QA'
  Add(printer_name_queue)
  prnque:printer_name = 'LABEL - SDS'
  Add(printer_name_queue)
  prnque:printer_name = 'LETTER'
  Add(printer_name_queue)
  prnque:printer_name = 'PICKING NOTE - RETAIL'
  Add(printer_name_queue)
  prnque:printer_name = 'STOCK LABEL'
  Add(printer_name_queue)
  prnque:printer_name = 'STOCK REQUEST LABEL'
  Add(printer_name_queue)
  prnque:printer_name = 'UNALLOCATED JOBS REPORT'
  Add(printer_name_queue)
  prnque:printer_name = 'QA FAILED NOTE'
  Add(printer_name_queue)
  
  set(LOCATION)
  loop while Access:LOCATION.Next() = Level:Benign
    !Found
    prnque:printer_name = 'PICKING NOTE - ' & loc:Location
    Add(printer_name_queue)
  End
  
  tmp:CurrentPrinter = PRINTER{PROPPRINT:Device}
  
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(dep:Record,History::dep:Record)
  SELF.AddHistoryField(?dep:Printer_Name,1)
  SELF.AddHistoryField(?dep:Printer_Path,2)
  SELF.AddHistoryField(?dep:Copies,4)
  SELF.AddHistoryField(?dep:Background,3)
  SELF.AddUpdateFile(Access:DEFPRINT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFPRINT.Open
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFPRINT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  aePrn.Init()
  FREE(printer_queue_temp)
  LOOP aePrn:Ptr = 1 TO RECORDS(aePrn.PrinterQ)
     GET(aePrn.PrinterQ,aePrn:Ptr)
     printer_queue_temp=aePrn.PrinterQ.PrinterName
     ADD(printer_queue_temp)
  END                          ! Loaded before window opened so that
                               ! DROP controls using this queue are not blanked
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?dep:Printer_Name{prop:vcr} = TRUE
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  aePrn.Kill
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFPRINT.Close
    Relate:LOCATION.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupPrinter
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupPrinter, Accepted)
          If PrinterDialog('Select printer')
             dep:Printer_Path = Upper(PRINTER{PROPPRINT:Device})
             Display(?dep:Printer_Path)
             Printer{propprint:Device} = tmp:CurrentPrinter
          End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupPrinter, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

