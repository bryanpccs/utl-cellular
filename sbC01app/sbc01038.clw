

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01038.INC'),ONCE        !Local module procedure declarations
                     END


AboutScreen PROCEDURE                                 !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
RecipAddress         STRING(10000)
MsgSubject           STRING(255)
MsgNoteText          STRING(10000)
AttachList           STRING(1000)
CCAddress            STRING(10000)
BCCAddress           STRING(10000)
Connection           STRING('Default {20}')
Disconnect           BYTE(0)
RtnVal               BYTE(0)
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
window               WINDOW('About ServiceBase 2000'),AT(,,475,375),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,468,340),USE(?Sheet1),IMM,SPREAD
                         TAB('Licenced &By'),USE(?Tab2)
                           STRING('ServiceBase 2000'),AT(8,20),USE(?String1),FONT('Tahoma',36,,FONT:bold)
                           IMAGE('pcgrey.gif'),AT(344,20,124,140),USE(?Image1)
                           PROMPT('Cellular'),AT(8,56),USE(?Prompt28),FONT('Tahoma',28,,FONT:bold)
                           STRING('<169> 1998 - 2002'),AT(8,92),USE(?String2),FONT('Tahoma',12,,FONT:bold)
                           STRING('PC Control Systems Ltd.'),AT(8,108),USE(?String3),FONT(,14,,)
                           STRING('Hamilton House'),AT(8,120),USE(?String4),FONT(,14,,)
                           STRING('66 Palmerston Road'),AT(8,132),USE(?String5),FONT(,14,,)
                           STRING('Northampton'),AT(8,144),USE(?String6),FONT(,14,,)
                           STRING('UK'),AT(8,156),USE(?String7),FONT(,14,,)
                           STRING('NN1 5EX'),AT(8,168),USE(?String8),FONT(,14,,)
                           REGION,AT(84,240,144,12),USE(?MouseOver:2),IMM,CURSOR('finger.cur')
                           STRING('Telephone: '),AT(8,204),USE(?String9),TRN,FONT(,12,,,CHARSET:ANSI)
                           STRING('01604 601677'),AT(84,204),USE(?String11),TRN,FONT(,12,,FONT:bold)
                           REGION,AT(84,228,148,12),USE(?MouseOver),IMM,CURSOR('finger.cur')
                           STRING('support@pccontrolsystems.com'),AT(84,240,144,12),USE(?Emaillink),TRN,FONT('Tahoma',12,COLOR:Navy,FONT:bold+FONT:underline,CHARSET:ANSI)
                           STRING('www.pccontrolsystems.com'),AT(84,228,148,12),USE(?Weblink),TRN,FONT('Tahoma',12,COLOR:Navy,FONT:bold+FONT:underline,CHARSET:ANSI)
                           STRING('E-mail:'),AT(8,240),USE(?String13),FONT(,12,,,CHARSET:ANSI)
                           STRING('Fax: '),AT(8,216),USE(?String10),TRN,FONT(,12,,,CHARSET:ANSI)
                           STRING('01604 601676'),AT(84,216),USE(?String12),TRN,FONT(,12,,FONT:bold)
                           STRING('Homepage:'),AT(8,228),USE(?String14),FONT(,12,,,CHARSET:ANSI)
                           IMAGE('pvsw.gif'),AT(332,312),USE(?Image3)
                           IMAGE('pwrc55.gif'),AT(428,272),USE(?Image2)
                           GROUP('Version Information'),AT(8,276,232,64),USE(?Group5),BOXED
                             STRING('Current Version: '),AT(12,288),USE(?String17),FONT(,12,,,CHARSET:ANSI)
                             STRING(@s30),AT(132,288,104,12),USE(defv:VersionNumber),FONT('Tahoma',12,,FONT:bold)
                             STRING('Licence Number: '),AT(12,304),USE(?String18),FONT(,12,,,CHARSET:ANSI)
                             STRING(@s10),AT(132,304),USE(def:License_Number,,?DEF:License_Number:2),FONT('Tahoma',12,,FONT:bold)
                             STRING('Max. Licenced Users: '),AT(12,320),USE(?String19),FONT(,12,,,CHARSET:ANSI)
                             STRING(@n4),AT(132,320),USE(def:Maximum_Users,,?DEF:Maximum_Users:2),LEFT,FONT('Tahoma',12,,FONT:bold)
                           END
                         END
                       END
                       BUTTON('&OK'),AT(356,352,56,16),USE(?OkButton),LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(412,352,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,348,468,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?Prompt28{prop:FontColor} = -1
    ?Prompt28{prop:Color} = 15066597
    ?String2{prop:FontColor} = -1
    ?String2{prop:Color} = 15066597
    ?String3{prop:FontColor} = -1
    ?String3{prop:Color} = 15066597
    ?String4{prop:FontColor} = -1
    ?String4{prop:Color} = 15066597
    ?String5{prop:FontColor} = -1
    ?String5{prop:Color} = 15066597
    ?String6{prop:FontColor} = -1
    ?String6{prop:Color} = 15066597
    ?String7{prop:FontColor} = -1
    ?String7{prop:Color} = 15066597
    ?String8{prop:FontColor} = -1
    ?String8{prop:Color} = 15066597
    ?String9{prop:FontColor} = -1
    ?String9{prop:Color} = 15066597
    ?String11{prop:FontColor} = -1
    ?String11{prop:Color} = 15066597
    ?Emaillink{prop:FontColor} = -1
    ?Emaillink{prop:Color} = 15066597
    ?Weblink{prop:FontColor} = -1
    ?Weblink{prop:Color} = 15066597
    ?String13{prop:FontColor} = -1
    ?String13{prop:Color} = 15066597
    ?String10{prop:FontColor} = -1
    ?String10{prop:Color} = 15066597
    ?String12{prop:FontColor} = -1
    ?String12{prop:Color} = 15066597
    ?String14{prop:FontColor} = -1
    ?String14{prop:Color} = 15066597
    ?Group5{prop:Font,3} = -1
    ?Group5{prop:Color} = 15066597
    ?Group5{prop:Trn} = 0
    ?String17{prop:FontColor} = -1
    ?String17{prop:Color} = 15066597
    ?defv:VersionNumber{prop:FontColor} = -1
    ?defv:VersionNumber{prop:Color} = 15066597
    ?String18{prop:FontColor} = -1
    ?String18{prop:Color} = 15066597
    ?DEF:License_Number:2{prop:FontColor} = -1
    ?DEF:License_Number:2{prop:Color} = 15066597
    ?String19{prop:FontColor} = -1
    ?String19{prop:Color} = 15066597
    ?DEF:Maximum_Users:2{prop:FontColor} = -1
    ?DEF:Maximum_Users:2{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AboutScreen')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFAULTV.Open
  Relate:DEFSTOCK.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Set(DEFAULTV)
  Access:DEFAULTV.Next()
  If Error()
      If Access:DEFAULTV.PrimeRecord() = Level:Benign
          defv:VersionNumber    = '???'
          defv:NagDate    = Today()
          If Access:DEFAULTV.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:DEFAULTV..TryInsert() = Level:Benign
              !Insert Failed
          End !DIf Access:DEFAULTV..TryInsert() = Level:Benign
      End !If Access:DEFAULTV..PrimeRecord() = Level:Benign
      Set(DEFAULTV)
      Access:DEFAULTV.Next()
  End !Error()
  
  If defv:ReUpdate = 0
      NagScreen(1)
  Else !defv:ReUpdate = 0
      If Today() => defv:NagDate
          NagScreen(0)
      End !Today() > defv:NagDate
  
  End !defv:ReUpdate = 0
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFAULTV.Close
    Relate:DEFSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Set(Defaults)
      If access:defaults.next()
          get(defaults,0)
          if access:defaults.primerecord() = level:benign
              if access:defaults.insert()
                  access:defaults.cancelautoinc()
              end
          end!if access:defaults.primerecord() = level:benign
      End!If access:defaults.next()
      
      Set(defstock)
      If access:defstock.next()
          get(defstock,0)
          if access:defstock.primerecord() = level:benign
              if access:defstock.insert()
                  access:defstock.cancelautoinc()
              end
          end!if access:defstock.primerecord() = level:benign
      End!If access:defstock.next()
      
      
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

