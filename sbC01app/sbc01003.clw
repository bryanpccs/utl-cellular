

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01003.INC'),ONCE        !Local module procedure declarations
                     END


Loan_Changing_Screen PROCEDURE                        !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Changing Loan Unit'),AT(,,151,111),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,142,76),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           OPTION('Reason For Changing Loan Unit'),AT(9,8,134,68),USE(GLO:Select1),BOXED
                             RADIO('Replacing Faulty Unit'),AT(22,24),USE(?Option1:Radio1),VALUE('1')
                             RADIO('Alternative Model Required'),AT(22,40),USE(?Option1:Radio2),VALUE('2')
                             RADIO('Not Required : Re-stock Unit'),AT(22,56),USE(?Option1:Radio3),VALUE('3')
                           END
                         END
                       END
                       PANEL,AT(4,84,142,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(28,88,56,16),USE(?OkButton),LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(84,88,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?GLO:Select1{prop:Font,3} = -1
    ?GLO:Select1{prop:Color} = 15066597
    ?GLO:Select1{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    ?Option1:Radio3{prop:Font,3} = -1
    ?Option1:Radio3{prop:Color} = 15066597
    ?Option1:Radio3{prop:Trn} = 0
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Loan_Changing_Screen')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Option1:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If glo:select1 = ''
      	Case MessageEx('You must select a reason.','ServiceBase 2000',|
      	               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
      		Of 1 ! &OK Button
      	End!Case MessageEx
          Cycle
      End
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = ''
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Case glo:select2
          Of 'L'
              Window{prop:text} = 'Changing A Loan Unit'
              ?glo:select1{prop:text} = 'Reason For Changing Loan Unit'
          Of 'E'
              Window{prop:text} = 'Changing An Exchange Unit'
              ?glo:select1{prop:text} = 'Reason For Changing Exchange Unit'
          Of 'EA'
              Window{prop:text} = 'Changing An Exchange Unit'
              ?glo:select1{prop:text} = 'Reason For Changing Exchange Unit'
              Disable(?Option1:Radio2)
          Of 'LA'
              Window{prop:text} = 'Changing A Loan Unit Accessory'
              ?glo:select1{prop:text} = 'Reason For Changing Loan Unit Accessory'
              Disable(?Option1:Radio2)
      
      End
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

