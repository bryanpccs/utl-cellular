

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01013.INC'),ONCE        !Local module procedure declarations
                     END


Insert_Stardard_Charges PROCEDURE                     !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Charge_Type_Pointer           LIKE(GLO:Charge_Type_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGMOUSE         BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Unit_Type_Pointer             LIKE(GLO:Unit_Type_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Repair_Type_Pointer           LIKE(GLO:Repair_Type_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
ThisThreadActive BYTE
LocalRequest         LONG
save_man_id          USHORT,AUTO
Trade_Name_Temp      STRING(255)
FilesOpened          BYTE
Model_Number_Temp    STRING(30)
Cost_Temp            REAL
Charge_Type_Tag      STRING(1)
Unit_Type_Tag        STRING(1)
Repair_Type_Tag      STRING(1)
Manufacturer_Temp    STRING(30)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:cha              STRING(1)
tmp:war              STRING(1)
tmp:repcha           STRING(1)
tmp:repwar           STRING(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Model_Number_Temp
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Manufacturer_Temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
FDCB24::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
BRW2::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
Charge_Type_Tag        LIKE(Charge_Type_Tag)          !List box control field - type derived from local data
Charge_Type_Tag_Icon   LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
tmp:cha                LIKE(tmp:cha)                  !List box control field - type derived from local data
tmp:cha_NormalFG       LONG                           !Normal forground color
tmp:cha_NormalBG       LONG                           !Normal background color
tmp:cha_SelectedFG     LONG                           !Selected forground color
tmp:cha_SelectedBG     LONG                           !Selected background color
tmp:cha_Icon           LONG                           !Entry's icon ID
tmp:war                LIKE(tmp:war)                  !List box control field - type derived from local data
tmp:war_Icon           LONG                           !Entry's icon ID
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW3::View:Browse    VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
Unit_Type_Tag          LIKE(Unit_Type_Tag)            !List box control field - type derived from local data
Unit_Type_Tag_Icon     LONG                           !Entry's icon ID
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(REPAIRTY)
                       PROJECT(rep:Repair_Type)
                       PROJECT(rep:Manufacturer)
                       PROJECT(rep:Model_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
Repair_Type_Tag        LIKE(Repair_Type_Tag)          !List box control field - type derived from local data
Repair_Type_Tag_Icon   LONG                           !Entry's icon ID
rep:Repair_Type        LIKE(rep:Repair_Type)          !List box control field - type derived from field
tmp:repcha             LIKE(tmp:repcha)               !List box control field - type derived from local data
tmp:repcha_Icon        LONG                           !Entry's icon ID
tmp:repwar             LIKE(tmp:repwar)               !List box control field - type derived from local data
tmp:repwar_Icon        LONG                           !Entry's icon ID
rep:Manufacturer       LIKE(rep:Manufacturer)         !Primary key field - type derived from field
rep:Model_Number       LIKE(rep:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
quickwindow          WINDOW('Inserting/Amending Standard Charges'),AT(,,652,323),FONT('Tahoma',8,,),CENTER,IMM,ICON('Pc.ico'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,644,52),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?General_Tab)
                           PROMPT('Manufacturer'),AT(8,20),USE(?Prompt3)
                           COMBO(@s30),AT(84,20,124,10),USE(Manufacturer_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           BUTTON,AT(212,20,10,10),USE(?Lookup_Manufacturer),ICON('list3.ico')
                           BUTTON('View Standard Charges'),AT(392,32,84,16),USE(?view_charges),LEFT,ICON('Spy.gif')
                           PROMPT('Model Number'),AT(8,36,48,8),USE(?Prompt1)
                           COMBO(@s30),AT(84,36,124,10),USE(Model_Number_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Cost'),AT(268,36),USE(?Cost_Temp:Prompt),TRN
                           ENTRY(@n14.2),AT(300,36,64,10),USE(Cost_Temp),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       SHEET,AT(4,60,212,232),USE(?Sheet2),SPREAD
                         TAB('By Charge Type'),USE(?Tab2)
                           ENTRY(@s30),AT(8,76,124,10),USE(cha:Charge_Type),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           LIST,AT(8,92,144,176),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),COLUMN,FORMAT('11L(2)I@s1@97L(2)|~Charge Type~S(124)@s30@11L(2)*J~C~C@s1@11L(2)I~W~C@s1@/'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(156,92,56,16),USE(?Insert:3),LEFT,ICON('insert.gif')
                           BUTTON('&Delete'),AT(156,252,56,16),USE(?Delete:3),LEFT,ICON('delete.gif')
                           BUTTON('Tag All'),AT(56,272,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Tag'),AT(8,272,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('&Rev tags'),AT(396,300,12,16),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(408,300,12,16),USE(?DASSHOWTAG),HIDE
                           BUTTON('Untag All'),AT(104,272,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                         END
                       END
                       SHEET,AT(220,60,212,232),USE(?Sheet3),SPREAD
                         TAB('By Unit Type'),USE(?Tab3)
                           ENTRY(@s30),AT(224,76,124,10),USE(uni:Unit_Type),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('&Insert'),AT(372,92,56,16),USE(?Insert:2),LEFT,ICON('insert.gif')
                           BUTTON('&Delete'),AT(372,252,56,16),USE(?Delete:2),LEFT,ICON('delete.gif')
                           LIST,AT(224,92,144,176),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)~Unit Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('Tag All'),AT(272,272,48,16),USE(?DASTAGAll:2),LEFT,ICON('tagall.gif')
                           BUTTON('T&ag'),AT(224,272,48,16),USE(?DASTAG:2),LEFT,ICON('tag.gif')
                           BUTTON('&Rev tags'),AT(420,300,12,16),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(432,300,12,16),USE(?DASSHOWTAG:2),HIDE
                           BUTTON('Untag All'),AT(320,272,48,16),USE(?DASUNTAGALL:2),LEFT,ICON('untag.gif')
                         END
                       END
                       SHEET,AT(436,60,212,232),USE(?Sheet4),SPREAD
                         TAB('By Repair Type'),USE(?Tab4)
                           ENTRY(@s30),AT(440,76,124,10),USE(rep:Repair_Type),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           LIST,AT(440,92,144,176),USE(?List:3),IMM,NOBAR,VSCROLL,MSG('Browsing Records'),COLUMN,FORMAT('11L(2)I@s1@98L(2)|~Repair Type~S(124)@s30@11L(2)I~C~C@s1@11L(2)I~W~C@s1@'),FROM(Queue:Browse:2)
                           BUTTON('&Insert'),AT(588,92,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Delete'),AT(588,252,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                           BUTTON('Tag All'),AT(488,272,48,16),USE(?DASTAGAll:3),LEFT,ICON('tagall.gif')
                           BUTTON('Ta&g'),AT(440,272,48,16),USE(?DASTAG:3),LEFT,ICON('tag.gif')
                           BUTTON('&Rev tags'),AT(508,300,12,16),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(520,300,12,16),USE(?DASSHOWTAG:3),HIDE
                           BUTTON('Untag All'),AT(536,272,48,16),USE(?DASUNTAGALL:3),LEFT,ICON('untag.gif')
                         END
                       END
                       PANEL,AT(4,296,644,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Add To Standard Charges'),AT(8,300,112,16),USE(?AddToStandardCharges),LEFT,ICON('insert.gif')
                       BUTTON('Delete Standard Charges'),AT(124,300,112,16),USE(?DeleteStandardCharges),LEFT,ICON('Delete.gif')
                       BUTTON('Close'),AT(588,300,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB24               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW2::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW3                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW4                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  IncrementalLocatorClass          !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
save_mod_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?General_Tab{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?Manufacturer_Temp{prop:ReadOnly} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 15066597
    Elsif ?Manufacturer_Temp{prop:Req} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 8454143
    Else ! If ?Manufacturer_Temp{prop:Req} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 16777215
    End ! If ?Manufacturer_Temp{prop:Req} = True
    ?Manufacturer_Temp{prop:Trn} = 0
    ?Manufacturer_Temp{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?Model_Number_Temp{prop:ReadOnly} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 15066597
    Elsif ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 8454143
    Else ! If ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 16777215
    End ! If ?Model_Number_Temp{prop:Req} = True
    ?Model_Number_Temp{prop:Trn} = 0
    ?Model_Number_Temp{prop:FontStyle} = font:Bold
    ?Cost_Temp:Prompt{prop:FontColor} = -1
    ?Cost_Temp:Prompt{prop:Color} = 15066597
    If ?Cost_Temp{prop:ReadOnly} = True
        ?Cost_Temp{prop:FontColor} = 65793
        ?Cost_Temp{prop:Color} = 15066597
    Elsif ?Cost_Temp{prop:Req} = True
        ?Cost_Temp{prop:FontColor} = 65793
        ?Cost_Temp{prop:Color} = 8454143
    Else ! If ?Cost_Temp{prop:Req} = True
        ?Cost_Temp{prop:FontColor} = 65793
        ?Cost_Temp{prop:Color} = 16777215
    End ! If ?Cost_Temp{prop:Req} = True
    ?Cost_Temp{prop:Trn} = 0
    ?Cost_Temp{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?cha:Charge_Type{prop:ReadOnly} = True
        ?cha:Charge_Type{prop:FontColor} = 65793
        ?cha:Charge_Type{prop:Color} = 15066597
    Elsif ?cha:Charge_Type{prop:Req} = True
        ?cha:Charge_Type{prop:FontColor} = 65793
        ?cha:Charge_Type{prop:Color} = 8454143
    Else ! If ?cha:Charge_Type{prop:Req} = True
        ?cha:Charge_Type{prop:FontColor} = 65793
        ?cha:Charge_Type{prop:Color} = 16777215
    End ! If ?cha:Charge_Type{prop:Req} = True
    ?cha:Charge_Type{prop:Trn} = 0
    ?cha:Charge_Type{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    If ?uni:Unit_Type{prop:ReadOnly} = True
        ?uni:Unit_Type{prop:FontColor} = 65793
        ?uni:Unit_Type{prop:Color} = 15066597
    Elsif ?uni:Unit_Type{prop:Req} = True
        ?uni:Unit_Type{prop:FontColor} = 65793
        ?uni:Unit_Type{prop:Color} = 8454143
    Else ! If ?uni:Unit_Type{prop:Req} = True
        ?uni:Unit_Type{prop:FontColor} = 65793
        ?uni:Unit_Type{prop:Color} = 16777215
    End ! If ?uni:Unit_Type{prop:Req} = True
    ?uni:Unit_Type{prop:Trn} = 0
    ?uni:Unit_Type{prop:FontStyle} = font:Bold
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Sheet4{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    If ?rep:Repair_Type{prop:ReadOnly} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 15066597
    Elsif ?rep:Repair_Type{prop:Req} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 8454143
    Else ! If ?rep:Repair_Type{prop:Req} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 16777215
    End ! If ?rep:Repair_Type{prop:Req} = True
    ?rep:Repair_Type{prop:Trn} = 0
    ?rep:Repair_Type{prop:FontStyle} = font:Bold
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW2.UpdateBuffer
   GLO:Q_ChargeType.Charge_Type_Pointer = cha:Charge_Type
   GET(GLO:Q_ChargeType,GLO:Q_ChargeType.Charge_Type_Pointer)
  IF ERRORCODE()
     GLO:Q_ChargeType.Charge_Type_Pointer = cha:Charge_Type
     ADD(GLO:Q_ChargeType,GLO:Q_ChargeType.Charge_Type_Pointer)
    Charge_Type_Tag = '*'
  ELSE
    DELETE(GLO:Q_ChargeType)
    Charge_Type_Tag = ''
  END
    Queue:Browse.Charge_Type_Tag = Charge_Type_Tag
  IF (charge_type_tag = '*')
    Queue:Browse.Charge_Type_Tag_Icon = 3
  ELSE
    Queue:Browse.Charge_Type_Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW2.Reset
  FREE(GLO:Q_ChargeType)
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Q_ChargeType.Charge_Type_Pointer = cha:Charge_Type
     ADD(GLO:Q_ChargeType,GLO:Q_ChargeType.Charge_Type_Pointer)
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Q_ChargeType)
  BRW2.Reset
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Q_ChargeType)
    GET(GLO:Q_ChargeType,QR#)
    DASBRW::6:QUEUE = GLO:Q_ChargeType
    ADD(DASBRW::6:QUEUE)
  END
  FREE(GLO:Q_ChargeType)
  BRW2.Reset
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Charge_Type_Pointer = cha:Charge_Type
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Charge_Type_Pointer)
    IF ERRORCODE()
       GLO:Q_ChargeType.Charge_Type_Pointer = cha:Charge_Type
       ADD(GLO:Q_ChargeType,GLO:Q_ChargeType.Charge_Type_Pointer)
    END
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW2.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW3.UpdateBuffer
   GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
   GET(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
  IF ERRORCODE()
     GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
     ADD(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
    Unit_Type_Tag = '*'
  ELSE
    DELETE(GLO:Q_UnitType)
    Unit_Type_Tag = ''
  END
    Queue:Browse:1.Unit_Type_Tag = Unit_Type_Tag
  IF (unit_type_tag = '*')
    Queue:Browse:1.Unit_Type_Tag_Icon = 2
  ELSE
    Queue:Browse:1.Unit_Type_Tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::7:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(GLO:Q_UnitType)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
     ADD(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::7:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Q_UnitType)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::7:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Q_UnitType)
    GET(GLO:Q_UnitType,QR#)
    DASBRW::7:QUEUE = GLO:Q_UnitType
    ADD(DASBRW::7:QUEUE)
  END
  FREE(GLO:Q_UnitType)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Unit_Type_Pointer = uni:Unit_Type
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Unit_Type_Pointer)
    IF ERRORCODE()
       GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
       ADD(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW4.UpdateBuffer
   GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
   GET(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
  IF ERRORCODE()
     GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
     ADD(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
    Repair_Type_Tag = '*'
  ELSE
    DELETE(GLO:q_RepairType)
    Repair_Type_Tag = ''
  END
    Queue:Browse:2.Repair_Type_Tag = Repair_Type_Tag
  IF (repair_type_tag = '*')
    Queue:Browse:2.Repair_Type_Tag_Icon = 3
  ELSE
    Queue:Browse:2.Repair_Type_Tag_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::8:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(GLO:q_RepairType)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
     ADD(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::8:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:q_RepairType)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::8:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:q_RepairType)
    GET(GLO:q_RepairType,QR#)
    DASBRW::8:QUEUE = GLO:q_RepairType
    ADD(DASBRW::8:QUEUE)
  END
  FREE(GLO:q_RepairType)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Repair_Type_Pointer = rep:Repair_Type
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Repair_Type_Pointer)
    IF ERRORCODE()
       GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
       ADD(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
UnTagAll    Routine
    Do DASBRW::6:DASUNTAGALL
    Do DASBRW::7:DASUNTAGALL
    Do DASBRW::8:DASUNTAGALL
Insert_Charges      Routine
    Loop x# = 1 To Records(glo:Q_ChargeType)
        Get(glo:Q_ChargeType,x#)
        Loop y# = 1 To Records(glo:Q_UnitType)
            Get(glo:Q_UnitType,y#)
            Loop z# = 1 To Records(glo:Q_RepairType)
                Get(glo:Q_RepairType,z#)

                Do GetNextRecord2
                cancelcheck# += 1
                If cancelcheck# > (RecordsToProcess/100)
                    Do cancelcheck
                    If tmp:cancel = 1
                        Break
                    End!If tmp:cancel = 1
                    cancelcheck# = 0
                End!If cancelcheck# > 50

                access:repairty.clearkey(rep:repair_type_key)
                rep:manufacturer = manufacturer_temp
                rep:model_number = model_number_temp
                rep:repair_type  = glo:repair_type_pointer
                if access:repairty.fetch(rep:manufacturer_key) = Level:Benign
                    Case glo:select12
                        Of 'STANDARD'
                                access:stdchrge.clearkey(sta:model_number_charge_key)
                                sta:model_number = model_number_temp
                                sta:charge_type  = glo:charge_type_pointer
                                sta:unit_type    = glo:unit_type_pointer
                                sta:repair_type  = glo:repair_type_pointer
                                if access:stdchrge.fetch(sta:model_number_charge_key)
                                   sta:model_number = model_number_temp
                                    sta:charge_type  = glo:charge_type_pointer
                                    sta:unit_type    = glo:unit_type_pointer
                                    sta:repair_type  = glo:repair_type_pointer
                                    sta:cost         = cost_temp
                                    Access:stdchrge.Insert()
                                Else
                                    sta:cost    = cost_temp
                                    Access:stdchrge.Update()
                                end

                        Of 'TRADE'
                            access:trachrge.clearkey(trc:account_charge_key)
                            trc:account_number = glo:select11
                            trc:model_number   = model_number_temp
                            trc:charge_type    = glo:charge_type_pointer
                            trc:unit_type      = glo:unit_type_pointer
                            trc:repair_type    = glo:repair_type_pointer
                            if access:trachrge.fetch(trc:account_charge_key)
                                trc:account_number = glo:select11
                                trc:model_number   = model_number_temp
                                trc:charge_type    = glo:charge_type_pointer
                                trc:unit_type      = glo:unit_type_pointer
                                trc:repair_type    = glo:repair_type_pointer
                                trc:cost           = cost_temp
                                access:trachrge.insert()
                            Else
                                trc:cost            = cost_temp
                                access:trachrge.update()
                            end


                        Of 'SUBTRADE'
                            access:subchrge.clearkey(suc:model_repair_type_key)
                            suc:account_number = glo:select11
                            suc:model_number   = model_number_temp
                            suc:charge_type    = glo:charge_type_pointer
                            suc:unit_type      = glo:unit_type_pointer
                            suc:repair_type    = glo:repair_type_pointer
                            if access:subchrge.fetch(suc:model_repair_type_key)
                                suc:account_number = glo:select11
                                suc:model_number   = model_number_temp
                                suc:charge_type    = glo:charge_type_pointer
                                suc:unit_type      = glo:unit_type_pointer
                                suc:repair_type    = glo:repair_type_pointer
                                suc:cost           = cost_temp
                                access:subchrge.insert()
                            Else
                                suc:cost            = cost_temp
                                access:subchrge.update()
                            end

                    End
                end!if access:repairty.fetch(rep:manufacturer_key) = Level:Benign
            End
        End
    end
Delete_Charges      Routine
    Loop x# = 1 To Records(glo:Q_ChargeType)
        Get(glo:Q_ChargeType,x#)
        Loop y# = 1 To Records(glo:Q_UnitType)
            Get(glo:Q_UnitType,y#)
            Loop z# = 1 To Records(glo:Q_RepairType)
                Get(glo:Q_RepairType,z#)

                Do GetNextRecord2
                cancelcheck# += 1
                If cancelcheck# > (RecordsToProcess/100)
                    Do cancelcheck
                    If tmp:cancel = 1
                        Break
                    End!If tmp:cancel = 1
                    cancelcheck# = 0
                End!If cancelcheck# > 50

                access:repairty.clearkey(rep:repair_type_key)
                rep:manufacturer = manufacturer_temp
                rep:model_number = model_number_temp
                rep:repair_type  = glo:repair_type_pointer
                if access:repairty.fetch(rep:manufacturer_key) = Level:Benign
                    Case glo:select12
                        Of 'STANDARD'
                                access:stdchrge.clearkey(sta:model_number_charge_key)
                                sta:model_number = model_number_temp
                                sta:charge_type  = glo:charge_type_pointer
                                sta:unit_type    = glo:unit_type_pointer
                                sta:repair_type  = glo:repair_type_pointer
                                if access:stdchrge.fetch(sta:model_number_charge_key)
                                Else
                                    Delete(stdchrge)
                                end

                        Of 'TRADE'
                            access:trachrge.clearkey(trc:account_charge_key)
                            trc:account_number = glo:select11
                            trc:model_number   = model_number_temp
                            trc:charge_type    = glo:charge_type_pointer
                            trc:unit_type      = glo:unit_type_pointer
                            trc:repair_type    = glo:repair_type_pointer
                            if access:trachrge.fetch(trc:account_charge_key)
                            Else
                                Delete(trachrge)
                            end


                        Of 'SUBTRADE'
                            access:subchrge.clearkey(suc:model_repair_type_key)
                            suc:account_number = glo:select11
                            suc:model_number   = model_number_temp
                            suc:charge_type    = glo:charge_type_pointer
                            suc:unit_type      = glo:unit_type_pointer
                            suc:repair_type    = glo:repair_type_pointer
                            if access:subchrge.fetch(suc:model_repair_type_key)
                            Else
                                Delete(subchrge)
                            end

                    End
                end!if access:repairty.fetch(rep:manufacturer_key) = Level:Benign
            End
        End
    end
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Insert_Stardard_Charges')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Insert_Stardard_Charges'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CHARTYPE.Open
  Access:STDCHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:TRACHRGE.UseFile
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:CHARTYPE,SELF)
  BRW3.Init(?List:2,Queue:Browse:1.ViewPosition,BRW3::View:Browse,Queue:Browse:1,Relate:UNITTYPE,SELF)
  BRW4.Init(?List:3,Queue:Browse:2.ViewPosition,BRW4::View:Browse,Queue:Browse:2,Relate:REPAIRTY,SELF)
  OPEN(quickwindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ! support for CPCS
  BRW2.Q &= Queue:Browse
  BRW2.RetainRow = 0
  BRW2.AddSortOrder(,cha:Charge_Type_Key)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(?CHA:Charge_Type,cha:Charge_Type,1,BRW2)
  BIND('Charge_Type_Tag',Charge_Type_Tag)
  BIND('tmp:cha',tmp:cha)
  BIND('tmp:war',tmp:war)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick.ico'
  ?List{PROP:IconList,3} = '~tick1.ico'
  BRW2.AddField(Charge_Type_Tag,BRW2.Q.Charge_Type_Tag)
  BRW2.AddField(cha:Charge_Type,BRW2.Q.cha:Charge_Type)
  BRW2.AddField(tmp:cha,BRW2.Q.tmp:cha)
  BRW2.AddField(tmp:war,BRW2.Q.tmp:war)
  BRW3.Q &= Queue:Browse:1
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,uni:Unit_Type_Key)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(?uni:Unit_Type,uni:Unit_Type,1,BRW3)
  BIND('Unit_Type_Tag',Unit_Type_Tag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(Unit_Type_Tag,BRW3.Q.Unit_Type_Tag)
  BRW3.AddField(uni:Unit_Type,BRW3.Q.uni:Unit_Type)
  BRW4.Q &= Queue:Browse:2
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,rep:Model_Number_Key)
  BRW4.AddRange(rep:Model_Number,Model_Number_Temp)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(?rep:Repair_Type,rep:Repair_Type,1,BRW4)
  BIND('Repair_Type_Tag',Repair_Type_Tag)
  BIND('tmp:repcha',tmp:repcha)
  BIND('tmp:repwar',tmp:repwar)
  BIND('Model_Number_Temp',Model_Number_Temp)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick.ico'
  ?List:3{PROP:IconList,3} = '~tick1.ico'
  BRW4.AddField(Repair_Type_Tag,BRW4.Q.Repair_Type_Tag)
  BRW4.AddField(rep:Repair_Type,BRW4.Q.rep:Repair_Type)
  BRW4.AddField(tmp:repcha,BRW4.Q.tmp:repcha)
  BRW4.AddField(tmp:repwar,BRW4.Q.tmp:repwar)
  BRW4.AddField(rep:Manufacturer,BRW4.Q.rep:Manufacturer)
  BRW4.AddField(rep:Model_Number,BRW4.Q.rep:Model_Number)
  FDCB5.Init(Model_Number_Temp,?Model_Number_Temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(mod:Manufacturer_Key)
  FDCB5.AddRange(mod:Manufacturer,Manufacturer_Temp)
  FDCB5.AddField(mod:Model_Number,FDCB5.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB24.Init(Manufacturer_Temp,?Manufacturer_Temp,Queue:FileDropCombo:1.ViewPosition,FDCB24::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB24.Q &= Queue:FileDropCombo:1
  FDCB24.AddSortOrder(man:Manufacturer_Key)
  FDCB24.AddField(man:Manufacturer,FDCB24.Q.man:Manufacturer)
  FDCB24.AddField(man:RecordNumber,FDCB24.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB24.WindowComponent)
  FDCB24.DefaultFill = 0
  BRW4.AskProcedure = 1
  BRW3.AskProcedure = 2
  BRW2.AskProcedure = 3
  BRW2.Popup.AddItemMimic('Popup32',?DASTAG)
  BRW2.Popup.AddItemMimic('Popup33',?DASTAGAll)
  BRW2.Popup.AddItem('-','SeparatorPopup34')
  BRW2.Popup.AddItemMimic('Popup34',?DASUNTAGALL)
  BRW3.Popup.AddItemMimic('Popup35',?DASTAG:2)
  BRW3.Popup.AddItemMimic('Popup36',?DASTAGAll:2)
  BRW3.Popup.AddItem('-','SeparatorPopup37')
  BRW3.Popup.AddItemMimic('Popup37',?DASUNTAGALL:2)
  BRW4.Popup.AddItemMimic('Popup38',?DASTAG:3)
  BRW4.Popup.AddItemMimic('Popup39',?DASTAGAll:3)
  BRW4.Popup.AddItem('-','SeparatorPopup40')
  BRW4.Popup.AddItemMimic('Popup40',?DASUNTAGALL:3)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Q_ChargeType)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Q_UnitType)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:q_RepairType)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Q_ChargeType)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Q_UnitType)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:q_RepairType)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Insert_Stardard_Charges'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  error# = 0
  If number = 1
  
      If manufacturer_temp = '' Or model_number_temp = ''
  		Case MessageEx('You must select a Manufacturer and Model Number.','ServiceBase 2000',|
  		               'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  		End!Case MessageEx
          error# = 1
      End!If manufacturer_temp = '' Or model_number_temp = ''
  
  End!If number = 1
  
  If error# = 0
  
  
      glo:select13   = manufacturer_temp
      glo:select14   = model_number_temp
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_Repair_Type
      UpdateUnittype
      UpdateCharType
    END
    ReturnValue = GlobalResponse
  END
      glo:select13   = ''
      glo:select14   = ''
  End!End!If error# = 0
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Manufacturer_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
      FDCB5.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
    OF ?Lookup_Manufacturer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Manufacturer, Accepted)
      case globalresponse
          of requestcompleted
              manufacturer_temp   = man:manufacturer
      !        select(?+2)
          of requestcancelled
      !        man:manufacturer = ''
              select(?-1)
      end!case globalreponse
      display(?manufacturer_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Manufacturer, Accepted)
    OF ?view_charges
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?view_charges, Accepted)
      
          !! ** Bryan Harrison (c)1998 **
      
              GLO:Select1 = Model_Number_Temp
      
      
      Case glo:select12
          Of 'STANDARD'
       START(Browse_Standard_Charges,25000)           ! Initiate the Thread
          Of 'TRADE'
       START(Browse_Trade_Account_Charges,25000)      ! Initiate the Thread
          Of 'SUBTRADE'
       START(Browse_Sub_Account_Charges,25000)        ! Initiate the Thread
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?view_charges, Accepted)
    OF ?Model_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
      Brw4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?AddToStandardCharges
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddToStandardCharges, Accepted)
      Case AddChargesOption()
          Of 0 !Cancel
          Of 1 !Model Number
              Case MessageEx('The Charge Rate for the selected: Model Number, Charge Types, Unit Types and Repair Types will be changed.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
      
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
                      recordstoprocess    = Records(glo:Q_ChargeType) * Records(glo:Q_UnitType) * Records(glo:Q_RepairType)
                      setcursor(cursor:wait)
                  
                      Do insert_charges
                  
                      setcursor()
                      Close(progresswindow)
                      Do Untagall
                  Of 2 ! &No Button
              End!Case MessageEx
      
      
          Of 2 !Manufactuere
              Case MessageEx('The Charge Rate for the selected: Model Number, Charge Types, Unit Types and Repair Types will be changed.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      Case MessageEx('Warning! If you continue ALL Models under this Manufacturer will have the same pricing structure. Any previous pricing structure will be over-written.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000','Styles\idea.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                          Of 1 ! &Yes Button
                          recordspercycle     = 25
                          recordsprocessed    = 0
                          percentprogress     = 0
                          open(progresswindow)
                          progress:thermometer    = 0
                          ?progress:pcttext{prop:text} = '0% Completed'
                          recordstoprocess    = Records(modelnum) * Records(glo:Q_ChargeType) * Records(glo:Q_UnitType) * Records(glo:Q_RepairType)
                          manufacturer" = manufacturer_temp
                          model" = model_number_temp
                          setcursor(cursor:wait)
                          save_mod_id = access:modelnum.savefile()
                          access:modelnum.clearkey(mod:manufacturer_key)
                          mod:manufacturer = manufacturer_temp
                          set(mod:manufacturer_key,mod:manufacturer_key)
                          loop
                              if access:modelnum.next()
                                 break
                              end !if
                              if mod:manufacturer <> manufacturer_temp      |
                                  then break.  ! end if
                              yldcnt# += 1
                              if yldcnt# > 25
                                 yield() ; yldcnt# = 0
                              end !if
                              model_number_temp = mod:model_number
                  
                              Do insert_charges
                  
                          end !loop
                          access:modelnum.restorefile(save_mod_id)
                  
                  
                          setcursor()
                          Close(progresswindow)
                          Do Untagall
                          model_number_temp = model"
                          manufacturer_temp = manufacturer"
                  
                  
                          Of 2 ! &No Button
                      End!Case MessageEx
                  Of 2 ! &No Button
              End!Case MessageEx
      
      
          Of 3 !ALL
              Case MessageEx('The Charge Rate for the selected: Model Number, Charge Types, Unit Types and Repair Types will be changed.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                  
                      Case MessageEx('Warning! If you continue ALL Manufacturers and ALL Models will have the same pricing structure. Any previous pricing structure will be over-written.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000','Styles\idea.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                          recordspercycle     = 25
                          recordsprocessed    = 0
                          percentprogress     = 0
                          open(progresswindow)
                          progress:thermometer    = 0
                          ?progress:pcttext{prop:text} = '0% Completed'
                          recordstoprocess    = Records(modelnum) * Records(glo:Q_ChargeType) * Records(glo:Q_UnitType) * Records(glo:Q_RepairType) * Records(manufact)
                          manufacturer" = manufacturer_temp
                          model" = model_number_temp
                          setcursor(cursor:wait)
                  
                          save_man_id = access:manufact.savefile()
                          set(man:manufacturer_key)
                          loop
                              if access:manufact.next()
                                 break
                              end !if
                  
                              save_mod_id = access:modelnum.savefile()
                              access:modelnum.clearkey(mod:manufacturer_key)
                              mod:manufacturer = man:manufacturer
                              set(mod:manufacturer_key,mod:manufacturer_key)
                              loop
                                  if access:modelnum.next()
                                     break
                                  end !if
                                  if mod:manufacturer <> man:manufacturer      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  model_number_temp = mod:model_number
                  
                                  Do insert_charges
                  
                              end !loop
                              access:modelnum.restorefile(save_mod_id)
                          end !loop
                          access:manufact.restorefile(save_man_id)
                  
                  
                          setcursor()
                          Close(progresswindow)
                          Do Untagall
                          model_number_temp = model"
                          manufacturer_temp = manufacturer"
                  
                  
                          Of 2 ! &No Button
                      End!Case MessageEx
                  Of 2 ! &No Button
              End!Case MessageEx
      
      End !AddChargesOption
      BRW2.ResetSort(1)
      BRW3.ResetSort(1)
      BRW4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddToStandardCharges, Accepted)
    OF ?DeleteStandardCharges
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DeleteStandardCharges, Accepted)
      Case DeleteChargesOption()
          Of 0 !Cancel
          Of 1 !Model Number
              Case MessageEx('The Charge Rate for the selected: Model Number, Charge Types, Unit Types and Repair Types will be DELETED.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                             'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                  Of 1 ! &Yes Button
      
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
                      recordstoprocess    = Records(glo:Q_ChargeType) * Records(glo:Q_UnitType) * Records(glo:Q_RepairType)
                      setcursor(cursor:wait)
                  
                      Do Delete_charges
                  
                      setcursor()
                      Close(progresswindow)
                      Do Untagall
                  Of 2 ! &No Button
              End!Case MessageEx
      
      
          Of 2 !Manufactuere
              Case MessageEx('The Charge Rate for the selected: Model Number, Charge Types, Unit Types and Repair Types will be DELETED.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                             'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                  Of 1 ! &Yes Button
                          recordspercycle     = 25
                          recordsprocessed    = 0
                          percentprogress     = 0
                          open(progresswindow)
                          progress:thermometer    = 0
                          ?progress:pcttext{prop:text} = '0% Completed'
                          recordstoprocess    = Records(modelnum) * Records(glo:Q_ChargeType) * Records(glo:Q_UnitType) * Records(glo:Q_RepairType)
                          manufacturer" = manufacturer_temp
                          model" = model_number_temp
                          setcursor(cursor:wait)
                          save_mod_id = access:modelnum.savefile()
                          access:modelnum.clearkey(mod:manufacturer_key)
                          mod:manufacturer = manufacturer_temp
                          set(mod:manufacturer_key,mod:manufacturer_key)
                          loop
                              if access:modelnum.next()
                                 break
                              end !if
                              if mod:manufacturer <> manufacturer_temp      |
                                  then break.  ! end if
                              yldcnt# += 1
                              if yldcnt# > 25
                                 yield() ; yldcnt# = 0
                              end !if
                              model_number_temp = mod:model_number
                  
                              Do Delete_charges
                  
                          end !loop
                          access:modelnum.restorefile(save_mod_id)
                  
                  
                          setcursor()
                          Close(progresswindow)
                          Do Untagall
                          model_number_temp = model"
                          manufacturer_temp = manufacturer"
                  
                  Of 2 ! &No Button
              End!Case MessageEx
      
      
          Of 3 !ALL
              Case MessageEx('The Charge Rate for the selected: Model Number, Charge Types, Unit Types and Repair Types will be DELETED.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                             'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                  Of 1 ! &Yes Button
                  
                          recordspercycle     = 25
                          recordsprocessed    = 0
                          percentprogress     = 0
                          open(progresswindow)
                          progress:thermometer    = 0
                          ?progress:pcttext{prop:text} = '0% Completed'
                          recordstoprocess    = Records(modelnum) * Records(glo:Q_ChargeType) * Records(glo:Q_UnitType) * Records(glo:Q_RepairType) * Records(manufact)
                          manufacturer" = manufacturer_temp
                          model" = model_number_temp
                          setcursor(cursor:wait)
                  
                          save_man_id = access:manufact.savefile()
                          set(man:manufacturer_key)
                          loop
                              if access:manufact.next()
                                 break
                              end !if
                  
                              save_mod_id = access:modelnum.savefile()
                              access:modelnum.clearkey(mod:manufacturer_key)
                              mod:manufacturer = man:manufacturer
                              set(mod:manufacturer_key,mod:manufacturer_key)
                              loop
                                  if access:modelnum.next()
                                     break
                                  end !if
                                  if mod:manufacturer <> man:manufacturer      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  model_number_temp = mod:model_number
                  
                                  Do Delete_charges
                  
                              end !loop
                              access:modelnum.restorefile(save_mod_id)
                          end !loop
                          access:manufact.restorefile(save_man_id)
                  
                  
                          setcursor()
                          Close(progresswindow)
                          Do Untagall
                          model_number_temp = model"
                          manufacturer_temp = manufacturer"
                  
                  
                  Of 2 ! &No Button
              End!Case MessageEx
      
      End !AddChargesOption
      BRW2.ResetSort(1)
      BRW3.ResetSort(1)
      BRW4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DeleteStandardCharges, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::7:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:3)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseDown
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseDown)
      
          !! ** Bryan Harrison (c)1998 **
      
          GLO:Select1 = ''
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseDown)
    OF EVENT:GainFocus
      IF quickwindow{PROP:Iconize}=TRUE
        quickwindow{PROP:Iconize}=FALSE
        IF quickwindow{PROP:Active}<>TRUE
           quickwindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do UntagAll
      ! Okay Which Charge Structure Is This
      Case glo:select12
          Of 'STANDARD'
              quickwindow{prop:text} = 'Inserting/Amending Standard Charge Structure'
              ?general_tab{prop:text} = 'Charge Structure - Standard'
          Of 'TRADE'
      !        access:tradeacc.clearkey(tra:account_number_key)
      !        tra:account_number = glo:select11
      !        access:tradeacc.fetch(tra:account_number_key)
      !        trade_name_temp = Clip(tra:account_number) & ' (' & Clip(tra:company_name) & ')'
              trade_name_temp = Clip(glo:select11)
              quickwindow{prop:text} = 'Inserting/Amending Trade Account Specific Charge Structure'
              ?general_tab{prop:text} = 'Charge Structure - ' & Clip(trade_name_temp)
              ?addtostandardcharges{prop:text} = 'Add To Trade Account Charges'
              ?deletestandardcharges{prop:text} = 'Delete Trade Account Charges'
              ?view_charges{prop:text} = 'View Trade Account Charges'
          of 'SUBTRADE'
      !        access:subtracc.clearkey(sub:account_number_key)
      !        sub:account_number = glo:select11
      !        access:subtracc.fetch(sub:account_number_key)
      !        trade_name_temp = Clip(sub:account_number) & ' (' & Clip(sub:company_name) & ')'
              trade_name_temp = Clip(glo:select11)
              quickwindow{prop:text} = 'Inserting/Amending Trade Sub Account Specific Charge Structure'
              ?general_tab{prop:text} = 'Charge Structure - ' & Clip(trade_name_temp)
              ?addtostandardcharges{prop:text} = 'Add To Sub Account Charges'
              ?deletestandardcharges{prop:text} = 'Delete Sub Account Charges'
              ?view_charges{prop:text} = 'View Sub Account Charges'
      End
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW2.ResetSort(1)
      BRW3.ResetSort(1)
      BRW4.ResetSort(1)
      FDCB5.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_ChargeType.Charge_Type_Pointer = cha:Charge_Type
     GET(GLO:Q_ChargeType,GLO:Q_ChargeType.Charge_Type_Pointer)
    IF ERRORCODE()
      Charge_Type_Tag = ''
    ELSE
      Charge_Type_Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (charge_type_tag = '*')
    SELF.Q.Charge_Type_Tag_Icon = 3
  ELSE
    SELF.Q.Charge_Type_Tag_Icon = 1
  END
  SELF.Q.tmp:cha_NormalFG = -1
  SELF.Q.tmp:cha_NormalBG = 16777215
  SELF.Q.tmp:cha_SelectedFG = -1
  SELF.Q.tmp:cha_SelectedBG = 16777215
  IF (cha:warranty <> 'YES')
    SELF.Q.tmp:cha_Icon = 2
  ELSE
    SELF.Q.tmp:cha_Icon = 0
  END
  IF (cha:warranty = 'YES')
    SELF.Q.tmp:war_Icon = 2
  ELSE
    SELF.Q.tmp:war_Icon = 0
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW2.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW2::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW2::RecordStatus=ReturnValue
  IF BRW2::RecordStatus NOT=Record:OK THEN RETURN BRW2::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_ChargeType.Charge_Type_Pointer = cha:Charge_Type
     GET(GLO:Q_ChargeType,GLO:Q_ChargeType.Charge_Type_Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW2::RecordStatus
  RETURN ReturnValue


BRW3.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.DeleteControl=?Delete:2
  END


BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
     GET(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
    IF ERRORCODE()
      Unit_Type_Tag = ''
    ELSE
      Unit_Type_Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (unit_type_tag = '*')
    SELF.Q.Unit_Type_Tag_Icon = 2
  ELSE
    SELF.Q.Unit_Type_Tag_Icon = 1
  END


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_UnitType.Unit_Type_Pointer = uni:Unit_Type
     GET(GLO:Q_UnitType,GLO:Q_UnitType.Unit_Type_Pointer)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.DeleteControl=?Delete
  END


BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
     GET(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
    IF ERRORCODE()
      Repair_Type_Tag = ''
    ELSE
      Repair_Type_Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (repair_type_tag = '*')
    SELF.Q.Repair_Type_Tag_Icon = 3
  ELSE
    SELF.Q.Repair_Type_Tag_Icon = 1
  END
  IF (rep:chargeable = 'YES')
    SELF.Q.tmp:repcha_Icon = 2
  ELSE
    SELF.Q.tmp:repcha_Icon = 0
  END
  IF (rep:warranty = 'YES')
    SELF.Q.tmp:repwar_Icon = 2
  ELSE
    SELF.Q.tmp:repwar_Icon = 0
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
     GET(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue

