

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01033.INC'),ONCE        !Local module procedure declarations
                     END


StandardTexts PROCEDURE                               !Generated from procedure template - Window

ThisThreadActive BYTE
LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW5::View:Browse    VIEW(STANTEXT)
                       PROJECT(stt:Description)
                       PROJECT(stt:Text)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
stt:Description        LIKE(stt:Description)          !List box control field - type derived from field
stt:Text               LIKE(stt:Text)                 !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('General Defaults'),AT(,,395,219),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,388,184),USE(?Sheet1),SPREAD
                         TAB('Standard &Document Texts'),USE(?Standard_Document_Text_Tab)
                           ENTRY(@s30),AT(8,20,124,10),USE(stt:Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           LIST,AT(8,36,148,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)~Description~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(164,116,56,16),USE(?Insert),TRN,LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(164,140,56,16),USE(?Change),TRN,LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(164,164,56,16),USE(?Delete),TRN,LEFT,ICON('delete.gif')
                           TEXT,AT(160,36,228,76),USE(stt:Text),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),READONLY
                         END
                       END
                       BUTTON('&OK'),AT(276,196,56,16),USE(?OkButton),TRN,LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(332,196,56,16),USE(?CancelButton),TRN,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,192,388,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  IncrementalLocatorClass          !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Standard_Document_Text_Tab{prop:Color} = 15066597
    If ?stt:Description{prop:ReadOnly} = True
        ?stt:Description{prop:FontColor} = 65793
        ?stt:Description{prop:Color} = 15066597
    Elsif ?stt:Description{prop:Req} = True
        ?stt:Description{prop:FontColor} = 65793
        ?stt:Description{prop:Color} = 8454143
    Else ! If ?stt:Description{prop:Req} = True
        ?stt:Description{prop:FontColor} = 65793
        ?stt:Description{prop:Color} = 16777215
    End ! If ?stt:Description{prop:Req} = True
    ?stt:Description{prop:Trn} = 0
    ?stt:Description{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    If ?stt:Text{prop:ReadOnly} = True
        ?stt:Text{prop:FontColor} = 65793
        ?stt:Text{prop:Color} = 15066597
    Elsif ?stt:Text{prop:Req} = True
        ?stt:Text{prop:FontColor} = 65793
        ?stt:Text{prop:Color} = 8454143
    Else ! If ?stt:Text{prop:Req} = True
        ?stt:Text{prop:FontColor} = 65793
        ?stt:Text{prop:Color} = 16777215
    End ! If ?stt:Text{prop:Req} = True
    ?stt:Text{prop:Trn} = 0
    ?stt:Text{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('StandardTexts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?stt:Description
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='StandardTexts'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:STANTEXT.Open
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:STANTEXT,SELF)
  OPEN(window)
  SELF.Opened=True
  Set(Defaults)
  If access:defaults.next()
      get(defaults,0)
      if access:defaults.primerecord() = level:benign
          if access:defaults.insert()
              access:defaults.cancelautoinc()
          end
      end!if access:defaults.primerecord() = level:benign
  End!If access:defaults.next()
  
  
  
  
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,stt:Description_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(?stt:Description,stt:Description,1,BRW5)
  BRW5.AddField(stt:Description,BRW5.Q.stt:Description)
  BRW5.AddField(stt:Text,BRW5.Q.stt:Text)
  BRW5.AskProcedure = 1
  BRW5.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:STANTEXT.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='StandardTexts'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSTANTEXT
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !If def:auto_logout <> 'NO'
      !    If def:auto_logout_time < 60 Or def:auto_logout_time > 3600
      !        beep(beep:systemhand)  ;  yield()
      !        message('The logout time must be between 60 seconds (1 minute) and '&|
      !               '|3,600 seconds (1 hour).', |
      !               'ServiceBase 2000', icon:hand)
      !        def:auto_logout_time = ''
      !        Select(?def:auto_logout_time)
      !        Cycle
      !    End
      !End
      access:defaults.update()
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?stt:Description
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stt:Description, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stt:Description, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

