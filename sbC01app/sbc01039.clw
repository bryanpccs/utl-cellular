

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01039.INC'),ONCE        !Local module procedure declarations
                     END


NagScreen PROCEDURE (func:Type)                       !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('ServiceBase 2000'),AT(,,320,232),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:Black),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,312,224),USE(?Sheet1),COLOR(COLOR:White),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Update Required'),AT(164,88),USE(?Prompt1),FONT(,16,COLOR:Red,FONT:bold)
                           PROMPT('ServiceBase 2000 is overdue it''s latest update.'),AT(132,112,180,36),USE(?ErrorText),CENTER,FONT(,12,COLOR:Green,FONT:bold)
                           PROMPT('Please contact your System Supervisor who should be able to download the latest ' &|
   'version.'),AT(132,156,180,16),USE(?Prompt3),FONT(,8,,)
                           PROMPT('If you have any problems/questions, please contact PC Control Systems.'),AT(132,176,180,16),USE(?Prompt3:2),FONT(,8,,)
                           PROMPT('Click ''OK'' to clear this message.'),AT(168,196),USE(?Prompt5)
                           BUTTON('&OK'),AT(188,204,56,16),USE(?OK),LEFT,ICON('ok.gif')
                         END
                       END
                       IMAGE('cellti2k.jpg'),AT(8,8),USE(?Image2)
                       IMAGE('monhand.gif'),AT(8,92,116,128),USE(?Image1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('NagScreen')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      If func:Type = 1
          ?ErrorText{prop:Text} = 'An Error has occured with you program. Please install your latest update again.'
      End !func:Type = 1
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

