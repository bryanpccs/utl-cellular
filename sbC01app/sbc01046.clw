

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01046.INC'),ONCE        !Local module procedure declarations
                     END


SIDInterfaceDefaults PROCEDURE                        !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:SMSFTPAddress    STRING(30)
tmp:SMSFTPUsername   STRING(30)
window               WINDOW('SID Interface Defaults'),AT(,,308,340),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,2,300,306),USE(?Sheet1),SPREAD
                         TAB('SID Interface Defaults'),USE(?SIDInterfaceDefaultsTab)
                           GROUP('FTP Defaults'),AT(8,18,292,154),USE(?FTPGroup),BOXED
                           END
                           PROMPT('FTP Server'),AT(16,28),USE(?SID:FTPServer:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(92,28,124,10),USE(SID:FTPServer),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Server'),TIP('FTP Server')
                           PROMPT('FTP Username'),AT(16,44),USE(?SID:FTPUsername:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(92,44,124,10),USE(SID:FTPUsername),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Username'),TIP('FTP Username')
                           PROMPT('FTP Password'),AT(16,60),USE(?SID:FTPPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(92,60,124,10),USE(SID:FTPPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Password'),TIP('FTP Password'),PASSWORD
                           PROMPT('FTP Path'),AT(16,76),USE(?SID:FTPPath:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(92,76,124,10),USE(SID:FTPPath),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Path'),TIP('FTP Path')
                           PROMPT('Contact Name'),AT(16,92),USE(?SID:ContactName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(92,92,200,10),USE(SID:ContactName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Contact Name'),TIP('Contact Name'),UPR
                           PROMPT('Contact Number'),AT(16,108),USE(?SID:ContactNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(92,108,200,10),USE(SID:ContactNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Contact Number'),TIP('Contact Number'),UPR
                           PROMPT('FTP Alert Subject'),AT(16,124),USE(?SID:EmailSubject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(92,124,200,10),USE(SID:EmailSubject),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Subject'),TIP('Email Subject')
                           PROMPT('FTP Alert Recipients'),AT(16,140),USE(?SID:EmailToAddress:Prompt),TRN
                           ENTRY(@s255),AT(92,140,200,10),USE(SID:EmailToAddress),LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),MSG('To Email Address'),TIP('To Email Address')
                           PROMPT('Send FTP Alert After'),AT(16,156),USE(?SID:ProcessFailureCount:Prompt),TRN,FONT('Tahoma',,,,CHARSET:ANSI)
                           ENTRY(@s8),AT(92,156,64,10),USE(SID:ProcessFailureCount),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Send Alert Email After (X) Successive Process Failures'),TIP('Send Alert Email After (X) Successive Process Failures'),UPR
                           STRING('Successive Process Failures'),AT(164,156),USE(?StrProcessFailures),TRN
                           GROUP('Email Defaults'),AT(8,176,292,126),USE(?EmailGroup),BOXED
                           END
                           PROMPT('SMTP Server'),AT(16,188),USE(?SID:SMTPServer:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s50),AT(92,188,200,10),USE(SID:SMTPServer),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Server'),TIP('SMTP Server'),UPR
                           PROMPT('SMTP Port Number'),AT(16,204,68,),USE(?SID:SMTPPort:Prompt),TRN
                           ENTRY(@s3),AT(92,204,64,10),USE(SID:SMTPPort),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Service Port Number'),TIP('Email Service Port Number'),UPR
                           PROMPT('SMTP User Name'),AT(16,220),USE(?SID:SMTPUserName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(92,220,200,10),USE(SID:SMTPUserName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP User Name'),TIP('SMTP User Name')
                           PROMPT('SMTP Password'),AT(16,236),USE(?SID:SMTPPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(92,236,200,10),USE(SID:SMTPPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Password'),TIP('SMTP Password'),PASSWORD
                           PROMPT('Email From Address'),AT(16,252),USE(?SID:EmailFromAddress:Prompt),TRN
                           ENTRY(@s255),AT(92,252,200,10),USE(SID:EmailFromAddress),LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),MSG('From Email Address'),TIP('From Email Address')
                           PROMPT('Alert Email Recipients'),AT(16,268),USE(?SID:ContactEmail:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(92,268,200,10),USE(SID:ContactEmail),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Contact Email'),TIP('Contact Email')
                           PROMPT('Tote Alert Recipients'),AT(16,285),USE(?SID:ToteAlertEmail:Prompt),TRN
                           ENTRY(@s255),AT(92,284,200,10),USE(SID:ToteAlertEmail),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Tote Alert Recipients'),TIP('Tote Alert Recipients')
                         END
                       END
                       BUTTON('&OK'),AT(188,316,56,16),USE(?OkButton),TRN,LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(244,316,56,16),USE(?CancelButton),TRN,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,312,300,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?SIDInterfaceDefaultsTab{prop:Color} = 15066597
    ?FTPGroup{prop:Font,3} = -1
    ?FTPGroup{prop:Color} = 15066597
    ?FTPGroup{prop:Trn} = 0
    ?SID:FTPServer:Prompt{prop:FontColor} = -1
    ?SID:FTPServer:Prompt{prop:Color} = 15066597
    If ?SID:FTPServer{prop:ReadOnly} = True
        ?SID:FTPServer{prop:FontColor} = 65793
        ?SID:FTPServer{prop:Color} = 15066597
    Elsif ?SID:FTPServer{prop:Req} = True
        ?SID:FTPServer{prop:FontColor} = 65793
        ?SID:FTPServer{prop:Color} = 8454143
    Else ! If ?SID:FTPServer{prop:Req} = True
        ?SID:FTPServer{prop:FontColor} = 65793
        ?SID:FTPServer{prop:Color} = 16777215
    End ! If ?SID:FTPServer{prop:Req} = True
    ?SID:FTPServer{prop:Trn} = 0
    ?SID:FTPServer{prop:FontStyle} = font:Bold
    ?SID:FTPUsername:Prompt{prop:FontColor} = -1
    ?SID:FTPUsername:Prompt{prop:Color} = 15066597
    If ?SID:FTPUsername{prop:ReadOnly} = True
        ?SID:FTPUsername{prop:FontColor} = 65793
        ?SID:FTPUsername{prop:Color} = 15066597
    Elsif ?SID:FTPUsername{prop:Req} = True
        ?SID:FTPUsername{prop:FontColor} = 65793
        ?SID:FTPUsername{prop:Color} = 8454143
    Else ! If ?SID:FTPUsername{prop:Req} = True
        ?SID:FTPUsername{prop:FontColor} = 65793
        ?SID:FTPUsername{prop:Color} = 16777215
    End ! If ?SID:FTPUsername{prop:Req} = True
    ?SID:FTPUsername{prop:Trn} = 0
    ?SID:FTPUsername{prop:FontStyle} = font:Bold
    ?SID:FTPPassword:Prompt{prop:FontColor} = -1
    ?SID:FTPPassword:Prompt{prop:Color} = 15066597
    If ?SID:FTPPassword{prop:ReadOnly} = True
        ?SID:FTPPassword{prop:FontColor} = 65793
        ?SID:FTPPassword{prop:Color} = 15066597
    Elsif ?SID:FTPPassword{prop:Req} = True
        ?SID:FTPPassword{prop:FontColor} = 65793
        ?SID:FTPPassword{prop:Color} = 8454143
    Else ! If ?SID:FTPPassword{prop:Req} = True
        ?SID:FTPPassword{prop:FontColor} = 65793
        ?SID:FTPPassword{prop:Color} = 16777215
    End ! If ?SID:FTPPassword{prop:Req} = True
    ?SID:FTPPassword{prop:Trn} = 0
    ?SID:FTPPassword{prop:FontStyle} = font:Bold
    ?SID:FTPPath:Prompt{prop:FontColor} = -1
    ?SID:FTPPath:Prompt{prop:Color} = 15066597
    If ?SID:FTPPath{prop:ReadOnly} = True
        ?SID:FTPPath{prop:FontColor} = 65793
        ?SID:FTPPath{prop:Color} = 15066597
    Elsif ?SID:FTPPath{prop:Req} = True
        ?SID:FTPPath{prop:FontColor} = 65793
        ?SID:FTPPath{prop:Color} = 8454143
    Else ! If ?SID:FTPPath{prop:Req} = True
        ?SID:FTPPath{prop:FontColor} = 65793
        ?SID:FTPPath{prop:Color} = 16777215
    End ! If ?SID:FTPPath{prop:Req} = True
    ?SID:FTPPath{prop:Trn} = 0
    ?SID:FTPPath{prop:FontStyle} = font:Bold
    ?SID:ContactName:Prompt{prop:FontColor} = -1
    ?SID:ContactName:Prompt{prop:Color} = 15066597
    If ?SID:ContactName{prop:ReadOnly} = True
        ?SID:ContactName{prop:FontColor} = 65793
        ?SID:ContactName{prop:Color} = 15066597
    Elsif ?SID:ContactName{prop:Req} = True
        ?SID:ContactName{prop:FontColor} = 65793
        ?SID:ContactName{prop:Color} = 8454143
    Else ! If ?SID:ContactName{prop:Req} = True
        ?SID:ContactName{prop:FontColor} = 65793
        ?SID:ContactName{prop:Color} = 16777215
    End ! If ?SID:ContactName{prop:Req} = True
    ?SID:ContactName{prop:Trn} = 0
    ?SID:ContactName{prop:FontStyle} = font:Bold
    ?SID:ContactNumber:Prompt{prop:FontColor} = -1
    ?SID:ContactNumber:Prompt{prop:Color} = 15066597
    If ?SID:ContactNumber{prop:ReadOnly} = True
        ?SID:ContactNumber{prop:FontColor} = 65793
        ?SID:ContactNumber{prop:Color} = 15066597
    Elsif ?SID:ContactNumber{prop:Req} = True
        ?SID:ContactNumber{prop:FontColor} = 65793
        ?SID:ContactNumber{prop:Color} = 8454143
    Else ! If ?SID:ContactNumber{prop:Req} = True
        ?SID:ContactNumber{prop:FontColor} = 65793
        ?SID:ContactNumber{prop:Color} = 16777215
    End ! If ?SID:ContactNumber{prop:Req} = True
    ?SID:ContactNumber{prop:Trn} = 0
    ?SID:ContactNumber{prop:FontStyle} = font:Bold
    ?SID:EmailSubject:Prompt{prop:FontColor} = -1
    ?SID:EmailSubject:Prompt{prop:Color} = 15066597
    If ?SID:EmailSubject{prop:ReadOnly} = True
        ?SID:EmailSubject{prop:FontColor} = 65793
        ?SID:EmailSubject{prop:Color} = 15066597
    Elsif ?SID:EmailSubject{prop:Req} = True
        ?SID:EmailSubject{prop:FontColor} = 65793
        ?SID:EmailSubject{prop:Color} = 8454143
    Else ! If ?SID:EmailSubject{prop:Req} = True
        ?SID:EmailSubject{prop:FontColor} = 65793
        ?SID:EmailSubject{prop:Color} = 16777215
    End ! If ?SID:EmailSubject{prop:Req} = True
    ?SID:EmailSubject{prop:Trn} = 0
    ?SID:EmailSubject{prop:FontStyle} = font:Bold
    ?SID:EmailToAddress:Prompt{prop:FontColor} = -1
    ?SID:EmailToAddress:Prompt{prop:Color} = 15066597
    If ?SID:EmailToAddress{prop:ReadOnly} = True
        ?SID:EmailToAddress{prop:FontColor} = 65793
        ?SID:EmailToAddress{prop:Color} = 15066597
    Elsif ?SID:EmailToAddress{prop:Req} = True
        ?SID:EmailToAddress{prop:FontColor} = 65793
        ?SID:EmailToAddress{prop:Color} = 8454143
    Else ! If ?SID:EmailToAddress{prop:Req} = True
        ?SID:EmailToAddress{prop:FontColor} = 65793
        ?SID:EmailToAddress{prop:Color} = 16777215
    End ! If ?SID:EmailToAddress{prop:Req} = True
    ?SID:EmailToAddress{prop:Trn} = 0
    ?SID:EmailToAddress{prop:FontStyle} = font:Bold
    ?SID:ProcessFailureCount:Prompt{prop:FontColor} = -1
    ?SID:ProcessFailureCount:Prompt{prop:Color} = 15066597
    If ?SID:ProcessFailureCount{prop:ReadOnly} = True
        ?SID:ProcessFailureCount{prop:FontColor} = 65793
        ?SID:ProcessFailureCount{prop:Color} = 15066597
    Elsif ?SID:ProcessFailureCount{prop:Req} = True
        ?SID:ProcessFailureCount{prop:FontColor} = 65793
        ?SID:ProcessFailureCount{prop:Color} = 8454143
    Else ! If ?SID:ProcessFailureCount{prop:Req} = True
        ?SID:ProcessFailureCount{prop:FontColor} = 65793
        ?SID:ProcessFailureCount{prop:Color} = 16777215
    End ! If ?SID:ProcessFailureCount{prop:Req} = True
    ?SID:ProcessFailureCount{prop:Trn} = 0
    ?SID:ProcessFailureCount{prop:FontStyle} = font:Bold
    ?StrProcessFailures{prop:FontColor} = -1
    ?StrProcessFailures{prop:Color} = 15066597
    ?EmailGroup{prop:Font,3} = -1
    ?EmailGroup{prop:Color} = 15066597
    ?EmailGroup{prop:Trn} = 0
    ?SID:SMTPServer:Prompt{prop:FontColor} = -1
    ?SID:SMTPServer:Prompt{prop:Color} = 15066597
    If ?SID:SMTPServer{prop:ReadOnly} = True
        ?SID:SMTPServer{prop:FontColor} = 65793
        ?SID:SMTPServer{prop:Color} = 15066597
    Elsif ?SID:SMTPServer{prop:Req} = True
        ?SID:SMTPServer{prop:FontColor} = 65793
        ?SID:SMTPServer{prop:Color} = 8454143
    Else ! If ?SID:SMTPServer{prop:Req} = True
        ?SID:SMTPServer{prop:FontColor} = 65793
        ?SID:SMTPServer{prop:Color} = 16777215
    End ! If ?SID:SMTPServer{prop:Req} = True
    ?SID:SMTPServer{prop:Trn} = 0
    ?SID:SMTPServer{prop:FontStyle} = font:Bold
    ?SID:SMTPPort:Prompt{prop:FontColor} = -1
    ?SID:SMTPPort:Prompt{prop:Color} = 15066597
    If ?SID:SMTPPort{prop:ReadOnly} = True
        ?SID:SMTPPort{prop:FontColor} = 65793
        ?SID:SMTPPort{prop:Color} = 15066597
    Elsif ?SID:SMTPPort{prop:Req} = True
        ?SID:SMTPPort{prop:FontColor} = 65793
        ?SID:SMTPPort{prop:Color} = 8454143
    Else ! If ?SID:SMTPPort{prop:Req} = True
        ?SID:SMTPPort{prop:FontColor} = 65793
        ?SID:SMTPPort{prop:Color} = 16777215
    End ! If ?SID:SMTPPort{prop:Req} = True
    ?SID:SMTPPort{prop:Trn} = 0
    ?SID:SMTPPort{prop:FontStyle} = font:Bold
    ?SID:SMTPUserName:Prompt{prop:FontColor} = -1
    ?SID:SMTPUserName:Prompt{prop:Color} = 15066597
    If ?SID:SMTPUserName{prop:ReadOnly} = True
        ?SID:SMTPUserName{prop:FontColor} = 65793
        ?SID:SMTPUserName{prop:Color} = 15066597
    Elsif ?SID:SMTPUserName{prop:Req} = True
        ?SID:SMTPUserName{prop:FontColor} = 65793
        ?SID:SMTPUserName{prop:Color} = 8454143
    Else ! If ?SID:SMTPUserName{prop:Req} = True
        ?SID:SMTPUserName{prop:FontColor} = 65793
        ?SID:SMTPUserName{prop:Color} = 16777215
    End ! If ?SID:SMTPUserName{prop:Req} = True
    ?SID:SMTPUserName{prop:Trn} = 0
    ?SID:SMTPUserName{prop:FontStyle} = font:Bold
    ?SID:SMTPPassword:Prompt{prop:FontColor} = -1
    ?SID:SMTPPassword:Prompt{prop:Color} = 15066597
    If ?SID:SMTPPassword{prop:ReadOnly} = True
        ?SID:SMTPPassword{prop:FontColor} = 65793
        ?SID:SMTPPassword{prop:Color} = 15066597
    Elsif ?SID:SMTPPassword{prop:Req} = True
        ?SID:SMTPPassword{prop:FontColor} = 65793
        ?SID:SMTPPassword{prop:Color} = 8454143
    Else ! If ?SID:SMTPPassword{prop:Req} = True
        ?SID:SMTPPassword{prop:FontColor} = 65793
        ?SID:SMTPPassword{prop:Color} = 16777215
    End ! If ?SID:SMTPPassword{prop:Req} = True
    ?SID:SMTPPassword{prop:Trn} = 0
    ?SID:SMTPPassword{prop:FontStyle} = font:Bold
    ?SID:EmailFromAddress:Prompt{prop:FontColor} = -1
    ?SID:EmailFromAddress:Prompt{prop:Color} = 15066597
    If ?SID:EmailFromAddress{prop:ReadOnly} = True
        ?SID:EmailFromAddress{prop:FontColor} = 65793
        ?SID:EmailFromAddress{prop:Color} = 15066597
    Elsif ?SID:EmailFromAddress{prop:Req} = True
        ?SID:EmailFromAddress{prop:FontColor} = 65793
        ?SID:EmailFromAddress{prop:Color} = 8454143
    Else ! If ?SID:EmailFromAddress{prop:Req} = True
        ?SID:EmailFromAddress{prop:FontColor} = 65793
        ?SID:EmailFromAddress{prop:Color} = 16777215
    End ! If ?SID:EmailFromAddress{prop:Req} = True
    ?SID:EmailFromAddress{prop:Trn} = 0
    ?SID:EmailFromAddress{prop:FontStyle} = font:Bold
    ?SID:ContactEmail:Prompt{prop:FontColor} = -1
    ?SID:ContactEmail:Prompt{prop:Color} = 15066597
    If ?SID:ContactEmail{prop:ReadOnly} = True
        ?SID:ContactEmail{prop:FontColor} = 65793
        ?SID:ContactEmail{prop:Color} = 15066597
    Elsif ?SID:ContactEmail{prop:Req} = True
        ?SID:ContactEmail{prop:FontColor} = 65793
        ?SID:ContactEmail{prop:Color} = 8454143
    Else ! If ?SID:ContactEmail{prop:Req} = True
        ?SID:ContactEmail{prop:FontColor} = 65793
        ?SID:ContactEmail{prop:Color} = 16777215
    End ! If ?SID:ContactEmail{prop:Req} = True
    ?SID:ContactEmail{prop:Trn} = 0
    ?SID:ContactEmail{prop:FontStyle} = font:Bold
    ?SID:ToteAlertEmail:Prompt{prop:FontColor} = -1
    ?SID:ToteAlertEmail:Prompt{prop:Color} = 15066597
    If ?SID:ToteAlertEmail{prop:ReadOnly} = True
        ?SID:ToteAlertEmail{prop:FontColor} = 65793
        ?SID:ToteAlertEmail{prop:Color} = 15066597
    Elsif ?SID:ToteAlertEmail{prop:Req} = True
        ?SID:ToteAlertEmail{prop:FontColor} = 65793
        ?SID:ToteAlertEmail{prop:Color} = 8454143
    Else ! If ?SID:ToteAlertEmail{prop:Req} = True
        ?SID:ToteAlertEmail{prop:FontColor} = 65793
        ?SID:ToteAlertEmail{prop:Color} = 16777215
    End ! If ?SID:ToteAlertEmail{prop:Req} = True
    ?SID:ToteAlertEmail{prop:Trn} = 0
    ?SID:ToteAlertEmail{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SIDInterfaceDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SID:FTPServer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SIDDEF.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Set(SIDDEF)
  If Access:SIDDEF.Next()
      If Access:SIDDEF.PrimeRecord() = Level:Benign
          If Access:SIDDEF.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:SIDDEF.TryInsert() = Level:Benign
              ! Insert Failed
              Access:SIDDEF.CancelAutoInc()
          End ! If Access:SIDDEF.TryInsert() = Level:Benign
      End !If Access:SIDDEF.PrimeRecord() = Level:Benign
  End ! If Access:SIDDEF.Next()
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SIDDEF.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      Access:SIDDEF.Update()
      
      
      
      
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

