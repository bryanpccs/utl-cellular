

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01044.INC'),ONCE        !Local module procedure declarations
                     END


SIDAlertDefaults PROCEDURE                            !Generated from procedure template - Window

LocalRequest         LONG
path_temp            STRING(255)
printer_name_temp    STRING(255)
FilesOpened          BYTE
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
label_queue_temp     QUEUE,PRE()
label_printer_temp   STRING(30)
                     END
tmp:Background       STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:SMSFTPAddress    STRING(30)
tmp:SMSFTPUsername   STRING(30)
window               WINDOW('General Defaults'),AT(,,680,432),FONT('Tahoma',8,,),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,2,672,398),USE(?Sheet1),SPREAD
                         TAB('SID Alert Defaults'),USE(?SIDAlertDefaultsTab)
                           PROMPT('SMTP Server'),AT(8,18),USE(?dem:SMTPServer:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s50),AT(84,18,232,10),USE(dem:SMTPServer),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Server'),TIP('SMTP Server'),UPR
                           PROMPT('SMTP Port Number'),AT(8,34,68,),USE(?dem:SMTPPort:Prompt),TRN
                           ENTRY(@s3),AT(84,34,64,10),USE(dem:SMTPPort),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Service Port Number'),TIP('Email Service Port Number'),UPR
                           PROMPT('SMTP User Name'),AT(8,50),USE(?dem:SMTPUserName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,50,232,10),USE(dem:SMTPUserName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP User Name'),TIP('SMTP User Name')
                           PROMPT('SMTP Password'),AT(8,66),USE(?dem:SMTPPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,66,232,10),USE(dem:SMTPPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMTP Password'),TIP('SMTP Password'),PASSWORD
                           PROMPT('Email From Address'),AT(8,82),USE(?dem:EmailFromAddress:Prompt),TRN
                           ENTRY(@s255),AT(84,82,232,10),USE(dem:EmailFromAddress),LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),MSG('From Email Address'),TIP('From Email Address')
                           PROMPT('Email To Address'),AT(8,98),USE(?dem:EmailToAddress:Prompt),TRN
                           ENTRY(@s255),AT(84,98,232,10),USE(dem:EmailToAddress),LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),MSG('To Email Address'),TIP('To Email Address')
                           PROMPT('SMS To Address'),AT(8,114),USE(?dem:SMSToAddress:Prompt),TRN
                           ENTRY(@s255),AT(84,114,232,10),USE(dem:SMSToAddress),LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),MSG('To Email Address (SMS)'),TIP('To Email Address (SMS)')
                           PROMPT('Admin To Address'),AT(8,130),USE(?dem:AdminToAddress:Prompt),TRN
                           ENTRY(@s255),AT(84,130,232,10),USE(dem:AdminToAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('To Email Address (Administrator)'),TIP('To Email Address (Administrator)')
                           PROMPT('FTP Address'),AT(8,144),USE(?dem:SMSFTPAddress:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,144,124,10),USE(dem:SMSFTPAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SMS FTP Address'),TIP('SMS FTP Address')
                           PROMPT('FTP Username'),AT(8,160),USE(?dem:SMSFTPUsername:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,160,124,10),USE(dem:SMSFTPUsername),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Username'),TIP('FTP Username')
                           PROMPT('FTP Password'),AT(8,176),USE(?dem:SMSFTPPassword:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,176,124,10),USE(dem:SMSFTPPassword),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Password'),TIP('FTP Password'),PASSWORD
                           PROMPT('FTP Location'),AT(8,192),USE(?dem:SMSFTPLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,192,124,10),USE(dem:SMSFTPLocation),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('FTP Location'),TIP('FTP Location')
                         END
                         TAB('Retail Message Templates'),USE(?Tab4)
                           STRING('R1) Device is initially booked onto SID.'),AT(8,18,276,10),USE(?StrR1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('SMS Message Text'),AT(8,39),USE(?dem:BookedRetailText:Prompt)
                           TEXT,AT(108,39,216,42),USE(dem:BookedRetailText),VSCROLL,LEFT,MSG('SMS Text (Device Booked At Retail Store)')
                           CHECK('Alert Active'),AT(8,52),USE(dem:R1SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,84),USE(?der1:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,84,216,10),USE(der1:Subject),LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(8,98),USE(?der1:R1Email:Prompt)
                           TEXT,AT(108,98,216,42),USE(der1:R1Email),VSCROLL,LEFT,MSG('R1 Email'),TIP('R1 Email')
                           CHECK('Alert Active'),AT(8,108),USE(dem:R1Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           STRING('R2) Job despatched from store.'),AT(8,144,276,10),USE(?StrR2),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('SMS Message Text'),AT(8,164),USE(?dem:DespFromStoreText:Prompt)
                           TEXT,AT(108,164,216,42),USE(dem:DespFromStoreText),VSCROLL,MSG('SMS Text (Device Despatched From Store)')
                           CHECK('Alert Active'),AT(8,178),USE(dem:R2SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,210),USE(?der2:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,210,216,10),USE(der2:Subject),LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(8,224),USE(?der2:R2Email:Prompt)
                           TEXT,AT(108,224,216,42),USE(der2:R2Email),VSCROLL,LEFT,MSG('E1 Email'),TIP('E1 Email')
                           CHECK('Alert Active'),AT(8,236),USE(dem:R2Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           STRING('R3) Despatched from Service Centre.'),AT(8,270,276,10),USE(?StrR3),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('SMS Message Text'),AT(8,292),USE(?dem:DespFromSCText:Prompt)
                           TEXT,AT(108,292,216,42),USE(dem:DespFromSCText),VSCROLL,FONT(,,,,CHARSET:ANSI),MSG('SMS Text (Device Despatched From ServiceBase)')
                           CHECK('Alert Active'),AT(8,304),USE(dem:R3SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,338),USE(?der3:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,338,216,10),USE(der3:Subject),LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(8,352),USE(?der3:R3Email:Prompt)
                           TEXT,AT(108,352,216,42),USE(der3:R3Email),VSCROLL,LEFT,MSG('R2 Email'),TIP('R2 Email')
                           CHECK('Alert Active'),AT(8,364),USE(dem:R3Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           STRING('R4) Device arrived at Retail Store.'),AT(352,18,276,10),USE(?StrR4),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('SMS Message Text'),AT(352,40),USE(?dem:ArrivedAtRetailText:Prompt)
                           TEXT,AT(452,36,216,42),USE(dem:ArrivedAtRetailText),VSCROLL,FONT(,,,,CHARSET:ANSI),MSG('SMS Text (Device Arrived At Retail Store)')
                           CHECK('Alert Active'),AT(352,52),USE(dem:R4SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(352,82),USE(?der4:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(452,82,216,10),USE(der4:Subject),LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(352,96),USE(?der4:R4Email:Prompt)
                           TEXT,AT(452,96,216,42),USE(der4:R4Email),VSCROLL,LEFT,FONT(,,,,CHARSET:ANSI),MSG('R3 Email'),TIP('R3 Email')
                           CHECK('Alert Active'),AT(352,108),USE(dem:R4Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                         END
                         TAB('Exchange Message Templates'),USE(?Tab2)
                           PROMPT('E1) Confirmation of booking and request has been sent to be processed.'),AT(8,18,276,10),USE(?dem2:E1SMS:Prompt),FONT(,,,FONT:bold)
                           PROMPT('SMS Message Text'),AT(8,39),USE(?Prompt14)
                           TEXT,AT(108,39,216,42),USE(dem2:E1SMS),VSCROLL,LEFT,MSG('E1 SMS'),TIP('E1 SMS')
                           CHECK('Alert Active'),AT(8,52),USE(dem2:E1SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,84),USE(?dee1:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,84,216,10),USE(dee1:Subject),LEFT,FONT('Tahoma',8,,,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(8,98),USE(?dem3:E1Email:Prompt)
                           TEXT,AT(108,98,216,42),USE(dee1:E1Email),VSCROLL,LEFT,MSG('E1 Email'),TIP('E1 Email')
                           CHECK('Alert Active'),AT(8,111),USE(dem2:E1Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('E2a) Despatch from CRC with details of delivery - No Battery. (Monday To Thursda' &|
   'y)'),AT(8,143,320,10),USE(?dem2:E2SMS:Prompt),FONT(,,,FONT:bold)
                           PROMPT('SMS Message Text'),AT(8,164),USE(?Prompt14:2)
                           TEXT,AT(108,164,216,42),USE(dem2:E2SMS),VSCROLL,LEFT,MSG('E2 SMS'),TIP('E2 SMS')
                           CHECK('Alert Active'),AT(8,178),USE(dem2:E2SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,210),USE(?dee2:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,210,216,10),USE(dee2:Subject),LEFT,FONT('Tahoma',8,,,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(8,224),USE(?dem3:E1Email:Prompt:2)
                           TEXT,AT(108,224,216,42),USE(dee2:E2Email),VSCROLL,LEFT,MSG('E1 Email'),TIP('E1 Email')
                           CHECK('Alert Active'),AT(8,236),USE(dem2:E2Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('E2b) Despatch from CRC with details of delivery - With Battery. (Monday To Thurs' &|
   'day)'),AT(348,143,320,10),USE(?PromptE2B),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('SMS Message Text'),AT(348,164),USE(?dem2:E2BSMS:Prompt)
                           TEXT,AT(448,164,216,42),USE(dem2:E2BSMS),VSCROLL,LEFT,MSG('E2 SMS'),TIP('E2 SMS')
                           CHECK('Alert Active'),AT(348,178),USE(dem2:E2BSMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(348,210),USE(?dee2B:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(448,210,216,10),USE(dee2B:Subject),LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(348,224),USE(?dee2B:E2BEmail:Prompt)
                           TEXT,AT(448,224,216,42),USE(dee2B:E2BEmail),VSCROLL,LEFT,MSG('E1 Email'),TIP('E1 Email')
                           CHECK('Alert Active'),AT(348,236),USE(dem2:E2BActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('E3a) Despatch from CRC with details of delivery - No Battery. (Friday)'),AT(8,268,320,10),USE(?dem2:E3SMS:Prompt),FONT(,,,FONT:bold)
                           PROMPT('SMS Message Text'),AT(8,290),USE(?Prompt14:3)
                           TEXT,AT(108,287,216,42),USE(dem2:E3SMS),VSCROLL,LEFT,MSG('E3 SMS'),TIP('E3 SMS')
                           CHECK('Alert Active'),AT(8,303),USE(dem2:E3SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,332),USE(?dee3:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,332,216,10),USE(dee3:Subject),LEFT,FONT('Tahoma',8,,,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(8,346),USE(?dem3:E1Email:Prompt:3)
                           TEXT,AT(108,346,216,42),USE(dee3:E3Email),VSCROLL,LEFT,MSG('E1 Email'),TIP('E1 Email')
                           CHECK('Alert Active'),AT(8,359),USE(dem2:E3Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('E3b) Despatch from CRC with details of delivery - With Battery. (Friday)'),AT(348,268,320,10),USE(?PromptE3B),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('SMS Message Text'),AT(348,290),USE(?dem2:E3BSMS:Prompt)
                           TEXT,AT(448,287,216,42),USE(dem2:E3BSMS),VSCROLL,LEFT,MSG('E2 SMS'),TIP('E2 SMS')
                           CHECK('Alert Active'),AT(348,303),USE(dem2:E3BSMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(348,332),USE(?dee3B:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(448,332,216,10),USE(dee3B:Subject),LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(348,346),USE(?dee3B:E3BEmail:Prompt)
                           TEXT,AT(448,346,216,42),USE(dee3B:E3BEmail),VSCROLL,LEFT,MSG('E1 Email'),TIP('E1 Email')
                           CHECK('Alert Active'),AT(348,359),USE(dem2:E3BActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                         END
                         TAB('Postal Repair Message Templates'),USE(?Tab3)
                           PROMPT('B1) Confirmation of booking and request has been sent to be processed.'),AT(8,18,316,10),USE(?dem2:E1SMS:Prompt:2),FONT(,,,FONT:bold)
                           PROMPT('B4) Device despatched from repair centre. (Monday To Thursday)'),AT(356,18,316,10),USE(?dem2:E1SMS:Prompt:3),FONT(,,,FONT:bold)
                           CHECK('Alert Active'),AT(8,112),USE(dem2:B1Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           CHECK('Alert Active'),AT(356,112),USE(dem2:B4Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('SMS Message Text'),AT(8,39),USE(?Prompt14:4)
                           TEXT,AT(108,39,216,42),USE(dem2:B1SMS),VSCROLL,LEFT,MSG('B1 SMS'),TIP('B1 SMS')
                           TEXT,AT(108,98,216,42),USE(deb1:B1Email),VSCROLL,LEFT,MSG('B1 Email'),TIP('B1 Email')
                           PROMPT('SMS Message Text'),AT(356,39),USE(?Prompt14:7)
                           TEXT,AT(108,162,216,42),USE(dem2:B2SMS),VSCROLL,LEFT,MSG('B2 SMS'),TIP('B2 SMS')
                           TEXT,AT(108,220,216,42),USE(deb2:B2Email),VSCROLL,LEFT,MSG('B1 Email'),TIP('B1 Email')
                           TEXT,AT(108,287,216,42),USE(dem2:B3SMS),VSCROLL,LEFT,MSG('B3 SMS'),TIP('B3 SMS')
                           CHECK('Alert Active'),AT(8,300),USE(dem2:B3SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,332),USE(?deb3:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,332,216,10),USE(deb3:Subject),LEFT,FONT('Tahoma',8,,,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           TEXT,AT(108,346,216,42),USE(deb3:B3Email),VSCROLL,LEFT,MSG('B3 Email'),TIP('B3 Email')
                           TEXT,AT(456,39,216,42),USE(dem2:B4SMS),VSCROLL,LEFT,MSG('B4 SMS'),TIP('B4 SMS')
                           CHECK('Alert Active'),AT(8,50),USE(dem2:B1SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           CHECK('Alert Active'),AT(356,50),USE(dem2:B4SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,82),USE(?deb1:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,84,216,10),USE(deb1:Subject),LEFT,FONT('Tahoma',8,,,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Subject'),AT(356,84),USE(?deb4:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(456,84,216,10),USE(deb4:Subject),LEFT,FONT('Tahoma',8,,,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(8,98),USE(?dem3:E1Email:Prompt:4)
                           PROMPT('Email Message Text'),AT(356,98),USE(?dem3:E1Email:Prompt:8)
                           TEXT,AT(456,98,216,42),USE(deb4:B4Email),VSCROLL,LEFT,MSG('B5 Email'),TIP('B5 Email')
                           PROMPT('B2) Envelope despatched to customer.'),AT(8,143,320,10),USE(?dem2:E2SMS:Prompt:2),FONT(,,,FONT:bold)
                           PROMPT('B5) Device despatched from repair centre. (Friday)'),AT(356,143,320,10),USE(?dem2:E2SMS:Prompt:3),FONT(,,,FONT:bold)
                           CHECK('Alert Active'),AT(8,231),USE(dem2:B2Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           CHECK('Alert Active'),AT(356,231),USE(dem2:B5Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('SMS Message Text'),AT(8,162),USE(?Prompt14:5)
                           PROMPT('SMS Message Text'),AT(356,162),USE(?Prompt14:8)
                           TEXT,AT(456,162,216,42),USE(dem2:B5SMS),VSCROLL,LEFT,MSG('B6 SMS'),TIP('B6 SMS')
                           CHECK('Alert Active'),AT(356,175),USE(dem2:B5SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           CHECK('Alert Active'),AT(8,175),USE(dem2:B2SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,207),USE(?deb2:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,207,216,10),USE(deb2:Subject),LEFT,FONT('Tahoma',8,,,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Subject'),AT(356,207),USE(?deb5:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(456,207,216,10),USE(deb5:Subject),LEFT,FONT('Tahoma',8,,,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           PROMPT('Email Message Text'),AT(8,220),USE(?dem3:E1Email:Prompt:5)
                           PROMPT('Email Message Text'),AT(356,220),USE(?dem3:E1Email:Prompt:7)
                           TEXT,AT(456,220,216,42),USE(deb5:B5Email),VSCROLL,LEFT,MSG('B6 Email'),TIP('B6 Email')
                           PROMPT('B3) Device received at repair centre.'),AT(8,266,320,10),USE(?dem2:E3SMS:Prompt:2),FONT(,,,FONT:bold)
                           CHECK('Alert Active'),AT(8,356),USE(dem2:B3Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Text'),AT(8,346),USE(?dem3:E1Email:Prompt:6)
                           PROMPT('SMS Message Text'),AT(8,287),USE(?Prompt14:6)
                         END
                         TAB('Back To Base Message Templates (2)'),USE(?Tab5)
                           PROMPT('B6) First notification repair envelope has not been received.'),AT(8,18,316,10),USE(?PromptB6),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('SMS Message Text'),AT(8,39),USE(?dem2:B6SMS:Prompt)
                           TEXT,AT(108,39,216,42),USE(dem2:B6SMS),VSCROLL,LEFT,MSG('B1 SMS'),TIP('B1 SMS')
                           CHECK('Alert Active'),AT(8,50),USE(dem2:B6SMSActive),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,84),USE(?deb6:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,84,216,10),USE(deb6:Subject),LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           CHECK('Alert Active'),AT(8,110),USE(dem2:B6Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Text'),AT(8,98),USE(?deb6:B6Email:Prompt)
                           TEXT,AT(108,98,216,42),USE(deb6:B6Email),VSCROLL,LEFT,MSG('E1 Email'),TIP('E1 Email')
                           PROMPT('Notification Days'),AT(8,144),USE(?dem2:B6NotifyDays:Prompt),FONT('Tahoma',,,,CHARSET:ANSI)
                           ENTRY(@s8),AT(108,144,64,10),USE(dem2:B6NotifyDays),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),MSG('B6 Notify Days'),TIP('B6 Notify Days')
                           PROMPT('B7) Second notification repair envelope has not been received.'),AT(8,164,316,10),USE(?PromptB7),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('SMS Message Text'),AT(8,182),USE(?dem2:B7SMS:Prompt)
                           TEXT,AT(108,182,216,42),USE(dem2:B7SMS),VSCROLL,LEFT,FONT(,,,,CHARSET:ANSI),MSG('B1 SMS'),TIP('B1 SMS')
                           CHECK('Alert Active'),AT(8,194),USE(dem2:B7SMSActive),TRN,FONT(,,,,CHARSET:ANSI),MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Subject'),AT(8,228),USE(?deb7:Subject:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(108,228,216,10),USE(deb7:Subject),LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),MSG('Subject'),TIP('Subject')
                           CHECK('Alert Active'),AT(8,254),USE(dem2:B7Active),TRN,MSG('Alert Active'),TIP('Alert Active'),VALUE('1','0')
                           PROMPT('Email Message Text'),AT(8,242),USE(?deb7:B7Email:Prompt)
                           TEXT,AT(108,242,216,42),USE(deb7:B7Email),VSCROLL,LEFT,MSG('E1 Email'),TIP('E1 Email')
                           PROMPT('Notification Days'),AT(8,288),USE(?dem2:B7NotifyDays:Prompt),TRN,FONT('Tahoma',,,,CHARSET:ANSI)
                           ENTRY(@s8),AT(108,288,64,10),USE(dem2:B7NotifyDays),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),MSG('B6 Notify Days'),TIP('B6 Notify Days')
                         END
                       END
                       BUTTON('&OK'),AT(560,410,56,16),USE(?OkButton),TRN,LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(616,410,56,16),USE(?CancelButton),TRN,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,406,672,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?SIDAlertDefaultsTab{prop:Color} = 15066597
    ?dem:SMTPServer:Prompt{prop:FontColor} = -1
    ?dem:SMTPServer:Prompt{prop:Color} = 15066597
    If ?dem:SMTPServer{prop:ReadOnly} = True
        ?dem:SMTPServer{prop:FontColor} = 65793
        ?dem:SMTPServer{prop:Color} = 15066597
    Elsif ?dem:SMTPServer{prop:Req} = True
        ?dem:SMTPServer{prop:FontColor} = 65793
        ?dem:SMTPServer{prop:Color} = 8454143
    Else ! If ?dem:SMTPServer{prop:Req} = True
        ?dem:SMTPServer{prop:FontColor} = 65793
        ?dem:SMTPServer{prop:Color} = 16777215
    End ! If ?dem:SMTPServer{prop:Req} = True
    ?dem:SMTPServer{prop:Trn} = 0
    ?dem:SMTPServer{prop:FontStyle} = font:Bold
    ?dem:SMTPPort:Prompt{prop:FontColor} = -1
    ?dem:SMTPPort:Prompt{prop:Color} = 15066597
    If ?dem:SMTPPort{prop:ReadOnly} = True
        ?dem:SMTPPort{prop:FontColor} = 65793
        ?dem:SMTPPort{prop:Color} = 15066597
    Elsif ?dem:SMTPPort{prop:Req} = True
        ?dem:SMTPPort{prop:FontColor} = 65793
        ?dem:SMTPPort{prop:Color} = 8454143
    Else ! If ?dem:SMTPPort{prop:Req} = True
        ?dem:SMTPPort{prop:FontColor} = 65793
        ?dem:SMTPPort{prop:Color} = 16777215
    End ! If ?dem:SMTPPort{prop:Req} = True
    ?dem:SMTPPort{prop:Trn} = 0
    ?dem:SMTPPort{prop:FontStyle} = font:Bold
    ?dem:SMTPUserName:Prompt{prop:FontColor} = -1
    ?dem:SMTPUserName:Prompt{prop:Color} = 15066597
    If ?dem:SMTPUserName{prop:ReadOnly} = True
        ?dem:SMTPUserName{prop:FontColor} = 65793
        ?dem:SMTPUserName{prop:Color} = 15066597
    Elsif ?dem:SMTPUserName{prop:Req} = True
        ?dem:SMTPUserName{prop:FontColor} = 65793
        ?dem:SMTPUserName{prop:Color} = 8454143
    Else ! If ?dem:SMTPUserName{prop:Req} = True
        ?dem:SMTPUserName{prop:FontColor} = 65793
        ?dem:SMTPUserName{prop:Color} = 16777215
    End ! If ?dem:SMTPUserName{prop:Req} = True
    ?dem:SMTPUserName{prop:Trn} = 0
    ?dem:SMTPUserName{prop:FontStyle} = font:Bold
    ?dem:SMTPPassword:Prompt{prop:FontColor} = -1
    ?dem:SMTPPassword:Prompt{prop:Color} = 15066597
    If ?dem:SMTPPassword{prop:ReadOnly} = True
        ?dem:SMTPPassword{prop:FontColor} = 65793
        ?dem:SMTPPassword{prop:Color} = 15066597
    Elsif ?dem:SMTPPassword{prop:Req} = True
        ?dem:SMTPPassword{prop:FontColor} = 65793
        ?dem:SMTPPassword{prop:Color} = 8454143
    Else ! If ?dem:SMTPPassword{prop:Req} = True
        ?dem:SMTPPassword{prop:FontColor} = 65793
        ?dem:SMTPPassword{prop:Color} = 16777215
    End ! If ?dem:SMTPPassword{prop:Req} = True
    ?dem:SMTPPassword{prop:Trn} = 0
    ?dem:SMTPPassword{prop:FontStyle} = font:Bold
    ?dem:EmailFromAddress:Prompt{prop:FontColor} = -1
    ?dem:EmailFromAddress:Prompt{prop:Color} = 15066597
    If ?dem:EmailFromAddress{prop:ReadOnly} = True
        ?dem:EmailFromAddress{prop:FontColor} = 65793
        ?dem:EmailFromAddress{prop:Color} = 15066597
    Elsif ?dem:EmailFromAddress{prop:Req} = True
        ?dem:EmailFromAddress{prop:FontColor} = 65793
        ?dem:EmailFromAddress{prop:Color} = 8454143
    Else ! If ?dem:EmailFromAddress{prop:Req} = True
        ?dem:EmailFromAddress{prop:FontColor} = 65793
        ?dem:EmailFromAddress{prop:Color} = 16777215
    End ! If ?dem:EmailFromAddress{prop:Req} = True
    ?dem:EmailFromAddress{prop:Trn} = 0
    ?dem:EmailFromAddress{prop:FontStyle} = font:Bold
    ?dem:EmailToAddress:Prompt{prop:FontColor} = -1
    ?dem:EmailToAddress:Prompt{prop:Color} = 15066597
    If ?dem:EmailToAddress{prop:ReadOnly} = True
        ?dem:EmailToAddress{prop:FontColor} = 65793
        ?dem:EmailToAddress{prop:Color} = 15066597
    Elsif ?dem:EmailToAddress{prop:Req} = True
        ?dem:EmailToAddress{prop:FontColor} = 65793
        ?dem:EmailToAddress{prop:Color} = 8454143
    Else ! If ?dem:EmailToAddress{prop:Req} = True
        ?dem:EmailToAddress{prop:FontColor} = 65793
        ?dem:EmailToAddress{prop:Color} = 16777215
    End ! If ?dem:EmailToAddress{prop:Req} = True
    ?dem:EmailToAddress{prop:Trn} = 0
    ?dem:EmailToAddress{prop:FontStyle} = font:Bold
    ?dem:SMSToAddress:Prompt{prop:FontColor} = -1
    ?dem:SMSToAddress:Prompt{prop:Color} = 15066597
    If ?dem:SMSToAddress{prop:ReadOnly} = True
        ?dem:SMSToAddress{prop:FontColor} = 65793
        ?dem:SMSToAddress{prop:Color} = 15066597
    Elsif ?dem:SMSToAddress{prop:Req} = True
        ?dem:SMSToAddress{prop:FontColor} = 65793
        ?dem:SMSToAddress{prop:Color} = 8454143
    Else ! If ?dem:SMSToAddress{prop:Req} = True
        ?dem:SMSToAddress{prop:FontColor} = 65793
        ?dem:SMSToAddress{prop:Color} = 16777215
    End ! If ?dem:SMSToAddress{prop:Req} = True
    ?dem:SMSToAddress{prop:Trn} = 0
    ?dem:SMSToAddress{prop:FontStyle} = font:Bold
    ?dem:AdminToAddress:Prompt{prop:FontColor} = -1
    ?dem:AdminToAddress:Prompt{prop:Color} = 15066597
    If ?dem:AdminToAddress{prop:ReadOnly} = True
        ?dem:AdminToAddress{prop:FontColor} = 65793
        ?dem:AdminToAddress{prop:Color} = 15066597
    Elsif ?dem:AdminToAddress{prop:Req} = True
        ?dem:AdminToAddress{prop:FontColor} = 65793
        ?dem:AdminToAddress{prop:Color} = 8454143
    Else ! If ?dem:AdminToAddress{prop:Req} = True
        ?dem:AdminToAddress{prop:FontColor} = 65793
        ?dem:AdminToAddress{prop:Color} = 16777215
    End ! If ?dem:AdminToAddress{prop:Req} = True
    ?dem:AdminToAddress{prop:Trn} = 0
    ?dem:AdminToAddress{prop:FontStyle} = font:Bold
    ?dem:SMSFTPAddress:Prompt{prop:FontColor} = -1
    ?dem:SMSFTPAddress:Prompt{prop:Color} = 15066597
    If ?dem:SMSFTPAddress{prop:ReadOnly} = True
        ?dem:SMSFTPAddress{prop:FontColor} = 65793
        ?dem:SMSFTPAddress{prop:Color} = 15066597
    Elsif ?dem:SMSFTPAddress{prop:Req} = True
        ?dem:SMSFTPAddress{prop:FontColor} = 65793
        ?dem:SMSFTPAddress{prop:Color} = 8454143
    Else ! If ?dem:SMSFTPAddress{prop:Req} = True
        ?dem:SMSFTPAddress{prop:FontColor} = 65793
        ?dem:SMSFTPAddress{prop:Color} = 16777215
    End ! If ?dem:SMSFTPAddress{prop:Req} = True
    ?dem:SMSFTPAddress{prop:Trn} = 0
    ?dem:SMSFTPAddress{prop:FontStyle} = font:Bold
    ?dem:SMSFTPUsername:Prompt{prop:FontColor} = -1
    ?dem:SMSFTPUsername:Prompt{prop:Color} = 15066597
    If ?dem:SMSFTPUsername{prop:ReadOnly} = True
        ?dem:SMSFTPUsername{prop:FontColor} = 65793
        ?dem:SMSFTPUsername{prop:Color} = 15066597
    Elsif ?dem:SMSFTPUsername{prop:Req} = True
        ?dem:SMSFTPUsername{prop:FontColor} = 65793
        ?dem:SMSFTPUsername{prop:Color} = 8454143
    Else ! If ?dem:SMSFTPUsername{prop:Req} = True
        ?dem:SMSFTPUsername{prop:FontColor} = 65793
        ?dem:SMSFTPUsername{prop:Color} = 16777215
    End ! If ?dem:SMSFTPUsername{prop:Req} = True
    ?dem:SMSFTPUsername{prop:Trn} = 0
    ?dem:SMSFTPUsername{prop:FontStyle} = font:Bold
    ?dem:SMSFTPPassword:Prompt{prop:FontColor} = -1
    ?dem:SMSFTPPassword:Prompt{prop:Color} = 15066597
    If ?dem:SMSFTPPassword{prop:ReadOnly} = True
        ?dem:SMSFTPPassword{prop:FontColor} = 65793
        ?dem:SMSFTPPassword{prop:Color} = 15066597
    Elsif ?dem:SMSFTPPassword{prop:Req} = True
        ?dem:SMSFTPPassword{prop:FontColor} = 65793
        ?dem:SMSFTPPassword{prop:Color} = 8454143
    Else ! If ?dem:SMSFTPPassword{prop:Req} = True
        ?dem:SMSFTPPassword{prop:FontColor} = 65793
        ?dem:SMSFTPPassword{prop:Color} = 16777215
    End ! If ?dem:SMSFTPPassword{prop:Req} = True
    ?dem:SMSFTPPassword{prop:Trn} = 0
    ?dem:SMSFTPPassword{prop:FontStyle} = font:Bold
    ?dem:SMSFTPLocation:Prompt{prop:FontColor} = -1
    ?dem:SMSFTPLocation:Prompt{prop:Color} = 15066597
    If ?dem:SMSFTPLocation{prop:ReadOnly} = True
        ?dem:SMSFTPLocation{prop:FontColor} = 65793
        ?dem:SMSFTPLocation{prop:Color} = 15066597
    Elsif ?dem:SMSFTPLocation{prop:Req} = True
        ?dem:SMSFTPLocation{prop:FontColor} = 65793
        ?dem:SMSFTPLocation{prop:Color} = 8454143
    Else ! If ?dem:SMSFTPLocation{prop:Req} = True
        ?dem:SMSFTPLocation{prop:FontColor} = 65793
        ?dem:SMSFTPLocation{prop:Color} = 16777215
    End ! If ?dem:SMSFTPLocation{prop:Req} = True
    ?dem:SMSFTPLocation{prop:Trn} = 0
    ?dem:SMSFTPLocation{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    ?StrR1{prop:FontColor} = -1
    ?StrR1{prop:Color} = 15066597
    ?dem:BookedRetailText:Prompt{prop:FontColor} = -1
    ?dem:BookedRetailText:Prompt{prop:Color} = 15066597
    If ?dem:BookedRetailText{prop:ReadOnly} = True
        ?dem:BookedRetailText{prop:FontColor} = 65793
        ?dem:BookedRetailText{prop:Color} = 15066597
    Elsif ?dem:BookedRetailText{prop:Req} = True
        ?dem:BookedRetailText{prop:FontColor} = 65793
        ?dem:BookedRetailText{prop:Color} = 8454143
    Else ! If ?dem:BookedRetailText{prop:Req} = True
        ?dem:BookedRetailText{prop:FontColor} = 65793
        ?dem:BookedRetailText{prop:Color} = 16777215
    End ! If ?dem:BookedRetailText{prop:Req} = True
    ?dem:BookedRetailText{prop:Trn} = 0
    ?dem:BookedRetailText{prop:FontStyle} = font:Bold
    ?dem:R1SMSActive{prop:Font,3} = -1
    ?dem:R1SMSActive{prop:Color} = 15066597
    ?dem:R1SMSActive{prop:Trn} = 0
    ?der1:Subject:Prompt{prop:FontColor} = -1
    ?der1:Subject:Prompt{prop:Color} = 15066597
    If ?der1:Subject{prop:ReadOnly} = True
        ?der1:Subject{prop:FontColor} = 65793
        ?der1:Subject{prop:Color} = 15066597
    Elsif ?der1:Subject{prop:Req} = True
        ?der1:Subject{prop:FontColor} = 65793
        ?der1:Subject{prop:Color} = 8454143
    Else ! If ?der1:Subject{prop:Req} = True
        ?der1:Subject{prop:FontColor} = 65793
        ?der1:Subject{prop:Color} = 16777215
    End ! If ?der1:Subject{prop:Req} = True
    ?der1:Subject{prop:Trn} = 0
    ?der1:Subject{prop:FontStyle} = font:Bold
    ?der1:R1Email:Prompt{prop:FontColor} = -1
    ?der1:R1Email:Prompt{prop:Color} = 15066597
    If ?der1:R1Email{prop:ReadOnly} = True
        ?der1:R1Email{prop:FontColor} = 65793
        ?der1:R1Email{prop:Color} = 15066597
    Elsif ?der1:R1Email{prop:Req} = True
        ?der1:R1Email{prop:FontColor} = 65793
        ?der1:R1Email{prop:Color} = 8454143
    Else ! If ?der1:R1Email{prop:Req} = True
        ?der1:R1Email{prop:FontColor} = 65793
        ?der1:R1Email{prop:Color} = 16777215
    End ! If ?der1:R1Email{prop:Req} = True
    ?der1:R1Email{prop:Trn} = 0
    ?der1:R1Email{prop:FontStyle} = font:Bold
    ?dem:R1Active{prop:Font,3} = -1
    ?dem:R1Active{prop:Color} = 15066597
    ?dem:R1Active{prop:Trn} = 0
    ?StrR2{prop:FontColor} = -1
    ?StrR2{prop:Color} = 15066597
    ?dem:DespFromStoreText:Prompt{prop:FontColor} = -1
    ?dem:DespFromStoreText:Prompt{prop:Color} = 15066597
    If ?dem:DespFromStoreText{prop:ReadOnly} = True
        ?dem:DespFromStoreText{prop:FontColor} = 65793
        ?dem:DespFromStoreText{prop:Color} = 15066597
    Elsif ?dem:DespFromStoreText{prop:Req} = True
        ?dem:DespFromStoreText{prop:FontColor} = 65793
        ?dem:DespFromStoreText{prop:Color} = 8454143
    Else ! If ?dem:DespFromStoreText{prop:Req} = True
        ?dem:DespFromStoreText{prop:FontColor} = 65793
        ?dem:DespFromStoreText{prop:Color} = 16777215
    End ! If ?dem:DespFromStoreText{prop:Req} = True
    ?dem:DespFromStoreText{prop:Trn} = 0
    ?dem:DespFromStoreText{prop:FontStyle} = font:Bold
    ?dem:R2SMSActive{prop:Font,3} = -1
    ?dem:R2SMSActive{prop:Color} = 15066597
    ?dem:R2SMSActive{prop:Trn} = 0
    ?der2:Subject:Prompt{prop:FontColor} = -1
    ?der2:Subject:Prompt{prop:Color} = 15066597
    If ?der2:Subject{prop:ReadOnly} = True
        ?der2:Subject{prop:FontColor} = 65793
        ?der2:Subject{prop:Color} = 15066597
    Elsif ?der2:Subject{prop:Req} = True
        ?der2:Subject{prop:FontColor} = 65793
        ?der2:Subject{prop:Color} = 8454143
    Else ! If ?der2:Subject{prop:Req} = True
        ?der2:Subject{prop:FontColor} = 65793
        ?der2:Subject{prop:Color} = 16777215
    End ! If ?der2:Subject{prop:Req} = True
    ?der2:Subject{prop:Trn} = 0
    ?der2:Subject{prop:FontStyle} = font:Bold
    ?der2:R2Email:Prompt{prop:FontColor} = -1
    ?der2:R2Email:Prompt{prop:Color} = 15066597
    If ?der2:R2Email{prop:ReadOnly} = True
        ?der2:R2Email{prop:FontColor} = 65793
        ?der2:R2Email{prop:Color} = 15066597
    Elsif ?der2:R2Email{prop:Req} = True
        ?der2:R2Email{prop:FontColor} = 65793
        ?der2:R2Email{prop:Color} = 8454143
    Else ! If ?der2:R2Email{prop:Req} = True
        ?der2:R2Email{prop:FontColor} = 65793
        ?der2:R2Email{prop:Color} = 16777215
    End ! If ?der2:R2Email{prop:Req} = True
    ?der2:R2Email{prop:Trn} = 0
    ?der2:R2Email{prop:FontStyle} = font:Bold
    ?dem:R2Active{prop:Font,3} = -1
    ?dem:R2Active{prop:Color} = 15066597
    ?dem:R2Active{prop:Trn} = 0
    ?StrR3{prop:FontColor} = -1
    ?StrR3{prop:Color} = 15066597
    ?dem:DespFromSCText:Prompt{prop:FontColor} = -1
    ?dem:DespFromSCText:Prompt{prop:Color} = 15066597
    If ?dem:DespFromSCText{prop:ReadOnly} = True
        ?dem:DespFromSCText{prop:FontColor} = 65793
        ?dem:DespFromSCText{prop:Color} = 15066597
    Elsif ?dem:DespFromSCText{prop:Req} = True
        ?dem:DespFromSCText{prop:FontColor} = 65793
        ?dem:DespFromSCText{prop:Color} = 8454143
    Else ! If ?dem:DespFromSCText{prop:Req} = True
        ?dem:DespFromSCText{prop:FontColor} = 65793
        ?dem:DespFromSCText{prop:Color} = 16777215
    End ! If ?dem:DespFromSCText{prop:Req} = True
    ?dem:DespFromSCText{prop:Trn} = 0
    ?dem:DespFromSCText{prop:FontStyle} = font:Bold
    ?dem:R3SMSActive{prop:Font,3} = -1
    ?dem:R3SMSActive{prop:Color} = 15066597
    ?dem:R3SMSActive{prop:Trn} = 0
    ?der3:Subject:Prompt{prop:FontColor} = -1
    ?der3:Subject:Prompt{prop:Color} = 15066597
    If ?der3:Subject{prop:ReadOnly} = True
        ?der3:Subject{prop:FontColor} = 65793
        ?der3:Subject{prop:Color} = 15066597
    Elsif ?der3:Subject{prop:Req} = True
        ?der3:Subject{prop:FontColor} = 65793
        ?der3:Subject{prop:Color} = 8454143
    Else ! If ?der3:Subject{prop:Req} = True
        ?der3:Subject{prop:FontColor} = 65793
        ?der3:Subject{prop:Color} = 16777215
    End ! If ?der3:Subject{prop:Req} = True
    ?der3:Subject{prop:Trn} = 0
    ?der3:Subject{prop:FontStyle} = font:Bold
    ?der3:R3Email:Prompt{prop:FontColor} = -1
    ?der3:R3Email:Prompt{prop:Color} = 15066597
    If ?der3:R3Email{prop:ReadOnly} = True
        ?der3:R3Email{prop:FontColor} = 65793
        ?der3:R3Email{prop:Color} = 15066597
    Elsif ?der3:R3Email{prop:Req} = True
        ?der3:R3Email{prop:FontColor} = 65793
        ?der3:R3Email{prop:Color} = 8454143
    Else ! If ?der3:R3Email{prop:Req} = True
        ?der3:R3Email{prop:FontColor} = 65793
        ?der3:R3Email{prop:Color} = 16777215
    End ! If ?der3:R3Email{prop:Req} = True
    ?der3:R3Email{prop:Trn} = 0
    ?der3:R3Email{prop:FontStyle} = font:Bold
    ?dem:R3Active{prop:Font,3} = -1
    ?dem:R3Active{prop:Color} = 15066597
    ?dem:R3Active{prop:Trn} = 0
    ?StrR4{prop:FontColor} = -1
    ?StrR4{prop:Color} = 15066597
    ?dem:ArrivedAtRetailText:Prompt{prop:FontColor} = -1
    ?dem:ArrivedAtRetailText:Prompt{prop:Color} = 15066597
    If ?dem:ArrivedAtRetailText{prop:ReadOnly} = True
        ?dem:ArrivedAtRetailText{prop:FontColor} = 65793
        ?dem:ArrivedAtRetailText{prop:Color} = 15066597
    Elsif ?dem:ArrivedAtRetailText{prop:Req} = True
        ?dem:ArrivedAtRetailText{prop:FontColor} = 65793
        ?dem:ArrivedAtRetailText{prop:Color} = 8454143
    Else ! If ?dem:ArrivedAtRetailText{prop:Req} = True
        ?dem:ArrivedAtRetailText{prop:FontColor} = 65793
        ?dem:ArrivedAtRetailText{prop:Color} = 16777215
    End ! If ?dem:ArrivedAtRetailText{prop:Req} = True
    ?dem:ArrivedAtRetailText{prop:Trn} = 0
    ?dem:ArrivedAtRetailText{prop:FontStyle} = font:Bold
    ?dem:R4SMSActive{prop:Font,3} = -1
    ?dem:R4SMSActive{prop:Color} = 15066597
    ?dem:R4SMSActive{prop:Trn} = 0
    ?der4:Subject:Prompt{prop:FontColor} = -1
    ?der4:Subject:Prompt{prop:Color} = 15066597
    If ?der4:Subject{prop:ReadOnly} = True
        ?der4:Subject{prop:FontColor} = 65793
        ?der4:Subject{prop:Color} = 15066597
    Elsif ?der4:Subject{prop:Req} = True
        ?der4:Subject{prop:FontColor} = 65793
        ?der4:Subject{prop:Color} = 8454143
    Else ! If ?der4:Subject{prop:Req} = True
        ?der4:Subject{prop:FontColor} = 65793
        ?der4:Subject{prop:Color} = 16777215
    End ! If ?der4:Subject{prop:Req} = True
    ?der4:Subject{prop:Trn} = 0
    ?der4:Subject{prop:FontStyle} = font:Bold
    ?der4:R4Email:Prompt{prop:FontColor} = -1
    ?der4:R4Email:Prompt{prop:Color} = 15066597
    If ?der4:R4Email{prop:ReadOnly} = True
        ?der4:R4Email{prop:FontColor} = 65793
        ?der4:R4Email{prop:Color} = 15066597
    Elsif ?der4:R4Email{prop:Req} = True
        ?der4:R4Email{prop:FontColor} = 65793
        ?der4:R4Email{prop:Color} = 8454143
    Else ! If ?der4:R4Email{prop:Req} = True
        ?der4:R4Email{prop:FontColor} = 65793
        ?der4:R4Email{prop:Color} = 16777215
    End ! If ?der4:R4Email{prop:Req} = True
    ?der4:R4Email{prop:Trn} = 0
    ?der4:R4Email{prop:FontStyle} = font:Bold
    ?dem:R4Active{prop:Font,3} = -1
    ?dem:R4Active{prop:Color} = 15066597
    ?dem:R4Active{prop:Trn} = 0
    ?Tab2{prop:Color} = 15066597
    ?dem2:E1SMS:Prompt{prop:FontColor} = -1
    ?dem2:E1SMS:Prompt{prop:Color} = 15066597
    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    If ?dem2:E1SMS{prop:ReadOnly} = True
        ?dem2:E1SMS{prop:FontColor} = 65793
        ?dem2:E1SMS{prop:Color} = 15066597
    Elsif ?dem2:E1SMS{prop:Req} = True
        ?dem2:E1SMS{prop:FontColor} = 65793
        ?dem2:E1SMS{prop:Color} = 8454143
    Else ! If ?dem2:E1SMS{prop:Req} = True
        ?dem2:E1SMS{prop:FontColor} = 65793
        ?dem2:E1SMS{prop:Color} = 16777215
    End ! If ?dem2:E1SMS{prop:Req} = True
    ?dem2:E1SMS{prop:Trn} = 0
    ?dem2:E1SMS{prop:FontStyle} = font:Bold
    ?dem2:E1SMSActive{prop:Font,3} = -1
    ?dem2:E1SMSActive{prop:Color} = 15066597
    ?dem2:E1SMSActive{prop:Trn} = 0
    ?dee1:Subject:Prompt{prop:FontColor} = -1
    ?dee1:Subject:Prompt{prop:Color} = 15066597
    If ?dee1:Subject{prop:ReadOnly} = True
        ?dee1:Subject{prop:FontColor} = 65793
        ?dee1:Subject{prop:Color} = 15066597
    Elsif ?dee1:Subject{prop:Req} = True
        ?dee1:Subject{prop:FontColor} = 65793
        ?dee1:Subject{prop:Color} = 8454143
    Else ! If ?dee1:Subject{prop:Req} = True
        ?dee1:Subject{prop:FontColor} = 65793
        ?dee1:Subject{prop:Color} = 16777215
    End ! If ?dee1:Subject{prop:Req} = True
    ?dee1:Subject{prop:Trn} = 0
    ?dee1:Subject{prop:FontStyle} = font:Bold
    ?dem3:E1Email:Prompt{prop:FontColor} = -1
    ?dem3:E1Email:Prompt{prop:Color} = 15066597
    If ?dee1:E1Email{prop:ReadOnly} = True
        ?dee1:E1Email{prop:FontColor} = 65793
        ?dee1:E1Email{prop:Color} = 15066597
    Elsif ?dee1:E1Email{prop:Req} = True
        ?dee1:E1Email{prop:FontColor} = 65793
        ?dee1:E1Email{prop:Color} = 8454143
    Else ! If ?dee1:E1Email{prop:Req} = True
        ?dee1:E1Email{prop:FontColor} = 65793
        ?dee1:E1Email{prop:Color} = 16777215
    End ! If ?dee1:E1Email{prop:Req} = True
    ?dee1:E1Email{prop:Trn} = 0
    ?dee1:E1Email{prop:FontStyle} = font:Bold
    ?dem2:E1Active{prop:Font,3} = -1
    ?dem2:E1Active{prop:Color} = 15066597
    ?dem2:E1Active{prop:Trn} = 0
    ?dem2:E2SMS:Prompt{prop:FontColor} = -1
    ?dem2:E2SMS:Prompt{prop:Color} = 15066597
    ?Prompt14:2{prop:FontColor} = -1
    ?Prompt14:2{prop:Color} = 15066597
    If ?dem2:E2SMS{prop:ReadOnly} = True
        ?dem2:E2SMS{prop:FontColor} = 65793
        ?dem2:E2SMS{prop:Color} = 15066597
    Elsif ?dem2:E2SMS{prop:Req} = True
        ?dem2:E2SMS{prop:FontColor} = 65793
        ?dem2:E2SMS{prop:Color} = 8454143
    Else ! If ?dem2:E2SMS{prop:Req} = True
        ?dem2:E2SMS{prop:FontColor} = 65793
        ?dem2:E2SMS{prop:Color} = 16777215
    End ! If ?dem2:E2SMS{prop:Req} = True
    ?dem2:E2SMS{prop:Trn} = 0
    ?dem2:E2SMS{prop:FontStyle} = font:Bold
    ?dem2:E2SMSActive{prop:Font,3} = -1
    ?dem2:E2SMSActive{prop:Color} = 15066597
    ?dem2:E2SMSActive{prop:Trn} = 0
    ?dee2:Subject:Prompt{prop:FontColor} = -1
    ?dee2:Subject:Prompt{prop:Color} = 15066597
    If ?dee2:Subject{prop:ReadOnly} = True
        ?dee2:Subject{prop:FontColor} = 65793
        ?dee2:Subject{prop:Color} = 15066597
    Elsif ?dee2:Subject{prop:Req} = True
        ?dee2:Subject{prop:FontColor} = 65793
        ?dee2:Subject{prop:Color} = 8454143
    Else ! If ?dee2:Subject{prop:Req} = True
        ?dee2:Subject{prop:FontColor} = 65793
        ?dee2:Subject{prop:Color} = 16777215
    End ! If ?dee2:Subject{prop:Req} = True
    ?dee2:Subject{prop:Trn} = 0
    ?dee2:Subject{prop:FontStyle} = font:Bold
    ?dem3:E1Email:Prompt:2{prop:FontColor} = -1
    ?dem3:E1Email:Prompt:2{prop:Color} = 15066597
    If ?dee2:E2Email{prop:ReadOnly} = True
        ?dee2:E2Email{prop:FontColor} = 65793
        ?dee2:E2Email{prop:Color} = 15066597
    Elsif ?dee2:E2Email{prop:Req} = True
        ?dee2:E2Email{prop:FontColor} = 65793
        ?dee2:E2Email{prop:Color} = 8454143
    Else ! If ?dee2:E2Email{prop:Req} = True
        ?dee2:E2Email{prop:FontColor} = 65793
        ?dee2:E2Email{prop:Color} = 16777215
    End ! If ?dee2:E2Email{prop:Req} = True
    ?dee2:E2Email{prop:Trn} = 0
    ?dee2:E2Email{prop:FontStyle} = font:Bold
    ?dem2:E2Active{prop:Font,3} = -1
    ?dem2:E2Active{prop:Color} = 15066597
    ?dem2:E2Active{prop:Trn} = 0
    ?PromptE2B{prop:FontColor} = -1
    ?PromptE2B{prop:Color} = 15066597
    ?dem2:E2BSMS:Prompt{prop:FontColor} = -1
    ?dem2:E2BSMS:Prompt{prop:Color} = 15066597
    If ?dem2:E2BSMS{prop:ReadOnly} = True
        ?dem2:E2BSMS{prop:FontColor} = 65793
        ?dem2:E2BSMS{prop:Color} = 15066597
    Elsif ?dem2:E2BSMS{prop:Req} = True
        ?dem2:E2BSMS{prop:FontColor} = 65793
        ?dem2:E2BSMS{prop:Color} = 8454143
    Else ! If ?dem2:E2BSMS{prop:Req} = True
        ?dem2:E2BSMS{prop:FontColor} = 65793
        ?dem2:E2BSMS{prop:Color} = 16777215
    End ! If ?dem2:E2BSMS{prop:Req} = True
    ?dem2:E2BSMS{prop:Trn} = 0
    ?dem2:E2BSMS{prop:FontStyle} = font:Bold
    ?dem2:E2BSMSActive{prop:Font,3} = -1
    ?dem2:E2BSMSActive{prop:Color} = 15066597
    ?dem2:E2BSMSActive{prop:Trn} = 0
    ?dee2B:Subject:Prompt{prop:FontColor} = -1
    ?dee2B:Subject:Prompt{prop:Color} = 15066597
    If ?dee2B:Subject{prop:ReadOnly} = True
        ?dee2B:Subject{prop:FontColor} = 65793
        ?dee2B:Subject{prop:Color} = 15066597
    Elsif ?dee2B:Subject{prop:Req} = True
        ?dee2B:Subject{prop:FontColor} = 65793
        ?dee2B:Subject{prop:Color} = 8454143
    Else ! If ?dee2B:Subject{prop:Req} = True
        ?dee2B:Subject{prop:FontColor} = 65793
        ?dee2B:Subject{prop:Color} = 16777215
    End ! If ?dee2B:Subject{prop:Req} = True
    ?dee2B:Subject{prop:Trn} = 0
    ?dee2B:Subject{prop:FontStyle} = font:Bold
    ?dee2B:E2BEmail:Prompt{prop:FontColor} = -1
    ?dee2B:E2BEmail:Prompt{prop:Color} = 15066597
    If ?dee2B:E2BEmail{prop:ReadOnly} = True
        ?dee2B:E2BEmail{prop:FontColor} = 65793
        ?dee2B:E2BEmail{prop:Color} = 15066597
    Elsif ?dee2B:E2BEmail{prop:Req} = True
        ?dee2B:E2BEmail{prop:FontColor} = 65793
        ?dee2B:E2BEmail{prop:Color} = 8454143
    Else ! If ?dee2B:E2BEmail{prop:Req} = True
        ?dee2B:E2BEmail{prop:FontColor} = 65793
        ?dee2B:E2BEmail{prop:Color} = 16777215
    End ! If ?dee2B:E2BEmail{prop:Req} = True
    ?dee2B:E2BEmail{prop:Trn} = 0
    ?dee2B:E2BEmail{prop:FontStyle} = font:Bold
    ?dem2:E2BActive{prop:Font,3} = -1
    ?dem2:E2BActive{prop:Color} = 15066597
    ?dem2:E2BActive{prop:Trn} = 0
    ?dem2:E3SMS:Prompt{prop:FontColor} = -1
    ?dem2:E3SMS:Prompt{prop:Color} = 15066597
    ?Prompt14:3{prop:FontColor} = -1
    ?Prompt14:3{prop:Color} = 15066597
    If ?dem2:E3SMS{prop:ReadOnly} = True
        ?dem2:E3SMS{prop:FontColor} = 65793
        ?dem2:E3SMS{prop:Color} = 15066597
    Elsif ?dem2:E3SMS{prop:Req} = True
        ?dem2:E3SMS{prop:FontColor} = 65793
        ?dem2:E3SMS{prop:Color} = 8454143
    Else ! If ?dem2:E3SMS{prop:Req} = True
        ?dem2:E3SMS{prop:FontColor} = 65793
        ?dem2:E3SMS{prop:Color} = 16777215
    End ! If ?dem2:E3SMS{prop:Req} = True
    ?dem2:E3SMS{prop:Trn} = 0
    ?dem2:E3SMS{prop:FontStyle} = font:Bold
    ?dem2:E3SMSActive{prop:Font,3} = -1
    ?dem2:E3SMSActive{prop:Color} = 15066597
    ?dem2:E3SMSActive{prop:Trn} = 0
    ?dee3:Subject:Prompt{prop:FontColor} = -1
    ?dee3:Subject:Prompt{prop:Color} = 15066597
    If ?dee3:Subject{prop:ReadOnly} = True
        ?dee3:Subject{prop:FontColor} = 65793
        ?dee3:Subject{prop:Color} = 15066597
    Elsif ?dee3:Subject{prop:Req} = True
        ?dee3:Subject{prop:FontColor} = 65793
        ?dee3:Subject{prop:Color} = 8454143
    Else ! If ?dee3:Subject{prop:Req} = True
        ?dee3:Subject{prop:FontColor} = 65793
        ?dee3:Subject{prop:Color} = 16777215
    End ! If ?dee3:Subject{prop:Req} = True
    ?dee3:Subject{prop:Trn} = 0
    ?dee3:Subject{prop:FontStyle} = font:Bold
    ?dem3:E1Email:Prompt:3{prop:FontColor} = -1
    ?dem3:E1Email:Prompt:3{prop:Color} = 15066597
    If ?dee3:E3Email{prop:ReadOnly} = True
        ?dee3:E3Email{prop:FontColor} = 65793
        ?dee3:E3Email{prop:Color} = 15066597
    Elsif ?dee3:E3Email{prop:Req} = True
        ?dee3:E3Email{prop:FontColor} = 65793
        ?dee3:E3Email{prop:Color} = 8454143
    Else ! If ?dee3:E3Email{prop:Req} = True
        ?dee3:E3Email{prop:FontColor} = 65793
        ?dee3:E3Email{prop:Color} = 16777215
    End ! If ?dee3:E3Email{prop:Req} = True
    ?dee3:E3Email{prop:Trn} = 0
    ?dee3:E3Email{prop:FontStyle} = font:Bold
    ?dem2:E3Active{prop:Font,3} = -1
    ?dem2:E3Active{prop:Color} = 15066597
    ?dem2:E3Active{prop:Trn} = 0
    ?PromptE3B{prop:FontColor} = -1
    ?PromptE3B{prop:Color} = 15066597
    ?dem2:E3BSMS:Prompt{prop:FontColor} = -1
    ?dem2:E3BSMS:Prompt{prop:Color} = 15066597
    If ?dem2:E3BSMS{prop:ReadOnly} = True
        ?dem2:E3BSMS{prop:FontColor} = 65793
        ?dem2:E3BSMS{prop:Color} = 15066597
    Elsif ?dem2:E3BSMS{prop:Req} = True
        ?dem2:E3BSMS{prop:FontColor} = 65793
        ?dem2:E3BSMS{prop:Color} = 8454143
    Else ! If ?dem2:E3BSMS{prop:Req} = True
        ?dem2:E3BSMS{prop:FontColor} = 65793
        ?dem2:E3BSMS{prop:Color} = 16777215
    End ! If ?dem2:E3BSMS{prop:Req} = True
    ?dem2:E3BSMS{prop:Trn} = 0
    ?dem2:E3BSMS{prop:FontStyle} = font:Bold
    ?dem2:E3BSMSActive{prop:Font,3} = -1
    ?dem2:E3BSMSActive{prop:Color} = 15066597
    ?dem2:E3BSMSActive{prop:Trn} = 0
    ?dee3B:Subject:Prompt{prop:FontColor} = -1
    ?dee3B:Subject:Prompt{prop:Color} = 15066597
    If ?dee3B:Subject{prop:ReadOnly} = True
        ?dee3B:Subject{prop:FontColor} = 65793
        ?dee3B:Subject{prop:Color} = 15066597
    Elsif ?dee3B:Subject{prop:Req} = True
        ?dee3B:Subject{prop:FontColor} = 65793
        ?dee3B:Subject{prop:Color} = 8454143
    Else ! If ?dee3B:Subject{prop:Req} = True
        ?dee3B:Subject{prop:FontColor} = 65793
        ?dee3B:Subject{prop:Color} = 16777215
    End ! If ?dee3B:Subject{prop:Req} = True
    ?dee3B:Subject{prop:Trn} = 0
    ?dee3B:Subject{prop:FontStyle} = font:Bold
    ?dee3B:E3BEmail:Prompt{prop:FontColor} = -1
    ?dee3B:E3BEmail:Prompt{prop:Color} = 15066597
    If ?dee3B:E3BEmail{prop:ReadOnly} = True
        ?dee3B:E3BEmail{prop:FontColor} = 65793
        ?dee3B:E3BEmail{prop:Color} = 15066597
    Elsif ?dee3B:E3BEmail{prop:Req} = True
        ?dee3B:E3BEmail{prop:FontColor} = 65793
        ?dee3B:E3BEmail{prop:Color} = 8454143
    Else ! If ?dee3B:E3BEmail{prop:Req} = True
        ?dee3B:E3BEmail{prop:FontColor} = 65793
        ?dee3B:E3BEmail{prop:Color} = 16777215
    End ! If ?dee3B:E3BEmail{prop:Req} = True
    ?dee3B:E3BEmail{prop:Trn} = 0
    ?dee3B:E3BEmail{prop:FontStyle} = font:Bold
    ?dem2:E3BActive{prop:Font,3} = -1
    ?dem2:E3BActive{prop:Color} = 15066597
    ?dem2:E3BActive{prop:Trn} = 0
    ?Tab3{prop:Color} = 15066597
    ?dem2:E1SMS:Prompt:2{prop:FontColor} = -1
    ?dem2:E1SMS:Prompt:2{prop:Color} = 15066597
    ?dem2:E1SMS:Prompt:3{prop:FontColor} = -1
    ?dem2:E1SMS:Prompt:3{prop:Color} = 15066597
    ?dem2:B1Active{prop:Font,3} = -1
    ?dem2:B1Active{prop:Color} = 15066597
    ?dem2:B1Active{prop:Trn} = 0
    ?dem2:B4Active{prop:Font,3} = -1
    ?dem2:B4Active{prop:Color} = 15066597
    ?dem2:B4Active{prop:Trn} = 0
    ?Prompt14:4{prop:FontColor} = -1
    ?Prompt14:4{prop:Color} = 15066597
    If ?dem2:B1SMS{prop:ReadOnly} = True
        ?dem2:B1SMS{prop:FontColor} = 65793
        ?dem2:B1SMS{prop:Color} = 15066597
    Elsif ?dem2:B1SMS{prop:Req} = True
        ?dem2:B1SMS{prop:FontColor} = 65793
        ?dem2:B1SMS{prop:Color} = 8454143
    Else ! If ?dem2:B1SMS{prop:Req} = True
        ?dem2:B1SMS{prop:FontColor} = 65793
        ?dem2:B1SMS{prop:Color} = 16777215
    End ! If ?dem2:B1SMS{prop:Req} = True
    ?dem2:B1SMS{prop:Trn} = 0
    ?dem2:B1SMS{prop:FontStyle} = font:Bold
    If ?deb1:B1Email{prop:ReadOnly} = True
        ?deb1:B1Email{prop:FontColor} = 65793
        ?deb1:B1Email{prop:Color} = 15066597
    Elsif ?deb1:B1Email{prop:Req} = True
        ?deb1:B1Email{prop:FontColor} = 65793
        ?deb1:B1Email{prop:Color} = 8454143
    Else ! If ?deb1:B1Email{prop:Req} = True
        ?deb1:B1Email{prop:FontColor} = 65793
        ?deb1:B1Email{prop:Color} = 16777215
    End ! If ?deb1:B1Email{prop:Req} = True
    ?deb1:B1Email{prop:Trn} = 0
    ?deb1:B1Email{prop:FontStyle} = font:Bold
    ?Prompt14:7{prop:FontColor} = -1
    ?Prompt14:7{prop:Color} = 15066597
    If ?dem2:B2SMS{prop:ReadOnly} = True
        ?dem2:B2SMS{prop:FontColor} = 65793
        ?dem2:B2SMS{prop:Color} = 15066597
    Elsif ?dem2:B2SMS{prop:Req} = True
        ?dem2:B2SMS{prop:FontColor} = 65793
        ?dem2:B2SMS{prop:Color} = 8454143
    Else ! If ?dem2:B2SMS{prop:Req} = True
        ?dem2:B2SMS{prop:FontColor} = 65793
        ?dem2:B2SMS{prop:Color} = 16777215
    End ! If ?dem2:B2SMS{prop:Req} = True
    ?dem2:B2SMS{prop:Trn} = 0
    ?dem2:B2SMS{prop:FontStyle} = font:Bold
    If ?deb2:B2Email{prop:ReadOnly} = True
        ?deb2:B2Email{prop:FontColor} = 65793
        ?deb2:B2Email{prop:Color} = 15066597
    Elsif ?deb2:B2Email{prop:Req} = True
        ?deb2:B2Email{prop:FontColor} = 65793
        ?deb2:B2Email{prop:Color} = 8454143
    Else ! If ?deb2:B2Email{prop:Req} = True
        ?deb2:B2Email{prop:FontColor} = 65793
        ?deb2:B2Email{prop:Color} = 16777215
    End ! If ?deb2:B2Email{prop:Req} = True
    ?deb2:B2Email{prop:Trn} = 0
    ?deb2:B2Email{prop:FontStyle} = font:Bold
    If ?dem2:B3SMS{prop:ReadOnly} = True
        ?dem2:B3SMS{prop:FontColor} = 65793
        ?dem2:B3SMS{prop:Color} = 15066597
    Elsif ?dem2:B3SMS{prop:Req} = True
        ?dem2:B3SMS{prop:FontColor} = 65793
        ?dem2:B3SMS{prop:Color} = 8454143
    Else ! If ?dem2:B3SMS{prop:Req} = True
        ?dem2:B3SMS{prop:FontColor} = 65793
        ?dem2:B3SMS{prop:Color} = 16777215
    End ! If ?dem2:B3SMS{prop:Req} = True
    ?dem2:B3SMS{prop:Trn} = 0
    ?dem2:B3SMS{prop:FontStyle} = font:Bold
    ?dem2:B3SMSActive{prop:Font,3} = -1
    ?dem2:B3SMSActive{prop:Color} = 15066597
    ?dem2:B3SMSActive{prop:Trn} = 0
    ?deb3:Subject:Prompt{prop:FontColor} = -1
    ?deb3:Subject:Prompt{prop:Color} = 15066597
    If ?deb3:Subject{prop:ReadOnly} = True
        ?deb3:Subject{prop:FontColor} = 65793
        ?deb3:Subject{prop:Color} = 15066597
    Elsif ?deb3:Subject{prop:Req} = True
        ?deb3:Subject{prop:FontColor} = 65793
        ?deb3:Subject{prop:Color} = 8454143
    Else ! If ?deb3:Subject{prop:Req} = True
        ?deb3:Subject{prop:FontColor} = 65793
        ?deb3:Subject{prop:Color} = 16777215
    End ! If ?deb3:Subject{prop:Req} = True
    ?deb3:Subject{prop:Trn} = 0
    ?deb3:Subject{prop:FontStyle} = font:Bold
    If ?deb3:B3Email{prop:ReadOnly} = True
        ?deb3:B3Email{prop:FontColor} = 65793
        ?deb3:B3Email{prop:Color} = 15066597
    Elsif ?deb3:B3Email{prop:Req} = True
        ?deb3:B3Email{prop:FontColor} = 65793
        ?deb3:B3Email{prop:Color} = 8454143
    Else ! If ?deb3:B3Email{prop:Req} = True
        ?deb3:B3Email{prop:FontColor} = 65793
        ?deb3:B3Email{prop:Color} = 16777215
    End ! If ?deb3:B3Email{prop:Req} = True
    ?deb3:B3Email{prop:Trn} = 0
    ?deb3:B3Email{prop:FontStyle} = font:Bold
    If ?dem2:B4SMS{prop:ReadOnly} = True
        ?dem2:B4SMS{prop:FontColor} = 65793
        ?dem2:B4SMS{prop:Color} = 15066597
    Elsif ?dem2:B4SMS{prop:Req} = True
        ?dem2:B4SMS{prop:FontColor} = 65793
        ?dem2:B4SMS{prop:Color} = 8454143
    Else ! If ?dem2:B4SMS{prop:Req} = True
        ?dem2:B4SMS{prop:FontColor} = 65793
        ?dem2:B4SMS{prop:Color} = 16777215
    End ! If ?dem2:B4SMS{prop:Req} = True
    ?dem2:B4SMS{prop:Trn} = 0
    ?dem2:B4SMS{prop:FontStyle} = font:Bold
    ?dem2:B1SMSActive{prop:Font,3} = -1
    ?dem2:B1SMSActive{prop:Color} = 15066597
    ?dem2:B1SMSActive{prop:Trn} = 0
    ?dem2:B4SMSActive{prop:Font,3} = -1
    ?dem2:B4SMSActive{prop:Color} = 15066597
    ?dem2:B4SMSActive{prop:Trn} = 0
    ?deb1:Subject:Prompt{prop:FontColor} = -1
    ?deb1:Subject:Prompt{prop:Color} = 15066597
    If ?deb1:Subject{prop:ReadOnly} = True
        ?deb1:Subject{prop:FontColor} = 65793
        ?deb1:Subject{prop:Color} = 15066597
    Elsif ?deb1:Subject{prop:Req} = True
        ?deb1:Subject{prop:FontColor} = 65793
        ?deb1:Subject{prop:Color} = 8454143
    Else ! If ?deb1:Subject{prop:Req} = True
        ?deb1:Subject{prop:FontColor} = 65793
        ?deb1:Subject{prop:Color} = 16777215
    End ! If ?deb1:Subject{prop:Req} = True
    ?deb1:Subject{prop:Trn} = 0
    ?deb1:Subject{prop:FontStyle} = font:Bold
    ?deb4:Subject:Prompt{prop:FontColor} = -1
    ?deb4:Subject:Prompt{prop:Color} = 15066597
    If ?deb4:Subject{prop:ReadOnly} = True
        ?deb4:Subject{prop:FontColor} = 65793
        ?deb4:Subject{prop:Color} = 15066597
    Elsif ?deb4:Subject{prop:Req} = True
        ?deb4:Subject{prop:FontColor} = 65793
        ?deb4:Subject{prop:Color} = 8454143
    Else ! If ?deb4:Subject{prop:Req} = True
        ?deb4:Subject{prop:FontColor} = 65793
        ?deb4:Subject{prop:Color} = 16777215
    End ! If ?deb4:Subject{prop:Req} = True
    ?deb4:Subject{prop:Trn} = 0
    ?deb4:Subject{prop:FontStyle} = font:Bold
    ?dem3:E1Email:Prompt:4{prop:FontColor} = -1
    ?dem3:E1Email:Prompt:4{prop:Color} = 15066597
    ?dem3:E1Email:Prompt:8{prop:FontColor} = -1
    ?dem3:E1Email:Prompt:8{prop:Color} = 15066597
    If ?deb4:B4Email{prop:ReadOnly} = True
        ?deb4:B4Email{prop:FontColor} = 65793
        ?deb4:B4Email{prop:Color} = 15066597
    Elsif ?deb4:B4Email{prop:Req} = True
        ?deb4:B4Email{prop:FontColor} = 65793
        ?deb4:B4Email{prop:Color} = 8454143
    Else ! If ?deb4:B4Email{prop:Req} = True
        ?deb4:B4Email{prop:FontColor} = 65793
        ?deb4:B4Email{prop:Color} = 16777215
    End ! If ?deb4:B4Email{prop:Req} = True
    ?deb4:B4Email{prop:Trn} = 0
    ?deb4:B4Email{prop:FontStyle} = font:Bold
    ?dem2:E2SMS:Prompt:2{prop:FontColor} = -1
    ?dem2:E2SMS:Prompt:2{prop:Color} = 15066597
    ?dem2:E2SMS:Prompt:3{prop:FontColor} = -1
    ?dem2:E2SMS:Prompt:3{prop:Color} = 15066597
    ?dem2:B2Active{prop:Font,3} = -1
    ?dem2:B2Active{prop:Color} = 15066597
    ?dem2:B2Active{prop:Trn} = 0
    ?dem2:B5Active{prop:Font,3} = -1
    ?dem2:B5Active{prop:Color} = 15066597
    ?dem2:B5Active{prop:Trn} = 0
    ?Prompt14:5{prop:FontColor} = -1
    ?Prompt14:5{prop:Color} = 15066597
    ?Prompt14:8{prop:FontColor} = -1
    ?Prompt14:8{prop:Color} = 15066597
    If ?dem2:B5SMS{prop:ReadOnly} = True
        ?dem2:B5SMS{prop:FontColor} = 65793
        ?dem2:B5SMS{prop:Color} = 15066597
    Elsif ?dem2:B5SMS{prop:Req} = True
        ?dem2:B5SMS{prop:FontColor} = 65793
        ?dem2:B5SMS{prop:Color} = 8454143
    Else ! If ?dem2:B5SMS{prop:Req} = True
        ?dem2:B5SMS{prop:FontColor} = 65793
        ?dem2:B5SMS{prop:Color} = 16777215
    End ! If ?dem2:B5SMS{prop:Req} = True
    ?dem2:B5SMS{prop:Trn} = 0
    ?dem2:B5SMS{prop:FontStyle} = font:Bold
    ?dem2:B5SMSActive{prop:Font,3} = -1
    ?dem2:B5SMSActive{prop:Color} = 15066597
    ?dem2:B5SMSActive{prop:Trn} = 0
    ?dem2:B2SMSActive{prop:Font,3} = -1
    ?dem2:B2SMSActive{prop:Color} = 15066597
    ?dem2:B2SMSActive{prop:Trn} = 0
    ?deb2:Subject:Prompt{prop:FontColor} = -1
    ?deb2:Subject:Prompt{prop:Color} = 15066597
    If ?deb2:Subject{prop:ReadOnly} = True
        ?deb2:Subject{prop:FontColor} = 65793
        ?deb2:Subject{prop:Color} = 15066597
    Elsif ?deb2:Subject{prop:Req} = True
        ?deb2:Subject{prop:FontColor} = 65793
        ?deb2:Subject{prop:Color} = 8454143
    Else ! If ?deb2:Subject{prop:Req} = True
        ?deb2:Subject{prop:FontColor} = 65793
        ?deb2:Subject{prop:Color} = 16777215
    End ! If ?deb2:Subject{prop:Req} = True
    ?deb2:Subject{prop:Trn} = 0
    ?deb2:Subject{prop:FontStyle} = font:Bold
    ?deb5:Subject:Prompt{prop:FontColor} = -1
    ?deb5:Subject:Prompt{prop:Color} = 15066597
    If ?deb5:Subject{prop:ReadOnly} = True
        ?deb5:Subject{prop:FontColor} = 65793
        ?deb5:Subject{prop:Color} = 15066597
    Elsif ?deb5:Subject{prop:Req} = True
        ?deb5:Subject{prop:FontColor} = 65793
        ?deb5:Subject{prop:Color} = 8454143
    Else ! If ?deb5:Subject{prop:Req} = True
        ?deb5:Subject{prop:FontColor} = 65793
        ?deb5:Subject{prop:Color} = 16777215
    End ! If ?deb5:Subject{prop:Req} = True
    ?deb5:Subject{prop:Trn} = 0
    ?deb5:Subject{prop:FontStyle} = font:Bold
    ?dem3:E1Email:Prompt:5{prop:FontColor} = -1
    ?dem3:E1Email:Prompt:5{prop:Color} = 15066597
    ?dem3:E1Email:Prompt:7{prop:FontColor} = -1
    ?dem3:E1Email:Prompt:7{prop:Color} = 15066597
    If ?deb5:B5Email{prop:ReadOnly} = True
        ?deb5:B5Email{prop:FontColor} = 65793
        ?deb5:B5Email{prop:Color} = 15066597
    Elsif ?deb5:B5Email{prop:Req} = True
        ?deb5:B5Email{prop:FontColor} = 65793
        ?deb5:B5Email{prop:Color} = 8454143
    Else ! If ?deb5:B5Email{prop:Req} = True
        ?deb5:B5Email{prop:FontColor} = 65793
        ?deb5:B5Email{prop:Color} = 16777215
    End ! If ?deb5:B5Email{prop:Req} = True
    ?deb5:B5Email{prop:Trn} = 0
    ?deb5:B5Email{prop:FontStyle} = font:Bold
    ?dem2:E3SMS:Prompt:2{prop:FontColor} = -1
    ?dem2:E3SMS:Prompt:2{prop:Color} = 15066597
    ?dem2:B3Active{prop:Font,3} = -1
    ?dem2:B3Active{prop:Color} = 15066597
    ?dem2:B3Active{prop:Trn} = 0
    ?dem3:E1Email:Prompt:6{prop:FontColor} = -1
    ?dem3:E1Email:Prompt:6{prop:Color} = 15066597
    ?Prompt14:6{prop:FontColor} = -1
    ?Prompt14:6{prop:Color} = 15066597
    ?Tab5{prop:Color} = 15066597
    ?PromptB6{prop:FontColor} = -1
    ?PromptB6{prop:Color} = 15066597
    ?dem2:B6SMS:Prompt{prop:FontColor} = -1
    ?dem2:B6SMS:Prompt{prop:Color} = 15066597
    If ?dem2:B6SMS{prop:ReadOnly} = True
        ?dem2:B6SMS{prop:FontColor} = 65793
        ?dem2:B6SMS{prop:Color} = 15066597
    Elsif ?dem2:B6SMS{prop:Req} = True
        ?dem2:B6SMS{prop:FontColor} = 65793
        ?dem2:B6SMS{prop:Color} = 8454143
    Else ! If ?dem2:B6SMS{prop:Req} = True
        ?dem2:B6SMS{prop:FontColor} = 65793
        ?dem2:B6SMS{prop:Color} = 16777215
    End ! If ?dem2:B6SMS{prop:Req} = True
    ?dem2:B6SMS{prop:Trn} = 0
    ?dem2:B6SMS{prop:FontStyle} = font:Bold
    ?dem2:B6SMSActive{prop:Font,3} = -1
    ?dem2:B6SMSActive{prop:Color} = 15066597
    ?dem2:B6SMSActive{prop:Trn} = 0
    ?deb6:Subject:Prompt{prop:FontColor} = -1
    ?deb6:Subject:Prompt{prop:Color} = 15066597
    If ?deb6:Subject{prop:ReadOnly} = True
        ?deb6:Subject{prop:FontColor} = 65793
        ?deb6:Subject{prop:Color} = 15066597
    Elsif ?deb6:Subject{prop:Req} = True
        ?deb6:Subject{prop:FontColor} = 65793
        ?deb6:Subject{prop:Color} = 8454143
    Else ! If ?deb6:Subject{prop:Req} = True
        ?deb6:Subject{prop:FontColor} = 65793
        ?deb6:Subject{prop:Color} = 16777215
    End ! If ?deb6:Subject{prop:Req} = True
    ?deb6:Subject{prop:Trn} = 0
    ?deb6:Subject{prop:FontStyle} = font:Bold
    ?dem2:B6Active{prop:Font,3} = -1
    ?dem2:B6Active{prop:Color} = 15066597
    ?dem2:B6Active{prop:Trn} = 0
    ?deb6:B6Email:Prompt{prop:FontColor} = -1
    ?deb6:B6Email:Prompt{prop:Color} = 15066597
    If ?deb6:B6Email{prop:ReadOnly} = True
        ?deb6:B6Email{prop:FontColor} = 65793
        ?deb6:B6Email{prop:Color} = 15066597
    Elsif ?deb6:B6Email{prop:Req} = True
        ?deb6:B6Email{prop:FontColor} = 65793
        ?deb6:B6Email{prop:Color} = 8454143
    Else ! If ?deb6:B6Email{prop:Req} = True
        ?deb6:B6Email{prop:FontColor} = 65793
        ?deb6:B6Email{prop:Color} = 16777215
    End ! If ?deb6:B6Email{prop:Req} = True
    ?deb6:B6Email{prop:Trn} = 0
    ?deb6:B6Email{prop:FontStyle} = font:Bold
    ?dem2:B6NotifyDays:Prompt{prop:FontColor} = -1
    ?dem2:B6NotifyDays:Prompt{prop:Color} = 15066597
    If ?dem2:B6NotifyDays{prop:ReadOnly} = True
        ?dem2:B6NotifyDays{prop:FontColor} = 65793
        ?dem2:B6NotifyDays{prop:Color} = 15066597
    Elsif ?dem2:B6NotifyDays{prop:Req} = True
        ?dem2:B6NotifyDays{prop:FontColor} = 65793
        ?dem2:B6NotifyDays{prop:Color} = 8454143
    Else ! If ?dem2:B6NotifyDays{prop:Req} = True
        ?dem2:B6NotifyDays{prop:FontColor} = 65793
        ?dem2:B6NotifyDays{prop:Color} = 16777215
    End ! If ?dem2:B6NotifyDays{prop:Req} = True
    ?dem2:B6NotifyDays{prop:Trn} = 0
    ?dem2:B6NotifyDays{prop:FontStyle} = font:Bold
    ?PromptB7{prop:FontColor} = -1
    ?PromptB7{prop:Color} = 15066597
    ?dem2:B7SMS:Prompt{prop:FontColor} = -1
    ?dem2:B7SMS:Prompt{prop:Color} = 15066597
    If ?dem2:B7SMS{prop:ReadOnly} = True
        ?dem2:B7SMS{prop:FontColor} = 65793
        ?dem2:B7SMS{prop:Color} = 15066597
    Elsif ?dem2:B7SMS{prop:Req} = True
        ?dem2:B7SMS{prop:FontColor} = 65793
        ?dem2:B7SMS{prop:Color} = 8454143
    Else ! If ?dem2:B7SMS{prop:Req} = True
        ?dem2:B7SMS{prop:FontColor} = 65793
        ?dem2:B7SMS{prop:Color} = 16777215
    End ! If ?dem2:B7SMS{prop:Req} = True
    ?dem2:B7SMS{prop:Trn} = 0
    ?dem2:B7SMS{prop:FontStyle} = font:Bold
    ?dem2:B7SMSActive{prop:Font,3} = -1
    ?dem2:B7SMSActive{prop:Color} = 15066597
    ?dem2:B7SMSActive{prop:Trn} = 0
    ?deb7:Subject:Prompt{prop:FontColor} = -1
    ?deb7:Subject:Prompt{prop:Color} = 15066597
    If ?deb7:Subject{prop:ReadOnly} = True
        ?deb7:Subject{prop:FontColor} = 65793
        ?deb7:Subject{prop:Color} = 15066597
    Elsif ?deb7:Subject{prop:Req} = True
        ?deb7:Subject{prop:FontColor} = 65793
        ?deb7:Subject{prop:Color} = 8454143
    Else ! If ?deb7:Subject{prop:Req} = True
        ?deb7:Subject{prop:FontColor} = 65793
        ?deb7:Subject{prop:Color} = 16777215
    End ! If ?deb7:Subject{prop:Req} = True
    ?deb7:Subject{prop:Trn} = 0
    ?deb7:Subject{prop:FontStyle} = font:Bold
    ?dem2:B7Active{prop:Font,3} = -1
    ?dem2:B7Active{prop:Color} = 15066597
    ?dem2:B7Active{prop:Trn} = 0
    ?deb7:B7Email:Prompt{prop:FontColor} = -1
    ?deb7:B7Email:Prompt{prop:Color} = 15066597
    If ?deb7:B7Email{prop:ReadOnly} = True
        ?deb7:B7Email{prop:FontColor} = 65793
        ?deb7:B7Email{prop:Color} = 15066597
    Elsif ?deb7:B7Email{prop:Req} = True
        ?deb7:B7Email{prop:FontColor} = 65793
        ?deb7:B7Email{prop:Color} = 8454143
    Else ! If ?deb7:B7Email{prop:Req} = True
        ?deb7:B7Email{prop:FontColor} = 65793
        ?deb7:B7Email{prop:Color} = 16777215
    End ! If ?deb7:B7Email{prop:Req} = True
    ?deb7:B7Email{prop:Trn} = 0
    ?deb7:B7Email{prop:FontStyle} = font:Bold
    ?dem2:B7NotifyDays:Prompt{prop:FontColor} = -1
    ?dem2:B7NotifyDays:Prompt{prop:Color} = 15066597
    If ?dem2:B7NotifyDays{prop:ReadOnly} = True
        ?dem2:B7NotifyDays{prop:FontColor} = 65793
        ?dem2:B7NotifyDays{prop:Color} = 15066597
    Elsif ?dem2:B7NotifyDays{prop:Req} = True
        ?dem2:B7NotifyDays{prop:FontColor} = 65793
        ?dem2:B7NotifyDays{prop:Color} = 8454143
    Else ! If ?dem2:B7NotifyDays{prop:Req} = True
        ?dem2:B7NotifyDays{prop:FontColor} = 65793
        ?dem2:B7NotifyDays{prop:Color} = 16777215
    End ! If ?dem2:B7NotifyDays{prop:Req} = True
    ?dem2:B7NotifyDays{prop:Trn} = 0
    ?dem2:B7NotifyDays{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SIDAlertDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?dem:SMTPServer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFEMAB1.Open
  Relate:DEFEMAB2.Open
  Relate:DEFEMAB3.Open
  Relate:DEFEMAB4.Open
  Relate:DEFEMAB5.Open
  Relate:DEFEMAB6.Open
  Relate:DEFEMAB7.Open
  Relate:DEFEMAE1.Open
  Relate:DEFEMAE2.Open
  Relate:DEFEMAE3.Open
  Relate:DEFEMAI2.Open
  Relate:DEFEMAIL.Open
  Relate:DEFEMAR1.Open
  Relate:DEFEMAR2.Open
  Relate:DEFEMAR3.Open
  Relate:DEFEMAR4.Open
  Relate:DEFEME2B.Open
  Relate:DEFEME3B.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Set(DefEmail)
  If access:DefEmail.next()
      get(DefEmail,0)
      if access:DefEmail.primerecord() = level:benign
          if access:DefEmail.insert()
              access:DefEmail.cancelautoinc()
          end
      end!if access:DefEmail.primerecord() = level:benign
  End!If access:DefEmail.next()

  Set(DEFEMAI2)
  If Access:DEFEMAI2.Next()
      If Access:DEFEMAI2.PrimeRecord() = Level:Benign
          If Access:DEFEMAI2.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAE1.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAI2.CancelAutoInc()
          End ! If Access:DEFEMAE1.TryInsert() = Level:Benign
      End !If Access:DEFEMAE1.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEE1.Next()
  
  Set(DEFEMAE1)
  If Access:DEFEMAE1.Next()
      If Access:DEFEMAE1.PrimeRecord() = Level:Benign
          If Access:DEFEMAE1.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAE1.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAE1.CancelAutoInc()
          End ! If Access:DEFEMAE1.TryInsert() = Level:Benign
      End !If Access:DEFEMAE1.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEE1.Next()

  Set(DEFEMAE2)
  If Access:DEFEMAE2.Next()
      If Access:DEFEMAE2.PrimeRecord() = Level:Benign
          If Access:DEFEMAE2.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAE2.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAE2.CancelAutoInc()
          End ! If Access:DEFEMAE2.TryInsert() = Level:Benign
      End !If Access:DEFEMAE2.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEE2.Next()

  Set(DEFEME2B)
  If Access:DEFEME2B.Next()
      If Access:DEFEME2B.PrimeRecord() = Level:Benign
          If Access:DEFEME2B.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEME2B.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEME2B.CancelAutoInc()
          End ! If Access:DEFEME2B.TryInsert() = Level:Benign
      End !If Access:DEFEME2B.PrimeRecord() = Level:Benign
  End ! If Access:DEFEME2B.Next()

  Set(DEFEMAE3)
  If Access:DEFEMAE3.Next()
      If Access:DEFEMAE3.PrimeRecord() = Level:Benign
          If Access:DEFEMAE3.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAE3.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAE3.CancelAutoInc()
          End ! If Access:DEFEMAE3.TryInsert() = Level:Benign
      End !If Access:DEFEMAE3.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEE3.Next()

  Set(DEFEME3B)
  If Access:DEFEME3B.Next()
      If Access:DEFEME3B.PrimeRecord() = Level:Benign
          If Access:DEFEME3B.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEME3B.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEME3B.CancelAutoInc()
          End ! If Access:DEFEME3B.TryInsert() = Level:Benign
      End !If Access:DEFEME3B.PrimeRecord() = Level:Benign
  End ! If Access:DEFEME3B.Next()

  Set(DEFEMAB1)
  If Access:DEFEMAB1.Next()
      If Access:DEFEMAB1.PrimeRecord() = Level:Benign
          If Access:DEFEMAB1.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAB1.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAB1.CancelAutoInc()
          End ! If Access:DEFEMAB1.TryInsert() = Level:Benign
      End !If Access:DEFEMAB1.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEB1.Next()

  Set(DEFEMAB2)
  If Access:DEFEMAB2.Next()
      If Access:DEFEMAB2.PrimeRecord() = Level:Benign
          If Access:DEFEMAB2.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAB2.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAB2.CancelAutoInc()
          End ! If Access:DEFEMAB2.TryInsert() = Level:Benign
      End !If Access:DEFEMAB2.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEB2.Next()

  Set(DEFEMAB3)
  If Access:DEFEMAB3.Next()
      If Access:DEFEMAB3.PrimeRecord() = Level:Benign
          If Access:DEFEMAB3.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAB3.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAB3.CancelAutoInc()
          End ! If Access:DEFEMAB3.TryInsert() = Level:Benign
      End !If Access:DEFEMAB3.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEB3.Next()

  Set(DEFEMAB4)
  If Access:DEFEMAB4.Next()
      If Access:DEFEMAB4.PrimeRecord() = Level:Benign
          If Access:DEFEMAB4.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAB4.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAB4.CancelAutoInc()
          End ! If Access:DEFEMAB4.TryInsert() = Level:Benign
      End !If Access:DEFEMAB4.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEB4.Next()

  Set(DEFEMAB5)
  If Access:DEFEMAB5.Next()
      If Access:DEFEMAB5.PrimeRecord() = Level:Benign
          If Access:DEFEMAB5.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAB5.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAB5.CancelAutoInc()
          End ! If Access:DEFEMAB5.TryInsert() = Level:Benign
      End !If Access:DEFEMAB5.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEB5.Next()
  
  Set(DEFEMAB6)
  If Access:DEFEMAB6.Next()
      If Access:DEFEMAB6.PrimeRecord() = Level:Benign
          If Access:DEFEMAB6.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAB6.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAB6.CancelAutoInc()
          End ! If Access:DEFEMAB6.TryInsert() = Level:Benign
      End !If Access:DEFEMAB6.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEB6.Next()

  Set(DEFEMAB7)
  If Access:DEFEMAB7.Next()
      If Access:DEFEMAB7.PrimeRecord() = Level:Benign
          If Access:DEFEMAB7.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAB7.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAB7.CancelAutoInc()
          End ! If Access:DEFEMAB7.TryInsert() = Level:Benign
      End !If Access:DEFEMAB7.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAEB7.Next()

  Set(DEFEMAR1)
  If Access:DEFEMAR1.Next()
      If Access:DEFEMAR1.PrimeRecord() = Level:Benign
          If Access:DEFEMAR1.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAR1.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAR1.CancelAutoInc()
          End ! If Access:DEFEMAR1.TryInsert() = Level:Benign
      End !If Access:DEFEMAR1.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAR1.Next()

  Set(DEFEMAR2)
  If Access:DEFEMAR2.Next()
      If Access:DEFEMAR2.PrimeRecord() = Level:Benign
          If Access:DEFEMAR2.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAR2.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAR2.CancelAutoInc()
          End ! If Access:DEFEMAR2.TryInsert() = Level:Benign
      End !If Access:DEFEMAR2.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAR2.Next()

  Set(DEFEMAR3)
  If Access:DEFEMAR3.Next()
      If Access:DEFEMAR3.PrimeRecord() = Level:Benign
          If Access:DEFEMAR3.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAR3.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAR3.CancelAutoInc()
          End ! If Access:DEFEMAR3.TryInsert() = Level:Benign
      End !If Access:DEFEMAR3.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAR3.Next()

  Set(DEFEMAR4)
  If Access:DEFEMAR4.Next()
      If Access:DEFEMAR4.PrimeRecord() = Level:Benign
          If Access:DEFEMAR4.TryInsert() = Level:Benign
              ! Insert Successful
          Else ! If Access:DEFEMAR4.TryInsert() = Level:Benign
              ! Insert Failed
              Access:DEFEMAR4.CancelAutoInc()
          End ! If Access:DEFEMAR4.TryInsert() = Level:Benign
      End !If Access:DEFEMAR4.PrimeRecord() = Level:Benign
  End ! If Access:DEFEMAR4.Next()
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFEMAB1.Close
    Relate:DEFEMAB2.Close
    Relate:DEFEMAB3.Close
    Relate:DEFEMAB4.Close
    Relate:DEFEMAB5.Close
    Relate:DEFEMAB6.Close
    Relate:DEFEMAB7.Close
    Relate:DEFEMAE1.Close
    Relate:DEFEMAE2.Close
    Relate:DEFEMAE3.Close
    Relate:DEFEMAI2.Close
    Relate:DEFEMAIL.Close
    Relate:DEFEMAR1.Close
    Relate:DEFEMAR2.Close
    Relate:DEFEMAR3.Close
    Relate:DEFEMAR4.Close
    Relate:DEFEME2B.Close
    Relate:DEFEME3B.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:DefEmail.update()
      Access:DEFEMAI2.Update()
      Access:DEFEMAE1.Update()
      Access:DEFEMAE2.Update()
      Access:DEFEMAE3.Update()
      Access:DEFEMAB1.Update()
      Access:DEFEMAB2.Update()
      Access:DEFEMAB3.Update()
      Access:DEFEMAB4.Update()
      Access:DEFEMAB5.Update()
      Access:DEFEMAB6.Update()
      Access:DEFEMAB7.Update()
      Access:DEFEME2B.Update()
      Access:DEFEME3B.Update()
      Access:DEFEMAR1.Update()
      Access:DEFEMAR2.Update()
      Access:DEFEMAR3.Update()
      Access:DEFEMAR4.Update()
      
      
      
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

