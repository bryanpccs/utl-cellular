

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01030.INC'),ONCE        !Local module procedure declarations
                     END


RetailSalesDefaults PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::def:Record  LIKE(def:RECORD),STATIC
QuickWindow          WINDOW('Retail Sales Defaults'),AT(,,218,84),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('RetailSalesDefaults'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,48),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           CHECK('Retail Back Orders    '),AT(8,20),USE(def:RetBackOrders),LEFT,MSG('Retail Back Orders'),TIP('Retail Back Orders'),VALUE('1','0')
                           PROMPT('Euro Exchange Rate'),AT(8,36),USE(?def:EuroRate:Prompt),TRN
                           ENTRY(@n23.11),AT(84,36,64,12),USE(def:EuroRate),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Euro Exchange Rate'),TIP('Euro Exchange Rate'),UPR
                           STRING('(Euros to Sterling)'),AT(152,37),USE(?String1)
                         END
                       END
                       PANEL,AT(4,56,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,60,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(156,60,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?def:RetBackOrders{prop:Font,3} = -1
    ?def:RetBackOrders{prop:Color} = 15066597
    ?def:RetBackOrders{prop:Trn} = 0
    ?def:EuroRate:Prompt{prop:FontColor} = -1
    ?def:EuroRate:Prompt{prop:Color} = 15066597
    If ?def:EuroRate{prop:ReadOnly} = True
        ?def:EuroRate{prop:FontColor} = 65793
        ?def:EuroRate{prop:Color} = 15066597
    Elsif ?def:EuroRate{prop:Req} = True
        ?def:EuroRate{prop:FontColor} = 65793
        ?def:EuroRate{prop:Color} = 8454143
    Else ! If ?def:EuroRate{prop:Req} = True
        ?def:EuroRate{prop:FontColor} = 65793
        ?def:EuroRate{prop:Color} = 16777215
    End ! If ?def:EuroRate{prop:Req} = True
    ?def:EuroRate{prop:Trn} = 0
    ?def:EuroRate{prop:FontStyle} = font:Bold
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Retail Sales Defaults'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Open
    SET(DEFAULTS)
    CASE Access:DEFAULTS.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:DEFAULTS.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('RetailSalesDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?def:RetBackOrders
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(def:Record,History::def:Record)
  SELF.AddHistoryField(?def:RetBackOrders,126)
  SELF.AddHistoryField(?def:EuroRate,129)
  SELF.AddUpdateFile(Access:DEFAULTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFAULTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(DEFAULTS)
      Access:DEFAULTS.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

