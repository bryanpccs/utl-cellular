

   MEMBER('sbc01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBC01011.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Standard_Charges PROCEDURE                     !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
FilesOpened          BYTE
Model_Number_Temp    STRING(30)
tmp:manufacturer     STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Model_Number_Temp
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tmp:manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STDCHRGE)
                       PROJECT(sta:Charge_Type)
                       PROJECT(sta:Unit_Type)
                       PROJECT(sta:Repair_Type)
                       PROJECT(sta:Cost)
                       PROJECT(sta:Model_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sta:Charge_Type        LIKE(sta:Charge_Type)          !List box control field - type derived from field
sta:Unit_Type          LIKE(sta:Unit_Type)            !List box control field - type derived from field
sta:Repair_Type        LIKE(sta:Repair_Type)          !List box control field - type derived from field
sta:Cost               LIKE(sta:Cost)                 !List box control field - type derived from field
sta:Model_Number       LIKE(sta:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
FDCB8::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
QuickWindow          WINDOW('Browse The Standard Charge File'),AT(,,484,256),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Standard_Charges'),SYSTEM,GRAY,RESIZE
                       PANEL,AT(4,4,392,36),USE(?Panel1),FILL(COLOR:Gray)
                       COMBO(@s30),AT(68,8,124,10),USE(tmp:manufacturer),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                       PROMPT('Model Number'),AT(8,24),USE(?Prompt1),FONT(,,COLOR:White,)
                       PROMPT('Manufacturer'),AT(8,8),USE(?Prompt1:2),FONT(,,COLOR:White,)
                       COMBO(@s30),AT(68,24,124,10),USE(Model_Number_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       BUTTON('Refresh List'),AT(196,24,69,12),USE(?Button6),LEFT,ICON('Spy.gif')
                       LIST,AT(8,76,384,172),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('126L(2)|M~Charge Type~@s30@104L(2)|M~Unit Type~@s30@100L(2)|M~Repair Type~@s30@6' &|
   '0R(2)|M~Cost~@n14.2@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(400,156,76,20),USE(?Insert:2),LEFT,ICON('Insert.ico')
                       BUTTON('&Change'),AT(400,180,76,20),USE(?Change:2),LEFT,ICON('Edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(400,204,76,20),USE(?Delete:2),LEFT,ICON('Delete.ico')
                       SHEET,AT(4,44,392,208),USE(?CurrentTab),SPREAD
                         TAB('By Charge Type'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,60,124,10),USE(sta:Charge_Type),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Unit Type'),USE(?Tab2)
                           ENTRY(@s30),AT(8,60,124,10),USE(sta:Unit_Type),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Repair Type'),USE(?Tab3)
                           ENTRY(@s30),AT(8,60,124,10),USE(sta:Repair_Type),LEFT,FONT('Tahoma',8,,FONT:bold),MSG('q'),TIP('q'),UPR
                         END
                         TAB('By Cost'),USE(?Tab4)
                           ENTRY(@n14.2),AT(8,60,124,10),USE(sta:Cost),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('Close'),AT(400,232,76,20),USE(?Close),LEFT,ICON('Cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  FilterLocatorClass               !Default Locator
BRW1::Sort1:Locator  FilterLocatorClass               !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort2:Locator  FilterLocatorClass               !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1{prop:Fill} = 15066597

    If ?tmp:manufacturer{prop:ReadOnly} = True
        ?tmp:manufacturer{prop:FontColor} = 65793
        ?tmp:manufacturer{prop:Color} = 15066597
    Elsif ?tmp:manufacturer{prop:Req} = True
        ?tmp:manufacturer{prop:FontColor} = 65793
        ?tmp:manufacturer{prop:Color} = 8454143
    Else ! If ?tmp:manufacturer{prop:Req} = True
        ?tmp:manufacturer{prop:FontColor} = 65793
        ?tmp:manufacturer{prop:Color} = 16777215
    End ! If ?tmp:manufacturer{prop:Req} = True
    ?tmp:manufacturer{prop:Trn} = 0
    ?tmp:manufacturer{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    If ?Model_Number_Temp{prop:ReadOnly} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 15066597
    Elsif ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 8454143
    Else ! If ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 16777215
    End ! If ?Model_Number_Temp{prop:Req} = True
    ?Model_Number_Temp{prop:Trn} = 0
    ?Model_Number_Temp{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?sta:Charge_Type{prop:ReadOnly} = True
        ?sta:Charge_Type{prop:FontColor} = 65793
        ?sta:Charge_Type{prop:Color} = 15066597
    Elsif ?sta:Charge_Type{prop:Req} = True
        ?sta:Charge_Type{prop:FontColor} = 65793
        ?sta:Charge_Type{prop:Color} = 8454143
    Else ! If ?sta:Charge_Type{prop:Req} = True
        ?sta:Charge_Type{prop:FontColor} = 65793
        ?sta:Charge_Type{prop:Color} = 16777215
    End ! If ?sta:Charge_Type{prop:Req} = True
    ?sta:Charge_Type{prop:Trn} = 0
    ?sta:Charge_Type{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?sta:Unit_Type{prop:ReadOnly} = True
        ?sta:Unit_Type{prop:FontColor} = 65793
        ?sta:Unit_Type{prop:Color} = 15066597
    Elsif ?sta:Unit_Type{prop:Req} = True
        ?sta:Unit_Type{prop:FontColor} = 65793
        ?sta:Unit_Type{prop:Color} = 8454143
    Else ! If ?sta:Unit_Type{prop:Req} = True
        ?sta:Unit_Type{prop:FontColor} = 65793
        ?sta:Unit_Type{prop:Color} = 16777215
    End ! If ?sta:Unit_Type{prop:Req} = True
    ?sta:Unit_Type{prop:Trn} = 0
    ?sta:Unit_Type{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?sta:Repair_Type{prop:ReadOnly} = True
        ?sta:Repair_Type{prop:FontColor} = 65793
        ?sta:Repair_Type{prop:Color} = 15066597
    Elsif ?sta:Repair_Type{prop:Req} = True
        ?sta:Repair_Type{prop:FontColor} = 65793
        ?sta:Repair_Type{prop:Color} = 8454143
    Else ! If ?sta:Repair_Type{prop:Req} = True
        ?sta:Repair_Type{prop:FontColor} = 65793
        ?sta:Repair_Type{prop:Color} = 16777215
    End ! If ?sta:Repair_Type{prop:Req} = True
    ?sta:Repair_Type{prop:Trn} = 0
    ?sta:Repair_Type{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    If ?sta:Cost{prop:ReadOnly} = True
        ?sta:Cost{prop:FontColor} = 65793
        ?sta:Cost{prop:Color} = 15066597
    Elsif ?sta:Cost{prop:Req} = True
        ?sta:Cost{prop:FontColor} = 65793
        ?sta:Cost{prop:Color} = 8454143
    Else ! If ?sta:Cost{prop:Req} = True
        ?sta:Cost{prop:FontColor} = 65793
        ?sta:Cost{prop:Color} = 16777215
    End ! If ?sta:Cost{prop:Req} = True
    ?sta:Cost{prop:Trn} = 0
    ?sta:Cost{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Standard_Charges')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Standard_Charges'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STDCHRGE,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,sta:Unit_Type_Key)
  BRW1.AddRange(sta:Model_Number,Model_Number_Temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?sta:Unit_Type,sta:Unit_Type,1,BRW1)
  BRW1::Sort1:Locator.FloatRight = 1
  BRW1.AddSortOrder(,sta:Repair_Type_Key)
  BRW1.AddRange(sta:Model_Number,Model_Number_Temp)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?sta:Repair_Type,sta:Repair_Type,1,BRW1)
  BRW1::Sort2:Locator.FloatRight = 1
  BRW1.AddSortOrder(,sta:Cost_Key)
  BRW1.AddRange(sta:Model_Number,Model_Number_Temp)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?sta:Cost,sta:Cost,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sta:Charge_Type_Key)
  BRW1.AddRange(sta:Model_Number,Model_Number_Temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sta:Charge_Type,sta:Charge_Type,1,BRW1)
  BRW1::Sort0:Locator.FloatRight = 1
  BIND('Model_Number_Temp',Model_Number_Temp)
  BRW1.AddField(sta:Charge_Type,BRW1.Q.sta:Charge_Type)
  BRW1.AddField(sta:Unit_Type,BRW1.Q.sta:Unit_Type)
  BRW1.AddField(sta:Repair_Type,BRW1.Q.sta:Repair_Type)
  BRW1.AddField(sta:Cost,BRW1.Q.sta:Cost)
  BRW1.AddField(sta:Model_Number,BRW1.Q.sta:Model_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB5.Init(Model_Number_Temp,?Model_Number_Temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(mod:Manufacturer_Key)
  FDCB5.AddRange(mod:Manufacturer,tmp:manufacturer)
  FDCB5.AddField(mod:Model_Number,FDCB5.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB8.Init(tmp:manufacturer,?tmp:manufacturer,Queue:FileDropCombo:1.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:1
  FDCB8.AddSortOrder(man:Manufacturer_Key)
  FDCB8.AddField(man:Manufacturer,FDCB8.Q.man:Manufacturer)
  FDCB8.AddField(man:RecordNumber,FDCB8.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Charge Type'
    ?Tab2{PROP:TEXT} = 'By Unit Type'
    ?Tab3{PROP:TEXT} = 'By Repair Type'
    ?Tab4{PROP:TEXT} = 'By Cost'
    ?Browse:1{PROP:FORMAT} ='126L(2)|M~Charge Type~@s30@#1#104L(2)|M~Unit Type~@s30@#2#100L(2)|M~Repair Type~@s30@#3#60R(2)|M~Cost~@n14.2@#4#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Standard_Charges'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSTDCHRGE
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:manufacturer, Accepted)
      FDCB5.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:manufacturer, Accepted)
    OF ?Model_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
    OF ?Button6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='126L(2)|M~Charge Type~@s30@#1#104L(2)|M~Unit Type~@s30@#2#100L(2)|M~Repair Type~@s30@#3#60R(2)|M~Cost~@n14.2@#4#'
          ?Tab:2{PROP:TEXT} = 'By Charge Type'
        OF 2
          ?Browse:1{PROP:FORMAT} ='104L(2)|M~Unit Type~@s30@#2#126L(2)|M~Charge Type~@s30@#1#100L(2)|M~Repair Type~@s30@#3#60R(2)|M~Cost~@n14.2@#4#'
          ?Tab2{PROP:TEXT} = 'By Unit Type'
        OF 3
          ?Browse:1{PROP:FORMAT} ='100L(2)|M~Repair Type~@s30@#3#126L(2)|M~Charge Type~@s30@#1#104L(2)|M~Unit Type~@s30@#2#60R(2)|M~Cost~@n14.2@#4#'
          ?Tab3{PROP:TEXT} = 'By Repair Type'
        OF 4
          ?Browse:1{PROP:FORMAT} ='60R(2)|M~Cost~@n14.2@#4#126L(2)|M~Charge Type~@s30@#1#104L(2)|M~Unit Type~@s30@#2#100L(2)|M~Repair Type~@s30@#3#'
          ?Tab4{PROP:TEXT} = 'By Cost'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseDown
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseDown)
      glo:select1  = ''
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseDown)
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      model_number_temp   = glo:select1
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

