

   MEMBER('cellular.clw')                             ! This is a MEMBER module


!------------ File: CARISMA, version 1 ----------------------
!------------ modified 25.09.2006 at 15:24:49 -----------------
MOD:stFileName:CARISMAVer1  STRING(260)
CARISMAVer1            FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:CARISMAVer1),PRE(CARISMAVer1)
RecordNumberKey          KEY(CARISMAVer1:RecordNumber),NOCASE,PRIMARY
ManufactModColourKey     KEY(CARISMAVer1:Manufacturer,CARISMAVer1:ModelNo,CARISMAVer1:Colour),DUP,NOCASE
CarismaCodeKey           KEY(CARISMAVer1:CarismaCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
Colour                      STRING(30)
CarismaCode                 STRING(30)
                         END
                       END
!------------ End File: CARISMA, version 1 ------------------

!------------ File: CARISMA, version 2 ----------------------
!------------ modified 18.10.2006 at 10:44:22 -----------------
MOD:stFileName:CARISMAVer2  STRING(260)
CARISMAVer2            FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:CARISMAVer2),PRE(CARISMAVer2),CREATE
RecordNumberKey          KEY(CARISMAVer2:RecordNumber),NOCASE,PRIMARY
ManufactModColourKey     KEY(CARISMAVer2:Manufacturer,CARISMAVer2:ModelNo,CARISMAVer2:Colour),DUP,NOCASE
ContractCodeKey          KEY(CARISMAVer2:ContractOracleCode),DUP,NOCASE
PAYTCodeKey              KEY(CARISMAVer2:PAYTOracleCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
Colour                      STRING(30)
ContractOracleCode          STRING(30)
PAYTOracleCode              STRING(30)
                         END
                       END
!------------ End File: CARISMA, version 2 ------------------

!------------ File: DEFEMAIL, version 1 ----------------------
!------------ modified 26.01.2005 at 11:54:30 -----------------
MOD:stFileName:DEFEMAILVer1  STRING(260)
DEFEMAILVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAILVer1),PRE(DEFEMAILVer1)
RecordNumberKey          KEY(DEFEMAILVer1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
SMSToAddress                STRING(255)
EmailToAddress              STRING(255)
AdminToAddress              STRING(255)
DespFromSCText              STRING(160)
ArrivedAtRetailText         STRING(160)
                         END
                       END
!------------ End File: DEFEMAIL, version 1 ------------------

!------------ File: DEFEMAIL, version 2 ----------------------
!------------ modified 12.06.2006 at 09:46:35 -----------------
MOD:stFileName:DEFEMAILVer2  STRING(260)
DEFEMAILVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAILVer2),PRE(DEFEMAILVer2),CREATE
RecordNumberKey          KEY(DEFEMAILVer2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
SMSToAddress                STRING(255)
EmailToAddress              STRING(255)
AdminToAddress              STRING(255)
DespFromSCText              STRING(160)
ArrivedAtRetailText         STRING(160)
SMSFTPAddress               STRING(30)
SMSFTPUsername              STRING(30)
SMSFTPPassword              STRING(30)
SMSFTPLocation              STRING(255)
                         END
                       END
!------------ End File: DEFEMAIL, version 2 ------------------

!------------ File: DEFEMAIL, version 3 ----------------------
!------------ modified 09.10.2006 at 11:30:18 -----------------
MOD:stFileName:DEFEMAILVer3  STRING(260)
DEFEMAILVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAILVer3),PRE(DEFEMAILVer3),CREATE
RecordNumberKey          KEY(DEFEMAILVer3:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
SMSToAddress                STRING(255)
EmailToAddress              STRING(255)
AdminToAddress              STRING(255)
DespFromSCText              STRING(480)
ArrivedAtRetailText         STRING(480)
SMSFTPAddress               STRING(30)
SMSFTPUsername              STRING(30)
SMSFTPPassword              STRING(30)
SMSFTPLocation              STRING(255)
R1Active                    BYTE
R2Active                    BYTE
R3Active                    BYTE
R1SMSActive                 BYTE
R2SMSActive                 BYTE
R3SMSActive                 BYTE
BookedRetailText            STRING(480)
                         END
                       END
!------------ End File: DEFEMAIL, version 3 ------------------

!------------ File: REGIONS, version 1 ----------------------
!------------ modified 16.08.2004 at 11:24:19 -----------------
MOD:stFileName:REGIONSVer1  STRING(260)
REGIONSVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:REGIONSVer1),PRE(REGIONSVer1)
RegionIDKey              KEY(REGIONSVer1:RegionID),NOCASE,PRIMARY
RegionNameKey            KEY(REGIONSVer1:RegionName),NOCASE
Record                   RECORD,PRE()
RegionID                    LONG
RegionName                  STRING(30)
                         END
                       END
!------------ End File: REGIONS, version 1 ------------------

!------------ File: REGIONS, version 2 ----------------------
!------------ modified 20.04.2005 at 13:54:25 -----------------
MOD:stFileName:REGIONSVer2  STRING(260)
REGIONSVer2            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:REGIONSVer2),PRE(REGIONSVer2),CREATE
RegionIDKey              KEY(REGIONSVer2:RegionID),NOCASE,PRIMARY
RegionNameKey            KEY(REGIONSVer2:RegionName),NOCASE
Record                   RECORD,PRE()
RegionID                    LONG
RegionName                  STRING(30)
BankHolidaysFileName        STRING(100)
TransitDaysToSC             LONG
TransitDaysToStore          LONG
                         END
                       END
!------------ End File: REGIONS, version 2 ------------------

!------------ File: ACCREG, version 1 ----------------------
!------------ modified 16.08.2004 at 11:24:19 -----------------
MOD:stFileName:ACCREGVer1  STRING(260)
ACCREGVer1             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:ACCREGVer1),PRE(ACCREGVer1)
AccRegIDKey              KEY(ACCREGVer1:AccRegID),NOCASE,PRIMARY
AccountNoKey             KEY(ACCREGVer1:AccountNo),NOCASE
RegionNameKey            KEY(ACCREGVer1:RegionName,ACCREGVer1:AccountNo),DUP,NOCASE
Record                   RECORD,PRE()
AccRegID                    LONG
AccountNo                   STRING(15)
RegionName                  STRING(30)
                         END
                       END
!------------ End File: ACCREG, version 1 ------------------

!------------ File: ACCREG, version 2 ----------------------
!------------ modified 20.04.2005 at 13:54:25 -----------------
MOD:stFileName:ACCREGVer2  STRING(260)
ACCREGVer2             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:ACCREGVer2),PRE(ACCREGVer2),CREATE
AccRegIDKey              KEY(ACCREGVer2:AccRegID),NOCASE,PRIMARY
AccountNoKey             KEY(ACCREGVer2:AccountNo),NOCASE
RegionNameKey            KEY(ACCREGVer2:RegionName,ACCREGVer2:AccountNo),DUP,NOCASE
Record                   RECORD,PRE()
AccRegID                    LONG
AccountNo                   STRING(15)
RegionName                  STRING(30)
TransitDaysException        BYTE
TransitDaysToSC             LONG
TransitDaysToStore          LONG
                         END
                       END
!------------ End File: ACCREG, version 2 ------------------

!------------ File: SIDACREG, version 1 ----------------------
!------------ modified 05.09.2006 at 10:43:48 -----------------
MOD:stFileName:SIDACREGVer1  STRING(260)
SIDACREGVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SIDACREGVer1),PRE(SIDACREGVer1)
RecordNumberKey          KEY(SIDACREGVer1:RecordNumber),NOCASE,PRIMARY
SCAccRegIDKey            KEY(SIDACREGVer1:SCAccountID,SIDACREGVer1:AccRegID),NOCASE
AccRegIDKey              KEY(SIDACREGVer1:AccRegID),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SCAccountID                 LONG
AccRegID                    LONG
TransitDaysException        BYTE
TransitDaysToSC             LONG
TransitDaysToStore          LONG
                         END
                       END
!------------ End File: SIDACREG, version 1 ------------------

!------------ File: XMLJOBS, version 1 ----------------------
!------------ modified 03.08.2004 at 10:44:45 -----------------
MOD:stFileName:XMLJOBSVer1  STRING(260)
XMLJOBSVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer1),PRE(XMLJOBSVer1)
RecordNumberKey          KEY(XMLJOBSVer1:RECORD_NUMBER),NOCASE,PRIMARY
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
COMPANY                     STRING(4)
JOB_NO                      LONG
TR_NO                       STRING(20)
ASC_CODE                    STRING(30)
MODEL_CODE                  STRING(30)
SERIAL_NO                   STRING(20)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_LAST_NAME          STRING(30)
CONSUMER_TEL_NUMBER1        STRING(15)
                         END
                       END
!------------ End File: XMLJOBS, version 1 ------------------

!------------ File: XMLJOBS, version 2 ----------------------
!------------ modified 04.08.2004 at 11:34:43 -----------------
MOD:stFileName:XMLJOBSVer2  STRING(260)
XMLJOBSVer2            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer2),PRE(XMLJOBSVer2),CREATE
RecordNumberKey          KEY(XMLJOBSVer2:RECORD_NUMBER),NOCASE,PRIMARY
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
COMPANY                     STRING(4)
JOB_NO                      LONG
TR_NO                       STRING(20)
ASC_CODE                    STRING(30)
MODEL_CODE                  STRING(30)
SERIAL_NO                   STRING(20)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_LAST_NAME          STRING(30)
CONSUMER_TEL_NUMBER1        STRING(15)
CONSUMER_POSTCODE           STRING(10)
                         END
                       END
!------------ End File: XMLJOBS, version 2 ------------------

!------------ File: XMLJOBS, version 3 ----------------------
!------------ modified 04.08.2004 at 12:01:18 -----------------
MOD:stFileName:XMLJOBSVer3  STRING(260)
XMLJOBSVer3            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer3),PRE(XMLJOBSVer3),CREATE
RecordNumberKey          KEY(XMLJOBSVer3:RECORD_NUMBER),NOCASE,PRIMARY
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
COMPANY                     STRING(4)
JOB_NO                      LONG
TR_NO                       STRING(20)
ASC_CODE                    STRING(30)
MODEL_CODE                  STRING(30)
SERIAL_NO                   STRING(20)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_LAST_NAME          STRING(30)
CONSUMER_TEL_NUMBER1        STRING(15)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_HOUSE_NUMBER       STRING(20)
CONSUMER_STREET             STRING(30)
                         END
                       END
!------------ End File: XMLJOBS, version 3 ------------------

!------------ File: XMLJOBS, version 4 ----------------------
!------------ modified 04.08.2004 at 16:13:21 -----------------
MOD:stFileName:XMLJOBSVer4  STRING(260)
XMLJOBSVer4            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer4),PRE(XMLJOBSVer4),CREATE
RecordNumberKey          KEY(XMLJOBSVer4:RECORD_NUMBER),NOCASE,PRIMARY
State_Job_Number_Key     KEY(XMLJOBSVer4:RECORD_STATE,XMLJOBSVer4:JOB_NO),DUP,NOCASE,OPT
Surname_Key              KEY(XMLJOBSVer4:CONSUMER_LAST_NAME),DUP,NOCASE
Postcode_Key             KEY(XMLJOBSVer4:CONSUMER_POSTCODE),DUP,NOCASE
Model_Key                KEY(XMLJOBSVer4:MODEL_CODE),DUP,NOCASE
Serial_No_Key            KEY(XMLJOBSVer4:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
COMPANY                     STRING(4)
JOB_NO                      LONG
TR_NO                       STRING(20)
ASC_CODE                    STRING(30)
MODEL_CODE                  STRING(30)
SERIAL_NO                   STRING(20)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_LAST_NAME          STRING(30)
CONSUMER_TEL_NUMBER1        STRING(15)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_HOUSE_NUMBER       STRING(20)
CONSUMER_STREET             STRING(30)
                         END
                       END
!------------ End File: XMLJOBS, version 4 ------------------

!------------ File: XMLJOBS, version 5 ----------------------
!------------ modified 05.08.2004 at 09:20:54 -----------------
MOD:stFileName:XMLJOBSVer5  STRING(260)
XMLJOBSVer5            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer5),PRE(XMLJOBSVer5),CREATE
RecordNumberKey          KEY(XMLJOBSVer5:RECORD_NUMBER),NOCASE,PRIMARY
State_Job_Number_Key     KEY(XMLJOBSVer5:RECORD_STATE,XMLJOBSVer5:JOB_NO),DUP,NOCASE,OPT
Surname_Key              KEY(XMLJOBSVer5:CONSUMER_LAST_NAME),DUP,NOCASE
Postcode_Key             KEY(XMLJOBSVer5:CONSUMER_POSTCODE),DUP,NOCASE
Model_Key                KEY(XMLJOBSVer5:MODEL_CODE),DUP,NOCASE
Serial_No_Key            KEY(XMLJOBSVer5:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
COMPANY                     STRING(4)
JOB_NO                      LONG
TR_NO                       STRING(20)
ASC_CODE                    STRING(30)
MODEL_CODE                  STRING(30)
SERIAL_NO                   STRING(20)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_LAST_NAME          STRING(30)
CONSUMER_TEL_NUMBER1        STRING(15)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_HOUSE_NUMBER       STRING(20)
CONSUMER_STREET             STRING(30)
                         END
                       END
!------------ End File: XMLJOBS, version 5 ------------------

!------------ File: XMLJOBS, version 6 ----------------------
!------------ modified 05.08.2004 at 15:49:03 -----------------
MOD:stFileName:XMLJOBSVer6  STRING(260)
XMLJOBSVer6            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer6),PRE(XMLJOBSVer6),CREATE
RecordNumberKey          KEY(XMLJOBSVer6:RECORD_NUMBER),NOCASE,PRIMARY
JobNumberKey             KEY(XMLJOBSVer6:JOB_NO),DUP,NOCASE,OPT
State_Job_Number_Key     KEY(XMLJOBSVer6:RECORD_STATE,XMLJOBSVer6:JOB_NO),DUP,NOCASE,OPT
Surname_Key              KEY(XMLJOBSVer6:CONSUMER_LAST_NAME),DUP,NOCASE
Postcode_Key             KEY(XMLJOBSVer6:CONSUMER_POSTCODE),DUP,NOCASE
Model_Key                KEY(XMLJOBSVer6:MODEL_CODE),DUP,NOCASE
Serial_No_Key            KEY(XMLJOBSVer6:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
COMPANY                     STRING(4)
JOB_NO                      LONG
TR_NO                       STRING(20)
ASC_CODE                    STRING(30)
MODEL_CODE                  STRING(30)
SERIAL_NO                   STRING(20)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_LAST_NAME          STRING(30)
CONSUMER_TEL_NUMBER1        STRING(15)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_HOUSE_NUMBER       STRING(20)
CONSUMER_STREET             STRING(30)
                         END
                       END
!------------ End File: XMLJOBS, version 6 ------------------

!------------ File: XMLJOBS, version 7 ----------------------
!------------ modified 06.08.2004 at 10:18:20 -----------------
MOD:stFileName:XMLJOBSVer7  STRING(260)
XMLJOBSVer7            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer7),PRE(XMLJOBSVer7),CREATE
RecordNumberKey          KEY(XMLJOBSVer7:RECORD_NUMBER),NOCASE,PRIMARY
JobNumberKey             KEY(XMLJOBSVer7:JOB_NO),DUP,NOCASE,OPT
State_Job_Number_Key     KEY(XMLJOBSVer7:RECORD_STATE,XMLJOBSVer7:JOB_NO),DUP,NOCASE,OPT
Surname_Key              KEY(XMLJOBSVer7:CONSUMER_LAST_NAME),DUP,NOCASE
Postcode_Key             KEY(XMLJOBSVer7:CONSUMER_POSTCODE),DUP,NOCASE
Model_Key                KEY(XMLJOBSVer7:MODEL_CODE),DUP,NOCASE
Serial_No_Key            KEY(XMLJOBSVer7:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
                         END
                       END
!------------ End File: XMLJOBS, version 7 ------------------

!------------ File: XMLJOBS, version 8 ----------------------
!------------ modified 06.08.2004 at 10:42:27 -----------------
MOD:stFileName:XMLJOBSVer8  STRING(260)
XMLJOBSVer8            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer8),PRE(XMLJOBSVer8),CREATE
RecordNumberKey          KEY(XMLJOBSVer8:RECORD_NUMBER),NOCASE,PRIMARY
JobNumberKey             KEY(XMLJOBSVer8:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XMLJOBSVer8:RECORD_STATE,XMLJOBSVer8:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XMLJOBSVer8:RECORD_STATE,XMLJOBSVer8:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XMLJOBSVer8:RECORD_STATE,XMLJOBSVer8:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XMLJOBSVer8:RECORD_STATE,XMLJOBSVer8:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XMLJOBSVer8:RECORD_STATE,XMLJOBSVer8:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
                         END
                       END
!------------ End File: XMLJOBS, version 8 ------------------

!------------ File: XMLJOBS, version 9 ----------------------
!------------ modified 13.08.2004 at 09:02:44 -----------------
MOD:stFileName:XMLJOBSVer9  STRING(260)
XMLJOBSVer9            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer9),PRE(XMLJOBSVer9),CREATE
RecordNumberKey          KEY(XMLJOBSVer9:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XMLJOBSVer9:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XMLJOBSVer9:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XMLJOBSVer9:RECORD_STATE,XMLJOBSVer9:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XMLJOBSVer9:RECORD_STATE,XMLJOBSVer9:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XMLJOBSVer9:RECORD_STATE,XMLJOBSVer9:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XMLJOBSVer9:RECORD_STATE,XMLJOBSVer9:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XMLJOBSVer9:RECORD_STATE,XMLJOBSVer9:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
                         END
                       END
!------------ End File: XMLJOBS, version 9 ------------------

!------------ File: XMLJOBS, version 10 ----------------------
!------------ modified 13.08.2004 at 14:49:51 -----------------
MOD:stFileName:XMLJOBSVer10  STRING(260)
XMLJOBSVer10           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer10),PRE(XMLJOBSVer10),CREATE
RecordNumberKey          KEY(XMLJOBSVer10:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XMLJOBSVer10:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XMLJOBSVer10:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XMLJOBSVer10:RECORD_STATE,XMLJOBSVer10:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XMLJOBSVer10:RECORD_STATE,XMLJOBSVer10:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XMLJOBSVer10:RECORD_STATE,XMLJOBSVer10:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XMLJOBSVer10:RECORD_STATE,XMLJOBSVer10:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XMLJOBSVer10:RECORD_STATE,XMLJOBSVer10:SERIAL_NO),DUP,NOCASE
EXCEPTION_DESC              MEMO(1000)
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
                         END
                       END
!------------ End File: XMLJOBS, version 10 ------------------

!------------ File: XMLJOBS, version 11 ----------------------
!------------ modified 13.08.2004 at 15:10:17 -----------------
MOD:stFileName:XMLJOBSVer11  STRING(260)
XMLJOBSVer11           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer11),PRE(XMLJOBSVer11),CREATE
RecordNumberKey          KEY(XMLJOBSVer11:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XMLJOBSVer11:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XMLJOBSVer11:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XMLJOBSVer11:RECORD_STATE,XMLJOBSVer11:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XMLJOBSVer11:RECORD_STATE,XMLJOBSVer11:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XMLJOBSVer11:RECORD_STATE,XMLJOBSVer11:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XMLJOBSVer11:RECORD_STATE,XMLJOBSVer11:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XMLJOBSVer11:RECORD_STATE,XMLJOBSVer11:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
EXCEPTION_DESC              STRING(255)
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
                         END
                       END
!------------ End File: XMLJOBS, version 11 ------------------

!------------ File: XMLJOBS, version 12 ----------------------
!------------ modified 19.08.2004 at 15:12:56 -----------------
MOD:stFileName:XMLJOBSVer12  STRING(260)
XMLJOBSVer12           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer12),PRE(XMLJOBSVer12),CREATE
RecordNumberKey          KEY(XMLJOBSVer12:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XMLJOBSVer12:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XMLJOBSVer12:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XMLJOBSVer12:RECORD_STATE,XMLJOBSVer12:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XMLJOBSVer12:RECORD_STATE,XMLJOBSVer12:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XMLJOBSVer12:RECORD_STATE,XMLJOBSVer12:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XMLJOBSVer12:RECORD_STATE,XMLJOBSVer12:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XMLJOBSVer12:RECORD_STATE,XMLJOBSVer12:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
EXCEPTION_DESC              STRING(255)
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
TR_STATUS_DESC              STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
                         END
                       END
!------------ End File: XMLJOBS, version 12 ------------------

!------------ File: XMLJOBS, version 13 ----------------------
!------------ modified 19.08.2004 at 16:09:47 -----------------
MOD:stFileName:XMLJOBSVer13  STRING(260)
XMLJOBSVer13           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer13),PRE(XMLJOBSVer13),CREATE
RecordNumberKey          KEY(XMLJOBSVer13:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XMLJOBSVer13:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XMLJOBSVer13:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XMLJOBSVer13:RECORD_STATE,XMLJOBSVer13:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XMLJOBSVer13:RECORD_STATE,XMLJOBSVer13:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XMLJOBSVer13:RECORD_STATE,XMLJOBSVer13:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XMLJOBSVer13:RECORD_STATE,XMLJOBSVer13:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XMLJOBSVer13:RECORD_STATE,XMLJOBSVer13:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
EXCEPTION_DESC              STRING(255)
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
TR_REASON                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
REPAIR_COMPLETE_DATE        DATE
REPAIR_COMPLETE_TIME        TIME
GOODS_DELIVERY_DATE         DATE
GOODS_DELIVERY_TIME         TIME
                         END
                       END
!------------ End File: XMLJOBS, version 13 ------------------

!------------ File: XMLJOBS, version 14 ----------------------
!------------ modified 20.08.2004 at 10:04:00 -----------------
MOD:stFileName:XMLJOBSVer14  STRING(260)
XMLJOBSVer14           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer14),PRE(XMLJOBSVer14),CREATE
RecordNumberKey          KEY(XMLJOBSVer14:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XMLJOBSVer14:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XMLJOBSVer14:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XMLJOBSVer14:RECORD_STATE,XMLJOBSVer14:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XMLJOBSVer14:RECORD_STATE,XMLJOBSVer14:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XMLJOBSVer14:RECORD_STATE,XMLJOBSVer14:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XMLJOBSVer14:RECORD_STATE,XMLJOBSVer14:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XMLJOBSVer14:RECORD_STATE,XMLJOBSVer14:SERIAL_NO),DUP,NOCASE
EXCEPTION_DESC              MEMO(1000)
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
TR_REASON                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
REPAIR_COMPLETE_DATE        DATE
REPAIR_COMPLETE_TIME        TIME
GOODS_DELIVERY_DATE         DATE
GOODS_DELIVERY_TIME         TIME
                         END
                       END
!------------ End File: XMLJOBS, version 14 ------------------

!------------ File: XMLJOBS, version 15 ----------------------
!------------ modified 26.08.2004 at 15:47:13 -----------------
MOD:stFileName:XMLJOBSVer15  STRING(260)
XMLJOBSVer15           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer15),PRE(XMLJOBSVer15),CREATE
RecordNumberKey          KEY(XMLJOBSVer15:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XMLJOBSVer15:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XMLJOBSVer15:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XMLJOBSVer15:RECORD_STATE,XMLJOBSVer15:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XMLJOBSVer15:RECORD_STATE,XMLJOBSVer15:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XMLJOBSVer15:RECORD_STATE,XMLJOBSVer15:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XMLJOBSVer15:RECORD_STATE,XMLJOBSVer15:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XMLJOBSVer15:RECORD_STATE,XMLJOBSVer15:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
EXCEPTION_DESC              STRING(255)
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
TR_REASON                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
REPAIR_COMPLETE_DATE        DATE
REPAIR_COMPLETE_TIME        TIME
GOODS_DELIVERY_DATE         DATE
GOODS_DELIVERY_TIME         TIME
                         END
                       END
!------------ End File: XMLJOBS, version 15 ------------------

!------------ File: XMLJOBS, version 16 ----------------------
!------------ modified 01.10.2004 at 14:42:46 -----------------
MOD:stFileName:XMLJOBSVer16  STRING(260)
XMLJOBSVer16           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLJOBSVer16),PRE(XMLJOBSVer16),CREATE
RecordNumberKey          KEY(XMLJOBSVer16:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XMLJOBSVer16:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XMLJOBSVer16:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XMLJOBSVer16:RECORD_STATE,XMLJOBSVer16:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XMLJOBSVer16:RECORD_STATE,XMLJOBSVer16:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XMLJOBSVer16:RECORD_STATE,XMLJOBSVer16:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XMLJOBSVer16:RECORD_STATE,XMLJOBSVer16:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XMLJOBSVer16:RECORD_STATE,XMLJOBSVer16:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
EXCEPTION_DESC              STRING(255)
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
TR_REASON                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
REPAIR_COMPLETE_DATE        DATE
REPAIR_COMPLETE_TIME        TIME
GOODS_DELIVERY_DATE         DATE
GOODS_DELIVERY_TIME         TIME
SYMPTOM1_DESC               STRING(40)
SYMPTOM2_DESC               STRING(40)
SYMPTOM3_DESC               STRING(40)
BP_NO                       STRING(10)
                         END
                       END
!------------ End File: XMLJOBS, version 16 ------------------

!------------ File: MULDESP, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:MULDESPVer1  STRING(260)
MULDESPVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MULDESPVer1),PRE(MULDESPVer1)
RecordNumberKey          KEY(MULDESPVer1:RecordNumber),NOCASE,PRIMARY
BatchNumberKey           KEY(MULDESPVer1:BatchNumber),DUP,NOCASE
AccountNumberKey         KEY(MULDESPVer1:AccountNumber),DUP,NOCASE
CourierKey               KEY(MULDESPVer1:Courier),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
BatchNumber                 STRING(30)
AccountNumber               STRING(30)
Courier                     STRING(30)
                         END
                       END
!------------ End File: MULDESP, version 1 ------------------

!------------ File: IMEISHIP, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:IMEISHIPVer1  STRING(260)
IMEISHIPVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:IMEISHIPVer1),PRE(IMEISHIPVer1)
RecordNumberKey          KEY(IMEISHIPVer1:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEISHIPVer1:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEISHIPVer1:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
                         END
                       END
!------------ End File: IMEISHIP, version 1 ------------------

!------------ File: IMEISHIP, version 2 ----------------------
!------------ modified 06.12.2005 at 10:29:16 -----------------
MOD:stFileName:IMEISHIPVer2  STRING(260)
IMEISHIPVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:IMEISHIPVer2),PRE(IMEISHIPVer2),CREATE
RecordNumberKey          KEY(IMEISHIPVer2:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEISHIPVer2:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEISHIPVer2:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
BER                         BYTE
                         END
                       END
!------------ End File: IMEISHIP, version 2 ------------------

!------------ File: IMEISHIP, version 3 ----------------------
!------------ modified 10.02.2009 at 10:38:37 -----------------
MOD:stFileName:IMEISHIPVer3  STRING(260)
IMEISHIPVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:IMEISHIPVer3),PRE(IMEISHIPVer3),CREATE
RecordNumberKey          KEY(IMEISHIPVer3:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEISHIPVer3:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEISHIPVer3:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
BER                         BYTE
ManualEntry                 BYTE
                         END
                       END
!------------ End File: IMEISHIP, version 3 ------------------

!------------ File: JOBTHIRD, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:JOBTHIRDVer1  STRING(260)
JOBTHIRDVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBTHIRDVer1),PRE(JOBTHIRDVer1)
RecordNumberKey          KEY(JOBTHIRDVer1:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBTHIRDVer1:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(JOBTHIRDVer1:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(JOBTHIRDVer1:InIMEI),DUP,NOCASE
OutDateKey               KEY(JOBTHIRDVer1:RefNumber,JOBTHIRDVer1:DateOut,JOBTHIRDVer1:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(JOBTHIRDVer1:ThirdPartyNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                       END
!------------ End File: JOBTHIRD, version 1 ------------------

!------------ File: LOGRETRN, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:LOGRETRNVer1  STRING(260)
LOGRETRNVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:LOGRETRNVer1),PRE(LOGRETRNVer1)
RefNumberKey             KEY(LOGRETRNVer1:RefNumber),NOCASE,PRIMARY
ClubNokiaKey             KEY(LOGRETRNVer1:ClubNokiaSite,LOGRETRNVer1:Date),DUP,NOCASE
DateKey                  KEY(LOGRETRNVer1:Date),DUP,NOCASE
ReturnsNoKey             KEY(LOGRETRNVer1:ReturnsNumber,LOGRETRNVer1:Date),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
ClubNokiaSite               STRING(30)
ModelNumber                 STRING(30)
Quantity                    LONG
ReturnsNumber               STRING(30)
Date                        DATE
ReturnType                  STRING(3)
DummyField                  STRING(1)
                         END
                       END
!------------ End File: LOGRETRN, version 1 ------------------

!------------ File: PRODCODE, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:PRODCODEVer1  STRING(260)
PRODCODEVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PRODCODEVer1),PRE(PRODCODEVer1)
RecordNumberKey          KEY(PRODCODEVer1:RecordNumber),NOCASE,PRIMARY
ProductCodeKey           KEY(PRODCODEVer1:ProductCode),NOCASE
ModelProductKey          KEY(PRODCODEVer1:ModelNumber,PRODCODEVer1:ProductCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ModelNumber                 STRING(30)
ProductCode                 STRING(30)
DummyField                  STRING(2)
                         END
                       END
!------------ End File: PRODCODE, version 1 ------------------

!------------ File: LOGSTOCK, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:LOGSTOCKVer1  STRING(260)
LOGSTOCKVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:LOGSTOCKVer1),PRE(LOGSTOCKVer1)
RefNumberKey             KEY(LOGSTOCKVer1:RefNumber),NOCASE,PRIMARY
SalesKey                 KEY(LOGSTOCKVer1:SalesCode),DUP,NOCASE
DescriptionKey           KEY(LOGSTOCKVer1:Description),DUP,NOCASE
SalesModelNoKey          KEY(LOGSTOCKVer1:SalesCode,LOGSTOCKVer1:ModelNumber),NOCASE
RefModelNoKey            KEY(LOGSTOCKVer1:RefNumber,LOGSTOCKVer1:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
SalesCode                   STRING(30)
Description                 STRING(30)
ModelNumber                 STRING(30)
DummyField                  STRING(4)
                         END
                       END
!------------ End File: LOGSTOCK, version 1 ------------------

!------------ File: DEFEMAB2, version 1 ----------------------
!------------ modified 22.02.2006 at 15:15:32 -----------------
MOD:stFileName:DEFEMAB2Ver1  STRING(260)
DEFEMAB2Ver1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAB2Ver1),PRE(DEFEMAB2Ver1)
RecordNumberKey          KEY(DEFEMAB2Ver1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
B2Email                     STRING(4000)
                         END
                       END
!------------ End File: DEFEMAB2, version 1 ------------------

!------------ File: DEFEMAB3, version 1 ----------------------
!------------ modified 22.02.2006 at 15:15:32 -----------------
MOD:stFileName:DEFEMAB3Ver1  STRING(260)
DEFEMAB3Ver1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAB3Ver1),PRE(DEFEMAB3Ver1)
RecordNumberKey          KEY(DEFEMAB3Ver1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
B3Email                     STRING(4000)
                         END
                       END
!------------ End File: DEFEMAB3, version 1 ------------------

!------------ File: DEFEMAB4, version 1 ----------------------
!------------ modified 22.02.2006 at 15:15:32 -----------------
MOD:stFileName:DEFEMAB4Ver1  STRING(260)
DEFEMAB4Ver1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAB4Ver1),PRE(DEFEMAB4Ver1)
RecordNumberKey          KEY(DEFEMAB4Ver1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
B4Email                     STRING(4000)
                         END
                       END
!------------ End File: DEFEMAB4, version 1 ------------------

!------------ File: DEFEMAB5, version 1 ----------------------
!------------ modified 22.02.2006 at 15:15:32 -----------------
MOD:stFileName:DEFEMAB5Ver1  STRING(260)
DEFEMAB5Ver1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAB5Ver1),PRE(DEFEMAB5Ver1)
RecordNumberKey          KEY(DEFEMAB5Ver1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
B5Email                     STRING(4000)
                         END
                       END
!------------ End File: DEFEMAB5, version 1 ------------------

!------------ File: DEFEMAE2, version 1 ----------------------
!------------ modified 22.02.2006 at 15:15:32 -----------------
MOD:stFileName:DEFEMAE2Ver1  STRING(260)
DEFEMAE2Ver1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAE2Ver1),PRE(DEFEMAE2Ver1)
RecordNumberKey          KEY(DEFEMAE2Ver1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E2Email                     STRING(4000)
                         END
                       END
!------------ End File: DEFEMAE2, version 1 ------------------

!------------ File: DEFEMAE3, version 1 ----------------------
!------------ modified 22.02.2006 at 15:15:32 -----------------
MOD:stFileName:DEFEMAE3Ver1  STRING(260)
DEFEMAE3Ver1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAE3Ver1),PRE(DEFEMAE3Ver1)
RecordNumberKey          KEY(DEFEMAE3Ver1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E3Email                     STRING(4000)
                         END
                       END
!------------ End File: DEFEMAE3, version 1 ------------------

!------------ File: DEFEMAB1, version 1 ----------------------
!------------ modified 22.02.2006 at 15:15:32 -----------------
MOD:stFileName:DEFEMAB1Ver1  STRING(260)
DEFEMAB1Ver1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAB1Ver1),PRE(DEFEMAB1Ver1)
RecordNumberKey          KEY(DEFEMAB1Ver1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
B1Email                     STRING(4000)
                         END
                       END
!------------ End File: DEFEMAB1, version 1 ------------------

!------------ File: DEFEMAE1, version 1 ----------------------
!------------ modified 22.02.2006 at 11:14:36 -----------------
MOD:stFileName:DEFEMAE1Ver1  STRING(260)
DEFEMAE1Ver1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAE1Ver1),PRE(DEFEMAE1Ver1)
RecordNumberKey          KEY(DEFEMAE1Ver1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1Email                     STRING(5000)
E2Email                     STRING(5000)
E3Email                     STRING(5000)
                         END
                       END
!------------ End File: DEFEMAE1, version 1 ------------------

!------------ File: DEFEMAE1, version 2 ----------------------
!------------ modified 22.02.2006 at 11:26:16 -----------------
MOD:stFileName:DEFEMAE1Ver2  STRING(260)
DEFEMAE1Ver2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAE1Ver2),PRE(DEFEMAE1Ver2),CREATE
RecordNumberKey          KEY(DEFEMAE1Ver2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1Email                     STRING(5000)
                         END
                       END
!------------ End File: DEFEMAE1, version 2 ------------------

!------------ File: DEFEMAE1, version 3 ----------------------
!------------ modified 22.02.2006 at 12:06:41 -----------------
MOD:stFileName:DEFEMAE1Ver3  STRING(260)
DEFEMAE1Ver3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAE1Ver3),PRE(DEFEMAE1Ver3),CREATE
RecordNumberKey          KEY(DEFEMAE1Ver3:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1Email                     STRING(4500)
                         END
                       END
!------------ End File: DEFEMAE1, version 3 ------------------

!------------ File: DEFEMAE1, version 4 ----------------------
!------------ modified 22.02.2006 at 14:22:09 -----------------
MOD:stFileName:DEFEMAE1Ver4  STRING(260)
DEFEMAE1Ver4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAE1Ver4),PRE(DEFEMAE1Ver4),CREATE
RecordNumberKey          KEY(DEFEMAE1Ver4:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1Email                     STRING(4000)
                         END
                       END
!------------ End File: DEFEMAE1, version 4 ------------------

!------------ File: DEFEMAE1, version 5 ----------------------
!------------ modified 22.02.2006 at 15:15:32 -----------------
MOD:stFileName:DEFEMAE1Ver5  STRING(260)
DEFEMAE1Ver5           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAE1Ver5),PRE(DEFEMAE1Ver5),CREATE
RecordNumberKey          KEY(DEFEMAE1Ver5:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1Email                     STRING(4000)
                         END
                       END
!------------ End File: DEFEMAE1, version 5 ------------------

!------------ File: DEFEMAI2, version 1 ----------------------
!------------ modified 22.02.2006 at 10:55:58 -----------------
MOD:stFileName:DEFEMAI2Ver1  STRING(260)
DEFEMAI2Ver1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAI2Ver1),PRE(DEFEMAI2Ver1)
RecordNumberKey          KEY(DEFEMAI2Ver1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1SMS                       STRING(480)
E2SMS                       STRING(480)
E3SMS                       STRING(480)
E1Email                     STRING(5000)
E2Email                     STRING(5000)
E3Email                     STRING(5000)
                         END
                       END
!------------ End File: DEFEMAI2, version 1 ------------------

!------------ File: DEFEMAI2, version 2 ----------------------
!------------ modified 22.02.2006 at 11:03:03 -----------------
MOD:stFileName:DEFEMAI2Ver2  STRING(260)
DEFEMAI2Ver2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAI2Ver2),PRE(DEFEMAI2Ver2),CREATE
RecordNumberKey          KEY(DEFEMAI2Ver2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1SMS                       STRING(480)
E2SMS                       STRING(480)
E3SMS                       STRING(480)
                         END
                       END
!------------ End File: DEFEMAI2, version 2 ------------------

!------------ File: DEFEMAI2, version 3 ----------------------
!------------ modified 22.02.2006 at 11:14:36 -----------------
MOD:stFileName:DEFEMAI2Ver3  STRING(260)
DEFEMAI2Ver3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAI2Ver3),PRE(DEFEMAI2Ver3),CREATE
RecordNumberKey          KEY(DEFEMAI2Ver3:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1SMS                       STRING(480)
E2SMS                       STRING(480)
E3SMS                       STRING(480)
B1SMS                       STRING(480)
B2SMS                       STRING(480)
B3SMS                       STRING(480)
B4SMS                       STRING(480)
B5SMS                       STRING(480)
                         END
                       END
!------------ End File: DEFEMAI2, version 3 ------------------

!------------ File: DEFEMAI2, version 4 ----------------------
!------------ modified 09.03.2006 at 13:55:33 -----------------
MOD:stFileName:DEFEMAI2Ver4  STRING(260)
DEFEMAI2Ver4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAI2Ver4),PRE(DEFEMAI2Ver4),CREATE
RecordNumberKey          KEY(DEFEMAI2Ver4:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1SMS                       STRING(480)
E2SMS                       STRING(480)
E3SMS                       STRING(480)
B1SMS                       STRING(480)
B2SMS                       STRING(480)
B3SMS                       STRING(480)
B4SMS                       STRING(480)
B5SMS                       STRING(480)
E1Subject                   STRING(60)
E2Subject                   STRING(60)
E3Subject                   STRING(60)
B1Subject                   STRING(60)
B2Subject                   STRING(60)
B3Subject                   STRING(60)
B4Subject                   STRING(60)
B5Subject                   STRING(60)
E1Active                    BYTE
E2Active                    BYTE
E3Active                    BYTE
B1Active                    BYTE
B2Active                    BYTE
B3Active                    BYTE
B4Active                    BYTE
B5Active                    BYTE
                         END
                       END
!------------ End File: DEFEMAI2, version 4 ------------------

!------------ File: DEFEMAI2, version 5 ----------------------
!------------ modified 09.03.2006 at 14:02:52 -----------------
MOD:stFileName:DEFEMAI2Ver5  STRING(260)
DEFEMAI2Ver5           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAI2Ver5),PRE(DEFEMAI2Ver5),CREATE
RecordNumberKey          KEY(DEFEMAI2Ver5:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1SMS                       STRING(480)
E2SMS                       STRING(480)
E3SMS                       STRING(480)
B1SMS                       STRING(480)
B2SMS                       STRING(480)
B3SMS                       STRING(480)
B4SMS                       STRING(480)
B5SMS                       STRING(480)
E1Active                    BYTE
E2Active                    BYTE
E3Active                    BYTE
B1Active                    BYTE
B2Active                    BYTE
B3Active                    BYTE
B4Active                    BYTE
B5Active                    BYTE
                         END
                       END
!------------ End File: DEFEMAI2, version 5 ------------------

!------------ File: DEFEMAI2, version 6 ----------------------
!------------ modified 18.04.2006 at 13:10:59 -----------------
MOD:stFileName:DEFEMAI2Ver6  STRING(260)
DEFEMAI2Ver6           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAI2Ver6),PRE(DEFEMAI2Ver6),CREATE
RecordNumberKey          KEY(DEFEMAI2Ver6:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1SMS                       STRING(480)
E2SMS                       STRING(480)
E3SMS                       STRING(480)
B1SMS                       STRING(480)
B2SMS                       STRING(480)
B3SMS                       STRING(480)
B4SMS                       STRING(480)
B5SMS                       STRING(480)
E1Active                    BYTE
E2Active                    BYTE
E3Active                    BYTE
B1Active                    BYTE
B2Active                    BYTE
B3Active                    BYTE
B4Active                    BYTE
B5Active                    BYTE
E1SMSActive                 BYTE
E2SMSActive                 BYTE
E3SMSActive                 BYTE
B1SMSActive                 BYTE
B2SMSActive                 BYTE
B3SMSActive                 BYTE
B4SMSActive                 BYTE
B5SMSActive                 BYTE
                         END
                       END
!------------ End File: DEFEMAI2, version 6 ------------------

!------------ File: DEFEMAI2, version 7 ----------------------
!------------ modified 26.07.2006 at 16:05:14 -----------------
MOD:stFileName:DEFEMAI2Ver7  STRING(260)
DEFEMAI2Ver7           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAI2Ver7),PRE(DEFEMAI2Ver7),CREATE
RecordNumberKey          KEY(DEFEMAI2Ver7:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1SMS                       STRING(480)
E2SMS                       STRING(480)
E3SMS                       STRING(480)
B1SMS                       STRING(480)
B2SMS                       STRING(480)
B3SMS                       STRING(480)
B4SMS                       STRING(480)
B5SMS                       STRING(480)
B6SMS                       STRING(480)
B7SMS                       STRING(480)
E1Active                    BYTE
E2Active                    BYTE
E3Active                    BYTE
B1Active                    BYTE
B2Active                    BYTE
B3Active                    BYTE
B4Active                    BYTE
B5Active                    BYTE
B6Active                    BYTE
B7Active                    BYTE
E1SMSActive                 BYTE
E2SMSActive                 BYTE
E3SMSActive                 BYTE
B1SMSActive                 BYTE
B2SMSActive                 BYTE
B3SMSActive                 BYTE
B4SMSActive                 BYTE
B5SMSActive                 BYTE
B6SMSActive                 BYTE
B7SMSActive                 BYTE
B6NotifyDays                LONG
B7NotifyDays                LONG
                         END
                       END
!------------ End File: DEFEMAI2, version 7 ------------------

!------------ File: DEFEMAI2, version 8 ----------------------
!------------ modified 28.07.2006 at 14:33:54 -----------------
MOD:stFileName:DEFEMAI2Ver8  STRING(260)
DEFEMAI2Ver8           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFEMAI2Ver8),PRE(DEFEMAI2Ver8),CREATE
RecordNumberKey          KEY(DEFEMAI2Ver8:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1SMS                       STRING(480)
E2SMS                       STRING(480)
E2BSMS                      STRING(480)
E3SMS                       STRING(480)
E3BSMS                      STRING(480)
B1SMS                       STRING(480)
B2SMS                       STRING(480)
B3SMS                       STRING(480)
B4SMS                       STRING(480)
B5SMS                       STRING(480)
B6SMS                       STRING(480)
B7SMS                       STRING(480)
E1Active                    BYTE
E2Active                    BYTE
E2BActive                   BYTE
E3Active                    BYTE
E3BActive                   BYTE
B1Active                    BYTE
B2Active                    BYTE
B3Active                    BYTE
B4Active                    BYTE
B5Active                    BYTE
B6Active                    BYTE
B7Active                    BYTE
E1SMSActive                 BYTE
E2SMSActive                 BYTE
E2BSMSActive                BYTE
E3SMSActive                 BYTE
E3BSMSActive                BYTE
B1SMSActive                 BYTE
B2SMSActive                 BYTE
B3SMSActive                 BYTE
B4SMSActive                 BYTE
B5SMSActive                 BYTE
B6SMSActive                 BYTE
B7SMSActive                 BYTE
B6NotifyDays                LONG
B7NotifyDays                LONG
                         END
                       END
!------------ End File: DEFEMAI2, version 8 ------------------

!------------ File: DEFSIDEX, version 1 ----------------------
!------------ modified 22.03.2006 at 12:17:49 -----------------
MOD:stFileName:DEFSIDEXVer1  STRING(260)
DEFSIDEXVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFSIDEXVer1),PRE(DEFSIDEXVer1)
RecordNumberKey          KEY(DEFSIDEXVer1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(50)
SMTPPortNumber              STRING(6)
SMTPUserName                STRING(60)
SMTPPassword                STRING(60)
FromEmailAddress            STRING(255)
EmailSubject                STRING(100)
EmailRecipientList          STRING(255)
DateFrom                    DATE
DateTo                      DATE
                         END
                       END
!------------ End File: DEFSIDEX, version 1 ------------------

!------------ File: DEFSIDEX, version 2 ----------------------
!------------ modified 23.03.2006 at 15:59:22 -----------------
MOD:stFileName:DEFSIDEXVer2  STRING(260)
DEFSIDEXVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:DEFSIDEXVer2),PRE(DEFSIDEXVer2),CREATE
RecordNumberKey          KEY(DEFSIDEXVer2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(50)
SMTPPortNumber              STRING(6)
SMTPUserName                STRING(60)
SMTPPassword                STRING(60)
FromEmailAddress            STRING(255)
EmailSubject                STRING(100)
EmailRecipientList          STRING(255)
DateFrom                    DATE
DateTo                      DATE
FTPServer                   STRING(60)
FTPUsername                 STRING(60)
FTPPassword                 STRING(60)
FTPExportPath               STRING(255)
                         END
                       END
!------------ End File: DEFSIDEX, version 2 ------------------

!------------ File: RETSALES, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:RETSALESVer1  STRING(260)
RETSALESVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RETSALESVer1),PRE(RETSALESVer1)
Ref_Number_Key           KEY(RETSALESVer1:Ref_Number),NOCASE,PRIMARY
Invoice_Number_Key       KEY(RETSALESVer1:Invoice_Number),DUP,NOCASE
Despatched_Purchase_Key  KEY(RETSALESVer1:Despatched,RETSALESVer1:Purchase_Order_Number),DUP,NOCASE
Despatched_Ref_Key       KEY(RETSALESVer1:Despatched,RETSALESVer1:Ref_Number),DUP,NOCASE
Despatched_Account_Key   KEY(RETSALESVer1:Despatched,RETSALESVer1:Account_Number,RETSALESVer1:Purchase_Order_Number),DUP,NOCASE
Account_Number_Key       KEY(RETSALESVer1:Account_Number,RETSALESVer1:Ref_Number),DUP,NOCASE
Purchase_Number_Key      KEY(RETSALESVer1:Purchase_Order_Number),DUP,NOCASE
Despatch_Number_Key      KEY(RETSALESVer1:Despatch_Number),DUP,NOCASE
Date_Booked_Key          KEY(RETSALESVer1:date_booked),DUP,NOCASE
AccountInvoiceKey        KEY(RETSALESVer1:Account_Number,RETSALESVer1:Invoice_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Account_Number              STRING(15)
Contact_Name                STRING(30)
Purchase_Order_Number       STRING(30)
Delivery_Collection         STRING(3)
Payment_Method              STRING(3)
Courier_Cost                REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier                     STRING(30)
Consignment_Number          STRING(30)
Date_Despatched             DATE
Despatched                  STRING(3)
Despatch_Number             LONG
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Courier_Cost        REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
WebOrderNumber              LONG
WebDateCreated              DATE
WebTimeCreated              STRING(20)
WebCreatedUser              STRING(30)
Address_Group               GROUP
Postcode                      STRING(10)
Company_Name                  STRING(30)
Building_Name                 STRING(30)
Address_Line1                 STRING(30)
Address_Line2                 STRING(30)
Address_Line3                 STRING(30)
Telephone_Number              STRING(15)
Fax_Number                    STRING(15)
                            END
Address_Delivery_Group      GROUP
Postcode_Delivery             STRING(10)
Company_Name_Delivery         STRING(30)
Building_Name_Delivery        STRING(30)
Address_Line1_Delivery        STRING(30)
Address_Line2_Delivery        STRING(30)
Address_Line3_Delivery        STRING(30)
Telephone_Delivery            STRING(15)
Fax_Number_Delivery           STRING(15)
                            END
Delivery_Text               STRING(255)
Invoice_Text                STRING(255)
                         END
                       END
!------------ End File: RETSALES, version 1 ------------------

!------------ File: RETSALES, version 2 ----------------------
!------------ modified 06.03.2002 at 12:13:10 -----------------
MOD:stFileName:RETSALESVer2  STRING(260)
RETSALESVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RETSALESVer2),PRE(RETSALESVer2),CREATE
Ref_Number_Key           KEY(RETSALESVer2:Ref_Number),NOCASE,PRIMARY
Invoice_Number_Key       KEY(RETSALESVer2:Invoice_Number),DUP,NOCASE
Despatched_Purchase_Key  KEY(RETSALESVer2:Despatched,RETSALESVer2:Purchase_Order_Number),DUP,NOCASE
Despatched_Ref_Key       KEY(RETSALESVer2:Despatched,RETSALESVer2:Ref_Number),DUP,NOCASE
Despatched_Account_Key   KEY(RETSALESVer2:Despatched,RETSALESVer2:Account_Number,RETSALESVer2:Purchase_Order_Number),DUP,NOCASE
Account_Number_Key       KEY(RETSALESVer2:Account_Number,RETSALESVer2:Ref_Number),DUP,NOCASE
Purchase_Number_Key      KEY(RETSALESVer2:Purchase_Order_Number),DUP,NOCASE
Despatch_Number_Key      KEY(RETSALESVer2:Despatch_Number),DUP,NOCASE
Date_Booked_Key          KEY(RETSALESVer2:date_booked),DUP,NOCASE
AccountInvoiceKey        KEY(RETSALESVer2:Account_Number,RETSALESVer2:Invoice_Number),DUP,NOCASE
DespatchedPurchDateKey   KEY(RETSALESVer2:Despatched,RETSALESVer2:Purchase_Order_Number,RETSALESVer2:date_booked),DUP,NOCASE
DespatchNoRefNoKey       KEY(RETSALESVer2:Despatch_Number,RETSALESVer2:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Account_Number              STRING(15)
Contact_Name                STRING(30)
Purchase_Order_Number       STRING(30)
Delivery_Collection         STRING(3)
Payment_Method              STRING(3)
Courier_Cost                REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier                     STRING(30)
Consignment_Number          STRING(30)
Date_Despatched             DATE
Despatched                  STRING(3)
Despatch_Number             LONG
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Courier_Cost        REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
WebOrderNumber              LONG
WebDateCreated              DATE
WebTimeCreated              STRING(20)
WebCreatedUser              STRING(30)
Address_Group               GROUP
Postcode                      STRING(10)
Company_Name                  STRING(30)
Building_Name                 STRING(30)
Address_Line1                 STRING(30)
Address_Line2                 STRING(30)
Address_Line3                 STRING(30)
Telephone_Number              STRING(15)
Fax_Number                    STRING(15)
                            END
Address_Delivery_Group      GROUP
Postcode_Delivery             STRING(10)
Company_Name_Delivery         STRING(30)
Building_Name_Delivery        STRING(30)
Address_Line1_Delivery        STRING(30)
Address_Line2_Delivery        STRING(30)
Address_Line3_Delivery        STRING(30)
Telephone_Delivery            STRING(15)
Fax_Number_Delivery           STRING(15)
                            END
Delivery_Text               STRING(255)
Invoice_Text                STRING(255)
                         END
                       END
!------------ End File: RETSALES, version 2 ------------------

!------------ File: TEAMS, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:TEAMSVer1  STRING(260)
TEAMSVer1              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:TEAMSVer1),PRE(TEAMSVer1)
Record_Number_Key        KEY(TEAMSVer1:Record_Number),NOCASE,PRIMARY
Team_Key                 KEY(TEAMSVer1:Team),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Team                        STRING(30)
                         END
                       END
!------------ End File: TEAMS, version 1 ------------------

!------------ File: MERGE, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:MERGEVer1  STRING(260)
MERGEVer1              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MERGEVer1),PRE(MERGEVer1),BINDABLE
RefNumberKey             KEY(MERGEVer1:RefNumber),NOCASE,PRIMARY
FieldNameKey             KEY(MERGEVer1:FieldName),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
FieldName                   STRING(30)
FileName                    STRING(60)
Type                        STRING(3)
Description                 STRING(255)
Capitals                    BYTE
                         END
                       END
!------------ End File: MERGE, version 1 ------------------

!------------ File: SIDDEF, version 1 ----------------------
!------------ modified 30.08.2006 at 09:38:37 -----------------
MOD:stFileName:SIDDEFVer1  STRING(260)
SIDDEFVer1             FILE,DRIVER('Btrieve'),PRE(SIDDEFVer1),NAME(MOD:stFileName:SIDDEFVer1)
Record                   RECORD,PRE()
RecordNumber                LONG
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
EmailSubject                STRING(60)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
                         END
                       END
!------------ End File: SIDDEF, version 1 ------------------

!------------ File: SIDDEF, version 2 ----------------------
!------------ modified 30.08.2006 at 17:14:36 -----------------
MOD:stFileName:SIDDEFVer2  STRING(260)
SIDDEFVer2             FILE,DRIVER('Btrieve'),PRE(SIDDEFVer2),NAME(MOD:stFileName:SIDDEFVer2),CREATE
RecordNumberKey          KEY(SIDDEFVer2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
EmailSubject                STRING(60)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
                         END
                       END
!------------ End File: SIDDEF, version 2 ------------------

!------------ File: SIDDEF, version 3 ----------------------
!------------ modified 31.08.2006 at 10:50:27 -----------------
MOD:stFileName:SIDDEFVer3  STRING(260)
SIDDEFVer3             FILE,DRIVER('Btrieve'),PRE(SIDDEFVer3),NAME(MOD:stFileName:SIDDEFVer3),CREATE
RecordNumberKey          KEY(SIDDEFVer3:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
EmailSubject                STRING(60)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
FTPFailedCount              LONG
                         END
                       END
!------------ End File: SIDDEF, version 3 ------------------

!------------ File: SIDDEF, version 4 ----------------------
!------------ modified 06.09.2006 at 10:57:23 -----------------
MOD:stFileName:SIDDEFVer4  STRING(260)
SIDDEFVer4             FILE,DRIVER('Btrieve'),PRE(SIDDEFVer4),NAME(MOD:stFileName:SIDDEFVer4),CREATE
RecordNumberKey          KEY(SIDDEFVer4:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
EmailSubject                STRING(60)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
FTPFailedCount              LONG
UnallocTransitToSC          LONG
UnallocTurnaround           LONG
UnallocTransitStore         LONG
                         END
                       END
!------------ End File: SIDDEF, version 4 ------------------

!------------ File: SIDDEF, version 5 ----------------------
!------------ modified 07.02.2007 at 11:57:59 -----------------
MOD:stFileName:SIDDEFVer5  STRING(260)
SIDDEFVer5             FILE,DRIVER('Btrieve'),PRE(SIDDEFVer5),NAME(MOD:stFileName:SIDDEFVer5),CREATE
RecordNumberKey          KEY(SIDDEFVer5:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
EmailSubject                STRING(60)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
FTPFailedCount              LONG
UnallocTransitToSC          LONG
UnallocTurnaround           LONG
UnallocTransitStore         LONG
WebJobConn                  STRING(50)
CPMConn                     STRING(50)
                         END
                       END
!------------ End File: SIDDEF, version 5 ------------------

!------------ File: ORDPEND, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:ORDPENDVer1  STRING(260)
ORDPENDVer1            FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ORDPENDVer1),PRE(ORDPENDVer1)
Ref_Number_Key           KEY(ORDPENDVer1:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ORDPENDVer1:Supplier,ORDPENDVer1:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ORDPENDVer1:Supplier,ORDPENDVer1:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ORDPENDVer1:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ORDPENDVer1:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ORDPENDVer1:Awaiting_Stock,ORDPENDVer1:Supplier,ORDPENDVer1:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
                         END
                       END
!------------ End File: ORDPEND, version 1 ------------------

!------------ File: ORDPEND, version 2 ----------------------
!------------ modified 09.08.2002 at 11:39:36 -----------------
MOD:stFileName:ORDPENDVer2  STRING(260)
ORDPENDVer2            FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ORDPENDVer2),PRE(ORDPENDVer2),CREATE
Ref_Number_Key           KEY(ORDPENDVer2:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ORDPENDVer2:Supplier,ORDPENDVer2:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ORDPENDVer2:Supplier,ORDPENDVer2:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ORDPENDVer2:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ORDPENDVer2:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ORDPENDVer2:Awaiting_Stock,ORDPENDVer2:Supplier,ORDPENDVer2:Part_Number),DUP,NOCASE
PartRecordNumberKey      KEY(ORDPENDVer2:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
PartRecordNumber            LONG
                         END
                       END
!------------ End File: ORDPEND, version 2 ------------------

!------------ File: ORDPEND, version 3 ----------------------
!------------ modified 07.10.2004 at 15:20:19 -----------------
MOD:stFileName:ORDPENDVer3  STRING(260)
ORDPENDVer3            FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ORDPENDVer3),PRE(ORDPENDVer3),CREATE
Ref_Number_Key           KEY(ORDPENDVer3:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ORDPENDVer3:Supplier,ORDPENDVer3:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ORDPENDVer3:Supplier,ORDPENDVer3:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ORDPENDVer3:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ORDPENDVer3:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ORDPENDVer3:Awaiting_Stock,ORDPENDVer3:Supplier,ORDPENDVer3:Part_Number),DUP,NOCASE
PartRecordNumberKey      KEY(ORDPENDVer3:PartRecordNumber),DUP,NOCASE
Job_Number_Key           KEY(ORDPENDVer3:Job_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
PartRecordNumber            LONG
                         END
                       END
!------------ End File: ORDPEND, version 3 ------------------

!------------ File: STOHIST, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:STOHISTVer1  STRING(260)
STOHISTVer1            FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOHISTVer1),PRE(STOHISTVer1)
Ref_Number_Key           KEY(STOHISTVer1:Ref_Number,STOHISTVer1:Date),DUP,NOCASE
record_number_key        KEY(STOHISTVer1:Record_Number),NOCASE,PRIMARY
Transaction_Type_Key     KEY(STOHISTVer1:Ref_Number,STOHISTVer1:Transaction_Type,STOHISTVer1:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
User                        STRING(3)
Transaction_Type            STRING(3)
Despatch_Note_Number        STRING(30)
Job_Number                  LONG
Sales_Number                LONG
Quantity                    REAL
Date                        DATE
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Notes                       STRING(255)
Information                 STRING(255)
                         END
                       END
!------------ End File: STOHIST, version 1 ------------------

!------------ File: EXREASON, version 1 ----------------------
!------------ modified 09.12.2002 at 10:25:00 -----------------
MOD:stFileName:EXREASONVer1  STRING(260)
EXREASONVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:EXREASONVer1),PRE(EXREASONVer1)
RecordNumberKey          KEY(EXREASONVer1:RecordNumber),NOCASE,PRIMARY
ReasonKey                KEY(EXREASONVer1:ReasonType,EXREASONVer1:Reason),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReasonType                  BYTE
Reason                      STRING(255)
                         END
                       END
!------------ End File: EXREASON, version 1 ------------------

!------------ File: REPTYDEF, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:REPTYDEFVer1  STRING(260)
REPTYDEFVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:REPTYDEFVer1),PRE(REPTYDEFVer1)
Repair_Type_Key          KEY(REPTYDEFVer1:Repair_Type),NOCASE,PRIMARY
Chargeable_Key           KEY(REPTYDEFVer1:Chargeable,REPTYDEFVer1:Repair_Type),DUP,NOCASE
Warranty_Key             KEY(REPTYDEFVer1:Warranty,REPTYDEFVer1:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
WarrantyCode                STRING(5)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
                         END
                       END
!------------ End File: REPTYDEF, version 1 ------------------

!------------ File: JOBSE, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:JOBSEVer1  STRING(260)
JOBSEVer1              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer1),PRE(JOBSEVer1)
RecordNumberKey          KEY(JOBSEVer1:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer1:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
                         END
                       END
!------------ End File: JOBSE, version 1 ------------------

!------------ File: JOBSE, version 2 ----------------------
!------------ modified 09.08.2002 at 11:39:37 -----------------
MOD:stFileName:JOBSEVer2  STRING(260)
JOBSEVer2              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer2),PRE(JOBSEVer2),CREATE
RecordNumberKey          KEY(JOBSEVer2:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
                         END
                       END
!------------ End File: JOBSE, version 2 ------------------

!------------ File: JOBSE, version 3 ----------------------
!------------ modified 09.12.2002 at 10:25:00 -----------------
MOD:stFileName:JOBSEVer3  STRING(260)
JOBSEVer3              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer3),PRE(JOBSEVer3),CREATE
RecordNumberKey          KEY(JOBSEVer3:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer3:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
                         END
                       END
!------------ End File: JOBSE, version 3 ------------------

!------------ File: JOBSE, version 4 ----------------------
!------------ modified 16.12.2002 at 16:58:50 -----------------
MOD:stFileName:JOBSEVer4  STRING(260)
JOBSEVer4              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer4),PRE(JOBSEVer4),CREATE
RecordNumberKey          KEY(JOBSEVer4:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer4:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
                         END
                       END
!------------ End File: JOBSE, version 4 ------------------

!------------ File: JOBSE, version 5 ----------------------
!------------ modified 04.07.2003 at 12:14:27 -----------------
MOD:stFileName:JOBSEVer5  STRING(260)
JOBSEVer5              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer5),PRE(JOBSEVer5),CREATE
RecordNumberKey          KEY(JOBSEVer5:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer5:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
                         END
                       END
!------------ End File: JOBSE, version 5 ------------------

!------------ File: JOBSE, version 6 ----------------------
!------------ modified 29.01.2004 at 10:05:51 -----------------
MOD:stFileName:JOBSEVer6  STRING(260)
JOBSEVer6              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer6),PRE(JOBSEVer6),CREATE
RecordNumberKey          KEY(JOBSEVer6:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer6:RefNumber),DUP,NOCASE
InWorkshopDateKey        KEY(JOBSEVer6:InWorkshopDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
                         END
                       END
!------------ End File: JOBSE, version 6 ------------------

!------------ File: JOBSE, version 7 ----------------------
!------------ modified 18.02.2004 at 14:50:58 -----------------
MOD:stFileName:JOBSEVer7  STRING(260)
JOBSEVer7              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer7),PRE(JOBSEVer7),CREATE
RecordNumberKey          KEY(JOBSEVer7:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer7:RefNumber),DUP,NOCASE
InWorkshopDateKey        KEY(JOBSEVer7:InWorkshopDate),DUP,NOCASE
CompleteRepairKey        KEY(JOBSEVer7:CompleteRepairDate,JOBSEVer7:CompleteRepairTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
                         END
                       END
!------------ End File: JOBSE, version 7 ------------------

!------------ File: JOBSE, version 8 ----------------------
!------------ modified 05.04.2005 at 09:28:39 -----------------
MOD:stFileName:JOBSEVer8  STRING(260)
JOBSEVer8              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer8),PRE(JOBSEVer8),CREATE
RecordNumberKey          KEY(JOBSEVer8:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer8:RefNumber),DUP,NOCASE
InWorkshopDateKey        KEY(JOBSEVer8:InWorkshopDate),DUP,NOCASE
CompleteRepairKey        KEY(JOBSEVer8:CompleteRepairDate,JOBSEVer8:CompleteRepairTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
EstimatedDespatchDate       DATE
                         END
                       END
!------------ End File: JOBSE, version 8 ------------------

!------------ File: JOBSE, version 9 ----------------------
!------------ modified 05.04.2005 at 10:06:21 -----------------
MOD:stFileName:JOBSEVer9  STRING(260)
JOBSEVer9              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer9),PRE(JOBSEVer9),CREATE
RecordNumberKey          KEY(JOBSEVer9:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer9:RefNumber),DUP,NOCASE
InWorkshopDateKey        KEY(JOBSEVer9:InWorkshopDate),DUP,NOCASE
CompleteRepairKey        KEY(JOBSEVer9:CompleteRepairDate,JOBSEVer9:CompleteRepairTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
CustomerCollectionTime      TIME
EstimatedDespatchDate       DATE
EstimatedDespatchTime       TIME
                         END
                       END
!------------ End File: JOBSE, version 9 ------------------

!------------ File: MANFAULT, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:MANFAULTVer1  STRING(260)
MANFAULTVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANFAULTVer1),PRE(MANFAULTVer1)
Field_Number_Key         KEY(MANFAULTVer1:Manufacturer,MANFAULTVer1:Field_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
                         END
                       END
!------------ End File: MANFAULT, version 1 ------------------

!------------ File: MANFAULT, version 2 ----------------------
!------------ modified 09.08.2002 at 11:39:37 -----------------
MOD:stFileName:MANFAULTVer2  STRING(260)
MANFAULTVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANFAULTVer2),PRE(MANFAULTVer2),CREATE
Field_Number_Key         KEY(MANFAULTVer2:Manufacturer,MANFAULTVer2:Field_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
                         END
                       END
!------------ End File: MANFAULT, version 2 ------------------

!------------ File: MANFAULT, version 3 ----------------------
!------------ modified 03.03.2004 at 16:07:17 -----------------
MOD:stFileName:MANFAULTVer3  STRING(260)
MANFAULTVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANFAULTVer3),PRE(MANFAULTVer3),CREATE
Field_Number_Key         KEY(MANFAULTVer3:Manufacturer,MANFAULTVer3:Field_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
DefaultValue                STRING(30)
                         END
                       END
!------------ End File: MANFAULT, version 3 ------------------

!------------ File: PICKNOTE, version 1 ----------------------
!------------ modified 15.01.2004 at 15:07:21 -----------------
MOD:stFileName:PICKNOTEVer1  STRING(260)
PICKNOTEVer1           FILE,DRIVER('Btrieve'),OEM,PRE(PICKNOTEVer1),NAME(MOD:stFileName:PICKNOTEVer1)
Record                   RECORD,PRE()
PickNoteRef                 LONG
JobReference                LONG
PickNoteNumber              LONG
EngineerCode                STRING(3)
Location                    STRING(30)
IsPrinted                   BYTE
PrintedBy                   STRING(3)
PrintedDate                 DATE
PrintedTime                 TIME
IsConfirmed                 BYTE
ConfirmedBy                 STRING(3)
ConfirmedDate               DATE
ConfirmedTime               TIME
ConfirmedNotes              STRING(255)
                         END
                       END
!------------ End File: PICKNOTE, version 1 ------------------

!------------ File: PICKNOTE, version 2 ----------------------
!------------ modified 16.01.2004 at 13:55:31 -----------------
MOD:stFileName:PICKNOTEVer2  STRING(260)
PICKNOTEVer2           FILE,DRIVER('Btrieve'),OEM,PRE(PICKNOTEVer2),NAME(MOD:stFileName:PICKNOTEVer2),CREATE
keyonjobno               KEY(PICKNOTEVer2:JobReference),DUP,NOCASE,OPT
Record                   RECORD,PRE()
PickNoteRef                 LONG
JobReference                LONG
PickNoteNumber              LONG
EngineerCode                STRING(3)
Location                    STRING(30)
IsPrinted                   BYTE
PrintedBy                   STRING(3)
PrintedDate                 DATE
PrintedTime                 TIME
IsConfirmed                 BYTE
ConfirmedBy                 STRING(3)
ConfirmedDate               DATE
ConfirmedTime               TIME
ConfirmedNotes              STRING(255)
                         END
                       END
!------------ End File: PICKNOTE, version 2 ------------------

!------------ File: PICKNOTE, version 3 ----------------------
!------------ modified 20.01.2004 at 16:15:01 -----------------
MOD:stFileName:PICKNOTEVer3  STRING(260)
PICKNOTEVer3           FILE,DRIVER('Btrieve'),OEM,PRE(PICKNOTEVer3),NAME(MOD:stFileName:PICKNOTEVer3),CREATE
keypicknote              KEY(PICKNOTEVer3:PickNoteRef),NOCASE,PRIMARY
keyonjobno               KEY(PICKNOTEVer3:JobReference),DUP,NOCASE
Record                   RECORD,PRE()
PickNoteRef                 LONG
JobReference                LONG
PickNoteNumber              LONG
EngineerCode                STRING(3)
Location                    STRING(30)
IsPrinted                   BYTE
PrintedBy                   STRING(3)
PrintedDate                 DATE
PrintedTime                 TIME
IsConfirmed                 BYTE
ConfirmedBy                 STRING(3)
ConfirmedDate               DATE
ConfirmedTime               TIME
ConfirmedNotes              STRING(255)
                         END
                       END
!------------ End File: PICKNOTE, version 3 ------------------

!------------ File: PICKNOTE, version 4 ----------------------
!------------ modified 21.01.2004 at 11:45:04 -----------------
MOD:stFileName:PICKNOTEVer4  STRING(260)
PICKNOTEVer4           FILE,DRIVER('Btrieve'),OEM,PRE(PICKNOTEVer4),NAME(MOD:stFileName:PICKNOTEVer4),CREATE
keypicknote              KEY(PICKNOTEVer4:PickNoteRef),NOCASE,PRIMARY
keyonjobno               KEY(PICKNOTEVer4:JobReference),DUP,NOCASE
Record                   RECORD,PRE()
PickNoteRef                 LONG
JobReference                LONG
PickNoteNumber              LONG
EngineerCode                STRING(3)
Location                    STRING(30)
IsPrinted                   BYTE
PrintedBy                   STRING(3)
PrintedDate                 DATE
PrintedTime                 TIME
IsConfirmed                 BYTE
ConfirmedBy                 STRING(3)
ConfirmedDate               DATE
ConfirmedTime               TIME
ConfirmedNotes              STRING(255)
Usercode                    STRING(3)
                         END
                       END
!------------ End File: PICKNOTE, version 4 ------------------

!------------ File: COURIER, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:COURIERVer1  STRING(260)
COURIERVer1            FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:COURIERVer1),PRE(COURIERVer1)
Courier_Key              KEY(COURIERVer1:Courier),NOCASE,PRIMARY
Courier_Type_Key         KEY(COURIERVer1:Courier_Type,COURIERVer1:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Courier                     STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Export_Path                 STRING(255)
Include_Estimate            STRING(3)
Include_Chargeable          STRING(3)
Include_Warranty            STRING(3)
Service                     STRING(15)
Import_Path                 STRING(255)
Courier_Type                STRING(30)
Last_Despatch_Time          TIME
ICOLSuffix                  STRING(3)
ContractNumber              STRING(20)
ANCPath                     STRING(255)
ANCCount                    LONG
DespatchClose               STRING(3)
LabGOptions                 STRING(60)
CustomerCollection          BYTE
NewStatus                   STRING(30)
NoOfDespatchNotes           LONG
AutoConsignmentNo           BYTE
LastConsignmentNo           LONG
                         END
                       END
!------------ End File: COURIER, version 1 ------------------

!------------ File: ESNMODEL, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:ESNMODELVer1  STRING(260)
ESNMODELVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ESNMODELVer1),PRE(ESNMODELVer1)
Record_Number_Key        KEY(ESNMODELVer1:Record_Number),NOCASE,PRIMARY
ESN_Key                  KEY(ESNMODELVer1:ESN,ESNMODELVer1:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(ESNMODELVer1:ESN),DUP,NOCASE
Model_Number_Key         KEY(ESNMODELVer1:Model_Number,ESNMODELVer1:ESN),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
ESN                         STRING(6)
Model_Number                STRING(30)
                         END
                       END
!------------ End File: ESNMODEL, version 1 ------------------

!------------ File: STOCK, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:56 -----------------
MOD:stFileName:STOCKVer1  STRING(260)
STOCKVer1              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOCKVer1),PRE(STOCKVer1)
Ref_Number_Key           KEY(STOCKVer1:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer1:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer1:Location,STOCKVer1:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer1:Location,STOCKVer1:Accessory,STOCKVer1:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer1:Location,STOCKVer1:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer1:Location,STOCKVer1:Accessory,STOCKVer1:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer1:Location,STOCKVer1:Accessory,STOCKVer1:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer1:Manufacturer,STOCKVer1:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer1:Location,STOCKVer1:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer1:Location,STOCKVer1:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer1:Location,STOCKVer1:Accessory,STOCKVer1:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer1:Location,STOCKVer1:Ref_Number,STOCKVer1:Part_Number,STOCKVer1:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer1:Location,STOCKVer1:Manufacturer,STOCKVer1:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer1:Location,STOCKVer1:Accessory,STOCKVer1:Manufacturer,STOCKVer1:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer1:Location,STOCKVer1:Part_Number,STOCKVer1:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer1:Ref_Number,STOCKVer1:Part_Number,STOCKVer1:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer1:Minimum_Stock,STOCKVer1:Location,STOCKVer1:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer1:Minimum_Stock,STOCKVer1:Location,STOCKVer1:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer1:Location,STOCKVer1:Shelf_Location,STOCKVer1:Second_Location,STOCKVer1:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer1:QuantityRequested,STOCKVer1:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer1:ExchangeUnit,STOCKVer1:Location,STOCKVer1:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer1:ExchangeUnit,STOCKVer1:Location,STOCKVer1:Description),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
                         END
                       END
!------------ End File: STOCK, version 1 ------------------

!------------ File: STOCK, version 2 ----------------------
!------------ modified 09.08.2002 at 11:39:37 -----------------
MOD:stFileName:STOCKVer2  STRING(260)
STOCKVer2              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOCKVer2),PRE(STOCKVer2),CREATE
Ref_Number_Key           KEY(STOCKVer2:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer2:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer2:Location,STOCKVer2:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer2:Location,STOCKVer2:Accessory,STOCKVer2:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer2:Location,STOCKVer2:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer2:Location,STOCKVer2:Accessory,STOCKVer2:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer2:Location,STOCKVer2:Accessory,STOCKVer2:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer2:Manufacturer,STOCKVer2:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer2:Location,STOCKVer2:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer2:Location,STOCKVer2:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer2:Location,STOCKVer2:Accessory,STOCKVer2:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer2:Location,STOCKVer2:Ref_Number,STOCKVer2:Part_Number,STOCKVer2:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer2:Location,STOCKVer2:Manufacturer,STOCKVer2:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer2:Location,STOCKVer2:Accessory,STOCKVer2:Manufacturer,STOCKVer2:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer2:Location,STOCKVer2:Part_Number,STOCKVer2:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer2:Ref_Number,STOCKVer2:Part_Number,STOCKVer2:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer2:Minimum_Stock,STOCKVer2:Location,STOCKVer2:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer2:Minimum_Stock,STOCKVer2:Location,STOCKVer2:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer2:Location,STOCKVer2:Shelf_Location,STOCKVer2:Second_Location,STOCKVer2:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer2:QuantityRequested,STOCKVer2:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer2:ExchangeUnit,STOCKVer2:Location,STOCKVer2:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer2:ExchangeUnit,STOCKVer2:Location,STOCKVer2:Description),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
AllowDuplicate              BYTE
                         END
                       END
!------------ End File: STOCK, version 2 ------------------

!------------ File: STOCK, version 3 ----------------------
!------------ modified 06.01.2003 at 15:18:08 -----------------
MOD:stFileName:STOCKVer3  STRING(260)
STOCKVer3              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STOCKVer3),PRE(STOCKVer3),CREATE
Ref_Number_Key           KEY(STOCKVer3:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer3:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer3:Location,STOCKVer3:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer3:Location,STOCKVer3:Accessory,STOCKVer3:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer3:Location,STOCKVer3:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer3:Location,STOCKVer3:Accessory,STOCKVer3:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer3:Location,STOCKVer3:Accessory,STOCKVer3:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer3:Manufacturer,STOCKVer3:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer3:Location,STOCKVer3:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer3:Location,STOCKVer3:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer3:Location,STOCKVer3:Accessory,STOCKVer3:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer3:Location,STOCKVer3:Ref_Number,STOCKVer3:Part_Number,STOCKVer3:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer3:Location,STOCKVer3:Manufacturer,STOCKVer3:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer3:Location,STOCKVer3:Accessory,STOCKVer3:Manufacturer,STOCKVer3:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer3:Location,STOCKVer3:Part_Number,STOCKVer3:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer3:Ref_Number,STOCKVer3:Part_Number,STOCKVer3:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer3:Minimum_Stock,STOCKVer3:Location,STOCKVer3:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer3:Minimum_Stock,STOCKVer3:Location,STOCKVer3:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer3:Location,STOCKVer3:Shelf_Location,STOCKVer3:Second_Location,STOCKVer3:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer3:QuantityRequested,STOCKVer3:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer3:ExchangeUnit,STOCKVer3:Location,STOCKVer3:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer3:ExchangeUnit,STOCKVer3:Location,STOCKVer3:Description),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
AllowDuplicate              BYTE
ExcludeFromEDI              BYTE
                         END
                       END
!------------ End File: STOCK, version 3 ------------------

!------------ File: LOGSTOLC, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:LOGSTOLCVer1  STRING(260)
LOGSTOLCVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:LOGSTOLCVer1),PRE(LOGSTOLCVer1)
RefNumberKey             KEY(LOGSTOLCVer1:RefNumber),NOCASE,PRIMARY
LocationKey              KEY(LOGSTOLCVer1:Location),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Location                    STRING(30)
                         END
                       END
!------------ End File: LOGSTOLC, version 1 ------------------

!------------ File: LOGSERST, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:LOGSERSTVer1  STRING(260)
LOGSERSTVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:LOGSERSTVer1),PRE(LOGSERSTVer1)
RecordNumberKey          KEY(LOGSERSTVer1:RecordNumber),NOCASE,PRIMARY
ESNKey                   KEY(LOGSERSTVer1:ESN),DUP,NOCASE
RefNumberKey             KEY(LOGSERSTVer1:RefNumber,LOGSERSTVer1:ESN),DUP,NOCASE
ESNStatusKey             KEY(LOGSERSTVer1:RefNumber,LOGSERSTVer1:Status,LOGSERSTVer1:ESN),DUP,NOCASE
NokiaStatusKey           KEY(LOGSERSTVer1:RefNumber,LOGSERSTVer1:Status,LOGSERSTVer1:ClubNokia,LOGSERSTVer1:ESN),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
ESN                         STRING(30)
Status                      STRING(10)
ClubNokia                   STRING(30)
DummyField                  STRING(4)
                         END
                       END
!------------ End File: LOGSERST, version 1 ------------------

!------------ File: LOGSTLOC, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:LOGSTLOCVer1  STRING(260)
LOGSTLOCVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:LOGSTLOCVer1),PRE(LOGSTLOCVer1)
RecordNumberKey          KEY(LOGSTLOCVer1:RecordNumber),NOCASE,PRIMARY
RefLocationKey           KEY(LOGSTLOCVer1:RefNumber,LOGSTLOCVer1:Location),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Location                    STRING(30)
AvlQuantity                 LONG
DesQuantity                 LONG
DummyField                  STRING(1)
                         END
                       END
!------------ End File: LOGSTLOC, version 1 ------------------

!------------ File: TRAFAULO, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:TRAFAULOVer1  STRING(260)
TRAFAULOVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRAFAULOVer1),PRE(TRAFAULOVer1)
Field_Key                KEY(TRAFAULOVer1:AccountNumber,TRAFAULOVer1:Field_Number,TRAFAULOVer1:Field,TRAFAULOVer1:Description),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                       END
!------------ End File: TRAFAULO, version 1 ------------------

!------------ File: MPXJOBS, version 1 ----------------------
!------------ modified 06.12.2005 at 10:29:19 -----------------
MOD:stFileName:MPXJOBSVer1  STRING(260)
MPXJOBSVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXJOBSVer1),PRE(MPXJOBSVer1)
RecordNoKey              KEY(MPXJOBSVer1:RecordNo),NOCASE,OPT,PRIMARY
Record                   RECORD,PRE()
RecordNo                    LONG
                         END
                       END
!------------ End File: MPXJOBS, version 1 ------------------

!------------ File: MPXJOBS, version 2 ----------------------
!------------ modified 08.12.2005 at 13:17:18 -----------------
MOD:stFileName:MPXJOBSVer2  STRING(260)
MPXJOBSVer2            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXJOBSVer2),PRE(MPXJOBSVer2),CREATE
RecordNoKey              KEY(MPXJOBSVer2:RecordNo),NOCASE,OPT,PRIMARY
Record                   RECORD,PRE()
RecordNo                    LONG
Record_State                BYTE
Exception_Code              LONG
Exception_Desc              STRING(255)
RefNumber                   LONG
Status                      STRING(30)
ID                          LONG
CustCTN                     STRING(50)
QuoteValue                  LONG
ModelID                     STRING(50)
QuoteQ1                     BYTE
QuoteQ2                     BYTE
QuoteQ3                     BYTE
QuoteACC1                   BYTE
QuoteACC2                   BYTE
QuoteACC3                   BYTE
QuoteACC4                   BYTE
QuoteACC5                   BYTE
CustTitle                   STRING(50)
CustFirstName               STRING(50)
CustLastName                STRING(50)
CustAdd1                    STRING(50)
CustAdd2                    STRING(50)
CustAdd3                    STRING(50)
CustAdd4                    STRING(50)
CustPostCode                STRING(50)
CustRef                     STRING(50)
CustNetwork                 STRING(50)
EnvLetter                   STRING(50)
InspType                    STRING(50)
EnvType                     STRING(50)
EnvCount                    LONG
                         END
                       END
!------------ End File: MPXJOBS, version 2 ------------------

!------------ File: MPXJOBS, version 3 ----------------------
!------------ modified 08.12.2005 at 14:04:40 -----------------
MOD:stFileName:MPXJOBSVer3  STRING(260)
MPXJOBSVer3            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXJOBSVer3),PRE(MPXJOBSVer3),CREATE
RecordNoKey              KEY(MPXJOBSVer3:RecordNo),NOCASE,OPT,PRIMARY
IDKey                    KEY(MPXJOBSVer3:ID),DUP,NOCASE,OPT
JobNoKey                 KEY(MPXJOBSVer3:RefNumber),DUP,NOCASE,OPT
StateJobNoKey            KEY(MPXJOBSVer3:Record_State,MPXJOBSVer3:RefNumber),DUP,NOCASE,OPT
StateSurnameKey          KEY(MPXJOBSVer3:Record_State,MPXJOBSVer3:CustLastName),DUP,NOCASE,OPT
StatePostcodeKey         KEY(MPXJOBSVer3:Record_State,MPXJOBSVer3:CustPostCode),DUP,NOCASE,OPT
StateModelKey            KEY(MPXJOBSVer3:Record_State,MPXJOBSVer3:ModelID),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNo                    LONG
Record_State                BYTE
Exception_Code              LONG
Exception_Desc              STRING(255)
RefNumber                   LONG
Status                      STRING(30)
ID                          LONG
CustCTN                     STRING(50)
QuoteValue                  LONG
ModelID                     STRING(50)
QuoteQ1                     BYTE
QuoteQ2                     BYTE
QuoteQ3                     BYTE
QuoteACC1                   BYTE
QuoteACC2                   BYTE
QuoteACC3                   BYTE
QuoteACC4                   BYTE
QuoteACC5                   BYTE
CustTitle                   STRING(50)
CustFirstName               STRING(50)
CustLastName                STRING(50)
CustAdd1                    STRING(50)
CustAdd2                    STRING(50)
CustAdd3                    STRING(50)
CustAdd4                    STRING(50)
CustPostCode                STRING(50)
CustRef                     STRING(50)
CustNetwork                 STRING(50)
EnvLetter                   STRING(50)
InspType                    STRING(50)
EnvType                     STRING(50)
EnvCount                    LONG
                         END
                       END
!------------ End File: MPXJOBS, version 3 ------------------

!------------ File: MPXJOBS, version 4 ----------------------
!------------ modified 08.12.2005 at 15:32:31 -----------------
MOD:stFileName:MPXJOBSVer4  STRING(260)
MPXJOBSVer4            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXJOBSVer4),PRE(MPXJOBSVer4),CREATE
RecordNoKey              KEY(MPXJOBSVer4:RecordNo),NOCASE,OPT,PRIMARY
IDKey                    KEY(MPXJOBSVer4:ID),DUP,NOCASE,OPT
JobNoKey                 KEY(MPXJOBSVer4:InspACRJobNo),DUP,NOCASE,OPT
StateJobNoKey            KEY(MPXJOBSVer4:Record_State,MPXJOBSVer4:InspACRJobNo),DUP,NOCASE,OPT
StateSurnameKey          KEY(MPXJOBSVer4:Record_State,MPXJOBSVer4:CustLastName),DUP,NOCASE,OPT
StatePostcodeKey         KEY(MPXJOBSVer4:Record_State,MPXJOBSVer4:CustPostCode),DUP,NOCASE,OPT
StateModelKey            KEY(MPXJOBSVer4:Record_State,MPXJOBSVer4:ModelID),DUP,NOCASE,OPT
StateReportCodeKey       KEY(MPXJOBSVer4:Record_State,MPXJOBSVer4:ReportCode),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNo                    LONG
Record_State                BYTE
ReportCode                  LONG
Exception_Code              LONG
Exception_Desc              STRING(255)
ID                          LONG
InspACRJobNo                LONG
EnvDateSent                 DATE
InspACRStatus               STRING(50)
InspDateRecd                DATE
InspModelID                 STRING(50)
InspFlag                    BYTE
InspAttention               STRING(255)
InspQty                     LONG
InspPackageCond             STRING(50)
InspQ1                      BYTE
InspQ2                      BYTE
InspQ3                      BYTE
InspACC1                    BYTE
InspACC2                    BYTE
InspACC3                    BYTE
InspACC4                    BYTE
InspACC5                    BYTE
InspGrade                   STRING(50)
InspIMEI                    STRING(15)
InspNotes                   STRING(255)
InspDate                    DATE
InspBrand                   STRING(50)
InspInWarranty              BYTE
DespatchConsignment         STRING(50)
DespatchDate                DATE
CustCTN                     STRING(50)
QuoteValue                  LONG
ModelID                     STRING(50)
QuoteQ1                     BYTE
QuoteQ2                     BYTE
QuoteQ3                     BYTE
QuoteACC1                   BYTE
QuoteACC2                   BYTE
QuoteACC3                   BYTE
QuoteACC4                   BYTE
QuoteACC5                   BYTE
CustTitle                   STRING(50)
CustFirstName               STRING(50)
CustLastName                STRING(50)
CustAdd1                    STRING(50)
CustAdd2                    STRING(50)
CustAdd3                    STRING(50)
CustAdd4                    STRING(50)
CustPostCode                STRING(50)
CustRef                     STRING(50)
CustNetwork                 STRING(50)
EnvLetter                   STRING(50)
InspType                    STRING(50)
EnvType                     STRING(50)
EnvCount                    LONG
                         END
                       END
!------------ End File: MPXJOBS, version 4 ------------------

!------------ File: MPXJOBS, version 5 ----------------------
!------------ modified 08.12.2005 at 15:46:01 -----------------
MOD:stFileName:MPXJOBSVer5  STRING(260)
MPXJOBSVer5            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXJOBSVer5),PRE(MPXJOBSVer5),CREATE
RecordNoKey              KEY(MPXJOBSVer5:RecordNo),NOCASE,OPT,PRIMARY
IDKey                    KEY(MPXJOBSVer5:ID),DUP,NOCASE,OPT
JobNoKey                 KEY(MPXJOBSVer5:InspACRJobNo),DUP,NOCASE,OPT
StateJobNoKey            KEY(MPXJOBSVer5:Record_State,MPXJOBSVer5:InspACRJobNo),DUP,NOCASE,OPT
StateSurnameKey          KEY(MPXJOBSVer5:Record_State,MPXJOBSVer5:CustLastName),DUP,NOCASE,OPT
StatePostcodeKey         KEY(MPXJOBSVer5:Record_State,MPXJOBSVer5:CustPostCode),DUP,NOCASE,OPT
StateModelKey            KEY(MPXJOBSVer5:Record_State,MPXJOBSVer5:ModelID),DUP,NOCASE,OPT
StateReportCodeKey       KEY(MPXJOBSVer5:Record_State,MPXJOBSVer5:ReportCode),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNo                    LONG
Record_State                BYTE
ReportCode                  LONG
Exception_Code              LONG
Exception_Desc              STRING(255)
ImportDate                  DATE
ImportTime                  TIME
ID                          LONG
InspACRJobNo                LONG
EnvDateSent                 DATE
InspACRStatus               STRING(50)
InspDateRecd                DATE
InspModelID                 STRING(50)
InspFlag                    BYTE
InspAttention               STRING(255)
InspQty                     LONG
InspPackageCond             STRING(50)
InspQ1                      BYTE
InspQ2                      BYTE
InspQ3                      BYTE
InspACC1                    BYTE
InspACC2                    BYTE
InspACC3                    BYTE
InspACC4                    BYTE
InspACC5                    BYTE
InspGrade                   STRING(50)
InspIMEI                    STRING(15)
InspNotes                   STRING(255)
InspDate                    DATE
InspBrand                   STRING(50)
InspInWarranty              BYTE
DespatchConsignment         STRING(50)
DespatchDate                DATE
CustCTN                     STRING(50)
QuoteValue                  LONG
ModelID                     STRING(50)
QuoteQ1                     BYTE
QuoteQ2                     BYTE
QuoteQ3                     BYTE
QuoteACC1                   BYTE
QuoteACC2                   BYTE
QuoteACC3                   BYTE
QuoteACC4                   BYTE
QuoteACC5                   BYTE
CustTitle                   STRING(50)
CustFirstName               STRING(50)
CustLastName                STRING(50)
CustAdd1                    STRING(50)
CustAdd2                    STRING(50)
CustAdd3                    STRING(50)
CustAdd4                    STRING(50)
CustPostCode                STRING(50)
CustRef                     STRING(50)
CustNetwork                 STRING(50)
EnvLetter                   STRING(50)
InspType                    STRING(50)
EnvType                     STRING(50)
EnvCount                    LONG
                         END
                       END
!------------ End File: MPXJOBS, version 5 ------------------

!------------ File: MPXSTAT, version 1 ----------------------
!------------ modified 06.12.2005 at 10:29:19 -----------------
MOD:stFileName:MPXSTATVer1  STRING(260)
MPXSTATVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXSTATVer1),PRE(MPXSTATVer1)
RecordNoKey              KEY(MPXSTATVer1:RecordNo),NOCASE,OPT,PRIMARY
MpxStatusKey             KEY(MPXSTATVer1:MPX_STATUS),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNo                    LONG
MPX_STATUS                  LONG
SB_STATUS                   STRING(30)
                         END
                       END
!------------ End File: MPXSTAT, version 1 ------------------

!------------ File: MPXSTAT, version 2 ----------------------
!------------ modified 08.12.2005 at 09:36:26 -----------------
MOD:stFileName:MPXSTATVer2  STRING(260)
MPXSTATVer2            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXSTATVer2),PRE(MPXSTATVer2),CREATE
RecordNoKey              KEY(MPXSTATVer2:RecordNo),NOCASE,OPT,PRIMARY
MpxStatusKey             KEY(MPXSTATVer2:MPX_STATUS),DUP,NOCASE,OPT
SBStatusKey              KEY(MPXSTATVer2:SB_STATUS),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNo                    LONG
MPX_STATUS                  LONG
SB_STATUS                   STRING(30)
                         END
                       END
!------------ End File: MPXSTAT, version 2 ------------------

!------------ File: TRAFAULT, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:TRAFAULTVer1  STRING(260)
TRAFAULTVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRAFAULTVer1),PRE(TRAFAULTVer1)
RecordNumberKey          KEY(TRAFAULTVer1:RecordNumber),NOCASE,PRIMARY
Field_Number_Key         KEY(TRAFAULTVer1:AccountNumber,TRAFAULTVer1:Field_Number),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
                         END
                       END
!------------ End File: TRAFAULT, version 1 ------------------

!------------ File: TRAFAULT, version 2 ----------------------
!------------ modified 03.03.2004 at 16:07:17 -----------------
MOD:stFileName:TRAFAULTVer2  STRING(260)
TRAFAULTVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRAFAULTVer2),PRE(TRAFAULTVer2),CREATE
RecordNumberKey          KEY(TRAFAULTVer2:RecordNumber),NOCASE,PRIMARY
Field_Number_Key         KEY(TRAFAULTVer2:AccountNumber,TRAFAULTVer2:Field_Number),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
DefaultValue                STRING(30)
                         END
                       END
!------------ End File: TRAFAULT, version 2 ------------------

!------------ File: STATCRIT, version 1 ----------------------
!------------ modified 07.03.2002 at 10:55:22 -----------------
MOD:stFileName:STATCRITVer1  STRING(260)
STATCRITVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:STATCRITVer1),PRE(STATCRITVer1)
RecordNumberKey          KEY(STATCRITVer1:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(STATCRITVer1:Description),NOCASE
DescriptionOptionKey     KEY(STATCRITVer1:Description,STATCRITVer1:OptionType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(60)
OptionType                  STRING(30)
FieldValue                  STRING(40)
                         END
                       END
!------------ End File: STATCRIT, version 1 ------------------

!------------ File: STATCRIT, version 2 ----------------------
!------------ modified 07.03.2002 at 11:12:57 -----------------
MOD:stFileName:STATCRITVer2  STRING(260)
STATCRITVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:STATCRITVer2),PRE(STATCRITVer2),CREATE
RecordNumberKey          KEY(STATCRITVer2:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(STATCRITVer2:Description),DUP,NOCASE
DescriptionOptionKey     KEY(STATCRITVer2:Description,STATCRITVer2:OptionType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(60)
OptionType                  STRING(30)
FieldValue                  STRING(40)
                         END
                       END
!------------ End File: STATCRIT, version 2 ------------------

!------------ File: NOTESDEL, version 1 ----------------------
!------------ modified 15.01.2004 at 15:07:21 -----------------
MOD:stFileName:NOTESDELVer1  STRING(260)
NOTESDELVer1           FILE,DRIVER('Btrieve'),OEM,PRE(NOTESDELVer1),NAME(MOD:stFileName:NOTESDELVer1)
Record                   RECORD,PRE()
ReasonCode                  STRING(30)
ReasonText                  STRING(80)
                         END
                       END
!------------ End File: NOTESDEL, version 1 ------------------

!------------ File: PICKDET, version 1 ----------------------
!------------ modified 15.01.2004 at 15:07:21 -----------------
MOD:stFileName:PICKDETVer1  STRING(260)
PICKDETVer1            FILE,DRIVER('Btrieve'),OEM,PRE(PICKDETVer1),NAME(MOD:stFileName:PICKDETVer1)
Record                   RECORD,PRE()
PickDetailRef               LONG
PickNoteRef                 LONG
PartRefNumber               LONG
Quantity                    LONG
IsChargeable                BYTE
RequestDate                 DATE
RequestTime                 TIME
EngineerCode                STRING(3)
IsInStock                   BYTE
IsPrinted                   BYTE
IsDelete                    BYTE
DeleteReason                STRING(255)
                         END
                       END
!------------ End File: PICKDET, version 1 ------------------

!------------ File: PICKDET, version 2 ----------------------
!------------ modified 19.01.2004 at 14:29:17 -----------------
MOD:stFileName:PICKDETVer2  STRING(260)
PICKDETVer2            FILE,DRIVER('Btrieve'),OEM,PRE(PICKDETVer2),NAME(MOD:stFileName:PICKDETVer2),CREATE
keyonpicknote            KEY(PICKDETVer2:PickNoteRef),NOCASE,PRIMARY
Record                   RECORD,PRE()
PickDetailRef               LONG
PickNoteRef                 LONG
PartRefNumber               LONG
Quantity                    LONG
IsChargeable                BYTE
RequestDate                 DATE
RequestTime                 TIME
EngineerCode                STRING(3)
IsInStock                   BYTE
IsPrinted                   BYTE
IsDelete                    BYTE
DeleteReason                STRING(255)
                         END
                       END
!------------ End File: PICKDET, version 2 ------------------

!------------ File: PICKDET, version 3 ----------------------
!------------ modified 20.01.2004 at 16:15:02 -----------------
MOD:stFileName:PICKDETVer3  STRING(260)
PICKDETVer3            FILE,DRIVER('Btrieve'),OEM,PRE(PICKDETVer3),NAME(MOD:stFileName:PICKDETVer3),CREATE
keypickdet               KEY(PICKDETVer3:PickDetailRef),NOCASE,PRIMARY
keyonpicknote            KEY(PICKDETVer3:PickNoteRef),DUP,NOCASE
Record                   RECORD,PRE()
PickDetailRef               LONG
PickNoteRef                 LONG
PartRefNumber               LONG
Quantity                    LONG
IsChargeable                BYTE
RequestDate                 DATE
RequestTime                 TIME
EngineerCode                STRING(3)
IsInStock                   BYTE
IsPrinted                   BYTE
IsDelete                    BYTE
DeleteReason                STRING(255)
                         END
                       END
!------------ End File: PICKDET, version 3 ------------------

!------------ File: PICKDET, version 4 ----------------------
!------------ modified 21.01.2004 at 11:45:05 -----------------
MOD:stFileName:PICKDETVer4  STRING(260)
PICKDETVer4            FILE,DRIVER('Btrieve'),OEM,PRE(PICKDETVer4),NAME(MOD:stFileName:PICKDETVer4),CREATE
keypickdet               KEY(PICKDETVer4:PickDetailRef),NOCASE,PRIMARY
keyonpicknote            KEY(PICKDETVer4:PickNoteRef),DUP,NOCASE
Record                   RECORD,PRE()
PickDetailRef               LONG
PickNoteRef                 LONG
PartRefNumber               LONG
Quantity                    LONG
IsChargeable                BYTE
RequestDate                 DATE
RequestTime                 TIME
EngineerCode                STRING(3)
IsInStock                   BYTE
IsPrinted                   BYTE
IsDelete                    BYTE
DeleteReason                STRING(255)
Usercode                    STRING(3)
                         END
                       END
!------------ End File: PICKDET, version 4 ------------------

!------------ File: PICKDET, version 5 ----------------------
!------------ modified 15.03.2004 at 15:37:25 -----------------
MOD:stFileName:PICKDETVer5  STRING(260)
PICKDETVer5            FILE,DRIVER('Btrieve'),OEM,PRE(PICKDETVer5),NAME(MOD:stFileName:PICKDETVer5),CREATE
keypickdet               KEY(PICKDETVer5:PickDetailRef),NOCASE,PRIMARY
keyonpicknote            KEY(PICKDETVer5:PickNoteRef),DUP,NOCASE
Record                   RECORD,PRE()
PickDetailRef               LONG
PickNoteRef                 LONG
PartRefNumber               LONG
Quantity                    LONG
IsChargeable                BYTE
RequestDate                 DATE
RequestTime                 TIME
EngineerCode                STRING(3)
IsInStock                   BYTE
IsPrinted                   BYTE
IsDelete                    BYTE
DeleteReason                STRING(255)
Usercode                    STRING(3)
StockPartRefNumber          LONG
PartNumber                  LONG
                         END
                       END
!------------ End File: PICKDET, version 5 ------------------

!------------ File: USERS, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:USERSVer1  STRING(260)
USERSVer1              FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:USERSVer1),PRE(USERSVer1)
User_Code_Key            KEY(USERSVer1:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(USERSVer1:User_Type,USERSVer1:Active,USERSVer1:Surname),DUP,NOCASE
Surname_Active_Key       KEY(USERSVer1:Active,USERSVer1:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(USERSVer1:Active,USERSVer1:User_Code),DUP,NOCASE
User_Type_Key            KEY(USERSVer1:User_Type,USERSVer1:Surname),DUP,NOCASE
surname_key              KEY(USERSVer1:Surname),DUP,NOCASE
password_key             KEY(USERSVer1:Password),NOCASE
Logged_In_Key            KEY(USERSVer1:Logged_In,USERSVer1:Surname),DUP,NOCASE
Team_Surname             KEY(USERSVer1:Team,USERSVer1:Surname),DUP,NOCASE
Active_Team_Surname      KEY(USERSVer1:Team,USERSVer1:Active,USERSVer1:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
                         END
                       END
!------------ End File: USERS, version 1 ------------------

!------------ File: SIDREG, version 1 ----------------------
!------------ modified 05.09.2006 at 10:43:51 -----------------
MOD:stFileName:SIDREGVer1  STRING(260)
SIDREGVer1             FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SIDREGVer1),PRE(SIDREGVer1)
SIDRegIDKey              KEY(SIDREGVer1:SIDRegID),NOCASE,PRIMARY
SCRegionNameKey          KEY(SIDREGVer1:SCAccountID,SIDREGVer1:RegionName),NOCASE
RegionNameKey            KEY(SIDREGVer1:RegionName),DUP,NOCASE
Record                   RECORD,PRE()
SIDRegID                    LONG
SCAccountID                 LONG
RegionName                  STRING(30)
TransitDaysToSC             LONG
TransitDaysToStore          LONG
                         END
                       END
!------------ End File: SIDREG, version 1 ------------------

!------------ File: PENDMAIL, version 1 ----------------------
!------------ modified 22.02.2006 at 16:43:30 -----------------
MOD:stFileName:PENDMAILVer1  STRING(260)
PENDMAILVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer1),PRE(PENDMAILVer1)
RecordNumberKey          KEY(PENDMAILVer1:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer1:DateCreated,-PENDMAILVer1:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer1:MessageType,-PENDMAILVer1:DateCreated,-PENDMAILVer1:TimeCreated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(4)
RefNumber                   LONG
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
                         END
                       END
!------------ End File: PENDMAIL, version 1 ------------------

!------------ File: PENDMAIL, version 2 ----------------------
!------------ modified 24.02.2006 at 16:24:30 -----------------
MOD:stFileName:PENDMAILVer2  STRING(260)
PENDMAILVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer2),PRE(PENDMAILVer2),CREATE
RecordNumberKey          KEY(PENDMAILVer2:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer2:DateCreated,-PENDMAILVer2:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer2:MessageType,-PENDMAILVer2:DateCreated,-PENDMAILVer2:TimeCreated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
                         END
                       END
!------------ End File: PENDMAIL, version 2 ------------------

!------------ File: PENDMAIL, version 3 ----------------------
!------------ modified 27.02.2006 at 11:20:24 -----------------
MOD:stFileName:PENDMAILVer3  STRING(260)
PENDMAILVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer3),PRE(PENDMAILVer3),CREATE
RecordNumberKey          KEY(PENDMAILVer3:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer3:DateCreated,-PENDMAILVer3:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer3:MessageType,-PENDMAILVer3:DateCreated,-PENDMAILVer3:TimeCreated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
                         END
                       END
!------------ End File: PENDMAIL, version 3 ------------------

!------------ File: PENDMAIL, version 4 ----------------------
!------------ modified 28.02.2006 at 10:57:42 -----------------
MOD:stFileName:PENDMAILVer4  STRING(260)
PENDMAILVer4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer4),PRE(PENDMAILVer4),CREATE
RecordNumberKey          KEY(PENDMAILVer4:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer4:DateCreated,-PENDMAILVer4:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer4:MessageType,-PENDMAILVer4:DateCreated,-PENDMAILVer4:TimeCreated),DUP,NOCASE
MessageSentKey           KEY(PENDMAILVer4:MessageType,PENDMAILVer4:DateSent),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
                         END
                       END
!------------ End File: PENDMAIL, version 4 ------------------

!------------ File: PENDMAIL, version 5 ----------------------
!------------ modified 28.02.2006 at 11:12:54 -----------------
MOD:stFileName:PENDMAILVer5  STRING(260)
PENDMAILVer5           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer5),PRE(PENDMAILVer5),CREATE
RecordNumberKey          KEY(PENDMAILVer5:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer5:DateCreated,-PENDMAILVer5:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer5:MessageType,-PENDMAILVer5:DateCreated,-PENDMAILVer5:TimeCreated),DUP,NOCASE
SMSSentKey               KEY(PENDMAILVer5:SMSEmail,PENDMAILVer5:DateSent),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
                         END
                       END
!------------ End File: PENDMAIL, version 5 ------------------

!------------ File: PENDMAIL, version 6 ----------------------
!------------ modified 27.07.2006 at 14:29:43 -----------------
MOD:stFileName:PENDMAILVer6  STRING(260)
PENDMAILVer6           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer6),PRE(PENDMAILVer6),CREATE
RecordNumberKey          KEY(PENDMAILVer6:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer6:DateCreated,-PENDMAILVer6:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer6:MessageType,-PENDMAILVer6:DateCreated,-PENDMAILVer6:TimeCreated),DUP,NOCASE
SMSSentKey               KEY(PENDMAILVer6:SMSEmail,PENDMAILVer6:DateSent),DUP,NOCASE
MessageRefKey            KEY(PENDMAILVer6:MessageType,PENDMAILVer6:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
                         END
                       END
!------------ End File: PENDMAIL, version 6 ------------------

!------------ File: PENDMAIL, version 7 ----------------------
!------------ modified 27.07.2006 at 15:15:05 -----------------
MOD:stFileName:PENDMAILVer7  STRING(260)
PENDMAILVer7           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer7),PRE(PENDMAILVer7),CREATE
RecordNumberKey          KEY(PENDMAILVer7:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer7:DateCreated,-PENDMAILVer7:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer7:MessageType,-PENDMAILVer7:DateCreated,-PENDMAILVer7:TimeCreated),DUP,NOCASE
SMSSentKey               KEY(PENDMAILVer7:SMSEmail,PENDMAILVer7:DateSent),DUP,NOCASE
MessageRefKey            KEY(PENDMAILVer7:MessageType,PENDMAILVer7:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
Dummy                       BYTE
                         END
                       END
!------------ End File: PENDMAIL, version 7 ------------------

!------------ File: PENDMAIL, version 8 ----------------------
!------------ modified 20.09.2007 at 15:46:54 -----------------
MOD:stFileName:PENDMAILVer8  STRING(260)
PENDMAILVer8           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer8),PRE(PENDMAILVer8),CREATE
RecordNumberKey          KEY(PENDMAILVer8:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer8:DateCreated,-PENDMAILVer8:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer8:MessageType,-PENDMAILVer8:DateCreated,-PENDMAILVer8:TimeCreated),DUP,NOCASE
SMSSentKey               KEY(PENDMAILVer8:SMSEmail,PENDMAILVer8:DateSent),DUP,NOCASE
MessageRefKey            KEY(PENDMAILVer8:MessageType,PENDMAILVer8:RefNumber),DUP,NOCASE
MessageJobNumber         KEY(PENDMAILVer8:MessageType,PENDMAILVer8:SIDJobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
BookingType                 STRING(30)
Status                      STRING(30)
SIDJobNumber                STRING(30)
                         END
                       END
!------------ End File: PENDMAIL, version 8 ------------------

!------------ File: PENDMAIL, version 9 ----------------------
!------------ modified 20.09.2007 at 15:59:36 -----------------
MOD:stFileName:PENDMAILVer9  STRING(260)
PENDMAILVer9           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer9),PRE(PENDMAILVer9),CREATE
RecordNumberKey          KEY(PENDMAILVer9:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer9:DateCreated,-PENDMAILVer9:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer9:MessageType,-PENDMAILVer9:DateCreated,-PENDMAILVer9:TimeCreated),DUP,NOCASE
SMSSentKey               KEY(PENDMAILVer9:SMSEmail,PENDMAILVer9:DateSent),DUP,NOCASE
MessageRefKey            KEY(PENDMAILVer9:MessageType,PENDMAILVer9:RefNumber),DUP,NOCASE
JobNumberKey             KEY(PENDMAILVer9:SIDJobNumber),DUP,NOCASE
RefNumberKey             KEY(PENDMAILVer9:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
BookingType                 STRING(30)
Status                      STRING(30)
SIDJobNumber                STRING(30)
                         END
                       END
!------------ End File: PENDMAIL, version 9 ------------------

!------------ File: PENDMAIL, version 10 ----------------------
!------------ modified 24.09.2007 at 13:32:41 -----------------
MOD:stFileName:PENDMAILVer10  STRING(260)
PENDMAILVer10          FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer10),PRE(PENDMAILVer10),CREATE
RecordNumberKey          KEY(PENDMAILVer10:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer10:DateCreated,-PENDMAILVer10:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer10:MessageType,-PENDMAILVer10:DateCreated,-PENDMAILVer10:TimeCreated),DUP,NOCASE
SMSSentKey               KEY(PENDMAILVer10:SMSEmail,PENDMAILVer10:DateSent),DUP,NOCASE
MessageRefKey            KEY(PENDMAILVer10:MessageType,PENDMAILVer10:RefNumber),DUP,NOCASE
JobNumberKey             KEY(PENDMAILVer10:SIDJobNumber),DUP,NOCASE
RefNumberKey             KEY(PENDMAILVer10:RefNumber),DUP,NOCASE
ReminderKey              KEY(PENDMAILVer10:Reminder),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
BookingType                 STRING(30)
Status                      STRING(30)
SIDJobNumber                STRING(30)
Reminder                    BYTE
                         END
                       END
!------------ End File: PENDMAIL, version 10 ------------------

!------------ File: PENDMAIL, version 11 ----------------------
!------------ modified 24.09.2007 at 16:26:37 -----------------
MOD:stFileName:PENDMAILVer11  STRING(260)
PENDMAILVer11          FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer11),PRE(PENDMAILVer11),CREATE
RecordNumberKey          KEY(PENDMAILVer11:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer11:DateCreated,-PENDMAILVer11:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer11:MessageType,-PENDMAILVer11:DateCreated,-PENDMAILVer11:TimeCreated),DUP,NOCASE
SMSSentKey               KEY(PENDMAILVer11:SMSEmail,PENDMAILVer11:DateSent),DUP,NOCASE
MessageRefKey            KEY(PENDMAILVer11:MessageType,PENDMAILVer11:RefNumber),DUP,NOCASE
JobNumberKey             KEY(PENDMAILVer11:SIDJobNumber),DUP,NOCASE
RefNumberKey             KEY(PENDMAILVer11:RefNumber),DUP,NOCASE
ReminderKey              KEY(PENDMAILVer11:Reminder),DUP,NOCASE
ReminderRefNumberKey     KEY(PENDMAILVer11:Reminder,PENDMAILVer11:RefNumber),DUP,NOCASE
ReminderSIDJobKey        KEY(PENDMAILVer11:Reminder,PENDMAILVer11:SIDJobNumber),DUP,NOCASE
ReminderDateCreatedKey   KEY(PENDMAILVer11:Reminder,PENDMAILVer11:DateCreated,PENDMAILVer11:TimeCreated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
BookingType                 STRING(30)
Status                      STRING(30)
SIDJobNumber                STRING(30)
Reminder                    BYTE
ReminderDescription         STRING(60)
                         END
                       END
!------------ End File: PENDMAIL, version 11 ------------------

!------------ File: PENDMAIL, version 12 ----------------------
!------------ modified 25.09.2007 at 10:42:37 -----------------
MOD:stFileName:PENDMAILVer12  STRING(260)
PENDMAILVer12          FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer12),PRE(PENDMAILVer12),CREATE
RecordNumberKey          KEY(PENDMAILVer12:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer12:DateCreated,-PENDMAILVer12:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer12:MessageType,-PENDMAILVer12:DateCreated,-PENDMAILVer12:TimeCreated),DUP,NOCASE
SMSSentKey               KEY(PENDMAILVer12:SMSEmail,PENDMAILVer12:DateSent),DUP,NOCASE
MessageRefKey            KEY(PENDMAILVer12:MessageType,PENDMAILVer12:RefNumber),DUP,NOCASE
JobNumberKey             KEY(PENDMAILVer12:SIDJobNumber),DUP,NOCASE
RefNumberKey             KEY(PENDMAILVer12:RefNumber),DUP,NOCASE
ReminderKey              KEY(PENDMAILVer12:Reminder),DUP,NOCASE
ReminderRefNumberKey     KEY(PENDMAILVer12:Reminder,PENDMAILVer12:RefNumber),DUP,NOCASE
ReminderSIDJobKey        KEY(PENDMAILVer12:Reminder,PENDMAILVer12:SIDJobNumber),DUP,NOCASE
ReminderDateCreatedKey   KEY(PENDMAILVer12:Reminder,PENDMAILVer12:DateCreated,PENDMAILVer12:TimeCreated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
BookingType                 STRING(30)
Status                      STRING(30)
SIDJobNumber                STRING(30)
Reminder                    BYTE
ReminderDescription         STRING(60)
CustomerUTL                 STRING(8)
                         END
                       END
!------------ End File: PENDMAIL, version 12 ------------------

!------------ File: PENDMAIL, version 13 ----------------------
!------------ modified 25.09.2007 at 11:22:56 -----------------
MOD:stFileName:PENDMAILVer13  STRING(260)
PENDMAILVer13          FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:PENDMAILVer13),PRE(PENDMAILVer13),CREATE
RecordNumberKey          KEY(PENDMAILVer13:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer13:DateCreated,-PENDMAILVer13:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer13:MessageType,-PENDMAILVer13:DateCreated,-PENDMAILVer13:TimeCreated),DUP,NOCASE
SMSSentKey               KEY(PENDMAILVer13:SMSEmail,PENDMAILVer13:DateSent),DUP,NOCASE
MessageRefKey            KEY(PENDMAILVer13:MessageType,PENDMAILVer13:RefNumber),DUP,NOCASE
JobNumberKey             KEY(PENDMAILVer13:SIDJobNumber),DUP,NOCASE
RefNumberKey             KEY(PENDMAILVer13:RefNumber),DUP,NOCASE
ReminderKey              KEY(PENDMAILVer13:Reminder),DUP,NOCASE
ReminderRefNumberKey     KEY(PENDMAILVer13:Reminder,PENDMAILVer13:RefNumber),DUP,NOCASE
ReminderSIDJobKey        KEY(PENDMAILVer13:Reminder,PENDMAILVer13:SIDJobNumber),DUP,NOCASE
ReminderDateCreatedKey   KEY(PENDMAILVer13:Reminder,PENDMAILVer13:DateCreated,PENDMAILVer13:TimeCreated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
BookingType                 STRING(30)
Status                      STRING(30)
SIDJobNumber                STRING(30)
Reminder                    BYTE
CustomerUTL                 STRING(8)
                         END
                       END
!------------ End File: PENDMAIL, version 13 ------------------

!------------ File: DEFAULTS, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:DEFAULTSVer1  STRING(260)
DEFAULTSVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:DEFAULTSVer1),PRE(DEFAULTSVer1)
RecordNumberKey          KEY(DEFAULTSVer1:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Name                   STRING(30)
record_number               REAL
Version_Number              STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
VAT_Number                  STRING(30)
Use_Invoice_Address         STRING(3)
Use_For_Order               STRING(3)
Invoice_Company_Name        STRING(30)
Invoice_Address_Line1       STRING(30)
Invoice_Address_Line2       STRING(30)
Invoice_Address_Line3       STRING(30)
Invoice_Postcode            STRING(15)
Invoice_Telephone_Number    STRING(15)
Invoice_Fax_Number          STRING(15)
InvoiceEmailAddress         STRING(255)
Invoice_VAT_Number          STRING(30)
OrderCompanyName            STRING(30)
OrderAddressLine1           STRING(30)
OrderAddressLine2           STRING(30)
OrderAddressLine3           STRING(30)
OrderPostcode               STRING(30)
OrderTelephoneNumber        STRING(30)
OrderFaxNumber              STRING(30)
OrderEmailAddress           STRING(255)
Use_Postcode                STRING(3)
Postcode_Path               STRING(255)
PostcodeDll                 STRING(3)
Vat_Rate_Labour             STRING(2)
Vat_Rate_Parts              STRING(2)
License_Number              STRING(10)
Maximum_Users               REAL
Warranty_Period             REAL
Automatic_Replicate         STRING(3)
Automatic_Replicate_Field   STRING(15)
Estimate_If_Over            REAL
Start_Work_Hours            TIME
End_Work_Hours              TIME
Include_Saturday            STRING(3)
Include_Sunday              STRING(3)
Show_Mobile_Number          STRING(3)
Show_Loan_Exchange_Details  STRING(3)
Use_Credit_Limit            STRING(3)
Hide_Physical_Damage        STRING(3)
Hide_Insurance              STRING(3)
Hide_Authority_Number       STRING(3)
Hide_Advance_Payment        STRING(3)
HideColour                  STRING(3)
HideInCourier               STRING(3)
HideIntFault                STRING(3)
HideProduct                 STRING(3)
HideLocation                BYTE
Allow_Bouncer               STRING(3)
Job_Batch_Number            REAL
QA_Required                 STRING(3)
QA_Before_Complete          STRING(3)
QAPreliminary               STRING(3)
QAExchLoan                  STRING(3)
CompleteAtQA                BYTE
RapidQA                     STRING(3)
ValidateESN                 STRING(3)
ValidateDesp                STRING(3)
Force_Accessory_Check       STRING(3)
Force_Initial_Transit_Type  STRING(1)
Force_Mobile_Number         STRING(1)
Force_Model_Number          STRING(1)
Force_Unit_Type             STRING(1)
Force_Fault_Description     STRING(1)
Force_ESN                   STRING(1)
Force_MSN                   STRING(1)
Force_Job_Type              STRING(1)
Force_Engineer              STRING(1)
Force_Invoice_Text          STRING(1)
Force_Repair_Type           STRING(1)
Force_Authority_Number      STRING(1)
Force_Fault_Coding          STRING(1)
Force_Spares                STRING(1)
Force_Incoming_Courier      STRING(1)
Force_Outoing_Courier       STRING(1)
Force_DOP                   STRING(1)
Order_Number                STRING(1)
Customer_Name               STRING(1)
ForcePostcode               STRING(1)
ForceDelPostcode            STRING(1)
ForceCommonFault            STRING(1)
Remove_Backgrounds          STRING(3)
Use_Loan_Exchange_Label     STRING(3)
Use_Job_Label               STRING(3)
UseSmallLabel               BYTE
Job_Label                   STRING(255)
add_stock_label             STRING(3)
receive_stock_label         STRING(3)
QA_Failed_Label             STRING(3)
QA_Failed_Report            STRING(3)
QAPassLabel                 BYTE
ThirdPartyNote              STRING(3)
Vodafone_Import_Path        STRING(255)
Label_Printer_Type          STRING(30)
Browse_Option               STRING(3)
ANCCollectionNo             LONG
Use_Sage                    STRING(3)
Global_Nominal_Code         STRING(30)
Parts_Stock_Code            STRING(30)
Labour_Stock_Code           STRING(30)
Courier_Stock_Code          STRING(30)
Parts_Description           STRING(60)
Labour_Description          STRING(60)
Courier_Description         STRING(60)
Parts_Code                  STRING(30)
Labour_Code                 STRING(30)
Courier_Code                STRING(30)
User_Name_Sage              STRING(30)
Password_Sage               STRING(30)
Company_Number_Sage         STRING(30)
Path_Sage                   STRING(255)
BouncerTime                 LONG
ExportPath                  STRING(255)
PrintBarcode                BYTE
ChaChargeType               STRING(30)
WarChargeType               STRING(30)
RetBackOrders               BYTE
RemoveWorkshopDespatch      BYTE
SummaryOrders               BYTE
EuroRate                    REAL
IrishVatRate                REAL
EmailServerAddress          STRING(255)
EmailServerPort             LONG
Job_Label_Accessories       STRING(3)
ShowRepTypeCosts            BYTE
                         END
                       END
!------------ End File: DEFAULTS, version 1 ------------------

!------------ File: DEFAULTS, version 2 ----------------------
!------------ modified 15.01.2004 at 15:07:22 -----------------
MOD:stFileName:DEFAULTSVer2  STRING(260)
DEFAULTSVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:DEFAULTSVer2),PRE(DEFAULTSVer2),CREATE
RecordNumberKey          KEY(DEFAULTSVer2:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Name                   STRING(30)
PickNoteNormal              BYTE
PickNoteMainStore           BYTE
PickNoteChangeStatus        BYTE
PickNoteJobStatus           STRING(30)
record_number               REAL
Version_Number              STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
VAT_Number                  STRING(30)
Use_Invoice_Address         STRING(3)
Use_For_Order               STRING(3)
Invoice_Company_Name        STRING(30)
Invoice_Address_Line1       STRING(30)
Invoice_Address_Line2       STRING(30)
Invoice_Address_Line3       STRING(30)
Invoice_Postcode            STRING(15)
Invoice_Telephone_Number    STRING(15)
Invoice_Fax_Number          STRING(15)
InvoiceEmailAddress         STRING(255)
Invoice_VAT_Number          STRING(30)
OrderCompanyName            STRING(30)
OrderAddressLine1           STRING(30)
OrderAddressLine2           STRING(30)
OrderAddressLine3           STRING(30)
OrderPostcode               STRING(30)
OrderTelephoneNumber        STRING(30)
OrderFaxNumber              STRING(30)
OrderEmailAddress           STRING(255)
Use_Postcode                STRING(3)
Postcode_Path               STRING(255)
PostcodeDll                 STRING(3)
Vat_Rate_Labour             STRING(2)
Vat_Rate_Parts              STRING(2)
License_Number              STRING(10)
Maximum_Users               REAL
Warranty_Period             REAL
Automatic_Replicate         STRING(3)
Automatic_Replicate_Field   STRING(15)
Estimate_If_Over            REAL
Start_Work_Hours            TIME
End_Work_Hours              TIME
Include_Saturday            STRING(3)
Include_Sunday              STRING(3)
Show_Mobile_Number          STRING(3)
Show_Loan_Exchange_Details  STRING(3)
Use_Credit_Limit            STRING(3)
Hide_Physical_Damage        STRING(3)
Hide_Insurance              STRING(3)
Hide_Authority_Number       STRING(3)
Hide_Advance_Payment        STRING(3)
HideColour                  STRING(3)
HideInCourier               STRING(3)
HideIntFault                STRING(3)
HideProduct                 STRING(3)
HideLocation                BYTE
Allow_Bouncer               STRING(3)
Job_Batch_Number            REAL
QA_Required                 STRING(3)
QA_Before_Complete          STRING(3)
QAPreliminary               STRING(3)
QAExchLoan                  STRING(3)
CompleteAtQA                BYTE
RapidQA                     STRING(3)
ValidateESN                 STRING(3)
ValidateDesp                STRING(3)
Force_Accessory_Check       STRING(3)
Force_Initial_Transit_Type  STRING(1)
Force_Mobile_Number         STRING(1)
Force_Model_Number          STRING(1)
Force_Unit_Type             STRING(1)
Force_Fault_Description     STRING(1)
Force_ESN                   STRING(1)
Force_MSN                   STRING(1)
Force_Job_Type              STRING(1)
Force_Engineer              STRING(1)
Force_Invoice_Text          STRING(1)
Force_Repair_Type           STRING(1)
Force_Authority_Number      STRING(1)
Force_Fault_Coding          STRING(1)
Force_Spares                STRING(1)
Force_Incoming_Courier      STRING(1)
Force_Outoing_Courier       STRING(1)
Force_DOP                   STRING(1)
Order_Number                STRING(1)
Customer_Name               STRING(1)
ForcePostcode               STRING(1)
ForceDelPostcode            STRING(1)
ForceCommonFault            STRING(1)
Remove_Backgrounds          STRING(3)
Use_Loan_Exchange_Label     STRING(3)
Use_Job_Label               STRING(3)
UseSmallLabel               BYTE
Job_Label                   STRING(255)
add_stock_label             STRING(3)
receive_stock_label         STRING(3)
QA_Failed_Label             STRING(3)
QA_Failed_Report            STRING(3)
QAPassLabel                 BYTE
ThirdPartyNote              STRING(3)
Vodafone_Import_Path        STRING(255)
Label_Printer_Type          STRING(30)
Browse_Option               STRING(3)
ANCCollectionNo             LONG
Use_Sage                    STRING(3)
Global_Nominal_Code         STRING(30)
Parts_Stock_Code            STRING(30)
Labour_Stock_Code           STRING(30)
Courier_Stock_Code          STRING(30)
Parts_Description           STRING(60)
Labour_Description          STRING(60)
Courier_Description         STRING(60)
Parts_Code                  STRING(30)
Labour_Code                 STRING(30)
Courier_Code                STRING(30)
User_Name_Sage              STRING(30)
Password_Sage               STRING(30)
Company_Number_Sage         STRING(30)
Path_Sage                   STRING(255)
BouncerTime                 LONG
ExportPath                  STRING(255)
PrintBarcode                BYTE
ChaChargeType               STRING(30)
WarChargeType               STRING(30)
RetBackOrders               BYTE
RemoveWorkshopDespatch      BYTE
SummaryOrders               BYTE
EuroRate                    REAL
IrishVatRate                REAL
EmailServerAddress          STRING(255)
EmailServerPort             LONG
Job_Label_Accessories       STRING(3)
ShowRepTypeCosts            BYTE
                         END
                       END
!------------ End File: DEFAULTS, version 2 ------------------

!------------ File: DEFAULTS, version 3 ----------------------
!------------ modified 16.01.2004 at 13:55:32 -----------------
MOD:stFileName:DEFAULTSVer3  STRING(260)
DEFAULTSVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:DEFAULTSVer3),PRE(DEFAULTSVer3),CREATE
RecordNumberKey          KEY(DEFAULTSVer3:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Name                   STRING(30)
record_number               REAL
Version_Number              STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
VAT_Number                  STRING(30)
Use_Invoice_Address         STRING(3)
Use_For_Order               STRING(3)
Invoice_Company_Name        STRING(30)
Invoice_Address_Line1       STRING(30)
Invoice_Address_Line2       STRING(30)
Invoice_Address_Line3       STRING(30)
Invoice_Postcode            STRING(15)
Invoice_Telephone_Number    STRING(15)
Invoice_Fax_Number          STRING(15)
InvoiceEmailAddress         STRING(255)
Invoice_VAT_Number          STRING(30)
OrderCompanyName            STRING(30)
OrderAddressLine1           STRING(30)
OrderAddressLine2           STRING(30)
OrderAddressLine3           STRING(30)
OrderPostcode               STRING(30)
OrderTelephoneNumber        STRING(30)
OrderFaxNumber              STRING(30)
OrderEmailAddress           STRING(255)
Use_Postcode                STRING(3)
Postcode_Path               STRING(255)
PostcodeDll                 STRING(3)
Vat_Rate_Labour             STRING(2)
Vat_Rate_Parts              STRING(2)
License_Number              STRING(10)
Maximum_Users               REAL
Warranty_Period             REAL
Automatic_Replicate         STRING(3)
Automatic_Replicate_Field   STRING(15)
Estimate_If_Over            REAL
Start_Work_Hours            TIME
End_Work_Hours              TIME
Include_Saturday            STRING(3)
Include_Sunday              STRING(3)
Show_Mobile_Number          STRING(3)
Show_Loan_Exchange_Details  STRING(3)
Use_Credit_Limit            STRING(3)
Hide_Physical_Damage        STRING(3)
Hide_Insurance              STRING(3)
Hide_Authority_Number       STRING(3)
Hide_Advance_Payment        STRING(3)
HideColour                  STRING(3)
HideInCourier               STRING(3)
HideIntFault                STRING(3)
HideProduct                 STRING(3)
HideLocation                BYTE
Allow_Bouncer               STRING(3)
Job_Batch_Number            REAL
QA_Required                 STRING(3)
QA_Before_Complete          STRING(3)
QAPreliminary               STRING(3)
QAExchLoan                  STRING(3)
CompleteAtQA                BYTE
RapidQA                     STRING(3)
ValidateESN                 STRING(3)
ValidateDesp                STRING(3)
Force_Accessory_Check       STRING(3)
Force_Initial_Transit_Type  STRING(1)
Force_Mobile_Number         STRING(1)
Force_Model_Number          STRING(1)
Force_Unit_Type             STRING(1)
Force_Fault_Description     STRING(1)
Force_ESN                   STRING(1)
Force_MSN                   STRING(1)
Force_Job_Type              STRING(1)
Force_Engineer              STRING(1)
Force_Invoice_Text          STRING(1)
Force_Repair_Type           STRING(1)
Force_Authority_Number      STRING(1)
Force_Fault_Coding          STRING(1)
Force_Spares                STRING(1)
Force_Incoming_Courier      STRING(1)
Force_Outoing_Courier       STRING(1)
Force_DOP                   STRING(1)
Order_Number                STRING(1)
Customer_Name               STRING(1)
ForcePostcode               STRING(1)
ForceDelPostcode            STRING(1)
ForceCommonFault            STRING(1)
Remove_Backgrounds          STRING(3)
Use_Loan_Exchange_Label     STRING(3)
Use_Job_Label               STRING(3)
UseSmallLabel               BYTE
Job_Label                   STRING(255)
add_stock_label             STRING(3)
receive_stock_label         STRING(3)
QA_Failed_Label             STRING(3)
QA_Failed_Report            STRING(3)
QAPassLabel                 BYTE
ThirdPartyNote              STRING(3)
Vodafone_Import_Path        STRING(255)
Label_Printer_Type          STRING(30)
Browse_Option               STRING(3)
ANCCollectionNo             LONG
Use_Sage                    STRING(3)
Global_Nominal_Code         STRING(30)
Parts_Stock_Code            STRING(30)
Labour_Stock_Code           STRING(30)
Courier_Stock_Code          STRING(30)
Parts_Description           STRING(60)
Labour_Description          STRING(60)
Courier_Description         STRING(60)
Parts_Code                  STRING(30)
Labour_Code                 STRING(30)
Courier_Code                STRING(30)
User_Name_Sage              STRING(30)
Password_Sage               STRING(30)
Company_Number_Sage         STRING(30)
Path_Sage                   STRING(255)
BouncerTime                 LONG
ExportPath                  STRING(255)
PrintBarcode                BYTE
ChaChargeType               STRING(30)
WarChargeType               STRING(30)
RetBackOrders               BYTE
RemoveWorkshopDespatch      BYTE
SummaryOrders               BYTE
EuroRate                    REAL
IrishVatRate                REAL
EmailServerAddress          STRING(255)
EmailServerPort             LONG
Job_Label_Accessories       STRING(3)
ShowRepTypeCosts            BYTE
PickNoteNormal              BYTE
PickNoteMainStore           BYTE
PickNoteChangeStatus        BYTE
PickNoteJobStatus           STRING(30)
                         END
                       END
!------------ End File: DEFAULTS, version 3 ------------------

!------------ File: DEFAULTS, version 4 ----------------------
!------------ modified 03.03.2004 at 16:07:18 -----------------
MOD:stFileName:DEFAULTSVer4  STRING(260)
DEFAULTSVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:DEFAULTSVer4),PRE(DEFAULTSVer4),CREATE
RecordNumberKey          KEY(DEFAULTSVer4:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Name                   STRING(30)
record_number               REAL
Version_Number              STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
VAT_Number                  STRING(30)
Use_Invoice_Address         STRING(3)
Use_For_Order               STRING(3)
Invoice_Company_Name        STRING(30)
Invoice_Address_Line1       STRING(30)
Invoice_Address_Line2       STRING(30)
Invoice_Address_Line3       STRING(30)
Invoice_Postcode            STRING(15)
Invoice_Telephone_Number    STRING(15)
Invoice_Fax_Number          STRING(15)
InvoiceEmailAddress         STRING(255)
Invoice_VAT_Number          STRING(30)
OrderCompanyName            STRING(30)
OrderAddressLine1           STRING(30)
OrderAddressLine2           STRING(30)
OrderAddressLine3           STRING(30)
OrderPostcode               STRING(30)
OrderTelephoneNumber        STRING(30)
OrderFaxNumber              STRING(30)
OrderEmailAddress           STRING(255)
Use_Postcode                STRING(3)
Postcode_Path               STRING(255)
PostcodeDll                 STRING(3)
Vat_Rate_Labour             STRING(2)
Vat_Rate_Parts              STRING(2)
License_Number              STRING(10)
Maximum_Users               REAL
Warranty_Period             REAL
Automatic_Replicate         STRING(3)
Automatic_Replicate_Field   STRING(15)
Estimate_If_Over            REAL
Start_Work_Hours            TIME
End_Work_Hours              TIME
Include_Saturday            STRING(3)
Include_Sunday              STRING(3)
Show_Mobile_Number          STRING(3)
Show_Loan_Exchange_Details  STRING(3)
Use_Credit_Limit            STRING(3)
Hide_Physical_Damage        STRING(3)
Hide_Insurance              STRING(3)
Hide_Authority_Number       STRING(3)
Hide_Advance_Payment        STRING(3)
HideColour                  STRING(3)
HideInCourier               STRING(3)
HideIntFault                STRING(3)
HideProduct                 STRING(3)
HideLocation                BYTE
Allow_Bouncer               STRING(3)
Job_Batch_Number            REAL
QA_Required                 STRING(3)
QA_Before_Complete          STRING(3)
QAPreliminary               STRING(3)
QAExchLoan                  STRING(3)
CompleteAtQA                BYTE
RapidQA                     STRING(3)
ValidateESN                 STRING(3)
ValidateDesp                STRING(3)
Force_Accessory_Check       STRING(3)
Force_Initial_Transit_Type  STRING(1)
Force_Mobile_Number         STRING(1)
Force_Model_Number          STRING(1)
Force_Unit_Type             STRING(1)
Force_Fault_Description     STRING(1)
Force_ESN                   STRING(1)
Force_MSN                   STRING(1)
Force_Job_Type              STRING(1)
Force_Engineer              STRING(1)
Force_Invoice_Text          STRING(1)
Force_Repair_Type           STRING(1)
Force_Authority_Number      STRING(1)
Force_Fault_Coding          STRING(1)
Force_Spares                STRING(1)
Force_Incoming_Courier      STRING(1)
Force_Outoing_Courier       STRING(1)
Force_DOP                   STRING(1)
Order_Number                STRING(1)
Customer_Name               STRING(1)
ForcePostcode               STRING(1)
ForceDelPostcode            STRING(1)
ForceCommonFault            STRING(1)
Remove_Backgrounds          STRING(3)
Use_Loan_Exchange_Label     STRING(3)
Use_Job_Label               STRING(3)
UseSmallLabel               BYTE
Job_Label                   STRING(255)
add_stock_label             STRING(3)
receive_stock_label         STRING(3)
QA_Failed_Label             STRING(3)
QA_Failed_Report            STRING(3)
QAPassLabel                 BYTE
ThirdPartyNote              STRING(3)
Vodafone_Import_Path        STRING(255)
Label_Printer_Type          STRING(30)
Browse_Option               STRING(3)
ANCCollectionNo             LONG
Use_Sage                    STRING(3)
Global_Nominal_Code         STRING(30)
Parts_Stock_Code            STRING(30)
Labour_Stock_Code           STRING(30)
Courier_Stock_Code          STRING(30)
Parts_Description           STRING(60)
Labour_Description          STRING(60)
Courier_Description         STRING(60)
Parts_Code                  STRING(30)
Labour_Code                 STRING(30)
Courier_Code                STRING(30)
User_Name_Sage              STRING(30)
Password_Sage               STRING(30)
Company_Number_Sage         STRING(30)
Path_Sage                   STRING(255)
BouncerTime                 LONG
ExportPath                  STRING(255)
PrintBarcode                BYTE
ChaChargeType               STRING(30)
WarChargeType               STRING(30)
RetBackOrders               BYTE
RemoveWorkshopDespatch      BYTE
SummaryOrders               BYTE
EuroRate                    REAL
IrishVatRate                REAL
EmailServerAddress          STRING(255)
EmailServerPort             LONG
Job_Label_Accessories       STRING(3)
ShowRepTypeCosts            BYTE
PickNoteNormal              BYTE
PickNoteMainStore           BYTE
PickNoteChangeStatus        BYTE
PickNoteJobStatus           STRING(30)
UseMultipleJobBookingDefaults BYTE
                         END
                       END
!------------ End File: DEFAULTS, version 4 ------------------

!------------ File: DEFAULTS, version 5 ----------------------
!------------ modified 04.03.2004 at 09:33:29 -----------------
MOD:stFileName:DEFAULTSVer5  STRING(260)
DEFAULTSVer5           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:DEFAULTSVer5),PRE(DEFAULTSVer5),CREATE
RecordNumberKey          KEY(DEFAULTSVer5:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Name                   STRING(30)
record_number               REAL
Version_Number              STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
VAT_Number                  STRING(30)
Use_Invoice_Address         STRING(3)
Use_For_Order               STRING(3)
Invoice_Company_Name        STRING(30)
Invoice_Address_Line1       STRING(30)
Invoice_Address_Line2       STRING(30)
Invoice_Address_Line3       STRING(30)
Invoice_Postcode            STRING(15)
Invoice_Telephone_Number    STRING(15)
Invoice_Fax_Number          STRING(15)
InvoiceEmailAddress         STRING(255)
Invoice_VAT_Number          STRING(30)
OrderCompanyName            STRING(30)
OrderAddressLine1           STRING(30)
OrderAddressLine2           STRING(30)
OrderAddressLine3           STRING(30)
OrderPostcode               STRING(30)
OrderTelephoneNumber        STRING(30)
OrderFaxNumber              STRING(30)
OrderEmailAddress           STRING(255)
Use_Postcode                STRING(3)
Postcode_Path               STRING(255)
PostcodeDll                 STRING(3)
Vat_Rate_Labour             STRING(2)
Vat_Rate_Parts              STRING(2)
License_Number              STRING(10)
Maximum_Users               REAL
Warranty_Period             REAL
Automatic_Replicate         STRING(3)
Automatic_Replicate_Field   STRING(15)
Estimate_If_Over            REAL
Start_Work_Hours            TIME
End_Work_Hours              TIME
Include_Saturday            STRING(3)
Include_Sunday              STRING(3)
Show_Mobile_Number          STRING(3)
Show_Loan_Exchange_Details  STRING(3)
Use_Credit_Limit            STRING(3)
Hide_Physical_Damage        STRING(3)
Hide_Insurance              STRING(3)
Hide_Authority_Number       STRING(3)
Hide_Advance_Payment        STRING(3)
HideColour                  STRING(3)
HideInCourier               STRING(3)
HideIntFault                STRING(3)
HideProduct                 STRING(3)
HideLocation                BYTE
Allow_Bouncer               STRING(3)
Job_Batch_Number            REAL
QA_Required                 STRING(3)
QA_Before_Complete          STRING(3)
QAPreliminary               STRING(3)
QAExchLoan                  STRING(3)
CompleteAtQA                BYTE
RapidQA                     STRING(3)
ValidateESN                 STRING(3)
ValidateDesp                STRING(3)
Force_Accessory_Check       STRING(3)
Force_Initial_Transit_Type  STRING(1)
Force_Mobile_Number         STRING(1)
Force_Model_Number          STRING(1)
Force_Unit_Type             STRING(1)
Force_Fault_Description     STRING(1)
Force_ESN                   STRING(1)
Force_MSN                   STRING(1)
Force_Job_Type              STRING(1)
Force_Engineer              STRING(1)
Force_Invoice_Text          STRING(1)
Force_Repair_Type           STRING(1)
Force_Authority_Number      STRING(1)
Force_Fault_Coding          STRING(1)
Force_Spares                STRING(1)
Force_Incoming_Courier      STRING(1)
Force_Outoing_Courier       STRING(1)
Force_DOP                   STRING(1)
Order_Number                STRING(1)
Customer_Name               STRING(1)
ForcePostcode               STRING(1)
ForceDelPostcode            STRING(1)
ForceCommonFault            STRING(1)
Remove_Backgrounds          STRING(3)
Use_Loan_Exchange_Label     STRING(3)
Use_Job_Label               STRING(3)
UseSmallLabel               BYTE
Job_Label                   STRING(255)
add_stock_label             STRING(3)
receive_stock_label         STRING(3)
QA_Failed_Label             STRING(3)
QA_Failed_Report            STRING(3)
QAPassLabel                 BYTE
ThirdPartyNote              STRING(3)
Vodafone_Import_Path        STRING(255)
Label_Printer_Type          STRING(30)
Browse_Option               STRING(3)
ANCCollectionNo             LONG
Use_Sage                    STRING(3)
Global_Nominal_Code         STRING(30)
Parts_Stock_Code            STRING(30)
Labour_Stock_Code           STRING(30)
Courier_Stock_Code          STRING(30)
Parts_Description           STRING(60)
Labour_Description          STRING(60)
Courier_Description         STRING(60)
Parts_Code                  STRING(30)
Labour_Code                 STRING(30)
Courier_Code                STRING(30)
User_Name_Sage              STRING(30)
Password_Sage               STRING(30)
Company_Number_Sage         STRING(30)
Path_Sage                   STRING(255)
BouncerTime                 LONG
ExportPath                  STRING(255)
PrintBarcode                BYTE
ChaChargeType               STRING(30)
WarChargeType               STRING(30)
RetBackOrders               BYTE
RemoveWorkshopDespatch      BYTE
SummaryOrders               BYTE
EuroRate                    REAL
IrishVatRate                REAL
EmailServerAddress          STRING(255)
EmailServerPort             LONG
Job_Label_Accessories       STRING(3)
ShowRepTypeCosts            BYTE
PickNoteNormal              BYTE
PickNoteMainStore           BYTE
PickNoteChangeStatus        BYTE
PickNoteJobStatus           STRING(30)
                         END
                       END
!------------ End File: DEFAULTS, version 5 ------------------

!------------ File: DEFAULTS, version 6 ----------------------
!------------ modified 07.10.2004 at 14:58:06 -----------------
MOD:stFileName:DEFAULTSVer6  STRING(260)
DEFAULTSVer6           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:DEFAULTSVer6),PRE(DEFAULTSVer6),CREATE
RecordNumberKey          KEY(DEFAULTSVer6:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Name                   STRING(30)
record_number               REAL
Version_Number              STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
VAT_Number                  STRING(30)
Use_Invoice_Address         STRING(3)
Use_For_Order               STRING(3)
Invoice_Company_Name        STRING(30)
Invoice_Address_Line1       STRING(30)
Invoice_Address_Line2       STRING(30)
Invoice_Address_Line3       STRING(30)
Invoice_Postcode            STRING(15)
Invoice_Telephone_Number    STRING(15)
Invoice_Fax_Number          STRING(15)
InvoiceEmailAddress         STRING(255)
Invoice_VAT_Number          STRING(30)
OrderCompanyName            STRING(30)
OrderAddressLine1           STRING(30)
OrderAddressLine2           STRING(30)
OrderAddressLine3           STRING(30)
OrderPostcode               STRING(30)
OrderTelephoneNumber        STRING(30)
OrderFaxNumber              STRING(30)
OrderEmailAddress           STRING(255)
Use_Postcode                STRING(3)
Postcode_Path               STRING(255)
PostcodeDll                 STRING(3)
Vat_Rate_Labour             STRING(2)
Vat_Rate_Parts              STRING(2)
License_Number              STRING(10)
Maximum_Users               REAL
Warranty_Period             REAL
Automatic_Replicate         STRING(3)
Automatic_Replicate_Field   STRING(15)
Estimate_If_Over            REAL
Start_Work_Hours            TIME
End_Work_Hours              TIME
Include_Saturday            STRING(3)
Include_Sunday              STRING(3)
Show_Mobile_Number          STRING(3)
Show_Loan_Exchange_Details  STRING(3)
Use_Credit_Limit            STRING(3)
Hide_Physical_Damage        STRING(3)
Hide_Insurance              STRING(3)
Hide_Authority_Number       STRING(3)
Hide_Advance_Payment        STRING(3)
HideColour                  STRING(3)
HideInCourier               STRING(3)
HideIntFault                STRING(3)
HideProduct                 STRING(3)
HideLocation                BYTE
Allow_Bouncer               STRING(3)
Job_Batch_Number            REAL
QA_Required                 STRING(3)
QA_Before_Complete          STRING(3)
QAPreliminary               STRING(3)
QAExchLoan                  STRING(3)
CompleteAtQA                BYTE
RapidQA                     STRING(3)
ValidateESN                 STRING(3)
ValidateDesp                STRING(3)
Force_Accessory_Check       STRING(3)
Force_Initial_Transit_Type  STRING(1)
Force_Mobile_Number         STRING(1)
Force_Model_Number          STRING(1)
Force_Unit_Type             STRING(1)
Force_Fault_Description     STRING(1)
Force_ESN                   STRING(1)
Force_MSN                   STRING(1)
Force_Job_Type              STRING(1)
Force_Engineer              STRING(1)
Force_Invoice_Text          STRING(1)
Force_Repair_Type           STRING(1)
Force_Authority_Number      STRING(1)
Force_Fault_Coding          STRING(1)
Force_Spares                STRING(1)
Force_Incoming_Courier      STRING(1)
Force_Outoing_Courier       STRING(1)
Force_DOP                   STRING(1)
Order_Number                STRING(1)
Customer_Name               STRING(1)
ForcePostcode               STRING(1)
ForceDelPostcode            STRING(1)
ForceCommonFault            STRING(1)
Remove_Backgrounds          STRING(3)
Use_Loan_Exchange_Label     STRING(3)
Use_Job_Label               STRING(3)
UseSmallLabel               BYTE
Job_Label                   STRING(255)
add_stock_label             STRING(3)
receive_stock_label         STRING(3)
QA_Failed_Label             STRING(3)
QA_Failed_Report            STRING(3)
QAPassLabel                 BYTE
ThirdPartyNote              STRING(3)
Vodafone_Import_Path        STRING(255)
Label_Printer_Type          STRING(30)
Browse_Option               STRING(3)
ANCCollectionNo             LONG
Use_Sage                    STRING(3)
Global_Nominal_Code         STRING(30)
Parts_Stock_Code            STRING(30)
Labour_Stock_Code           STRING(30)
Courier_Stock_Code          STRING(30)
Parts_Description           STRING(60)
Labour_Description          STRING(60)
Courier_Description         STRING(60)
Parts_Code                  STRING(30)
Labour_Code                 STRING(30)
Courier_Code                STRING(30)
User_Name_Sage              STRING(30)
Password_Sage               STRING(30)
Company_Number_Sage         STRING(30)
Path_Sage                   STRING(255)
BouncerTime                 LONG
ExportPath                  STRING(255)
PrintBarcode                BYTE
ChaChargeType               STRING(30)
WarChargeType               STRING(30)
RetBackOrders               BYTE
RemoveWorkshopDespatch      BYTE
SummaryOrders               BYTE
EuroRate                    REAL
IrishVatRate                REAL
EmailServerAddress          STRING(255)
EmailServerPort             LONG
Job_Label_Accessories       STRING(3)
ShowRepTypeCosts            BYTE
PickNoteNormal              BYTE
PickNoteMainStore           BYTE
PickNoteChangeStatus        BYTE
PickNoteJobStatus           STRING(30)
TeamPerformanceTicker       BYTE
TickerRefreshRate           SHORT
                         END
                       END
!------------ End File: DEFAULTS, version 6 ------------------

!------------ File: MANFPALO, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:MANFPALOVer1  STRING(260)
MANFPALOVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANFPALOVer1),PRE(MANFPALOVer1)
Field_Key                KEY(MANFPALOVer1:Manufacturer,MANFPALOVer1:Field_Number,MANFPALOVer1:Field),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                       END
!------------ End File: MANFPALO, version 1 ------------------

!------------ File: MANFAUPA, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:MANFAUPAVer1  STRING(260)
MANFAUPAVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANFAUPAVer1),PRE(MANFAUPAVer1)
Field_Number_Key         KEY(MANFAUPAVer1:Manufacturer,MANFAUPAVer1:Field_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
                         END
                       END
!------------ End File: MANFAUPA, version 1 ------------------

!------------ File: SIDALERT, version 1 ----------------------
!------------ modified 17.09.2007 at 12:04:21 -----------------
MOD:stFileName:SIDALERTVer1  STRING(260)
SIDALERTVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDALERTVer1),PRE(SIDALERTVer1)
RecordNumberKey          KEY(SIDALERTVer1:RecordNumber),NOCASE,PRIMARY
StatusKey                KEY(SIDALERTVer1:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Status                      STRING(30)
BookingType                 STRING(30)
Description                 STRING(60)
Monday                      STRING(20)
Tuesday                     STRING(20)
Wednesday                   STRING(20)
Thursday                    STRING(20)
Friday                      STRING(20)
Saturday                    STRING(20)
Sunday                      STRING(20)
SendTo                      STRING(20)
SMSAlertActive              STRING(20)
CutOffTime                  TIME
CutOffTimeType              STRING(20)
BankHolidays                STRING(20)
SMSMessageText              STRING(480)
EmailAlertActive            STRING(20)
EmailMessageSubject         STRING(60)
EmailMessageText            STRING(4000)
                         END
                       END
!------------ End File: SIDALERT, version 1 ------------------

!------------ File: SIDALERT, version 2 ----------------------
!------------ modified 17.09.2007 at 14:15:34 -----------------
MOD:stFileName:SIDALERTVer2  STRING(260)
SIDALERTVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDALERTVer2),PRE(SIDALERTVer2),CREATE
RecordNumberKey          KEY(SIDALERTVer2:RecordNumber),NOCASE,PRIMARY
StatusKey                KEY(SIDALERTVer2:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Status                      STRING(30)
BookingType                 STRING(30)
Description                 STRING(60)
Monday                      STRING(20)
Tuesday                     STRING(20)
Wednesday                   STRING(20)
Thursday                    STRING(20)
Friday                      STRING(20)
Saturday                    STRING(20)
Sunday                      STRING(20)
SendTo                      STRING(20)
SMSAlertActive              STRING(20)
CutOffTime                  TIME
CutOffTimeType              STRING(20)
BankHolidays                STRING(20)
SMSMessageText              STRING(480)
EmailAlertActive            STRING(20)
EmailMessageSubject         STRING(60)
                         END
                       END
!------------ End File: SIDALERT, version 2 ------------------

!------------ File: SIDALERT, version 3 ----------------------
!------------ modified 18.09.2007 at 13:42:15 -----------------
MOD:stFileName:SIDALERTVer3  STRING(260)
SIDALERTVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDALERTVer3),PRE(SIDALERTVer3),CREATE
RecordNumberKey          KEY(SIDALERTVer3:RecordNumber),NOCASE,PRIMARY
StatusKey                KEY(SIDALERTVer3:Status),DUP,NOCASE
BookingTypeStatusKey     KEY(SIDALERTVer3:BookingType,SIDALERTVer3:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Status                      STRING(30)
BookingType                 STRING(30)
Description                 STRING(60)
Monday                      STRING(20)
Tuesday                     STRING(20)
Wednesday                   STRING(20)
Thursday                    STRING(20)
Friday                      STRING(20)
Saturday                    STRING(20)
Sunday                      STRING(20)
SendTo                      STRING(20)
SMSAlertActive              STRING(20)
CutOffTime                  TIME
CutOffTimeType              STRING(20)
BankHolidays                STRING(20)
SMSMessageText              STRING(480)
EmailAlertActive            STRING(20)
EmailMessageSubject         STRING(60)
                         END
                       END
!------------ End File: SIDALERT, version 3 ------------------

!------------ File: SIDALERT, version 4 ----------------------
!------------ modified 18.09.2007 at 13:59:17 -----------------
MOD:stFileName:SIDALERTVer4  STRING(260)
SIDALERTVer4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDALERTVer4),PRE(SIDALERTVer4),CREATE
RecordNumberKey          KEY(SIDALERTVer4:RecordNumber),NOCASE,PRIMARY
StatusKey                KEY(SIDALERTVer4:Status),DUP,NOCASE
BookingTypeStatusKey     KEY(SIDALERTVer4:BookingType,SIDALERTVer4:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Status                      STRING(30)
BookingType                 STRING(30)
Description                 STRING(60)
Monday                      BYTE
Tuesday                     BYTE
Wednesday                   BYTE
Thursday                    BYTE
Friday                      BYTE
Saturday                    BYTE
Sunday                      BYTE
SendTo                      BYTE
SMSAlertActive              BYTE
CutOffTime                  TIME
CutOffTimeType              BYTE
BankHolidays                BYTE
SMSMessageText              STRING(480)
EmailAlertActive            BYTE
EmailMessageSubject         STRING(60)
                         END
                       END
!------------ End File: SIDALERT, version 4 ------------------

!------------ File: SIDALERT, version 5 ----------------------
!------------ modified 20.09.2007 at 11:16:57 -----------------
MOD:stFileName:SIDALERTVer5  STRING(260)
SIDALERTVer5           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDALERTVer5),PRE(SIDALERTVer5),CREATE
RecordNumberKey          KEY(SIDALERTVer5:RecordNumber),NOCASE,PRIMARY
StatusKey                KEY(SIDALERTVer5:Status),DUP,NOCASE
BookingTypeStatusKey     KEY(SIDALERTVer5:BookingType,SIDALERTVer5:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Status                      STRING(30)
BookingType                 STRING(30)
Description                 STRING(60)
Monday                      BYTE
Tuesday                     BYTE
Wednesday                   BYTE
Thursday                    BYTE
Friday                      BYTE
Saturday                    BYTE
Sunday                      BYTE
SendTo                      BYTE
SMSAlertActive              BYTE
CutOffTime                  TIME
CutOffTimeType              BYTE
BankHolidays                BYTE
SMSMessageText              STRING(480)
EmailAlertActive            BYTE
EmailMessageSubject         STRING(60)
WithBattery                 BYTE
SystemAlert                 BYTE
                         END
                       END
!------------ End File: SIDALERT, version 5 ------------------

!------------ File: SIDALERT, version 6 ----------------------
!------------ modified 21.09.2007 at 16:04:40 -----------------
MOD:stFileName:SIDALERTVer6  STRING(260)
SIDALERTVer6           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDALERTVer6),PRE(SIDALERTVer6),CREATE
RecordNumberKey          KEY(SIDALERTVer6:RecordNumber),NOCASE,PRIMARY
StatusKey                KEY(SIDALERTVer6:Status),DUP,NOCASE
BookingTypeStatusKey     KEY(SIDALERTVer6:BookingType,SIDALERTVer6:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Status                      STRING(3)
BookingType                 STRING(30)
Description                 STRING(60)
Monday                      BYTE
Tuesday                     BYTE
Wednesday                   BYTE
Thursday                    BYTE
Friday                      BYTE
Saturday                    BYTE
Sunday                      BYTE
SendTo                      BYTE
SMSAlertActive              BYTE
CutOffTime                  TIME
CutOffTimeType              BYTE
BankHolidays                BYTE
SMSMessageText              STRING(480)
EmailAlertActive            BYTE
EmailMessageSubject         STRING(60)
SystemAlert                 BYTE
CheckAccessories            BYTE
WithBattery                 BYTE
                         END
                       END
!------------ End File: SIDALERT, version 6 ------------------

!------------ File: SIDALDEF, version 1 ----------------------
!------------ modified 20.09.2007 at 14:33:59 -----------------
MOD:stFileName:SIDALDEFVer1  STRING(260)
SIDALDEFVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDALDEFVer1),PRE(SIDALDEFVer1)
RecordNumberKey          KEY(SIDALDEFVer1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(100)
SMTPPortNumber              LONG
SMTPUserName                STRING(100)
SMTPPassword                STRING(100)
EmailFromAddress            STRING(255)
UTLEmailAddress             STRING(255)
                         END
                       END
!------------ End File: SIDALDEF, version 1 ------------------

!------------ File: SIDALDEF, version 2 ----------------------
!------------ modified 27.09.2007 at 09:47:16 -----------------
MOD:stFileName:SIDALDEFVer2  STRING(260)
SIDALDEFVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDALDEFVer2),PRE(SIDALDEFVer2),CREATE
RecordNumberKey          KEY(SIDALDEFVer2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(100)
SMTPPortNumber              LONG
SMTPUserName                STRING(100)
SMTPPassword                STRING(100)
EmailFromAddress            STRING(255)
UTLEmailAddress             STRING(255)
SMSURL                      STRING(255)
                         END
                       END
!------------ End File: SIDALDEF, version 2 ------------------

!------------ File: SIDALDEF, version 3 ----------------------
!------------ modified 02.11.2007 at 09:39:26 -----------------
MOD:stFileName:SIDALDEFVer3  STRING(260)
SIDALDEFVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDALDEFVer3),PRE(SIDALDEFVer3),CREATE
RecordNumberKey          KEY(SIDALDEFVer3:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(100)
SMTPPortNumber              LONG
SMTPUserName                STRING(100)
SMTPPassword                STRING(100)
EmailFromAddress            STRING(255)
UTLEmailAddress             STRING(255)
SMSURL                      STRING(255)
SMSUsername                 STRING(100)
SMSPassword                 STRING(100)
                         END
                       END
!------------ End File: SIDALDEF, version 3 ------------------

!------------ File: SIDALDEF, version 4 ----------------------
!------------ modified 17.01.2008 at 15:08:35 -----------------
MOD:stFileName:SIDALDEFVer4  STRING(260)
SIDALDEFVer4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDALDEFVer4),PRE(SIDALDEFVer4),CREATE
RecordNumberKey          KEY(SIDALDEFVer4:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(100)
SMTPPortNumber              LONG
SMTPUserName                STRING(100)
SMTPPassword                STRING(100)
EmailFromAddress            STRING(255)
UTLEmailAddress             STRING(255)
SMSURL                      STRING(255)
SMSUsername                 STRING(100)
SMSPassword                 STRING(100)
ReplyToAddress              STRING(255)
                         END
                       END
!------------ End File: SIDALDEF, version 4 ------------------

!------------ File: SIDREMIN, version 1 ----------------------
!------------ modified 19.09.2007 at 11:47:34 -----------------
MOD:stFileName:SIDREMINVer1  STRING(260)
SIDREMINVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SIDREMINVer1),PRE(SIDREMINVer1)
RecordNumberKey          KEY(SIDREMINVer1:RecordNumber),NOCASE,PRIMARY
ElapsedKey               KEY(SIDREMINVer1:SIDALERTRecordNumber,SIDREMINVer1:ElapsedDays),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SIDALERTRecordNumber        LONG
Description                 STRING(60)
ElapsedDays                 LONG
SendTo                      BYTE
SMSAlertActive              BYTE
CutOffTime                  TIME
CutOffTimeType              BYTE
BankHolidays                BYTE
SMSMessageText              STRING(480)
EmailAlertActive            BYTE
EmailSubject                STRING(60)
                         END
                       END
!------------ End File: SIDREMIN, version 1 ------------------

!------------ File: SIDRRCT, version 1 ----------------------
!------------ modified 25.06.2010 at 10:28:19 -----------------
MOD:stFileName:SIDRRCTVer1  STRING(260)
SIDRRCTVer1            FILE,DRIVER('Btrieve'),PRE(SIDRRCTVer1),NAME(MOD:stFileName:SIDRRCTVer1)
RecordNumberKey          KEY(SIDRRCTVer1:RecordNumber),NOCASE,PRIMARY
TierKey                  KEY(SIDRRCTVer1:Tier),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Tier                        BYTE
ChargeValue                 PDECIMAL(6,2)
SkuCode                     STRING(30)
                         END
                       END
!------------ End File: SIDRRCT, version 1 ------------------

!------------ File: SIDMODTT, version 1 ----------------------
!------------ modified 08.01.2009 at 11:20:46 -----------------
MOD:stFileName:SIDMODTTVer1  STRING(260)
SIDMODTTVer1           FILE,DRIVER('Btrieve'),PRE(SIDMODTTVer1),NAME(MOD:stFileName:SIDMODTTVer1)
RecordNumberKey          KEY(SIDMODTTVer1:RecordNumber),NOCASE,PRIMARY
ModelNoKey               KEY(SIDMODTTVer1:ModelNo),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
TurnaroundException         BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysExch          LONG
                         END
                       END
!------------ End File: SIDMODTT, version 1 ------------------

!------------ File: SIDMODTT, version 2 ----------------------
!------------ modified 08.01.2009 at 14:07:29 -----------------
MOD:stFileName:SIDMODTTVer2  STRING(260)
SIDMODTTVer2           FILE,DRIVER('Btrieve'),PRE(SIDMODTTVer2),NAME(MOD:stFileName:SIDMODTTVer2),CREATE
RecordNumberKey          KEY(SIDMODTTVer2:RecordNumber),NOCASE,PRIMARY
ModelNoKey               KEY(SIDMODTTVer2:ModelNo),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
TurnaroundException         BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysExch          LONG
                         END
                       END
!------------ End File: SIDMODTT, version 2 ------------------

!------------ File: SIDSRVCN, version 1 ----------------------
!------------ modified 30.08.2006 at 17:14:38 -----------------
MOD:stFileName:SIDSRVCNVer1  STRING(260)
SIDSRVCNVer1           FILE,DRIVER('Btrieve'),PRE(SIDSRVCNVer1),NAME(MOD:stFileName:SIDSRVCNVer1)
SCAccountIDKey           KEY(SIDSRVCNVer1:SCAccountID),NOCASE,PRIMARY
ServiceCentreNameKey     KEY(SIDSRVCNVer1:ServiceCentreName),DUP,NOCASE
Record                   RECORD,PRE()
SCAccountID                 LONG
ServiceCentreName           STRING(30)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
Postcode                    STRING(15)
AccountActive               BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysB2B           LONG
BankHolidaysFileName        STRING(100)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
UseServiceCentreFTP         BYTE
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
                         END
                       END
!------------ End File: SIDSRVCN, version 1 ------------------

!------------ File: SIDSRVCN, version 2 ----------------------
!------------ modified 31.08.2006 at 10:50:29 -----------------
MOD:stFileName:SIDSRVCNVer2  STRING(260)
SIDSRVCNVer2           FILE,DRIVER('Btrieve'),PRE(SIDSRVCNVer2),NAME(MOD:stFileName:SIDSRVCNVer2),CREATE
SCAccountIDKey           KEY(SIDSRVCNVer2:SCAccountID),NOCASE,PRIMARY
ServiceCentreNameKey     KEY(SIDSRVCNVer2:ServiceCentreName),DUP,NOCASE
Record                   RECORD,PRE()
SCAccountID                 LONG
ServiceCentreName           STRING(30)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
Postcode                    STRING(15)
AccountActive               BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysExch          LONG
BankHolidaysFileName        STRING(100)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
UseServiceCentreFTP         BYTE
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
FTPFailedCount              LONG
                         END
                       END
!------------ End File: SIDSRVCN, version 2 ------------------

!------------ File: SIDSRVCN, version 3 ----------------------
!------------ modified 04.09.2006 at 10:27:43 -----------------
MOD:stFileName:SIDSRVCNVer3  STRING(260)
SIDSRVCNVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SIDSRVCNVer3),PRE(SIDSRVCNVer3),CREATE
SCAccountIDKey           KEY(SIDSRVCNVer3:SCAccountID),NOCASE,PRIMARY
ServiceCentreNameKey     KEY(SIDSRVCNVer3:ServiceCentreName),DUP,NOCASE
Record                   RECORD,PRE()
SCAccountID                 LONG
ServiceCentreName           STRING(30)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
Postcode                    STRING(15)
AccountActive               BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysExch          LONG
BankHolidaysFileName        STRING(100)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
UseServiceCentreFTP         BYTE
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
FTPFailedCount              LONG
                         END
                       END
!------------ End File: SIDSRVCN, version 3 ------------------

!------------ File: SIDSRVCN, version 4 ----------------------
!------------ modified 27.09.2006 at 14:36:32 -----------------
MOD:stFileName:SIDSRVCNVer4  STRING(260)
SIDSRVCNVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SIDSRVCNVer4),PRE(SIDSRVCNVer4),CREATE
SCAccountIDKey           KEY(SIDSRVCNVer4:SCAccountID),NOCASE,PRIMARY
ServiceCentreNameKey     KEY(SIDSRVCNVer4:ServiceCentreName),DUP,NOCASE
Record                   RECORD,PRE()
SCAccountID                 LONG
ServiceCentreName           STRING(30)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
Postcode                    STRING(15)
AccountActive               BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysExch          LONG
BankHolidaysFileName        STRING(100)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
UseServiceCentreFTP         BYTE
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
FTPFailedCount              LONG
MonRepairCapacity           LONG
TueRepairCapacity           LONG
WedRepairCapacity           LONG
ThuRepairCapacity           LONG
FriRepairCapacity           LONG
                         END
                       END
!------------ End File: SIDSRVCN, version 4 ------------------

!------------ File: SIDSRVCN, version 5 ----------------------
!------------ modified 29.09.2006 at 10:33:14 -----------------
MOD:stFileName:SIDSRVCNVer5  STRING(260)
SIDSRVCNVer5           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SIDSRVCNVer5),PRE(SIDSRVCNVer5),CREATE
SCAccountIDKey           KEY(SIDSRVCNVer5:SCAccountID),NOCASE,PRIMARY
ServiceCentreNameKey     KEY(SIDSRVCNVer5:ServiceCentreName),DUP,NOCASE
Record                   RECORD,PRE()
SCAccountID                 LONG
ServiceCentreName           STRING(30)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
Postcode                    STRING(15)
AccountActive               BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysExch          LONG
BankHolidaysFileName        STRING(100)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
UseServiceCentreFTP         BYTE
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
FTPFailedCount              LONG
MonRepairCapacity           LONG
TueRepairCapacity           LONG
WedRepairCapacity           LONG
ThuRepairCapacity           LONG
FriRepairCapacity           LONG
ExchangeImportType          BYTE
                         END
                       END
!------------ End File: SIDSRVCN, version 5 ------------------

!------------ File: SIDMODSC, version 1 ----------------------
!------------ modified 31.08.2006 at 10:50:29 -----------------
MOD:stFileName:SIDMODSCVer1  STRING(260)
SIDMODSCVer1           FILE,DRIVER('Btrieve'),PRE(SIDMODSCVer1),NAME(MOD:stFileName:SIDMODSCVer1)
RecordNumberKey          KEY(SIDMODSCVer1:RecordNumber),NOCASE,PRIMARY
ManModelTypeKey          KEY(SIDMODSCVer1:Manufacturer,SIDMODSCVer1:ModelNo,SIDMODSCVer1:BookingType),NOCASE
SCAccountIDKey           KEY(SIDMODSCVer1:SCAccountID),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
BookingType                 STRING(1)
SCAccountID                 LONG
                         END
                       END
!------------ End File: SIDMODSC, version 1 ------------------

!------------ File: SIDMODSC, version 2 ----------------------
!------------ modified 07.01.2009 at 11:33:17 -----------------
MOD:stFileName:SIDMODSCVer2  STRING(260)
SIDMODSCVer2           FILE,DRIVER('Btrieve'),PRE(SIDMODSCVer2),NAME(MOD:stFileName:SIDMODSCVer2),CREATE
RecordNumberKey          KEY(SIDMODSCVer2:RecordNumber),NOCASE,PRIMARY
ManModelTypeKey          KEY(SIDMODSCVer2:Manufacturer,SIDMODSCVer2:ModelNo,SIDMODSCVer2:BookingType),NOCASE
SCAccountIDKey           KEY(SIDMODSCVer2:SCAccountID),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
BookingType                 STRING(1)
SCAccountID                 LONG
TurnaroundException         BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysExch          LONG
                         END
                       END
!------------ End File: SIDMODSC, version 2 ------------------

!------------ File: SIDMODSC, version 3 ----------------------
!------------ modified 08.01.2009 at 10:52:03 -----------------
MOD:stFileName:SIDMODSCVer3  STRING(260)
SIDMODSCVer3           FILE,DRIVER('Btrieve'),PRE(SIDMODSCVer3),NAME(MOD:stFileName:SIDMODSCVer3),CREATE
RecordNumberKey          KEY(SIDMODSCVer3:RecordNumber),NOCASE,PRIMARY
ManModelTypeKey          KEY(SIDMODSCVer3:Manufacturer,SIDMODSCVer3:ModelNo,SIDMODSCVer3:BookingType),NOCASE
SCAccountIDKey           KEY(SIDMODSCVer3:SCAccountID),DUP,NOCASE
ModelTypeKey             KEY(SIDMODSCVer3:ModelNo,SIDMODSCVer3:BookingType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
BookingType                 STRING(1)
SCAccountID                 LONG
TurnaroundException         BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysExch          LONG
                         END
                       END
!------------ End File: SIDMODSC, version 3 ------------------

!------------ File: SIDNWDAY, version 1 ----------------------
!------------ modified 01.09.2006 at 10:32:25 -----------------
MOD:stFileName:SIDNWDAYVer1  STRING(260)
SIDNWDAYVer1           FILE,DRIVER('Btrieve'),PRE(SIDNWDAYVer1),NAME(MOD:stFileName:SIDNWDAYVer1)
RecordNumberKey          KEY(SIDNWDAYVer1:RecordNumber),NOCASE,PRIMARY
SCBookTypeDayKey         KEY(SIDNWDAYVer1:SCAccountID,SIDNWDAYVer1:BookingType,SIDNWDAYVer1:NonWorkingDay),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SCAccountID                 LONG
BookingType                 STRING(1)
NonWorkingDay               DATE
                         END
                       END
!------------ End File: SIDNWDAY, version 1 ------------------

!------------ File: SMOREMIN, version 1 ----------------------
!------------ modified 10.03.2010 at 10:48:15 -----------------
MOD:stFileName:SMOREMINVer1  STRING(260)
SMOREMINVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:SMOREMINVer1),PRE(SMOREMINVer1)
RecordNumberKey          KEY(SMOREMINVer1:RecordNumber),NOCASE,PRIMARY
SIDREMINRecordNumberKey  KEY(SMOREMINVer1:SIDREMINRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SIDREMINRecordNumber        LONG
SMSAlertActive              BYTE
SMSMessageText              STRING(480)
EmailAlertActive            BYTE
EmailSubject                STRING(60)
                         END
                       END
!------------ End File: SMOREMIN, version 1 ------------------

!------------ File: MANFAULO, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:MANFAULOVer1  STRING(260)
MANFAULOVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANFAULOVer1),PRE(MANFAULOVer1)
Field_Key                KEY(MANFAULOVer1:Manufacturer,MANFAULOVer1:Field_Number,MANFAULOVer1:Field),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                       END
!------------ End File: MANFAULO, version 1 ------------------

!------------ File: RETSTOCK, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:RETSTOCKVer1  STRING(260)
RETSTOCKVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:RETSTOCKVer1),PRE(RETSTOCKVer1)
Record_Number_Key        KEY(RETSTOCKVer1:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Part_Number),DUP,NOCASE
Despatched_Key           KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Despatched,RETSTOCKVer1:Despatch_Note_Number,RETSTOCKVer1:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(RETSTOCKVer1:Ref_Number,RETSTOCKVer1:Order_Number),DUP,NOCASE
DespatchedKey            KEY(RETSTOCKVer1:Despatched),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
                         END
                       END
!------------ End File: RETSTOCK, version 1 ------------------

!------------ File: ORDPARTS, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:ORDPARTSVer1  STRING(260)
ORDPARTSVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:ORDPARTSVer1),PRE(ORDPARTSVer1)
Order_Number_Key         KEY(ORDPARTSVer1:Order_Number,ORDPARTSVer1:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(ORDPARTSVer1:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(ORDPARTSVer1:Part_Ref_Number,ORDPARTSVer1:Part_Number,ORDPARTSVer1:Description),DUP,NOCASE
Part_Number_Key          KEY(ORDPARTSVer1:Order_Number,ORDPARTSVer1:Part_Number,ORDPARTSVer1:Description),DUP,NOCASE
Description_Key          KEY(ORDPARTSVer1:Order_Number,ORDPARTSVer1:Description,ORDPARTSVer1:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(ORDPARTSVer1:All_Received,ORDPARTSVer1:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(ORDPARTSVer1:Account_Number,ORDPARTSVer1:Part_Type,ORDPARTSVer1:All_Received,ORDPARTSVer1:Part_Number),DUP,NOCASE
Allocated_Key            KEY(ORDPARTSVer1:Part_Type,ORDPARTSVer1:All_Received,ORDPARTSVer1:Allocated_To_Sale,ORDPARTSVer1:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(ORDPARTSVer1:Part_Type,ORDPARTSVer1:All_Received,ORDPARTSVer1:Allocated_To_Sale,ORDPARTSVer1:Account_Number,ORDPARTSVer1:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(ORDPARTSVer1:Order_Number,ORDPARTSVer1:Part_Type,ORDPARTSVer1:All_Received,ORDPARTSVer1:Part_Number,ORDPARTSVer1:Description),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
                         END
                       END
!------------ End File: ORDPARTS, version 1 ------------------

!------------ File: LOCATION, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:57 -----------------
MOD:stFileName:LOCATIONVer1  STRING(260)
LOCATIONVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:LOCATIONVer1),PRE(LOCATIONVer1)
RecordNumberKey          KEY(LOCATIONVer1:RecordNumber),NOCASE,PRIMARY
Location_Key             KEY(LOCATIONVer1:Location),NOCASE
Main_Store_Key           KEY(LOCATIONVer1:Main_Store,LOCATIONVer1:Location),DUP,NOCASE
ActiveLocationKey        KEY(LOCATIONVer1:Active,LOCATIONVer1:Location),DUP,NOCASE
ActiveMainStoreKey       KEY(LOCATIONVer1:Active,LOCATIONVer1:Main_Store,LOCATIONVer1:Location),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
Main_Store                  STRING(3)
Active                      BYTE
                         END
                       END
!------------ End File: LOCATION, version 1 ------------------

!------------ File: LOCATION, version 2 ----------------------
!------------ modified 15.01.2004 at 15:07:22 -----------------
MOD:stFileName:LOCATIONVer2  STRING(260)
LOCATIONVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:LOCATIONVer2),PRE(LOCATIONVer2),CREATE
RecordNumberKey          KEY(LOCATIONVer2:RecordNumber),NOCASE,PRIMARY
Location_Key             KEY(LOCATIONVer2:Location),NOCASE
Main_Store_Key           KEY(LOCATIONVer2:Main_Store,LOCATIONVer2:Location),DUP,NOCASE
ActiveLocationKey        KEY(LOCATIONVer2:Active,LOCATIONVer2:Location),DUP,NOCASE
ActiveMainStoreKey       KEY(LOCATIONVer2:Active,LOCATIONVer2:Main_Store,LOCATIONVer2:Location),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
PickNoteEnable              BYTE
Location                    STRING(30)
Main_Store                  STRING(3)
Active                      BYTE
                         END
                       END
!------------ End File: LOCATION, version 2 ------------------

!------------ File: JOBS, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:58 -----------------
MOD:stFileName:JOBSVer1  STRING(260)
JOBSVer1               FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:JOBSVer1),PRE(JOBSVer1),BINDABLE
Ref_Number_Key           KEY(JOBSVer1:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(JOBSVer1:Model_Number,JOBSVer1:Unit_Type),DUP,NOCASE
EngCompKey               KEY(JOBSVer1:Engineer,JOBSVer1:Completed,JOBSVer1:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(JOBSVer1:Engineer,JOBSVer1:Workshop,JOBSVer1:Ref_Number),DUP,NOCASE
Surname_Key              KEY(JOBSVer1:Surname),DUP,NOCASE
MobileNumberKey          KEY(JOBSVer1:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(JOBSVer1:ESN),DUP,NOCASE
MSN_Key                  KEY(JOBSVer1:MSN),DUP,NOCASE
AccountNumberKey         KEY(JOBSVer1:Account_Number,JOBSVer1:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(JOBSVer1:Account_Number,JOBSVer1:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(JOBSVer1:Model_Number),DUP,NOCASE
Engineer_Key             KEY(JOBSVer1:Engineer,JOBSVer1:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(JOBSVer1:date_booked),DUP,NOCASE
DateCompletedKey         KEY(JOBSVer1:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(JOBSVer1:Model_Number,JOBSVer1:Date_Completed),DUP,NOCASE
By_Status                KEY(JOBSVer1:Current_Status,JOBSVer1:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(JOBSVer1:Current_Status,JOBSVer1:Location,JOBSVer1:Ref_Number),DUP,NOCASE
Location_Key             KEY(JOBSVer1:Location,JOBSVer1:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(JOBSVer1:Third_Party_Site,JOBSVer1:Third_Party_Printed,JOBSVer1:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(JOBSVer1:Third_Party_Site,JOBSVer1:Third_Party_Printed,JOBSVer1:ESN),DUP,NOCASE
ThirdMsnKey              KEY(JOBSVer1:Third_Party_Site,JOBSVer1:Third_Party_Printed,JOBSVer1:MSN),DUP,NOCASE
PriorityTypeKey          KEY(JOBSVer1:Job_Priority,JOBSVer1:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(JOBSVer1:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(JOBSVer1:Manufacturer,JOBSVer1:EDI,JOBSVer1:EDI_Batch_Number,JOBSVer1:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(JOBSVer1:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(JOBSVer1:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(JOBSVer1:Batch_Number,JOBSVer1:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(JOBSVer1:Batch_Number,JOBSVer1:Current_Status,JOBSVer1:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(JOBSVer1:Batch_Number,JOBSVer1:Model_Number,JOBSVer1:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(JOBSVer1:Batch_Number,JOBSVer1:Invoice_Number,JOBSVer1:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(JOBSVer1:Batch_Number,JOBSVer1:Completed,JOBSVer1:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(JOBSVer1:Chargeable_Job,JOBSVer1:Account_Number,JOBSVer1:Invoice_Number,JOBSVer1:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(JOBSVer1:Invoice_Exception,JOBSVer1:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(JOBSVer1:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(JOBSVer1:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(JOBSVer1:Despatched,JOBSVer1:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(JOBSVer1:Despatched,JOBSVer1:Account_Number,JOBSVer1:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(JOBSVer1:Despatched,JOBSVer1:Current_Courier,JOBSVer1:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(JOBSVer1:Despatched,JOBSVer1:Account_Number,JOBSVer1:Current_Courier,JOBSVer1:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(JOBSVer1:Despatch_Number,JOBSVer1:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(JOBSVer1:Courier,JOBSVer1:Date_Despatched,JOBSVer1:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(JOBSVer1:Loan_Courier,JOBSVer1:Loan_Despatched,JOBSVer1:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(JOBSVer1:Exchange_Courier,JOBSVer1:Exchange_Despatched,JOBSVer1:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(JOBSVer1:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(JOBSVer1:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(JOBSVer1:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(JOBSVer1:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(JOBSVer1:Bouncer,JOBSVer1:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(JOBSVer1:Engineer,JOBSVer1:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(JOBSVer1:Exchange_Status,JOBSVer1:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(JOBSVer1:Loan_Status,JOBSVer1:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(JOBSVer1:Exchange_Status,JOBSVer1:Location,JOBSVer1:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(JOBSVer1:Loan_Status,JOBSVer1:Location,JOBSVer1:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(JOBSVer1:InvoiceAccount,JOBSVer1:InvoiceBatch,JOBSVer1:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(JOBSVer1:InvoiceAccount,JOBSVer1:InvoiceBatch,JOBSVer1:InvoiceStatus,JOBSVer1:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                       END
!------------ End File: JOBS, version 1 ------------------

!------------ File: JOBS, version 2 ----------------------
!------------ modified 20.02.2002 at 14:06:54 -----------------
MOD:stFileName:JOBSVer2  STRING(260)
JOBSVer2               FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:JOBSVer2),PRE(JOBSVer2),BINDABLE,CREATE
Ref_Number_Key           KEY(JOBSVer2:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(JOBSVer2:Model_Number,JOBSVer2:Unit_Type),DUP,NOCASE
EngCompKey               KEY(JOBSVer2:Engineer,JOBSVer2:Completed,JOBSVer2:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(JOBSVer2:Engineer,JOBSVer2:Workshop,JOBSVer2:Ref_Number),DUP,NOCASE
Surname_Key              KEY(JOBSVer2:Surname,JOBSVer2:Address_Line1),DUP,NOCASE
MobileNumberKey          KEY(JOBSVer2:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(JOBSVer2:ESN),DUP,NOCASE
MSN_Key                  KEY(JOBSVer2:MSN),DUP,NOCASE
AccountNumberKey         KEY(JOBSVer2:Account_Number,JOBSVer2:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(JOBSVer2:Account_Number,JOBSVer2:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(JOBSVer2:Model_Number),DUP,NOCASE
Engineer_Key             KEY(JOBSVer2:Engineer,JOBSVer2:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(JOBSVer2:date_booked),DUP,NOCASE
DateCompletedKey         KEY(JOBSVer2:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(JOBSVer2:Model_Number,JOBSVer2:Date_Completed),DUP,NOCASE
By_Status                KEY(JOBSVer2:Current_Status,JOBSVer2:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(JOBSVer2:Current_Status,JOBSVer2:Location,JOBSVer2:Ref_Number),DUP,NOCASE
Location_Key             KEY(JOBSVer2:Location,JOBSVer2:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(JOBSVer2:Third_Party_Site,JOBSVer2:Third_Party_Printed,JOBSVer2:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(JOBSVer2:Third_Party_Site,JOBSVer2:Third_Party_Printed,JOBSVer2:ESN),DUP,NOCASE
ThirdMsnKey              KEY(JOBSVer2:Third_Party_Site,JOBSVer2:Third_Party_Printed,JOBSVer2:MSN),DUP,NOCASE
PriorityTypeKey          KEY(JOBSVer2:Job_Priority,JOBSVer2:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(JOBSVer2:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(JOBSVer2:Manufacturer,JOBSVer2:EDI,JOBSVer2:EDI_Batch_Number,JOBSVer2:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(JOBSVer2:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(JOBSVer2:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(JOBSVer2:Batch_Number,JOBSVer2:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(JOBSVer2:Batch_Number,JOBSVer2:Current_Status,JOBSVer2:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(JOBSVer2:Batch_Number,JOBSVer2:Model_Number,JOBSVer2:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(JOBSVer2:Batch_Number,JOBSVer2:Invoice_Number,JOBSVer2:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(JOBSVer2:Batch_Number,JOBSVer2:Completed,JOBSVer2:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(JOBSVer2:Chargeable_Job,JOBSVer2:Account_Number,JOBSVer2:Invoice_Number,JOBSVer2:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(JOBSVer2:Invoice_Exception,JOBSVer2:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(JOBSVer2:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(JOBSVer2:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(JOBSVer2:Despatched,JOBSVer2:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(JOBSVer2:Despatched,JOBSVer2:Account_Number,JOBSVer2:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(JOBSVer2:Despatched,JOBSVer2:Current_Courier,JOBSVer2:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(JOBSVer2:Despatched,JOBSVer2:Account_Number,JOBSVer2:Current_Courier,JOBSVer2:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(JOBSVer2:Despatch_Number,JOBSVer2:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(JOBSVer2:Courier,JOBSVer2:Date_Despatched,JOBSVer2:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(JOBSVer2:Loan_Courier,JOBSVer2:Loan_Despatched,JOBSVer2:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(JOBSVer2:Exchange_Courier,JOBSVer2:Exchange_Despatched,JOBSVer2:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(JOBSVer2:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(JOBSVer2:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(JOBSVer2:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(JOBSVer2:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(JOBSVer2:Bouncer,JOBSVer2:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(JOBSVer2:Engineer,JOBSVer2:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(JOBSVer2:Exchange_Status,JOBSVer2:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(JOBSVer2:Loan_Status,JOBSVer2:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(JOBSVer2:Exchange_Status,JOBSVer2:Location,JOBSVer2:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(JOBSVer2:Loan_Status,JOBSVer2:Location,JOBSVer2:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(JOBSVer2:InvoiceAccount,JOBSVer2:InvoiceBatch,JOBSVer2:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(JOBSVer2:InvoiceAccount,JOBSVer2:InvoiceBatch,JOBSVer2:InvoiceStatus,JOBSVer2:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                       END
!------------ End File: JOBS, version 2 ------------------

!------------ File: JOBS, version 3 ----------------------
!------------ modified 20.02.2002 at 14:32:57 -----------------
MOD:stFileName:JOBSVer3  STRING(260)
JOBSVer3               FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:JOBSVer3),PRE(JOBSVer3),BINDABLE,CREATE
Ref_Number_Key           KEY(JOBSVer3:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(JOBSVer3:Model_Number,JOBSVer3:Unit_Type),DUP,NOCASE
EngCompKey               KEY(JOBSVer3:Engineer,JOBSVer3:Completed,JOBSVer3:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(JOBSVer3:Engineer,JOBSVer3:Workshop,JOBSVer3:Ref_Number),DUP,NOCASE
Surname_Key              KEY(JOBSVer3:Surname),DUP,NOCASE
MobileNumberKey          KEY(JOBSVer3:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(JOBSVer3:ESN),DUP,NOCASE
MSN_Key                  KEY(JOBSVer3:MSN),DUP,NOCASE
AccountNumberKey         KEY(JOBSVer3:Account_Number,JOBSVer3:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(JOBSVer3:Account_Number,JOBSVer3:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(JOBSVer3:Model_Number),DUP,NOCASE
Engineer_Key             KEY(JOBSVer3:Engineer,JOBSVer3:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(JOBSVer3:date_booked),DUP,NOCASE
DateCompletedKey         KEY(JOBSVer3:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(JOBSVer3:Model_Number,JOBSVer3:Date_Completed),DUP,NOCASE
By_Status                KEY(JOBSVer3:Current_Status,JOBSVer3:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(JOBSVer3:Current_Status,JOBSVer3:Location,JOBSVer3:Ref_Number),DUP,NOCASE
Location_Key             KEY(JOBSVer3:Location,JOBSVer3:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(JOBSVer3:Third_Party_Site,JOBSVer3:Third_Party_Printed,JOBSVer3:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(JOBSVer3:Third_Party_Site,JOBSVer3:Third_Party_Printed,JOBSVer3:ESN),DUP,NOCASE
ThirdMsnKey              KEY(JOBSVer3:Third_Party_Site,JOBSVer3:Third_Party_Printed,JOBSVer3:MSN),DUP,NOCASE
PriorityTypeKey          KEY(JOBSVer3:Job_Priority,JOBSVer3:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(JOBSVer3:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(JOBSVer3:Manufacturer,JOBSVer3:EDI,JOBSVer3:EDI_Batch_Number,JOBSVer3:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(JOBSVer3:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(JOBSVer3:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(JOBSVer3:Batch_Number,JOBSVer3:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(JOBSVer3:Batch_Number,JOBSVer3:Current_Status,JOBSVer3:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(JOBSVer3:Batch_Number,JOBSVer3:Model_Number,JOBSVer3:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(JOBSVer3:Batch_Number,JOBSVer3:Invoice_Number,JOBSVer3:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(JOBSVer3:Batch_Number,JOBSVer3:Completed,JOBSVer3:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(JOBSVer3:Chargeable_Job,JOBSVer3:Account_Number,JOBSVer3:Invoice_Number,JOBSVer3:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(JOBSVer3:Invoice_Exception,JOBSVer3:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(JOBSVer3:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(JOBSVer3:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(JOBSVer3:Despatched,JOBSVer3:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(JOBSVer3:Despatched,JOBSVer3:Account_Number,JOBSVer3:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(JOBSVer3:Despatched,JOBSVer3:Current_Courier,JOBSVer3:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(JOBSVer3:Despatched,JOBSVer3:Account_Number,JOBSVer3:Current_Courier,JOBSVer3:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(JOBSVer3:Despatch_Number,JOBSVer3:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(JOBSVer3:Courier,JOBSVer3:Date_Despatched,JOBSVer3:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(JOBSVer3:Loan_Courier,JOBSVer3:Loan_Despatched,JOBSVer3:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(JOBSVer3:Exchange_Courier,JOBSVer3:Exchange_Despatched,JOBSVer3:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(JOBSVer3:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(JOBSVer3:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(JOBSVer3:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(JOBSVer3:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(JOBSVer3:Bouncer,JOBSVer3:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(JOBSVer3:Engineer,JOBSVer3:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(JOBSVer3:Exchange_Status,JOBSVer3:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(JOBSVer3:Loan_Status,JOBSVer3:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(JOBSVer3:Exchange_Status,JOBSVer3:Location,JOBSVer3:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(JOBSVer3:Loan_Status,JOBSVer3:Location,JOBSVer3:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(JOBSVer3:InvoiceAccount,JOBSVer3:InvoiceBatch,JOBSVer3:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(JOBSVer3:InvoiceAccount,JOBSVer3:InvoiceBatch,JOBSVer3:InvoiceStatus,JOBSVer3:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                       END
!------------ End File: JOBS, version 3 ------------------

!------------ File: JOBS, version 4 ----------------------
!------------ modified 25.06.2003 at 15:38:14 -----------------
MOD:stFileName:JOBSVer4  STRING(260)
JOBSVer4               FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:JOBSVer4),PRE(JOBSVer4),BINDABLE,CREATE
Ref_Number_Key           KEY(JOBSVer4:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(JOBSVer4:Model_Number,JOBSVer4:Unit_Type),DUP,NOCASE
EngCompKey               KEY(JOBSVer4:Engineer,JOBSVer4:Completed,JOBSVer4:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(JOBSVer4:Engineer,JOBSVer4:Workshop,JOBSVer4:Ref_Number),DUP,NOCASE
Surname_Key              KEY(JOBSVer4:Surname),DUP,NOCASE
MobileNumberKey          KEY(JOBSVer4:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(JOBSVer4:ESN),DUP,NOCASE
MSN_Key                  KEY(JOBSVer4:MSN),DUP,NOCASE
AccountNumberKey         KEY(JOBSVer4:Account_Number,JOBSVer4:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(JOBSVer4:Account_Number,JOBSVer4:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(JOBSVer4:Model_Number),DUP,NOCASE
Engineer_Key             KEY(JOBSVer4:Engineer,JOBSVer4:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(JOBSVer4:date_booked),DUP,NOCASE
DateCompletedKey         KEY(JOBSVer4:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(JOBSVer4:Model_Number,JOBSVer4:Date_Completed),DUP,NOCASE
By_Status                KEY(JOBSVer4:Current_Status,JOBSVer4:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(JOBSVer4:Current_Status,JOBSVer4:Location,JOBSVer4:Ref_Number),DUP,NOCASE
Location_Key             KEY(JOBSVer4:Location,JOBSVer4:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(JOBSVer4:Third_Party_Site,JOBSVer4:Third_Party_Printed,JOBSVer4:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(JOBSVer4:Third_Party_Site,JOBSVer4:Third_Party_Printed,JOBSVer4:ESN),DUP,NOCASE
ThirdMsnKey              KEY(JOBSVer4:Third_Party_Site,JOBSVer4:Third_Party_Printed,JOBSVer4:MSN),DUP,NOCASE
PriorityTypeKey          KEY(JOBSVer4:Job_Priority,JOBSVer4:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(JOBSVer4:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(JOBSVer4:Manufacturer,JOBSVer4:EDI,JOBSVer4:EDI_Batch_Number,JOBSVer4:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(JOBSVer4:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(JOBSVer4:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(JOBSVer4:Batch_Number,JOBSVer4:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(JOBSVer4:Batch_Number,JOBSVer4:Current_Status,JOBSVer4:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(JOBSVer4:Batch_Number,JOBSVer4:Model_Number,JOBSVer4:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(JOBSVer4:Batch_Number,JOBSVer4:Invoice_Number,JOBSVer4:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(JOBSVer4:Batch_Number,JOBSVer4:Completed,JOBSVer4:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(JOBSVer4:Chargeable_Job,JOBSVer4:Account_Number,JOBSVer4:Invoice_Number,JOBSVer4:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(JOBSVer4:Invoice_Exception,JOBSVer4:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(JOBSVer4:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(JOBSVer4:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(JOBSVer4:Despatched,JOBSVer4:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(JOBSVer4:Despatched,JOBSVer4:Account_Number,JOBSVer4:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(JOBSVer4:Despatched,JOBSVer4:Current_Courier,JOBSVer4:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(JOBSVer4:Despatched,JOBSVer4:Account_Number,JOBSVer4:Current_Courier,JOBSVer4:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(JOBSVer4:Despatch_Number,JOBSVer4:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(JOBSVer4:Courier,JOBSVer4:Date_Despatched,JOBSVer4:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(JOBSVer4:Loan_Courier,JOBSVer4:Loan_Despatched,JOBSVer4:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(JOBSVer4:Exchange_Courier,JOBSVer4:Exchange_Despatched,JOBSVer4:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(JOBSVer4:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(JOBSVer4:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(JOBSVer4:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(JOBSVer4:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(JOBSVer4:Bouncer,JOBSVer4:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(JOBSVer4:Engineer,JOBSVer4:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(JOBSVer4:Exchange_Status,JOBSVer4:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(JOBSVer4:Loan_Status,JOBSVer4:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(JOBSVer4:Exchange_Status,JOBSVer4:Location,JOBSVer4:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(JOBSVer4:Loan_Status,JOBSVer4:Location,JOBSVer4:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(JOBSVer4:InvoiceAccount,JOBSVer4:InvoiceBatch,JOBSVer4:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(JOBSVer4:InvoiceAccount,JOBSVer4:InvoiceBatch,JOBSVer4:InvoiceStatus,JOBSVer4:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
RF_Board_IMEI               STRING(20)
                         END
                       END
!------------ End File: JOBS, version 4 ------------------

!------------ File: MODELNUM, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:58 -----------------
MOD:stFileName:MODELNUMVer1  STRING(260)
MODELNUMVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer1),PRE(MODELNUMVer1)
Model_Number_Key         KEY(MODELNUMVer1:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer1:Manufacturer,MODELNUMVer1:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer1:Manufacturer,MODELNUMVer1:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
Dummy_Field                 STRING(1)
                         END
                       END
!------------ End File: MODELNUM, version 1 ------------------

!------------ File: MODELNUM, version 2 ----------------------
!------------ modified 12.03.2002 at 14:29:14 -----------------
MOD:stFileName:MODELNUMVer2  STRING(260)
MODELNUMVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer2),PRE(MODELNUMVer2),CREATE
Model_Number_Key         KEY(MODELNUMVer2:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer2:Manufacturer,MODELNUMVer2:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer2:Manufacturer,MODELNUMVer2:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
                         END
                       END
!------------ End File: MODELNUM, version 2 ------------------

!------------ File: MODELNUM, version 3 ----------------------
!------------ modified 10.05.2004 at 11:12:54 -----------------
MOD:stFileName:MODELNUMVer3  STRING(260)
MODELNUMVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer3),PRE(MODELNUMVer3),CREATE
Model_Number_Key         KEY(MODELNUMVer3:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer3:Manufacturer,MODELNUMVer3:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer3:Manufacturer,MODELNUMVer3:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
                         END
                       END
!------------ End File: MODELNUM, version 3 ------------------

!------------ File: MODELNUM, version 4 ----------------------
!------------ modified 01.06.2005 at 14:56:26 -----------------
MOD:stFileName:MODELNUMVer4  STRING(260)
MODELNUMVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer4),PRE(MODELNUMVer4),CREATE
Model_Number_Key         KEY(MODELNUMVer4:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer4:Manufacturer,MODELNUMVer4:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer4:Manufacturer,MODELNUMVer4:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
                         END
                       END
!------------ End File: MODELNUM, version 4 ------------------

!------------ File: MODELNUM, version 5 ----------------------
!------------ modified 16.11.2005 at 11:18:40 -----------------
MOD:stFileName:MODELNUMVer5  STRING(260)
MODELNUMVer5           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer5),PRE(MODELNUMVer5),CREATE
Model_Number_Key         KEY(MODELNUMVer5:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer5:Manufacturer,MODELNUMVer5:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer5:Manufacturer,MODELNUMVer5:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
                         END
                       END
!------------ End File: MODELNUM, version 5 ------------------

!------------ File: MODELNUM, version 6 ----------------------
!------------ modified 28.09.2007 at 10:00:24 -----------------
MOD:stFileName:MODELNUMVer6  STRING(260)
MODELNUMVer6           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer6),PRE(MODELNUMVer6),CREATE
Model_Number_Key         KEY(MODELNUMVer6:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer6:Manufacturer,MODELNUMVer6:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer6:Manufacturer,MODELNUMVer6:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
                         END
                       END
!------------ End File: MODELNUM, version 6 ------------------

!------------ File: MODELNUM, version 7 ----------------------
!------------ modified 17.03.2008 at 09:59:43 -----------------
MOD:stFileName:MODELNUMVer7  STRING(260)
MODELNUMVer7           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer7),PRE(MODELNUMVer7),CREATE
Model_Number_Key         KEY(MODELNUMVer7:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer7:Manufacturer,MODELNUMVer7:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer7:Manufacturer,MODELNUMVer7:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
                         END
                       END
!------------ End File: MODELNUM, version 7 ------------------

!------------ File: MODELNUM, version 8 ----------------------
!------------ modified 03.07.2008 at 14:38:53 -----------------
MOD:stFileName:MODELNUMVer8  STRING(260)
MODELNUMVer8           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer8),PRE(MODELNUMVer8),CREATE
Model_Number_Key         KEY(MODELNUMVer8:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer8:Manufacturer,MODELNUMVer8:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer8:Manufacturer,MODELNUMVer8:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
                         END
                       END
!------------ End File: MODELNUM, version 8 ------------------

!------------ File: MODELNUM, version 9 ----------------------
!------------ modified 04.11.2008 at 11:05:40 -----------------
MOD:stFileName:MODELNUMVer9  STRING(260)
MODELNUMVer9           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer9),PRE(MODELNUMVer9),CREATE
Model_Number_Key         KEY(MODELNUMVer9:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer9:Manufacturer,MODELNUMVer9:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer9:Manufacturer,MODELNUMVer9:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
                         END
                       END
!------------ End File: MODELNUM, version 9 ------------------

!------------ File: MODELNUM, version 10 ----------------------
!------------ modified 21.07.2009 at 10:13:19 -----------------
MOD:stFileName:MODELNUMVer10  STRING(260)
MODELNUMVer10          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer10),PRE(MODELNUMVer10),CREATE
Model_Number_Key         KEY(MODELNUMVer10:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer10:Manufacturer,MODELNUMVer10:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer10:Manufacturer,MODELNUMVer10:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
                         END
                       END
!------------ End File: MODELNUM, version 10 ------------------

!------------ File: MODELNUM, version 11 ----------------------
!------------ modified 26.01.2010 at 10:12:56 -----------------
MOD:stFileName:MODELNUMVer11  STRING(260)
MODELNUMVer11          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer11),PRE(MODELNUMVer11),CREATE
Model_Number_Key         KEY(MODELNUMVer11:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer11:Manufacturer,MODELNUMVer11:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer11:Manufacturer,MODELNUMVer11:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
ModelSpecificURL1           STRING(250)
ModelSpecificURL2           STRING(250)
ModelSpecificURL3           STRING(250)
                         END
                       END
!------------ End File: MODELNUM, version 11 ------------------

!------------ File: MODELNUM, version 12 ----------------------
!------------ modified 25.02.2010 at 10:47:36 -----------------
MOD:stFileName:MODELNUMVer12  STRING(260)
MODELNUMVer12          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer12),PRE(MODELNUMVer12),CREATE
Model_Number_Key         KEY(MODELNUMVer12:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer12:Manufacturer,MODELNUMVer12:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer12:Manufacturer,MODELNUMVer12:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
ModelSpecificURL1           STRING(250)
ModelSpecificURL2           STRING(250)
ModelSpecificURL3           STRING(250)
CCExchange28Day             BYTE
                         END
                       END
!------------ End File: MODELNUM, version 12 ------------------

!------------ File: MODELNUM, version 13 ----------------------
!------------ modified 25.06.2010 at 10:28:22 -----------------
MOD:stFileName:MODELNUMVer13  STRING(260)
MODELNUMVer13          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer13),PRE(MODELNUMVer13),CREATE
Model_Number_Key         KEY(MODELNUMVer13:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer13:Manufacturer,MODELNUMVer13:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer13:Manufacturer,MODELNUMVer13:Unit_Type),DUP,NOCASE
RetailRepairTierKey      KEY(MODELNUMVer13:RetailRepairTier),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
ModelSpecificURL1           STRING(250)
ModelSpecificURL2           STRING(250)
ModelSpecificURL3           STRING(250)
CCExchange28Day             BYTE
RetailRepairTier            BYTE
ExchangeCharge              PDECIMAL(6,2)
PostalRepairCharge          PDECIMAL(6,2)
                         END
                       END
!------------ End File: MODELNUM, version 13 ------------------

!------------ File: MODELNUM, version 14 ----------------------
!------------ modified 01.07.2010 at 15:00:25 -----------------
MOD:stFileName:MODELNUMVer14  STRING(260)
MODELNUMVer14          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer14),PRE(MODELNUMVer14),CREATE
Model_Number_Key         KEY(MODELNUMVer14:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer14:Manufacturer,MODELNUMVer14:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer14:Manufacturer,MODELNUMVer14:Unit_Type),DUP,NOCASE
RetailRepairTierKey      KEY(MODELNUMVer14:RetailRepairTier),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
ModelSpecificURL1           STRING(250)
ModelSpecificURL2           STRING(250)
ModelSpecificURL3           STRING(250)
CCExchange28Day             BYTE
RetailRepairTier            BYTE
ExchangeCharge              PDECIMAL(6,2)
PostalRepairCharge          PDECIMAL(6,2)
Replacement28Day            BYTE
                         END
                       END
!------------ End File: MODELNUM, version 14 ------------------

!------------ File: MODELNUM, version 15 ----------------------
!------------ modified 12.12.2012 at 11:32:15 -----------------
MOD:stFileName:MODELNUMVer15  STRING(260)
MODELNUMVer15          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer15),PRE(MODELNUMVer15),CREATE
Model_Number_Key         KEY(MODELNUMVer15:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer15:Manufacturer,MODELNUMVer15:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer15:Manufacturer,MODELNUMVer15:Unit_Type),DUP,NOCASE
RetailRepairTierKey      KEY(MODELNUMVer15:RetailRepairTier),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
ModelSpecificURL1           STRING(250)
ModelSpecificURL2           STRING(250)
ModelSpecificURL3           STRING(250)
CCExchange28Day             BYTE
RetailRepairTier            BYTE
ExchangeCharge              PDECIMAL(6,2)
PostalRepairCharge          PDECIMAL(6,2)
Replacement28Day            BYTE
TabletDevice                BYTE
                         END
                       END
!------------ End File: MODELNUM, version 15 ------------------

!------------ File: MODELNUM, version 16 ----------------------
!------------ modified 08.02.2013 at 12:44:28 -----------------
MOD:stFileName:MODELNUMVer16  STRING(260)
MODELNUMVer16          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer16),PRE(MODELNUMVer16),CREATE
Model_Number_Key         KEY(MODELNUMVer16:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer16:Manufacturer,MODELNUMVer16:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer16:Manufacturer,MODELNUMVer16:Unit_Type),DUP,NOCASE
RetailRepairTierKey      KEY(MODELNUMVer16:RetailRepairTier),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
ModelSpecificURL1           STRING(250)
ModelSpecificURL2           STRING(250)
ModelSpecificURL3           STRING(250)
CCExchange28Day             BYTE
RetailRepairTier            BYTE
ExchangeCharge              PDECIMAL(6,2)
PostalRepairCharge          PDECIMAL(6,2)
Replacement28Day            BYTE
TabletDevice                BYTE
ReturnPeriod                LONG
                         END
                       END
!------------ End File: MODELNUM, version 16 ------------------

!------------ File: MODELNUM, version 17 ----------------------
!------------ modified 14.03.2013 at 09:27:22 -----------------
MOD:stFileName:MODELNUMVer17  STRING(260)
MODELNUMVer17          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer17),PRE(MODELNUMVer17),CREATE
Model_Number_Key         KEY(MODELNUMVer17:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer17:Manufacturer,MODELNUMVer17:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer17:Manufacturer,MODELNUMVer17:Unit_Type),DUP,NOCASE
RetailRepairTierKey      KEY(MODELNUMVer17:RetailRepairTier),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
ModelSpecificURL1           STRING(250)
ModelSpecificURL2           STRING(250)
ModelSpecificURL3           STRING(250)
CCExchange28Day             BYTE
RetailRepairTier            BYTE
ExchangeCharge              PDECIMAL(6,2)
PostalRepairCharge          PDECIMAL(6,2)
Replacement28Day            BYTE
TabletDevice                BYTE
ReturnPeriod                LONG
BERCharge                   PDECIMAL(6,2)
                         END
                       END
!------------ End File: MODELNUM, version 17 ------------------

!------------ File: MODELNUM, version 18 ----------------------
!------------ modified 11.05.2015 at 11:41:18 -----------------
MOD:stFileName:MODELNUMVer18  STRING(260)
MODELNUMVer18          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer18),PRE(MODELNUMVer18),CREATE
Model_Number_Key         KEY(MODELNUMVer18:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer18:Manufacturer,MODELNUMVer18:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer18:Manufacturer,MODELNUMVer18:Unit_Type),DUP,NOCASE
RetailRepairTierKey      KEY(MODELNUMVer18:RetailRepairTier),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
ModelSpecificURL1           STRING(250)
ModelSpecificURL2           STRING(250)
ModelSpecificURL3           STRING(250)
CCExchange28Day             BYTE
RetailRepairTier            BYTE
ExchangeCharge              PDECIMAL(6,2)
PostalRepairCharge          PDECIMAL(6,2)
Replacement28Day            BYTE
TabletDevice                BYTE
ReturnPeriod                LONG
BERCharge                   PDECIMAL(6,2)
Router                      LONG
                         END
                       END
!------------ End File: MODELNUM, version 18 ------------------

!------------ File: MODELNUM, version 19 ----------------------
!------------ modified 18.05.2015 at 16:10:25 -----------------
MOD:stFileName:MODELNUMVer19  STRING(260)
MODELNUMVer19          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MODELNUMVer19),PRE(MODELNUMVer19),CREATE
Model_Number_Key         KEY(MODELNUMVer19:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer19:Manufacturer,MODELNUMVer19:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer19:Manufacturer,MODELNUMVer19:Unit_Type),DUP,NOCASE
RetailRepairTierKey      KEY(MODELNUMVer19:RetailRepairTier),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
ModelSpecificURL1           STRING(250)
ModelSpecificURL2           STRING(250)
ModelSpecificURL3           STRING(250)
CCExchange28Day             BYTE
RetailRepairTier            BYTE
ExchangeCharge              PDECIMAL(6,2)
PostalRepairCharge          PDECIMAL(6,2)
Replacement28Day            BYTE
TabletDevice                BYTE
ReturnPeriod                LONG
BERCharge                   PDECIMAL(6,2)
Router                      LONG
AccessoryProduct            LONG
SerialisedProduct           LONG
                         END
                       END
!------------ End File: MODELNUM, version 19 ------------------

!------------ File: STATUS, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:58 -----------------
MOD:stFileName:STATUSVer1  STRING(260)
STATUSVer1             FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STATUSVer1),PRE(STATUSVer1)
Ref_Number_Only_Key      KEY(STATUSVer1:Ref_Number),NOCASE,PRIMARY
Status_Key               KEY(STATUSVer1:Status),DUP,NOCASE
Heading_Key              KEY(STATUSVer1:Heading_Ref_Number,STATUSVer1:Status),NOCASE
Ref_Number_Key           KEY(STATUSVer1:Heading_Ref_Number,STATUSVer1:Ref_Number),NOCASE
LoanKey                  KEY(STATUSVer1:Loan,STATUSVer1:Status),DUP,NOCASE
ExchangeKey              KEY(STATUSVer1:Exchange,STATUSVer1:Status),DUP,NOCASE
JobKey                   KEY(STATUSVer1:Job,STATUSVer1:Status),DUP,NOCASE
TurnJobKey               KEY(STATUSVer1:Use_Turnaround_Time,STATUSVer1:Job,STATUSVer1:Status),DUP,NOCASE
Record                   RECORD,PRE()
Heading_Ref_Number          REAL
Status                      STRING(30)
Ref_Number                  REAL
Use_Turnaround_Time         STRING(3)
Use_Turnaround_Days         STRING(3)
Turnaround_Days             LONG
Use_Turnaround_Hours        STRING(3)
Turnaround_Hours            LONG
Notes                       STRING(1000)
Loan                        STRING(3)
Exchange                    STRING(3)
Job                         STRING(3)
SystemStatus                STRING(3)
EngineerStatus              BYTE
                         END
                       END
!------------ End File: STATUS, version 1 ------------------

!------------ File: STATUS, version 2 ----------------------
!------------ modified 09.08.2002 at 11:40:02 -----------------
MOD:stFileName:STATUSVer2  STRING(260)
STATUSVer2             FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STATUSVer2),PRE(STATUSVer2),CREATE
Ref_Number_Only_Key      KEY(STATUSVer2:Ref_Number),NOCASE,PRIMARY
Status_Key               KEY(STATUSVer2:Status),DUP,NOCASE
Heading_Key              KEY(STATUSVer2:Heading_Ref_Number,STATUSVer2:Status),NOCASE
Ref_Number_Key           KEY(STATUSVer2:Heading_Ref_Number,STATUSVer2:Ref_Number),NOCASE
LoanKey                  KEY(STATUSVer2:Loan,STATUSVer2:Status),DUP,NOCASE
ExchangeKey              KEY(STATUSVer2:Exchange,STATUSVer2:Status),DUP,NOCASE
JobKey                   KEY(STATUSVer2:Job,STATUSVer2:Status),DUP,NOCASE
TurnJobKey               KEY(STATUSVer2:Use_Turnaround_Time,STATUSVer2:Job,STATUSVer2:Status),DUP,NOCASE
Record                   RECORD,PRE()
Heading_Ref_Number          REAL
Status                      STRING(30)
Ref_Number                  REAL
Use_Turnaround_Time         STRING(3)
Use_Turnaround_Days         STRING(3)
Turnaround_Days             LONG
Use_Turnaround_Hours        STRING(3)
Turnaround_Hours            LONG
Notes                       STRING(1000)
Loan                        STRING(3)
Exchange                    STRING(3)
Job                         STRING(3)
SystemStatus                STRING(3)
EngineerStatus              BYTE
EnableEmail                 BYTE
SenderEmailAddress          STRING(255)
EmailSubject                STRING(255)
EmailBody                   STRING(500)
                         END
                       END
!------------ End File: STATUS, version 2 ------------------

!------------ File: STATUS, version 3 ----------------------
!------------ modified 15.01.2004 at 15:07:25 -----------------
MOD:stFileName:STATUSVer3  STRING(260)
STATUSVer3             FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:STATUSVer3),PRE(STATUSVer3),CREATE
Ref_Number_Only_Key      KEY(STATUSVer3:Ref_Number),NOCASE,PRIMARY
Status_Key               KEY(STATUSVer3:Status),DUP,NOCASE
Heading_Key              KEY(STATUSVer3:Heading_Ref_Number,STATUSVer3:Status),NOCASE
Ref_Number_Key           KEY(STATUSVer3:Heading_Ref_Number,STATUSVer3:Ref_Number),NOCASE
LoanKey                  KEY(STATUSVer3:Loan,STATUSVer3:Status),DUP,NOCASE
ExchangeKey              KEY(STATUSVer3:Exchange,STATUSVer3:Status),DUP,NOCASE
JobKey                   KEY(STATUSVer3:Job,STATUSVer3:Status),DUP,NOCASE
TurnJobKey               KEY(STATUSVer3:Use_Turnaround_Time,STATUSVer3:Job,STATUSVer3:Status),DUP,NOCASE
Record                   RECORD,PRE()
Heading_Ref_Number          REAL
PickNoteEnable              BYTE
Status                      STRING(30)
Ref_Number                  REAL
Use_Turnaround_Time         STRING(3)
Use_Turnaround_Days         STRING(3)
Turnaround_Days             LONG
Use_Turnaround_Hours        STRING(3)
Turnaround_Hours            LONG
Notes                       STRING(1000)
Loan                        STRING(3)
Exchange                    STRING(3)
Job                         STRING(3)
SystemStatus                STRING(3)
EngineerStatus              BYTE
EnableEmail                 BYTE
SenderEmailAddress          STRING(255)
EmailSubject                STRING(255)
EmailBody                   STRING(500)
                         END
                       END
!------------ End File: STATUS, version 3 ------------------

!------------ File: TRADEACC, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:58 -----------------
MOD:stFileName:TRADEACCVer1  STRING(260)
TRADEACCVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer1),PRE(TRADEACCVer1)
Account_Number_Key       KEY(TRADEACCVer1:Account_Number),NOCASE,PRIMARY
Company_Name_Key         KEY(TRADEACCVer1:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
                         END
                       END
!------------ End File: TRADEACC, version 1 ------------------

!------------ File: TRADEACC, version 2 ----------------------
!------------ modified 25.02.2002 at 15:13:27 -----------------
MOD:stFileName:TRADEACCVer2  STRING(260)
TRADEACCVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer2),PRE(TRADEACCVer2),CREATE
Account_Number_Key       KEY(TRADEACCVer2:Account_Number),NOCASE,PRIMARY
Company_Name_Key         KEY(TRADEACCVer2:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
                         END
                       END
!------------ End File: TRADEACC, version 2 ------------------

!------------ File: TRADEACC, version 3 ----------------------
!------------ modified 04.03.2002 at 16:58:27 -----------------
MOD:stFileName:TRADEACCVer3  STRING(260)
TRADEACCVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer3),PRE(TRADEACCVer3),CREATE
Account_Number_Key       KEY(TRADEACCVer3:Account_Number),NOCASE,PRIMARY
Company_Name_Key         KEY(TRADEACCVer3:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
                         END
                       END
!------------ End File: TRADEACC, version 3 ------------------

!------------ File: TRADEACC, version 4 ----------------------
!------------ modified 11.03.2002 at 14:52:48 -----------------
MOD:stFileName:TRADEACCVer4  STRING(260)
TRADEACCVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer4),PRE(TRADEACCVer4),CREATE
Account_Number_Key       KEY(TRADEACCVer4:Account_Number),NOCASE,PRIMARY
Company_Name_Key         KEY(TRADEACCVer4:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
                         END
                       END
!------------ End File: TRADEACC, version 4 ------------------

!------------ File: TRADEACC, version 5 ----------------------
!------------ modified 09.08.2002 at 11:40:02 -----------------
MOD:stFileName:TRADEACCVer5  STRING(260)
TRADEACCVer5           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer5),PRE(TRADEACCVer5),CREATE
RecordNumberKey          KEY(TRADEACCVer5:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer5:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer5:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
                         END
                       END
!------------ End File: TRADEACC, version 5 ------------------

!------------ File: TRADEACC, version 6 ----------------------
!------------ modified 09.12.2002 at 15:10:29 -----------------
MOD:stFileName:TRADEACCVer6  STRING(260)
TRADEACCVer6           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer6),PRE(TRADEACCVer6),CREATE
RecordNumberKey          KEY(TRADEACCVer6:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer6:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer6:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
MakeSplitJob                BYTE
SplitJobStatus              STRING(30)
SplitExchangeStatus         STRING(30)
SplitCChargeType            STRING(30)
SplitWChargeType            STRING(30)
                         END
                       END
!------------ End File: TRADEACC, version 6 ------------------

!------------ File: TRADEACC, version 7 ----------------------
!------------ modified 14.05.2003 at 14:26:14 -----------------
MOD:stFileName:TRADEACCVer7  STRING(260)
TRADEACCVer7           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:TRADEACCVer7),PRE(TRADEACCVer7),CREATE
RecordNumberKey          KEY(TRADEACCVer7:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer7:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer7:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
MakeSplitJob                BYTE
SplitJobStatus              STRING(30)
SplitExchangeStatus         STRING(30)
SplitCChargeType            STRING(30)
SplitWChargeType            STRING(30)
CheckChargeablePartsCost    BYTE
MaxChargeablePartsCost      REAL
                         END
                       END
!------------ End File: TRADEACC, version 7 ------------------

!------------ File: SUBTRACC, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:58 -----------------
MOD:stFileName:SUBTRACCVer1  STRING(260)
SUBTRACCVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUBTRACCVer1),PRE(SUBTRACCVer1)
Main_Account_Key         KEY(SUBTRACCVer1:Main_Account_Number,SUBTRACCVer1:Account_Number),NOCASE,PRIMARY
Main_Name_Key            KEY(SUBTRACCVer1:Main_Account_Number,SUBTRACCVer1:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer1:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer1:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer1:Main_Account_Number,SUBTRACCVer1:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer1:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
                         END
                       END
!------------ End File: SUBTRACC, version 1 ------------------

!------------ File: SUBTRACC, version 2 ----------------------
!------------ modified 25.02.2002 at 15:13:27 -----------------
MOD:stFileName:SUBTRACCVer2  STRING(260)
SUBTRACCVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUBTRACCVer2),PRE(SUBTRACCVer2),CREATE
Main_Account_Key         KEY(SUBTRACCVer2:Main_Account_Number,SUBTRACCVer2:Account_Number),NOCASE,PRIMARY
Main_Name_Key            KEY(SUBTRACCVer2:Main_Account_Number,SUBTRACCVer2:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer2:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer2:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer2:Main_Account_Number,SUBTRACCVer2:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer2:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
                         END
                       END
!------------ End File: SUBTRACC, version 2 ------------------

!------------ File: SUBTRACC, version 3 ----------------------
!------------ modified 04.03.2002 at 16:58:27 -----------------
MOD:stFileName:SUBTRACCVer3  STRING(260)
SUBTRACCVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUBTRACCVer3),PRE(SUBTRACCVer3),CREATE
Main_Account_Key         KEY(SUBTRACCVer3:Main_Account_Number,SUBTRACCVer3:Account_Number),NOCASE,PRIMARY
Main_Name_Key            KEY(SUBTRACCVer3:Main_Account_Number,SUBTRACCVer3:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer3:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer3:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer3:Main_Account_Number,SUBTRACCVer3:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer3:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
                         END
                       END
!------------ End File: SUBTRACC, version 3 ------------------

!------------ File: SUBTRACC, version 4 ----------------------
!------------ modified 11.03.2002 at 14:52:48 -----------------
MOD:stFileName:SUBTRACCVer4  STRING(260)
SUBTRACCVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUBTRACCVer4),PRE(SUBTRACCVer4),CREATE
Main_Account_Key         KEY(SUBTRACCVer4:Main_Account_Number,SUBTRACCVer4:Account_Number),NOCASE,PRIMARY
Main_Name_Key            KEY(SUBTRACCVer4:Main_Account_Number,SUBTRACCVer4:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer4:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer4:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer4:Main_Account_Number,SUBTRACCVer4:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer4:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
                         END
                       END
!------------ End File: SUBTRACC, version 4 ------------------

!------------ File: SUBTRACC, version 5 ----------------------
!------------ modified 09.08.2002 at 11:40:02 -----------------
MOD:stFileName:SUBTRACCVer5  STRING(260)
SUBTRACCVer5           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUBTRACCVer5),PRE(SUBTRACCVer5),CREATE
RecordNumberKey          KEY(SUBTRACCVer5:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(SUBTRACCVer5:Main_Account_Number,SUBTRACCVer5:Account_Number),NOCASE
Main_Name_Key            KEY(SUBTRACCVer5:Main_Account_Number,SUBTRACCVer5:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer5:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer5:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer5:Main_Account_Number,SUBTRACCVer5:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer5:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
                         END
                       END
!------------ End File: SUBTRACC, version 5 ------------------

!------------ File: SUBTRACC, version 6 ----------------------
!------------ modified 21.07.2005 at 14:31:28 -----------------
MOD:stFileName:SUBTRACCVer6  STRING(260)
SUBTRACCVer6           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUBTRACCVer6),PRE(SUBTRACCVer6),CREATE
RecordNumberKey          KEY(SUBTRACCVer6:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(SUBTRACCVer6:Main_Account_Number,SUBTRACCVer6:Account_Number),NOCASE
Main_Name_Key            KEY(SUBTRACCVer6:Main_Account_Number,SUBTRACCVer6:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer6:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer6:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer6:Main_Account_Number,SUBTRACCVer6:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer6:Company_Name),DUP,NOCASE
Contact_Centre_Key       KEY(SUBTRACCVer6:IsContactCentre,SUBTRACCVer6:Account_Number),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
IsContactCentre             BYTE
                         END
                       END
!------------ End File: SUBTRACC, version 6 ------------------

!------------ File: SUBTRACC, version 7 ----------------------
!------------ modified 16.11.2005 at 11:18:41 -----------------
MOD:stFileName:SUBTRACCVer7  STRING(260)
SUBTRACCVer7           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:SUBTRACCVer7),PRE(SUBTRACCVer7),CREATE
RecordNumberKey          KEY(SUBTRACCVer7:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(SUBTRACCVer7:Main_Account_Number,SUBTRACCVer7:Account_Number),NOCASE
Main_Name_Key            KEY(SUBTRACCVer7:Main_Account_Number,SUBTRACCVer7:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer7:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer7:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer7:Main_Account_Number,SUBTRACCVer7:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer7:Company_Name),DUP,NOCASE
Contact_Centre_Key       KEY(SUBTRACCVer7:IsContactCentre,SUBTRACCVer7:Account_Number),DUP,NOCASE
RetailStoreKey           KEY(SUBTRACCVer7:VodafoneRetailStore,SUBTRACCVer7:Account_Number),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
IsContactCentre             BYTE
VodafoneRetailStore         BYTE
                         END
                       END
!------------ End File: SUBTRACC, version 7 ------------------

!------------ File: MANUFACT, version 1 ----------------------
!------------ modified 18.02.2002 at 10:19:58 -----------------
MOD:stFileName:MANUFACTVer1  STRING(260)
MANUFACTVer1           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer1),PRE(MANUFACTVer1)
RecordNumberKey          KEY(MANUFACTVer1:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer1:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
                         END
                       END
!------------ End File: MANUFACT, version 1 ------------------

!------------ File: MANUFACT, version 2 ----------------------
!------------ modified 09.08.2002 at 11:40:02 -----------------
MOD:stFileName:MANUFACTVer2  STRING(260)
MANUFACTVer2           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer2),PRE(MANUFACTVer2),CREATE
RecordNumberKey          KEY(MANUFACTVer2:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer2:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
                         END
                       END
!------------ End File: MANUFACT, version 2 ------------------

!------------ File: MANUFACT, version 3 ----------------------
!------------ modified 02.06.2003 at 14:52:45 -----------------
MOD:stFileName:MANUFACTVer3  STRING(260)
MANUFACTVer3           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer3),PRE(MANUFACTVer3),CREATE
RecordNumberKey          KEY(MANUFACTVer3:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer3:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
                         END
                       END
!------------ End File: MANUFACT, version 3 ------------------

!------------ File: MANUFACT, version 4 ----------------------
!------------ modified 10.05.2004 at 13:24:52 -----------------
MOD:stFileName:MANUFACTVer4  STRING(260)
MANUFACTVer4           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer4),PRE(MANUFACTVer4),CREATE
RecordNumberKey          KEY(MANUFACTVer4:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer4:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
                         END
                       END
!------------ End File: MANUFACT, version 4 ------------------

!------------ File: MANUFACT, version 5 ----------------------
!------------ modified 20.05.2004 at 14:36:14 -----------------
MOD:stFileName:MANUFACTVer5  STRING(260)
MANUFACTVer5           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer5),PRE(MANUFACTVer5),CREATE
RecordNumberKey          KEY(MANUFACTVer5:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer5:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
                         END
                       END
!------------ End File: MANUFACT, version 5 ------------------

!------------ File: MANUFACT, version 6 ----------------------
!------------ modified 01.06.2005 at 14:56:27 -----------------
MOD:stFileName:MANUFACTVer6  STRING(260)
MANUFACTVer6           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer6),PRE(MANUFACTVer6),CREATE
RecordNumberKey          KEY(MANUFACTVer6:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer6:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
IsCCentreSpecific           BYTE
                         END
                       END
!------------ End File: MANUFACT, version 6 ------------------

!------------ File: MANUFACT, version 7 ----------------------
!------------ modified 28.09.2005 at 16:02:39 -----------------
MOD:stFileName:MANUFACTVer7  STRING(260)
MANUFACTVer7           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer7),PRE(MANUFACTVer7),CREATE
RecordNumberKey          KEY(MANUFACTVer7:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer7:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
IsCCentreSpecific           BYTE
EDIFileType                 STRING(30)
                         END
                       END
!------------ End File: MANUFACT, version 7 ------------------

!------------ File: MANUFACT, version 8 ----------------------
!------------ modified 17.03.2008 at 09:59:44 -----------------
MOD:stFileName:MANUFACTVer8  STRING(260)
MANUFACTVer8           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer8),PRE(MANUFACTVer8),CREATE
RecordNumberKey          KEY(MANUFACTVer8:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer8:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
IsCCentreSpecific           BYTE
EDIFileType                 STRING(30)
VodafoneISPAccount          BYTE
                         END
                       END
!------------ End File: MANUFACT, version 8 ------------------

!------------ File: MANUFACT, version 9 ----------------------
!------------ modified 03.07.2008 at 14:25:19 -----------------
MOD:stFileName:MANUFACTVer9  STRING(260)
MANUFACTVer9           FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer9),PRE(MANUFACTVer9),CREATE
RecordNumberKey          KEY(MANUFACTVer9:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer9:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
IsCCentreSpecific           BYTE
EDIFileType                 STRING(30)
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
                         END
                       END
!------------ End File: MANUFACT, version 9 ------------------

!------------ File: MANUFACT, version 10 ----------------------
!------------ modified 03.07.2008 at 14:38:54 -----------------
MOD:stFileName:MANUFACTVer10  STRING(260)
MANUFACTVer10          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer10),PRE(MANUFACTVer10),CREATE
RecordNumberKey          KEY(MANUFACTVer10:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer10:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
IsCCentreSpecific           BYTE
EDIFileType                 STRING(30)
VodafoneISPAccount          BYTE
                         END
                       END
!------------ End File: MANUFACT, version 10 ------------------

!------------ File: MANUFACT, version 11 ----------------------
!------------ modified 04.11.2008 at 11:05:40 -----------------
MOD:stFileName:MANUFACTVer11  STRING(260)
MANUFACTVer11          FILE,DRIVER('Btrieve'),NAME(MOD:stFileName:MANUFACTVer11),PRE(MANUFACTVer11),CREATE
RecordNumberKey          KEY(MANUFACTVer11:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer11:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
IsCCentreSpecific           BYTE
EDIFileType                 STRING(30)
VodafoneISPAccount          BYTE
KnownIssueDisplay           BYTE
                         END
                       END
!------------ End File: MANUFACT, version 11 ------------------

!------------ File: XMLDEFS, version 1 ----------------------
!------------ modified 03.08.2004 at 10:44:50 -----------------
MOD:stFileName:XMLDEFSVer1  STRING(260)
XMLDEFSVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLDEFSVer1),PRE(XMLDEFSVer1)
RecordNumberKey          KEY(XMLDEFSVer1:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(15)
ChargeType                  STRING(30)
TransitType                 STRING(30)
JobTurnaroundTime           STRING(30)
UnitType                    STRING(30)
CollectionStatus            STRING(30)
DropOffStatus               STRING(30)
DeliveryText                STRING(255)
                         END
                       END
!------------ End File: XMLDEFS, version 1 ------------------

!------------ File: XMLDEFS, version 2 ----------------------
!------------ modified 16.08.2004 at 12:56:11 -----------------
MOD:stFileName:XMLDEFSVer2  STRING(260)
XMLDEFSVer2            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLDEFSVer2),PRE(XMLDEFSVer2),CREATE
RecordNumberKey          KEY(XMLDEFSVer2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserCode                    STRING(3)
AccountNumber               STRING(15)
ChargeType                  STRING(30)
TransitType                 STRING(30)
JobTurnaroundTime           STRING(30)
UnitType                    STRING(30)
CollectionStatus            STRING(30)
DropOffStatus               STRING(30)
DeliveryText                STRING(255)
                         END
                       END
!------------ End File: XMLDEFS, version 2 ------------------

!------------ File: XMLDEFS, version 3 ----------------------
!------------ modified 26.08.2004 at 15:47:19 -----------------
MOD:stFileName:XMLDEFSVer3  STRING(260)
XMLDEFSVer3            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLDEFSVer3),PRE(XMLDEFSVer3),CREATE
RecordNumberKey          KEY(XMLDEFSVer3:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserCode                    STRING(3)
AccountNumber               STRING(15)
ChargeType                  STRING(30)
WarrantyChargeType          STRING(30)
TransitType                 STRING(30)
JobTurnaroundTime           STRING(30)
UnitType                    STRING(30)
CollectionStatus            STRING(30)
DropOffStatus               STRING(30)
DeliveryText                STRING(255)
                         END
                       END
!------------ End File: XMLDEFS, version 3 ------------------

!------------ File: XMLSB, version 1 ----------------------
!------------ modified 03.08.2004 at 10:44:50 -----------------
MOD:stFileName:XMLSBVer1  STRING(260)
XMLSBVer1              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLSBVer1),PRE(XMLSBVer1)
SBStatusKey              KEY(XMLSBVer1:SBStatus),NOCASE,PRIMARY
FKStatusCodeKey          KEY(XMLSBVer1:FKStatusCode),DUP,NOCASE
Record                   RECORD,PRE()
SBStatus                    STRING(30)
FKStatusCode                STRING(2)
                         END
                       END
!------------ End File: XMLSB, version 1 ------------------

!------------ File: MPXDEFS, version 1 ----------------------
!------------ modified 06.12.2005 at 10:29:23 -----------------
MOD:stFileName:MPXDEFSVer1  STRING(260)
MPXDEFSVer1            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXDEFSVer1),PRE(MPXDEFSVer1)
AccountNoKey             KEY(MPXDEFSVer1:AccountNo),NOCASE,OPT,PRIMARY
Record                   RECORD,PRE()
AccountNo                   STRING(15)
ChargeType                  STRING(30)
WarrantyType                STRING(30)
TransitType                 STRING(30)
UnitType                    STRING(30)
CollectionStatus            STRING(30)
DropOffStatus               STRING(30)
DeliveryText                STRING(255)
DropPointStatus             STRING(30)
DropPointLocation           STRING(30)
                         END
                       END
!------------ End File: MPXDEFS, version 1 ------------------

!------------ File: MPXDEFS, version 2 ----------------------
!------------ modified 06.12.2005 at 16:46:38 -----------------
MOD:stFileName:MPXDEFSVer2  STRING(260)
MPXDEFSVer2            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXDEFSVer2),PRE(MPXDEFSVer2),CREATE
AccountNoKey             KEY(MPXDEFSVer2:AccountNo),NOCASE,OPT,PRIMARY
Record                   RECORD,PRE()
AccountNo                   STRING(15)
UserCode                    STRING(3)
ChargeType                  STRING(30)
WarrantyType                STRING(30)
TransitType                 STRING(30)
UnitType                    STRING(30)
CollectionStatus            STRING(30)
DropOffStatus               STRING(30)
DeliveryText                STRING(255)
DropPointStatus             STRING(30)
DropPointLocation           STRING(30)
                         END
                       END
!------------ End File: MPXDEFS, version 2 ------------------

!------------ File: MPXDEFS, version 3 ----------------------
!------------ modified 19.12.2005 at 09:46:42 -----------------
MOD:stFileName:MPXDEFSVer3  STRING(260)
MPXDEFSVer3            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXDEFSVer3),PRE(MPXDEFSVer3),CREATE
AccountNoKey             KEY(MPXDEFSVer3:AccountNo),NOCASE,OPT,PRIMARY
Record                   RECORD,PRE()
AccountNo                   STRING(15)
UserCode                    STRING(3)
IsChargeable                BYTE
ChargeType                  STRING(30)
WarrantyType                STRING(30)
IsWarranty                  BYTE
TransitType                 STRING(30)
UnitType                    STRING(30)
CollectionStatus            STRING(30)
DropOffStatus               STRING(30)
DeliveryText                STRING(255)
Status                      STRING(30)
Location                    STRING(30)
DropPointStatus             STRING(30)
DropPointLocation           STRING(30)
                         END
                       END
!------------ End File: MPXDEFS, version 3 ------------------

!------------ File: MPXDEFS, version 4 ----------------------
!------------ modified 19.12.2005 at 10:05:37 -----------------
MOD:stFileName:MPXDEFSVer4  STRING(260)
MPXDEFSVer4            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXDEFSVer4),PRE(MPXDEFSVer4),CREATE
AccountNoKey             KEY(MPXDEFSVer4:AccountNo),NOCASE,OPT,PRIMARY
Record                   RECORD,PRE()
AccountNo                   STRING(15)
UserCode                    STRING(3)
IsChargeable                BYTE
ChargeType                  STRING(30)
IsWarranty                  BYTE
WarrantyType                STRING(30)
TransitType                 STRING(30)
UnitType                    STRING(30)
DeliveryText                STRING(255)
Status                      STRING(30)
Location                    STRING(30)
                         END
                       END
!------------ End File: MPXDEFS, version 4 ------------------

!------------ File: MPXDEFS, version 5 ----------------------
!------------ modified 19.12.2005 at 10:56:18 -----------------
MOD:stFileName:MPXDEFSVer5  STRING(260)
MPXDEFSVer5            FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:MPXDEFSVer5),PRE(MPXDEFSVer5),CREATE
AccountNoKey             KEY(MPXDEFSVer5:AccountNo),NOCASE,OPT,PRIMARY
Record                   RECORD,PRE()
AccountNo                   STRING(15)
UserCode                    STRING(3)
IsChargeable                BYTE
ChargeType                  STRING(30)
IsWarranty                  BYTE
WarrantyType                STRING(30)
TransitType                 STRING(30)
UnitType                    STRING(30)
DeliveryText                STRING(255)
Status                      STRING(30)
Location                    STRING(30)
EnvCountStatus              STRING(30)
                         END
                       END
!------------ End File: MPXDEFS, version 5 ------------------

!------------ File: XMLSAM, version 1 ----------------------
!------------ modified 03.08.2004 at 10:44:50 -----------------
MOD:stFileName:XMLSAMVer1  STRING(260)
XMLSAMVer1             FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:XMLSAMVer1),PRE(XMLSAMVer1)
StatusCodeKey            KEY(XMLSAMVer1:StatusCode),NOCASE,PRIMARY
Record                   RECORD,PRE()
StatusCode                  STRING(2)
Description                 STRING(30)
                         END
                       END
!------------ End File: XMLSAM, version 1 ------------------

!------------ Declaration Current Files Structure --------------------

!------------ modified 03.07.2008 at 15:21:06 -----------------
MOD:stFileName:CARISMAVer3  STRING(260)
CARISMAVer3            FILE,DRIVER('Btrieve'),NAME('CARISMA.DAT'),PRE(CARISMAVer3),CREATE
RecordNumberKey          KEY(CARISMAVer3:RecordNumber),NOCASE,PRIMARY
ManufactModColourKey     KEY(CARISMAVer3:Manufacturer,CARISMAVer3:ModelNo,CARISMAVer3:Colour),DUP,NOCASE
ContractCodeKey          KEY(CARISMAVer3:ContractOracleCode),DUP,NOCASE
PAYTCodeKey              KEY(CARISMAVer3:PAYTOracleCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
Colour                      STRING(30)
ContractOracleCode          STRING(30)
PAYTOracleCode              STRING(30)
ContractActive              BYTE
PAYTActive                  BYTE
                         END
                       END

!------------ modified 11.10.2006 at 13:58:44 -----------------
MOD:stFileName:DEFEMAILVer4  STRING(260)
DEFEMAILVer4           FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAIL.DAT'),PRE(DEFEMAILVer4),CREATE
RecordNumberKey          KEY(DEFEMAILVer4:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
SMSToAddress                STRING(255)
EmailToAddress              STRING(255)
AdminToAddress              STRING(255)
DespFromSCText              STRING(480)
ArrivedAtRetailText         STRING(480)
SMSFTPAddress               STRING(30)
SMSFTPUsername              STRING(30)
SMSFTPPassword              STRING(30)
SMSFTPLocation              STRING(255)
R1Active                    BYTE
R2Active                    BYTE
R3Active                    BYTE
R1SMSActive                 BYTE
R2SMSActive                 BYTE
R3SMSActive                 BYTE
BookedRetailText            STRING(480)
R4Active                    BYTE
R4SMSActive                 BYTE
DespFromStoreText           STRING(480)
                         END
                       END

!------------ modified 05.09.2006 at 10:43:48 -----------------
MOD:stFileName:REGIONSVer3  STRING(260)
REGIONSVer3            FILE,DRIVER('Btrieve'),NAME('REGIONS.DAT'),PRE(REGIONSVer3),CREATE
RegionIDKey              KEY(REGIONSVer3:RegionID),NOCASE,PRIMARY
RegionNameKey            KEY(REGIONSVer3:RegionName),NOCASE
Record                   RECORD,PRE()
RegionID                    LONG
RegionName                  STRING(30)
BankHolidaysFileName        STRING(100)
                         END
                       END

!------------ modified 05.09.2006 at 10:43:48 -----------------
MOD:stFileName:ACCREGVer3  STRING(260)
ACCREGVer3             FILE,DRIVER('Btrieve'),NAME('ACCREG.DAT'),PRE(ACCREGVer3),CREATE
AccRegIDKey              KEY(ACCREGVer3:AccRegID),NOCASE,PRIMARY
AccountNoKey             KEY(ACCREGVer3:AccountNo),NOCASE
RegionNameKey            KEY(ACCREGVer3:RegionName,ACCREGVer3:AccountNo),DUP,NOCASE
Record                   RECORD,PRE()
AccRegID                    LONG
AccountNo                   STRING(15)
RegionName                  STRING(30)
                         END
                       END

!------------ modified 05.09.2006 at 16:03:27 -----------------
MOD:stFileName:SIDACREGVer2  STRING(260)
SIDACREGVer2           FILE,DRIVER('Btrieve'),NAME('SIDACREG.DAT'),PRE(SIDACREGVer2),CREATE
RecordNumberKey          KEY(SIDACREGVer2:RecordNumber),NOCASE,PRIMARY
SCAccRegIDKey            KEY(SIDACREGVer2:SCAccountID,SIDACREGVer2:AccRegID),DUP,NOCASE
AccRegIDKey              KEY(SIDACREGVer2:AccRegID),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SCAccountID                 LONG
AccRegID                    LONG
TransitDaysException        BYTE
TransitDaysToSC             LONG
TransitDaysToStore          LONG
                         END
                       END

!------------ modified 19.09.2005 at 14:08:07 -----------------
MOD:stFileName:XMLJOBSVer17  STRING(260)
XMLJOBSVer17           FILE,DRIVER('Btrieve'),OEM,NAME('XMLJOBS.DAT'),PRE(XMLJOBSVer17),CREATE
RecordNumberKey          KEY(XMLJOBSVer17:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XMLJOBSVer17:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XMLJOBSVer17:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XMLJOBSVer17:RECORD_STATE,XMLJOBSVer17:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XMLJOBSVer17:RECORD_STATE,XMLJOBSVer17:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XMLJOBSVer17:RECORD_STATE,XMLJOBSVer17:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XMLJOBSVer17:RECORD_STATE,XMLJOBSVer17:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XMLJOBSVer17:RECORD_STATE,XMLJOBSVer17:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
EXCEPTION_DESC              STRING(255)
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
TR_REASON                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
REPAIR_COMPLETE_DATE        DATE
REPAIR_COMPLETE_TIME        TIME
GOODS_DELIVERY_DATE         DATE
GOODS_DELIVERY_TIME         TIME
SYMPTOM1_DESC               STRING(40)
SYMPTOM2_DESC               STRING(40)
SYMPTOM3_DESC               STRING(40)
BP_NO                       STRING(10)
INQUIRY_TEXT                STRING(254)
                         END
                       END

!------------ modified 06.03.2002 at 09:47:33 -----------------
MOD:stFileName:MULDESPVer2  STRING(260)
MULDESPVer2            FILE,DRIVER('Btrieve'),OEM,NAME('MULDESP.DAT'),PRE(MULDESPVer2),CREATE
RecordNumberKey          KEY(MULDESPVer2:RecordNumber),NOCASE,PRIMARY
BatchNumberKey           KEY(MULDESPVer2:BatchNumber),DUP,NOCASE
AccountNumberKey         KEY(MULDESPVer2:AccountNumber),DUP,NOCASE
CourierKey               KEY(MULDESPVer2:Courier),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
BatchNumber                 STRING(30)
AccountNumber               STRING(30)
Courier                     STRING(30)
BatchTotal                  LONG
                         END
                       END

!------------ modified 12.02.2009 at 15:34:33 -----------------
MOD:stFileName:IMEISHIPVer4  STRING(260)
IMEISHIPVer4           FILE,DRIVER('Btrieve'),OEM,NAME('IMEISHIP.DAT'),PRE(IMEISHIPVer4),CREATE
RecordNumberKey          KEY(IMEISHIPVer4:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEISHIPVer4:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEISHIPVer4:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
BER                         BYTE
ManualEntry                 BYTE
ProductCode                 STRING(30)
                         END
                       END

!------------ modified 12.03.2002 at 11:37:11 -----------------
MOD:stFileName:JOBTHIRDVer2  STRING(260)
JOBTHIRDVer2           FILE,DRIVER('Btrieve'),OEM,NAME('JOBTHIRD.DAT'),PRE(JOBTHIRDVer2),CREATE
RecordNumberKey          KEY(JOBTHIRDVer2:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBTHIRDVer2:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(JOBTHIRDVer2:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(JOBTHIRDVer2:InIMEI),DUP,NOCASE
OutDateKey               KEY(JOBTHIRDVer2:RefNumber,JOBTHIRDVer2:DateOut,JOBTHIRDVer2:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(JOBTHIRDVer2:ThirdPartyNumber),DUP,NOCASE
OriginalIMEIKey          KEY(JOBTHIRDVer2:OriginalIMEI),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                       END

!------------ modified 09.08.2002 at 11:39:35 -----------------
MOD:stFileName:LOGRETRNVer2  STRING(260)
LOGRETRNVer2           FILE,DRIVER('Btrieve'),OEM,NAME('LOGRETRN.DAT'),PRE(LOGRETRNVer2),CREATE
RefNumberKey             KEY(LOGRETRNVer2:RefNumber),NOCASE,PRIMARY
ClubNokiaKey             KEY(LOGRETRNVer2:ClubNokiaSite,LOGRETRNVer2:Date),DUP,NOCASE
DateKey                  KEY(LOGRETRNVer2:Date),DUP,NOCASE
ReturnsNoKey             KEY(LOGRETRNVer2:ReturnsNumber,LOGRETRNVer2:Date),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
ClubNokiaSite               STRING(30)
ModelNumber                 STRING(30)
Quantity                    LONG
ReturnsNumber               STRING(30)
Date                        DATE
ReturnType                  STRING(3)
DummyField                  STRING(1)
To_Site                     STRING(30)
SalesCode                   STRING(30)
                         END
                       END

!------------ modified 23.05.2013 at 16:57:43 -----------------
MOD:stFileName:PRODCODEVer2  STRING(260)
PRODCODEVer2           FILE,DRIVER('Btrieve'),OEM,NAME('PRODCODE.DAT'),PRE(PRODCODEVer2),CREATE
RecordNumberKey          KEY(PRODCODEVer2:RecordNumber),NOCASE,PRIMARY
ProductCodeKey           KEY(PRODCODEVer2:ProductCode),NOCASE
ModelProductKey          KEY(PRODCODEVer2:ModelNumber,PRODCODEVer2:ProductCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ModelNumber                 STRING(30)
ProductCode                 STRING(30)
WarrantyPeriod              LONG
                         END
                       END

!------------ modified 09.08.2002 at 11:39:35 -----------------
MOD:stFileName:LOGSTOCKVer2  STRING(260)
LOGSTOCKVer2           FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTOCK.DAT'),PRE(LOGSTOCKVer2),CREATE
RefNumberKey             KEY(LOGSTOCKVer2:RefNumber),NOCASE,PRIMARY
SalesKey                 KEY(LOGSTOCKVer2:SalesCode),DUP,NOCASE
DescriptionKey           KEY(LOGSTOCKVer2:Description),DUP,NOCASE
SalesModelNoKey          KEY(LOGSTOCKVer2:SalesCode,LOGSTOCKVer2:ModelNumber),NOCASE
RefModelNoKey            KEY(LOGSTOCKVer2:RefNumber,LOGSTOCKVer2:ModelNumber),DUP,NOCASE
ModelNumberKey           KEY(LOGSTOCKVer2:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
SalesCode                   STRING(30)
Description                 STRING(30)
ModelNumber                 STRING(30)
DummyField                  STRING(4)
                         END
                       END

!------------ modified 09.03.2006 at 14:22:19 -----------------
MOD:stFileName:DEFEMAB2Ver2  STRING(260)
DEFEMAB2Ver2           FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAB2.DAT'),PRE(DEFEMAB2Ver2),CREATE
RecordNumberKey          KEY(DEFEMAB2Ver2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
B2Email                     STRING(4000)
Subject                     STRING(60)
                         END
                       END

!------------ modified 09.03.2006 at 14:22:19 -----------------
MOD:stFileName:DEFEMAB3Ver2  STRING(260)
DEFEMAB3Ver2           FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAB3.DAT'),PRE(DEFEMAB3Ver2),CREATE
RecordNumberKey          KEY(DEFEMAB3Ver2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
B3Email                     STRING(4000)
Subject                     STRING(60)
                         END
                       END

!------------ modified 09.03.2006 at 14:22:19 -----------------
MOD:stFileName:DEFEMAB4Ver2  STRING(260)
DEFEMAB4Ver2           FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAB4.DAT'),PRE(DEFEMAB4Ver2),CREATE
RecordNumberKey          KEY(DEFEMAB4Ver2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
B4Email                     STRING(4000)
Subject                     STRING(60)
                         END
                       END

!------------ modified 09.03.2006 at 14:22:19 -----------------
MOD:stFileName:DEFEMAB5Ver2  STRING(260)
DEFEMAB5Ver2           FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAB5.DAT'),PRE(DEFEMAB5Ver2),CREATE
RecordNumberKey          KEY(DEFEMAB5Ver2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
B5Email                     STRING(4000)
Subject                     STRING(60)
                         END
                       END

!------------ modified 09.03.2006 at 14:22:19 -----------------
MOD:stFileName:DEFEMAE2Ver2  STRING(260)
DEFEMAE2Ver2           FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAE2.DAT'),PRE(DEFEMAE2Ver2),CREATE
RecordNumberKey          KEY(DEFEMAE2Ver2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E2Email                     STRING(4000)
Subject                     STRING(60)
                         END
                       END

!------------ modified 09.03.2006 at 14:22:19 -----------------
MOD:stFileName:DEFEMAE3Ver2  STRING(260)
DEFEMAE3Ver2           FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAE3.DAT'),PRE(DEFEMAE3Ver2),CREATE
RecordNumberKey          KEY(DEFEMAE3Ver2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E3Email                     STRING(4000)
Subject                     STRING(60)
                         END
                       END

!------------ modified 09.03.2006 at 14:22:19 -----------------
MOD:stFileName:DEFEMAB1Ver2  STRING(260)
DEFEMAB1Ver2           FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAB1.DAT'),PRE(DEFEMAB1Ver2),CREATE
RecordNumberKey          KEY(DEFEMAB1Ver2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
B1Email                     STRING(4000)
Subject                     STRING(60)
                         END
                       END

!------------ modified 09.03.2006 at 14:02:52 -----------------
MOD:stFileName:DEFEMAE1Ver6  STRING(260)
DEFEMAE1Ver6           FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAE1.DAT'),PRE(DEFEMAE1Ver6),CREATE
RecordNumberKey          KEY(DEFEMAE1Ver6:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1Email                     STRING(4000)
Subject                     STRING(60)
                         END
                       END

!------------ modified 28.07.2006 at 16:01:30 -----------------
MOD:stFileName:DEFEMAI2Ver9  STRING(260)
DEFEMAI2Ver9           FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAI2.DAT'),PRE(DEFEMAI2Ver9),CREATE
RecordNumberKey          KEY(DEFEMAI2Ver9:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
E1SMS                       STRING(480)
E2SMS                       STRING(480)
E3SMS                       STRING(480)
B1SMS                       STRING(480)
B2SMS                       STRING(480)
B3SMS                       STRING(480)
B4SMS                       STRING(480)
B5SMS                       STRING(480)
B6SMS                       STRING(480)
B7SMS                       STRING(480)
E1Active                    BYTE
E2Active                    BYTE
E3Active                    BYTE
B1Active                    BYTE
B2Active                    BYTE
B3Active                    BYTE
B4Active                    BYTE
B5Active                    BYTE
B6Active                    BYTE
B7Active                    BYTE
E1SMSActive                 BYTE
E2SMSActive                 BYTE
E3SMSActive                 BYTE
B1SMSActive                 BYTE
B2SMSActive                 BYTE
B3SMSActive                 BYTE
B4SMSActive                 BYTE
B5SMSActive                 BYTE
B6SMSActive                 BYTE
B7SMSActive                 BYTE
B6NotifyDays                LONG
B7NotifyDays                LONG
E2BSMS                      STRING(480)
E3BSMS                      STRING(480)
E2BActive                   BYTE
E3BActive                   BYTE
E2BSMSActive                BYTE
E3BSMSActive                BYTE
                         END
                       END

!------------ modified 28.03.2006 at 15:03:30 -----------------
MOD:stFileName:DEFSIDEXVer3  STRING(260)
DEFSIDEXVer3           FILE,DRIVER('Btrieve'),OEM,NAME('DEFSIDEX.DAT'),PRE(DEFSIDEXVer3),CREATE
RecordNumberKey          KEY(DEFSIDEXVer3:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(50)
SMTPPortNumber              STRING(6)
SMTPUserName                STRING(60)
SMTPPassword                STRING(60)
FromEmailAddress            STRING(255)
EmailSubject                STRING(100)
EmailRecipientList          STRING(255)
FTPServer                   STRING(60)
FTPUsername                 STRING(60)
FTPPassword                 STRING(60)
FTPExportPath               STRING(255)
ExportDays                  LONG
                         END
                       END

!------------ modified 07.03.2002 at 11:12:55 -----------------
MOD:stFileName:RETSALESVer3  STRING(260)
RETSALESVer3           FILE,DRIVER('Btrieve'),OEM,NAME('RETSALES.DAT'),PRE(RETSALESVer3),CREATE
Ref_Number_Key           KEY(RETSALESVer3:Ref_Number),NOCASE,PRIMARY
Invoice_Number_Key       KEY(RETSALESVer3:Invoice_Number),DUP,NOCASE
Despatched_Purchase_Key  KEY(RETSALESVer3:Despatched,RETSALESVer3:Purchase_Order_Number),DUP,NOCASE
Despatched_Ref_Key       KEY(RETSALESVer3:Despatched,RETSALESVer3:Ref_Number),DUP,NOCASE
Despatched_Account_Key   KEY(RETSALESVer3:Despatched,RETSALESVer3:Account_Number,RETSALESVer3:Purchase_Order_Number),DUP,NOCASE
Account_Number_Key       KEY(RETSALESVer3:Account_Number,RETSALESVer3:Ref_Number),DUP,NOCASE
Purchase_Number_Key      KEY(RETSALESVer3:Purchase_Order_Number),DUP,NOCASE
Despatch_Number_Key      KEY(RETSALESVer3:Despatch_Number),DUP,NOCASE
Date_Booked_Key          KEY(RETSALESVer3:date_booked),DUP,NOCASE
AccountInvoiceKey        KEY(RETSALESVer3:Account_Number,RETSALESVer3:Invoice_Number),DUP,NOCASE
DespatchedPurchDateKey   KEY(RETSALESVer3:Despatched,RETSALESVer3:Purchase_Order_Number,RETSALESVer3:date_booked),DUP,NOCASE
DespatchNoRefNoKey       KEY(RETSALESVer3:Despatch_Number,RETSALESVer3:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Account_Number              STRING(15)
Contact_Name                STRING(30)
Purchase_Order_Number       STRING(30)
Delivery_Collection         STRING(3)
Payment_Method              STRING(3)
Courier_Cost                REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier                     STRING(30)
Consignment_Number          STRING(30)
Date_Despatched             DATE
Despatched                  STRING(3)
Despatch_Number             LONG
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Courier_Cost        REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
WebOrderNumber              LONG
WebDateCreated              DATE
WebTimeCreated              STRING(20)
WebCreatedUser              STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Building_Name               STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Postcode_Delivery           STRING(10)
Company_Name_Delivery       STRING(30)
Building_Name_Delivery      STRING(30)
Address_Line1_Delivery      STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Fax_Number_Delivery         STRING(15)
Delivery_Text               STRING(255)
Invoice_Text                STRING(255)
                         END
                       END

!------------ modified 07.10.2004 at 09:49:38 -----------------
MOD:stFileName:TEAMSVer2  STRING(260)
TEAMSVer2              FILE,DRIVER('Btrieve'),OEM,NAME('TEAMS.DAT'),PRE(TEAMSVer2),CREATE
Record_Number_Key        KEY(TEAMSVer2:Record_Number),NOCASE,PRIMARY
Team_Key                 KEY(TEAMSVer2:Team),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Team                        STRING(30)
Completed_Jobs              LONG
                         END
                       END

!------------ modified 21.09.2007 at 11:14:57 -----------------
MOD:stFileName:MERGEVer2  STRING(260)
MERGEVer2              FILE,DRIVER('Btrieve'),OEM,NAME('MERGE.DAT'),PRE(MERGEVer2),BINDABLE,CREATE
RefNumberKey             KEY(MERGEVer2:RefNumber),NOCASE,PRIMARY
FieldNameKey             KEY(MERGEVer2:FieldName),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
FieldName                   STRING(30)
FileName                    STRING(60)
Type                        STRING(3)
Description                 STRING(255)
Capitals                    BYTE
TotalFieldLength            LONG
                         END
                       END

!------------ modified 24.07.2007 at 14:56:17 -----------------
MOD:stFileName:SIDDEFVer6  STRING(260)
SIDDEFVer6             FILE,DRIVER('Btrieve'),PRE(SIDDEFVer6),NAME('SIDDEF'),CREATE
RecordNumberKey          KEY(SIDDEFVer6:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
EmailSubject                STRING(60)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
FTPFailedCount              LONG
UnallocTransitToSC          LONG
UnallocTurnaround           LONG
UnallocTransitStore         LONG
WebJobConn                  STRING(50)
CPMConn                     STRING(50)
ToteAlertEmail              STRING(255)
                         END
                       END

!------------ modified 21.10.2004 at 14:02:41 -----------------
MOD:stFileName:ORDPENDVer4  STRING(260)
ORDPENDVer4            FILE,DRIVER('Btrieve'),NAME('ORDPEND.DAT'),PRE(ORDPENDVer4),CREATE
Ref_Number_Key           KEY(ORDPENDVer4:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ORDPENDVer4:Supplier,ORDPENDVer4:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ORDPENDVer4:Supplier,ORDPENDVer4:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ORDPENDVer4:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ORDPENDVer4:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ORDPENDVer4:Awaiting_Stock,ORDPENDVer4:Supplier,ORDPENDVer4:Part_Number),DUP,NOCASE
PartRecordNumberKey      KEY(ORDPENDVer4:PartRecordNumber),DUP,NOCASE
Job_Number_Key           KEY(ORDPENDVer4:Job_Number),DUP,NOCASE
Supplier_Job_Key         KEY(ORDPENDVer4:Supplier,ORDPENDVer4:Job_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
PartRecordNumber            LONG
                         END
                       END

!------------ modified 20.02.2002 at 10:49:38 -----------------
MOD:stFileName:STOHISTVer2  STRING(260)
STOHISTVer2            FILE,DRIVER('Btrieve'),NAME('STOHIST.DAT'),PRE(STOHISTVer2),CREATE
Ref_Number_Key           KEY(STOHISTVer2:Ref_Number,STOHISTVer2:Date),DUP,NOCASE
record_number_key        KEY(STOHISTVer2:Record_Number),NOCASE,PRIMARY
Transaction_Type_Key     KEY(STOHISTVer2:Ref_Number,STOHISTVer2:Transaction_Type,STOHISTVer2:Date),DUP,NOCASE
DateKey                  KEY(STOHISTVer2:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
User                        STRING(3)
Transaction_Type            STRING(3)
Despatch_Note_Number        STRING(30)
Job_Number                  LONG
Sales_Number                LONG
Quantity                    REAL
Date                        DATE
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Notes                       STRING(255)
Information                 STRING(255)
                         END
                       END

!------------ modified 09.12.2002 at 11:07:47 -----------------
MOD:stFileName:EXREASONVer2  STRING(260)
EXREASONVer2           FILE,DRIVER('Btrieve'),OEM,NAME('EXREASON.DAT'),PRE(EXREASONVer2),CREATE
RecordNumberKey          KEY(EXREASONVer2:RecordNumber),NOCASE,PRIMARY
ReasonKey                KEY(EXREASONVer2:ReasonType,EXREASONVer2:Reason),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReasonType                  BYTE
Reason                      STRING(80)
                         END
                       END

!------------ modified 09.08.2002 at 11:39:36 -----------------
MOD:stFileName:REPTYDEFVer2  STRING(260)
REPTYDEFVer2           FILE,DRIVER('Btrieve'),NAME('REPTYDEF.DAT'),PRE(REPTYDEFVer2),CREATE
Repair_Type_Key          KEY(REPTYDEFVer2:Repair_Type),NOCASE,PRIMARY
Chargeable_Key           KEY(REPTYDEFVer2:Chargeable,REPTYDEFVer2:Repair_Type),DUP,NOCASE
Warranty_Key             KEY(REPTYDEFVer2:Warranty,REPTYDEFVer2:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
WarrantyCode                STRING(5)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
BER                         BYTE
                         END
                       END

!------------ modified 06.12.2005 at 10:29:18 -----------------
MOD:stFileName:JOBSEVer10  STRING(260)
JOBSEVer10             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE.DAT'),PRE(JOBSEVer10),CREATE
RecordNumberKey          KEY(JOBSEVer10:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer10:RefNumber),DUP,NOCASE
InWorkshopDateKey        KEY(JOBSEVer10:InWorkshopDate),DUP,NOCASE
CompleteRepairKey        KEY(JOBSEVer10:CompleteRepairDate,JOBSEVer10:CompleteRepairTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
CustomerCollectionTime      TIME
EstimatedDespatchDate       DATE
EstimatedDespatchTime       TIME
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                       END

!------------ modified 04.03.2004 at 09:33:28 -----------------
MOD:stFileName:MANFAULTVer4  STRING(260)
MANFAULTVer4           FILE,DRIVER('Btrieve'),NAME('MANFAULT.DAT'),PRE(MANFAULTVer4),CREATE
Field_Number_Key         KEY(MANFAULTVer4:Manufacturer,MANFAULTVer4:Field_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
                         END
                       END

!------------ modified 24.02.2004 at 11:23:01 -----------------
MOD:stFileName:PICKNOTEVer5  STRING(260)
PICKNOTEVer5           FILE,DRIVER('Btrieve'),OEM,PRE(PICKNOTEVer5),NAME('PICKNOTE'),CREATE
keypicknote              KEY(PICKNOTEVer5:PickNoteRef),NOCASE,PRIMARY
keyonlocation            KEY(PICKNOTEVer5:Location),DUP,NOCASE
keyonjobno               KEY(PICKNOTEVer5:JobReference),DUP,NOCASE
Record                   RECORD,PRE()
PickNoteRef                 LONG
JobReference                LONG
PickNoteNumber              LONG
EngineerCode                STRING(3)
Location                    STRING(30)
IsPrinted                   BYTE
PrintedBy                   STRING(3)
PrintedDate                 DATE
PrintedTime                 TIME
IsConfirmed                 BYTE
ConfirmedBy                 STRING(3)
ConfirmedDate               DATE
ConfirmedTime               TIME
ConfirmedNotes              STRING(255)
Usercode                    STRING(3)
                         END
                       END

!------------ modified 17.12.2002 at 14:46:20 -----------------
MOD:stFileName:COURIERVer2  STRING(260)
COURIERVer2            FILE,DRIVER('Btrieve'),NAME('COURIER.DAT'),PRE(COURIERVer2),CREATE
Courier_Key              KEY(COURIERVer2:Courier),NOCASE,PRIMARY
Courier_Type_Key         KEY(COURIERVer2:Courier_Type,COURIERVer2:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Courier                     STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Export_Path                 STRING(255)
Include_Estimate            STRING(3)
Include_Chargeable          STRING(3)
Include_Warranty            STRING(3)
Service                     STRING(15)
Import_Path                 STRING(255)
Courier_Type                STRING(30)
Last_Despatch_Time          TIME
ICOLSuffix                  STRING(3)
ContractNumber              STRING(20)
ANCPath                     STRING(255)
ANCCount                    LONG
DespatchClose               STRING(3)
LabGOptions                 STRING(60)
CustomerCollection          BYTE
NewStatus                   STRING(30)
NoOfDespatchNotes           LONG
AutoConsignmentNo           BYTE
LastConsignmentNo           LONG
PrintLabel                  BYTE
                         END
                       END

!------------ modified 28.09.2007 at 10:00:19 -----------------
MOD:stFileName:ESNMODELVer2  STRING(260)
ESNMODELVer2           FILE,DRIVER('Btrieve'),NAME('ESNMODEL.DAT'),PRE(ESNMODELVer2),CREATE
Record_Number_Key        KEY(ESNMODELVer2:Record_Number),NOCASE,PRIMARY
ESN_Key                  KEY(ESNMODELVer2:ESN,ESNMODELVer2:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(ESNMODELVer2:ESN),DUP,NOCASE
Model_Number_Key         KEY(ESNMODELVer2:Model_Number,ESNMODELVer2:ESN),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
ESN                         STRING(8)
Model_Number                STRING(30)
                         END
                       END

!------------ modified 25.06.2003 at 15:38:11 -----------------
MOD:stFileName:STOCKVer4  STRING(260)
STOCKVer4              FILE,DRIVER('Btrieve'),NAME('STOCK.DAT'),PRE(STOCKVer4),CREATE
Ref_Number_Key           KEY(STOCKVer4:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(STOCKVer4:Sundry_Item),DUP,NOCASE
Description_Key          KEY(STOCKVer4:Location,STOCKVer4:Description),DUP,NOCASE
Description_Accessory_Key KEY(STOCKVer4:Location,STOCKVer4:Accessory,STOCKVer4:Description),DUP,NOCASE
Shelf_Location_Key       KEY(STOCKVer4:Location,STOCKVer4:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(STOCKVer4:Location,STOCKVer4:Accessory,STOCKVer4:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(STOCKVer4:Location,STOCKVer4:Accessory,STOCKVer4:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(STOCKVer4:Manufacturer,STOCKVer4:Part_Number),DUP,NOCASE
Supplier_Key             KEY(STOCKVer4:Location,STOCKVer4:Supplier),DUP,NOCASE
Location_Key             KEY(STOCKVer4:Location,STOCKVer4:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(STOCKVer4:Location,STOCKVer4:Accessory,STOCKVer4:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(STOCKVer4:Location,STOCKVer4:Ref_Number,STOCKVer4:Part_Number,STOCKVer4:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(STOCKVer4:Location,STOCKVer4:Manufacturer,STOCKVer4:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(STOCKVer4:Location,STOCKVer4:Accessory,STOCKVer4:Manufacturer,STOCKVer4:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(STOCKVer4:Location,STOCKVer4:Part_Number,STOCKVer4:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(STOCKVer4:Ref_Number,STOCKVer4:Part_Number,STOCKVer4:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(STOCKVer4:Minimum_Stock,STOCKVer4:Location,STOCKVer4:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(STOCKVer4:Minimum_Stock,STOCKVer4:Location,STOCKVer4:Description),DUP,NOCASE
SecondLocKey             KEY(STOCKVer4:Location,STOCKVer4:Shelf_Location,STOCKVer4:Second_Location,STOCKVer4:Part_Number),DUP,NOCASE
RequestedKey             KEY(STOCKVer4:QuantityRequested,STOCKVer4:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(STOCKVer4:ExchangeUnit,STOCKVer4:Location,STOCKVer4:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(STOCKVer4:ExchangeUnit,STOCKVer4:Location,STOCKVer4:Description),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
AllowDuplicate              BYTE
ExcludeFromEDI              BYTE
RF_Board                    BYTE
                         END
                       END

!------------ modified 09.08.2002 at 11:39:37 -----------------
MOD:stFileName:LOGSTOLCVer2  STRING(260)
LOGSTOLCVer2           FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTOLC.DAT'),PRE(LOGSTOLCVer2),CREATE
RefNumberKey             KEY(LOGSTOLCVer2:RefNumber),NOCASE,PRIMARY
LocationKey              KEY(LOGSTOLCVer2:Location),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Location                    STRING(30)
Mark_Returns_Available      BYTE
                         END
                       END

!------------ modified 09.08.2002 at 11:39:37 -----------------
MOD:stFileName:LOGSERSTVer2  STRING(260)
LOGSERSTVer2           FILE,DRIVER('Btrieve'),OEM,NAME('LOGSERST.DAT'),PRE(LOGSERSTVer2),CREATE
RecordNumberKey          KEY(LOGSERSTVer2:RecordNumber),NOCASE,PRIMARY
ESNKey                   KEY(LOGSERSTVer2:ESN),DUP,NOCASE
RefNumberKey             KEY(LOGSERSTVer2:RefNumber,LOGSERSTVer2:ESN),DUP,NOCASE
ESNStatusKey             KEY(LOGSERSTVer2:RefNumber,LOGSERSTVer2:Status,LOGSERSTVer2:ESN),DUP,NOCASE
NokiaStatusKey           KEY(LOGSERSTVer2:RefNumber,LOGSERSTVer2:Status,LOGSERSTVer2:ClubNokia,LOGSERSTVer2:ESN),DUP,NOCASE
AllocNo_Key              KEY(LOGSERSTVer2:AllocNo,LOGSERSTVer2:Status,LOGSERSTVer2:ESN),DUP,NOCASE
Status_Alloc_Key         KEY(LOGSERSTVer2:RefNumber,LOGSERSTVer2:ClubNokia,LOGSERSTVer2:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
ESN                         STRING(30)
Status                      STRING(10)
ClubNokia                   STRING(30)
AllocNo                     LONG
DummyField                  STRING(4)
                         END
                       END

!------------ modified 09.08.2002 at 11:39:58 -----------------
MOD:stFileName:LOGSTLOCVer2  STRING(260)
LOGSTLOCVer2           FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTLOC.DAT'),PRE(LOGSTLOCVer2),CREATE
RecordNumberKey          KEY(LOGSTLOCVer2:RecordNumber),NOCASE,PRIMARY
RefLocationKey           KEY(LOGSTLOCVer2:RefNumber,LOGSTLOCVer2:Location),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Location                    STRING(30)
AvlQuantity                 LONG
DesQuantity                 LONG
allocquantity               LONG
DummyField                  STRING(1)
                         END
                       END

!------------ modified 09.08.2002 at 11:39:58 -----------------
MOD:stFileName:TRAFAULOVer2  STRING(260)
TRAFAULOVer2           FILE,DRIVER('Btrieve'),NAME('TRAFAULO.DAT'),PRE(TRAFAULOVer2),CREATE
Field_Key                KEY(TRAFAULOVer2:AccountNumber,TRAFAULOVer2:Field_Number,TRAFAULOVer2:Field,TRAFAULOVer2:Description),NOCASE,PRIMARY
DescriptionKey           KEY(TRAFAULOVer2:AccountNumber,TRAFAULOVer2:Field_Number,TRAFAULOVer2:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                       END

!------------ modified 19.12.2005 at 14:35:55 -----------------
MOD:stFileName:MPXJOBSVer6  STRING(260)
MPXJOBSVer6            FILE,DRIVER('Btrieve'),OEM,NAME('MPXJOBS.DAT'),PRE(MPXJOBSVer6),CREATE
RecordNoKey              KEY(MPXJOBSVer6:RecordNo),NOCASE,PRIMARY
IDKey                    KEY(MPXJOBSVer6:ID),DUP,NOCASE
JobNoKey                 KEY(MPXJOBSVer6:InspACRJobNo),DUP,NOCASE
StateJobNoKey            KEY(MPXJOBSVer6:Record_State,MPXJOBSVer6:InspACRJobNo),DUP,NOCASE
StateSurnameKey          KEY(MPXJOBSVer6:Record_State,MPXJOBSVer6:CustLastName),DUP,NOCASE
StatePostcodeKey         KEY(MPXJOBSVer6:Record_State,MPXJOBSVer6:CustPostCode),DUP,NOCASE
StateModelKey            KEY(MPXJOBSVer6:Record_State,MPXJOBSVer6:ModelID),DUP,NOCASE
StateReportCodeKey       KEY(MPXJOBSVer6:Record_State,MPXJOBSVer6:ReportCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
Record_State                BYTE
ReportCode                  LONG
Exception_Code              LONG
Exception_Desc              STRING(255)
ImportDate                  DATE
ImportTime                  TIME
ID                          LONG
InspACRJobNo                LONG
EnvDateSent                 DATE
InspACRStatus               STRING(50)
InspDateRecd                DATE
InspModelID                 STRING(50)
InspFlag                    BYTE
InspAttention               STRING(255)
InspQty                     LONG
InspPackageCond             STRING(50)
InspQ1                      BYTE
InspQ2                      BYTE
InspQ3                      BYTE
InspACC1                    BYTE
InspACC2                    BYTE
InspACC3                    BYTE
InspACC4                    BYTE
InspACC5                    BYTE
InspGrade                   STRING(50)
InspIMEI                    STRING(15)
InspNotes                   STRING(255)
InspDate                    DATE
InspBrand                   STRING(50)
InspInWarranty              BYTE
DespatchConsignment         STRING(50)
DespatchDate                DATE
CustCTN                     STRING(50)
QuoteValue                  LONG
ModelID                     STRING(50)
QuoteQ1                     BYTE
QuoteQ2                     BYTE
QuoteQ3                     BYTE
QuoteACC1                   BYTE
QuoteACC2                   BYTE
QuoteACC3                   BYTE
QuoteACC4                   BYTE
QuoteACC5                   BYTE
CustTitle                   STRING(50)
CustFirstName               STRING(50)
CustLastName                STRING(50)
CustAdd1                    STRING(50)
CustAdd2                    STRING(50)
CustAdd3                    STRING(50)
CustAdd4                    STRING(50)
CustPostCode                STRING(50)
CustRef                     STRING(50)
CustNetwork                 STRING(50)
EnvLetter                   STRING(50)
InspType                    STRING(50)
EnvType                     STRING(50)
EnvCount                    LONG
                         END
                       END

!------------ modified 19.12.2005 at 14:35:55 -----------------
MOD:stFileName:MPXSTATVer3  STRING(260)
MPXSTATVer3            FILE,DRIVER('Btrieve'),OEM,NAME('MPXSTAT.DAT'),PRE(MPXSTATVer3),CREATE
RecordNoKey              KEY(MPXSTATVer3:RecordNo),NOCASE,PRIMARY
MpxStatusKey             KEY(MPXSTATVer3:MPX_STATUS),DUP,NOCASE
SBStatusKey              KEY(MPXSTATVer3:SB_STATUS),DUP,NOCASE
Record                   RECORD,PRE()
RecordNo                    LONG
MPX_STATUS                  LONG
SB_STATUS                   STRING(30)
                         END
                       END

!------------ modified 04.03.2004 at 09:33:28 -----------------
MOD:stFileName:TRAFAULTVer3  STRING(260)
TRAFAULTVer3           FILE,DRIVER('Btrieve'),NAME('TRAFAULT.DAT'),PRE(TRAFAULTVer3),CREATE
RecordNumberKey          KEY(TRAFAULTVer3:RecordNumber),NOCASE,PRIMARY
Field_Number_Key         KEY(TRAFAULTVer3:AccountNumber,TRAFAULTVer3:Field_Number),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
                         END
                       END

!------------ modified 07.03.2002 at 11:19:50 -----------------
MOD:stFileName:STATCRITVer3  STRING(260)
STATCRITVer3           FILE,DRIVER('Btrieve'),OEM,NAME('STATCRIT.DAT'),PRE(STATCRITVer3),CREATE
RecordNumberKey          KEY(STATCRITVer3:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(STATCRITVer3:Description),DUP,NOCASE
DescriptionOptionKey     KEY(STATCRITVer3:Description,STATCRITVer3:OptionType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(60)
OptionType                  STRING(30)
FieldValue                  STRING(40)
                         END
                       END

!------------ modified 02.02.2004 at 11:26:01 -----------------
MOD:stFileName:NOTESDELVer2  STRING(260)
NOTESDELVer2           FILE,DRIVER('Btrieve'),OEM,PRE(NOTESDELVer2),NAME('NOTESDEL'),CREATE
keyonnotesdel            KEY(NOTESDELVer2:ReasonCode),NOCASE,PRIMARY
Record                   RECORD,PRE()
ReasonCode                  STRING(30)
ReasonText                  STRING(80)
                         END
                       END

!------------ modified 15.03.2004 at 16:00:55 -----------------
MOD:stFileName:PICKDETVer6  STRING(260)
PICKDETVer6            FILE,DRIVER('Btrieve'),OEM,PRE(PICKDETVer6),NAME('PICKDET'),CREATE
keypickdet               KEY(PICKDETVer6:PickDetailRef),NOCASE,PRIMARY
keyonpicknote            KEY(PICKDETVer6:PickNoteRef),DUP,NOCASE
Record                   RECORD,PRE()
PickDetailRef               LONG
PickNoteRef                 LONG
PartRefNumber               LONG
Quantity                    LONG
IsChargeable                BYTE
RequestDate                 DATE
RequestTime                 TIME
EngineerCode                STRING(3)
IsInStock                   BYTE
IsPrinted                   BYTE
IsDelete                    BYTE
DeleteReason                STRING(255)
Usercode                    STRING(3)
StockPartRefNumber          LONG
PartNumber                  STRING(30)
                         END
                       END

!------------ modified 09.08.2002 at 11:39:58 -----------------
MOD:stFileName:USERSVer2  STRING(260)
USERSVer2              FILE,DRIVER('Btrieve'),NAME('USERS.DAT'),PRE(USERSVer2),CREATE
User_Code_Key            KEY(USERSVer2:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(USERSVer2:User_Type,USERSVer2:Active,USERSVer2:Surname),DUP,NOCASE
Surname_Active_Key       KEY(USERSVer2:Active,USERSVer2:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(USERSVer2:Active,USERSVer2:User_Code),DUP,NOCASE
User_Type_Key            KEY(USERSVer2:User_Type,USERSVer2:Surname),DUP,NOCASE
surname_key              KEY(USERSVer2:Surname),DUP,NOCASE
password_key             KEY(USERSVer2:Password),NOCASE
Logged_In_Key            KEY(USERSVer2:Logged_In,USERSVer2:Surname),DUP,NOCASE
Team_Surname             KEY(USERSVer2:Team,USERSVer2:Surname),DUP,NOCASE
Active_Team_Surname      KEY(USERSVer2:Team,USERSVer2:Active,USERSVer2:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
StockFromLocationOnly       BYTE
EmailAddress                STRING(255)
                         END
                       END

!------------ modified 05.09.2006 at 16:03:31 -----------------
MOD:stFileName:SIDREGVer2  STRING(260)
SIDREGVer2             FILE,DRIVER('Btrieve'),NAME('SIDREG.DAT'),PRE(SIDREGVer2),CREATE
SIDRegIDKey              KEY(SIDREGVer2:SIDRegID),NOCASE,PRIMARY
SCRegionNameKey          KEY(SIDREGVer2:SCAccountID,SIDREGVer2:RegionName),DUP,NOCASE
RegionNameKey            KEY(SIDREGVer2:RegionName),DUP,NOCASE
Record                   RECORD,PRE()
SIDRegID                    LONG
SCAccountID                 LONG
RegionName                  STRING(30)
TransitDaysToSC             LONG
TransitDaysToStore          LONG
                         END
                       END

!------------ modified 18.03.2010 at 16:09:26 -----------------
MOD:stFileName:PENDMAILVer14  STRING(260)
PENDMAILVer14          FILE,DRIVER('Btrieve'),OEM,NAME('PENDMAIL.DAT'),PRE(PENDMAILVer14),CREATE
RecordNumberKey          KEY(PENDMAILVer14:RecordNumber),NOCASE,PRIMARY
DateCreatedKey           KEY(-PENDMAILVer14:DateCreated,-PENDMAILVer14:TimeCreated),DUP,NOCASE
DateMessageKey           KEY(PENDMAILVer14:MessageType,-PENDMAILVer14:DateCreated,-PENDMAILVer14:TimeCreated),DUP,NOCASE
SMSSentKey               KEY(PENDMAILVer14:SMSEmail,PENDMAILVer14:DateSent),DUP,NOCASE
MessageRefKey            KEY(PENDMAILVer14:MessageType,PENDMAILVer14:RefNumber),DUP,NOCASE
JobNumberKey             KEY(PENDMAILVer14:SIDJobNumber),DUP,NOCASE
RefNumberKey             KEY(PENDMAILVer14:RefNumber),DUP,NOCASE
ReminderKey              KEY(PENDMAILVer14:Reminder),DUP,NOCASE
ReminderRefNumberKey     KEY(PENDMAILVer14:Reminder,PENDMAILVer14:RefNumber),DUP,NOCASE
ReminderSIDJobKey        KEY(PENDMAILVer14:Reminder,PENDMAILVer14:SIDJobNumber),DUP,NOCASE
ReminderDateCreatedKey   KEY(PENDMAILVer14:Reminder,PENDMAILVer14:DateCreated,PENDMAILVer14:TimeCreated),DUP,NOCASE
SMSSentDateCreatedKey    KEY(PENDMAILVer14:SMSEmail,PENDMAILVer14:DateSent,-PENDMAILVer14:DateCreated,-PENDMAILVer14:TimeCreated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
DateCreated                 DATE
TimeCreated                 TIME
MessageType                 STRING(3)
SMSEmail                    STRING(5)
RefNumber                   LONG
DateToSend                  DATE
TimeToSend                  TIME
DateSent                    DATE
TimeSent                    TIME
ErrorMessage                STRING(255)
MobileNumber                STRING(30)
EmailAddress                STRING(255)
BookingType                 STRING(30)
Status                      STRING(30)
SIDJobNumber                STRING(30)
Reminder                    BYTE
CustomerUTL                 STRING(8)
                         END
                       END

!------------ modified 27.10.2004 at 15:52:09 -----------------
MOD:stFileName:DEFAULTSVer7  STRING(260)
DEFAULTSVer7           FILE,DRIVER('Btrieve'),NAME('DEFAULTS.DAT'),PRE(DEFAULTSVer7),CREATE
RecordNumberKey          KEY(DEFAULTSVer7:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Name                   STRING(30)
record_number               REAL
Version_Number              STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
VAT_Number                  STRING(30)
Use_Invoice_Address         STRING(3)
Use_For_Order               STRING(3)
Invoice_Company_Name        STRING(30)
Invoice_Address_Line1       STRING(30)
Invoice_Address_Line2       STRING(30)
Invoice_Address_Line3       STRING(30)
Invoice_Postcode            STRING(15)
Invoice_Telephone_Number    STRING(15)
Invoice_Fax_Number          STRING(15)
InvoiceEmailAddress         STRING(255)
Invoice_VAT_Number          STRING(30)
OrderCompanyName            STRING(30)
OrderAddressLine1           STRING(30)
OrderAddressLine2           STRING(30)
OrderAddressLine3           STRING(30)
OrderPostcode               STRING(30)
OrderTelephoneNumber        STRING(30)
OrderFaxNumber              STRING(30)
OrderEmailAddress           STRING(255)
Use_Postcode                STRING(3)
Postcode_Path               STRING(255)
PostcodeDll                 STRING(3)
Vat_Rate_Labour             STRING(2)
Vat_Rate_Parts              STRING(2)
License_Number              STRING(10)
Maximum_Users               REAL
Warranty_Period             REAL
Automatic_Replicate         STRING(3)
Automatic_Replicate_Field   STRING(15)
Estimate_If_Over            REAL
Start_Work_Hours            TIME
End_Work_Hours              TIME
Include_Saturday            STRING(3)
Include_Sunday              STRING(3)
Show_Mobile_Number          STRING(3)
Show_Loan_Exchange_Details  STRING(3)
Use_Credit_Limit            STRING(3)
Hide_Physical_Damage        STRING(3)
Hide_Insurance              STRING(3)
Hide_Authority_Number       STRING(3)
Hide_Advance_Payment        STRING(3)
HideColour                  STRING(3)
HideInCourier               STRING(3)
HideIntFault                STRING(3)
HideProduct                 STRING(3)
HideLocation                BYTE
Allow_Bouncer               STRING(3)
Job_Batch_Number            REAL
QA_Required                 STRING(3)
QA_Before_Complete          STRING(3)
QAPreliminary               STRING(3)
QAExchLoan                  STRING(3)
CompleteAtQA                BYTE
RapidQA                     STRING(3)
ValidateESN                 STRING(3)
ValidateDesp                STRING(3)
Force_Accessory_Check       STRING(3)
Force_Initial_Transit_Type  STRING(1)
Force_Mobile_Number         STRING(1)
Force_Model_Number          STRING(1)
Force_Unit_Type             STRING(1)
Force_Fault_Description     STRING(1)
Force_ESN                   STRING(1)
Force_MSN                   STRING(1)
Force_Job_Type              STRING(1)
Force_Engineer              STRING(1)
Force_Invoice_Text          STRING(1)
Force_Repair_Type           STRING(1)
Force_Authority_Number      STRING(1)
Force_Fault_Coding          STRING(1)
Force_Spares                STRING(1)
Force_Incoming_Courier      STRING(1)
Force_Outoing_Courier       STRING(1)
Force_DOP                   STRING(1)
Order_Number                STRING(1)
Customer_Name               STRING(1)
ForcePostcode               STRING(1)
ForceDelPostcode            STRING(1)
ForceCommonFault            STRING(1)
Remove_Backgrounds          STRING(3)
Use_Loan_Exchange_Label     STRING(3)
Use_Job_Label               STRING(3)
UseSmallLabel               BYTE
Job_Label                   STRING(255)
add_stock_label             STRING(3)
receive_stock_label         STRING(3)
QA_Failed_Label             STRING(3)
QA_Failed_Report            STRING(3)
QAPassLabel                 BYTE
ThirdPartyNote              STRING(3)
Vodafone_Import_Path        STRING(255)
Label_Printer_Type          STRING(30)
Browse_Option               STRING(3)
ANCCollectionNo             LONG
Use_Sage                    STRING(3)
Global_Nominal_Code         STRING(30)
Parts_Stock_Code            STRING(30)
Labour_Stock_Code           STRING(30)
Courier_Stock_Code          STRING(30)
Parts_Description           STRING(60)
Labour_Description          STRING(60)
Courier_Description         STRING(60)
Parts_Code                  STRING(30)
Labour_Code                 STRING(30)
Courier_Code                STRING(30)
User_Name_Sage              STRING(30)
Password_Sage               STRING(30)
Company_Number_Sage         STRING(30)
Path_Sage                   STRING(255)
BouncerTime                 LONG
ExportPath                  STRING(255)
PrintBarcode                BYTE
ChaChargeType               STRING(30)
WarChargeType               STRING(30)
RetBackOrders               BYTE
RemoveWorkshopDespatch      BYTE
SummaryOrders               BYTE
EuroRate                    REAL
IrishVatRate                REAL
EmailServerAddress          STRING(255)
EmailServerPort             LONG
Job_Label_Accessories       STRING(3)
ShowRepTypeCosts            BYTE
PickNoteNormal              BYTE
PickNoteMainStore           BYTE
PickNoteChangeStatus        BYTE
PickNoteJobStatus           STRING(30)
TeamPerformanceTicker       BYTE
TickerRefreshRate           SHORT
UseOriginalBookingTemplate  BYTE
                         END
                       END

!------------ modified 09.08.2002 at 11:39:59 -----------------
MOD:stFileName:MANFPALOVer2  STRING(260)
MANFPALOVer2           FILE,DRIVER('Btrieve'),NAME('MANFPALO.DAT'),PRE(MANFPALOVer2),CREATE
Field_Key                KEY(MANFPALOVer2:Manufacturer,MANFPALOVer2:Field_Number,MANFPALOVer2:Field),NOCASE,PRIMARY
DescriptionKey           KEY(MANFPALOVer2:Manufacturer,MANFPALOVer2:Field_Number,MANFPALOVer2:Description),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                       END

!------------ modified 09.08.2002 at 11:39:59 -----------------
MOD:stFileName:MANFAUPAVer2  STRING(260)
MANFAUPAVer2           FILE,DRIVER('Btrieve'),NAME('MANFAUPA.DAT'),PRE(MANFAUPAVer2),CREATE
Field_Number_Key         KEY(MANFAUPAVer2:Manufacturer,MANFAUPAVer2:Field_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
                         END
                       END

!------------ modified 10.08.2015 at 13:20:36 -----------------
MOD:stFileName:SIDALERTVer7  STRING(260)
SIDALERTVer7           FILE,DRIVER('Btrieve'),OEM,NAME('SIDALERT.DAT'),PRE(SIDALERTVer7),CREATE
RecordNumberKey          KEY(SIDALERTVer7:RecordNumber),NOCASE,PRIMARY
StatusKey                KEY(SIDALERTVer7:Status),DUP,NOCASE
BookingTypeStatusKey     KEY(SIDALERTVer7:BookingType,SIDALERTVer7:Status),DUP,NOCASE
BookingRetailerStatusKey KEY(SIDALERTVer7:BookingType,SIDALERTVer7:Retailer,SIDALERTVer7:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Status                      STRING(3)
BookingType                 STRING(30)
Description                 STRING(60)
Monday                      BYTE
Tuesday                     BYTE
Wednesday                   BYTE
Thursday                    BYTE
Friday                      BYTE
Saturday                    BYTE
Sunday                      BYTE
SendTo                      BYTE
SMSAlertActive              BYTE
CutOffTime                  TIME
CutOffTimeType              BYTE
BankHolidays                BYTE
SMSMessageText              STRING(480)
EmailAlertActive            BYTE
EmailMessageSubject         STRING(60)
SystemAlert                 BYTE
CheckAccessories            BYTE
WithBattery                 BYTE
Retailer                    STRING(30)
                         END
                       END

!------------ modified 25.02.2014 at 09:55:12 -----------------
MOD:stFileName:SIDALDEFVer5  STRING(260)
SIDALDEFVer5           FILE,DRIVER('Btrieve'),OEM,NAME('SIDALDEF.DAT'),PRE(SIDALDEFVer5),CREATE
RecordNumberKey          KEY(SIDALDEFVer5:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(100)
SMTPPortNumber              LONG
SMTPUserName                STRING(100)
SMTPPassword                STRING(100)
EmailFromAddress            STRING(255)
UTLEmailAddress             STRING(255)
SMSURL                      STRING(255)
SMSUsername                 STRING(100)
SMSPassword                 STRING(100)
ReplyToAddress              STRING(255)
SMSFTPPath                  STRING(100)
                         END
                       END

!------------ modified 21.09.2007 at 16:04:40 -----------------
MOD:stFileName:SIDREMINVer2  STRING(260)
SIDREMINVer2           FILE,DRIVER('Btrieve'),OEM,NAME('SIDREMIN.DAT'),PRE(SIDREMINVer2),CREATE
RecordNumberKey          KEY(SIDREMINVer2:RecordNumber),NOCASE,PRIMARY
ElapsedKey               KEY(SIDREMINVer2:SIDALERTRecordNumber,SIDREMINVer2:ElapsedDays),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SIDALERTRecordNumber        LONG
Description                 STRING(60)
ElapsedDays                 LONG
SendTo                      BYTE
SMSAlertActive              BYTE
BankHolidays                BYTE
SMSMessageText              STRING(480)
EmailAlertActive            BYTE
EmailSubject                STRING(60)
                         END
                       END

!------------ modified 05.07.2010 at 10:50:06 -----------------
MOD:stFileName:SIDRRCTVer2  STRING(260)
SIDRRCTVer2            FILE,DRIVER('Btrieve'),NAME('SIDRRCT.DAT'),PRE(SIDRRCTVer2),CREATE
RecordNumberKey          KEY(SIDRRCTVer2:RecordNumber),NOCASE,PRIMARY
TierKey                  KEY(SIDRRCTVer2:Tier),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Tier                        BYTE
ChargeValue                 PDECIMAL(6,2)
SkuCode                     STRING(30)
                         END
                       END

!------------ modified 09.01.2009 at 11:20:41 -----------------
MOD:stFileName:SIDMODTTVer3  STRING(260)
SIDMODTTVer3           FILE,DRIVER('Btrieve'),PRE(SIDMODTTVer3),NAME('SIDMODTT'),CREATE
RecordNumberKey          KEY(SIDMODTTVer3:RecordNumber),NOCASE,PRIMARY
ModelNoKey               KEY(SIDMODTTVer3:ModelNo),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
TurnaroundException         BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysExch          LONG
ActiveTextRetail            BYTE
TextRetail                  STRING(250)
ActiveTextExch              BYTE
TextExch                    STRING(250)
                         END
                       END

!------------ modified 11.08.2011 at 15:17:23 -----------------
MOD:stFileName:SIDSRVCNVer6  STRING(260)
SIDSRVCNVer6           FILE,DRIVER('Btrieve'),NAME('SIDSRVCN.DAT'),PRE(SIDSRVCNVer6),CREATE
SCAccountIDKey           KEY(SIDSRVCNVer6:SCAccountID),NOCASE,PRIMARY
ServiceCentreNameKey     KEY(SIDSRVCNVer6:ServiceCentreName),DUP,NOCASE
Record                   RECORD,PRE()
SCAccountID                 LONG
ServiceCentreName           STRING(30)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
Postcode                    STRING(15)
AccountActive               BYTE
TurnaroundDaysRetail        LONG
TurnaroundDaysExch          LONG
BankHolidaysFileName        STRING(100)
ContactName                 STRING(30)
ContactNumber               STRING(30)
ContactEmail                STRING(255)
UseServiceCentreFTP         BYTE
FTPServer                   STRING(30)
FTPUsername                 STRING(30)
FTPPassword                 STRING(30)
FTPPath                     STRING(255)
EmailToAddress              STRING(255)
ProcessFailureCount         LONG
FTPFailedCount              LONG
MonRepairCapacity           LONG
TueRepairCapacity           LONG
WedRepairCapacity           LONG
ThuRepairCapacity           LONG
FriRepairCapacity           LONG
ExchangeImportType          BYTE
AccessoryOrders             BYTE
                         END
                       END

!------------ modified 08.01.2009 at 11:20:46 -----------------
MOD:stFileName:SIDMODSCVer4  STRING(260)
SIDMODSCVer4           FILE,DRIVER('Btrieve'),PRE(SIDMODSCVer4),NAME('SIDMODSC'),CREATE
RecordNumberKey          KEY(SIDMODSCVer4:RecordNumber),NOCASE,PRIMARY
ManModelTypeKey          KEY(SIDMODSCVer4:Manufacturer,SIDMODSCVer4:ModelNo,SIDMODSCVer4:BookingType),NOCASE
SCAccountIDKey           KEY(SIDMODSCVer4:SCAccountID),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNo                     STRING(30)
BookingType                 STRING(1)
SCAccountID                 LONG
                         END
                       END

!------------ modified 14.01.2008 at 15:09:07 -----------------
MOD:stFileName:SIDNWDAYVer2  STRING(260)
SIDNWDAYVer2           FILE,DRIVER('Btrieve'),PRE(SIDNWDAYVer2),NAME('SIDNWDAY'),CREATE
RecordNumberKey          KEY(SIDNWDAYVer2:RecordNumber),NOCASE,PRIMARY
SCDayKey                 KEY(SIDNWDAYVer2:SCAccountID,SIDNWDAYVer2:NonWorkingDay),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SCAccountID                 LONG
NonWorkingDay               DATE
                         END
                       END

!------------ modified 12.03.2010 at 09:29:03 -----------------
MOD:stFileName:SMOREMINVer2  STRING(260)
SMOREMINVer2           FILE,DRIVER('Btrieve'),OEM,NAME('SMOREMIN.DAT'),PRE(SMOREMINVer2),CREATE
RecordNumberKey          KEY(SMOREMINVer2:RecordNumber),NOCASE,PRIMARY
SIDREMINRecordNumberKey  KEY(SMOREMINVer2:SIDREMINRecordNumber,SMOREMINVer2:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
SIDREMINRecordNumber        LONG
ModelNumber                 STRING(30)
SMSAlertActive              BYTE
SMSMessageText              STRING(480)
EmailAlertActive            BYTE
EmailSubject                STRING(60)
                         END
                       END

!------------ modified 09.08.2002 at 11:39:59 -----------------
MOD:stFileName:MANFAULOVer2  STRING(260)
MANFAULOVer2           FILE,DRIVER('Btrieve'),NAME('MANFAULO.DAT'),PRE(MANFAULOVer2),CREATE
Field_Key                KEY(MANFAULOVer2:Manufacturer,MANFAULOVer2:Field_Number,MANFAULOVer2:Field),NOCASE,PRIMARY
DescriptionKey           KEY(MANFAULOVer2:Manufacturer,MANFAULOVer2:Field_Number,MANFAULOVer2:Description),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                       END

!------------ modified 06.03.2002 at 12:13:13 -----------------
MOD:stFileName:RETSTOCKVer2  STRING(260)
RETSTOCKVer2           FILE,DRIVER('Btrieve'),OEM,NAME('RETSTOCK.DAT'),PRE(RETSTOCKVer2),CREATE
Record_Number_Key        KEY(RETSTOCKVer2:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Part_Number),DUP,NOCASE
Despatched_Key           KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Despatched,RETSTOCKVer2:Despatch_Note_Number,RETSTOCKVer2:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Order_Number),DUP,NOCASE
DespatchedKey            KEY(RETSTOCKVer2:Despatched),DUP,NOCASE
DespatchedPartKey        KEY(RETSTOCKVer2:Ref_Number,RETSTOCKVer2:Despatched,RETSTOCKVer2:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
                         END
                       END

!------------ modified 09.08.2002 at 11:39:59 -----------------
MOD:stFileName:ORDPARTSVer2  STRING(260)
ORDPARTSVer2           FILE,DRIVER('Btrieve'),NAME('ORDPARTS.DAT'),PRE(ORDPARTSVer2),CREATE
Order_Number_Key         KEY(ORDPARTSVer2:Order_Number,ORDPARTSVer2:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(ORDPARTSVer2:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(ORDPARTSVer2:Part_Ref_Number,ORDPARTSVer2:Part_Number,ORDPARTSVer2:Description),DUP,NOCASE
Part_Number_Key          KEY(ORDPARTSVer2:Order_Number,ORDPARTSVer2:Part_Number,ORDPARTSVer2:Description),DUP,NOCASE
Description_Key          KEY(ORDPARTSVer2:Order_Number,ORDPARTSVer2:Description,ORDPARTSVer2:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(ORDPARTSVer2:All_Received,ORDPARTSVer2:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(ORDPARTSVer2:Account_Number,ORDPARTSVer2:Part_Type,ORDPARTSVer2:All_Received,ORDPARTSVer2:Part_Number),DUP,NOCASE
Allocated_Key            KEY(ORDPARTSVer2:Part_Type,ORDPARTSVer2:All_Received,ORDPARTSVer2:Allocated_To_Sale,ORDPARTSVer2:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(ORDPARTSVer2:Part_Type,ORDPARTSVer2:All_Received,ORDPARTSVer2:Allocated_To_Sale,ORDPARTSVer2:Account_Number,ORDPARTSVer2:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(ORDPARTSVer2:Order_Number,ORDPARTSVer2:Part_Type,ORDPARTSVer2:All_Received,ORDPARTSVer2:Part_Number,ORDPARTSVer2:Description),DUP,NOCASE
PartRecordNumberKey      KEY(ORDPARTSVer2:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
PartRecordNumber            LONG
                         END
                       END

!------------ modified 16.01.2004 at 13:55:32 -----------------
MOD:stFileName:LOCATIONVer3  STRING(260)
LOCATIONVer3           FILE,DRIVER('Btrieve'),NAME('LOCATION.DAT'),PRE(LOCATIONVer3),CREATE
RecordNumberKey          KEY(LOCATIONVer3:RecordNumber),NOCASE,PRIMARY
Location_Key             KEY(LOCATIONVer3:Location),NOCASE
Main_Store_Key           KEY(LOCATIONVer3:Main_Store,LOCATIONVer3:Location),DUP,NOCASE
ActiveLocationKey        KEY(LOCATIONVer3:Active,LOCATIONVer3:Location),DUP,NOCASE
ActiveMainStoreKey       KEY(LOCATIONVer3:Active,LOCATIONVer3:Main_Store,LOCATIONVer3:Location),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
Main_Store                  STRING(3)
Active                      BYTE
PickNoteEnable              BYTE
                         END
                       END

!------------ modified 04.07.2003 at 13:27:53 -----------------
MOD:stFileName:JOBSVer5  STRING(260)
JOBSVer5               FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(JOBSVer5),BINDABLE,CREATE
Ref_Number_Key           KEY(JOBSVer5:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(JOBSVer5:Model_Number,JOBSVer5:Unit_Type),DUP,NOCASE
EngCompKey               KEY(JOBSVer5:Engineer,JOBSVer5:Completed,JOBSVer5:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(JOBSVer5:Engineer,JOBSVer5:Workshop,JOBSVer5:Ref_Number),DUP,NOCASE
Surname_Key              KEY(JOBSVer5:Surname),DUP,NOCASE
MobileNumberKey          KEY(JOBSVer5:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(JOBSVer5:ESN),DUP,NOCASE
MSN_Key                  KEY(JOBSVer5:MSN),DUP,NOCASE
AccountNumberKey         KEY(JOBSVer5:Account_Number,JOBSVer5:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(JOBSVer5:Account_Number,JOBSVer5:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(JOBSVer5:Model_Number),DUP,NOCASE
Engineer_Key             KEY(JOBSVer5:Engineer,JOBSVer5:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(JOBSVer5:date_booked),DUP,NOCASE
DateCompletedKey         KEY(JOBSVer5:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(JOBSVer5:Model_Number,JOBSVer5:Date_Completed),DUP,NOCASE
By_Status                KEY(JOBSVer5:Current_Status,JOBSVer5:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(JOBSVer5:Current_Status,JOBSVer5:Location,JOBSVer5:Ref_Number),DUP,NOCASE
Location_Key             KEY(JOBSVer5:Location,JOBSVer5:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(JOBSVer5:Third_Party_Site,JOBSVer5:Third_Party_Printed,JOBSVer5:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(JOBSVer5:Third_Party_Site,JOBSVer5:Third_Party_Printed,JOBSVer5:ESN),DUP,NOCASE
ThirdMsnKey              KEY(JOBSVer5:Third_Party_Site,JOBSVer5:Third_Party_Printed,JOBSVer5:MSN),DUP,NOCASE
PriorityTypeKey          KEY(JOBSVer5:Job_Priority,JOBSVer5:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(JOBSVer5:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(JOBSVer5:Manufacturer,JOBSVer5:EDI,JOBSVer5:EDI_Batch_Number,JOBSVer5:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(JOBSVer5:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(JOBSVer5:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(JOBSVer5:Batch_Number,JOBSVer5:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(JOBSVer5:Batch_Number,JOBSVer5:Current_Status,JOBSVer5:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(JOBSVer5:Batch_Number,JOBSVer5:Model_Number,JOBSVer5:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(JOBSVer5:Batch_Number,JOBSVer5:Invoice_Number,JOBSVer5:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(JOBSVer5:Batch_Number,JOBSVer5:Completed,JOBSVer5:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(JOBSVer5:Chargeable_Job,JOBSVer5:Account_Number,JOBSVer5:Invoice_Number,JOBSVer5:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(JOBSVer5:Invoice_Exception,JOBSVer5:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(JOBSVer5:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(JOBSVer5:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(JOBSVer5:Despatched,JOBSVer5:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(JOBSVer5:Despatched,JOBSVer5:Account_Number,JOBSVer5:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(JOBSVer5:Despatched,JOBSVer5:Current_Courier,JOBSVer5:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(JOBSVer5:Despatched,JOBSVer5:Account_Number,JOBSVer5:Current_Courier,JOBSVer5:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(JOBSVer5:Despatch_Number,JOBSVer5:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(JOBSVer5:Courier,JOBSVer5:Date_Despatched,JOBSVer5:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(JOBSVer5:Loan_Courier,JOBSVer5:Loan_Despatched,JOBSVer5:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(JOBSVer5:Exchange_Courier,JOBSVer5:Exchange_Despatched,JOBSVer5:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(JOBSVer5:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(JOBSVer5:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(JOBSVer5:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(JOBSVer5:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(JOBSVer5:Bouncer,JOBSVer5:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(JOBSVer5:Engineer,JOBSVer5:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(JOBSVer5:Exchange_Status,JOBSVer5:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(JOBSVer5:Loan_Status,JOBSVer5:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(JOBSVer5:Exchange_Status,JOBSVer5:Location,JOBSVer5:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(JOBSVer5:Loan_Status,JOBSVer5:Location,JOBSVer5:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(JOBSVer5:InvoiceAccount,JOBSVer5:InvoiceBatch,JOBSVer5:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(JOBSVer5:InvoiceAccount,JOBSVer5:InvoiceBatch,JOBSVer5:InvoiceStatus,JOBSVer5:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                       END

!------------ modified 10.07.2015 at 16:34:13 -----------------
MOD:stFileName:MODELNUMVer20  STRING(260)
MODELNUMVer20          FILE,DRIVER('Btrieve'),NAME('MODELNUM.DAT'),PRE(MODELNUMVer20),CREATE
Model_Number_Key         KEY(MODELNUMVer20:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(MODELNUMVer20:Manufacturer,MODELNUMVer20:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(MODELNUMVer20:Manufacturer,MODELNUMVer20:Unit_Type),DUP,NOCASE
RetailRepairTierKey      KEY(MODELNUMVer20:RetailRepairTier),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
UnsupportedExchange         BYTE
UnitCost                    REAL
VodafoneISPAccount          BYTE
OEM                         BYTE
OEMManufacturer             STRING(30)
KnownIssueDisplay           BYTE
RetailExchange              BYTE
CBUPostal                   BYTE
CBUExchange                 BYTE
ISPPostal                   BYTE
ISPExchange                 BYTE
EBUPostal                   BYTE
EBUExchange                 BYTE
ModelSpecificURL1           STRING(250)
ModelSpecificURL2           STRING(250)
ModelSpecificURL3           STRING(250)
CCExchange28Day             BYTE
RetailRepairTier            BYTE
ExchangeCharge              PDECIMAL(6,2)
PostalRepairCharge          PDECIMAL(6,2)
Replacement28Day            BYTE
TabletDevice                BYTE
ReturnPeriod                LONG
BERCharge                   PDECIMAL(6,2)
Router                      LONG
AccessoryProduct            LONG
SerialisedProduct           LONG
AccessoryRepair             LONG
AccessoryReturn             LONG
                         END
                       END

!------------ modified 16.01.2004 at 13:55:35 -----------------
MOD:stFileName:STATUSVer4  STRING(260)
STATUSVer4             FILE,DRIVER('Btrieve'),NAME('STATUS.DAT'),PRE(STATUSVer4),CREATE
Ref_Number_Only_Key      KEY(STATUSVer4:Ref_Number),NOCASE,PRIMARY
Status_Key               KEY(STATUSVer4:Status),DUP,NOCASE
Heading_Key              KEY(STATUSVer4:Heading_Ref_Number,STATUSVer4:Status),NOCASE
Ref_Number_Key           KEY(STATUSVer4:Heading_Ref_Number,STATUSVer4:Ref_Number),NOCASE
LoanKey                  KEY(STATUSVer4:Loan,STATUSVer4:Status),DUP,NOCASE
ExchangeKey              KEY(STATUSVer4:Exchange,STATUSVer4:Status),DUP,NOCASE
JobKey                   KEY(STATUSVer4:Job,STATUSVer4:Status),DUP,NOCASE
TurnJobKey               KEY(STATUSVer4:Use_Turnaround_Time,STATUSVer4:Job,STATUSVer4:Status),DUP,NOCASE
Record                   RECORD,PRE()
Heading_Ref_Number          REAL
Status                      STRING(30)
Ref_Number                  REAL
Use_Turnaround_Time         STRING(3)
Use_Turnaround_Days         STRING(3)
Turnaround_Days             LONG
Use_Turnaround_Hours        STRING(3)
Turnaround_Hours            LONG
Notes                       STRING(1000)
Loan                        STRING(3)
Exchange                    STRING(3)
Job                         STRING(3)
SystemStatus                STRING(3)
EngineerStatus              BYTE
EnableEmail                 BYTE
SenderEmailAddress          STRING(255)
EmailSubject                STRING(255)
EmailBody                   STRING(500)
PickNoteEnable              BYTE
                         END
                       END

!------------ modified 25.02.2004 at 11:34:04 -----------------
MOD:stFileName:TRADEACCVer8  STRING(260)
TRADEACCVer8           FILE,DRIVER('Btrieve'),NAME('TRADEACC.DAT'),PRE(TRADEACCVer8),CREATE
RecordNumberKey          KEY(TRADEACCVer8:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(TRADEACCVer8:Account_Number),NOCASE
Company_Name_Key         KEY(TRADEACCVer8:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
MakeSplitJob                BYTE
SplitJobStatus              STRING(30)
SplitExchangeStatus         STRING(30)
SplitCChargeType            STRING(30)
SplitWChargeType            STRING(30)
CheckChargeablePartsCost    BYTE
MaxChargeablePartsCost      REAL
UseAlternateDespatchNote    BYTE
                         END
                       END

!------------ modified 17.03.2008 at 09:59:43 -----------------
MOD:stFileName:SUBTRACCVer8  STRING(260)
SUBTRACCVer8           FILE,DRIVER('Btrieve'),NAME('SUBTRACC.DAT'),PRE(SUBTRACCVer8),CREATE
RecordNumberKey          KEY(SUBTRACCVer8:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(SUBTRACCVer8:Main_Account_Number,SUBTRACCVer8:Account_Number),NOCASE
Main_Name_Key            KEY(SUBTRACCVer8:Main_Account_Number,SUBTRACCVer8:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(SUBTRACCVer8:Account_Number),NOCASE
Branch_Key               KEY(SUBTRACCVer8:Branch),DUP,NOCASE
Main_Branch_Key          KEY(SUBTRACCVer8:Main_Account_Number,SUBTRACCVer8:Branch),DUP,NOCASE
Company_Name_Key         KEY(SUBTRACCVer8:Company_Name),DUP,NOCASE
Contact_Centre_Key       KEY(SUBTRACCVer8:IsContactCentre,SUBTRACCVer8:Account_Number),DUP,NOCASE
RetailStoreKey           KEY(SUBTRACCVer8:VodafoneRetailStore,SUBTRACCVer8:Account_Number),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
IsContactCentre             BYTE
VodafoneRetailStore         BYTE
ContactCentreAccType        BYTE
                         END
                       END

!------------ modified 20.07.2009 at 11:53:00 -----------------
MOD:stFileName:MANUFACTVer12  STRING(260)
MANUFACTVer12          FILE,DRIVER('Btrieve'),NAME('MANUFACT.DAT'),PRE(MANUFACTVer12),CREATE
RecordNumberKey          KEY(MANUFACTVer12:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(MANUFACTVer12:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
IsCCentreSpecific           BYTE
EDIFileType                 STRING(30)
VodafoneISPAccount          BYTE
KnownIssueDisplay           BYTE
VodafoneEBUAccount          BYTE
                         END
                       END

!------------ modified 19.09.2005 at 14:08:12 -----------------
MOD:stFileName:XMLDEFSVer4  STRING(260)
XMLDEFSVer4            FILE,DRIVER('Btrieve'),OEM,NAME('XMLDEFS.DAT'),PRE(XMLDEFSVer4),CREATE
RecordNumberKey          KEY(XMLDEFSVer4:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserCode                    STRING(3)
AccountNumber               STRING(15)
ChargeType                  STRING(30)
WarrantyChargeType          STRING(30)
TransitType                 STRING(30)
JobTurnaroundTime           STRING(30)
UnitType                    STRING(30)
CollectionStatus            STRING(30)
DropOffStatus               STRING(30)
DeliveryText                STRING(255)
DropPointStatus             STRING(30)
DropPointLocation           STRING(30)
                         END
                       END

!------------ modified 19.08.2004 at 14:06:39 -----------------
MOD:stFileName:XMLSBVer2  STRING(260)
XMLSBVer2              FILE,DRIVER('Btrieve'),OEM,NAME('XMLSB.DAT'),PRE(XMLSBVer2),CREATE
SBStatusKey              KEY(XMLSBVer2:SBStatus),NOCASE,PRIMARY
FKStatusCodeKey          KEY(XMLSBVer2:FKStatusCode),DUP,NOCASE
Record                   RECORD,PRE()
SBStatus                    STRING(30)
FKStatusCode                STRING(4)
                         END
                       END

!------------ modified 19.12.2005 at 14:35:59 -----------------
MOD:stFileName:MPXDEFSVer6  STRING(260)
MPXDEFSVer6            FILE,DRIVER('Btrieve'),OEM,NAME('MPXDEFS.DAT'),PRE(MPXDEFSVer6),CREATE
AccountNoKey             KEY(MPXDEFSVer6:AccountNo),NOCASE,PRIMARY
Record                   RECORD,PRE()
AccountNo                   STRING(15)
UserCode                    STRING(3)
IsChargeable                BYTE
ChargeType                  STRING(30)
IsWarranty                  BYTE
WarrantyType                STRING(30)
TransitType                 STRING(30)
UnitType                    STRING(30)
DeliveryText                STRING(255)
Status                      STRING(30)
Location                    STRING(30)
EnvCountStatus              STRING(30)
                         END
                       END

!------------ modified 19.08.2004 at 14:06:39 -----------------
MOD:stFileName:XMLSAMVer2  STRING(260)
XMLSAMVer2             FILE,DRIVER('Btrieve'),OEM,NAME('XMLSAM.DAT'),PRE(XMLSAMVer2),CREATE
StatusCodeKey            KEY(XMLSAMVer2:StatusCode),NOCASE,PRIMARY
Record                   RECORD,PRE()
StatusCode                  STRING(4)
                         END
                       END

!------------ End of Declaration Current Files Structure -------------

DCDummySQLTableOwner   STRING(260)
MOD:stFileName STRING(260)

FileQueue QUEUE,PRE()
FileLabel   &FILE
FileName    &STRING
          END

TempFileQueue QUEUE,PRE()
TempFileLabel   &FILE
TempFileName    &STRING
              END

ListExt       QUEUE,PRE()
Extention       CSTRING(260)
              END

                     MAP
                       INCLUDE('CELLU003.INC'),ONCE        !Local module procedure declarations
ConvertCARISMA         PROCEDURE(BYTE),BYTE
ConvertDEFEMAIL        PROCEDURE(BYTE),BYTE
ConvertREGIONS         PROCEDURE(BYTE),BYTE
ConvertACCREG          PROCEDURE(BYTE),BYTE
ConvertSIDACREG        PROCEDURE(BYTE),BYTE
ConvertXMLJOBS         PROCEDURE(BYTE),BYTE
ConvertMULDESP         PROCEDURE(BYTE),BYTE
ConvertIMEISHIP        PROCEDURE(BYTE),BYTE
ConvertJOBTHIRD        PROCEDURE(BYTE),BYTE
ConvertLOGRETRN        PROCEDURE(BYTE),BYTE
ConvertPRODCODE        PROCEDURE(BYTE),BYTE
ConvertLOGSTOCK        PROCEDURE(BYTE),BYTE
ConvertDEFEMAB2        PROCEDURE(BYTE),BYTE
ConvertDEFEMAB3        PROCEDURE(BYTE),BYTE
ConvertDEFEMAB4        PROCEDURE(BYTE),BYTE
ConvertDEFEMAB5        PROCEDURE(BYTE),BYTE
ConvertDEFEMAE2        PROCEDURE(BYTE),BYTE
ConvertDEFEMAE3        PROCEDURE(BYTE),BYTE
ConvertDEFEMAB1        PROCEDURE(BYTE),BYTE
ConvertDEFEMAE1        PROCEDURE(BYTE),BYTE
ConvertDEFEMAI2        PROCEDURE(BYTE),BYTE
ConvertDEFSIDEX        PROCEDURE(BYTE),BYTE
ConvertRETSALES        PROCEDURE(BYTE),BYTE
ConvertTEAMS           PROCEDURE(BYTE),BYTE
ConvertMERGE           PROCEDURE(BYTE),BYTE
ConvertSIDDEF          PROCEDURE(BYTE),BYTE
ConvertORDPEND         PROCEDURE(BYTE),BYTE
ConvertSTOHIST         PROCEDURE(BYTE),BYTE
ConvertEXREASON        PROCEDURE(BYTE),BYTE
ConvertREPTYDEF        PROCEDURE(BYTE),BYTE
ConvertJOBSE           PROCEDURE(BYTE),BYTE
ConvertMANFAULT        PROCEDURE(BYTE),BYTE
ConvertPICKNOTE        PROCEDURE(BYTE),BYTE
ConvertCOURIER         PROCEDURE(BYTE),BYTE
ConvertESNMODEL        PROCEDURE(BYTE),BYTE
ConvertSTOCK           PROCEDURE(BYTE),BYTE
ConvertLOGSTOLC        PROCEDURE(BYTE),BYTE
ConvertLOGSERST        PROCEDURE(BYTE),BYTE
ConvertLOGSTLOC        PROCEDURE(BYTE),BYTE
ConvertTRAFAULO        PROCEDURE(BYTE),BYTE
ConvertMPXJOBS         PROCEDURE(BYTE),BYTE
ConvertMPXSTAT         PROCEDURE(BYTE),BYTE
ConvertTRAFAULT        PROCEDURE(BYTE),BYTE
ConvertSTATCRIT        PROCEDURE(BYTE),BYTE
ConvertNOTESDEL        PROCEDURE(BYTE),BYTE
ConvertPICKDET         PROCEDURE(BYTE),BYTE
ConvertUSERS           PROCEDURE(BYTE),BYTE
ConvertSIDREG          PROCEDURE(BYTE),BYTE
ConvertPENDMAIL        PROCEDURE(BYTE),BYTE
ConvertDEFAULTS        PROCEDURE(BYTE),BYTE
ConvertMANFPALO        PROCEDURE(BYTE),BYTE
ConvertMANFAUPA        PROCEDURE(BYTE),BYTE
ConvertSIDALERT        PROCEDURE(BYTE),BYTE
ConvertSIDALDEF        PROCEDURE(BYTE),BYTE
ConvertSIDREMIN        PROCEDURE(BYTE),BYTE
ConvertSIDRRCT         PROCEDURE(BYTE),BYTE
ConvertSIDMODTT        PROCEDURE(BYTE),BYTE
ConvertSIDSRVCN        PROCEDURE(BYTE),BYTE
ConvertSIDMODSC        PROCEDURE(BYTE),BYTE
ConvertSIDNWDAY        PROCEDURE(BYTE),BYTE
ConvertSMOREMIN        PROCEDURE(BYTE),BYTE
ConvertMANFAULO        PROCEDURE(BYTE),BYTE
ConvertRETSTOCK        PROCEDURE(BYTE),BYTE
ConvertORDPARTS        PROCEDURE(BYTE),BYTE
ConvertLOCATION        PROCEDURE(BYTE),BYTE
ConvertJOBS            PROCEDURE(BYTE),BYTE
ConvertMODELNUM        PROCEDURE(BYTE),BYTE
ConvertSTATUS          PROCEDURE(BYTE),BYTE
ConvertTRADEACC        PROCEDURE(BYTE),BYTE
ConvertSUBTRACC        PROCEDURE(BYTE),BYTE
ConvertMANUFACT        PROCEDURE(BYTE),BYTE
ConvertXMLDEFS         PROCEDURE(BYTE),BYTE
ConvertXMLSB           PROCEDURE(BYTE),BYTE
ConvertMPXDEFS         PROCEDURE(BYTE),BYTE
ConvertXMLSAM          PROCEDURE(BYTE),BYTE
ProcessFileQueue       PROCEDURE(),BYTE
ErrorBox               PROCEDURE(STRING)
FileErrorBox           PROCEDURE()
CreateTempName         PROCEDURE(*STRING,STRING,FILE)
RemoveFiles            PROCEDURE(FILE,STRING)
GetFileNameWithoutOwner PROCEDURE(STRING),STRING
RenameFile             PROCEDURE(FILE,STRING)
PrepareExtentionQueue  PROCEDURE(FILE)
PrepareMultiTableFileName PROCEDURE(FILE,STRING,*STRING),BYTE
PrepareDestFile        PROCEDURE(FILE,STRING,FILE,STRING,STRING),BYTE
RenameDestFile         PROCEDURE(FILE,STRING,STRING),BYTE
                       MODULE('ClarionRTL')
DC:FnMerge               PROCEDURE(*CSTRING,*CSTRING,*CSTRING,*CSTRING,*CSTRING),RAW,NAME('_fnmerge')
DC:FnSplit               PROCEDURE(*CSTRING,*CSTRING,*CSTRING,*CSTRING,*CSTRING),SHORT,RAW,NAME('_fnsplit')
DC:SetError              PROCEDURE(LONG),NAME('Cla$seterror')
DC:SetErrorFile          PROCEDURE(FILE),NAME('Cla$SetErrorFile')
                       END
                     END



DC:FilesConverter PROCEDURE(<STRING PAR:FileLabel>,<STRING PAR:FileName>)
  CODE
  IF ~OMITTED(1) AND ~OMITTED(2) AND PAR:FileLabel AND PAR:FileName
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('CARISMA')
      CARISMAVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertCARISMA(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFEMAIL')
      DEFEMAILVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFEMAIL(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('REGIONS')
      REGIONSVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertREGIONS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('ACCREG')
      ACCREGVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertACCREG(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDACREG')
      SIDACREGVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDACREG(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('XMLJOBS')
      XMLJOBSVer17{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertXMLJOBS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MULDESP')
      MULDESPVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMULDESP(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('IMEISHIP')
      IMEISHIPVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertIMEISHIP(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('JOBTHIRD')
      JOBTHIRDVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertJOBTHIRD(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOGRETRN')
      LOGRETRNVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOGRETRN(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('PRODCODE')
      PRODCODEVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertPRODCODE(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOGSTOCK')
      LOGSTOCKVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOGSTOCK(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFEMAB2')
      DEFEMAB2Ver2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFEMAB2(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFEMAB3')
      DEFEMAB3Ver2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFEMAB3(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFEMAB4')
      DEFEMAB4Ver2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFEMAB4(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFEMAB5')
      DEFEMAB5Ver2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFEMAB5(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFEMAE2')
      DEFEMAE2Ver2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFEMAE2(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFEMAE3')
      DEFEMAE3Ver2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFEMAE3(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFEMAB1')
      DEFEMAB1Ver2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFEMAB1(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFEMAE1')
      DEFEMAE1Ver6{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFEMAE1(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFEMAI2')
      DEFEMAI2Ver9{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFEMAI2(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFSIDEX')
      DEFSIDEXVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFSIDEX(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('RETSALES')
      RETSALESVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertRETSALES(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('TEAMS')
      TEAMSVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertTEAMS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MERGE')
      MERGEVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMERGE(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDDEF')
      SIDDEFVer6{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDDEF(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('ORDPEND')
      ORDPENDVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertORDPEND(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('STOHIST')
      STOHISTVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSTOHIST(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('EXREASON')
      EXREASONVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertEXREASON(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('REPTYDEF')
      REPTYDEFVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertREPTYDEF(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('JOBSE')
      JOBSEVer10{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertJOBSE(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MANFAULT')
      MANFAULTVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMANFAULT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('PICKNOTE')
      PICKNOTEVer5{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertPICKNOTE(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('COURIER')
      COURIERVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertCOURIER(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('ESNMODEL')
      ESNMODELVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertESNMODEL(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('STOCK')
      STOCKVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSTOCK(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOGSTOLC')
      LOGSTOLCVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOGSTOLC(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOGSERST')
      LOGSERSTVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOGSERST(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOGSTLOC')
      LOGSTLOCVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOGSTLOC(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('TRAFAULO')
      TRAFAULOVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertTRAFAULO(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MPXJOBS')
      MPXJOBSVer6{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMPXJOBS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MPXSTAT')
      MPXSTATVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMPXSTAT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('TRAFAULT')
      TRAFAULTVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertTRAFAULT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('STATCRIT')
      STATCRITVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSTATCRIT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('NOTESDEL')
      NOTESDELVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertNOTESDEL(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('PICKDET')
      PICKDETVer6{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertPICKDET(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('USERS')
      USERSVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertUSERS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDREG')
      SIDREGVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDREG(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('PENDMAIL')
      PENDMAILVer14{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertPENDMAIL(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('DEFAULTS')
      DEFAULTSVer7{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertDEFAULTS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MANFPALO')
      MANFPALOVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMANFPALO(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MANFAUPA')
      MANFAUPAVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMANFAUPA(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDALERT')
      SIDALERTVer7{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDALERT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDALDEF')
      SIDALDEFVer5{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDALDEF(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDREMIN')
      SIDREMINVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDREMIN(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDRRCT')
      SIDRRCTVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDRRCT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDMODTT')
      SIDMODTTVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDMODTT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDSRVCN')
      SIDSRVCNVer6{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDSRVCN(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDMODSC')
      SIDMODSCVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDMODSC(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SIDNWDAY')
      SIDNWDAYVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSIDNWDAY(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SMOREMIN')
      SMOREMINVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSMOREMIN(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MANFAULO')
      MANFAULOVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMANFAULO(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('RETSTOCK')
      RETSTOCKVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertRETSTOCK(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('ORDPARTS')
      ORDPARTSVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertORDPARTS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('LOCATION')
      LOCATIONVer3{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertLOCATION(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('JOBS')
      JOBSVer5{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertJOBS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MODELNUM')
      MODELNUMVer20{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMODELNUM(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('STATUS')
      STATUSVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSTATUS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('TRADEACC')
      TRADEACCVer8{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertTRADEACC(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('SUBTRACC')
      SUBTRACCVer8{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertSUBTRACC(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MANUFACT')
      MANUFACTVer12{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMANUFACT(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('XMLDEFS')
      XMLDEFSVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertXMLDEFS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('XMLSB')
      XMLSBVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertXMLSB(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('MPXDEFS')
      MPXDEFSVer6{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertMPXDEFS(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('XMLSAM')
      XMLSAMVer2{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertXMLSAM(0) THEN RETURN(1).
    END
  ELSE
    IF ConvertCARISMA(1) THEN RETURN(1).
    IF ConvertDEFEMAIL(1) THEN RETURN(1).
    IF ConvertREGIONS(1) THEN RETURN(1).
    IF ConvertACCREG(1) THEN RETURN(1).
    IF ConvertSIDACREG(1) THEN RETURN(1).
    IF ConvertXMLJOBS(1) THEN RETURN(1).
    IF ConvertMULDESP(1) THEN RETURN(1).
    IF ConvertIMEISHIP(1) THEN RETURN(1).
    IF ConvertJOBTHIRD(1) THEN RETURN(1).
    IF ConvertLOGRETRN(1) THEN RETURN(1).
    IF ConvertPRODCODE(1) THEN RETURN(1).
    IF ConvertLOGSTOCK(1) THEN RETURN(1).
    IF ConvertDEFEMAB2(1) THEN RETURN(1).
    IF ConvertDEFEMAB3(1) THEN RETURN(1).
    IF ConvertDEFEMAB4(1) THEN RETURN(1).
    IF ConvertDEFEMAB5(1) THEN RETURN(1).
    IF ConvertDEFEMAE2(1) THEN RETURN(1).
    IF ConvertDEFEMAE3(1) THEN RETURN(1).
    IF ConvertDEFEMAB1(1) THEN RETURN(1).
    IF ConvertDEFEMAE1(1) THEN RETURN(1).
    IF ConvertDEFEMAI2(1) THEN RETURN(1).
    IF ConvertDEFSIDEX(1) THEN RETURN(1).
    IF ConvertRETSALES(1) THEN RETURN(1).
    IF ConvertTEAMS(1) THEN RETURN(1).
    IF ConvertMERGE(1) THEN RETURN(1).
    IF ConvertSIDDEF(1) THEN RETURN(1).
    IF ConvertORDPEND(1) THEN RETURN(1).
    IF ConvertSTOHIST(1) THEN RETURN(1).
    IF ConvertEXREASON(1) THEN RETURN(1).
    IF ConvertREPTYDEF(1) THEN RETURN(1).
    IF ConvertJOBSE(1) THEN RETURN(1).
    IF ConvertMANFAULT(1) THEN RETURN(1).
    IF ConvertPICKNOTE(1) THEN RETURN(1).
    IF ConvertCOURIER(1) THEN RETURN(1).
    IF ConvertESNMODEL(1) THEN RETURN(1).
    IF ConvertSTOCK(1) THEN RETURN(1).
    IF ConvertLOGSTOLC(1) THEN RETURN(1).
    IF ConvertLOGSERST(1) THEN RETURN(1).
    IF ConvertLOGSTLOC(1) THEN RETURN(1).
    IF ConvertTRAFAULO(1) THEN RETURN(1).
    IF ConvertMPXJOBS(1) THEN RETURN(1).
    IF ConvertMPXSTAT(1) THEN RETURN(1).
    IF ConvertTRAFAULT(1) THEN RETURN(1).
    IF ConvertSTATCRIT(1) THEN RETURN(1).
    IF ConvertNOTESDEL(1) THEN RETURN(1).
    IF ConvertPICKDET(1) THEN RETURN(1).
    IF ConvertUSERS(1) THEN RETURN(1).
    IF ConvertSIDREG(1) THEN RETURN(1).
    IF ConvertPENDMAIL(1) THEN RETURN(1).
    IF ConvertDEFAULTS(1) THEN RETURN(1).
    IF ConvertMANFPALO(1) THEN RETURN(1).
    IF ConvertMANFAUPA(1) THEN RETURN(1).
    IF ConvertSIDALERT(1) THEN RETURN(1).
    IF ConvertSIDALDEF(1) THEN RETURN(1).
    IF ConvertSIDREMIN(1) THEN RETURN(1).
    IF ConvertSIDRRCT(1) THEN RETURN(1).
    IF ConvertSIDMODTT(1) THEN RETURN(1).
    IF ConvertSIDSRVCN(1) THEN RETURN(1).
    IF ConvertSIDMODSC(1) THEN RETURN(1).
    IF ConvertSIDNWDAY(1) THEN RETURN(1).
    IF ConvertSMOREMIN(1) THEN RETURN(1).
    IF ConvertMANFAULO(1) THEN RETURN(1).
    IF ConvertRETSTOCK(1) THEN RETURN(1).
    IF ConvertORDPARTS(1) THEN RETURN(1).
    IF ConvertLOCATION(1) THEN RETURN(1).
    IF ConvertJOBS(1) THEN RETURN(1).
    IF ConvertMODELNUM(1) THEN RETURN(1).
    IF ConvertSTATUS(1) THEN RETURN(1).
    IF ConvertTRADEACC(1) THEN RETURN(1).
    IF ConvertSUBTRACC(1) THEN RETURN(1).
    IF ConvertMANUFACT(1) THEN RETURN(1).
    IF ConvertXMLDEFS(1) THEN RETURN(1).
    IF ConvertXMLSB(1) THEN RETURN(1).
    IF ConvertMPXDEFS(1) THEN RETURN(1).
    IF ConvertXMLSAM(1) THEN RETURN(1).
  END
  RETURN(0)
!---------------------------------------------------------------------

ConvertCARISMA PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = CARISMAVer3{PROP:Name}
  MOD:stFileName:CARISMAVer1 = 'CARISMA.DAT'
  FileQueue.FileLabel &= CARISMAVer1
  FileQueue.FileName &= MOD:stFileName:CARISMAVer1
  ADD(FileQueue)
  MOD:stFileName:CARISMAVer2 = 'CARISMA.DAT'
  FileQueue.FileLabel &= CARISMAVer2
  FileQueue.FileName &= MOD:stFileName:CARISMAVer2
  ADD(FileQueue)
  MOD:stFileName:CARISMAVer3 = 'CARISMA.DAT'
  FileQueue.FileLabel &= CARISMAVer3
  FileQueue.FileName &= MOD:stFileName:CARISMAVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'CARISMA'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFEMAIL PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFEMAILVer4{PROP:Name}
  MOD:stFileName:DEFEMAILVer1 = 'DEFEMAIL.DAT'
  FileQueue.FileLabel &= DEFEMAILVer1
  FileQueue.FileName &= MOD:stFileName:DEFEMAILVer1
  ADD(FileQueue)
  MOD:stFileName:DEFEMAILVer2 = 'DEFEMAIL.DAT'
  FileQueue.FileLabel &= DEFEMAILVer2
  FileQueue.FileName &= MOD:stFileName:DEFEMAILVer2
  ADD(FileQueue)
  MOD:stFileName:DEFEMAILVer3 = 'DEFEMAIL.DAT'
  FileQueue.FileLabel &= DEFEMAILVer3
  FileQueue.FileName &= MOD:stFileName:DEFEMAILVer3
  ADD(FileQueue)
  MOD:stFileName:DEFEMAILVer4 = 'DEFEMAIL.DAT'
  FileQueue.FileLabel &= DEFEMAILVer4
  FileQueue.FileName &= MOD:stFileName:DEFEMAILVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFEMAIL'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertREGIONS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = REGIONSVer3{PROP:Name}
  MOD:stFileName:REGIONSVer1 = 'REGIONS.DAT'
  FileQueue.FileLabel &= REGIONSVer1
  FileQueue.FileName &= MOD:stFileName:REGIONSVer1
  ADD(FileQueue)
  MOD:stFileName:REGIONSVer2 = 'REGIONS.DAT'
  FileQueue.FileLabel &= REGIONSVer2
  FileQueue.FileName &= MOD:stFileName:REGIONSVer2
  ADD(FileQueue)
  MOD:stFileName:REGIONSVer3 = 'REGIONS.DAT'
  FileQueue.FileLabel &= REGIONSVer3
  FileQueue.FileName &= MOD:stFileName:REGIONSVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'REGIONS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertACCREG PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = ACCREGVer3{PROP:Name}
  MOD:stFileName:ACCREGVer1 = 'ACCREG.DAT'
  FileQueue.FileLabel &= ACCREGVer1
  FileQueue.FileName &= MOD:stFileName:ACCREGVer1
  ADD(FileQueue)
  MOD:stFileName:ACCREGVer2 = 'ACCREG.DAT'
  FileQueue.FileLabel &= ACCREGVer2
  FileQueue.FileName &= MOD:stFileName:ACCREGVer2
  ADD(FileQueue)
  MOD:stFileName:ACCREGVer3 = 'ACCREG.DAT'
  FileQueue.FileLabel &= ACCREGVer3
  FileQueue.FileName &= MOD:stFileName:ACCREGVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'ACCREG'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDACREG PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDACREGVer2{PROP:Name}
  MOD:stFileName:SIDACREGVer1 = 'SIDACREG.DAT'
  FileQueue.FileLabel &= SIDACREGVer1
  FileQueue.FileName &= MOD:stFileName:SIDACREGVer1
  ADD(FileQueue)
  MOD:stFileName:SIDACREGVer2 = 'SIDACREG.DAT'
  FileQueue.FileLabel &= SIDACREGVer2
  FileQueue.FileName &= MOD:stFileName:SIDACREGVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDACREG'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertXMLJOBS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = XMLJOBSVer17{PROP:Name}
  MOD:stFileName:XMLJOBSVer1 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer1
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer1
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer2 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer2
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer2
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer3 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer3
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer3
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer4 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer4
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer4
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer5 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer5
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer5
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer6 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer6
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer6
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer7 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer7
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer7
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer8 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer8
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer8
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer9 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer9
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer9
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer10 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer10
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer10
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer11 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer11
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer11
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer12 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer12
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer12
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer13 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer13
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer13
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer14 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer14
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer14
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer15 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer15
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer15
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer16 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer16
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer16
  ADD(FileQueue)
  MOD:stFileName:XMLJOBSVer17 = 'XMLJOBS.DAT'
  FileQueue.FileLabel &= XMLJOBSVer17
  FileQueue.FileName &= MOD:stFileName:XMLJOBSVer17
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'XMLJOBS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMULDESP PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MULDESPVer2{PROP:Name}
  MOD:stFileName:MULDESPVer1 = 'MULDESP.DAT'
  FileQueue.FileLabel &= MULDESPVer1
  FileQueue.FileName &= MOD:stFileName:MULDESPVer1
  ADD(FileQueue)
  MOD:stFileName:MULDESPVer2 = 'MULDESP.DAT'
  FileQueue.FileLabel &= MULDESPVer2
  FileQueue.FileName &= MOD:stFileName:MULDESPVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MULDESP'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertIMEISHIP PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = IMEISHIPVer4{PROP:Name}
  MOD:stFileName:IMEISHIPVer1 = 'IMEISHIP.DAT'
  FileQueue.FileLabel &= IMEISHIPVer1
  FileQueue.FileName &= MOD:stFileName:IMEISHIPVer1
  ADD(FileQueue)
  MOD:stFileName:IMEISHIPVer2 = 'IMEISHIP.DAT'
  FileQueue.FileLabel &= IMEISHIPVer2
  FileQueue.FileName &= MOD:stFileName:IMEISHIPVer2
  ADD(FileQueue)
  MOD:stFileName:IMEISHIPVer3 = 'IMEISHIP.DAT'
  FileQueue.FileLabel &= IMEISHIPVer3
  FileQueue.FileName &= MOD:stFileName:IMEISHIPVer3
  ADD(FileQueue)
  MOD:stFileName:IMEISHIPVer4 = 'IMEISHIP.DAT'
  FileQueue.FileLabel &= IMEISHIPVer4
  FileQueue.FileName &= MOD:stFileName:IMEISHIPVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'IMEISHIP'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertJOBTHIRD PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = JOBTHIRDVer2{PROP:Name}
  MOD:stFileName:JOBTHIRDVer1 = 'JOBTHIRD.DAT'
  FileQueue.FileLabel &= JOBTHIRDVer1
  FileQueue.FileName &= MOD:stFileName:JOBTHIRDVer1
  ADD(FileQueue)
  MOD:stFileName:JOBTHIRDVer2 = 'JOBTHIRD.DAT'
  FileQueue.FileLabel &= JOBTHIRDVer2
  FileQueue.FileName &= MOD:stFileName:JOBTHIRDVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'JOBTHIRD'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOGRETRN PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOGRETRNVer2{PROP:Name}
  MOD:stFileName:LOGRETRNVer1 = 'LOGRETRN.DAT'
  FileQueue.FileLabel &= LOGRETRNVer1
  FileQueue.FileName &= MOD:stFileName:LOGRETRNVer1
  ADD(FileQueue)
  MOD:stFileName:LOGRETRNVer2 = 'LOGRETRN.DAT'
  FileQueue.FileLabel &= LOGRETRNVer2
  FileQueue.FileName &= MOD:stFileName:LOGRETRNVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOGRETRN'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertPRODCODE PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = PRODCODEVer2{PROP:Name}
  MOD:stFileName:PRODCODEVer1 = 'PRODCODE.DAT'
  FileQueue.FileLabel &= PRODCODEVer1
  FileQueue.FileName &= MOD:stFileName:PRODCODEVer1
  ADD(FileQueue)
  MOD:stFileName:PRODCODEVer2 = 'PRODCODE.DAT'
  FileQueue.FileLabel &= PRODCODEVer2
  FileQueue.FileName &= MOD:stFileName:PRODCODEVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'PRODCODE'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOGSTOCK PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOGSTOCKVer2{PROP:Name}
  MOD:stFileName:LOGSTOCKVer1 = 'LOGSTOCK.DAT'
  FileQueue.FileLabel &= LOGSTOCKVer1
  FileQueue.FileName &= MOD:stFileName:LOGSTOCKVer1
  ADD(FileQueue)
  MOD:stFileName:LOGSTOCKVer2 = 'LOGSTOCK.DAT'
  FileQueue.FileLabel &= LOGSTOCKVer2
  FileQueue.FileName &= MOD:stFileName:LOGSTOCKVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOGSTOCK'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFEMAB2 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFEMAB2Ver2{PROP:Name}
  MOD:stFileName:DEFEMAB2Ver1 = 'DEFEMAB2.DAT'
  FileQueue.FileLabel &= DEFEMAB2Ver1
  FileQueue.FileName &= MOD:stFileName:DEFEMAB2Ver1
  ADD(FileQueue)
  MOD:stFileName:DEFEMAB2Ver2 = 'DEFEMAB2.DAT'
  FileQueue.FileLabel &= DEFEMAB2Ver2
  FileQueue.FileName &= MOD:stFileName:DEFEMAB2Ver2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFEMAB2'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFEMAB3 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFEMAB3Ver2{PROP:Name}
  MOD:stFileName:DEFEMAB3Ver1 = 'DEFEMAB3.DAT'
  FileQueue.FileLabel &= DEFEMAB3Ver1
  FileQueue.FileName &= MOD:stFileName:DEFEMAB3Ver1
  ADD(FileQueue)
  MOD:stFileName:DEFEMAB3Ver2 = 'DEFEMAB3.DAT'
  FileQueue.FileLabel &= DEFEMAB3Ver2
  FileQueue.FileName &= MOD:stFileName:DEFEMAB3Ver2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFEMAB3'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFEMAB4 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFEMAB4Ver2{PROP:Name}
  MOD:stFileName:DEFEMAB4Ver1 = 'DEFEMAB4.DAT'
  FileQueue.FileLabel &= DEFEMAB4Ver1
  FileQueue.FileName &= MOD:stFileName:DEFEMAB4Ver1
  ADD(FileQueue)
  MOD:stFileName:DEFEMAB4Ver2 = 'DEFEMAB4.DAT'
  FileQueue.FileLabel &= DEFEMAB4Ver2
  FileQueue.FileName &= MOD:stFileName:DEFEMAB4Ver2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFEMAB4'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFEMAB5 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFEMAB5Ver2{PROP:Name}
  MOD:stFileName:DEFEMAB5Ver1 = 'DEFEMAB5.DAT'
  FileQueue.FileLabel &= DEFEMAB5Ver1
  FileQueue.FileName &= MOD:stFileName:DEFEMAB5Ver1
  ADD(FileQueue)
  MOD:stFileName:DEFEMAB5Ver2 = 'DEFEMAB5.DAT'
  FileQueue.FileLabel &= DEFEMAB5Ver2
  FileQueue.FileName &= MOD:stFileName:DEFEMAB5Ver2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFEMAB5'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFEMAE2 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFEMAE2Ver2{PROP:Name}
  MOD:stFileName:DEFEMAE2Ver1 = 'DEFEMAE2.DAT'
  FileQueue.FileLabel &= DEFEMAE2Ver1
  FileQueue.FileName &= MOD:stFileName:DEFEMAE2Ver1
  ADD(FileQueue)
  MOD:stFileName:DEFEMAE2Ver2 = 'DEFEMAE2.DAT'
  FileQueue.FileLabel &= DEFEMAE2Ver2
  FileQueue.FileName &= MOD:stFileName:DEFEMAE2Ver2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFEMAE2'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFEMAE3 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFEMAE3Ver2{PROP:Name}
  MOD:stFileName:DEFEMAE3Ver1 = 'DEFEMAE3.DAT'
  FileQueue.FileLabel &= DEFEMAE3Ver1
  FileQueue.FileName &= MOD:stFileName:DEFEMAE3Ver1
  ADD(FileQueue)
  MOD:stFileName:DEFEMAE3Ver2 = 'DEFEMAE3.DAT'
  FileQueue.FileLabel &= DEFEMAE3Ver2
  FileQueue.FileName &= MOD:stFileName:DEFEMAE3Ver2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFEMAE3'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFEMAB1 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFEMAB1Ver2{PROP:Name}
  MOD:stFileName:DEFEMAB1Ver1 = 'DEFEMAB1.DAT'
  FileQueue.FileLabel &= DEFEMAB1Ver1
  FileQueue.FileName &= MOD:stFileName:DEFEMAB1Ver1
  ADD(FileQueue)
  MOD:stFileName:DEFEMAB1Ver2 = 'DEFEMAB1.DAT'
  FileQueue.FileLabel &= DEFEMAB1Ver2
  FileQueue.FileName &= MOD:stFileName:DEFEMAB1Ver2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFEMAB1'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFEMAE1 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFEMAE1Ver6{PROP:Name}
  MOD:stFileName:DEFEMAE1Ver1 = 'DEFEMAI3.DAT'
  FileQueue.FileLabel &= DEFEMAE1Ver1
  FileQueue.FileName &= MOD:stFileName:DEFEMAE1Ver1
  ADD(FileQueue)
  MOD:stFileName:DEFEMAE1Ver2 = 'DEFEMAI3.DAT'
  FileQueue.FileLabel &= DEFEMAE1Ver2
  FileQueue.FileName &= MOD:stFileName:DEFEMAE1Ver2
  ADD(FileQueue)
  MOD:stFileName:DEFEMAE1Ver3 = 'DEFEMAI3.DAT'
  FileQueue.FileLabel &= DEFEMAE1Ver3
  FileQueue.FileName &= MOD:stFileName:DEFEMAE1Ver3
  ADD(FileQueue)
  MOD:stFileName:DEFEMAE1Ver4 = 'DEFEMAI3.DAT'
  FileQueue.FileLabel &= DEFEMAE1Ver4
  FileQueue.FileName &= MOD:stFileName:DEFEMAE1Ver4
  ADD(FileQueue)
  MOD:stFileName:DEFEMAE1Ver5 = 'DEFEMAE1.DAT'
  FileQueue.FileLabel &= DEFEMAE1Ver5
  FileQueue.FileName &= MOD:stFileName:DEFEMAE1Ver5
  ADD(FileQueue)
  MOD:stFileName:DEFEMAE1Ver6 = 'DEFEMAE1.DAT'
  FileQueue.FileLabel &= DEFEMAE1Ver6
  FileQueue.FileName &= MOD:stFileName:DEFEMAE1Ver6
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFEMAE1'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFEMAI2 PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFEMAI2Ver9{PROP:Name}
  MOD:stFileName:DEFEMAI2Ver1 = 'DEFEMAI2.DAT'
  FileQueue.FileLabel &= DEFEMAI2Ver1
  FileQueue.FileName &= MOD:stFileName:DEFEMAI2Ver1
  ADD(FileQueue)
  MOD:stFileName:DEFEMAI2Ver2 = 'DEFEMAI2.DAT'
  FileQueue.FileLabel &= DEFEMAI2Ver2
  FileQueue.FileName &= MOD:stFileName:DEFEMAI2Ver2
  ADD(FileQueue)
  MOD:stFileName:DEFEMAI2Ver3 = 'DEFEMAI2.DAT'
  FileQueue.FileLabel &= DEFEMAI2Ver3
  FileQueue.FileName &= MOD:stFileName:DEFEMAI2Ver3
  ADD(FileQueue)
  MOD:stFileName:DEFEMAI2Ver4 = 'DEFEMAI2.DAT'
  FileQueue.FileLabel &= DEFEMAI2Ver4
  FileQueue.FileName &= MOD:stFileName:DEFEMAI2Ver4
  ADD(FileQueue)
  MOD:stFileName:DEFEMAI2Ver5 = 'DEFEMAI2.DAT'
  FileQueue.FileLabel &= DEFEMAI2Ver5
  FileQueue.FileName &= MOD:stFileName:DEFEMAI2Ver5
  ADD(FileQueue)
  MOD:stFileName:DEFEMAI2Ver6 = 'DEFEMAI2.DAT'
  FileQueue.FileLabel &= DEFEMAI2Ver6
  FileQueue.FileName &= MOD:stFileName:DEFEMAI2Ver6
  ADD(FileQueue)
  MOD:stFileName:DEFEMAI2Ver7 = 'DEFEMAI2.DAT'
  FileQueue.FileLabel &= DEFEMAI2Ver7
  FileQueue.FileName &= MOD:stFileName:DEFEMAI2Ver7
  ADD(FileQueue)
  MOD:stFileName:DEFEMAI2Ver8 = 'DEFEMAI2.DAT'
  FileQueue.FileLabel &= DEFEMAI2Ver8
  FileQueue.FileName &= MOD:stFileName:DEFEMAI2Ver8
  ADD(FileQueue)
  MOD:stFileName:DEFEMAI2Ver9 = 'DEFEMAI2.DAT'
  FileQueue.FileLabel &= DEFEMAI2Ver9
  FileQueue.FileName &= MOD:stFileName:DEFEMAI2Ver9
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFEMAI2'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFSIDEX PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFSIDEXVer3{PROP:Name}
  MOD:stFileName:DEFSIDEXVer1 = 'DEFSIDEX.DAT'
  FileQueue.FileLabel &= DEFSIDEXVer1
  FileQueue.FileName &= MOD:stFileName:DEFSIDEXVer1
  ADD(FileQueue)
  MOD:stFileName:DEFSIDEXVer2 = 'DEFSIDEX.DAT'
  FileQueue.FileLabel &= DEFSIDEXVer2
  FileQueue.FileName &= MOD:stFileName:DEFSIDEXVer2
  ADD(FileQueue)
  MOD:stFileName:DEFSIDEXVer3 = 'DEFSIDEX.DAT'
  FileQueue.FileLabel &= DEFSIDEXVer3
  FileQueue.FileName &= MOD:stFileName:DEFSIDEXVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFSIDEX'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertRETSALES PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = RETSALESVer3{PROP:Name}
  MOD:stFileName:RETSALESVer1 = 'RETSALES.DAT'
  FileQueue.FileLabel &= RETSALESVer1
  FileQueue.FileName &= MOD:stFileName:RETSALESVer1
  ADD(FileQueue)
  MOD:stFileName:RETSALESVer2 = 'RETSALES.DAT'
  FileQueue.FileLabel &= RETSALESVer2
  FileQueue.FileName &= MOD:stFileName:RETSALESVer2
  ADD(FileQueue)
  MOD:stFileName:RETSALESVer3 = 'RETSALES.DAT'
  FileQueue.FileLabel &= RETSALESVer3
  FileQueue.FileName &= MOD:stFileName:RETSALESVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'RETSALES'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertTEAMS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = TEAMSVer2{PROP:Name}
  MOD:stFileName:TEAMSVer1 = 'TEAMS.DAT'
  FileQueue.FileLabel &= TEAMSVer1
  FileQueue.FileName &= MOD:stFileName:TEAMSVer1
  ADD(FileQueue)
  MOD:stFileName:TEAMSVer2 = 'TEAMS.DAT'
  FileQueue.FileLabel &= TEAMSVer2
  FileQueue.FileName &= MOD:stFileName:TEAMSVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'TEAMS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMERGE PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MERGEVer2{PROP:Name}
  MOD:stFileName:MERGEVer1 = 'MERGE.DAT'
  FileQueue.FileLabel &= MERGEVer1
  FileQueue.FileName &= MOD:stFileName:MERGEVer1
  ADD(FileQueue)
  MOD:stFileName:MERGEVer2 = 'MERGE.DAT'
  FileQueue.FileLabel &= MERGEVer2
  FileQueue.FileName &= MOD:stFileName:MERGEVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MERGE'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDDEF PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDDEFVer6{PROP:Name}
  MOD:stFileName:SIDDEFVer1 = 'SIDDEF'
  FileQueue.FileLabel &= SIDDEFVer1
  FileQueue.FileName &= MOD:stFileName:SIDDEFVer1
  ADD(FileQueue)
  MOD:stFileName:SIDDEFVer2 = 'SIDDEF'
  FileQueue.FileLabel &= SIDDEFVer2
  FileQueue.FileName &= MOD:stFileName:SIDDEFVer2
  ADD(FileQueue)
  MOD:stFileName:SIDDEFVer3 = 'SIDDEF'
  FileQueue.FileLabel &= SIDDEFVer3
  FileQueue.FileName &= MOD:stFileName:SIDDEFVer3
  ADD(FileQueue)
  MOD:stFileName:SIDDEFVer4 = 'SIDDEF'
  FileQueue.FileLabel &= SIDDEFVer4
  FileQueue.FileName &= MOD:stFileName:SIDDEFVer4
  ADD(FileQueue)
  MOD:stFileName:SIDDEFVer5 = 'SIDDEF'
  FileQueue.FileLabel &= SIDDEFVer5
  FileQueue.FileName &= MOD:stFileName:SIDDEFVer5
  ADD(FileQueue)
  MOD:stFileName:SIDDEFVer6 = 'SIDDEF'
  FileQueue.FileLabel &= SIDDEFVer6
  FileQueue.FileName &= MOD:stFileName:SIDDEFVer6
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDDEF'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertORDPEND PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = ORDPENDVer4{PROP:Name}
  MOD:stFileName:ORDPENDVer1 = 'ORDPEND.DAT'
  FileQueue.FileLabel &= ORDPENDVer1
  FileQueue.FileName &= MOD:stFileName:ORDPENDVer1
  ADD(FileQueue)
  MOD:stFileName:ORDPENDVer2 = 'ORDPEND.DAT'
  FileQueue.FileLabel &= ORDPENDVer2
  FileQueue.FileName &= MOD:stFileName:ORDPENDVer2
  ADD(FileQueue)
  MOD:stFileName:ORDPENDVer3 = 'ORDPEND.DAT'
  FileQueue.FileLabel &= ORDPENDVer3
  FileQueue.FileName &= MOD:stFileName:ORDPENDVer3
  ADD(FileQueue)
  MOD:stFileName:ORDPENDVer4 = 'ORDPEND.DAT'
  FileQueue.FileLabel &= ORDPENDVer4
  FileQueue.FileName &= MOD:stFileName:ORDPENDVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'ORDPEND'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSTOHIST PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = STOHISTVer2{PROP:Name}
  MOD:stFileName:STOHISTVer1 = 'STOHIST.DAT'
  FileQueue.FileLabel &= STOHISTVer1
  FileQueue.FileName &= MOD:stFileName:STOHISTVer1
  ADD(FileQueue)
  MOD:stFileName:STOHISTVer2 = 'STOHIST.DAT'
  FileQueue.FileLabel &= STOHISTVer2
  FileQueue.FileName &= MOD:stFileName:STOHISTVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'STOHIST'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertEXREASON PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = EXREASONVer2{PROP:Name}
  MOD:stFileName:EXREASONVer1 = 'EXREASON.DAT'
  FileQueue.FileLabel &= EXREASONVer1
  FileQueue.FileName &= MOD:stFileName:EXREASONVer1
  ADD(FileQueue)
  MOD:stFileName:EXREASONVer2 = 'EXREASON.DAT'
  FileQueue.FileLabel &= EXREASONVer2
  FileQueue.FileName &= MOD:stFileName:EXREASONVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'EXREASON'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertREPTYDEF PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = REPTYDEFVer2{PROP:Name}
  MOD:stFileName:REPTYDEFVer1 = 'REPTYDEF.DAT'
  FileQueue.FileLabel &= REPTYDEFVer1
  FileQueue.FileName &= MOD:stFileName:REPTYDEFVer1
  ADD(FileQueue)
  MOD:stFileName:REPTYDEFVer2 = 'REPTYDEF.DAT'
  FileQueue.FileLabel &= REPTYDEFVer2
  FileQueue.FileName &= MOD:stFileName:REPTYDEFVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'REPTYDEF'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertJOBSE PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = JOBSEVer10{PROP:Name}
  MOD:stFileName:JOBSEVer1 = 'JOBSE.DAT'
  FileQueue.FileLabel &= JOBSEVer1
  FileQueue.FileName &= MOD:stFileName:JOBSEVer1
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer2 = 'JOBSE.DAT'
  FileQueue.FileLabel &= JOBSEVer2
  FileQueue.FileName &= MOD:stFileName:JOBSEVer2
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer3 = 'JOBSE.DAT'
  FileQueue.FileLabel &= JOBSEVer3
  FileQueue.FileName &= MOD:stFileName:JOBSEVer3
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer4 = 'JOBSE.DAT'
  FileQueue.FileLabel &= JOBSEVer4
  FileQueue.FileName &= MOD:stFileName:JOBSEVer4
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer5 = 'JOBSE.DAT'
  FileQueue.FileLabel &= JOBSEVer5
  FileQueue.FileName &= MOD:stFileName:JOBSEVer5
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer6 = 'JOBSE.DAT'
  FileQueue.FileLabel &= JOBSEVer6
  FileQueue.FileName &= MOD:stFileName:JOBSEVer6
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer7 = 'JOBSE.DAT'
  FileQueue.FileLabel &= JOBSEVer7
  FileQueue.FileName &= MOD:stFileName:JOBSEVer7
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer8 = 'JOBSE.DAT'
  FileQueue.FileLabel &= JOBSEVer8
  FileQueue.FileName &= MOD:stFileName:JOBSEVer8
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer9 = 'JOBSE.DAT'
  FileQueue.FileLabel &= JOBSEVer9
  FileQueue.FileName &= MOD:stFileName:JOBSEVer9
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer10 = 'JOBSE.DAT'
  FileQueue.FileLabel &= JOBSEVer10
  FileQueue.FileName &= MOD:stFileName:JOBSEVer10
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'JOBSE'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMANFAULT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MANFAULTVer4{PROP:Name}
  MOD:stFileName:MANFAULTVer1 = 'MANFAULT.DAT'
  FileQueue.FileLabel &= MANFAULTVer1
  FileQueue.FileName &= MOD:stFileName:MANFAULTVer1
  ADD(FileQueue)
  MOD:stFileName:MANFAULTVer2 = 'MANFAULT.DAT'
  FileQueue.FileLabel &= MANFAULTVer2
  FileQueue.FileName &= MOD:stFileName:MANFAULTVer2
  ADD(FileQueue)
  MOD:stFileName:MANFAULTVer3 = 'MANFAULT.DAT'
  FileQueue.FileLabel &= MANFAULTVer3
  FileQueue.FileName &= MOD:stFileName:MANFAULTVer3
  ADD(FileQueue)
  MOD:stFileName:MANFAULTVer4 = 'MANFAULT.DAT'
  FileQueue.FileLabel &= MANFAULTVer4
  FileQueue.FileName &= MOD:stFileName:MANFAULTVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MANFAULT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertPICKNOTE PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = PICKNOTEVer5{PROP:Name}
  MOD:stFileName:PICKNOTEVer1 = 'PICKNOTE'
  FileQueue.FileLabel &= PICKNOTEVer1
  FileQueue.FileName &= MOD:stFileName:PICKNOTEVer1
  ADD(FileQueue)
  MOD:stFileName:PICKNOTEVer2 = 'PICKNOTE'
  FileQueue.FileLabel &= PICKNOTEVer2
  FileQueue.FileName &= MOD:stFileName:PICKNOTEVer2
  ADD(FileQueue)
  MOD:stFileName:PICKNOTEVer3 = 'PICKNOTE'
  FileQueue.FileLabel &= PICKNOTEVer3
  FileQueue.FileName &= MOD:stFileName:PICKNOTEVer3
  ADD(FileQueue)
  MOD:stFileName:PICKNOTEVer4 = 'PICKNOTE'
  FileQueue.FileLabel &= PICKNOTEVer4
  FileQueue.FileName &= MOD:stFileName:PICKNOTEVer4
  ADD(FileQueue)
  MOD:stFileName:PICKNOTEVer5 = 'PICKNOTE'
  FileQueue.FileLabel &= PICKNOTEVer5
  FileQueue.FileName &= MOD:stFileName:PICKNOTEVer5
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'PICKNOTE'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertCOURIER PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = COURIERVer2{PROP:Name}
  MOD:stFileName:COURIERVer1 = 'COURIER.DAT'
  FileQueue.FileLabel &= COURIERVer1
  FileQueue.FileName &= MOD:stFileName:COURIERVer1
  ADD(FileQueue)
  MOD:stFileName:COURIERVer2 = 'COURIER.DAT'
  FileQueue.FileLabel &= COURIERVer2
  FileQueue.FileName &= MOD:stFileName:COURIERVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'COURIER'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertESNMODEL PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = ESNMODELVer2{PROP:Name}
  MOD:stFileName:ESNMODELVer1 = 'ESNMODEL.DAT'
  FileQueue.FileLabel &= ESNMODELVer1
  FileQueue.FileName &= MOD:stFileName:ESNMODELVer1
  ADD(FileQueue)
  MOD:stFileName:ESNMODELVer2 = 'ESNMODEL.DAT'
  FileQueue.FileLabel &= ESNMODELVer2
  FileQueue.FileName &= MOD:stFileName:ESNMODELVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'ESNMODEL'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSTOCK PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = STOCKVer4{PROP:Name}
  MOD:stFileName:STOCKVer1 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer1
  FileQueue.FileName &= MOD:stFileName:STOCKVer1
  ADD(FileQueue)
  MOD:stFileName:STOCKVer2 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer2
  FileQueue.FileName &= MOD:stFileName:STOCKVer2
  ADD(FileQueue)
  MOD:stFileName:STOCKVer3 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer3
  FileQueue.FileName &= MOD:stFileName:STOCKVer3
  ADD(FileQueue)
  MOD:stFileName:STOCKVer4 = 'STOCK.DAT'
  FileQueue.FileLabel &= STOCKVer4
  FileQueue.FileName &= MOD:stFileName:STOCKVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'STOCK'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOGSTOLC PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOGSTOLCVer2{PROP:Name}
  MOD:stFileName:LOGSTOLCVer1 = 'LOGSTOLC.DAT'
  FileQueue.FileLabel &= LOGSTOLCVer1
  FileQueue.FileName &= MOD:stFileName:LOGSTOLCVer1
  ADD(FileQueue)
  MOD:stFileName:LOGSTOLCVer2 = 'LOGSTOLC.DAT'
  FileQueue.FileLabel &= LOGSTOLCVer2
  FileQueue.FileName &= MOD:stFileName:LOGSTOLCVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOGSTOLC'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOGSERST PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOGSERSTVer2{PROP:Name}
  MOD:stFileName:LOGSERSTVer1 = 'LOGSERST.DAT'
  FileQueue.FileLabel &= LOGSERSTVer1
  FileQueue.FileName &= MOD:stFileName:LOGSERSTVer1
  ADD(FileQueue)
  MOD:stFileName:LOGSERSTVer2 = 'LOGSERST.DAT'
  FileQueue.FileLabel &= LOGSERSTVer2
  FileQueue.FileName &= MOD:stFileName:LOGSERSTVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOGSERST'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOGSTLOC PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOGSTLOCVer2{PROP:Name}
  MOD:stFileName:LOGSTLOCVer1 = 'LOGSTLOC.DAT'
  FileQueue.FileLabel &= LOGSTLOCVer1
  FileQueue.FileName &= MOD:stFileName:LOGSTLOCVer1
  ADD(FileQueue)
  MOD:stFileName:LOGSTLOCVer2 = 'LOGSTLOC.DAT'
  FileQueue.FileLabel &= LOGSTLOCVer2
  FileQueue.FileName &= MOD:stFileName:LOGSTLOCVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOGSTLOC'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertTRAFAULO PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = TRAFAULOVer2{PROP:Name}
  MOD:stFileName:TRAFAULOVer1 = 'TRAFAULO.DAT'
  FileQueue.FileLabel &= TRAFAULOVer1
  FileQueue.FileName &= MOD:stFileName:TRAFAULOVer1
  ADD(FileQueue)
  MOD:stFileName:TRAFAULOVer2 = 'TRAFAULO.DAT'
  FileQueue.FileLabel &= TRAFAULOVer2
  FileQueue.FileName &= MOD:stFileName:TRAFAULOVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'TRAFAULO'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMPXJOBS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MPXJOBSVer6{PROP:Name}
  MOD:stFileName:MPXJOBSVer1 = 'MPXJOBS.DAT'
  FileQueue.FileLabel &= MPXJOBSVer1
  FileQueue.FileName &= MOD:stFileName:MPXJOBSVer1
  ADD(FileQueue)
  MOD:stFileName:MPXJOBSVer2 = 'MPXJOBS.DAT'
  FileQueue.FileLabel &= MPXJOBSVer2
  FileQueue.FileName &= MOD:stFileName:MPXJOBSVer2
  ADD(FileQueue)
  MOD:stFileName:MPXJOBSVer3 = 'MPXJOBS.DAT'
  FileQueue.FileLabel &= MPXJOBSVer3
  FileQueue.FileName &= MOD:stFileName:MPXJOBSVer3
  ADD(FileQueue)
  MOD:stFileName:MPXJOBSVer4 = 'MPXJOBS.DAT'
  FileQueue.FileLabel &= MPXJOBSVer4
  FileQueue.FileName &= MOD:stFileName:MPXJOBSVer4
  ADD(FileQueue)
  MOD:stFileName:MPXJOBSVer5 = 'MPXJOBS.DAT'
  FileQueue.FileLabel &= MPXJOBSVer5
  FileQueue.FileName &= MOD:stFileName:MPXJOBSVer5
  ADD(FileQueue)
  MOD:stFileName:MPXJOBSVer6 = 'MPXJOBS.DAT'
  FileQueue.FileLabel &= MPXJOBSVer6
  FileQueue.FileName &= MOD:stFileName:MPXJOBSVer6
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MPXJOBS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMPXSTAT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MPXSTATVer3{PROP:Name}
  MOD:stFileName:MPXSTATVer1 = 'MPXSTAT.DAT'
  FileQueue.FileLabel &= MPXSTATVer1
  FileQueue.FileName &= MOD:stFileName:MPXSTATVer1
  ADD(FileQueue)
  MOD:stFileName:MPXSTATVer2 = 'MPXSTAT.DAT'
  FileQueue.FileLabel &= MPXSTATVer2
  FileQueue.FileName &= MOD:stFileName:MPXSTATVer2
  ADD(FileQueue)
  MOD:stFileName:MPXSTATVer3 = 'MPXSTAT.DAT'
  FileQueue.FileLabel &= MPXSTATVer3
  FileQueue.FileName &= MOD:stFileName:MPXSTATVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MPXSTAT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertTRAFAULT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = TRAFAULTVer3{PROP:Name}
  MOD:stFileName:TRAFAULTVer1 = 'TRAFAULT.DAT'
  FileQueue.FileLabel &= TRAFAULTVer1
  FileQueue.FileName &= MOD:stFileName:TRAFAULTVer1
  ADD(FileQueue)
  MOD:stFileName:TRAFAULTVer2 = 'TRAFAULT.DAT'
  FileQueue.FileLabel &= TRAFAULTVer2
  FileQueue.FileName &= MOD:stFileName:TRAFAULTVer2
  ADD(FileQueue)
  MOD:stFileName:TRAFAULTVer3 = 'TRAFAULT.DAT'
  FileQueue.FileLabel &= TRAFAULTVer3
  FileQueue.FileName &= MOD:stFileName:TRAFAULTVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'TRAFAULT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSTATCRIT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = STATCRITVer3{PROP:Name}
  MOD:stFileName:STATCRITVer1 = 'STATCRIT.DAT'
  FileQueue.FileLabel &= STATCRITVer1
  FileQueue.FileName &= MOD:stFileName:STATCRITVer1
  ADD(FileQueue)
  MOD:stFileName:STATCRITVer2 = 'STATCRIT.DAT'
  FileQueue.FileLabel &= STATCRITVer2
  FileQueue.FileName &= MOD:stFileName:STATCRITVer2
  ADD(FileQueue)
  MOD:stFileName:STATCRITVer3 = 'STATCRIT.DAT'
  FileQueue.FileLabel &= STATCRITVer3
  FileQueue.FileName &= MOD:stFileName:STATCRITVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'STATCRIT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertNOTESDEL PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = NOTESDELVer2{PROP:Name}
  MOD:stFileName:NOTESDELVer1 = 'NOTESDEL'
  FileQueue.FileLabel &= NOTESDELVer1
  FileQueue.FileName &= MOD:stFileName:NOTESDELVer1
  ADD(FileQueue)
  MOD:stFileName:NOTESDELVer2 = 'NOTESDEL'
  FileQueue.FileLabel &= NOTESDELVer2
  FileQueue.FileName &= MOD:stFileName:NOTESDELVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'NOTESDEL'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertPICKDET PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = PICKDETVer6{PROP:Name}
  MOD:stFileName:PICKDETVer1 = 'PICKDET'
  FileQueue.FileLabel &= PICKDETVer1
  FileQueue.FileName &= MOD:stFileName:PICKDETVer1
  ADD(FileQueue)
  MOD:stFileName:PICKDETVer2 = 'PICKDET'
  FileQueue.FileLabel &= PICKDETVer2
  FileQueue.FileName &= MOD:stFileName:PICKDETVer2
  ADD(FileQueue)
  MOD:stFileName:PICKDETVer3 = 'PICKDET'
  FileQueue.FileLabel &= PICKDETVer3
  FileQueue.FileName &= MOD:stFileName:PICKDETVer3
  ADD(FileQueue)
  MOD:stFileName:PICKDETVer4 = 'PICKDET'
  FileQueue.FileLabel &= PICKDETVer4
  FileQueue.FileName &= MOD:stFileName:PICKDETVer4
  ADD(FileQueue)
  MOD:stFileName:PICKDETVer5 = 'PICKDET'
  FileQueue.FileLabel &= PICKDETVer5
  FileQueue.FileName &= MOD:stFileName:PICKDETVer5
  ADD(FileQueue)
  MOD:stFileName:PICKDETVer6 = 'PICKDET'
  FileQueue.FileLabel &= PICKDETVer6
  FileQueue.FileName &= MOD:stFileName:PICKDETVer6
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'PICKDET'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertUSERS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = USERSVer2{PROP:Name}
  MOD:stFileName:USERSVer1 = 'USERS.DAT'
  FileQueue.FileLabel &= USERSVer1
  FileQueue.FileName &= MOD:stFileName:USERSVer1
  ADD(FileQueue)
  MOD:stFileName:USERSVer2 = 'USERS.DAT'
  FileQueue.FileLabel &= USERSVer2
  FileQueue.FileName &= MOD:stFileName:USERSVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'USERS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDREG PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDREGVer2{PROP:Name}
  MOD:stFileName:SIDREGVer1 = 'SIDREG.DAT'
  FileQueue.FileLabel &= SIDREGVer1
  FileQueue.FileName &= MOD:stFileName:SIDREGVer1
  ADD(FileQueue)
  MOD:stFileName:SIDREGVer2 = 'SIDREG.DAT'
  FileQueue.FileLabel &= SIDREGVer2
  FileQueue.FileName &= MOD:stFileName:SIDREGVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDREG'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertPENDMAIL PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = PENDMAILVer14{PROP:Name}
  MOD:stFileName:PENDMAILVer1 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer1
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer1
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer2 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer2
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer2
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer3 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer3
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer3
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer4 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer4
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer4
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer5 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer5
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer5
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer6 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer6
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer6
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer7 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer7
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer7
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer8 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer8
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer8
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer9 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer9
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer9
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer10 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer10
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer10
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer11 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer11
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer11
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer12 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer12
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer12
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer13 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer13
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer13
  ADD(FileQueue)
  MOD:stFileName:PENDMAILVer14 = 'PENDMAIL.DAT'
  FileQueue.FileLabel &= PENDMAILVer14
  FileQueue.FileName &= MOD:stFileName:PENDMAILVer14
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'PENDMAIL'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertDEFAULTS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = DEFAULTSVer7{PROP:Name}
  MOD:stFileName:DEFAULTSVer1 = 'DEFAULTS.DAT'
  FileQueue.FileLabel &= DEFAULTSVer1
  FileQueue.FileName &= MOD:stFileName:DEFAULTSVer1
  ADD(FileQueue)
  MOD:stFileName:DEFAULTSVer2 = 'DEFAULTS.DAT'
  FileQueue.FileLabel &= DEFAULTSVer2
  FileQueue.FileName &= MOD:stFileName:DEFAULTSVer2
  ADD(FileQueue)
  MOD:stFileName:DEFAULTSVer3 = 'DEFAULTS.DAT'
  FileQueue.FileLabel &= DEFAULTSVer3
  FileQueue.FileName &= MOD:stFileName:DEFAULTSVer3
  ADD(FileQueue)
  MOD:stFileName:DEFAULTSVer4 = 'DEFAULTS.DAT'
  FileQueue.FileLabel &= DEFAULTSVer4
  FileQueue.FileName &= MOD:stFileName:DEFAULTSVer4
  ADD(FileQueue)
  MOD:stFileName:DEFAULTSVer5 = 'DEFAULTS.DAT'
  FileQueue.FileLabel &= DEFAULTSVer5
  FileQueue.FileName &= MOD:stFileName:DEFAULTSVer5
  ADD(FileQueue)
  MOD:stFileName:DEFAULTSVer6 = 'DEFAULTS.DAT'
  FileQueue.FileLabel &= DEFAULTSVer6
  FileQueue.FileName &= MOD:stFileName:DEFAULTSVer6
  ADD(FileQueue)
  MOD:stFileName:DEFAULTSVer7 = 'DEFAULTS.DAT'
  FileQueue.FileLabel &= DEFAULTSVer7
  FileQueue.FileName &= MOD:stFileName:DEFAULTSVer7
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'DEFAULTS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMANFPALO PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MANFPALOVer2{PROP:Name}
  MOD:stFileName:MANFPALOVer1 = 'MANFPALO.DAT'
  FileQueue.FileLabel &= MANFPALOVer1
  FileQueue.FileName &= MOD:stFileName:MANFPALOVer1
  ADD(FileQueue)
  MOD:stFileName:MANFPALOVer2 = 'MANFPALO.DAT'
  FileQueue.FileLabel &= MANFPALOVer2
  FileQueue.FileName &= MOD:stFileName:MANFPALOVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MANFPALO'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMANFAUPA PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MANFAUPAVer2{PROP:Name}
  MOD:stFileName:MANFAUPAVer1 = 'MANFAUPA.DAT'
  FileQueue.FileLabel &= MANFAUPAVer1
  FileQueue.FileName &= MOD:stFileName:MANFAUPAVer1
  ADD(FileQueue)
  MOD:stFileName:MANFAUPAVer2 = 'MANFAUPA.DAT'
  FileQueue.FileLabel &= MANFAUPAVer2
  FileQueue.FileName &= MOD:stFileName:MANFAUPAVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MANFAUPA'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDALERT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDALERTVer7{PROP:Name}
  MOD:stFileName:SIDALERTVer1 = 'SIDALERT.DAT'
  FileQueue.FileLabel &= SIDALERTVer1
  FileQueue.FileName &= MOD:stFileName:SIDALERTVer1
  ADD(FileQueue)
  MOD:stFileName:SIDALERTVer2 = 'SIDALERT.DAT'
  FileQueue.FileLabel &= SIDALERTVer2
  FileQueue.FileName &= MOD:stFileName:SIDALERTVer2
  ADD(FileQueue)
  MOD:stFileName:SIDALERTVer3 = 'SIDALERT.DAT'
  FileQueue.FileLabel &= SIDALERTVer3
  FileQueue.FileName &= MOD:stFileName:SIDALERTVer3
  ADD(FileQueue)
  MOD:stFileName:SIDALERTVer4 = 'SIDALERT.DAT'
  FileQueue.FileLabel &= SIDALERTVer4
  FileQueue.FileName &= MOD:stFileName:SIDALERTVer4
  ADD(FileQueue)
  MOD:stFileName:SIDALERTVer5 = 'SIDALERT.DAT'
  FileQueue.FileLabel &= SIDALERTVer5
  FileQueue.FileName &= MOD:stFileName:SIDALERTVer5
  ADD(FileQueue)
  MOD:stFileName:SIDALERTVer6 = 'SIDALERT.DAT'
  FileQueue.FileLabel &= SIDALERTVer6
  FileQueue.FileName &= MOD:stFileName:SIDALERTVer6
  ADD(FileQueue)
  MOD:stFileName:SIDALERTVer7 = 'SIDALERT.DAT'
  FileQueue.FileLabel &= SIDALERTVer7
  FileQueue.FileName &= MOD:stFileName:SIDALERTVer7
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDALERT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDALDEF PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDALDEFVer5{PROP:Name}
  MOD:stFileName:SIDALDEFVer1 = 'SIDALDEF.DAT'
  FileQueue.FileLabel &= SIDALDEFVer1
  FileQueue.FileName &= MOD:stFileName:SIDALDEFVer1
  ADD(FileQueue)
  MOD:stFileName:SIDALDEFVer2 = 'SIDALDEF.DAT'
  FileQueue.FileLabel &= SIDALDEFVer2
  FileQueue.FileName &= MOD:stFileName:SIDALDEFVer2
  ADD(FileQueue)
  MOD:stFileName:SIDALDEFVer3 = 'SIDALDEF.DAT'
  FileQueue.FileLabel &= SIDALDEFVer3
  FileQueue.FileName &= MOD:stFileName:SIDALDEFVer3
  ADD(FileQueue)
  MOD:stFileName:SIDALDEFVer4 = 'SIDALDEF.DAT'
  FileQueue.FileLabel &= SIDALDEFVer4
  FileQueue.FileName &= MOD:stFileName:SIDALDEFVer4
  ADD(FileQueue)
  MOD:stFileName:SIDALDEFVer5 = 'SIDALDEF.DAT'
  FileQueue.FileLabel &= SIDALDEFVer5
  FileQueue.FileName &= MOD:stFileName:SIDALDEFVer5
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDALDEF'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDREMIN PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDREMINVer2{PROP:Name}
  MOD:stFileName:SIDREMINVer1 = 'SIDREMIN.DAT'
  FileQueue.FileLabel &= SIDREMINVer1
  FileQueue.FileName &= MOD:stFileName:SIDREMINVer1
  ADD(FileQueue)
  MOD:stFileName:SIDREMINVer2 = 'SIDREMIN.DAT'
  FileQueue.FileLabel &= SIDREMINVer2
  FileQueue.FileName &= MOD:stFileName:SIDREMINVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDREMIN'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDRRCT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDRRCTVer2{PROP:Name}
  MOD:stFileName:SIDRRCTVer1 = 'SIDRRCT'
  FileQueue.FileLabel &= SIDRRCTVer1
  FileQueue.FileName &= MOD:stFileName:SIDRRCTVer1
  ADD(FileQueue)
  MOD:stFileName:SIDRRCTVer2 = 'SIDRRCT.DAT'
  FileQueue.FileLabel &= SIDRRCTVer2
  FileQueue.FileName &= MOD:stFileName:SIDRRCTVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDRRCT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDMODTT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDMODTTVer3{PROP:Name}
  MOD:stFileName:SIDMODTTVer1 = 'SIDMODTT'
  FileQueue.FileLabel &= SIDMODTTVer1
  FileQueue.FileName &= MOD:stFileName:SIDMODTTVer1
  ADD(FileQueue)
  MOD:stFileName:SIDMODTTVer2 = 'SIDMODTT'
  FileQueue.FileLabel &= SIDMODTTVer2
  FileQueue.FileName &= MOD:stFileName:SIDMODTTVer2
  ADD(FileQueue)
  MOD:stFileName:SIDMODTTVer3 = 'SIDMODTT'
  FileQueue.FileLabel &= SIDMODTTVer3
  FileQueue.FileName &= MOD:stFileName:SIDMODTTVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDMODTT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDSRVCN PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDSRVCNVer6{PROP:Name}
  MOD:stFileName:SIDSRVCNVer1 = 'SIDSRVCN'
  FileQueue.FileLabel &= SIDSRVCNVer1
  FileQueue.FileName &= MOD:stFileName:SIDSRVCNVer1
  ADD(FileQueue)
  MOD:stFileName:SIDSRVCNVer2 = 'SIDSRVCN'
  FileQueue.FileLabel &= SIDSRVCNVer2
  FileQueue.FileName &= MOD:stFileName:SIDSRVCNVer2
  ADD(FileQueue)
  MOD:stFileName:SIDSRVCNVer3 = 'SIDSRVCN.DAT'
  FileQueue.FileLabel &= SIDSRVCNVer3
  FileQueue.FileName &= MOD:stFileName:SIDSRVCNVer3
  ADD(FileQueue)
  MOD:stFileName:SIDSRVCNVer4 = 'SIDSRVCN.DAT'
  FileQueue.FileLabel &= SIDSRVCNVer4
  FileQueue.FileName &= MOD:stFileName:SIDSRVCNVer4
  ADD(FileQueue)
  MOD:stFileName:SIDSRVCNVer5 = 'SIDSRVCN.DAT'
  FileQueue.FileLabel &= SIDSRVCNVer5
  FileQueue.FileName &= MOD:stFileName:SIDSRVCNVer5
  ADD(FileQueue)
  MOD:stFileName:SIDSRVCNVer6 = 'SIDSRVCN.DAT'
  FileQueue.FileLabel &= SIDSRVCNVer6
  FileQueue.FileName &= MOD:stFileName:SIDSRVCNVer6
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDSRVCN'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDMODSC PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDMODSCVer4{PROP:Name}
  MOD:stFileName:SIDMODSCVer1 = 'SIDMODSC'
  FileQueue.FileLabel &= SIDMODSCVer1
  FileQueue.FileName &= MOD:stFileName:SIDMODSCVer1
  ADD(FileQueue)
  MOD:stFileName:SIDMODSCVer2 = 'SIDMODSC'
  FileQueue.FileLabel &= SIDMODSCVer2
  FileQueue.FileName &= MOD:stFileName:SIDMODSCVer2
  ADD(FileQueue)
  MOD:stFileName:SIDMODSCVer3 = 'SIDMODSC'
  FileQueue.FileLabel &= SIDMODSCVer3
  FileQueue.FileName &= MOD:stFileName:SIDMODSCVer3
  ADD(FileQueue)
  MOD:stFileName:SIDMODSCVer4 = 'SIDMODSC'
  FileQueue.FileLabel &= SIDMODSCVer4
  FileQueue.FileName &= MOD:stFileName:SIDMODSCVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDMODSC'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSIDNWDAY PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SIDNWDAYVer2{PROP:Name}
  MOD:stFileName:SIDNWDAYVer1 = 'SIDNWDAY'
  FileQueue.FileLabel &= SIDNWDAYVer1
  FileQueue.FileName &= MOD:stFileName:SIDNWDAYVer1
  ADD(FileQueue)
  MOD:stFileName:SIDNWDAYVer2 = 'SIDNWDAY'
  FileQueue.FileLabel &= SIDNWDAYVer2
  FileQueue.FileName &= MOD:stFileName:SIDNWDAYVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SIDNWDAY'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSMOREMIN PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SMOREMINVer2{PROP:Name}
  MOD:stFileName:SMOREMINVer1 = 'SMOREMIN.DAT'
  FileQueue.FileLabel &= SMOREMINVer1
  FileQueue.FileName &= MOD:stFileName:SMOREMINVer1
  ADD(FileQueue)
  MOD:stFileName:SMOREMINVer2 = 'SMOREMIN.DAT'
  FileQueue.FileLabel &= SMOREMINVer2
  FileQueue.FileName &= MOD:stFileName:SMOREMINVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SMOREMIN'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMANFAULO PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MANFAULOVer2{PROP:Name}
  MOD:stFileName:MANFAULOVer1 = 'MANFAULO.DAT'
  FileQueue.FileLabel &= MANFAULOVer1
  FileQueue.FileName &= MOD:stFileName:MANFAULOVer1
  ADD(FileQueue)
  MOD:stFileName:MANFAULOVer2 = 'MANFAULO.DAT'
  FileQueue.FileLabel &= MANFAULOVer2
  FileQueue.FileName &= MOD:stFileName:MANFAULOVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MANFAULO'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertRETSTOCK PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = RETSTOCKVer2{PROP:Name}
  MOD:stFileName:RETSTOCKVer1 = 'RETSTOCK.DAT'
  FileQueue.FileLabel &= RETSTOCKVer1
  FileQueue.FileName &= MOD:stFileName:RETSTOCKVer1
  ADD(FileQueue)
  MOD:stFileName:RETSTOCKVer2 = 'RETSTOCK.DAT'
  FileQueue.FileLabel &= RETSTOCKVer2
  FileQueue.FileName &= MOD:stFileName:RETSTOCKVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'RETSTOCK'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertORDPARTS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = ORDPARTSVer2{PROP:Name}
  MOD:stFileName:ORDPARTSVer1 = 'ORDPARTS.DAT'
  FileQueue.FileLabel &= ORDPARTSVer1
  FileQueue.FileName &= MOD:stFileName:ORDPARTSVer1
  ADD(FileQueue)
  MOD:stFileName:ORDPARTSVer2 = 'ORDPARTS.DAT'
  FileQueue.FileLabel &= ORDPARTSVer2
  FileQueue.FileName &= MOD:stFileName:ORDPARTSVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'ORDPARTS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertLOCATION PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = LOCATIONVer3{PROP:Name}
  MOD:stFileName:LOCATIONVer1 = 'LOCATION.DAT'
  FileQueue.FileLabel &= LOCATIONVer1
  FileQueue.FileName &= MOD:stFileName:LOCATIONVer1
  ADD(FileQueue)
  MOD:stFileName:LOCATIONVer2 = 'LOCATION.DAT'
  FileQueue.FileLabel &= LOCATIONVer2
  FileQueue.FileName &= MOD:stFileName:LOCATIONVer2
  ADD(FileQueue)
  MOD:stFileName:LOCATIONVer3 = 'LOCATION.DAT'
  FileQueue.FileLabel &= LOCATIONVer3
  FileQueue.FileName &= MOD:stFileName:LOCATIONVer3
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'LOCATION'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertJOBS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = JOBSVer5{PROP:Name}
  MOD:stFileName:JOBSVer1 = 'JOBS.DAT'
  FileQueue.FileLabel &= JOBSVer1
  FileQueue.FileName &= MOD:stFileName:JOBSVer1
  ADD(FileQueue)
  MOD:stFileName:JOBSVer2 = 'JOBS.DAT'
  FileQueue.FileLabel &= JOBSVer2
  FileQueue.FileName &= MOD:stFileName:JOBSVer2
  ADD(FileQueue)
  MOD:stFileName:JOBSVer3 = 'JOBS.DAT'
  FileQueue.FileLabel &= JOBSVer3
  FileQueue.FileName &= MOD:stFileName:JOBSVer3
  ADD(FileQueue)
  MOD:stFileName:JOBSVer4 = 'JOBS.DAT'
  FileQueue.FileLabel &= JOBSVer4
  FileQueue.FileName &= MOD:stFileName:JOBSVer4
  ADD(FileQueue)
  MOD:stFileName:JOBSVer5 = 'JOBS.DAT'
  FileQueue.FileLabel &= JOBSVer5
  FileQueue.FileName &= MOD:stFileName:JOBSVer5
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'JOBS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMODELNUM PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MODELNUMVer20{PROP:Name}
  MOD:stFileName:MODELNUMVer1 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer1
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer1
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer2 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer2
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer2
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer3 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer3
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer3
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer4 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer4
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer4
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer5 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer5
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer5
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer6 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer6
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer6
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer7 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer7
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer7
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer8 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer8
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer8
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer9 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer9
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer9
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer10 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer10
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer10
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer11 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer11
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer11
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer12 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer12
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer12
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer13 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer13
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer13
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer14 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer14
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer14
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer15 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer15
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer15
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer16 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer16
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer16
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer17 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer17
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer17
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer18 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer18
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer18
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer19 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer19
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer19
  ADD(FileQueue)
  MOD:stFileName:MODELNUMVer20 = 'MODELNUM.DAT'
  FileQueue.FileLabel &= MODELNUMVer20
  FileQueue.FileName &= MOD:stFileName:MODELNUMVer20
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MODELNUM'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSTATUS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = STATUSVer4{PROP:Name}
  MOD:stFileName:STATUSVer1 = 'STATUS.DAT'
  FileQueue.FileLabel &= STATUSVer1
  FileQueue.FileName &= MOD:stFileName:STATUSVer1
  ADD(FileQueue)
  MOD:stFileName:STATUSVer2 = 'STATUS.DAT'
  FileQueue.FileLabel &= STATUSVer2
  FileQueue.FileName &= MOD:stFileName:STATUSVer2
  ADD(FileQueue)
  MOD:stFileName:STATUSVer3 = 'STATUS.DAT'
  FileQueue.FileLabel &= STATUSVer3
  FileQueue.FileName &= MOD:stFileName:STATUSVer3
  ADD(FileQueue)
  MOD:stFileName:STATUSVer4 = 'STATUS.DAT'
  FileQueue.FileLabel &= STATUSVer4
  FileQueue.FileName &= MOD:stFileName:STATUSVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'STATUS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertTRADEACC PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = TRADEACCVer8{PROP:Name}
  MOD:stFileName:TRADEACCVer1 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer1
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer1
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer2 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer2
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer2
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer3 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer3
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer3
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer4 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer4
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer4
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer5 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer5
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer5
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer6 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer6
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer6
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer7 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer7
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer7
  ADD(FileQueue)
  MOD:stFileName:TRADEACCVer8 = 'TRADEACC.DAT'
  FileQueue.FileLabel &= TRADEACCVer8
  FileQueue.FileName &= MOD:stFileName:TRADEACCVer8
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'TRADEACC'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertSUBTRACC PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = SUBTRACCVer8{PROP:Name}
  MOD:stFileName:SUBTRACCVer1 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer1
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer1
  ADD(FileQueue)
  MOD:stFileName:SUBTRACCVer2 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer2
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer2
  ADD(FileQueue)
  MOD:stFileName:SUBTRACCVer3 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer3
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer3
  ADD(FileQueue)
  MOD:stFileName:SUBTRACCVer4 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer4
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer4
  ADD(FileQueue)
  MOD:stFileName:SUBTRACCVer5 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer5
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer5
  ADD(FileQueue)
  MOD:stFileName:SUBTRACCVer6 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer6
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer6
  ADD(FileQueue)
  MOD:stFileName:SUBTRACCVer7 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer7
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer7
  ADD(FileQueue)
  MOD:stFileName:SUBTRACCVer8 = 'SUBTRACC.DAT'
  FileQueue.FileLabel &= SUBTRACCVer8
  FileQueue.FileName &= MOD:stFileName:SUBTRACCVer8
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'SUBTRACC'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMANUFACT PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MANUFACTVer12{PROP:Name}
  MOD:stFileName:MANUFACTVer1 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer1
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer1
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer2 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer2
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer2
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer3 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer3
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer3
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer4 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer4
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer4
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer5 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer5
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer5
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer6 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer6
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer6
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer7 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer7
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer7
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer8 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer8
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer8
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer9 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer9
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer9
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer10 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer10
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer10
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer11 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer11
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer11
  ADD(FileQueue)
  MOD:stFileName:MANUFACTVer12 = 'MANUFACT.DAT'
  FileQueue.FileLabel &= MANUFACTVer12
  FileQueue.FileName &= MOD:stFileName:MANUFACTVer12
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MANUFACT'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertXMLDEFS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = XMLDEFSVer4{PROP:Name}
  MOD:stFileName:XMLDEFSVer1 = 'XMLDEFS.DAT'
  FileQueue.FileLabel &= XMLDEFSVer1
  FileQueue.FileName &= MOD:stFileName:XMLDEFSVer1
  ADD(FileQueue)
  MOD:stFileName:XMLDEFSVer2 = 'XMLDEFS.DAT'
  FileQueue.FileLabel &= XMLDEFSVer2
  FileQueue.FileName &= MOD:stFileName:XMLDEFSVer2
  ADD(FileQueue)
  MOD:stFileName:XMLDEFSVer3 = 'XMLDEFS.DAT'
  FileQueue.FileLabel &= XMLDEFSVer3
  FileQueue.FileName &= MOD:stFileName:XMLDEFSVer3
  ADD(FileQueue)
  MOD:stFileName:XMLDEFSVer4 = 'XMLDEFS.DAT'
  FileQueue.FileLabel &= XMLDEFSVer4
  FileQueue.FileName &= MOD:stFileName:XMLDEFSVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'XMLDEFS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertXMLSB PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = XMLSBVer2{PROP:Name}
  MOD:stFileName:XMLSBVer1 = 'XMLSB.DAT'
  FileQueue.FileLabel &= XMLSBVer1
  FileQueue.FileName &= MOD:stFileName:XMLSBVer1
  ADD(FileQueue)
  MOD:stFileName:XMLSBVer2 = 'XMLSB.DAT'
  FileQueue.FileLabel &= XMLSBVer2
  FileQueue.FileName &= MOD:stFileName:XMLSBVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'XMLSB'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertMPXDEFS PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = MPXDEFSVer6{PROP:Name}
  MOD:stFileName:MPXDEFSVer1 = 'MPXDEFS.DAT'
  FileQueue.FileLabel &= MPXDEFSVer1
  FileQueue.FileName &= MOD:stFileName:MPXDEFSVer1
  ADD(FileQueue)
  MOD:stFileName:MPXDEFSVer2 = 'MPXDEFS.DAT'
  FileQueue.FileLabel &= MPXDEFSVer2
  FileQueue.FileName &= MOD:stFileName:MPXDEFSVer2
  ADD(FileQueue)
  MOD:stFileName:MPXDEFSVer3 = 'MPXDEFS.DAT'
  FileQueue.FileLabel &= MPXDEFSVer3
  FileQueue.FileName &= MOD:stFileName:MPXDEFSVer3
  ADD(FileQueue)
  MOD:stFileName:MPXDEFSVer4 = 'MPXDEFS.DAT'
  FileQueue.FileLabel &= MPXDEFSVer4
  FileQueue.FileName &= MOD:stFileName:MPXDEFSVer4
  ADD(FileQueue)
  MOD:stFileName:MPXDEFSVer5 = 'MPXDEFS.DAT'
  FileQueue.FileLabel &= MPXDEFSVer5
  FileQueue.FileName &= MOD:stFileName:MPXDEFSVer5
  ADD(FileQueue)
  MOD:stFileName:MPXDEFSVer6 = 'MPXDEFS.DAT'
  FileQueue.FileLabel &= MPXDEFSVer6
  FileQueue.FileName &= MOD:stFileName:MPXDEFSVer6
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'MPXDEFS'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertXMLSAM PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = XMLSAMVer2{PROP:Name}
  MOD:stFileName:XMLSAMVer1 = 'XMLSAM.DAT'
  FileQueue.FileLabel &= XMLSAMVer1
  FileQueue.FileName &= MOD:stFileName:XMLSAMVer1
  ADD(FileQueue)
  MOD:stFileName:XMLSAMVer2 = 'XMLSAM.DAT'
  FileQueue.FileLabel &= XMLSAMVer2
  FileQueue.FileName &= MOD:stFileName:XMLSAMVer2
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'XMLSAM'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ProcessFileQueue PROCEDURE

LOC:lLoopIndex        LONG,AUTO
LOC:lTempIndex        LONG,AUTO
LOC:stStatusFullBuild STRING(3),AUTO
LOC:byReturnCode      BYTE(1)
LOC:byErrorStatus     BYTE(0)

  CODE
  FREE(TempFileQueue)
  LOOP LOC:lLoopIndex = RECORDS(FileQueue) TO 1 BY -1
    GET(FileQueue,LOC:lLoopIndex)
    OPEN(FileQueue.FileLabel,42h)
    IF ERRORCODE()
      IF ERRORCODE() = BadKeyErr
        IF UPPER(FileQueue.FileLabel{PROP:Driver}) <> 'CLARION' AND UPPER(FileQueue.FileLabel{PROP:Driver}) <> 'BTRIEVE'
          CLOSE(FileQueue.FileLabel)
          OPEN(FileQueue.FileLabel,12h)
          IF UPPER(FileQueue.FileLabel{PROP:Driver}) = 'TOPSPEED'
            LOC:stStatusFullBuild = SEND(FileQueue.FileLabel,'FULLBUILD')
            Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=On')
          END
          BUILD(FileQueue.FileLabel)
          IF UPPER(FileQueue.FileLabel{PROP:Driver}) = 'TOPSPEED'
            IF CLIP(UPPER(LOC:stStatusFullBuild)) = 'ON'
              Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=On')
            ELSE
              Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=Off')
            END
          END
        ELSE
          LOC:byErrorStatus = 1
          IF LOC:lLoopIndex <> 1
            CYCLE
          END
        END
      END
    ELSE
      IF LOC:lLoopIndex = RECORDS(FileQueue)
        CLOSE(FileQueue.FileLabel)
        LOC:byReturnCode = 0
        DO ProcedureReturn
      END
    END
    CLOSE(FileQueue.FileLabel)
    OPEN(FileQueue.FileLabel,12h)
    IF ERRORCODE()
      CASE ERRORCODE()
      OF NoAccessErr
        IF MESSAGE('Converting can not be finished because data file|"' & CLIP(ERRORFILE()) & '"|is locked by other station.|Ask users to quit all programs, which uses file|"' & CLIP(ERRORFILE()) & '"|and try again.','Conversion Error',ICON:Question,BUTTON:RETRY+BUTTON:ABORT,BUTTON:RETRY) = BUTTON:RETRY
          LOC:lLoopIndex += 1
          CYCLE
        ELSE
          DO ProcedureReturn
        END
      OF InvalidFileErr
      OROF NoPathErr
      OROF NoFileErr
      OROF BadKeyErr
        CASE ERRORCODE()
        OF NoPathErr
        OROF NoFileErr      ; IF LOC:byErrorStatus = 0 THEN LOC:byErrorStatus = 2.
        OF InvalidFileErr   ; IF LOC:byErrorStatus = 2 THEN LOC:byErrorStatus = 0.
        END
        IF LOC:lLoopIndex = 1
          CASE LOC:byErrorStatus
          OF 1 ; ErrorBox('Keys must be rebuild|File: ' & ERRORFILE())
          OF 2 ; LOC:byReturnCode = 0
          ELSE ; ErrorBox('No match file structure|File: ' & ERRORFILE())
          END  
          DO ProcedureReturn
        ELSE
          IF FileQueue.FileLabel &= XMLJOBSVer10
            DO AddQueueTempFile
          END
          IF FileQueue.FileLabel &= XMLJOBSVer14
            DO AddQueueTempFile
          END
        END
      ELSE
        IF LOC:lLoopIndex = 1 THEN FileErrorBox(); DO ProcedureReturn.
      END
    ELSE
      CLOSE(FileQueue.FileLabel)
      LOOP LOC:lTempIndex = 1 TO RECORDS(TempFileQueue)
        GET(TempFileQueue,LOC:lTempIndex)
        CreateTempName(TempFileQueue.TempFileName,'m' & LOC:lTempIndex,TempFileQueue.TempFileLabel)
        CASE UPPER(TempFileQueue.TempFileLabel{PROP:Driver})
        ELSE
          RemoveFiles(TempFileQueue.TempFileLabel,TempFileQueue.TempFileName)
        END
        CREATE(TempFileQueue.TempFileLabel)
        IF ERRORCODE() THEN FileErrorBox(); DO ProcedureReturn.
        OPEN(TempFileQueue.TempFileLabel,12h)
        IF ERRORCODE() THEN FileErrorBox(); DO ProcedureReturn.
      END
      LOC:byReturnCode = ConvertFiles(LOC:lLoopIndex)
      LOOP LOC:lTempIndex = 1 TO RECORDS(TempFileQueue)
        GET(TempFileQueue,LOC:lTempIndex)
        CLOSE(TempFileQueue.TempFileLabel)
        CASE UPPER(TempFileQueue.TempFileLabel{PROP:Driver})
        ELSE
          RemoveFiles(TempFileQueue.TempFileLabel,TempFileQueue.TempFileName)
        END
      END
      DO ProcedureReturn
    END
  END
  LOC:byReturnCode = 0
  DO ProcedureReturn
!----------------------------------------------------------

AddQueueTempFile ROUTINE
  CLEAR(TempFileQueue)
  TempFileQueue.TempFileLabel &= FileQueue.FileLabel
  TempFileQueue.TempFileName  &= FileQueue.FileName
  ADD(TempFileQueue)
  EXIT
!---------------------------------------------------------------------

ProcedureReturn ROUTINE
  FREE(FileQueue)
  FREE(TempFileQueue)
  RETURN(LOC:byReturnCode)
!---------------------------------------------------------------------

ErrorBox PROCEDURE(STRING ErrorMessage)
  CODE
  MESSAGE('Conversion impossible!|' & CLIP(ErrorMessage),'Conversion Error',ICON:Exclamation,BUTTON:ABORT)
  RETURN
!---------------------------------------------------------------------

FileErrorBox PROCEDURE
  CODE
  ErrorBox('Error: ' & CLIP(CHOOSE(ERRORCODE() = 90,FILEERROR(),ERROR())) & '|Error File: ' & CHOOSE(LEN(ERRORFILE()) > 0,CLIP(ERRORFILE()),CLIP(MOD:stFileName)))
  RETURN
!---------------------------------------------------------------------

CreateTempName PROCEDURE(*STRING PAR:ProcessFileName,STRING PAR:Postfix,FILE PAR:File)

LOC:cstPath     CSTRING(260),AUTO
LOC:cstDrive    CSTRING(260),AUTO
LOC:cstDir      CSTRING(260),AUTO
LOC:cstName     CSTRING(260),AUTO
LOC:cstExt      CSTRING(260),AUTO
LOC:cstCopyName CSTRING(260),AUTO
LOC:cstCopyExt  CSTRING(260),AUTO

  CODE
  CASE UPPER(PAR:File{PROP:Driver})
  OF 'MSSQL'
  OROF 'SQLANYWHERE'
    LOC:cstPath = CLIP(PAR:ProcessFileName) & '_' & CLIP(PAR:Postfix)
  ELSE
    LOC:cstPath = CLIP(PAR:ProcessFileName)
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF LOC:cstName[1] = '!'
      LOC:cstCopyName = LOC:cstName
      LOC:cstCopyExt  = LOC:cstExt
      LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
      Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
      LOC:cstDir = LOC:cstDir & LOC:cstName & '$' & LOC:cstCopyName[2 : LEN(LOC:cstCopyName)] & '$' & CLIP(PAR:Postfix) & LOC:cstExt & '\'
      DC:FnMerge(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstCopyName,LOC:cstCopyExt)
    ELSE
      LOC:cstName = CLIP(LOC:cstName & '$' & PAR:Postfix)
      DC:FnMerge(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    END
  END
  PAR:ProcessFileName = LOC:cstPath
  RETURN
!---------------------------------------------------------------------

RemoveFiles PROCEDURE(FILE PAR:ProcessFileLabel,STRING PAR:ProcessFileName)

LOC:cstPath    CSTRING(260),AUTO
LOC:cstDrive   CSTRING(260),AUTO
LOC:cstDir     CSTRING(260),AUTO
LOC:cstName    CSTRING(260),AUTO
LOC:cstExt     CSTRING(260),AUTO
LOC:lLoopIndex LONG,AUTO
LOC:lLoopExt   LONG,AUTO
ListDir        QUEUE(FILE:Queue),PRE(LD)
               END

  CODE
  PrepareExtentionQueue(PAR:ProcessFileLabel)
  LOC:cstPath = CLIP(PAR:ProcessFileName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOOP LOC:lLoopExt = 1 TO RECORDS(ListExt)
    GET(ListExt,LOC:lLoopExt)
    FREE(ListDir)
    DIRECTORY(ListDir,LOC:cstDrive & LOC:cstDir & LOC:cstName & ListExt.Extention,ff_:ARCHIVE)
    LOOP LOC:lLoopIndex = RECORDS(ListDir) TO 1 BY -1
      GET(ListDir,LOC:lLoopIndex)
      IF CLIP(LD:ShortName) = '..' OR CLIP(LD:ShortName) = '.' THEN CYCLE.
      REMOVE(LOC:cstDrive & LOC:cstDir & LD:Name)
    END
  END
  FREE(ListExt)
  FREE(ListDir)
  RETURN
!---------------------------------------------------------------------

GetFileNameWithoutOwner PROCEDURE(STRING PAR:FileName)

LOC:lPosition LONG,AUTO

  CODE
  IF PAR:FileName
    LOC:lPosition = INSTRING('.',PAR:FileName,1,1)
    IF LOC:lPosition
      RETURN(PAR:FileName[LOC:lPosition + 1 : LEN(CLIP(PAR:FileName))])
    ELSE
      RETURN(PAR:FileName)
    END
  ELSE
    RETURN('')
  END
!---------------------------------------------------------------------

RenameFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:TargetName)

LOC:lLoopIndex       LONG,AUTO
LOC:cstPath          CSTRING(260),AUTO
LOC:cstDrive         CSTRING(260),AUTO
LOC:cstDir           CSTRING(260),AUTO
LOC:cstName          CSTRING(260),AUTO
LOC:cstExt           CSTRING(260),AUTO
ListDir              QUEUE(FILE:Queue),PRE(LD)
                     END
LOC:cstTempName      CSTRING(260),AUTO
LOC:stTempSourceName STRING(260),AUTO
LOC:stTempTargetName STRING(260),AUTO
LOC:lErrorCode       LONG,AUTO
LOC:lLoopExt         LONG,AUTO
loc:extcount         LONG,AUTO
loc:Result           CSTRING(34),AUTO

  CODE
  LOC:cstPath = CLIP(PAR:SourceFile{PROP:Name})
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOC:cstTempName = LOC:cstDrive & LOC:cstDir & LOC:cstName
  LOC:stTempSourceName = LOC:cstDrive & LOC:cstDir
  LOC:cstPath = CLIP(PAR:TargetName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    RemoveFiles(PAR:SourceFile,LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1])
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOC:stTempTargetName = LOC:cstDrive & LOC:cstDir & LOC:cstName
  PrepareExtentionQueue(PAR:SourceFile)
  LOOP LOC:lLoopExt = 1 TO RECORDS(ListExt)
    GET(ListExt,LOC:lLoopExt)
    FREE(ListDir)
    DIRECTORY(ListDir,LOC:cstTempName & ListExt.Extention,ff_:ARCHIVE)
    LOOP LOC:lLoopIndex = RECORDS(ListDir) TO 1 BY -1
      GET(ListDir,LOC:lLoopIndex)
      IF CLIP(LD:ShortName) = '..' OR CLIP(LD:ShortName) = '.' THEN CYCLE.
      LOC:cstPath = CLIP(LOC:stTempSourceName) & CLIP(LD:Name)
      Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
      RENAME(CLIP(LOC:stTempSourceName) & LD:Name,CLIP(LOC:stTempTargetName) & LOC:cstExt)
      !Look for and rename extensions
      Loop loc:extcount = 1 to 255
          LtoA(loc:extcount,loc:Result,16)
          If Len(loc:Result) = 1
              loc:Result = '0' & loc:Result
          End ! If Len(loc:Result) = 1
          loc:Result = UPPER(loc:Result)

          If Exists(CLIP(LOC:stTempSourceName) & Clip(loc:cstName) & '.^' & loc:Result)
              Rename(CLIP(LOC:stTempSourceName) & Clip(loc:cstName) & '.^' & loc:Result,Clip(loc:stTempTargetName) & '.^' & loc:Result)
          Else
              Break
          End ! If Exists(CLip(loc:stTempSourceName) & '.^' & loc:Result)
      End ! Loop loc:extcount = 1 to 255

      IF ERRORCODE() 
        LOC:lErrorCode = ERRORCODE()
        FREE(ListExt)
        FREE(ListDir)
        DC:SetError(LOC:lErrorCode)
        RETURN
      END
    END
  END
  FREE(ListExt)
  FREE(ListDir)
  RETURN
!---------------------------------------------------------------------

PrepareExtentionQueue PROCEDURE(FILE PAR:ProcessFileLabel)

LOC:lLoopIndex LONG,AUTO
LOC:cstPath    CSTRING(260),AUTO
LOC:cstDrive   CSTRING(260),AUTO
LOC:cstDir     CSTRING(260),AUTO
LOC:cstName    CSTRING(260),AUTO
LOC:cstExt     CSTRING(260),AUTO

  CODE
  LOC:cstPath = CLIP(PAR:ProcessFileLabel{PROP:Name})
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  FREE(ListExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF ~LOC:cstExt
      ListExt.Extention = '.tps'
    ELSE
      ListExt.Extention = LOC:cstExt
    END
    ADD(ListExt)
  ELSE
    IF ~LOC:cstExt
      CASE UPPER(SUB(PAR:ProcessFileLabel{PROP:Driver},1,3))
      OF   'CLA'
      OROF   'BTR'
        ListExt.Extention = '.dat'
      OF   'TOP'
        ListExt.Extention = '.tps'
      OF   'DBA'
      OROF 'CLI'
      OROF 'FOX'
        ListExt.Extention = '.dbf'
      END
    ELSE
      ListExt.Extention = LOC:cstExt
    END
    ADD(ListExt)
    CASE UPPER(SUB(PAR:ProcessFileLabel{PROP:Driver},1,3))
    OF 'CLA'
      ListExt.Extention = '.mem'
      ADD(ListExt)
      LOOP LOC:lLoopIndex = 1 TO 99
        ListExt.Extention = '.k' & FORMAT(LOC:lLoopIndex,@N02)
        ADD(ListExt)
        ListExt.Extention = '.i' & FORMAT(LOC:lLoopIndex,@N02)
        ADD(ListExt)
      END
    OF 'BTR'
      ListExt.Extention = '.dbt'
      ADD(ListExt)
      ListExt.Extention = '.ndx'
      ADD(ListExt)
    OF 'DBA'
      ListExt.Extention = '.ndx'
      ADD(ListExt)
      ListExt.Extention = '.mdx'
      ADD(ListExt)
      ListExt.Extention = '.dbt'
      ADD(ListExt)
    OF 'CLI'
      ListExt.Extention = '.ntx'
      ADD(ListExt)
      ListExt.Extention = '.cdx'
      ADD(ListExt)
      ListExt.Extention = '.dbt'
      ADD(ListExt)
    OF 'FOX'
      ListExt.Extention = '.inx'
      ADD(ListExt)
      ListExt.Extention = '.cdx'
      ADD(ListExt)
      ListExt.Extention = '.fbt'
      ADD(ListExt)
    END
  END
  RETURN
!---------------------------------------------------------------------

PrepareMultiTableFileName PROCEDURE(FILE PAR:File,STRING PAR:SourceName,*STRING PAR:TargetName)

LOC:cstPath      CSTRING(260),AUTO
LOC:cstDrive     CSTRING(260),AUTO
LOC:cstDir       CSTRING(260),AUTO
LOC:cstName      CSTRING(260),AUTO
LOC:cstExt       CSTRING(260),AUTO
LOC:byMultyTable BYTE(False)

  CODE
  CLEAR(PAR:TargetName)
  LOC:cstPath = CLIP(PAR:SourceName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF UPPER(PAR:File{PROP:Driver}) = 'TOPSPEED' AND LOC:cstName[1] = '!'
    PAR:TargetName = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF ~LOC:cstExt
      PAR:TargetName = CLIP(PAR:TargetName) & '.tps'
    END
    LOC:byMultyTable = True
  END
  RETURN(LOC:byMultyTable)
!---------------------------------------------------------------------

PrepareDestFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:SourceName,FILE PAR:TargetFile,STRING PAR:TargetName,STRING PAR:OriginalTargetName)

LOC:stTempSourceFile STRING(260),AUTO
LOC:stTempTargetFile STRING(260),AUTO
LOC:byMultyTable     BYTE(False)

  CODE
  IF PrepareMultiTableFileName(PAR:SourceFile,PAR:SourceName,LOC:stTempSourceFile)
    IF PrepareMultiTableFileName(PAR:TargetFile,PAR:TargetName,LOC:stTempTargetFile)
      SETCURSOR(CURSOR:Wait)
      COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
      SETCURSOR
      IF ERRORCODE()
        DC:SetErrorFile(PAR:SourceFile)
        RETURN(1)
      END
      PAR:SourceFile{PROP:Name} = PAR:TargetName
      REMOVE(PAR:SourceFile)
      IF ERRORCODE()
        DC:SetErrorFile(PAR:SourceFile)
        RETURN(1)
      END
      PAR:SourceFile{PROP:Name} = PAR:SourceName
      LOC:byMultyTable = True
    END
  ELSE
    IF PrepareMultiTableFileName(PAR:TargetFile,PAR:TargetName,LOC:stTempTargetFile)
      IF PrepareMultiTableFileName(PAR:TargetFile,PAR:OriginalTargetName,LOC:stTempSourceFile)
        IF EXISTS(LOC:stTempSourceFile)
          SETCURSOR(CURSOR:Wait)
          COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
          SETCURSOR
          IF ERRORCODE()
            DC:SetErrorFile(PAR:SourceFile)
            RETURN(1)
          END
        END
        LOC:byMultyTable = True
      END
    END
  END
  IF LOC:byMultyTable = False
    RemoveFiles(PAR:TargetFile,PAR:TargetName)
  END
  RETURN(0)
!---------------------------------------------------------------------

RenameDestFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:TargetName,STRING PAR:OriginalTargetName)

LOC:stTempSourceFile STRING(260),AUTO
LOC:stTempTargetFile STRING(260),AUTO
LOC:byMultyTable     BYTE(False)
ROU:lErrorCode       LONG,AUTO

  CODE
  IF ~PrepareMultiTableFileName(PAR:SourceFile,PAR:OriginalTargetName,LOC:stTempSourceFile)
    IF PrepareMultiTableFileName(PAR:SourceFile,PAR:TargetName,LOC:stTempTargetFile)
      IF PrepareMultiTableFileName(PAR:SourceFile,PAR:SourceFile{PROP:Name},LOC:stTempSourceFile)
        SETCURSOR(CURSOR:Wait)
        COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
        SETCURSOR
        IF ERRORCODE()
          DC:SetErrorFile(PAR:SourceFile)
          RETURN(1)
        END
        REMOVE(PAR:SourceFile)
        IF ERRORCODE()
          ROU:lErrorCode = ERRORCODE()
          RemoveFiles(PAR:SourceFile,LOC:stTempTargetFile)
          DC:SetError(ROU:lErrorCode)
          DC:SetErrorFile(PAR:SourceFile)
          RETURN(1)
        END
        LOC:byMultyTable = True
      END
    END
  END
  IF LOC:byMultyTable = False
    RenameFile(PAR:SourceFile,PAR:TargetName)
    IF ERRORCODE()
      RETURN(1)
    END
  END
  RETURN(0)
!---------------------------------------------------------------------

ConvertFiles FUNCTION(LONG Version)

LocalRequest         LONG
tmp:LocationNumber   LONG
tmp:AutoSub          LONG
tmp:AutoTra          LONG
OriginalRequest      LONG
LocalResponse        LONG
FilesOpened          LONG
WindowOpened         LONG
WindowInitialized    LONG
ForceRefresh         LONG
ReturnCode           BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
LOC:byTPSDriver         BYTE,AUTO
LOC:byDestSQLDriver     BYTE,AUTO
LOC:byDestMSSQLDriver   BYTE,AUTO
LOC:byDestASADriver     BYTE,AUTO
LOC:bySourceSQLDriver   BYTE,AUTO
LOC:bySourceMSSQLDriver BYTE,AUTO
LOC:bySourceASADriver   BYTE,AUTO
LOC:byTransactionActive BYTE,AUTO
LOC:rfSourceFileLabel   &FILE
LOC:stSourceFileName    STRING(260),AUTO
LOC:rfDestFileLabel     &FILE
LOC:stDestFileName      STRING(260),AUTO
LOC:stTargetFileName    STRING(260),AUTO
RecordProcessed      LONG
RecordTotal          LONG
InfoString           STRING(50)
DummyFileName1       STRING(260)
DummyFileName2       STRING(260)
SaveRecordProcessed  LONG
SavePercent          LONG
SaveFillBar          LONG

SPBBorderControl     SIGNED
SPBFillControl       SIGNED
SPBPercentControl    SIGNED

window               WINDOW('File Conversion'),AT(,,224,52),FONT('Arial',8,,),CENTER,GRAY
                       PANEL,AT(4,4,216,44),USE(?Panel1),BEVEL(-1)
                       PROGRESS,USE(RecordProcessed),AT(12,12,200,12),RANGE(0,100)
                       STRING(@s50),AT(12,32,200,),USE(InfoString),CENTER,FONT(,,COLOR:Navy,,CHARSET:ANSI)
                     END
  CODE
  PUSHBIND
  SETCURSOR
  ReturnCode = 1
  LocalRequest = GlobalRequest
  OriginalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  ForceRefresh = False
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  IF KEYCODE() = MouseRight
    SETKEYCODE(0)
  END
  DO PrepareProcedure
  ACCEPT
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:OpenWindow
      IF NOT WindowInitialized
        DO InitializeWindow
        WindowInitialized = True
      END
      SELECT(?Panel1)
    OF EVENT:GainFocus
      ForceRefresh = True
      IF NOT WindowInitialized
        DO InitializeWindow
        WindowInitialized = True
      ELSE
        DO RefreshWindow
      END
    OF Event:Rejected
      BEEP
      DISPLAY(?)
      SELECT(?)
    END
  END
  DO ProcedureReturn
!---------------------------------------------------------------------------
PrepareProcedure ROUTINE
  FilesOpened = True
  OPEN(window)
  WindowOpened=True
  window{PROP:Text} = CLIP(window{PROP:Text}) & ' - ' & MOD:stFileName
  DO ConversionProcess
  DO ProcedureReturn
!---------------------------------------------------------------------------
ProcedureReturn ROUTINE
!|
!| This routine provides a common procedure exit point for all template
!| generated procedures.
!|
!| First, all of the files opened by this procedure are closed.
!|
!| Next, if it was opened by this procedure, the window is closed.
!|
!| Next, GlobalResponse is assigned a value to signal the calling procedure
!| what happened in this procedure.
!|
!| Next, we replace the BINDings that were in place when the procedure initialized
!| (and saved with PUSHBIND) using POPBIND.
!|
!| Finally, we return to the calling procedure, passing ReturnCode back.
!|
  IF FilesOpened
  END
  IF WindowOpened
    CLOSE(window)
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  POPBIND
  RETURN(ReturnCode)
!---------------------------------------------------------------------------
InitializeWindow ROUTINE
!|
!| This routine is used to prepare any control templates for use. It should be called once
!| per procedure.
!|
  DO RefreshWindow
!---------------------------------------------------------------------------
RefreshWindow ROUTINE
!|
!| This routine is used to keep all displays and control templates current.
!|
  IF window{Prop:AcceptAll} THEN EXIT.
  DISPLAY()
  ForceRefresh = False
!---------------------------------------------------------------------------
SyncWindow ROUTINE
!|
!| This routine is used to insure that any records pointed to in control
!| templates are fetched before any procedures are called via buttons or menu
!| options.
!|
!---------------------------------------------------------------------------

ConversionProcess ROUTINE
  DATA
ROU:fCurrentFile      &FILE
ROU:lConvertedRecNum  LONG,AUTO
ROU:lRecordsThisCycle LONG,AUTO
ROU:lDim1             LONG,AUTO
ROU:lDim2             LONG,AUTO
ROU:lDim3             LONG,AUTO
ROU:lDim4             LONG,AUTO
ROU:byError           BYTE,AUTO
  CODE
  GET(FileQueue,Version)
  LOC:rfSourceFileLabel &= FileQueue.FileLabel
  LOC:stSourceFileName   = FileQueue.FileName
  GET(FileQueue,RECORDS(FileQueue))
  LOC:rfDestFileLabel &= FileQueue.FileLabel
  LOC:stDestFileName   = FileQueue.FileName
  LOC:stTargetFileName = LOC:stDestFileName
  LOC:byTransactionActive = False
  LOC:byTPSDriver         = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'TOPSPEED',True,False)
  LOC:bySourceMSSQLDriver = CHOOSE(UPPER(LOC:rfSourceFileLabel{PROP:Driver}) = 'MSSQL',True,False)
  LOC:bySourceASADriver   = CHOOSE(UPPER(LOC:rfSourceFileLabel{PROP:Driver}) = 'SQLANYWHERE',True,False)
  LOC:bySourceSQLDriver   = LOC:bySourceMSSQLDriver + LOC:bySourceASADriver
  LOC:byDestMSSQLDriver   = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'MSSQL',True,False)
  LOC:byDestASADriver     = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'SQLANYWHERE',True,False)
  LOC:byDestSQLDriver     = LOC:byDestMSSQLDriver + LOC:byDestASADriver
  CreateTempName(LOC:stDestFileName,'tmp',LOC:rfDestFileLabel)
  IF LOC:byDestSQLDriver
    DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
  ELSE
    IF PrepareMultiTableFileName(LOC:rfDestFileLabel,LOC:stDestFileName,DummyFileName1) AND |
       (PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:stSourceFileName,DummyFileName1) OR |
        (PrepareMultiTableFileName(LOC:rfDestFileLabel,LOC:stTargetFileName,DummyFileName2) AND EXISTS(DummyFileName2)) |
       )
      InfoString = 'File copying...'
      DISPLAY
    END
    IF PrepareDestFile(LOC:rfSourceFileLabel,LOC:stSourceFileName,LOC:rfDestFileLabel,LOC:stDestFileName,LOC:stTargetFileName) THEN DO PreReturn; EXIT.
  END
  OPEN(LOC:rfSourceFileLabel,12h)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
  CREATE(LOC:rfDestFileLabel)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
  OPEN(LOC:rfDestFileLabel,12h)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  IF LOC:byTPSDriver 
    ROU:lConvertedRecNum = 0
  ELSE
    IF ~LOC:byDestSQLDriver THEN STREAM(LOC:rfDestFileLabel).
  END
  ?RecordProcessed{PROP:Use} = RecordProcessed
  ?InfoString{PROP:Use} = InfoString
  DO InitalizeSolidProgressBar
  ?RecordProcessed{PROP:RangeHigh} = RECORDS(LOC:rfSourceFileLabel)
  RecordProcessed = 0
  RecordTotal     = RECORDS(LOC:rfSourceFileLabel)
  SaveRecordProcessed = 0
  SavePercent         = 0
  SaveFillBar         = 0
  SETCURSOR(CURSOR:Wait)
  DISPLAY
  SET(LOC:rfSourceFileLabel)
  IF LOC:byTPSDriver
    LOGOUT(1,LOC:rfDestFileLabel)
    IF ERRORCODE() THEN DO PreReturn; EXIT.
    LOC:byTransactionActive = True
  END
  ROU:byError = 0
  window{PROP:Timer} = 1
  ACCEPT
    CASE EVENT()
    OF EVENT:GainFocus
      DISPLAY
    OF EVENT:CloseWindow
      CYCLE
    OF EVENT:Completed
      BREAK
    OF EVENT:Timer
      ROU:lRecordsThisCycle = 0
      LOOP WHILE ROU:lRecordsThisCycle < 25
        NEXT(LOC:rfSourceFileLabel)
        IF ERRORCODE()
          POST(EVENT:Completed)
          BREAK
        END
        ROU:fCurrentFile &= LOC:rfSourceFileLabel
        IF ROU:fCurrentFile &= CARISMAVer1
          ! Field "PAYTOracleCode" added
          ! Field "CarismaCode" changed label to "ContractOracleCode"
          ! Key/Index "ContractCodeKey" changed (CarismaCodeKey KEY(cma:CarismaCode),DUP,NOCASE -> ContractCodeKey KEY(cma:ContractOracleCode),DUP,NOCASE)
          ! Key/Index "PAYTCodeKey" added
          CLEAR(CARISMAVer2:RECORD)
          CARISMAVer2:RECORD :=: CARISMAVer1:RECORD
          CARISMAVer2:ContractOracleCode = CARISMAVer1:CarismaCode
          ROU:fCurrentFile &= CARISMAVer2
        END
        IF ROU:fCurrentFile &= CARISMAVer2
          ! Field "ContractActive" added
          ! Field "PAYTActive" added
          CLEAR(CARISMAVer3:RECORD)
          CARISMAVer3:RECORD :=: CARISMAVer2:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(CARISMA, 2)
          If CARISMAVer3:ContractOracleCode <> ''
              CARISMAVer3:ContractActive = 1
          End ! If CARISMAVer3:ContractCode <> ''
          If CARISMAVer3:PAYTOracleCode <> ''
              CARISMAVer3:PAYTActive = 1
          End ! If CARISMAVer3:PAYTCode <> ''
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(CARISMA, 2)
        END
        IF ROU:fCurrentFile &= DEFEMAILVer1
          ! Field "SMSFTPAddress" added
          ! Field "SMSFTPUsername" added
          ! Field "SMSFTPPassword" added
          ! Field "SMSFTPLocation" added
          CLEAR(DEFEMAILVer2:RECORD)
          DEFEMAILVer2:RECORD :=: DEFEMAILVer1:RECORD
          ROU:fCurrentFile &= DEFEMAILVer2
        END
        IF ROU:fCurrentFile &= DEFEMAILVer2
          ! Field "DespFromSCText" changed (DespFromSCText STRING(160) -> DespFromSCText STRING(480))
          ! Field "ArrivedAtRetailText" changed (ArrivedAtRetailText STRING(160) -> ArrivedAtRetailText STRING(480))
          ! Field "R1Active" added
          ! Field "R2Active" added
          ! Field "R3Active" added
          ! Field "R1SMSActive" added
          ! Field "R2SMSActive" added
          ! Field "R3SMSActive" added
          ! Field "BookedRetailText" added
          CLEAR(DEFEMAILVer3:RECORD)
          DEFEMAILVer3:RECORD :=: DEFEMAILVer2:RECORD
          ROU:fCurrentFile &= DEFEMAILVer3
        END
        IF ROU:fCurrentFile &= DEFEMAILVer3
          ! Field "R4Active" added
          ! Field "R4SMSActive" added
          ! Field "DespFromStoreText" added
          CLEAR(DEFEMAILVer4:RECORD)
          DEFEMAILVer4:RECORD :=: DEFEMAILVer3:RECORD
        END
        IF ROU:fCurrentFile &= REGIONSVer1
          ! Field "BankHolidaysFileName" added
          ! Field "TransitDaysToSC" added
          ! Field "TransitDaysToStore" added
          CLEAR(REGIONSVer2:RECORD)
          REGIONSVer2:RECORD :=: REGIONSVer1:RECORD
          ROU:fCurrentFile &= REGIONSVer2
        END
        IF ROU:fCurrentFile &= REGIONSVer2
          ! Attribute "OEM" added
          ! Field "TransitDaysToSC" removed
          ! Field "TransitDaysToStore" removed
          CLEAR(REGIONSVer3:RECORD)
          REGIONSVer3:RECORD :=: REGIONSVer2:RECORD
        END
        IF ROU:fCurrentFile &= ACCREGVer1
          ! Field "TransitDaysException" added
          ! Field "TransitDaysToSC" added
          ! Field "TransitDaysToStore" added
          CLEAR(ACCREGVer2:RECORD)
          ACCREGVer2:RECORD :=: ACCREGVer1:RECORD
          ROU:fCurrentFile &= ACCREGVer2
        END
        IF ROU:fCurrentFile &= ACCREGVer2
          ! Attribute "OEM" added
          ! Field "TransitDaysException" removed
          ! Field "TransitDaysToSC" removed
          ! Field "TransitDaysToStore" removed
          CLEAR(ACCREGVer3:RECORD)
          ACCREGVer3:RECORD :=: ACCREGVer2:RECORD
        END
        IF ROU:fCurrentFile &= SIDACREGVer1
          ! Key/Index "SCAccRegIDKey" changed (SCAccRegIDKey KEY(sar:SCAccountID,sar:AccRegID),NOCASE -> SCAccRegIDKey KEY(sar:SCAccountID,sar:AccRegID),DUP,NOCASE)
          CLEAR(SIDACREGVer2:RECORD)
          SIDACREGVer2:RECORD :=: SIDACREGVer1:RECORD
        END
        IF ROU:fCurrentFile &= XMLJOBSVer1
          ! Field "CONSUMER_POSTCODE" added
          CLEAR(XMLJOBSVer2:RECORD)
          XMLJOBSVer2:RECORD :=: XMLJOBSVer1:RECORD
          ROU:fCurrentFile &= XMLJOBSVer2
        END
        IF ROU:fCurrentFile &= XMLJOBSVer2
          ! Field "CONSUMER_HOUSE_NUMBER" added
          ! Field "CONSUMER_STREET" added
          CLEAR(XMLJOBSVer3:RECORD)
          XMLJOBSVer3:RECORD :=: XMLJOBSVer2:RECORD
          ROU:fCurrentFile &= XMLJOBSVer3
        END
        IF ROU:fCurrentFile &= XMLJOBSVer3
          ! Key/Index "State_Job_Number_Key" added
          CLEAR(XMLJOBSVer4:RECORD)
          XMLJOBSVer4:RECORD :=: XMLJOBSVer3:RECORD
          ROU:fCurrentFile &= XMLJOBSVer4
        END
        IF ROU:fCurrentFile &= XMLJOBSVer4
          ! Field "IN_OUT_WARRANTY" added
          CLEAR(XMLJOBSVer5:RECORD)
          XMLJOBSVer5:RECORD :=: XMLJOBSVer4:RECORD
          ROU:fCurrentFile &= XMLJOBSVer5
        END
        IF ROU:fCurrentFile &= XMLJOBSVer5
          ! Key/Index "JobNumberKey" added
          CLEAR(XMLJOBSVer6:RECORD)
          XMLJOBSVer6:RECORD :=: XMLJOBSVer5:RECORD
          ROU:fCurrentFile &= XMLJOBSVer6
        END
        IF ROU:fCurrentFile &= XMLJOBSVer6
          ! Field "CONSUMER_STREET" removed
          ! Field "EXCEPTION_CODE" added
          ! Field "TR_NO" changed (TR_NO STRING(20) -> TR_NO STRING(30))
          ! Field "ASC_CODE" changed (ASC_CODE STRING(30) -> ASC_CODE STRING(10))
          ! Field "MODEL_CODE" changed (MODEL_CODE STRING(30) -> MODEL_CODE STRING(40))
          ! Field "SERIAL_NO" changed (SERIAL_NO STRING(20) -> SERIAL_NO STRING(16))
          ! Field "CONSUMER_TITLE" added
          ! Field "CONSUMER_FIRST_NAME" added
          ! Field "CONSUMER_LAST_NAME" changed (CONSUMER_LAST_NAME STRING(30) -> CONSUMER_LAST_NAME STRING(40))
          ! Field "CONSUMER_TEL_NUMBER1" changed (CONSUMER_TEL_NUMBER1 STRING(15) -> CONSUMER_TEL_NUMBER1 STRING(30))
          ! Field "CONSUMER_FAX_NUMBER" added
          ! Field "CONSUMER_EMAIL" added
          ! Field "CONSUMER_COUNTRY" added
          ! Field "CONSUMER_REGION" added
          ! Field "CONSUMER_CITY" added
          ! Field "CONSUMER_STREET" added
          CLEAR(XMLJOBSVer7:RECORD)
          XMLJOBSVer7:RECORD :=: XMLJOBSVer6:RECORD
          ROU:fCurrentFile &= XMLJOBSVer7
        END
        IF ROU:fCurrentFile &= XMLJOBSVer7
          ! Key/Index "JobNumberKey" changed (JobNumberKey KEY(XJB:JOB_NO),DUP,NOCASE,OPT -> JobNumberKey KEY(XJB:JOB_NO),DUP,NOCASE)
          ! Key/Index "State_Job_Number_Key" changed (State_Job_Number_Key KEY(XJB:RECORD_STATE,XJB:JOB_NO),DUP,NOCASE,OPT -> State_Job_Number_Key KEY(XJB:RECORD_STATE,XJB:JOB_NO),DUP,NOCASE)
          ! Key/Index "State_Surname_Key" changed (Surname_Key KEY(XJB:CONSUMER_LAST_NAME),DUP,NOCASE -> State_Surname_Key KEY(XJB:RECORD_STATE,XJB:CONSUMER_LAST_NAME),DUP,NOCASE)
          ! Key/Index "State_Postcode_Key" changed (Postcode_Key KEY(XJB:CONSUMER_POSTCODE),DUP,NOCASE -> State_Postcode_Key KEY(XJB:RECORD_STATE,XJB:CONSUMER_POSTCODE),DUP,NOCASE)
          ! Key/Index "State_Model_Key" changed (Model_Key KEY(XJB:MODEL_CODE),DUP,NOCASE -> State_Model_Key KEY(XJB:RECORD_STATE,XJB:MODEL_CODE),DUP,NOCASE)
          ! Key/Index "State_Serial_No_Key" changed (Serial_No_Key KEY(XJB:SERIAL_NO),DUP,NOCASE -> State_Serial_No_Key KEY(XJB:RECORD_STATE,XJB:SERIAL_NO),DUP,NOCASE)
          CLEAR(XMLJOBSVer8:RECORD)
          XMLJOBSVer8:RECORD :=: XMLJOBSVer7:RECORD
          ROU:fCurrentFile &= XMLJOBSVer8
        END
        IF ROU:fCurrentFile &= XMLJOBSVer8
          ! Key/Index "TR_NO_KEY" added
          CLEAR(XMLJOBSVer9:RECORD)
          XMLJOBSVer9:RECORD :=: XMLJOBSVer8:RECORD
          ROU:fCurrentFile &= XMLJOBSVer9
        END
        IF ROU:fCurrentFile &= XMLJOBSVer9
          ! Field "EXCEPTION_DESC" added
          CLEAR(XMLJOBSVer10:RECORD)
          CLEAR(XMLJOBSVer10:EXCEPTION_DESC)
          XMLJOBSVer10:RECORD :=: XMLJOBSVer9:RECORD
          ROU:fCurrentFile &= XMLJOBSVer10
        END
        IF ROU:fCurrentFile &= XMLJOBSVer10
          ! Field "EXCEPTION_DESC" changed (EXCEPTION_DESC MEMO(1000) -> EXCEPTION_DESC STRING(255))
          CLEAR(XMLJOBSVer11:RECORD)
          XMLJOBSVer11:RECORD :=: XMLJOBSVer10:RECORD
          XMLJOBSVer11:EXCEPTION_DESC = XMLJOBSVer10:EXCEPTION_DESC
          ROU:fCurrentFile &= XMLJOBSVer11
        END
        IF ROU:fCurrentFile &= XMLJOBSVer11
          ! Field "TR_STATUS_DESC" added
          CLEAR(XMLJOBSVer12:RECORD)
          XMLJOBSVer12:RECORD :=: XMLJOBSVer11:RECORD
          ROU:fCurrentFile &= XMLJOBSVer12
        END
        IF ROU:fCurrentFile &= XMLJOBSVer12
          ! Field "REPAIR_COMPLETE_DATE" added
          ! Field "REPAIR_COMPLETE_TIME" added
          ! Field "GOODS_DELIVERY_DATE" added
          ! Field "GOODS_DELIVERY_TIME" added
          ! Field "TR_STATUS_DESC" changed label to "TR_REASON"
          CLEAR(XMLJOBSVer13:RECORD)
          XMLJOBSVer13:RECORD :=: XMLJOBSVer12:RECORD
          XMLJOBSVer13:TR_REASON = XMLJOBSVer12:TR_STATUS_DESC
          ROU:fCurrentFile &= XMLJOBSVer13
        END
        IF ROU:fCurrentFile &= XMLJOBSVer13
          ! Field "EXCEPTION_DESC" changed (EXCEPTION_DESC STRING(255) -> EXCEPTION_DESC MEMO(1000))
          CLEAR(XMLJOBSVer14:RECORD)
          CLEAR(XMLJOBSVer14:EXCEPTION_DESC)
          XMLJOBSVer14:RECORD :=: XMLJOBSVer13:RECORD
          XMLJOBSVer14:EXCEPTION_DESC = XMLJOBSVer13:EXCEPTION_DESC
          ROU:fCurrentFile &= XMLJOBSVer14
        END
        IF ROU:fCurrentFile &= XMLJOBSVer14
          ! Field "EXCEPTION_DESC" changed (EXCEPTION_DESC MEMO(1000) -> EXCEPTION_DESC STRING(255))
          CLEAR(XMLJOBSVer15:RECORD)
          XMLJOBSVer15:RECORD :=: XMLJOBSVer14:RECORD
          XMLJOBSVer15:EXCEPTION_DESC = XMLJOBSVer14:EXCEPTION_DESC
          ROU:fCurrentFile &= XMLJOBSVer15
        END
        IF ROU:fCurrentFile &= XMLJOBSVer15
          ! Field "SYMPTOM1_DESC" added
          ! Field "SYMPTOM2_DESC" added
          ! Field "SYMPTOM3_DESC" added
          ! Field "BP_NO" added
          CLEAR(XMLJOBSVer16:RECORD)
          XMLJOBSVer16:RECORD :=: XMLJOBSVer15:RECORD
          ROU:fCurrentFile &= XMLJOBSVer16
        END
        IF ROU:fCurrentFile &= XMLJOBSVer16
          ! Field "INQUIRY_TEXT" added
          CLEAR(XMLJOBSVer17:RECORD)
          XMLJOBSVer17:RECORD :=: XMLJOBSVer16:RECORD
        END
        IF ROU:fCurrentFile &= MULDESPVer1
          ! Field "BatchTotal" added
          CLEAR(MULDESPVer2:RECORD)
          MULDESPVer2:RECORD :=: MULDESPVer1:RECORD
        END
        IF ROU:fCurrentFile &= IMEISHIPVer1
          ! Field "BER" added
          CLEAR(IMEISHIPVer2:RECORD)
          IMEISHIPVer2:RECORD :=: IMEISHIPVer1:RECORD
          ROU:fCurrentFile &= IMEISHIPVer2
        END
        IF ROU:fCurrentFile &= IMEISHIPVer2
          ! Field "ManualEntry" added
          CLEAR(IMEISHIPVer3:RECORD)
          IMEISHIPVer3:RECORD :=: IMEISHIPVer2:RECORD
          ROU:fCurrentFile &= IMEISHIPVer3
        END
        IF ROU:fCurrentFile &= IMEISHIPVer3
          ! Field "ProductCode" added
          CLEAR(IMEISHIPVer4:RECORD)
          IMEISHIPVer4:RECORD :=: IMEISHIPVer3:RECORD
        END
        IF ROU:fCurrentFile &= JOBTHIRDVer1
          ! Key/Index "OriginalIMEIKey" added
          CLEAR(JOBTHIRDVer2:RECORD)
          JOBTHIRDVer2:RECORD :=: JOBTHIRDVer1:RECORD
        END
        IF ROU:fCurrentFile &= LOGRETRNVer1
          ! Field "To_Site" added
          ! Field "SalesCode" added
          CLEAR(LOGRETRNVer2:RECORD)
          LOGRETRNVer2:RECORD :=: LOGRETRNVer1:RECORD
        END
        IF ROU:fCurrentFile &= PRODCODEVer1
          ! Field "DummyField" removed
          ! Field "WarrantyPeriod" added
          CLEAR(PRODCODEVer2:RECORD)
          PRODCODEVer2:RECORD :=: PRODCODEVer1:RECORD
        END
        IF ROU:fCurrentFile &= LOGSTOCKVer1
          ! Key/Index "ModelNumberKey" added
          CLEAR(LOGSTOCKVer2:RECORD)
          LOGSTOCKVer2:RECORD :=: LOGSTOCKVer1:RECORD
        END
        IF ROU:fCurrentFile &= DEFEMAB2Ver1
          ! Field "Subject" added
          CLEAR(DEFEMAB2Ver2:RECORD)
          DEFEMAB2Ver2:RECORD :=: DEFEMAB2Ver1:RECORD
        END
        IF ROU:fCurrentFile &= DEFEMAB3Ver1
          ! Field "Subject" added
          CLEAR(DEFEMAB3Ver2:RECORD)
          DEFEMAB3Ver2:RECORD :=: DEFEMAB3Ver1:RECORD
        END
        IF ROU:fCurrentFile &= DEFEMAB4Ver1
          ! Field "Subject" added
          CLEAR(DEFEMAB4Ver2:RECORD)
          DEFEMAB4Ver2:RECORD :=: DEFEMAB4Ver1:RECORD
        END
        IF ROU:fCurrentFile &= DEFEMAB5Ver1
          ! Field "Subject" added
          CLEAR(DEFEMAB5Ver2:RECORD)
          DEFEMAB5Ver2:RECORD :=: DEFEMAB5Ver1:RECORD
        END
        IF ROU:fCurrentFile &= DEFEMAE2Ver1
          ! Field "Subject" added
          CLEAR(DEFEMAE2Ver2:RECORD)
          DEFEMAE2Ver2:RECORD :=: DEFEMAE2Ver1:RECORD
        END
        IF ROU:fCurrentFile &= DEFEMAE3Ver1
          ! Field "Subject" added
          CLEAR(DEFEMAE3Ver2:RECORD)
          DEFEMAE3Ver2:RECORD :=: DEFEMAE3Ver1:RECORD
        END
        IF ROU:fCurrentFile &= DEFEMAB1Ver1
          ! Field "Subject" added
          CLEAR(DEFEMAB1Ver2:RECORD)
          DEFEMAB1Ver2:RECORD :=: DEFEMAB1Ver1:RECORD
        END
        IF ROU:fCurrentFile &= DEFEMAE1Ver1
          ! Field "E2Email" removed
          ! Field "E3Email" removed
          CLEAR(DEFEMAE1Ver2:RECORD)
          DEFEMAE1Ver2:RECORD :=: DEFEMAE1Ver1:RECORD
          ROU:fCurrentFile &= DEFEMAE1Ver2
        END
        IF ROU:fCurrentFile &= DEFEMAE1Ver2
          ! Field "E1Email" changed (E1Email STRING(5000) -> E1Email STRING(4500))
          CLEAR(DEFEMAE1Ver3:RECORD)
          DEFEMAE1Ver3:RECORD :=: DEFEMAE1Ver2:RECORD
          ROU:fCurrentFile &= DEFEMAE1Ver3
        END
        IF ROU:fCurrentFile &= DEFEMAE1Ver3
          ! Field "E1Email" changed (E1Email STRING(4500) -> E1Email STRING(4000))
          CLEAR(DEFEMAE1Ver4:RECORD)
          DEFEMAE1Ver4:RECORD :=: DEFEMAE1Ver3:RECORD
          ROU:fCurrentFile &= DEFEMAE1Ver4
        END
        IF ROU:fCurrentFile &= DEFEMAE1Ver4
          ! Attribute "NAME" changed ('DEFEMAI3.DAT' -> 'DEFEMAE1.DAT')
          ! Key/Index "RecordNumberKey" changed (RecordNumberKey KEY(dem3:RecordNumber),NOCASE,PRIMARY -> RecordNumberKey KEY(dee1:RecordNumber),NOCASE,PRIMARY)
          CLEAR(DEFEMAE1Ver5:RECORD)
          DEFEMAE1Ver5:RECORD :=: DEFEMAE1Ver4:RECORD
          ROU:fCurrentFile &= DEFEMAE1Ver5
        END
        IF ROU:fCurrentFile &= DEFEMAE1Ver5
          ! Field "Subject" added
          CLEAR(DEFEMAE1Ver6:RECORD)
          DEFEMAE1Ver6:RECORD :=: DEFEMAE1Ver5:RECORD
        END
        IF ROU:fCurrentFile &= DEFEMAI2Ver1
          ! Field "E1Email" removed
          ! Field "E2Email" removed
          ! Field "E3Email" removed
          CLEAR(DEFEMAI2Ver2:RECORD)
          DEFEMAI2Ver2:RECORD :=: DEFEMAI2Ver1:RECORD
          ROU:fCurrentFile &= DEFEMAI2Ver2
        END
        IF ROU:fCurrentFile &= DEFEMAI2Ver2
          ! Field "B1SMS" added
          ! Field "B2SMS" added
          ! Field "B3SMS" added
          ! Field "B4SMS" added
          ! Field "B5SMS" added
          CLEAR(DEFEMAI2Ver3:RECORD)
          DEFEMAI2Ver3:RECORD :=: DEFEMAI2Ver2:RECORD
          ROU:fCurrentFile &= DEFEMAI2Ver3
        END
        IF ROU:fCurrentFile &= DEFEMAI2Ver3
          ! Field "E1Subject" added
          ! Field "E2Subject" added
          ! Field "E3Subject" added
          ! Field "B1Subject" added
          ! Field "B2Subject" added
          ! Field "B3Subject" added
          ! Field "B4Subject" added
          ! Field "B5Subject" added
          ! Field "E1Active" added
          ! Field "E2Active" added
          ! Field "E3Active" added
          ! Field "B1Active" added
          ! Field "B2Active" added
          ! Field "B3Active" added
          ! Field "B4Active" added
          ! Field "B5Active" added
          CLEAR(DEFEMAI2Ver4:RECORD)
          DEFEMAI2Ver4:RECORD :=: DEFEMAI2Ver3:RECORD
          ROU:fCurrentFile &= DEFEMAI2Ver4
        END
        IF ROU:fCurrentFile &= DEFEMAI2Ver4
          ! Field "E1Subject" removed
          ! Field "E2Subject" removed
          ! Field "E3Subject" removed
          ! Field "B1Subject" removed
          ! Field "B2Subject" removed
          ! Field "B3Subject" removed
          ! Field "B4Subject" removed
          ! Field "B5Subject" removed
          CLEAR(DEFEMAI2Ver5:RECORD)
          DEFEMAI2Ver5:RECORD :=: DEFEMAI2Ver4:RECORD
          ROU:fCurrentFile &= DEFEMAI2Ver5
        END
        IF ROU:fCurrentFile &= DEFEMAI2Ver5
          ! Field "E1SMSActive" added
          ! Field "E2SMSActive" added
          ! Field "E3SMSActive" added
          ! Field "B1SMSActive" added
          ! Field "B2SMSActive" added
          ! Field "B3SMSActive" added
          ! Field "B4SMSActive" added
          ! Field "B5SMSActive" added
          CLEAR(DEFEMAI2Ver6:RECORD)
          DEFEMAI2Ver6:RECORD :=: DEFEMAI2Ver5:RECORD
          ROU:fCurrentFile &= DEFEMAI2Ver6
        END
        IF ROU:fCurrentFile &= DEFEMAI2Ver6
          ! Field "B6SMS" added
          ! Field "B7SMS" added
          ! Field "B6Active" added
          ! Field "B7Active" added
          ! Field "B6SMSActive" added
          ! Field "B7SMSActive" added
          ! Field "B6NotifyDays" added
          ! Field "B7NotifyDays" added
          CLEAR(DEFEMAI2Ver7:RECORD)
          DEFEMAI2Ver7:RECORD :=: DEFEMAI2Ver6:RECORD
          ROU:fCurrentFile &= DEFEMAI2Ver7
        END
        IF ROU:fCurrentFile &= DEFEMAI2Ver7
          ! Field "E2BSMS" added
          ! Field "E3BSMS" added
          ! Field "E2BActive" added
          ! Field "E3BActive" added
          ! Field "E2BSMSActive" added
          ! Field "E3BSMSActive" added
          CLEAR(DEFEMAI2Ver8:RECORD)
          DEFEMAI2Ver8:RECORD :=: DEFEMAI2Ver7:RECORD
          ROU:fCurrentFile &= DEFEMAI2Ver8
        END
        IF ROU:fCurrentFile &= DEFEMAI2Ver8
          ! Field "E3SMS" shifted up on 1 position(s)
          ! Field "B1SMS" shifted up on 2 position(s)
          ! Field "B2SMS" shifted up on 2 position(s)
          ! Field "B3SMS" shifted up on 2 position(s)
          ! Field "B4SMS" shifted up on 2 position(s)
          ! Field "B5SMS" shifted up on 2 position(s)
          ! Field "B6SMS" shifted up on 2 position(s)
          ! Field "B7SMS" shifted up on 2 position(s)
          ! Field "E1Active" shifted up on 2 position(s)
          ! Field "E2Active" shifted up on 2 position(s)
          ! Field "E3Active" shifted up on 3 position(s)
          ! Field "B1Active" shifted up on 4 position(s)
          ! Field "B2Active" shifted up on 4 position(s)
          ! Field "B3Active" shifted up on 4 position(s)
          ! Field "B4Active" shifted up on 4 position(s)
          ! Field "B5Active" shifted up on 4 position(s)
          ! Field "B6Active" shifted up on 4 position(s)
          ! Field "B7Active" shifted up on 4 position(s)
          ! Field "E1SMSActive" shifted up on 4 position(s)
          ! Field "E2SMSActive" shifted up on 4 position(s)
          ! Field "E3SMSActive" shifted up on 5 position(s)
          ! Field "B1SMSActive" shifted up on 6 position(s)
          ! Field "B2SMSActive" shifted up on 6 position(s)
          ! Field "B3SMSActive" shifted up on 6 position(s)
          ! Field "B4SMSActive" shifted up on 6 position(s)
          ! Field "B5SMSActive" shifted up on 6 position(s)
          ! Field "B6SMSActive" shifted up on 6 position(s)
          ! Field "B7SMSActive" shifted up on 6 position(s)
          ! Field "B6NotifyDays" shifted up on 6 position(s)
          ! Field "B7NotifyDays" shifted up on 6 position(s)
          ! Field "E2BSMS" shifted down on 30 position(s)
          ! Field "E3BSMS" shifted down on 29 position(s)
          ! Field "E2BActive" shifted down on 20 position(s)
          ! Field "E3BActive" shifted down on 19 position(s)
          ! Field "E2BSMSActive" shifted down on 10 position(s)
          ! Field "E3BSMSActive" shifted down on 9 position(s)
          CLEAR(DEFEMAI2Ver9:RECORD)
          DEFEMAI2Ver9:RECORD :=: DEFEMAI2Ver8:RECORD
        END
        IF ROU:fCurrentFile &= DEFSIDEXVer1
          ! Field "FTPServer" added
          ! Field "FTPUsername" added
          ! Field "FTPPassword" added
          ! Field "FTPExportPath" added
          CLEAR(DEFSIDEXVer2:RECORD)
          DEFSIDEXVer2:RECORD :=: DEFSIDEXVer1:RECORD
          ROU:fCurrentFile &= DEFSIDEXVer2
        END
        IF ROU:fCurrentFile &= DEFSIDEXVer2
          ! Field "DateFrom" removed
          ! Field "DateTo" removed
          ! Field "ExportDays" added
          CLEAR(DEFSIDEXVer3:RECORD)
          DEFSIDEXVer3:RECORD :=: DEFSIDEXVer2:RECORD
        END
        IF ROU:fCurrentFile &= RETSALESVer1
          ! Key/Index "DespatchedPurchDateKey" added
          CLEAR(RETSALESVer2:RECORD)
          RETSALESVer2:RECORD :=: RETSALESVer1:RECORD
          ROU:fCurrentFile &= RETSALESVer2
        END
        IF ROU:fCurrentFile &= RETSALESVer2
          ! Field "Address_Group" removed
          ! Field "Address_Delivery_Group" removed
          CLEAR(RETSALESVer3:RECORD)
          RETSALESVer3:RECORD :=: RETSALESVer2:RECORD
        END
        IF ROU:fCurrentFile &= TEAMSVer1
          ! Field "Completed_Jobs" added
          CLEAR(TEAMSVer2:RECORD)
          TEAMSVer2:RECORD :=: TEAMSVer1:RECORD
        END
        IF ROU:fCurrentFile &= MERGEVer1
          ! Field "TotalFieldLength" added
          CLEAR(MERGEVer2:RECORD)
          MERGEVer2:RECORD :=: MERGEVer1:RECORD
        END
        IF ROU:fCurrentFile &= SIDDEFVer1
          ! Key/Index "RecordNumberKey" added
          CLEAR(SIDDEFVer2:RECORD)
          SIDDEFVer2:RECORD :=: SIDDEFVer1:RECORD
          ROU:fCurrentFile &= SIDDEFVer2
        END
        IF ROU:fCurrentFile &= SIDDEFVer2
          ! Field "FTPFailedCount" added
          CLEAR(SIDDEFVer3:RECORD)
          SIDDEFVer3:RECORD :=: SIDDEFVer2:RECORD
          ROU:fCurrentFile &= SIDDEFVer3
        END
        IF ROU:fCurrentFile &= SIDDEFVer3
          ! Field "UnallocTransitToSC" added
          ! Field "UnallocTurnaround" added
          ! Field "UnallocTransitStore" added
          CLEAR(SIDDEFVer4:RECORD)
          SIDDEFVer4:RECORD :=: SIDDEFVer3:RECORD
          ROU:fCurrentFile &= SIDDEFVer4
        END
        IF ROU:fCurrentFile &= SIDDEFVer4
          ! Field "WebJobConn" added
          ! Field "CPMConn" added
          CLEAR(SIDDEFVer5:RECORD)
          SIDDEFVer5:RECORD :=: SIDDEFVer4:RECORD
          ROU:fCurrentFile &= SIDDEFVer5
        END
        IF ROU:fCurrentFile &= SIDDEFVer5
          ! Field "ToteAlertEmail" added
          CLEAR(SIDDEFVer6:RECORD)
          SIDDEFVer6:RECORD :=: SIDDEFVer5:RECORD
        END
        IF ROU:fCurrentFile &= ORDPENDVer1
          ! Field "PartRecordNumber" added
          ! Key/Index "PartRecordNumberKey" added
          CLEAR(ORDPENDVer2:RECORD)
          ORDPENDVer2:RECORD :=: ORDPENDVer1:RECORD
          ROU:fCurrentFile &= ORDPENDVer2
        END
        IF ROU:fCurrentFile &= ORDPENDVer2
          ! Key/Index "Job_Number_Key" added
          CLEAR(ORDPENDVer3:RECORD)
          ORDPENDVer3:RECORD :=: ORDPENDVer2:RECORD
          ROU:fCurrentFile &= ORDPENDVer3
        END
        IF ROU:fCurrentFile &= ORDPENDVer3
          ! Key/Index "Supplier_Job_Key" added
          CLEAR(ORDPENDVer4:RECORD)
          ORDPENDVer4:RECORD :=: ORDPENDVer3:RECORD
        END
        IF ROU:fCurrentFile &= STOHISTVer1
          ! Key/Index "DateKey" added
          CLEAR(STOHISTVer2:RECORD)
          STOHISTVer2:RECORD :=: STOHISTVer1:RECORD
        END
        IF ROU:fCurrentFile &= EXREASONVer1
          ! Field "Reason" changed (Reason STRING(255) -> Reason STRING(80))
          CLEAR(EXREASONVer2:RECORD)
          EXREASONVer2:RECORD :=: EXREASONVer1:RECORD
        END
        IF ROU:fCurrentFile &= REPTYDEFVer1
          ! Field "BER" added
          CLEAR(REPTYDEFVer2:RECORD)
          REPTYDEFVer2:RECORD :=: REPTYDEFVer1:RECORD
        END
        IF ROU:fCurrentFile &= JOBSEVer1
          ! Field "FailedDelivery" added
          ! Field "CConfirmSecondEntry" added
          ! Field "WConfirmSecondEntry" added
          ! Field "EndUserEmailAddress" added
          ! Field "Network" added
          CLEAR(JOBSEVer2:RECORD)
          JOBSEVer2:RECORD :=: JOBSEVer1:RECORD
          ROU:fCurrentFile &= JOBSEVer2
        END
        IF ROU:fCurrentFile &= JOBSEVer2
          ! Field "ExchangeReason" added
          ! Field "LoanReason" added
          CLEAR(JOBSEVer3:RECORD)
          JOBSEVer3:RECORD :=: JOBSEVer2:RECORD
          ROU:fCurrentFile &= JOBSEVer3
        END
        IF ROU:fCurrentFile &= JOBSEVer3
          ! Field "InWorkshopDate" added
          ! Field "InWorkshopTime" added
          CLEAR(JOBSEVer4:RECORD)
          JOBSEVer4:RECORD :=: JOBSEVer3:RECORD
          ROU:fCurrentFile &= JOBSEVer4
        END
        IF ROU:fCurrentFile &= JOBSEVer4
          ! Field "Pre_RF_Board_IMEI" added
          CLEAR(JOBSEVer5:RECORD)
          JOBSEVer5:RECORD :=: JOBSEVer4:RECORD
          ROU:fCurrentFile &= JOBSEVer5
        END
        IF ROU:fCurrentFile &= JOBSEVer5
          ! Key/Index "InWorkshopDateKey" added
          CLEAR(JOBSEVer6:RECORD)
          JOBSEVer6:RECORD :=: JOBSEVer5:RECORD
          ROU:fCurrentFile &= JOBSEVer6
        END
        IF ROU:fCurrentFile &= JOBSEVer6
          ! Field "CompleteRepairType" added
          ! Field "CompleteRepairDate" added
          ! Field "CompleteRepairTime" added
          ! Key/Index "CompleteRepairKey" added
          CLEAR(JOBSEVer7:RECORD)
          JOBSEVer7:RECORD :=: JOBSEVer6:RECORD
          ROU:fCurrentFile &= JOBSEVer7
        END
        IF ROU:fCurrentFile &= JOBSEVer7
          ! Field "CustomerCollectionDate" added
          ! Field "EstimatedDespatchDate" added
          CLEAR(JOBSEVer8:RECORD)
          JOBSEVer8:RECORD :=: JOBSEVer7:RECORD
          ROU:fCurrentFile &= JOBSEVer8
        END
        IF ROU:fCurrentFile &= JOBSEVer8
          ! Field "CustomerCollectionTime" added
          ! Field "EstimatedDespatchTime" added
          CLEAR(JOBSEVer9:RECORD)
          JOBSEVer9:RECORD :=: JOBSEVer8:RECORD
          ROU:fCurrentFile &= JOBSEVer9
        END
        IF ROU:fCurrentFile &= JOBSEVer9
          ! Field "FaultCode13" added
          ! Field "FaultCode14" added
          ! Field "FaultCode15" added
          ! Field "FaultCode16" added
          ! Field "FaultCode17" added
          ! Field "FaultCode18" added
          ! Field "FaultCode19" added
          ! Field "FaultCode20" added
          CLEAR(JOBSEVer10:RECORD)
          JOBSEVer10:RECORD :=: JOBSEVer9:RECORD
        END
        IF ROU:fCurrentFile &= MANFAULTVer1
          ! Field "ForceFormat" added
          ! Field "FieldFormat" added
          ! Field "DateType" added
          CLEAR(MANFAULTVer2:RECORD)
          MANFAULTVer2:RECORD :=: MANFAULTVer1:RECORD
          ROU:fCurrentFile &= MANFAULTVer2
        END
        IF ROU:fCurrentFile &= MANFAULTVer2
          ! Field "DefaultValue" added
          CLEAR(MANFAULTVer3:RECORD)
          MANFAULTVer3:RECORD :=: MANFAULTVer2:RECORD
          ROU:fCurrentFile &= MANFAULTVer3
        END
        IF ROU:fCurrentFile &= MANFAULTVer3
          ! Field "DefaultValue" removed
          CLEAR(MANFAULTVer4:RECORD)
          MANFAULTVer4:RECORD :=: MANFAULTVer3:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(MANFAULT, 3)
              MANFAULTVER4:DateType   = '@d6'
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(MANFAULT, 3)
        END
        IF ROU:fCurrentFile &= PICKNOTEVer1
          ! Key/Index "keyonjobno" added
          CLEAR(PICKNOTEVer2:RECORD)
          PICKNOTEVer2:RECORD :=: PICKNOTEVer1:RECORD
          ROU:fCurrentFile &= PICKNOTEVer2
        END
        IF ROU:fCurrentFile &= PICKNOTEVer2
          ! Key/Index "keypicknote" added
          CLEAR(PICKNOTEVer3:RECORD)
          PICKNOTEVer3:RECORD :=: PICKNOTEVer2:RECORD
          ROU:fCurrentFile &= PICKNOTEVer3
        END
        IF ROU:fCurrentFile &= PICKNOTEVer3
          ! Field "Usercode" added
          CLEAR(PICKNOTEVer4:RECORD)
          PICKNOTEVer4:RECORD :=: PICKNOTEVer3:RECORD
          ROU:fCurrentFile &= PICKNOTEVer4
        END
        IF ROU:fCurrentFile &= PICKNOTEVer4
          ! Key/Index "keyonlocation" added
          CLEAR(PICKNOTEVer5:RECORD)
          PICKNOTEVer5:RECORD :=: PICKNOTEVer4:RECORD
        END
        IF ROU:fCurrentFile &= COURIERVer1
          ! Field "PrintLabel" added
          CLEAR(COURIERVer2:RECORD)
          COURIERVer2:RECORD :=: COURIERVer1:RECORD
        END
        IF ROU:fCurrentFile &= ESNMODELVer1
          ! Field "ESN" changed (ESN STRING(6) -> ESN STRING(8))
          CLEAR(ESNMODELVer2:RECORD)
          ESNMODELVer2:RECORD :=: ESNMODELVer1:RECORD
        END
        IF ROU:fCurrentFile &= STOCKVer1
          ! Field "E1" added
          ! Field "E2" added
          ! Field "E3" added
          ! Field "AllowDuplicate" added
          CLEAR(STOCKVer2:RECORD)
          STOCKVer2:RECORD :=: STOCKVer1:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(STOCK, 1)
              STOCKVer2:E1 = 1
              STOCKVer2:E2 = 1
              STOCKVer2:E3 = 1
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(STOCK, 1)
          ROU:fCurrentFile &= STOCKVer2
        END
        IF ROU:fCurrentFile &= STOCKVer2
          ! Field "ExcludeFromEDI" added
          CLEAR(STOCKVer3:RECORD)
          STOCKVer3:RECORD :=: STOCKVer2:RECORD
          ROU:fCurrentFile &= STOCKVer3
        END
        IF ROU:fCurrentFile &= STOCKVer3
          ! Field "RF_Board" added
          CLEAR(STOCKVer4:RECORD)
          STOCKVer4:RECORD :=: STOCKVer3:RECORD
        END
        IF ROU:fCurrentFile &= LOGSTOLCVer1
          ! Field "Mark_Returns_Available" added
          CLEAR(LOGSTOLCVer2:RECORD)
          LOGSTOLCVer2:RECORD :=: LOGSTOLCVer1:RECORD
        END
        IF ROU:fCurrentFile &= LOGSERSTVer1
          ! Field "AllocNo" added
          ! Key/Index "AllocNo_Key" added
          CLEAR(LOGSERSTVer2:RECORD)
          LOGSERSTVer2:RECORD :=: LOGSERSTVer1:RECORD
        END
        IF ROU:fCurrentFile &= LOGSTLOCVer1
          ! Field "allocquantity" added
          CLEAR(LOGSTLOCVer2:RECORD)
          LOGSTLOCVer2:RECORD :=: LOGSTLOCVer1:RECORD
        END
        IF ROU:fCurrentFile &= TRAFAULOVer1
          ! Key/Index "DescriptionKey" added
          CLEAR(TRAFAULOVer2:RECORD)
          TRAFAULOVer2:RECORD :=: TRAFAULOVer1:RECORD
        END
        IF ROU:fCurrentFile &= MPXJOBSVer1
          ! Field "Record_State" added
          ! Field "Exception_Code" added
          ! Field "Exception_Desc" added
          ! Field "RefNumber" added
          ! Field "Status" added
          ! Field "ID" added
          ! Field "CustCTN" added
          ! Field "QuoteValue" added
          ! Field "ModelID" added
          ! Field "QuoteQ1" added
          ! Field "QuoteQ2" added
          ! Field "QuoteQ3" added
          ! Field "QuoteACC1" added
          ! Field "QuoteACC2" added
          ! Field "QuoteACC3" added
          ! Field "QuoteACC4" added
          ! Field "QuoteACC5" added
          ! Field "CustTitle" added
          ! Field "CustFirstName" added
          ! Field "CustLastName" added
          ! Field "CustAdd1" added
          ! Field "CustAdd2" added
          ! Field "CustAdd3" added
          ! Field "CustAdd4" added
          ! Field "CustPostCode" added
          ! Field "CustRef" added
          ! Field "CustNetwork" added
          ! Field "EnvLetter" added
          ! Field "InspType" added
          ! Field "EnvType" added
          ! Field "EnvCount" added
          CLEAR(MPXJOBSVer2:RECORD)
          MPXJOBSVer2:RECORD :=: MPXJOBSVer1:RECORD
          ROU:fCurrentFile &= MPXJOBSVer2
        END
        IF ROU:fCurrentFile &= MPXJOBSVer2
          ! Key/Index "IDKey" added
          CLEAR(MPXJOBSVer3:RECORD)
          MPXJOBSVer3:RECORD :=: MPXJOBSVer2:RECORD
          ROU:fCurrentFile &= MPXJOBSVer3
        END
        IF ROU:fCurrentFile &= MPXJOBSVer3
          ! Field "ReportCode" changed (Status STRING(30) -> ReportCode LONG)
          ! Field "EnvDateSent" added
          ! Field "InspACRStatus" added
          ! Field "InspDateRecd" added
          ! Field "InspModelID" added
          ! Field "InspFlag" added
          ! Field "InspAttention" added
          ! Field "InspQty" added
          ! Field "InspPackageCond" added
          ! Field "InspQ1" added
          ! Field "InspQ2" added
          ! Field "InspQ3" added
          ! Field "InspACC1" added
          ! Field "InspACC2" added
          ! Field "InspACC3" added
          ! Field "InspACC4" added
          ! Field "InspACC5" added
          ! Field "InspGrade" added
          ! Field "InspIMEI" added
          ! Field "InspNotes" added
          ! Field "InspDate" added
          ! Field "InspBrand" added
          ! Field "InspInWarranty" added
          ! Field "DespatchConsignment" added
          ! Field "DespatchDate" added
          ! Field "RefNumber" changed label to "InspACRJobNo"
          ! Field "Status" changed label to "ReportCode"
          ! Key/Index "JobNoKey" changed (JobNoKey KEY(MXJ:RefNumber),DUP,NOCASE,OPT -> JobNoKey KEY(MXJ:InspACRJobNo),DUP,NOCASE,OPT)
          ! Key/Index "StateJobNoKey" changed (StateJobNoKey KEY(MXJ:Record_State,MXJ:RefNumber),DUP,NOCASE,OPT -> StateJobNoKey KEY(MXJ:Record_State,MXJ:InspACRJobNo),DUP,NOCASE,OPT)
          ! Key/Index "StateReportCodeKey" added
          CLEAR(MPXJOBSVer4:RECORD)
          MPXJOBSVer4:RECORD :=: MPXJOBSVer3:RECORD
          MPXJOBSVer4:InspACRJobNo = MPXJOBSVer3:RefNumber
          MPXJOBSVer4:ReportCode = MPXJOBSVer3:Status
          ROU:fCurrentFile &= MPXJOBSVer4
        END
        IF ROU:fCurrentFile &= MPXJOBSVer4
          ! Field "ImportDate" added
          ! Field "ImportTime" added
          CLEAR(MPXJOBSVer5:RECORD)
          MPXJOBSVer5:RECORD :=: MPXJOBSVer4:RECORD
          ROU:fCurrentFile &= MPXJOBSVer5
        END
        IF ROU:fCurrentFile &= MPXJOBSVer5
          ! Key/Index "RecordNoKey" changed (RecordNoKey KEY(MXJ:RecordNo),NOCASE,OPT,PRIMARY -> RecordNoKey KEY(MXJ:RecordNo),NOCASE,PRIMARY)
          ! Key/Index "IDKey" changed (IDKey KEY(MXJ:ID),DUP,NOCASE,OPT -> IDKey KEY(MXJ:ID),DUP,NOCASE)
          ! Key/Index "JobNoKey" changed (JobNoKey KEY(MXJ:InspACRJobNo),DUP,NOCASE,OPT -> JobNoKey KEY(MXJ:InspACRJobNo),DUP,NOCASE)
          ! Key/Index "StateJobNoKey" changed (StateJobNoKey KEY(MXJ:Record_State,MXJ:InspACRJobNo),DUP,NOCASE,OPT -> StateJobNoKey KEY(MXJ:Record_State,MXJ:InspACRJobNo),DUP,NOCASE)
          ! Key/Index "StateSurnameKey" changed (StateSurnameKey KEY(MXJ:Record_State,MXJ:CustLastName),DUP,NOCASE,OPT -> StateSurnameKey KEY(MXJ:Record_State,MXJ:CustLastName),DUP,NOCASE)
          ! Key/Index "StatePostcodeKey" changed (StatePostcodeKey KEY(MXJ:Record_State,MXJ:CustPostCode),DUP,NOCASE,OPT -> StatePostcodeKey KEY(MXJ:Record_State,MXJ:CustPostCode),DUP,NOCASE)
          ! Key/Index "StateModelKey" changed (StateModelKey KEY(MXJ:Record_State,MXJ:ModelID),DUP,NOCASE,OPT -> StateModelKey KEY(MXJ:Record_State,MXJ:ModelID),DUP,NOCASE)
          ! Key/Index "StateReportCodeKey" changed (StateReportCodeKey KEY(MXJ:Record_State,MXJ:ReportCode),DUP,NOCASE,OPT -> StateReportCodeKey KEY(MXJ:Record_State,MXJ:ReportCode),DUP,NOCASE)
          CLEAR(MPXJOBSVer6:RECORD)
          MPXJOBSVer6:RECORD :=: MPXJOBSVer5:RECORD
        END
        IF ROU:fCurrentFile &= MPXSTATVer1
          ! Key/Index "SBStatusKey" added
          CLEAR(MPXSTATVer2:RECORD)
          MPXSTATVer2:RECORD :=: MPXSTATVer1:RECORD
          ROU:fCurrentFile &= MPXSTATVer2
        END
        IF ROU:fCurrentFile &= MPXSTATVer2
          ! Key/Index "RecordNoKey" changed (RecordNoKey KEY(MXS:RecordNo),NOCASE,OPT,PRIMARY -> RecordNoKey KEY(MXS:RecordNo),NOCASE,PRIMARY)
          ! Key/Index "MpxStatusKey" changed (MpxStatusKey KEY(MXS:MPX_STATUS),DUP,NOCASE,OPT -> MpxStatusKey KEY(MXS:MPX_STATUS),DUP,NOCASE)
          ! Key/Index "SBStatusKey" changed (SBStatusKey KEY(MXS:SB_STATUS),DUP,NOCASE,OPT -> SBStatusKey KEY(MXS:SB_STATUS),DUP,NOCASE)
          CLEAR(MPXSTATVer3:RECORD)
          MPXSTATVer3:RECORD :=: MPXSTATVer2:RECORD
        END
        IF ROU:fCurrentFile &= TRAFAULTVer1
          ! Field "DefaultValue" added
          CLEAR(TRAFAULTVer2:RECORD)
          TRAFAULTVer2:RECORD :=: TRAFAULTVer1:RECORD
          ROU:fCurrentFile &= TRAFAULTVer2
        END
        IF ROU:fCurrentFile &= TRAFAULTVer2
          ! Field "DefaultValue" removed
          CLEAR(TRAFAULTVer3:RECORD)
          TRAFAULTVer3:RECORD :=: TRAFAULTVer2:RECORD
        END
        IF ROU:fCurrentFile &= STATCRITVer1
          ! Key/Index "RecordNumberKey" changed (RecordNumberKey KEY(STAC:RecordNumber),NOCASE,PRIMARY -> RecordNumberKey KEY(stac:RecordNumber),NOCASE,PRIMARY)
          ! Key/Index "DescriptionKey" changed (DescriptionKey KEY(STAC:Description),NOCASE -> DescriptionKey KEY(stac:Description),DUP,NOCASE)
          ! Key/Index "DescriptionOptionKey" changed (DescriptionOptionKey KEY(STAC:Description,STAC:OptionType),DUP,NOCASE -> DescriptionOptionKey KEY(stac:Description,stac:OptionType),NOCASE)
          CLEAR(STATCRITVer2:RECORD)
          STATCRITVer2:RECORD :=: STATCRITVer1:RECORD
          ROU:fCurrentFile &= STATCRITVer2
        END
        IF ROU:fCurrentFile &= STATCRITVer2
          ! Key/Index "DescriptionOptionKey" changed (DescriptionOptionKey KEY(stac:Description,stac:OptionType),NOCASE -> DescriptionOptionKey KEY(stac:Description,stac:OptionType),DUP,NOCASE)
          CLEAR(STATCRITVer3:RECORD)
          STATCRITVer3:RECORD :=: STATCRITVer2:RECORD
        END
        IF ROU:fCurrentFile &= NOTESDELVer1
          ! Key/Index "keyonnotesdel" added
          CLEAR(NOTESDELVer2:RECORD)
          NOTESDELVer2:RECORD :=: NOTESDELVer1:RECORD
        END
        IF ROU:fCurrentFile &= PICKDETVer1
          ! Key/Index "keyonpicknote" added
          CLEAR(PICKDETVer2:RECORD)
          PICKDETVer2:RECORD :=: PICKDETVer1:RECORD
          ROU:fCurrentFile &= PICKDETVer2
        END
        IF ROU:fCurrentFile &= PICKDETVer2
          ! Key/Index "keypickdet" added
          CLEAR(PICKDETVer3:RECORD)
          PICKDETVer3:RECORD :=: PICKDETVer2:RECORD
          ROU:fCurrentFile &= PICKDETVer3
        END
        IF ROU:fCurrentFile &= PICKDETVer3
          ! Field "Usercode" added
          CLEAR(PICKDETVer4:RECORD)
          PICKDETVer4:RECORD :=: PICKDETVer3:RECORD
          ROU:fCurrentFile &= PICKDETVer4
        END
        IF ROU:fCurrentFile &= PICKDETVer4
          ! Field "StockPartRefNumber" added
          ! Field "PartNumber" added
          CLEAR(PICKDETVer5:RECORD)
          PICKDETVer5:RECORD :=: PICKDETVer4:RECORD
          ROU:fCurrentFile &= PICKDETVer5
        END
        IF ROU:fCurrentFile &= PICKDETVer5
          ! Field "PartNumber" changed (PartNumber LONG -> PartNumber STRING(30))
          CLEAR(PICKDETVer6:RECORD)
          PICKDETVer6:RECORD :=: PICKDETVer5:RECORD
        END
        IF ROU:fCurrentFile &= USERSVer1
          ! Field "StockFromLocationOnly" added
          ! Field "EmailAddress" added
          CLEAR(USERSVer2:RECORD)
          USERSVer2:RECORD :=: USERSVer1:RECORD
        END
        IF ROU:fCurrentFile &= SIDREGVer1
          ! Key/Index "SCRegionNameKey" changed (SCRegionNameKey KEY(srg:SCAccountID,srg:RegionName),NOCASE -> SCRegionNameKey KEY(srg:SCAccountID,srg:RegionName),DUP,NOCASE)
          CLEAR(SIDREGVer2:RECORD)
          SIDREGVer2:RECORD :=: SIDREGVer1:RECORD
        END
        IF ROU:fCurrentFile &= PENDMAILVer1
          ! Field "SMSEmail" changed (SMSEmail STRING(4) -> SMSEmail STRING(5))
          ! Key/Index "RecordNumberKey" changed (RecordNumberKey KEY(pen:RecordNumber),NOCASE,PRIMARY -> RecordNumberKey KEY(pem:RecordNumber),NOCASE,PRIMARY)
          ! Key/Index "DateCreatedKey" changed (DateCreatedKey KEY(-pen:DateCreated,-pen:TimeCreated),DUP,NOCASE -> DateCreatedKey KEY(-pem:DateCreated,-pem:TimeCreated),DUP,NOCASE)
          ! Key/Index "DateMessageKey" changed (DateMessageKey KEY(pen:MessageType,-pen:DateCreated,-pen:TimeCreated),DUP,NOCASE -> DateMessageKey KEY(pem:MessageType,-pem:DateCreated,-pem:TimeCreated),DUP,NOCASE)
          CLEAR(PENDMAILVer2:RECORD)
          PENDMAILVer2:RECORD :=: PENDMAILVer1:RECORD
          ROU:fCurrentFile &= PENDMAILVer2
        END
        IF ROU:fCurrentFile &= PENDMAILVer2
          ! Field "DateToSend" added
          ! Field "TimeToSend" added
          CLEAR(PENDMAILVer3:RECORD)
          PENDMAILVer3:RECORD :=: PENDMAILVer2:RECORD
          ROU:fCurrentFile &= PENDMAILVer3
        END
        IF ROU:fCurrentFile &= PENDMAILVer3
          ! Key/Index "MessageSentKey" added
          CLEAR(PENDMAILVer4:RECORD)
          PENDMAILVer4:RECORD :=: PENDMAILVer3:RECORD
          ROU:fCurrentFile &= PENDMAILVer4
        END
        IF ROU:fCurrentFile &= PENDMAILVer4
          ! Key/Index "SMSSentKey" changed (MessageSentKey KEY(pem:MessageType,pem:DateSent),DUP,NOCASE -> SMSSentKey KEY(pem:SMSEmail,pem:DateSent),DUP,NOCASE)
          CLEAR(PENDMAILVer5:RECORD)
          PENDMAILVer5:RECORD :=: PENDMAILVer4:RECORD
          ROU:fCurrentFile &= PENDMAILVer5
        END
        IF ROU:fCurrentFile &= PENDMAILVer5
          ! Key/Index "MessageRefKey" added
          CLEAR(PENDMAILVer6:RECORD)
          PENDMAILVer6:RECORD :=: PENDMAILVer5:RECORD
          ROU:fCurrentFile &= PENDMAILVer6
        END
        IF ROU:fCurrentFile &= PENDMAILVer6
          ! Field "Dummy" added
          CLEAR(PENDMAILVer7:RECORD)
          PENDMAILVer7:RECORD :=: PENDMAILVer6:RECORD
          ROU:fCurrentFile &= PENDMAILVer7
        END
        IF ROU:fCurrentFile &= PENDMAILVer7
          ! Field "Dummy" removed
          ! Field "BookingType" added
          ! Field "Status" added
          ! Field "SIDJobNumber" added
          ! Key/Index "MessageJobNumber" added
          CLEAR(PENDMAILVer8:RECORD)
          PENDMAILVer8:RECORD :=: PENDMAILVer7:RECORD
          ROU:fCurrentFile &= PENDMAILVer8
        END
        IF ROU:fCurrentFile &= PENDMAILVer8
          ! Key/Index "JobNumberKey" changed (MessageJobNumber KEY(pem:MessageType,pem:SIDJobNumber),DUP,NOCASE -> JobNumberKey KEY(pem:SIDJobNumber),DUP,NOCASE)
          ! Key/Index "RefNumberKey" added
          CLEAR(PENDMAILVer9:RECORD)
          PENDMAILVer9:RECORD :=: PENDMAILVer8:RECORD
          ROU:fCurrentFile &= PENDMAILVer9
        END
        IF ROU:fCurrentFile &= PENDMAILVer9
          ! Field "Reminder" added
          ! Key/Index "ReminderKey" added
          CLEAR(PENDMAILVer10:RECORD)
          PENDMAILVer10:RECORD :=: PENDMAILVer9:RECORD
          ROU:fCurrentFile &= PENDMAILVer10
        END
        IF ROU:fCurrentFile &= PENDMAILVer10
          ! Field "ReminderDescription" added
          ! Key/Index "ReminderRefNumberKey" added
          CLEAR(PENDMAILVer11:RECORD)
          PENDMAILVer11:RECORD :=: PENDMAILVer10:RECORD
          ROU:fCurrentFile &= PENDMAILVer11
        END
        IF ROU:fCurrentFile &= PENDMAILVer11
          ! Field "CustomerUTL" added
          CLEAR(PENDMAILVer12:RECORD)
          PENDMAILVer12:RECORD :=: PENDMAILVer11:RECORD
          ROU:fCurrentFile &= PENDMAILVer12
        END
        IF ROU:fCurrentFile &= PENDMAILVer12
          ! Field "ReminderDescription" removed
          CLEAR(PENDMAILVer13:RECORD)
          PENDMAILVer13:RECORD :=: PENDMAILVer12:RECORD
          ROU:fCurrentFile &= PENDMAILVer13
        END
        IF ROU:fCurrentFile &= PENDMAILVer13
          ! Key/Index "SMSSentDateCreatedKey" added
          CLEAR(PENDMAILVer14:RECORD)
          PENDMAILVer14:RECORD :=: PENDMAILVer13:RECORD
        END
        IF ROU:fCurrentFile &= DEFAULTSVer1
          ! Field "PickNoteNormal" added
          ! Field "PickNoteMainStore" added
          ! Field "PickNoteChangeStatus" added
          ! Field "PickNoteJobStatus" added
          CLEAR(DEFAULTSVer2:RECORD)
          DEFAULTSVer2:RECORD :=: DEFAULTSVer1:RECORD
          ROU:fCurrentFile &= DEFAULTSVer2
        END
        IF ROU:fCurrentFile &= DEFAULTSVer2
          ! Field "record_number" shifted up on 4 position(s)
          ! Field "Version_Number" shifted up on 4 position(s)
          ! Field "Address_Line1" shifted up on 4 position(s)
          ! Field "Address_Line2" shifted up on 4 position(s)
          ! Field "Address_Line3" shifted up on 4 position(s)
          ! Field "Postcode" shifted up on 4 position(s)
          ! Field "Telephone_Number" shifted up on 4 position(s)
          ! Field "Fax_Number" shifted up on 4 position(s)
          ! Field "EmailAddress" shifted up on 4 position(s)
          ! Field "VAT_Number" shifted up on 4 position(s)
          ! Field "Use_Invoice_Address" shifted up on 4 position(s)
          ! Field "Use_For_Order" shifted up on 4 position(s)
          ! Field "Invoice_Company_Name" shifted up on 4 position(s)
          ! Field "Invoice_Address_Line1" shifted up on 4 position(s)
          ! Field "Invoice_Address_Line2" shifted up on 4 position(s)
          ! Field "Invoice_Address_Line3" shifted up on 4 position(s)
          ! Field "Invoice_Postcode" shifted up on 4 position(s)
          ! Field "Invoice_Telephone_Number" shifted up on 4 position(s)
          ! Field "Invoice_Fax_Number" shifted up on 4 position(s)
          ! Field "InvoiceEmailAddress" shifted up on 4 position(s)
          ! Field "Invoice_VAT_Number" shifted up on 4 position(s)
          ! Field "OrderCompanyName" shifted up on 4 position(s)
          ! Field "OrderAddressLine1" shifted up on 4 position(s)
          ! Field "OrderAddressLine2" shifted up on 4 position(s)
          ! Field "OrderAddressLine3" shifted up on 4 position(s)
          ! Field "OrderPostcode" shifted up on 4 position(s)
          ! Field "OrderTelephoneNumber" shifted up on 4 position(s)
          ! Field "OrderFaxNumber" shifted up on 4 position(s)
          ! Field "OrderEmailAddress" shifted up on 4 position(s)
          ! Field "Use_Postcode" shifted up on 4 position(s)
          ! Field "Postcode_Path" shifted up on 4 position(s)
          ! Field "PostcodeDll" shifted up on 4 position(s)
          ! Field "Vat_Rate_Labour" shifted up on 4 position(s)
          ! Field "Vat_Rate_Parts" shifted up on 4 position(s)
          ! Field "License_Number" shifted up on 4 position(s)
          ! Field "Maximum_Users" shifted up on 4 position(s)
          ! Field "Warranty_Period" shifted up on 4 position(s)
          ! Field "Automatic_Replicate" shifted up on 4 position(s)
          ! Field "Automatic_Replicate_Field" shifted up on 4 position(s)
          ! Field "Estimate_If_Over" shifted up on 4 position(s)
          ! Field "Start_Work_Hours" shifted up on 4 position(s)
          ! Field "End_Work_Hours" shifted up on 4 position(s)
          ! Field "Include_Saturday" shifted up on 4 position(s)
          ! Field "Include_Sunday" shifted up on 4 position(s)
          ! Field "Show_Mobile_Number" shifted up on 4 position(s)
          ! Field "Show_Loan_Exchange_Details" shifted up on 4 position(s)
          ! Field "Use_Credit_Limit" shifted up on 4 position(s)
          ! Field "Hide_Physical_Damage" shifted up on 4 position(s)
          ! Field "Hide_Insurance" shifted up on 4 position(s)
          ! Field "Hide_Authority_Number" shifted up on 4 position(s)
          ! Field "Hide_Advance_Payment" shifted up on 4 position(s)
          ! Field "HideColour" shifted up on 4 position(s)
          ! Field "HideInCourier" shifted up on 4 position(s)
          ! Field "HideIntFault" shifted up on 4 position(s)
          ! Field "HideProduct" shifted up on 4 position(s)
          ! Field "HideLocation" shifted up on 4 position(s)
          ! Field "Allow_Bouncer" shifted up on 4 position(s)
          ! Field "Job_Batch_Number" shifted up on 4 position(s)
          ! Field "QA_Required" shifted up on 4 position(s)
          ! Field "QA_Before_Complete" shifted up on 4 position(s)
          ! Field "QAPreliminary" shifted up on 4 position(s)
          ! Field "QAExchLoan" shifted up on 4 position(s)
          ! Field "CompleteAtQA" shifted up on 4 position(s)
          ! Field "RapidQA" shifted up on 4 position(s)
          ! Field "ValidateESN" shifted up on 4 position(s)
          ! Field "ValidateDesp" shifted up on 4 position(s)
          ! Field "Force_Accessory_Check" shifted up on 4 position(s)
          ! Field "Force_Initial_Transit_Type" shifted up on 4 position(s)
          ! Field "Force_Mobile_Number" shifted up on 4 position(s)
          ! Field "Force_Model_Number" shifted up on 4 position(s)
          ! Field "Force_Unit_Type" shifted up on 4 position(s)
          ! Field "Force_Fault_Description" shifted up on 4 position(s)
          ! Field "Force_ESN" shifted up on 4 position(s)
          ! Field "Force_MSN" shifted up on 4 position(s)
          ! Field "Force_Job_Type" shifted up on 4 position(s)
          ! Field "Force_Engineer" shifted up on 4 position(s)
          ! Field "Force_Invoice_Text" shifted up on 4 position(s)
          ! Field "Force_Repair_Type" shifted up on 4 position(s)
          ! Field "Force_Authority_Number" shifted up on 4 position(s)
          ! Field "Force_Fault_Coding" shifted up on 4 position(s)
          ! Field "Force_Spares" shifted up on 4 position(s)
          ! Field "Force_Incoming_Courier" shifted up on 4 position(s)
          ! Field "Force_Outoing_Courier" shifted up on 4 position(s)
          ! Field "Force_DOP" shifted up on 4 position(s)
          ! Field "Order_Number" shifted up on 4 position(s)
          ! Field "Customer_Name" shifted up on 4 position(s)
          ! Field "ForcePostcode" shifted up on 4 position(s)
          ! Field "ForceDelPostcode" shifted up on 4 position(s)
          ! Field "ForceCommonFault" shifted up on 4 position(s)
          ! Field "Remove_Backgrounds" shifted up on 4 position(s)
          ! Field "Use_Loan_Exchange_Label" shifted up on 4 position(s)
          ! Field "Use_Job_Label" shifted up on 4 position(s)
          ! Field "UseSmallLabel" shifted up on 4 position(s)
          ! Field "Job_Label" shifted up on 4 position(s)
          ! Field "add_stock_label" shifted up on 4 position(s)
          ! Field "receive_stock_label" shifted up on 4 position(s)
          ! Field "QA_Failed_Label" shifted up on 4 position(s)
          ! Field "QA_Failed_Report" shifted up on 4 position(s)
          ! Field "QAPassLabel" shifted up on 4 position(s)
          ! Field "ThirdPartyNote" shifted up on 4 position(s)
          ! Field "Vodafone_Import_Path" shifted up on 4 position(s)
          ! Field "Label_Printer_Type" shifted up on 4 position(s)
          ! Field "Browse_Option" shifted up on 4 position(s)
          ! Field "ANCCollectionNo" shifted up on 4 position(s)
          ! Field "Use_Sage" shifted up on 4 position(s)
          ! Field "Global_Nominal_Code" shifted up on 4 position(s)
          ! Field "Parts_Stock_Code" shifted up on 4 position(s)
          ! Field "Labour_Stock_Code" shifted up on 4 position(s)
          ! Field "Courier_Stock_Code" shifted up on 4 position(s)
          ! Field "Parts_Description" shifted up on 4 position(s)
          ! Field "Labour_Description" shifted up on 4 position(s)
          ! Field "Courier_Description" shifted up on 4 position(s)
          ! Field "Parts_Code" shifted up on 4 position(s)
          ! Field "Labour_Code" shifted up on 4 position(s)
          ! Field "Courier_Code" shifted up on 4 position(s)
          ! Field "User_Name_Sage" shifted up on 4 position(s)
          ! Field "Password_Sage" shifted up on 4 position(s)
          ! Field "Company_Number_Sage" shifted up on 4 position(s)
          ! Field "Path_Sage" shifted up on 4 position(s)
          ! Field "BouncerTime" shifted up on 4 position(s)
          ! Field "ExportPath" shifted up on 4 position(s)
          ! Field "PrintBarcode" shifted up on 4 position(s)
          ! Field "ChaChargeType" shifted up on 4 position(s)
          ! Field "WarChargeType" shifted up on 4 position(s)
          ! Field "RetBackOrders" shifted up on 4 position(s)
          ! Field "RemoveWorkshopDespatch" shifted up on 4 position(s)
          ! Field "SummaryOrders" shifted up on 4 position(s)
          ! Field "EuroRate" shifted up on 4 position(s)
          ! Field "IrishVatRate" shifted up on 4 position(s)
          ! Field "EmailServerAddress" shifted up on 4 position(s)
          ! Field "EmailServerPort" shifted up on 4 position(s)
          ! Field "Job_Label_Accessories" shifted up on 4 position(s)
          ! Field "ShowRepTypeCosts" shifted up on 4 position(s)
          ! Field "PickNoteNormal" shifted down on 133 position(s)
          ! Field "PickNoteMainStore" shifted down on 133 position(s)
          ! Field "PickNoteChangeStatus" shifted down on 133 position(s)
          ! Field "PickNoteJobStatus" shifted down on 133 position(s)
          CLEAR(DEFAULTSVer3:RECORD)
          DEFAULTSVer3:RECORD :=: DEFAULTSVer2:RECORD
          ROU:fCurrentFile &= DEFAULTSVer3
        END
        IF ROU:fCurrentFile &= DEFAULTSVer3
          ! Field "UseMultipleJobBookingDefaults" added
          CLEAR(DEFAULTSVer4:RECORD)
          DEFAULTSVer4:RECORD :=: DEFAULTSVer3:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(DEFAULTS, 3)
          DEFAULTSVer4:RetBackOrders = 1
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(DEFAULTS, 3)
          ROU:fCurrentFile &= DEFAULTSVer4
        END
        IF ROU:fCurrentFile &= DEFAULTSVer4
          ! Field "UseMultipleJobBookingDefaults" removed
          CLEAR(DEFAULTSVer5:RECORD)
          DEFAULTSVer5:RECORD :=: DEFAULTSVer4:RECORD
          ROU:fCurrentFile &= DEFAULTSVer5
        END
        IF ROU:fCurrentFile &= DEFAULTSVer5
          ! Field "TeamPerformanceTicker" added
          ! Field "TickerRefreshRate" added
          CLEAR(DEFAULTSVer6:RECORD)
          DEFAULTSVer6:RECORD :=: DEFAULTSVer5:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(DEFAULTS, 5)
          DEFAULTSVer6:RemoveWorkshopDespatch = 0
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(DEFAULTS, 5)
          ROU:fCurrentFile &= DEFAULTSVer6
        END
        IF ROU:fCurrentFile &= DEFAULTSVer6
          ! Field "UseOriginalBookingTemplate" added
          CLEAR(DEFAULTSVer7:RECORD)
          DEFAULTSVer7:RECORD :=: DEFAULTSVer6:RECORD
        END
        IF ROU:fCurrentFile &= MANFPALOVer1
          ! Key/Index "DescriptionKey" added
          CLEAR(MANFPALOVer2:RECORD)
          MANFPALOVer2:RECORD :=: MANFPALOVer1:RECORD
        END
        IF ROU:fCurrentFile &= MANFAUPAVer1
          ! Field "ForceFormat" added
          ! Field "FieldFormat" added
          ! Field "DateType" added
          CLEAR(MANFAUPAVer2:RECORD)
          MANFAUPAVer2:RECORD :=: MANFAUPAVer1:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(MANFAUPA, 1)
              MANFAUPAVer2:DateType = '@d6'
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(MANFAUPA, 1)
        END
        IF ROU:fCurrentFile &= SIDALERTVer1
          ! Field "EmailMessageText" removed
          CLEAR(SIDALERTVer2:RECORD)
          SIDALERTVer2:RECORD :=: SIDALERTVer1:RECORD
          ROU:fCurrentFile &= SIDALERTVer2
        END
        IF ROU:fCurrentFile &= SIDALERTVer2
          ! Key/Index "BookingTypeStatusKey" added
          CLEAR(SIDALERTVer3:RECORD)
          SIDALERTVer3:RECORD :=: SIDALERTVer2:RECORD
          ROU:fCurrentFile &= SIDALERTVer3
        END
        IF ROU:fCurrentFile &= SIDALERTVer3
          ! Field "Monday" changed (Monday STRING(20) -> Monday BYTE)
          ! Field "Tuesday" changed (Tuesday STRING(20) -> Tuesday BYTE)
          ! Field "Wednesday" changed (Wednesday STRING(20) -> Wednesday BYTE)
          ! Field "Thursday" changed (Thursday STRING(20) -> Thursday BYTE)
          ! Field "Friday" changed (Friday STRING(20) -> Friday BYTE)
          ! Field "Saturday" changed (Saturday STRING(20) -> Saturday BYTE)
          ! Field "Sunday" changed (Sunday STRING(20) -> Sunday BYTE)
          ! Field "SendTo" changed (SendTo STRING(20) -> SendTo BYTE)
          ! Field "SMSAlertActive" changed (SMSAlertActive STRING(20) -> SMSAlertActive BYTE)
          ! Field "CutOffTimeType" changed (CutOffTimeType STRING(20) -> CutOffTimeType BYTE)
          ! Field "BankHolidays" changed (BankHolidays STRING(20) -> BankHolidays BYTE)
          ! Field "EmailAlertActive" changed (EmailAlertActive STRING(20) -> EmailAlertActive BYTE)
          CLEAR(SIDALERTVer4:RECORD)
          SIDALERTVer4:RECORD :=: SIDALERTVer3:RECORD
          ROU:fCurrentFile &= SIDALERTVer4
        END
        IF ROU:fCurrentFile &= SIDALERTVer4
          ! Field "WithBattery" added
          ! Field "SystemAlert" added
          CLEAR(SIDALERTVer5:RECORD)
          SIDALERTVer5:RECORD :=: SIDALERTVer4:RECORD
          ROU:fCurrentFile &= SIDALERTVer5
        END
        IF ROU:fCurrentFile &= SIDALERTVer5
          ! Field "Status" changed (Status STRING(30) -> Status STRING(3))
          ! Field "CheckAccessories" added
          CLEAR(SIDALERTVer6:RECORD)
          SIDALERTVer6:RECORD :=: SIDALERTVer5:RECORD
          ROU:fCurrentFile &= SIDALERTVer6
        END
        IF ROU:fCurrentFile &= SIDALERTVer6
          ! Field "Retailer" added
          ! Key/Index "BookingRetailerStatusKey" added
          CLEAR(SIDALERTVer7:RECORD)
          SIDALERTVer7:RECORD :=: SIDALERTVer6:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(SIDALERT, 6)
            SIDALERTVer7:Retailer = 'VODAFONE' ! #6221 Prime the retailed to be 'VODAFONE' (DBH: 10/08/2015)
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(SIDALERT, 6)
        END
        IF ROU:fCurrentFile &= SIDALDEFVer1
          ! Field "SMSURL" added
          CLEAR(SIDALDEFVer2:RECORD)
          SIDALDEFVer2:RECORD :=: SIDALDEFVer1:RECORD
          ROU:fCurrentFile &= SIDALDEFVer2
        END
        IF ROU:fCurrentFile &= SIDALDEFVer2
          ! Field "SMSUsername" added
          ! Field "SMSPassword" added
          CLEAR(SIDALDEFVer3:RECORD)
          SIDALDEFVer3:RECORD :=: SIDALDEFVer2:RECORD
          ROU:fCurrentFile &= SIDALDEFVer3
        END
        IF ROU:fCurrentFile &= SIDALDEFVer3
          ! Field "ReplyToAddress" added
          CLEAR(SIDALDEFVer4:RECORD)
          SIDALDEFVer4:RECORD :=: SIDALDEFVer3:RECORD
          ROU:fCurrentFile &= SIDALDEFVer4
        END
        IF ROU:fCurrentFile &= SIDALDEFVer4
          ! Field "SMSFTPPath" added
          CLEAR(SIDALDEFVer5:RECORD)
          SIDALDEFVer5:RECORD :=: SIDALDEFVer4:RECORD
        END
        IF ROU:fCurrentFile &= SIDREMINVer1
          ! Field "CutOffTime" removed
          ! Field "CutOffTimeType" removed
          CLEAR(SIDREMINVer2:RECORD)
          SIDREMINVer2:RECORD :=: SIDREMINVer1:RECORD
        END
        IF ROU:fCurrentFile &= SIDRRCTVer1
          ! Attribute "NAME" changed (<Empty> -> 'SIDRRCT.DAT')
          CLEAR(SIDRRCTVer2:RECORD)
          SIDRRCTVer2:RECORD :=: SIDRRCTVer1:RECORD
        END
        IF ROU:fCurrentFile &= SIDMODTTVer1
          ! Key/Index "ModelNoKey" changed (ModelNoKey KEY(smt:ModelNo),NOCASE -> ModelNoKey KEY(smt:ModelNo),DUP,NOCASE)
          CLEAR(SIDMODTTVer2:RECORD)
          SIDMODTTVer2:RECORD :=: SIDMODTTVer1:RECORD
          ROU:fCurrentFile &= SIDMODTTVer2
        END
        IF ROU:fCurrentFile &= SIDMODTTVer2
          ! Field "ActiveTextRetail" added
          ! Field "TextRetail" added
          ! Field "ActiveTextExch" added
          ! Field "TextExch" added
          CLEAR(SIDMODTTVer3:RECORD)
          SIDMODTTVer3:RECORD :=: SIDMODTTVer2:RECORD
        END
        IF ROU:fCurrentFile &= SIDSRVCNVer1
          ! Field "FTPFailedCount" added
          ! Field "TurnaroundDaysB2B" changed label to "TurnaroundDaysExch"
          CLEAR(SIDSRVCNVer2:RECORD)
          SIDSRVCNVer2:RECORD :=: SIDSRVCNVer1:RECORD
          SIDSRVCNVer2:TurnaroundDaysExch = SIDSRVCNVer1:TurnaroundDaysB2B
          ROU:fCurrentFile &= SIDSRVCNVer2
        END
        IF ROU:fCurrentFile &= SIDSRVCNVer2
          ! Attribute "NAME" changed (<Empty> -> 'SIDSRVCN.DAT')
          CLEAR(SIDSRVCNVer3:RECORD)
          SIDSRVCNVer3:RECORD :=: SIDSRVCNVer2:RECORD
          ROU:fCurrentFile &= SIDSRVCNVer3
        END
        IF ROU:fCurrentFile &= SIDSRVCNVer3
          ! Field "MonRepairCapacity" added
          ! Field "TueRepairCapacity" added
          ! Field "WedRepairCapacity" added
          ! Field "ThuRepairCapacity" added
          ! Field "FriRepairCapacity" added
          CLEAR(SIDSRVCNVer4:RECORD)
          SIDSRVCNVer4:RECORD :=: SIDSRVCNVer3:RECORD
          ROU:fCurrentFile &= SIDSRVCNVer4
        END
        IF ROU:fCurrentFile &= SIDSRVCNVer4
          ! Field "ExchangeImportType" added
          CLEAR(SIDSRVCNVer5:RECORD)
          SIDSRVCNVer5:RECORD :=: SIDSRVCNVer4:RECORD
          ROU:fCurrentFile &= SIDSRVCNVer5
        END
        IF ROU:fCurrentFile &= SIDSRVCNVer5
          ! Field "AccessoryOrders" added
          CLEAR(SIDSRVCNVer6:RECORD)
          SIDSRVCNVer6:RECORD :=: SIDSRVCNVer5:RECORD
        END
        IF ROU:fCurrentFile &= SIDMODSCVer1
          ! Field "TurnaroundException" added
          ! Field "TurnaroundDaysRetail" added
          ! Field "TurnaroundDaysExch" added
          CLEAR(SIDMODSCVer2:RECORD)
          SIDMODSCVer2:RECORD :=: SIDMODSCVer1:RECORD
          ROU:fCurrentFile &= SIDMODSCVer2
        END
        IF ROU:fCurrentFile &= SIDMODSCVer2
          ! Key/Index "ModelTypeKey" added
          CLEAR(SIDMODSCVer3:RECORD)
          SIDMODSCVer3:RECORD :=: SIDMODSCVer2:RECORD
          ROU:fCurrentFile &= SIDMODSCVer3
        END
        IF ROU:fCurrentFile &= SIDMODSCVer3
          ! Field "TurnaroundException" removed
          ! Field "TurnaroundDaysRetail" removed
          ! Field "TurnaroundDaysExch" removed
          ! Key/Index "ModelTypeKey" removed
          CLEAR(SIDMODSCVer4:RECORD)
          SIDMODSCVer4:RECORD :=: SIDMODSCVer3:RECORD
        END
        IF ROU:fCurrentFile &= SIDNWDAYVer1
          ! Field "BookingType" removed
          ! Key/Index "SCDayKey" changed (SCBookTypeDayKey KEY(nwd:SCAccountID,nwd:BookingType,nwd:NonWorkingDay),DUP,NOCASE -> SCDayKey KEY(nwd:SCAccountID,nwd:NonWorkingDay),DUP,NOCASE)
          CLEAR(SIDNWDAYVer2:RECORD)
          SIDNWDAYVer2:RECORD :=: SIDNWDAYVer1:RECORD
        END
        IF ROU:fCurrentFile &= SMOREMINVer1
          ! Field "ModelNumber" added
          ! Key/Index "SIDREMINRecordNumberKey" changed (SIDREMINRecordNumberKey KEY(smm:SIDREMINRecordNumber),DUP,NOCASE -> SIDREMINRecordNumberKey KEY(smm:SIDREMINRecordNumber,smm:ModelNumber),DUP,NOCASE)
          CLEAR(SMOREMINVer2:RECORD)
          SMOREMINVer2:RECORD :=: SMOREMINVer1:RECORD
        END
        IF ROU:fCurrentFile &= MANFAULOVer1
          ! Key/Index "DescriptionKey" added
          CLEAR(MANFAULOVer2:RECORD)
          MANFAULOVer2:RECORD :=: MANFAULOVer1:RECORD
        END
        IF ROU:fCurrentFile &= RETSTOCKVer1
          ! Key/Index "DespatchedPartKey" added
          CLEAR(RETSTOCKVer2:RECORD)
          RETSTOCKVer2:RECORD :=: RETSTOCKVer1:RECORD
        END
        IF ROU:fCurrentFile &= ORDPARTSVer1
          ! Field "PartRecordNumber" added
          ! Key/Index "PartRecordNumberKey" added
          CLEAR(ORDPARTSVer2:RECORD)
          ORDPARTSVer2:RECORD :=: ORDPARTSVer1:RECORD
        END
        IF ROU:fCurrentFile &= LOCATIONVer1
          ! Field "PickNoteEnable" added
          CLEAR(LOCATIONVer2:RECORD)
          LOCATIONVer2:RECORD :=: LOCATIONVer1:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(LOCATION, 1)
              tmp:LocationNumber += 1
              LOCATIONVer2:RecordNumber   = tmp:LocationNumber
              LOCATIONVer2:Active         = 1
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(LOCATION, 1)
          ROU:fCurrentFile &= LOCATIONVer2
        END
        IF ROU:fCurrentFile &= LOCATIONVer2
          ! Field "Location" shifted up on 1 position(s)
          ! Field "Main_Store" shifted up on 1 position(s)
          ! Field "Active" shifted up on 1 position(s)
          ! Field "PickNoteEnable" shifted down on 3 position(s)
          CLEAR(LOCATIONVer3:RECORD)
          LOCATIONVer3:RECORD :=: LOCATIONVer2:RECORD
        END
        IF ROU:fCurrentFile &= JOBSVer1
          ! Key/Index "Surname_Key" changed (Surname_Key KEY(job:Surname),DUP,NOCASE -> Surname_Key KEY(job:Surname,job:Address_Line1),DUP,NOCASE)
          CLEAR(JOBSVer2:RECORD)
          JOBSVer2:RECORD :=: JOBSVer1:RECORD
          ROU:fCurrentFile &= JOBSVer2
        END
        IF ROU:fCurrentFile &= JOBSVer2
          ! Key/Index "Surname_Key" changed (Surname_Key KEY(job:Surname,job:Address_Line1),DUP,NOCASE -> Surname_Key KEY(job:Surname),DUP,NOCASE)
          CLEAR(JOBSVer3:RECORD)
          JOBSVer3:RECORD :=: JOBSVer2:RECORD
          ROU:fCurrentFile &= JOBSVer3
        END
        IF ROU:fCurrentFile &= JOBSVer3
          ! Field "RF_Board_IMEI" added
          CLEAR(JOBSVer4:RECORD)
          JOBSVer4:RECORD :=: JOBSVer3:RECORD
          ROU:fCurrentFile &= JOBSVer4
        END
        IF ROU:fCurrentFile &= JOBSVer4
          ! Field "RF_Board_IMEI" removed
          CLEAR(JOBSVer5:RECORD)
          JOBSVer5:RECORD :=: JOBSVer4:RECORD
        END
        IF ROU:fCurrentFile &= MODELNUMVer1
          ! Field "Dummy_Field" removed
          ! Field "ExchangeUnitMinLevel" added
          CLEAR(MODELNUMVer2:RECORD)
          MODELNUMVer2:RECORD :=: MODELNUMVer1:RECORD
          ROU:fCurrentFile &= MODELNUMVer2
        END
        IF ROU:fCurrentFile &= MODELNUMVer2
          ! Field "IsVodafoneSpecific" added
          CLEAR(MODELNUMVer3:RECORD)
          MODELNUMVer3:RECORD :=: MODELNUMVer2:RECORD
          ROU:fCurrentFile &= MODELNUMVer3
        END
        IF ROU:fCurrentFile &= MODELNUMVer3
          ! Field "IsCCentreSpecific" added
          CLEAR(MODELNUMVer4:RECORD)
          MODELNUMVer4:RECORD :=: MODELNUMVer3:RECORD
          ROU:fCurrentFile &= MODELNUMVer4
        END
        IF ROU:fCurrentFile &= MODELNUMVer4
          ! Field "UnsupportedExchange" added
          CLEAR(MODELNUMVer5:RECORD)
          MODELNUMVer5:RECORD :=: MODELNUMVer4:RECORD
          ROU:fCurrentFile &= MODELNUMVer5
        END
        IF ROU:fCurrentFile &= MODELNUMVer5
          ! Field "UnitCost" added
          CLEAR(MODELNUMVer6:RECORD)
          MODELNUMVer6:RECORD :=: MODELNUMVer5:RECORD
          ROU:fCurrentFile &= MODELNUMVer6
        END
        IF ROU:fCurrentFile &= MODELNUMVer6
          ! Field "VodafoneISPAccount" added
          CLEAR(MODELNUMVer7:RECORD)
          MODELNUMVer7:RECORD :=: MODELNUMVer6:RECORD
          ROU:fCurrentFile &= MODELNUMVer7
        END
        IF ROU:fCurrentFile &= MODELNUMVer7
          ! Field "OEM" added
          ! Field "OEMManufacturer" added
          CLEAR(MODELNUMVer8:RECORD)
          MODELNUMVer8:RECORD :=: MODELNUMVer7:RECORD
          ROU:fCurrentFile &= MODELNUMVer8
        END
        IF ROU:fCurrentFile &= MODELNUMVer8
          ! Field "KnownIssueDisplay" added
          CLEAR(MODELNUMVer9:RECORD)
          MODELNUMVer9:RECORD :=: MODELNUMVer8:RECORD
          ROU:fCurrentFile &= MODELNUMVer9
        END
        IF ROU:fCurrentFile &= MODELNUMVer9
          ! Field "RetailExchange" added
          ! Field "CBUPostal" added
          ! Field "CBUExchange" added
          ! Field "ISPPostal" added
          ! Field "ISPExchange" added
          ! Field "EBUPostal" added
          ! Field "EBUExchange" added
          CLEAR(MODELNUMVer10:RECORD)
          MODELNUMVer10:RECORD :=: MODELNUMVer9:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(MODELNUM, 9)
              !Set the Vodafone CBU / ISP / EBU defaults
              if MODELNUMVer10:IsCCentreSpecific = 1
                MODELNUMVer10:CBUPostal = 1
                MODELNUMVer10:CBUExchange = 1
              end
          
              if MODELNUMVer10:VodafoneISPAccount = 1
                MODELNUMVer10:ISPPostal = 1
                MODELNUMVer10:ISPExchange = 1
              end
          
              if MODELNUMVer10:UnsupportedExchange = 1
                MODELNUMVer10:CBUExchange = 0
                MODELNUMVer10:ISPExchange = 0
                MODELNUMVer10:EBUExchange = 0
              end
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(MODELNUM, 9)
          ROU:fCurrentFile &= MODELNUMVer10
        END
        IF ROU:fCurrentFile &= MODELNUMVer10
          ! Field "ModelSpecificURL1" added
          ! Field "ModelSpecificURL2" added
          ! Field "ModelSpecificURL3" added
          CLEAR(MODELNUMVer11:RECORD)
          MODELNUMVer11:RECORD :=: MODELNUMVer10:RECORD
          ROU:fCurrentFile &= MODELNUMVer11
        END
        IF ROU:fCurrentFile &= MODELNUMVer11
          ! Field "CCExchange28Day" added
          CLEAR(MODELNUMVer12:RECORD)
          MODELNUMVer12:RECORD :=: MODELNUMVer11:RECORD
          ROU:fCurrentFile &= MODELNUMVer12
        END
        IF ROU:fCurrentFile &= MODELNUMVer12
          ! Field "RetailRepairTier" added
          ! Field "ExchangeCharge" added
          ! Field "PostalRepairCharge" added
          ! Key/Index "RetailRepairTierKey" added
          CLEAR(MODELNUMVer13:RECORD)
          MODELNUMVer13:RECORD :=: MODELNUMVer12:RECORD
          ROU:fCurrentFile &= MODELNUMVer13
        END
        IF ROU:fCurrentFile &= MODELNUMVer13
          ! Field "Replacement28Day" added
          CLEAR(MODELNUMVer14:RECORD)
          MODELNUMVer14:RECORD :=: MODELNUMVer13:RECORD
          ROU:fCurrentFile &= MODELNUMVer14
        END
        IF ROU:fCurrentFile &= MODELNUMVer14
          ! Field "TabletDevice" added
          CLEAR(MODELNUMVer15:RECORD)
          MODELNUMVer15:RECORD :=: MODELNUMVer14:RECORD
          ROU:fCurrentFile &= MODELNUMVer15
        END
        IF ROU:fCurrentFile &= MODELNUMVer15
          ! Field "ReturnPeriod" added
          CLEAR(MODELNUMVer16:RECORD)
          MODELNUMVer16:RECORD :=: MODELNUMVer15:RECORD
          ROU:fCurrentFile &= MODELNUMVer16
        END
        IF ROU:fCurrentFile &= MODELNUMVer16
          ! Field "BERCharge" added
          CLEAR(MODELNUMVer17:RECORD)
          MODELNUMVer17:RECORD :=: MODELNUMVer16:RECORD
          ROU:fCurrentFile &= MODELNUMVer17
        END
        IF ROU:fCurrentFile &= MODELNUMVer17
          ! Field "Router" added
          CLEAR(MODELNUMVer18:RECORD)
          MODELNUMVer18:RECORD :=: MODELNUMVer17:RECORD
          ROU:fCurrentFile &= MODELNUMVer18
        END
        IF ROU:fCurrentFile &= MODELNUMVer18
          ! Field "AccessoryProduct" added
          ! Field "SerialisedProduct" added
          CLEAR(MODELNUMVer19:RECORD)
          MODELNUMVer19:RECORD :=: MODELNUMVer18:RECORD
          ROU:fCurrentFile &= MODELNUMVer19
        END
        IF ROU:fCurrentFile &= MODELNUMVer19
          ! Field "AccessoryRepair" added
          ! Field "AccessoryReturn" added
          CLEAR(MODELNUMVer20:RECORD)
          MODELNUMVer20:RECORD :=: MODELNUMVer19:RECORD
        END
        IF ROU:fCurrentFile &= STATUSVer1
          ! Field "EnableEmail" added
          ! Field "SenderEmailAddress" added
          ! Field "EmailSubject" added
          ! Field "EmailBody" added
          CLEAR(STATUSVer2:RECORD)
          STATUSVer2:RECORD :=: STATUSVer1:RECORD
          ROU:fCurrentFile &= STATUSVer2
        END
        IF ROU:fCurrentFile &= STATUSVer2
          ! Field "PickNoteEnable" added
          CLEAR(STATUSVer3:RECORD)
          STATUSVer3:RECORD :=: STATUSVer2:RECORD
          ROU:fCurrentFile &= STATUSVer3
        END
        IF ROU:fCurrentFile &= STATUSVer3
          ! Field "Status" shifted up on 1 position(s)
          ! Field "Ref_Number" shifted up on 1 position(s)
          ! Field "Use_Turnaround_Time" shifted up on 1 position(s)
          ! Field "Use_Turnaround_Days" shifted up on 1 position(s)
          ! Field "Turnaround_Days" shifted up on 1 position(s)
          ! Field "Use_Turnaround_Hours" shifted up on 1 position(s)
          ! Field "Turnaround_Hours" shifted up on 1 position(s)
          ! Field "Notes" shifted up on 1 position(s)
          ! Field "Loan" shifted up on 1 position(s)
          ! Field "Exchange" shifted up on 1 position(s)
          ! Field "Job" shifted up on 1 position(s)
          ! Field "SystemStatus" shifted up on 1 position(s)
          ! Field "EngineerStatus" shifted up on 1 position(s)
          ! Field "EnableEmail" shifted up on 1 position(s)
          ! Field "SenderEmailAddress" shifted up on 1 position(s)
          ! Field "EmailSubject" shifted up on 1 position(s)
          ! Field "EmailBody" shifted up on 1 position(s)
          ! Field "PickNoteEnable" shifted down on 17 position(s)
          CLEAR(STATUSVer4:RECORD)
          STATUSVer4:RECORD :=: STATUSVer3:RECORD
        END
        IF ROU:fCurrentFile &= TRADEACCVer1
          ! Field "WebPassword1" added
          ! Field "WebPassword2" added
          CLEAR(TRADEACCVer2:RECORD)
          TRADEACCVer2:RECORD :=: TRADEACCVer1:RECORD
          ROU:fCurrentFile &= TRADEACCVer2
        END
        IF ROU:fCurrentFile &= TRADEACCVer2
          ! Field "InvoiceAtDespatch" added
          ! Field "InvoiceType" added
          CLEAR(TRADEACCVer3:RECORD)
          TRADEACCVer3:RECORD :=: TRADEACCVer2:RECORD
          ROU:fCurrentFile &= TRADEACCVer3
        END
        IF ROU:fCurrentFile &= TRADEACCVer3
          ! Field "IndividualSummary" added
          CLEAR(TRADEACCVer4:RECORD)
          TRADEACCVer4:RECORD :=: TRADEACCVer3:RECORD
          ROU:fCurrentFile &= TRADEACCVer4
        END
        IF ROU:fCurrentFile &= TRADEACCVer4
          ! Field "RecordNumber" added
          ! Field "UseTradeContactNo" added
          ! Field "StopThirdParty" added
          ! Field "E1" added
          ! Field "E2" added
          ! Field "E3" added
          ! Field "UseDespatchDetails" added
          ! Field "AltTelephoneNumber" added
          ! Field "AltFaxNumber" added
          ! Field "AltEmailAddress" added
          ! Field "AutoSendStatusEmails" added
          ! Field "EmailRecipientList" added
          ! Field "EmailEndUser" added
          ! Key/Index "RecordNumberKey" added
          CLEAR(TRADEACCVer5:RECORD)
          TRADEACCVer5:RECORD :=: TRADEACCVer4:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(TRADEACC, 4)
              tmp:AutoTra += 1
              TRADEACCVer5:RecordNumber   = tmp:AutoTra
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(TRADEACC, 4)
          ROU:fCurrentFile &= TRADEACCVer5
        END
        IF ROU:fCurrentFile &= TRADEACCVer5
          ! Field "MakeSplitJob" added
          ! Field "SplitJobStatus" added
          ! Field "SplitExchangeStatus" added
          ! Field "SplitCChargeType" added
          ! Field "SplitWChargeType" added
          CLEAR(TRADEACCVer6:RECORD)
          TRADEACCVer6:RECORD :=: TRADEACCVer5:RECORD
          ROU:fCurrentFile &= TRADEACCVer6
        END
        IF ROU:fCurrentFile &= TRADEACCVer6
          ! Field "CheckChargeablePartsCost" added
          ! Field "MaxChargeablePartsCost" added
          CLEAR(TRADEACCVer7:RECORD)
          TRADEACCVer7:RECORD :=: TRADEACCVer6:RECORD
          ROU:fCurrentFile &= TRADEACCVer7
        END
        IF ROU:fCurrentFile &= TRADEACCVer7
          ! Field "UseAlternateDespatchNote" added
          CLEAR(TRADEACCVer8:RECORD)
          TRADEACCVer8:RECORD :=: TRADEACCVer7:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(TRADEACC, 7)
              TRADEACCVer8:E1 = 1
              TRADEACCVer8:E2 = 1
              TRADEACCVer8:E3 = 1
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(TRADEACC, 7)
        END
        IF ROU:fCurrentFile &= SUBTRACCVer1
          ! Field "WebPassword1" added
          ! Field "WebPassword2" added
          CLEAR(SUBTRACCVer2:RECORD)
          SUBTRACCVer2:RECORD :=: SUBTRACCVer1:RECORD
          ROU:fCurrentFile &= SUBTRACCVer2
        END
        IF ROU:fCurrentFile &= SUBTRACCVer2
          ! Field "InvoiceAtDespatch" added
          ! Field "InvoiceType" added
          CLEAR(SUBTRACCVer3:RECORD)
          SUBTRACCVer3:RECORD :=: SUBTRACCVer2:RECORD
          ROU:fCurrentFile &= SUBTRACCVer3
        END
        IF ROU:fCurrentFile &= SUBTRACCVer3
          ! Field "IndividualSummary" added
          CLEAR(SUBTRACCVer4:RECORD)
          SUBTRACCVer4:RECORD :=: SUBTRACCVer3:RECORD
          ROU:fCurrentFile &= SUBTRACCVer4
        END
        IF ROU:fCurrentFile &= SUBTRACCVer4
          ! Field "RecordNumber" added
          ! Field "UseTradeContactNo" added
          ! Field "E1" added
          ! Field "E2" added
          ! Field "E3" added
          ! Field "UseDespatchDetails" added
          ! Field "AltTelephoneNumber" added
          ! Field "AltFaxNumber" added
          ! Field "AltEmailAddress" added
          ! Field "UseAlternativeAdd" added
          ! Field "AutoSendStatusEmails" added
          ! Field "EmailRecipientList" added
          ! Field "EmailEndUser" added
          ! Key/Index "RecordNumberKey" added
          CLEAR(SUBTRACCVer5:RECORD)
          SUBTRACCVer5:RECORD :=: SUBTRACCVer4:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(SUBTRACC, 4)
              tmp:AutoSub += 1
              SUBTRACCVer5:RecordNumber   = tmp:AutoSub
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(SUBTRACC, 4)
          ROU:fCurrentFile &= SUBTRACCVer5
        END
        IF ROU:fCurrentFile &= SUBTRACCVer5
          ! Field "IsContactCentre" added
          ! Key/Index "Contact_Centre_Key" added
          CLEAR(SUBTRACCVer6:RECORD)
          SUBTRACCVer6:RECORD :=: SUBTRACCVer5:RECORD
          ROU:fCurrentFile &= SUBTRACCVer6
        END
        IF ROU:fCurrentFile &= SUBTRACCVer6
          ! Field "VodafoneRetailStore" added
          ! Key/Index "RetailStoreKey" added
          CLEAR(SUBTRACCVer7:RECORD)
          SUBTRACCVer7:RECORD :=: SUBTRACCVer6:RECORD
          ROU:fCurrentFile &= SUBTRACCVer7
        END
        IF ROU:fCurrentFile &= SUBTRACCVer7
          ! Field "VodafoneISPAccount" added
          CLEAR(SUBTRACCVer8:RECORD)
          SUBTRACCVer8:RECORD :=: SUBTRACCVer7:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(SUBTRACC, 7)
              SUBTRACCVer8:E1 = 1
              SUBTRACCVer8:E2 = 1
              SUBTRACCVer8:E3 = 1
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(SUBTRACC, 7)
        END
        IF ROU:fCurrentFile &= MANUFACTVer1
          ! Field "UseProductCode" added
          ! Field "QAParts" added
          ! Field "QANetwork" added
          CLEAR(MANUFACTVer2:RECORD)
          MANUFACTVer2:RECORD :=: MANUFACTVer1:RECORD
          ROU:fCurrentFile &= MANUFACTVer2
        END
        IF ROU:fCurrentFile &= MANUFACTVer2
          ! Field "DisallowNA" added
          CLEAR(MANUFACTVer3:RECORD)
          MANUFACTVer3:RECORD :=: MANUFACTVer2:RECORD
          ROU:fCurrentFile &= MANUFACTVer3
        END
        IF ROU:fCurrentFile &= MANUFACTVer3
          ! Field "IsVodafoneSpecific" added
          CLEAR(MANUFACTVer4:RECORD)
          MANUFACTVer4:RECORD :=: MANUFACTVer3:RECORD
          ROU:fCurrentFile &= MANUFACTVer4
        END
        IF ROU:fCurrentFile &= MANUFACTVer4
          ! Field "MSNFieldMask" added
          CLEAR(MANUFACTVer5:RECORD)
          MANUFACTVer5:RECORD :=: MANUFACTVer4:RECORD
          ROU:fCurrentFile &= MANUFACTVer5
        END
        IF ROU:fCurrentFile &= MANUFACTVer5
          ! Field "IsCCentreSpecific" added
          CLEAR(MANUFACTVer6:RECORD)
          MANUFACTVer6:RECORD :=: MANUFACTVer5:RECORD
          ROU:fCurrentFile &= MANUFACTVer6
        END
        IF ROU:fCurrentFile &= MANUFACTVer6
          ! Field "EDIFileType" added
          CLEAR(MANUFACTVer7:RECORD)
          MANUFACTVer7:RECORD :=: MANUFACTVer6:RECORD
          ! Before Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(MANUFACT, 6)
              !Set the EDI File Type to the same as the Manufacturer
              MANUFACTVer7:EDIFileType = MANUFACTVer7:Manufacturer
          ! After Embed Point: %AfterDeepAssigmentConversionProcessRoutine) DESC(DC Converter generator - ConversionProcess Routine: After Deep Assignment) ARG(MANUFACT, 6)
          ROU:fCurrentFile &= MANUFACTVer7
        END
        IF ROU:fCurrentFile &= MANUFACTVer7
          ! Field "VodafoneISPAccount" added
          CLEAR(MANUFACTVer8:RECORD)
          MANUFACTVer8:RECORD :=: MANUFACTVer7:RECORD
          ROU:fCurrentFile &= MANUFACTVer8
        END
        IF ROU:fCurrentFile &= MANUFACTVer8
          ! Field "OEM" added
          ! Field "OEMManufacturer" added
          CLEAR(MANUFACTVer9:RECORD)
          MANUFACTVer9:RECORD :=: MANUFACTVer8:RECORD
          ROU:fCurrentFile &= MANUFACTVer9
        END
        IF ROU:fCurrentFile &= MANUFACTVer9
          ! Field "OEM" removed
          ! Field "OEMManufacturer" removed
          CLEAR(MANUFACTVer10:RECORD)
          MANUFACTVer10:RECORD :=: MANUFACTVer9:RECORD
          ROU:fCurrentFile &= MANUFACTVer10
        END
        IF ROU:fCurrentFile &= MANUFACTVer10
          ! Field "KnownIssueDisplay" added
          CLEAR(MANUFACTVer11:RECORD)
          MANUFACTVer11:RECORD :=: MANUFACTVer10:RECORD
          ROU:fCurrentFile &= MANUFACTVer11
        END
        IF ROU:fCurrentFile &= MANUFACTVer11
          ! Field "VodafoneEBUAccount" added
          CLEAR(MANUFACTVer12:RECORD)
          MANUFACTVer12:RECORD :=: MANUFACTVer11:RECORD
        END
        IF ROU:fCurrentFile &= XMLDEFSVer1
          ! Field "UserCode" added
          CLEAR(XMLDEFSVer2:RECORD)
          XMLDEFSVer2:RECORD :=: XMLDEFSVer1:RECORD
          ROU:fCurrentFile &= XMLDEFSVer2
        END
        IF ROU:fCurrentFile &= XMLDEFSVer2
          ! Field "WarrantyChargeType" added
          CLEAR(XMLDEFSVer3:RECORD)
          XMLDEFSVer3:RECORD :=: XMLDEFSVer2:RECORD
          ROU:fCurrentFile &= XMLDEFSVer3
        END
        IF ROU:fCurrentFile &= XMLDEFSVer3
          ! Field "DropPointStatus" added
          ! Field "DropPointLocation" added
          CLEAR(XMLDEFSVer4:RECORD)
          XMLDEFSVer4:RECORD :=: XMLDEFSVer3:RECORD
        END
        IF ROU:fCurrentFile &= XMLSBVer1
          ! Field "FKStatusCode" changed (FKStatusCode STRING(2) -> FKStatusCode STRING(4))
          CLEAR(XMLSBVer2:RECORD)
          XMLSBVer2:RECORD :=: XMLSBVer1:RECORD
        END
        IF ROU:fCurrentFile &= MPXDEFSVer1
          ! Field "UserCode" added
          CLEAR(MPXDEFSVer2:RECORD)
          MPXDEFSVer2:RECORD :=: MPXDEFSVer1:RECORD
          ROU:fCurrentFile &= MPXDEFSVer2
        END
        IF ROU:fCurrentFile &= MPXDEFSVer2
          ! Field "IsChargeable" added
          ! Field "IsWarranty" added
          ! Field "Status" added
          ! Field "Location" added
          CLEAR(MPXDEFSVer3:RECORD)
          MPXDEFSVer3:RECORD :=: MPXDEFSVer2:RECORD
          ROU:fCurrentFile &= MPXDEFSVer3
        END
        IF ROU:fCurrentFile &= MPXDEFSVer3
          ! Field "CollectionStatus" removed
          ! Field "DropOffStatus" removed
          ! Field "DropPointStatus" removed
          ! Field "DropPointLocation" removed
          CLEAR(MPXDEFSVer4:RECORD)
          MPXDEFSVer4:RECORD :=: MPXDEFSVer3:RECORD
          ROU:fCurrentFile &= MPXDEFSVer4
        END
        IF ROU:fCurrentFile &= MPXDEFSVer4
          ! Field "EnvCountStatus" added
          CLEAR(MPXDEFSVer5:RECORD)
          MPXDEFSVer5:RECORD :=: MPXDEFSVer4:RECORD
          ROU:fCurrentFile &= MPXDEFSVer5
        END
        IF ROU:fCurrentFile &= MPXDEFSVer5
          ! Key/Index "AccountNoKey" changed (AccountNoKey KEY(MXD:AccountNo),NOCASE,OPT,PRIMARY -> AccountNoKey KEY(MXD:AccountNo),NOCASE,PRIMARY)
          CLEAR(MPXDEFSVer6:RECORD)
          MPXDEFSVer6:RECORD :=: MPXDEFSVer5:RECORD
        END
        IF ROU:fCurrentFile &= XMLSAMVer1
          ! Field "Description" removed
          ! Field "StatusCode" changed (StatusCode STRING(2) -> StatusCode STRING(4))
          CLEAR(XMLSAMVer2:RECORD)
          XMLSAMVer2:RECORD :=: XMLSAMVer1:RECORD
        END
        IF ROU:fCurrentFile &= CARISMAVer2
          IF ~CARISMAVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFEMAILVer3
          IF ~DEFEMAILVer4:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= REGIONSVer2
          IF ~REGIONSVer3:RegionID
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~REGIONSVer3:RegionName
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= ACCREGVer2
          IF ~ACCREGVer3:AccRegID
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~ACCREGVer3:AccountNo
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDACREGVer1
          IF ~SIDACREGVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= XMLJOBSVer16
          IF ~XMLJOBSVer17:RECORD_NUMBER
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MULDESPVer1
          IF ~MULDESPVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= IMEISHIPVer3
          IF ~IMEISHIPVer4:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= JOBTHIRDVer1
          IF ~JOBTHIRDVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOGRETRNVer1
          IF ~LOGRETRNVer2:RefNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= PRODCODEVer1
          IF ~PRODCODEVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~PRODCODEVer2:ProductCode
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOGSTOCKVer1
          IF ~LOGSTOCKVer2:RefNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~LOGSTOCKVer2:SalesCode AND ~LOGSTOCKVer2:ModelNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFEMAB2Ver1
          IF ~DEFEMAB2Ver2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFEMAB3Ver1
          IF ~DEFEMAB3Ver2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFEMAB4Ver1
          IF ~DEFEMAB4Ver2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFEMAB5Ver1
          IF ~DEFEMAB5Ver2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFEMAE2Ver1
          IF ~DEFEMAE2Ver2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFEMAE3Ver1
          IF ~DEFEMAE3Ver2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFEMAB1Ver1
          IF ~DEFEMAB1Ver2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFEMAE1Ver5
          IF ~DEFEMAE1Ver6:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFEMAI2Ver8
          IF ~DEFEMAI2Ver9:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFSIDEXVer2
          IF ~DEFSIDEXVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= RETSALESVer2
          IF ~RETSALESVer3:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= TEAMSVer1
          IF ~TEAMSVer2:Record_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~TEAMSVer2:Team
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MERGEVer1
          IF ~MERGEVer2:RefNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~MERGEVer2:FieldName
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDDEFVer5
          IF ~SIDDEFVer6:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= ORDPENDVer3
          IF ~ORDPENDVer4:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= STOHISTVer1
          IF ~STOHISTVer2:Record_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= EXREASONVer1
          IF ~EXREASONVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= REPTYDEFVer1
          IF ~REPTYDEFVer2:Repair_Type
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= JOBSEVer9
          IF ~JOBSEVer10:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MANFAULTVer3
          IF ~MANFAULTVer4:Manufacturer AND ~MANFAULTVer4:Field_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= PICKNOTEVer4
          IF ~PICKNOTEVer5:PickNoteRef
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= COURIERVer1
          IF ~COURIERVer2:Courier
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= ESNMODELVer1
          IF ~ESNMODELVer2:Record_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= STOCKVer3
          IF ~STOCKVer4:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOGSTOLCVer1
          IF ~LOGSTOLCVer2:RefNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~LOGSTOLCVer2:Location
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOGSERSTVer1
          IF ~LOGSERSTVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOGSTLOCVer1
          IF ~LOGSTLOCVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~LOGSTLOCVer2:RefNumber AND ~LOGSTLOCVer2:Location
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= TRAFAULOVer1
          IF ~TRAFAULOVer2:AccountNumber AND ~TRAFAULOVer2:Field_Number AND ~TRAFAULOVer2:Field AND ~TRAFAULOVer2:Description
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MPXJOBSVer5
          IF ~MPXJOBSVer6:RecordNo
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MPXSTATVer2
          IF ~MPXSTATVer3:RecordNo
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= TRAFAULTVer2
          IF ~TRAFAULTVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~TRAFAULTVer3:AccountNumber AND ~TRAFAULTVer3:Field_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= STATCRITVer2
          IF ~STATCRITVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= NOTESDELVer1
          IF ~NOTESDELVer2:ReasonCode
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= PICKDETVer5
          IF ~PICKDETVer6:PickDetailRef
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= USERSVer1
          IF ~USERSVer2:User_Code
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~USERSVer2:Password
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDREGVer1
          IF ~SIDREGVer2:SIDRegID
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= PENDMAILVer13
          IF ~PENDMAILVer14:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= DEFAULTSVer6
          IF ~DEFAULTSVer7:record_number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MANFPALOVer1
          IF ~MANFPALOVer2:Manufacturer AND ~MANFPALOVer2:Field_Number AND ~MANFPALOVer2:Field
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MANFAUPAVer1
          IF ~MANFAUPAVer2:Manufacturer AND ~MANFAUPAVer2:Field_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDALERTVer6
          IF ~SIDALERTVer7:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDALDEFVer4
          IF ~SIDALDEFVer5:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDREMINVer1
          IF ~SIDREMINVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDRRCTVer1
          IF ~SIDRRCTVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDMODTTVer2
          IF ~SIDMODTTVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDSRVCNVer5
          IF ~SIDSRVCNVer6:SCAccountID
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDMODSCVer3
          IF ~SIDMODSCVer4:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~SIDMODSCVer4:Manufacturer AND ~SIDMODSCVer4:ModelNo AND ~SIDMODSCVer4:BookingType
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SIDNWDAYVer1
          IF ~SIDNWDAYVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SMOREMINVer1
          IF ~SMOREMINVer2:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MANFAULOVer1
          IF ~MANFAULOVer2:Manufacturer AND ~MANFAULOVer2:Field_Number AND ~MANFAULOVer2:Field
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= RETSTOCKVer1
          IF ~RETSTOCKVer2:Record_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= ORDPARTSVer1
          IF ~ORDPARTSVer2:Record_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= LOCATIONVer2
          IF ~LOCATIONVer3:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~LOCATIONVer3:Location
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= JOBSVer4
          IF ~JOBSVer5:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MODELNUMVer19
          IF ~MODELNUMVer20:Model_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= STATUSVer3
          IF ~STATUSVer4:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~STATUSVer4:Heading_Ref_Number AND ~STATUSVer4:Status
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~STATUSVer4:Heading_Ref_Number AND ~STATUSVer4:Ref_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= TRADEACCVer7
          IF ~TRADEACCVer8:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~TRADEACCVer8:Account_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= SUBTRACCVer7
          IF ~SUBTRACCVer8:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~SUBTRACCVer8:Main_Account_Number AND ~SUBTRACCVer8:Account_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~SUBTRACCVer8:Account_Number
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MANUFACTVer11
          IF ~MANUFACTVer12:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
          IF ~MANUFACTVer12:Manufacturer
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= XMLDEFSVer3
          IF ~XMLDEFSVer4:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= XMLSBVer1
          IF ~XMLSBVer2:SBStatus
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= MPXDEFSVer5
          IF ~MPXDEFSVer6:AccountNo
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= XMLSAMVer1
          IF ~XMLSAMVer2:StatusCode
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF LOC:byTPSDriver
          ADD(LOC:rfDestFileLabel)
        ELSE
          APPEND(LOC:rfDestFileLabel)
        END
        IF ERRORCODE()
          DO PreReturn
          ROU:byError = 1
          POST(EVENT:Completed)
          BREAK
        END
        IF LOC:byTPSDriver
          ROU:lConvertedRecNum += 1
          IF ROU:lConvertedRecNum % 1000 = 0
            COMMIT
            IF ERRORCODE()
              DO PreReturn
              ROU:byError = 1
              POST(EVENT:Completed)
              BREAK
            END
            LOC:byTransactionActive = False
            LOGOUT(1,LOC:rfDestFileLabel)
            IF ERRORCODE()
              DO PreReturn
              ROU:byError = 1
              POST(EVENT:Completed)
              BREAK
            END
            LOC:byTransactionActive = True
          END
        END
        RecordProcessed += 1
        DO DisplaySolidProgressBar
        InfoString = 'Records processed ' & RecordProcessed & ' from ' & RecordTotal
        DISPLAY
        ROU:lRecordsThisCycle += 1
      END
    END
  END
  IF ROU:byError = 0
    IF LOC:byTPSDriver
      COMMIT
      IF ERRORCODE() THEN DO PreReturn; EXIT.
      LOC:byTransactionActive = False
    ELSE
      InfoString = 'Now rebuild keys...'
      DISPLAY
      IF ~LOC:byDestSQLDriver THEN FLUSH(LOC:rfDestFileLabel).
      BUILD(LOC:rfDestFileLabel)
    END

    ReturnCode = 0
    LocalResponse = RequestCompleted
    DO PreReturn
  END
  EXIT
!----------------------------------------------------------

PreReturn ROUTINE
  DATA
ROU:lErrorCode        LONG,AUTO
ROU:stTempFile        STRING(260),AUTO
  CODE
  SETCURSOR
  IF ReturnCode
    IF LOC:byTPSDriver AND LOC:byTransactionActive
      ROU:lErrorCode = ERRORCODE()
      ROLLBACK
      DC:SetError(ROU:lErrorCode)
      DC:SetErrorFile(LOC:rfSourceFileLabel)
    END
    IF ~LOC:byDestSQLDriver AND ~LOC:byTPSDriver THEN FLUSH(LOC:rfDestFileLabel).
    FileErrorBox()
  END
  CLOSE(LOC:rfSourceFileLabel)
  CLOSE(LOC:rfDestFileLabel)
  IF ReturnCode
    DO RemoveTempFile
  ELSE
    IF LOC:bySourceSQLDriver
      ROU:stTempFile = LOC:stSourceFileName
      CreateTempName(ROU:stTempFile,FORMAT(Version,@N03),LOC:rfSourceFileLabel)
      DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    ELSE
      ROU:stTempFile = LOC:stSourceFileName
      CreateTempName(ROU:stTempFile,FORMAT(Version,@N03),LOC:rfSourceFileLabel)
      RemoveFiles(LOC:rfSourceFileLabel,ROU:stTempFile)
      IF ~PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:stTargetFileName,DummyFileName1)
        IF PrepareMultiTableFileName(LOC:rfSourceFileLabel,ROU:stTempFile,DummyFileName1)
          IF PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:rfSourceFileLabel{PROP:Name},DummyFileName1)
            InfoString = 'File copying...'
            DISPLAY
          END
        END
      END
      IF RenameDestFile(LOC:rfSourceFileLabel,ROU:stTempFile,LOC:stTargetFileName)
        ROU:lErrorCode = ERRORCODE()
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    END
    IF LOC:byDestSQLDriver
      LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
      DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        IF LOC:bySourceSQLDriver
          DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
        ELSE
          LOC:rfSourceFileLabel{PROP:Name} = ROU:stTempFile
          RenameFile(LOC:rfSourceFileLabel,LOC:stSourceFileName)
        END
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    ELSE
      LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
      RemoveFiles(LOC:rfDestFileLabel,LOC:stTargetFileName)
      RenameFile(LOC:rfDestFileLabel,LOC:stTargetFileName)
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        IF LOC:bySourceSQLDriver
          DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
        ELSE
          LOC:rfSourceFileLabel{PROP:Name} = ROU:stTempFile
          RenameFile(LOC:rfSourceFileLabel,LOC:stSourceFileName)
        END
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    END
    IF LOC:byDestSQLDriver
      DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
    ELSE
      RemoveFiles(LOC:rfDestFileLabel,LOC:stDestFileName)
    END
  END
  EXIT
!----------------------------------------------------------

RemoveTempFile ROUTINE
  IF LOC:byDestSQLDriver
    DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
  ELSE
    LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
    REMOVE(LOC:rfDestFileLabel)
  END
  EXIT
!----------------------------------------------------------


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ConvertFiles',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'ConvertFiles',1)
    SolaceViewVars('tmp:LocationNumber',tmp:LocationNumber,'ConvertFiles',1)
    SolaceViewVars('tmp:AutoSub',tmp:AutoSub,'ConvertFiles',1)
    SolaceViewVars('tmp:AutoTra',tmp:AutoTra,'ConvertFiles',1)
    SolaceViewVars('OriginalRequest',OriginalRequest,'ConvertFiles',1)
    SolaceViewVars('LocalResponse',LocalResponse,'ConvertFiles',1)
    SolaceViewVars('FilesOpened',FilesOpened,'ConvertFiles',1)
    SolaceViewVars('WindowOpened',WindowOpened,'ConvertFiles',1)
    SolaceViewVars('WindowInitialized',WindowInitialized,'ConvertFiles',1)
    SolaceViewVars('ForceRefresh',ForceRefresh,'ConvertFiles',1)
    SolaceViewVars('ReturnCode',ReturnCode,'ConvertFiles',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RecordProcessed;  SolaceCtrlName = '?RecordProcessed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InfoString;  SolaceCtrlName = '?InfoString';Add(LSolCtrlQ,+SolaceUseRef)



InitalizeSolidProgressBar ROUTINE
  DATA
ROU:lPercentControlWidth   LONG
ROU:lPercentControlHeight  LONG
ROU:lProgressControlHeight LONG
ROU:byOldPropPixels        BYTE
  CODE
  ROU:byOldPropPixels = window{PROP:Pixels}
  window{PROP:Buffer} = True
  window{PROP:Pixels} = True
  ?RecordProcessed{PROP:Hide} = True

  SPBBorderControl = CREATE(0,CREATE:Panel,?RecordProcessed{PROP:Parent})
  SPBBorderControl{PROP:Xpos}   = ?RecordProcessed{PROP:Xpos}
  SPBBorderControl{PROP:Ypos}   = ?RecordProcessed{PROP:Ypos}
  SPBBorderControl{PROP:Width}  = ?RecordProcessed{PROP:Width}
  SPBBorderControl{PROP:Height} = ?RecordProcessed{PROP:Height}
  IF ?RecordProcessed{PROP:Background} <> 0FFFFFFFFH
    SPBBorderControl{PROP:Fill} = ?RecordProcessed{PROP:Background}
  END
  SPBBorderControl{PROP:BevelOuter} = -1
  SPBBorderControl{PROP:Hide} = False

  SPBFillControl = CREATE(0,CREATE:Box,?RecordProcessed{PROP:Parent})
  SPBFillControl{PROP:Xpos}   = ?RecordProcessed{PROP:Xpos} + 1
  SPBFillControl{PROP:Ypos}   = ?RecordProcessed{PROP:Ypos} + 1
  SPBFillControl{PROP:Width}  = 0
  SPBFillControl{PROP:Height} = ?RecordProcessed{PROP:Height} - 1
  IF ?RecordProcessed{PROP:SelectedFillColor} <> 0FFFFFFFFH
    SPBFillControl{PROP:Fill} = ?RecordProcessed{PROP:SelectedFillColor}
  ELSE
    SPBFillControl{PROP:Fill} = 08000000DH
  END
  SPBFillControl{PROP:Hide} = False

  SPBPercentControl = CREATE(0,CREATE:String,?RecordProcessed{PROP:Parent})
  SPBPercentControl{PROP:FontStyle} = FONT:bold
  SPBPercentControl{PROP:FontSize}  = ?RecordProcessed{PROP:Height}
  LOOP
    ROU:lPercentControlHeight  = SPBPercentControl{PROP:Height}
    ROU:lProgressControlHeight = ?RecordProcessed{PROP:Height}
    IF ROU:lPercentControlHeight <= ROU:lProgressControlHeight OR SPBPercentControl{PROP:FontSize} = 1
      BREAK
    END
    IF SPBPercentControl{PROP:FontSize} < 8
      SPBPercentControl{PROP:FontName} = 'Small Fonts'
    END
    SPBPercentControl{PROP:FontSize} = SPBPercentControl{PROP:FontSize} - 1
  END

  SPBPercentControl{PROP:Text}      = '100%'
  ROU:lPercentControlWidth          = SPBPercentControl{PROP:Width}
  SPBPercentControl{PROP:Xpos}      = ?RecordProcessed{PROP:Xpos} + (?RecordProcessed{PROP:Width} - ROU:lPercentControlWidth) / 2
  SPBPercentControl{PROP:Width}     = ROU:lPercentControlWidth
  SPBPercentControl{PROP:Ypos}      = ?RecordProcessed{PROP:Ypos} + (?RecordProcessed{PROP:Height} - SPBPercentControl{PROP:Height}) / 2 + 1
  IF ?RecordProcessed{PROP:SelectedColor} <> 0FFFFFFFFH
    SPBPercentControl{PROP:FontColor} = ?RecordProcessed{PROP:SelectedColor}
  ELSE
    SPBPercentControl{PROP:FontColor} = 0FFFFFFH
  END
  SPBPercentControl{PROP:Trn}       = True
  SPBPercentControl{PROP:Center}    = True
  SPBPercentControl{PROP:Text}      = '0%'
  SPBPercentControl{PROP:Hide}      = False
  window{PROP:Pixels} = ROU:byOldPropPixels
  EXIT
!---------------------------------------------------------------------

DisplaySolidProgressBar ROUTINE
  DATA
ROU:lRangeLow  LONG,AUTO
ROU:lRangeHigh LONG,AUTO
ROU:lPercent   LONG,AUTO
ROU:lFillBar   LONG,AUTO
  CODE
  IF SaveRecordProcessed <> RecordProcessed
    ROU:lRangeLow  = ?RecordProcessed{PROP:RangeLow}
    ROU:lRangeHigh = ?RecordProcessed{PROP:RangeHigh}
    IF RecordProcessed <= ROU:lRangeHigh
      ROU:lFillBar = (RecordProcessed / (ROU:lRangeHigh - ROU:lRangeLow)) * (?RecordProcessed{PROP:Width} - 1)
      IF ROU:lFillBar <> SaveFillBar
        SPBFillControl{PROP:Width} = ROU:lFillBar
        DISPLAY(SPBFillControl)
        SaveFillBar = ROU:lFillBar
      END
      ROU:lPercent = (RecordProcessed / (ROU:lRangeHigh - ROU:lRangeLow)) * 100
      IF ROU:lPercent <> SavePercent
        SPBPercentControl{PROP:Text} = FORMAT(ROU:lPercent,@N3) & '%'
        DISPLAY(SPBPercentControl)
        SavePercent = ROU:lPercent
      END
      SaveRecordProcessed = RecordProcessed
    ELSE
      RecordProcessed = ROU:lRangeHigh
    END
  END
  EXIT
!---------------------------------------------------------------------
