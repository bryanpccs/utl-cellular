

   MEMBER('cellular.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('CELLU007.INC'),ONCE        !Local module procedure declarations
                     END


GetVersionNumber     PROCEDURE                        ! Declare Procedure
VersionNo            STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'GetVersionNumber')      !Add Procedure to Log
  end


    Relate:DEFAULTV.Open()
    set(DEFAULTV)
    access:DEFAULTV.next()
    VersionNo = defv:VersionNumber
    Relate:DEFAULTV.Close()

    RETURN  VersionNo


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'GetVersionNumber',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('VersionNo',VersionNo,'GetVersionNumber',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
