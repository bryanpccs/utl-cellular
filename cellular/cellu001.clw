

   MEMBER('cellular.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetTalk.inc'),ONCE

                     MAP
                       INCLUDE('CELLU001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELLU007.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Frame

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
LocalRequest         LONG
DDEServerNumber      LONG
DdeAppName           STRING(20)
ComputerName         CSTRING(20)
ComputerNameLength   ULONG
save_tra_id          USHORT,AUTO
save_sub_id          USHORT,AUTO
tmp:PopupString      CSTRING(255)
save_job_id          USHORT,AUTO
start_help_temp      STRING(255)
pos                  STRING(255)
loc:lastcount        LONG
loc:lasttime         LONG
FilesOpened          BYTE
save_ope_id          USHORT,AUTO
save_sup_id          USHORT,AUTO
CurrentTab           STRING(80)
FP::PopupString      STRING(4096)
FP::ReturnValues     QUEUE,PRE(FP:)
RVPtr                LONG
                     END
comp_name_temp       STRING(255)
AllowUserToExit      BYTE
Date_Temp            STRING(60)
Time_Temp            STRING(60)
SRCF::Version        STRING(512)
SRCF::Format         STRING(512)
CPCSExecutePopUp     BYTE
BackerDDEName        STRING(30)
FrameTitle           CSTRING(128)
OtherPgmHwnd         Hwnd !Handle=unsigned=hwnd
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
AppFrame             APPLICATION('ServiceBase 2000 Cellular Progress System'),AT(,,679,415),FONT('Tahoma',8,,),CENTER,HVSCROLL,ICON('Pc.ico'),ALRT(F10Key),ALRT(MouseRight),STATUS(-1,-1,-1,0),WALLPAPER('UTLBack.jpg'),CENTERED,SYSTEM,MAX,MAXIMIZE,RESIZE,IMM
                       MENUBAR
                         MENU('File'),USE(?File),FIRST
                           ITEM('&Print Setup ...'),AT(,,,12),USE(?PrintSetup),MSG('Setup printer'),STD(STD:PrintSetup),ICON('printsm.gif')
                           ITEM,SEPARATOR
                           ITEM('&Re-Login'),AT(,,,12),USE(?ReLogin),ICON('disksm.gif')
                           ITEM,SEPARATOR
                           MENU('Themes'),AT(,,,12),USE(?FileThemes),ICON('stopsm.gif')
                             ITEM('Disable Theme'),USE(?FileThemesMenu)
                             ITEM('Re-enable Theme'),USE(?FileThemesReenableTheme)
                           END
                           ITEM,SEPARATOR
                           ITEM('E&xit'),AT(,,,12),USE(?Exit),MSG('Exit this application'),STD(STD:Close),ICON('exitsm.gif')
                         END
                         MENU('&Edit')
                           ITEM('Cu&t'),USE(?Cut),MSG('Remove item to Windows Clipboard'),STD(STD:Cut)
                           ITEM('&Copy'),USE(?Copy),MSG('Copy item to Windows Clipboard'),STD(STD:Copy)
                           ITEM('&Paste'),USE(?Paste),MSG('Paste contents of Windows Clipboard'),STD(STD:Paste)
                         END
                         MENU('&System Administration'),USE(?SystemAdministration)
                           MENU('System Defaults'),AT(,,120,12),USE(?SystemDefaults),ICON('ordersm.gif')
                             ITEM('License Defaults'),USE(?LicenseDefaults)
                             MENU('General Defaults'),USE(?GeneralDefaults)
                               ITEM('Main Defaults'),USE(?MainDefaults)
                               ITEM('Compulsory Fields'),USE(?CompulsoryFields)
                               ITEM('Standard Texts'),USE(?StandardTexts)
                               ITEM('Printing Defaults'),USE(?PrintingDefaults)
                               ITEM('Booking Defaults'),USE(?BookingDefaults)
                               MENU('Email Defaults'),USE(?EmailDefaults)
                                 ITEM('Server Defaults'),USE(?ServerDefaults)
                                 ITEM('Recipient Types'),USE(?RecipientTypes)
                               END
                               ITEM('Computer Location Defaults'),USE(?ComputerDefaults)
                               ITEM,SEPARATOR
                               ITEM('Stock Control Defaults'),USE(?StockControlDefaults)
                               ITEM('Retail Sales Defaults'),USE(?RetailSalesDefaults)
                             END
                             ITEM('Sage Defaults'),USE(?SageDefaults)
                             MENU('Import Defaults'),USE(?SystemAdminstrationSystemDefaultsImportDefaults)
                               ITEM('CRC Defaults'),USE(?CRCDefaults)
                               ITEM('Samsung Defaults'),USE(?SamsungDefaults)
                               ITEM('Siemens Defaults'),USE(?SiemensDefaults)
                               ITEM('Vodafone Defaults'),USE(?VodafoneDefaults)
                               ITEM('Web Defaults'),USE(?WebDefaults)
                             END
                             MENU('SID Defaults'),USE(?SystemAdministrationSystemDefaultsSIDDefaults)
                               ITEM('SID Interface Defaults'),USE(?SIDInterfaceDefaults)
                               ITEM('SID Service Centres'),USE(?SIDServiceCentres)
                               ITEM('Vodafone Account Regions'),USE(?VodafoneAccountRegions)
                               ITEM('SID Alert Defaults'),USE(?SIDAlertDefaults_OLD),HIDE
                               ITEM('IMEI Import FTP Defaults'),USE(?SystemAdministrationSystemDefaultsSIDDefaultsIM)
                               ITEM('Automated SID Extract Defaults'),USE(?AutomatedSIDExtractDefaults)
                               ITEM('Store Performance Report Defaults'),USE(?StorePerformanceReportDefaults)
                               ITEM('SID Alert Defaults'),USE(?SIDAlertDefaults)
                               ITEM('Retail Repair Charge Tiers'),USE(?RetailRepairChargeTiers)
                             END
                           END
                           MENU('User Defaults'),AT(,,,12),USE(?UserDefaults),ICON('menqsm.gif')
                             ITEM('Access Levels'),USE(?accesslevels)
                             ITEM('System Users'),USE(?users)
                           END
                           MENU('Financial Defaults'),AT(,,,12),USE(?FinancialDefaults),ICON('moneysm.gif')
                             ITEM('Charge Rate Defaults'),USE(?ChargeRateDefaults),MSG('Browse Default Charges')
                             ITEM('Charge Types'),USE(?ChargeTypes)
                             ITEM('Repair Types'),USE(?RepairTypes)
                             ITEM('V.A.T. Codes'),USE(?VATCodes)
                             MENU('Utilities'),USE(?SystemAdminstrationFinancialDefaultsUtilities)
                               ITEM('Reprice Jobs Routine'),USE(?RepriceJobsRoutine)
                             END
                           END
                           MENU('Stock Control Defaults'),AT(,,,12),USE(?SystemAdminstrationStockControlDefaults),ICON('stocksm.gif')
                             ITEM('Manufacturers'),USE(?manufacturers)
                             ITEM('Suppliers'),USE(?Suppliers)
                             ITEM('Site Locations'),USE(?SiteLocations)
                             ITEM('Internal Locations'),USE(?InternalLocations)
                             ITEM('Loan/Exchange Stock Type'),USE(?LoanExchangeStockType)
                             ITEM('Model Prices Export/Import'),USE(?SystemAdministrationStockControlDefaultsModelPr)
                           END
                           MENU('Trade Account Defaults'),AT(,,,12),USE(?TradeAccountDefaults),ICON('trucksm.gif')
                             ITEM('Browse Trade Header Accounts'),USE(?TradeAccounts),MSG('Browse Trade Accounts')
                             ITEM('Browse Trade Sub Accounts'),USE(?BrowseTradeSubAccounts)
                             MENU('Trade Account Utilities'),USE(?TradeAccountUtilities)
                               ITEM('Auto-Change Courier'),USE(?AutoChangeCourier)
                             END
                           END
                           MENU('General Lookups'),AT(,,,12),USE(?Lookups),ICON('findersm.gif')
                             ITEM('Accessories'),USE(?accessories)
                             ITEM('Action'),USE(?Action)
                             ITEM('Colours'),USE(?Colours)
                             MENU('Couriers'),USE(?SystemAdminstrationGeneralLookupsCouries)
                               ITEM('Couriers'),USE(?couriers)
                               ITEM('City Link Services'),USE(?CityLinkServices)
                             END
                             ITEM('Job Turnaround Times'),USE(?JobTurnaroundTimes)
                             ITEM('Initial Transit Types'),USE(?InitialTransitTypes)
                             ITEM('Letters'),USE(?Letters)
                             ITEM('Mail Merge Letters'),USE(?MailMergeLetters)
                             ITEM('Networks'),USE(?Networks)
                             ITEM('Payment Types'),USE(?PaymentTypes)
                             ITEM('QA Failure Reasons'),USE(?QAFailureReasons)
                             ITEM('Query Reasons'),USE(?QueryReasons)
                             MENU('Status Types'),USE(?SystemAdminstrationGeneralLookupsStatusTypes)
                               ITEM('Status Types'),USE(?StatusTypes)
                               ITEM('Turnaround Time Utility'),USE(?TurnaroundTimeUtility)
                             END
                             ITEM('Teams'),USE(?Teams)
                             ITEM('Third Party Repairers'),USE(?thirdPartyRepairers)
                             ITEM('Unit Types'),USE(?UnitTypes)
                           END
                         END
                         MENU('&Browse'),USE(?Browse)
                           ITEM('Contacts'),AT(,,120,12),USE(?Contacts),ICON('clipbrsm.gif')
                           MENU('Common Faults'),AT(,,,12),USE(?BrowseCommonFaults),ICON('faultsm.gif')
                             ITEM('Common Faults Categories'),USE(?CommonFaultsCategories)
                             ITEM('Common Faults List'),USE(?CommonFaultsList)
                           END
                           ITEM('Loan Units'),AT(,,,12),USE(?LoanUnits),ICON('stockqsm.gif')
                           MENU('Exchange Units'),AT(,,,12),USE(?BrowseExchangeUnits),ICON('stockqsm.gif')
                             ITEM('By Available Stock'),USE(?ExchangeUnits)
                             ITEM('By Audit Number'),USE(?ByAuditNumber)
                             ITEM('Rapid Insert Wizard'),USE(?RapidInsertWizard)
                             ITEM('Minimum Stock Routine'),USE(?BrowseExchangeUnitsMinimum)
                             ITEM('Exchange/Loan Reasons'),USE(?ExchangeLoanReasons)
                           END
                           ITEM('I.M.E.I. To Model Correlation'),AT(,,,12),USE(?ESNToModelCorrelation),ICON('auditsm.gif')
                           ITEM('Bouncers'),AT(,,,12),USE(?Bouncers),ICON('transsm.gif')
                           ITEM('Engineers Status'),AT(,,,12),USE(?EngineersStatus),ICON('MenWrksm.gif')
                           ITEM('Picking Notes'),AT(,,,12),USE(?BrowsePickingNotes),ICON('Picksm.gif')
                         END
                         MENU('U&tilities'),USE(?Utilities)
                           ITEM('Internal E-mail'),AT(,,120,12),USE(?InternalEmail),ICON('mailsm.gif')
                           ITEM('Users Logged In'),AT(,,,12),USE(?UsersLoggedIn),ICON('menqsm.gif')
                           ITEM('Network User Enquiry'),AT(,,,12),USE(?NetworkUserEnquiry),ICON('menwrksm.gif')
                           ITEM('Remote TCP/IP Setup'),AT(,,,12),USE(?RemoteTCPIPSetup),ICON('stopsm.gif')
                           ITEM('ServiceBase Chat'),AT(,,,12),USE(?ServiceBaseChat),ICON('telcomsm.gif')
                           ITEM('Clear Audit Trail'),AT(,,,12),USE(?ClearAuditTrail),ICON('auditsm.gif')
                           ITEM('Re-Submit For Despatch'),AT(,,,12),USE(?ReDespatch),ICON('despsm.gif')
                           ITEM('Manual Despatch Confirmation'),AT(,,,12),USE(?ManualDespatchConfirmation),ICON('despsm.gif')
                           ITEM('Remove Blank Records'),AT(,,,12),USE(?UtilitiesRemoveBlankRecords),ICON('auditsm.gif')
                         END
                         MENU('Rapid Update'),USE(?RapidUpdate)
                           ITEM('14-Day Repackaging Process'),USE(?Repackage),ICON('fixersm.gif')
                           ITEM('Rapid Repair Process'),USE(?VodaphoneONR),ICON('fixersm.gif')
                           ITEM('New Job Booking'),AT(,,120,12),USE(?JobBooking),ICON('Findersm.gif')
                           ITEM('Multiple Job Booking'),AT(,,,12),USE(?MultipleJobBooking),ICON('processm.gif')
                           ITEM('Estimate Procedure'),AT(,,,12),USE(?EstimateProcedure),ICON('moneysm.gif')
                           ITEM('Receive Exchange Unit'),AT(,,,12),USE(?ReceiveExchangeUnit),DISABLE,HIDE,ICON('stockqsm.gif')
                           ITEM('Status Change / Job Allocation'),AT(,,,12),USE(?StatusChange),ICON('transsm.gif')
                           ITEM('QA Procedure'),AT(,,,12),USE(?QAProcedure),ICON('spysm.gif')
                           ITEM('Engineer Update'),AT(,,,12),USE(?EngineerUpdate),ICON('fixersm.gif')
                           ITEM('3rd Party Despatch'),AT(,,,12),USE(?3rdPartyDespatch),ICON('stocksm.gif')
                           ITEM('Exchange Validation'),AT(,,,12),USE(?RapidUpdateExchangeValidation),ICON('spysm.gif')
                           ITEM('Job Update'),AT(,,,12),USE(?RapidJobUpdate),ICON('Findersm.gif')
                           ITEM('Multiple Job Update'),AT(,,,12),USE(?MultipleJobUpdate),ICON('processm.gif')
                           ITEM('Authorise Loan Returns'),AT(,,,12),USE(?AuthoriseLoanReturns),ICON('stockqsm.gif')
                           ITEM('Reception Despatch'),AT(,,,12),USE(?ReceptionDespatch),ICON('despsm.gif')
                           ITEM('Restock Loan/Exchange Units'),AT(,,,12),USE(?RestockExchangeUnits),ICON('stocksm.gif')
                         END
                         MENU('&Procedures'),USE(?Procedures)
                           MENU('Warranty Process'),AT(,,100,12),USE(?WarrantyProcess),ICON('Listbxsm.gif')
                             ITEM('Pending Claims'),USE(?PendingClaims)
                             ITEM('Reconcile Claims'),USE(?ReconcileClaims)
                           END
                           MENU('Import Procedures'),AT(,,,12),USE(?ImportProcedures),ICON('disksm.gif')
                             ITEM('Samsung Imports'),USE(?SamsungImports)
                             ITEM('Siemens Import'),USE(?SiemensImport)
                             ITEM('CRC Imports'),USE(?CRCImport)
                             ITEM('ACR Imports'),USE(?HaysImports)
                             ITEM('Intec Import'),USE(?IntecImport)
                           END
                           MENU('Export Procedures'),AT(,,,12),USE(?ExportProcedures),ICON('disksm.gif')
                             MENU('Job Exports'),USE(?ProceduresExportProceduresJobExports)
                               ITEM('Data Export'),USE(?DataExport)
                               ITEM('NEC Authorisation Export'),USE(?NECAuthorisationExport)
                               ITEM('Completed Jobs Statistics Export'),USE(?CompletedJobsStatisticsExport)
                               ITEM('TLC Daily Export'),USE(?TLCDailyExport)
                               ITEM('CRC Exports'),USE(?CRCExports)
                               ITEM('ACR Exports'),USE(?HaysExports)
                               ITEM('Picking Note Export'),USE(?ProceduresExportProceduresJobExportsPickingNote)
                             END
                             MENU('Stock Exports'),USE(?ProceduresExportProceduresStock)
                               ITEM('Parts Usage Export'),USE(?PartsUsageExport)
                               ITEM('Spares Used Export'),USE(?SparesUsedExport)
                               ITEM('Spares Received Export'),USE(?SparesReceivedExport)
                               ITEM('Stock Levels Export'),USE(?StockLevelsExport)
                               ITEM('Price List Export'),USE(?PriceListExport)
                             END
                             MENU('EDI Exports'),USE(?ProceduresExportProceduresEDI)
                               ITEM('Sharp Receive Data'),USE(?ReceiveData)
                               ITEM('Sharp ProcessedData'),USE(?ProcessedData)
                               ITEM('Sharp Replaced Parts Data'),USE(?ReplacedPartsData)
                               ITEM('Sagem Daily Export'),USE(?SagemDailyExport)
                               ITEM('Alcatel EDI Export'),USE(?AlcatelEDIExport)
                               ITEM('VK EDI Export'),USE(?VKEdiExport)
                             END
                           END
                           MENU('Despatch Procedures'),AT(,,,12),USE(?DespatchProcedures),ICON('despsm.gif')
                             MENU('ANC'),USE(?ANC)
                               ITEM('Defaults'),USE(?ANCDefaults)
                               ITEM,SEPARATOR
                               ITEM('Deliver Only'),USE(?ANCDeliverOnly)
                               ITEM('Deliver And Collect'),USE(?ANCDeliverAndCollect)
                               ITEM,SEPARATOR
                               ITEM('Collection Notes: Loan Returns'),USE(?ANCLoanReturns)
                             END
                             MENU('Label G'),USE(?LabelG)
                               ITEM('Consolidation Export'),USE(?ConsolidationExport)
                             END
                             MENU('Royal Mail'),USE(?RoyalMail)
                               ITEM('Royal Mail Export'),USE(?RoyalMailExport)
                               ITEM('Royal Mail Import'),USE(?RoyalMailImport)
                             END
                             MENU('Parceline'),USE(?Parceline)
                               ITEM('Parceline Export'),USE(?ParcelineExport)
                             END
                             MENU('UPS'),USE(?UPS)
                               ITEM('UPS Export'),USE(?UPSExport)
                             END
                           END
                           MENU('Invoicing'),AT(,,,12),USE(?Invoicing),ICON('moneysm.gif')
                             MENU('Utilities'),USE(?ProceduresInvoicingUtilities)
                               ITEM('Proforma Maintenance'),USE(?ProformaMaintenance)
                             END
                             ITEM('View Zero Value Exceptions'),USE(?InvoiceExceptions)
                             ITEM('Process Valued Jobs'),USE(?ProcessValuedJobs)
                             ITEM('Job Validation And Invoice Procedure'),USE(?JobValidationAndInvoiceProcedure)
                             ITEM('Browse Invoices'),USE(?Invoices)
                           END
                           MENU('Stock Control'),AT(,,,12),USE(?ProceduresStockControl),ICON('stocksm.gif')
                             ITEM('Minimum Stock Level'),USE(?MinimumStockLevel)
                             ITEM('Pending Orders'),USE(?PendingOrders)
                             ITEM('Receive Orders'),USE(?Receiving)
                             ITEM('Replicate Main Store Stock'),USE(?ReplicateMainStoreStock)
                           END
                           MENU('Mail Merge'),AT(,,,12),USE(?ProceduresMailMerge),ICON('mailsm.gif')
                             ITEM('Rapid Letter/Label Printing'),USE(?RapidLetterPrinting)
                           END
                           MENU('Search Facilities'),AT(,,,12),USE(?SearchFacilities),ICON('Findersm.gif')
                             ITEM('Search Consignment Numbers'),USE(?SearchConsignmentNumbers)
                           END
                         END
                         MENU('&Reports'),USE(?Reports)
                           MENU('Report Wizards'),AT(,,100,12),USE(?ReportWizards),ICON('Wizardsm.gif')
                             ITEM('SID Job Extract'),USE(?ReportsReportWizardsSIDJobExtract)
                             ITEM('Status Report'),USE(?StatusReport)
                             ITEM('Repair Activity Report'),USE(?RepairActivityReport)
                             ITEM('Repair Performance Report'),USE(?RepairPerformanceReport)
                           END
                           MENU('Courier Reports'),AT(,,100,12),USE(?CourierReports),ICON('trucksm.gif')
                             ITEM('Exchange Exceptions Report'),USE(?ExchangeExceptionsReport)
                             ITEM('Exchange Claims Report'),USE(?ExchangeClaimsReport)
                             ITEM('Courier Collection Report'),USE(?CourierCollectionReport)
                             ITEM('Special Delivery Report'),USE(?SpecialDeliveryReport)
                             ITEM('Exchange Units In Channel'),USE(?ExchangeUnitsInChannel)
                             ITEM('Exchange Unit Audit Report'),USE(?ExchangeUnitAuditReport)
                             ITEM('Overdue Loans Report'),USE(?OverdueLoansReport)
                             ITEM('Tote Report'),USE(?ToteReport)
                           END
                           MENU('Stock Reports'),AT(,,,12),USE(?ReportsStockReports),ICON('stocksm.gif')
                             MENU('Stock Value Report'),USE(?StockValueReport)
                               ITEM('Summary'),USE(?stockValueReportSummary)
                               ITEM('Detailed'),USE(?StockValueReportDetailed)
                             END
                             ITEM('Stock Check Report'),USE(?StockCheckReport)
                             ITEM('Stock Usage Report'),USE(?StockUsageReport)
                             ITEM('Parts Not Received Report'),USE(?PartsNotReceivedReport)
                             ITEM('Stock Forecast Report'),USE(?StockForecastReport)
                             ITEM('Job Pending Parts Report'),USE(?JobPendingPartsReport)
                             ITEM('QA Failure Report'),USE(?QAFailureReport)
                           END
                           MENU('Third Party Service'),AT(,,,12),USE(?ThirdPartyService),HIDE,ICON('stocksm2.gif')
                             ITEM('Third Party Despatch Note'),USE(?ThirdPartyDespatchNote)
                             ITEM('Overdue Returns'),USE(?OverdueReturns)
                           END
                           MENU('Retail Reports'),AT(,,,12),USE(?RetailReports),ICON('moneysm.gif')
                             ITEM('Retail Sales Valuation Report'),USE(?RetailSalesValuationReport)
                           END
                           MENU('Income Reports'),AT(,,,12),USE(?ReportsIncomeReports),ICON('moneysm.gif')
                             ITEM('Income Report'),USE(?IncomeReport)
                           END
                           MENU('Status Reports'),AT(,,,12),USE(?ReportsStatusReports),ICON('auditsm.gif')
                             ITEM('Status Summary Report'),USE(?StatusSummaryReport)
                             ITEM('Overdue Status Report'),USE(?OverdueStatusReport)
                             ITEM('Repair/Exchange Report'),USE(?RepairExchangeReport)
                             ITEM('Customer Enquiry Report'),USE(?CustomerEnquiryReport)
                             ITEM('Colour Report'),USE(?ColourReport)
                             ITEM('Exchange Reason Report'),USE(?ExchangeReasonReport)
                           END
                           ITEM('Jobs Status Report'),AT(,,,12),USE(?ReportsJobsStatusReport),ICON('auditsm.gif')
                           ITEM('Web Order Summary Report'),AT(,,,12),USE(?ReportsWebOrderSummaryReport),ICON('auditsm.gif')
                         END
                         MENU('Retail Sales'),USE(?RetailSales)
                           ITEM('Insert Retail Sales'),AT(,,,12),USE(?InsertRetailSales),ICON('moneysm.gif')
                           ITEM('Process Web Orders'),AT(,,,12),USE(?WebOrders),ICON('mailsm.gif')
                           ITEM,SEPARATOR
                           ITEM('Picking Reconciliation'),AT(,,,12),USE(?RetailSalesPickingReconciliation),ICON('fingersm.gif')
                           ITEM('Cash Sales Awaiting Payment'),AT(,,,12),USE(?SalesAwaitingPayment),ICON('moneysm.gif')
                           ITEM('Despatch Procedure'),AT(,,,12),USE(?RetailSalesDespatchProcedure),ICON('stocksm.gif')
                           ITEM('Back Order Processing'),AT(,,,12),USE(?RetailBackOrderProcessing),ICON('Listbxsm.gif')
                           ITEM('Browse All Sales'),AT(,,,12),USE(?RetailSalesBrowseAllSales),ICON('FINDERSM.GIF')
                         END
                         ITEM('Enhancements'),USE(?Enhancements)
                         MENU('&Window'),MSG('Create and Arrange windows'),STD(STD:WindowList)
                           ITEM('T&ile'),USE(?Tile),MSG('Make all open windows visible'),STD(STD:TileWindow)
                           ITEM('&Cascade'),USE(?Cascade),MSG('Stack all open windows'),STD(STD:CascadeWindow)
                           ITEM('&Arrange Icons'),USE(?Arrange),MSG('Align all window icons'),STD(STD:ArrangeIcons)
                         END
                         ITEM('About'),USE(?About)
                         MENU('Help'),USE(?Help2),RIGHT
                           ITEM('Key Stages'),USE(?HelpKeyStages)
                           ITEM,SEPARATOR
                           ITEM('ServiceBase 2000 Help'),USE(?Help),LAST
                         END
                       END
                       TOOLBAR,AT(0,0,,22),COLOR(COLOR:Silver),WALLPAPER('toolbar.gif'),TILED
                         BUTTON('&Job Progress'),AT(4,1,76,20),USE(?Browse_Jobs_Button),FLAT,LEFT,ICON('Finder.gif')
                         BUTTON('Stoc&k Control'),AT(348,1,76,20),USE(?StockControl),FLAT,LEFT,ICON('stock_br.gif')
                         BUTTON('Rapid Job &Update'),AT(172,1,76,20),USE(?Rapid_Update),FLAT,LEFT,ICON('spaceshp.gif')
                         BUTTON('Exit'),AT(560,1,76,20),USE(?Button6),FLAT,LEFT,ICON('exitcomp.gif'),STD(STD:Close)
                         BUTTON('Custom Reports'),AT(260,1,76,20),USE(?Reports:2),FLAT,LEFT,ICON('order.gif')
                         BUTTON('Rapid Procedures'),AT(88,1,76,20),USE(?RapidProcedures),FLAT,LEFT,ICON('SPACESHP.GIF')
                         PANEL,AT(548,1,1,21),USE(?Panel2),BEVEL(0,0,02010H)
                       END
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
TempFilePath         CSTRING(255)


progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Local Data Classes
ThisNetClient        CLASS(NetAutoCloseServer)        !Generated by NetTalk Extension (Class Definition)

                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
Login      Routine
    glo:select1 = 'N'
    Insert_Password
    If glo:select1 = 'Y'
        Halt
    Else
        Access:USERS.Open
        Access:USERS.UseFile

        Access:DEFAULTS.Open
        Access:DEFAULTS.UseFile()

        Access:USERS.ClearKey(use:Password_Key)
        use:Password = glo:Password
        Access:USERS.Fetch(use:Password_Key)

        Set(DEFAULTS)
        Access:DEFAULTS.Next()

        0{prop:statustext,2} = 'Current User: '& Capitalize(Clip(use:forename) & ' ' & Clip(use:surname))

        If glo:Password <> 'JOB:ENTER'
            Do Security_Check
        End
        Access:USERS.Close
        Access:DEFAULTS.Close
    End
    glo:select1 = ''

Log_out      Routine
    access:users.open
    access:users.usefile
    access:logged.open
    access:logged.usefile
    access:users.clearkey(use:password_key)
    use:password = glo:password
    if access:users.fetch(use:password_key) = Level:Benign
        access:logged.clearkey(log:user_code_key)
        log:user_code = use:user_code
        log:time      = glo:TimeLogged
        if access:logged.fetch(log:user_code_key) = Level:Benign
            delete(logged)
            If Error()
                Stop(Error())
            End !If Error()
        end !if
    end !if
    access:users.close
    access:logged.close
Security_Check      Routine
!!! - FILE - !!!
    Access:DEFAULTS.Open()
    Access:DEFAULTS.UseFile()

    Access:USERS_ALIAS.Open()
    Access:USERS_ALIAS.UseFile()
    Access:ACCAREAS_ALIAS.Open()
    Access:ACCAREAS_ALIAS.UseFile

    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    ! Start Change 3348 BE(24/11/03)
    IF ((use:user_type = 'ENGINEER') AND (securitycheck('ENGINEERS COMPUTER LOCATION') = Level:Benign)) THEN
        ComputerNameLength = 20
        IF (GetComputerName(ComputerName, ComputerNameLength)) THEN
            LocationTemp"  = GETINI(UPPER(ComputerName[1 : ComputerNameLength]),'LOCATION',, CLIP(PATH()) & '\Computer.ini')
            IF (LocationTemp" <> '') THEN
                use:Location = CLIP(LocationTemp")
                access:users.update()
            END
        END
    END
    ! End Change 3348 BE(24/11/03)

    ! Start Change 4628 BE(24/09/2004)
    !If ~Instring('C.R.C.',def:User_Name,1,1) And ~Instring('HAYS',def:User_Name,1,1)
    If ~Instring('C.R.C.',def:User_Name,1,1) And ~Instring('ACR',def:User_Name,1,1)
    ! End Change 4628 BE(24/09/2004)
        Destroy(?CRCDefaults)
    End!If Instring('C.R.C.',def:User_Name,1,1)

    If ~Instring('C.R.C.',def:User_Name,1,1)
        Destroy(?CRCImport)
        Destroy(?CRCExports)
        ! Start Change  2826 BE(19/08/03)
        Destroy(?VodaphoneONR)
        ! End Change  2826 BE(19/08/03)
    End !If ~Instring('C.R.C.',def:User_Name,1,1)

    ! Start Change 4628 BE(24/09/2004)
    !If ~Instring('HAYS',def:User_Name,1,1)
    If ~Instring('ACR',def:User_Name,1,1)
    ! End Change 4628 BE(24/09/2004)
        Destroy(?HaysImports)
        Destroy(?HaysExports)
        ! Start Change 2458 BE(02/05/03)
        DESTROY(?SamsungDefaults)
        DESTROY(?SamsungImports)
        ! End Change 2458 BE(02/05/03)
    End !If ~Instring('HAYS',def:User_Name,1,1)

    ! Start Change 4628 BE(24/09/2004)
    !If Instring('HAYS',def:User_Name,1,1)
    !    ?CRCDefaults{prop:Text} = 'Hays Defaults'
    If Instring('ACR',def:User_Name,1,1)
        ?CRCDefaults{prop:Text} = 'ACR Defaults'
    ! End Change 4628 BE(24/09/2004)
    End !If Instring('HAYS',def:User_Name,1,1)

    If ~Instring('INTEC',def:User_Name,1,1)
        Destroy(?IntecImport)
        ! Start Change 3074 BE(09/09/03)
        DESTROY(?ReceiveData)
        DESTROY(?ProcessedData)
        DESTROY(?ReplacedPartsData)
        ! End Change 3074 BE(09/09/03)
        ! Start Change 3684 BE(10/12/03)
        DESTROY(?Repackage)
        ! End Change 3684 BE(10/12/03)
    ! Start Change 3684 BE(10/12/03)
    ELSE
        IF (securitycheck('14-DAY REPACKAGING') = Level:Benign)
            ENABLE(?Repackage)
        ELSE
            DISABLE(?Repackage)
        END
        ! End Change 3684 BE(10/12/03)
    End !If ~Instring('INTEC',def:User_Name,1,1)

    if securitycheck('PRINT SETUP') = level:benign
        enable(?printsetup)
    else!if securitycheck('PRINT SETUP') = level:benign
        disable(?printsetup)
    end!if securitycheck('PRINT SETUP') = level:benign

    if securitycheck('RE-LOGIN') = level:benign
        enable(?relogin)
    else!if securitycheck('RE-LOGIN') = level:benign
        disable(?relogin)
    end!if securitycheck('RE-LOGIN') = level:benign


!!! - SYSTEM ADMINISTRATION - !!!

!!! - SYSTEM DEFAULTS - !!!

    if securitycheck('LICENSE DEFAULTS') = level:benign
        enable(?licensedefaults)
    else!if securitycheck('LICENSE DEFAULTS') = level:benign
        disable(?licensedefaults)
    end!if securitycheck('LICENSE DEFAULTS') = level:benign

    if securitycheck('GENERAL DEFAULTS') = level:benign
        enable(?generaldefaults)
    else!if securitycheck('GENERAL DEFAULTS') = level:benign
        disable(?generaldefaults)
    end!if securitycheck('GENERAL DEFAULTS') = level:benign

    if securitycheck('SAGE DEFAULTS') = level:benign
        enable(?sagedefaults)
    else!if securitycheck('SAGE DEFAULTS') = level:benign
        disable(?sagedefaults)
    end!if securitycheck('SAGE DEFAULTS') = level:benign

    If SecurityCheck('SIEMENS DEFAULTS') = level:benign
        Enable(?SiemensDefaults)
    Else!If SecurityCheck('SIEMENS DEFAULTS')
        Disable(?SIEMENSDEFAULTS)
    End!If SecurityCheck('SIEMENS DEFAULTS')

    If SecurityCheck('VODAFONE DEFAULTS') = level:benign
        Enable(?VODAFONEDEFAULTS)
    Else
        Disable(?VODAFONEDEFAULTS)
    End

    If SecurityCheck('WEB DEFAULTS') = level:benign
        Enable(?WEBDEFAULTS)
    Else
        Disable(?WEBDEFAULTS)
    End

    If SecurityCheck('SID ALERT DEFAULTS') = level:benign
        Enable(?SIDAlertDefaults)
    Else
        Disable(?SIDAlertDefaults)
    End

    If SecurityCHeck('SID INTERFACE DEFAULTS') = Level:Benign
        Enable(?SIDInterfaceDefaults)
    Else ! If SecurityCHeck('SID INTERFACE DEFAUTLS') = Level:Benign
        Disable(?SIDInterfaceDefaults)
    End ! If SecurityCHeck('SID INTERFACE DEFAUTLS') = Level:Benign

    If SecurityCheck('SID SERVICE CENTRES') = Level:Benign
        Enable(?SIDServiceCentres)
    Else ! If SecurityCheck('SID SERVICE CENTRES') = Level:Benign
        Disable(?SIDServiceCentres)
    End ! If SecurityCheck('SID SERVICE CENTRES') = Level:Benign

    If SecurityCheck('VODAFONE ACCOUNT REGIONS') = Level:Benign
        Enable(?VodafoneAccountRegions)
    Else ! If SecurityCheck('VODAFONE ACCOUNT REGIONS') = Level:Benign
        Disable(?VodafoneAccountRegions)
    End ! If SecurityCheck('VODAFONE ACCOUNT REGIONS') = Level:Benign

    If SecurityCheck('AUTOMATED SID EXTRACT DEFAULTS') = Level:Benign
        Enable(?AutomatedSIDExtractDefaults)
    Else ! If SecurityCheck('AUTOMATED SID EXTRACT DEFAULTS') = Level:Benign
        Disable(?AutomatedSIDExtractDefaults)
    End ! If SecurityCheck('AUTOMATED SID EXTRACT DEFAULTS') = Level:Benign
        
    If SecurityCheck('STORE PERFORMANCE REP DEFAULTS') = Level:Benign
        Enable(?StorePerformanceReportDefaults)
    Else ! If SecurityCheck('STORE PERFORMANCE REP DEFAULTS') = Level:Benign
        Disable(?StorePerformanceReportDefaults)
    End ! If SecurityCheck('STORE PERFORMANCE REP DEFAULTS') = Level:Benign

    ! Start Change 3348 BE(24/11/03)
    If SecurityCheck('COMPUTER LOCATION DEFAULTS') = level:benign
        Enable(?COMPUTERDEFAULTS)
    Else
        Disable(?COMPUTERDEFAULTS)
    End
    ! End Change 3348 BE(24/11/03)
    
!!! - USER DEFAULTS - !!!

    if securitycheck('USERS - BROWSE') = level:benign
        enable(?users)
    else!if securitycheck('USERS - BROWSE') = level:benign
        disable(?users)
    end!if securitycheck('USERS - BROWSE') = level:benign

    if securitycheck('ACCESS LEVELS - BROWSE') = level:benign
        enable(?accesslevels)
    else!if securitycheck('ACCESS LEVELS - BROWSE') = level:benign
        disable(?accesslevels)
    end!if securitycheck('ACCESS LEVELS - BROWSE') = level:benign

!!! _ FINANCIAL DEFAULTS - !!!

    if securitycheck('CHARGE RATE DEFAULTS - BROWSE') = level:benign
        enable(?chargeratedefaults)
    else!if securitycheck('CHARGE RATE DEFAULTS - BROWSE') = level:benign
        disable(?chargeratedefaults)
    end!if securitycheck('CHARGE RATE DEFAULTS - BROWSE') = level:benign

    if securitycheck('CHARGE TYPES - BROWSE') = level:benign
        enable(?chargetypes)
    else!if securitycheck('CHARGE TYPES - BROWSE') = level:benign
        disable(?chargetypes)
    end!if securitycheck('CHARGE TYPES - BROWSE') = level:benign

    If SecurityCheck('REPAIR TYPES - BROWSE') = level:benign
        Enable(?REPAIRTYPES)
    Else
        Disable(?REPAIRTYPES)
    End

    if securitycheck('VAT CODES - BROWSE') = level:benign
        enable(?vatcodes)
    else!if securitycheck('VAT CODES - BROWSE') = level:benign
        disable(?vatcodes)
    end!if securitycheck('VAT CODES - BROWSE') = level:benign

    if securitycheck('REPAIR TYPES - BROWSE') = level:benign
        enable(?paymenttypes)
    else!if securitycheck('REPAIR TYPES - BROWSE') = level:benign
        disable(?paymenttypes)
    end!if securitycheck('REPAIR TYPES - BROWSE') = level:benign

    if securitycheck('REPRICE JOBS ROUTINE') = level:benign
        enable(?repricejobsroutine)
    else!if securitycheck('REPRICE JOBS ROUTINE') = level:benign
        disable(?repricejobsroutine)
    end!if securitycheck('REPRICE JOBS ROUTINE') = level:benign

!!! - STOCK CONTROL DEFAULTS - !!!
  
    if securitycheck('MANUFACTURERS - BROWSE') = level:benign
        enable(?manufacturers)
    else!if securitycheck('MANUFACTURERS - BROWSE') = level:benign
        disable(?manufacturers)
    end!if securitycheck('MANUFACTURERS - BROWSE') = level:benign

    if securitycheck('SITE LOCATIONS - BROWSE') = level:benign
        enable(?sitelocations)
    else!if securitycheck('SITE LOCATIONS - BROWSE') = level:benign
        disable(?sitelocations)
    end!if securitycheck('SITE LOCATIONS - BROWSE') = level:benign

    if securitycheck('SUPPLIERS - BROWSE') = level:benign
        enable(?suppliers)
    else!if securitycheck('SUPPLIERS - BROWSE') = level:benign
        disable(?suppliers)
    end!if securitycheck('SUPPLIERS - BROWSE') = level:benign

    if securitycheck('INTERNAL LOCATIONS - BROWSE') = level:benign
        enable(?internallocations)
    else!if securitycheck('INTERNAL LOCATIONS - BROWSE') = level:benign
        disable(?internallocations)
    end!if securitycheck('INTERNAL LOCATIONS - BROWSE') = level:benign

    if securitycheck('LOAN/EXCHANGE STOCK TYPE') = level:benign
        enable(?loanexchangestocktype)
    else!if securitycheck('LOAN/EXCHANGE STOCK TYPE') = level:benign
        disable(?loanexchangestocktype)
    end!if securitycheck('LOAN/EXCHANGE STOCK TYPE') = level:benign

!!! - TRADE ACCOUNT DEFAULTS - !!!

    if securitycheck('TRADE ACCOUNTS - BROWSE') = level:benign
        enable(?tradeaccounts)
        enable(?browsetradesubaccounts)
    else!if securitycheck('TRADE ACCOUNTS - BROWSE') = level:benign
        disable(?tradeaccounts)
        disable(?browsetradesubaccounts)
    end!if securitycheck('TRADE ACCOUNTS - BROWSE') = level:benign



    if securitycheck('AUTO CHANGE COURIER') = level:benign
        enable(?autochangecourier)
    else!if securitycheck('AUTO CHANGE COURIER') = level:benign
        disable(?autochangecourier)
    end!if securitycheck('AUTO CHANGE COURIER') = level:benign


!!! - GENERAL LOOKUPS - !!!

    If SecurityCheck('ACTIONS - BROWSE') = level:benign
        Enable(?ACTION)
    Else
        Disable(?ACTION)
    End

    if securitycheck('ACCESSORIES - BROWSE') = level:benign
        enable(?accessories)
    else!if securitycheck('ACCESSORIES - BROWSE') = level:benign
        disable(?accessories)
    end!if securitycheck('ACCESSORIES - BROWSE') = level:benign

    If SecurityCheck('COLOURS - BROWSE') = level:Benign
        Enable(?COLOURS)
    Else
        Disable(?COLOURS)
    End

    if securitycheck('COURIERS - BROWSE') = level:benign
        enable(?couriers)
    else!if securitycheck('COURIERS - BROWSE') = level:benign
        disable(?couriers)
    end!if securitycheck('COURIERS - BROWSE') = level:benign

    If SecurityCheck('CITY LINK SERVICES') = level:benign
        Enable(?CITYLINKSERVICES)
    Else
        Disable(?CITYLINKSERVICES)
    End

    if securitycheck('JOB TURNAROUND TIMES - BROWSE') = level:benign
        enable(?jobturnaroundtimes)
    else!if securitycheck('JOB TURNAROUNDTIMES - BROWSE') = level:benign
        disable(?jobturnaroundtimes)
    end!if securitycheck('JOB TURNAROUNDTIMES - BROWSE') = level:benign

    if securitycheck('INITIAL TRANSIT TYPES - BROWSE') = level:benign
        enable(?initialtransittypes)
    else!if securitycheck('INITIAL TRANSIT TYPES - BROWSE') = level:benign
        disable(?initialtransittypes)
    end!if securitycheck('INITIAL TRANSIT TYPES - BROWSE') = level:benign

    if securitycheck('LETTERS - BROWSE') = level:benign
        enable(?letters)
    else!if securitycheck('LETTERS - BROWSE') = level:benign
        disable(?letters)
    end!if securitycheck('LETTERS - BROWSE') = level:benign

    If SecurityCheck('MAIL MERGE LETTERS - BROWSE') = level:benign
        Enable(?MAILMERGELETTERS)
    Else
        Disable(?MAILMERGELETTERS)
    End

    if securitycheck('PAYMENT TYPES - BROWSE') = level:benign
        enable(?paymenttypes)
    else!if securitycheck('PAYMENT TYPES - BROWSE') = level:benign
        disable(?paymenttypes)
    end!if securitycheck('PAYMENT TYPES - BROWSE') = level:benign

    if securitycheck('QUERY REASONS - BROWSE') = level:benign
        enable(?queryreasons)
    else!if securitycheck('QUERY REASONS - BROWSE') = level:benign
        disable(?queryreasons)
    end!if securitycheck('QUERY REASONS - BROWSE') = level:benign

    If SecurityCheck('QA FAILURE REASONS - BROWSE') = level:benign
        Enable(?QAFAILUREREASONS)
    Else
        Disable(?QAFAILUREREASONS)
    End

    if securitycheck('STATUS TYPES - BROWSE') = level:benign
        enable(?statustypes)
    else!if securitycheck('STATUS TYPES - BROWSE') = level:benign
        disable(?statustypes)
    end!if securitycheck('STATUS TYPES - BROWSE') = level:benign

    if securitycheck('TURNAROUND TIME UTILITY') = level:benign
        enable(?turnaroundtimeutility)
    else!if securitycheck('TURNAROUND TIME UTILITY') = level:benign
        disable(?turnaroundtimeutility)
    end!if securitycheck('TURNAROUND TIME UTILITY') = level:benign

!    check_access('PROCESS CODES - BROWSE',x")
!    if x" = true
!        enable(?processcodes)
!    else
!        disable(?processcodes)
!    end

    if securitycheck('TEAMS - BROWSE') = level:benign
        enable(?teams)
    else!if securitycheck('TEAMS - BROWSE') = level:benign
        disable(?teams)
    end!if securitycheck('TEAMS - BROWSE') = level:benign

    if securitycheck('THIRD PARTY REPAIRERS - BROWSE') = level:benign
        enable(?thirdpartyrepairers)
    else!if securitycheck('THIRD PARTY REPAIRERS - BROWSE') = level:benign
        disable(?thirdpartyrepairers)
    end!if securitycheck('THIRD PARTY REPAIRERS - BROWSE') = level:benign

    if securitycheck('UNIT TYPES - BROWSE') = level:benign
        enable(?unittypes)
    else!if securitycheck('UNIT TYPES - BROWSE') = level:benign
        disable(?unittypes)
    end!if securitycheck('UNIT TYPES - BROWSE') = level:benign

!!! - BROWSE - !!!
    If SecurityCheck('BOUNCERS') = level:benign
        Enable(?BOUNCERS)
    Else
        Disable(?BOUNCERS)
    End

    if securitycheck('CONTACTS - BROWSE') = level:benign
        enable(?contacts)
    else!if securitycheck('CONTACTS - BROWSE') = level:benign
        disable(?contacts)
    end!if securitycheck('CONTACTS - BROWSE') = level:benign

    if securitycheck('COMMON FAULTS - BROWSE') = level:benign
        enable(?commonfaultslist)
    else!if securitycheck('COMMON FAULTS - BROWSE') = level:benign
        disable(?commonfaultslist)
    end!if securitycheck('COMMON FAULTS - BROWSE') = level:benign

    if securitycheck('COMMON FAULTS CAT. - BROWSE') = level:benign
        enable(?commonfaultscategories)
    else!if securitycheck('COMMON FAULTS CAT. - BROWSE') = level:benign
        disable(?commonfaultscategories)
    end!if securitycheck('COMMON FAULTS CAT. - BROWSE') = level:benign

    if securitycheck('LOAN UNITS - BROWSE') = level:benign
        enable(?loanunits)
    else!if securitycheck('LOAN UNITS - BROWSE') = level:benign
        disable(?loanunits)
    end!if securitycheck('LOAN UNITS - BROWSE') = level:benign

    if securitycheck('EXCHANGE UNITS - BROWSE') = level:benign
        enable(?exchangeunits)
    else!if securitycheck('EXCHANGE UNITS - BROWSE') = level:benign
        disable(?exchangeunits)
    end!if securitycheck('EXCHANGE UNITS - BROWSE') = level:benign

    if securitycheck('AUDIT NUMBERS - BROWSE') = level:benign
        enable(?byauditnumber)
    else!if securitycheck('AUDIT NUMBERS - BROWSE') = level:benign
        disable(?byauditnumber)
    end!if securitycheck('AUDIT NUMBERS - BROWSE') = level:benign

    if securitycheck('RAPID EXCHANGE INSERT WIZARD') = level:benign
        enable(?rapidinsertwizard)
    else!if securitycheck('RAPID EXCHANGE INSERT WIZARD') = level:benign
        disable(?rapidinsertwizard)
    end!if securitycheck('RAPID EXCHANGE INSERT WIZARD') = level:benign

    if securitycheck('ESN TO MODEL CORRELATION') = level:benign
        enable(?esntomodelcorrelation)
    else!if securitycheck('ESN TO MODEL CORRELATION') = level:benign
        disable(?esntomodelcorrelation)
    end!if securitycheck('ESN TO MODEL CORRELATION') = level:benign

    if securitycheck('INVOICES - BROWSE') = level:benign
        enable(?invoices)
    else!if securitycheck('INVOICES - BROWSE') = level:benign
        disable(?invoices)
    end!if securitycheck('INVOICES - BROWSE') = level:benign
    If SecurityCheck('ENGINEERS STATUS') = level:benign
        Enable(?ENGINEERSSTATUS)
    Else
        Disable(?ENGINEERSSTATUS)
    End

!!! - UTILITIES - !!!

    if securitycheck('INTERNAL EMAIL - BROWSE') = level:benign
        enable(?internalemail)
    else!if securitycheck('INTERNAL EMAIL - BROWSE') = level:benign
        disable(?internalemail)
    end!if securitycheck('INTERNAL EMAIL - BROWSE') = level:benign
    If SecurityCheck('USERS LOGGED IN') = level:benign
        Enable(?USERSLOGGEDIN)
    Else
        Disable(?USERSLOGGEDIN)
    End

    If SecurityCheck('REMOTE TCPIP SETUP') = level:benign
        Enable(?REMOTETCPIPSETUP)
    Else
        Disable(?REMOTETCPIPSETUP)
    End

    If SecurityCheck('SERVICEBASE CHAT') = level:benign
        Enable(?SERVICEBASECHAT)
    Else
        Disable(?SERVICEBASECHAT)
    End

    if securitycheck('NETWORK USER ENQUIRY - BROWSE') = level:benign
        enable(?networkuserenquiry)
    else!if securitycheck('NETWORK USER ENQUIRY - BROWSE') = level:benign
        disable(?networkuserenquiry)
    end!if securitycheck('NETWORK USER ENQUIRY - BROWSE') = level:benign

    if securitycheck('CLEAR AUDIT TRAIL') = level:benign
        enable(?clearaudittrail)
    else!if securitycheck('CLEAR AUDIT TRAIL') = level:benign
        disable(?clearaudittrail)
    end!if securitycheck('CLEAR AUDIT TRAIL') = level:benign

    if securitycheck('RE-SUBMIT FOR DESPATCH') = level:benign
        enable(?redespatch)
    else!if securitycheck('RE-SUBMIT FOR DESPATCH') = level:benign
        disable(?redespatch)
    end!if securitycheck('RE-SUBMIT FOR DESPATCH') = level:benign

    if securitycheck('MANUAL DESPATCH CONFIRMATION') = level:benign
        enable(?manualdespatchconfirmation)
    else!if securitycheck('MANUAL DESPATCH CONFIRMATION') = level:benign
        disable(?manualdespatchconfirmation)
    end!if securitycheck('MANUAL DESPATCH CONFIRMATION') = level:benign

!!! - RAPID UPDATE - !!!
    
    if securitycheck('RAPID - JOB BOOKING') = level:benign
        enable(?jobbooking)
    else!if securitycheck('RAPID - JOB BOOKING') = level:benign
        disable(?jobbooking)
    end!if securitycheck('RAPID - JOB BOOKING') = level:benign

    If SecurityCheck('RAPID - MULTIPLE JOB BOOKING') = level:benign
        Enable(?MULTIPLEJOBBOOKING)
    Else
        Disable(?MULTIPLEJOBBOOKING)
    End

    If SecurityCheck('RAPID - THIRD PARTY DESPATCH') = level:benign
        Enable(?3RDPARTYDESPATCH)
    Else
        Disable(?3RDPARTYDESPATCH)
    End

    if securitycheck('RAPID - QA PROCEDURE') = level:benign
        enable(?qaprocedure)
    else!if securitycheck('RAPID - QA PROCEDURE') = level:benign
        disable(?qaprocedure)
    end!if securitycheck('RAPID - QA PROCEDURE') = level:benign

    if securitycheck('RAPID - STATUS CHANGE') = level:benign
        enable(?statuschange)
    else!if securitycheck('RAPID - STATUS CHANGE') = level:benign
        disable(?statuschange)
    end!if securitycheck('RAPID - STATUS CHANGE') = level:benign

    if securitycheck('RAPID - ENGINEER UPDATE') = level:benign
        enable(?engineerupdate)
    else!if securitycheck('RAPID - ENGINEER UPDATE') = level:benign
        disable(?engineerupdate)
    end!if securitycheck('RAPID - ENGINEER UPDATE') = level:benign

    if securitycheck('RAPID - RESTOCK EXCHANGE UNITS') = level:benign
        enable(?restockexchangeunits)
    else!if securitycheck('RAPID - RESTOCK EXCHANGE UNITS') = level:benign
        disable(?restockexchangeunits)
    end!if securitycheck('RAPID - RESTOCK EXCHANGE UNITS') = level:benign

    If SecurityCheck('RAPID JOB UPDATE') = level:benign
        Enable(?RAPIDJOBUPDATE)
    Else
        Disable(?RAPIDJOBUPDATE)
    End

    If SecurityCheck('RAPID - AUTHORISE LOAN RETURNS') = level:benign
        Enable(?AUTHORISELOANRETURNS)                 
    Else
        Disable(?AUTHORISELOANRETURNS)
    End

    If SecurityCheck('RAPID - RECEPTION DESPATCH') = level:benign
        Enable(?RECEPTIONDESPATCH)
    Else                                           
        Disable(?RECEPTIONDESPATCH)
    End

!!! - PROCEDURES - !!!

!!! - WARRANTY PROCESS - !!!

    if securitycheck('PENDING CLAIMS') = level:benign
        enable(?pendingclaims)
    else!if securitycheck('PENDING CLAIMS') = level:benign
        disable(?pendingclaims)
    end!if securitycheck('PENDING CLAIMS') = level:benign

    if securitycheck('RECONCILE CLAIMS') = level:benign
        enable(?reconcileclaims)
    else!if securitycheck('RECONCILE CLAIMS') = level:benign
        disable(?reconcileclaims)
    end!if securitycheck('RECONCILE CLAIMS') = level:benign

!!! - IMPORT PROCEDURES - !!!

    if securitycheck('SIEMENS IMPORT') = level:benign
        enable(?siemensimport)
    else!if securitycheck('SIEMENS IMPORT') = level:benign
        disable(?siemensimport)
    end!if securitycheck('SIEMENS IMPORT') = level:benign

!!! - EXPORT PROCEDURES - !!!

    If SecurityCheck('COMPLETED JOBS STATS EXPORT') = level:benign
        Enable(?COMPLETEDJOBSSTATISTICSEXPORT)
    Else
        Disable(?COMPLETEDJOBSSTATISTICSEXPORT)
    End

    If SecurityCheck('TLC DAILY EXPORT') = level:benign
        Enable(?TLCDAILYEXPORT)
    Else
        Disable(?TLCDAILYEXPORT)
    End

    if securitycheck('DATA EXPORT') = level:benign
        enable(?dataexport)
    else!if securitycheck('DATA EXPORT') = level:benign
        disable(?dataexport)
    end!if securitycheck('DATA EXPORT') = level:benign

    If SecurityCheck('PARTS USAGE EXPORT') = level:benign
        Enable(?PARTSUSAGEEXPORT)
    Else
        Disable(?PARTSUSAGEEXPORT)
    End

    If SecurityCheck('PRICE LIST EXPORT') = level:benign
        Enable(?PRICELISTEXPORT)
    Else
        Disable(?PRICELISTEXPORT)
    End

    If SecurityCheck('SAGEM DAILY EXPORT') = level:benign
        Enable(?SAGEMDAILYEXPORT)
    Else
        Disable(?SAGEMDAILYEXPORT)
    End

    If SecurityCheck('ALCATEL EDI EXPORT') = level:benign
        Enable(?ALCATELEDIEXPORT)
    Else
        Disable(?ALCATELEDIEXPORT)
    End

    if securitycheck('SPARES USED EXPORT') = level:benign
        enable(?sparesusedexport)
    else!if securitycheck('SPARES USED EXPORT') = level:benign
        disable(?sparesusedexport)
    end!if securitycheck('SPARES USED EXPORT') = level:benign

    if securitycheck('SPARES RECEIVED EXPORT') = level:benign
        enable(?sparesreceivedexport)
    else!if securitycheck('SPARES RECEIVED EXPORT') = level:benign
        disable(?sparesreceivedexport)
    end!if securitycheck('SPARES RECEIVED EXPORT') = level:benign

    if securitycheck('STOCK LEVELS EXPORT') = level:benign
        enable(?stocklevelsexport)
    else!if securitycheck('STOCK LEVELS EXPORT') = level:benign
        disable(?stocklevelsexport)
    end!if securitycheck('STOCK LEVELS EXPORT') = level:benign

    if securitycheck('NEC AUTHORISATION EXPORT') = level:benign
        enable(?necauthorisationexport)
    else!if securitycheck('NEC AUTHORISATION EXPORT') = level:benign
        disable(?necauthorisationexport)
    end!if securitycheck('NEC AUTHORISATION EXPORT') = level:benign

!!! - DESPATCH - !!!
    if securitycheck('ANC - DELIVER ONLY') = level:benign
        enable(?ancdeliveronly)
    else!if securitycheck('ANC - DELIVER ONLY') = level:benign
        disable(?ancdeliveronly)
    end!if securitycheck('ANC - DELIVER ONLY') = level:benign

    if securitycheck('ANC - DEFAULTS') = level:benign
        enable(?ancdefaults)
    else!if securitycheck('ANC - DEFAULTS') = level:benign
        disable(?ancdefaults)
    end!if securitycheck('ANC - DEFAULTS') = level:benign

    if securitycheck('LABEL G CONSOLIDATION EXPORT') = level:benign
        enable(?consolidationexport)
    else!if securitycheck('LABEL G CONSOLIDATION EXPORT') = level:benign
        disable(?consolidationexport)
    end!if securitycheck('LABEL G CONSOLIDATION EXPORT') = level:benign

    if securitycheck('ANC - DELIVER AND COLLECT') = level:benign
        enable(?ancdeliverandcollect)
    else!if securitycheck('ANC - DELIVERY AND COLLECT') = level:benign
        disable(?ancdeliverandcollect)
    end!if securitycheck('ANC - DELIVERY AND COLLECT') = level:benign

    if securitycheck('ANC - LOAN RETURNS') = level:benign
        enable(?ancloanreturns)
    else!if securitycheck('ANC - LOAN RETURNS') = level:benign
        disable(?ancloanreturns)
    end!if securitycheck('ANC - LOAN RETURNS') = level:benign

    If SecurityCheck('ROYAL MAIL EXPORT') = level:benign
        Enable(?ROYALMAILEXPORT)
    Else
        Disable(?ROYALMAILEXPORT)
    End

    If SecurityCheck('ROYAL MAIL IMPORT') = level:benign
        Enable(?ROYALMAILIMPORT)
    Else
        Disable(?ROYALMAILIMPORT)
    End

    If SecurityCheck('PARCELINE EXPORT') = level:benign
        Enable(?PARCELINEEXPORT)
    Else
        Disable(?PARCELINEEXPORT)
    End

    If SecurityCheck('UPS EXPORT') = level:benign
        Enable(?UPSEXPORT)
    Else
        Disable(?UPSEXPORT)
    End

!!! - INVOICING - !!!

!    check_access('CREATE INVOICE',x")
!    if x" = true
!        enable(?createinvoice)
!    else
!        disable(?createinvoice)
!    end

    if securitycheck('INVOICE EXCEPTIONS') = level:benign
        enable(?invoiceexceptions)
    else!if securitycheck('INVOICE EXCEPTIONS') = level:benign
        disable(?invoiceexceptions)
    end!if securitycheck('INVOICE EXCEPTIONS') = level:benign

!!! - PARTS ORDERS - !!!
    if securitycheck('PENDING ORDERS') = level:benign
        enable(?pendingorders)
    else!if securitycheck('PENDING ORDERS') = level:benign
        disable(?pendingorders)
    end!if securitycheck('PENDING ORDERS') = level:benign

    if securitycheck('PARTS ORDER RECEIVING') = level:benign
        enable(?receiving)
    else!if securitycheck('PARTS ORDER RECEIVING') = level:benign
        disable(?receiving)
    end!if securitycheck('PARTS ORDER RECEIVING') = level:benign

    if securitycheck('RETAIL BACK ORDER PROCESSING') = level:benign
        enable(?RetailBackOrderProcessing)
    else!if securitycheck('PARTS ORDER RECEIVING') = level:benign
        disable(?RetailBackOrderProcessing)
    end!if securitycheck('PARTS ORDER RECEIVING') = level:benign

    If SecurityCheck('PROCESS VALUED JOBS') = level:benign
        Enable(?PROCESSVALUEDJOBS)
    Else
        Disable(?PROCESSVALUEDJOBS)
    End

    If SecurityCheck('JOB VALIDATION AND INV PROC') = level:benign
        Enable(?JOBVALIDATIONANDINVOICEPROCEDURE)
    Else
        Disable(?JOBVALIDATIONANDINVOICEPROCEDURE)
    End

    If SecurityCheck('RAPID LETTER PRINTING') = level:benign
        Enable(?RAPIDLETTERPRINTING)
    Else
        Disable(?RAPIDLETTERPRINTING)
    End

    If SecurityCheck('SEARCH CONSIGNMENT NUMBERS') = level:benign
        Enable(?SEARCHCONSIGNMENTNUMBERS)
    Else
        Disable(?SEARCHCONSIGNMENTNUMBERS)
    End

!!! - STOCK CONTROL - !!!

    if securitycheck('MINIMUM STOCK LEVEL ROUTINE') = level:benign
        enable(?minimumstocklevel)
    else!if securitycheck('MINIMUM STOCK LEVEL ROUTINE') = level:benign
        disable(?minimumstocklevel)
    end!if securitycheck('MINIMUM STOCK LEVEL ROUTINE') = level:benign

    if securitycheck('REPLICATE MAIN STORE STOCK') = level:benign
        enable(?replicatemainstorestock)
    else!if securitycheck('REPLICATE MAIN STORE STOCK') = level:benign
        disable(?replicatemainstorestock)
    end!if securitycheck('REPLICATE MAIN STORE STOCK') = level:benign

!!! - VODAFONE EXCHANGE - !!!

!    check_access('VODA - IMPORT EXCHANGE FILE',x")
!    if x" = true
!        enable(?importexchangefile)
!    else
!        disable(?importexchangefile)
!    end
!
!    check_access('VODA - ALLOCATE EXCHANGE UNIT',x")
!    if x" = true
!        enable(?allocateexchangeunit)
!    else
!        disable(?allocateexchangeunit)
!    end
!
!    check_access('VODA - DESPATCH PROCEDURE',x")
!    if x" = true
!        enable(?despatchprocedure)
!    else
!        disable(?despatchprocedure)
!    end
!
!    check_access('VODA - QA FAILURE',x")
!    if x" = true
!        enable(?qafailure)
!    else
!        disable(?qafailure)
!    end
!
!    check_access('VODA - UPDATE VODAFONE',x")
!    if x" = true
!        enable(?updatevodafone)
!    else
!        disable(?updatevodafone)
!    end

!!! - REPORTS - !!!

!!! - REPORT WIZARDS - !!!

    if securitycheck('STATUS REPORT') = level:benign
        enable(?statusreport)
    else!if securitycheck('STATUS REPORT') = level:benign
        disable(?statusreport)
    end!if securitycheck('STATUS REPORT') = level:benign

    if securitycheck('REPAIR ACTIVITY REPORT') = level:benign
        enable(?repairactivityreport)
    else!if securitycheck('REPAIR ACTIVITY REPORT') = level:benign
        disable(?repairactivityreport)
    end!if securitycheck('REPAIR ACTIVITY REPORT') = level:benign

    if securitycheck('REPAIR PERFORMANCE REPORT') = level:benign
        enable(?repairperformancereport)
    else!if securitycheck('REPAIR ACTIVITY REPORT') = level:benign
        disable(?repairperformancereport)
    end!if securitycheck('REPAIR ACTIVITY REPORT') = level:benign

!!! - COURIER REPORTS - !!!    

    If SecurityCheck('EXCHANGE UNIT AUDIT REPORT') = level:benign
        Enable(?EXCHANGEUNITAUDITREPORT)
    Else
        Disable(?EXCHANGEUNITAUDITREPORT)
    End

    If SecurityCheck('STOCK CHECK REPORT') = level:benign
        Enable(?STOCKCHECKREPORT)
    Else
        Disable(?STOCKCHECKREPORT)
    End

    If SecurityCheck('RETAIL SALES VALUATION REPORT') = level:benign
        Enable(?RETAILSALESVALUATIONREPORT)
    Else
        Disable(?RETAILSALESVALUATIONREPORT)
    End

    If SecurityCheck('REPAIR EXCHANGE REPORT') = level:benign
        Enable(?REPAIREXCHANGEREPORT)
    Else
        Disable(?REPAIREXCHANGEREPORT)
    End

    If SecurityCheck('CUSTOMER ENQUIRY REPORT') = level:benign
        Enable(?CUSTOMERENQUIRYREPORT)
    Else
        Disable(?CUSTOMERENQUIRYREPORT)
    End

    if securitycheck('EXCHANGE EXCEPTIONS REPORT') = level:benign
        enable(?exchangeexceptionsreport)
    else!if securitycheck('EXCHANGE EXCEPTIONS REPORT') = level:benign
        disable(?exchangeexceptionsreport)
    end!if securitycheck('EXCHANGE EXCEPTIONS REPORT') = level:benign

    if securitycheck('EXCHANGE CLAIMS REPORT') = level:benign
        enable(?exchangeclaimsreport)
    else!if securitycheck('EXCHANGE CLAIMS REPORT') = level:benign
        disable(?exchangeclaimsreport)
    end!if securitycheck('EXCHANGE CLAIMS REPORT') = level:benign

    if securitycheck('COURIER COLLECTION REPORT') = level:benign
        enable(?couriercollectionreport)
    else!if securitycheck('COURIER COLLECTION REPORT') = level:benign
        disable(?couriercollectionreport)
    end!if securitycheck('COURIER COLLECTION REPORT') = level:benign

    if securitycheck('SPECIAL DELIVERY REPORT') = level:benign
        enable(?specialdeliveryreport)
    else!if securitycheck('SPECIAL DELIVERY REPORT') = level:benign
        disable(?specialdeliveryreport)
    end!if securitycheck('SPECIAL DELIVERY REPORT') = level:benign

    if securitycheck('EXCHANGE UNITS IN CHANNEL') = level:benign
        enable(?exchangeunitsinchannel)
    else!if securitycheck('EXCHANGE UNITS IN CHANNEL') = level:benign
        disable(?exchangeunitsinchannel)
    end!if securitycheck('EXCHANGE UNITS IN CHANNEL') = level:benign

    if securitycheck('OVERDUE LOANS REPORT') = level:benign
        enable(?overdueloansreport)
    else!if securitycheck('OVERDUE LOANS REPORT') = level:benign
        disable(?overdueloansreport)
    end!if securitycheck('OVERDUE LOANS REPORT') = level:benign


!!! - STOCK REPORTS - !!!

    if securitycheck('STOCK VALUE REPORT') = level:benign
        enable(?stockvaluereport)
    else!if securitycheck('STOCK VALUE REPORT') = level:benign
        disable(?stockvaluereport)
    end!if securitycheck('STOCK VALUE REPORT') = level:benign

    if securitycheck('STOCK USAGE REPORT') = level:benign
        enable(?stockusagereport)
    else!if securitycheck('STOCK USAGE REPORT') = level:benign
        disable(?stockusagereport)
    end!if securitycheck('STOCK USAGE REPORT') = level:benign

    if securitycheck('PARTS NOT RECEIVED REPORT') = level:benign
        enable(?partsnotreceivedreport)
    else!if securitycheck('PARTS NOT RECEIVED REPORT') = level:benign
        disable(?partsnotreceivedreport)
    end!if securitycheck('PARTS NOT RECEIVED REPORT') = level:benign

    if securitycheck('STOCK FORECAST REPORT') = level:benign
        enable(?stockforecastreport)
    else!if securitycheck('STOCK FORECAST REPORT') = level:benign
        disable(?stockforecastreport)
    end!if securitycheck('STOCK FORECAST REPORT') = level:benign

    if securitycheck('JOB PENDING PARTS REPORT') = level:benign
        enable(?jobpendingpartsreport)
    else!if securitycheck('JOB PENDING PARTS REPORT') = level:benign
        disable(?jobpendingpartsreport)
    end!if securitycheck('JOB PENDING PARTS REPORT') = level:benign

!!! - THIRD PARTY SERVICE - !!!

    if securitycheck('THIRD PARTY DESPATCH NOTE') = level:benign
        enable(?thirdpartydespatchnote)
    else!if securitycheck('THIRD PARTY DESPATCH NOTE') = level:benign
        disable(?thirdpartydespatchnote)
    end!if securitycheck('THIRD PARTY DESPATCH NOTE') = level:benign

    if securitycheck('OVERDUE RETURNS') = level:benign
        enable(?overduereturns)
    else!if securitycheck('OVERDUE RETURNS') = level:benign
        disable(?overduereturns)
    end!if securitycheck('OVERDUE RETURNS') = level:benign

!!! - INCOME REPORTS - !!!

    if securitycheck('INCOME REPORT') = level:benign
        enable(?incomereport)
    else!if securitycheck('INCOME REPORT') = level:benign
        disable(?incomereport)
    end!if securitycheck('INCOME REPORT') = level:benign

    if securitycheck('STATUS SUMMARY REPORT') = level:benign
        enable(?statussummaryreport)
    else!if securitycheck('STATUS SUMMARY REPORT') = level:benign
        disable(?statussummaryreport)
    end!if securitycheck('STATUS SUMMARY REPORT') = level:benign

    if securitycheck('OVERDUE STATUS REPORT') = level:benign
        enable(?overduestatusreport)
    else!if securitycheck('OVERDUE STATUS REPORT') = level:benign
        disable(?overduestatusreport)
    end!if securitycheck('OVERDUE STATUS REPORT') = level:benign

    if securitycheck('QA FAILURE REPORT') = level:benign
        enable(?qafailurereport)
    else!if securitycheck('QA FAILURE REPORT') = level:benign
        disable(?qafailurereport)
    end!if securitycheck('QA FAILURE REPORT') = level:benign

! Start Change 3338 BE(10/10/03)
    IF (securitycheck('EXCHANGE REASON REPORT') = level:benign) THEN
        enable(?ExchangeReasonReport)
    ELSE
        disable(?ExchangeReasonReport)
    END
! End Change 3338 BE(10/10/03)

    If SecurityCheck('RETAIL SALES') = Level:benign
        Enable(?RetailSales)
    Else
        Disable(?RetailSales)
    End !If SecurityCheck('RETAIL SALES') = Level:benign
  
    if securitycheck('PICK PART CONFIRMATION') = level:benign
      enable(?BrowsePickingNotes)
    else!if securitycheck('PICK PART CONFIRMATION') = level:benign
      disable(?BrowsePickingNotes)
    end!if securitycheck('PICK PART CONFIRMATION') = level:benign

    Access:DEFAULTS.Close()
    Access:USERS_ALIAS.Close()
    Access:ACCAREAS_ALIAS.Close()
Menu::File ROUTINE                                    !Code for menu items on ?File
  CASE ACCEPTED()
  OF ?ReLogin
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReLogin, Accepted)
    Do Log_out
    Do Login
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReLogin, Accepted)
  END
Menu::FileThemes ROUTINE                              !Code for menu items on ?FileThemes
  CASE ACCEPTED()
  OF ?FileThemesMenu
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FileThemesMenu, Accepted)
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FileThemesMenu, Accepted)
  OF ?FileThemesReenableTheme
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FileThemesReenableTheme, Accepted)
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FileThemesReenableTheme, Accepted)
  END
Menu::SystemAdministration ROUTINE                    !Code for menu items on ?SystemAdministration
Menu::SystemDefaults ROUTINE                          !Code for menu items on ?SystemDefaults
  CASE ACCEPTED()
  OF ?LicenseDefaults
    Defaults_Window
  OF ?SageDefaults
    Sage_Defaults
  END
Menu::GeneralDefaults ROUTINE                         !Code for menu items on ?GeneralDefaults
  CASE ACCEPTED()
  OF ?MainDefaults
    MainDefaults
  OF ?CompulsoryFields
    CompulsoryFields
  OF ?StandardTexts
    StandardTexts
  OF ?PrintingDefaults
    PrintingDefaults
  OF ?BookingDefaults
    BookingDefaults
  OF ?ComputerDefaults
    ComputerNameDefaults
  OF ?StockControlDefaults
    StockDefaults
  OF ?RetailSalesDefaults
    RetailSalesDefaults
  END
Menu::EmailDefaults ROUTINE                           !Code for menu items on ?EmailDefaults
  CASE ACCEPTED()
  OF ?ServerDefaults
    EmailDefaults
  OF ?RecipientTypes
    BrowseRecipientTypes
  END
Menu::SystemAdminstrationSystemDefaultsImportDefaults ROUTINE !Code for menu items on ?SystemAdminstrationSystemDefaultsImportDefaults
  CASE ACCEPTED()
  OF ?CRCDefaults
    CRCDefaults
  OF ?SamsungDefaults
    Samsung_Defaults
  OF ?SiemensDefaults
    eps_defaults
  OF ?VodafoneDefaults
    Vodafone_Defaults
  OF ?WebDefaults
    Browse_Web_Defaults
  END
Menu::SystemAdministrationSystemDefaultsSIDDefaults ROUTINE !Code for menu items on ?SystemAdministrationSystemDefaultsSIDDefaults
  CASE ACCEPTED()
  OF ?SIDInterfaceDefaults
    SIDInterfaceDefaults
  OF ?SIDServiceCentres
    BrowseSIDServiceCentres
  OF ?VodafoneAccountRegions
    BrowseAccountRegions
  OF ?SIDAlertDefaults_OLD
    SIDAlertDefaults
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SIDAlertDefaults_OLD, Accepted)
    RUN('SIDALERTBROWSE.EXE %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SIDAlertDefaults_OLD, Accepted)
  OF ?SystemAdministrationSystemDefaultsSIDDefaultsIM
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SystemAdministrationSystemDefaultsSIDDefaultsIM, Accepted)
    RUN('locaterimportdefaults.exe')
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SystemAdministrationSystemDefaultsSIDDefaultsIM, Accepted)
  OF ?AutomatedSIDExtractDefaults
    SIDJobExtractDefaults
  OF ?StorePerformanceReportDefaults
    StorePerfReportDefaults
  OF ?SIDAlertDefaults
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SIDAlertDefaults, Accepted)
    RUN('sidalertbrowse.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SIDAlertDefaults, Accepted)
  OF ?RetailRepairChargeTiers
    BrowseRetailRepairChargeTiers
  END
Menu::UserDefaults ROUTINE                            !Code for menu items on ?UserDefaults
  CASE ACCEPTED()
  OF ?accesslevels
    Browse_access_levels
  OF ?users
    Browse_Users
  END
Menu::FinancialDefaults ROUTINE                       !Code for menu items on ?FinancialDefaults
  CASE ACCEPTED()
  OF ?ChargeRateDefaults
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChargeRateDefaults, Accepted)
    glo:select12 = 'STANDARD'
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChargeRateDefaults, Accepted)
    Insert_Stardard_Charges
  OF ?ChargeTypes
    Browse_Charge_Type
  OF ?RepairTypes
    Browse_Default_Repair_Types
  OF ?VATCodes
    Browse_vat_code
  END
Menu::SystemAdminstrationFinancialDefaultsUtilities ROUTINE !Code for menu items on ?SystemAdminstrationFinancialDefaultsUtilities
  CASE ACCEPTED()
  OF ?RepriceJobsRoutine
    RepriceJobs
  END
Menu::SystemAdminstrationStockControlDefaults ROUTINE !Code for menu items on ?SystemAdminstrationStockControlDefaults
  CASE ACCEPTED()
  OF ?manufacturers
    BrowseMANUFACT
  OF ?Suppliers
    Browse_Suppliers
  OF ?SiteLocations
    Browse_Stock_Locations
  OF ?InternalLocations
    Browse_Internal_Location
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InternalLocations, Accepted)
    glo:select1 = ''
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InternalLocations, Accepted)
  OF ?LoanExchangeStockType
    Browse_Stock_Type
  OF ?SystemAdministrationStockControlDefaultsModelPr
    RUN('MODELPRICINGEXPORT.EXE ')
    ThisWindow.Reset(1)
  END
Menu::TradeAccountDefaults ROUTINE                    !Code for menu items on ?TradeAccountDefaults
  CASE ACCEPTED()
  OF ?TradeAccounts
    BrowseTRADEACC
  OF ?BrowseTradeSubAccounts
    Browse_Sub_Accounts
  END
Menu::TradeAccountUtilities ROUTINE                   !Code for menu items on ?TradeAccountUtilities
  CASE ACCEPTED()
  OF ?AutoChangeCourier
    Auto_Change_Courier
  END
Menu::Lookups ROUTINE                                 !Code for menu items on ?Lookups
  CASE ACCEPTED()
  OF ?accessories
    Browse_Default_Accessory
  OF ?Action
    Browse_action
  OF ?Colours
    Browse_Default_Colours
  OF ?JobTurnaroundTimes
    Browse_Turnaround_Time
  OF ?InitialTransitTypes
    Browse_Transit_Types
  OF ?Letters
    Browse_Letters
  OF ?MailMergeLetters
    BrowseLetters
  OF ?Networks
    BrowseNetworks
  OF ?PaymentTypes
    Browse_Payment_Types
  OF ?QAFailureReasons
    Browse_QA_Reason
  OF ?QueryReasons
    Browse_Query_Reason
  OF ?Teams
    Browse_Teams
  OF ?thirdPartyRepairers
    Browse_3rdParty
  OF ?UnitTypes
    BrowseUNITTYPE
  END
Menu::SystemAdminstrationGeneralLookupsCouries ROUTINE !Code for menu items on ?SystemAdminstrationGeneralLookupsCouries
  CASE ACCEPTED()
  OF ?couriers
    Browse_Courier
  OF ?CityLinkServices
    Browse_City_Service
  END
Menu::SystemAdminstrationGeneralLookupsStatusTypes ROUTINE !Code for menu items on ?SystemAdminstrationGeneralLookupsStatusTypes
  CASE ACCEPTED()
  OF ?StatusTypes
    Browse_Status_Only
  OF ?TurnaroundTimeUtility
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TurnaroundTimeUtility, Accepted)
    Case MessageEx('Warning! This facility will reset the Status Turnaround Times for EACH job.<13,10><13,10>It will take a long time to complete, and CANNOT be reversed.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                   'Styles\warn.ico','|&Yes|&No',2,2,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
            Case MessageEx('You have selected to RESET the Status Turnaround Times for ALL jobs.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                           'Styles\warn.ico','|&Yes|&No',2,2,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                Of 1 ! &Yes Button
                    TurnaroundTimeProcess()
                Of 2 ! &No Button
            End!Case MessageEx
        Of 2 ! &No Button
    End!Case MessageEx
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TurnaroundTimeUtility, Accepted)
  END
Menu::Browse ROUTINE                                  !Code for menu items on ?Browse
  CASE ACCEPTED()
  OF ?Contacts
    START(Browse_Contacts, 25000)
  OF ?LoanUnits
    Browse_Loan('')
  OF ?ESNToModelCorrelation
    Browse_ESN_Model
  OF ?Bouncers
    Browse_Bouncers
  OF ?EngineersStatus
    Engineer_Status_Screen
  OF ?BrowsePickingNotes
    BrowsePick
  END
Menu::BrowseCommonFaults ROUTINE                      !Code for menu items on ?BrowseCommonFaults
  CASE ACCEPTED()
  OF ?CommonFaultsCategories
    Browse_Common_Category
  OF ?CommonFaultsList
    Browse_Common_Faults
  END
Menu::BrowseExchangeUnits ROUTINE                     !Code for menu items on ?BrowseExchangeUnits
  CASE ACCEPTED()
  OF ?ExchangeUnits
    Browse_Exchange('')
  OF ?ByAuditNumber
    Browse_Exchange_By_Audit('')
  OF ?RapidInsertWizard
    Rapid_Exchange_Unit
  OF ?BrowseExchangeUnitsMinimum
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BrowseExchangeUnitsMinimum, Accepted)
    Case MessageEx('Warning! This routine may take a long time to run.'&|
      '<13,10>'&|
      '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                   'Styles\warn.ico','|&Yes|&No',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
            glo:File_Name   = 'F' & Format(Clock(),@n07) & '.TMP'
            MinimumExchangeProcess()
            BrowseExchangeMinStock
            Remove(glo:File_Name)
        Of 2 ! &No Button
    End!Case MessageEx
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BrowseExchangeUnitsMinimum, Accepted)
  OF ?ExchangeLoanReasons
    BrowseExchangeLoanReasons
  END
Menu::Utilities ROUTINE                               !Code for menu items on ?Utilities
  CASE ACCEPTED()
  OF ?InternalEmail
    Internal_Mail
  OF ?UsersLoggedIn
    Browse_Logged_In
  OF ?NetworkUserEnquiry
    Network_User_Enquiry
  OF ?RemoteTCPIPSetup
    Remote_Control
  OF ?ServiceBaseChat
    TCP_IP_Chat
  OF ?ClearAuditTrail
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearAuditTrail, Accepted)
    Glo:select1 = ''
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearAuditTrail, Accepted)
    Remove_Audit
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearAuditTrail, Accepted)
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearAuditTrail, Accepted)
  OF ?ReDespatch
    Re_Despatch
  OF ?ManualDespatchConfirmation
    Force_Despatch
  OF ?UtilitiesRemoveBlankRecords
    RemoveBlankRecords
  END
Menu::RapidUpdate ROUTINE                             !Code for menu items on ?RapidUpdate
  CASE ACCEPTED()
  OF ?Repackage
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Repackage, Accepted)
    RUN('cel14day.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Repackage, Accepted)
  OF ?VodaphoneONR
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VodaphoneONR, Accepted)
    RUN('celrapvo.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VodaphoneONR, Accepted)
  OF ?JobBooking
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobBooking, Accepted)
    RUN('celrapid.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobBooking, Accepted)
  OF ?MultipleJobBooking
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MultipleJobBooking, Accepted)
    RUN('celrapml.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MultipleJobBooking, Accepted)
  OF ?EstimateProcedure
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EstimateProcedure, Accepted)
    RUN('celrapes.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EstimateProcedure, Accepted)
  OF ?ReceiveExchangeUnit
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReceiveExchangeUnit, Accepted)
    RUN('celraper.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReceiveExchangeUnit, Accepted)
  OF ?StatusChange
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StatusChange, Accepted)
    RUN('celrapst.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StatusChange, Accepted)
  OF ?QAProcedure
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?QAProcedure, Accepted)
    RUN('celrapqa.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?QAProcedure, Accepted)
  OF ?EngineerUpdate
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EngineerUpdate, Accepted)
    RUN('celrapen.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EngineerUpdate, Accepted)
  OF ?3rdPartyDespatch
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?3rdPartyDespatch, Accepted)
    RUN('celraptp.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?3rdPartyDespatch, Accepted)
  OF ?RapidUpdateExchangeValidation
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RapidUpdateExchangeValidation, Accepted)
    RUN('celrapva.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RapidUpdateExchangeValidation, Accepted)
  OF ?RapidJobUpdate
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RapidJobUpdate, Accepted)
    Post(event:accepted,?rapid_update)
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RapidJobUpdate, Accepted)
  OF ?MultipleJobUpdate
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MultipleJobUpdate, Accepted)
    RUN('celrapmu.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MultipleJobUpdate, Accepted)
  OF ?AuthoriseLoanReturns
    START(Authorise_Loan_Returns, 25000)
  OF ?ReceptionDespatch
    START(Rapid_Reception_Despatch, 25000)
  OF ?RestockExchangeUnits
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RestockExchangeUnits, Accepted)
    RUN('celraprs.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RestockExchangeUnits, Accepted)
  END
Menu::Procedures ROUTINE                              !Code for menu items on ?Procedures
Menu::WarrantyProcess ROUTINE                         !Code for menu items on ?WarrantyProcess
  CASE ACCEPTED()
  OF ?PendingClaims
    EDI_Claims
  OF ?ReconcileClaims
    Reconcile_Claims
  END
Menu::ImportProcedures ROUTINE                        !Code for menu items on ?ImportProcedures
  CASE ACCEPTED()
  OF ?SamsungImports
    ImportSamsung
  OF ?SiemensImport
    Siemens_Import
  OF ?CRCImport
    CRCImports
  OF ?HaysImports
    HaysImports
  OF ?IntecImport
    Intecimport
  END
Menu::ExportProcedures ROUTINE                        !Code for menu items on ?ExportProcedures
Menu::ProceduresExportProceduresJobExports ROUTINE    !Code for menu items on ?ProceduresExportProceduresJobExports
  CASE ACCEPTED()
  OF ?DataExport
    Data_Export_Wizard
  OF ?NECAuthorisationExport
    NEC_Export_Criteria
  OF ?CompletedJobsStatisticsExport
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CompletedJobsStatisticsExport, Accepted)
    glo:tradetmp    = 'TR' & Sub(Clock(),1,6) & '.TMP'
    Remove(glo:tradetmp)
    CompletedJobStatisticsExportCriteria
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CompletedJobsStatisticsExport, Accepted)
  OF ?TLCDailyExport
    TLCExportCriteria
  OF ?CRCExports
    CRCExports
  OF ?HaysExports
    HaysExports
  OF ?ProceduresExportProceduresJobExportsPickingNote
    Pick_Export_Criteria
  END
Menu::ProceduresExportProceduresStock ROUTINE         !Code for menu items on ?ProceduresExportProceduresStock
  CASE ACCEPTED()
  OF ?PartsUsageExport
    Part_Usage_Export
  OF ?SparesUsedExport
    Spares_Date_Range('Spares Used Export')
  OF ?SparesReceivedExport
    Spares_Date_Range('Spares Received Export')
  OF ?StockLevelsExport
    Count_Stock_Export
  OF ?PriceListExport
    PriceListExport
  END
Menu::ProceduresExportProceduresEDI ROUTINE           !Code for menu items on ?ProceduresExportProceduresEDI
  CASE ACCEPTED()
  OF ?ReceiveData
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReceiveData, Accepted)
    RUN('sbcr0078.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReceiveData, Accepted)
  OF ?ProcessedData
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessedData, Accepted)
    RUN('sbcr0079.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessedData, Accepted)
  OF ?ReplacedPartsData
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplacedPartsData, Accepted)
    RUN('sbcr0080.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplacedPartsData, Accepted)
  OF ?SagemDailyExport
    Date_Range('Sagem Daily Export')
  OF ?AlcatelEDIExport
    AlcatelBatchNumber
  OF ?VKEdiExport
    Date_Range('VK EDI Export')
  END
Menu::DespatchProcedures ROUTINE                      !Code for menu items on ?DespatchProcedures
Menu::ANC ROUTINE                                     !Code for menu items on ?ANC
  CASE ACCEPTED()
  OF ?ANCDefaults
    ANC_Collection_Number
  OF ?ANCDeliverOnly
    ANC_Despatch_Routine('DELIVER')
  OF ?ANCDeliverAndCollect
    ANC_Despatch_Routine('COLLECT')
  OF ?ANCLoanReturns
    ANC_Loan_Return
  END
Menu::LabelG ROUTINE                                  !Code for menu items on ?LabelG
  CASE ACCEPTED()
  OF ?ConsolidationExport
    LabelG_Consolidation
  END
Menu::RoyalMail ROUTINE                               !Code for menu items on ?RoyalMail
  CASE ACCEPTED()
  OF ?RoyalMailExport
    RoyalMail_Despatch_Routine
  OF ?RoyalMailImport
    RoyalMail_Import_Routine
  END
Menu::Parceline ROUTINE                               !Code for menu items on ?Parceline
  CASE ACCEPTED()
  OF ?ParcelineExport
    Parceline_Despatch_Routine
  END
Menu::UPS ROUTINE                                     !Code for menu items on ?UPS
  CASE ACCEPTED()
  OF ?UPSExport
    UPS_Despatch_Routine
  END
Menu::Invoicing ROUTINE                               !Code for menu items on ?Invoicing
  CASE ACCEPTED()
  OF ?InvoiceExceptions
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InvoiceExceptions, Accepted)
    Case MessageEx('Are you sure you want to view the Zero Value Exceptions?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
            glo:file_name   = 'I' & FOrmat(Clock(),@n_7) & '.DAT'
            START(Exceptions_Invoice_Wizard,25000)
        Of 2 ! &No Button
    End!Case MessageEx
    
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InvoiceExceptions, Accepted)
  OF ?ProcessValuedJobs
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessValuedJobs, Accepted)
    Case MessageEx('Are you sure you want to Process the Valued Jobs?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
            ! Start Change BE031 BE(02/07/2004)
            !glo:file_name   = 'I' & FOrmat(Clock(),@n_7) & '.DAT'
            glo:tradetmp   = 'I' & FOrmat(Clock(),@n_7) & '.DAT'
            START(Create_Invoice_Wizard,25000)
            ! End Change BE031 BE(02/07/2004)
        Of 2 ! &No Button
    End!Case MessageEx
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessValuedJobs, Accepted)
  OF ?JobValidationAndInvoiceProcedure
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobValidationAndInvoiceProcedure, Accepted)
    glo:tradetmp   = 'I' & FOrmat(Clock(),@n_7) & '.TMP'
    START(View_Proforma,25000)
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobValidationAndInvoiceProcedure, Accepted)
  OF ?Invoices
    Browse_Invoice
  END
Menu::ProceduresInvoicingUtilities ROUTINE            !Code for menu items on ?ProceduresInvoicingUtilities
  CASE ACCEPTED()
  OF ?ProformaMaintenance
    ProformaMaintenance
  END
Menu::ProceduresStockControl ROUTINE                  !Code for menu items on ?ProceduresStockControl
  CASE ACCEPTED()
  OF ?MinimumStockLevel
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MinimumStockLevel, Accepted)
    glo:File_Name   = 'S' & Format(CLock(),@n07) & '.TMP'
    Start(BrowseMinimumStock,25000)
    
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MinimumStockLevel, Accepted)
  OF ?PendingOrders
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PendingOrders, Accepted)
    If CreatePendingOrders()
        Browse_Pending_Orders
    Else !CreatePendingOrders
        Case MessageEx('There are no pending orders.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
    End !CreatePendingOrders
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PendingOrders, Accepted)
  OF ?Receiving
    Browse_Parts_Orders
  OF ?ReplicateMainStoreStock
    Pick_Other_Location('ALL',0)
  END
Menu::ProceduresMailMerge ROUTINE                     !Code for menu items on ?ProceduresMailMerge
  CASE ACCEPTED()
  OF ?RapidLetterPrinting
    RapidLetterPrint
  END
Menu::SearchFacilities ROUTINE                        !Code for menu items on ?SearchFacilities
  CASE ACCEPTED()
  OF ?SearchConsignmentNumbers
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SearchConsignmentNumbers, Accepted)
    glo:file_name   = 'C' & Format(Clock(),@n07) & '.TMP'
    Remove(glo:file_name)
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SearchConsignmentNumbers, Accepted)
    ConsignmentSearch
  END
Menu::Reports ROUTINE                                 !Code for menu items on ?Reports
  CASE ACCEPTED()
  OF ?ReportsJobsStatusReport
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReportsJobsStatusReport, Accepted)
     RUN('sbcr0082.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReportsJobsStatusReport, Accepted)
  OF ?ReportsWebOrderSummaryReport
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReportsWebOrderSummaryReport, Accepted)
     RUN('sbcr0081.exe %' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReportsWebOrderSummaryReport, Accepted)
  END
Menu::ReportWizards ROUTINE                           !Code for menu items on ?ReportWizards
  CASE ACCEPTED()
  OF ?ReportsReportWizardsSIDJobExtract
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReportsReportWizardsSIDJobExtract, Accepted)
    Return" = SIDExtract(0)
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReportsReportWizardsSIDJobExtract, Accepted)
  OF ?StatusReport
    Status_Report_Criteria
  OF ?RepairActivityReport
    EPS_Repair_Activity_Criteria
  OF ?RepairPerformanceReport
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RepairPerformanceReport, Accepted)
    glo:tradetmp   = 'I' & FOrmat(Clock(),@n_7) & '.TMP'
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RepairPerformanceReport, Accepted)
    RepairPerformanceCriteria
  END
Menu::CourierReports ROUTINE                          !Code for menu items on ?CourierReports
  CASE ACCEPTED()
  OF ?ExchangeExceptionsReport
    Exchange_Exceptions_Criteria('E')
  OF ?ExchangeClaimsReport
    Exchange_Exceptions_Criteria('C')
  OF ?CourierCollectionReport
    Date_Range('Courier Collection Report')
  OF ?SpecialDeliveryReport
    Special_Delivery_Criteria('SPECIAL DELIVERY')
  OF ?ExchangeUnitsInChannel
    Units_In_Channel_Criteria
  OF ?ExchangeUnitAuditReport
    Exchange_Unit_Audit_Criteria
  OF ?OverdueLoansReport
    Overdue_Loan_Criteria
  OF ?ToteReport
    ToteReportCriteria
  END
Menu::ReportsStockReports ROUTINE                     !Code for menu items on ?ReportsStockReports
  CASE ACCEPTED()
  OF ?StockCheckReport
    Stock_Check_Criteria
  OF ?StockUsageReport
    Stock_Usage_Criteria
  OF ?PartsNotReceivedReport
    Parts_Not_Received_Criteria
  OF ?StockForecastReport
    Stock_Forecast_Criteria
  OF ?JobPendingPartsReport
    Job_Pending_Parts_Criteria
  OF ?QAFailureReport
    Date_Range('QA Failure Report')
  END
Menu::StockValueReport ROUTINE                        !Code for menu items on ?StockValueReport
  CASE ACCEPTED()
  OF ?stockValueReportSummary
    Stock_Value_Criteria
  OF ?StockValueReportDetailed
    Stock_Value_Detailed_Criteria
  END
Menu::ThirdPartyService ROUTINE                       !Code for menu items on ?ThirdPartyService
  CASE ACCEPTED()
  OF ?ThirdPartyDespatchNote
    Third_Party_Despatch_Criteria
  END
Menu::RetailReports ROUTINE                           !Code for menu items on ?RetailReports
  CASE ACCEPTED()
  OF ?RetailSalesValuationReport
    Retail_Sales_Valuation_Criteria
  END
Menu::ReportsIncomeReports ROUTINE                    !Code for menu items on ?ReportsIncomeReports
  CASE ACCEPTED()
  OF ?IncomeReport
    Income_Report_Criteria
  END
Menu::ReportsStatusReports ROUTINE                    !Code for menu items on ?ReportsStatusReports
  CASE ACCEPTED()
  OF ?StatusSummaryReport
    Date_Range('Status Summary Report')
  OF ?OverdueStatusReport
    Overdue_status_criteria
  OF ?RepairExchangeReport
    RepairExchangeReportCriteria
  OF ?CustomerEnquiryReport
    CustomerEnquiryReportCriteria
  OF ?ColourReport
    NetworkReportCriteria
  OF ?ExchangeReasonReport
    ExchangeReasonCriteria
  END
Menu::RetailSales ROUTINE                             !Code for menu items on ?RetailSales
  CASE ACCEPTED()
  OF ?InsertRetailSales
    Update_Retail_Sales(0)
  OF ?WebOrders
    BrowseWebOrders
  OF ?RetailSalesPickingReconciliation
    BrowsePickingReconciliation
  OF ?SalesAwaitingPayment
    BrowseAwaitingPayment
  OF ?RetailSalesDespatchProcedure
    Browse_To_Despatch
  OF ?RetailBackOrderProcessing
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RetailBackOrderProcessing, Accepted)
    Access:USERS.Open()
    Access:USERS.UseFile()
    Access:USERS.Clearkey(use:Password_Key)
    use:Password    = glo:Password
    Access:USERS.Tryfetch(use:Password_Key)
    
    glo:File_Name   = 'M' & Clip(use:User_Code) & Format(Day(Today()),@n02) & '.TMP'
    glo:File_Name2   = 'N' & Clip(use:User_Code) & Format(Day(Today()),@n02) & '.TMP'
    glo:Tradetmp    = 'O' & Clip(use:User_Code) & Format(Day(Today()),@n02) & '.TMP'
    
    Access:USERS.Close()
    BackOrderProcessing()
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RetailBackOrderProcessing, Accepted)
  OF ?RetailSalesBrowseAllSales
    Browse_All_Retail_Sales
  END
Menu::Help2 ROUTINE                                   !Code for menu items on ?Help2
  CASE ACCEPTED()
  OF ?HelpKeyStages
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?HelpKeyStages, Accepted)
    RUN('keystage.exe /' & Clip(glo:password))
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?HelpKeyStages, Accepted)
  OF ?Help
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Help, Accepted)
    run('c:\windows\hh.exe help.chm')
    If error()
        stop(error())
    End
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Help, Accepted)
  END


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Main',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Main',1)
    SolaceViewVars('DDEServerNumber',DDEServerNumber,'Main',1)
    SolaceViewVars('DdeAppName',DdeAppName,'Main',1)
    SolaceViewVars('ComputerName',ComputerName,'Main',1)
    SolaceViewVars('ComputerNameLength',ComputerNameLength,'Main',1)
    SolaceViewVars('save_tra_id',save_tra_id,'Main',1)
    SolaceViewVars('save_sub_id',save_sub_id,'Main',1)
    SolaceViewVars('tmp:PopupString',tmp:PopupString,'Main',1)
    SolaceViewVars('save_job_id',save_job_id,'Main',1)
    SolaceViewVars('start_help_temp',start_help_temp,'Main',1)
    SolaceViewVars('pos',pos,'Main',1)
    SolaceViewVars('loc:lastcount',loc:lastcount,'Main',1)
    SolaceViewVars('loc:lasttime',loc:lasttime,'Main',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Main',1)
    SolaceViewVars('save_ope_id',save_ope_id,'Main',1)
    SolaceViewVars('save_sup_id',save_sup_id,'Main',1)
    SolaceViewVars('CurrentTab',CurrentTab,'Main',1)
    SolaceViewVars('FP::PopupString',FP::PopupString,'Main',1)
    SolaceViewVars('FP::ReturnValues:RVPtr',FP::ReturnValues:RVPtr,'Main',1)
    SolaceViewVars('comp_name_temp',comp_name_temp,'Main',1)
    SolaceViewVars('AllowUserToExit',AllowUserToExit,'Main',1)
    SolaceViewVars('Date_Temp',Date_Temp,'Main',1)
    SolaceViewVars('Time_Temp',Time_Temp,'Main',1)
    SolaceViewVars('SRCF::Version',SRCF::Version,'Main',1)
    SolaceViewVars('SRCF::Format',SRCF::Format,'Main',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'Main',1)
    SolaceViewVars('BackerDDEName',BackerDDEName,'Main',1)
    SolaceViewVars('FrameTitle',FrameTitle,'Main',1)
    SolaceViewVars('OtherPgmHwnd',OtherPgmHwnd,'Main',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?File;  SolaceCtrlName = '?File';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PrintSetup;  SolaceCtrlName = '?PrintSetup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReLogin;  SolaceCtrlName = '?ReLogin';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FileThemes;  SolaceCtrlName = '?FileThemes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FileThemesMenu;  SolaceCtrlName = '?FileThemesMenu';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FileThemesReenableTheme;  SolaceCtrlName = '?FileThemesReenableTheme';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exit;  SolaceCtrlName = '?Exit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cut;  SolaceCtrlName = '?Cut';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Copy;  SolaceCtrlName = '?Copy';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Paste;  SolaceCtrlName = '?Paste';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdministration;  SolaceCtrlName = '?SystemAdministration';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemDefaults;  SolaceCtrlName = '?SystemDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LicenseDefaults;  SolaceCtrlName = '?LicenseDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GeneralDefaults;  SolaceCtrlName = '?GeneralDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MainDefaults;  SolaceCtrlName = '?MainDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CompulsoryFields;  SolaceCtrlName = '?CompulsoryFields';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StandardTexts;  SolaceCtrlName = '?StandardTexts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PrintingDefaults;  SolaceCtrlName = '?PrintingDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BookingDefaults;  SolaceCtrlName = '?BookingDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EmailDefaults;  SolaceCtrlName = '?EmailDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ServerDefaults;  SolaceCtrlName = '?ServerDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RecipientTypes;  SolaceCtrlName = '?RecipientTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ComputerDefaults;  SolaceCtrlName = '?ComputerDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockControlDefaults;  SolaceCtrlName = '?StockControlDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailSalesDefaults;  SolaceCtrlName = '?RetailSalesDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SageDefaults;  SolaceCtrlName = '?SageDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdminstrationSystemDefaultsImportDefaults;  SolaceCtrlName = '?SystemAdminstrationSystemDefaultsImportDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CRCDefaults;  SolaceCtrlName = '?CRCDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SamsungDefaults;  SolaceCtrlName = '?SamsungDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SiemensDefaults;  SolaceCtrlName = '?SiemensDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VodafoneDefaults;  SolaceCtrlName = '?VodafoneDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WebDefaults;  SolaceCtrlName = '?WebDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdministrationSystemDefaultsSIDDefaults;  SolaceCtrlName = '?SystemAdministrationSystemDefaultsSIDDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SIDInterfaceDefaults;  SolaceCtrlName = '?SIDInterfaceDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SIDServiceCentres;  SolaceCtrlName = '?SIDServiceCentres';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VodafoneAccountRegions;  SolaceCtrlName = '?VodafoneAccountRegions';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SIDAlertDefaults_OLD;  SolaceCtrlName = '?SIDAlertDefaults_OLD';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdministrationSystemDefaultsSIDDefaultsIM;  SolaceCtrlName = '?SystemAdministrationSystemDefaultsSIDDefaultsIM';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AutomatedSIDExtractDefaults;  SolaceCtrlName = '?AutomatedSIDExtractDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StorePerformanceReportDefaults;  SolaceCtrlName = '?StorePerformanceReportDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SIDAlertDefaults;  SolaceCtrlName = '?SIDAlertDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailRepairChargeTiers;  SolaceCtrlName = '?RetailRepairChargeTiers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UserDefaults;  SolaceCtrlName = '?UserDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accesslevels;  SolaceCtrlName = '?accesslevels';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?users;  SolaceCtrlName = '?users';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FinancialDefaults;  SolaceCtrlName = '?FinancialDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChargeRateDefaults;  SolaceCtrlName = '?ChargeRateDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChargeTypes;  SolaceCtrlName = '?ChargeTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RepairTypes;  SolaceCtrlName = '?RepairTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VATCodes;  SolaceCtrlName = '?VATCodes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdminstrationFinancialDefaultsUtilities;  SolaceCtrlName = '?SystemAdminstrationFinancialDefaultsUtilities';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RepriceJobsRoutine;  SolaceCtrlName = '?RepriceJobsRoutine';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdminstrationStockControlDefaults;  SolaceCtrlName = '?SystemAdminstrationStockControlDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?manufacturers;  SolaceCtrlName = '?manufacturers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Suppliers;  SolaceCtrlName = '?Suppliers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SiteLocations;  SolaceCtrlName = '?SiteLocations';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InternalLocations;  SolaceCtrlName = '?InternalLocations';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LoanExchangeStockType;  SolaceCtrlName = '?LoanExchangeStockType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdministrationStockControlDefaultsModelPr;  SolaceCtrlName = '?SystemAdministrationStockControlDefaultsModelPr';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TradeAccountDefaults;  SolaceCtrlName = '?TradeAccountDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TradeAccounts;  SolaceCtrlName = '?TradeAccounts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BrowseTradeSubAccounts;  SolaceCtrlName = '?BrowseTradeSubAccounts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TradeAccountUtilities;  SolaceCtrlName = '?TradeAccountUtilities';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AutoChangeCourier;  SolaceCtrlName = '?AutoChangeCourier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookups;  SolaceCtrlName = '?Lookups';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessories;  SolaceCtrlName = '?accessories';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Action;  SolaceCtrlName = '?Action';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Colours;  SolaceCtrlName = '?Colours';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdminstrationGeneralLookupsCouries;  SolaceCtrlName = '?SystemAdminstrationGeneralLookupsCouries';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?couriers;  SolaceCtrlName = '?couriers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CityLinkServices;  SolaceCtrlName = '?CityLinkServices';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobTurnaroundTimes;  SolaceCtrlName = '?JobTurnaroundTimes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InitialTransitTypes;  SolaceCtrlName = '?InitialTransitTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Letters;  SolaceCtrlName = '?Letters';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MailMergeLetters;  SolaceCtrlName = '?MailMergeLetters';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Networks;  SolaceCtrlName = '?Networks';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PaymentTypes;  SolaceCtrlName = '?PaymentTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QAFailureReasons;  SolaceCtrlName = '?QAFailureReasons';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QueryReasons;  SolaceCtrlName = '?QueryReasons';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SystemAdminstrationGeneralLookupsStatusTypes;  SolaceCtrlName = '?SystemAdminstrationGeneralLookupsStatusTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StatusTypes;  SolaceCtrlName = '?StatusTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TurnaroundTimeUtility;  SolaceCtrlName = '?TurnaroundTimeUtility';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Teams;  SolaceCtrlName = '?Teams';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?thirdPartyRepairers;  SolaceCtrlName = '?thirdPartyRepairers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UnitTypes;  SolaceCtrlName = '?UnitTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse;  SolaceCtrlName = '?Browse';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Contacts;  SolaceCtrlName = '?Contacts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BrowseCommonFaults;  SolaceCtrlName = '?BrowseCommonFaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CommonFaultsCategories;  SolaceCtrlName = '?CommonFaultsCategories';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CommonFaultsList;  SolaceCtrlName = '?CommonFaultsList';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LoanUnits;  SolaceCtrlName = '?LoanUnits';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BrowseExchangeUnits;  SolaceCtrlName = '?BrowseExchangeUnits';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnits;  SolaceCtrlName = '?ExchangeUnits';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ByAuditNumber;  SolaceCtrlName = '?ByAuditNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RapidInsertWizard;  SolaceCtrlName = '?RapidInsertWizard';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BrowseExchangeUnitsMinimum;  SolaceCtrlName = '?BrowseExchangeUnitsMinimum';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeLoanReasons;  SolaceCtrlName = '?ExchangeLoanReasons';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ESNToModelCorrelation;  SolaceCtrlName = '?ESNToModelCorrelation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Bouncers;  SolaceCtrlName = '?Bouncers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EngineersStatus;  SolaceCtrlName = '?EngineersStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BrowsePickingNotes;  SolaceCtrlName = '?BrowsePickingNotes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Utilities;  SolaceCtrlName = '?Utilities';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InternalEmail;  SolaceCtrlName = '?InternalEmail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UsersLoggedIn;  SolaceCtrlName = '?UsersLoggedIn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?NetworkUserEnquiry;  SolaceCtrlName = '?NetworkUserEnquiry';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RemoteTCPIPSetup;  SolaceCtrlName = '?RemoteTCPIPSetup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ServiceBaseChat;  SolaceCtrlName = '?ServiceBaseChat';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ClearAuditTrail;  SolaceCtrlName = '?ClearAuditTrail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReDespatch;  SolaceCtrlName = '?ReDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ManualDespatchConfirmation;  SolaceCtrlName = '?ManualDespatchConfirmation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UtilitiesRemoveBlankRecords;  SolaceCtrlName = '?UtilitiesRemoveBlankRecords';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RapidUpdate;  SolaceCtrlName = '?RapidUpdate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Repackage;  SolaceCtrlName = '?Repackage';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VodaphoneONR;  SolaceCtrlName = '?VodaphoneONR';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobBooking;  SolaceCtrlName = '?JobBooking';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MultipleJobBooking;  SolaceCtrlName = '?MultipleJobBooking';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EstimateProcedure;  SolaceCtrlName = '?EstimateProcedure';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReceiveExchangeUnit;  SolaceCtrlName = '?ReceiveExchangeUnit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StatusChange;  SolaceCtrlName = '?StatusChange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QAProcedure;  SolaceCtrlName = '?QAProcedure';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EngineerUpdate;  SolaceCtrlName = '?EngineerUpdate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?3rdPartyDespatch;  SolaceCtrlName = '?3rdPartyDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RapidUpdateExchangeValidation;  SolaceCtrlName = '?RapidUpdateExchangeValidation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RapidJobUpdate;  SolaceCtrlName = '?RapidJobUpdate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MultipleJobUpdate;  SolaceCtrlName = '?MultipleJobUpdate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AuthoriseLoanReturns;  SolaceCtrlName = '?AuthoriseLoanReturns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReceptionDespatch;  SolaceCtrlName = '?ReceptionDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RestockExchangeUnits;  SolaceCtrlName = '?RestockExchangeUnits';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Procedures;  SolaceCtrlName = '?Procedures';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarrantyProcess;  SolaceCtrlName = '?WarrantyProcess';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PendingClaims;  SolaceCtrlName = '?PendingClaims';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReconcileClaims;  SolaceCtrlName = '?ReconcileClaims';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ImportProcedures;  SolaceCtrlName = '?ImportProcedures';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SamsungImports;  SolaceCtrlName = '?SamsungImports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SiemensImport;  SolaceCtrlName = '?SiemensImport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CRCImport;  SolaceCtrlName = '?CRCImport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?HaysImports;  SolaceCtrlName = '?HaysImports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IntecImport;  SolaceCtrlName = '?IntecImport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExportProcedures;  SolaceCtrlName = '?ExportProcedures';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProceduresExportProceduresJobExports;  SolaceCtrlName = '?ProceduresExportProceduresJobExports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DataExport;  SolaceCtrlName = '?DataExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?NECAuthorisationExport;  SolaceCtrlName = '?NECAuthorisationExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CompletedJobsStatisticsExport;  SolaceCtrlName = '?CompletedJobsStatisticsExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TLCDailyExport;  SolaceCtrlName = '?TLCDailyExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CRCExports;  SolaceCtrlName = '?CRCExports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?HaysExports;  SolaceCtrlName = '?HaysExports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProceduresExportProceduresJobExportsPickingNote;  SolaceCtrlName = '?ProceduresExportProceduresJobExportsPickingNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProceduresExportProceduresStock;  SolaceCtrlName = '?ProceduresExportProceduresStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PartsUsageExport;  SolaceCtrlName = '?PartsUsageExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SparesUsedExport;  SolaceCtrlName = '?SparesUsedExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SparesReceivedExport;  SolaceCtrlName = '?SparesReceivedExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockLevelsExport;  SolaceCtrlName = '?StockLevelsExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PriceListExport;  SolaceCtrlName = '?PriceListExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProceduresExportProceduresEDI;  SolaceCtrlName = '?ProceduresExportProceduresEDI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReceiveData;  SolaceCtrlName = '?ReceiveData';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProcessedData;  SolaceCtrlName = '?ProcessedData';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReplacedPartsData;  SolaceCtrlName = '?ReplacedPartsData';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SagemDailyExport;  SolaceCtrlName = '?SagemDailyExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AlcatelEDIExport;  SolaceCtrlName = '?AlcatelEDIExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VKEdiExport;  SolaceCtrlName = '?VKEdiExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DespatchProcedures;  SolaceCtrlName = '?DespatchProcedures';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ANC;  SolaceCtrlName = '?ANC';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ANCDefaults;  SolaceCtrlName = '?ANCDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ANCDeliverOnly;  SolaceCtrlName = '?ANCDeliverOnly';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ANCDeliverAndCollect;  SolaceCtrlName = '?ANCDeliverAndCollect';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ANCLoanReturns;  SolaceCtrlName = '?ANCLoanReturns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LabelG;  SolaceCtrlName = '?LabelG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ConsolidationExport;  SolaceCtrlName = '?ConsolidationExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RoyalMail;  SolaceCtrlName = '?RoyalMail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RoyalMailExport;  SolaceCtrlName = '?RoyalMailExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RoyalMailImport;  SolaceCtrlName = '?RoyalMailImport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Parceline;  SolaceCtrlName = '?Parceline';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ParcelineExport;  SolaceCtrlName = '?ParcelineExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UPS;  SolaceCtrlName = '?UPS';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UPSExport;  SolaceCtrlName = '?UPSExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Invoicing;  SolaceCtrlName = '?Invoicing';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProceduresInvoicingUtilities;  SolaceCtrlName = '?ProceduresInvoicingUtilities';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProformaMaintenance;  SolaceCtrlName = '?ProformaMaintenance';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InvoiceExceptions;  SolaceCtrlName = '?InvoiceExceptions';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProcessValuedJobs;  SolaceCtrlName = '?ProcessValuedJobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobValidationAndInvoiceProcedure;  SolaceCtrlName = '?JobValidationAndInvoiceProcedure';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Invoices;  SolaceCtrlName = '?Invoices';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProceduresStockControl;  SolaceCtrlName = '?ProceduresStockControl';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MinimumStockLevel;  SolaceCtrlName = '?MinimumStockLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PendingOrders;  SolaceCtrlName = '?PendingOrders';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Receiving;  SolaceCtrlName = '?Receiving';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReplicateMainStoreStock;  SolaceCtrlName = '?ReplicateMainStoreStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProceduresMailMerge;  SolaceCtrlName = '?ProceduresMailMerge';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RapidLetterPrinting;  SolaceCtrlName = '?RapidLetterPrinting';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SearchFacilities;  SolaceCtrlName = '?SearchFacilities';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SearchConsignmentNumbers;  SolaceCtrlName = '?SearchConsignmentNumbers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reports;  SolaceCtrlName = '?Reports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReportWizards;  SolaceCtrlName = '?ReportWizards';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReportsReportWizardsSIDJobExtract;  SolaceCtrlName = '?ReportsReportWizardsSIDJobExtract';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StatusReport;  SolaceCtrlName = '?StatusReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RepairActivityReport;  SolaceCtrlName = '?RepairActivityReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RepairPerformanceReport;  SolaceCtrlName = '?RepairPerformanceReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CourierReports;  SolaceCtrlName = '?CourierReports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeExceptionsReport;  SolaceCtrlName = '?ExchangeExceptionsReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeClaimsReport;  SolaceCtrlName = '?ExchangeClaimsReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CourierCollectionReport;  SolaceCtrlName = '?CourierCollectionReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SpecialDeliveryReport;  SolaceCtrlName = '?SpecialDeliveryReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnitsInChannel;  SolaceCtrlName = '?ExchangeUnitsInChannel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnitAuditReport;  SolaceCtrlName = '?ExchangeUnitAuditReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OverdueLoansReport;  SolaceCtrlName = '?OverdueLoansReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ToteReport;  SolaceCtrlName = '?ToteReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReportsStockReports;  SolaceCtrlName = '?ReportsStockReports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockValueReport;  SolaceCtrlName = '?StockValueReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stockValueReportSummary;  SolaceCtrlName = '?stockValueReportSummary';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockValueReportDetailed;  SolaceCtrlName = '?StockValueReportDetailed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockCheckReport;  SolaceCtrlName = '?StockCheckReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockUsageReport;  SolaceCtrlName = '?StockUsageReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PartsNotReceivedReport;  SolaceCtrlName = '?PartsNotReceivedReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockForecastReport;  SolaceCtrlName = '?StockForecastReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobPendingPartsReport;  SolaceCtrlName = '?JobPendingPartsReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QAFailureReport;  SolaceCtrlName = '?QAFailureReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ThirdPartyService;  SolaceCtrlName = '?ThirdPartyService';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ThirdPartyDespatchNote;  SolaceCtrlName = '?ThirdPartyDespatchNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OverdueReturns;  SolaceCtrlName = '?OverdueReturns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailReports;  SolaceCtrlName = '?RetailReports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailSalesValuationReport;  SolaceCtrlName = '?RetailSalesValuationReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReportsIncomeReports;  SolaceCtrlName = '?ReportsIncomeReports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IncomeReport;  SolaceCtrlName = '?IncomeReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReportsStatusReports;  SolaceCtrlName = '?ReportsStatusReports';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StatusSummaryReport;  SolaceCtrlName = '?StatusSummaryReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OverdueStatusReport;  SolaceCtrlName = '?OverdueStatusReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RepairExchangeReport;  SolaceCtrlName = '?RepairExchangeReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CustomerEnquiryReport;  SolaceCtrlName = '?CustomerEnquiryReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ColourReport;  SolaceCtrlName = '?ColourReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeReasonReport;  SolaceCtrlName = '?ExchangeReasonReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReportsJobsStatusReport;  SolaceCtrlName = '?ReportsJobsStatusReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReportsWebOrderSummaryReport;  SolaceCtrlName = '?ReportsWebOrderSummaryReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailSales;  SolaceCtrlName = '?RetailSales';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InsertRetailSales;  SolaceCtrlName = '?InsertRetailSales';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WebOrders;  SolaceCtrlName = '?WebOrders';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailSalesPickingReconciliation;  SolaceCtrlName = '?RetailSalesPickingReconciliation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SalesAwaitingPayment;  SolaceCtrlName = '?SalesAwaitingPayment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailSalesDespatchProcedure;  SolaceCtrlName = '?RetailSalesDespatchProcedure';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailBackOrderProcessing;  SolaceCtrlName = '?RetailBackOrderProcessing';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailSalesBrowseAllSales;  SolaceCtrlName = '?RetailSalesBrowseAllSales';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Enhancements;  SolaceCtrlName = '?Enhancements';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tile;  SolaceCtrlName = '?Tile';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cascade;  SolaceCtrlName = '?Cascade';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Arrange;  SolaceCtrlName = '?Arrange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?About;  SolaceCtrlName = '?About';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help2;  SolaceCtrlName = '?Help2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?HelpKeyStages;  SolaceCtrlName = '?HelpKeyStages';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse_Jobs_Button;  SolaceCtrlName = '?Browse_Jobs_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StockControl;  SolaceCtrlName = '?StockControl';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Rapid_Update;  SolaceCtrlName = '?Rapid_Update';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button6;  SolaceCtrlName = '?Button6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reports:2;  SolaceCtrlName = '?Reports:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RapidProcedures;  SolaceCtrlName = '?RapidProcedures';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GLODC:ConversionResult = DC:FilesConverter()
  GlobalErrors.SetProcedureName('Main')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Main')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
    FrameTitle= 'ServiceBase 2000 Cellular Progress System'
    OtherPgmHwnd=FindWindow( ,FrameTitle)
    If OtherPgmHwnd then
      If IsIconic(OtherPgmHwnd) then
          OpenIcon(OtherPgmHwnd)
          EJShowWindow(OtherPgmHwnd,1)
       end
     Halt()
     end
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Close(StartWindow)
  OPEN(AppFrame)
  SELF.Opened=True
                                               ! Generated by NetTalk Extension (Start)
  ThisNetClient.DefaultWarningMessage = 'For maintainence reasons this program needs to be closed down. Please immediately save what you are doing and exit the program.'
  ThisNetClient.DefaultCloseDownMessage = 'This application is being closed down by an administrator.'
  ThisNetClient.WarningTimer = 4500
  ThisNetClient.CloseDownTimer = 1000
  ThisNetClient.init('NetCloseApp' & 'cellular', NET:StartService+NET:DontAnnounceNow)
  if ThisNetClient.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  ThisNetClient.Announce(NET:OnlyOncePerThread)     ! Generated by NetTalk Extension
  Start_Screen
  If glo:select1 <> 'OK'
      Halt
  End!If glo:select1 <> 'OK'
  glo:select1 = ''
  Do Login
  
  If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      ?ColourReport{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & ' Report'
      ?Colours{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & 's'
  End !Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
  
  ! Start Change 4916 BE(29/10/2004)
  version" = GetVersionNumber()
  access:defaults.open()
  access:defaults.usefile()
  set(defaults)
  access:defaults.next()
  IF (CLIP(def:Version_Number) <> CLIP(version")) THEN
      def:version_Number = version"
      access:defaults.update()
  END
  access:defaults.close()
  0{Prop:StatusText,3} = 'PC Control Systems Limited �2002 (Ver ' & CLIP(version") & ')'
  
  ! End Change 4916 BE(29/10/2004)
  
  ! Start Ticker
  ! Start Change 4696 BE(15/10/2004)
  Access:DEFAULTS.Open
  Access:DEFAULTS.UseFile()
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  IF (def:TeamPerformanceTicker) THEN
      DdeAppName = 'CELLULAR' & FORMAT(CLOCK(), @t05)
      DDEServerNumber = DDESERVER(CLIP(DdeAppName))
      IF DDEServerNumber THEN
          RUN(CLIP(PATH()) & '\Ticker.exe APP=' & CLIP(DdeAppName))
      END
  END
  Access:Defaults.Close
  ! End Change 4696 BE(15/10/2004)
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ThisNetClient.Kill(Net:StopService+NET:DontAnnounceNow) ! Generated by NetTalk Extension
  ThisNetClient.Announce(NET:OnlyOncePerThread)     ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
    Do log_out
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Main',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    ELSE
      DO Menu::File                                   !Process menu items on ?File menu
      DO Menu::FileThemes                             !Process menu items on ?FileThemes menu
      DO Menu::SystemAdministration                   !Process menu items on ?SystemAdministration menu
      DO Menu::SystemDefaults                         !Process menu items on ?SystemDefaults menu
      DO Menu::GeneralDefaults                        !Process menu items on ?GeneralDefaults menu
      DO Menu::EmailDefaults                          !Process menu items on ?EmailDefaults menu
      DO Menu::SystemAdminstrationSystemDefaultsImportDefaults !Process menu items on ?SystemAdminstrationSystemDefaultsImportDefaults menu
      DO Menu::SystemAdministrationSystemDefaultsSIDDefaults !Process menu items on ?SystemAdministrationSystemDefaultsSIDDefaults menu
      DO Menu::UserDefaults                           !Process menu items on ?UserDefaults menu
      DO Menu::FinancialDefaults                      !Process menu items on ?FinancialDefaults menu
      DO Menu::SystemAdminstrationFinancialDefaultsUtilities !Process menu items on ?SystemAdminstrationFinancialDefaultsUtilities menu
      DO Menu::SystemAdminstrationStockControlDefaults !Process menu items on ?SystemAdminstrationStockControlDefaults menu
      DO Menu::TradeAccountDefaults                   !Process menu items on ?TradeAccountDefaults menu
      DO Menu::TradeAccountUtilities                  !Process menu items on ?TradeAccountUtilities menu
      DO Menu::Lookups                                !Process menu items on ?Lookups menu
      DO Menu::SystemAdminstrationGeneralLookupsCouries !Process menu items on ?SystemAdminstrationGeneralLookupsCouries menu
      DO Menu::SystemAdminstrationGeneralLookupsStatusTypes !Process menu items on ?SystemAdminstrationGeneralLookupsStatusTypes menu
      DO Menu::Browse                                 !Process menu items on ?Browse menu
      DO Menu::BrowseCommonFaults                     !Process menu items on ?BrowseCommonFaults menu
      DO Menu::BrowseExchangeUnits                    !Process menu items on ?BrowseExchangeUnits menu
      DO Menu::Utilities                              !Process menu items on ?Utilities menu
      DO Menu::RapidUpdate                            !Process menu items on ?RapidUpdate menu
      DO Menu::Procedures                             !Process menu items on ?Procedures menu
      DO Menu::WarrantyProcess                        !Process menu items on ?WarrantyProcess menu
      DO Menu::ImportProcedures                       !Process menu items on ?ImportProcedures menu
      DO Menu::ExportProcedures                       !Process menu items on ?ExportProcedures menu
      DO Menu::ProceduresExportProceduresJobExports   !Process menu items on ?ProceduresExportProceduresJobExports menu
      DO Menu::ProceduresExportProceduresStock        !Process menu items on ?ProceduresExportProceduresStock menu
      DO Menu::ProceduresExportProceduresEDI          !Process menu items on ?ProceduresExportProceduresEDI menu
      DO Menu::DespatchProcedures                     !Process menu items on ?DespatchProcedures menu
      DO Menu::ANC                                    !Process menu items on ?ANC menu
      DO Menu::LabelG                                 !Process menu items on ?LabelG menu
      DO Menu::RoyalMail                              !Process menu items on ?RoyalMail menu
      DO Menu::Parceline                              !Process menu items on ?Parceline menu
      DO Menu::UPS                                    !Process menu items on ?UPS menu
      DO Menu::Invoicing                              !Process menu items on ?Invoicing menu
      DO Menu::ProceduresInvoicingUtilities           !Process menu items on ?ProceduresInvoicingUtilities menu
      DO Menu::ProceduresStockControl                 !Process menu items on ?ProceduresStockControl menu
      DO Menu::ProceduresMailMerge                    !Process menu items on ?ProceduresMailMerge menu
      DO Menu::SearchFacilities                       !Process menu items on ?SearchFacilities menu
      DO Menu::Reports                                !Process menu items on ?Reports menu
      DO Menu::ReportWizards                          !Process menu items on ?ReportWizards menu
      DO Menu::CourierReports                         !Process menu items on ?CourierReports menu
      DO Menu::ReportsStockReports                    !Process menu items on ?ReportsStockReports menu
      DO Menu::StockValueReport                       !Process menu items on ?StockValueReport menu
      DO Menu::ThirdPartyService                      !Process menu items on ?ThirdPartyService menu
      DO Menu::RetailReports                          !Process menu items on ?RetailReports menu
      DO Menu::ReportsIncomeReports                   !Process menu items on ?ReportsIncomeReports menu
      DO Menu::ReportsStatusReports                   !Process menu items on ?ReportsStatusReports menu
      DO Menu::RetailSales                            !Process menu items on ?RetailSales menu
      DO Menu::Help2                                  !Process menu items on ?Help2 menu
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Enhancements
      START(Browse_New_Features, 25000)
    OF ?About
      AboutScreen
    OF ?Browse_Jobs_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse_Jobs_Button, Accepted)
      Access:USERS_ALIAS.Open()
      Access:USERS_ALIAS.UseFile()
      Access:ACCAREAS_ALIAS.Open()
      Access:ACCAREAS_ALIAS.UseFile()
      If SecurityCheck('JOB PROGRESS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',icon:hand,'|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0)
              Of 1 ! &OK Button
          End!Case MessagheEx
      Else!if passed" = False
          Browse_Jobs
      End!if passed" = False
      Access:USERS_ALIAS.Close()
      Access:ACCAREAS_ALIAS.Close()
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse_Jobs_Button, Accepted)
    OF ?StockControl
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockControl, Accepted)
      Access:USERS_ALIAS.Open()
      Access:USERS_ALIAS.UseFile()
      Access:ACCAREAS_ALIAS.Open()
      Access:ACCAREAS_ALIAS.UseFile()
      If SecurityCheck('STOCK CONTROL')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',icon:hand,'|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessagheEx
      Else!if passed" = False
          Start(Browse_Stock,25000)
      End!if passed" = False
      Access:USERS_ALIAS.Close()
      Access:ACCAREAS_ALIAS.Close()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockControl, Accepted)
    OF ?Rapid_Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Rapid_Update, Accepted)
      Access:USERS_ALIAS.Open()
      Access:USERS_ALIAS.UseFile()
      Access:ACCAREAS_ALIAS.Open()
      Access:ACCAREAS_ALIAS.UseFile()
      If SecurityCheck('RAPID JOB UPDATE')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',icon:hand,'|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessagheEx
      Else!if passed" = False
          Start(Rapid_Job_Update,25000)
      End!if passed" = False
      Access:USERS_ALIAS.Close()
      Access:ACCAREAS_ALIAS.Close()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Rapid_Update, Accepted)
    OF ?Reports:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reports:2, Accepted)
      ! Start Change 3338 BE(10/10/03)
      Access:USERS_ALIAS.Open()
      Access:USERS_ALIAS.UseFile()
      Access:ACCAREAS_ALIAS.Open()
      Access:ACCAREAS_ALIAS.UseFile()
      If SecurityCheck('CUSTOM REPORTS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',icon:hand, |
                         '&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0)
              Of 1 ! &OK Button
          End!Case MessagheEx
      Else!if passed" = False
          Start(ExternalReports,25000)
      End!if passed" = False
      Access:USERS_ALIAS.Close()
      Access:ACCAREAS_ALIAS.Close()
      ! End Change 3338 BE(10/10/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reports:2, Accepted)
    OF ?RapidProcedures
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RapidProcedures, Accepted)
      ! Start Change 3338 BE(10/10/03)
      Access:USERS_ALIAS.Open()
      Access:USERS_ALIAS.UseFile()
      Access:ACCAREAS_ALIAS.Open()
      Access:ACCAREAS_ALIAS.UseFile()
      If SecurityCheck('RAPID PROCEDURES')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',icon:hand, |
                         '&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0)
              Of 1 ! &OK Button
          End!Case MessagheEx
      Else!if passed" = False
          Start(CallRapids,25000)
      End!if passed" = False
      Access:USERS_ALIAS.Close()
      Access:ACCAREAS_ALIAS.Close()
      ! End Change 3338 BE(10/10/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RapidProcedures, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisNetClient.TakeEvent()                 ! Generated by NetTalk Extension
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Main')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F10Key
              Do Log_Out
              Do Login
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

