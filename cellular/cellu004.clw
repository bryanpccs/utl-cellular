

   MEMBER('cellular.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELLU004.INC'),ONCE        !Local module procedure declarations
                     END


Start_Screen PROCEDURE                                !Generated from procedure template - Window

LocalRequest         LONG
OriginalRequest      LONG
LocalResponse        LONG
FilesOpened          BYTE
WindowOpened         LONG
WindowInitialized    LONG
ForceRefresh         LONG
ASCIIFileSize        LONG
ASCIIBytesThisRead   LONG
ASCIIBytesRead       LONG
ASCIIBytesThisCycle  LONG
ASCIIPercentProgress BYTE
FrameTitle           CSTRING(128)
OtherPgmHwnd         Hwnd !Handle=unsigned=hwnd
tmp:LicenseText      STRING(10000)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW,AT(,,323,345),FONT('Tahoma',8,,),COLOR(COLOR:White),CENTER,IMM,ALRT(AltShiftF12),TIMER(100),GRAY,DOUBLE
                       IMAGE('cel2ktit.gif'),AT(16,4,284,80),USE(?Image3)
                       TEXT,AT(3,90,316,218),USE(tmp:LicenseText),SKIP,VSCROLL,FONT('Tahoma',,,,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                       STRING('Click ''OK'' to assent to the aforementioned conditions.'),AT(77,312),USE(?OKText),TRN,CENTER
                       BUTTON('&OK'),AT(77,324,56,16),USE(?Ok),FLAT,LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(189,324,56,16),USE(?Cancel),FLAT,LEFT,ICON('Cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Start_Screen',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Start_Screen',1)
    SolaceViewVars('OriginalRequest',OriginalRequest,'Start_Screen',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Start_Screen',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Start_Screen',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Start_Screen',1)
    SolaceViewVars('WindowInitialized',WindowInitialized,'Start_Screen',1)
    SolaceViewVars('ForceRefresh',ForceRefresh,'Start_Screen',1)
    SolaceViewVars('ASCIIFileSize',ASCIIFileSize,'Start_Screen',1)
    SolaceViewVars('ASCIIBytesThisRead',ASCIIBytesThisRead,'Start_Screen',1)
    SolaceViewVars('ASCIIBytesRead',ASCIIBytesRead,'Start_Screen',1)
    SolaceViewVars('ASCIIBytesThisCycle',ASCIIBytesThisCycle,'Start_Screen',1)
    SolaceViewVars('ASCIIPercentProgress',ASCIIPercentProgress,'Start_Screen',1)
    SolaceViewVars('FrameTitle',FrameTitle,'Start_Screen',1)
    SolaceViewVars('OtherPgmHwnd',OtherPgmHwnd,'Start_Screen',1)
    SolaceViewVars('tmp:LicenseText',tmp:LicenseText,'Start_Screen',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Image3;  SolaceCtrlName = '?Image3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LicenseText;  SolaceCtrlName = '?tmp:LicenseText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OKText;  SolaceCtrlName = '?OKText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ok;  SolaceCtrlName = '?Ok';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Start_Screen')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Start_Screen')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCESDEF.Open
  Relate:ACTION.Open
  Relate:CITYSERV.Open
  Relate:COLOUR.Open
  Relate:COMMCAT.Open
  Relate:COMMONCP_ALIAS.Open
  Relate:COMMONFA_ALIAS.Open
  Relate:COMMONWP_ALIAS.Open
  Relate:CONSIGN.Open
  Relate:CONTACTS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFEDI.Open
  Relate:DEFEDI2.Open
  Relate:DEFEMAE1.Open
  Relate:DEFEMAI2.Open
  Relate:DEFEMAIL.Open
  Relate:DEFEPS.Open
  Relate:DEFPRINT.Open
  Relate:DEFRAPID.Open
  Relate:DEFSTOCK.Open
  Relate:DEFWEB.Open
  Relate:DESBATCH.Open
  Relate:DISCOUNT.Open
  Relate:EDIBATCH.Open
  Relate:ESNMODEL.Open
  Relate:EXCAUDIT.Open
  Relate:EXCCHRGE.Open
  Relate:IACTION.Open
  Relate:IAPPENG.Open
  Relate:IAPPTYPE.Open
  Relate:IAUDIT.Open
  Relate:ICONTHIST.Open
  Relate:IEXPDEFS.Open
  Relate:IMEILOG.Open
  Relate:INSDEF.Open
  Relate:INVPARTS.Open
  Relate:ISUBCONT.Open
  Relate:JOBBATCH.Open
  Relate:JOBEXACC.Open
  Relate:JOBPAYMT.Open
  Relate:JOBSVODA.Open
  Relate:LETTERS.Open
  Relate:LOAN_ALIAS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:LOGGED.Open
  Relate:MERGE.Open
  Relate:MERGELET.Open
  Relate:MESSAGES.Open
  Relate:MULDESP.Open
  Relate:MULTIDEF.Open
  Relate:NEWFEAT.Open
  Relate:NOTESENG.Open
  Relate:NOTESINV.Open
  Relate:ORDPARTS_ALIAS.Open
  Relate:ORDPEND_ALIAS.Open
  Relate:PARTS_ALIAS.Open
  Relate:PAYTYPES.Open
  Relate:PENDMAIL.Open
  Relate:PROCCODE.Open
  Relate:PROINV.Open
  Relate:QAREASON.Open
  Relate:QUERYREA.Open
  Relate:REPTYDEF.Open
  Relate:RETDESNO.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:SIDALDEF.Open
  Relate:SIDALER2.Open
  Relate:SIDDEF.Open
  Relate:STAHEAD.Open
  Relate:STANTEXT.Open
  Relate:STDCHRGE_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WARPARTS_ALIAS.Open
  Access:ACCESSOR.UseFile
  Access:ALLLEVEL.UseFile
  Access:AUDIT.UseFile
  Access:BOUNCER.UseFile
  Access:CHARTYPE.UseFile
  Access:COMMONCP.UseFile
  Access:COMMONFA.UseFile
  Access:COMMONWP.UseFile
  Access:CONTHIST.UseFile
  Access:COURIER.UseFile
  Access:DEFCHRGE.UseFile
  Access:ESTPARTS.UseFile
  Access:EXCHACC.UseFile
  Access:EXCHANGE.UseFile
  Access:EXCHHIST.UseFile
  Access:INVOICE.UseFile
  Access:JOBACC.UseFile
  Access:JOBLOHIS.UseFile
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBVODAC.UseFile
  Access:LOAN.UseFile
  Access:LOANACC.UseFile
  Access:LOANHIST.UseFile
  Access:LOCATION.UseFile
  Access:LOCINTER.UseFile
  Access:LOCSHELF.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAUPA.UseFile
  Access:MANFPALO.UseFile
  Access:MANUFACT.UseFile
  Access:MODELCOL.UseFile
  Access:MODELNUM.UseFile
  Access:NOTESFAU.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:PARTS.UseFile
  Access:PRIORITY.UseFile
  Access:REPAIRTY.UseFile
  Access:STATUS.UseFile
  Access:STDCHRGE.UseFile
  Access:STOCK.UseFile
  Access:STOCKTYP.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  Access:SUBCHRGE.UseFile
  Access:SUBTRACC.UseFile
  Access:SUPPLIER.UseFile
  Access:TEAMS.UseFile
  Access:TRACHAR.UseFile
  Access:TRACHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:TRDMODEL.UseFile
  Access:TRDPARTY.UseFile
  Access:TRDSPEC.UseFile
  Access:UNITTYPE.UseFile
  Access:USELEVEL.UseFile
  Access:USERS.UseFile
  Access:USMASSIG.UseFile
  Access:USUASSIG.UseFile
  Access:WARPARTS.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBEXHIS.UseFile
  Access:RETPAY.UseFile
  Access:RETSALES.UseFile
  Access:RETSTOCK.UseFile
  Access:STOESN.UseFile
  Access:TRDBATCH.UseFile
  Access:JOBNOTES.UseFile
  Access:MERGETXT.UseFile
  Access:AUDSTATS.UseFile
  Access:ICLIENTS.UseFile
  Access:IEMAILS.UseFile
  Access:IENGINEERS.UseFile
  Access:IEQUIP.UseFile
  Access:IEXPUSE.UseFile
  Access:IJOBSTATUS.UseFile
  Access:IJOBTYPE.UseFile
  Access:INSAPPS.UseFile
  Access:INSJOBS.UseFile
  Access:INSMAKE.UseFile
  Access:INSMODELS.UseFile
  Access:IRECEIVE.UseFile
  Access:ISUBPOST.UseFile
  Access:MULDESPJ.UseFile
  Access:PRODCODE.UseFile
  Access:SIDALERT.UseFile
  SELF.FilesOpened = True
  tmp:LicenseText = 'Copyright C 1993-2005 PC Control Systems Ltd.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>SOFTWARE LICENCE AGREEMENT<13,10>Copying, duplicating or otherwise distributing any part of this product without the  prior written consent of an authorised representative of the above is prohibited. This Software is licensed subject to the following conditions.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>LICENCE AGREEMENT. This is a legal agreement between you (either an individual or an entity) and the manufacturer of the computer software  PC Control Systems Ltd. If you operate this SOFTWARE  you are agreeing to be bound by the terms of this agreement. If you do not agree to the terms of this agreement, promptly return the SOFTWARE and the accompanying items (including written materials and binders or other containers) to the place you obtained them from. Discretionary compensation may be paid dependent upon the time elapsed since' & |
                  ' the purchase date of the SOFTWARE in all events any form of compensation will be at the sole discretion of and expressly set by PC Control Systems Ltd.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>GRANT OF LICENCE.  This Licence Agreement permits you to use one copy of the software program ( the "SOFTWARE") on a single computer if you purchased a single user version or on a network of computers if you purchased a network user version. The SOFTWARE is in "use" on a computer when it is loaded into temporary memory (i.e. RAM) or installed into permanent memory (i.e. hard disk, CD-ROM, or other  storage device) of that computer.  Network users are limited to the number of  simultaneous accesses to the SOFTWARE  by the network. This is dependent upon the user version purchased.  The user version is clearly stated on the original SOFTWARE purchase invoice.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>COPYRIGHT:  The enclosed SOFTWARE and Documentation is protected by copyright laws and international treaty provisions and are the proprietary products of PC Control Systems Ltd. and its third party suppliers from whom PC Control Systems Ltd. has licensed  portions of the Software.  Such suppliers are expressly  understood  to be beneficiaries of the terms and provisions of this  Agreement.  All rights that are not expressly  granted are reserved by' & |
                  ' PC Control Systems Ltd. or its suppliers. You must treat the SOFTWARE like any other copyrighted material (e.g. a book or musical recording) except that you may either (a) make one copy of the SOFTWARE solely for backup or archival purposes, or (b) transfer the SOFTWARE to a single hard disk provided you keep the original solely for backup or archival purposes.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>You may not copy the written materials accompanying the SOFTWARE..'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>DUEL MEDIA SOFTWARE . If the SOFTWARE  package contains both 3.5" and 5.25" or 3.5" and CD ROM disks, then you may use only the disks appropriate for your single-user computer or your file server.  You may not use the other disks on any other computer or loan, rent, lease, or transfer them to another user except as part of the permanent transfer (as provided above) of all SOFTWARE and written materials..'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>LIMITED WARRANTY.  PC Control Systems Ltd. warrants that (a) the SOFTWARE will perform substantially in accordance with the accompanying written materials for a period of ninety (90) days from the date of receipt, and (b) any hardware accompanying the SOFTWARE will be free of defects in materials and workmanship under normal use and service for a period of one (1) year from the date of receipt. Any implied warranties on the SOFTWARE and hardware are limited to ninety (90) days and one  (1) year, respectively.  Some jurisdictions do not allow limitations duration of an implied warranty, so the above limitation may not' & |
                      ' apply to you.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>CUSTOMER REMEDIES.  PC Control Systems Ltd. and its suppliers entire liability and your exclusive remedy shall be, at PC Control Systems Ltd.  option, either (a) return of the price paid , or (b) repair or replacement of the SOFTWARE that does not meet PC Control Systems Ltd. Warranty and which is returned to PC Control Systems Ltd. with a copy of your SOFTWARE' & |
                  ' purchase invoice.  This Limited Warranty is void if failure of the SOFTWARE has resulted from accident, abuse, or misapplication.  Any replacement SOFTWARE will be warranted for the remainder of the original warranty period or thirty (30) days, whichever is the longer.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>NO OTHER WARRANTIES.  To the maximum extent permitted by applicable law, PC Control Systems Ltd. and its suppliers disclaim all other warranties, either  express or implied, including, but not limited to implied warranties of merchantability and fitness for a particular purpose, with regard to the SOFTWARE, the accompanying written materials, and any accompanying hardware.  This limited warranty gives you specific legal rights, and you may also have other rights which vary from jurisdiction to jurisdiction.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>NO LIABILITY FOR CONSEQUENTIAL DAMAGES. To the maximum extent permitted by applicable law, in no event shall PC Control Systems Ltd.  or its suppliers be liable for any damage  whatsoever ( including without  limitation, damages for loss of business profits, business interruption, loss of business information, or any other pecuniary loss)  arising  out of the use of or inability to use this product,  even if PC Control Systems Ltd. has been advised  of the possibility  of such damages.  Because some jurisdictions do not allow the exclusion or limitation of liability for consequential or incidental damages, the above limitation may not apply to you.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>PCCS ANNUAL SUPPORT AGREEMENT.  The Annual Support Agreement is compulsory for all Licensed users of PC Control Systems Ltd.  software products.  It will give the Licensee unlimited access to our Technical Support Help Desk during normal office hours. It is to be used for  assistance in the event that ServiceBase (or any other PC Control Systems Limited manufactured software) fails to operate or a general system failure occurs that is affecting PCCS manufactured software.  Access to the Help Desk outside normal office hours must be restricted to reasonable times.  This  type of support is by way of mobile telephony.  Two emergency numbers will be issued to the Licensee.  These numbers may be changed or amended from time to time.  Any change will be published to the Licensee as and when made.  This support must be restricted to emergency calls only relating to business critical issues and only where no manual workaround can be found.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>If required, site visits will be supplied on a chargeable basis along the following guidelines:-'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>On-site:-<9,9>Travel @ 50 pence per mile + On-site labour at �60 per hour'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,9,9>On-site response time is normally within 8 hours.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10,13,10>PCCS ANNUAL LICENSE AGREEMENT. All PC Control Systems Ltd. Software products are subject to an Annual License Agreement.  This agreement is mandatory and must in all cases be renewed on the Due date unless PC Control Systems Ltd. receives a Cancellation Notice in writing clearly stating that the License is to be cancelled and the date the cancellation will commence from.   The Cancellation Notice must be received 90 days before the next License renewal date.   The use of all PC Control Systems Ltd software covered by the Cancellation Notice must be cease on the expiry of the current license or 90 days following receipt of the Cancellation Notice, whichever is the sooner.   In no event will the use of unlicensed PC Control Systems Ltd manufactured software be permitted.   All instances of ' &|
                    'PC Control Systems Ltd software  must be immediately  removed from all machines it was previously loaded onto including both servers, client computers  and/or standalone computers.  Although it is acknowledged that the data contained in the applications is the property of the Licensee, PC Control Systems Ltd unlicensed software applications cannot be utilised to view, amend or manipulate this data.' &|
                    '  The licensee warrants that it will abide by the terms of this condition in particular and all other license conditions in general.'
  OPEN(window)
  SELF.Opened=True
  glo:select1 = 'CANCEL'
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCESDEF.Close
    Relate:ACTION.Close
    Relate:CITYSERV.Close
    Relate:COLOUR.Close
    Relate:COMMCAT.Close
    Relate:COMMONCP_ALIAS.Close
    Relate:COMMONFA_ALIAS.Close
    Relate:COMMONWP_ALIAS.Close
    Relate:CONSIGN.Close
    Relate:CONTACTS.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFEDI.Close
    Relate:DEFEDI2.Close
    Relate:DEFEMAE1.Close
    Relate:DEFEMAI2.Close
    Relate:DEFEMAIL.Close
    Relate:DEFEPS.Close
    Relate:DEFPRINT.Close
    Relate:DEFRAPID.Close
    Relate:DEFSTOCK.Close
    Relate:DEFWEB.Close
    Relate:DESBATCH.Close
    Relate:DISCOUNT.Close
    Relate:EDIBATCH.Close
    Relate:ESNMODEL.Close
    Relate:EXCAUDIT.Close
    Relate:EXCCHRGE.Close
    Relate:IACTION.Close
    Relate:IAPPENG.Close
    Relate:IAPPTYPE.Close
    Relate:IAUDIT.Close
    Relate:ICONTHIST.Close
    Relate:IEXPDEFS.Close
    Relate:IMEILOG.Close
    Relate:INSDEF.Close
    Relate:INVPARTS.Close
    Relate:ISUBCONT.Close
    Relate:JOBBATCH.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:JOBSVODA.Close
    Relate:LETTERS.Close
    Relate:LOAN_ALIAS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:LOGGED.Close
    Relate:MERGE.Close
    Relate:MERGELET.Close
    Relate:MESSAGES.Close
    Relate:MULDESP.Close
    Relate:MULTIDEF.Close
    Relate:NEWFEAT.Close
    Relate:NOTESENG.Close
    Relate:NOTESINV.Close
    Relate:ORDPARTS_ALIAS.Close
    Relate:ORDPEND_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:PAYTYPES.Close
    Relate:PENDMAIL.Close
    Relate:PROCCODE.Close
    Relate:PROINV.Close
    Relate:QAREASON.Close
    Relate:QUERYREA.Close
    Relate:REPTYDEF.Close
    Relate:RETDESNO.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:SIDALDEF.Close
    Relate:SIDALER2.Close
    Relate:SIDDEF.Close
    Relate:STAHEAD.Close
    Relate:STANTEXT.Close
    Relate:STDCHRGE_ALIAS.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WARPARTS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Start_Screen',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Ok
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
      glo:select1 = 'OK'
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Start_Screen')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      If KeyCode() = AltShiftF12
        MessageEx('ACCAREAS: ' & Format((Size(acc:record)/1024),@n6.2) & '<13,10>' & |
                  'ACCESDEF: ' & Format((Size(ACD:record)/1024),@n6.2) & '<13,10>' & |
                  'ACCESSOR: ' & Format((Size(ACR:record)/1024),@n6.2) & '<13,10>' & |
                  'ACTION:   ' & Format((Size(ACT:record)/1024),@n6.2) & '<13,10>' & |
                  'ALLLEVEL: ' & Format((Size(ALL:record)/1024),@n6.2) & '<13,10>' & |
                  'AUDIT:    ' & Format((Size(AUD:record)/1024),@n6.2) & '<13,10>' & |
                  'BOUNCER:  ' & Format((Size(BOU:record)/1024),@n6.2) & '<13,10>' & |
                  'CHARTYPE: ' & Format((Size(CHA:record)/1024),@n6.2) & '<13,10>' & |
                  'CITYSERV: ' & Format((Size(CIT:record)/1024),@n6.2) & '<13,10>' & |
                  'COLOUR:   ' & Format((Size(COL:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMCAT:  ' & Format((Size(CMC:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMONCP: ' & Format((Size(CCP:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMONFA: ' & Format((Size(COM:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMONWP: ' & Format((Size(CWP:record)/1024),@n6.2) & '<13,10>' & |
                  'CONSIGN:  ' & Format((Size(CNS:record)/1024),@n6.2) & '<13,10>' & |
                  'CONTACTS: ' & Format((Size(CON:record)/1024),@n6.2)& '<13,10>' & |
                  'CONTHIST: ' & Format((Size(CHT:record)/1024),@n6.2) & '<13,10>' & |
                  'COURIER:  ' & Format((Size(COU:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFAULTS: ' & Format((Size(DEF:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFCHRGE: ' & Format((Size(DEC:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFEDI:   ' & Format((Size(EDI:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFEDI2:  ' & Format((Size(ED2:record)/1024),@n6.2),'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
      
      
        MessageEx('DEFEMAIL: ' & Format((Size(DEM:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFEMAI2: ' & Format((Size(DEM2:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFEMAE1: ' & Format((Size(DEE1:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFPRINT: ' & Format((Size(DEP:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFRAPID: ' & Format((Size(DER:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFSTOCK: ' & Format((Size(DST:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFWEB:   ' & Format((Size(DEW:record)/1024),@n6.2) & '<13,10>' & |
                  'DESBATCH: ' & Format((Size(DBT:record)/1024),@n6.2) & '<13,10>' & |
                  'DISCOUNT: ' & Format((Size(DIS:record)/1024),@n6.2) & '<13,10>' & |
                  'EDIBATCH: ' & Format((Size(EBT:record)/1024),@n6.2) & '<13,10>' & |
                  'ESNMODEL: ' & Format((Size(ESN:record)/1024),@n6.2) & '<13,10>' & |
                  'ESTPARTS: ' & Format((Size(EPR:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCAUDIT: ' & Format((Size(EXA:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCCHRGE: ' & Format((Size(EXC:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCHACC:  ' & Format((Size(XCA:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCHANGE: ' & Format((Size(XCH:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCHHIST: ' & Format((Size(EXH:record)/1024),@n6.2) & '<13,10>' & |
                  'INVOICE:  ' & Format((Size(INV:record)/1024),@n6.2) & '<13,10>' & |
                  'INVPARTS: ' & Format((Size(IVP:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBACC:   ' & Format((Size(JAC:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBBATCH: ' & Format((Size(JBT:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBEXACC: ' & Format((Size(JEA:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBEXHIS: ' & Format((Size(JXH:record)/1024),@n6.2) ,'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
      
        MessageEx('JOBLOHIS: ' & Format((Size(JLH:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBPAYMT: ' & Format((Size(JPT:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBS:     ' & Format((Size(JOB:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBNOTES: ' & Format((Size(JBN:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBSTAGE: ' & Format((Size(JST:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBSVODA: ' & Format((Size(JVF:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBVODAC: ' & Format((Size(JVA:record)/1024),@n6.2) & '<13,10>' & |
                  'LETTERS:  ' & Format((Size(LET:record)/1024),@n6.2) & '<13,10>' & |
                  'LOAN:     ' & Format((Size(LOA:record)/1024),@n6.2) & '<13,10>' & |
                  'LOANACC:  ' & Format((Size(LAC:record)/1024),@n6.2) & '<13,10>' & |
                  'LOANHIST: ' & Format((Size(LOH:record)/1024),@n6.2) & '<13,10>' & |
                  'LOCATION: ' & Format((Size(LOC:record)/1024),@n6.2) & '<13,10>' & |
                  'LOCINTER: ' & Format((Size(LOI:record)/1024),@n6.2) & '<13,10>' & |
                  'LOCSHELF: ' & Format((Size(LOS:record)/1024),@n6.2) & '<13,10>' & |
                  'LOGGED:   ' & Format((Size(LOG:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFAULO: ' & Format((Size(MFO:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFAULT: ' & Format((Size(MAF:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFAUPA: ' & Format((Size(MAP:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFPALO: ' & Format((Size(MFP:record)/1024),@n6.2) & '<13,10>' & |
                  'MANUFACT: ' & Format((Size(MAN:record)/1024),@n6.2) & '<13,10>' & |
                  'MERGE:    ' & Format((Size(MER:record)/1024),@n6.2) ,'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
        MessageEx('MERGELET: ' & Format((Size(MRG:record)/1024),@n6.2) & '<13,10>' & |
                  'MERGETXT: ' & Format((Size(MRT:record)/1024),@n6.2) & '<13,10>' & |
                  'MESSAGES: ' & Format((Size(MES:record)/1024),@n6.2) & '<13,10>' & |
                  'MODELCOL: ' & Format((Size(MOC:record)/1024),@n6.2) & '<13,10>' & |
                  'MODELNUM: ' & Format((Size(MOD:record)/1024),@n6.2) & '<13,10>' & |
                  'NEWFEAT:  ' & Format((Size(FEA:record)/1024),@n6.2) & '<13,10>' & |
                  'NOTESENG: ' & Format((Size(NOE:record)/1024),@n6.2) & '<13,10>' & |
                  'NOTESFAU: ' & Format((Size(NOF:record)/1024),@n6.2) & '<13,10>' & |
                  'NOTESINV: ' & Format((Size(NOI:record)/1024),@n6.2) & '<13,10>' & |
                  'ORDERS:   ' & Format((Size(ORD:record)/1024),@n6.2) & '<13,10>' & |
                  'ORDPARTS: ' & Format((Size(ORP:record)/1024),@n6.2) & '<13,10>' & |
                  'ORDPEND:  ' & Format((Size(OPE:record)/1024),@n6.2) & '<13,10>' & |
                  'PARTS:    ' & Format((Size(PAR:record)/1024),@n6.2) & '<13,10>' & |
                  'PAYTYPES: ' & Format((Size(PAY:record)/1024),@n6.2) & '<13,10>' & |
                  'PENDMAIL: ' & Format((Size(PEM:record)/1024),@n6.2) & '<13,10>' & |
                  'PRIORITY: ' & Format((Size(PRI:record)/1024),@n6.2) & '<13,10>' & |
                  'PROCCODE: ' & Format((Size(PRO:record)/1024),@n6.2) & '<13,10>' & |
                  'PROINV:   ' & Format((Size(PRV:record)/1024),@n6.2) & '<13,10>' & |
                  'QAREASON: ' & Format((Size(QAR:record)/1024),@n6.2) & '<13,10>' & |
                  'QUERYREA: ' & Format((Size(QUE:record)/1024),@n6.2) & '<13,10>' & |
                  'REPAIRTY: ' & Format((Size(REP:record)/1024),@n6.2),'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
        MessageEx('REPTYDEF: ' & Format((Size(RTD:record)/1024),@n6.2) & '<13,10>' & |
                  'RETDESNO: ' & Format((Size(RdN:record)/1024),@n6.2) & '<13,10>' & |
                  'RETPAY:   ' & Format((Size(RTP:record)/1024),@n6.2) & '<13,10>' & |
                  'RETSALES: ' & Format((Size(RET:record)/1024),@n6.2) & '<13,10>' & |   
                  'RETSTOCK: ' & Format((Size(RES:record)/1024),@n6.2) & '<13,10>' & |
                  'SIDALDEF: ' & Format((Size(SDD:record)/1024),@n6.2) & '<13,10>' & |
                  'SIDALERT: ' & Format((Size(SDL:record)/1024),@n6.2) & '<13,10>' & |
                  'SIDALER2: ' & Format((Size(SDL2:record)/1024),@n6.2) & '<13,10>' & |
                  'STAHEAD:  ' & Format((Size(STH:record)/1024),@n6.2) & '<13,10>' & |
                  'STANTEXT: ' & Format((Size(STT:record)/1024),@n6.2) & '<13,10>' & |
                  'STATUS:   ' & Format((Size(STS:record)/1024),@n6.2) & '<13,10>' & |
                  'STDCHRGE: ' & Format((Size(STA:record)/1024),@n6.2) & '<13,10>' & |
                  'STOCK:    ' & Format((Size(STO:record)/1024),@n6.2) & '<13,10>' & |
                  'STOCKTYP: ' & Format((Size(STP:record)/1024),@n6.2) & '<13,10>' & |
                  'STOESN:   ' & Format((Size(STE:record)/1024),@n6.2) & '<13,10>' & |
                  'STOHIST:  ' & Format((Size(SHI:record)/1024),@n6.2) & '<13,10>' & |
                  'STOMODEL: ' & Format((Size(STM:record)/1024),@n6.2) & '<13,10>' & |
                  'SUBCHRGE: ' & Format((Size(SUC:record)/1024),@n6.2) & '<13,10>' & |
                  'SUBTRACC: ' & Format((Size(SUB:record)/1024),@n6.2) & '<13,10>' & |
                  'SUPPLIER: ' & Format((Size(SUP:record)/1024),@n6.2) & '<13,10>' & |
                  'TEAMS:    ' & Format((Size(TEA:record)/1024),@n6.2) & '<13,10>' & |
                  'TRACHAR:  ' & Format((Size(TCH:record)/1024),@n6.2) & '<13,10>' & |
                  'TRADEACC: ' & Format((Size(TRA:record)/1024),@n6.2) ,'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
        MessageEx('TRANTYPE: ' & Format((Size(TRT:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDBATCH: ' & Format((Size(TRB:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDMODEL: ' & Format((Size(TRM:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDPARTY: ' & Format((Size(TRD:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDSPEC:  ' & Format((Size(TSP:record)/1024),@n6.2) & '<13,10>' & |
                  'TURNARND: ' & Format((Size(TUR:record)/1024),@n6.2) & '<13,10>' & |
                  'UNITTYPE: ' & Format((Size(UNI:record)/1024),@n6.2) & '<13,10>' & |
                  'USELEVEL: ' & Format((Size(LEV:record)/1024),@n6.2) & '<13,10>' & |
                  'USERS:    ' & Format((Size(USE:record)/1024),@n6.2) & '<13,10>' & |
                  'USMASSIG: ' & Format((Size(USM:record)/1024),@n6.2) & '<13,10>' & |
                  'USUMASIG: ' & Format((Size(USU:record)/1024),@n6.2) & '<13,10>' & |
                  'VATCODE:  ' & Format((Size(VAT:record)/1024),@n6.2) & '<13,10>' & |
                  'WARPARTS: ' & Format((Size(WPR:record)/1024),@n6.2),'ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Courier New',10,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
      End!If KeyCode() = ShiftAltF12
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:LoseFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(LoseFocus)
      Error#    = SetWindowPos(0{Prop:Handle},-1,0,0,0,0,3)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(LoseFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

