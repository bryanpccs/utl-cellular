

   MEMBER('sbcr0077.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBCR0001.INC'),ONCE        !Local module procedure declarations
                     END


PartsAvailabilityReport PROCEDURE                     !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
ACCOUNTKEY                    LIKE(GLO:ACCOUNTKEY)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
StartDate            DATE
EndDate              DATE
AccTag               STRING(1)
TempDate             DATE
Date1                DATE
Date2                DATE
Date3                DATE
InvoiceDate          DATE
OrderDate            DATE
SavePath             STRING(255)
Q3Day                LONG
LOC_GROUP            GROUP,PRE(loc)
ApplicationName      STRING(30)
ProgramName          STRING(30)
UserName             STRING(61)
Filename             STRING(255)
                     END
EXCEL_GROUP          GROUP,PRE(xl)
Excel                SIGNED ! OLE Automation holder
VersionString        STRING(50)
Version              REAL
                     END
AvailQ               QUEUE,PRE(AVQ)
Company              STRING(30)
PartNo               STRING(30)
Qty                  LONG
Qty1                 LONG
Qty2                 LONG
Qty3                 LONG
                     END
RecordCount          LONG
NumDays              LONG(3)
BRW4::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
AccTag                 LIKE(AccTag)                   !List box control field - type derived from local data
AccTag_Icon            LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
CSVRecord     File,Driver('BASIC'),Pre(csvrec),Name(Out_Filename),Create,Bindable,Thread
Record                  Record
Company                     STRING(30)
!SaleNo                      STRING(30)    ! ****
!PrevSaleNo                  STRING(30)    ! ****
!OrderNo                     STRING(30)    ! ****
PartNo                      STRING(30)
!OrderDate                   STRING(30)    ! ****
!InvoiceDate                 STRING(30)    ! ****
!Plus1Date                   STRING(30)    ! ****
!Plus2Date                   STRING(30)    ! ****
!Plus3Date                   STRING(30)    ! ****
Qty                         STRING(30)
Qty1                        STRING(30)
Qty2                        STRING(30)
Qty3                        STRING(30)
Qty3Plus                    STRING(30)
UnavailPc                   STRING(30)
                        End
              End
! Excel EQUATES

!----------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic  EQUATE(0FFFFEFF7h) ! Constants.
xlSolid      EQUATE(        1 ) ! Constants.
xlLeft       EQUATE(0FFFFEFDDh) ! Constants.
xlRight      EQUATE(0FFFFEFC8h) ! Constants.
xlLastCell   EQUATE(       11 ) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.

xlCenter     EQUATE(0FFFFEFF4h) ! Excel.Constants
xlBottom     EQUATE(0FFFFEFF5h) ! Excel.Constants
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!----------------------------------------------------
    MAP
CheckAccount    PROCEDURE(STRING arg:Account),BYTE
    END
! Logfile Declarations
!LOGFILE               FILE,DRIVER('ASCII'),OEM,PRE(log),CREATE,BINDABLE,THREAD
!Record                   RECORD,PRE()
!recbuff                     STRING(256)
!                         END
!                     END
!
!               MAP
!OpenLog                  PROCEDURE(STRING f, BYTE newflag=0),BYTE
!CloseLog                 PROCEDURE()
!WriteLog                 PROCEDURE(STRING s, BYTE logflag=0)
!              END
window               WINDOW('Parts Availability Report'),AT(,,199,219),FONT('Tahoma',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       PROMPT('Order Date From'),AT(12,136),USE(?Prompt1),TRN
                       ENTRY(@D08B),AT(84,136,64,10),USE(StartDate)
                       BUTTON,AT(152,136,10,10),USE(?PopCalendar),IMM,FLAT,LEFT,ICON('calenda2.ico')
                       PROMPT('Order Date To'),AT(12,148),USE(?Prompt2),TRN
                       ENTRY(@D08B),AT(84,148,64,10),USE(EndDate)
                       BUTTON,AT(152,148,10,10),USE(?PopCalendar:2),FLAT,LEFT,ICON('calenda2.ico')
                       SHEET,AT(4,2,192,186),USE(?Sheet1),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Report Criteria'),AT(8,8),USE(?Prompt6)
                           LIST,AT(12,20,176,88),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11LJ@s1@60L|M~Account Number~L(2)@s15@'),FROM(Queue:Browse)
                           BUTTON('sho&W tags'),AT(29,53,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag'),AT(12,112,56,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('&Rev tags'),AT(29,73,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('tag &All'),AT(72,112,56,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag all'),AT(132,112,56,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                           PROMPT('No of Days'),AT(12,160),USE(?Prompt5),TRN
                           SPIN(@n-14),AT(84,160,44,12),USE(NumDays),RANGE(3,9999),STEP(1)
                           PROMPT('Record No.'),AT(20,176),USE(?RecordCountPrompt),TRN,HIDE
                           STRING(@n8),AT(64,176,32,),USE(RecordCount),TRN,HIDE,RIGHT
                           PROMPT('Prompt 4'),AT(100,176),USE(?TotalPrompt),HIDE,LEFT
                         END
                       END
                       PANEL,AT(4,192,192,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('E&xport To Excel'),AT(76,196,56,16),USE(?OKButton),FLAT,LEFT,ICON('EXCEL.ICO'),DEFAULT
                       BUTTON('&Cancel'),AT(136,196,56,16),USE(?CancelButton),FLAT,LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Open                   PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?StartDate{prop:ReadOnly} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 15066597
    Elsif ?StartDate{prop:Req} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 8454143
    Else ! If ?StartDate{prop:Req} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 16777215
    End ! If ?StartDate{prop:Req} = True
    ?StartDate{prop:Trn} = 0
    ?StartDate{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?EndDate{prop:ReadOnly} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 15066597
    Elsif ?EndDate{prop:Req} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 8454143
    Else ! If ?EndDate{prop:Req} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 16777215
    End ! If ?EndDate{prop:Req} = True
    ?EndDate{prop:Trn} = 0
    ?EndDate{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?NumDays{prop:ReadOnly} = True
        ?NumDays{prop:FontColor} = 65793
        ?NumDays{prop:Color} = 15066597
    Elsif ?NumDays{prop:Req} = True
        ?NumDays{prop:FontColor} = 65793
        ?NumDays{prop:Color} = 8454143
    Else ! If ?NumDays{prop:Req} = True
        ?NumDays{prop:FontColor} = 65793
        ?NumDays{prop:Color} = 16777215
    End ! If ?NumDays{prop:Req} = True
    ?NumDays{prop:Trn} = 0
    ?NumDays{prop:FontStyle} = font:Bold
    ?RecordCountPrompt{prop:FontColor} = -1
    ?RecordCountPrompt{prop:Color} = 15066597
    ?RecordCount{prop:FontColor} = -1
    ?RecordCount{prop:Color} = 15066597
    ?TotalPrompt{prop:FontColor} = -1
    ?TotalPrompt{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   AccTagQ.ACCOUNTKEY = sub:Account_Number
   GET(AccTagQ,AccTagQ.ACCOUNTKEY)
  IF ERRORCODE()
     AccTagQ.ACCOUNTKEY = sub:Account_Number
     ADD(AccTagQ,AccTagQ.ACCOUNTKEY)
    AccTag = '*'
  ELSE
    DELETE(AccTagQ)
    AccTag = ''
  END
    Queue:Browse.AccTag = AccTag
  IF (AccTag='*')
    Queue:Browse.AccTag_Icon = 2
  ELSE
    Queue:Browse.AccTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(AccTagQ)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     AccTagQ.ACCOUNTKEY = sub:Account_Number
     ADD(AccTagQ,AccTagQ.ACCOUNTKEY)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(AccTagQ)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(AccTagQ)
    GET(AccTagQ,QR#)
    DASBRW::5:QUEUE = AccTagQ
    ADD(DASBRW::5:QUEUE)
  END
  FREE(AccTagQ)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.ACCOUNTKEY = sub:Account_Number
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.ACCOUNTKEY)
    IF ERRORCODE()
       AccTagQ.ACCOUNTKEY = sub:Account_Number
       ADD(AccTagQ,AccTagQ.ACCOUNTKEY)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
XL_SETUP    ROUTINE
        xl:Excel = Create(0, Create:OLE)
        xl:Excel{PROP:Create} = 'Excel.Application'

        xl:Excel{'Application.DisplayAlerts'}  = 0         ! no alerts to the user (do you want to save the file?)
        xl:Excel{'Application.ScreenUpdating'} = 0
        xl:Excel{'Application.Visible'}        = 0
        xl:Excel{'Application.Calculation'}    = xlCalculationManual

        xl:Version = 0
        xl:VersionString = xl:Excel{'Application.OperatingSystem'}

        len# = LEN(CLIP(xl:VersionString))
        LOOP x# = len# TO 1 BY -1
            IF (SUB(xl:VersionString, x#, 1) = ' ') THEN
                xl:Version = CLIP(SUB(xl:VersionString, x#+1, len#-x#))
                BREAK
            END
        END

        xl:Excel{'Application.Workbooks.Add()'}

        xl:Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'} = CLIP(LOC:ProgramName)
        xl:Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'} = CLIP(LOC:UserName)
        xl:Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = CLIP(LOC:ApplicationName)

        xl:Excel{'Sheets("Sheet2").Select'}
        xl:Excel{'ActiveWindow.SelectedSheets.Delete'}

        xl:Excel{'Sheets("Sheet3").Select'}
        xl:Excel{'ActiveWindow.SelectedSheets.Delete'}

        xl:Excel{'Sheets("Sheet1").Select'}
XL_FINALIZE ROUTINE
        xl:Excel{'Application.Calculation'} = xlCalculationAutomatic
        xl:Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        xl:Excel{'Application.ActiveWorkBook.Close()'}
XL_TITLE_ATTRIBUTES ROUTINE

        !xl:Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        xl:Excel{'Selection.Interior.ColorIndex'}        = 6 ! Yellow
        xl:Excel{'Selection.Interior.Pattern'}           = xlSolid
        xL:Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        xl:Excel{'Selection.Font.Bold'} = 1

        ! xlEdgeTop = 8
        xl:Excel{'Selection.Borders(8).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(8).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(8).ColorIndex'} = xlAutomatic

        ! xlEdgeLeft = 7
        xl:Excel{'Selection.Borders(7).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(7).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(7).ColorIndex'} = xlAutomatic

        ! xlEdgeRight = 10
        xl:Excel{'Selection.Borders(10).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(10).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(10).ColorIndex'} = xlAutomatic

        |xlEdgeBottom = 9
        xl:Excel{'Selection.Borders(9).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(9).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(9).ColorIndex'} = xlAutomatic
XL_TITLE_ATTRIBUTES_2 ROUTINE

        !xl:Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        xl:Excel{'Selection.Interior.ColorIndex'}        = 6 ! Yellow
        xl:Excel{'Selection.Interior.Pattern'}           = xlSolid
        xL:Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !xl:Excel{'Selection.Font.Bold'} = 1

        ! xlEdgeTop = 8
        xl:Excel{'Selection.Borders(8).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(8).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(8).ColorIndex'} = xlAutomatic

        ! xlEdgeLeft = 7
        xl:Excel{'Selection.Borders(7).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(7).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(7).ColorIndex'} = xlAutomatic

        ! xlEdgeRight = 10
        xl:Excel{'Selection.Borders(10).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(10).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(10).ColorIndex'} = xlAutomatic

        |xlEdgeBottom = 9
        xl:Excel{'Selection.Borders(9).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(9).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(9).ColorIndex'} = xlAutomatic
XL_DETAIL_ATTRIBUTES    ROUTINE

        ! xlEdgeTop = 8
        xl:Excel{'Selection.Borders(8).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(8).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(8).ColorIndex'} = xlAutomatic

        ! xlEdgeLeft = 7
        xl:Excel{'Selection.Borders(7).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(7).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(7).ColorIndex'} = xlAutomatic

        ! xlEdgeRight = 10
        xl:Excel{'Selection.Borders(10).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(10).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(10).ColorIndex'} = xlAutomatic

        |xlEdgeBottom = 9
        xl:Excel{'Selection.Borders(9).LineStyle'}  = xlContinuous
        xl:Excel{'Selection.Borders(9).Weight'}     = xlThin
        xl:Excel{'Selection.Borders(9).ColorIndex'} = xlAutomatic
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE

    CommandLine = CLIP(COMMAND(''))

    tmpPos = INSTRING('%', CommandLine)
    IF (NOT tmpPos) THEN
        MessageEx('Attempting to use ' & CLIP(LOC:ProgramName) & '<10,13>'           & |
            '   without using ' & CLIP(LOC:ApplicationName) & '.<10,13>'                        & |
            '   Start ' & CLIP(LOC:ApplicationName) & ' and run the report from there.<10,13>',   |
            CLIP(LOC:ApplicationName),                                                            |
            'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
       POST(Event:CloseWindow)
       EXIT
    END

    SET(USERS)
    Access:USERS.Clearkey(use:Password_Key)
    glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
    Access:USERS.Clearkey(use:Password_Key)
    use:Password = glo:Password
    IF (Access:USERS.Tryfetch(use:Password_Key) <> Level:benign) THEN
        MessageEx('Unable to find your logged in user details.', |
                CLIP(LOC:ApplicationName),                                  |
                'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
        POST(Event:CloseWindow)
        EXIT
    END

    IF (CLIP(use:Forename) = '') THEN
        LOC:UserName = use:Surname
    ELSE
        LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname
    END

    IF (CLIP(LOC:UserName) = '') THEN
        LOC:UserName = '<' & use:User_Code & '>'
    END

    EXIT
CheckAccount    PROCEDURE(STRING arg:Account)
    CODE
    GLO:ACCOUNTKEY = arg:Account
    GET(AccTagQ, GLO:ACCOUNTKEY)
    IF (ERRORCODE()) THEN
        RETURN false
    END
    RETURN true
! Logfile Procedures
!OpenLog     PROCEDURE(arg:f, arg:newflag)
!    CODE
!    LOGFILE{PROP:NAME} = CLIP(arg:f)
!
!    IF (EXISTS(arg:f)) THEN
!        IF (arg:newflag) THEN
!            REMOVE(LOGFILE)
!            CREATE(LOGFILE)
!        END
!    ELSE
!        CREATE(LOGFILE)
!    END
!
!    OPEN(LOGFILE)
!    IF (ERRORCODE()) THEN
!        RETURN false
!    END
!    RETURN true
!
!CloseLog     PROCEDURE()
!    CODE
!    CLOSE(LOGFILE)
!    RETURN
!
!
!WriteLog     PROCEDURE(arg:s, arg:logflag)
!    CODE
!    CLEAR(LOGFILE)
!    IF (arg:logflag) THEN
!        log:recbuff = FORMAT(TODAY(), @d06) & ' ' & FORMAT(CLOCK(), @t04) & ' ' & CLIP(arg:s)
!    ELSE
!        log:recbuff = CLIP(arg:s)
!    END
!    ADD(LOGFILE)
!    RETURN

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PartsAvailabilityReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:SUBTRACC.Open
  Access:RETSTOCK.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?StartDate{Prop:Alrt,255} = MouseLeft2
  ?EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW4.Q &= Queue:Browse
  BRW4.AddSortOrder(,sub:Account_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,sub:Account_Number,1,BRW4)
  BIND('AccTag',AccTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(AccTag,BRW4.Q.AccTag)
  BRW4.AddField(sub:Account_Number,BRW4.Q.sub:Account_Number)
  BRW4.AddField(sub:RecordNumber,BRW4.Q.sub:RecordNumber)
  BRW4.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(AccTagQ)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:SUBTRACC.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  EndDate   = TODAY()
  StartDate = DATE(MONTH(EndDate), 1, YEAR(EndDate))
  
  LOC:ApplicationName = 'ServiceBase 2000'
  LOC:ProgramName = 'Parts Availability Report'
  LOC:UserName = ''
  
  DO GetUserName
    
  DISPLAY
  PARENT.Open


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          StartDate = TINCALENDARStyle1(StartDate)
          Display(?StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          EndDate = TINCALENDARStyle1(EndDate)
          Display(?EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OKButton
      ThisWindow.Update
      savepath = PATH()
      
      SET(defaults)
      access:defaults.next()
      
      IF (def:exportpath <> '') THEN
          LOC:Filename = CLIP(def:exportpath) & '\PartsAvailability.xls'
      ELSE
          LOC:Filename = 'C:\PartsAvailability.xls'
      END
      
      IF (NOT FILEDIALOG('Choose File',LOC:Filename,'Excel Files|*.xls|All Files|*.*', |
                                          file:keepdir + file:noerror + file:longname)) THEN
          SETPATH(savepath)
      ELSE
          SETPATH(savepath)
      
          ! DEBUG ********************************
          !OpenLog('C:\PartsAvailability.txt')
          ! DEBUG ********************************
      
          IF (StartDate > EndDate) THEN
              TempDate = EndDate
              EndDate = StartDate
              StartDate = TempDate
          END
      
          SETCURSOR(cursor:wait)
      
          DO XL_SETUP
      
          RecordCount = 0
          TempCount# = 0
          TempCount2# = 0
          UNHIDE(?RecordCountPrompt)
          UNHIDE(?RecordCount)
          DISPLAY()
      
          IF (xl:Version < 5) THEN
              SETCLIPBOARD('Parts Availability Report')
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'Selection.Font.Bold'} = 1
              xl:Excel{'ActiveCell.Offset(1, 0).Select'}
              SETCLIPBOARD('Start Date:')
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              SETCLIPBOARD(FORMAT(StartDate, @D08B))
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'Selection.HorizontalAlignment'} = xlLeft
              xl:Excel{'ActiveCell.Offset(1, -1).Select'}
              SETCLIPBOARD('End Date:')
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              SETCLIPBOARD(FORMAT(EndDate, @D08B))
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'Selection.HorizontalAlignment'} = xlLeft
              xl:Excel{'ActiveCell.Offset(1, -1).Select'}
              SETCLIPBOARD('Created By:')
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              SETCLIPBOARD(CLIP(LOC:Username))
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'ActiveCell.Offset(1, -1).Select'}
              SETCLIPBOARD('Created Date:')
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              SETCLIPBOARD(FORMAT(Today(), @D08B))
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'Selection.HorizontalAlignment'} = xlLeft
              xl:Excel{'ActiveCell.Offset(2, -1).Select'}
          ELSE
              xl:Excel{'ActiveCell.Formula'}  = 'Parts Availability Report'
              xl:Excel{'Selection.Font.Bold'} = 1
              xl:Excel{'ActiveCell.Offset(1, 0).Select'}
              xl:Excel{'ActiveCell.Formula'}  = 'Start Date:'
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.Formula'}  = FORMAT(StartDate, @D08B)
              xl:Excel{'Selection.HorizontalAlignment'} = xlLeft
              xl:Excel{'ActiveCell.Offset(1, -1).Select'}
              xl:Excel{'ActiveCell.Formula'}  = 'End Date:'
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.Formula'}  = FORMAT(EndDate, @D08B)
              xl:Excel{'Selection.HorizontalAlignment'} = xlLeft
              xl:Excel{'ActiveCell.Offset(1, -1).Select'}
              xl:Excel{'ActiveCell.Formula'}  = 'Created By:'
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.Formula'} = CLIP(LOC:Username)
              xl:Excel{'ActiveCell.Offset(1, -1).Select'}
              xl:Excel{'ActiveCell.Formula'}  = 'Created Date:'
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.Formula'} = FORMAT(Today(), @D08B)
              xl:Excel{'Selection.HorizontalAlignment'} = xlLeft
              xl:Excel{'ActiveCell.Offset(2, -1).Select'}
          END
      
          FREE(AvailQ)
          access:RETSALES.clearkey(ret:date_booked_key)
          ret:date_booked = StartDate
          SET(ret:date_booked_key,ret:date_booked_key)
          LOOP
              !IF ((access:retsales.next() <> Level:benign) OR (ret:date_booked > TempDate)) THEN
              !    BREAK
              !END
              ! Explanation:
              ! Cannot stop processing after end date because backorders may be filled after this date
              ! and they would not get included in count of parts not available within 3 days.
              IF (access:retsales.next() <> Level:benign) THEN
                  BREAK
              END
      
              ! Start Change 4638 BE(30/09/2004)
              IF NOT CheckAccount(ret:Account_Number) THEN
                  CYCLE
              END
              ! End Change 4638 BE(30/09/2004)
      
              TempCount# += 1
              TempCount2# += 1
              IF (TempCount2# >= 100) THEN
                  TempCount2# = 0
                  RecordCount = TempCount#
                  DISPLAY(?RecordCount)
              END
      
              access:retstock.clearkey(res:Part_Number_Key)
              res:Ref_Number = ret:Ref_Number
              SET(res:Part_Number_Key,res:Part_Number_Key)
              LOOP
                  IF ((access:retstock.next() <> Level:benign) OR (res:Ref_Number <> ret:Ref_Number)) THEN
                      BREAK
                  END
      
                  ! Ignore Back Order Lines that have been filled
                  ! The newly created orders will account for these parts
                  IF (res:despatched = 'OLD') THEN
                      CYCLE
                  END
      
                  IF (res:Previous_Sale_Number = 0) THEN
                      OrderDate = ret:Date_Booked
                  ELSE
                      access:RETSALES_ALIAS.clearkey(res_ali:ref_number_key)
                      res_ali:ref_number = res:Previous_Sale_Number
                      IF (access:RETSALES_ALIAS.fetch(res_ali:ref_number_key) = Level:Benign) THEN
                          OrderDate = res_ali:Date_Booked
                      ELSE
                          OrderDate = ret:Date_Booked
                      END
                  END
      
                  ! Disregard Lines outside the Order Date Range
                  IF ((OrderDate < StartDate) OR (OrderDate > EndDate)) THEN
                      CYCLE
                  END
      
                  InvoiceDate =  ret:Invoice_date
      
                  ! ******************************************************
                  ! * Need to make this calculation account for weekends *
                  ! * i.e. we are interested in parts invoiced within    *
                  ! *      3 days excluding wekends.                     *
                  ! ******************************************************
      
                  Date1 = OrderDate
                  Date2 = OrderDate
                  Date3 = OrderDate
                  count# = 0
                  LOOP
                      Date3 += 1
                      IF (count# < 2) THEN
                          Date2 += 1
                      END
                      IF (count# < 1) THEN
                          Date1 += 1
                      END
                      DayNo# = Date3 % 7
                      IF ((DayNo# = 6) OR (DayNo# = 0)) THEN
                          Cycle
                      END
                      count# += 1
                      ! Start Change 4638 BE (30/09/2004)
                      !IF (count# >= 3) THEN
                      IF (count# >= NumDays) THEN
                      ! End Change 4638 BE (30/09/2004)
                          BREAK
                      END
                  END
      
                  Qty1# = 0
                  Qty2# = 0
                  Qty3# = 0
                  IF ((res:despatched = 'YES') AND (InvoiceDate >= OrderDate)) THEN
                      IF (InvoiceDate <= Date1) THEN
                          Qty1# = res:Quantity
                      ELSIF (InvoiceDate <= Date2) THEN
                          Qty2# = res:Quantity
                      ELSIF (InvoiceDate <= Date3) THEN
                          Qty3# = res:Quantity
                      END
                  END
      
                  ! DEBUG ********************************************************
                  !WriteLog('Part No = ' & CLIP(res:Part_Number) & |
                  !         ' : Invoice Date = ' & FORMAT(InvoiceDate, @d06b) & |
                  !         ' : Order Date = ' & FORMAT(OrderDate, @d06b) & |
                  !         ' : Quantity = ' & CLIP(res:Quantity))
                  ! DEBUG ********************************************************
      
                  AVQ:PartNo = res:Part_Number
                  GET(AvailQ, +AVQ:PartNo)
                  IF (ERRORCODE()) THEN
                      CLEAR(AvailQ)
                      AVQ:Company  = ret:Company_Name
                      AVQ:PartNo  = res:Part_Number
                      AVQ:Qty = res:Quantity
                      AVQ:Qty1 = Qty1#
                      AVQ:Qty2 = Qty2#
                      AVQ:Qty3 = Qty3#
                      ADD(AvailQ, +AVQ:PartNo)
                      !IF (ERRORCODE()) THEN
                      !    MESSAGE(ERROR())
                      !END
                  ELSE
                      AVQ:Qty += res:Quantity
                      AVQ:Qty1 += Qty1#
                      AVQ:Qty2 += Qty2#
                      AVQ:Qty3 += Qty3#
                      PUT(AvailQ, +AVQ:PartNo)
                      !IF (ERRORCODE()) THEN
                      !    MESSAGE(ERROR())
                      !END
                  END
      
              END
          END
      
          ?TotalPrompt{PROP:Text} = 'of ' & LEFT(CLIP(RECORDS(AVailQ)))
          UNHIDE(?TotalPrompt)
          RecordCount = 0
          DISPLAY(?RecordCount)
      
          IF (xl:Version < 5) THEN
              xl:Excel{'ActiveCell.ColumnWidth'} = 27
              SETCLIPBOARD('Company')
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 8.43
              SETCLIPBOARD('Part No.')
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 15.14
              SETCLIPBOARD('Qty ordered')
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 8.43
              SETCLIPBOARD('Day 1')
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 8.43
              SETCLIPBOARD('Day 2')
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 8.43
              ! Start Change 4638 BE(30/09/2004)
              !SETCLIPBOARD('Day 3')
              SETCLIPBOARD('Day ' & LEFT(NumDays))
              ! End Change 4638 BE(30/09/2004)
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 14.43
              ! Start Change 4638 BE(30/09/2004)
              !SETCLIPBOARD('Over 3 days')
              SETCLIPBOARD('Over ' & LEFT(NumDays) & ' days')
              ! End Change 4638 BE(30/09/2004)
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 13.14
              SETCLIPBOARD('% unavailable')
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'Selection.HorizontalAlignment'} = xlCenter
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(1, -7).Select'}
          ELSE
              xl:Excel{'ActiveCell.ColumnWidth'} = 27
              xl:Excel{'ActiveCell.Formula'}  = 'Company'
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 8.43
              xl:Excel{'ActiveCell.Formula'}  = 'Part No.'
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 15.14
              xl:Excel{'ActiveCell.Formula'}  = 'Qty ordered'
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 8.43
              xl:Excel{'ActiveCell.Formula'}  = 'Day 1'
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 8.43
              xl:Excel{'ActiveCell.Formula'}  = 'Day 2'
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 8.43
              ! Start Change 4638 BE(30/09/2004)
              !xl:Excel{'ActiveCell.Formula'}  = 'Day 3'
              xl:Excel{'ActiveCell.Formula'}  = 'Day ' & LEFT(NumDays)
              ! Start Change 4638 BE(30/09/2004)
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 14.43
              ! Start change 4638 BE(30/09/2004)
              !xl:Excel{'ActiveCell.Formula'}  = 'Over 3 days'
              xl:Excel{'ActiveCell.Formula'}  = 'Over ' & LEFT(NumDays) & ' days'
              ! End change 4638 BE(30/09/2004)
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.ColumnWidth'} = 13.14
              xl:Excel{'ActiveCell.Formula'}  = '% unavailable'
              xl:Excel{'Selection.HorizontalAlignment'} = xlCenter
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(1, -7).Select'}
          END
      
          TotalQty# = 0
          TotalQty1# = 0
          TotalQty2# = 0
          TotalQty3# = 0
      
          LOOP ix# = 1 TO RECORDS(AvailQ)
              GET(AvailQ, ix#)
      
              RecordCount += 1
              DISPLAY(?RecordCount)
      
              TotalQty# += AVQ:Qty
              TotalQty1# += AVQ:Qty1
              TotalQty2# += AVQ:Qty2
              TotalQty3# += AVQ:Qty3
      
              Qty3Plus# = AVQ:Qty - (AVQ:Qty1 + AVQ:Qty2 + AVQ:Qty3)
              x$ = 0
              IF ((Qty3Plus# > 0) AND (AVQ:Qty > 0)) THEN
                  x$ = (Qty3Plus# / AVQ:Qty) * 100
              END
      
              IF (xl:Version < 5) THEN
                  SETCLIPBOARD(CLIP(AVQ:Company))
                  xl:Excel{'ActiveSheet.Paste()'}
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  SETCLIPBOARD(CLIP(AVQ:PartNo))
                  xl:Excel{'ActiveSheet.Paste()'}
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  SETCLIPBOARD(FORMAT(AVQ:Qty, @n8))
                  xl:Excel{'ActiveSheet.Paste()'}
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  SETCLIPBOARD(FORMAT(AVQ:Qty1, @n8))
                  xl:Excel{'ActiveSheet.Paste()'}
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  SETCLIPBOARD(FORMAT(AVQ:Qty2, @n8))
                  xl:Excel{'ActiveSheet.Paste()'}
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  SETCLIPBOARD(FORMAT(AVQ:Qty3, @n8))
                  xl:Excel{'ActiveSheet.Paste()'}
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  SETCLIPBOARD(FORMAT(Qty3Plus#, @n8))
                  xl:Excel{'ActiveSheet.Paste()'}
                  xl:Excel{'Selection.HorizontalAlignment'} = xlCenter
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  SETCLIPBOARD(CLIP(LEFT(FORMAT(x$, @n3))) & '%')
                  xl:Excel{'ActiveSheet.Paste()'}
                  xl:Excel{'Selection.HorizontalAlignment'} = xlCenter
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(1, -7).Select'}
              ELSE
                  xl:Excel{'ActiveCell.Formula'}  = CLIP(AVQ:Company)
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  xl:Excel{'ActiveCell.Formula'}  = CLIP(AVQ:PartNo)
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  xl:Excel{'ActiveCell.Formula'}  = FORMAT(AVQ:Qty, @n8)
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  xl:Excel{'ActiveCell.Formula'}  = FORMAT(AVQ:Qty1, @n8)
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  xl:Excel{'ActiveCell.Formula'}  =  FORMAT(AVQ:Qty2, @n8)
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  xl:Excel{'ActiveCell.Formula'}  = FORMAT(AVQ:Qty3, @n8)
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  xl:Excel{'ActiveCell.Formula'}  = FORMAT(Qty3Plus#, @n8)
                  xl:Excel{'Selection.HorizontalAlignment'} = xlCenter
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(0, 1).Select'}
                  xl:Excel{'ActiveCell.Formula'}  = CLIP(LEFT(FORMAT(x$, @n3))) & '%'
                  xl:Excel{'Selection.HorizontalAlignment'} = xlCenter
                  DO XL_DETAIL_ATTRIBUTES
                  xl:Excel{'ActiveCell.Offset(1, -7).Select'}
              END
          END
      
          Qty3Days# = TotalQty1# + TotalQty2# + TotalQty3#
          x$ = 0
          IF ((Qty3Days# > 0) AND (TotalQty# > 0)) THEN
              x$ = (Qty3Days# / TotalQty#) * 100
          END
      
          xl:Excel{'ActiveCell.Offset(0, 2).Select'}
      
          IF (xl:Version < 5) THEN
              SETCLIPBOARD(FORMAT(TotalQty#, @n8))
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              SETCLIPBOARD(FORMAT(TotalQty1#, @n8))
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES_2
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              SETCLIPBOARD(FORMAT(TotalQty2#, @n8))
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES_2
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              SETCLIPBOARD(FORMAT(TotalQty3#, @n8))
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES_2
              xl:Excel{'ActiveCell.Offset(2, -5).Select'}
              ! Start Change 4638 BE(30/09/2004)
              !SETCLIPBOARD('Total delivered within 3 days')
              SETCLIPBOARD('Total delivered within ' & LEFT(NumDays) & ' days')
              ! Start Change 4638 BE(30/09/2004)
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'Selection.HorizontalAlignment'} = xlCenter
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(1, 0).Select'}
              SETCLIPBOARD(FORMAT(Qty3Days#, @n8))
              xl:Excel{'ActiveSheet.Paste()'}
              xl:Excel{'Selection.HorizontalAlignment'} = xlCenter
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(2, 0).Select'}
              SETCLIPBOARD('% availability')
              xl:Excel{'ActiveSheet.Paste()'}
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              SETCLIPBOARD(CLIP(LEFT(FORMAT(x$, @n3))) & '%')
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveSheet.Paste()'}
          ELSE
              xl:Excel{'ActiveCell.Formula'}  = FORMAT(TotalQty#, @n8)
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.Formula'}  = FORMAT(TotalQty1#, @n8)
              DO XL_TITLE_ATTRIBUTES_2
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.Formula'}  = FORMAT(TotalQty2#, @n8)
              DO XL_TITLE_ATTRIBUTES_2
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.Formula'}  = FORMAT(TotalQty3#, @n8)
              DO XL_TITLE_ATTRIBUTES_2
              xl:Excel{'ActiveCell.Offset(2, -5).Select'}
              ! Start Change 4638 BE(30/09/2004)
              !xl:Excel{'ActiveCell.Formula'}  = 'Total delivered within 3 days'
              xl:Excel{'ActiveCell.Formula'}  = 'Total delivered within ' & LEFT(NumDays) & ' days'
              ! End Change 4638 BE(30/09/2004)
              xl:Excel{'Selection.HorizontalAlignment'} = xlCenter
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(1, 0).Select'}
              xl:Excel{'ActiveCell.Formula'}  = FORMAT(Qty3Days#, @n8)
              xl:Excel{'Selection.HorizontalAlignment'} = xlCenter
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(2, 0).Select'}
              xl:Excel{'ActiveCell.Formula'}  = '% availability'
              DO XL_TITLE_ATTRIBUTES
              xl:Excel{'ActiveCell.Offset(0, 1).Select'}
              xl:Excel{'ActiveCell.Formula'}  = CLIP(LEFT(FORMAT(x$, @n3))) & '%'
              DO XL_TITLE_ATTRIBUTES
          END
      
          FREE(AvailQ)
      
          DO XL_FINALIZE
      
          SETCURSOR()
      
          ! DEBUG *******************
          !CloseLog()
          ! DEBUG *******************
      
          MESSAGE('Availability Report Complete')
          POST(Event:CloseWindow)
      END
    OF ?CancelButton
      ThisWindow.Update
      POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     AccTagQ.ACCOUNTKEY = sub:Account_Number
     GET(AccTagQ,AccTagQ.ACCOUNTKEY)
    IF ERRORCODE()
      AccTag = ''
    ELSE
      AccTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (AccTag='*')
    SELF.Q.AccTag_Icon = 2
  ELSE
    SELF.Q.AccTag_Icon = 1
  END


BRW4.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     AccTagQ.ACCOUNTKEY = sub:Account_Number
     GET(AccTagQ,AccTagQ.ACCOUNTKEY)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue

