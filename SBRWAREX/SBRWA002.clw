

   MEMBER('SBRWAREX.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBRWA002.INC'),ONCE        !Local module procedure declarations
                     END


ExportProcess PROCEDURE (func:StartDate,func:EndDate) !Generated from procedure template - Process

Progress:Thermometer BYTE
tmp:StartDate        DATE
tmp:EndDate          DATE
SheetDesc            CSTRING(41)
tmp:LinePrice        REAL
tmp:AccountName      STRING(30)
Process:View         VIEW(RETSALES)
                       PROJECT(ret:Ref_Number)
                       JOIN(res:Part_Number_Key,ret:Ref_Number)
                       END
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,148,60),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(4,4,140,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(4,30,140,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,40,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
?F1SS                EQUATE(1000)
!# SW2 Extension            STRING(3)
f1Action             BYTE
f1FileName           CSTRING(256)
RowNumber            USHORT
StartRow             LONG
StartCol             LONG
Template             CSTRING(129)
f1Disabled           BYTE(0)
ssFieldQ              QUEUE(tqField).
CQ                    QUEUE(tqField).
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Progress:UserString{prop:FontColor} = -1
    ?Progress:UserString{prop:Color} = 15066597
    ?Progress:PctText{prop:FontColor} = -1
    ?Progress:PctText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!---------------------
ssInit         ROUTINE
!---------------------
  DO ssBuildQueue                                     ! Initialize f1 field queue
  IF NOT UsBrowse(SsFieldQ,'ExportProcess',,,,CQ,SheetDesc,StartRow,StartCol,Template)
    ThisWindow.Kill
    RETURN
  END

!---------------------
ssBuildQueue   ROUTINE
!---------------------
    ssFieldQ.Name = 'ret:Ref_Number'
    ssFieldQ.Desc = 'Sales Transaction Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:AccountName'
    ssFieldQ.Desc = 'Trade Account Name'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'ret:Account_Number'
    ssFieldQ.Desc = 'Account Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'ret:Purchase_Order_Number'
    ssFieldQ.Desc = 'Purchase Order Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Format(ret:date_booked,@D17)'
    ssFieldQ.Desc = 'Order Date'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'ret:Consignment_Number'
    ssFieldQ.Desc = 'Consignment Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Format(ret:Date_Despatched,@d17)'
    ssFieldQ.Desc = 'Consignment Date'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'ret:Invoice_Number'
    ssFieldQ.Desc = 'Invoice Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'res:Part_Number'
    ssFieldQ.Desc = 'Part Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'res:Description'
    ssFieldQ.Desc = 'Description'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'res:Quantity'
    ssFieldQ.Desc = 'Quantity'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'res:Purchase_Cost'
    ssFieldQ.Desc = 'Purchase Price'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:LinePrice'
    ssFieldQ.Desc = 'Line Price'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
  SORT(ssFieldQ,+ssFieldQ.Desc)


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ExportProcess')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
    BIND('tmp:AccountName',tmp:AccountName)
    BIND('tmp:LinePrice',tmp:LinePrice)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  DO ssInit
  Relate:RETSALES.Open
  Relate:SUBTRACC.Open
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
  OPEN(ProgressWindow)
  SELF.Opened=True
  Do RecolourWindow
  IF ThisWindow.Response <> RequestCancelled
    UsInitDoc(CQ,?F1ss,StartRow,StartCol,Template)
    ?F1SS{'Sheets("Sheet1").Name'} = SheetDesc
    IF Template = ''
      RowNumber = StartRow + 1
    ELSE
      RowNumber = StartRow
    END
  END
  ! support for CPCS
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:RETSALES, ?Progress:PctText, Progress:Thermometer, ProgressMgr, ret:date_booked)
  ThisProcess.AddSortOrder(ret:Date_Booked_Key)
  ThisProcess.AddRange(ret:date_booked,tmp:StartDate,tmp:EndDate)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Warranty Exchange Export'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(RETSALES,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  Do RecolourWindow


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
    Relate:SUBTRACC.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Progress:Cancel
                                                      ! SW2 This section changed
      IF Message('Would you like to view the partially created spreadsheet?','Process Cancelled',ICON:Question,Button:Yes+Button:No,Button:No)|
      = Button:Yes
        UsDeInit(f1FileName, ?F1SS, 1)
      END
      ReturnValue = Level:Fatal
      RETURN ReturnValue
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ThisWindow.Response = RequestCompleted
    f1FileName = 'WARREXP'
    UsDeInit(f1FileName, ?F1SS, 1)
  
  ELSE
    IF RowNumber THEN ?F1SS{'quit()'}.
  END
  ReturnValue = PARENT.TakeCloseEvent()
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  If ret:Payment_Method <> 'EXC'
      Return Level:User
  End !ret:Payment_Method <> 'EXC'
  
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = ret:Account_Number
  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
      !Found
      tmp:AccountName = sub:Company_Name
  End!If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  
  tmp:LinePrice   = Round(res:Purchase_Cost * res:Quantity,.01)
  ReturnValue = PARENT.TakeRecord()
  UsFillRow(CQ,?F1SS,RowNumber,StartCol)
  RETURN ReturnValue

