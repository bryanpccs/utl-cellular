

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01067.INC'),ONCE        !Local module procedure declarations
                     END



Siemens_Import PROCEDURE                              !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
AccountNumber        STRING(15)
temp_string          STRING(30)
TransitType          STRING(30)
LetterName           STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Import Jobs (Siemens)'),AT(,,247,123),FONT('Arial',8,,),CENTER,GRAY,DOUBLE
                       PANEL,AT(4,4,240,88),USE(?Panel1)
                       PROMPT('Select the file you wish to import'),AT(8,9),USE(?Prompt1),TRN,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                       PROMPT('Filename'),AT(16,24),USE(?Prompt2),TRN
                       ENTRY(@s255),AT(84,24,140,10),USE(glo:file_name),FONT(,,,FONT:bold)
                       BUTTON,AT(228,24,10,10),USE(?LookupFile),SKIP,LEFT,ICON('list3.ico')
                       PROMPT('Letter Name'),AT(16,40),USE(?Prompt3),TRN
                       ENTRY(@s30),AT(84,40,140,10),USE(LetterName),FONT(,,,FONT:bold)
                       BUTTON,AT(228,40,10,10),USE(?LetterButton),SKIP,LEFT,ICON('list3.ico')
                       PROMPT('Account Number'),AT(16,56),USE(?Prompt4),TRN
                       PROMPT('Transit Type'),AT(16,72),USE(?Prompt5),TRN
                       ENTRY(@s30),AT(84,72,140,10),USE(TransitType),FONT(,,,FONT:bold)
                       BUTTON,AT(228,72,10,10),USE(?CallLookup:2),SKIP,LEFT,ICON('list3.ico')
                       ENTRY(@s15),AT(84,56,140,10),USE(AccountNumber),FONT(,,,FONT:bold)
                       BUTTON,AT(228,56,10,10),USE(?CallLookup),SKIP,LEFT,ICON('list3.ico')
                       PANEL,AT(4,96,240,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('Import Jobs'),AT(128,100,56,16),USE(?ImportButton),LEFT,ICON('disk.gif'),DEFAULT
                       BUTTON('Cancel'),AT(184,100,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup4          SelectFileClass
!Save Entry Fields Incase Of Lookup
look:AccountNumber                Like(AccountNumber)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
GetNextRecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
EndPrintRun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Siemens_Import',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('AccountNumber',AccountNumber,'Siemens_Import',1)
    SolaceViewVars('temp_string',temp_string,'Siemens_Import',1)
    SolaceViewVars('TransitType',TransitType,'Siemens_Import',1)
    SolaceViewVars('LetterName',LetterName,'Siemens_Import',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:file_name;  SolaceCtrlName = '?glo:file_name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFile;  SolaceCtrlName = '?LookupFile';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LetterName;  SolaceCtrlName = '?LetterName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LetterButton;  SolaceCtrlName = '?LetterButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TransitType;  SolaceCtrlName = '?TransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup:2;  SolaceCtrlName = '?CallLookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AccountNumber;  SolaceCtrlName = '?AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CallLookup;  SolaceCtrlName = '?CallLookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ImportButton;  SolaceCtrlName = '?ImportButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Siemens_Import')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Siemens_Import')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFEPS.Open
  Relate:LETTERS.Open
  Relate:MERGELET.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Access:SUBTRACC.UseFile
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:JOBNOTES.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
    Loop DBHControl# = Firstfield() To LastField()
        If DBHControl#{prop:type} = Create:OPTION Or |
            DBHControl#{prop:type} = Create:RADIO Or |
            DBhControl#{prop:type} = Create:GROUP
            DBHControl#{prop:trn} = 1      
        End!DBhControl#{prop:type} = 'GROUP'
    End!Loop DBHControl# = Firstfield() To LastField()
      SET(defeps)
      IF (access:defeps.next() <> Level:benign) THEN
          beep(beep:systemhand)  ;  yield()
          message('You have not setup the defaults for the Siemens Import.', |
                  'ServiceBase 2000', icon:hand)
          POST(Event:CloseWindow)
      END
  
      IF (dee:account_number = '') THEN
          beep(beep:systemhand)  ;  yield()
          message('You have not setup the defaults for the Siemens Import.', |
                      'ServiceBase 2000', icon:hand)
          POST(Event:CloseWindow)
      End
  
      AccountNumber = dee:Account_Number
      TransitType = dee:Transit_Type
  FileLookup4.Init
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:LongName)
  FileLookup4.SetMask('CSV Files','*.csv')
  FileLookup4.AddMask('Text Files','*.txt')
  FileLookup4.AddMask('All Files','*.*')
  FileLookup4.DefaultFile='EPSJOBS.CSV'
  FileLookup4.WindowTitle='Pick File'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFEPS.Close
    Relate:LETTERS.Close
    Relate:MERGELET.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Siemens_Import',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Transit_Types
      Browse_Sub_Accounts
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?ImportButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
          IF (glo:file_name = '') THEN
              beep(beep:systemexclamation)  ;  yield()
              message('Please select an input file. ', |
                      'ServiceBase 2000', icon:exclamation)
          ELSE
              access:epscsv.open()
              access:epscsv.usefile()
              glo:file_name2 = 'S' & Sub(Clock(),1,7) & '.DAT'
              Remove(epsimp)
              access:epsimp.open()
              access:epsimp.usefile()
      
              recordspercycle     = 25
              recordsprocessed    = 0
              percentprogress     = 0
              setcursor(cursor:wait)
              open(progresswindow)
              progress:thermometer    = 0
              ?progress:pcttext{prop:text} = '0% Completed'
              record_count# = 0
              setcursor(cursor:wait)
              set(epscsv)
              loop
                  if access:epscsv.next()
                     break
                  end !if
                  record_count# += 1
              end !loop
              setcursor()
              recordstoprocess    =  record_count#
      
              set(epscsv)
              loop
                  if access:epscsv.next()
                     break
                  end !if
                  Do GetNextRecord2
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
      
                  get(epsimp,0)
                  if access:epsimp.primerecord() = level:benign
      
                      eps:label15 = clip(left(eps:label15))
                      str_len#  = len(eps:label15)
                      str_pos#  = 1
                      eps:label15 = upper(sub(eps:label15,str_pos#,1)) & sub(eps:label15,str_pos#+1,str_len#-1)
                      loop str_pos# = 1 to str_len#
                          if sub(eps:label15,str_pos#,1) = '<11>'
                              eps:label15 = sub(eps:label15,1,str_pos#-1) & ' ' & sub(eps:label15,str_pos#+1,str_len#-1)
                          end
                      end
                      epi:fault_description             = Clip(Upper(eps:label15))
                      epi:model_number                  = Upper(Clip(eps:label1))
                      epi:title                         = Upper(Clip(eps:label4))
                      epi:initial                       = Upper(Sub(eps:label5,1,1))
                      epi:surname                       = Upper(Clip(eps:label6))
                      temp_string = Clip(left(eps:label7))
                      string_length# = Len(temp_string)
                      string_position# = 1
                      temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
                      Loop string_position# = 1 To string_length#
                          If sub(temp_string,string_position#,1) = ' '
                              temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
                          End
                      End
                      eps:label7  = temp_string
      
                      epi:telephone_collection          = Upper(Clip(Eps:Label7))
                      epi:fault_code1                   = Upper(Clip(eps:label9))
                      temp_string = Clip(left(eps:label8))
                      string_length# = Len(temp_string)
                      string_position# = 1
                      temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
                      Loop string_position# = 1 To string_length#
                          If sub(temp_string,string_position#,1) = ' '
                              temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
                          End
                      End
                      eps:label8    = temp_string
                      epi:esn                           = Upper(Clip(Eps:label8))
                      Eps:label10 = clip(left(Eps:label10))
                      str_len#  = len(Eps:label10)
                      str_pos#  = 1
                      Eps:label10 = upper(sub(Eps:label10,str_pos#,1)) & sub(Eps:label10,str_pos#+1,str_len#-1)
                      loop str_pos# = 1 to str_len#
                          if sub(Eps:label10,str_pos#,1) = '<11>'
                              Eps:label10 = sub(Eps:label10,1,str_pos#-1) & ' ' & sub(Eps:label10,str_pos#+1,str_len#-1)
                          end
                      end
                      eps:label10 = Clip(left(eps:label10))
                      epi:company_name_collection       = Upper(Clip(eps:label10))
                      Eps:label11 = clip(left(Eps:label11))
                      str_len#  = len(Eps:label11)
                      str_pos#  = 1
                      Eps:label11 = upper(sub(Eps:label11,str_pos#,1)) & sub(Eps:label11,str_pos#+1,str_len#-1)
                      loop str_pos# = 1 to str_len#
                          if sub(Eps:label11,str_pos#,1) = '<11>'
                              Eps:label11 = sub(Eps:label11,1,str_pos#-1) & ' ' & sub(Eps:label11,str_pos#+1,str_len#-1)
                          end
                      end
                      epi:address_line1_collection      = Upper(Clip(Eps:label11))
                      Eps:label12 = clip(left(Eps:label12))
                      str_len#  = len(Eps:label12)
                      str_pos#  = 1
                      Eps:label12 = upper(sub(Eps:label12,str_pos#,1)) & sub(Eps:label12,str_pos#+1,str_len#-1)
                      loop str_pos# = 1 to str_len#
                          if sub(ePS:label12,str_pos#,1) = '<11>'
                              Eps:label12 = sub(Eps:label12,1,str_pos#-1) & ' ' & sub(Eps:label12,str_pos#+1,str_len#-1)
                          end
                      end
                      epi:address_line2_collection      = Upper(Clip(eps:label12)) 
                      eps:label13 = clip(left(eps:label13))
                      str_len#  = len(eps:label13)
                      str_pos#  = 1
                      eps:label13 = upper(sub(eps:label13,str_pos#,1)) & sub(eps:label13,str_pos#+1,str_len#-1)
                      loop str_pos# = 1 to str_len#
                          if sub(eps:label13,str_pos#,1) = '<11>'
                              eps:label13 = sub(eps:label13,1,str_pos#-1) & ' ' & sub(eps:label13,str_pos#+1,str_len#-1)
                          end
                      end
                      epi:address_line3_collection      = Upper(Clip(eps:label13))
                      epi:order_number                  = Upper(Clip(eps:label16))
                      epi:postcode_collection           = Upper(Clip(eps:label14))
                      epi:passed = 'YES'
                      access:modelnum.clearkey(mod:model_number_key)
                      mod:model_number = epi:model_number
                      if access:modelnum.fetch(mod:model_number_key)
                          epi:passed = 'MOD'
                      end!if access:modelnum.fetch(mod:model_number_key)
                      Set(Defaults)
                      access:defaults.next()
                      setclipboard(epi:postcode_collection)
                      If def:use_postcode = 'YES'
                          If ValidatePostcode(epi:Postcode_Collection)
                              If epi:passed = 'MOD'
                                  epi:passed = 'MPS'
                              Else!If epi:passed = 'MOD'
                                  epi:passed = 'POS'
                              End!If epi:passed = 'MOD'
                          End !If ValidatePostcode(epi:Postcode_Collection)
                      End!If def:use_postcode = 'YES'    
                      if access:epsimp.tryinsert()
                          access:epsimp.cancelautoinc()     
                      end    
                  end!if access:epsimp.primerecord() = level:benign
              end !loop
              setcursor()
              ! Start Change 2756 BE(18/06/03)
              !setcursor()
              ! Start Change 2756 BE(18/06/03)
              close(progresswindow)
              access:epscsv.close()
      
              beep(beep:systemexclamation)  ;  yield()
              message('The Import File has been read. '&|
                      '|Click OK to view the file before importing.', |
                      'ServiceBase 2000', icon:exclamation)
      
              globalresponse = requestcancelled
              Browse_Siemens_Import
              If Globalresponse = RequestCompleted
                  recordspercycle     = 25
                  recordsprocessed    = 0
                  percentprogress     = 0
                  setcursor(cursor:wait)
                  open(progresswindow)
                  progress:thermometer    = 0
                  ?progress:pcttext{prop:text} = '0% Completed'
                  recordstoprocess    =  Records(glo:Queue)
      
                  Loop x# = 1 To Records(glo:Queue)
                      Get(glo:Queue,x#)
                      Do GetNextRecord2
                      access:epsimp.clearkey(epi:record_number_key)
                      epi:record_number = glo:pointer
                      access:epsimp.fetch(epi:record_number_key)
                      if access:jobs.primerecord() = level:benign
                          GetStatus(0,1,'JOB') !new job booking
      
                          If access:jobnotes.primerecord() = Level:Benign
                              jbn:RefNumber   = job:ref_number
                              jbn:fault_description             = epi:fault_description
                              access:jobnotes.tryinsert()
                          End!If access:jobnotes.primerecord() = Level:Benign
                          access:users.clearkey(use:password_key)
                          use:password    =glo:password
                          If access:users.fetch(use:password_key) = Level:Benign
                              job:who_booked                    = use:user_code
                          End
                          job:date_booked                   = Today()
                          job:time_booked                   = Clock()
                          job:warranty_job                  = 'YES'
                          job:model_number                  = epi:model_number
                          access:modelnum.clearkey(mod:model_number_key)
                          mod:model_number = job:model_number
                          if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                              job:manufacturer               = mod:manufacturer
                              If mod:product_type <> ''
                                  job:fault_code1 = mod:product_type
                              End!If mod:product_type <> ''
                              If mod:specify_unit_type = 'YES'
                                  job:unit_type = mod:unit_type
                              End
                          end
                          job:esn                           = epi:esn
                          job:msn                           = 'N/A'
                          job:order_number                  = epi:order_number
                          job:title                         = epi:title
                          job:initial                       = epi:initial
                          job:surname                       = epi:surname
                          job:postcode_collection           = epi:postcode_collection
                          job:company_name_collection       = epi:company_name_collection
                          job:address_line1_collection      = epi:address_line1_collection
                          job:address_line2_collection      = epi:address_line2_collection
                          job:address_line3_collection      = epi:address_line3_collection
                          job:telephone_collection          = epi:telephone_collection
                          JOB:Postcode_Delivery             = job:postcode_collection
                          JOB:Address_Line1_Delivery        = JOB:Address_Line1_Collection
                          JOB:Address_Line2_Delivery        = JOB:Address_Line2_Collection
                          JOB:Address_Line3_Delivery        = JOB:Address_Line3_Collection
                          JOB:Telephone_Delivery            = JOB:Telephone_Collection
                          job:company_name_delivery         = job:company_name_collection
                          job:fault_code3                   = epi:fault_code1
                          job:edi                           = 'EDI'
              
                          Set(defeps)
                          access:defeps.next()
                          ! Start Change 2756 BE(18/06/03)
                          !cjob:account_number                = dee:account_number
                          job:account_number = AccountNumber
                          ! Start Change 2756 BE(18/06/03)
                          access:subtracc.clearkey(sub:account_number_key)
                          sub:account_number = job:account_number
                          if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                              access:tradeacc.clearkey(tra:account_number_key)
                              tra:account_number = sub:main_account_number
                              if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                                  If tra:use_sub_accounts <> 'YES'
                                      JOB:Postcode        =tra:Postcode
                                      JOB:Company_Name    =tra:Company_Name
                                      JOB:Address_Line1   =tra:Address_Line1
                                      JOB:Address_Line2   =tra:Address_Line2
                                      JOB:Address_Line3   =tra:Address_Line3
                                      JOB:Telephone_Number=tra:Telephone_Number
                                      JOB:Fax_Number      =tra:Fax_Number
                                      job:courier = tra:courier_outgoing
                                      job:incoming_courier = tra:courier_incoming
                                      job:exchange_courier = job:courier
                                      job:loan_courier = job:courier
                                      If tra:use_delivery_address = 'YES'
                                          job:postcode_delivery   = tra:postcode
                                          job:company_name_delivery   = tra:company_name
                                          job:address_line1_delivery  = tra:address_line1
                                          job:address_line2_delivery  = tra:address_line2
                                          job:address_line3_delivery  = tra:address_line3
                                          job:telephone_delivery      = tra:telephone_number
                                      End !If tra:use_delivery_address = 'YES'
                                      job:courier_cost = tra:courier_cost
                                      job:courier_cost_warranty = tra:courier_cost
                                  Else
                                      job:courier = sub:courier_outgoing
                                      job:incoming_courier = sub:courier_incoming
                                      job:exchange_courier = job:courier
                                      job:loan_courier = job:courier
                                      If tra:invoice_sub_accounts = 'YES'
                                          JOB:Postcode        =sub:Postcode
                                          JOB:Company_Name    =sub:Company_Name
                                          JOB:Address_Line1   =sub:Address_Line1
                                          JOB:Address_Line2   =sub:Address_Line2
                                          JOB:Address_Line3   =sub:Address_Line3
                                          JOB:Telephone_Number=sub:Telephone_Number
                                          JOB:Fax_Number      =sub:Fax_Number
                                          job:courier_cost = sub:courier_cost
                                          job:courier_cost_warranty = sub:courier_cost
                                      Else
                                          job:courier_cost = tra:courier_cost
                                          job:courier_cost_warranty = tra:courier_cost
                                          JOB:Postcode        =tra:Postcode
                                          JOB:Company_Name    =tra:Company_Name
                                          JOB:Address_Line1   =tra:Address_Line1
                                          JOB:Address_Line2   =tra:Address_Line2
                                          JOB:Address_Line3   =tra:Address_Line3
                                          JOB:Telephone_Number=tra:Telephone_Number
                                          JOB:Fax_Number      =tra:Fax_Number
                                      End
                                      If sub:use_delivery_address = 'YES'
                                          job:postcode_delivery      = sub:postcode
                                          job:company_name_delivery  = sub:company_name
                                          job:address_line1_delivery = sub:address_line1
                                          job:address_line2_delivery = sub:address_line2
                                          job:address_line3_delivery = sub:address_line3
                                          job:telephone_delivery     = sub:telephone_number
                                      End
                                  End
                              end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                          end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                          job:warranty_charge_type          = dee:warranty_charge_type
                          ! Start Change 2756 BE)18/06/03)
                          !job:transit_type                  = dee:transit_type
                          job:transit_type = TransitType
                          ! Start Change 2756 BE)18/06/03)
                          Access:TRANTYPE.ClearKey(trt:Transit_Type_Key)
                          trt:Transit_Type = job:Transit_Type
                          If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                              !Found
                              job:Exchange_Status = trt:ExchangeStatus
                              job:Loan_Status     = trt:LoanStatus
                              If job:Location = '' and trt:InternalLocation <> ''
                                  job:Location    = trt:InternalLocation
                              End !If job:Location = '' and trt:InternalLocation <> ''
                          Else!If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                          job:turnaround_time               = dee:job_priority
                          job:unit_type                     = dee:unit_type
                          access:jobnotes.primerecord()
                          jbn:refnumber   = job:ref_number
                          jbn:delivery_text                 = dee:delivery_text
                          access:jobnotes.tryinsert()
                          access:turnarnd.clearkey(tur:turnaround_time_key)
                          tur:turnaround_time = job:turnaround_time
                          if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
                              Turnaround_Routine(tur:days,tur:hours,end_date",end_time")
                              job:turnaround_end_date = end_date"
                              job:turnaround_end_time = end_time"
                          end!if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
                          If dee:status_Type <> ''
                              access:status.clearkey(sts:status_key)
                              sts:status = dee:status_type
                              if access:status.fetch(sts:status_key) = Level:Benign
                                  GetStatus(sts:ref_number,1,'JOB')
      
                              end!if access:status.fetch(sts:status_key) = Level:Benign
      
                          Else !If dee:status_Type <> ''
                              GetStatus(Sub(trt:Initial_Status,1,3),1,'JOB')
                          End !If dee:status_Type <> ''
              
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:notes         = 'JOB AUTOMATICALLY CREATED THROUGH THE SIEMENS IMPORT ROUTINE'
                              aud:ref_number    = job:ref_number
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'IMPORT ROUTINE'
                              if access:audit.insert()
                                  access:audit.cancelautoinc()
                              end
                          end!if access:audit.primerecord() = level:benign
                          if access:jobs.insert()
                              access:jobs.cancelautoinc()
                          end
      
                          IF (LetterName <> '') THEN
                              PrintLetter(job:Ref_Number,LetterName)
                          END
      
                      end!if access:jobs.primerecord() = level:benign
                  End!Loop x# = 1 To Records(glo:Queue)
                  setcursor()
                  close(progresswindow)
                  beep(beep:systemasterisk)  ;  yield()
                  message('Import Completed.', |
                          'ServiceBase 2000', icon:asterisk)
              End!If Globalresponse = RequestCompleted
              access:epsimp.close
              Remove(epsimp)
          end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
    OF ?CancelButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFile
      ThisWindow.Update
      glo:file_name = Upper(FileLookup4.Ask(1)  )
      DISPLAY
    OF ?LetterButton
      ThisWindow.Update
      LetterName = BrowseAllLetters()
      ThisWindow.Reset
    OF ?CallLookup:2
      ThisWindow.Update
      trt:Transit_Type = TransitType
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        TransitType = trt:Transit_Type
      END
      ThisWindow.Reset(1)
    OF ?AccountNumber
      IF AccountNumber OR ?AccountNumber{Prop:Req}
        sub:Account_Number = AccountNumber
        !Save Lookup Field Incase Of error
        look:AccountNumber        = AccountNumber
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            AccountNumber = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            AccountNumber = look:AccountNumber
            SELECT(?AccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      sub:Account_Number = AccountNumber
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        AccountNumber = sub:Account_Number
      END
      ThisWindow.Reset(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Siemens_Import')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?TransitType
      trt:Transit_Type = TransitType
      IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
        IF SELF.Run(1,SelectRecord) = RequestCompleted
          TransitType = trt:Transit_Type
        END
      END
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

