

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBF01002.INC'),ONCE        !Local module procedure declarations
                     END


Create_Chargeable_Invoice PROCEDURE                   ! Declare Procedure
pos                  STRING(255)
invoice_number_temp  REAL
total_temp           REAL
labour_temp          REAL
parts_temp           REAL
courier_temp         REAL
failed_temp          BYTE
VAT_Rate_Labour_Temp REAL
VAT_Rate_Parts_Temp  REAL
Account_Number_Temp  STRING(15)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
save_job_id   ushort,auto

windowsdir      lpstr(144)
systemdir       word
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Create_Chargeable_Invoice')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
   Relate:INVOICE.Open
   Relate:CHARTYPE.Open
   Relate:JOBSTAGE.Open
   Relate:STATUS.Open
   Relate:STAHEAD.Open
   Relate:VATCODE.Open
   Relate:DEFAULTS.Open
   Relate:USERS.Open
    systemdir   = getsystemdirectory(windowsdir,size(windowsdir))
    Set(defaults)
    access:defaults.next()
    Clear(glo:q_invoice)
    Free(glo:q_invoice)

    If Records(glo:Queue)
        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        setcursor(cursor:wait)
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'
        count_exceptions$ = 0
        recordstoprocess = records(job:ChaInvoiceKey)

        Loop x$ = 1 To Records(glo:Queue)                                                !Loop Through Trade Account 
            Get(glo:Queue,x$)                                                            !Tag List
            ?progress:userstring{prop:text} = 'Trade Account: ' & Clip(glo:pointer)
            Do getnextrecord2
            job_total$ = 0
            labour_temp = 0
            parts_temp = 0
            courier_temp = 0
!Check Trade Account And Vat Codes
            vat_rate_labour_temp = 0
            vat_rate_parts_temp = 0
            despatch# = 0

            access:subtracc.clearkey(sub:account_number_key)                                !Get Trade Account VAT amounts
            sub:account_number = glo:pointer                                             !And work out if despatch needed
            if access:subtracc.fetch(sub:account_number_key)                                !or not
                Cycle
            Else!if access:subtracc.fetch(sub:account_number_key)
                access:tradeacc.clearkey(tra:account_number_key)
                tra:account_number = sub:main_account_number
                if access:tradeacc.fetch(tra:account_number_key)
                    Cycle
                Else!if access:tradeacc.fetch(tra:account_number_key)
                    If tra:use_sub_accounts = 'YES'
                        If sub:despatch_invoiced_jobs = 'YES'
                            If sub:despatch_paid_jobs = 'YES'
                                despatch# = 2
                            Else!If sub:despatch_paid_jobs = 'YES'
                                despatch# = 1
                            End!If sub:despatch_paid_jobs = 'YES'
                        End!If sub:despatch_invoiced_jobs = 'YES'

                        access:vatcode.clearkey(vat:vat_code_key)
                        vat:vat_code = sub:labour_vat_code
                        if access:vatcode.fetch(vat:vat_code_key)
                            Cycle
                        Else!if access:vatcode.fetch(vat:vat_code_key)
                            vat_rate_labour_temp    = vat:vat_rate
                        end!if access:vatcode.fetch(vat:vat_code_key)
                        access:vatcode.clearkey(vat:vat_code_key)
                        vat:vat_code = sub:parts_vat_code
                        if access:vatcode.fetch(vat:vat_code_key)
                            Cycle
                        Else!if access:vatcode.fetch(vat:vat_code_key)
                            vat_rate_parts_temp    = vat:vat_rate
                        end!if access:vatcode.fetch(vat:vat_code_key)
                    Else!If tra:use_sub_accounts = 'YES'
                        If tra:despatch_invoiced_jobs = 'YES'
                            If tra:despatch_paid_jobs = 'YES'
                                despatch# = 2
                            Else!If tra:despatch_paid_jobs = 'YES'
                                despatch# = 1
                            End!If tra:despatch_paid_jobs = 'YES'
                        End!If tra:despatch_invoiced_jobs = 'YES'
                        access:vatcode.clearkey(vat:vat_code_key)
                        vat:vat_code = tra:labour_vat_code
                        if access:vatcode.fetch(vat:vat_code_key)
                            Cycle
                        Else!if access:vatcode.fetch(vat:vat_code_key)
                            vat_rate_labour_temp    = vat:vat_rate
                        end!if access:vatcode.fetch(vat:vat_code_key)
                        access:vatcode.clearkey(vat:vat_code_key)
                        vat:vat_code = tra:parts_vat_code
                        if access:vatcode.fetch(vat:vat_code_key)
                            Cycle
                        Else!if access:vatcode.fetch(vat:vat_code_key)
                            vat_rate_parts_temp    = vat:vat_rate
                        end!if access:vatcode.fetch(vat:vat_code_key)
                    End!If tra:use_sub_accounts = 'YES'
                end
            end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign

            get(invoice,0)                                                                  !Prime The Invoice
            if access:invoice.primerecord() = level:benign
                count_jobs$ = 0
                setcursor(cursor:wait)
                save_job_id = access:jobs.savefile()
                access:jobs.clearkey(job:ChaInvoiceKey)                            !Loop Through All Ready To
                job:account_number = glo:pointer                                         !Invoice Jobs
                job:chargeable_job = 'YES'
                job:invoice_number = ''
                set(job:ChaInvoiceKey,job:ChaInvoiceKey)
                loop
                    if access:jobs.next()
                       break
                    end !if
                    if job:account_number <> glo:pointer      |
                    or job:chargeable_job <> 'YES' |
                    or job:invoice_number <> ''      |
                        then break.  ! end if
                    If job:invoice_exception = 'YES'
                        Cycle
                    End!If job:invoice_exception = 'YES'|
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    If job:date_completed = ''
                        Cycle
                    Else
                        If def:qa_required = 'YES'
                            If ~(job:qa_passed = 'YES' Or job:qa_second_passed = 'YES')
                                cycle
                            End
                        End
                    End
                    If job:bouncer <> ''
                        job:invoice_exception = 'YES'
                        job:invoice_failure_reason = 'BOUNCER'
                        access:jobs.update()
                        count_exceptions$ += 1
                        Cycle
                    End!If job:bouncer <> ''

                    labour" = ''
                    parts" = ''
                    If job:chargeable_job = 'YES'                                           !Price The Jobs
                        If job:ignore_chargeable_charges <> 'YES'
                            Pricing_Routine('C',labour",parts",pass",a")
                            If pass" = False
                                job:invoice_exception = 'YES'
                                job:invoice_failure_reason = 'NO PRICING STRUCTURE FOUND'
                                access:jobs.update()
                                count_exceptions$ += 1
                                Cycle
                            Else
                                job:labour_cost = labour"
                                job:parts_cost  = parts"
                            End!If pass" = False
                        End!If job:ignore_chargeable_charges <> 'YES'
                    End!If job:chargeable_job = 'YES' and pass" = False                     !Check Thresholds
                    If labour" < glo:select1
                        job:invoice_exception = 'YES'
                        job:invoice_failure_reason = 'LABOUR COST BELOW THRESHOLD'
                        access:jobs.update()
                        count_exceptions$ += 1
                        Cycle
                    End
                    If parts" < glo:select2
                        job:invoice_exception = 'YES'
                        job:invoice_failure_reason = 'PARTS COST BELOW THRESHOLD'
                        access:jobs.update()
                        count_exceptions$ += 1
                        Cycle
                    End                                                                     !Mark As Despatched
                    Case despatch#
                        Of 1
                            If job:paid = 'YES'
                                If job:despatched = 'YES'
                                    GetStatus(910,0,'JOB') !despatched paid

                                Else!If job:despatched = 'YES'
                                    GetStatus(810,0,'JOB') !ready to despatch

                                    job:despatched = 'REA'
                                    job:despatch_type = 'JOB'
                                    job:current_courier = job:courier
                                End!If job:despatched <> 'YES'
                            Else!If job:paid = 'YES'
                                GetStatus(803,0,'JOB') !held awaiting despatch
                                
                            End!If job:paid = 'YES'
                        Of 2
                            If job:despatched = 'YES'
                                If job:paid = 'YES'
                                    GetStatus(910,0,'JOB') !despatched paid


                                Else!If job:paid = 'YES'
                                    GetStatus(905,0,'JOB') !despatched unpaid


                                End!If job:paid = 'YES'
                                
                            Else!If job:despatched = 'YES'
                                GetStatus(810,0,'JOB') !ready to despatch

                                job:despatched = 'REA'
                                job:despatch_type = 'JOB'
                                job:current_courier = job:courier
                            End!If job:despatched <> 'YES'
                    End!Case despatch#

                    labour_temp += job:labour_cost                                          !Work Out Totals
                    parts_temp  += job:parts_cost
                    courier_temp += job:courier_cost
                    total_temp += job:labour_cost + job:parts_cost + job:courier_cost
                    count_jobs$ += 1
                    If def:use_sage <> 'YES'                                                    !Don't Write Invoice Totals
                        pos = Position(job:ChaInvoiceKey)                          !If Using Sage
                        job:invoice_courier_cost = job:courier_cost
                        job:invoice_labour_cost = job:labour_cost
                        job:invoice_parts_cost  = job:parts_cost
                        job:invoice_sub_total = job:invoice_courier_cost + job:invoice_labour_cost + job:invoice_parts_cost
                        job:invoice_exception = 'NO'
                        job:invoice_number  = inv:invoice_number
                        job:invoice_date    = Today()
                        access:jobs.update()
                        Reset(job:ChaInvoiceKey,pos)
                    End!If def:sage = 'YES'
                end !loop
                access:jobs.restorefile(save_job_id)
                setcursor()                                                                 !End Of Jobs Loop
                If def:use_sage = 'YES'
                    failed_temp = 0                                                         !If using Sage then write the
                    DO sage_bit                                                             !Export files, and get the
                    If failed_temp = 1                                                      !Invoice Number back.
                        access:invoice.cancelautoinc()
                        Cycle
                    End!If failed_temp = 1
                    save_job_id = access:jobs.savefile()                                    !Loop through jobs again
                    access:jobs.clearkey(job:ChaInvoiceKey)
                    job:chargeable_job = 'YES'
                    job:account_number = glo:pointer
                    job:invoice_number = ''
                    set(job:ChaInvoiceKey,job:ChaInvoiceKey)
                    loop
                        if access:jobs.next()
                           break
                        end !if
                        if job:chargeable_job <> 'YES'      |
                        or job:account_number <> glo:pointer      |
                        or job:invoice_number <> ''      |
                            then break.  ! end if
                        If job:invoice_exception <> 'NO'
                            Cycle
                        End!If job:invoice_exception <> 'NO'
                        yldcnt# += 1
                        if yldcnt# > 25
                           yield() ; yldcnt# = 0
                        end !if
                        pos = Position(job:ChaInvoiceKey)                          !Write the Invoice Totals and
                        job:invoice_courier_cost = job:courier_cost                         !Invoice Number
                        job:invoice_labour_cost = job:labour_cost
                        job:invoice_parts_cost  = job:parts_cost
                        job:invoice_sub_total = job:invoice_courier_cost + job:invoice_labour_cost + job:invoice_parts_cost
                        job:invoice_exception = 'NO'
                        job:invoice_number  = invoice_number_temp
                        job:invoice_date    = Today()
                        access:jobs.update()
                        Reset(job:ChaInvoiceKey,pos)
                    end !loop
                    access:jobs.restorefile(save_job_id)
                End!If def:use_sage = 'YES'

                If count_jobs$ = 0                                                          !Have any jobs been found
                    access:invoice.cancelautoinc()
                Else !If count_jobs$ = 0
                    inv:job_number         = job:ref_number
                    inv:invoice_Type       = 'CHA'
                    inv:date_created       = Today()
                    inv:account_number     = glo:pointer
                    inv:total              = total_temp
                    inv:vat_rate_labour    = vat_rate_labour_temp
                    inv:vat_rate_parts     = vat_rate_parts_temp
                    inv:vat_rate_retail    = ''
                    inv:vat_number         = def:vat_number
                    inv:invoice_vat_number = ''
                    inv:currency           = ''
                    inv:batch_number       = ''
                    inv:manufacturer       = ''
                    inv:claim_reference    = ''
                    inv:total_claimed      = ''
                    inv:labour_paid        = labour_temp
                    inv:parts_paid         = parts_temp
                    inv:courier_paid       = courier_temp
                    inv:reconciled_date    = ''
                    If def:use_sage = 'YES'                                                 !Fill in the invoice number is Sage
                        inv:invoice_number = invoice_number_temp
                    Else
                        glo:invoice_number = inv:invoice_number                          !Fill in Queue, for printing the
                        glo:account_number = inv:account_number                          !Invoice(s)
                        Add(glo:q_invoice)
                    End!If def:use_sage = 'YES'

                    If access:invoice.insert()
                        access:invoice.cancelautoinc()
                    ELse!If access:invoice.insert()
                        If def:use_sage = 'YES'
                            beep(beep:systemasterisk)  ;  yield()
                            case message('Create Sage Invoice No: '&clip(inv:invoice_number), |
                                    'ServiceBase 2000', icon:asterisk, |
                                     button:ok, button:ok, 0)
                            of button:ok
                            end !case
                        End!If def:use_sage = 'YES'
                    End!If access:invoice.insert()
                End !If count_jobs$ = 0
            end!if access:invoice.primerecord() = level:benign
        End!Loop x$ = 1 To Records(glo:Queue)
    End!If Records(glo:Queue)
    glo:select1 = ''
    glo:select2 = ''
    setcursor()
    Close(Progresswindow)
    If records(glo:q_invoice) 
        If def:use_sage <> 'YES'
            Summary_Invoice
        End!If def:use_sage <> 'YES'
    Else
        If count_exceptions$
            Case MessageEx('There are no new jobs to invoice.<13,10><13,10>'&Clip(count_exceptions$)&' jobs have been excluded from invoicing. Check the Invoice Exceptions list for the reasons.','ServiceBase 2000','Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
                Of 1 ! &OK Button
            End!Case MessageEx
        Else!If count_exception$
            Case MessageEx('There are no new jobs to invoice.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        End!If count_exception$
    End!If records(glo:Queue2)


   Relate:JOBS.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
   Relate:INVOICE.Close
   Relate:CHARTYPE.Close
   Relate:JOBSTAGE.Close
   Relate:STATUS.Close
   Relate:STAHEAD.Close
   Relate:VATCODE.Close
   Relate:DEFAULTS.Close
   Relate:USERS.Close
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
delete_invoice      Routine
    access:invoice.clearkey(inv:invoice_number_key)
    inv:invoice_number  = keyfield#
    If access:invoice.fetch(inv:invoice_number_key) = Level:Benign
        Delete(invoice)
    End
sage_bit        Routine
            failed_temp = 0                                                                 !Check If Failed
            invoice_number_temp = 0
            sage_error# = 0
            If def:use_sage = 'YES'
                glo:file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
                Remove(paramss)
                access:paramss.open()
                access:paramss.usefile()
!LABOUR                                                                                     !Write Labour Part
                get(paramss,0)
                if access:paramss.primerecord() = Level:Benign
                    prm:account_ref       = Stripcomma(sub:Account_number)
                    If tra:invoice_sub_accounts = 'YES'
                        prm:name              = Stripcomma(sub:company_name)
                        prm:address_1         = Stripcomma(sub:address_line1)
                        prm:address_2         = Stripcomma(sub:address_line2)
                        prm:address_3         = Stripcomma(sub:address_line3)
                        prm:address_4         = ''
                        prm:address_5         = Stripcomma(sub:postcode)
                        prm:cust_tel_number   = Stripcomma(sub:telephone_number)
                        prm:contact_name      = Stripcomma(sub:contact_name)
                    Else!If tra:invoice_sub_accounts = 'YES'
                        prm:name              = Stripcomma(tra:company_name)
                        prm:address_1         = Stripcomma(tra:address_line1)
                        prm:address_2         = Stripcomma(tra:address_line2)
                        prm:address_3         = Stripcomma(tra:address_line3)
                        prm:address_4         = ''
                        prm:address_5         = Stripcomma(tra:postcode)
                        prm:cust_tel_number   = Stripcomma(tra:telephone_number)
                        prm:contact_name      = Stripcomma(tra:contact_name)
                    End!If tra:invoice_sub_accounts = 'YES'
                    prm:del_address_1     = ''
                    prm:del_address_2     = ''
                    prm:del_address_3     = ''
                    prm:del_address_4     = ''
                    prm:del_address_5     = ''
                    prm:notes_1           = ''
                    prm:notes_2           = ''
                    prm:notes_3           = ''
                    access:users.clearkey(use:password_key)
                    use:password =glo:password
                    access:users.fetch(use:password_key)
                    prm:taken_by = use:user_code
                    prm:order_number      = ''
                    prm:cust_order_number = ''
                    prm:payment_ref       = ''
                    prm:global_nom_code   = Stripcomma(def:global_nominal_code)
                    prm:global_details    = ''
                    prm:items_net         = Stripcomma(labour_temp + parts_temp + courier_temp)
                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (vat_rate_labour_temp/100),.01) + |
                                                        Round(job:parts_cost * (vat_rate_parts_temp/100),.01) + |
                                                        Round(job:courier_cost * (vat_rate_labour_temp/100),.01))
                    prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
                    prm:description       = Stripcomma(DEF:Labour_Description)
                    prm:nominal_code      = Stripcomma(DEF:Labour_Code)
                    prm:qty_order         = '1'
                    prm:unit_price        = '0'
                    prm:net_amount        = Stripcomma(labour_temp)
                    prm:tax_amount        = Stripcomma(Round(labour_temp * (vat_rate_labour_temp/100),.01))
                    prm:comment_1         = ''
                    prm:comment_2         = ''
                    prm:unit_of_sale      = '0'
                    prm:full_net_amount   = '0'
                    prm:invoice_date      = '*'
                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                    prm:user_name         = Clip(DEF:User_Name_Sage)
                    prm:password          = Clip(DEF:Password_Sage)
                    prm:set_invoice_number= '*'
                    prm:invoice_no        = '*'
                    access:paramss.insert()
                end!if access:paramss.primerecord() = Level:Benign
!PARTS
                get(paramss,0)                                                               !Write Parts Section
                if access:paramss.primerecord() = Level:Benign
                    prm:account_ref       = Stripcomma(sub:Account_number)
                    If tra:invoice_sub_accounts = 'YES'
                        prm:name              = Stripcomma(sub:company_name)
                        prm:address_1         = Stripcomma(sub:address_line1)
                        prm:address_2         = Stripcomma(sub:address_line2)
                        prm:address_3         = Stripcomma(sub:address_line3)
                        prm:address_4         = ''
                        prm:address_5         = Stripcomma(sub:postcode)
                        prm:cust_tel_number   = Stripcomma(sub:telephone_number)
                        prm:contact_name      = Stripcomma(sub:contact_name)
                    Else!If tra:invoice_sub_accounts = 'YES'
                        prm:name              = Stripcomma(tra:company_name)
                        prm:address_1         = Stripcomma(tra:address_line1)
                        prm:address_2         = Stripcomma(tra:address_line2)
                        prm:address_3         = Stripcomma(tra:address_line3)
                        prm:address_4         = ''
                        prm:address_5         = Stripcomma(tra:postcode)
                        prm:cust_tel_number   = Stripcomma(tra:telephone_number)
                        prm:contact_name      = Stripcomma(tra:contact_name)
                    End!If tra:invoice_sub_accounts = 'YES'
                    prm:del_address_1     = ''
                    prm:del_address_2     = ''
                    prm:del_address_3     = ''
                    prm:del_address_4     = ''
                    prm:del_address_5     = ''
                    prm:notes_1           = ''
                    prm:notes_2           = ''
                    prm:notes_3           = ''
                    access:users.clearkey(use:password_key)
                    use:password =glo:password
                    access:users.fetch(use:password_key)
                    prm:taken_by = use:user_code
                    prm:order_number      = ''
                    prm:cust_order_number = ''
                    prm:payment_ref       = ''
                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                    prm:global_details    = ''
                    prm:items_net         = Stripcomma(labour_temp + parts_temp + courier_temp)
                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (vat_rate_labour_temp/100),.01) + |
                                                        Round(job:parts_cost * (vat_rate_parts_temp/100),.01) + |
                                                        Round(job:courier_cost * (vat_rate_labour_temp/100),.01))
                    prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
                    prm:description       = Stripcomma(DEF:Parts_Description)
                    prm:nominal_code      = Stripcomma(DEF:Parts_Code)
                    prm:qty_order         = '1'
                    prm:unit_price        = '0'
                    prm:net_amount        = Stripcomma(parts_temp)
                    prm:tax_amount        = Stripcomma(Round(parts_temp * (vat_rate_parts_temp/100),.01))
                    prm:comment_1         = ''
                    prm:comment_2         = ''
                    prm:unit_of_sale      = '0'
                    prm:full_net_amount   = '0'
                    prm:invoice_date      = '*'
                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                    prm:user_name         = Clip(DEF:User_Name_Sage)
                    prm:password          = Clip(DEF:Password_Sage)
                    prm:set_invoice_number= '*'
                    prm:invoice_no        = '*'
                    access:paramss.insert()
                end!if access:paramss.primerecord() = Level:Benign
!COURIER
                get(paramss,0)                                                               !Write Courier Part
                if access:paramss.primerecord() = Level:Benign
                    prm:account_ref       = Stripcomma(sub:Account_number)
                    If tra:invoice_sub_accounts = 'YES'
                        prm:name              = Stripcomma(sub:company_name)
                        prm:address_1         = Stripcomma(sub:address_line1)
                        prm:address_2         = Stripcomma(sub:address_line2)
                        prm:address_3         = Stripcomma(sub:address_line3)
                        prm:address_4         = ''
                        prm:address_5         = Stripcomma(sub:postcode)
                        prm:cust_tel_number   = Stripcomma(sub:telephone_number)
                        prm:contact_name      = Stripcomma(sub:contact_name)
                    Else!If tra:invoice_sub_accounts = 'YES'
                        prm:name              = Stripcomma(tra:company_name)
                        prm:address_1         = Stripcomma(tra:address_line1)
                        prm:address_2         = Stripcomma(tra:address_line2)
                        prm:address_3         = Stripcomma(tra:address_line3)
                        prm:address_4         = ''
                        prm:address_5         = Stripcomma(tra:postcode)
                        prm:cust_tel_number   = Stripcomma(tra:telephone_number)
                        prm:contact_name      = Stripcomma(tra:contact_name)
                    End!If tra:invoice_sub_accounts = 'YES'
                    prm:del_address_1     = ''
                    prm:del_address_2     = ''
                    prm:del_address_3     = ''
                    prm:del_address_4     = ''
                    prm:del_address_5     = ''
                    prm:notes_1           = ''
                    prm:notes_2           = ''
                    prm:notes_3           = ''
                    access:users.clearkey(use:password_key)
                    use:password =glo:password
                    access:users.fetch(use:password_key)
                    prm:taken_by = use:user_code
                    prm:order_number      = ''
                    prm:cust_order_number = ''
                    prm:payment_ref       = ''
                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                    prm:global_details    = ''
                    prm:items_net         = Stripcomma(labour_temp + parts_temp + courier_temp)
                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (vat_rate_labour_temp/100),.01) + |
                                                        Round(job:parts_cost * (vat_rate_parts_temp/100),.01) + |
                                                        Round(job:courier_cost * (vat_rate_labour_temp/100),.01))
                    prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
                    prm:description       = Stripcomma(DEF:Parts_Description)
                    prm:nominal_code      = Stripcomma(DEF:Parts_Code)
                    prm:qty_order         = '1'
                    prm:unit_price        = '0'
                    prm:net_amount        = Stripcomma(job:courier_cost)
                    prm:tax_amount        = Stripcomma(Round(job:courier_cost * (vat_rate_labour_temp/100),.01))
                    prm:comment_1         = ''
                    prm:comment_2         = ''
                    prm:unit_of_sale      = '0'
                    prm:full_net_amount   = '0'
                    prm:invoice_date      = '*'
                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                    prm:user_name         = Clip(DEF:User_Name_Sage)
                    prm:password          = Clip(DEF:Password_Sage)
                    prm:set_invoice_number= '*'
                    prm:invoice_no        = '*'
                    access:paramss.insert()
                end!if access:paramss.primerecord() = Level:Benign
                access:paramss.close()
                Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)                                !Run Sage Prog to get invoice no
                sage_error# = 0
                access:paramss.open()
                access:paramss.usefile()
                Set(paramss,0)
                If access:paramss.next()
                    sage_error# = 1
                    failed_temp = 1
                Else!If access:paramss.next()
                    If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
                        sage_error# = 1
                        failed_temp = 1
                    Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
                        invoice_number_temp = prm:invoice_no
                        If invoice_number_temp = 0
                            failed_temp = 1
                        End!If invoice_number_temp = 0
                    End!If prm:invoice_no = '-1'
                End!If access:paramss.next()
                access:paramss.close()
            End!If def:use_sage = 'YES'


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Create_Chargeable_Invoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('pos',pos,'Create_Chargeable_Invoice',1)
    SolaceViewVars('invoice_number_temp',invoice_number_temp,'Create_Chargeable_Invoice',1)
    SolaceViewVars('total_temp',total_temp,'Create_Chargeable_Invoice',1)
    SolaceViewVars('labour_temp',labour_temp,'Create_Chargeable_Invoice',1)
    SolaceViewVars('parts_temp',parts_temp,'Create_Chargeable_Invoice',1)
    SolaceViewVars('courier_temp',courier_temp,'Create_Chargeable_Invoice',1)
    SolaceViewVars('failed_temp',failed_temp,'Create_Chargeable_Invoice',1)
    SolaceViewVars('VAT_Rate_Labour_Temp',VAT_Rate_Labour_Temp,'Create_Chargeable_Invoice',1)
    SolaceViewVars('VAT_Rate_Parts_Temp',VAT_Rate_Parts_Temp,'Create_Chargeable_Invoice',1)
    SolaceViewVars('Account_Number_Temp',Account_Number_Temp,'Create_Chargeable_Invoice',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
