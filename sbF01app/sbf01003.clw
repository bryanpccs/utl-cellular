

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01003.INC'),ONCE        !Local module procedure declarations
                     END


EPS_Repair_Activity_Criteria PROCEDURE                !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Model_Number_Pointer          LIKE(GLO:Model_Number_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::14:TAGFLAG         BYTE(0)
DASBRW::14:TAGMOUSE        BYTE(0)
DASBRW::14:TAGDISPSTATUS   BYTE(0)
DASBRW::14:QUEUE          QUEUE
Pointer2                      LIKE(GLO:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::16:TAGFLAG         BYTE(0)
DASBRW::16:TAGMOUSE        BYTE(0)
DASBRW::16:TAGDISPSTATUS   BYTE(0)
DASBRW::16:QUEUE          QUEUE
Pointer3                      LIKE(GLO:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
FilesOpened          BYTE
part_count_temp      REAL
job_count_temp       REAL
Total_Queue          QUEUE,PRE(tmp)
NFF_Total            REAL
BER_Total            REAL
Labour_Total         REAL
Parts_Total          REAL
all_sources_total    REAL
exchange_only_total  REAL
                     END
all_sources_temp     REAL
exchange_only_temp   REAL
savepath             STRING(255)
TabNumber            BYTE(1)
model_number_temp    STRING('''ALL''')
trade_account_temp   STRING(3)
model_tag            STRING(1)
manufacturer_temp    STRING(30)
trade_tag            STRING(1)
charge_type_tag      STRING(1)
charge_type_tag2     STRING(1)
model_tag2           STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select2
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
FDCB7::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:RecordNumber)
                     END
BRW4::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
model_tag              LIKE(model_tag)                !List box control field - type derived from local data
model_tag_Icon         LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW3::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
trade_tag              LIKE(trade_tag)                !List box control field - type derived from local data
trade_tag_Icon         LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW13::View:Browse   VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
charge_type_tag        LIKE(charge_type_tag)          !List box control field - type derived from local data
charge_type_tag_Icon   LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW15::View:Browse   VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
charge_type_tag2       LIKE(charge_type_tag2)         !List box control field - type derived from local data
charge_type_tag2_Icon  LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('Repair Activity Criteria'),AT(,,310,316),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,304,280),USE(?Sheet1),BELOW,WIZARD,SPREAD
                         TAB('Tab1'),USE(?Tab1)
                           PROMPT('Manufacturer Details'),AT(8,36),USE(?Prompt3),FONT(,12,,FONT:bold)
                           PROMPT('Manufacturer'),AT(8,60),USE(?Prompt1)
                           COMBO(@s30),AT(84,60,124,10),USE(GLO:Select1),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           LIST,AT(84,80,152,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('10LI@s1@120L(2)~Model Number~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Tag'),AT(84,228,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All'),AT(136,228,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           PROMPT('Model Numbers'),AT(8,80),USE(?Prompt4)
                           BUTTON('&Untag All'),AT(188,228,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(12,108,32,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(12,124,32,13),USE(?DASSHOWTAG),HIDE
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Trade Account Details'),AT(8,36),USE(?Prompt5),FONT(,12,,FONT:bold)
                           LIST,AT(84,80,152,144),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('10LI@s1@60L(2)~Account Number~@s15@'),FROM(Queue:Browse:1)
                           BUTTON('&Tag'),AT(84,228,48,16),USE(?DASTAG:2),LEFT,ICON('tag.gif')
                           BUTTON('Tag All'),AT(136,228,48,16),USE(?DASTAGAll:2),LEFT,ICON('tagall.gif')
                           BUTTON('Untag All'),AT(188,228,48,16),USE(?DASUNTAGALL:2),LEFT,ICON('untag.gif')
                           PROMPT('Exchange Account'),AT(8,60),USE(?Prompt7)
                           BUTTON('&Rev tags'),AT(44,112,32,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(44,128,32,13),USE(?DASSHOWTAG:2),HIDE
                           COMBO(@s40),AT(84,60,124,10),USE(GLO:Select2),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('60L(2)@s15@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           PROMPT('All Sources'),AT(8,80),USE(?Prompt6)
                         END
                         TAB('Tab 3'),USE(?Tab3)
                           PROMPT('No Fault Found Charge Types'),AT(8,36),USE(?Prompt8),FONT(,12,,FONT:bold)
                           PROMPT('NFF Charge Types'),AT(8,60),USE(?Prompt9)
                           LIST,AT(84,60,152,144),USE(?List:3),IMM,VSCROLL,FONT('Tahoma',8,,,CHARSET:ANSI),MSG('Browsing Records'),FORMAT('10LI@s1@120L(2)~Charge Type~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Tag'),AT(84,208,48,16),USE(?DASTAG:3),LEFT,ICON('tag.gif')
                           BUTTON('Tag All'),AT(136,208,48,16),USE(?DASTAGAll:3),LEFT,ICON('tagall.gif')
                           BUTTON('Untag All'),AT(188,208,48,16),USE(?DASUNTAGALL:3),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(40,96,32,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(40,112,32,13),USE(?DASSHOWTAG:3),HIDE
                         END
                         TAB('Tab 4'),USE(?Tab4)
                           PROMPT('Beyond Economic Repair Charge Types'),AT(8,36),USE(?Prompt10),FONT(,12,,FONT:bold)
                           PROMPT('BER Charge Types'),AT(8,60),USE(?Prompt11)
                           LIST,AT(84,60,152,144),USE(?List:4),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('10LI@s1@120L(2)~Charge Type~@s30@'),FROM(Queue:Browse:3)
                           BUTTON('&Tag'),AT(84,208,48,16),USE(?DASTAG:4),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All'),AT(136,208,48,16),USE(?DASTAGAll:4),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(188,208,48,16),USE(?DASUNTAGALL:4),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(44,92,32,13),USE(?DASREVTAG:4),HIDE
                           BUTTON('sho&W tags'),AT(44,108,32,13),USE(?DASSHOWTAG:4),HIDE
                         END
                         TAB('Tab 5'),USE(?Tab5)
                           PROMPT('Miscellaneous'),AT(8,36),USE(?Prompt16),FONT(,12,,FONT:bold)
                           GROUP('Hit Rate Part Number'),AT(12,56,288,36),USE(?Group1),BOXED
                             PROMPT('Part Number'),AT(48,72),USE(?glo:select4:Prompt)
                             ENTRY(@s40),AT(124,72,124,10),USE(GLO:Select4),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             BUTTON,AT(252,72,10,10),USE(?Button32),ICON('list3.ico')
                           END
                           GROUP('Administration And Carriage Charge'),AT(12,100,288,36),USE(?Group1:2),BOXED
                             PROMPT('Charge'),AT(48,116),USE(?glo:select3:Prompt)
                             ENTRY(@n14.2b),AT(124,116,64,10),USE(GLO:Select3),FONT(,,,FONT:bold)
                           END
                         END
                         TAB('Tab 6'),USE(?Tab6)
                           PROMPT('Date Range'),AT(8,36),USE(?Prompt13),FONT(,12,,FONT:bold)
                           PROMPT('Start Date'),AT(79,76),USE(?glo:select11:Prompt)
                           ENTRY(@d6b),AT(153,76,64,10),USE(GLO:Select11),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(221,76,10,10),USE(?PopCalendar),ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(79,96),USE(?glo:select12:Prompt)
                           ENTRY(@d6b),AT(153,96,64,10),USE(GLO:Select12),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(221,96,10,10),USE(?PopCalendar:2),ICON('Calenda2.ico')
                         END
                       END
                       PROMPT('Welcome To The Repair Activity Report Wizard'),AT(8,8),USE(?Prompt2),FONT(,14,COLOR:Navy,FONT:bold)
                       PANEL,AT(4,288,304,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,292,56,16),USE(?Back_Button),LEFT,ICON('back.gif')
                       BUTTON('Next'),AT(64,292,56,16),USE(?Next_Button),LEFT,ICON('next.gif')
                       BUTTON('&Finish'),AT(120,292,56,16),USE(?Finish_Button),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(248,292,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW3                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW13                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW13::Sort0:Locator StepLocatorClass                 !Default Locator
BRW15                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW15::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_job_id   ushort,auto
save_uni_id   ushort,auto
save_wpr_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?GLO:Select1{prop:ReadOnly} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 15066597
    Elsif ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 8454143
    Else ! If ?GLO:Select1{prop:Req} = True
        ?GLO:Select1{prop:FontColor} = 65793
        ?GLO:Select1{prop:Color} = 16777215
    End ! If ?GLO:Select1{prop:Req} = True
    ?GLO:Select1{prop:Trn} = 0
    ?GLO:Select1{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?GLO:Select2{prop:ReadOnly} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 15066597
    Elsif ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 8454143
    Else ! If ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 16777215
    End ! If ?GLO:Select2{prop:Req} = True
    ?GLO:Select2{prop:Trn} = 0
    ?GLO:Select2{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Tab4{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?List:4{prop:FontColor} = 65793
    ?List:4{prop:Color}= 16777215
    ?List:4{prop:Color,2} = 16777215
    ?List:4{prop:Color,3} = 12937777
    ?Tab5{prop:Color} = 15066597
    ?Prompt16{prop:FontColor} = -1
    ?Prompt16{prop:Color} = 15066597
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?glo:select4:Prompt{prop:FontColor} = -1
    ?glo:select4:Prompt{prop:Color} = 15066597
    If ?GLO:Select4{prop:ReadOnly} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 15066597
    Elsif ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 8454143
    Else ! If ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 16777215
    End ! If ?GLO:Select4{prop:Req} = True
    ?GLO:Select4{prop:Trn} = 0
    ?GLO:Select4{prop:FontStyle} = font:Bold
    ?Group1:2{prop:Font,3} = -1
    ?Group1:2{prop:Color} = 15066597
    ?Group1:2{prop:Trn} = 0
    ?glo:select3:Prompt{prop:FontColor} = -1
    ?glo:select3:Prompt{prop:Color} = 15066597
    If ?GLO:Select3{prop:ReadOnly} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 15066597
    Elsif ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 8454143
    Else ! If ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 16777215
    End ! If ?GLO:Select3{prop:Req} = True
    ?GLO:Select3{prop:Trn} = 0
    ?GLO:Select3{prop:FontStyle} = font:Bold
    ?Tab6{prop:Color} = 15066597
    ?Prompt13{prop:FontColor} = -1
    ?Prompt13{prop:Color} = 15066597
    ?glo:select11:Prompt{prop:FontColor} = -1
    ?glo:select11:Prompt{prop:Color} = 15066597
    If ?GLO:Select11{prop:ReadOnly} = True
        ?GLO:Select11{prop:FontColor} = 65793
        ?GLO:Select11{prop:Color} = 15066597
    Elsif ?GLO:Select11{prop:Req} = True
        ?GLO:Select11{prop:FontColor} = 65793
        ?GLO:Select11{prop:Color} = 8454143
    Else ! If ?GLO:Select11{prop:Req} = True
        ?GLO:Select11{prop:FontColor} = 65793
        ?GLO:Select11{prop:Color} = 16777215
    End ! If ?GLO:Select11{prop:Req} = True
    ?GLO:Select11{prop:Trn} = 0
    ?GLO:Select11{prop:FontStyle} = font:Bold
    ?glo:select12:Prompt{prop:FontColor} = -1
    ?glo:select12:Prompt{prop:Color} = 15066597
    If ?GLO:Select12{prop:ReadOnly} = True
        ?GLO:Select12{prop:FontColor} = 65793
        ?GLO:Select12{prop:Color} = 15066597
    Elsif ?GLO:Select12{prop:Req} = True
        ?GLO:Select12{prop:FontColor} = 65793
        ?GLO:Select12{prop:Color} = 8454143
    Else ! If ?GLO:Select12{prop:Req} = True
        ?GLO:Select12{prop:FontColor} = 65793
        ?GLO:Select12{prop:Color} = 16777215
    End ! If ?GLO:Select12{prop:Req} = True
    ?GLO:Select12{prop:Trn} = 0
    ?GLO:Select12{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
   GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
  IF ERRORCODE()
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    model_tag = '*'
  ELSE
    DELETE(GLO:Q_ModelNumber)
    model_tag = ''
  END
    Queue:Browse.model_tag = model_tag
  IF (model_tag = '*')
    Queue:Browse.model_tag_Icon = 2
  ELSE
    Queue:Browse.model_tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(GLO:Q_ModelNumber)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Q_ModelNumber)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Q_ModelNumber)
    GET(GLO:Q_ModelNumber,QR#)
    DASBRW::6:QUEUE = GLO:Q_ModelNumber
    ADD(DASBRW::6:QUEUE)
  END
  FREE(GLO:Q_ModelNumber)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Model_Number_Pointer = mod:Model_Number
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Model_Number_Pointer)
    IF ERRORCODE()
       GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
       ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW3.UpdateBuffer
   GLO:Queue.Pointer = sub:Account_Number
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = sub:Account_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
    trade_tag = '*'
  ELSE
    DELETE(GLO:Queue)
    trade_tag = ''
  END
    Queue:Browse:1.trade_tag = trade_tag
  IF (trade_tag = '*')
    Queue:Browse:1.trade_tag_Icon = 2
  ELSE
    Queue:Browse:1.trade_tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = sub:Account_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::8:QUEUE = GLO:Queue
    ADD(DASBRW::8:QUEUE)
  END
  FREE(GLO:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = sub:Account_Number
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::14:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW13.UpdateBuffer
   GLO:Queue2.Pointer2 = cha:Charge_Type
   GET(GLO:Queue2,GLO:Queue2.Pointer2)
  IF ERRORCODE()
     GLO:Queue2.Pointer2 = cha:Charge_Type
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    charge_type_tag = '*'
  ELSE
    DELETE(GLO:Queue2)
    charge_type_tag = ''
  END
    Queue:Browse:2.charge_type_tag = charge_type_tag
  IF (charge_type_tag = '*')
    Queue:Browse:2.charge_type_tag_Icon = 2
  ELSE
    Queue:Browse:2.charge_type_tag_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::14:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW13.Reset
  FREE(GLO:Queue2)
  LOOP
    NEXT(BRW13::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue2.Pointer2 = cha:Charge_Type
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
  END
  SETCURSOR
  BRW13.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::14:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue2)
  BRW13.Reset
  SETCURSOR
  BRW13.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::14:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::14:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue2)
    GET(GLO:Queue2,QR#)
    DASBRW::14:QUEUE = GLO:Queue2
    ADD(DASBRW::14:QUEUE)
  END
  FREE(GLO:Queue2)
  BRW13.Reset
  LOOP
    NEXT(BRW13::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::14:QUEUE.Pointer2 = cha:Charge_Type
     GET(DASBRW::14:QUEUE,DASBRW::14:QUEUE.Pointer2)
    IF ERRORCODE()
       GLO:Queue2.Pointer2 = cha:Charge_Type
       ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW13.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::14:DASSHOWTAG Routine
   CASE DASBRW::14:TAGDISPSTATUS
   OF 0
      DASBRW::14:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::14:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::14:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW13.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::16:DASTAGONOFF Routine
  GET(Queue:Browse:3,CHOICE(?List:4))
  BRW15.UpdateBuffer
   GLO:Queue3.Pointer3 = cha:Charge_Type
   GET(GLO:Queue3,GLO:Queue3.Pointer3)
  IF ERRORCODE()
     GLO:Queue3.Pointer3 = cha:Charge_Type
     ADD(GLO:Queue3,GLO:Queue3.Pointer3)
    charge_type_tag2 = '*'
  ELSE
    DELETE(GLO:Queue3)
    charge_type_tag2 = ''
  END
    Queue:Browse:3.charge_type_tag2 = charge_type_tag2
  IF (charge_type_tag2 = '*')
    Queue:Browse:3.charge_type_tag2_Icon = 2
  ELSE
    Queue:Browse:3.charge_type_tag2_Icon = 1
  END
  PUT(Queue:Browse:3)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::16:DASTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW15.Reset
  FREE(GLO:Queue3)
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue3.Pointer3 = cha:Charge_Type
     ADD(GLO:Queue3,GLO:Queue3.Pointer3)
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::16:DASUNTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue3)
  BRW15.Reset
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::16:DASREVTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::16:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue3)
    GET(GLO:Queue3,QR#)
    DASBRW::16:QUEUE = GLO:Queue3
    ADD(DASBRW::16:QUEUE)
  END
  FREE(GLO:Queue3)
  BRW15.Reset
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::16:QUEUE.Pointer3 = cha:Charge_Type
     GET(DASBRW::16:QUEUE,DASBRW::16:QUEUE.Pointer3)
    IF ERRORCODE()
       GLO:Queue3.Pointer3 = cha:Charge_Type
       ADD(GLO:Queue3,GLO:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::16:DASSHOWTAG Routine
   CASE DASBRW::16:TAGDISPSTATUS
   OF 0
      DASBRW::16:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::16:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::16:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:4{PROP:Text} = 'Show All'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:4{PROP:Text})
   BRW15.ResetSort(1)
   SELECT(?List:4,CHOICE(?List:4))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'EPS_Repair_Activity_Criteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('part_count_temp',part_count_temp,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('job_count_temp',job_count_temp,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('Total_Queue:NFF_Total',Total_Queue:NFF_Total,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('Total_Queue:BER_Total',Total_Queue:BER_Total,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('Total_Queue:Labour_Total',Total_Queue:Labour_Total,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('Total_Queue:Parts_Total',Total_Queue:Parts_Total,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('Total_Queue:all_sources_total',Total_Queue:all_sources_total,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('Total_Queue:exchange_only_total',Total_Queue:exchange_only_total,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('all_sources_temp',all_sources_temp,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('exchange_only_temp',exchange_only_temp,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('savepath',savepath,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('TabNumber',TabNumber,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('model_number_temp',model_number_temp,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('trade_account_temp',trade_account_temp,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('model_tag',model_tag,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('manufacturer_temp',manufacturer_temp,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('trade_tag',trade_tag,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('charge_type_tag',charge_type_tag,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('charge_type_tag2',charge_type_tag2,'EPS_Repair_Activity_Criteria',1)
    SolaceViewVars('model_tag2',model_tag2,'EPS_Repair_Activity_Criteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:2;  SolaceCtrlName = '?DASTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:2;  SolaceCtrlName = '?DASTAGAll:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:2;  SolaceCtrlName = '?DASUNTAGALL:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:2;  SolaceCtrlName = '?DASREVTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:2;  SolaceCtrlName = '?DASSHOWTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:3;  SolaceCtrlName = '?DASTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:3;  SolaceCtrlName = '?DASTAGAll:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:3;  SolaceCtrlName = '?DASUNTAGALL:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:3;  SolaceCtrlName = '?DASREVTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:3;  SolaceCtrlName = '?DASSHOWTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:4;  SolaceCtrlName = '?List:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:4;  SolaceCtrlName = '?DASTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:4;  SolaceCtrlName = '?DASTAGAll:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:4;  SolaceCtrlName = '?DASUNTAGALL:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:4;  SolaceCtrlName = '?DASREVTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:4;  SolaceCtrlName = '?DASSHOWTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16;  SolaceCtrlName = '?Prompt16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select4:Prompt;  SolaceCtrlName = '?glo:select4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select4;  SolaceCtrlName = '?GLO:Select4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button32;  SolaceCtrlName = '?Button32';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1:2;  SolaceCtrlName = '?Group1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select3:Prompt;  SolaceCtrlName = '?glo:select3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt13;  SolaceCtrlName = '?Prompt13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select11:Prompt;  SolaceCtrlName = '?glo:select11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select11;  SolaceCtrlName = '?GLO:Select11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select12:Prompt;  SolaceCtrlName = '?glo:select12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select12;  SolaceCtrlName = '?GLO:Select12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Back_Button;  SolaceCtrlName = '?Back_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Next_Button;  SolaceCtrlName = '?Next_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish_Button;  SolaceCtrlName = '?Finish_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('EPS_Repair_Activity_Criteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'EPS_Repair_Activity_Criteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:MODELNUM,SELF)
  BRW3.Init(?List:2,Queue:Browse:1.ViewPosition,BRW3::View:Browse,Queue:Browse:1,Relate:SUBTRACC,SELF)
  BRW13.Init(?List:3,Queue:Browse:2.ViewPosition,BRW13::View:Browse,Queue:Browse:2,Relate:CHARTYPE,SELF)
  BRW15.Init(?List:4,Queue:Browse:3.ViewPosition,BRW15::View:Browse,Queue:Browse:3,Relate:CHARTYPE,SELF)
  OPEN(Window)
  SELF.Opened=True
  Hide(?Finish_Button)
  Clear(glo:G_Select1)
  Select(?Sheet1,TabNumber)
  Disable(?Back_Button)
  glo:select11 = Deformat('1/1/1990',@d6)
  glo:select12 = Today()
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?glo:select11{Prop:Alrt,255} = MouseLeft2
  ?glo:select12{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,mod:Manufacturer_Key)
  BRW4.AddRange(mod:Manufacturer,GLO:Select1)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,mod:Model_Number,1,BRW4)
  BIND('model_tag',model_tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(model_tag,BRW4.Q.model_tag)
  BRW4.AddField(mod:Model_Number,BRW4.Q.mod:Model_Number)
  BRW4.AddField(mod:Manufacturer,BRW4.Q.mod:Manufacturer)
  BRW3.Q &= Queue:Browse:1
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,sub:Account_Number_Key)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,sub:Account_Number,1,BRW3)
  BIND('trade_tag',trade_tag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(trade_tag,BRW3.Q.trade_tag)
  BRW3.AddField(sub:Account_Number,BRW3.Q.sub:Account_Number)
  BRW3.AddField(sub:RecordNumber,BRW3.Q.sub:RecordNumber)
  BRW13.Q &= Queue:Browse:2
  BRW13.RetainRow = 0
  BRW13.AddSortOrder(,cha:Warranty_Key)
  BRW13.AddLocator(BRW13::Sort0:Locator)
  BRW13::Sort0:Locator.Init(,cha:Warranty,1,BRW13)
  BRW13.SetFilter('(Upper(CHA:Warranty) = ''YES'')')
  BIND('charge_type_tag',charge_type_tag)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW13.AddField(charge_type_tag,BRW13.Q.charge_type_tag)
  BRW13.AddField(cha:Charge_Type,BRW13.Q.cha:Charge_Type)
  BRW13.AddField(cha:Warranty,BRW13.Q.cha:Warranty)
  BRW15.Q &= Queue:Browse:3
  BRW15.RetainRow = 0
  BRW15.AddSortOrder(,cha:Warranty_Key)
  BRW15.AddLocator(BRW15::Sort0:Locator)
  BRW15::Sort0:Locator.Init(,cha:Warranty,1,BRW15)
  BRW15.SetFilter('(Upper(CHA:Warranty) = ''YES'')')
  BIND('charge_type_tag2',charge_type_tag2)
  ?List:4{PROP:IconList,1} = '~notick1.ico'
  ?List:4{PROP:IconList,2} = '~tick1.ico'
  BRW15.AddField(charge_type_tag2,BRW15.Q.charge_type_tag2)
  BRW15.AddField(cha:Charge_Type,BRW15.Q.cha:Charge_Type)
  BRW15.AddField(cha:Warranty,BRW15.Q.cha:Warranty)
  FDCB2.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(man:Manufacturer_Key)
  FDCB2.AddField(man:Manufacturer,FDCB2.Q.man:Manufacturer)
  FDCB2.AddField(man:RecordNumber,FDCB2.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB7.Init(GLO:Select2,?GLO:Select2,Queue:FileDropCombo:1.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:1,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:1
  FDCB7.AddSortOrder(sub:Account_Number_Key)
  FDCB7.AddField(sub:Account_Number,FDCB7.Q.sub:Account_Number)
  FDCB7.AddField(sub:RecordNumber,FDCB7.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  BRW4.AddToolbarTarget(Toolbar)
  BRW3.AddToolbarTarget(Toolbar)
  BRW13.AddToolbarTarget(Toolbar)
  BRW15.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Q_ModelNumber)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue3)
  ?DASSHOWTAG:4{PROP:Text} = 'Show All'
  ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  Clear(glo:G_Select1)
  Clear(glo:G_Select1)
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'EPS_Repair_Activity_Criteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?GLO:Select1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
      BRW4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button32
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Manufacturer_Stock
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button32, Accepted)
      glo:select4  = sto:part_number
      Display(?glo:select4)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button32, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select11 = TINCALENDARStyle1(GLO:Select11)
          Display(?glo:select11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select12 = TINCALENDARStyle1(GLO:Select12)
          Display(?glo:select12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Back_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Back_Button, Accepted)
      Hide(?Finish_Button)
      Enable(?Back_Button)
      Enable(?Next_Button)
      !Case TabNumber
      !    Of 5
      !        TabNumber = 1
      !        Disable(?Back_Button)
      !    Of 4
      !        If same_model_temp = True
      !            TabNumber = 3
      !        Else
      !            TabNumber = 1
      !            Disable(?Back_Button)
      !        End
      !
      !    Of 3
      !        If use_fault_codes_temp = True
      !            Tabnumber = 2
      !        else
      !            Tabnumber = 1
      !            Disable(?Back_Button)
      !        End
      !    Of 1
      !        Disable(?Back_Button)
      !    Else
      !        TabNumber -= 1
      !
      !End
      TabNumber -=1
      Case TabNumber
          Of 1
              Disable(?Back_Button)
      End!Case TabNumber
      Select(?Sheet1,TabNumber)
      BRW3.ResetSort(1)
      BRW4.ResetSort(1)
      BRW13.ResetSort(1)
      BRW15.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Back_Button, Accepted)
    OF ?Next_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Next_Button, Accepted)
      Hide(?Finish_Button)
      Enable(?Back_Button)
      Enable(?Next_Button)
      !Case TabNumber
      !    Of 5
      !        TabNumber = 1
      !        Disable(?Back_Button)
      !    Of 4
      !        If same_model_temp = True
      !            TabNumber = 3
      !        Else
      !            TabNumber = 1
      !            Disable(?Back_Button)
      !        End
      !
      !    Of 3
      !        If use_fault_codes_temp = True
      !            Tabnumber = 2
      !        else
      !            Tabnumber = 1
      !            Disable(?Back_Button)
      !        End
      !    Of 1
      !        Disable(?Back_Button)
      !    Else
      !        TabNumber -= 1
      !
      !End
      TabNumber +=1
      Case TabNumber
          Of 6
              Disable(?Next_Button)
              Unhide(?Finish_Button)
      End!Case TabNumber
      Select(?Sheet1,TabNumber)
      BRW3.ResetSort(1)
      BRW4.ResetSort(1)
      BRW13.ResetSort(1)
      BRW15.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Next_Button, Accepted)
    OF ?Finish_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish_Button, Accepted)
      beep(beep:systemquestion)  ;  yield()
      case message('Run Procedure?', |
              'ServiceBase 2000', icon:question, |
               button:yes+button:no, button:no, 0)
      of button:yes
      
          savepath = path()
          set(defaults)
          access:defaults.next()
          If def:exportpath <> ''
              glo:file_name = Clip(def:exportpath) & '\REPACT.CSV'
          Else!If def:exportpath <> ''
              glo:file_name = 'C:\REPACT.CSV'
          End!If def:exportpath <> ''
      
          if not filedialog('Save File As',glo:file_name,'CSV Files|*.csv|Text Files|*.txt|All Files|*.*',1)
              setpath(savepath)
          else!if not filedialog('Save File As',file_name,'CSV Files|*.csv|Text Files|*.txt|All Files|*.*',1)
              setpath(savepath)
              Remove(xrepact)
              If access:xrepact.open() = Level:Benign
                  access:xrepact.usefile()
                  recordspercycle     = 25
                  recordsprocessed    = 0
                  percentprogress     = 0
                  setcursor(cursor:wait)
                  open(progresswindow)
                  progress:thermometer    = 0
                  ?progress:pcttext{prop:text} = '0% Completed'
      
                  recordstoprocess    = Records(glo:Q_ModelNumber)
      
                  Clear(xre:record)
                  xre:field1 = 'Repair Activity'
                  xre:field2 = 'Date Range: '
                  xre:field3 = Format(glo:select11,@d6b) & ' - ' & Format(glo:select12,@d6b)
                  access:xrepact.insert()
      
                  Clear(xre:record)
                  xre:field1  = 'Equipment Type'
                  xre:field2  = 'All Sources Qty'
                  xre:field3  = 'Exchange Only Qty'
                  access:xrepact.insert()
      
                  Sort(glo:Q_ModelNumber,glo:model_number_pointer) !Model Number
                  Loop x# = 1 To Records(glo:Q_ModelNumber)
                      Get(glo:Q_ModelNumber,x#)
                      Do GetNextRecord2
                      save_uni_id = access:unittype.savefile() !Unit Types
                      access:unittype.clearkey(uni:unit_type_key)
                      set(uni:unit_type_key)
                      loop
                          if access:unittype.next()
                             break
                          end !if
      
                          Sort(glo:Queue,glo:pointer)
                          Loop y# = 1 To Records(glo:Queue) !Trade Customers
                              Get(glo:Queue,y#)
      
                              All_Sources_Temp = 0
                              Exchange_Only_Temp = 0
                              found# = 0
                              part_count_temp = 0
                              job_count_temp = 0
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:date_booked_key)
                              job:date_booked = glo:select11
                              set(job:date_booked_key,job:date_booked_key)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:date_booked > glo:select12      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  If job:model_number <> glo:model_number_pointer
                                      Cycle
                                  End!If job:model_number <> glo:model_number_pointer
                                  If job:unit_type <> uni:unit_Type
                                      Cycle
                                  End!If job:unit_type <> uni:unit_Type
                                  IF job:account_number = glo:select2
                                      exchange_only_temp += 1
                                      found# = 1
                                  End!IF job:account_number = glo:select2
                                  If job:account_number = glo:pointer
                                      all_sources_temp += 1
                                      found# = 1
                                  End!If job:account_number = glo:pointer1
                                  Sort(glo:Queue2,glo:pointer2)
                                  glo:pointer2 = job:warranty_charge_type
                                  Get(glo:Queue2,glo:pointer2)
                                  If ~Error()
                                      tmp:nff_total += 1
                                  End
                                  Sort(glo:Queue3,glo:pointer3)
                                  glo:pointer3 = job:warranty_charge_type
                                  Get(glo:Queue3,glo:pointer3)
                                  If ~Error()
                                      tmp:ber_total += 1
                                  End
      !Hit Rate Count
                                  job_count_temp += 1
                                  save_wpr_id = access:warparts.savefile()
                                  access:warparts.clearkey(wpr:part_number_key)
                                  wpr:ref_number  = job:ref_number
                                  set(wpr:part_number_key,wpr:part_number_key)
                                  loop
                                      if access:warparts.next()
                                         break
                                      end !if
                                      if wpr:ref_number  <> job:ref_number      |
                                          then break.  ! end if
                                      part_count_temp += 1
                                  end !loop
                                  access:warparts.restorefile(save_wpr_id)
      
                              end !loop
                              access:jobs.restorefile(save_job_id)
      
                          End!Loop y# = 1 To Records(glo:Queue1)
                          If found# = 1
                              Clear(xre:record)
                              xre:field1  = Clip(glo:model_number_pointer) & ' ' & Clip(uni:unit_type)
                              xre:field2  = Format(all_sources_temp,@n12b)
                              xre:field3  = Format(exchange_only_temp,@n12b)
                              access:xrepact.insert()
                              tmp:all_sources_total += all_sources_temp
                              tmp:exchange_only_total += exchange_only_temp
                          End!If found# = 1
                      end !loop
                      access:unittype.restorefile(save_uni_id)
      
                  End!Loop x# = 1 To Records(glo:Q_ModelNumber)
                  setcursor()
                  close(progresswindow)
                  Clear(xre:record)
                  xre:field1  = 'Totals'
                  xre:field2  = tmp:all_sources_total
                  xre:field3  = tmp:exchange_only_total
                  access:xrepact.insert()
                  Clear(xre:record)
                  xre:field1  = 'Average Cycle Time'
                  xre:field3  = '90 Day Bounce Rate'
                  access:xrepact.insert()
                  Clear(xre:record)
                  xre:field1  = 'Exchange NFF'
                  xre:field2  = tmp:nff_total
                  xre:field3  = 'Average Labour'
                  access:xrepact.insert()
                  Clear(xre:record)
                  xre:field1  = 'Exchange BER'
                  xre:field2  = tmp:ber_total
                  xre:field3  = 'Average Parts Cost'
                  access:xrepact.insert()
                  Clear(xre:record)
                  xre:field1  = 'Hit Rate'
                  xre:field2  = Format(((part_count_temp * 100)/job_count_temp),@n14.2) & '%'
                  xre:field3  = 'Admin & Carriage'
                  xre:field4  = Format(glo:select3,@n14.2)
                  access:xrepact.insert()
      
              End!If access:xrepact.open() = Level:Benign
              access:xrepact.close()
              beep(beep:systemhand)  ;  yield()
              message('Routine Completed.', |
                      'ServiceBase 2000', icon:hand)
          end!if not filedialog('Save File As',file_name,'CSV Files|*.csv|Text Files|*.txt|All Files|*.*',1)
      of button:no
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish_Button, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'EPS_Repair_Activity_Criteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?GLO:Select11
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select12
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::14:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:3)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:4
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:4{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:4{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::16:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:4)
               ?List:4{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      DO DASBRW::8:DASUNTAGALL
      DO DASBRW::14:DASUNTAGALL
      DO DASBRW::16:DASUNTAGALL
      DO DASBRW::6:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    IF ERRORCODE()
      model_tag = ''
    ELSE
      model_tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (model_tag = '*')
    SELF.Q.model_tag_Icon = 2
  ELSE
    SELF.Q.model_tag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = sub:Account_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      trade_tag = ''
    ELSE
      trade_tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (trade_tag = '*')
    SELF.Q.trade_tag_Icon = 2
  ELSE
    SELF.Q.trade_tag_Icon = 1
  END


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = sub:Account_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue


BRW13.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = cha:Charge_Type
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    IF ERRORCODE()
      charge_type_tag = ''
    ELSE
      charge_type_tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (charge_type_tag = '*')
    SELF.Q.charge_type_tag_Icon = 2
  ELSE
    SELF.Q.charge_type_tag_Icon = 1
  END


BRW13.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW13.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW13::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW13::RecordStatus=ReturnValue
  IF BRW13::RecordStatus NOT=Record:OK THEN RETURN BRW13::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = cha:Charge_Type
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    EXECUTE DASBRW::14:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW13::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW13::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW13::RecordStatus
  RETURN ReturnValue


BRW15.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue3.Pointer3 = cha:Charge_Type
     GET(GLO:Queue3,GLO:Queue3.Pointer3)
    IF ERRORCODE()
      charge_type_tag2 = ''
    ELSE
      charge_type_tag2 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (charge_type_tag2 = '*')
    SELF.Q.charge_type_tag2_Icon = 2
  ELSE
    SELF.Q.charge_type_tag2_Icon = 1
  END


BRW15.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW15.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW15::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW15::RecordStatus=ReturnValue
  IF BRW15::RecordStatus NOT=Record:OK THEN RETURN BRW15::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue3.Pointer3 = cha:Charge_Type
     GET(GLO:Queue3,GLO:Queue3.Pointer3)
    EXECUTE DASBRW::16:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW15::RecordStatus
  RETURN ReturnValue

