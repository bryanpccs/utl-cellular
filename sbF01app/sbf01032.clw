

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01032.INC'),ONCE        !Local module procedure declarations
                     END


Replicate_Common_Faults PROCEDURE                     !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Model_Number_Pointer          LIKE(GLO:Model_Number_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
FilesOpened          BYTE
save_cwp_alias_id    USHORT,AUTO
save_ccp_ali_id      USHORT,AUTO
Model_Number_Temp    STRING(30)
Description_Temp     STRING(30)
Manufacturer_Temp    STRING(30)
tag_temp             STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Manufacturer_Temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
window               WINDOW('Replicate Common Fault'),AT(,,220,307),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,5,212,272),USE(?Sheet1),SPREAD
                         TAB('Replicate Common Faults'),USE(?Tab1)
                           PROMPT('Model Number'),AT(8,21),USE(?Model_Number_Temp:Prompt),TRN
                           ENTRY(@s30),AT(84,21,124,10),USE(Model_Number_Temp),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Description'),AT(8,37),USE(?Description_Temp:Prompt),TRN
                           ENTRY(@s30),AT(84,37,124,10),USE(Description_Temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR
                           PROMPT('Replicate the above fault to the following models:'),AT(8,57),USE(?Prompt3),FONT(,8,,FONT:bold)
                           SHEET,AT(32,73,160,200),USE(?Sheet2),SPREAD
                             TAB('By Model Number'),USE(?Tab2)
                             END
                             TAB('By Manufacturer'),USE(?Tab3)
                               COMBO(@s30),AT(36,89,124,10),USE(Manufacturer_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M~Manufacturer~@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                               BUTTON('sho&W tags'),AT(80,173,70,13),USE(?DASSHOWTAG),HIDE
                               BUTTON('&Rev tags'),AT(84,149,50,13),USE(?DASREVTAG),HIDE
                             END
                           END
                           LIST,AT(36,105,152,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),ALRT(MouseRight),FORMAT('11L(2)I@s1@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Tag'),AT(36,253,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All'),AT(88,253,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(140,253,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                         END
                       END
                       PANEL,AT(4,281,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Replicate'),AT(8,284,56,16),USE(?OkButton),LEFT,ICON('copy.gif')
                       BUTTON('Close'),AT(156,284,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW2::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet2) = 2
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Model_Number_Temp:Prompt{prop:FontColor} = -1
    ?Model_Number_Temp:Prompt{prop:Color} = 15066597
    If ?Model_Number_Temp{prop:ReadOnly} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 15066597
    Elsif ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 8454143
    Else ! If ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 16777215
    End ! If ?Model_Number_Temp{prop:Req} = True
    ?Model_Number_Temp{prop:Trn} = 0
    ?Model_Number_Temp{prop:FontStyle} = font:Bold
    ?Description_Temp:Prompt{prop:FontColor} = -1
    ?Description_Temp:Prompt{prop:Color} = 15066597
    If ?Description_Temp{prop:ReadOnly} = True
        ?Description_Temp{prop:FontColor} = 65793
        ?Description_Temp{prop:Color} = 15066597
    Elsif ?Description_Temp{prop:Req} = True
        ?Description_Temp{prop:FontColor} = 65793
        ?Description_Temp{prop:Color} = 8454143
    Else ! If ?Description_Temp{prop:Req} = True
        ?Description_Temp{prop:FontColor} = 65793
        ?Description_Temp{prop:Color} = 16777215
    End ! If ?Description_Temp{prop:Req} = True
    ?Description_Temp{prop:Trn} = 0
    ?Description_Temp{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    If ?Manufacturer_Temp{prop:ReadOnly} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 15066597
    Elsif ?Manufacturer_Temp{prop:Req} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 8454143
    Else ! If ?Manufacturer_Temp{prop:Req} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 16777215
    End ! If ?Manufacturer_Temp{prop:Req} = True
    ?Manufacturer_Temp{prop:Trn} = 0
    ?Manufacturer_Temp{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW2.UpdateBuffer
   GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
   GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
  IF ERRORCODE()
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Q_ModelNumber)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW2.Reset
  FREE(GLO:Q_ModelNumber)
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Q_ModelNumber)
  BRW2.Reset
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Q_ModelNumber)
    GET(GLO:Q_ModelNumber,QR#)
    DASBRW::5:QUEUE = GLO:Q_ModelNumber
    ADD(DASBRW::5:QUEUE)
  END
  FREE(GLO:Q_ModelNumber)
  BRW2.Reset
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Model_Number_Pointer = mod:Model_Number
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Model_Number_Pointer)
    IF ERRORCODE()
       GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
       ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    END
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW2.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Replicate_Common_Faults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Replicate_Common_Faults',1)
    SolaceViewVars('save_cwp_alias_id',save_cwp_alias_id,'Replicate_Common_Faults',1)
    SolaceViewVars('save_ccp_ali_id',save_ccp_ali_id,'Replicate_Common_Faults',1)
    SolaceViewVars('Model_Number_Temp',Model_Number_Temp,'Replicate_Common_Faults',1)
    SolaceViewVars('Description_Temp',Description_Temp,'Replicate_Common_Faults',1)
    SolaceViewVars('Manufacturer_Temp',Manufacturer_Temp,'Replicate_Common_Faults',1)
    SolaceViewVars('tag_temp',tag_temp,'Replicate_Common_Faults',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_Number_Temp:Prompt;  SolaceCtrlName = '?Model_Number_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_Number_Temp;  SolaceCtrlName = '?Model_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Description_Temp:Prompt;  SolaceCtrlName = '?Description_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Description_Temp;  SolaceCtrlName = '?Description_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer_Temp;  SolaceCtrlName = '?Manufacturer_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Replicate_Common_Faults')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Replicate_Common_Faults')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Model_Number_Temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:COMMONCP.Open
  Relate:COMMONCP_ALIAS.Open
  Relate:COMMONFA_ALIAS.Open
  Relate:COMMONWP_ALIAS.Open
  Relate:MANUFACT.Open
  Access:COMMONFA.UseFile
  Access:COMMONWP.UseFile
  SELF.FilesOpened = True
  access:commonfa_alias.clearkey(com_ali:ref_number_key)
  com_ali:ref_number = glo:select1
  if access:commonfa_alias.fetch(com_ali:ref_number_key) = Level:Benign
      model_number_temp   = com_ali:model_number
      description_temp    = com_ali:description
  Else!if access:commonfa.fetch(com:ref_number_key) = Level:Benign
      Post(Event:CloseWindow)
  end!if access:commonfa.fetch(com:ref_number_key) = Level:Benign
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:MODELNUM,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW2.Q &= Queue:Browse
  BRW2.RetainRow = 0
  BRW2.AddSortOrder(,mod:Manufacturer_Key)
  BRW2.AddRange(mod:Manufacturer,Manufacturer_Temp)
  BRW2.AddLocator(BRW2::Sort1:Locator)
  BRW2::Sort1:Locator.Init(,mod:Model_Number,1,BRW2)
  BRW2.AddSortOrder(,mod:Model_Number_Key)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,mod:Model_Number,1,BRW2)
  BIND('tag_temp',tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW2.AddField(tag_temp,BRW2.Q.tag_temp)
  BRW2.AddField(mod:Model_Number,BRW2.Q.mod:Model_Number)
  BRW2.AddField(mod:Manufacturer,BRW2.Q.mod:Manufacturer)
  FDCB3.Init(Manufacturer_Temp,?Manufacturer_Temp,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(man:Manufacturer_Key)
  FDCB3.AddField(man:Manufacturer,FDCB3.Q.man:Manufacturer)
  FDCB3.AddField(man:RecordNumber,FDCB3.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Q_ModelNumber)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Q_ModelNumber)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:COMMONCP.Close
    Relate:COMMONCP_ALIAS.Close
    Relate:COMMONFA_ALIAS.Close
    Relate:COMMONWP_ALIAS.Close
    Relate:MANUFACT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Replicate_Common_Faults',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Manufacturer_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
      BRW2.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      beep(beep:systemquestion)  ;  yield()
      case message('Are you sure you want to replicate the above Fault to all '&|
              'the tagged models?', |
              'ServiceBase 2000', icon:question, |
               button:yes+button:no, button:no, 0)
      of button:yes
      Compile('***',Debug=1)
          Message('glo:select1' & glo:select1,'Debug Message',icon:exclamation)
      ***
      
          recordspercycle     = 25
          recordsprocessed    = 0
          percentprogress     = 0
          setcursor(cursor:wait)
          open(progresswindow)
          progress:thermometer    = 0
          ?progress:pcttext{prop:text} = '0% Completed'
          access:commonfa_alias.clearkey(com_ali:ref_number_key)
          com_ali:ref_number = glo:select1
          if access:commonfa_alias.fetch(com_ali:ref_number_key) = Level:Benign
          
              recordstoprocess    = Records(glo:Q_ModelNumber)
      
              Loop x# = 1 To Records(glo:Q_ModelNumber)
                  Get(glo:Q_ModelNumber,x#)
                  Do GetNextRecord2
                  access:commonfa.clearkey(com:description_key)
                  com:model_number = glo:model_number_pointer
                  com:category     = com_ali:category
                  com:description  = com_ali:description
                  if access:commonfa.tryfetch(com:description_key)
                      access:commonfa.clearkey(com:Ref_number_key)
                      get(commonfa,0)
      Compile('***',Debug=1)
          Message('Before Prime','Debug Message',icon:exclamation)
      ***
                      if access:commonfa.primerecord() = level:benign
      Compile('***',Debug=1)
          Message('After Prime','Debug Message',icon:exclamation)
      ***
                          ref_number$                 = com:ref_number
                          com:record                 :=: com_ali:record
                          com:ref_number              = ref_number$
                          com:model_number            = glo:model_number_pointer
                          com:engineers_notes         = com_ali:engineers_notes
                          com:invoice_text            = com_ali:invoice_text
      Compile('***',Debug=1)
          Message('Before inserting Commonfa','Debug Message',icon:exclamation)
      ***
                          if access:commonfa.tryupdate()
                              access:commonfa.cancelautoinc()
                          Else!if access:commonfa.tryupdate()
      Compile('***',Debug=1)
          Message('Common Fault Added','Debug Message',icon:exclamation)
      ***
                              access:commcat.clearkey(cmc:category_key)
                              cmc:model_number = com:model_number
                              cmc:category     = com:category
                              if access:commcat.fetch(cmc:category_key)
                                  get(commcat,0)
                                  if access:commcat.primerecord() = level:benign
                                      cmc:model_number  = com:model_number
                                      cmc:category      = com:category
                                      access:commcat.tryupdate()
      
                                  End!if access:commcat.primerecord() = level:benign
                              End!if access:commcat.fetch(cmc:category_key)
                   
                              save_ccp_ali_id = access:commoncp_alias.savefile()
                              access:commoncp_alias.clearkey(ccp_ali:ref_number_key)
                              ccp_ali:ref_number = glo:select1
                              set(ccp_ali:ref_number_key,ccp_ali:ref_number_key)
                              loop
                                  if access:commoncp_alias.next()
                                     break
                                  end !if
                                  if ccp_ali:ref_number <> glo:select1      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  get(commoncp,0)
                                  if access:commoncp.primerecord() = level:benign
                                      record_number$          = ccp:record_number
                                      ccp:record             :=: ccp_ali:record
                                      ccp:record_number       = record_number$
                                      ccp:ref_number          = com:ref_number
                                      if access:commoncp.tryupdate()
                                          access:commoncp.cancelautoinc()
                                      end
                                  end!if access:commoncp.primerecord() = level:benign
                              end !loop
                              save_cwp_alias_id = access:commonwp_alias.savefile()
                              access:commonwp_alias.clearkey(cwp_alias:ref_number_key)
                              cwp_alias:ref_number = glo:select1
                              set(cwp_alias:ref_number_key,cwp_alias:ref_number_key)
                              loop
                                  if access:commonwp_alias.next()
                                     break
                                  end !if
                                  if cwp_alias:ref_number <> glo:select1      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  get(commonwp,0)
                                  if access:commonwp.primerecord() = level:benign
                                      record_number$          = cwp:record_number
                                      cwp:record             :=: cwp_alias:record
                                      cwp:record_number       = record_number$
                                      cwp:ref_number          = com:ref_number
                                      if access:commonwp.tryupdate()
                                          access:commonwp.cancelautoinc()
                                      end
                                  end!if access:commoncp.primerecord() = level:benign
                              end !loop
                          end!if access:commonfa.tryupdate()
                      end!if access:commonfa.primerecord() = level:benign
                  End!if access:commonfa.tryfetch(com:description_key)
              End!Loop x# = 1 To Records(glo:Q_ModelNumber)
              setcursor()
              close(progresswindow)
              beep(beep:systemasterisk)  ;  yield()
              message('Replication Completed.', |
                      'ServiceBase 2000', icon:asterisk)
          end!if access:commonfa.fetch(com:ref_number_key) = Level:Benign
      of button:no
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Replicate_Common_Faults')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::5:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW2.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW2.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW2::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW2::RecordStatus=ReturnValue
  IF BRW2::RecordStatus NOT=Record:OK THEN RETURN BRW2::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW2::RecordStatus
  RETURN ReturnValue

