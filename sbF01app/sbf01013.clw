

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01013.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Siemens_Import PROCEDURE                       !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
FilesOpened          BYTE
tag_temp             STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(EPSIMP)
                       PROJECT(epi:Model_Number)
                       PROJECT(epi:Title)
                       PROJECT(epi:Initial)
                       PROJECT(epi:Surname)
                       PROJECT(epi:Company_Name_Collection)
                       PROJECT(epi:ESN)
                       PROJECT(epi:Address_Line1_Collection)
                       PROJECT(epi:Address_Line2_Collection)
                       PROJECT(epi:Address_Line3_Collection)
                       PROJECT(epi:Postcode_Collection)
                       PROJECT(epi:Telephone_Collection)
                       PROJECT(epi:Fault_Code1)
                       PROJECT(epi:Order_Number)
                       PROJECT(epi:Fault_Description)
                       PROJECT(epi:Record_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
epi:Model_Number       LIKE(epi:Model_Number)         !List box control field - type derived from field
epi:Model_Number_NormalFG LONG                        !Normal forground color
epi:Model_Number_NormalBG LONG                        !Normal background color
epi:Model_Number_SelectedFG LONG                      !Selected forground color
epi:Model_Number_SelectedBG LONG                      !Selected background color
epi:Title              LIKE(epi:Title)                !List box control field - type derived from field
epi:Title_NormalFG     LONG                           !Normal forground color
epi:Title_NormalBG     LONG                           !Normal background color
epi:Title_SelectedFG   LONG                           !Selected forground color
epi:Title_SelectedBG   LONG                           !Selected background color
epi:Initial            LIKE(epi:Initial)              !List box control field - type derived from field
epi:Initial_NormalFG   LONG                           !Normal forground color
epi:Initial_NormalBG   LONG                           !Normal background color
epi:Initial_SelectedFG LONG                           !Selected forground color
epi:Initial_SelectedBG LONG                           !Selected background color
epi:Surname            LIKE(epi:Surname)              !List box control field - type derived from field
epi:Surname_NormalFG   LONG                           !Normal forground color
epi:Surname_NormalBG   LONG                           !Normal background color
epi:Surname_SelectedFG LONG                           !Selected forground color
epi:Surname_SelectedBG LONG                           !Selected background color
epi:Company_Name_Collection LIKE(epi:Company_Name_Collection) !List box control field - type derived from field
epi:Company_Name_Collection_NormalFG LONG             !Normal forground color
epi:Company_Name_Collection_NormalBG LONG             !Normal background color
epi:Company_Name_Collection_SelectedFG LONG           !Selected forground color
epi:Company_Name_Collection_SelectedBG LONG           !Selected background color
epi:ESN                LIKE(epi:ESN)                  !List box control field - type derived from field
epi:ESN_NormalFG       LONG                           !Normal forground color
epi:ESN_NormalBG       LONG                           !Normal background color
epi:ESN_SelectedFG     LONG                           !Selected forground color
epi:ESN_SelectedBG     LONG                           !Selected background color
epi:Address_Line1_Collection LIKE(epi:Address_Line1_Collection) !List box control field - type derived from field
epi:Address_Line1_Collection_NormalFG LONG            !Normal forground color
epi:Address_Line1_Collection_NormalBG LONG            !Normal background color
epi:Address_Line1_Collection_SelectedFG LONG          !Selected forground color
epi:Address_Line1_Collection_SelectedBG LONG          !Selected background color
epi:Address_Line2_Collection LIKE(epi:Address_Line2_Collection) !List box control field - type derived from field
epi:Address_Line2_Collection_NormalFG LONG            !Normal forground color
epi:Address_Line2_Collection_NormalBG LONG            !Normal background color
epi:Address_Line2_Collection_SelectedFG LONG          !Selected forground color
epi:Address_Line2_Collection_SelectedBG LONG          !Selected background color
epi:Address_Line3_Collection LIKE(epi:Address_Line3_Collection) !List box control field - type derived from field
epi:Address_Line3_Collection_NormalFG LONG            !Normal forground color
epi:Address_Line3_Collection_NormalBG LONG            !Normal background color
epi:Address_Line3_Collection_SelectedFG LONG          !Selected forground color
epi:Address_Line3_Collection_SelectedBG LONG          !Selected background color
epi:Postcode_Collection LIKE(epi:Postcode_Collection) !List box control field - type derived from field
epi:Postcode_Collection_NormalFG LONG                 !Normal forground color
epi:Postcode_Collection_NormalBG LONG                 !Normal background color
epi:Postcode_Collection_SelectedFG LONG               !Selected forground color
epi:Postcode_Collection_SelectedBG LONG               !Selected background color
epi:Telephone_Collection LIKE(epi:Telephone_Collection) !List box control field - type derived from field
epi:Telephone_Collection_NormalFG LONG                !Normal forground color
epi:Telephone_Collection_NormalBG LONG                !Normal background color
epi:Telephone_Collection_SelectedFG LONG              !Selected forground color
epi:Telephone_Collection_SelectedBG LONG              !Selected background color
epi:Fault_Code1        LIKE(epi:Fault_Code1)          !List box control field - type derived from field
epi:Fault_Code1_NormalFG LONG                         !Normal forground color
epi:Fault_Code1_NormalBG LONG                         !Normal background color
epi:Fault_Code1_SelectedFG LONG                       !Selected forground color
epi:Fault_Code1_SelectedBG LONG                       !Selected background color
epi:Order_Number       LIKE(epi:Order_Number)         !List box control field - type derived from field
epi:Order_Number_NormalFG LONG                        !Normal forground color
epi:Order_Number_NormalBG LONG                        !Normal background color
epi:Order_Number_SelectedFG LONG                      !Selected forground color
epi:Order_Number_SelectedBG LONG                      !Selected background color
epi:Fault_Description  STRING(SIZE(epi:Fault_Description)) !List box control field - STRING defined to hold MEMO's contents
epi:Fault_Description_NormalFG LONG                   !Normal forground color
epi:Fault_Description_NormalBG LONG                   !Normal background color
epi:Fault_Description_SelectedFG LONG                 !Selected forground color
epi:Fault_Description_SelectedBG LONG                 !Selected background color
epi:Record_Number      LIKE(epi:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Siemens Import File'),AT(,,529,329),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Siemens_Import'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,20,424,276),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('11LI@s1@87L(2)|M*~Model Number~@s30@23L(2)|M*~Title~@s4@22L(2)|M*~Initial~@s1@12' &|
   '0L(2)|M*~Surname~@s30@120L(2)|M*~Company Name~@s30@120L(2)|M*~ESN/IMEI~@s30@120L' &|
   '(2)|M*~Address Line 1~@s30@120L(2)|M*~Address Line 2~@s30@120L(2)|M*~Address Lin' &|
   'e 3~@s30@40L(2)|M*~Postcode~@s10@60L(2)|M*~Telephone Number~@s15@120L(2)|M*~Faul' &|
   't Code~@s30@120L(2)|M*~Order Number~@s30@1020L(2)|M*~Fault Description~@s255@'),FROM(Queue:Browse:1)
                       BUTTON('Import Tagged Jobs'),AT(448,20,76,20),USE(?Select),LEFT,ICON(ICON:NextPage)
                       PANEL,AT(448,48,77,49),USE(?Panel1),FILL(COLOR:Gray)
                       BOX,AT(452,51,9,9),USE(?Box1),COLOR(COLOR:Blue),FILL(COLOR:Blue)
                       PROMPT('- Invalid Postcode'),AT(464,51),USE(?Prompt1),FONT(,,COLOR:White,,CHARSET:ANSI)
                       PROMPT('- Invalid Model'),AT(464,64),USE(?Prompt1:2),FONT(,,COLOR:White,,CHARSET:ANSI)
                       PROMPT('- Invalid Model && Postcode'),AT(464,76,56,20),USE(?Prompt1:3),FONT(,,COLOR:White,,CHARSET:ANSI)
                       BOX,AT(452,64,9,9),USE(?Box1:2),COLOR(COLOR:Red),FILL(COLOR:Red)
                       BOX,AT(452,76,9,9),USE(?Box1:3),COLOR(COLOR:Purple),FILL(COLOR:Purple)
                       BUTTON('&View Record'),AT(448,148,76,20),USE(?Change:2),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('Remove Tagged Jobs'),AT(448,272,76,20),USE(?Remove_Tagged_Jobs),LEFT,ICON('delete.ico')
                       SHEET,AT(4,4,436,320),USE(?CurrentTab),SPREAD
                         TAB('Record Order'),USE(?Tab:2)
                           BUTTON('&Tag'),AT(8,300,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All'),AT(68,300,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(128,300,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(288,304,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(340,304,70,13),USE(?DASSHOWTAG),HIDE
                         END
                       END
                       BUTTON('Close'),AT(448,304,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?Prompt1:3{prop:FontColor} = -1
    ?Prompt1:3{prop:Color} = 15066597
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Queue.Pointer = epi:Record_Number
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = epi:Record_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = epi:Record_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::5:QUEUE = GLO:Queue
    ADD(DASBRW::5:QUEUE)
  END
  FREE(GLO:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer = epi:Record_Number
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = epi:Record_Number
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Siemens_Import',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Siemens_Import',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Siemens_Import',1)
    SolaceViewVars('tag_temp',tag_temp,'Browse_Siemens_Import',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select;  SolaceCtrlName = '?Select';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1;  SolaceCtrlName = '?Box1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:3;  SolaceCtrlName = '?Prompt1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1:2;  SolaceCtrlName = '?Box1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1:3;  SolaceCtrlName = '?Box1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Remove_Tagged_Jobs;  SolaceCtrlName = '?Remove_Tagged_Jobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Siemens_Import')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Siemens_Import')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EPSIMP.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EPSIMP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,)
  BIND('tag_temp',tag_temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(epi:Model_Number,BRW1.Q.epi:Model_Number)
  BRW1.AddField(epi:Title,BRW1.Q.epi:Title)
  BRW1.AddField(epi:Initial,BRW1.Q.epi:Initial)
  BRW1.AddField(epi:Surname,BRW1.Q.epi:Surname)
  BRW1.AddField(epi:Company_Name_Collection,BRW1.Q.epi:Company_Name_Collection)
  BRW1.AddField(epi:ESN,BRW1.Q.epi:ESN)
  BRW1.AddField(epi:Address_Line1_Collection,BRW1.Q.epi:Address_Line1_Collection)
  BRW1.AddField(epi:Address_Line2_Collection,BRW1.Q.epi:Address_Line2_Collection)
  BRW1.AddField(epi:Address_Line3_Collection,BRW1.Q.epi:Address_Line3_Collection)
  BRW1.AddField(epi:Postcode_Collection,BRW1.Q.epi:Postcode_Collection)
  BRW1.AddField(epi:Telephone_Collection,BRW1.Q.epi:Telephone_Collection)
  BRW1.AddField(epi:Fault_Code1,BRW1.Q.epi:Fault_Code1)
  BRW1.AddField(epi:Order_Number,BRW1.Q.epi:Order_Number)
  BRW1.AddField(epi:Fault_Description,BRW1.Q.epi:Fault_Description)
  BRW1.AddField(epi:Record_Number,BRW1.Q.epi:Record_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:EPSIMP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Siemens_Import',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateEPSCSV
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Remove_Tagged_Jobs
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove_Tagged_Jobs, Accepted)
      beep(beep:systemquestion)  ;  yield()
      case message('Are you sure you want to remove the Tagged Jobs from the '&|
              'import list?', |
              'ServiceBase 2000', icon:question, |
               button:yes+button:no, button:no, 0)
      of button:yes
          Loop x# = 1 To Records(glo:Queue)
              Get(glo:Queue,x#)
              access:epsimp.clearkey(epi:record_number_key)
              epi:record_number = glo:pointer
              If access:epsimp.fetch(epi:record_number_key) = Level:Benign
                  Delete(epsimp)
              End!If access:epsimp:fetch(epi:record_number_key) = Level:Benign
          End!Loop x# = 1 To Records(glo:Queue)
      of button:no
      end !case
      Thiswindow.reset(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove_Tagged_Jobs, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Siemens_Import')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::5:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change:2
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = epi:Record_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END
  SELF.Q.epi:Model_Number_NormalFG = -1
  SELF.Q.epi:Model_Number_NormalBG = -1
  SELF.Q.epi:Model_Number_SelectedFG = -1
  SELF.Q.epi:Model_Number_SelectedBG = -1
  SELF.Q.epi:Title_NormalFG = -1
  SELF.Q.epi:Title_NormalBG = -1
  SELF.Q.epi:Title_SelectedFG = -1
  SELF.Q.epi:Title_SelectedBG = -1
  SELF.Q.epi:Initial_NormalFG = -1
  SELF.Q.epi:Initial_NormalBG = -1
  SELF.Q.epi:Initial_SelectedFG = -1
  SELF.Q.epi:Initial_SelectedBG = -1
  SELF.Q.epi:Surname_NormalFG = -1
  SELF.Q.epi:Surname_NormalBG = -1
  SELF.Q.epi:Surname_SelectedFG = -1
  SELF.Q.epi:Surname_SelectedBG = -1
  SELF.Q.epi:Company_Name_Collection_NormalFG = -1
  SELF.Q.epi:Company_Name_Collection_NormalBG = -1
  SELF.Q.epi:Company_Name_Collection_SelectedFG = -1
  SELF.Q.epi:Company_Name_Collection_SelectedBG = -1
  SELF.Q.epi:ESN_NormalFG = -1
  SELF.Q.epi:ESN_NormalBG = -1
  SELF.Q.epi:ESN_SelectedFG = -1
  SELF.Q.epi:ESN_SelectedBG = -1
  SELF.Q.epi:Address_Line1_Collection_NormalFG = -1
  SELF.Q.epi:Address_Line1_Collection_NormalBG = -1
  SELF.Q.epi:Address_Line1_Collection_SelectedFG = -1
  SELF.Q.epi:Address_Line1_Collection_SelectedBG = -1
  SELF.Q.epi:Address_Line2_Collection_NormalFG = -1
  SELF.Q.epi:Address_Line2_Collection_NormalBG = -1
  SELF.Q.epi:Address_Line2_Collection_SelectedFG = -1
  SELF.Q.epi:Address_Line2_Collection_SelectedBG = -1
  SELF.Q.epi:Address_Line3_Collection_NormalFG = -1
  SELF.Q.epi:Address_Line3_Collection_NormalBG = -1
  SELF.Q.epi:Address_Line3_Collection_SelectedFG = -1
  SELF.Q.epi:Address_Line3_Collection_SelectedBG = -1
  SELF.Q.epi:Postcode_Collection_NormalFG = -1
  SELF.Q.epi:Postcode_Collection_NormalBG = -1
  SELF.Q.epi:Postcode_Collection_SelectedFG = -1
  SELF.Q.epi:Postcode_Collection_SelectedBG = -1
  SELF.Q.epi:Telephone_Collection_NormalFG = -1
  SELF.Q.epi:Telephone_Collection_NormalBG = -1
  SELF.Q.epi:Telephone_Collection_SelectedFG = -1
  SELF.Q.epi:Telephone_Collection_SelectedBG = -1
  SELF.Q.epi:Fault_Code1_NormalFG = -1
  SELF.Q.epi:Fault_Code1_NormalBG = -1
  SELF.Q.epi:Fault_Code1_SelectedFG = -1
  SELF.Q.epi:Fault_Code1_SelectedBG = -1
  SELF.Q.epi:Order_Number_NormalFG = -1
  SELF.Q.epi:Order_Number_NormalBG = -1
  SELF.Q.epi:Order_Number_SelectedFG = -1
  SELF.Q.epi:Order_Number_SelectedBG = -1
  SELF.Q.epi:Fault_Description_NormalFG = -1
  SELF.Q.epi:Fault_Description_NormalBG = -1
  SELF.Q.epi:Fault_Description_SelectedFG = -1
  SELF.Q.epi:Fault_Description_SelectedBG = -1
   
   
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Model_Number_NormalFG = 255
     SELF.Q.epi:Model_Number_NormalBG = 16777215
     SELF.Q.epi:Model_Number_SelectedFG = 16777215
     SELF.Q.epi:Model_Number_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Model_Number_NormalFG = 16711680
     SELF.Q.epi:Model_Number_NormalBG = 16777215
     SELF.Q.epi:Model_Number_SelectedFG = 16777215
     SELF.Q.epi:Model_Number_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Model_Number_NormalFG = 8388736
     SELF.Q.epi:Model_Number_NormalBG = 16777215
     SELF.Q.epi:Model_Number_SelectedFG = 16777215
     SELF.Q.epi:Model_Number_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Model_Number_NormalFG = -1
     SELF.Q.epi:Model_Number_NormalBG = -1
     SELF.Q.epi:Model_Number_SelectedFG = -1
     SELF.Q.epi:Model_Number_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Title_NormalFG = 255
     SELF.Q.epi:Title_NormalBG = 16777215
     SELF.Q.epi:Title_SelectedFG = 16777215
     SELF.Q.epi:Title_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Title_NormalFG = 16711680
     SELF.Q.epi:Title_NormalBG = 16777215
     SELF.Q.epi:Title_SelectedFG = 16777215
     SELF.Q.epi:Title_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Title_NormalFG = 8388736
     SELF.Q.epi:Title_NormalBG = 16777215
     SELF.Q.epi:Title_SelectedFG = 16777215
     SELF.Q.epi:Title_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Title_NormalFG = -1
     SELF.Q.epi:Title_NormalBG = -1
     SELF.Q.epi:Title_SelectedFG = -1
     SELF.Q.epi:Title_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Initial_NormalFG = 255
     SELF.Q.epi:Initial_NormalBG = 16777215
     SELF.Q.epi:Initial_SelectedFG = 16777215
     SELF.Q.epi:Initial_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Initial_NormalFG = 16711680
     SELF.Q.epi:Initial_NormalBG = 16777215
     SELF.Q.epi:Initial_SelectedFG = 16777215
     SELF.Q.epi:Initial_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Initial_NormalFG = 8388736
     SELF.Q.epi:Initial_NormalBG = 16777215
     SELF.Q.epi:Initial_SelectedFG = 16777215
     SELF.Q.epi:Initial_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Initial_NormalFG = -1
     SELF.Q.epi:Initial_NormalBG = -1
     SELF.Q.epi:Initial_SelectedFG = -1
     SELF.Q.epi:Initial_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Surname_NormalFG = 255
     SELF.Q.epi:Surname_NormalBG = 16777215
     SELF.Q.epi:Surname_SelectedFG = 16777215
     SELF.Q.epi:Surname_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Surname_NormalFG = 16711680
     SELF.Q.epi:Surname_NormalBG = 16777215
     SELF.Q.epi:Surname_SelectedFG = 16777215
     SELF.Q.epi:Surname_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Surname_NormalFG = 8388736
     SELF.Q.epi:Surname_NormalBG = 16777215
     SELF.Q.epi:Surname_SelectedFG = 16777215
     SELF.Q.epi:Surname_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Surname_NormalFG = -1
     SELF.Q.epi:Surname_NormalBG = -1
     SELF.Q.epi:Surname_SelectedFG = -1
     SELF.Q.epi:Surname_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Company_Name_Collection_NormalFG = 255
     SELF.Q.epi:Company_Name_Collection_NormalBG = 16777215
     SELF.Q.epi:Company_Name_Collection_SelectedFG = 16777215
     SELF.Q.epi:Company_Name_Collection_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Company_Name_Collection_NormalFG = 16711680
     SELF.Q.epi:Company_Name_Collection_NormalBG = 16777215
     SELF.Q.epi:Company_Name_Collection_SelectedFG = 16777215
     SELF.Q.epi:Company_Name_Collection_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Company_Name_Collection_NormalFG = 8388736
     SELF.Q.epi:Company_Name_Collection_NormalBG = 16777215
     SELF.Q.epi:Company_Name_Collection_SelectedFG = 16777215
     SELF.Q.epi:Company_Name_Collection_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Company_Name_Collection_NormalFG = -1
     SELF.Q.epi:Company_Name_Collection_NormalBG = -1
     SELF.Q.epi:Company_Name_Collection_SelectedFG = -1
     SELF.Q.epi:Company_Name_Collection_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:ESN_NormalFG = 255
     SELF.Q.epi:ESN_NormalBG = 16777215
     SELF.Q.epi:ESN_SelectedFG = 16777215
     SELF.Q.epi:ESN_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:ESN_NormalFG = 16711680
     SELF.Q.epi:ESN_NormalBG = 16777215
     SELF.Q.epi:ESN_SelectedFG = 16777215
     SELF.Q.epi:ESN_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:ESN_NormalFG = 8388736
     SELF.Q.epi:ESN_NormalBG = 16777215
     SELF.Q.epi:ESN_SelectedFG = 16777215
     SELF.Q.epi:ESN_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:ESN_NormalFG = -1
     SELF.Q.epi:ESN_NormalBG = -1
     SELF.Q.epi:ESN_SelectedFG = -1
     SELF.Q.epi:ESN_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Address_Line1_Collection_NormalFG = 255
     SELF.Q.epi:Address_Line1_Collection_NormalBG = 16777215
     SELF.Q.epi:Address_Line1_Collection_SelectedFG = 16777215
     SELF.Q.epi:Address_Line1_Collection_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Address_Line1_Collection_NormalFG = 16711680
     SELF.Q.epi:Address_Line1_Collection_NormalBG = 16777215
     SELF.Q.epi:Address_Line1_Collection_SelectedFG = 16777215
     SELF.Q.epi:Address_Line1_Collection_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Address_Line1_Collection_NormalFG = 8388736
     SELF.Q.epi:Address_Line1_Collection_NormalBG = 16777215
     SELF.Q.epi:Address_Line1_Collection_SelectedFG = 16777215
     SELF.Q.epi:Address_Line1_Collection_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Address_Line1_Collection_NormalFG = -1
     SELF.Q.epi:Address_Line1_Collection_NormalBG = -1
     SELF.Q.epi:Address_Line1_Collection_SelectedFG = -1
     SELF.Q.epi:Address_Line1_Collection_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Address_Line2_Collection_NormalFG = 255
     SELF.Q.epi:Address_Line2_Collection_NormalBG = 16777215
     SELF.Q.epi:Address_Line2_Collection_SelectedFG = 16777215
     SELF.Q.epi:Address_Line2_Collection_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Address_Line2_Collection_NormalFG = 16711680
     SELF.Q.epi:Address_Line2_Collection_NormalBG = 16777215
     SELF.Q.epi:Address_Line2_Collection_SelectedFG = 16777215
     SELF.Q.epi:Address_Line2_Collection_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Address_Line2_Collection_NormalFG = 8388736
     SELF.Q.epi:Address_Line2_Collection_NormalBG = 16777215
     SELF.Q.epi:Address_Line2_Collection_SelectedFG = 16777215
     SELF.Q.epi:Address_Line2_Collection_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Address_Line2_Collection_NormalFG = -1
     SELF.Q.epi:Address_Line2_Collection_NormalBG = -1
     SELF.Q.epi:Address_Line2_Collection_SelectedFG = -1
     SELF.Q.epi:Address_Line2_Collection_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Address_Line3_Collection_NormalFG = 255
     SELF.Q.epi:Address_Line3_Collection_NormalBG = 16777215
     SELF.Q.epi:Address_Line3_Collection_SelectedFG = 16777215
     SELF.Q.epi:Address_Line3_Collection_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Address_Line3_Collection_NormalFG = 16711680
     SELF.Q.epi:Address_Line3_Collection_NormalBG = 16777215
     SELF.Q.epi:Address_Line3_Collection_SelectedFG = 16777215
     SELF.Q.epi:Address_Line3_Collection_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Address_Line3_Collection_NormalFG = 8388736
     SELF.Q.epi:Address_Line3_Collection_NormalBG = 16777215
     SELF.Q.epi:Address_Line3_Collection_SelectedFG = 16777215
     SELF.Q.epi:Address_Line3_Collection_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Address_Line3_Collection_NormalFG = -1
     SELF.Q.epi:Address_Line3_Collection_NormalBG = -1
     SELF.Q.epi:Address_Line3_Collection_SelectedFG = -1
     SELF.Q.epi:Address_Line3_Collection_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Postcode_Collection_NormalFG = 255
     SELF.Q.epi:Postcode_Collection_NormalBG = 16777215
     SELF.Q.epi:Postcode_Collection_SelectedFG = 16777215
     SELF.Q.epi:Postcode_Collection_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Postcode_Collection_NormalFG = 16711680
     SELF.Q.epi:Postcode_Collection_NormalBG = 16777215
     SELF.Q.epi:Postcode_Collection_SelectedFG = 16777215
     SELF.Q.epi:Postcode_Collection_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Postcode_Collection_NormalFG = 8388736
     SELF.Q.epi:Postcode_Collection_NormalBG = 16777215
     SELF.Q.epi:Postcode_Collection_SelectedFG = 16777215
     SELF.Q.epi:Postcode_Collection_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Postcode_Collection_NormalFG = -1
     SELF.Q.epi:Postcode_Collection_NormalBG = -1
     SELF.Q.epi:Postcode_Collection_SelectedFG = -1
     SELF.Q.epi:Postcode_Collection_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Telephone_Collection_NormalFG = 255
     SELF.Q.epi:Telephone_Collection_NormalBG = 16777215
     SELF.Q.epi:Telephone_Collection_SelectedFG = 16777215
     SELF.Q.epi:Telephone_Collection_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Telephone_Collection_NormalFG = 16711680
     SELF.Q.epi:Telephone_Collection_NormalBG = 16777215
     SELF.Q.epi:Telephone_Collection_SelectedFG = 16777215
     SELF.Q.epi:Telephone_Collection_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Telephone_Collection_NormalFG = 8388736
     SELF.Q.epi:Telephone_Collection_NormalBG = 16777215
     SELF.Q.epi:Telephone_Collection_SelectedFG = 16777215
     SELF.Q.epi:Telephone_Collection_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Telephone_Collection_NormalFG = -1
     SELF.Q.epi:Telephone_Collection_NormalBG = -1
     SELF.Q.epi:Telephone_Collection_SelectedFG = -1
     SELF.Q.epi:Telephone_Collection_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Fault_Code1_NormalFG = 255
     SELF.Q.epi:Fault_Code1_NormalBG = 16777215
     SELF.Q.epi:Fault_Code1_SelectedFG = 16777215
     SELF.Q.epi:Fault_Code1_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Fault_Code1_NormalFG = 16711680
     SELF.Q.epi:Fault_Code1_NormalBG = 16777215
     SELF.Q.epi:Fault_Code1_SelectedFG = 16777215
     SELF.Q.epi:Fault_Code1_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Fault_Code1_NormalFG = 8388736
     SELF.Q.epi:Fault_Code1_NormalBG = 16777215
     SELF.Q.epi:Fault_Code1_SelectedFG = 16777215
     SELF.Q.epi:Fault_Code1_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Fault_Code1_NormalFG = -1
     SELF.Q.epi:Fault_Code1_NormalBG = -1
     SELF.Q.epi:Fault_Code1_SelectedFG = -1
     SELF.Q.epi:Fault_Code1_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Order_Number_NormalFG = 255
     SELF.Q.epi:Order_Number_NormalBG = 16777215
     SELF.Q.epi:Order_Number_SelectedFG = 16777215
     SELF.Q.epi:Order_Number_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Order_Number_NormalFG = 16711680
     SELF.Q.epi:Order_Number_NormalBG = 16777215
     SELF.Q.epi:Order_Number_SelectedFG = 16777215
     SELF.Q.epi:Order_Number_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Order_Number_NormalFG = 8388736
     SELF.Q.epi:Order_Number_NormalBG = 16777215
     SELF.Q.epi:Order_Number_SelectedFG = 16777215
     SELF.Q.epi:Order_Number_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Order_Number_NormalFG = -1
     SELF.Q.epi:Order_Number_NormalBG = -1
     SELF.Q.epi:Order_Number_SelectedFG = -1
     SELF.Q.epi:Order_Number_SelectedBG = -1
   END
   IF (epi:Passed = 'MOD')
     SELF.Q.epi:Fault_Description_NormalFG = 255
     SELF.Q.epi:Fault_Description_NormalBG = 16777215
     SELF.Q.epi:Fault_Description_SelectedFG = 16777215
     SELF.Q.epi:Fault_Description_SelectedBG = 255
   ELSIF(epi:Passed = 'POS')
     SELF.Q.epi:Fault_Description_NormalFG = 16711680
     SELF.Q.epi:Fault_Description_NormalBG = 16777215
     SELF.Q.epi:Fault_Description_SelectedFG = 16777215
     SELF.Q.epi:Fault_Description_SelectedBG = 16711680
   ELSIF(epi:Passed = 'MPS')
     SELF.Q.epi:Fault_Description_NormalFG = 8388736
     SELF.Q.epi:Fault_Description_NormalBG = 16777215
     SELF.Q.epi:Fault_Description_SelectedFG = 16777215
     SELF.Q.epi:Fault_Description_SelectedBG = 8388736
   ELSE
     SELF.Q.epi:Fault_Description_NormalFG = -1
     SELF.Q.epi:Fault_Description_NormalBG = -1
     SELF.Q.epi:Fault_Description_SelectedFG = -1
     SELF.Q.epi:Fault_Description_SelectedBG = -1
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = epi:Record_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

