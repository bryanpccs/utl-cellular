

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01012.INC'),ONCE        !Local module procedure declarations
                     END


UpdateEPSCSV PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::epi:Record  LIKE(epi:RECORD),STATIC
QuickWindow          WINDOW('Update the EPSCSV File'),AT(,,358,232),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateEPSCSV'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,352,196),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Model Number'),AT(8,20),USE(?EPS:LABEL1:Prompt)
                           ENTRY(@s30),AT(84,20,124,10),USE(epi:Model_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Invalid Postcode'),AT(224,20,128,28),USE(?invalid_postcode:prompt),HIDE,FONT(,11,COLOR:Red,FONT:bold)
                           ENTRY(@s4),AT(84,36,32,10),USE(epi:Title),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           ENTRY(@s1),AT(120,36,12,10),USE(epi:Initial),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Surname'),AT(8,36),USE(?EPS:LABEL6:Prompt)
                           ENTRY(@s30),AT(136,36,72,10),USE(epi:Surname),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Telephone Number'),AT(8,52),USE(?EPS:LABEL7:Prompt)
                           ENTRY(@s15),AT(84,52,124,10),USE(epi:Telephone_Collection),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(68,20,10,10),USE(?Lookup_Model),ICON('list3.ico')
                           PROMPT('E.S.N. / I.M.E.I.'),AT(7,68),USE(?EPI:ESN:Prompt),TRN
                           ENTRY(@s30),AT(84,68,124,10),USE(epi:ESN),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code'),AT(8,84),USE(?EPS:LABEL9:Prompt)
                           ENTRY(@s30),AT(84,84,124,10),USE(epi:Fault_Code1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Company Name'),AT(8,100),USE(?EPS:LABEL10:Prompt)
                           ENTRY(@s30),AT(84,100,124,10),USE(epi:Company_Name_Collection),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON('&Move Down'),AT(212,100,68,10),USE(?move_down),LEFT,ICON('small_down.ico')
                           BUTTON('&Move Down'),AT(212,116,68,10),USE(?move_down:2),LEFT,ICON('small_down.ico')
                           PROMPT('Address'),AT(8,116),USE(?EPS:LABEL11:Prompt),TRN
                           ENTRY(@s30),AT(84,116,124,10),USE(epi:Address_Line1_Collection),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON('&Move Down'),AT(212,128,68,10),USE(?move_down:3),LEFT,ICON('small_down.ico')
                           ENTRY(@s30),AT(84,128,124,10),USE(epi:Address_Line2_Collection),FONT('Tahoma',,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON('&Move Down'),AT(212,140,68,10),USE(?move_down:4),LEFT,ICON('small_down.ico')
                           ENTRY(@s30),AT(84,140,124,10),USE(epi:Address_Line3_Collection),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Postcode'),AT(8,152),USE(?Eps:LABEL14:Prompt)
                           ENTRY(@s10),AT(84,152,64,10),USE(epi:Postcode_Collection),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Fault Desription'),AT(8,168),USE(?EPS:LABEL15:Prompt),TRN
                           ENTRY(@s255),AT(84,168,264,10),USE(epi:Fault_Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Order Number'),AT(8,184),USE(?EPI:Order_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,184,124,10),USE(epi:Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       PANEL,AT(4,204,352,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(240,208,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(296,208,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?EPS:LABEL1:Prompt{prop:FontColor} = -1
    ?EPS:LABEL1:Prompt{prop:Color} = 15066597
    If ?epi:Model_Number{prop:ReadOnly} = True
        ?epi:Model_Number{prop:FontColor} = 65793
        ?epi:Model_Number{prop:Color} = 15066597
    Elsif ?epi:Model_Number{prop:Req} = True
        ?epi:Model_Number{prop:FontColor} = 65793
        ?epi:Model_Number{prop:Color} = 8454143
    Else ! If ?epi:Model_Number{prop:Req} = True
        ?epi:Model_Number{prop:FontColor} = 65793
        ?epi:Model_Number{prop:Color} = 16777215
    End ! If ?epi:Model_Number{prop:Req} = True
    ?epi:Model_Number{prop:Trn} = 0
    ?epi:Model_Number{prop:FontStyle} = font:Bold
    ?invalid_postcode:prompt{prop:FontColor} = -1
    ?invalid_postcode:prompt{prop:Color} = 15066597
    If ?epi:Title{prop:ReadOnly} = True
        ?epi:Title{prop:FontColor} = 65793
        ?epi:Title{prop:Color} = 15066597
    Elsif ?epi:Title{prop:Req} = True
        ?epi:Title{prop:FontColor} = 65793
        ?epi:Title{prop:Color} = 8454143
    Else ! If ?epi:Title{prop:Req} = True
        ?epi:Title{prop:FontColor} = 65793
        ?epi:Title{prop:Color} = 16777215
    End ! If ?epi:Title{prop:Req} = True
    ?epi:Title{prop:Trn} = 0
    ?epi:Title{prop:FontStyle} = font:Bold
    If ?epi:Initial{prop:ReadOnly} = True
        ?epi:Initial{prop:FontColor} = 65793
        ?epi:Initial{prop:Color} = 15066597
    Elsif ?epi:Initial{prop:Req} = True
        ?epi:Initial{prop:FontColor} = 65793
        ?epi:Initial{prop:Color} = 8454143
    Else ! If ?epi:Initial{prop:Req} = True
        ?epi:Initial{prop:FontColor} = 65793
        ?epi:Initial{prop:Color} = 16777215
    End ! If ?epi:Initial{prop:Req} = True
    ?epi:Initial{prop:Trn} = 0
    ?epi:Initial{prop:FontStyle} = font:Bold
    ?EPS:LABEL6:Prompt{prop:FontColor} = -1
    ?EPS:LABEL6:Prompt{prop:Color} = 15066597
    If ?epi:Surname{prop:ReadOnly} = True
        ?epi:Surname{prop:FontColor} = 65793
        ?epi:Surname{prop:Color} = 15066597
    Elsif ?epi:Surname{prop:Req} = True
        ?epi:Surname{prop:FontColor} = 65793
        ?epi:Surname{prop:Color} = 8454143
    Else ! If ?epi:Surname{prop:Req} = True
        ?epi:Surname{prop:FontColor} = 65793
        ?epi:Surname{prop:Color} = 16777215
    End ! If ?epi:Surname{prop:Req} = True
    ?epi:Surname{prop:Trn} = 0
    ?epi:Surname{prop:FontStyle} = font:Bold
    ?EPS:LABEL7:Prompt{prop:FontColor} = -1
    ?EPS:LABEL7:Prompt{prop:Color} = 15066597
    If ?epi:Telephone_Collection{prop:ReadOnly} = True
        ?epi:Telephone_Collection{prop:FontColor} = 65793
        ?epi:Telephone_Collection{prop:Color} = 15066597
    Elsif ?epi:Telephone_Collection{prop:Req} = True
        ?epi:Telephone_Collection{prop:FontColor} = 65793
        ?epi:Telephone_Collection{prop:Color} = 8454143
    Else ! If ?epi:Telephone_Collection{prop:Req} = True
        ?epi:Telephone_Collection{prop:FontColor} = 65793
        ?epi:Telephone_Collection{prop:Color} = 16777215
    End ! If ?epi:Telephone_Collection{prop:Req} = True
    ?epi:Telephone_Collection{prop:Trn} = 0
    ?epi:Telephone_Collection{prop:FontStyle} = font:Bold
    ?EPI:ESN:Prompt{prop:FontColor} = -1
    ?EPI:ESN:Prompt{prop:Color} = 15066597
    If ?epi:ESN{prop:ReadOnly} = True
        ?epi:ESN{prop:FontColor} = 65793
        ?epi:ESN{prop:Color} = 15066597
    Elsif ?epi:ESN{prop:Req} = True
        ?epi:ESN{prop:FontColor} = 65793
        ?epi:ESN{prop:Color} = 8454143
    Else ! If ?epi:ESN{prop:Req} = True
        ?epi:ESN{prop:FontColor} = 65793
        ?epi:ESN{prop:Color} = 16777215
    End ! If ?epi:ESN{prop:Req} = True
    ?epi:ESN{prop:Trn} = 0
    ?epi:ESN{prop:FontStyle} = font:Bold
    ?EPS:LABEL9:Prompt{prop:FontColor} = -1
    ?EPS:LABEL9:Prompt{prop:Color} = 15066597
    If ?epi:Fault_Code1{prop:ReadOnly} = True
        ?epi:Fault_Code1{prop:FontColor} = 65793
        ?epi:Fault_Code1{prop:Color} = 15066597
    Elsif ?epi:Fault_Code1{prop:Req} = True
        ?epi:Fault_Code1{prop:FontColor} = 65793
        ?epi:Fault_Code1{prop:Color} = 8454143
    Else ! If ?epi:Fault_Code1{prop:Req} = True
        ?epi:Fault_Code1{prop:FontColor} = 65793
        ?epi:Fault_Code1{prop:Color} = 16777215
    End ! If ?epi:Fault_Code1{prop:Req} = True
    ?epi:Fault_Code1{prop:Trn} = 0
    ?epi:Fault_Code1{prop:FontStyle} = font:Bold
    ?EPS:LABEL10:Prompt{prop:FontColor} = -1
    ?EPS:LABEL10:Prompt{prop:Color} = 15066597
    If ?epi:Company_Name_Collection{prop:ReadOnly} = True
        ?epi:Company_Name_Collection{prop:FontColor} = 65793
        ?epi:Company_Name_Collection{prop:Color} = 15066597
    Elsif ?epi:Company_Name_Collection{prop:Req} = True
        ?epi:Company_Name_Collection{prop:FontColor} = 65793
        ?epi:Company_Name_Collection{prop:Color} = 8454143
    Else ! If ?epi:Company_Name_Collection{prop:Req} = True
        ?epi:Company_Name_Collection{prop:FontColor} = 65793
        ?epi:Company_Name_Collection{prop:Color} = 16777215
    End ! If ?epi:Company_Name_Collection{prop:Req} = True
    ?epi:Company_Name_Collection{prop:Trn} = 0
    ?epi:Company_Name_Collection{prop:FontStyle} = font:Bold
    ?EPS:LABEL11:Prompt{prop:FontColor} = -1
    ?EPS:LABEL11:Prompt{prop:Color} = 15066597
    If ?epi:Address_Line1_Collection{prop:ReadOnly} = True
        ?epi:Address_Line1_Collection{prop:FontColor} = 65793
        ?epi:Address_Line1_Collection{prop:Color} = 15066597
    Elsif ?epi:Address_Line1_Collection{prop:Req} = True
        ?epi:Address_Line1_Collection{prop:FontColor} = 65793
        ?epi:Address_Line1_Collection{prop:Color} = 8454143
    Else ! If ?epi:Address_Line1_Collection{prop:Req} = True
        ?epi:Address_Line1_Collection{prop:FontColor} = 65793
        ?epi:Address_Line1_Collection{prop:Color} = 16777215
    End ! If ?epi:Address_Line1_Collection{prop:Req} = True
    ?epi:Address_Line1_Collection{prop:Trn} = 0
    ?epi:Address_Line1_Collection{prop:FontStyle} = font:Bold
    If ?epi:Address_Line2_Collection{prop:ReadOnly} = True
        ?epi:Address_Line2_Collection{prop:FontColor} = 65793
        ?epi:Address_Line2_Collection{prop:Color} = 15066597
    Elsif ?epi:Address_Line2_Collection{prop:Req} = True
        ?epi:Address_Line2_Collection{prop:FontColor} = 65793
        ?epi:Address_Line2_Collection{prop:Color} = 8454143
    Else ! If ?epi:Address_Line2_Collection{prop:Req} = True
        ?epi:Address_Line2_Collection{prop:FontColor} = 65793
        ?epi:Address_Line2_Collection{prop:Color} = 16777215
    End ! If ?epi:Address_Line2_Collection{prop:Req} = True
    ?epi:Address_Line2_Collection{prop:Trn} = 0
    ?epi:Address_Line2_Collection{prop:FontStyle} = font:Bold
    If ?epi:Address_Line3_Collection{prop:ReadOnly} = True
        ?epi:Address_Line3_Collection{prop:FontColor} = 65793
        ?epi:Address_Line3_Collection{prop:Color} = 15066597
    Elsif ?epi:Address_Line3_Collection{prop:Req} = True
        ?epi:Address_Line3_Collection{prop:FontColor} = 65793
        ?epi:Address_Line3_Collection{prop:Color} = 8454143
    Else ! If ?epi:Address_Line3_Collection{prop:Req} = True
        ?epi:Address_Line3_Collection{prop:FontColor} = 65793
        ?epi:Address_Line3_Collection{prop:Color} = 16777215
    End ! If ?epi:Address_Line3_Collection{prop:Req} = True
    ?epi:Address_Line3_Collection{prop:Trn} = 0
    ?epi:Address_Line3_Collection{prop:FontStyle} = font:Bold
    ?Eps:LABEL14:Prompt{prop:FontColor} = -1
    ?Eps:LABEL14:Prompt{prop:Color} = 15066597
    If ?epi:Postcode_Collection{prop:ReadOnly} = True
        ?epi:Postcode_Collection{prop:FontColor} = 65793
        ?epi:Postcode_Collection{prop:Color} = 15066597
    Elsif ?epi:Postcode_Collection{prop:Req} = True
        ?epi:Postcode_Collection{prop:FontColor} = 65793
        ?epi:Postcode_Collection{prop:Color} = 8454143
    Else ! If ?epi:Postcode_Collection{prop:Req} = True
        ?epi:Postcode_Collection{prop:FontColor} = 65793
        ?epi:Postcode_Collection{prop:Color} = 16777215
    End ! If ?epi:Postcode_Collection{prop:Req} = True
    ?epi:Postcode_Collection{prop:Trn} = 0
    ?epi:Postcode_Collection{prop:FontStyle} = font:Bold
    ?EPS:LABEL15:Prompt{prop:FontColor} = -1
    ?EPS:LABEL15:Prompt{prop:Color} = 15066597
    If ?epi:Fault_Description{prop:ReadOnly} = True
        ?epi:Fault_Description{prop:FontColor} = 65793
        ?epi:Fault_Description{prop:Color} = 15066597
    Elsif ?epi:Fault_Description{prop:Req} = True
        ?epi:Fault_Description{prop:FontColor} = 65793
        ?epi:Fault_Description{prop:Color} = 8454143
    Else ! If ?epi:Fault_Description{prop:Req} = True
        ?epi:Fault_Description{prop:FontColor} = 65793
        ?epi:Fault_Description{prop:Color} = 16777215
    End ! If ?epi:Fault_Description{prop:Req} = True
    ?epi:Fault_Description{prop:Trn} = 0
    ?epi:Fault_Description{prop:FontStyle} = font:Bold
    ?EPI:Order_Number:Prompt{prop:FontColor} = -1
    ?EPI:Order_Number:Prompt{prop:Color} = 15066597
    If ?epi:Order_Number{prop:ReadOnly} = True
        ?epi:Order_Number{prop:FontColor} = 65793
        ?epi:Order_Number{prop:Color} = 15066597
    Elsif ?epi:Order_Number{prop:Req} = True
        ?epi:Order_Number{prop:FontColor} = 65793
        ?epi:Order_Number{prop:Color} = 8454143
    Else ! If ?epi:Order_Number{prop:Req} = True
        ?epi:Order_Number{prop:FontColor} = 65793
        ?epi:Order_Number{prop:Color} = 16777215
    End ! If ?epi:Order_Number{prop:Req} = True
    ?epi:Order_Number{prop:Trn} = 0
    ?epi:Order_Number{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateEPSCSV',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateEPSCSV',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateEPSCSV',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateEPSCSV',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EPS:LABEL1:Prompt;  SolaceCtrlName = '?EPS:LABEL1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Model_Number;  SolaceCtrlName = '?epi:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?invalid_postcode:prompt;  SolaceCtrlName = '?invalid_postcode:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Title;  SolaceCtrlName = '?epi:Title';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Initial;  SolaceCtrlName = '?epi:Initial';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EPS:LABEL6:Prompt;  SolaceCtrlName = '?EPS:LABEL6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Surname;  SolaceCtrlName = '?epi:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EPS:LABEL7:Prompt;  SolaceCtrlName = '?EPS:LABEL7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Telephone_Collection;  SolaceCtrlName = '?epi:Telephone_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Model;  SolaceCtrlName = '?Lookup_Model';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EPI:ESN:Prompt;  SolaceCtrlName = '?EPI:ESN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:ESN;  SolaceCtrlName = '?epi:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EPS:LABEL9:Prompt;  SolaceCtrlName = '?EPS:LABEL9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Fault_Code1;  SolaceCtrlName = '?epi:Fault_Code1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EPS:LABEL10:Prompt;  SolaceCtrlName = '?EPS:LABEL10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Company_Name_Collection;  SolaceCtrlName = '?epi:Company_Name_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?move_down;  SolaceCtrlName = '?move_down';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?move_down:2;  SolaceCtrlName = '?move_down:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EPS:LABEL11:Prompt;  SolaceCtrlName = '?EPS:LABEL11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Address_Line1_Collection;  SolaceCtrlName = '?epi:Address_Line1_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?move_down:3;  SolaceCtrlName = '?move_down:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Address_Line2_Collection;  SolaceCtrlName = '?epi:Address_Line2_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?move_down:4;  SolaceCtrlName = '?move_down:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Address_Line3_Collection;  SolaceCtrlName = '?epi:Address_Line3_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Eps:LABEL14:Prompt;  SolaceCtrlName = '?Eps:LABEL14:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Postcode_Collection;  SolaceCtrlName = '?epi:Postcode_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EPS:LABEL15:Prompt;  SolaceCtrlName = '?EPS:LABEL15:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Fault_Description;  SolaceCtrlName = '?epi:Fault_Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EPI:Order_Number:Prompt;  SolaceCtrlName = '?EPI:Order_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epi:Order_Number;  SolaceCtrlName = '?epi:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'dlkfjsdlfkj'
  OF ChangeRecord
    ActionMessage = 'Changing The Siemens Import File'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateEPSCSV')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateEPSCSV')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?EPS:LABEL1:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(epi:Record,History::epi:Record)
  SELF.AddHistoryField(?epi:Model_Number,2)
  SELF.AddHistoryField(?epi:Title,3)
  SELF.AddHistoryField(?epi:Initial,4)
  SELF.AddHistoryField(?epi:Surname,5)
  SELF.AddHistoryField(?epi:Telephone_Collection,6)
  SELF.AddHistoryField(?epi:ESN,7)
  SELF.AddHistoryField(?epi:Fault_Code1,8)
  SELF.AddHistoryField(?epi:Company_Name_Collection,9)
  SELF.AddHistoryField(?epi:Address_Line1_Collection,10)
  SELF.AddHistoryField(?epi:Address_Line2_Collection,11)
  SELF.AddHistoryField(?epi:Address_Line3_Collection,12)
  SELF.AddHistoryField(?epi:Postcode_Collection,13)
  SELF.AddHistoryField(?epi:Fault_Description,16)
  SELF.AddHistoryField(?epi:Order_Number,14)
  SELF.AddUpdateFile(Access:EPSIMP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EPSIMP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:EPSIMP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EPSIMP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateEPSCSV',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Lookup_Model
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Select_Model_Number
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Model, Accepted)
      case globalresponse
          of requestcompleted
              epi:model_number = mod:model_number  
              select(?+2)
          of requestcancelled
              epi:model_number = ''
              select(?-1)
      end!case globalreponse
      display(?epi:model_number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Model, Accepted)
    OF ?move_down
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?move_down, Accepted)
      If EPI:Address_Line1_Collection = ''
          EPI:Address_Line1_Collection = EPI:Company_Name_Collection
          EPI:Company_Name_Collection = ''
      Else
          beep(beep:systemhand)  ;  yield()
          message('Cannot move! There is already information in the address field.', |
                  'ServiceBase 2000', icon:hand)
      End
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?move_down, Accepted)
    OF ?move_down:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?move_down:2, Accepted)
      If EPI:Address_Line2_Collection = ''
          EPI:Address_Line2_Collection = EPI:Address_Line1_Collection
          EPI:Address_Line1_Collection = ''
      Else
          beep(beep:systemhand)  ;  yield()
          message('Cannot move! There is already information in the address field.', |
                  'ServiceBase 2000', icon:hand)
      End
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?move_down:2, Accepted)
    OF ?move_down:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?move_down:3, Accepted)
      If EPI:Address_Line3_Collection = ''
          EPI:Address_Line3_Collection = EPI:Address_Line2_Collection
          EPI:Address_Line2_Collection = ''
      Else
          beep(beep:systemhand)  ;  yield()
          message('Cannot move! There is already information in the address field.', |
                  'ServiceBase 2000', icon:hand)
      End
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?move_down:3, Accepted)
    OF ?move_down:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?move_down:4, Accepted)
      If EPI:Postcode_Collection = ''
          EPI:Postcode_Collection = EPI:Address_Line3_Collection
          EPI:Address_Line3_Collection = ''
      Else
          beep(beep:systemhand)  ;  yield()
          message('Cannot move! There is already information in the address field.', |
                  'ServiceBase 2000', icon:hand)
      End
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?move_down:4, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateEPSCSV')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      if epi:passed <> 'YES'
          Unhide(?invalid_postcode:prompt)
          Case epi:passed
              Of 'POS'
                  ?invalid_postcode:prompt{prop:text} = 'Invalid Postcode'
              Of 'MOD'
                  ?invalid_postcode:prompt{prop:text} = 'Invalid Model Number'
              Of 'MPS'
                  ?invalid_postcode:prompt{prop:text} = 'Invalid Model Number And Postcode'
      
          End!Case edi:passed
      Else
          Hide(?invalid_postcode:prompt)
      End
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

