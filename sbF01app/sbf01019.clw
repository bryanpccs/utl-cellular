

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01019.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Minimum_Stock PROCEDURE                        !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
pos                  STRING(255)
save_sto_id          USHORT,AUTO
FilesOpened          BYTE
Location_Temp        STRING(30)
save_ope_id          USHORT,AUTO
save_orp_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Location_Temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Supplier)
                       PROJECT(sto:Minimum_Level)
                       PROJECT(sto:Quantity_Stock)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Minimum_Stock)
                       PROJECT(sto:Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Supplier           LIKE(sto:Supplier)             !List box control field - type derived from field
sto:Minimum_Level      LIKE(sto:Minimum_Level)        !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Primary key field - type derived from field
sto:Minimum_Stock      LIKE(sto:Minimum_Stock)        !Browse key field - type derived from field
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Minimum Stock File'),AT(,,536,328),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Minimum_Stock'),SYSTEM,GRAY,DOUBLE
                       PANEL,AT(4,4,444,20),USE(?Panel1),FILL(COLOR:Silver)
                       PROMPT('Location'),AT(8,8),USE(?Prompt1),TRN
                       COMBO(@s30),AT(52,8,124,10),USE(Location_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       LIST,AT(8,60,436,260),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('105L(2)|M~Part Number~@s30@123L(2)|M~Description~@s30@106L(2)|M~Supplier~@s30@41' &|
   'R(2)|M~Min Level~@n8@50D(2)|M~Qty In Stock~L@N8@'),FROM(Queue:Browse:1)
                       BUTTON('&Process'),AT(456,32,76,20),USE(?Change),LEFT,ICON('arrow.ico')
                       BUTTON('&Process All'),AT(456,64,76,20),USE(?ProcessAll),LEFT,ICON('arrow.ico')
                       SHEET,AT(4,28,444,296),USE(?CurrentTab),SPREAD
                         TAB('By Part Number'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,44,124,10),USE(sto:Part_Number),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Description'),USE(?Tab2)
                           ENTRY(@s30),AT(8,44,124,10),USE(sto:Description),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('Close'),AT(456,304,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass CLASS(StepStringClass)          !Default Step Manager
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

BRW1::Sort1:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 2
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1{prop:Fill} = 15066597

    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?Location_Temp{prop:ReadOnly} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 15066597
    Elsif ?Location_Temp{prop:Req} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 8454143
    Else ! If ?Location_Temp{prop:Req} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 16777215
    End ! If ?Location_Temp{prop:Req} = True
    ?Location_Temp{prop:Trn} = 0
    ?Location_Temp{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?sto:Part_Number{prop:ReadOnly} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 15066597
    Elsif ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 8454143
    Else ! If ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 16777215
    End ! If ?sto:Part_Number{prop:Req} = True
    ?sto:Part_Number{prop:Trn} = 0
    ?sto:Part_Number{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?sto:Description{prop:ReadOnly} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 15066597
    Elsif ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 8454143
    Else ! If ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 16777215
    End ! If ?sto:Description{prop:Req} = True
    ?sto:Description{prop:Trn} = 0
    ?sto:Description{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Minimum_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Minimum_Stock',1)
    SolaceViewVars('pos',pos,'Browse_Minimum_Stock',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Browse_Minimum_Stock',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Minimum_Stock',1)
    SolaceViewVars('Location_Temp',Location_Temp,'Browse_Minimum_Stock',1)
    SolaceViewVars('save_ope_id',save_ope_id,'Browse_Minimum_Stock',1)
    SolaceViewVars('save_orp_id',save_orp_id,'Browse_Minimum_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_Temp;  SolaceCtrlName = '?Location_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProcessAll;  SolaceCtrlName = '?ProcessAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Part_Number;  SolaceCtrlName = '?sto:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Description;  SolaceCtrlName = '?sto:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Minimum_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Minimum_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  !Work Out The Minimum Stock
  recordspercycle     = 25
  recordsprocessed    = 0
  percentprogress     = 0
  setcursor(cursor:wait)
  open(progresswindow)
  progress:thermometer    = 0
  ?progress:pcttext{prop:text} = '0% Completed'
  
  recordstoprocess    = Records(Stock)
  
  setcursor(cursor:wait)
  
  save_sto_id = access:stock.savefile()
  set(sto:ref_number_key)
  loop
      if access:stock.next()
         break
      end !if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      Do getnextrecord2
  
      If sto:sundry_item = 'YES'
          Cycle
      End!If sto:sundry_item = 'YES'
  
      sto:minimum_stock = 'NO'
      STO:Quantity_To_Order = 0                                                               !Go throught the pending order
      setcursor(cursor:wait)                                                                  !file. And work out the quantity
      save_ope_id = access:ordpend.savefile()                                                 !awaiting order
      access:ordpend.clearkey(ope:part_ref_number_key)
      ope:part_ref_number =  sto:ref_number
      set(ope:part_ref_number_key,ope:part_ref_number_key)
      loop
          if access:ordpend.next()
             break
          end !if
          if ope:part_ref_number <> sto:ref_number      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          sto:quantity_to_order += ope:quantity
      end !loop
      access:ordpend.restorefile(save_ope_id)
      setcursor()
  
  
      sto:quantity_on_order = 0                                                               !Go throught the order parts
      setcursor(cursor:wait)                                                                  !file. But check with the order
      save_orp_id = access:ordparts.savefile()                                                !file first to see if the whole
      access:ordparts.clearkey(orp:ref_number_key)                                            !order has been received. This
      orp:part_ref_number = sto:ref_number                                                    !should hopefully speed things
      set(orp:ref_number_key,orp:ref_number_key)                                              !up.
      loop
          if access:ordparts.next()
             break
          end !if
          if orp:part_ref_number <> sto:ref_number      |
              then break.  ! end if
          access:orders.clearkey(ord:order_number_key)
          ord:order_number = orp:order_number
          if access:orders.fetch(ord:order_number_key) = Level:Benign
              If ord:all_received <> 'YES'
                  If orp:all_received <> 'YES'
                      sto:quantity_on_order += orp:quantity
                  End!If orp:all_received <> 'YES'
              End!If ord:all_received <> 'YES'
          end!if access:orders.fetch(ord:order_number_key) = Level:Benign
      end !loop
      access:ordparts.restorefile(save_orp_id)
      setcursor()
  
  
      If (sto:quantity_stock + sto:quantity_to_order + sto:quantity_on_order) < sto:minimum_level
          sto:minimum_stock = 'YES'
      End!If (sto:quantity_stock + sto:quantity_to_order + sto:quantity_on_order) < sto:minimum_level
      access:stock.update()
  end !loop
  access:stock.restorefile(save_sto_id)
  setcursor()
  
  Set(defstock)
  access:defstock.next()
  If dst:use_site_location = 'YES'
      location_temp = dst:site_location
      Display(?location_temp)
  End
  
  setcursor()
  close(progresswindow)
  
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,sto:Minimum_Description_Key)
  BRW1.AddRange(sto:Location,Location_Temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sto:Minimum_Part_Number_Key)
  BRW1.AddRange(sto:Location,Location_Temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Supplier,BRW1.Q.sto:Supplier)
  BRW1.AddField(sto:Minimum_Level,BRW1.Q.sto:Minimum_Level)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Minimum_Stock,BRW1.Q.sto:Minimum_Stock)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB5.Init(Location_Temp,?Location_Temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(loc:Location_Key)
  FDCB5.AddField(loc:Location,FDCB5.Q.loc:Location)
  FDCB5.AddField(loc:RecordNumber,FDCB5.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Part Number'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='105L(2)|M~Part Number~@s30@#1#123L(2)|M~Description~@s30@#2#106L(2)|M~Supplier~@s30@#3#41R(2)|M~Min Level~@n8@#4#50D(2)|M~Qty In Stock~L@N8@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Minimum_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Process_Minimum_Stock
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Location_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, Accepted)
    OF ?Change
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
    OF ?ProcessAll
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessAll, Accepted)
      Case MessageEx('If you continue all the parts displayed in this Minimum Stock Browse will be ordered.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              x# =  MessageEx('Do you want to create orders for the selected location of '&Clip(location_temp)&' or for ALL locations?','ServiceBase 2000',|
                             'Styles\question.ico','|Location: '&CLip(location_temp)&'|&All Locations|&Cancel',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
              If x# <> 3
                  recordspercycle     = 25
                  recordsprocessed    = 0
                  percentprogress     = 0
                  setcursor(cursor:wait)
                  open(progresswindow)
                  progress:thermometer    = 0
                  ?progress:pcttext{prop:text} = '0% Completed'
      
                  recordstoprocess    = Records(sto:minimum_part_number_key)
      
                  save_sto_id = access:stock.savefile()
                  access:stock.clearkey(sto:minimum_part_number_key)
                  sto:minimum_stock = 'YES'
                  If x# = 1
                      sto:location      = location_temp
                  End!If x# = 1
                  set(sto:minimum_part_number_key,sto:minimum_part_number_key)
                  loop
                      if access:stock.next()
                         break
                      end !if
                      if sto:minimum_stock <> 'YES'      |
                          then break.  ! end if
                      If x# = 1
                          If sto:location      <> location_temp
                              Break
                          End!If sto:location      <> location_temp
                      End!If x# = 1
                      Do GetNextRecord2
                      If sto:reorder_level - sto:quantity_stock >0
                          get(ordpend,0)
                          if access:ordpend.primerecord() = level:benign
                              ope:part_ref_number = sto:ref_number
                              ope:part_type       = 'STO'
                              ope:supplier        = sto:supplier
                              ope:part_number     = sto:part_number
                              ope:description     = sto:description
                              ope:quantity        = STO:Reorder_Level - STO:Quantity_Stock
                              access:ordpend.insert()
                          end!if access:ordpend.primerecord() = level:benign
                      End!If sto:reorder_level - sto:quantity_stock >0
                      sto:pending_ref_number = ope:ref_number
                      pos = Position(sto:minimum_part_number_key)
                      sto:minimum_stock = 'NO'
                      access:stock.update()
                      Reset(sto:minimum_part_number_key,pos)
      
                  end !loop
                  access:stock.restorefile(save_sto_id)
                  setcursor()
                  close(progresswindow)
                  Case MessageEx('Process Completed.','ServiceBase 2000',|
                                 'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
      
              End!If x# = 3
      
      Of 2 ! &No Button
      End!Case MessageEx
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessAll, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Minimum_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='105L(2)|M~Part Number~@s30@#1#123L(2)|M~Description~@s30@#2#106L(2)|M~Supplier~@s30@#3#41R(2)|M~Min Level~@n8@#4#50D(2)|M~Qty In Stock~L@N8@#5#'
          ?Tab:2{PROP:TEXT} = 'By Part Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='123L(2)|M~Description~@s30@#2#105L(2)|M~Part Number~@s30@#1#106L(2)|M~Supplier~@s30@#3#41R(2)|M~Min Level~@n8@#4#50D(2)|M~Qty In Stock~L@N8@#5#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sto:Part_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Part_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Part_Number, Selected)
    OF ?sto:Description
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Description, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Description, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1::Sort0:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Method Executable Code Section) ARG(1, , Init, (BYTE Controls,BYTE Mode))
  sto:minimum_stock = 'YES'
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Method Executable Code Section) ARG(1, , Init, (BYTE Controls,BYTE Mode))


BRW1::Sort1:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 1, Init, (BYTE Controls,BYTE Mode))
  sto:minimum_stock = 'YES'
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 1, Init, (BYTE Controls,BYTE Mode))


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

