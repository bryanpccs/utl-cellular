

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01043.INC'),ONCE        !Local module procedure declarations
                     END


ANC_Loan_Return PROCEDURE                             !Generated from procedure template - Window

job_number_temp      LONG
sav:path             STRING(255)
esn_temp             STRING(16)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Loan Return'),AT(,,220,83),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,48),USE(?Sheet1),SPREAD
                         TAB('Loan Return'),USE(?Tab1)
                           PROMPT('Job Number'),AT(8,20),USE(?job_number_temp:Prompt)
                           ENTRY(@s8),AT(84,20,64,10),USE(job_number_temp),LEFT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('E.S.N.'),AT(8,36),USE(?esn_temp:Prompt)
                           ENTRY(@s16),AT(84,36,124,10),USE(esn_temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(4,56,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Finis&h'),AT(156,60,56,16),USE(?Cancel),LEFT,ICON('thumbs.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?job_number_temp:Prompt{prop:FontColor} = -1
    ?job_number_temp:Prompt{prop:Color} = 15066597
    If ?job_number_temp{prop:ReadOnly} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 15066597
    Elsif ?job_number_temp{prop:Req} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 8454143
    Else ! If ?job_number_temp{prop:Req} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 16777215
    End ! If ?job_number_temp{prop:Req} = True
    ?job_number_temp{prop:Trn} = 0
    ?job_number_temp{prop:FontStyle} = font:Bold
    ?esn_temp:Prompt{prop:FontColor} = -1
    ?esn_temp:Prompt{prop:Color} = 15066597
    If ?esn_temp{prop:ReadOnly} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 15066597
    Elsif ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 8454143
    Else ! If ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 16777215
    End ! If ?esn_temp{prop:Req} = True
    ?esn_temp{prop:Trn} = 0
    ?esn_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ANC_Loan_Return',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('job_number_temp',job_number_temp,'ANC_Loan_Return',1)
    SolaceViewVars('sav:path',sav:path,'ANC_Loan_Return',1)
    SolaceViewVars('esn_temp',esn_temp,'ANC_Loan_Return',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_number_temp:Prompt;  SolaceCtrlName = '?job_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_number_temp;  SolaceCtrlName = '?job_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp:Prompt;  SolaceCtrlName = '?esn_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp;  SolaceCtrlName = '?esn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ANC_Loan_Return')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ANC_Loan_Return')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?job_number_temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:COURIER.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ANC_Loan_Return',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?esn_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn_temp, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = job_number_temp
      if access:jobs.tryfetch(job:ref_number_key)
          Case MessageEx('Cannot find selected Job Number.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
              Of 1 ! &OK Button
          End!Case MessageEx
          Select(?job_number_temp)
      Else!if access:jobs.tryfetch(job:ref_number_key)
          If esn_temp <> job:esn
              Case MessageEx('The selected E.S.N. / I.M.E.I. does not match the selected Job Number.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Select(?esn_temp)
          Else!If esn_temp <> job:esn
              If job:loan_unit_Number = ''
                  Case MessageEx('The selected Job has not had a Loan Unit Issued to it.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  esn_temp = ''
                  Select(?job_number_temp)
              Else!If job:loan_unit_Number <> ''
                  If job:date_completed = ''
                      Case MessageEx('The selected Job has not been competed.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      esn_temp = ''
                      Select(?job_number_temp)
      
                  Else!If job:date_completed = ''
                      job:despatched = 'REA'
                      job:despatch_type = 'JOB'
                      job:current_courier = job:courier
                      access:courier.clearkey(cou:courier_key)
                      cou:courier = job:courier
                      if access:courier.tryfetch(cou:courier_key) = Level:benign
                          glo:file_name = 'C:\ANC.TXT'
                          Remove(expgen)
                          If access:expgen.open()
                              Stop(error())
                              Return Level:Benign
                          End!If access:expgen.open()
                          access:expgen.usefile()
                          Clear(gen:record)
                          gen:line1   = job:ref_number
                          gen:line1   = Sub(gen:line1,1,10) & job:company_name
                          gen:line1   = Sub(gen:line1,1,40) & job:address_line1_delivery
                          gen:line1   = Sub(gen:line1,1,70) & job:address_line2_delivery
                          gen:line1   = Sub(gen:line1,1,100) & job:address_line3_delivery
                          gen:line1   = Sub(gen:line1,1,130) & job:postcode_delivery
                          gen:line1   = Sub(gen:line1,1,150) & job:telephone_delivery
                          gen:line1   = Sub(gen:line1,1,175) & job:unit_type
                          gen:line1   = Sub(gen:line1,1,195) & job:model_number
                          gen:line1   = Sub(gen:line1,1,215) & cou:ContractNumber
                          access:expgen.insert()
                          Close(expgen)
                          access:expgen.close()
                          setcursor(cursor:wait)
                          sav:path    = path()
                          setpath(Clip(cou:ancpath))
                          Run('ancpaper.exe',1)
                          Setpath(sav:path)
                          setcursor()
                          cou:anccount += 1
                          If cou:ANCCount = 999
                              cou:ANCCount = 1
                          End
                          access:courier.update()
                          Set(Defaults)
                          access:defaults.next()
      
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:notes         = 'ORIGINAL COLLECTION DETAILS:- ' & |
                                                  '<13,10>NOTE NUMBER: ' & Clip(job:incoming_consignment_number) &|
                                                  '<13,10>COURIER: ' & Clip(job:incoming_courier) &|
                                                  '<13,10>DATE: ' & Format(job:incoming_date,@d6) &|
                                                  '<13,10,13,10>LOAN RETURN DETAILS:- ' &|
                                                  '<13,10>NOTE NUMBER: ' & Clip(def:anccollectionno) &|
                                                  '<13,10>COURIER: ' & Clip(job:courier) &|
                                                  '<13,10>DATE: ' & Format(Today(),@d6)
      
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'ANC COLLECTION NOTE PRINTED: LOAN RETURNED'
                              aud:type          = 'LOA'
                              access:audit.insert()
                          end!�if access:audit.primerecord() = level:benign
                          job:incoming_consignment_number = def:anccollectionno
                          job:Incoming_courier = job:courier
                          job:incoming_date   = Today()
      
                          DEF:ANCCollectionNo += 10
                          access:defaults.update()
      
                          access:jobs.update()
      
      
                          job_number_temp = ''
                          esn_temp = ''
                          Select(?job_number_temp)
                      end!if access:courier.tryfetch(cou:courier_key) = Level:benign
                  End!If job:date_completed = ''
              End!If job:loan_unit_Number <> ''
          End!If esn_temp <> job:esn
      End!if access:jobs.tryfetch(job:ref_number_key)
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn_temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ANC_Loan_Return')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

