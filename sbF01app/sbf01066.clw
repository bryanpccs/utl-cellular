

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01066.INC'),ONCE        !Local module procedure declarations
                     END



Make_Estimate_Job PROCEDURE                           !Generated from procedure template - Window

tmpJobNumber         LONG
ChargeType_temp      STRING(30)
RepairType_temp      STRING(30)
DefaultRepairType    STRING(30)
DefaultChargeType    STRING(30)
RepairType_Valid     BYTE(0)
MaxTabs              BYTE(2)
TabNumber            BYTE(1)
save_epr_id          USHORT
save_wpr_id          USHORT
save_sto_id          USHORT
save_shi_id          USHORT
save_use_id          USHORT
epQueue              QUEUE,PRE(epq)
record_number        REAL
Ref_Number           REAL
Adjustment           STRING(3)
Part_Ref_Number      REAL
Part_Number          STRING(30)
Description          STRING(30)
Supplier             STRING(30)
Purchase_Cost        REAL
Sale_Cost            REAL
Retail_Cost          REAL
Quantity             REAL
Warranty_Part        STRING(3)
Exclude_From_Order   STRING(3)
Despatch_Note_Number STRING(30)
Date_Ordered         DATE
Pending_Ref_Number   REAL
Order_Number         REAL
Date_Received        DATE
Order_Part_Number    REAL
Status_Date          DATE
Fault_Code1          STRING(30)
Fault_Code2          STRING(30)
Fault_Code3          STRING(30)
Fault_Code4          STRING(30)
Fault_Code5          STRING(30)
Fault_Code6          STRING(30)
Fault_Code7          STRING(30)
Fault_Code8          STRING(30)
Fault_Code9          STRING(30)
Fault_Code10         STRING(30)
Fault_Code11         STRING(30)
Fault_Code12         STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW4::View:Browse    VIEW(ESTPARTS)
                       PROJECT(epr:Part_Number)
                       PROJECT(epr:Description)
                       PROJECT(epr:Quantity)
                       PROJECT(epr:record_number)
                       PROJECT(epr:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
epr:Part_Number        LIKE(epr:Part_Number)          !List box control field - type derived from field
epr:Description        LIKE(epr:Description)          !List box control field - type derived from field
epr:Quantity           LIKE(epr:Quantity)             !List box control field - type derived from field
epr:record_number      LIKE(epr:record_number)        !Primary key field - type derived from field
epr:Ref_Number         LIKE(epr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
mo:SelectedTab  Long  ! makeover template ! LocalTreat = Wizard ; PT = Window  WT = Wizard
window               WINDOW('Make Estimate Job Wizard'),AT(,,425,212),FONT('Arial',8,,),IMM,GRAY,DOUBLE
                       SHEET,AT(4,4,416,176),USE(?Sheet1)
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('This function will make the job a chargeable estimate job.'),AT(40,37),USE(?Prompt1),FONT(,10,,)
                           PROMPT('Any warranty element will be removed.  Any warranty parts will'),AT(40,48),USE(?Prompt2),FONT(,10,,)
                           PROMPT('be restocked to their original location.'),AT(40,59),USE(?Prompt3),FONT(,10,,)
                           PROMPT('Chargeable Repair Type:'),AT(47,116),USE(?RepairTypePrompt),HIDE
                           ENTRY(@s30),AT(132,96,124,10),USE(ChargeType_temp),UPR
                           BUTTON,AT(260,96,10,10),USE(?LookupChargeType),SKIP,LEFT,ICON('list3.ico')
                           PROMPT('Chargeable Charge Type:'),AT(44,96),USE(?Prompt5)
                           ENTRY(@s30),AT(132,116,124,10),USE(RepairType_temp),HIDE,UPR
                           BUTTON,AT(260,116,10,10),USE(?LookupRepairType),SKIP,TRN,HIDE,LEFT,ICON('list3.ico')
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           LIST,AT(8,20,344,156),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('125L|M~Part Number~L(2)@s30@124L|M~Description~L(2)@s30@32L|M~Quantity~L(2)@n4@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(360,120,56,16),USE(?Insert),LEFT,ICON('insert.ico')
                           BUTTON('&Change'),AT(360,140,56,16),USE(?Change),LEFT,ICON('edit.ico')
                           BUTTON('&Delete'),AT(360,160,56,16),USE(?Delete),LEFT,ICON('delete.ico')
                         END
                       END
                       PANEL,AT(4,184,416,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,188,56,16),USE(?BackButton),LEFT,ICON(ICON:VCRrewind)
                       BUTTON('&Next'),AT(64,188,56,16),USE(?NextButton),LEFT,ICON(ICON:VCRfastforward)
                       BUTTON('&Finish'),AT(120,188,56,16),USE(?FinishButton),LEFT,ICON('thumbs.gif')
                       BUTTON('&Cancel'),AT(360,188,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
ValidateRepairType ROUTINE
    RepairType_Valid = 0
    Access:REPAIRTY.ClearKey(rep:Model_Chargeable_Key)
    rep:Model_Number = job:Model_Number
    rep:Chargeable   = 'YES'
    rep:Repair_Type  = RepairType_temp
    IF (Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign) THEN
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = job:Account_Number
        IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign) THEN
            CheckHeadTrade# = 0
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign) THEN
                IF (tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES') THEN
                    Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                    suc:Account_Number = job:Account_Number
                    suc:Model_Number   = job:Model_Number
                    suc:Charge_Type    = ChargeType_temp
                    suc:Unit_Type      = job:Unit_Type
                    suc:Repair_Type    = RepairType_temp
                    IF (Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign) THEN
                        RepairType_Valid = 1
                    ELSE
                        CheckHeadTrade# = 1
                    END
                ELSE
                    CheckHeadTrade# = 1
                END
                IF (CheckHeadTrade# = 1) THEN
                    Access:TRACHRGE.ClearKey(trc:Account_Charge_Key)
                    trc:Account_Number = tra:Account_Number
                    trc:Model_Number   = job:Model_Number
                    trc:Charge_Type    = ChargeType_temp
                    trc:Unit_Type      = job:Unit_Type
                    trc:Repair_Type    = RepairType_temp
                    IF (Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign) THEN
                        RepairType_Valid = 1
                    ELSE
                        Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
                        sta:Model_Number = job:Model_Number
                        sta:Charge_Type  = ChargeType_temp
                        sta:Unit_Type    = job:Unit_Type
                        sta:Repair_Type  = RepairType_temp
                        IF (Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign) THEN
                            RepairType_Valid = 1
                        END
                    END
                END
            END
        END
    END


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Make_Estimate_Job',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmpJobNumber',tmpJobNumber,'Make_Estimate_Job',1)
    SolaceViewVars('ChargeType_temp',ChargeType_temp,'Make_Estimate_Job',1)
    SolaceViewVars('RepairType_temp',RepairType_temp,'Make_Estimate_Job',1)
    SolaceViewVars('DefaultRepairType',DefaultRepairType,'Make_Estimate_Job',1)
    SolaceViewVars('DefaultChargeType',DefaultChargeType,'Make_Estimate_Job',1)
    SolaceViewVars('RepairType_Valid',RepairType_Valid,'Make_Estimate_Job',1)
    SolaceViewVars('MaxTabs',MaxTabs,'Make_Estimate_Job',1)
    SolaceViewVars('TabNumber',TabNumber,'Make_Estimate_Job',1)
    SolaceViewVars('save_epr_id',save_epr_id,'Make_Estimate_Job',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Make_Estimate_Job',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Make_Estimate_Job',1)
    SolaceViewVars('save_shi_id',save_shi_id,'Make_Estimate_Job',1)
    SolaceViewVars('save_use_id',save_use_id,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:record_number',epQueue:record_number,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Ref_Number',epQueue:Ref_Number,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Adjustment',epQueue:Adjustment,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Part_Ref_Number',epQueue:Part_Ref_Number,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Part_Number',epQueue:Part_Number,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Description',epQueue:Description,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Supplier',epQueue:Supplier,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Purchase_Cost',epQueue:Purchase_Cost,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Sale_Cost',epQueue:Sale_Cost,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Retail_Cost',epQueue:Retail_Cost,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Quantity',epQueue:Quantity,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Warranty_Part',epQueue:Warranty_Part,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Exclude_From_Order',epQueue:Exclude_From_Order,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Despatch_Note_Number',epQueue:Despatch_Note_Number,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Date_Ordered',epQueue:Date_Ordered,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Pending_Ref_Number',epQueue:Pending_Ref_Number,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Order_Number',epQueue:Order_Number,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Date_Received',epQueue:Date_Received,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Order_Part_Number',epQueue:Order_Part_Number,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Status_Date',epQueue:Status_Date,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code1',epQueue:Fault_Code1,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code2',epQueue:Fault_Code2,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code3',epQueue:Fault_Code3,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code4',epQueue:Fault_Code4,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code5',epQueue:Fault_Code5,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code6',epQueue:Fault_Code6,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code7',epQueue:Fault_Code7,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code8',epQueue:Fault_Code8,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code9',epQueue:Fault_Code9,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code10',epQueue:Fault_Code10,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code11',epQueue:Fault_Code11,'Make_Estimate_Job',1)
    SolaceViewVars('epQueue:Fault_Code12',epQueue:Fault_Code12,'Make_Estimate_Job',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RepairTypePrompt;  SolaceCtrlName = '?RepairTypePrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChargeType_temp;  SolaceCtrlName = '?ChargeType_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupChargeType;  SolaceCtrlName = '?LookupChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RepairType_temp;  SolaceCtrlName = '?RepairType_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupRepairType;  SolaceCtrlName = '?LookupRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BackButton;  SolaceCtrlName = '?BackButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?NextButton;  SolaceCtrlName = '?NextButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FinishButton;  SolaceCtrlName = '?FinishButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Make_Estimate_Job')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Make_Estimate_Job')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmpJobNumber = job:Ref_Number
  Relate:CHARTYPE.Open
  Access:JOBS.UseFile
  Access:REPAIRTY.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:WARPARTS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:ESTPARTS,SELF)
  OPEN(window)
  SELF.Opened=True
    Loop DBHControl# = Firstfield() To LastField()
        If DBHControl#{prop:type} = Create:OPTION Or |
            DBHControl#{prop:type} = Create:RADIO Or |
            DBhControl#{prop:type} = Create:GROUP
            DBHControl#{prop:trn} = 1      
        End!DBhControl#{prop:type} = 'GROUP'
    End!Loop DBHControl# = Firstfield() To LastField()
  ?List{prop:vcr} = TRUE
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,epr:Part_Number_Key)
  BRW4.AddRange(epr:Ref_Number,tmpJobNumber)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,epr:Part_Number,1,BRW4)
  BRW4.AddField(epr:Part_Number,BRW4.Q.epr:Part_Number)
  BRW4.AddField(epr:Description,BRW4.Q.epr:Description)
  BRW4.AddField(epr:Quantity,BRW4.Q.epr:Quantity)
  BRW4.AddField(epr:record_number,BRW4.Q.epr:record_number)
  BRW4.AddField(epr:Ref_Number,BRW4.Q.epr:Ref_Number)
  HIDE(?FinishButton)
  SELECT(?Sheet1, TabNumber)
  DISABLE(?BackButton)
  
  !
  ! Save State of ESTPARTS File in case it needs to be restored in event of CANCEL
  !
  
  Clear(epQueue)
  Free(epQueue)
  
  save_epr_id = Access:ESTPARTS.SaveFile()
  Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
  epr:Ref_Number = job:Ref_Number
  SET(epr:Part_Number_Key, epr:Part_Number_Key)
  LOOP
      IF ((Access:ESTPARTS.NEXT() <> Level:Benign) OR (epr:Ref_Number <> job:Ref_Number)) THEN
           BREAK
      END
  
      epq:record_number        = epr:record_number
      epq:Ref_Number           = epr:Ref_Number
      epq:Adjustment           = epr:Adjustment
      epq:Part_Ref_Number      = epr:Part_Ref_Number
      epq:Part_Number          = epr:Part_Number
      epq:Description          = epr:Description
      epq:Supplier             = epr:Supplier
      epq:Purchase_Cost        = epr:Purchase_Cost
      epq:Sale_Cost            = epr:Sale_Cost
      epq:Retail_Cost          = epr:Retail_Cost
      epq:Quantity             = epr:Quantity
      epq:Warranty_Part        = epr:Warranty_Part
      epq:Exclude_From_Order   = epr:Exclude_From_Order
      epq:Despatch_Note_Number = epr:Despatch_Note_Number
      epq:Date_Ordered         = epr:Date_Ordered
      epq:Pending_Ref_Number   = epr:Pending_Ref_Number
      epq:Order_Number         = epr:Order_Number
      epq:Date_Received        = epr:Date_Received
      epq:Order_Part_Number    = epr:Order_Part_Number
      epq:Status_Date          = epr:Status_Date
      epq:Fault_Code1          = epr:Fault_Code1
      epq:Fault_Code2          = epr:Fault_Code2
      epq:Fault_Code3          = epr:Fault_Code3
      epq:Fault_Code4          = epr:Fault_Code4
      epq:Fault_Code5          = epr:Fault_Code5
      epq:Fault_Code6          = epr:Fault_Code6
      epq:Fault_Code7          = epr:Fault_Code7
      epq:Fault_Code8          = epr:Fault_Code8
      epq:Fault_Code9          = epr:Fault_Code9
      epq:Fault_Code10         = epr:Fault_Code10
      epq:Fault_Code11         = epr:Fault_Code11
      epq:Fault_Code12         = epr:Fault_Code12
  
      ADD(epQueue)
  END
  
  Access:ESTPARTS.RestoreFile(save_epr_id)
  ThisMakeover.SetWindow(Win:Wizard)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,?Sheet1)
  ?LookupChargeType{prop:flat} = 1        
  ?LookupChargeType{Prop:trn} = 1 
  ?LookupRepairType{prop:flat} = 1        
  ?LookupRepairType{Prop:trn} = 1 
      
  
  BRW4.AskProcedure = 1
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Make_Estimate_Job',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Estimate_Part
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?BackButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BackButton, Accepted)
      TabNumber -= 1
      CASE TabNumber
          OF 1
              HIDE(?FinishButton)
              DISABLE(?BackButton)
              ENABLE(?NextButton)
              SELECT(?Sheet1, TabNumber)
          OF MaxTabs
              UNHIDE(?FinishButton)
              ENABLE(?BackButton)
              DISABLE(?NextButton)
              SELECT(?Sheet1, TabNumber)
          ELSE
              HIDE(?FinishButton)
              ENABLE(?BackButton)
              ENABLE(?NextButton)
              SELECT(?Sheet1, TabNumber)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BackButton, Accepted)
    OF ?NextButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NextButton, Accepted)
      TabNumber += 1
      CASE TabNumber
          OF 1
              HIDE(?FinishButton)
              DISABLE(?BackButton)
              ENABLE(?NextButton)
              SELECT(?Sheet1, TabNumber)
          OF MaxTabs
              IF (CLIP(ChargeType_temp) = '') THEN
                  MessageEx('Error! Charge Type is Invalid','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand, |
                                 msgex:samewidths,84,26,0)
                  TabNumber -= 1
              ELSIF ((?RepairType_temp{PROP:HIDE} = 0) AND (CLIP(RepairType_temp) = '')) THEN
                  MessageEx('Error! Repair Type is Invalid','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand, |
                                 msgex:samewidths,84,26,0)
                  TabNumber -= 1
              ELSE
                  UNHIDE(?FinishButton)
                  ENABLE(?BackButton)
                  DISABLE(?NextButton)
                  SELECT(?Sheet1, TabNumber)
                  BRW4.ResetSort(1)
              END
          ELSE
              HIDE(?FinishButton)
              ENABLE(?BackButton)
              ENABLE(?NextButton)
              SELECT(?Sheet1, TabNumber)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NextButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ChargeType_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChargeType_temp, Accepted)
      ChargeTypeOk# = 1
      IF (CLIP(ChargeType_temp) <> '') THEN
          Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
          cha:Charge_Type  = ChargeType_temp
          IF (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) <> Level:Benign) THEN
               MessageEx('Error! Charge Type is Invalid','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand, |
                             msgex:samewidths,84,26,0)
              Post(Event:Accepted,?LookupChargeType)
              ChargeTypeOk# = 0
          END
      END
      IF (ChargeTypeOk# = 1) THEN
          IF (PricingStructure(job:Account_Number, job:Model_Number, job:Unit_Type, ChargeType_temp, RepairType_temp)) THEN
              HIDE(?RepairTypePrompt)
              HIDE(?RepairType_temp)
              HIDE(?LookupRepairType)
          ELSE
              UNHIDE(?RepairTypePrompt)
              UNHIDE(?RepairType_temp)
              UNHIDE(?LookupRepairType)
              DO ValidateRepairType
              IF (RepairType_Valid = 0) THEN
                  RepairType_temp = ''
              END
          END
      END
      DISPLAY(?ChargeType_temp)
      DISPLAY(?RepairType_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChargeType_temp, Accepted)
    OF ?LookupChargeType
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickChargeableChargeTypes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupChargeType, Accepted)
      IF (globalresponse = requestcompleted) THEN
          ChargeType_temp = cha:charge_type
      ELSE
          IF (CLIP(ChargeType_temp) <> '') THEN
              Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
              cha:Charge_Type  = ChargeType_temp
              IF (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) <> Level:Benign) THEN
                  ChargeType_temp = ''
              END
          END
      END
      POST(Event:Accepted, ?ChargeType_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupChargeType, Accepted)
    OF ?RepairType_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RepairType_temp, Accepted)
      IF (~0{prop:acceptall}) THEN
          If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,ChargeType_temp,RepairType_temp)
              MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type, Unit Type and Repair Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
          ELSIF (job:Model_Number = '' Or ChargeType_temp = '' Or job:Account_Number = '' Or job:Unit_Type = '') THEN
              MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
          ELSE
              IF (CLIP(RepairType_temp) <> '') THEN
                  DO ValidateRepairType
                  IF (RepairType_Valid = 0) THEN
                      MessageEx('Error! Repair Type is Invalid','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand, |
                                 msgex:samewidths,84,26,0)
                      POST(Event:Accepted,?LookupRepairType)
                  END
              END
              DISPLAY(?RepairType_temp)
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RepairType_temp, Accepted)
    OF ?LookupRepairType
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupRepairType, Accepted)
      IF (job:Model_Number = '' Or ChargeType_temp = '' Or job:Account_Number = '' Or job:Unit_Type = '') THEN
          MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
      ELSIF (PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,ChargeType_temp,RepairType_temp)) THEN
              MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type, Unit Type and Repair Type.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
      ELSE
          temp" = PickRepairTypes(job:Model_Number,job:Account_Number,ChargeType_temp,job:Unit_Type,'C')
          IF (CLIP(temp") <> '')
              RepairType_temp = temp"
          ELSE
              DO ValidateRepairType
              IF (RepairType_Valid = 0) THEN
                  RepairType_temp = ''
              END
          END
          POST(Event:Accepted, ?RepairType_temp)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupRepairType, Accepted)
    OF ?FinishButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FinishButton, Accepted)
      ! Return Warranty Parts to Stock
      IF (job:Warranty_Job = 'YES') THEN
          save_wpr_id = Access:WARPARTS.SaveFile()
          save_sto_id = Access:STOCK.SaveFile()
          save_shi_id = Access:STOHIST.SaveFile()
          save_use_id = Access:USERS.SaveFile()
          access:USERS.clearkey(use:password_key)
          use:password = glo:password
          access:USERS.fetch(use:password_key)
          Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
          wpr:Ref_Number  = job:Ref_Number
          SET(wpr:Part_Number_Key, wpr:Part_Number_Key)
          LOOP
              IF ((Access:WARPARTS.NEXT() <> Level:Benign) OR (wpr:Ref_Number  <> job:Ref_Number)) THEN
                  BREAK
              END
              IF ((wpr:Order_Number = '') AND (wpr:Part_Ref_Number <> '') AND (wpr:Pending_Ref_Number = 0)) THEN
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = wpr:Part_Ref_Number
                  IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign) THEN
                      IF (sto:Sundry_Item <> 'YES') THEN
                          sto:quantity_stock += wpr:quantity
                          IF (Access:STOCK.update() = Level:Benign) THEN
                              GET(STOHIST,0)
                              IF (Access:STOHIST.primerecord() = level:Benign) THEN
                                  shi:ref_number = sto:ref_number
                                  shi:user                  = use:user_code
                                  shi:date                 = today()
                                  shi:transaction_type     = 'ADD'
                                  shi:despatch_note_number = wpr:despatch_note_number
                                  shi:job_number           = job:Ref_number
                                  shi:quantity             = wpr:quantity
                                  shi:purchase_cost        = wpr:purchase_cost
                                  shi:sale_cost            = wpr:sale_cost
                                  shi:retail_cost          = wpr:retail_cost
                                  shi:information          = 'WARRANTY PART REMOVED FROM JOB'
                                  IF (Access:STOHIST.insert() <> Level:Benign) THEN
                                      Access:STOHIST.cancelautoinc()
                                  END
                              END
                          END
                      END
                      Access:WARPARTS.DeleteRecord(0)
                  END
              ELSE
                  IF (CLIP(wpr:Part_Number) = 'ADJUSTMENT') THEN
                      Access:WARPARTS.DeleteRecord(0)
                  END
              END
           END
          Access:WARPARTS.RestoreFile(save_wpr_id)
          Access:STOCK.RestoreFile(save_sto_id)
          Access:STOHIST.RestoreFile(save_shi_id)
          Access:USERS.RestoreFile(save_use_id)
      END
      
      ! initialise return variables
      glo:Select17 = 'OK'
      glo:Select18 = ChargeType_temp
      IF (?RepairType_temp{PROP:HIDE} = 1) THEN
          glo:Select19 = ''
      ELSE
          glo:Select19 = RepairType_temp
      END
      
      ! save default values
      IF (ChargeType_temp <> DefaultChargeType) THEN
          PUTINI('ESTIMATEJOB','ChargeType', ChargeType_temp, CLIP(PATH()) & '\CELRAPEN.INI')
      END
      
      IF ((?RepairType_temp{PROP:HIDE} = 0) AND (RepairType_temp <> DefaultRepairType)) THEN
          PUTINI('ESTIMATEJOB','RepairType', RepairType_temp, CLIP(PATH()) & '\CELRAPEN.INI')
      END
      
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FinishButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      ! Restore ESTPARTS File to Original State
      !
      Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
      epr:Ref_Number = job:Ref_Number
      SET(epr:Part_Number_Key, epr:Part_Number_Key)
      LOOP
          IF ((Access:ESTPARTS.NEXT() <> Level:Benign) OR (epr:Ref_Number <> job:Ref_Number)) THEN
               BREAK
          END
          Access:ESTPARTS.DeleteRecord(0)
      END
      
      LOOP ix# = 1 TO RECORDS(epQueue) BY 1
          GET(epQueue, ix#)
          IF (Access:ESTPARTS.PrimeRecord() = Level:Benign) THEN
      
              epr:Ref_Number           = epq:Ref_Number
              epr:Adjustment           = epq:Adjustment
              epr:Part_Ref_Number      = epq:Part_Ref_Number
              epr:Part_Number          = epq:Part_Number
              epr:Description          = epq:Description
              epr:Supplier             = epq:Supplier
              epr:Purchase_Cost        = epq:Purchase_Cost
              epr:Sale_Cost            = epq:Sale_Cost
              epr:Retail_Cost          = epq:Retail_Cost
              epr:Quantity             = epq:Quantity
              epr:Warranty_Part        = epq:Warranty_Part
              epr:Exclude_From_Order   = epq:Exclude_From_Order
              epr:Despatch_Note_Number = epq:Despatch_Note_Number
              epr:Date_Ordered         = epq:Date_Ordered
              epr:Pending_Ref_Number   = epq:Pending_Ref_Number
              epr:Order_Number         = epq:Order_Number
              epr:Date_Received        = epq:Date_Received
              epr:Order_Part_Number    = epq:Order_Part_Number
              epr:Status_Date          = epq:Status_Date
              epr:Fault_Code1          = epq:Fault_Code1
              epr:Fault_Code2          = epq:Fault_Code2
              epr:Fault_Code3          = epq:Fault_Code3
              epr:Fault_Code4          = epq:Fault_Code4
              epr:Fault_Code5          = epq:Fault_Code5
              epr:Fault_Code6          = epq:Fault_Code6
              epr:Fault_Code7          = epq:Fault_Code7
              epr:Fault_Code8          = epq:Fault_Code8
              epr:Fault_Code9          = epq:Fault_Code9
              epr:Fault_Code10         = epq:Fault_Code10
              epr:Fault_Code11         = epq:Fault_Code11
              epr:Fault_Code12         = epq:Fault_Code12
      
              IF (Access:ESTPARTS.Insert() <> Level:Benign) THEN
                  Access:ESTPARTS.CancelAutoInc()
              END
          END
      END
      
      glo:Select17 = 'CANCEL'
      glo:Select18 = ''
      glo:Select19 = ''
      
      Post(Event:Closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Make_Estimate_Job')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ?Sheet1{PROP:WIZARD} = true
      DefaultChargeType = Clip(GETINI('ESTIMATEJOB','ChargeType','',CLIP(PATH())&'\CELRAPEN.INI'))
      DefaultRepairType = Clip(GETINI('ESTIMATEJOB','RepairType','',CLIP(PATH())&'\CELRAPEN.INI'))
      ChargeType_temp = ''
      IF (CLIP(defaultChargeType) <> '') THEN
          Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
          cha:Charge_Type  = DefaultChargeType
          IF (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign) THEN
              ChargeType_temp = DefaultChargeType
          END
      END
      DISPLAY(?ChargeType_temp)
      RepairType_temp = DefaultRepairType
      IF (PricingStructure(job:Account_Number, job:Model_Number, job:Unit_Type, ChargeType_temp, RepairType_temp) = Level:Benign) THEN
          UNHIDE(?RepairTypePrompt)
          UNHIDE(?RepairType_temp)
          UNHIDE(?LookupRepairType)
          IF (CLIP(RepairType_temp) <> '') THEN
              DO ValidateRepairType
              IF (RepairType_Valid = 0) THEN
                  RepairType_temp = ''
              END
          END
      END
      DISPLAY(?RepairType_temp)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end

