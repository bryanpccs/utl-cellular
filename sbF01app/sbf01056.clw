

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01056.INC'),ONCE        !Local module procedure declarations
                     END


Multiple_Job_Wizard_End PROCEDURE                     !Generated from procedure template - Window

tmp:esn              STRING(16)
tmp:msn              STRING(10)
tmp:ModelNumber      STRING(30)
tmp:UnitType         STRING(30)
tmp:Accessories      STRING(30)
tmp:OrderNumber      STRING(30)
tmp:Location         STRING(30)
tmp:FaultDescription STRING(255)
tmp:MobileNumber     STRING(20)
tmp:CChargeType      STRING(30)
tmp:CRepairType      STRING(30)
tmp:WChargeType      STRING(30)
tmp:WRepairType      STRING(30)
tmp:DOP              DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Multiple Job Booking Wizard'),AT(,,327,323),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,320,288),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Multiple Job Booking Wizard'),AT(8,8),USE(?Prompt1),FONT(,16,,FONT:bold)
                           PROMPT('Finishing - Creating Jobs'),AT(96,28),USE(?Prompt2),FONT(,12,,FONT:bold)
                           PROMPT('E.S.N. / I.M.E.I.'),AT(96,48),USE(?tmp:esn:Prompt)
                           ENTRY(@s16),AT(180,48,124,10),USE(tmp:esn),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('M.S.N.'),AT(96,64),USE(?tmp:msn:Prompt)
                           ENTRY(@s10),AT(180,64,64,10),USE(tmp:msn),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Model Number'),AT(96,80),USE(?tmp:ModelNumber:Prompt)
                           ENTRY(@s30),AT(180,80,124,10),USE(tmp:ModelNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Unit Type'),AT(96,96),USE(?tmp:UnitType:Prompt)
                           ENTRY(@s30),AT(180,96,124,10),USE(tmp:UnitType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Accessories'),AT(96,112),USE(?tmp:Accessories:Prompt)
                           ENTRY(@s30),AT(180,112,124,10),USE(tmp:Accessories),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Order Number'),AT(96,128),USE(?tmp:OrderNumber:Prompt)
                           ENTRY(@s30),AT(180,128,124,10),USE(tmp:OrderNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Location'),AT(96,144),USE(?tmp:Location:Prompt)
                           ENTRY(@s30),AT(180,144,124,10),USE(tmp:Location),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Fault Description'),AT(96,160),USE(?tmp:Location:Prompt:2)
                           TEXT,AT(180,160,124,20),USE(tmp:FaultDescription),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Mobile Number'),AT(96,188),USE(?tmp:MobileNumber:Prompt)
                           ENTRY(@s20),AT(180,188,124,10),USE(tmp:MobileNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Chargeable Charge Type'),AT(96,204),USE(?tmp:CChargeType:Prompt)
                           ENTRY(@s30),AT(180,204,124,10),USE(tmp:CChargeType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Repair Type'),AT(96,220),USE(?tmp:CRepairType:Prompt)
                           ENTRY(@s30),AT(180,220,124,10),USE(tmp:CRepairType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Warranty Charge Type'),AT(96,236),USE(?tmp:WChargeType:Prompt)
                           ENTRY(@s30),AT(180,236,124,10),USE(tmp:WChargeType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Warranty Repair Type'),AT(96,252),USE(?tmp:WRepairType:Prompt)
                           ENTRY(@s30),AT(180,252,124,10),USE(tmp:WRepairType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('D.O.P.'),AT(96,268),USE(?tmp:DOP:Prompt)
                           ENTRY(@d6),AT(180,268,64,10),USE(tmp:DOP),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       IMAGE('wizard.gif'),AT(12,92),USE(?Image1)
                       PANEL,AT(4,296,320,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Insert Multiple Jobs'),AT(8,300,80,16),USE(?Button1),LEFT,ICON('insert.gif')
                       BUTTON('Create Job And Insert Another'),AT(92,300,80,16),USE(?Button1:2),LEFT,ICON('insert.gif')
                       BUTTON('Create Job And Finish'),AT(176,300,80,16),USE(?Button1:3),LEFT,ICON('insert.gif')
                       BUTTON('Finish'),AT(264,300,56,16),USE(?Button4),LEFT,ICON('thumbs.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?tmp:esn:Prompt{prop:FontColor} = -1
    ?tmp:esn:Prompt{prop:Color} = 15066597
    If ?tmp:esn{prop:ReadOnly} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 15066597
    Elsif ?tmp:esn{prop:Req} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 8454143
    Else ! If ?tmp:esn{prop:Req} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 16777215
    End ! If ?tmp:esn{prop:Req} = True
    ?tmp:esn{prop:Trn} = 0
    ?tmp:esn{prop:FontStyle} = font:Bold
    ?tmp:msn:Prompt{prop:FontColor} = -1
    ?tmp:msn:Prompt{prop:Color} = 15066597
    If ?tmp:msn{prop:ReadOnly} = True
        ?tmp:msn{prop:FontColor} = 65793
        ?tmp:msn{prop:Color} = 15066597
    Elsif ?tmp:msn{prop:Req} = True
        ?tmp:msn{prop:FontColor} = 65793
        ?tmp:msn{prop:Color} = 8454143
    Else ! If ?tmp:msn{prop:Req} = True
        ?tmp:msn{prop:FontColor} = 65793
        ?tmp:msn{prop:Color} = 16777215
    End ! If ?tmp:msn{prop:Req} = True
    ?tmp:msn{prop:Trn} = 0
    ?tmp:msn{prop:FontStyle} = font:Bold
    ?tmp:ModelNumber:Prompt{prop:FontColor} = -1
    ?tmp:ModelNumber:Prompt{prop:Color} = 15066597
    If ?tmp:ModelNumber{prop:ReadOnly} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 15066597
    Elsif ?tmp:ModelNumber{prop:Req} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 8454143
    Else ! If ?tmp:ModelNumber{prop:Req} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 16777215
    End ! If ?tmp:ModelNumber{prop:Req} = True
    ?tmp:ModelNumber{prop:Trn} = 0
    ?tmp:ModelNumber{prop:FontStyle} = font:Bold
    ?tmp:UnitType:Prompt{prop:FontColor} = -1
    ?tmp:UnitType:Prompt{prop:Color} = 15066597
    If ?tmp:UnitType{prop:ReadOnly} = True
        ?tmp:UnitType{prop:FontColor} = 65793
        ?tmp:UnitType{prop:Color} = 15066597
    Elsif ?tmp:UnitType{prop:Req} = True
        ?tmp:UnitType{prop:FontColor} = 65793
        ?tmp:UnitType{prop:Color} = 8454143
    Else ! If ?tmp:UnitType{prop:Req} = True
        ?tmp:UnitType{prop:FontColor} = 65793
        ?tmp:UnitType{prop:Color} = 16777215
    End ! If ?tmp:UnitType{prop:Req} = True
    ?tmp:UnitType{prop:Trn} = 0
    ?tmp:UnitType{prop:FontStyle} = font:Bold
    ?tmp:Accessories:Prompt{prop:FontColor} = -1
    ?tmp:Accessories:Prompt{prop:Color} = 15066597
    If ?tmp:Accessories{prop:ReadOnly} = True
        ?tmp:Accessories{prop:FontColor} = 65793
        ?tmp:Accessories{prop:Color} = 15066597
    Elsif ?tmp:Accessories{prop:Req} = True
        ?tmp:Accessories{prop:FontColor} = 65793
        ?tmp:Accessories{prop:Color} = 8454143
    Else ! If ?tmp:Accessories{prop:Req} = True
        ?tmp:Accessories{prop:FontColor} = 65793
        ?tmp:Accessories{prop:Color} = 16777215
    End ! If ?tmp:Accessories{prop:Req} = True
    ?tmp:Accessories{prop:Trn} = 0
    ?tmp:Accessories{prop:FontStyle} = font:Bold
    ?tmp:OrderNumber:Prompt{prop:FontColor} = -1
    ?tmp:OrderNumber:Prompt{prop:Color} = 15066597
    If ?tmp:OrderNumber{prop:ReadOnly} = True
        ?tmp:OrderNumber{prop:FontColor} = 65793
        ?tmp:OrderNumber{prop:Color} = 15066597
    Elsif ?tmp:OrderNumber{prop:Req} = True
        ?tmp:OrderNumber{prop:FontColor} = 65793
        ?tmp:OrderNumber{prop:Color} = 8454143
    Else ! If ?tmp:OrderNumber{prop:Req} = True
        ?tmp:OrderNumber{prop:FontColor} = 65793
        ?tmp:OrderNumber{prop:Color} = 16777215
    End ! If ?tmp:OrderNumber{prop:Req} = True
    ?tmp:OrderNumber{prop:Trn} = 0
    ?tmp:OrderNumber{prop:FontStyle} = font:Bold
    ?tmp:Location:Prompt{prop:FontColor} = -1
    ?tmp:Location:Prompt{prop:Color} = 15066597
    If ?tmp:Location{prop:ReadOnly} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 15066597
    Elsif ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 8454143
    Else ! If ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 16777215
    End ! If ?tmp:Location{prop:Req} = True
    ?tmp:Location{prop:Trn} = 0
    ?tmp:Location{prop:FontStyle} = font:Bold
    ?tmp:Location:Prompt:2{prop:FontColor} = -1
    ?tmp:Location:Prompt:2{prop:Color} = 15066597
    If ?tmp:FaultDescription{prop:ReadOnly} = True
        ?tmp:FaultDescription{prop:FontColor} = 65793
        ?tmp:FaultDescription{prop:Color} = 15066597
    Elsif ?tmp:FaultDescription{prop:Req} = True
        ?tmp:FaultDescription{prop:FontColor} = 65793
        ?tmp:FaultDescription{prop:Color} = 8454143
    Else ! If ?tmp:FaultDescription{prop:Req} = True
        ?tmp:FaultDescription{prop:FontColor} = 65793
        ?tmp:FaultDescription{prop:Color} = 16777215
    End ! If ?tmp:FaultDescription{prop:Req} = True
    ?tmp:FaultDescription{prop:Trn} = 0
    ?tmp:FaultDescription{prop:FontStyle} = font:Bold
    ?tmp:MobileNumber:Prompt{prop:FontColor} = -1
    ?tmp:MobileNumber:Prompt{prop:Color} = 15066597
    If ?tmp:MobileNumber{prop:ReadOnly} = True
        ?tmp:MobileNumber{prop:FontColor} = 65793
        ?tmp:MobileNumber{prop:Color} = 15066597
    Elsif ?tmp:MobileNumber{prop:Req} = True
        ?tmp:MobileNumber{prop:FontColor} = 65793
        ?tmp:MobileNumber{prop:Color} = 8454143
    Else ! If ?tmp:MobileNumber{prop:Req} = True
        ?tmp:MobileNumber{prop:FontColor} = 65793
        ?tmp:MobileNumber{prop:Color} = 16777215
    End ! If ?tmp:MobileNumber{prop:Req} = True
    ?tmp:MobileNumber{prop:Trn} = 0
    ?tmp:MobileNumber{prop:FontStyle} = font:Bold
    ?tmp:CChargeType:Prompt{prop:FontColor} = -1
    ?tmp:CChargeType:Prompt{prop:Color} = 15066597
    If ?tmp:CChargeType{prop:ReadOnly} = True
        ?tmp:CChargeType{prop:FontColor} = 65793
        ?tmp:CChargeType{prop:Color} = 15066597
    Elsif ?tmp:CChargeType{prop:Req} = True
        ?tmp:CChargeType{prop:FontColor} = 65793
        ?tmp:CChargeType{prop:Color} = 8454143
    Else ! If ?tmp:CChargeType{prop:Req} = True
        ?tmp:CChargeType{prop:FontColor} = 65793
        ?tmp:CChargeType{prop:Color} = 16777215
    End ! If ?tmp:CChargeType{prop:Req} = True
    ?tmp:CChargeType{prop:Trn} = 0
    ?tmp:CChargeType{prop:FontStyle} = font:Bold
    ?tmp:CRepairType:Prompt{prop:FontColor} = -1
    ?tmp:CRepairType:Prompt{prop:Color} = 15066597
    If ?tmp:CRepairType{prop:ReadOnly} = True
        ?tmp:CRepairType{prop:FontColor} = 65793
        ?tmp:CRepairType{prop:Color} = 15066597
    Elsif ?tmp:CRepairType{prop:Req} = True
        ?tmp:CRepairType{prop:FontColor} = 65793
        ?tmp:CRepairType{prop:Color} = 8454143
    Else ! If ?tmp:CRepairType{prop:Req} = True
        ?tmp:CRepairType{prop:FontColor} = 65793
        ?tmp:CRepairType{prop:Color} = 16777215
    End ! If ?tmp:CRepairType{prop:Req} = True
    ?tmp:CRepairType{prop:Trn} = 0
    ?tmp:CRepairType{prop:FontStyle} = font:Bold
    ?tmp:WChargeType:Prompt{prop:FontColor} = -1
    ?tmp:WChargeType:Prompt{prop:Color} = 15066597
    If ?tmp:WChargeType{prop:ReadOnly} = True
        ?tmp:WChargeType{prop:FontColor} = 65793
        ?tmp:WChargeType{prop:Color} = 15066597
    Elsif ?tmp:WChargeType{prop:Req} = True
        ?tmp:WChargeType{prop:FontColor} = 65793
        ?tmp:WChargeType{prop:Color} = 8454143
    Else ! If ?tmp:WChargeType{prop:Req} = True
        ?tmp:WChargeType{prop:FontColor} = 65793
        ?tmp:WChargeType{prop:Color} = 16777215
    End ! If ?tmp:WChargeType{prop:Req} = True
    ?tmp:WChargeType{prop:Trn} = 0
    ?tmp:WChargeType{prop:FontStyle} = font:Bold
    ?tmp:WRepairType:Prompt{prop:FontColor} = -1
    ?tmp:WRepairType:Prompt{prop:Color} = 15066597
    If ?tmp:WRepairType{prop:ReadOnly} = True
        ?tmp:WRepairType{prop:FontColor} = 65793
        ?tmp:WRepairType{prop:Color} = 15066597
    Elsif ?tmp:WRepairType{prop:Req} = True
        ?tmp:WRepairType{prop:FontColor} = 65793
        ?tmp:WRepairType{prop:Color} = 8454143
    Else ! If ?tmp:WRepairType{prop:Req} = True
        ?tmp:WRepairType{prop:FontColor} = 65793
        ?tmp:WRepairType{prop:Color} = 16777215
    End ! If ?tmp:WRepairType{prop:Req} = True
    ?tmp:WRepairType{prop:Trn} = 0
    ?tmp:WRepairType{prop:FontStyle} = font:Bold
    ?tmp:DOP:Prompt{prop:FontColor} = -1
    ?tmp:DOP:Prompt{prop:Color} = 15066597
    If ?tmp:DOP{prop:ReadOnly} = True
        ?tmp:DOP{prop:FontColor} = 65793
        ?tmp:DOP{prop:Color} = 15066597
    Elsif ?tmp:DOP{prop:Req} = True
        ?tmp:DOP{prop:FontColor} = 65793
        ?tmp:DOP{prop:Color} = 8454143
    Else ! If ?tmp:DOP{prop:Req} = True
        ?tmp:DOP{prop:FontColor} = 65793
        ?tmp:DOP{prop:Color} = 16777215
    End ! If ?tmp:DOP{prop:Req} = True
    ?tmp:DOP{prop:Trn} = 0
    ?tmp:DOP{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Multiple_Job_Wizard_End',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:esn',tmp:esn,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:msn',tmp:msn,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:ModelNumber',tmp:ModelNumber,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:UnitType',tmp:UnitType,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:Accessories',tmp:Accessories,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:OrderNumber',tmp:OrderNumber,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:Location',tmp:Location,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:FaultDescription',tmp:FaultDescription,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:MobileNumber',tmp:MobileNumber,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:CChargeType',tmp:CChargeType,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:CRepairType',tmp:CRepairType,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:WChargeType',tmp:WChargeType,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:WRepairType',tmp:WRepairType,'Multiple_Job_Wizard_End',1)
    SolaceViewVars('tmp:DOP',tmp:DOP,'Multiple_Job_Wizard_End',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn:Prompt;  SolaceCtrlName = '?tmp:esn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn;  SolaceCtrlName = '?tmp:esn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:msn:Prompt;  SolaceCtrlName = '?tmp:msn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:msn;  SolaceCtrlName = '?tmp:msn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber:Prompt;  SolaceCtrlName = '?tmp:ModelNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber;  SolaceCtrlName = '?tmp:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:UnitType:Prompt;  SolaceCtrlName = '?tmp:UnitType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:UnitType;  SolaceCtrlName = '?tmp:UnitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Accessories:Prompt;  SolaceCtrlName = '?tmp:Accessories:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Accessories;  SolaceCtrlName = '?tmp:Accessories';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:OrderNumber:Prompt;  SolaceCtrlName = '?tmp:OrderNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:OrderNumber;  SolaceCtrlName = '?tmp:OrderNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location:Prompt;  SolaceCtrlName = '?tmp:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location;  SolaceCtrlName = '?tmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location:Prompt:2;  SolaceCtrlName = '?tmp:Location:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:FaultDescription;  SolaceCtrlName = '?tmp:FaultDescription';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:MobileNumber:Prompt;  SolaceCtrlName = '?tmp:MobileNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:MobileNumber;  SolaceCtrlName = '?tmp:MobileNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CChargeType:Prompt;  SolaceCtrlName = '?tmp:CChargeType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CChargeType;  SolaceCtrlName = '?tmp:CChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CRepairType:Prompt;  SolaceCtrlName = '?tmp:CRepairType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CRepairType;  SolaceCtrlName = '?tmp:CRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:WChargeType:Prompt;  SolaceCtrlName = '?tmp:WChargeType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:WChargeType;  SolaceCtrlName = '?tmp:WChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:WRepairType:Prompt;  SolaceCtrlName = '?tmp:WRepairType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:WRepairType;  SolaceCtrlName = '?tmp:WRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DOP:Prompt;  SolaceCtrlName = '?tmp:DOP:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DOP;  SolaceCtrlName = '?tmp:DOP';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button1;  SolaceCtrlName = '?Button1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button1:2;  SolaceCtrlName = '?Button1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button1:3;  SolaceCtrlName = '?Button1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Multiple_Job_Wizard_End')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Multiple_Job_Wizard_End')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Multiple_Job_Wizard_End',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Multiple_Job_Wizard_End')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

