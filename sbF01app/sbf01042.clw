

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01042.INC'),ONCE        !Local module procedure declarations
                     END


Despatch_Batch PROCEDURE (f_batch_number)             !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
incomplete_temp      LONG
tmp:consignmentno    STRING(30)
tmp:DespatchDate     STRING(20)
tmp:courier          STRING(30)
save_job_id          USHORT,AUTO
complete_temp        LONG
despatch_temp        LONG
batch_quantity_temp  LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Batch Analysis'),AT(,,223,159),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,216,124),USE(?Sheet1),SPREAD
                         TAB('Batch Analysis'),USE(?Tab1)
                           STRING('Batch Number:'),AT(12,24),USE(?String1),FONT(,10,,,CHARSET:ANSI)
                           STRING(@s8),AT(138,24),USE(f_batch_number),RIGHT,FONT(,10,,FONT:bold,CHARSET:ANSI)
                           STRING('NOT Ready To Despatch'),AT(12,44),USE(?String1:2),FONT(,10,COLOR:Red,,CHARSET:ANSI)
                           STRING(@s8),AT(138,44),USE(incomplete_temp),RIGHT,FONT(,10,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(138,64),USE(complete_temp),RIGHT,FONT(,10,COLOR:Green,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(138,84),USE(despatch_temp),RIGHT,FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(138,104),USE(batch_quantity_temp),RIGHT,FONT(,10,,FONT:bold,CHARSET:ANSI)
                           STRING('Ready To Despatch'),AT(12,64),USE(?String1:3),FONT(,10,COLOR:Green,,CHARSET:ANSI)
                           STRING('Previously Despatched'),AT(12,84),USE(?String1:4),FONT(,10,COLOR:Navy,,CHARSET:ANSI)
                           STRING('Batch Quantity'),AT(12,104),USE(?String1:5),FONT(,10,,,CHARSET:ANSI)
                         END
                       END
                       BUTTON('&Scan And Despatch'),AT(8,136,64,16),USE(?Scan_Despatch),LEFT,ICON('scanner.gif'),DEFAULT
                       BUTTON('&Bulk Despatch'),AT(80,136,64,16),USE(?Bulk_Despatch),LEFT,ICON('desp_sm.gif')
                       PANEL,AT(4,132,216,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Close'),AT(152,136,64,16),USE(?Close),LEFT,ICON('cancel.gif'),DEFAULT
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?f_batch_number{prop:FontColor} = -1
    ?f_batch_number{prop:Color} = 15066597
    ?String1:2{prop:FontColor} = -1
    ?String1:2{prop:Color} = 15066597
    ?incomplete_temp{prop:FontColor} = -1
    ?incomplete_temp{prop:Color} = 15066597
    ?complete_temp{prop:FontColor} = -1
    ?complete_temp{prop:Color} = 15066597
    ?despatch_temp{prop:FontColor} = -1
    ?despatch_temp{prop:Color} = 15066597
    ?batch_quantity_temp{prop:FontColor} = -1
    ?batch_quantity_temp{prop:Color} = 15066597
    ?String1:3{prop:FontColor} = -1
    ?String1:3{prop:Color} = 15066597
    ?String1:4{prop:FontColor} = -1
    ?String1:4{prop:Color} = 15066597
    ?String1:5{prop:FontColor} = -1
    ?String1:5{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Despatch_Batch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('incomplete_temp',incomplete_temp,'Despatch_Batch',1)
    SolaceViewVars('tmp:consignmentno',tmp:consignmentno,'Despatch_Batch',1)
    SolaceViewVars('tmp:DespatchDate',tmp:DespatchDate,'Despatch_Batch',1)
    SolaceViewVars('tmp:courier',tmp:courier,'Despatch_Batch',1)
    SolaceViewVars('save_job_id',save_job_id,'Despatch_Batch',1)
    SolaceViewVars('complete_temp',complete_temp,'Despatch_Batch',1)
    SolaceViewVars('despatch_temp',despatch_temp,'Despatch_Batch',1)
    SolaceViewVars('batch_quantity_temp',batch_quantity_temp,'Despatch_Batch',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?f_batch_number;  SolaceCtrlName = '?f_batch_number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:2;  SolaceCtrlName = '?String1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?incomplete_temp;  SolaceCtrlName = '?incomplete_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?complete_temp;  SolaceCtrlName = '?complete_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_temp;  SolaceCtrlName = '?despatch_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?batch_quantity_temp;  SolaceCtrlName = '?batch_quantity_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:3;  SolaceCtrlName = '?String1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:4;  SolaceCtrlName = '?String1:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:5;  SolaceCtrlName = '?String1:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Scan_Despatch;  SolaceCtrlName = '?Scan_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Bulk_Despatch;  SolaceCtrlName = '?Bulk_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Despatch_Batch')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Despatch_Batch')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DESBATCH.Open
  Relate:EXCHANGE.Open
  Access:JOBS.UseFile
  Access:EXCHHIST.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DESBATCH.Close
    Relate:EXCHANGE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Despatch_Batch',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Scan_Despatch
      ThisWindow.Update
      Scan_Batch_Despatch(f_batch_number)
      ThisWindow.Reset
    OF ?Bulk_Despatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Bulk_Despatch, Accepted)
      Case MessageEx('You are about to mark the WHOLE batch as Despatched.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              tmp:courier = ''
              Pick_Courier_Consignment(tmp:courier,tmp:consignmentno,tmp:despatchDate)
      
              If tmp:courier <> ''
      
                  count# = 0
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:batch_number_key)
                  job:batch_number = f_batch_number
                  set(job:batch_number_key,job:batch_number_key)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:batch_number <> f_batch_number      |
                          then break.  ! end if
                      account"    = job:account_number
                      If job:despatched = 'REA'
                          count# += 1
                      End
                  end !loop
                  access:jobs.restorefile(save_job_id)
      
                  If count# = 0
                      Case MessageEx('There are no jobs to despatch.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If count# = 0
      
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      setcursor(cursor:wait)
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
                      recordstoprocess    = count#
      
                      If access:desbatch.primerecord() = level:Benign
                          If access:desbatch.tryinsert() = Level:Benign
      
      
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:batch_number_key)
                              job:batch_number = f_batch_number
                              set(job:batch_number_key,job:batch_number_key)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:batch_number <> f_batch_number      |
                                      then break.  ! end if
                                  Do GetNextRecord2
                                  If job:despatched = 'REA'
                                      If job:exchange_unit_number <> ''
                                          If job:exchange_consignment_number = ''
                                              job:exchange_consignment_number = tmp:consignmentno
                                              job:exchange_despatched = tmp:despatchdate
                                              job:exchange_despatch_number    = dbt:batch_number
                                              job:exchange_courier    = tmp:courier
                                              job:despatched = ''
                                              access:users.clearkey(use:password_key)
                                              use:password    =glo:password
                                              access:users.tryfetch(use:password_key)
                                              job:exchange_despatched_user    = use:user_code
                                              !call the exchange status routine
                                              GetStatus(125,1,'EXC') !exchange unit despatched
      
                                              access:jobs.update()
      
                                              get(audit,0)
                                              if access:audit.primerecord() = level:benign
                                                  aud:notes         = 'BATCH NUMBER: ' & CLip(f_batch_number) & |
                                                                      '<13,10>COURIER: ' & Clip(job:exchange_courier) &|
                                                                      '<13,10>CONSIGNMENT NOTE NUMBER: ' & Clip(job:exchange_consignment_number)
                                                  aud:ref_number    = job:ref_number
                                                  aud:date          = today()
                                                  aud:time          = clock()
                                                  access:users.clearkey(use:password_key)
                                                  use:password =glo:password
                                                  access:users.fetch(use:password_key)
                                                  aud:user = use:user_code
                                                  aud:type  = 'EXC'
                                                  aud:action        = 'BATCH DESPATCH: EXCHANGE UNIT'
                                                  access:audit.insert()
                                              end!�if access:audit.primerecord() = level:benign
      
                                              access:exchange.clearkey(xch:ref_number_key)
                                              xch:ref_number  = job:Exchange_unit_number
                                              If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                                                  xch:available = 'DES'
                                                  access:exchange.update()
                                                  Get(exchhist,0)
                                                  If access:exchhist.primerecord() = Level:Benign
                                                      exh:ref_Number  = xch:ref_number
                                                      exh:date        = Today()
                                                      exh:time        = Clock()
                                                      access:users.clearkey(use:password_key)
                                                      use:password    =glo:password
                                                      access:users.fetch(use:password_key)
                                                      exh:user    = use:user_code
                                                      If access:exchhist.insert()
                                                          access:exchhist.cancelautoinc()
                                                      End!If access:exchhist.insert()
                                                  End!If access:exchhist.primerecord() = Level:Benign
                                              End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
      
                                          End!If job:exchange_consignment_number = ''
                                      Else!If job:exchange_unit_number <> ''
                                          If job:consignment_number = ''
                                              job:Consignment_number  = tmp:ConsignmentNo
                                              job:date_Despatched     = tmp:DespatchDate
                                              job:courier             = tmp:Courier
                                              job:despatch_number     = dbt:batch_number
                                              job:despatched          = 'YES'
                                              access:users.clearkey(use:password_key)
                                              use:password    =glo:password
                                              access:users.fetch(use:password_key)
                                              job:despatch_user   = use:user_code
                                              If job:paid = 'YES'
                                                  !call the status routine
                                                  GetStatus(910,1,'JOB') !despatched paid
      
                                              Else!If job:paid = 'YES'
                                                  !call the status routine
                                                  GetStatus(905,1,'JOB') !despatched unpaid
      
                                              End!If job:paid = 'YES'
                                              access:jobs.update()
      
                                              get(audit,0)
                                              if access:audit.primerecord() = level:benign
                                                  aud:notes         = 'BATCH NUMBER: ' & CLip(f_batch_number) & |
                                                                      '<13,10>COURIER: ' & Clip(job:courier) &|
                                                                      '<13,10>CONSIGNMENT NOTE NUMBER: ' & Clip(job:consignment_number)
                                                  aud:ref_number    = job:ref_number
                                                  aud:date          = today()
                                                  aud:time          = clock()
                                                  access:users.clearkey(use:password_key)
                                                  use:password =glo:password
                                                  access:users.fetch(use:password_key)
                                                  aud:user = use:user_code
                                                  aud:action        = 'BATCH DESPATCH: JOB'
                                                  access:audit.insert()
                                              end!�if access:audit.primerecord() = level:benign
      
                                              access:locinter.clearkey(loi:location_key)
                                              loi:location    = job:location
                                              If access:locinter.tryfetch(loi:location_key)
                                                  If loi:allocate_spaces = 'YES'
                                                      loi:current_spaces += 1
                                                      loi:location_available = 'YES'
                                                      access:locinter.update()
                                                  End!If loi:allocate_spaces = 'YES'
                                              End!If access:locinter.tryfetch(loi:location_key)
      
                                          End!If job:consignment_number = ''
                                      End!If job:exchange_unit_number <> ''
                                  End!If job:despatched = 'REA'
                              end !loop
                              access:jobs.restorefile(save_job_id)
                              setcursor()
                              close(progresswindow)
                              glo:select1  = account"
                              glo:select2  = f_batch_number
                              glo:select3  = tmp:ConsignmentNo
                              glo:select4  = tmp:Courier
                              glo:select5  = dbt:batch_number
                              Batch_Despatch_Note
                              glo:select1  = ''
                              glo:select2  = ''
                              glo:select3  = ''
                              glo:select4  = ''
                              glo:select5  = ''
                          End!If access:desbatch.tryinsert()
                      End!If access:desbatch.primerecord() = level:Benign
                  End!If count# <> 0
              End!If courier" <> ''
              Post(Event:Closewindow)
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Bulk_Despatch, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Despatch_Batch')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      setcursor(cursor:wait)
      save_job_id = access:jobs.savefile()
      access:jobs.clearkey(job:batch_number_key)
      job:batch_number = f_batch_number
      set(job:batch_number_key,job:batch_number_key)
      loop
          if access:jobs.next()
             break
          end !if
          if job:batch_number <> f_batch_number      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          If job:date_Completed = '' and job:exchange_unit_number = ''
              incomplete_temp += 1
          End!If job:date_Completed = ''
          IF job:despatched = 'REA'
              complete_temp += 1
          End!IF job:despatch = 'REA'
          If job:exchange_unit_number <> ''
              If job:exchange_consignment_number <> ''
                  despatch_temp += 1
              End!If exchange_consignemt_number <> ''
          Else!If job:exchange_unit_number <> ''
              If job:consignment_number <> ''
                  despatch_temp += 1
              End!If job:despatch = 'YES'
          End!If job:exchange_unit_number <> ''
      
      end !loop
      access:jobs.restorefile(save_job_id)
      setcursor()
      batch_quantity_temp = incomplete_temp + complete_temp + despatch_temp
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

