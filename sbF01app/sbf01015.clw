

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01015.INC'),ONCE        !Local module procedure declarations
                     END


UpdateMessages PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
user_name_temp       STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::mes:Record  LIKE(mes:RECORD),STATIC
QuickWindow          WINDOW('Update the MESSAGES File'),AT(,,336,176),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateMessages'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,328,140),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Message Date'),AT(8,20),USE(?MES:Date:Prompt)
                           ENTRY(@d6),AT(84,20,60,10),USE(mes:Date),SKIP,RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           ENTRY(@t1),AT(148,20,60,10),USE(mes:Time),SKIP,RIGHT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),READONLY
                           BUTTON,AT(112,36,10,10),USE(?Lookup_User),ICON('list3.ico')
                           PROMPT('User Code'),AT(8,36),USE(?MES:Message_For:Prompt),TRN
                           ENTRY(@s3),AT(84,36,24,10),USE(mes:Message_For),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           ENTRY(@s60),AT(128,36,124,10),USE(user_name_temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Comment'),AT(8,52),USE(?MES:Comment:Prompt)
                           ENTRY(@s80),AT(84,52,200,10),USE(mes:Comment),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Message'),AT(8,68),USE(?MES:Message_Memo:Prompt)
                           TEXT,AT(84,68,200,48),USE(mes:Message_Memo),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Read Message'),AT(84,124),USE(mes:Read),VALUE('YES','NO')
                         END
                       END
                       PANEL,AT(4,148,328,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(216,152,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(272,152,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?MES:Date:Prompt{prop:FontColor} = -1
    ?MES:Date:Prompt{prop:Color} = 15066597
    If ?mes:Date{prop:ReadOnly} = True
        ?mes:Date{prop:FontColor} = 65793
        ?mes:Date{prop:Color} = 15066597
    Elsif ?mes:Date{prop:Req} = True
        ?mes:Date{prop:FontColor} = 65793
        ?mes:Date{prop:Color} = 8454143
    Else ! If ?mes:Date{prop:Req} = True
        ?mes:Date{prop:FontColor} = 65793
        ?mes:Date{prop:Color} = 16777215
    End ! If ?mes:Date{prop:Req} = True
    ?mes:Date{prop:Trn} = 0
    ?mes:Date{prop:FontStyle} = font:Bold
    If ?mes:Time{prop:ReadOnly} = True
        ?mes:Time{prop:FontColor} = 65793
        ?mes:Time{prop:Color} = 15066597
    Elsif ?mes:Time{prop:Req} = True
        ?mes:Time{prop:FontColor} = 65793
        ?mes:Time{prop:Color} = 8454143
    Else ! If ?mes:Time{prop:Req} = True
        ?mes:Time{prop:FontColor} = 65793
        ?mes:Time{prop:Color} = 16777215
    End ! If ?mes:Time{prop:Req} = True
    ?mes:Time{prop:Trn} = 0
    ?mes:Time{prop:FontStyle} = font:Bold
    ?MES:Message_For:Prompt{prop:FontColor} = -1
    ?MES:Message_For:Prompt{prop:Color} = 15066597
    If ?mes:Message_For{prop:ReadOnly} = True
        ?mes:Message_For{prop:FontColor} = 65793
        ?mes:Message_For{prop:Color} = 15066597
    Elsif ?mes:Message_For{prop:Req} = True
        ?mes:Message_For{prop:FontColor} = 65793
        ?mes:Message_For{prop:Color} = 8454143
    Else ! If ?mes:Message_For{prop:Req} = True
        ?mes:Message_For{prop:FontColor} = 65793
        ?mes:Message_For{prop:Color} = 16777215
    End ! If ?mes:Message_For{prop:Req} = True
    ?mes:Message_For{prop:Trn} = 0
    ?mes:Message_For{prop:FontStyle} = font:Bold
    If ?user_name_temp{prop:ReadOnly} = True
        ?user_name_temp{prop:FontColor} = 65793
        ?user_name_temp{prop:Color} = 15066597
    Elsif ?user_name_temp{prop:Req} = True
        ?user_name_temp{prop:FontColor} = 65793
        ?user_name_temp{prop:Color} = 8454143
    Else ! If ?user_name_temp{prop:Req} = True
        ?user_name_temp{prop:FontColor} = 65793
        ?user_name_temp{prop:Color} = 16777215
    End ! If ?user_name_temp{prop:Req} = True
    ?user_name_temp{prop:Trn} = 0
    ?user_name_temp{prop:FontStyle} = font:Bold
    ?MES:Comment:Prompt{prop:FontColor} = -1
    ?MES:Comment:Prompt{prop:Color} = 15066597
    If ?mes:Comment{prop:ReadOnly} = True
        ?mes:Comment{prop:FontColor} = 65793
        ?mes:Comment{prop:Color} = 15066597
    Elsif ?mes:Comment{prop:Req} = True
        ?mes:Comment{prop:FontColor} = 65793
        ?mes:Comment{prop:Color} = 8454143
    Else ! If ?mes:Comment{prop:Req} = True
        ?mes:Comment{prop:FontColor} = 65793
        ?mes:Comment{prop:Color} = 16777215
    End ! If ?mes:Comment{prop:Req} = True
    ?mes:Comment{prop:Trn} = 0
    ?mes:Comment{prop:FontStyle} = font:Bold
    ?MES:Message_Memo:Prompt{prop:FontColor} = -1
    ?MES:Message_Memo:Prompt{prop:Color} = 15066597
    If ?mes:Message_Memo{prop:ReadOnly} = True
        ?mes:Message_Memo{prop:FontColor} = 65793
        ?mes:Message_Memo{prop:Color} = 15066597
    Elsif ?mes:Message_Memo{prop:Req} = True
        ?mes:Message_Memo{prop:FontColor} = 65793
        ?mes:Message_Memo{prop:Color} = 8454143
    Else ! If ?mes:Message_Memo{prop:Req} = True
        ?mes:Message_Memo{prop:FontColor} = 65793
        ?mes:Message_Memo{prop:Color} = 16777215
    End ! If ?mes:Message_Memo{prop:Req} = True
    ?mes:Message_Memo{prop:Trn} = 0
    ?mes:Message_Memo{prop:FontStyle} = font:Bold
    ?mes:Read{prop:Font,3} = -1
    ?mes:Read{prop:Color} = 15066597
    ?mes:Read{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMessages',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMessages',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateMessages',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMessages',1)
    SolaceViewVars('user_name_temp',user_name_temp,'UpdateMessages',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MES:Date:Prompt;  SolaceCtrlName = '?MES:Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mes:Date;  SolaceCtrlName = '?mes:Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mes:Time;  SolaceCtrlName = '?mes:Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_User;  SolaceCtrlName = '?Lookup_User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MES:Message_For:Prompt;  SolaceCtrlName = '?MES:Message_For:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mes:Message_For;  SolaceCtrlName = '?mes:Message_For';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?user_name_temp;  SolaceCtrlName = '?user_name_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MES:Comment:Prompt;  SolaceCtrlName = '?MES:Comment:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mes:Comment;  SolaceCtrlName = '?mes:Comment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MES:Message_Memo:Prompt;  SolaceCtrlName = '?MES:Message_Memo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mes:Message_Memo;  SolaceCtrlName = '?mes:Message_Memo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mes:Read;  SolaceCtrlName = '?mes:Read';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Message'
  OF ChangeRecord
    ActionMessage = 'Changing A Message'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMessages')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMessages')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MES:Date:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mes:Record,History::mes:Record)
  SELF.AddHistoryField(?mes:Date,2)
  SELF.AddHistoryField(?mes:Time,3)
  SELF.AddHistoryField(?mes:Message_For,4)
  SELF.AddHistoryField(?mes:Comment,6)
  SELF.AddHistoryField(?mes:Message_Memo,8)
  SELF.AddHistoryField(?mes:Read,7)
  SELF.AddUpdateFile(Access:MESSAGES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MESSAGES.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MESSAGES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Thiswindow.request = InsertRecord
      access:users.clearkey(use:password_key)
      use:password =glo:password
      if access:users.fetch(use:password_key) = Level:Benign
         mes:message_from = use:user_code
      end
      mes:message_for = ''
  Else
      access:users.clearkey(use:user_code_key)
      use:user_code = mes:message_for
      if access:users.fetch(use:user_code_key) = Level:Benign
         user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
      end
  End!If Thiswindow.request = InsertRecord
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MESSAGES.Close
    Relate:USERS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMessages',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    mes:Date = Today()
    mes:Time = Clock()
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Lookup_User
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Users
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_User, Accepted)
      case globalresponse
          of requestcompleted
              mes:message_for = use:user_code
              user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
              select(?+2)
          of requestcancelled
              mes:message_for = ''
              user_name_temp = ''
              select(?-1)
      end!case globalreponse
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_User, Accepted)
    OF ?mes:Message_For
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mes:Message_For, Accepted)
      access:users.clearkey(use:user_code_key)
      use:user_code = mes:message_for
      if access:users.fetch(use:user_code_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_users
          if globalresponse = requestcompleted
              mes:message_for = use:user_code
              user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
              Display()
          end
          globalrequest     = saverequest#
      Else
          user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
      end!if access:users.fetch(use:user_code_key)
      Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mes:Message_For, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMessages')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

