

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01045.INC'),ONCE        !Local module procedure declarations
                     END


Re_Despatch PROCEDURE                                 !Generated from procedure template - Window

job_number_temp      LONG
esn_temp             STRING(16)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Re-Submit For Despatch'),AT(,,220,170),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,72),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('WARNING! This procedure should only be used in the event of an error.'),AT(8,8,200,20),USE(?Prompt1),FONT(,10,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           PROMPT('This procedure will remove all Despatch Details from the selected job and return' &|
   ' it to a ''Ready To Despatch'' state.'),AT(8,32,204,28),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('(Note: An entry will be recorded in the Audit Trail)'),AT(8,60),USE(?Prompt3)
                         END
                       END
                       SHEET,AT(4,80,212,60),USE(?Sheet2),SPREAD
                         TAB('Re-Submit For Despatch'),USE(?Tab2)
                           PROMPT('Job Number'),AT(8,100),USE(?job_number_temp:Prompt)
                           ENTRY(@s8),AT(84,101,64,10),USE(job_number_temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('I.M.E.I. Number'),AT(8,120),USE(?esn_temp:Prompt)
                           ENTRY(@s16),AT(84,120,124,10),USE(esn_temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                         END
                       END
                       PANEL,AT(4,144,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Resubmit'),AT(8,148,56,16),USE(?Resubmit),LEFT,ICON('desp_sm.gif')
                       BUTTON('Finish'),AT(156,148,56,16),USE(?Close),LEFT,ICON('thumbs.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?job_number_temp:Prompt{prop:FontColor} = -1
    ?job_number_temp:Prompt{prop:Color} = 15066597
    If ?job_number_temp{prop:ReadOnly} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 15066597
    Elsif ?job_number_temp{prop:Req} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 8454143
    Else ! If ?job_number_temp{prop:Req} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 16777215
    End ! If ?job_number_temp{prop:Req} = True
    ?job_number_temp{prop:Trn} = 0
    ?job_number_temp{prop:FontStyle} = font:Bold
    ?esn_temp:Prompt{prop:FontColor} = -1
    ?esn_temp:Prompt{prop:Color} = 15066597
    If ?esn_temp{prop:ReadOnly} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 15066597
    Elsif ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 8454143
    Else ! If ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 16777215
    End ! If ?esn_temp{prop:Req} = True
    ?esn_temp{prop:Trn} = 0
    ?esn_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Re_Despatch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('job_number_temp',job_number_temp,'Re_Despatch',1)
    SolaceViewVars('esn_temp',esn_temp,'Re_Despatch',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_number_temp:Prompt;  SolaceCtrlName = '?job_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_number_temp;  SolaceCtrlName = '?job_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp:Prompt;  SolaceCtrlName = '?esn_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp;  SolaceCtrlName = '?esn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Resubmit;  SolaceCtrlName = '?Resubmit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Re_Despatch')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Re_Despatch')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STAHEAD.Open
  Access:JOBS.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:COURIER.UseFile
  Access:LOAN.UseFile
  Access:JOBSTAGE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STAHEAD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Re_Despatch',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Resubmit
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Resubmit, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = job_number_temp
      if access:jobs.tryfetch(job:ref_number_key)
          Case MessageEx('Cannot find selected Job Number.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
              Of 1 ! &OK Button
          End!Case MessageEx
          Select(?job_number_temp)
      Else!if access:jobs.tryfetch(job:ref_number_key)
          If esn_temp = job:esn
              despatch# = 1
              If job:despatched <> 'YES'
                  Case MessageEx('The selected Job has NOT been previously despatched. <13,10><13,10>Do you wish to mark this job as ''Ready To Despatch''','ServiceBase 2000','Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          despatch# = 0
                          Select(?job_number_temp)
                          esn_temp = ''
                  End!Case MessageEx
              End!If job:despatched <> 'YES'
              If despatch# = 1
                  get(audit,0)
                  if access:audit.primerecord() = level:benign
                      aud:notes         = 'DESPATCH DETAILS REMOVED:-<13,10,13,10>CONSIGNMENT NUMBER: ' & CLip(job:consignment_number) & |
                                          '<13,10>DESPATCH NUMBER: ' & Clip(job:despatch_number) & |
                                          '<13,10>DESPATCH USER: ' & Clip(job:despatch_user) & |
                                          '<13,10>DESPATCH DATE: ' & Clip(job:date_despatched)
                      aud:ref_number    = job:ref_number
                      aud:date          = today()
                      aud:time          = clock()
                      access:users.clearkey(use:password_key)
                      use:password =glo:password
                      access:users.fetch(use:password_key)
                      aud:user = use:user_code
                      aud:action        = 'RE-SUBMIT FOR DESPATCH: JOB'
                      access:audit.insert()
                  end!�if access:audit.primerecord() = level:benign
      
                  job:Date_Despatched = DespatchANC(job:Courier,'JOB')
      !            Include('companc.inc')
                  job:Current_Courier = job:Courier
                  job:consignment_number = ''
                  job:despatch_number = ''
                  job:despatch_user = ''
                  GetStatus(810,1,'JOB')
                  access:jobs.update()
                  job_number_temp = ''
                  esn_temp = ''
                  Select(?job_number_temp)
                  Case MessageEx('Job Updated.','ServiceBase 2000',|
                                 'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              End!If despatch# = 1
          Else!If esn_temp = job:esn
              If job:loan_unit_number <> ''
                  access:loan.clearkey(loa:ref_number_key)
                  loa:ref_number = job:loan_unit_number
                  if access:loan.tryfetch(loa:ref_number_key)
                      error# = 1
                      Case MessageEx('The selected E.S.N. / I.M.E.I. cannot be found on the selected job.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      esn_temp = ''
                      Select(?esn_temp)
                  Else!if access:loan.tryfetch(loa:ref_number_key)
                      error# = 0
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:notes         = 'DESPATCH DETAILS REMOVED:-<13,10,13,10>CONSIGNMENT NUMBER: ' & CLip(job:loan_consignment_number) & |
                                              '<13,10>DESPATCH NUMBER: ' & Clip(job:loan_despatch_number) & |
                                              '<13,10>DESPATCH USER: ' & Clip(job:loan_despatched_user) & |
                                              '<13,10>DESPATCH DATE: ' & Clip(job:loan_despatched)
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:type = 'LOA'
                          aud:action        = 'RE-SUBMIT FOR DESPATCH: LOAN UNIT'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                      job:loan_Despatched = DespatchANC(job:Loan_Courier,'LOA')
                      job:Current_Courier = job:Loan_Courier
                      job:loan_consignment_number = ''
                      job:loan_despatch_number = ''
                      job:loan_despatched_user = ''
                      !call the loan status routine
                      GetStatus(105,1,'LOA')
                      access:jobs.update()
                      job_number_temp = ''
                      esn_temp = ''
                      Select(?job_number_temp)
                      Case MessageEx('Job Updated.','ServiceBase 2000',|
                                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End!if access:loan.tryfetch(loa:ref_number_key)
              End!If job:loan_unit_number <> ''
              If job:exchange_unit_number <> ''
                  access:exchange.clearkey(xch:ref_number_key)
                  xch:ref_number  = job:exchange_unit_number
                  If access:exchange.tryfetch(xch:ref_number_key)
                      error# = 1
                      Case MessageEx('The selected I.M.E.I. number cannot be found on the selected job.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      esn_temp = ''
                      Select(?esn_temp)
                  Else!If access:exchange.tryfetch(xch:ref_number_key)
                      error# = 0
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:notes         = 'DESPATCH DETAILS REMOVED:-<13,10,13,10>CONSIGNMENT NUMBER: ' & CLip(job:exchange_consignment_number) & |
                                              '<13,10>DESPATCH NUMBER: ' & Clip(job:exchange_despatch_number) & |
                                              '<13,10>DESPATCH USER: ' & Clip(job:exchange_despatched_user) & |
                                              '<13,10>DESPATCH DATE: ' & Clip(job:exchange_despatched)
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:type = 'EXC'
                          aud:action        = 'RE-SUBMIT FOR DESPATCH: EXCHANGE UNIT'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                      job:Exchange_Despatched = DespatchANC(job:Exchange_Courier,'EXC')
                      job:Current_Courier     = job:Exchange_Courier
                      job:exchange_consignment_number = ''
                      job:exchange_despatch_number = ''
                      job:exchange_despatched_user = ''
                      !call the exchange status routine
                      GetStatus(110,1,'EXC')
                      access:jobs.update()
                      job_number_temp = ''
                      esn_temp = ''
                      Select(?job_number_temp)
                      Case MessageEx('Job Updated.','ServiceBase 2000',|
                                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End!If access:exchange.tryfetch(xch:ref_number_key)
              End!If job:exchange_unit_number <> ''
          End!If esn_temp = job:esn
      End!!if access:jobs.tryfetch(job:ref_number_key)
      Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Resubmit, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Re_Despatch')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

