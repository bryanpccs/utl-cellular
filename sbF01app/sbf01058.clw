

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01058.INC'),ONCE        !Local module procedure declarations
                     END


RoyalMail_Despatch_Routine PROCEDURE                  !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
start_consignment_number_temp LONG
current_consignment_number_temp LONG
sav:path             STRING(255)
path_temp            STRING(255)
save_job_id          USHORT,AUTO
save_cou_id          USHORT,AUTO
despatch_date_temp   DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Courier          STRING(30)
tmp:RoyalMail        STRING('ROYAL MAIL {20}')
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Royal Mail Despatch Routine'),AT(,,220,91),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,56),USE(?Sheet1),SPREAD
                         TAB('Royal Mail Despatch Routine'),USE(?Tab1)
                           PROMPT('Courier'),AT(8,24),USE(?Prompt2)
                           COMBO(@s20),AT(84,24,124,10),USE(tmp:Courier),IMM,VSCROLL,FONT(,,,FONT:bold),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Despatch Date'),AT(8,40),USE(?despatch_date_temp:Prompt)
                           ENTRY(@d6),AT(84,40,64,10),USE(despatch_date_temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,40,10,10),USE(?PopCalendar),ICON('calenda2.ico')
                         END
                       END
                       BUTTON('&Create Export'),AT(8,68,56,16),USE(?OkButton),LEFT,ICON('desp_sm.gif'),DEFAULT
                       BUTTON('Close'),AT(156,68,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,64,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
RoyalMailFile   FILE,DRIVER('BASIC','/ALWAYSQUOTE = off'),PRE(ROYM),NAME(filename3),CREATE,BINDABLE,THREAD ! Record length 1016
Record              Record
RecordID                String(2)
CustomerID              String(35)
Title                   String(5)
Initials                String(5)
Surname                 String(30)
Company                 String(30)
Address1                String(30)
Address2                String(30)
Address3                String(30)
Address4                String(30)
Postcode                String(30)
NoOfItems               String(3)
OrderRefNumber          String(35)
                    End
                End
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?tmp:Courier{prop:ReadOnly} = True
        ?tmp:Courier{prop:FontColor} = 65793
        ?tmp:Courier{prop:Color} = 15066597
    Elsif ?tmp:Courier{prop:Req} = True
        ?tmp:Courier{prop:FontColor} = 65793
        ?tmp:Courier{prop:Color} = 8454143
    Else ! If ?tmp:Courier{prop:Req} = True
        ?tmp:Courier{prop:FontColor} = 65793
        ?tmp:Courier{prop:Color} = 16777215
    End ! If ?tmp:Courier{prop:Req} = True
    ?tmp:Courier{prop:Trn} = 0
    ?tmp:Courier{prop:FontStyle} = font:Bold
    ?despatch_date_temp:Prompt{prop:FontColor} = -1
    ?despatch_date_temp:Prompt{prop:Color} = 15066597
    If ?despatch_date_temp{prop:ReadOnly} = True
        ?despatch_date_temp{prop:FontColor} = 65793
        ?despatch_date_temp{prop:Color} = 15066597
    Elsif ?despatch_date_temp{prop:Req} = True
        ?despatch_date_temp{prop:FontColor} = 65793
        ?despatch_date_temp{prop:Color} = 8454143
    Else ! If ?despatch_date_temp{prop:Req} = True
        ?despatch_date_temp{prop:FontColor} = 65793
        ?despatch_date_temp{prop:Color} = 16777215
    End ! If ?despatch_date_temp{prop:Req} = True
    ?despatch_date_temp{prop:Trn} = 0
    ?despatch_date_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RoyalMail_Despatch_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('start_consignment_number_temp',start_consignment_number_temp,'RoyalMail_Despatch_Routine',1)
    SolaceViewVars('current_consignment_number_temp',current_consignment_number_temp,'RoyalMail_Despatch_Routine',1)
    SolaceViewVars('sav:path',sav:path,'RoyalMail_Despatch_Routine',1)
    SolaceViewVars('path_temp',path_temp,'RoyalMail_Despatch_Routine',1)
    SolaceViewVars('save_job_id',save_job_id,'RoyalMail_Despatch_Routine',1)
    SolaceViewVars('save_cou_id',save_cou_id,'RoyalMail_Despatch_Routine',1)
    SolaceViewVars('despatch_date_temp',despatch_date_temp,'RoyalMail_Despatch_Routine',1)
    SolaceViewVars('tmp:Courier',tmp:Courier,'RoyalMail_Despatch_Routine',1)
    SolaceViewVars('tmp:RoyalMail',tmp:RoyalMail,'RoyalMail_Despatch_Routine',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Courier;  SolaceCtrlName = '?tmp:Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_date_temp:Prompt;  SolaceCtrlName = '?despatch_date_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_date_temp;  SolaceCtrlName = '?despatch_date_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RoyalMail_Despatch_Routine')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'RoyalMail_Despatch_Routine')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:STATUS.Open
  Access:COURIER.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:LOCINTER.UseFile
  Access:USERS.UseFile
  Access:EXCHHIST.UseFile
  Access:LOANHIST.UseFile
  Access:LOAN.UseFile
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?despatch_date_temp{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  FDCB5.Init(tmp:Courier,?tmp:Courier,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(cou:Courier_Type_Key)
  FDCB5.AddRange(cou:Courier_Type,tmp:RoyalMail)
  FDCB5.AddField(cou:Courier,FDCB5.Q.cou:Courier)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:STATUS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'RoyalMail_Despatch_Routine',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          despatch_date_temp = TINCALENDARStyle1(despatch_date_temp)
          Display(?despatch_date_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If tmp:Courier = ''
          Select(?tmp:Courier)
      Else!If tmp:Courier = ''
          Case MessageEx('Are you sure you want to create the Royal Mail export file?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
      
                  first# = 1
                  recordspercycle     = 25
                  recordsprocessed    = 0
                  percentprogress     = 0
                  setcursor(cursor:wait)
                  open(progresswindow)
                  progress:thermometer    = 0
                  ?progress:pcttext{prop:text} = '0% Completed'
                  ?progress:userstring{prop:text} = 'Writing Output File...'
                  recordstoprocess = Records(Jobs)
      
      !            save_cou_id = access:courier.savefile()
      !            access:courier.clearkey(cou:courier_type_key)
      !            cou:courier_type = 'ROYAL MAIL'
      !            set(cou:courier_type_key,cou:courier_type_key)
      !            loop
      !                if access:courier.next()
      !                   break
      !                end !if
      !                if cou:courier_type <> 'ROYAL MAIL'      |
      !                    then break.  ! end if
      !                yldcnt# += 1
      !                if yldcnt# > 25
      !                   yield() ; yldcnt# = 0
      !                end !if
      !                Do GetNextRecord2
      
                  Access:COURIER.ClearKey(cou:Courier_Key)
                  cou:Courier = tmp:Courier
                  If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
      
                      filename3 = Clip(cou:export_path) & '\RMDM.CSV'
      
                      Remove(RoyalMailFile)
      
                      error# = 0
                      Open(RoyalMailFile)
                      If Error()
                          Create(RoyalMailFile)
                          Open(RoyalMailFile)
                          If Error()
                              Stop(Error())
                              error# = 1
                          End!If Error()
                      End!If Error()
      
                      count# = 0
      
                      save_job_id = access:jobs.savefile()
                      access:jobs.clearkey(job:ReadyToCouKey)
                      job:despatched      = 'REA'
                      job:current_courier = tmp:Courier
                      set(job:ReadyToCouKey,job:ReadyToCouKey)
                      loop
                          if access:jobs.next()
                             break
                          end !if
                          if job:despatched      <> 'REA'      |
                          or job:current_courier <> tmp:Courier      |
                              then break.  ! end if
                  !            Case f_type
                  !                Of 'DELIVER'
                  !                    If job:despatch_type = 'EXC' Or job:despatch_type = 'LOA'
                  !                        If job:workshop <> 'YES'
                  !                            If job:third_party_site = ''
                  !                                Cycle
                  !                            End!If job:third_party_site <> ''
                  !                        End!If job:workshop <> 'YES'
                  !                    End!If job:despatch_type = 'EXC'
                  !                    If job:despatch_Type = 'JOB'
                  !                        If job:loan_unit_number <> ''
                  !                            Cycle
                  !                        End!If job:loan_unit_number <> ''
                  !                    End!If job:despatch_Type = 'JOB'
                  !                Of 'COLLECT'
                  !                    If job:despatch_type = 'EXC' Or job:despatch_type = 'LOA'
                  !                        If job:workshop = 'YES'
                  !                            Cycle
                  !                        Else!If job:workshop = 'YES'
                  !                            If job:third_party_site <> ''
                  !                                Cycle
                  !                            End!If job:third_party_site <> ''
                  !                        End!If job:workshop = 'YES'
                  !                    End!If job:despatch_type = 'EXC'
                  !                    If job:despatch_type = 'JOB'
                  !                        If job:loan_unit_number = ''
                  !                            Cycle
                  !                        End!If job:loan_unit_number = ''
                  !                    End!If job:despatch_type = 'JOB'
                  !            End!Case f_type
                          Case job:despatch_type
                              Of 'JOB'
                                  If job:date_despatched = despatch_date_temp
                                      count# += 1
                                  Else
                                      Cycle
                                  End!If job:date_despatched <> date_despatched_temp
                              Of 'LOA'
                                  If job:loan_despatched = despatch_date_temp
                                      count# += 1
                                  Else
                                      Cycle
                                  End!If job:date_despatched <> date_despatched_temp
      
                              Of 'EXC'
                                  If job:exchange_despatched = despatch_date_temp
                                      count# += 1
                                  Else
                                      Cycle
                                  End!If job:date_despatched <> date_despatched_temp
      
                          End!Case job:despatch_type
      
      
                          Clear(roym:Record)
                          roym:RecordID       = 'C2'
                          roym:CustomerID     = Clip(job:Ref_Number)
                          roym:Title          = StripComma(Clip(job:Title))
                          roym:Initials       = StripComma(Clip(job:Initial))
                          roym:Surname        = StripComma(Clip(job:Surname))
                          roym:Company        = StripComma(Clip(job:Company_Name_Delivery))
                          roym:Address1       = StripComma(Clip(job:Address_Line1_Delivery))
                          roym:Address2       = StripComma(Clip(job:Address_Line2_Delivery))
                          roym:Address3       = StripComma(Clip(job:Address_Line3_Delivery))
                          roym:Address4       = ''
                          roym:Postcode       = StripComma(Clip(job:Postcode_Delivery))
                          roym:NoOfItems      = '1'
                          roym:OrderRefNumber = StripComma(Clip(job:Order_Number))
                          Add(RoyalMailFile)
      
                          Case job:despatch_type
                              Of 'JOB'
                                  job:despatch_number = ''!dbt:batch_number
                                  job:despatched = 'YES'
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  job:despatch_user = use:user_code
                                  GetStatus(820,1,'JOB') !Awaiting Desp.Confirm
      
                              OF 'LOA'
                                  job:loan_despatch_number = ''!dbt:batch_number
                                  job:despatched = ''
                                  job:loan_status = 'DESPATCHED'
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  job:loan_despatched_user = use:user_code
                                  !call the loan status routine
                                  GetStatus(820,1,'LOA') !Awaiting Despatch Confirmation
      
                                  access:loan.clearkey(loa:ref_number_key)
                                  loa:ref_number = job:loan_unit_number
                                  if access:loan.fetch(loa:ref_number_key) = Level:Benign
                                      loa:available = 'DES'
                                      access:loan.update()                                        !Make Exchange Unit Available
                                      get(loanhist,0)
                                      if access:loanhist.primerecord() = Level:Benign
                                          loh:ref_number    = loa:ref_number
                                          loh:date          = Today()
                                          loh:time          = Clock()
                                          access:users.clearkey(use:password_key)
                                          use:password =glo:password
                                          access:users.fetch(use:password_key)
                                          loh:user = use:user_code
                                          loh:status        = 'UNIT DESPATCHED ON JOB: ' & CLip(job:Ref_number)
                                          if access:loanhist.insert()
                                             access:loanhist.cancelautoinc()
                                          end
                                      end!if access:exchhist.primerecord() = Level:Benign
                                  end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
      
                              Of 'EXC'
                                  job:exchange_despatch_number = ''!dbt:batch_number
                                  job:despatched = ''
                                  job:exchange_status = 'DESPATCHED'
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  job:exchange_despatched_user = use:user_code
                                  !call the exchange status routine
                                  GetStatus(820,1,'EXC') !Awaiting Despatch Confirmation
                                  access:exchange.clearkey(xch:ref_number_key)
                                  xch:ref_number = job:exchange_unit_number
                                  if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                                      xch:available = 'DES'
                                      access:exchange.update()                                        !Make Exchange Unit Available
                                      get(exchhist,0)
                                      if access:exchhist.primerecord() = Level:Benign
                                          exh:ref_number    = xch:ref_number
                                          exh:date          = Today()
                                          exh:time          = Clock()
                                          access:users.clearkey(use:password_key)
                                          use:password =glo:password
                                          access:users.fetch(use:password_key)
                                          exh:user = use:user_code
                                          exh:status        = 'UNIT DESPATCHED ON JOB: ' & CLip(job:Ref_number)
                                          if access:exchhist.insert()
                                             access:exchhist.cancelautoinc()
                                          end
                                      end!if access:exchhist.primerecord() = Level:Benign
                                  end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
      
                          End!Case job:despatch_type
                          
                          access:jobs.update()
                          If job:despatch_type = 'JOB'
                          
                              !Return Location
                              access:locinter.clearkey(loi:location_key)
                              loi:location = job:location
                              if access:locinter.fetch(loi:location_key) = Level:Benign
                                  If loi:allocate_spaces = 'YES'
                                      loi:current_spaces += 1
                                      loi:location_available = 'YES'
                                      access:locinter.update()
                                  End!If loi:allocate_spaces = 'YES'
                              end!if access:locinter.fetch(loi:location_key) = Level:Benign
      
                              print_despatch# = 0
                              access:subtracc.clearkey(sub:account_number_key)
                              sub:account_number = job:account_number
                              if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                                  access:tradeacc.clearkey(tra:account_number_key)
                                  tra:account_number = sub:main_account_number
                                  if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                                      If tra:use_sub_accounts = 'YES'
                                          If sub:print_despatch_despatch = 'YES'
                                              If SUB:despatch_note_per_item = 'YES'
                                                  print_despatch# = 1
                                              End!If tra:print_despatch_notes = 'YES'
                                          End!If sub:print_despatch_despatch = 'YES'
                                      Else!If tra:use_sub_accounts = 'YES'
                                          If tra:print_despatch_despatch = 'YES'
                                              If tra:despatch_note_per_item = 'YES'
                                                  print_despatch# = 1
                                              End!If sub:print_despatch_notes = 'YES'
                                          End!If tra:print_despatch_despatch = 'YES'
                                      End!If tra:use_sub_accounts = 'YES'
                                  end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                              end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                  !            If print_despatch# = 1
                  !                glo:select1 = job:ref_number
                  !                setcursor()
                  !                Despatch_Note
                  !                setcursor(cursor:wait)
                  !                glo:select1 = ''
                  !            End
                          End!If job:despatch_type = 'JOB'
      
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                                                      aud:ref_number    = job:ref_number
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              Case job:despatch_type
                                  Of 'JOB'
                                      aud:action        = 'ROYAL MAIL EXPORT ROUTINE: JOB EXPORTED'
                                  Of 'EXC'
                                      aud:action        = 'ROYAL MAIL EXPORT ROUTINE: EXCHANGE UNIT EXPORTED'
                                  Of 'LOA'
                                      aud:action        = 'ROYAL MAIL EXPORT ROUTINE: LOAN UNIT EXPORTED'
                              End!Case job:despatch_type
                              aud:notes         = 'AWAITING CONFIRMATION'
                              if access:audit.insert()
                                  access:audit.cancelautoinc()
                              end
                          end!if access:audit.primerecord() = level:benign
      
                      end !loop
                      access:jobs.restorefile(save_job_id)
                  !        access:expgen.close()
                  !        If count# <> 0
                  !            sav:path    = path()
                  !            setpath(Clip(cou:ancpath))
                  !            Run('ancpaper.exe',1)
                  !            Setpath(sav:path)
                  !            cou:anccount += 1
                  !            If cou:ANCCount = 999
                  !                cou:ANCCount = 1
                  !            End
                  !            access:courier.update()
                  !        End!If count# <> 0
                  End!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
      
                      Close(RoyalMailFile)
      !            end !loop
      !            access:courier.restorefile(save_cou_id)
                  setcursor()
                  close(progresswindow)
                  If count# <> 0
                      Case MessageEx('Export Completed.','ServiceBase 2000','Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
      
                  Else!If count# <> 0
                      Case MessageEx('There are no jobs to export.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End!If count# <> 0
      
              Of 2 ! &No Button
          End!Case MessageEx
      End!If tmp:Courier = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'RoyalMail_Despatch_Routine')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?despatch_date_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      despatch_date_temp = Today()
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

