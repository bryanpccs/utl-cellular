

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01026.INC'),ONCE        !Local module procedure declarations
                     END


UpdateCOMMONCP PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
save_ccp_ali_id      USHORT,AUTO
adjustment_temp      STRING(3)
quantity_temp        LONG
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?ccp:Supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
History::ccp:Record  LIKE(ccp:RECORD),STATIC
QuickWindow          WINDOW('Temporary Chargeable Part'),AT(,,289,247),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Warranty_Part'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,284,212),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Chargeable Part'),AT(8,20),USE(?Prompt8),FONT('Tahoma',12,COLOR:Navy,FONT:bold)
                           PROMPT('Part Number'),AT(8,36),USE(?ccp:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(ccp:Part_Number),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           BUTTON('Browse Stock'),AT(212,36,68,10),USE(?browse_stock_button),SKIP,LEFT,ICON('book.ico')
                           PROMPT('Description'),AT(8,52),USE(?ccp:Description:Prompt),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(ccp:Description),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Purchase Cost'),AT(8,68),USE(?CCP:Purchase_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,68,64,10),USE(ccp:Purchase_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Sale Cost'),AT(8,84),USE(?CCP:Sale_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,84,64,10),USE(ccp:Sale_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Supplier'),AT(8,100),USE(?Prompt19)
                           COMBO(@s30),AT(84,100,124,10),USE(ccp:Supplier),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Quantity'),AT(8,116),USE(?Prompt7)
                           SPIN(@s8),AT(84,116,64,10),USE(ccp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold),REQ,RANGE(1,99999999),STEP(1)
                           CHECK('Exclude From Order'),AT(84,132),USE(ccp:Exclude_From_Order),VALUE('YES','NO')
                         END
                         TAB('Fault Coding'),USE(?Fault_Code_Tab),HIDE
                           BUTTON,AT(228,24,10,10),USE(?PopCalendar),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 1:'),AT(8,24),USE(?ccp:Fault_Code1:Prompt),HIDE
                           ENTRY(@s30),AT(84,24,124,10),USE(ccp:Fault_Code1),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,24,124,10),USE(ccp:Fault_Code1,,?ccp:Fault_Code1:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 2:'),AT(8,40),USE(?ccp:Fault_Code2:Prompt),HIDE
                           ENTRY(@s30),AT(84,40,124,10),USE(ccp:Fault_Code2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,40,10,10),USE(?Button4:2),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,40,10,10),USE(?PopCalendar:2),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,24,10,10),USE(?Button4),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,40,124,10),USE(ccp:Fault_Code2,,?ccp:Fault_Code2:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 3:'),AT(8,56),USE(?ccp:Fault_Code3:Prompt),HIDE
                           ENTRY(@s30),AT(84,56,124,10),USE(ccp:Fault_Code3),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,56,10,10),USE(?Button4:3),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,56,10,10),USE(?PopCalendar:3),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,72,10,10),USE(?Button4:4),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,56,124,10),USE(ccp:Fault_Code3,,?ccp:Fault_Code3:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 4:'),AT(8,72),USE(?ccp:Fault_Code4:Prompt),HIDE
                           ENTRY(@s30),AT(84,72,124,10),USE(ccp:Fault_Code4),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,88,10,10),USE(?Button4:5),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,72,10,10),USE(?PopCalendar:4),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,104,10,10),USE(?Button4:6),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,88,10,10),USE(?PopCalendar:5),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,120,10,10),USE(?Button4:7),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 7:'),AT(8,120),USE(?ccp:Fault_Code7:Prompt),HIDE
                           ENTRY(@s30),AT(84,120,124,10),USE(ccp:Fault_Code7),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,120,10,10),USE(?PopCalendar:7),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,136,10,10),USE(?Button4:8),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,120,124,10),USE(ccp:Fault_Code7,,?ccp:Fault_Code7:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 8:'),AT(8,136),USE(?ccp:Fault_Code8:Prompt),HIDE
                           ENTRY(@s30),AT(84,136,124,10),USE(ccp:Fault_Code8),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,136,10,10),USE(?PopCalendar:8),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(228,104,10,10),USE(?PopCalendar:6),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,152,10,10),USE(?Button4:9),HIDE,ICON('list3.ico')
                           BUTTON,AT(212,168,10,10),USE(?Button4:10),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,136,124,10),USE(ccp:Fault_Code8,,?ccp:Fault_Code8:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 9:'),AT(8,152),USE(?ccp:Fault_Code9:Prompt),HIDE
                           ENTRY(@s30),AT(84,152,124,10),USE(ccp:Fault_Code9),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,152,124,10),USE(ccp:Fault_Code9,,?ccp:Fault_Code9:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 10:'),AT(8,168),USE(?ccp:Fault_Code10:Prompt),HIDE
                           ENTRY(@s30),AT(84,168,124,10),USE(ccp:Fault_Code10),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,152,10,10),USE(?PopCalendar:9),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 11:'),AT(8,184),USE(?ccp:Fault_Code11:Prompt),HIDE
                           ENTRY(@s30),AT(84,184,124,10),USE(ccp:Fault_Code11),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,168,10,10),USE(?PopCalendar:10),HIDE,ICON('Calenda2.ico')
                           ENTRY(@d6b),AT(52,168,124,10),USE(ccp:Fault_Code10,,?ccp:Fault_Code10:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 12:'),AT(8,200),USE(?ccp:Fault_Code12:Prompt),HIDE
                           ENTRY(@s30),AT(84,200,124,10),USE(ccp:Fault_Code12),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,184,10,10),USE(?PopCalendar:11),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,184,10,10),USE(?Button4:11),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,200,10,10),USE(?PopCalendar:12),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,200,10,10),USE(?Button4:12),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,184,124,10),USE(ccp:Fault_Code11,,?ccp:Fault_Code11:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,200,124,10),USE(ccp:Fault_Code12,,?ccp:Fault_Code12:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,72,124,10),USE(ccp:Fault_Code4,,?ccp:Fault_Code4:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 5:'),AT(8,88),USE(?ccp:Fault_Code5:Prompt),HIDE
                           ENTRY(@s30),AT(84,88,124,10),USE(ccp:Fault_Code5),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,88,124,10),USE(ccp:Fault_Code5,,?ccp:Fault_Code5:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 6:'),AT(8,104),USE(?ccp:Fault_Code6:Prompt),HIDE
                           ENTRY(@s30),AT(84,104,124,10),USE(ccp:Fault_Code6),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,104,124,10),USE(ccp:Fault_Code6,,?ccp:Fault_Code6:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       PANEL,AT(4,220,284,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(168,224,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(228,224,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_map_id   ushort,auto
save_war_ali_id     ushort,auto
save_partmp_ali_id     ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?ccp:Part_Number:Prompt{prop:FontColor} = -1
    ?ccp:Part_Number:Prompt{prop:Color} = 15066597
    If ?ccp:Part_Number{prop:ReadOnly} = True
        ?ccp:Part_Number{prop:FontColor} = 65793
        ?ccp:Part_Number{prop:Color} = 15066597
    Elsif ?ccp:Part_Number{prop:Req} = True
        ?ccp:Part_Number{prop:FontColor} = 65793
        ?ccp:Part_Number{prop:Color} = 8454143
    Else ! If ?ccp:Part_Number{prop:Req} = True
        ?ccp:Part_Number{prop:FontColor} = 65793
        ?ccp:Part_Number{prop:Color} = 16777215
    End ! If ?ccp:Part_Number{prop:Req} = True
    ?ccp:Part_Number{prop:Trn} = 0
    ?ccp:Part_Number{prop:FontStyle} = font:Bold
    ?ccp:Description:Prompt{prop:FontColor} = -1
    ?ccp:Description:Prompt{prop:Color} = 15066597
    If ?ccp:Description{prop:ReadOnly} = True
        ?ccp:Description{prop:FontColor} = 65793
        ?ccp:Description{prop:Color} = 15066597
    Elsif ?ccp:Description{prop:Req} = True
        ?ccp:Description{prop:FontColor} = 65793
        ?ccp:Description{prop:Color} = 8454143
    Else ! If ?ccp:Description{prop:Req} = True
        ?ccp:Description{prop:FontColor} = 65793
        ?ccp:Description{prop:Color} = 16777215
    End ! If ?ccp:Description{prop:Req} = True
    ?ccp:Description{prop:Trn} = 0
    ?ccp:Description{prop:FontStyle} = font:Bold
    ?CCP:Purchase_Cost:Prompt{prop:FontColor} = -1
    ?CCP:Purchase_Cost:Prompt{prop:Color} = 15066597
    If ?ccp:Purchase_Cost{prop:ReadOnly} = True
        ?ccp:Purchase_Cost{prop:FontColor} = 65793
        ?ccp:Purchase_Cost{prop:Color} = 15066597
    Elsif ?ccp:Purchase_Cost{prop:Req} = True
        ?ccp:Purchase_Cost{prop:FontColor} = 65793
        ?ccp:Purchase_Cost{prop:Color} = 8454143
    Else ! If ?ccp:Purchase_Cost{prop:Req} = True
        ?ccp:Purchase_Cost{prop:FontColor} = 65793
        ?ccp:Purchase_Cost{prop:Color} = 16777215
    End ! If ?ccp:Purchase_Cost{prop:Req} = True
    ?ccp:Purchase_Cost{prop:Trn} = 0
    ?ccp:Purchase_Cost{prop:FontStyle} = font:Bold
    ?CCP:Sale_Cost:Prompt{prop:FontColor} = -1
    ?CCP:Sale_Cost:Prompt{prop:Color} = 15066597
    If ?ccp:Sale_Cost{prop:ReadOnly} = True
        ?ccp:Sale_Cost{prop:FontColor} = 65793
        ?ccp:Sale_Cost{prop:Color} = 15066597
    Elsif ?ccp:Sale_Cost{prop:Req} = True
        ?ccp:Sale_Cost{prop:FontColor} = 65793
        ?ccp:Sale_Cost{prop:Color} = 8454143
    Else ! If ?ccp:Sale_Cost{prop:Req} = True
        ?ccp:Sale_Cost{prop:FontColor} = 65793
        ?ccp:Sale_Cost{prop:Color} = 16777215
    End ! If ?ccp:Sale_Cost{prop:Req} = True
    ?ccp:Sale_Cost{prop:Trn} = 0
    ?ccp:Sale_Cost{prop:FontStyle} = font:Bold
    ?Prompt19{prop:FontColor} = -1
    ?Prompt19{prop:Color} = 15066597
    If ?ccp:Supplier{prop:ReadOnly} = True
        ?ccp:Supplier{prop:FontColor} = 65793
        ?ccp:Supplier{prop:Color} = 15066597
    Elsif ?ccp:Supplier{prop:Req} = True
        ?ccp:Supplier{prop:FontColor} = 65793
        ?ccp:Supplier{prop:Color} = 8454143
    Else ! If ?ccp:Supplier{prop:Req} = True
        ?ccp:Supplier{prop:FontColor} = 65793
        ?ccp:Supplier{prop:Color} = 16777215
    End ! If ?ccp:Supplier{prop:Req} = True
    ?ccp:Supplier{prop:Trn} = 0
    ?ccp:Supplier{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?ccp:Quantity{prop:ReadOnly} = True
        ?ccp:Quantity{prop:FontColor} = 65793
        ?ccp:Quantity{prop:Color} = 15066597
    Elsif ?ccp:Quantity{prop:Req} = True
        ?ccp:Quantity{prop:FontColor} = 65793
        ?ccp:Quantity{prop:Color} = 8454143
    Else ! If ?ccp:Quantity{prop:Req} = True
        ?ccp:Quantity{prop:FontColor} = 65793
        ?ccp:Quantity{prop:Color} = 16777215
    End ! If ?ccp:Quantity{prop:Req} = True
    ?ccp:Quantity{prop:Trn} = 0
    ?ccp:Quantity{prop:FontStyle} = font:Bold
    ?ccp:Exclude_From_Order{prop:Font,3} = -1
    ?ccp:Exclude_From_Order{prop:Color} = 15066597
    ?ccp:Exclude_From_Order{prop:Trn} = 0
    ?Fault_Code_Tab{prop:Color} = 15066597
    ?ccp:Fault_Code1:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code1:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code1{prop:ReadOnly} = True
        ?ccp:Fault_Code1{prop:FontColor} = 65793
        ?ccp:Fault_Code1{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code1{prop:Req} = True
        ?ccp:Fault_Code1{prop:FontColor} = 65793
        ?ccp:Fault_Code1{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code1{prop:Req} = True
        ?ccp:Fault_Code1{prop:FontColor} = 65793
        ?ccp:Fault_Code1{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code1{prop:Req} = True
    ?ccp:Fault_Code1{prop:Trn} = 0
    ?ccp:Fault_Code1{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code1:2{prop:ReadOnly} = True
        ?ccp:Fault_Code1:2{prop:FontColor} = 65793
        ?ccp:Fault_Code1:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code1:2{prop:Req} = True
        ?ccp:Fault_Code1:2{prop:FontColor} = 65793
        ?ccp:Fault_Code1:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code1:2{prop:Req} = True
        ?ccp:Fault_Code1:2{prop:FontColor} = 65793
        ?ccp:Fault_Code1:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code1:2{prop:Req} = True
    ?ccp:Fault_Code1:2{prop:Trn} = 0
    ?ccp:Fault_Code1:2{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code2:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code2:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code2{prop:ReadOnly} = True
        ?ccp:Fault_Code2{prop:FontColor} = 65793
        ?ccp:Fault_Code2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code2{prop:Req} = True
        ?ccp:Fault_Code2{prop:FontColor} = 65793
        ?ccp:Fault_Code2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code2{prop:Req} = True
        ?ccp:Fault_Code2{prop:FontColor} = 65793
        ?ccp:Fault_Code2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code2{prop:Req} = True
    ?ccp:Fault_Code2{prop:Trn} = 0
    ?ccp:Fault_Code2{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code2:2{prop:ReadOnly} = True
        ?ccp:Fault_Code2:2{prop:FontColor} = 65793
        ?ccp:Fault_Code2:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code2:2{prop:Req} = True
        ?ccp:Fault_Code2:2{prop:FontColor} = 65793
        ?ccp:Fault_Code2:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code2:2{prop:Req} = True
        ?ccp:Fault_Code2:2{prop:FontColor} = 65793
        ?ccp:Fault_Code2:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code2:2{prop:Req} = True
    ?ccp:Fault_Code2:2{prop:Trn} = 0
    ?ccp:Fault_Code2:2{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code3:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code3:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code3{prop:ReadOnly} = True
        ?ccp:Fault_Code3{prop:FontColor} = 65793
        ?ccp:Fault_Code3{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code3{prop:Req} = True
        ?ccp:Fault_Code3{prop:FontColor} = 65793
        ?ccp:Fault_Code3{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code3{prop:Req} = True
        ?ccp:Fault_Code3{prop:FontColor} = 65793
        ?ccp:Fault_Code3{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code3{prop:Req} = True
    ?ccp:Fault_Code3{prop:Trn} = 0
    ?ccp:Fault_Code3{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code3:2{prop:ReadOnly} = True
        ?ccp:Fault_Code3:2{prop:FontColor} = 65793
        ?ccp:Fault_Code3:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code3:2{prop:Req} = True
        ?ccp:Fault_Code3:2{prop:FontColor} = 65793
        ?ccp:Fault_Code3:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code3:2{prop:Req} = True
        ?ccp:Fault_Code3:2{prop:FontColor} = 65793
        ?ccp:Fault_Code3:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code3:2{prop:Req} = True
    ?ccp:Fault_Code3:2{prop:Trn} = 0
    ?ccp:Fault_Code3:2{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code4:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code4:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code4{prop:ReadOnly} = True
        ?ccp:Fault_Code4{prop:FontColor} = 65793
        ?ccp:Fault_Code4{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code4{prop:Req} = True
        ?ccp:Fault_Code4{prop:FontColor} = 65793
        ?ccp:Fault_Code4{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code4{prop:Req} = True
        ?ccp:Fault_Code4{prop:FontColor} = 65793
        ?ccp:Fault_Code4{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code4{prop:Req} = True
    ?ccp:Fault_Code4{prop:Trn} = 0
    ?ccp:Fault_Code4{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code7:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code7:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code7{prop:ReadOnly} = True
        ?ccp:Fault_Code7{prop:FontColor} = 65793
        ?ccp:Fault_Code7{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code7{prop:Req} = True
        ?ccp:Fault_Code7{prop:FontColor} = 65793
        ?ccp:Fault_Code7{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code7{prop:Req} = True
        ?ccp:Fault_Code7{prop:FontColor} = 65793
        ?ccp:Fault_Code7{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code7{prop:Req} = True
    ?ccp:Fault_Code7{prop:Trn} = 0
    ?ccp:Fault_Code7{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code7:2{prop:ReadOnly} = True
        ?ccp:Fault_Code7:2{prop:FontColor} = 65793
        ?ccp:Fault_Code7:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code7:2{prop:Req} = True
        ?ccp:Fault_Code7:2{prop:FontColor} = 65793
        ?ccp:Fault_Code7:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code7:2{prop:Req} = True
        ?ccp:Fault_Code7:2{prop:FontColor} = 65793
        ?ccp:Fault_Code7:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code7:2{prop:Req} = True
    ?ccp:Fault_Code7:2{prop:Trn} = 0
    ?ccp:Fault_Code7:2{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code8:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code8:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code8{prop:ReadOnly} = True
        ?ccp:Fault_Code8{prop:FontColor} = 65793
        ?ccp:Fault_Code8{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code8{prop:Req} = True
        ?ccp:Fault_Code8{prop:FontColor} = 65793
        ?ccp:Fault_Code8{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code8{prop:Req} = True
        ?ccp:Fault_Code8{prop:FontColor} = 65793
        ?ccp:Fault_Code8{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code8{prop:Req} = True
    ?ccp:Fault_Code8{prop:Trn} = 0
    ?ccp:Fault_Code8{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code8:2{prop:ReadOnly} = True
        ?ccp:Fault_Code8:2{prop:FontColor} = 65793
        ?ccp:Fault_Code8:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code8:2{prop:Req} = True
        ?ccp:Fault_Code8:2{prop:FontColor} = 65793
        ?ccp:Fault_Code8:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code8:2{prop:Req} = True
        ?ccp:Fault_Code8:2{prop:FontColor} = 65793
        ?ccp:Fault_Code8:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code8:2{prop:Req} = True
    ?ccp:Fault_Code8:2{prop:Trn} = 0
    ?ccp:Fault_Code8:2{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code9:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code9:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code9{prop:ReadOnly} = True
        ?ccp:Fault_Code9{prop:FontColor} = 65793
        ?ccp:Fault_Code9{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code9{prop:Req} = True
        ?ccp:Fault_Code9{prop:FontColor} = 65793
        ?ccp:Fault_Code9{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code9{prop:Req} = True
        ?ccp:Fault_Code9{prop:FontColor} = 65793
        ?ccp:Fault_Code9{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code9{prop:Req} = True
    ?ccp:Fault_Code9{prop:Trn} = 0
    ?ccp:Fault_Code9{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code9:2{prop:ReadOnly} = True
        ?ccp:Fault_Code9:2{prop:FontColor} = 65793
        ?ccp:Fault_Code9:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code9:2{prop:Req} = True
        ?ccp:Fault_Code9:2{prop:FontColor} = 65793
        ?ccp:Fault_Code9:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code9:2{prop:Req} = True
        ?ccp:Fault_Code9:2{prop:FontColor} = 65793
        ?ccp:Fault_Code9:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code9:2{prop:Req} = True
    ?ccp:Fault_Code9:2{prop:Trn} = 0
    ?ccp:Fault_Code9:2{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code10:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code10:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code10{prop:ReadOnly} = True
        ?ccp:Fault_Code10{prop:FontColor} = 65793
        ?ccp:Fault_Code10{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code10{prop:Req} = True
        ?ccp:Fault_Code10{prop:FontColor} = 65793
        ?ccp:Fault_Code10{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code10{prop:Req} = True
        ?ccp:Fault_Code10{prop:FontColor} = 65793
        ?ccp:Fault_Code10{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code10{prop:Req} = True
    ?ccp:Fault_Code10{prop:Trn} = 0
    ?ccp:Fault_Code10{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code11:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code11:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code11{prop:ReadOnly} = True
        ?ccp:Fault_Code11{prop:FontColor} = 65793
        ?ccp:Fault_Code11{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code11{prop:Req} = True
        ?ccp:Fault_Code11{prop:FontColor} = 65793
        ?ccp:Fault_Code11{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code11{prop:Req} = True
        ?ccp:Fault_Code11{prop:FontColor} = 65793
        ?ccp:Fault_Code11{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code11{prop:Req} = True
    ?ccp:Fault_Code11{prop:Trn} = 0
    ?ccp:Fault_Code11{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code10:2{prop:ReadOnly} = True
        ?ccp:Fault_Code10:2{prop:FontColor} = 65793
        ?ccp:Fault_Code10:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code10:2{prop:Req} = True
        ?ccp:Fault_Code10:2{prop:FontColor} = 65793
        ?ccp:Fault_Code10:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code10:2{prop:Req} = True
        ?ccp:Fault_Code10:2{prop:FontColor} = 65793
        ?ccp:Fault_Code10:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code10:2{prop:Req} = True
    ?ccp:Fault_Code10:2{prop:Trn} = 0
    ?ccp:Fault_Code10:2{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code12:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code12:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code12{prop:ReadOnly} = True
        ?ccp:Fault_Code12{prop:FontColor} = 65793
        ?ccp:Fault_Code12{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code12{prop:Req} = True
        ?ccp:Fault_Code12{prop:FontColor} = 65793
        ?ccp:Fault_Code12{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code12{prop:Req} = True
        ?ccp:Fault_Code12{prop:FontColor} = 65793
        ?ccp:Fault_Code12{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code12{prop:Req} = True
    ?ccp:Fault_Code12{prop:Trn} = 0
    ?ccp:Fault_Code12{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code11:2{prop:ReadOnly} = True
        ?ccp:Fault_Code11:2{prop:FontColor} = 65793
        ?ccp:Fault_Code11:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code11:2{prop:Req} = True
        ?ccp:Fault_Code11:2{prop:FontColor} = 65793
        ?ccp:Fault_Code11:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code11:2{prop:Req} = True
        ?ccp:Fault_Code11:2{prop:FontColor} = 65793
        ?ccp:Fault_Code11:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code11:2{prop:Req} = True
    ?ccp:Fault_Code11:2{prop:Trn} = 0
    ?ccp:Fault_Code11:2{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code12:2{prop:ReadOnly} = True
        ?ccp:Fault_Code12:2{prop:FontColor} = 65793
        ?ccp:Fault_Code12:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code12:2{prop:Req} = True
        ?ccp:Fault_Code12:2{prop:FontColor} = 65793
        ?ccp:Fault_Code12:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code12:2{prop:Req} = True
        ?ccp:Fault_Code12:2{prop:FontColor} = 65793
        ?ccp:Fault_Code12:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code12:2{prop:Req} = True
    ?ccp:Fault_Code12:2{prop:Trn} = 0
    ?ccp:Fault_Code12:2{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code4:2{prop:ReadOnly} = True
        ?ccp:Fault_Code4:2{prop:FontColor} = 65793
        ?ccp:Fault_Code4:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code4:2{prop:Req} = True
        ?ccp:Fault_Code4:2{prop:FontColor} = 65793
        ?ccp:Fault_Code4:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code4:2{prop:Req} = True
        ?ccp:Fault_Code4:2{prop:FontColor} = 65793
        ?ccp:Fault_Code4:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code4:2{prop:Req} = True
    ?ccp:Fault_Code4:2{prop:Trn} = 0
    ?ccp:Fault_Code4:2{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code5:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code5:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code5{prop:ReadOnly} = True
        ?ccp:Fault_Code5{prop:FontColor} = 65793
        ?ccp:Fault_Code5{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code5{prop:Req} = True
        ?ccp:Fault_Code5{prop:FontColor} = 65793
        ?ccp:Fault_Code5{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code5{prop:Req} = True
        ?ccp:Fault_Code5{prop:FontColor} = 65793
        ?ccp:Fault_Code5{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code5{prop:Req} = True
    ?ccp:Fault_Code5{prop:Trn} = 0
    ?ccp:Fault_Code5{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code5:2{prop:ReadOnly} = True
        ?ccp:Fault_Code5:2{prop:FontColor} = 65793
        ?ccp:Fault_Code5:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code5:2{prop:Req} = True
        ?ccp:Fault_Code5:2{prop:FontColor} = 65793
        ?ccp:Fault_Code5:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code5:2{prop:Req} = True
        ?ccp:Fault_Code5:2{prop:FontColor} = 65793
        ?ccp:Fault_Code5:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code5:2{prop:Req} = True
    ?ccp:Fault_Code5:2{prop:Trn} = 0
    ?ccp:Fault_Code5:2{prop:FontStyle} = font:Bold
    ?ccp:Fault_Code6:Prompt{prop:FontColor} = -1
    ?ccp:Fault_Code6:Prompt{prop:Color} = 15066597
    If ?ccp:Fault_Code6{prop:ReadOnly} = True
        ?ccp:Fault_Code6{prop:FontColor} = 65793
        ?ccp:Fault_Code6{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code6{prop:Req} = True
        ?ccp:Fault_Code6{prop:FontColor} = 65793
        ?ccp:Fault_Code6{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code6{prop:Req} = True
        ?ccp:Fault_Code6{prop:FontColor} = 65793
        ?ccp:Fault_Code6{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code6{prop:Req} = True
    ?ccp:Fault_Code6{prop:Trn} = 0
    ?ccp:Fault_Code6{prop:FontStyle} = font:Bold
    If ?ccp:Fault_Code6:2{prop:ReadOnly} = True
        ?ccp:Fault_Code6:2{prop:FontColor} = 65793
        ?ccp:Fault_Code6:2{prop:Color} = 15066597
    Elsif ?ccp:Fault_Code6:2{prop:Req} = True
        ?ccp:Fault_Code6:2{prop:FontColor} = 65793
        ?ccp:Fault_Code6:2{prop:Color} = 8454143
    Else ! If ?ccp:Fault_Code6:2{prop:Req} = True
        ?ccp:Fault_Code6:2{prop:FontColor} = 65793
        ?ccp:Fault_Code6:2{prop:Color} = 16777215
    End ! If ?ccp:Fault_Code6:2{prop:Req} = True
    ?ccp:Fault_Code6:2{prop:Trn} = 0
    ?ccp:Fault_Code6:2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateCOMMONCP',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateCOMMONCP',1)
    SolaceViewVars('save_ccp_ali_id',save_ccp_ali_id,'UpdateCOMMONCP',1)
    SolaceViewVars('adjustment_temp',adjustment_temp,'UpdateCOMMONCP',1)
    SolaceViewVars('quantity_temp',quantity_temp,'UpdateCOMMONCP',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateCOMMONCP',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateCOMMONCP',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateCOMMONCP',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateCOMMONCP',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'UpdateCOMMONCP',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Part_Number:Prompt;  SolaceCtrlName = '?ccp:Part_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Part_Number;  SolaceCtrlName = '?ccp:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?browse_stock_button;  SolaceCtrlName = '?browse_stock_button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Description:Prompt;  SolaceCtrlName = '?ccp:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Description;  SolaceCtrlName = '?ccp:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CCP:Purchase_Cost:Prompt;  SolaceCtrlName = '?CCP:Purchase_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Purchase_Cost;  SolaceCtrlName = '?ccp:Purchase_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CCP:Sale_Cost:Prompt;  SolaceCtrlName = '?CCP:Sale_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Sale_Cost;  SolaceCtrlName = '?ccp:Sale_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt19;  SolaceCtrlName = '?Prompt19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Supplier;  SolaceCtrlName = '?ccp:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Quantity;  SolaceCtrlName = '?ccp:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Exclude_From_Order;  SolaceCtrlName = '?ccp:Exclude_From_Order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Code_Tab;  SolaceCtrlName = '?Fault_Code_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code1:Prompt;  SolaceCtrlName = '?ccp:Fault_Code1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code1;  SolaceCtrlName = '?ccp:Fault_Code1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code1:2;  SolaceCtrlName = '?ccp:Fault_Code1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code2:Prompt;  SolaceCtrlName = '?ccp:Fault_Code2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code2;  SolaceCtrlName = '?ccp:Fault_Code2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:2;  SolaceCtrlName = '?Button4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code2:2;  SolaceCtrlName = '?ccp:Fault_Code2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code3:Prompt;  SolaceCtrlName = '?ccp:Fault_Code3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code3;  SolaceCtrlName = '?ccp:Fault_Code3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:3;  SolaceCtrlName = '?Button4:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:4;  SolaceCtrlName = '?Button4:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code3:2;  SolaceCtrlName = '?ccp:Fault_Code3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code4:Prompt;  SolaceCtrlName = '?ccp:Fault_Code4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code4;  SolaceCtrlName = '?ccp:Fault_Code4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:5;  SolaceCtrlName = '?Button4:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:6;  SolaceCtrlName = '?Button4:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:7;  SolaceCtrlName = '?Button4:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code7:Prompt;  SolaceCtrlName = '?ccp:Fault_Code7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code7;  SolaceCtrlName = '?ccp:Fault_Code7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:8;  SolaceCtrlName = '?Button4:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code7:2;  SolaceCtrlName = '?ccp:Fault_Code7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code8:Prompt;  SolaceCtrlName = '?ccp:Fault_Code8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code8;  SolaceCtrlName = '?ccp:Fault_Code8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:9;  SolaceCtrlName = '?Button4:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:10;  SolaceCtrlName = '?Button4:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code8:2;  SolaceCtrlName = '?ccp:Fault_Code8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code9:Prompt;  SolaceCtrlName = '?ccp:Fault_Code9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code9;  SolaceCtrlName = '?ccp:Fault_Code9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code9:2;  SolaceCtrlName = '?ccp:Fault_Code9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code10:Prompt;  SolaceCtrlName = '?ccp:Fault_Code10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code10;  SolaceCtrlName = '?ccp:Fault_Code10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code11:Prompt;  SolaceCtrlName = '?ccp:Fault_Code11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code11;  SolaceCtrlName = '?ccp:Fault_Code11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code10:2;  SolaceCtrlName = '?ccp:Fault_Code10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code12:Prompt;  SolaceCtrlName = '?ccp:Fault_Code12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code12;  SolaceCtrlName = '?ccp:Fault_Code12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:11;  SolaceCtrlName = '?Button4:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:12;  SolaceCtrlName = '?Button4:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code11:2;  SolaceCtrlName = '?ccp:Fault_Code11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code12:2;  SolaceCtrlName = '?ccp:Fault_Code12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code4:2;  SolaceCtrlName = '?ccp:Fault_Code4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code5:Prompt;  SolaceCtrlName = '?ccp:Fault_Code5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code5;  SolaceCtrlName = '?ccp:Fault_Code5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code5:2;  SolaceCtrlName = '?ccp:Fault_Code5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code6:Prompt;  SolaceCtrlName = '?ccp:Fault_Code6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code6;  SolaceCtrlName = '?ccp:Fault_Code6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ccp:Fault_Code6:2;  SolaceCtrlName = '?ccp:Fault_Code6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Chargeable Part'
  OF ChangeRecord
    ActionMessage = 'Changing A Chargeable Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateCOMMONCP')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateCOMMONCP')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt8
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ccp:Record,History::ccp:Record)
  SELF.AddHistoryField(?ccp:Part_Number,7)
  SELF.AddHistoryField(?ccp:Description,8)
  SELF.AddHistoryField(?ccp:Purchase_Cost,10)
  SELF.AddHistoryField(?ccp:Sale_Cost,11)
  SELF.AddHistoryField(?ccp:Supplier,9)
  SELF.AddHistoryField(?ccp:Quantity,3)
  SELF.AddHistoryField(?ccp:Exclude_From_Order,6)
  SELF.AddHistoryField(?ccp:Fault_Code1,12)
  SELF.AddHistoryField(?ccp:Fault_Code1:2,12)
  SELF.AddHistoryField(?ccp:Fault_Code2,13)
  SELF.AddHistoryField(?ccp:Fault_Code2:2,13)
  SELF.AddHistoryField(?ccp:Fault_Code3,14)
  SELF.AddHistoryField(?ccp:Fault_Code3:2,14)
  SELF.AddHistoryField(?ccp:Fault_Code4,15)
  SELF.AddHistoryField(?ccp:Fault_Code7,18)
  SELF.AddHistoryField(?ccp:Fault_Code7:2,18)
  SELF.AddHistoryField(?ccp:Fault_Code8,19)
  SELF.AddHistoryField(?ccp:Fault_Code8:2,19)
  SELF.AddHistoryField(?ccp:Fault_Code9,20)
  SELF.AddHistoryField(?ccp:Fault_Code9:2,20)
  SELF.AddHistoryField(?ccp:Fault_Code10,21)
  SELF.AddHistoryField(?ccp:Fault_Code11,22)
  SELF.AddHistoryField(?ccp:Fault_Code10:2,21)
  SELF.AddHistoryField(?ccp:Fault_Code12,23)
  SELF.AddHistoryField(?ccp:Fault_Code11:2,22)
  SELF.AddHistoryField(?ccp:Fault_Code12:2,23)
  SELF.AddHistoryField(?ccp:Fault_Code4:2,15)
  SELF.AddHistoryField(?ccp:Fault_Code5,16)
  SELF.AddHistoryField(?ccp:Fault_Code5:2,16)
  SELF.AddHistoryField(?ccp:Fault_Code6,17)
  SELF.AddHistoryField(?ccp:Fault_Code6:2,17)
  SELF.AddUpdateFile(Access:COMMONCP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:COMMONCP.Open
  Relate:COMMONCP_ALIAS.Open
  Access:MANFAUPA.UseFile
  Access:MANUFACT.UseFile
  Access:STOCK.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:COMMONCP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  !Is this an adjustment?
  If ccp:Adjustment = 'YES'
      ?ccp:Part_Number{prop:Disable} = 1
      ?ccp:Description{prop:Disable} = 1
      ?ccp:Purchase_Cost{prop:Disable} = 1
      ?ccp:Sale_Cost{prop:Disable} = 1
      ?ccp:Supplier{prop:Disable} = 1
      ?ccp:Quantity{prop:Disable} = 1
      ?ccp:Exclude_From_Order{prop:Disable} = 1
      ?Browse_Stock_Button{prop:Disable} = 1
  End!If ccp:Adjustment = 'YES'
  Do RecolourWindow
  ?ccp:Fault_Code7:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code8:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code9:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code10:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code11:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code12:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code1:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code2:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code3:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code4:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code5:2{Prop:Alrt,255} = MouseLeft2
  ?ccp:Fault_Code6:2{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(ccp:Supplier,?ccp:Supplier,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sup:Company_Name_Key)
  FDCB6.AddField(sup:Company_Name,FDCB6.Q.sup:Company_Name)
  FDCB6.AddField(sup:RecordNumber,FDCB6.Q.sup:RecordNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:COMMONCP.Close
    Relate:COMMONCP_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateCOMMONCP',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ccp:Part_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ccp:Part_Number, Accepted)
      If ~0{prop:acceptall}
      access:stock.clearkey(sto:manufacturer_key)
      sto:manufacturer = glo:select11
      sto:part_number  = ccp:part_number
      if access:stock.fetch(sto:manufacturer_key)
          beep(beep:systemexclamation)  ;  yield()
          case message('This Part Number does not exist in the Stock. '&|
                  '||Do you want to select another Model Number or continue.', |
                  'ServiceBase 2000', icon:exclamation, |
                   'Select|Continue', 1, 0)
          of 1  ! name: select  (default)
              post(event:accepted,?browse_stock_button)
          of 2  ! name: continue
              Select(?ccp:description)
          end !case
      
      Else!if access:stock.fetch(sto:manufacturer_key)
          CCP:Part_Number     = sto:part_number
          CCP:Description     = sto:description
          CCP:Supplier        = sto:supplier
          CCP:Purchase_Cost   = sto:Purchase_Cost
          CCP:Sale_Cost       = sto:sale_cost
          ccp:part_ref_number = sto:ref_number
          Select(?ccp:quantity)
          display()
      end
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ccp:Part_Number, Accepted)
    OF ?browse_stock_button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?browse_stock_button, Accepted)
      access:users.clearkey(use:password_key)
      use:password    =glo:password
      access:users.fetch(use:password_key)
      
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      browse_model_stock(glo:select12,use:user_code)
      if globalresponse = requestcompleted
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = stm:ref_number
          if access:stock.fetch(sto:ref_number_key)
              beep(beep:systemhand)  ;  yield()
              message('Error! Cannot access the Stock File.', |
                      'ServiceBase 2000', icon:hand)
          Else!if access:stock.fetch(sto:ref_number_key)
              CCP:Part_Number     = sto:part_number
              CCP:Description     = sto:description
              CCP:Supplier        = sto:supplier
              CCP:Purchase_Cost   = sto:Purchase_Cost
              CCP:Sale_Cost       = sto:sale_cost
              ccp:part_ref_number = sto:ref_number
              Select(?ccp:quantity)
              display()
          end!if access:stock.fetch(sto:ref_number_key)
      end
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?browse_stock_button, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code1 = TINCALENDARStyle1(ccp:Fault_Code1)
          Display(?ccp:Fault_Code1:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:2, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 2
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code2 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:2, Accepted)
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code2 = TINCALENDARStyle1(ccp:Fault_Code2)
          Display(?ccp:Fault_Code2:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 1
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code1 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?Button4:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:3, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 3
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code3 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:3, Accepted)
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code3 = TINCALENDARStyle1(ccp:Fault_Code3)
          Display(?ccp:Fault_Code3:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:4, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 4
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code4 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:4, Accepted)
    OF ?Button4:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:5, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 5
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code5 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:5, Accepted)
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code4 = TINCALENDARStyle1(ccp:Fault_Code4)
          Display(?ccp:Fault_Code4:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:6, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 6
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code6 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:6, Accepted)
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code5 = TINCALENDARStyle1(ccp:Fault_Code5)
          Display(?ccp:Fault_Code5:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:7, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 7
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code7 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:7, Accepted)
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code7 = TINCALENDARStyle1(ccp:Fault_Code7)
          Display(?ccp:Fault_Code7:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:8, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 8
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code8 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:8, Accepted)
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code8 = TINCALENDARStyle1(ccp:Fault_Code8)
          Display(?ccp:Fault_Code8:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code6 = TINCALENDARStyle1(ccp:Fault_Code6)
          Display(?ccp:Fault_Code6:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:9, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 9
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code9 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:9, Accepted)
    OF ?Button4:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:10, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 10
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code10 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:10, Accepted)
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code9 = TINCALENDARStyle1(ccp:Fault_Code9)
          Display(?ccp:Fault_Code9:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code10 = TINCALENDARStyle1(ccp:Fault_Code10)
          Display(?ccp:Fault_Code10:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code11 = TINCALENDARStyle1(ccp:Fault_Code11)
          Display(?ccp:Fault_Code11:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:11, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 11
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code11 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:11, Accepted)
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          ccp:Fault_Code12 = TINCALENDARStyle1(ccp:Fault_Code12)
          Display(?ccp:Fault_Code12:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:12, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 12
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          ccp:fault_code12 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:12, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  found# = 0
  setcursor(cursor:wait)
  
  save_ccp_ali_id = access:commoncp_alias.savefile()
  access:commoncp_alias.clearkey(ccp_ali:refpartnumberkey)
  ccp_ali:ref_number  = ccp:ref_number
  ccp_ali:part_number = ccp:part_number
  set(ccp_ali:refpartnumberkey,ccp_ali:refpartnumberkey)
  loop
      if access:commoncp_alias.next()
         break
      end !if
      if ccp_ali:ref_number  <> ccp:ref_number      |
      or ccp_ali:part_number <> ccp:part_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      If ccp_ali:record_number <> ccp:record_number
          found# = 1
          Break
      End!If ccp_ali:record_number <> ccp:record_number
  end !loop
  access:commoncp_alias.restorefile(save_ccp_ali_id)
  setcursor()
  
  If found# = 1
      beep(beep:systemhand)  ;  yield()
      message('This part has already been selected.', |
             'ServiceBase 2000', icon:hand)
      Select(?ccp:part_number)
      Cycle
  End !If found# = 1
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateCOMMONCP')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?ccp:Fault_Code1:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?ccp:Fault_Code2:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?ccp:Fault_Code3:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?ccp:Fault_Code7:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?ccp:Fault_Code8:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?ccp:Fault_Code9:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?ccp:Fault_Code10:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?ccp:Fault_Code11:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?ccp:Fault_Code12:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  OF ?ccp:Fault_Code4:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?ccp:Fault_Code5:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?ccp:Fault_Code6:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ! Fault Coding (Hopefully)
      required# = 0
      
      found# = 0
      setcursor(cursor:wait)
      save_map_id = access:manfaupa.savefile()
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = glo:select11
      set(map:field_number_key,map:field_number_key)
      loop
          if access:manfaupa.next()
             break
          end !if
          if map:manufacturer <> glo:select11 |
              then break.  ! end if
      
          Case map:field_number
              Of 1
                  found# = 1
                  Unhide(?ccp:fault_code1:prompt)
                  ?ccp:fault_code1:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar)
                      ?popcalendar{prop:xpos} = 212
                      Unhide(?ccp:fault_code1:2)
                      ?ccp:fault_code1:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code1)
                      If map:lookup = 'YES'
                          Unhide(?button4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code1{prop:req} = 1
                      ?ccp:fault_code1:2{prop:req} = 1
                  else
                      ?ccp:fault_code1{prop:req} = 0
                      ?ccp:fault_code1:2{prop:req} = 0
                  End
              Of 2
                  found# = 1
                  Unhide(?ccp:fault_code2:prompt)
                  ?ccp:fault_code2:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:2)
                      ?popcalendar:2{prop:xpos} = 212
                      Unhide(?ccp:fault_code2:2)
                      ?ccp:fault_code2:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code2)
                      If map:lookup = 'YES'
                          Unhide(?button4:2)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code2{prop:req} = 1
                      ?ccp:fault_code2:2{prop:req} = 1
                  else
                      ?ccp:fault_code2{prop:req} = 0
                      ?ccp:fault_code2:2{prop:req} = 0
                  End
              Of 3
                  found# = 1
                  Unhide(?ccp:fault_code3:prompt)
                  ?ccp:fault_code3:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:3)
                      ?popcalendar:3{prop:xpos} = 212
                      Unhide(?ccp:fault_code3:2)
                      ?ccp:fault_code3:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code3)
                      If map:lookup = 'YES'
                          Unhide(?button4:3)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code3{prop:req} = 1
                      ?ccp:fault_code3:2{prop:req} = 1
                  else
                      ?ccp:fault_code3{prop:req} = 0
                      ?ccp:fault_code3:2{prop:req} = 0
                  End
              Of 4
                  found# = 1
                  Unhide(?ccp:fault_code4:prompt)
                  ?ccp:fault_code4:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:4)
                      ?popcalendar:4{prop:xpos} = 212
                      Unhide(?ccp:fault_code4:2)
                      ?ccp:fault_code4:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code4)
                      If map:lookup = 'YES'
                          Unhide(?button4:4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code4{prop:req} = 1
                      ?ccp:fault_code4:2{prop:req} = 1
                  else
                      ?ccp:fault_code4{prop:req} = 0
                      ?ccp:fault_code4:2{prop:req} = 0
                  End
              Of 5
                  found# = 1
                  Unhide(?ccp:fault_code5:prompt)
                  ?ccp:fault_code5:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:5)
                      ?popcalendar:5{prop:xpos} = 212
                      Unhide(?ccp:fault_code5:2)
                      ?ccp:fault_code5:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code5)
                      If map:lookup = 'YES'
                          Unhide(?button4:5)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code5{prop:req} = 1
                      ?ccp:fault_code5:2{prop:req} = 1
                  else
                      ?ccp:fault_code5{prop:req} = 0
                      ?ccp:fault_code5:2{prop:req} = 0
                  End
              Of 6
                  found# = 1
                  Unhide(?ccp:fault_code6:prompt)
                  ?ccp:fault_code6:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:6)
                      ?popcalendar:6{prop:xpos} = 212
                      Unhide(?ccp:fault_code6:2)
                      ?ccp:fault_code6:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code6)
                      If map:lookup = 'YES'
                          Unhide(?button4:6)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code6{prop:req} = 1
                      ?ccp:fault_code6:2{prop:req} = 1
                  else
                      ?ccp:fault_code6{prop:req} = 0
                      ?ccp:fault_code6:2{prop:req} = 0
                  End
              Of 7
                  found# = 1
                  Unhide(?ccp:fault_code7:prompt)
                  ?ccp:fault_code7:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:7)
                      ?popcalendar:7{prop:xpos} = 212
                      Unhide(?ccp:fault_code7:2)
                      ?ccp:fault_code7:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code7)
                      If map:lookup = 'YES'
                          Unhide(?button4:7)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code7{prop:req} = 1
                      ?ccp:fault_code7:2{prop:req} = 1
                  else
                      ?ccp:fault_code7{prop:req} = 0
                      ?ccp:fault_code7:2{prop:req} = 0
                  End
      
              Of 8
                  found# = 1
                  Unhide(?ccp:fault_code8:prompt)
                  ?ccp:fault_code8:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:8)
                      ?popcalendar:8{prop:xpos} = 212
                      Unhide(?ccp:fault_code8:2)
                      ?ccp:fault_code8:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code8)
                      If map:lookup = 'YES'
                          Unhide(?button4:8)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code8{prop:req} = 1
                      ?ccp:fault_code8:2{prop:req} = 1
                  else
                      ?ccp:fault_code8{prop:req} = 0
                      ?ccp:fault_code8:2{prop:req} = 0
                  End
      
              Of 9
                  found# = 1
                  Unhide(?ccp:fault_code9:prompt)
                  ?ccp:fault_code9:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:9)
                      ?popcalendar:9{prop:xpos} = 212
                      Unhide(?ccp:fault_code9:2)
                      ?ccp:fault_code9:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code9)
                      If map:lookup = 'YES'
                          Unhide(?button4:9)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code9{prop:req} = 1
                      ?ccp:fault_code9:2{prop:req} = 1
                  else
                      ?ccp:fault_code9{prop:req} = 0
                      ?ccp:fault_code9:2{prop:req} = 0
                  End
      
              Of 10
                  found# = 1
                  Unhide(?ccp:fault_code10:prompt)
                  ?ccp:fault_code10:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:10)
                      ?popcalendar:10{prop:xpos} = 212
                      Unhide(?ccp:fault_code10:2)
                      ?ccp:fault_code10:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code10)
                      If map:lookup = 'YES'
                          Unhide(?button4:10)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code10{prop:req} = 1
                      ?ccp:fault_code10:2{prop:req} = 1
                  else
                      ?ccp:fault_code10{prop:req} = 0
                      ?ccp:fault_code10:2{prop:req} = 0
                  End
      
              Of 11
                  found# = 1
                  Unhide(?ccp:fault_code11:prompt)
                  ?ccp:fault_code11:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:11)
                      ?popcalendar:11{prop:xpos} = 212
                      Unhide(?ccp:fault_code11:2)
                      ?ccp:fault_code11:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code11)
                      If map:lookup = 'YES'
                          Unhide(?button4:11)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code11{prop:req} = 1
                      ?ccp:fault_code11:2{prop:req} = 1
                  else
                      ?ccp:fault_code11{prop:req} = 0
                      ?ccp:fault_code11:2{prop:req} = 0
                  End
      
              Of 12
                  found# = 1
                  Unhide(?ccp:fault_code12:prompt)
                  ?ccp:fault_code12:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:12)
                      ?popcalendar:12{prop:xpos} = 212
                      Unhide(?ccp:fault_code12:2)
                      ?ccp:fault_code12:2{prop:xpos} = 84
                  Else
                      Unhide(?ccp:fault_code12)
                      If map:lookup = 'YES'
                          Unhide(?button4:12)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?ccp:fault_code12{prop:req} = 1
                      ?ccp:fault_code12:2{prop:req} = 1
                  else
                      ?ccp:fault_code12{prop:req} = 0
                      ?ccp:fault_code12:2{prop:req} = 0
                  End
      
          End !Case map:field_number
      end !loop
      access:manfaupa.restorefile(save_map_id)
      setcursor()
      
      If found# = 1
          Unhide(?fault_Code_tab)
      End!If found# = 1
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

