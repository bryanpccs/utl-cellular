

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01024.INC'),ONCE        !Local module procedure declarations
                     END


Fault_Codes_Common_Faults PROCEDURE                   !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Fault Codes'),AT(,,242,248),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:Teal),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,236,212),USE(?Sheet1),COLOR(COLOR:Silver),SPREAD
                         TAB('Fault Codes'),USE(?Tab1)
                           PROMPT('Fault Code 1:'),AT(8,20),USE(?glo:select1:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,20,124,10),USE(com:Fault_Code1),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,20,64,10),USE(com:Fault_Code1,,?COM:Fault_Code1:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(212,20,10,10),USE(?Lookup),FLAT,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,20,10,10),USE(?PopCalendar),FLAT,HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 2:'),AT(8,36),USE(?glo:select2:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,36,124,10),USE(com:Fault_Code2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,36,64,10),USE(com:Fault_Code2,,?COM:Fault_Code2:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(212,36,10,10),USE(?Lookup:2),FLAT,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,36,10,10),USE(?PopCalendar:2),FLAT,HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 3:'),AT(8,52),USE(?glo:select3:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,52,124,10),USE(com:Fault_Code3),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,52,64,10),USE(com:Fault_Code3,,?COM:Fault_Code3:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(212,52,10,10),USE(?Lookup:3),FLAT,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,52,10,10),USE(?PopCalendar:3),FLAT,HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 4:'),AT(8,68),USE(?glo:select4:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,68,124,10),USE(com:Fault_Code4),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,68,64,10),USE(com:Fault_Code4,,?COM:Fault_Code4:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(224,68,10,10),USE(?PopCalendar:4),FLAT,HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,68,10,10),USE(?Lookup:4),FLAT,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 5:'),AT(8,84),USE(?glo:select5:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,84,124,10),USE(com:Fault_Code5),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,84,64,10),USE(com:Fault_Code5,,?COM:Fault_Code5:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(224,84,10,10),USE(?PopCalendar:5),FLAT,HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,84,10,10),USE(?Lookup:5),FLAT,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 6:'),AT(8,100),USE(?glo:select6:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,100,124,10),USE(com:Fault_Code6),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,100,64,10),USE(com:Fault_Code6,,?COM:Fault_Code6:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(224,100,10,10),USE(?PopCalendar:6),FLAT,HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,100,10,10),USE(?Lookup:6),FLAT,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 7:'),AT(8,116),USE(?glo:select7:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,116,124,10),USE(com:Fault_Code7),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,116,64,10),USE(com:Fault_Code7,,?COM:Fault_Code7:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(224,116,10,10),USE(?PopCalendar:7),FLAT,HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,116,10,10),USE(?Lookup:7),FLAT,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 8:'),AT(8,132),USE(?glo:select8:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,132,124,10),USE(com:Fault_Code8),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,132,64,10),USE(com:Fault_Code8,,?COM:Fault_Code8:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(224,132,10,10),USE(?PopCalendar:8),FLAT,HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,132,10,10),USE(?Lookup:8),FLAT,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 9:'),AT(8,148),USE(?glo:select9:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,148,124,10),USE(com:Fault_Code9),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,148,64,10),USE(com:Fault_Code9,,?COM:Fault_Code9:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(224,148,10,10),USE(?PopCalendar:9),FLAT,HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,148,10,10),USE(?Lookup:9),FLAT,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 10:'),AT(8,164),USE(?glo:select10:Prompt),TRN,HIDE
                           ENTRY(@s255),AT(84,164,124,10),USE(com:Fault_Code10),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,164,64,10),USE(com:Fault_Code10,,?COM:Fault_Code10:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(224,164,10,10),USE(?PopCalendar:10),FLAT,HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,164,10,10),USE(?Lookup:10),FLAT,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 11:'),AT(8,180),USE(?glo:select11:Prompt),TRN,HIDE
                           ENTRY(@s255),AT(84,180,124,10),USE(com:Fault_Code11),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,180,64,10),USE(com:Fault_Code11,,?COM:Fault_Code11:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(224,180,10,10),USE(?PopCalendar:11),FLAT,HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,180,10,10),USE(?Lookup:11),FLAT,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 12:'),AT(8,196),USE(?glo:select12:Prompt),TRN,HIDE
                           ENTRY(@s255),AT(84,196,124,10),USE(com:Fault_Code12),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@d6b),AT(56,196,64,10),USE(com:Fault_Code12,,?COM:Fault_Code12:2),HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(224,196,10,10),USE(?PopCalendar:12),FLAT,HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,196,10,10),USE(?Lookup:12),FLAT,HIDE,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,220,236,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(180,224,56,16),USE(?OkButton),FLAT,LEFT,ICON('ok.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_maf_id     ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?glo:select1:Prompt{prop:FontColor} = -1
    ?glo:select1:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code1{prop:ReadOnly} = True
        ?com:Fault_Code1{prop:FontColor} = 65793
        ?com:Fault_Code1{prop:Color} = 15066597
    Elsif ?com:Fault_Code1{prop:Req} = True
        ?com:Fault_Code1{prop:FontColor} = 65793
        ?com:Fault_Code1{prop:Color} = 8454143
    Else ! If ?com:Fault_Code1{prop:Req} = True
        ?com:Fault_Code1{prop:FontColor} = 65793
        ?com:Fault_Code1{prop:Color} = 16777215
    End ! If ?com:Fault_Code1{prop:Req} = True
    ?com:Fault_Code1{prop:Trn} = 0
    ?com:Fault_Code1{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code1:2{prop:ReadOnly} = True
        ?COM:Fault_Code1:2{prop:FontColor} = 65793
        ?COM:Fault_Code1:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code1:2{prop:Req} = True
        ?COM:Fault_Code1:2{prop:FontColor} = 65793
        ?COM:Fault_Code1:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code1:2{prop:Req} = True
        ?COM:Fault_Code1:2{prop:FontColor} = 65793
        ?COM:Fault_Code1:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code1:2{prop:Req} = True
    ?COM:Fault_Code1:2{prop:Trn} = 0
    ?COM:Fault_Code1:2{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code2{prop:ReadOnly} = True
        ?com:Fault_Code2{prop:FontColor} = 65793
        ?com:Fault_Code2{prop:Color} = 15066597
    Elsif ?com:Fault_Code2{prop:Req} = True
        ?com:Fault_Code2{prop:FontColor} = 65793
        ?com:Fault_Code2{prop:Color} = 8454143
    Else ! If ?com:Fault_Code2{prop:Req} = True
        ?com:Fault_Code2{prop:FontColor} = 65793
        ?com:Fault_Code2{prop:Color} = 16777215
    End ! If ?com:Fault_Code2{prop:Req} = True
    ?com:Fault_Code2{prop:Trn} = 0
    ?com:Fault_Code2{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code2:2{prop:ReadOnly} = True
        ?COM:Fault_Code2:2{prop:FontColor} = 65793
        ?COM:Fault_Code2:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code2:2{prop:Req} = True
        ?COM:Fault_Code2:2{prop:FontColor} = 65793
        ?COM:Fault_Code2:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code2:2{prop:Req} = True
        ?COM:Fault_Code2:2{prop:FontColor} = 65793
        ?COM:Fault_Code2:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code2:2{prop:Req} = True
    ?COM:Fault_Code2:2{prop:Trn} = 0
    ?COM:Fault_Code2:2{prop:FontStyle} = font:Bold
    ?glo:select3:Prompt{prop:FontColor} = -1
    ?glo:select3:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code3{prop:ReadOnly} = True
        ?com:Fault_Code3{prop:FontColor} = 65793
        ?com:Fault_Code3{prop:Color} = 15066597
    Elsif ?com:Fault_Code3{prop:Req} = True
        ?com:Fault_Code3{prop:FontColor} = 65793
        ?com:Fault_Code3{prop:Color} = 8454143
    Else ! If ?com:Fault_Code3{prop:Req} = True
        ?com:Fault_Code3{prop:FontColor} = 65793
        ?com:Fault_Code3{prop:Color} = 16777215
    End ! If ?com:Fault_Code3{prop:Req} = True
    ?com:Fault_Code3{prop:Trn} = 0
    ?com:Fault_Code3{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code3:2{prop:ReadOnly} = True
        ?COM:Fault_Code3:2{prop:FontColor} = 65793
        ?COM:Fault_Code3:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code3:2{prop:Req} = True
        ?COM:Fault_Code3:2{prop:FontColor} = 65793
        ?COM:Fault_Code3:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code3:2{prop:Req} = True
        ?COM:Fault_Code3:2{prop:FontColor} = 65793
        ?COM:Fault_Code3:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code3:2{prop:Req} = True
    ?COM:Fault_Code3:2{prop:Trn} = 0
    ?COM:Fault_Code3:2{prop:FontStyle} = font:Bold
    ?glo:select4:Prompt{prop:FontColor} = -1
    ?glo:select4:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code4{prop:ReadOnly} = True
        ?com:Fault_Code4{prop:FontColor} = 65793
        ?com:Fault_Code4{prop:Color} = 15066597
    Elsif ?com:Fault_Code4{prop:Req} = True
        ?com:Fault_Code4{prop:FontColor} = 65793
        ?com:Fault_Code4{prop:Color} = 8454143
    Else ! If ?com:Fault_Code4{prop:Req} = True
        ?com:Fault_Code4{prop:FontColor} = 65793
        ?com:Fault_Code4{prop:Color} = 16777215
    End ! If ?com:Fault_Code4{prop:Req} = True
    ?com:Fault_Code4{prop:Trn} = 0
    ?com:Fault_Code4{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code4:2{prop:ReadOnly} = True
        ?COM:Fault_Code4:2{prop:FontColor} = 65793
        ?COM:Fault_Code4:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code4:2{prop:Req} = True
        ?COM:Fault_Code4:2{prop:FontColor} = 65793
        ?COM:Fault_Code4:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code4:2{prop:Req} = True
        ?COM:Fault_Code4:2{prop:FontColor} = 65793
        ?COM:Fault_Code4:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code4:2{prop:Req} = True
    ?COM:Fault_Code4:2{prop:Trn} = 0
    ?COM:Fault_Code4:2{prop:FontStyle} = font:Bold
    ?glo:select5:Prompt{prop:FontColor} = -1
    ?glo:select5:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code5{prop:ReadOnly} = True
        ?com:Fault_Code5{prop:FontColor} = 65793
        ?com:Fault_Code5{prop:Color} = 15066597
    Elsif ?com:Fault_Code5{prop:Req} = True
        ?com:Fault_Code5{prop:FontColor} = 65793
        ?com:Fault_Code5{prop:Color} = 8454143
    Else ! If ?com:Fault_Code5{prop:Req} = True
        ?com:Fault_Code5{prop:FontColor} = 65793
        ?com:Fault_Code5{prop:Color} = 16777215
    End ! If ?com:Fault_Code5{prop:Req} = True
    ?com:Fault_Code5{prop:Trn} = 0
    ?com:Fault_Code5{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code5:2{prop:ReadOnly} = True
        ?COM:Fault_Code5:2{prop:FontColor} = 65793
        ?COM:Fault_Code5:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code5:2{prop:Req} = True
        ?COM:Fault_Code5:2{prop:FontColor} = 65793
        ?COM:Fault_Code5:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code5:2{prop:Req} = True
        ?COM:Fault_Code5:2{prop:FontColor} = 65793
        ?COM:Fault_Code5:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code5:2{prop:Req} = True
    ?COM:Fault_Code5:2{prop:Trn} = 0
    ?COM:Fault_Code5:2{prop:FontStyle} = font:Bold
    ?glo:select6:Prompt{prop:FontColor} = -1
    ?glo:select6:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code6{prop:ReadOnly} = True
        ?com:Fault_Code6{prop:FontColor} = 65793
        ?com:Fault_Code6{prop:Color} = 15066597
    Elsif ?com:Fault_Code6{prop:Req} = True
        ?com:Fault_Code6{prop:FontColor} = 65793
        ?com:Fault_Code6{prop:Color} = 8454143
    Else ! If ?com:Fault_Code6{prop:Req} = True
        ?com:Fault_Code6{prop:FontColor} = 65793
        ?com:Fault_Code6{prop:Color} = 16777215
    End ! If ?com:Fault_Code6{prop:Req} = True
    ?com:Fault_Code6{prop:Trn} = 0
    ?com:Fault_Code6{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code6:2{prop:ReadOnly} = True
        ?COM:Fault_Code6:2{prop:FontColor} = 65793
        ?COM:Fault_Code6:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code6:2{prop:Req} = True
        ?COM:Fault_Code6:2{prop:FontColor} = 65793
        ?COM:Fault_Code6:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code6:2{prop:Req} = True
        ?COM:Fault_Code6:2{prop:FontColor} = 65793
        ?COM:Fault_Code6:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code6:2{prop:Req} = True
    ?COM:Fault_Code6:2{prop:Trn} = 0
    ?COM:Fault_Code6:2{prop:FontStyle} = font:Bold
    ?glo:select7:Prompt{prop:FontColor} = -1
    ?glo:select7:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code7{prop:ReadOnly} = True
        ?com:Fault_Code7{prop:FontColor} = 65793
        ?com:Fault_Code7{prop:Color} = 15066597
    Elsif ?com:Fault_Code7{prop:Req} = True
        ?com:Fault_Code7{prop:FontColor} = 65793
        ?com:Fault_Code7{prop:Color} = 8454143
    Else ! If ?com:Fault_Code7{prop:Req} = True
        ?com:Fault_Code7{prop:FontColor} = 65793
        ?com:Fault_Code7{prop:Color} = 16777215
    End ! If ?com:Fault_Code7{prop:Req} = True
    ?com:Fault_Code7{prop:Trn} = 0
    ?com:Fault_Code7{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code7:2{prop:ReadOnly} = True
        ?COM:Fault_Code7:2{prop:FontColor} = 65793
        ?COM:Fault_Code7:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code7:2{prop:Req} = True
        ?COM:Fault_Code7:2{prop:FontColor} = 65793
        ?COM:Fault_Code7:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code7:2{prop:Req} = True
        ?COM:Fault_Code7:2{prop:FontColor} = 65793
        ?COM:Fault_Code7:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code7:2{prop:Req} = True
    ?COM:Fault_Code7:2{prop:Trn} = 0
    ?COM:Fault_Code7:2{prop:FontStyle} = font:Bold
    ?glo:select8:Prompt{prop:FontColor} = -1
    ?glo:select8:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code8{prop:ReadOnly} = True
        ?com:Fault_Code8{prop:FontColor} = 65793
        ?com:Fault_Code8{prop:Color} = 15066597
    Elsif ?com:Fault_Code8{prop:Req} = True
        ?com:Fault_Code8{prop:FontColor} = 65793
        ?com:Fault_Code8{prop:Color} = 8454143
    Else ! If ?com:Fault_Code8{prop:Req} = True
        ?com:Fault_Code8{prop:FontColor} = 65793
        ?com:Fault_Code8{prop:Color} = 16777215
    End ! If ?com:Fault_Code8{prop:Req} = True
    ?com:Fault_Code8{prop:Trn} = 0
    ?com:Fault_Code8{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code8:2{prop:ReadOnly} = True
        ?COM:Fault_Code8:2{prop:FontColor} = 65793
        ?COM:Fault_Code8:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code8:2{prop:Req} = True
        ?COM:Fault_Code8:2{prop:FontColor} = 65793
        ?COM:Fault_Code8:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code8:2{prop:Req} = True
        ?COM:Fault_Code8:2{prop:FontColor} = 65793
        ?COM:Fault_Code8:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code8:2{prop:Req} = True
    ?COM:Fault_Code8:2{prop:Trn} = 0
    ?COM:Fault_Code8:2{prop:FontStyle} = font:Bold
    ?glo:select9:Prompt{prop:FontColor} = -1
    ?glo:select9:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code9{prop:ReadOnly} = True
        ?com:Fault_Code9{prop:FontColor} = 65793
        ?com:Fault_Code9{prop:Color} = 15066597
    Elsif ?com:Fault_Code9{prop:Req} = True
        ?com:Fault_Code9{prop:FontColor} = 65793
        ?com:Fault_Code9{prop:Color} = 8454143
    Else ! If ?com:Fault_Code9{prop:Req} = True
        ?com:Fault_Code9{prop:FontColor} = 65793
        ?com:Fault_Code9{prop:Color} = 16777215
    End ! If ?com:Fault_Code9{prop:Req} = True
    ?com:Fault_Code9{prop:Trn} = 0
    ?com:Fault_Code9{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code9:2{prop:ReadOnly} = True
        ?COM:Fault_Code9:2{prop:FontColor} = 65793
        ?COM:Fault_Code9:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code9:2{prop:Req} = True
        ?COM:Fault_Code9:2{prop:FontColor} = 65793
        ?COM:Fault_Code9:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code9:2{prop:Req} = True
        ?COM:Fault_Code9:2{prop:FontColor} = 65793
        ?COM:Fault_Code9:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code9:2{prop:Req} = True
    ?COM:Fault_Code9:2{prop:Trn} = 0
    ?COM:Fault_Code9:2{prop:FontStyle} = font:Bold
    ?glo:select10:Prompt{prop:FontColor} = -1
    ?glo:select10:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code10{prop:ReadOnly} = True
        ?com:Fault_Code10{prop:FontColor} = 65793
        ?com:Fault_Code10{prop:Color} = 15066597
    Elsif ?com:Fault_Code10{prop:Req} = True
        ?com:Fault_Code10{prop:FontColor} = 65793
        ?com:Fault_Code10{prop:Color} = 8454143
    Else ! If ?com:Fault_Code10{prop:Req} = True
        ?com:Fault_Code10{prop:FontColor} = 65793
        ?com:Fault_Code10{prop:Color} = 16777215
    End ! If ?com:Fault_Code10{prop:Req} = True
    ?com:Fault_Code10{prop:Trn} = 0
    ?com:Fault_Code10{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code10:2{prop:ReadOnly} = True
        ?COM:Fault_Code10:2{prop:FontColor} = 65793
        ?COM:Fault_Code10:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code10:2{prop:Req} = True
        ?COM:Fault_Code10:2{prop:FontColor} = 65793
        ?COM:Fault_Code10:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code10:2{prop:Req} = True
        ?COM:Fault_Code10:2{prop:FontColor} = 65793
        ?COM:Fault_Code10:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code10:2{prop:Req} = True
    ?COM:Fault_Code10:2{prop:Trn} = 0
    ?COM:Fault_Code10:2{prop:FontStyle} = font:Bold
    ?glo:select11:Prompt{prop:FontColor} = -1
    ?glo:select11:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code11{prop:ReadOnly} = True
        ?com:Fault_Code11{prop:FontColor} = 65793
        ?com:Fault_Code11{prop:Color} = 15066597
    Elsif ?com:Fault_Code11{prop:Req} = True
        ?com:Fault_Code11{prop:FontColor} = 65793
        ?com:Fault_Code11{prop:Color} = 8454143
    Else ! If ?com:Fault_Code11{prop:Req} = True
        ?com:Fault_Code11{prop:FontColor} = 65793
        ?com:Fault_Code11{prop:Color} = 16777215
    End ! If ?com:Fault_Code11{prop:Req} = True
    ?com:Fault_Code11{prop:Trn} = 0
    ?com:Fault_Code11{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code11:2{prop:ReadOnly} = True
        ?COM:Fault_Code11:2{prop:FontColor} = 65793
        ?COM:Fault_Code11:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code11:2{prop:Req} = True
        ?COM:Fault_Code11:2{prop:FontColor} = 65793
        ?COM:Fault_Code11:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code11:2{prop:Req} = True
        ?COM:Fault_Code11:2{prop:FontColor} = 65793
        ?COM:Fault_Code11:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code11:2{prop:Req} = True
    ?COM:Fault_Code11:2{prop:Trn} = 0
    ?COM:Fault_Code11:2{prop:FontStyle} = font:Bold
    ?glo:select12:Prompt{prop:FontColor} = -1
    ?glo:select12:Prompt{prop:Color} = 15066597
    If ?com:Fault_Code12{prop:ReadOnly} = True
        ?com:Fault_Code12{prop:FontColor} = 65793
        ?com:Fault_Code12{prop:Color} = 15066597
    Elsif ?com:Fault_Code12{prop:Req} = True
        ?com:Fault_Code12{prop:FontColor} = 65793
        ?com:Fault_Code12{prop:Color} = 8454143
    Else ! If ?com:Fault_Code12{prop:Req} = True
        ?com:Fault_Code12{prop:FontColor} = 65793
        ?com:Fault_Code12{prop:Color} = 16777215
    End ! If ?com:Fault_Code12{prop:Req} = True
    ?com:Fault_Code12{prop:Trn} = 0
    ?com:Fault_Code12{prop:FontStyle} = font:Bold
    If ?COM:Fault_Code12:2{prop:ReadOnly} = True
        ?COM:Fault_Code12:2{prop:FontColor} = 65793
        ?COM:Fault_Code12:2{prop:Color} = 15066597
    Elsif ?COM:Fault_Code12:2{prop:Req} = True
        ?COM:Fault_Code12:2{prop:FontColor} = 65793
        ?COM:Fault_Code12:2{prop:Color} = 8454143
    Else ! If ?COM:Fault_Code12:2{prop:Req} = True
        ?COM:Fault_Code12:2{prop:FontColor} = 65793
        ?COM:Fault_Code12:2{prop:Color} = 16777215
    End ! If ?COM:Fault_Code12:2{prop:Req} = True
    ?COM:Fault_Code12:2{prop:Trn} = 0
    ?COM:Fault_Code12:2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Fault_Coding        Routine   ! Manufacturers Fault Codes

    Hide(?popcalendar)
    Hide(?popcalendar:2)
    Hide(?popcalendar:3)
    Hide(?popcalendar:4)
    Hide(?popcalendar:5)
    Hide(?popcalendar:6)
    Hide(?popcalendar:7)
    Hide(?popcalendar:8)
    Hide(?popcalendar:9)
    Hide(?popcalendar:10)
    Hide(?popcalendar:11)
    Hide(?popcalendar:12)
    Hide(?lookup)
    Hide(?lookup:2)
    Hide(?lookup:3)
    Hide(?lookup:4)
    Hide(?lookup:5)
    Hide(?lookup:6)
    Hide(?lookup:7)
    Hide(?lookup:8)
    Hide(?lookup:9)
    Hide(?lookup:10)
    Hide(?lookup:11)
    Hide(?lookup:12)


    setcursor(cursor:wait)
    save_maf_id = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = glo:select11
    set(maf:field_number_key,maf:field_number_key)
    loop
        if access:manfault.next()
           break
        end !if
        if maf:manufacturer <> glo:select11      |
            then break.  ! end if
        Case maf:field_number
            Of 1
                Unhide(?glo:select1:prompt)
                ?glo:select1:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar)
                    ?popcalendar{prop:xpos} = 300
                    Unhide(?COM:Fault_Code1:2)
                    ?COM:Fault_Code1:2{prop:xpos} = 84
                Else
                    Unhide(?COM:Fault_Code1)
                    If maf:lookup = 'YES'
                        Unhide(?lookup)
                    End
                End
                If required# = 1
                    ?com:fault_code1{prop:req} = 1
                    ?com:fault_code1:2{prop:req} = 1
                Else
                    ?com:fault_code1{prop:req} = 0
                    ?com:fault_code1:2{prop:req} = 0
                End
            Of 2
                Unhide(?glo:select2:prompt)
                ?glo:select2:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:2)
                    ?popcalendar:2{prop:xpos} = 300
                    Unhide(?com:fault_code2:2)
                    ?com:fault_code2:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code2)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:2)
                    End
                End
                If required# = 1
                    ?com:fault_code2{prop:req} = 1
                    ?com:fault_code2:2{prop:req} = 1
                Else
                    ?com:fault_code2{prop:req} = 0
                    ?com:fault_code2:2{prop:req} = 0
                End

            Of 3
                Unhide(?glo:select3:prompt)
                ?glo:select3:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:3)
                    ?popcalendar:3{prop:xpos} = 300
                    Unhide(?com:fault_code3:2)
                    ?com:fault_code3:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code3)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:3)
                    End
                End
                If required# = 1
                    ?com:fault_code3{prop:req} = 1
                    ?com:fault_code3:2{prop:req} = 1
                Else
                    ?com:fault_code3{prop:req} = 0
                    ?com:fault_code3:2{prop:req} = 0
                End

            Of 4
                Unhide(?glo:select4:prompt)
                ?glo:select4:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:4)
                    ?popcalendar:4{prop:xpos} = 300
                    Unhide(?com:fault_code4:2)
                    ?com:fault_code4:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code4)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:4)
                    End
                End
                If required# = 1
                    ?com:fault_code4{prop:req} = 1
                    ?com:fault_code4:2{prop:req} = 1
                Else
                    ?com:fault_code4{prop:req} = 0
                    ?com:fault_code4:2{prop:req} = 0
                End

            Of 5
                Unhide(?glo:select5:prompt)
                ?glo:select5:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:5)
                    ?popcalendar:5{prop:xpos} = 300
                    Unhide(?com:fault_code5:2)
                    ?com:fault_code5:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code5)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:5)
                    End
                End
                If required# = 1
                    ?com:fault_code5{prop:req} = 1
                    ?com:fault_code5:2{prop:req} = 1
                Else
                    ?com:fault_code5{prop:req} = 0
                    ?com:fault_code5:2{prop:req} = 0
                End

            Of 6
                Unhide(?glo:select6:prompt)
                ?glo:select6:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:6)
                    ?popcalendar:6{prop:xpos} = 300
                    Unhide(?com:fault_code6:2)
                    ?com:fault_code6:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code6)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:6)
                    End
                End
                If required# = 1
                    ?com:fault_code6{prop:req} = 1
                    ?com:fault_code6:2{prop:req} = 1
                Else
                    ?com:fault_code6{prop:req} = 0
                    ?com:fault_code6:2{prop:req} = 0
                End

            Of 7
                Unhide(?glo:select7:prompt)
                ?glo:select7:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:7)
                    ?popcalendar:7{prop:xpos} = 300
                    Unhide(?com:fault_code7:2)
                    ?com:fault_code7:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code7)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:7)
                    End
                End
                If required# = 1
                    ?com:fault_code7{prop:req} = 1
                    ?com:fault_code7:2{prop:req} = 1
                Else
                    ?com:fault_code7{prop:req} = 0
                    ?com:fault_code7:2{prop:req} = 0
                End

            Of 8
                Unhide(?glo:select8:prompt)
                ?glo:select8:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:8)
                    ?popcalendar:8{prop:xpos} = 300
                    Unhide(?com:fault_code8:2)
                    ?com:fault_code8:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code8)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:8)
                    End
                End
                If required# = 1
                    ?com:fault_code8{prop:req} = 1
                    ?com:fault_code8:2{prop:req} = 1
                Else
                    ?com:fault_code8{prop:req} = 0
                    ?com:fault_code8:2{prop:req} = 0
                End

            Of 9
                Unhide(?glo:select9:prompt)
                ?glo:select9:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:9)
                    ?popcalendar:9{prop:xpos} = 300
                    Unhide(?com:fault_code9:2)
                    ?com:fault_code9:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code9)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:9)
                    End
                End
                If required# = 1
                    ?com:fault_code9{prop:req} = 1
                    ?com:fault_code9:2{prop:req} = 1
                Else
                    ?com:fault_code9{prop:req} = 0
                    ?com:fault_code9:2{prop:req} = 0
                End

            Of 10
                Unhide(?glo:select10:prompt)
                ?glo:select10:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:10)
                    ?popcalendar:10{prop:xpos} = 152
                    Unhide(?com:fault_code10:2)
                    ?com:fault_code10:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code10)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:10)
                    End
                End
                If required# = 1
                    ?com:fault_code10{prop:req} = 1
                    ?com:fault_code10:2{prop:req} = 1
                Else
                    ?com:fault_code10{prop:req} = 0
                    ?com:fault_code10:2{prop:req} = 0
                End

            Of 11
                Unhide(?glo:select11:prompt)
                ?glo:select11:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:11)
                    ?popcalendar:11{prop:xpos} = 152
                    Unhide(?com:fault_code11:2)
                    ?com:fault_code11:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code11)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:11)
                    End
                End
                If required# = 1
                    ?com:fault_code11{prop:req} = 1
                    ?com:fault_code11:2{prop:req} = 1
                Else
                    ?com:fault_code11{prop:req} = 0
                    ?com:fault_code11:2{prop:req} = 0
                End

            Of 12
                Unhide(?glo:select12:prompt)
                ?glo:select12:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:12)
                    ?popcalendar:12{prop:xpos} = 152
                    Unhide(?com:fault_code12:2)
                    ?com:fault_code12:2{prop:xpos} = 84
                Else
                    Unhide(?com:fault_code12)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:12)
                    End
                End
                If required# = 1
                    ?com:fault_code12{prop:req} = 1
                    ?com:fault_code12:2{prop:req} = 1
                Else
                    ?com:fault_code12{prop:req} = 0
                    ?com:fault_code12:2{prop:req} = 0
                End

        End
    end !loop
    access:manfault.restorefile(save_maf_id)
    setcursor()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Fault_Codes_Common_Faults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Fault_Codes_Common_Faults',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select1:Prompt;  SolaceCtrlName = '?glo:select1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code1;  SolaceCtrlName = '?com:Fault_Code1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code1:2;  SolaceCtrlName = '?COM:Fault_Code1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup;  SolaceCtrlName = '?Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code2;  SolaceCtrlName = '?com:Fault_Code2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code2:2;  SolaceCtrlName = '?COM:Fault_Code2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:2;  SolaceCtrlName = '?Lookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select3:Prompt;  SolaceCtrlName = '?glo:select3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code3;  SolaceCtrlName = '?com:Fault_Code3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code3:2;  SolaceCtrlName = '?COM:Fault_Code3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:3;  SolaceCtrlName = '?Lookup:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select4:Prompt;  SolaceCtrlName = '?glo:select4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code4;  SolaceCtrlName = '?com:Fault_Code4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code4:2;  SolaceCtrlName = '?COM:Fault_Code4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:4;  SolaceCtrlName = '?Lookup:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select5:Prompt;  SolaceCtrlName = '?glo:select5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code5;  SolaceCtrlName = '?com:Fault_Code5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code5:2;  SolaceCtrlName = '?COM:Fault_Code5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:5;  SolaceCtrlName = '?Lookup:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select6:Prompt;  SolaceCtrlName = '?glo:select6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code6;  SolaceCtrlName = '?com:Fault_Code6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code6:2;  SolaceCtrlName = '?COM:Fault_Code6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:6;  SolaceCtrlName = '?Lookup:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select7:Prompt;  SolaceCtrlName = '?glo:select7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code7;  SolaceCtrlName = '?com:Fault_Code7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code7:2;  SolaceCtrlName = '?COM:Fault_Code7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:7;  SolaceCtrlName = '?Lookup:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select8:Prompt;  SolaceCtrlName = '?glo:select8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code8;  SolaceCtrlName = '?com:Fault_Code8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code8:2;  SolaceCtrlName = '?COM:Fault_Code8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:8;  SolaceCtrlName = '?Lookup:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select9:Prompt;  SolaceCtrlName = '?glo:select9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code9;  SolaceCtrlName = '?com:Fault_Code9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code9:2;  SolaceCtrlName = '?COM:Fault_Code9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:9;  SolaceCtrlName = '?Lookup:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select10:Prompt;  SolaceCtrlName = '?glo:select10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code10;  SolaceCtrlName = '?com:Fault_Code10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code10:2;  SolaceCtrlName = '?COM:Fault_Code10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:10;  SolaceCtrlName = '?Lookup:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select11:Prompt;  SolaceCtrlName = '?glo:select11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code11;  SolaceCtrlName = '?com:Fault_Code11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code11:2;  SolaceCtrlName = '?COM:Fault_Code11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:11;  SolaceCtrlName = '?Lookup:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select12:Prompt;  SolaceCtrlName = '?glo:select12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Fault_Code12;  SolaceCtrlName = '?com:Fault_Code12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Fault_Code12:2;  SolaceCtrlName = '?COM:Fault_Code12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:12;  SolaceCtrlName = '?Lookup:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Fault_Codes_Common_Faults')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Fault_Codes_Common_Faults')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?glo:select1:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:COMMONFA.Open
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAULT.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ?COM:Fault_Code1:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code2:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code3:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code4:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code5:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code6:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code7:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code8:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code9:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code10:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code11:2{Prop:Alrt,255} = MouseLeft2
  ?COM:Fault_Code12:2{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:COMMONFA.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Fault_Codes_Common_Faults',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?com:Fault_Code1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code1, Accepted)
      If com:fault_code1 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 1
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 1
                  mfo:field        = com:fault_code1
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 1
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code1 = mfo:field
                      else
                          com:fault_code1 = ''
                          select(?-1)
                      end
                      display(?com:fault_code1)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If com:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code1, Accepted)
    OF ?Lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 1
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code1 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code1 = TINCALENDARStyle1(com:Fault_Code1)
          Display(?COM:Fault_Code1:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?com:Fault_Code2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code2, Accepted)
      If com:fault_code2 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 2
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 2
                  mfo:field        = com:fault_code2
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 2
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code2 = mfo:field
                      else
                          com:fault_code2 = ''
                          select(?-1)
                      end
                      display(?com:fault_code2)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code2, Accepted)
    OF ?Lookup:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 2
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code2 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code2 = TINCALENDARStyle1(com:Fault_Code2)
          Display(?COM:Fault_Code2:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?com:Fault_Code3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code3, Accepted)
      If com:fault_code3 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 3
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 3
                  mfo:field        = com:fault_code3
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 3
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code3 = mfo:field
                      else
                          com:fault_code3 = ''
                          select(?-1)
                      end
                      display(?com:fault_code3)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code3, Accepted)
    OF ?Lookup:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 3
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code3 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code3 = TINCALENDARStyle1(com:Fault_Code3)
          Display(?COM:Fault_Code3:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?com:Fault_Code4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code4, Accepted)
      If com:fault_code4 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 4
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 4
                  mfo:field        = com:fault_code4
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 4
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code4 = mfo:field
                      else
                          com:fault_code4 = ''
                          select(?-1)
                      end
                      display(?com:fault_code4)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code4, Accepted)
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code4 = TINCALENDARStyle1(com:Fault_Code4)
          Display(?COM:Fault_Code4:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 4
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code4 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
    OF ?com:Fault_Code5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code5, Accepted)
      If com:fault_code5 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 5
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 5
                  mfo:field        = com:fault_code5
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 5
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code5 = mfo:field
                      else
                          com:fault_code5 = ''
                          select(?-1)
                      end
                      display(?com:fault_code5)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code5, Accepted)
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code5 = TINCALENDARStyle1(com:Fault_Code5)
          Display(?COM:Fault_Code5:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 5
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code5 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
    OF ?com:Fault_Code6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code6, Accepted)
      If com:fault_code6 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 6
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 6
                  mfo:field        = com:fault_code6
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 6
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code6 = mfo:field
                      else
                          com:fault_code6 = ''
                          select(?-1)
                      end
                      display(?com:fault_code6)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code6, Accepted)
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code6 = TINCALENDARStyle1(com:Fault_Code6)
          Display(?COM:Fault_Code6:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 6
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code6 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
    OF ?com:Fault_Code7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code7, Accepted)
      If com:fault_code7 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 7
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 7
                  mfo:field        = com:fault_code7
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 7
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code7 = mfo:field
                      else
                          com:fault_code7 = ''
                          select(?-1)
                      end
                      display(?com:fault_code7)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code7, Accepted)
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code7 = TINCALENDARStyle1(com:Fault_Code7)
          Display(?COM:Fault_Code7:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 7
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code7 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
    OF ?com:Fault_Code8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code8, Accepted)
      If com:fault_code8 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 8
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 8
                  mfo:field        = com:fault_code8
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 8
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code8 = mfo:field
                      else
                          com:fault_code8 = ''
                          select(?-1)
                      end
                      display(?com:fault_code8)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code8, Accepted)
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code8 = TINCALENDARStyle1(com:Fault_Code8)
          Display(?COM:Fault_Code8:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 8
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code8 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
    OF ?com:Fault_Code9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code9, Accepted)
      If com:fault_code9 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 9
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 9
                  mfo:field        = com:fault_code9
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 9
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code9 = mfo:field
                      else
                          com:fault_code9 = ''
                          select(?-1)
                      end
                      display(?com:fault_code9)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code9, Accepted)
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code9 = TINCALENDARStyle1(com:Fault_Code9)
          Display(?COM:Fault_Code9:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 9
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code9 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
    OF ?com:Fault_Code10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code10, Accepted)
      If com:fault_code10<> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 10
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 10
                  mfo:field        = job:fault_code1
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 10
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code10= mfo:field
                      else
                          com:fault_code10= ''
                          select(?-1)
                      end
                      display(?com:fault_code10)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code10, Accepted)
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code10 = TINCALENDARStyle1(com:Fault_Code10)
          Display(?COM:Fault_Code10:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 10
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code10 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
    OF ?com:Fault_Code11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code11, Accepted)
      If com:fault_code11<> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 11
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 11
                  mfo:field        = com:fault_code11
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 11
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code11= mfo:field
                      else
                          com:fault_code11= ''
                          select(?-1)
                      end
                      display(?com:fault_code11)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code11, Accepted)
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code11 = TINCALENDARStyle1(com:Fault_Code11)
          Display(?COM:Fault_Code11:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 11
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code11 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
    OF ?com:Fault_Code12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code12, Accepted)
      If com:fault_code12 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select11
          maf:field_number = 12
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select11
                  mfo:field_number = 12
                  mfo:field        = com:fault_code12
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select11
                      glo:select2  = 12
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          com:fault_code12 = mfo:field
                      else
                          com:fault_code12 = ''
                          select(?-1)
                      end
                      display(?com:fault_code12)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Fault_Code12, Accepted)
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          com:Fault_Code12 = TINCALENDARStyle1(com:Fault_Code12)
          Display(?COM:Fault_Code12:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 12
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          com:fault_code12 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Fault_Codes_Common_Faults')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?COM:Fault_Code1:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?COM:Fault_Code2:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?COM:Fault_Code3:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?COM:Fault_Code4:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?COM:Fault_Code5:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?COM:Fault_Code6:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  OF ?COM:Fault_Code7:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?COM:Fault_Code8:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?COM:Fault_Code9:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?COM:Fault_Code10:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?COM:Fault_Code11:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?COM:Fault_Code12:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do Fault_Coding
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

