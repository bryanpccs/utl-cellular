

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01028.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Common_Faults PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
model_number_temp    STRING(30)
Category_Temp        STRING(30)
tmp:UseCategory      BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?model_number_temp
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Category_Temp
cmc:Category           LIKE(cmc:Category)             !List box control field - type derived from field
cmc:Record_Number      LIKE(cmc:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(COMMONFA)
                       PROJECT(com:Ref_Number)
                       PROJECT(com:Description)
                       PROJECT(com:Chargeable_Charge_Type)
                       PROJECT(com:Chargeable_Repair_Type)
                       PROJECT(com:Warranty_Charge_Type)
                       PROJECT(com:Warranty_Repair_Type)
                       PROJECT(com:Attach_Diagram)
                       PROJECT(com:Engineers_Notes)
                       PROJECT(com:Invoice_Text)
                       PROJECT(com:Model_Number)
                       PROJECT(com:Category)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
com:Ref_Number         LIKE(com:Ref_Number)           !List box control field - type derived from field
com:Description        LIKE(com:Description)          !List box control field - type derived from field
com:Chargeable_Charge_Type LIKE(com:Chargeable_Charge_Type) !List box control field - type derived from field
com:Chargeable_Repair_Type LIKE(com:Chargeable_Repair_Type) !List box control field - type derived from field
com:Warranty_Charge_Type LIKE(com:Warranty_Charge_Type) !List box control field - type derived from field
com:Warranty_Repair_Type LIKE(com:Warranty_Repair_Type) !List box control field - type derived from field
com:Attach_Diagram     LIKE(com:Attach_Diagram)       !List box control field - type derived from field
com:Engineers_Notes    LIKE(com:Engineers_Notes)      !Browse hot field - type derived from field
com:Invoice_Text       LIKE(com:Invoice_Text)         !Browse hot field - type derived from field
com:Model_Number       LIKE(com:Model_Number)         !Browse key field - type derived from field
com:Category           LIKE(com:Category)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(COMMONCP)
                       PROJECT(ccp:Description)
                       PROJECT(ccp:Part_Number)
                       PROJECT(ccp:Quantity)
                       PROJECT(ccp:Record_Number)
                       PROJECT(ccp:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
ccp:Description        LIKE(ccp:Description)          !List box control field - type derived from field
ccp:Part_Number        LIKE(ccp:Part_Number)          !List box control field - type derived from field
ccp:Quantity           LIKE(ccp:Quantity)             !List box control field - type derived from field
ccp:Record_Number      LIKE(ccp:Record_Number)        !Primary key field - type derived from field
ccp:Ref_Number         LIKE(ccp:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(COMMONWP)
                       PROJECT(cwp:Description)
                       PROJECT(cwp:Part_Number)
                       PROJECT(cwp:Quantity)
                       PROJECT(cwp:Record_Number)
                       PROJECT(cwp:Ref_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
cwp:Description        LIKE(cwp:Description)          !List box control field - type derived from field
cwp:Part_Number        LIKE(cwp:Part_Number)          !List box control field - type derived from field
cwp:Quantity           LIKE(cwp:Quantity)             !List box control field - type derived from field
cwp:Record_Number      LIKE(cwp:Record_Number)        !Primary key field - type derived from field
cwp:Ref_Number         LIKE(cwp:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB11::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
FDCB14::View:FileDropCombo VIEW(COMMCAT)
                       PROJECT(cmc:Category)
                       PROJECT(cmc:Record_Number)
                     END
QuickWindow          WINDOW('Browse The Common Faults File'),AT(,,668,348),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Common_Faults'),SYSTEM,GRAY,DOUBLE
                       PANEL,AT(4,4,576,20),USE(?Panel1:2),FILL(COLOR:Silver)
                       PROMPT('Model Number'),AT(8,8),USE(?Prompt5),COLOR(COLOR:Silver)
                       COMBO(@s30),AT(84,8,124,10),USE(model_number_temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       COMBO(@s30),AT(216,44,124,10),USE(Category_Temp),IMM,HIDE,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                       LIST,AT(8,60,568,120),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('44L(2)|M~Ref Number~@s8@124L(2)|M~Description~@s30@100L(2)|M~Charge Type~@s30@10' &|
   '0L(2)|M~Chargeable Repair Type~@s30@100L(2)|M~Warranty Charge Type~@s30@100L(2)|' &|
   'M~Warranty Repair Type~@s30@12L(2)|M~Attach Diagram~@s3@'),FROM(Queue:Browse:1)
                       BUTTON('View Diagram'),AT(588,44,76,20),USE(?DiagramButton),LEFT,ICON('spy.ico')
                       BUTTON('Replicate Fault'),AT(588,148,76,20),USE(?Replicate),LEFT,ICON('copy.gif')
                       BUTTON('&Select'),AT(588,20,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       CHECK('Select Category'),AT(140,44),USE(tmp:UseCategory),VALUE('1','0')
                       BUTTON('&Insert'),AT(588,252,76,20),USE(?Insert:3),LEFT,ICON('insert.ico')
                       BUTTON('&Change'),AT(588,276,76,20),USE(?Change:3),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(588,300,76,20),USE(?Delete:3),LEFT,ICON('delete.ico')
                       SHEET,AT(4,28,576,156),USE(?CurrentTab),SPREAD
                         TAB('By Ref Number'),USE(?Tab:2)
                           ENTRY(@s8),AT(8,44,64,10),USE(com:Ref_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By Description'),USE(?Tab:3)
                           ENTRY(@s30),AT(8,44,124,10),USE(com:Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       BUTTON('Close'),AT(588,324,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                       PROMPT('Invoice Text'),AT(300,284),USE(?COM:Invoice_Text:Prompt),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                       TEXT,AT(300,296,276,40),USE(com:Invoice_Text),SKIP,VSCROLL,FONT(,,,FONT:bold),READONLY
                       LIST,AT(8,204,272,72),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('101L(2)|M~Description~@s30@120L(2)|M~Part Number~@s30@32L(2)|M~Quantity~@s8@'),FROM(Queue:Browse)
                       LIST,AT(300,204,276,72),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('100L(2)|M~Description~@s30@120L(2)|M~Part Number~@s30@32L(2)|M~Quantity~@s8@'),FROM(Queue:Browse:2)
                       PANEL,AT(4,188,576,156),USE(?Panel1),FILL(COLOR:Silver)
                       PROMPT('Engineers Notes'),AT(8,284),USE(?COM:Engineers_Notes:Prompt),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                       PROMPT('Chargeable Parts'),AT(8,192),USE(?COM:Engineers_Notes:Prompt:2),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                       PROMPT('Warranty Parts'),AT(300,192),USE(?COM:Engineers_Notes:Prompt:3),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                       TEXT,AT(8,296,276,40),USE(com:Engineers_Notes),SKIP,VSCROLL,FONT(,,,FONT:bold),READONLY
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) =1 And Upper(Tmp:UseCategory) = 0
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) =1 And Upper(Tmp:UseCategory) = 1
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(Tmp:UseCategory) = 0
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(tmp:UseCategory) = 1
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB14               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
HK8::COM:Category         LIKE(COM:Category)
HK8::COM:Model_Number     LIKE(COM:Model_Number)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1:2{prop:Fill} = 15066597

    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?model_number_temp{prop:ReadOnly} = True
        ?model_number_temp{prop:FontColor} = 65793
        ?model_number_temp{prop:Color} = 15066597
    Elsif ?model_number_temp{prop:Req} = True
        ?model_number_temp{prop:FontColor} = 65793
        ?model_number_temp{prop:Color} = 8454143
    Else ! If ?model_number_temp{prop:Req} = True
        ?model_number_temp{prop:FontColor} = 65793
        ?model_number_temp{prop:Color} = 16777215
    End ! If ?model_number_temp{prop:Req} = True
    ?model_number_temp{prop:Trn} = 0
    ?model_number_temp{prop:FontStyle} = font:Bold
    If ?Category_Temp{prop:ReadOnly} = True
        ?Category_Temp{prop:FontColor} = 65793
        ?Category_Temp{prop:Color} = 15066597
    Elsif ?Category_Temp{prop:Req} = True
        ?Category_Temp{prop:FontColor} = 65793
        ?Category_Temp{prop:Color} = 8454143
    Else ! If ?Category_Temp{prop:Req} = True
        ?Category_Temp{prop:FontColor} = 65793
        ?Category_Temp{prop:Color} = 16777215
    End ! If ?Category_Temp{prop:Req} = True
    ?Category_Temp{prop:Trn} = 0
    ?Category_Temp{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?tmp:UseCategory{prop:Font,3} = -1
    ?tmp:UseCategory{prop:Color} = 15066597
    ?tmp:UseCategory{prop:Trn} = 0
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?com:Ref_Number{prop:ReadOnly} = True
        ?com:Ref_Number{prop:FontColor} = 65793
        ?com:Ref_Number{prop:Color} = 15066597
    Elsif ?com:Ref_Number{prop:Req} = True
        ?com:Ref_Number{prop:FontColor} = 65793
        ?com:Ref_Number{prop:Color} = 8454143
    Else ! If ?com:Ref_Number{prop:Req} = True
        ?com:Ref_Number{prop:FontColor} = 65793
        ?com:Ref_Number{prop:Color} = 16777215
    End ! If ?com:Ref_Number{prop:Req} = True
    ?com:Ref_Number{prop:Trn} = 0
    ?com:Ref_Number{prop:FontStyle} = font:Bold
    ?Tab:3{prop:Color} = 15066597
    If ?com:Description{prop:ReadOnly} = True
        ?com:Description{prop:FontColor} = 65793
        ?com:Description{prop:Color} = 15066597
    Elsif ?com:Description{prop:Req} = True
        ?com:Description{prop:FontColor} = 65793
        ?com:Description{prop:Color} = 8454143
    Else ! If ?com:Description{prop:Req} = True
        ?com:Description{prop:FontColor} = 65793
        ?com:Description{prop:Color} = 16777215
    End ! If ?com:Description{prop:Req} = True
    ?com:Description{prop:Trn} = 0
    ?com:Description{prop:FontStyle} = font:Bold
    ?COM:Invoice_Text:Prompt{prop:FontColor} = -1
    ?COM:Invoice_Text:Prompt{prop:Color} = 15066597
    If ?com:Invoice_Text{prop:ReadOnly} = True
        ?com:Invoice_Text{prop:FontColor} = 65793
        ?com:Invoice_Text{prop:Color} = 15066597
    Elsif ?com:Invoice_Text{prop:Req} = True
        ?com:Invoice_Text{prop:FontColor} = 65793
        ?com:Invoice_Text{prop:Color} = 8454143
    Else ! If ?com:Invoice_Text{prop:Req} = True
        ?com:Invoice_Text{prop:FontColor} = 65793
        ?com:Invoice_Text{prop:Color} = 16777215
    End ! If ?com:Invoice_Text{prop:Req} = True
    ?com:Invoice_Text{prop:Trn} = 0
    ?com:Invoice_Text{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    ?COM:Engineers_Notes:Prompt{prop:FontColor} = -1
    ?COM:Engineers_Notes:Prompt{prop:Color} = 15066597
    ?COM:Engineers_Notes:Prompt:2{prop:FontColor} = -1
    ?COM:Engineers_Notes:Prompt:2{prop:Color} = 15066597
    ?COM:Engineers_Notes:Prompt:3{prop:FontColor} = -1
    ?COM:Engineers_Notes:Prompt:3{prop:Color} = 15066597
    If ?com:Engineers_Notes{prop:ReadOnly} = True
        ?com:Engineers_Notes{prop:FontColor} = 65793
        ?com:Engineers_Notes{prop:Color} = 15066597
    Elsif ?com:Engineers_Notes{prop:Req} = True
        ?com:Engineers_Notes{prop:FontColor} = 65793
        ?com:Engineers_Notes{prop:Color} = 8454143
    Else ! If ?com:Engineers_Notes{prop:Req} = True
        ?com:Engineers_Notes{prop:FontColor} = 65793
        ?com:Engineers_Notes{prop:Color} = 16777215
    End ! If ?com:Engineers_Notes{prop:Req} = True
    ?com:Engineers_Notes{prop:Trn} = 0
    ?com:Engineers_Notes{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Refresh_Browse      Routine
!    com:model_number    = model_number_temp
!    brw1.addrange(com:category,category_temp)
!    brw1.applyrange
!    thiswindow.reset(1)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Common_Faults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Common_Faults',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Common_Faults',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Browse_Common_Faults',1)
    SolaceViewVars('Category_Temp',Category_Temp,'Browse_Common_Faults',1)
    SolaceViewVars('tmp:UseCategory',tmp:UseCategory,'Browse_Common_Faults',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1:2;  SolaceCtrlName = '?Panel1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?model_number_temp;  SolaceCtrlName = '?model_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Category_Temp;  SolaceCtrlName = '?Category_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DiagramButton;  SolaceCtrlName = '?DiagramButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Replicate;  SolaceCtrlName = '?Replicate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:UseCategory;  SolaceCtrlName = '?tmp:UseCategory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Ref_Number;  SolaceCtrlName = '?com:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Description;  SolaceCtrlName = '?com:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Invoice_Text:Prompt;  SolaceCtrlName = '?COM:Invoice_Text:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Invoice_Text;  SolaceCtrlName = '?com:Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Engineers_Notes:Prompt;  SolaceCtrlName = '?COM:Engineers_Notes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Engineers_Notes:Prompt:2;  SolaceCtrlName = '?COM:Engineers_Notes:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Engineers_Notes:Prompt:3;  SolaceCtrlName = '?COM:Engineers_Notes:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Engineers_Notes;  SolaceCtrlName = '?com:Engineers_Notes';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Common_Faults')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Common_Faults')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:COMMCAT.Open
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  If glo:select1 <> ''
      model_number_temp = glo:select1
  End!If glo:select1 <> ''
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:COMMONFA,SELF)
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:COMMONCP,SELF)
  BRW7.Init(?List:2,Queue:Browse:2.ViewPosition,BRW7::View:Browse,Queue:Browse:2,Relate:COMMONWP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,com:RefOnlyKey)
  BRW1.AddRange(com:Model_Number,model_number_temp)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?com:Ref_Number,com:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,com:Ref_Model_Key)
  BRW1.AddRange(com:Category,Category_Temp)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?com:Ref_Number,com:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,com:DescripOnlyKey)
  BRW1.AddRange(com:Model_Number,model_number_temp)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?com:Description,com:Description,1,BRW1)
  BRW1.AddSortOrder(,com:Description_Key)
  BRW1.AddRange(com:Category,Category_Temp)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?com:Description,com:Description,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,com:Description_Key)
  BRW1.AddRange(com:Category,Category_Temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?COM:Description,com:Description,1,BRW1)
  BIND('model_number_temp',model_number_temp)
  BIND('Category_Temp',Category_Temp)
  BIND('tmp:UseCategory',tmp:UseCategory)
  BRW1.AddField(com:Ref_Number,BRW1.Q.com:Ref_Number)
  BRW1.AddField(com:Description,BRW1.Q.com:Description)
  BRW1.AddField(com:Chargeable_Charge_Type,BRW1.Q.com:Chargeable_Charge_Type)
  BRW1.AddField(com:Chargeable_Repair_Type,BRW1.Q.com:Chargeable_Repair_Type)
  BRW1.AddField(com:Warranty_Charge_Type,BRW1.Q.com:Warranty_Charge_Type)
  BRW1.AddField(com:Warranty_Repair_Type,BRW1.Q.com:Warranty_Repair_Type)
  BRW1.AddField(com:Attach_Diagram,BRW1.Q.com:Attach_Diagram)
  BRW1.AddField(com:Engineers_Notes,BRW1.Q.com:Engineers_Notes)
  BRW1.AddField(com:Invoice_Text,BRW1.Q.com:Invoice_Text)
  BRW1.AddField(com:Model_Number,BRW1.Q.com:Model_Number)
  BRW1.AddField(com:Category,BRW1.Q.com:Category)
  BRW6.Q &= Queue:Browse
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,ccp:Ref_Number_Key)
  BRW6.AddRange(ccp:Ref_Number,Relate:COMMONCP,Relate:COMMONFA)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,ccp:Ref_Number,1,BRW6)
  BRW6.AddField(ccp:Description,BRW6.Q.ccp:Description)
  BRW6.AddField(ccp:Part_Number,BRW6.Q.ccp:Part_Number)
  BRW6.AddField(ccp:Quantity,BRW6.Q.ccp:Quantity)
  BRW6.AddField(ccp:Record_Number,BRW6.Q.ccp:Record_Number)
  BRW6.AddField(ccp:Ref_Number,BRW6.Q.ccp:Ref_Number)
  BRW7.Q &= Queue:Browse:2
  BRW7.RetainRow = 0
  BRW7.AddSortOrder(,cwp:Ref_Number_Key)
  BRW7.AddRange(cwp:Ref_Number,Relate:COMMONWP,Relate:COMMONFA)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,cwp:Ref_Number,1,BRW7)
  BRW7.AddField(cwp:Description,BRW7.Q.cwp:Description)
  BRW7.AddField(cwp:Part_Number,BRW7.Q.cwp:Part_Number)
  BRW7.AddField(cwp:Quantity,BRW7.Q.cwp:Quantity)
  BRW7.AddField(cwp:Record_Number,BRW7.Q.cwp:Record_Number)
  BRW7.AddField(cwp:Ref_Number,BRW7.Q.cwp:Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?tmp:UseCategory{Prop:Checked} = True
    UNHIDE(?Category_Temp)
  END
  IF ?tmp:UseCategory{Prop:Checked} = False
    HIDE(?Category_Temp)
  END
  BRW1.AskProcedure = 1
  FDCB11.Init(model_number_temp,?model_number_temp,Queue:FileDropCombo.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo
  FDCB11.AddSortOrder(mod:Model_Number_Key)
  FDCB11.AddField(mod:Model_Number,FDCB11.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  FDCB14.Init(Category_Temp,?Category_Temp,Queue:FileDropCombo:1.ViewPosition,FDCB14::View:FileDropCombo,Queue:FileDropCombo:1,Relate:COMMCAT,ThisWindow,GlobalErrors,0,1,0)
  FDCB14.Q &= Queue:FileDropCombo:1
  FDCB14.AddSortOrder(cmc:Category_Key)
  FDCB14.AddRange(cmc:Model_Number,model_number_temp)
  FDCB14.AddField(cmc:Category,FDCB14.Q.cmc:Category)
  FDCB14.AddField(cmc:Record_Number,FDCB14.Q.cmc:Record_Number)
  ThisWindow.AddItem(FDCB14.WindowComponent)
  FDCB14.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Ref Number'
    ?Tab:3{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='44L(2)|M~Ref Number~@s8@#1#124L(2)|M~Description~@s30@#2#100L(2)|M~Charge Type~@s30@#3#100L(2)|M~Chargeable Repair Type~@s30@#4#100L(2)|M~Warranty Charge Type~@s30@#5#100L(2)|M~Warranty Repair Type~@s30@#6#12L(2)|M~Attach Diagram~@s3@#7#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COMMCAT.Close
    Relate:MODELNUM.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Common_Faults',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of insertrecord
          check_access('COMMON FAULTS - INSERT',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
              access:commonfa.cancelautoinc()
          end
      of changerecord
          check_access('COMMON FAULTS - CHANGE',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
          end
      of deleterecord
          check_access('COMMON FAULTS - DELETE',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
          end
  end !case request
  
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Common_Faults
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  BRW6.ResetSort(1)
  BRW7.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?model_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?model_number_temp, Accepted)
        BRW1.ApplyRange
        BRW1.ResetSort(1)
        Select(?Browse:1)
      FDCB14.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?model_number_temp, Accepted)
    OF ?Category_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Category_Temp, Accepted)
        BRW1.ApplyRange
        BRW1.ResetSort(1)
        Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Category_Temp, Accepted)
    OF ?DiagramButton
      ThisWindow.Update
      View_Diagram
      ThisWindow.Reset
    OF ?Replicate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate, Accepted)
      Thiswindow.Reset(1)
      glo:select1 = com:ref_number
      If com:ref_number <> ''
          Replicate_Common_Faults
      End!If com:ref_number <> ''
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate, Accepted)
    OF ?tmp:UseCategory
      IF ?tmp:UseCategory{Prop:Checked} = True
        UNHIDE(?Category_Temp)
      END
      IF ?tmp:UseCategory{Prop:Checked} = False
        HIDE(?Category_Temp)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:UseCategory, Accepted)
        BRW1.ApplyRange
        BRW1.ResetSort(1)
        Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:UseCategory, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Common_Faults')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      DO refresh_browse
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='44L(2)|M~Ref Number~@s8@#1#124L(2)|M~Description~@s30@#2#100L(2)|M~Charge Type~@s30@#3#100L(2)|M~Chargeable Repair Type~@s30@#4#100L(2)|M~Warranty Charge Type~@s30@#5#100L(2)|M~Warranty Repair Type~@s30@#6#12L(2)|M~Attach Diagram~@s3@#7#'
          ?Tab:2{PROP:TEXT} = 'By Ref Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Description~@s30@#2#44L(2)|M~Ref Number~@s8@#1#100L(2)|M~Charge Type~@s30@#3#100L(2)|M~Chargeable Repair Type~@s30@#4#100L(2)|M~Warranty Charge Type~@s30@#5#100L(2)|M~Warranty Repair Type~@s30@#6#12L(2)|M~Attach Diagram~@s3@#7#'
          ?Tab:3{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?com:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Ref_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Ref_Number, Selected)
    OF ?com:Description
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Description, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Description, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      BRW6.ResetSort(1)
      BRW7.ResetSort(1)
      FDCB11.ResetQueue(1)
      FDCB14.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ApplyRange, (),BYTE)
  ReturnValue = PARENT.ApplyRange()
    IF Choice(?CurrentTab) =1 And Upper(Tmp:UseCategory) = 0
       GET(SELF.Order.RangeList.List,1)
       Self.Order.RangeList.List.Right = model_number_temp
    ELSIF Choice(?CurrentTab) =1 And Upper(Tmp:UseCategory) = 1
       GET(SELF.Order.RangeList.List,1)
       Self.Order.RangeList.List.Right = model_number_temp
       GET(SELF.Order.RangeList.List,2)
       Self.Order.RangeList.List.Right = Category_Temp
    ELSIF Choice(?CurrentTab) = 2 And Upper(Tmp:UseCategory) = 0
       GET(SELF.Order.RangeList.List,1)
       Self.Order.RangeList.List.Right = model_number_temp
    ELSE
       GET(SELF.Order.RangeList.List,1)
       Self.Order.RangeList.List.Right = model_number_temp
       GET(SELF.Order.RangeList.List,2)
       Self.Order.RangeList.List.Right = Category_Temp
    END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ApplyRange, (),BYTE)
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) =1 And Upper(Tmp:UseCategory) = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) =1 And Upper(Tmp:UseCategory) = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(Tmp:UseCategory) = 0
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(tmp:UseCategory) = 1
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  PARENT.SetQueueRecord
  If com:attach_diagram = 'YES'
      Enable(?DiagramButton)
  Else!If Records(queue:browse:1)
      Disable(?DiagramButton)
  End!If Records(queue:browse:1)
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

