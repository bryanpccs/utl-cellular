

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01039.INC'),ONCE        !Local module procedure declarations
                     END


JB:ComputerName CSTRING(255)
JB:ComputerLen  ULONG(255)
Rapid_Job_Update PROCEDURE                            !Generated from procedure template - Window

FilesOpened          BYTE
save_job_ali_id      USHORT,AUTO
sav:path             STRING(255)
tmp:accountnumber    STRING(20)
tmp:consignno        STRING(20)
tmp:oldconsignno     STRING(20)
tmp:labelerror       STRING('0 {29}')
save_cou_ali_id      USHORT,AUTO
account_number2_temp STRING(20)
job_number_temp      REAL
tmp:esn              STRING(16)
tmp:ParcelLineName   STRING(255),STATIC
tmp:WorkstationName  STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Rapid Jobs Update'),AT(,,219,83),FONT('Tahoma',8,,FONT:regular),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,212,48),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Job Number'),AT(8,20),USE(?Prompt1)
                           ENTRY(@p<<<<<<<<#pb),AT(88,20,64,10),USE(job_number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('I.M.E.I. Number'),AT(8,36),USE(?tmp:esn:Prompt)
                           ENTRY(@s16),AT(88,36,124,10),USE(tmp:esn),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(4,56,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,60,56,16),USE(?OK),LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(156,60,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
OutFile FILE,DRIVER('ASCII'),PRE(OUF),NAME(filename3),CREATE,BINDABLE,THREAD
Record  Record
line1       String(255)
            End
        End
Include('ParcDef.inc')
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?job_number_temp{prop:ReadOnly} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 15066597
    Elsif ?job_number_temp{prop:Req} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 8454143
    Else ! If ?job_number_temp{prop:Req} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 16777215
    End ! If ?job_number_temp{prop:Req} = True
    ?job_number_temp{prop:Trn} = 0
    ?job_number_temp{prop:FontStyle} = font:Bold
    ?tmp:esn:Prompt{prop:FontColor} = -1
    ?tmp:esn:Prompt{prop:Color} = 15066597
    If ?tmp:esn{prop:ReadOnly} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 15066597
    Elsif ?tmp:esn{prop:Req} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 8454143
    Else ! If ?tmp:esn{prop:Req} = True
        ?tmp:esn{prop:FontColor} = 65793
        ?tmp:esn{prop:Color} = 16777215
    End ! If ?tmp:esn{prop:Req} = True
    ?tmp:esn{prop:Trn} = 0
    ?tmp:esn{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Rapid_Job_Update',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Rapid_Job_Update',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Rapid_Job_Update',1)
    SolaceViewVars('sav:path',sav:path,'Rapid_Job_Update',1)
    SolaceViewVars('tmp:accountnumber',tmp:accountnumber,'Rapid_Job_Update',1)
    SolaceViewVars('tmp:consignno',tmp:consignno,'Rapid_Job_Update',1)
    SolaceViewVars('tmp:oldconsignno',tmp:oldconsignno,'Rapid_Job_Update',1)
    SolaceViewVars('tmp:labelerror',tmp:labelerror,'Rapid_Job_Update',1)
    SolaceViewVars('save_cou_ali_id',save_cou_ali_id,'Rapid_Job_Update',1)
    SolaceViewVars('account_number2_temp',account_number2_temp,'Rapid_Job_Update',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Rapid_Job_Update',1)
    SolaceViewVars('tmp:esn',tmp:esn,'Rapid_Job_Update',1)
    SolaceViewVars('tmp:ParcelLineName',tmp:ParcelLineName,'Rapid_Job_Update',1)
    SolaceViewVars('tmp:WorkstationName',tmp:WorkstationName,'Rapid_Job_Update',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_number_temp;  SolaceCtrlName = '?job_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn:Prompt;  SolaceCtrlName = '?tmp:esn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:esn;  SolaceCtrlName = '?tmp:esn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Rapid_Job_Update')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Rapid_Job_Update')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STAHEAD.Open
  Relate:VATCODE.Open
  Access:JOBS.UseFile
  Access:STATUS.UseFile
  Access:LOAN.UseFile
  Access:COURIER.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STAHEAD.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Rapid_Job_Update',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:esn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
      IF LEN(CLIP(tmp:esn)) = 18
        !Ericsson IMEI!
        tmp:esn = SUB(tmp:esn,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      error# = 0
      If job_number_Temp = ''
          Select(?Job_number_temp)
          error# = 1
      End!If job_number_Temp = ''
      If def:validateesn = 'YES' and error# = 0
          If tmp:esn = ''
              Select(?tmp:esn)
              error# = 1
          End!If tmp:esn = ''
      End!If def:validateesn = 'YES'
      If error# = 0
          continue# = 0
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number = job_number_temp
          If access:jobs.fetch(job:ref_number_key) = Level:Benign
              If def:validateesn = 'YES'
                  If job:esn <> tmp:esn
                      error# = 0
                      print_at_despatch# = 0
                      access:subtracc.clearkey(sub:account_number_key)
                      sub:account_number = job:account_number
                      if access:subtracc.fetch(sub:account_number_key) = level:benign
                          access:tradeacc.clearkey(tra:account_number_key) 
                          tra:account_number = sub:main_account_number
                          if access:tradeacc.fetch(tra:account_number_key) = level:benign
                              If TRA:Print_Despatch_Despatch = 'YES'
                                  print_at_despatch# = 1
                              End!If TRA:Print_Despatch_Despatch = 'YES'
                          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                      If job:exchange_unit_number <> ''
      
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number = job:exchange_unit_number
                          if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                              If xch:esn   = tmp:esn
      
                                  Case MessageEx('You have scanned the E.S.N. of the Exchange Unit attached to this job.<13,10><13,10>This unit should now be despatched. <13,10><13,10>The job wil be updated accordingly.','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
      
                                  despatch# = 0
                                  access:courier.clearkey(cou:courier_key)
                                  cou:courier = job:exchange_courier
                                  If access:courier.tryfetch(cou:courier_key) = Level:benign
                                      If cou:DespatchClose = 'YES' and job:despatch_Type = 'EXC'
                                          Case MessageEx('The Courier attached to this item is marked as "Despatch At Closing".<13,10><13,10>Do you wish to Despatch the Exchange Unit at this time?','ServiceBase 2000',|
                                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                              Of 1 ! &Yes Button
                                                  despatch# = 1
                                                  DespatchSingle()
                                                  print# = print_at_despatch#
                                              Of 2 ! &No Button
                                                  print# = 0
                                          End!Case MessageEx
                                      Else!If cou:DespatchClose = 'YES' and job:despatch_Type = 'EXC'
                                          print# = 1
                                      End!If cou:DespatchClose = 'YES' and job:despatch_Type = 'EXC'
                                  Else!If access:courier.tryfetch(cou:courier_key) = Level:benign
                                      print# = 1
                                  End!If access:courier.tryfetch(cou:courier_key) = Level:benign
      
                                  If despatch# = 0
                                      GetStatus(110,0,'EXC') !Despatch Exchange Unit
                                      access:jobs.update()
                                      print# = 1
                                  End!If despatch# = 0
                                  If print# = 1
                                      glo:select1  = job:ref_number
                                      Despatch_Note
                                      glo:select1  = ''
                                  End!If print# = 1
                              Else!If xch:esn   = tmp:esn
                                  error# = 1
                              End!If xch:esn   = tmp:esn
                          Else!If job:exchange_unit_number <> ''
                              error# = 1
                          End!End!If job:exchange_unit_number <> ''
                      Else!If job:exchange_unit_number <> ''
                          If job:loan_unit_number <> ''
                              access:loan.clearkey(loa:ref_number_key)
                              loa:ref_number  = job:loan_unit_number
                              If access:loan.tryfetch(loa:ref_number_key) = Level:Benign
                                  If loa:esn  = tmp:esn
      
                                      Case MessageEx('You have scanned the E.S.N. of the Loan Unit attached to this job.<13,10><13,10>This unit should now be despatched. <13,10><13,10>The job wil be updated accordingly.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                      despatch# = 0
                                      print# = 1
                                      access:courier.clearkey(cou:courier_key)
                                      cou:courier = job:loan_courier
                                      If access:courier.tryfetch(cou:courier_key) = Level:benign
      Compile('***',Debug=1)
          Message('cou:despatchclose: ' & cou:despatchclose,'Debug Message',icon:exclamation)
      ***
      Compile('***',Debug=1)
          Message('job:despatchttype: ' & job:despatch_type,'Debug Message',icon:exclamation)
      ***
      
                                          If cou:DespatchClose = 'YES' and job:despatch_Type = 'LOA'
                                              Case MessageEx('The Courier attached to this item is marked as "Despatch At Closing".<13,10><13,10>Do you wish to Despatch the Loan Unit at this time?','ServiceBase 2000',|
                                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                                  Of 1 ! &Yes Button
                                                      despatch# = 1
                                                      DespatchSingle()
                                                      print# = print_at_despatch#
                                                  Of 2 ! &No Button
                                                      print# = 0
                                              End!Case MessageEx
                                          End!If cou:DespatchClose = 'YES'
                                      End!If access:courier.tryfetch(cou:courier_key) = Level:benign
                                      If despatch#    = 0
                                          GetStatus(105,0,'LOA') !Despatch Exchange Unit
                                          access:jobs.update()
                                          print# = 1
                                      End!If despatch#    = 0
                                      If print# = 1
                                          glo:select1  = job:ref_number
                                          Despatch_Note
                                          glo:select1  = ''
                                      End!If print# = 1
                                  Else!If loa:esn  = tmp:esn
                                      error# = 1
                                  End!If loa:esn  = tmp:esn
                              Else!If access:loan.tryfetch(loa:ref_number_key) = Level:Benign
                                  error# = 1
                              End!If access:loan.tryfetch(loa:ref_number_key) = Level:Benign
                          Else!If job:loan_unit_number <> ''
                              error# = 1
                          End!If job:loan_unit_number <> ''
                      End!If job:exchange_unit_number <> ''
                      If error# = 1
                          Case MessageEx('The selected E.S.N. does NOT match any on the selected Job.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      End!If error# = 1
                  Else!End!If job:esn <> tmp:esn
                      continue# = 1
                  End!If job:esn <> tmp:esn
              Else!If def:validateesn = 'YES'
                  continue# = 1
              End!If def:validateesn = 'YES'
              If continue# = 1
                  saverequest# = globalrequest
                  If job:cancelled = 'YES'
                      globalrequest = viewrecord
                  Else!If job:cancelled = 'YES'
                      globalrequest = changerecord
                  End!If job:cancelled = 'YES'
                  ! Start Change 1744 BE(15/04/03)
                  glo:select20 = ''
                  ! End Change 1744 BE(15/04/03)
                  update_jobs_rapid
                  ! Start Change 1744 BE(15/04/03)
                  glo:select20 = ''
                  ! End Change 1744 BE(15/04/03)
                  globalrequest = saverequest#
                  Select(?job_number_temp)
                  tmp:esn = ''
      
              End!If continue# = 1
      
          Else!If access:jobs.fetch(job:ref_number_key) = Level:Benign
              beep(beep:systemhand)  ;  yield()
              message('Unable to find the selected Job Number.', |
                      'ServiceBase 2000', icon:hand)
              Select(?job_number_temp)
              tmp:esn = ''
          End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      End!If error# = 0
      tmp:esn = ''
      job_number_temp = ''
      Select(?job_number_temp)
      
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Rapid_Job_Update')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Set(Defaults)
      access:defaults.next()
      If def:validateesn <> 'YES'
          Disable(?tmp:esn:prompt)
          Disable(?tmp:esn)
      End!If def:validateesn <> 'YES'
        IF GetComputerName(JB:ComputerName,JB:ComputerLen).
        tmp:WorkstationName=JB:ComputerName
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

