

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01018.INC'),ONCE        !Local module procedure declarations
                     END


Process_Minimum_Stock PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
save_shi_id          USHORT,AUTO
Days_7_Temp          LONG
Days_30_Temp         LONG
Days_60_Temp         LONG
Days_90_Temp         LONG
Average_Temp         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
quantity_temp        REAL
Average_text_temp    STRING(8)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?sto:Supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
History::sto:Record  LIKE(sto:RECORD),STATIC
QuickWindow          WINDOW('Update the STOCK File'),AT(,,375,184),FONT('Tahoma',8,,,CHARSET:ANSI),CENTER,IMM,HLP('Process_Minimum_Stock'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,208,148),USE(?Sheet1),SPREAD
                         TAB('General Details'),USE(?Tab1)
                           PROMPT('Part Number'),AT(8,20),USE(?STO:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(sto:Part_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Description'),AT(8,36),USE(?STO:Description:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(sto:Description),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Supplier'),AT(8,52),USE(?sto:supplier:prompt)
                           COMBO(@s30),AT(84,52,124,10),USE(sto:Supplier),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Purchase Cost'),AT(8,68),USE(?STO:Purchase_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,68,64,10),USE(sto:Purchase_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Sale Cost'),AT(8,84),USE(?STO:Sale_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,84,64,10),USE(sto:Sale_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Minimum Level'),AT(8,100),USE(?sto:minimum_level:prompt)
                           SPIN(@p<<<<<<#p),AT(84,100,64,10),USE(sto:Minimum_Level),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,RANGE(0,99999999),STEP(1)
                           PROMPT('Make Up To '),AT(8,116),USE(?Prompt12)
                           SPIN(@p<<<<<<<#p),AT(84,116,64,10),USE(sto:Reorder_Level),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,RANGE(0,99999999),STEP(1)
                           PROMPT('Quantity To Order'),AT(8,132),USE(?quantity_temp:Prompt),TRN
                           SPIN(@p<<<<<<#p),AT(84,132,64,10),USE(quantity_temp),RIGHT,FONT(,,,FONT:bold),UPR,RANGE(0,9999999),STEP(1)
                         END
                       END
                       SHEET,AT(216,4,156,148),USE(?Sheet2),SPREAD
                         TAB('Stock Details'),USE(?Tab2)
                           PROMPT('Quantity In Stock'),AT(220,20),USE(?Prompt9),FONT(,8,,)
                           STRING(@N8),AT(316,20),USE(sto:Quantity_Stock),RIGHT,FONT(,8,,FONT:bold)
                           PROMPT('Quantity Awaiting Order'),AT(220,36),USE(?Prompt10),FONT(,8,,)
                           STRING(@N8),AT(316,36),USE(sto:Quantity_To_Order),RIGHT,FONT(,8,,FONT:bold)
                           PROMPT('Quantity On Order'),AT(220,52),USE(?Prompt11),FONT(,8,,)
                           STRING(@N8),AT(316,52),USE(sto:Quantity_On_Order),RIGHT,FONT(,8,,FONT:bold)
                           PROMPT('Stock Usage'),AT(220,72),USE(?Prompt13)
                           PROMPT('0 - 7 Days'),AT(220,84),USE(?Prompt14)
                           STRING(@n-14),AT(291,84),USE(Days_7_Temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('0 - 30 Days'),AT(220,96),USE(?Prompt14:2)
                           STRING(@n-14),AT(291,96),USE(Days_30_Temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('31 - 60 Days'),AT(220,108),USE(?Prompt14:3)
                           STRING(@n-14),AT(291,108),USE(Days_60_Temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('61 - 90 Days'),AT(220,120),USE(?Prompt14:4)
                           STRING(@n-14),AT(291,120),USE(Days_90_Temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(291,131,67,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Average Daily Use'),AT(220,136),USE(?Prompt17)
                           STRING(@s8),AT(304,136,60,12),USE(Average_text_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON('Cancel'),AT(312,160,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       BUTTON('Update And  Remove From Order'),AT(104,160,96,16),USE(?Remove),LEFT,ICON('Delete.gif')
                       BUTTON('OK'),AT(216,160,48,16),USE(?OK),HIDE,LEFT,ICON('ok.gif')
                       PANEL,AT(4,156,368,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Update And Order'),AT(8,160,96,16),USE(?Order),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?STO:Part_Number:Prompt{prop:FontColor} = -1
    ?STO:Part_Number:Prompt{prop:Color} = 15066597
    If ?sto:Part_Number{prop:ReadOnly} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 15066597
    Elsif ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 8454143
    Else ! If ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 16777215
    End ! If ?sto:Part_Number{prop:Req} = True
    ?sto:Part_Number{prop:Trn} = 0
    ?sto:Part_Number{prop:FontStyle} = font:Bold
    ?STO:Description:Prompt{prop:FontColor} = -1
    ?STO:Description:Prompt{prop:Color} = 15066597
    If ?sto:Description{prop:ReadOnly} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 15066597
    Elsif ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 8454143
    Else ! If ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 16777215
    End ! If ?sto:Description{prop:Req} = True
    ?sto:Description{prop:Trn} = 0
    ?sto:Description{prop:FontStyle} = font:Bold
    ?sto:supplier:prompt{prop:FontColor} = -1
    ?sto:supplier:prompt{prop:Color} = 15066597
    If ?sto:Supplier{prop:ReadOnly} = True
        ?sto:Supplier{prop:FontColor} = 65793
        ?sto:Supplier{prop:Color} = 15066597
    Elsif ?sto:Supplier{prop:Req} = True
        ?sto:Supplier{prop:FontColor} = 65793
        ?sto:Supplier{prop:Color} = 8454143
    Else ! If ?sto:Supplier{prop:Req} = True
        ?sto:Supplier{prop:FontColor} = 65793
        ?sto:Supplier{prop:Color} = 16777215
    End ! If ?sto:Supplier{prop:Req} = True
    ?sto:Supplier{prop:Trn} = 0
    ?sto:Supplier{prop:FontStyle} = font:Bold
    ?STO:Purchase_Cost:Prompt{prop:FontColor} = -1
    ?STO:Purchase_Cost:Prompt{prop:Color} = 15066597
    If ?sto:Purchase_Cost{prop:ReadOnly} = True
        ?sto:Purchase_Cost{prop:FontColor} = 65793
        ?sto:Purchase_Cost{prop:Color} = 15066597
    Elsif ?sto:Purchase_Cost{prop:Req} = True
        ?sto:Purchase_Cost{prop:FontColor} = 65793
        ?sto:Purchase_Cost{prop:Color} = 8454143
    Else ! If ?sto:Purchase_Cost{prop:Req} = True
        ?sto:Purchase_Cost{prop:FontColor} = 65793
        ?sto:Purchase_Cost{prop:Color} = 16777215
    End ! If ?sto:Purchase_Cost{prop:Req} = True
    ?sto:Purchase_Cost{prop:Trn} = 0
    ?sto:Purchase_Cost{prop:FontStyle} = font:Bold
    ?STO:Sale_Cost:Prompt{prop:FontColor} = -1
    ?STO:Sale_Cost:Prompt{prop:Color} = 15066597
    If ?sto:Sale_Cost{prop:ReadOnly} = True
        ?sto:Sale_Cost{prop:FontColor} = 65793
        ?sto:Sale_Cost{prop:Color} = 15066597
    Elsif ?sto:Sale_Cost{prop:Req} = True
        ?sto:Sale_Cost{prop:FontColor} = 65793
        ?sto:Sale_Cost{prop:Color} = 8454143
    Else ! If ?sto:Sale_Cost{prop:Req} = True
        ?sto:Sale_Cost{prop:FontColor} = 65793
        ?sto:Sale_Cost{prop:Color} = 16777215
    End ! If ?sto:Sale_Cost{prop:Req} = True
    ?sto:Sale_Cost{prop:Trn} = 0
    ?sto:Sale_Cost{prop:FontStyle} = font:Bold
    ?sto:minimum_level:prompt{prop:FontColor} = -1
    ?sto:minimum_level:prompt{prop:Color} = 15066597
    If ?sto:Minimum_Level{prop:ReadOnly} = True
        ?sto:Minimum_Level{prop:FontColor} = 65793
        ?sto:Minimum_Level{prop:Color} = 15066597
    Elsif ?sto:Minimum_Level{prop:Req} = True
        ?sto:Minimum_Level{prop:FontColor} = 65793
        ?sto:Minimum_Level{prop:Color} = 8454143
    Else ! If ?sto:Minimum_Level{prop:Req} = True
        ?sto:Minimum_Level{prop:FontColor} = 65793
        ?sto:Minimum_Level{prop:Color} = 16777215
    End ! If ?sto:Minimum_Level{prop:Req} = True
    ?sto:Minimum_Level{prop:Trn} = 0
    ?sto:Minimum_Level{prop:FontStyle} = font:Bold
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    If ?sto:Reorder_Level{prop:ReadOnly} = True
        ?sto:Reorder_Level{prop:FontColor} = 65793
        ?sto:Reorder_Level{prop:Color} = 15066597
    Elsif ?sto:Reorder_Level{prop:Req} = True
        ?sto:Reorder_Level{prop:FontColor} = 65793
        ?sto:Reorder_Level{prop:Color} = 8454143
    Else ! If ?sto:Reorder_Level{prop:Req} = True
        ?sto:Reorder_Level{prop:FontColor} = 65793
        ?sto:Reorder_Level{prop:Color} = 16777215
    End ! If ?sto:Reorder_Level{prop:Req} = True
    ?sto:Reorder_Level{prop:Trn} = 0
    ?sto:Reorder_Level{prop:FontStyle} = font:Bold
    ?quantity_temp:Prompt{prop:FontColor} = -1
    ?quantity_temp:Prompt{prop:Color} = 15066597
    If ?quantity_temp{prop:ReadOnly} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 15066597
    Elsif ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 8454143
    Else ! If ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 16777215
    End ! If ?quantity_temp{prop:Req} = True
    ?quantity_temp{prop:Trn} = 0
    ?quantity_temp{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?sto:Quantity_Stock{prop:FontColor} = -1
    ?sto:Quantity_Stock{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    ?sto:Quantity_To_Order{prop:FontColor} = -1
    ?sto:Quantity_To_Order{prop:Color} = 15066597
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?sto:Quantity_On_Order{prop:FontColor} = -1
    ?sto:Quantity_On_Order{prop:Color} = 15066597
    ?Prompt13{prop:FontColor} = -1
    ?Prompt13{prop:Color} = 15066597
    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    ?Days_7_Temp{prop:FontColor} = -1
    ?Days_7_Temp{prop:Color} = 15066597
    ?Prompt14:2{prop:FontColor} = -1
    ?Prompt14:2{prop:Color} = 15066597
    ?Days_30_Temp{prop:FontColor} = -1
    ?Days_30_Temp{prop:Color} = 15066597
    ?Prompt14:3{prop:FontColor} = -1
    ?Prompt14:3{prop:Color} = 15066597
    ?Days_60_Temp{prop:FontColor} = -1
    ?Days_60_Temp{prop:Color} = 15066597
    ?Prompt14:4{prop:FontColor} = -1
    ?Prompt14:4{prop:Color} = 15066597
    ?Days_90_Temp{prop:FontColor} = -1
    ?Days_90_Temp{prop:Color} = 15066597
    ?Prompt17{prop:FontColor} = -1
    ?Prompt17{prop:Color} = 15066597
    ?Average_text_temp{prop:FontColor} = -1
    ?Average_text_temp{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Process_Minimum_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Process_Minimum_Stock',1)
    SolaceViewVars('save_shi_id',save_shi_id,'Process_Minimum_Stock',1)
    SolaceViewVars('Days_7_Temp',Days_7_Temp,'Process_Minimum_Stock',1)
    SolaceViewVars('Days_30_Temp',Days_30_Temp,'Process_Minimum_Stock',1)
    SolaceViewVars('Days_60_Temp',Days_60_Temp,'Process_Minimum_Stock',1)
    SolaceViewVars('Days_90_Temp',Days_90_Temp,'Process_Minimum_Stock',1)
    SolaceViewVars('Average_Temp',Average_Temp,'Process_Minimum_Stock',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Process_Minimum_Stock',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Process_Minimum_Stock',1)
    SolaceViewVars('quantity_temp',quantity_temp,'Process_Minimum_Stock',1)
    SolaceViewVars('Average_text_temp',Average_text_temp,'Process_Minimum_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Part_Number:Prompt;  SolaceCtrlName = '?STO:Part_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Part_Number;  SolaceCtrlName = '?sto:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Description:Prompt;  SolaceCtrlName = '?STO:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Description;  SolaceCtrlName = '?sto:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:supplier:prompt;  SolaceCtrlName = '?sto:supplier:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Supplier;  SolaceCtrlName = '?sto:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Purchase_Cost:Prompt;  SolaceCtrlName = '?STO:Purchase_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Purchase_Cost;  SolaceCtrlName = '?sto:Purchase_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Sale_Cost:Prompt;  SolaceCtrlName = '?STO:Sale_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Sale_Cost;  SolaceCtrlName = '?sto:Sale_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:minimum_level:prompt;  SolaceCtrlName = '?sto:minimum_level:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Minimum_Level;  SolaceCtrlName = '?sto:Minimum_Level';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12;  SolaceCtrlName = '?Prompt12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Reorder_Level;  SolaceCtrlName = '?sto:Reorder_Level';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?quantity_temp:Prompt;  SolaceCtrlName = '?quantity_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?quantity_temp;  SolaceCtrlName = '?quantity_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Quantity_Stock;  SolaceCtrlName = '?sto:Quantity_Stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Quantity_To_Order;  SolaceCtrlName = '?sto:Quantity_To_Order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Quantity_On_Order;  SolaceCtrlName = '?sto:Quantity_On_Order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt13;  SolaceCtrlName = '?Prompt13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14;  SolaceCtrlName = '?Prompt14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Days_7_Temp;  SolaceCtrlName = '?Days_7_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14:2;  SolaceCtrlName = '?Prompt14:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Days_30_Temp;  SolaceCtrlName = '?Days_30_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14:3;  SolaceCtrlName = '?Prompt14:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Days_60_Temp;  SolaceCtrlName = '?Days_60_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14:4;  SolaceCtrlName = '?Prompt14:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Days_90_Temp;  SolaceCtrlName = '?Days_90_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17;  SolaceCtrlName = '?Prompt17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Average_text_temp;  SolaceCtrlName = '?Average_text_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Remove;  SolaceCtrlName = '?Remove';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Order;  SolaceCtrlName = '?Order';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Minimum Stock Level Item'
  OF ChangeRecord
    ActionMessage = 'Minimum Stock Level Item'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Process_Minimum_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Process_Minimum_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STO:Part_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sto:Record,History::sto:Record)
  SELF.AddHistoryField(?sto:Part_Number,3)
  SELF.AddHistoryField(?sto:Description,4)
  SELF.AddHistoryField(?sto:Supplier,5)
  SELF.AddHistoryField(?sto:Purchase_Cost,6)
  SELF.AddHistoryField(?sto:Sale_Cost,7)
  SELF.AddHistoryField(?sto:Minimum_Level,18)
  SELF.AddHistoryField(?sto:Reorder_Level,19)
  SELF.AddHistoryField(?sto:Quantity_Stock,15)
  SELF.AddHistoryField(?sto:Quantity_To_Order,16)
  SELF.AddHistoryField(?sto:Quantity_On_Order,17)
  SELF.AddUpdateFile(Access:STOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:STOCK.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  quantity_temp = STO:Reorder_Level - STO:Quantity_Stock
  If quantity_temp < 0
      quantity_temp = 0
  End!If quantity_temp < 0
  Display(?quantity_temp)
  
  Include('stockuse.inc')
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(sto:Supplier,?sto:Supplier,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sup:Company_Name_Key)
  FDCB6.AddField(sup:Company_Name,FDCB6.Q.sup:Company_Name)
  FDCB6.AddField(sup:RecordNumber,FDCB6.Q.sup:RecordNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:STOCK.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Process_Minimum_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Remove
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove, Accepted)
      sto:minimum_stock = 'NO'
      thiswindow.postcompleted()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?Order
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order, Accepted)
      get(ordpend,0)
      if access:ordpend.primerecord() = level:benign
      
          ope:part_ref_number = sto:ref_number
          ope:part_type       = 'STO'
          ope:supplier        = sto:supplier
          ope:part_number     = sto:part_number
          ope:description     = sto:description
          ope:quantity        = quantity_temp
          access:ordpend.insert()
      
          sto:quantity_to_order += quantity_temp
          sto:pending_ref_number = ope:ref_number
          sto:minimum_stock = 'NO'
      end!if access:ordpend.primerecord() = level:benign
      Thiswindow.postcompleted()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Process_Minimum_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

