

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01035.INC'),ONCE        !Local module procedure declarations
                     END


JB:ComputerName CSTRING(255)
JB:ComputerLen  ULONG(255)
Update_Jobs_Rapid PROCEDURE                           !Generated from procedure template - Window

MakeEstimateJobFlag  BYTE(0)
tmp_RF_Board_IMEI    STRING(20)
tmpCheckChargeableParts BYTE(0)
TotalPartsCost       REAL
TempStatusCode       STRING(30)
CurrentTab           STRING(80)
save_par_id2         USHORT
save_joe_id          USHORT,AUTO
save_taf_id          USHORT,AUTO
tmp:RestockingNote   BYTE(0)
tmp:chargetype       STRING(30)
tmp:WarrantyChargeType STRING(30)
tmp:RepairType       STRING(30)
tmp:WarrantyRepairType STRING(30)
tmp:UnitType         STRING(30)
invoice_number_temp  LONG
tmp:CreateInvoice    BYTE(0)
save_job_ali_id      USHORT,AUTO
tmp:ConsignNo        STRING(20)
tmp:OldConsignNo     STRING(20)
tmp:LabelError       STRING('0 {29}')
tmp:DespatchClose    BYTE(0)
save_lac_id          USHORT,AUTO
save_job_id          USHORT,AUTO
chargeable_job_temp  STRING(3)
warranty_job_temp    STRING(3)
save_cha_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
Print_Despatch_Note_temp STRING(3)
tmp:accountnumber    STRING(15)
account_number2_temp STRING(15)
sav:path             STRING(255)
save_cou_ali_id      USHORT,AUTO
error_queue_temp     QUEUE,PRE(err)
field                STRING(30)
                     END
error_message_temp   STRING(255)
error_type_temp      STRING(1),DIM(50)
check_for_bouncers_temp STRING(3)
saved_ref_number_temp REAL
saved_esn_temp       STRING(16)
save_aud_id          USHORT,AUTO
saved_msn_temp       STRING(16)
engineer_ref_number_temp REAL
engineer_quantity_temp REAL
main_store_ref_number_temp REAL
main_store_quantity_temp REAL
main_store_sundry_temp STRING(3)
engineer_sundry_temp STRING(3)
save_ccp_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
Force_Fault_Codes_Temp STRING('NO {1}')
date_completed_temp  DATE
time_completed_temp  TIME
qa_passed_temp       STRING(3)
qa_rejected_temp     STRING(3)
qa_second_passed_temp STRING(3)
location_temp        STRING(30)
date_error_temp      BYTE
engineer_name_temp   STRING(40)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
title_text_temp      STRING(255)
trade_account_temp   STRING(90)
model_details_temp   STRING(90)
DisplayString        STRING(255)
title_text2_temp     STRING(255)
engineer_temp        STRING(30)
check_for_bouncer_temp BYTE(0)
qa_rejection_reason_temp STRING(10000)
exchange_loan_temp   STRING(60)
tmp:invoicetext      STRING(255)
SaveGroup            GROUP,PRE(sav)
ChargeType           STRING(30)
WarrantyChargeType   STRING(30)
RepairType           STRING(30)
WarrantyRepairType   STRING(30)
ChargeableJob        STRING(3)
WarrantyJob          STRING(3)
                     END
ChangedGroup         GROUP,PRE(sav1)
cct                  BYTE(0)
wct                  BYTE(0)
crt                  BYTE(0)
wrt                  BYTE(0)
chj                  BYTE(0)
wrj                  BYTE(0)
                     END
tmp:ParcellineName   STRING(255),STATIC
tmp:WorkStationName  STRING(30)
tmp:SkillLevel       LONG
tmp:no               STRING('NO {1}')
tmp:yes              STRING('YES')
tmp:DateAllocated    DATE
tmp:Network          STRING(30)
tmp:JobSkillLevel    LONG
tmp:SaveAccessory    STRING(255),STATIC
partpntstore         USHORT
warpartpntstore      USHORT
SaveAccessoryQueue   QUEUE,PRE(savaccq)
Accessory            STRING(30)
Type                 BYTE(0)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?job:Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?job:Warranty_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB7::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
BRW9::View:Browse    VIEW(PARTS)
                       PROJECT(par:Part_Number)
                       PROJECT(par:Description)
                       PROJECT(par:Quantity)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
par:Part_Number        LIKE(par:Part_Number)          !List box control field - type derived from field
par:Part_Number_NormalFG LONG                         !Normal forground color
par:Part_Number_NormalBG LONG                         !Normal background color
par:Part_Number_SelectedFG LONG                       !Selected forground color
par:Part_Number_SelectedBG LONG                       !Selected background color
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Description_NormalFG LONG                         !Normal forground color
par:Description_NormalBG LONG                         !Normal background color
par:Description_SelectedFG LONG                       !Selected forground color
par:Description_SelectedBG LONG                       !Selected background color
par:Quantity           LIKE(par:Quantity)             !List box control field - type derived from field
par:Quantity_NormalFG  LONG                           !Normal forground color
par:Quantity_NormalBG  LONG                           !Normal background color
par:Quantity_SelectedFG LONG                          !Selected forground color
par:Quantity_SelectedBG LONG                          !Selected background color
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(WARPARTS)
                       PROJECT(wpr:Part_Number)
                       PROJECT(wpr:Description)
                       PROJECT(wpr:Quantity)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
wpr:Part_Number        LIKE(wpr:Part_Number)          !List box control field - type derived from field
wpr:Part_Number_NormalFG LONG                         !Normal forground color
wpr:Part_Number_NormalBG LONG                         !Normal background color
wpr:Part_Number_SelectedFG LONG                       !Selected forground color
wpr:Part_Number_SelectedBG LONG                       !Selected background color
wpr:Description        LIKE(wpr:Description)          !List box control field - type derived from field
wpr:Description_NormalFG LONG                         !Normal forground color
wpr:Description_NormalBG LONG                         !Normal background color
wpr:Description_SelectedFG LONG                       !Selected forground color
wpr:Description_SelectedBG LONG                       !Selected background color
wpr:Quantity           LIKE(wpr:Quantity)             !List box control field - type derived from field
wpr:Quantity_NormalFG  LONG                           !Normal forground color
wpr:Quantity_NormalBG  LONG                           !Normal background color
wpr:Quantity_SelectedFG LONG                          !Selected forground color
wpr:Quantity_SelectedBG LONG                          !Selected background color
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::job:Record  LIKE(job:RECORD),STATIC
QuickWindow          WINDOW('Rapid Job Update'),AT(,,655,348),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('Update_Jobs_Rapid'),ALRT(F10Key),ALRT(F3Key),ALRT(F4Key),ALRT(F5Key),ALRT(F6Key),ALRT(F7Key),ALRT(F8Key),ALRT(F9Key),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,648,100),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           STRING(@s255),AT(372,8,276,12),USE(title_text2_temp),RIGHT,FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('JOB NO:'),AT(8,8),USE(?Prompt20),FONT(,14,COLOR:Green,FONT:bold)
                           STRING('JOB COMPLETED'),AT(488,20),USE(?Completed),TRN,HIDE,LEFT,FONT(,20,COLOR:Red,FONT:bold)
                           STRING(@s8),AT(64,8),USE(job:Ref_Number),FONT(,14,COLOR:Green,FONT:bold,CHARSET:ANSI)
                           PROMPT('Trade Account: '),AT(8,24),USE(?Prompt1)
                           STRING(@s60),AT(84,24,228,12),USE(trade_account_temp),FONT(,,,FONT:bold)
                           PROMPT('Order Number:'),AT(8,32),USE(?Prompt1:2)
                           PROMPT('Model Details:'),AT(8,40),USE(?Prompt2)
                           STRING(@s60),AT(84,40,228,12),USE(model_details_temp),FONT(,,,FONT:bold)
                           PROMPT('I.M.E.I. Number'),AT(8,52),USE(?Prompt3)
                           STRING(@s18),AT(84,52),USE(job:ESN),FONT(,,,FONT:bold)
                           PROMPT('M.S.N.: '),AT(168,52),USE(?Prompt4)
                           STRING(@s16),AT(196,52),USE(job:MSN),FONT(,,,FONT:bold)
                           PROMPT('Job Status:'),AT(8,64),USE(?Prompt13)
                           STRING(@s30),AT(84,64),USE(TempStatusCode),FONT(,,,FONT:bold)
                           STRING(@s60),AT(400,88,248,12),USE(exchange_loan_temp),FONT(,10,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           PROMPT('QA Rejection Reason'),AT(328,20),USE(?qa_rejection_reason_temp:prompt),TRN,HIDE
                           PROMPT('Date Completed'),AT(488,40),USE(?JOB:Date_Completed:Prompt),TRN
                           ENTRY(@D6b),AT(588,40,36,10),USE(job:Date_Completed),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@t1),AT(624,40,24,10),USE(job:Time_Completed),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),READONLY
                           STRING(@s30),AT(84,32,228,12),USE(job:Order_Number),FONT(,,,FONT:bold)
                           COMBO(@s20),AT(84,88,124,10),USE(job:Warranty_Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           GROUP,AT(484,56,164,16),USE(?QA_Group)
                             CHECK('QA Passed'),AT(488,60),USE(job:QA_Passed),VALUE('YES','NO')
                             ENTRY(@d6b),AT(588,60,36,10),USE(job:Date_QA_Passed),SKIP,FONT(,,,FONT:bold),READONLY
                             ENTRY(@t1b),AT(624,60,24,10),USE(job:Time_QA_Passed),SKIP,FONT(,,,FONT:bold),READONLY
                           END
                           CHECK('Chargeable Job'),AT(8,76,72,12),USE(job:Chargeable_Job),LEFT,VALUE('YES','NO')
                           COMBO(@s30),AT(84,76,124,10),USE(job:Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('${30}'),AT(212,75),USE(?StatusWarning),TRN,HIDE,FONT(,14,COLOR:Red,FONT:bold)
                           CHECK('Warranty Job'),AT(8,88,72,12),USE(job:Warranty_Job),LEFT,VALUE('YES','NO')
                           TEXT,AT(328,28,144,44),USE(qa_rejection_reason_temp),SKIP,HIDE,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                         END
                       END
                       SHEET,AT(4,108,328,208),USE(?Sheet2),WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           BUTTON('Common Faults [F8]'),AT(244,216,20,16),USE(?Common_Faults),LEFT,ICON('fault.gif')
                           BUTTON('Amend Job Status [F9]'),AT(132,296,20,16),USE(?ChangeJobStatus),LEFT,ICON('Audit.gif')
                           PROMPT('View Accessories [F9]'),AT(232,296,60,16),USE(?Prompt22),LEFT
                           PROMPT('Amend Fault Codes [F7]'),AT(76,296,44,16),USE(?Prompt22:2),LEFT
                           PROMPT('Assign Common Fault [F6]'),AT(268,216,56,16),USE(?Prompt22:3),LEFT
                           BUTTON,AT(244,236,20,16),USE(?RemoveParts),LEFT,ICON('Bin.gif')
                           PROMPT('Remove Parts And Invoice Text'),AT(268,236,56,16),USE(?Prompt22:6),LEFT
                           BUTTON,AT(244,256,20,16),USE(?MakeEstimateJob),LEFT,ICON('Invoice.gif')
                           PROMPT('Change Job Status [F8]'),AT(156,296,44,16),USE(?Prompt22:4),LEFT
                           PROMPT('View Engineer History'),AT(252,164,60,16),USE(?Prompt22:5),CENTER
                           PROMPT('Network'),AT(8,188),USE(?tmp:Network:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,188,124,10),USE(tmp:Network),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Network'),TIP('Network'),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(212,188,10,10),USE(?LookupNetwork),SKIP,HIDE,ICON('List3.ico')
                           PROMPT('Repair Details'),AT(8,112),USE(?Prompt5),FONT(,10,COLOR:Navy,FONT:bold)
                           BUTTON('Amend Unit Type [F3]'),AT(84,116,20,16),USE(?AmendUnitType),LEFT,ICON('transfer.gif')
                           PROMPT('Amend Unit Type [F3]'),AT(108,116,48,15),USE(?Prompt21:2)
                           GROUP('Engineer Details'),AT(84,136,168,48),USE(?EngineerDetails),BOXED
                             BUTTON,AT(236,148,10,10),USE(?Lookup_Engineer),ICON('list3.ico')
                             PROMPT('Name:'),AT(88,148),USE(?Prompt17)
                             STRING(@s30),AT(112,148),USE(engineer_name_temp),TRN,FONT(,,,FONT:bold)
                             PROMPT('Eng Level:'),AT(88,172),USE(?Prompt17:3)
                             STRING(@s3),AT(124,172),USE(tmp:SkillLevel),TRN,FONT(,,,FONT:bold)
                             PROMPT('Allocated:'),AT(88,160),USE(?Prompt17:4)
                             STRING(@d6),AT(120,160),USE(tmp:DateAllocated),FONT(,,,FONT:bold)
                           END
                           BUTTON,AT(272,144,20,16),USE(?EngineerHistory),ICON('history.gif')
                           PROMPT('Job Level'),AT(8,172),USE(?tmp:JobSkillLevel:Prompt),TRN
                           SPIN(@n8),AT(56,172,24,10),USE(tmp:JobSkillLevel),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Level'),TIP('Job Level'),UPR,RANGE(0,10),STEP(1)
                           BUTTON('Allocate Job [F4]'),AT(180,116,20,16),USE(?AllocateJob),LEFT,ICON('MenWork2.gif')
                           PROMPT('Allocate Job [F4]'),AT(204,116,48,15),USE(?Prompt21)
                           PROMPT('Repair Type'),AT(8,204),USE(?JOB:Repair_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,204,124,10),USE(job:Repair_Type),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(212,204,10,10),USE(?LookupChargeableRepairType),SKIP,ICON('List3.ico')
                           PROMPT('Warranty Repair Type'),AT(8,220),USE(?JOB:Repair_Type_Warranty:Prompt),TRN
                           ENTRY(@s30),AT(84,220,124,10),USE(job:Repair_Type_Warranty),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(212,220,10,10),USE(?LookupWarrantyRepairType),SKIP,ICON('List3.ico')
                           PROMPT('Invoice Text'),AT(8,236),USE(?JOB:Invoice_Text:Prompt)
                           BUTTON,AT(212,236,10,10),USE(?Lookup_Invoice_Text),ICON('list3.ico')
                           TEXT,AT(84,236,124,36),USE(jbn:Invoice_Text),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Location'),AT(8,280),USE(?job:Location:Prompt),TRN
                           ENTRY(@s30),AT(84,280,124,10),USE(job:Location),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),ALRT(DownKey),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight)
                           BUTTON,AT(212,280,10,10),USE(?LookupLocation),SKIP,ICON('List3.ico')
                           STRING('Print Pick Note'),AT(268,280),USE(?PickNotePrompt)
                           BUTTON,AT(244,276,20,16),USE(?PickNote),ICON('pick.gif')
                           PROMPT('Make Estimate job'),AT(268,256,52,24),USE(?MakeEstimateJobPrompt)
                           BUTTON('&Fault Codes [F7]'),AT(52,296,20,16),USE(?Fault_Codes),LEFT,ICON('FauCode.gif')
                           BUTTON('Accessories [F6]'),AT(208,296,20,16),USE(?ViewAccessories),LEFT,ICON('Accessor.gif')
                         END
                       END
                       SHEET,AT(336,108,316,96),USE(?Sheet4),WIZARD,SPREAD
                         TAB('Tab 5'),USE(?Tab5)
                           PROMPT('Job Notes'),AT(340,112),USE(?jobNotes),FONT(,10,COLOR:Navy,FONT:bold)
                           PROMPT('Fault Description'),AT(340,124),USE(?JOB:Fault_Description:Prompt),TRN
                           PROMPT('Engineers Notes'),AT(500,124),USE(?JOB:Fault_Description:Prompt:2),TRN
                           TEXT,AT(340,132,144,68),USE(jbn:Fault_Description),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           TEXT,AT(500,132,144,52),USE(jbn:Engineers_Notes),SKIP,VSCROLL,FONT(,,,FONT:bold),READONLY
                           BUTTON('Amend Engineer Notes [F5]'),AT(500,188,144,12),USE(?engineers_notes),LEFT,ICON('History2.gif')
                         END
                       END
                       SHEET,AT(336,208,316,108),USE(?Sheet3),SPREAD
                         TAB('&Chargeable Parts'),USE(?ChargeableTab)
                           GROUP,AT(340,220,308,96),USE(?Chargeable_Group),DISABLE
                             LIST,AT(340,232,244,80),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('99L(2)|M*~Part Number~@s30@108L(2)|M*~Description~@s30@32R(2)*~Qty~L@n8@'),FROM(Queue:Browse)
                             BUTTON('&Insert'),AT(588,232,60,12),USE(?Multi_Insert),LEFT,ICON('Insert.ico')
                             BUTTON('Adjustment'),AT(588,252,60,12),USE(?Add_Chargeable_Adjustment),LEFT,ICON('Insert.ico')
                             BUTTON('&Insert '),AT(384,264,76,20),USE(?Insert),HIDE,LEFT,ICON('Insert.ico')
                             BUTTON('&Change'),AT(588,276,60,12),USE(?Change),LEFT,ICON('Edit.ico')
                             BUTTON('&Delete'),AT(588,300,60,12),USE(?Delete),LEFT,ICON('Delete.ico')
                           END
                         END
                         TAB('&Warranty Parts'),USE(?Warrantytab)
                           GROUP,AT(336,220,312,96),USE(?Warranty_Group),DISABLE
                             BUTTON('Insert'),AT(588,232,60,12),USE(?Multi_Insert:2),LEFT,ICON('Insert.ico')
                             BUTTON('Adjustment'),AT(588,256,60,12),USE(?Add_Warranty_Adjustment),LEFT,ICON('Insert.ico')
                             LIST,AT(340,232,244,80),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('100L(2)|M*~Part Number~@s30@108L(2)|M*~Description~@s30@32R(2)|M*~Qty~L@n8@'),FROM(Queue:Browse:1)
                             BUTTON('&Insert'),AT(400,264,56,16),USE(?Insert:3),HIDE,LEFT,ICON('Insert.ico')
                             BUTTON('&Change'),AT(588,280,60,12),USE(?Change:3),LEFT,ICON('Edit.ico')
                             BUTTON('&Delete'),AT(588,300,60,12),USE(?Delete:3),LEFT,ICON('Delete.ico')
                           END
                         END
                       END
                       PANEL,AT(4,320,648,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Costs'),AT(336,324,56,16),USE(?Costs),LEFT,ICON('Invoice.gif')
                       BUTTON('&Complete Job [F10]'),AT(8,324,92,16),USE(?CompleteButton),TRN,FLAT,LEFT,ICON('fixer.gif')
                       PANEL,AT(8,324,92,16),USE(?Panel2),FILL(COLOR:Lime)
                       BUTTON('&OK'),AT(536,324,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(592,324,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       BUTTON('Contact History'),AT(448,324,56,16),USE(?ContactHistory),LEFT,ICON('audit.gif')
                       BUTTON('&Create Invoice'),AT(392,324,56,16),USE(?Invoice),LEFT,ICON('money_sm.gif')
                       STRING('JOB CANCELLED'),AT(0,0,656,312),USE(?CancelText),TRN,HIDE,CENTER,FONT(,48,,FONT:bold),ANGLE(350)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_wpr_id   ushort,auto
save_par_id   ushort,auto
save_orp_id   ushort,auto
save_maf_id   ushort,auto
save_map_id   ushort,auto
save_jea_id   ushort,auto

! Start Change 2618 BE(06/08/03)
!
! include definition here rather than in CLARION Data section
! in order to exclude from Solace View which appears unable
! to cope with a GROUP ARRAY data item.
!
JobStatusColours     GROUP,PRE(JSC),DIM(8)
Colour               LONG
Action               BYTE
Status               STRING(30)
                     END
! Start Change 2618 BE(06/08/03)
windowsdir      lpstr(144)
systemdir       word
    Map
LocalValidateAccessories    Procedure(),Byte
UpdateRFBoardIMEI PROCEDURE (LONG,STRING)
    End
SaveAccessory    File,Driver('TOPSPEED'),Pre(savacc),Name(tmp:SaveAccessory),Create,Bindable,Thread
AccessoryKey            Key(savacc:Accessory),NOCASE,DUP
Record                  Record
Accessory               String(30)
                        End
                    End
TempFilePath         CSTRING(255)

!Save Entry Fields Incase Of Lookup
look:tmp:Network                Like(tmp:Network)
look:job:Repair_Type                Like(job:Repair_Type)
look:job:Repair_Type_Warranty                Like(job:Repair_Type_Warranty)
look:job:Location                Like(job:Location)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?title_text2_temp{prop:FontColor} = -1
    ?title_text2_temp{prop:Color} = 15066597
    ?Prompt20{prop:FontColor} = -1
    ?Prompt20{prop:Color} = 15066597
    ?Completed{prop:FontColor} = -1
    ?Completed{prop:Color} = 15066597
    ?job:Ref_Number{prop:FontColor} = -1
    ?job:Ref_Number{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?trade_account_temp{prop:FontColor} = -1
    ?trade_account_temp{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?model_details_temp{prop:FontColor} = -1
    ?model_details_temp{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?job:ESN{prop:FontColor} = -1
    ?job:ESN{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?job:MSN{prop:FontColor} = -1
    ?job:MSN{prop:Color} = 15066597
    ?Prompt13{prop:FontColor} = -1
    ?Prompt13{prop:Color} = 15066597
    ?TempStatusCode{prop:FontColor} = -1
    ?TempStatusCode{prop:Color} = 15066597
    ?exchange_loan_temp{prop:FontColor} = -1
    ?exchange_loan_temp{prop:Color} = 15066597
    ?qa_rejection_reason_temp:prompt{prop:FontColor} = -1
    ?qa_rejection_reason_temp:prompt{prop:Color} = 15066597
    ?JOB:Date_Completed:Prompt{prop:FontColor} = -1
    ?JOB:Date_Completed:Prompt{prop:Color} = 15066597
    If ?job:Date_Completed{prop:ReadOnly} = True
        ?job:Date_Completed{prop:FontColor} = 65793
        ?job:Date_Completed{prop:Color} = 15066597
    Elsif ?job:Date_Completed{prop:Req} = True
        ?job:Date_Completed{prop:FontColor} = 65793
        ?job:Date_Completed{prop:Color} = 8454143
    Else ! If ?job:Date_Completed{prop:Req} = True
        ?job:Date_Completed{prop:FontColor} = 65793
        ?job:Date_Completed{prop:Color} = 16777215
    End ! If ?job:Date_Completed{prop:Req} = True
    ?job:Date_Completed{prop:Trn} = 0
    ?job:Date_Completed{prop:FontStyle} = font:Bold
    If ?job:Time_Completed{prop:ReadOnly} = True
        ?job:Time_Completed{prop:FontColor} = 65793
        ?job:Time_Completed{prop:Color} = 15066597
    Elsif ?job:Time_Completed{prop:Req} = True
        ?job:Time_Completed{prop:FontColor} = 65793
        ?job:Time_Completed{prop:Color} = 8454143
    Else ! If ?job:Time_Completed{prop:Req} = True
        ?job:Time_Completed{prop:FontColor} = 65793
        ?job:Time_Completed{prop:Color} = 16777215
    End ! If ?job:Time_Completed{prop:Req} = True
    ?job:Time_Completed{prop:Trn} = 0
    ?job:Time_Completed{prop:FontStyle} = font:Bold
    ?job:Order_Number{prop:FontColor} = -1
    ?job:Order_Number{prop:Color} = 15066597
    If ?job:Warranty_Charge_Type{prop:ReadOnly} = True
        ?job:Warranty_Charge_Type{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type{prop:Color} = 15066597
    Elsif ?job:Warranty_Charge_Type{prop:Req} = True
        ?job:Warranty_Charge_Type{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type{prop:Color} = 8454143
    Else ! If ?job:Warranty_Charge_Type{prop:Req} = True
        ?job:Warranty_Charge_Type{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type{prop:Color} = 16777215
    End ! If ?job:Warranty_Charge_Type{prop:Req} = True
    ?job:Warranty_Charge_Type{prop:Trn} = 0
    ?job:Warranty_Charge_Type{prop:FontStyle} = font:Bold
    ?QA_Group{prop:Font,3} = -1
    ?QA_Group{prop:Color} = 15066597
    ?QA_Group{prop:Trn} = 0
    ?job:QA_Passed{prop:Font,3} = -1
    ?job:QA_Passed{prop:Color} = 15066597
    ?job:QA_Passed{prop:Trn} = 0
    If ?job:Date_QA_Passed{prop:ReadOnly} = True
        ?job:Date_QA_Passed{prop:FontColor} = 65793
        ?job:Date_QA_Passed{prop:Color} = 15066597
    Elsif ?job:Date_QA_Passed{prop:Req} = True
        ?job:Date_QA_Passed{prop:FontColor} = 65793
        ?job:Date_QA_Passed{prop:Color} = 8454143
    Else ! If ?job:Date_QA_Passed{prop:Req} = True
        ?job:Date_QA_Passed{prop:FontColor} = 65793
        ?job:Date_QA_Passed{prop:Color} = 16777215
    End ! If ?job:Date_QA_Passed{prop:Req} = True
    ?job:Date_QA_Passed{prop:Trn} = 0
    ?job:Date_QA_Passed{prop:FontStyle} = font:Bold
    If ?job:Time_QA_Passed{prop:ReadOnly} = True
        ?job:Time_QA_Passed{prop:FontColor} = 65793
        ?job:Time_QA_Passed{prop:Color} = 15066597
    Elsif ?job:Time_QA_Passed{prop:Req} = True
        ?job:Time_QA_Passed{prop:FontColor} = 65793
        ?job:Time_QA_Passed{prop:Color} = 8454143
    Else ! If ?job:Time_QA_Passed{prop:Req} = True
        ?job:Time_QA_Passed{prop:FontColor} = 65793
        ?job:Time_QA_Passed{prop:Color} = 16777215
    End ! If ?job:Time_QA_Passed{prop:Req} = True
    ?job:Time_QA_Passed{prop:Trn} = 0
    ?job:Time_QA_Passed{prop:FontStyle} = font:Bold
    ?job:Chargeable_Job{prop:Font,3} = -1
    ?job:Chargeable_Job{prop:Color} = 15066597
    ?job:Chargeable_Job{prop:Trn} = 0
    If ?job:Charge_Type{prop:ReadOnly} = True
        ?job:Charge_Type{prop:FontColor} = 65793
        ?job:Charge_Type{prop:Color} = 15066597
    Elsif ?job:Charge_Type{prop:Req} = True
        ?job:Charge_Type{prop:FontColor} = 65793
        ?job:Charge_Type{prop:Color} = 8454143
    Else ! If ?job:Charge_Type{prop:Req} = True
        ?job:Charge_Type{prop:FontColor} = 65793
        ?job:Charge_Type{prop:Color} = 16777215
    End ! If ?job:Charge_Type{prop:Req} = True
    ?job:Charge_Type{prop:Trn} = 0
    ?job:Charge_Type{prop:FontStyle} = font:Bold
    ?StatusWarning{prop:FontColor} = -1
    ?StatusWarning{prop:Color} = 15066597
    ?job:Warranty_Job{prop:Font,3} = -1
    ?job:Warranty_Job{prop:Color} = 15066597
    ?job:Warranty_Job{prop:Trn} = 0
    If ?qa_rejection_reason_temp{prop:ReadOnly} = True
        ?qa_rejection_reason_temp{prop:FontColor} = 65793
        ?qa_rejection_reason_temp{prop:Color} = 15066597
    Elsif ?qa_rejection_reason_temp{prop:Req} = True
        ?qa_rejection_reason_temp{prop:FontColor} = 65793
        ?qa_rejection_reason_temp{prop:Color} = 8454143
    Else ! If ?qa_rejection_reason_temp{prop:Req} = True
        ?qa_rejection_reason_temp{prop:FontColor} = 65793
        ?qa_rejection_reason_temp{prop:Color} = 16777215
    End ! If ?qa_rejection_reason_temp{prop:Req} = True
    ?qa_rejection_reason_temp{prop:Trn} = 0
    ?qa_rejection_reason_temp{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt22{prop:FontColor} = -1
    ?Prompt22{prop:Color} = 15066597
    ?Prompt22:2{prop:FontColor} = -1
    ?Prompt22:2{prop:Color} = 15066597
    ?Prompt22:3{prop:FontColor} = -1
    ?Prompt22:3{prop:Color} = 15066597
    ?Prompt22:6{prop:FontColor} = -1
    ?Prompt22:6{prop:Color} = 15066597
    ?Prompt22:4{prop:FontColor} = -1
    ?Prompt22:4{prop:Color} = 15066597
    ?Prompt22:5{prop:FontColor} = -1
    ?Prompt22:5{prop:Color} = 15066597
    ?tmp:Network:Prompt{prop:FontColor} = -1
    ?tmp:Network:Prompt{prop:Color} = 15066597
    If ?tmp:Network{prop:ReadOnly} = True
        ?tmp:Network{prop:FontColor} = 65793
        ?tmp:Network{prop:Color} = 15066597
    Elsif ?tmp:Network{prop:Req} = True
        ?tmp:Network{prop:FontColor} = 65793
        ?tmp:Network{prop:Color} = 8454143
    Else ! If ?tmp:Network{prop:Req} = True
        ?tmp:Network{prop:FontColor} = 65793
        ?tmp:Network{prop:Color} = 16777215
    End ! If ?tmp:Network{prop:Req} = True
    ?tmp:Network{prop:Trn} = 0
    ?tmp:Network{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt21:2{prop:FontColor} = -1
    ?Prompt21:2{prop:Color} = 15066597
    ?EngineerDetails{prop:Font,3} = -1
    ?EngineerDetails{prop:Color} = 15066597
    ?EngineerDetails{prop:Trn} = 0
    ?Prompt17{prop:FontColor} = -1
    ?Prompt17{prop:Color} = 15066597
    ?engineer_name_temp{prop:FontColor} = -1
    ?engineer_name_temp{prop:Color} = 15066597
    ?Prompt17:3{prop:FontColor} = -1
    ?Prompt17:3{prop:Color} = 15066597
    ?tmp:SkillLevel{prop:FontColor} = -1
    ?tmp:SkillLevel{prop:Color} = 15066597
    ?Prompt17:4{prop:FontColor} = -1
    ?Prompt17:4{prop:Color} = 15066597
    ?tmp:DateAllocated{prop:FontColor} = -1
    ?tmp:DateAllocated{prop:Color} = 15066597
    ?tmp:JobSkillLevel:Prompt{prop:FontColor} = -1
    ?tmp:JobSkillLevel:Prompt{prop:Color} = 15066597
    If ?tmp:JobSkillLevel{prop:ReadOnly} = True
        ?tmp:JobSkillLevel{prop:FontColor} = 65793
        ?tmp:JobSkillLevel{prop:Color} = 15066597
    Elsif ?tmp:JobSkillLevel{prop:Req} = True
        ?tmp:JobSkillLevel{prop:FontColor} = 65793
        ?tmp:JobSkillLevel{prop:Color} = 8454143
    Else ! If ?tmp:JobSkillLevel{prop:Req} = True
        ?tmp:JobSkillLevel{prop:FontColor} = 65793
        ?tmp:JobSkillLevel{prop:Color} = 16777215
    End ! If ?tmp:JobSkillLevel{prop:Req} = True
    ?tmp:JobSkillLevel{prop:Trn} = 0
    ?tmp:JobSkillLevel{prop:FontStyle} = font:Bold
    ?Prompt21{prop:FontColor} = -1
    ?Prompt21{prop:Color} = 15066597
    ?JOB:Repair_Type:Prompt{prop:FontColor} = -1
    ?JOB:Repair_Type:Prompt{prop:Color} = 15066597
    If ?job:Repair_Type{prop:ReadOnly} = True
        ?job:Repair_Type{prop:FontColor} = 65793
        ?job:Repair_Type{prop:Color} = 15066597
    Elsif ?job:Repair_Type{prop:Req} = True
        ?job:Repair_Type{prop:FontColor} = 65793
        ?job:Repair_Type{prop:Color} = 8454143
    Else ! If ?job:Repair_Type{prop:Req} = True
        ?job:Repair_Type{prop:FontColor} = 65793
        ?job:Repair_Type{prop:Color} = 16777215
    End ! If ?job:Repair_Type{prop:Req} = True
    ?job:Repair_Type{prop:Trn} = 0
    ?job:Repair_Type{prop:FontStyle} = font:Bold
    ?JOB:Repair_Type_Warranty:Prompt{prop:FontColor} = -1
    ?JOB:Repair_Type_Warranty:Prompt{prop:Color} = 15066597
    If ?job:Repair_Type_Warranty{prop:ReadOnly} = True
        ?job:Repair_Type_Warranty{prop:FontColor} = 65793
        ?job:Repair_Type_Warranty{prop:Color} = 15066597
    Elsif ?job:Repair_Type_Warranty{prop:Req} = True
        ?job:Repair_Type_Warranty{prop:FontColor} = 65793
        ?job:Repair_Type_Warranty{prop:Color} = 8454143
    Else ! If ?job:Repair_Type_Warranty{prop:Req} = True
        ?job:Repair_Type_Warranty{prop:FontColor} = 65793
        ?job:Repair_Type_Warranty{prop:Color} = 16777215
    End ! If ?job:Repair_Type_Warranty{prop:Req} = True
    ?job:Repair_Type_Warranty{prop:Trn} = 0
    ?job:Repair_Type_Warranty{prop:FontStyle} = font:Bold
    ?JOB:Invoice_Text:Prompt{prop:FontColor} = -1
    ?JOB:Invoice_Text:Prompt{prop:Color} = 15066597
    If ?jbn:Invoice_Text{prop:ReadOnly} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 15066597
    Elsif ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 8454143
    Else ! If ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 16777215
    End ! If ?jbn:Invoice_Text{prop:Req} = True
    ?jbn:Invoice_Text{prop:Trn} = 0
    ?jbn:Invoice_Text{prop:FontStyle} = font:Bold
    ?job:Location:Prompt{prop:FontColor} = -1
    ?job:Location:Prompt{prop:Color} = 15066597
    If ?job:Location{prop:ReadOnly} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 15066597
    Elsif ?job:Location{prop:Req} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 8454143
    Else ! If ?job:Location{prop:Req} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 16777215
    End ! If ?job:Location{prop:Req} = True
    ?job:Location{prop:Trn} = 0
    ?job:Location{prop:FontStyle} = font:Bold
    ?PickNotePrompt{prop:FontColor} = -1
    ?PickNotePrompt{prop:Color} = 15066597
    ?MakeEstimateJobPrompt{prop:FontColor} = -1
    ?MakeEstimateJobPrompt{prop:Color} = 15066597
    ?Sheet4{prop:Color} = 15066597
    ?Tab5{prop:Color} = 15066597
    ?jobNotes{prop:FontColor} = -1
    ?jobNotes{prop:Color} = 15066597
    ?JOB:Fault_Description:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Description:Prompt{prop:Color} = 15066597
    ?JOB:Fault_Description:Prompt:2{prop:FontColor} = -1
    ?JOB:Fault_Description:Prompt:2{prop:Color} = 15066597
    If ?jbn:Fault_Description{prop:ReadOnly} = True
        ?jbn:Fault_Description{prop:FontColor} = 65793
        ?jbn:Fault_Description{prop:Color} = 15066597
    Elsif ?jbn:Fault_Description{prop:Req} = True
        ?jbn:Fault_Description{prop:FontColor} = 65793
        ?jbn:Fault_Description{prop:Color} = 8454143
    Else ! If ?jbn:Fault_Description{prop:Req} = True
        ?jbn:Fault_Description{prop:FontColor} = 65793
        ?jbn:Fault_Description{prop:Color} = 16777215
    End ! If ?jbn:Fault_Description{prop:Req} = True
    ?jbn:Fault_Description{prop:Trn} = 0
    ?jbn:Fault_Description{prop:FontStyle} = font:Bold
    If ?jbn:Engineers_Notes{prop:ReadOnly} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 15066597
    Elsif ?jbn:Engineers_Notes{prop:Req} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 8454143
    Else ! If ?jbn:Engineers_Notes{prop:Req} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 16777215
    End ! If ?jbn:Engineers_Notes{prop:Req} = True
    ?jbn:Engineers_Notes{prop:Trn} = 0
    ?jbn:Engineers_Notes{prop:FontStyle} = font:Bold
    ?Sheet3{prop:Color} = 15066597
    ?ChargeableTab{prop:Color} = 15066597
    ?Chargeable_Group{prop:Font,3} = -1
    ?Chargeable_Group{prop:Color} = 15066597
    ?Chargeable_Group{prop:Trn} = 0
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Warrantytab{prop:Color} = 15066597
    ?Warranty_Group{prop:Font,3} = -1
    ?Warranty_Group{prop:Color} = 15066597
    ?Warranty_Group{prop:Trn} = 0
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    ?Panel2{prop:Fill} = 15066597

    ?CancelText{prop:FontColor} = -1
    ?CancelText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
InitMaxParts    ROUTINE    !Start Change 1895 BE(15/05/03)
    tmpCheckChargeableParts = 0
    TotalPartsCost = 0.0
    access:SUBTRACC.clearkey(sub:account_number_key)
    sub:Account_Number  = job:Account_Number
    IF (access:SUBTRACC.tryfetch(sub:account_number_key) = Level:Benign) THEN
        access:TRADEACC.clearkey(tra:account_number_key)
        tra:Account_Number  = sub:Main_Account_Number
        IF (access:TRADEACC.tryfetch(tra:account_number_key) = Level:Benign) THEN
            TmpCheckChargeableParts = tra:CheckChargeablePartsCost
        END
    END
!End Change 1895 BE(15/05/03)
TotalChargeableParts    ROUTINE ! Start Change 1895 BE(15/05/03)
    TotalPartsCost = 0
    save_par_id2 = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    SET(par:Part_Number_Key,par:Part_Number_Key)
    LOOP
        IF ((Access:PARTS.NEXT() <> Level:Benign) OR |
            (par:Ref_Number  <> job:Ref_Number)) THEN
            BREAK
        END
        IF (par:Adjustment <> 'YES') THEN
            TotalPartsCost += par:Purchase_Cost * par:Quantity
        END
    END
    Access:PARTS_ALIAS.RestoreFile(save_par_id2)
! End Change 1895 BE(15/05/03)
CheckRequiredFields     Routine

    If ForceRepairType('B')
!        If ?job:Repair_Type{prop:Hide} = 0
            ?job:Repair_Type{prop:color} = 0BDFFFFH
            ?job:Repair_Type{prop:Req} = 1
!        Else !If ?job:Repair_Type{prop:Hide} = 0
!            ?job:Repair_Type{prop:color} = color:White
!            ?job:Repair_Type{prop:Req} = 0
!        End !If ?job:Repair_Type{prop:Hide} = 0
!        If ?job:Repair_Type_Warranty{prop:Hide} = 0
            ?job:Repair_Type_Warranty{prop:color} = 0BDFFFFH
            ?job:Repair_Type_Warranty{prop:Req} = 1
!        Else !If ?job:Repair_Type_Warranty{prop:Hide} = 0
!            ?job:Repair_Type_Warranty{prop:color} = color:White
!            ?job:Repair_Type_Warranty{prop:Req} = 0
!        End !If ?job:Repair_Type_Warranty{prop:Hide} = 0
    Else !If ForceRepairType('B')
        ?job:Repair_Type{prop:color} = color:White
        ?job:Repair_Type{prop:Req} = 0
        ?job:Repair_Type_Warranty{prop:color} = color:White
        ?job:Repair_Type_Warranty{prop:Req} = 0
    End !If ForceRepairType('B')
MakePartsOrder      Routine
    If Access:ORDPEND.PrimeRecord() = Level:Benign
        ope:Part_Ref_Number = sto:Ref_number
        ope:Job_Number      = job:Ref_number
        ope:Part_Type       = 'JOB'
        ope:Supplier        = sto:Supplier
        ope:Part_Number     = sto:Part_Number
        ope:Description     = sto:Description
        ope:Quantity        = 1
        ope:Account_Number  = job:Account_Number
        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !If Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
MakeWarPartsOrder      Routine
    If Access:ORDPEND.PrimeRecord() = Level:Benign
        ope:Part_Ref_Number = sto:Ref_number
        ope:Job_Number      = job:Ref_number
        ope:Part_Type       = 'WAR'
        ope:Supplier        = sto:Supplier
        ope:Part_Number     = sto:Part_Number
        ope:Description     = sto:Description
        ope:Quantity        = 1
        ope:Account_Number  = job:Account_Number
        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !If Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
AddEngineer     Routine
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found

    Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
        Error# = 0
        Set(DEFAULT2)
        Access:DEFAULT2.Next()
        If de2:UserSkillLevel
            If use:SkillLevel < jobe:SkillLevel
                Case MessageEx('The selected Engineer''s Skill Level is not high enough to be allocated to this job.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                Error# = 1
                job:Engineer    = Engineer_Temp
            End !If use:SkillLevel < jobe:SkillLevel
        End !If de2:UserSkillLevel
        If Error# = 0
            Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
            tmp:SkillLevel   = use:SkillLevel
        End !If Error# = 0
    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

    Do_Status# = 0
    
    If job:Engineer <> Engineer_Temp    
        IF job:Date_Completed <> ''        
            Error# = 1            
            Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                           'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            End!Case MessageEx
        End !IF job:Date_Completed <> ''        
        If job:Invoice_Number <> ''
            Error# = 1            
            Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                           'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            End!Case MessageEx

        End !If job:Invoice_Number <> ''
        If Error# = 0
            If Engineer_Temp = ''

                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:notes         = 'SKILL LEVEL: ' & Clip(use:SkillLevel)
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    aud:type          = 'JOB'
                    access:users.clearkey(use:password_key)
                    use:password =glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'ENGINEER ALLOCATED: ' & CLip(job:engineer)
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:benign

                If job:In_Repair <> 'YES'
                    job:In_Repair = 'YES'
                    job:Date_In_Repair = Today()
                    job:Time_In_Repair = Clock()
                End !If job:In_Repair <> 'YES'

                do_status# = 1
                engineer_temp = job:engineer

                If Access:JOBSENG.PrimeRecord() = Level:Benign
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = job:Engineer
                    joe:DateAllocated = Today()
                    joe:TimeAllocated = Clock()
                    joe:AllocatedBy   = use:User_Code
                    access:users.clearkey(use:User_Code_Key)
                    use:User_Code   = job:Engineer
                    access:users.fetch(use:User_Code_Key)

                    joe:EngSkillLevel = use:SkillLevel
                    joe:JobSkillLevel = jobe:SkillLevel

                    If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:JOBSENG.TryInsert() = Level:Benign
                End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                
            Else !If Engineer_Temp = ''
                Case MessageEx('Are you sure you want to change the Engineer?','ServiceBase 2000',|
                               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                    Of 1 ! &Yes Button

                        get(audit,0)
                        if access:audit.primerecord() = level:benign
                            aud:notes         = 'NEW ENGINEER: ' & CLip(job:Engineer) & |
                                                '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                                                '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(engineer_temp)
                            aud:ref_number    = job:ref_number
                            aud:date          = today()
                            aud:time          = clock()
                            aud:type          = 'JOB'
                            access:users.clearkey(use:password_key)
                            use:password =glo:password
                            access:users.fetch(use:password_key)
                            aud:user = use:user_code
                            aud:action        = 'ENGINEER CHANGED TO ' & CLip(job:engineer)
                            access:audit.insert()
                        end!�if access:audit.primerecord() = level:benign

                        Engineer_Temp   = job:Engineer

                        If Access:JOBSENG.PrimeRecord() = Level:Benign
                            joe:JobNumber     = job:Ref_Number
                            joe:UserCode      = job:Engineer
                            joe:DateAllocated = Today()
                            joe:TimeAllocated = Clock()
                            joe:AllocatedBy   = use:User_Code
                            access:users.clearkey(use:User_Code_Key)
                            use:User_Code   = job:Engineer
                            access:users.fetch(use:User_Code_Key)

                            joe:EngSkillLevel = use:SkillLevel
                            joe:JobSkillLevel = jobe:SkillLevel

                            If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:JOBSENG.TryInsert() = Level:Benign
                        End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                    Of 2 ! &No Button
                        Job:Engineer    = Engineer_Temp
                End!Case MessageEx
            End !If Engineer_Temp = ''
        Else !!If Error# = 0
            job:Engineer    = Engineer_Temp
        End !If Error# = 0
        
    End !If job:Engineer <> Engineer_Temp    

    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
        Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
        tmp:SkillLevel   = use:SkillLevel
    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
    
    If do_status# = 1
        GetStatus(310,thiswindow.request,'JOB') !allocated to engineer
        ! Start Change 3940 BE(02/03/04)
        TempStatusCode = job:Current_Status
        ! End Change 3940 BE(02/03/04)

    End!If do_status# = 1

    Do Lookup_Engineer
stock_history       Routine        !Do The Prime, and Set The Quantity First
    shi:ref_number      = sto:ref_number
    access:users.clearkey(use:password_key)
    use:password        =glo:password
    access:users.fetch(use:password_key)
    shi:user            = use:user_code
    shi:date            = Today()
    shi:transaction_type    = 'DEC'
    shi:despatch_note_number    = par:despatch_note_number
    shi:job_number      = job:ref_number
    shi:purchase_cost   = par:purchase_cost
    shi:sale_cost       = par:sale_cost
    shi:retail_cost     = par:retail_cost
    shi:notes           = 'STOCK DECREMENTED'
    access:stohist.insert()
check_force_fault_codes    Routine
    force_fault_codes_temp = 'NO'
    If job:chargeable_job = 'YES'
        required# = 0
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:chargeable = 'YES'
    If job:warranty_job = 'YES' And force_fault_codes_temp <> 'YES'
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:warranty_charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:warranty_job = 'YES'
Enable_Disable      Routine
    If job:warranty_job = 'YES'
        Enable(?warranty_group)
    Else
        disable(?warranty_group)
    End

    If job:chargeable_job = 'YES'
        Enable(?chargeable_group)
    Else
        disable(?chargeable_group)
    End
Pricing     Routine
    If job:invoice_number <> ''
        job:labour_cost = job:invoice_labour_cost
        job:parts_cost  = job:invoice_parts_cost
    Else!End!If job:invoice_number <> ''
!       Pricing_Routine(labour",parts",labour_warranty",parts_warranty",pass",pass_warranty",discount",discount_warranty")
        Pricing_Routine('C',labour",parts",pass",a")
        If pass" = True
            job:labour_cost = labour"
            job:parts_cost  = parts"
        Else
            job:labour_cost = 0
            job:parts_cost  = 0
        End!If pass" = True
    End!If job:invoice_number <> ''
    IF job:invoice_number_warranty <> ''
        job:labour_cost_warranty = JOB:WInvoice_Labour_Cost
        job:parts_cost_warranty  = JOB:WInvoice_Parts_Cost
    Else!IF job:warranty_invoice_number
        Pricing_Routine('W',labour",parts",pass",a")
        If pass" = True
            job:labour_cost_warranty    = labour"
            job:parts_cost_warranty     = parts"
        Else
            job:labour_cost_warranty = 0
            job:parts_cost_warranty = 0
        End!If pass_warranty" = True
    End!IF job:warranty_invoice_number
    Display()
Check_Parts     Routine
    found_requested# = 0
    found_ordered# = 0
    found_received# = 0
    setcursor(cursor:wait)
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        If wpr:pending_ref_number <> ''
            found_requested# = 1
            Break
        End                                     
        If wpr:order_number <> ''
            If wpr:date_received = ''
                found_ordered# = 1
                ! Break !Check all the parts. Don't Break - TrkBs: 5057 (DBH: 07-12-2004)
            Else!If wpr:date_received = ''
                found_received# = 1
                ! Break !Check all the parts. Don't Break - TrkBs: 5057 (DBH: 07-12-2004)
            End!If wpr:date_received = ''
        End
    end !loop
    access:warparts.restorefile(save_wpr_id)
    setcursor()

    If found_requested# = 0
        setcursor(cursor:wait)
        save_par_id = access:parts.savefile()
        access:parts.clearkey(par:part_number_key)
        par:ref_number  = job:ref_number
        set(par:part_number_key,par:part_number_key)
        loop
            if access:parts.next()
               break
            end !if
            if par:ref_number  <> job:ref_number      |
                then break.  ! end if
            If par:pending_ref_number <> ''
                found_requested# = 1
                Break
            End                                     
            If par:order_number <> ''
                If par:date_received = ''
                    found_ordered# = 1
                    ! Break !Check all the parts. Don't Break - TrkBs: 5057 (DBH: 07-12-2004)
                Else!If par:date_received = ''
                    found_received# = 1
                    ! Break !Check all the parts. Don't Break - TrkBs: 5057 (DBH: 07-12-2004)
                End!If par:date_received = ''
            End
        end !loop
        access:parts.restorefile(save_par_id)
        setcursor()
    End!If found_requested# = 0
            
    If found_requested# = 1
        GetStatus(330,0,'JOB') !spares requested
        ! Start Change 3940 BE(02/03/04)
        TempStatusCode = job:Current_Status
        ! End Change 3940 BE(02/03/04)

        job:status_end_date = end_date"
        job:status_end_time = end_time"
        
        
    Else!If found_requested# = 1
        If found_ordered# = 1
            GetStatus(335,0,'JOB') !spares ordered
            ! Start Change 3940 BE(02/03/04)
            TempStatusCode = job:Current_Status
            ! End Change 3940 BE(02/03/04)

        Else!If found_ordered# = 1
            If found_received# = 1
                GetStatus(345,0,'JOB') !spares received
                ! Start Change 3940 BE(02/03/04)
                TempStatusCode = job:Current_Status
                ! End Change 3940 BE(02/03/04)

            End!If found_received# = 1
        End!If found_ordered# = 1
    End!If found_requested# = 1

    Display()

Lookup_Engineer     Routine
    !Engineer
    Save_joe_ID = Access:JOBSENG.SaveFile()
    ! Start Change 3940 BE(02/03/04)
    !Access:JOBSENG.ClearKey(joe:UserCodeOnlyKey)
    Access:JOBSENG.ClearKey(joe:UserCodeKey)
    joe:JobNumber = job:Ref_Number
    ! End Change 3940 BE(02/03/04)
    joe:UserCode      = job:Engineer
    joe:DateAllocated = Today()
    ! Start Change 3940 BE(02/03/04)
    !Set(joe:UserCodeOnlyKey,joe:UserCodeOnlyKey)
    Set(joe:UserCodeKey,joe:UserCodeKey)
    ! End Change 3940 BE(02/03/04)
    Loop
        If Access:JOBSENG.PREVIOUS()
           Break
        End !If
        ! Start Change 3940 BE(02/03/04)
        !If joe:UserCode      <> job:Engineer      |
        !Or joe:DateAllocated > Today()      |
        !    Then Break.  ! End If
        IF ((joe:JobNumber <> job:Ref_Number) OR |
            (joe:UserCode <> job:Engineer) OR |
            (joe:DateAllocated > Today())) THEN
            BREAK
        END
        ! End Change 3940 BE(02/03/04)
        tmp:SkillLevel      = joe:EngSkillLevel
        tmp:DateAllocated   = joe:DateAllocated
        Break
    End !Loop
    Access:JOBSENG.RestoreFile(Save_joe_ID)

    Access:users.Clearkey(use:user_code_key)
    use:user_code    = job:engineer
    If Access:users.Fetch(use:user_Code_Key) = Level:Benign
        engineer_name_temp   = Clip(use:forename) & ' ' & Clip(use:surname)
!        tmp:SkillLevel  = use:SkillLevel
    End
    Display()

Date_Completed      Routine     !When Date Completed Filled in
    Include('completed.inc')
    If error# = 1
        glo:errortext = 'This job cannot be completed due to the following error(s): <13,10>' & Clip(glo:errortext)
        Error_Text
        glo:errortext = ''
    End!If error# = 1

    !ThisMakeover.SetWindow(win:form)
QA_Group        Routine
    hide(?qa_group)
    If def:qa_before_complete = 'YES'
        If job:date_completed <> ''
            Unhide(?qa_group)
            Disable(?qa_group)
        Else!If job:date_completed = 'YES'
            Unhide(?qa_group)
            Disable(?qa_group)
        End!If job:date_completed = 'YES'
    End
    If def:qa_required = 'YES'
        If job:date_completed <> ''
            Unhide(?qa_group)
        Else
            Hide(?qa_group)
        End
    End
    If def:rapidqa = 'YES'
        Disable(?qa_group)
    End!If def:rapidqa = 'YES'
Check_For_Despatch        Routine
    Set(defaults)
    access:defaults.next()

    despatch# = 0

    if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        despatch# = 1
    Else!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If sub:despatch_invoiced_jobs <> 'YES' And sub:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                Else!If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If tra:despatch_invoiced_jobs <> 'YES' And tra:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                End!If tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
    End!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'

    If despatch# = 1
        tmp:DespatchClose   = 0
        tmp:RestockingNote  = 0
        If job:despatched <> 'YES'
            If ToBeLoaned() = 1
                restock# = 0
                If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                    Case MessageEx('A Loan Unit has been issued to this job but the Initial Transit Type has been set-up so that an Exchange Unit is required.<13,10><13,10>Do you wish to continue and mark this unit for Despatch back to the Customer, or do you want to Restock it?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Continue|&Restock Unit',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Continue Button
                        Of 2 ! &Restock Unit Button
                            ForceDespatch()
                            tmp:restockingnote = 1
                            restock# = 1
                    End!Case MessageEx
                End!If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                If restock# = 0
                    job:despatched  = 'LAT'
                    job:despatch_type   = 'JOB'
                End!If restock# = 0
            Else!IF ToBeLoaned() = 1
                If ToBeExchanged() ! Job Will Be Exchanged
                    ForceDespatch()
                    tmp:restockingnote = 1
                Else!If ToBeExchanged() ! Job Will Be Exchanged
                    job:date_Despatched = DespatchANC(job:courier,'JOB')

                    access:courier.clearkey(cou:courier_key)
                    cou:courier = job:courier
                    if access:courier.tryfetch(cou:courier_key) = Level:Benign
                        If cou:despatchclose = 'YES'
                            tmp:DespatchClose   = 1
                        Else!If cou:despatchclose = 'YES'
                            tmp:DespatchClose   = 0
                        End!If cou:despatchclose = 'YES'
                    End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                End!If ToBeExchanged() ! Job Will Be Exchanged
            End!If ToBeLoaned() = 1
        End!If job:despatched <> 'YES'
    End!If despatch# = 1
Add_Warranty_Part     Routine
    Clear(wpr:Record)
    get(warparts,0)
    if access:warparts.primerecord() = level:benign
        wpr:ref_number           = job:ref_number
        wpr:adjustment           = 'NO'
        wpr:part_number     = sto:part_number
        wpr:description     = sto:description
        wpr:supplier        = sto:supplier
        wpr:purchase_cost   = sto:purchase_cost
        wpr:sale_cost       = sto:sale_cost
        wpr:retail_cost     = sto:retail_cost
        wpr:quantity             = 1
        wpr:warranty_part        = 'NO'
        wpr:exclude_from_order   = 'NO'
        wpr:fault_codes_checked  = 'YES'
        wpr:part_ref_Number      = sto:ref_number
        wpr:date_ordered         = Today()
        If sto:assign_fault_codes = 'YES'
            wpr:fault_code1    = sto:fault_code1
            wpr:fault_code2    = sto:fault_code2
            wpr:fault_code3    = sto:fault_code3
            wpr:fault_code4    = sto:fault_code4
            wpr:fault_code5    = sto:fault_code5
            wpr:fault_code6    = sto:fault_code6
            wpr:fault_code7    = sto:fault_code7
            wpr:fault_code8    = sto:fault_code8
            wpr:fault_code9    = sto:fault_code9
            wpr:fault_code10   = sto:fault_code10
            wpr:fault_code11   = sto:fault_code11
            wpr:fault_code12   = sto:fault_code12
        End!If sto:assign_fault_codes = 'YES'
        if access:warparts.insert()
            access:warparts.cancelautoinc()
        end
    end!if access:warparts.primerecord() = level:benign

Add_Chargeable_Part     Routine
    Clear(par:record)
    get(parts,0)
    if access:parts.primerecord() = level:benign
        par:ref_number           = job:ref_number
        par:adjustment           = 'NO'
        par:part_number     = sto:part_number
        par:description     = sto:description
        par:supplier        = sto:supplier
        par:purchase_cost   = sto:purchase_cost
        par:sale_cost       = sto:sale_cost
        par:retail_cost     = sto:retail_cost
        par:quantity             = 1
        par:warranty_part        = 'NO'
        par:exclude_from_order   = 'NO'
        par:fault_codes_checked  = 'YES'
        par:part_ref_Number      = sto:ref_number
        par:date_ordered         = Today()
        If sto:assign_fault_codes = 'YES'
            par:fault_code1    = sto:fault_code1
            par:fault_code2    = sto:fault_code2
            par:fault_code3    = sto:fault_code3
            par:fault_code4    = sto:fault_code4
            par:fault_code5    = sto:fault_code5
            par:fault_code6    = sto:fault_code6
            par:fault_code7    = sto:fault_code7
            par:fault_code8    = sto:fault_code8
            par:fault_code9    = sto:fault_code9
            par:fault_code10   = sto:fault_code10
            par:fault_code11   = sto:fault_code11
            par:fault_code12   = sto:fault_code12
        End!If sto:assign_fault_codes = 'YES'
        if access:parts.insert()
            access:parts.cancelautoinc()
        end
    end!if access:warparts.primerecord() = level:benign
CountParts      Routine
    Count# = 0
    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        Count# += 1
    End !Loop
    Access:PARTS.RestoreFile(Save_par_ID)

    ?ChargeableTab{prop:Text} = 'Chargeable Parts (' & Count# & ')'

    Count# = 0
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        Count# += 1
    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)

    ?WarrantyTab{prop:Text} = 'Warranty Parts (' & Count# & ')'

    Display()
fill_lists      Routine
   BRW10.ResetSort(1)
   BRW9.ResetSort(1)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Jobs_Rapid',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('MakeEstimateJobFlag',MakeEstimateJobFlag,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp_RF_Board_IMEI',tmp_RF_Board_IMEI,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmpCheckChargeableParts',tmpCheckChargeableParts,'Update_Jobs_Rapid',1)
    SolaceViewVars('TotalPartsCost',TotalPartsCost,'Update_Jobs_Rapid',1)
    SolaceViewVars('TempStatusCode',TempStatusCode,'Update_Jobs_Rapid',1)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_par_id2',save_par_id2,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_joe_id',save_joe_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_taf_id',save_taf_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:RestockingNote',tmp:RestockingNote,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:chargetype',tmp:chargetype,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:WarrantyChargeType',tmp:WarrantyChargeType,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:RepairType',tmp:RepairType,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:WarrantyRepairType',tmp:WarrantyRepairType,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:UnitType',tmp:UnitType,'Update_Jobs_Rapid',1)
    SolaceViewVars('invoice_number_temp',invoice_number_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:CreateInvoice',tmp:CreateInvoice,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:ConsignNo',tmp:ConsignNo,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:OldConsignNo',tmp:OldConsignNo,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:LabelError',tmp:LabelError,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:DespatchClose',tmp:DespatchClose,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_lac_id',save_lac_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_job_id',save_job_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('chargeable_job_temp',chargeable_job_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('warranty_job_temp',warranty_job_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_cha_id',save_cha_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('Print_Despatch_Note_temp',Print_Despatch_Note_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:accountnumber',tmp:accountnumber,'Update_Jobs_Rapid',1)
    SolaceViewVars('account_number2_temp',account_number2_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('sav:path',sav:path,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_cou_ali_id',save_cou_ali_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('error_queue_temp:field',error_queue_temp:field,'Update_Jobs_Rapid',1)
    SolaceViewVars('error_message_temp',error_message_temp,'Update_Jobs_Rapid',1)
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'error_type_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",error_type_temp[SolaceDim1#],'Update_Jobs_Rapid',1)
      End
    
    
    SolaceViewVars('check_for_bouncers_temp',check_for_bouncers_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('saved_ref_number_temp',saved_ref_number_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('saved_esn_temp',saved_esn_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_aud_id',save_aud_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('saved_msn_temp',saved_msn_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('engineer_ref_number_temp',engineer_ref_number_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('engineer_quantity_temp',engineer_quantity_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('main_store_ref_number_temp',main_store_ref_number_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('main_store_quantity_temp',main_store_quantity_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('main_store_sundry_temp',main_store_sundry_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('engineer_sundry_temp',engineer_sundry_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_ccp_id',save_ccp_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('save_cwp_id',save_cwp_id,'Update_Jobs_Rapid',1)
    SolaceViewVars('Force_Fault_Codes_Temp',Force_Fault_Codes_Temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('date_completed_temp',date_completed_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('time_completed_temp',time_completed_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('qa_passed_temp',qa_passed_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('qa_rejected_temp',qa_rejected_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('qa_second_passed_temp',qa_second_passed_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('location_temp',location_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('date_error_temp',date_error_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('engineer_name_temp',engineer_name_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Jobs_Rapid',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Jobs_Rapid',1)
    SolaceViewVars('title_text_temp',title_text_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('trade_account_temp',trade_account_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('model_details_temp',model_details_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('DisplayString',DisplayString,'Update_Jobs_Rapid',1)
    SolaceViewVars('title_text2_temp',title_text2_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('engineer_temp',engineer_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('check_for_bouncer_temp',check_for_bouncer_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('qa_rejection_reason_temp',qa_rejection_reason_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('exchange_loan_temp',exchange_loan_temp,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:invoicetext',tmp:invoicetext,'Update_Jobs_Rapid',1)
    SolaceViewVars('SaveGroup:ChargeType',SaveGroup:ChargeType,'Update_Jobs_Rapid',1)
    SolaceViewVars('SaveGroup:WarrantyChargeType',SaveGroup:WarrantyChargeType,'Update_Jobs_Rapid',1)
    SolaceViewVars('SaveGroup:RepairType',SaveGroup:RepairType,'Update_Jobs_Rapid',1)
    SolaceViewVars('SaveGroup:WarrantyRepairType',SaveGroup:WarrantyRepairType,'Update_Jobs_Rapid',1)
    SolaceViewVars('SaveGroup:ChargeableJob',SaveGroup:ChargeableJob,'Update_Jobs_Rapid',1)
    SolaceViewVars('SaveGroup:WarrantyJob',SaveGroup:WarrantyJob,'Update_Jobs_Rapid',1)
    SolaceViewVars('ChangedGroup:cct',ChangedGroup:cct,'Update_Jobs_Rapid',1)
    SolaceViewVars('ChangedGroup:wct',ChangedGroup:wct,'Update_Jobs_Rapid',1)
    SolaceViewVars('ChangedGroup:crt',ChangedGroup:crt,'Update_Jobs_Rapid',1)
    SolaceViewVars('ChangedGroup:wrt',ChangedGroup:wrt,'Update_Jobs_Rapid',1)
    SolaceViewVars('ChangedGroup:chj',ChangedGroup:chj,'Update_Jobs_Rapid',1)
    SolaceViewVars('ChangedGroup:wrj',ChangedGroup:wrj,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:ParcellineName',tmp:ParcellineName,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:WorkStationName',tmp:WorkStationName,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:SkillLevel',tmp:SkillLevel,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:no',tmp:no,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:yes',tmp:yes,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:DateAllocated',tmp:DateAllocated,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:Network',tmp:Network,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:JobSkillLevel',tmp:JobSkillLevel,'Update_Jobs_Rapid',1)
    SolaceViewVars('tmp:SaveAccessory',tmp:SaveAccessory,'Update_Jobs_Rapid',1)
    SolaceViewVars('partpntstore',partpntstore,'Update_Jobs_Rapid',1)
    SolaceViewVars('warpartpntstore',warpartpntstore,'Update_Jobs_Rapid',1)
    SolaceViewVars('SaveAccessoryQueue:Accessory',SaveAccessoryQueue:Accessory,'Update_Jobs_Rapid',1)
    SolaceViewVars('SaveAccessoryQueue:Type',SaveAccessoryQueue:Type,'Update_Jobs_Rapid',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?title_text2_temp;  SolaceCtrlName = '?title_text2_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt20;  SolaceCtrlName = '?Prompt20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Completed;  SolaceCtrlName = '?Completed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ref_Number;  SolaceCtrlName = '?job:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trade_account_temp;  SolaceCtrlName = '?trade_account_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?model_details_temp;  SolaceCtrlName = '?model_details_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ESN;  SolaceCtrlName = '?job:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:MSN;  SolaceCtrlName = '?job:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt13;  SolaceCtrlName = '?Prompt13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TempStatusCode;  SolaceCtrlName = '?TempStatusCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?exchange_loan_temp;  SolaceCtrlName = '?exchange_loan_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?qa_rejection_reason_temp:prompt;  SolaceCtrlName = '?qa_rejection_reason_temp:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Date_Completed:Prompt;  SolaceCtrlName = '?JOB:Date_Completed:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Date_Completed;  SolaceCtrlName = '?job:Date_Completed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Time_Completed;  SolaceCtrlName = '?job:Time_Completed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Order_Number;  SolaceCtrlName = '?job:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Warranty_Charge_Type;  SolaceCtrlName = '?job:Warranty_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QA_Group;  SolaceCtrlName = '?QA_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:QA_Passed;  SolaceCtrlName = '?job:QA_Passed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Date_QA_Passed;  SolaceCtrlName = '?job:Date_QA_Passed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Time_QA_Passed;  SolaceCtrlName = '?job:Time_QA_Passed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Chargeable_Job;  SolaceCtrlName = '?job:Chargeable_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Charge_Type;  SolaceCtrlName = '?job:Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StatusWarning;  SolaceCtrlName = '?StatusWarning';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Warranty_Job;  SolaceCtrlName = '?job:Warranty_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?qa_rejection_reason_temp;  SolaceCtrlName = '?qa_rejection_reason_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Common_Faults;  SolaceCtrlName = '?Common_Faults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChangeJobStatus;  SolaceCtrlName = '?ChangeJobStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22;  SolaceCtrlName = '?Prompt22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22:2;  SolaceCtrlName = '?Prompt22:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22:3;  SolaceCtrlName = '?Prompt22:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RemoveParts;  SolaceCtrlName = '?RemoveParts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22:6;  SolaceCtrlName = '?Prompt22:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MakeEstimateJob;  SolaceCtrlName = '?MakeEstimateJob';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22:4;  SolaceCtrlName = '?Prompt22:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22:5;  SolaceCtrlName = '?Prompt22:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Network:Prompt;  SolaceCtrlName = '?tmp:Network:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Network;  SolaceCtrlName = '?tmp:Network';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupNetwork;  SolaceCtrlName = '?LookupNetwork';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AmendUnitType;  SolaceCtrlName = '?AmendUnitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt21:2;  SolaceCtrlName = '?Prompt21:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EngineerDetails;  SolaceCtrlName = '?EngineerDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Engineer;  SolaceCtrlName = '?Lookup_Engineer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17;  SolaceCtrlName = '?Prompt17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?engineer_name_temp;  SolaceCtrlName = '?engineer_name_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17:3;  SolaceCtrlName = '?Prompt17:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SkillLevel;  SolaceCtrlName = '?tmp:SkillLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17:4;  SolaceCtrlName = '?Prompt17:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DateAllocated;  SolaceCtrlName = '?tmp:DateAllocated';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EngineerHistory;  SolaceCtrlName = '?EngineerHistory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobSkillLevel:Prompt;  SolaceCtrlName = '?tmp:JobSkillLevel:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobSkillLevel;  SolaceCtrlName = '?tmp:JobSkillLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AllocateJob;  SolaceCtrlName = '?AllocateJob';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt21;  SolaceCtrlName = '?Prompt21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Repair_Type:Prompt;  SolaceCtrlName = '?JOB:Repair_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Repair_Type;  SolaceCtrlName = '?job:Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupChargeableRepairType;  SolaceCtrlName = '?LookupChargeableRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Repair_Type_Warranty:Prompt;  SolaceCtrlName = '?JOB:Repair_Type_Warranty:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Repair_Type_Warranty;  SolaceCtrlName = '?job:Repair_Type_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupWarrantyRepairType;  SolaceCtrlName = '?LookupWarrantyRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Invoice_Text:Prompt;  SolaceCtrlName = '?JOB:Invoice_Text:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Invoice_Text;  SolaceCtrlName = '?Lookup_Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Invoice_Text;  SolaceCtrlName = '?jbn:Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Location:Prompt;  SolaceCtrlName = '?job:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Location;  SolaceCtrlName = '?job:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PickNotePrompt;  SolaceCtrlName = '?PickNotePrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PickNote;  SolaceCtrlName = '?PickNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MakeEstimateJobPrompt;  SolaceCtrlName = '?MakeEstimateJobPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Codes;  SolaceCtrlName = '?Fault_Codes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ViewAccessories;  SolaceCtrlName = '?ViewAccessories';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobNotes;  SolaceCtrlName = '?jobNotes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Description:Prompt;  SolaceCtrlName = '?JOB:Fault_Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Description:Prompt:2;  SolaceCtrlName = '?JOB:Fault_Description:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Fault_Description;  SolaceCtrlName = '?jbn:Fault_Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Engineers_Notes;  SolaceCtrlName = '?jbn:Engineers_Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?engineers_notes;  SolaceCtrlName = '?engineers_notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChargeableTab;  SolaceCtrlName = '?ChargeableTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Chargeable_Group;  SolaceCtrlName = '?Chargeable_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Multi_Insert;  SolaceCtrlName = '?Multi_Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Add_Chargeable_Adjustment;  SolaceCtrlName = '?Add_Chargeable_Adjustment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Warrantytab;  SolaceCtrlName = '?Warrantytab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Warranty_Group;  SolaceCtrlName = '?Warranty_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Multi_Insert:2;  SolaceCtrlName = '?Multi_Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Add_Warranty_Adjustment;  SolaceCtrlName = '?Add_Warranty_Adjustment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Costs;  SolaceCtrlName = '?Costs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CompleteButton;  SolaceCtrlName = '?CompleteButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ContactHistory;  SolaceCtrlName = '?ContactHistory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Invoice;  SolaceCtrlName = '?Invoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelText;  SolaceCtrlName = '?CancelText';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Job'
  OF ChangeRecord
    ActionMessage = 'Changing A Job'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Jobs_Rapid')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Jobs_Rapid')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?title_text2_temp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(job:Record,History::job:Record)
  SELF.AddHistoryField(?job:Ref_Number,1)
  SELF.AddHistoryField(?job:ESN,16)
  SELF.AddHistoryField(?job:MSN,17)
  SELF.AddHistoryField(?job:Date_Completed,83)
  SELF.AddHistoryField(?job:Time_Completed,84)
  SELF.AddHistoryField(?job:Order_Number,42)
  SELF.AddHistoryField(?job:Warranty_Charge_Type,37)
  SELF.AddHistoryField(?job:QA_Passed,51)
  SELF.AddHistoryField(?job:Date_QA_Passed,52)
  SELF.AddHistoryField(?job:Time_QA_Passed,53)
  SELF.AddHistoryField(?job:Chargeable_Job,13)
  SELF.AddHistoryField(?job:Charge_Type,36)
  SELF.AddHistoryField(?job:Warranty_Job,12)
  SELF.AddHistoryField(?job:Repair_Type,89)
  SELF.AddHistoryField(?job:Repair_Type_Warranty,90)
  SELF.AddHistoryField(?job:Location,24)
  SELF.AddUpdateFile(Access:JOBS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:COMMONCP.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:EXCHANGE.Open
  Relate:JOBEXACC.Open
  Relate:JOBS2_ALIAS.Open
  Relate:NETWORKS.Open
  Relate:PARTS_ALIAS.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Relate:REPTYDEF.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WARPARTS_ALIAS.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:STOCK.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  Access:MANFAULT.UseFile
  Access:MANUFACT.UseFile
  Access:EXCHHIST.UseFile
  Access:JOBEXHIS.UseFile
  Access:COMMONFA.UseFile
  Access:COMMONWP.UseFile
  Access:USELEVEL.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:ORDERS.UseFile
  Access:STOMODEL.UseFile
  Access:COURIER.UseFile
  Access:JOBNOTES.UseFile
  Access:MANFAUPA.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:ALLLEVEL.UseFile
  Access:REPAIRTY.UseFile
  Access:TRAFAULT.UseFile
  Access:CONTHIST.UseFile
  Access:CHARTYPE.UseFile
  Access:JOBSENG.UseFile
  Access:JOBSE.UseFile
  Access:LOCINTER.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  access:jobnotes.clearkey(jbn:RefNumberKey)
  jbn:RefNumber   = job:ref_number
  If access:jobnotes.tryfetch(jbn:RefNumberKey)
      access:jobnotes.primerecord()
      jbn:refNumber   = job:ref_number
      access:jobnotes.insert()
  End!
  tmp:InvoiceText = jbn:Invoice_Text
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
      tmp:Network = jobe:Network
      tmp:JobSkillLevel = jobe:SkillLevel
  Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  !Will anything change?
  Include('JobChnge.inc','Save')
  !Save Accesories incase they change
  If GetTempPathA(255,TempFilePath)
      If Sub(TempFilePath,-1,1) = '\'
          tmp:SaveAccessory = Clip(TempFilePath) & 'SAVACC' & Clock() & '.TMP'
      Else !If Sub(TempFilePath,-1,1) = '\'
          tmp:SaveAccessory = Clip(TempFilePath) & '\SAVACC' & Clock() & '.TMP'
      End !If Sub(TempFilePath,-1,1) = '\'
      Remove(tmp:SaveAccessory)
  
      Create(SaveAccessory)
      Open(SaveAccessory)
      If ~Error()
          Save_jac_ID = Access:JOBACC.SaveFile()
          Access:JOBACC.ClearKey(jac:Ref_Number_Key)
          jac:Ref_Number = job:Ref_Number
          Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
          Loop
              If Access:JOBACC.NEXT()
                 Break
              End !If
              If jac:Ref_Number <> job:Ref_Number      |
                  Then Break.  ! End If
              savacc:Accessory = jac:Accessory
              Add(SaveAccessory)
          End !Loop
          Access:JOBACC.RestoreFile(Save_jac_ID)
      End !If ~Error()
      Close(SaveAccessory)
  End
  
  
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:PARTS,SELF)
  BRW10.Init(?List:2,Queue:Browse:1.ViewPosition,BRW10::View:Browse,Queue:Browse:1,Relate:WARPARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If job:batch_number <> ''
      title_text_temp ='Repair Details  -  Batch Number: ' & Clip(Format(job:batch_number,@p<<<<<<#p))
  Else!If job:batch_number <> ''
      title_text_temp ='Repair Details'
  End!If job:batch_number <> ''
  
  title_text2_temp = 'Date Booked: ' &  Clip(Format(job:date_booked,@d6b)) & ' ' & |
                                  Clip(Format(job:time_booked,@t1b)) & ' - ' & Clip(job:who_booked)
  
  If job:surname <> ''
      trade_account_temp = Clip(job:account_number) & ' (' & Clip(job:title) & ' ' & Clip(job:initial) & |
                              ' ' & Clip(job:surname) & ')'
  Else
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number    = job:Account_Number
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number    = sub:Main_Account_Number
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
  
          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          
      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
  
      trade_account_temp = Clip(job:account_number) & ' (' & Clip(sub:Company_Name) & ')'
  End
  
  model_details_temp = Clip(job:model_number) & ' ' & Clip(job:manufacturer) & ' - ' & Clip(job:unit_type)
  
  !Save Temporary Fields
  tmp:chargetype          = job:charge_type
  tmp:WarrantyChargeType  = job:Warranty_Charge_type
  tmp:RepairType          = job:repair_type
  tmp:WarrantyRepairType  = job:Repair_Type_Warranty
  tmp:UnitType            = job:Unit_type
  !Completed?
  If job:Date_Completed <> ''
      ?Completed{prop:hide} = 0
  End!If job:Date_Completed <> ''
  ! Status Warning
  ! Start Change 2618 BE(05/08/03)
  JSC:Colour[1] = COLOR:Maroon
  JSC:Action[1] = GETINI('JobStatusColours','MaroonAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[1] = GETINI('JobStatusColours','Maroon',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[2] = COLOR:Green
  JSC:Action[2] = GETINI('JobStatusColours','GreenAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[2] = GETINI('JobStatusColours','Green',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[3] = COLOR:Olive
  JSC:Action[3] = GETINI('JobStatusColours','OliveAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[3] = GETINI('JobStatusColours','Olive',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[4] = COLOR:Navy
  JSC:Action[4] = GETINI('JobStatusColours','NavyAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[4] = GETINI('JobStatusColours','Navy',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[5] = COLOR:Purple
  JSC:Action[5] = GETINI('JobStatusColours','PurpleAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[5] = GETINI('JobStatusColours','Purple',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[6] = COLOR:Teal
  JSC:Action[6] = GETINI('JobStatusColours','TealAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[6] = GETINI('JobStatusColours','Teal',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[7] = COLOR:Gray
  JSC:Action[7] = GETINI('JobStatusColours','GrayAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[7] = GETINI('JobStatusColours','Gray',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[8] = COLOR:Blue
  JSC:Action[8] = GETINI('JobStatusColours','BlueAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[8] = GETINI('JobStatusColours','Blue',,CLIP(PATH())&'\SB2KDEF.INI')
  
  HIDE(?StatusWarning)
  LOOP ix# = 1 TO 8
      IF ((JSC:Status[ix#] <> '') AND (job:current_status = JSC:Status[ix#])) THEN
          ?StatusWarning{PROP:Text} = job:current_status[5 : LEN(CLIP(job:current_status))]
          UNHIDE(?StatusWarning)
          !IF (JSC:Action[ix#]) THEN
          !    HIDE(?OK)
          !END
          BREAK
      END
  END
  ! End Change 2618 BE(05/08/03)
  If job:Workshop <> 'YES'
      ?Lookup_Engineer{prop:Disable} = 1
      ?AllocateJob{prop:Disable} = 1
  Else !job:Workshop <> 'YES'
      ?Lookup_Engineer{prop:Disable} = 0
      ?AllocateJob{prop:Disable} = 0
  End !job:Workshop <> 'YES'
  Access:ContHist.ClearKey(cht:Ref_Number_Key)
  cht:Ref_Number = job:Ref_Number
  SET(cht:Ref_Number_Key,cht:Ref_Number_Key)
  LOOP
    IF Access:ContHist.Next()
      BREAK
    END
    IF cht:Ref_Number <> job:Ref_Number
      BREAK
    END
    ?ContactHistory{PROP:FONTSTYLE} = FONT:Bold
    BREAK
  END
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      ?job:Location{prop:Hide} = 1
      ?job:Location:Prompt{prop:Hide} = 1
      ?LookupLocation{prop:Hide} = 1
  End !def:HideLocation
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  If de2:AllocateEngPassword
      ?Lookup_Engineer{prop:Hide} = 1
  End !de2:AllocateEngPassword
  
  ! Start Change 3804 BE(16/01/04)
  TempStatusCode = job:Current_Status
  ! End Change 3804 BE(16/01/04)
  Do CountParts
  !Complete at QA?
  Access:MANUFACT.ClearKey(man:Manufacturer_Key)
  man:Manufacturer = job:Manufacturer
  If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      !Found
      IF man:QAAtCompletion
          ?CompleteButton{prop:Text} = 'Complete Repair [F10]'
      End !IF man:QAAtCompletion
  Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!Complete AT QA?!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
  !Show Hide Fields
  If GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?tmp:Network{prop:Hide} = 1
      ?tmp:Network:Prompt{prop:Hide} = 1
      ?LookupNetwork{prop:Hide} = 1
  Else !GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?tmp:Network{prop:Hide} = 0
      ?tmp:Network:Prompt{prop:Hide} = 0
      ?LookupNetwork{prop:Hide} = 0
  End !GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  IF INSTRING('SIGMA',def:User_Name)
    ?tmp:Network:Prompt{PROP:Text} = 'Factory Code'
  END
    !Record in Use?
    If ThisWindow.Request <> Insertrecord
        Pointer# = Pointer(JOBS)
        Hold(JOBS,1)
        Get(JOBS,Pointer#)
        If Errorcode() = 43
            Case MessageEx('This job is currently in use by another station.<13><10>The screen will be VIEW ONLY','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
            End!Case MessageEx
            ThisWindow.Request = ViewRecord
            ?OK{prop:Hide} = 1
        End !If Errorcode() = 43
    End !ThisWindow.Request <> Insertrecord
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ! support for CPCS
  IF ?job:Location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?job:Location{Prop:Tip}
  END
  IF ?job:Location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?job:Location{Prop:Msg}
  END
  IF ?tmp:Network{Prop:Tip} AND ~?LookupNetwork{Prop:Tip}
     ?LookupNetwork{Prop:Tip} = 'Select ' & ?tmp:Network{Prop:Tip}
  END
  IF ?tmp:Network{Prop:Msg} AND ~?LookupNetwork{Prop:Msg}
     ?LookupNetwork{Prop:Msg} = 'Select ' & ?tmp:Network{Prop:Msg}
  END
  IF SELF.Request = ViewRecord
    ?job:Date_Completed{PROP:ReadOnly} = True
    ?job:Time_Completed{PROP:ReadOnly} = True
    DISABLE(?job:Warranty_Charge_Type)
    ?job:Date_QA_Passed{PROP:ReadOnly} = True
    ?job:Time_QA_Passed{PROP:ReadOnly} = True
    DISABLE(?job:Charge_Type)
    DISABLE(?Common_Faults)
    DISABLE(?ChangeJobStatus)
    DISABLE(?RemoveParts)
    DISABLE(?MakeEstimateJob)
    ?tmp:Network{PROP:ReadOnly} = True
    DISABLE(?LookupNetwork)
    DISABLE(?AmendUnitType)
    DISABLE(?Lookup_Engineer)
    ?engineer_name_temp{PROP:ReadOnly} = True
    DISABLE(?EngineerHistory)
    DISABLE(?AllocateJob)
    DISABLE(?job:Repair_Type)
    DISABLE(?LookupChargeableRepairType)
    DISABLE(?job:Repair_Type_Warranty)
    DISABLE(?LookupWarrantyRepairType)
    DISABLE(?Lookup_Invoice_Text)
    DISABLE(?job:Location)
    DISABLE(?LookupLocation)
    DISABLE(?PickNote)
    DISABLE(?Fault_Codes)
    DISABLE(?ViewAccessories)
    DISABLE(?engineers_notes)
    DISABLE(?Multi_Insert)
    DISABLE(?Add_Chargeable_Adjustment)
    DISABLE(?Insert)
    DISABLE(?Change)
    DISABLE(?Delete)
    DISABLE(?Multi_Insert:2)
    DISABLE(?Add_Warranty_Adjustment)
    DISABLE(?Insert:3)
    DISABLE(?Change:3)
    DISABLE(?Delete:3)
    DISABLE(?Costs)
    DISABLE(?CompleteButton)
    HIDE(?OK)
    DISABLE(?ContactHistory)
    DISABLE(?Invoice)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW9.Q &= Queue:Browse
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,par:Part_Number_Key)
  BRW9.AddRange(par:Ref_Number,Relate:PARTS,Relate:JOBS)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,par:Part_Number,1,BRW9)
  BRW9.AddField(par:Part_Number,BRW9.Q.par:Part_Number)
  BRW9.AddField(par:Description,BRW9.Q.par:Description)
  BRW9.AddField(par:Quantity,BRW9.Q.par:Quantity)
  BRW9.AddField(par:Record_Number,BRW9.Q.par:Record_Number)
  BRW9.AddField(par:Ref_Number,BRW9.Q.par:Ref_Number)
  BRW10.Q &= Queue:Browse:1
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,wpr:Part_Number_Key)
  BRW10.AddRange(wpr:Ref_Number,Relate:WARPARTS,Relate:JOBS)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,wpr:Part_Number,1,BRW10)
  BRW10.AddField(wpr:Part_Number,BRW10.Q.wpr:Part_Number)
  BRW10.AddField(wpr:Description,BRW10.Q.wpr:Description)
  BRW10.AddField(wpr:Quantity,BRW10.Q.wpr:Quantity)
  BRW10.AddField(wpr:Record_Number,BRW10.Q.wpr:Record_Number)
  BRW10.AddField(wpr:Ref_Number,BRW10.Q.wpr:Ref_Number)
  IF ?job:Chargeable_Job{Prop:Checked} = True
    UNHIDE(?JOB:Charge_Type)
    UNHIDE(?JOB:Repair_Type)
    UNHIDE(?job:repair_type:prompt)
    UNHIDE(?LookupChargeableRepairType)
    UNHIDE(?ChargeableTab)
    ENABLE(?Chargeable_Group)
  END
  IF ?job:Chargeable_Job{Prop:Checked} = False
    job:Repair_Type = ''
    HIDE(?JOB:Charge_Type)
    HIDE(?JOB:Repair_Type)
    HIDE(?job:repair_type:prompt)
    HIDE(?LookupChargeableRepairType)
    HIDE(?ChargeableTab)
    DISABLE(?Chargeable_Group)
  END
  IF ?job:Warranty_Job{Prop:Checked} = True
    UNHIDE(?JOB:Warranty_Charge_Type)
    UNHIDE(?JOB:Repair_Type_Warranty)
    UNHIDE(?job:repair_type_warranty:prompt)
    UNHIDE(?LookupWarrantyRepairType)
    UNHIDE(?Warrantytab)
    ENABLE(?Warranty_Group)
  END
  IF ?job:Warranty_Job{Prop:Checked} = False
    job:Repair_Type_Warranty = ''
    HIDE(?JOB:Warranty_Charge_Type)
    HIDE(?JOB:Repair_Type_Warranty)
    HIDE(?job:repair_type_warranty:prompt)
    HIDE(?LookupWarrantyRepairType)
    HIDE(?Warrantytab)
    DISABLE(?Warranty_Group)
  END
  FDCB6.Init(job:Charge_Type,?job:Charge_Type,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(cha:Warranty_Ref_Number_Key)
  FDCB6.AddRange(cha:Warranty,tmp:no)
  FDCB6.AddField(cha:Charge_Type,FDCB6.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB7.Init(job:Warranty_Charge_Type,?job:Warranty_Charge_Type,Queue:FileDropCombo:1.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:1,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:1
  FDCB7.AddSortOrder(cha:Warranty_Ref_Number_Key)
  FDCB7.AddRange(cha:Warranty,tmp:yes)
  FDCB7.AddField(cha:Charge_Type,FDCB7.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  BRW9.AskProcedure = 7
  BRW10.AskProcedure = 8
  BRW9.AddToolbarTarget(Toolbar)
  BRW10.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  Release(JOBS)
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:COMMONCP.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:EXCHANGE.Close
    Relate:JOBEXACC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:NETWORKS.Close
    Relate:PARTS_ALIAS.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
    Relate:REPTYDEF.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WARPARTS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Jobs_Rapid',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
      continue# = 1
      If job:model_number = ''
          Case MessageEx('You must select a Model Number before you can insert any parts.','ServiceBase 2000',|
                         'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
          End!Case MessageEx
          continue# = 0
      Else !If job:model_number = ''
          If job:charge_type = '' And (Field() = ?Insert Or Field() = ?Change Or Field() = ?Delete)
              Case MessageEx('You must select a Charge Type before you can insert any parts.','ServiceBase 2000',|
                             'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              End!Case MessageEx
              continue# = 0
          End
          If job:warranty_charge_type = '' And continue# = 1 And (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
              Case MessageEx('You must select a Warranty Charge Type before you can insert any parts.','ServiceBase 2000',|
                             'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              End!Case MessageEx
              continue# = 0
          End
  
      End !If job:model_number = ''
  
      If Field() = ?Insert Or Field() = ?Insert:3 Or Field() = ?Delete or Field() = ?Delete:3
          If job:date_completed <> ''
              Check_Access('AMEND COMPLETED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot Insert/Delete parts!<13,10><13,10>This job has been completed.','ServiceBase 2000',|
                                 'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  End!Case MessageEx
                  continue# = 0
              End
          End
          If job:invoice_Number <> ''
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  If request = InsertRecord Or request = DeleteRecord
                      continue# = 0
                      If Field() = ?Insert
                          Case MessageEx('Cannot Insert/Delete parts!<13,10><13,10>The Chargeable part of this job has been invoiced.','ServiceBase 2000',|
                                         'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          End!Case MessageEx
                      End!If number = 1
                      If Field() = ?Insert:3
                          Case MessageEx('Cannot Insert/Delete parts!<13,10><13,10>The Warranty part of this job has been invoiced.','ServiceBase 2000',|
                                         'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          End!Case MessageEx
                      End!If number = 1
  
                  End!If request = InsertRecord Or request = DeleteRecord
  
              End
          End
  
      End !If Field() = ?Insert Or Field() = ?Insert:3 Or Field() = ?Delete or Field() = ?Delete:3
  
  
      If continue# = 1
  
          If Field() = ?Insert
              glo:select11 = job:manufacturer
              glo:select12 = job:model_number
          End !If Field() = ?Insert
          If Field() = ?Delete
              !Stock Order
              If DeleteChargeablePart()
                  !Begin deletion of pick detail entry TH
                  par:Quantity = 0 !reset since it is about to be deleted - can utilise
                  if def:PickNoteNormal or def:PickNoteMainStore then
                      ChangePickingPart('chargeable')
                      DeletePickingParts('chargeable',0)
                  end
                  ! Start Change 2116 BE(26/06/03)
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = par:Part_Ref_Number
                  IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign) THEN
                       access:JOBSE.clearkey(jobe:RefNumberKey)
                       jobe:RefNumber  = job:Ref_Number
                       IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                           IF (access:JOBSE.primerecord() = Level:Benign) THEN
                               jobe:RefNumber  = job:Ref_Number
                               IF (access:JOBSE.tryinsert()) THEN
                                   access:JOBSE.cancelautoinc()
                               END
                           END
                       END
                       IF ((sto:RF_BOARD) AND (jobe:Pre_RF_Board_IMEI <> '')) THEN
                          GET(audit,0)
                          IF (access:audit.primerecord() = level:benign) THEN
                              aud:notes         = 'RF Board deleted from job.<13,10>New IMEI ' & Clip(jobe:Pre_RF_Board_IMEI) & |
                                                  '<13,10>Old IMEI ' & Clip(job:esn)
                              aud:ref_number    = job:ref_number
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(jobe:Pre_RF_Board_IMEI)
                              if access:audit.insert()
                                  access:audit.cancelautoinc()
                              end
                          END
                          job:ESN = jobe:Pre_RF_Board_IMEI
                          jobe:Pre_RF_Board_IMEI = ''
                          access:jobse.update()
                          access:jobs.update()
                          DISPLAY(?job:ESN)
                      END
                  END
                  ! End Change 2116 BE(26/06/03)
                  Delete(PARTS)
              End !If DeleteChargeablePart()
          End!If Field() = ?Delete
  
          If Field() = ?Insert:3
              glo:select11 = job:manufacturer
              glo:select12 = job:model_number
          End !If Field() = ?Insert:3
          If Field() = ?Delete:3
              If DeleteWarrantyPart()
                  !Begin deletion of pick detail entry TH
                  wpr:Quantity = 0 !reset since it is about to be deleted - can utilise
                  if def:PickNoteNormal or def:PickNoteMainStore then
                      ChangePickingPart('warranty')
                      DeletePickingParts('warranty',0)
                  end
                  ! Start Change 2116 BE(26/06/03)
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = wpr:Part_Ref_Number
                  IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign) THEN
                       access:JOBSE.clearkey(jobe:RefNumberKey)
                       jobe:RefNumber  = job:Ref_Number
                       IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                           IF (access:JOBSE.primerecord() = Level:Benign) THEN
                               jobe:RefNumber  = job:Ref_Number
                               IF (access:JOBSE.tryinsert()) THEN
                                   access:JOBSE.cancelautoinc()
                               END
                           END
                       END
                       IF ((sto:RF_BOARD) AND (jobe:Pre_RF_Board_IMEI <> '')) THEN
                          GET(audit,0)
                          IF (access:audit.primerecord() = level:benign) THEN
                              aud:notes         = 'RF Board deleted from job.<13,10>New IMEI ' & Clip(jobe:Pre_RF_Board_IMEI) & |
                                                  '<13,10>Old IMEI ' & Clip(job:esn)
                              aud:ref_number    = job:ref_number
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(jobe:Pre_RF_Board_IMEI)
                              if access:audit.insert()
                                  access:audit.cancelautoinc()
                              end
                          END
                          job:ESN = jobe:Pre_RF_Board_IMEI
                          jobe:Pre_RF_Board_IMEI = ''
                          access:jobse.update()
                          access:jobs.update()
                          DISPLAY(?job:ESN)
                      END
                  END
                  ! End Change 2116 BE(26/06/03)
                  Delete(WARPARTS)
              End !If DeleteWarrantyPart()
          End !If Field() = Delete:3
  
          If request <> 3
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Warranty_Charge_Types
      Browse_Warranty_Charge_Types
      PickNetworks
      BrowseREPAIRTY
      BrowseREPAIRTY_Warranty
      Browse_Available_Locations
      Update_Parts
      Update_Warranty_Part
    END
    ReturnValue = GlobalResponse
  END
      End !If request <> 3
  
      If (Field() = ?Insert Or Field() = ?Change Or Field() = ?Delete)
          !**** PICKING CODE TH 02/04
          if def:PickNoteNormal or def:PickNoteMainStore then
              if request = insertrecord and returnvalue = requestcompleted then
                  CreatePickingNote (sto:Location)
                  InsertPickingPart ('chargeable')
              end
              if request = changerecord and returnvalue = requestcompleted then
                  ChangePickingPart ('chargeable')
              end
          end
          !****
  
          If glo:select1 = 'NEW PENDING'
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = par:part_ref_number
              If access:stock.fetch(sto:ref_number_key)
                  beep(beep:systemhand)  ;  yield()
                  message('An Error has occured retrieving the details of this Stock Part.', |
                          'ServiceBase 2000', icon:hand)
              Else!If access:stock.fetch(sto:ref_number_key)
                  If Access:STOHIST.Primerecord() = Level:Benign
                      shi:Ref_Number              = sto:Ref_Number
                      shi:Transaction_Type        = 'DEC'
                      shi:Despatch_Note_Number    = par:Despatch_Note_Number
                      shi:Quantity                = sto:Quantity_Stock
                      shi:Date                    = Today()
                      shi:Purchase_Cost           = par:Purchase_Cost
                      shi:Sale_Cost               = par:Sale_Cost
                      shi:Job_Number              = job:Ref_Number
                      Access:USERS.Clearkey(use:Password_Key)
                      use:Password                = glo:Password
                      Access:USERS.Tryfetch(use:Password_Key)
                      shi:User                    = use:User_Code
                      shi:Notes                   = 'STOCK DECREMENTED'
                      shi:Information             = ''
                      If Access:STOHIST.Tryinsert()
                          Access:STOHIST.Cancelautoinc()
                      End!If Access:STOHIST.Tryinsert()
                  End!If Access:STOHIST.Primerecord() = Level:Benign
  
                  sto:quantity_stock     = 0
                  access:stock.update()
  
                  access:parts_alias.clearkey(par_ali:RefPartRefNoKey)
                  par_ali:ref_number      = job:ref_number
                  par_ali:part_ref_number = glo:select2
                  If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
                      par:ref_number           = par_ali:ref_number
                      par:adjustment           = par_ali:adjustment
                      par:part_ref_number      = par_ali:part_ref_number
  
                      par:part_number     = par_ali:part_number
                      par:description     = par_ali:description
                      par:supplier        = par_ali:supplier
                      par:purchase_cost   = par_ali:purchase_cost
                      par:sale_cost       = par_ali:sale_cost
                      par:retail_cost     = par_ali:retail_cost
                      par:quantity             = glo:select3
                      par:warranty_part        = 'NO'
                      par:exclude_from_order   = 'NO'
                      par:despatch_note_number = ''
                      par:date_ordered         = ''
                      par:date_received        = ''
                      par:pending_ref_number   = glo:select4
                      par:order_number         = ''
                      par:fault_code1    = par_ali:fault_code1
                      par:fault_code2    = par_ali:fault_code2
                      par:fault_code3    = par_ali:fault_code3
                      par:fault_code4    = par_ali:fault_code4
                      par:fault_code5    = par_ali:fault_code5
                      par:fault_code6    = par_ali:fault_code6
                      par:fault_code7    = par_ali:fault_code7
                      par:fault_code8    = par_ali:fault_code8
                      par:fault_code9    = par_ali:fault_code9
                      par:fault_code10   = par_ali:fault_code10
                      par:fault_code11   = par_ali:fault_code11
                      par:fault_code12   = par_ali:fault_code12
                      access:parts.insert()
                  end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign
              end !if access:stock.fetch(sto:ref_number_key = Level:Benign
              if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('chargeable').
          End!If glo:select1 = 'NEW PENDING'
      End
      If (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
          !**** PICKING CODE TH 02/04
          if def:PickNoteNormal or def:PickNoteMainStore then
              if request = insertrecord and returnvalue = requestcompleted then
                  CreatePickingNote (sto:Location)
                  InsertPickingPart ('warranty')
              end
              if request = changerecord and returnvalue = requestcompleted then
                  if def:PickNoteNormal or def:PickNoteMainStore then ChangePickingPart ('warranty').
              end
          end
          !****
  
          If glo:select1 = 'NEW PENDING'
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = wpr:part_ref_number
              If access:stock.fetch(sto:ref_number_key)
                  beep(beep:systemhand)  ;  yield()
                  message('An Error has occured retrieving the details of this Stock Part.', |
                          'ServiceBase 2000', icon:hand)
              Else!If access:stock.fetch(sto:ref_number_key)
                  If Access:STOHIST.Primerecord() = Level:Benign
                      shi:Ref_Number              = sto:Ref_Number
                      shi:Transaction_Type        = 'DEC'
                      shi:Despatch_Note_Number    = wpr:Despatch_Note_Number
                      shi:Quantity                = sto:Quantity_Stock
                      shi:Date                    = Today()
                      shi:Purchase_Cost           = wpr:Purchase_Cost
                      shi:Sale_Cost               = wpr:Sale_Cost
                      shi:Job_Number              = job:Ref_Number
                      Access:USERS.Clearkey(use:Password_Key)
                      use:Password                = glo:Password
                      Access:USERS.Tryfetch(use:Password_Key)
                      shi:User                    = use:User_Code
                      shi:Notes                   = 'STOCK DECREMENTED'
                      shi:Information             = ''
                      If Access:STOHIST.Tryinsert()
                          Access:STOHIST.Cancelautoinc()
                      End!If Access:STOHIST.Tryinsert()
                  End!If Access:STOHIST.Primerecord() = Level:Benign
                  sto:quantity_stock     = 0
                  access:stock.update()
  
                  access:warparts_alias.clearkey(war_ali:RefPartRefNoKey)
                  war_ali:ref_number      = job:ref_number
                  war_ali:part_ref_number = glo:select2
                  If access:warparts_alias.fetch(war_ali:RefPartRefNoKey) = Level:Benign
                      wpr:ref_number           = war_ali:ref_number
                      wpr:adjustment           = war_ali:adjustment
                      wpr:part_ref_number      = war_ali:part_ref_number
                      wpr:part_number     = war_ali:part_number
                      wpr:description     = war_ali:description
                      wpr:supplier        = war_ali:supplier
                      wpr:purchase_cost   = war_ali:purchase_cost
                      wpr:sale_cost       = war_ali:sale_cost
                      wpr:retail_cost     = war_ali:retail_cost
                      wpr:quantity             = glo:select3
                      wpr:warranty_part        = 'YES'
                      wpr:exclude_from_order   = 'NO'
                      wpr:despatch_note_number = ''
                      wpr:date_ordered         = ''
                      wpr:date_received        = ''
                      wpr:pending_ref_number   = glo:select4
                      wpr:order_number         = ''
                      wpr:fault_code1    = war_ali:fault_code1
                      wpr:fault_code2    = war_ali:fault_code2
                      wpr:fault_code3    = war_ali:fault_code3
                      wpr:fault_code4    = war_ali:fault_code4
                      wpr:fault_code5    = war_ali:fault_code5
                      wpr:fault_code6    = war_ali:fault_code6
                      wpr:fault_code7    = war_ali:fault_code7
                      wpr:fault_code8    = war_ali:fault_code8
                      wpr:fault_code9    = war_ali:fault_code9
                      wpr:fault_code10   = war_ali:fault_code10
                      wpr:fault_code11   = war_ali:fault_code11
                      wpr:fault_code12   = war_ali:fault_code12
                      access:warparts.insert()
                  end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign
              end !if access:stock.fetch(sto:ref_number_key = Level:Benign
              if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('warranty').
          End!If glo:select1 = 'NEW PENDING'
      End!If (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
  
      Else!!If continue# = 1
          If (Field() = ?Insert Or Field() = ?Change Or Field() = ?Delete)
              access:parts.cancelautoinc()
          End!If (Field() = ?Insert Or Field() = ?Change Or Field() = ?Delete)
          If (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
              access:warparts.cancelautoinc()
          End!If (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
              
  
      End !If continue# = 1
  
      Clear(glo:G_Select1)
      Clear(glo:G_Select1)
      Do check_parts
      Do Pricing
  
  Do CountParts
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?job:QA_Passed
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:QA_Passed, Accepted)
      If ~0{prop:acceptall}
          set(defaults)
          access:defaults.next()
          If job:qa_passed <> qa_passed_temp
              If job:qa_passed = 'NO'
                  check_access('RELEASE - QA PASSED',x")
                  If x" = False
                      Case MessageEx('You do not have access to this option','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      job:qa_passed   = qa_passed_temp
                  Else !If x" = False
                      qa_passed_temp  = job:qa_passed
                  End !If x" = False
              Else !If job:in_repair = 'NO'
                  If def:qa_required = 'YES'
                      Do Check_For_Despatch
                  End!If def:qa_required = 'YES'
                  pass# = 0
                  Include('accjchk.inc')
                  If pass# = 1
                      qa_passed_temp  = job:qa_passed
                      job:date_qa_passed = Today()
                      job:time_qa_passed = Clock()
                      GetStatus(610,0,'JOB') !manual qa passed
                      ! Start Change 3940 BE(02/03/04)
                      TempStatusCode = job:Current_Status
                      ! End Change 3940 BE(02/03/04)
      
                  End!If pass# = 1
      
              End !If job:in_repair = 'NO'
          End !If job:in_repair <> in_repair_temp
          Display()
      End!If ~0{prop:acceptall}
      
          
      
      
      
      
                  
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:QA_Passed, Accepted)
    OF ?job:Chargeable_Job
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Chargeable_Job, Accepted)
      Include('genjobs.inc','ChargeableJobAccepted')
      DO enable_Disable
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Chargeable_Job, Accepted)
    OF ?job:Warranty_Job
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Warranty_Job, Accepted)
      Include('GenJobs.inc','WarrantyJobAccepted')
      
      
      
      DO enable_Disable
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Warranty_Job, Accepted)
    OF ?Lookup_Invoice_Text
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Invoice_Text, Accepted)
      glo:Select1 = job:Manufacturer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Invoice_Text, Accepted)
    OF ?ContactHistory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ContactHistory, Accepted)
      glo:select12    = job:ref_number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ContactHistory, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?job:Warranty_Charge_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Warranty_Charge_Type, Accepted)
      If ~0{prop:acceptall}
          error# = 0
          If tmp:warrantychargetype   <> job:warranty_Charge_Type
              If tmp:warrantychargetype <> ''
                  Case MessageEx('Are you sure you want to change the Warranty Charge Type?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          error# = 1
                  End!Case MessageEx
              End!If tmp:chargetype <> ''
              If error# = 0
                  If CheckPricing('W')
                      error# = 1
                  End!If CheckPricing('C')
              End!If error# = 0
      
              If error# = 0
                  tmp:warrantychargetype  = job:Warranty_Charge_Type
              Else!If error# = 0
                  job:Warranty_Charge_Type = tmp:WarrantyChargetype
              End!If error# = 0
      
          End!If tmp:chargetype   <> job:Charge_Type
          Display()
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Warranty_Charge_Type, Accepted)
    OF ?job:Chargeable_Job
      IF ?job:Chargeable_Job{Prop:Checked} = True
        UNHIDE(?JOB:Charge_Type)
        UNHIDE(?JOB:Repair_Type)
        UNHIDE(?job:repair_type:prompt)
        UNHIDE(?LookupChargeableRepairType)
        UNHIDE(?ChargeableTab)
        ENABLE(?Chargeable_Group)
      END
      IF ?job:Chargeable_Job{Prop:Checked} = False
        job:Repair_Type = ''
        HIDE(?JOB:Charge_Type)
        HIDE(?JOB:Repair_Type)
        HIDE(?job:repair_type:prompt)
        HIDE(?LookupChargeableRepairType)
        HIDE(?ChargeableTab)
        DISABLE(?Chargeable_Group)
      END
      ThisWindow.Reset
    OF ?job:Charge_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Charge_Type, Accepted)
      If ~0{prop:acceptall}
          error# = 0
          If tmp:chargetype   <> job:Charge_Type
              If tmp:chargetype <> ''
                  Case MessageEx('Are you sure you want to change the Charge Type?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          error# = 1
                  End!Case MessageEx
              End!If tmp:chargetype <> ''
              If error# = 0
                  If CheckPricing('C')
                      error# = 1
                  End!If CheckPricing('C')
              End!If error# = 0
      
              If error# = 0
                  tmp:chargetype  = job:Charge_Type
              Else!If error# = 0
                  job:Charge_Type = tmp:Chargetype
              End!If error# = 0
      
          End!If tmp:chargetype   <> job:Charge_Type
          Display()
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Charge_Type, Accepted)
    OF ?job:Warranty_Job
      IF ?job:Warranty_Job{Prop:Checked} = True
        UNHIDE(?JOB:Warranty_Charge_Type)
        UNHIDE(?JOB:Repair_Type_Warranty)
        UNHIDE(?job:repair_type_warranty:prompt)
        UNHIDE(?LookupWarrantyRepairType)
        UNHIDE(?Warrantytab)
        ENABLE(?Warranty_Group)
      END
      IF ?job:Warranty_Job{Prop:Checked} = False
        job:Repair_Type_Warranty = ''
        HIDE(?JOB:Warranty_Charge_Type)
        HIDE(?JOB:Repair_Type_Warranty)
        HIDE(?job:repair_type_warranty:prompt)
        HIDE(?LookupWarrantyRepairType)
        HIDE(?Warrantytab)
        DISABLE(?Warranty_Group)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Warranty_Job, Accepted)
      If job:warranty_job <> 'YES'
          job:edi = 'XXX'
      End!If job:warranty_job = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Warranty_Job, Accepted)
    OF ?Common_Faults
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Common_Faults, Accepted)
      !Include('commonfa.inc')
      If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          Case MessageEx('Using Common Faults may ADD the following:<13,10>Parts, Invoice Text And Engineer''s Note<13,10><13,10>and OVERWRITE the following:<13,10>Job Types, Repair Types and Charge Types.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                         'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  saverequest#    = globalrequest
                  globalrequest   = requestcancelled
                  globalrequest   = selectrecord
                  glo:select1     = job:model_number
                  Browse_Common_Faults
                  glo:select1     = ''
                  If globalresponse = requestcompleted
                      CommonFaults(com:ref_number)
                      If tmp:invoicetext = ''
                        tmp:invoicetext = com:invoice_Text
                      Else!If tmp:engineersnotes = ''
                        tmp:invoicetext  = Clip(tmp:invoicetext) & '<13,10>' & com:invoice_Text
                      End!If tmp:engineersnotes = ''
                      If job:Chargeable_Job = 'YES'
                          Chargeable_Job_Temp = 'YES'
                      End !If job:Chargeable_Job = 'YES'
                      If job:Warranty_Job = 'YES'
                          Warranty_Job_Temp = 'YES'
                      End !If job:Warranty_Job = 'YES'
                      Post(Event:Accepted,?job:Chargeable_Job)
                      Post(Event:Accepted,?job:Warranty_Job)
                  End!If globalresponse = reuestcompleted
      
              Of 2 ! &No Button
          End!Case MessageEx
      End!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
      
      !Refresh Screen!
      ThisWindow.Reset(1)
      Do CountParts
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Common_Faults, Accepted)
    OF ?ChangeJobStatus
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Status
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChangeJobStatus, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              ! Start Change 3804 BE(16/01/04)
              !GetStatus(Sub(sts:Status,1,3),1,'JOB')
              TempStatusCode = sts:Status
              ! End Change 3804 BE(16/01/04)
          Of Requestcancelled
      End!Case Globalreponse
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChangeJobStatus, Accepted)
    OF ?RemoveParts
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RemoveParts, Accepted)
      RemovePartsAndInvoiceText()
      Do CountParts
      Display()
      BRW10.ResetSort(1)
      BRW9.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RemoveParts, Accepted)
    OF ?MakeEstimateJob
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MakeEstimateJob, Accepted)
      ! Start Change 1744 BE(22/04/03)
      Make_Estimate_Job
      IF (glo:Select17 = 'OK') THEN
          chargeable_job_temp = 'YES'
          job:Chargeable_Job = 'YES'
          warranty_job_temp = 'NO'
          job:Warranty_Job = 'NO'
          tmp:warrantychargetype = ''
          job:Warranty_Charge_Type = ''
          tmp:ChargeType = glo:Select18
          job:Charge_Type = glo:Select18
          tmp:RepairType = glo:Select19
          job:Repair_Type = glo:Select19
          job:Estimate_Ready = 'YES'
          ! Start Change 2823 BE(25/06/03)
          job:Estimate = 'YES'
          ! End Change 2823 BE(25/06/03)
          GetStatus(510,0,'JOB')
          ! Start Change 3940 BE(02/03/04)
          TempStatusCode = job:Current_Status
          ! End Change 3940 BE(02/03/04)
      
          DO CountParts
          BRW10.ResetSort(1)
          BRW9.ResetSort(1)
          MakeEstimateJobFlag = 1
          Self.CancelAction = Cancel:Save
          POST(Event:Accepted, ?Cancel)
      END
      ! End Change 1744 BE(22/04/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MakeEstimateJob, Accepted)
    OF ?tmp:Network
      IF tmp:Network OR ?tmp:Network{Prop:Req}
        net:Network = tmp:Network
        !Save Lookup Field Incase Of error
        look:tmp:Network        = tmp:Network
        IF Access:NETWORKS.TryFetch(net:NetworkKey)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:Network = net:Network
          ELSE
            !Restore Lookup On Error
            tmp:Network = look:tmp:Network
            SELECT(?tmp:Network)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupNetwork
      ThisWindow.Update
      net:Network = tmp:Network
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:Network = net:Network
          Select(?+1)
      ELSE
          Select(?tmp:Network)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Network)
    OF ?AmendUnitType
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseUNITTYPE
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AmendUnitType, Accepted)
      case globalresponse
          of requestcompleted
              error# = 0
              If job:unit_type <> tmp:unittype
                  Case MessageEx('Are you sure you want to change the Unit Type?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          error# = 1
                  End!Case MessageEx
              End!If job:unit_type <> tmp:unittype
              If error# = 0
                  If CheckPricing('C') Or CheckPricing('W')
                      error# = 1
                  End!If CheckPricing('C') Or CheckPricing('W')
              End!If error# = 0
      
              If error# = 0
                  tmp:unittype    = job:unit_type
                  job:unit_type = uni:unit_type
                  model_details_temp = Clip(job:model_number) & ' ' & Clip(job:manufacturer) & ' - ' & Clip(job:unit_type)
              Else!If error# = 0
                  job:unit_type   = tmp:unittype
              End!If error# = 0
      
          of requestcancelled
      end!case globalreponse
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AmendUnitType, Accepted)
    OF ?Lookup_Engineer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Engineer, Accepted)
      SET(DEFAULT2)
      Access:DEFAULT2.Next()
      
      Globalrequest = SelectRecord
      
      If de2:UserSkillLevel
          PickEngineersSkillLevel(jobe:SkillLevel)
      Else !If de2:UserSkillLevel
          Browse_Users_Job_Assignment
      End !If de2:UserSkillLevel
      Case GlobalResponse
          Of Requestcompleted
              job:Engineer = use:User_Code  
      
          Of Requestcancelled
      
      End!Case Globalreponse
      
      Do AddEngineer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Engineer, Accepted)
    OF ?EngineerHistory
      ThisWindow.Update
      BrowseEngineersOnJob(Job:Ref_Number)
      ThisWindow.Reset
    OF ?AllocateJob
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllocateJob, Accepted)
      Engineer"   = ''
      Engineer"   = InsertEngineerPassword()
      IF Engineer" <> ''
          job:Engineer = Engineer"
      
          Do AddEngineer
      
      End !Engineer" <>
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllocateJob, Accepted)
    OF ?job:Repair_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Repair_Type, Accepted)
      IF job:Repair_Type OR ?job:Repair_Type{Prop:Req}
        rep:Repair_Type = job:Repair_Type
        rep:Model_Number = job:model_number
        rep:Chargeable = job:chargeable_job
        GLO:Select1 = job:model_number
        !Save Lookup Field Incase Of error
        look:job:Repair_Type        = job:Repair_Type
        IF Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            job:Repair_Type = rep:Repair_Type
          ELSE
            CLEAR(rep:Model_Number)
            CLEAR(rep:Chargeable)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            job:Repair_Type = look:job:Repair_Type
            SELECT(?job:Repair_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
          PricingError#  = 0
          If job:Repair_Type <> tmp:RepairType
              If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
                  Case MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type, Unit Type and Repair Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  PricingError# = 1
              End !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
          End !If job:Repair_Type <> tmp:RepairType
          !Before Lookup
          If PricingError# = 0
              do_lookup# = 1
      
              If job:date_completed <> ''
                  If SecurityCheck('AMEND COMPLETED JOBS')
                      Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      do_lookup# = 0
                  End
              End
              If job:invoice_Number <> '' and do_lookup# = 1
                  If SecurityCheck('AMEND INVOICED JOBS')
                      Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      do_lookup# = 0
                  End
              End
      
              If job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''    
                  Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  do_lookup# = 0
              End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''    
      
              If do_lookup# = 1
      
                  IF job:Repair_Type
                      Access:REPAIRTY.ClearKey(rep:Model_Chargeable_Key)
                      rep:Model_Number = job:Model_Number
                      rep:Chargeable   = job:Chargeable_Job
                      rep:Repair_Type  = job:Repair_Type
                      If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                          !Found
                      Else!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          Post(Event:Accepted,?LookupChargeableRepairType)
      
                      End!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                  End!IF job:Repair_Type OR ?job:Repair_Type{Prop:Req}
      
                  If job:repair_type <> tmp:RepairType
                      error# = 0
                      Case CheckPricing('C')
                          Of 1
                              error# = 1
                          Else
                              If tmp:RepairType <> ''
                                  Case MessageEx('Are you sure you want to change the repair type?','ServiceBase 2000',|
                                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                      Of 1 ! &Yes Button
                                      Of 2 ! &No Button
                                          error# = 1
                                  End!Case MessageEx
                              End!If tmp:RepairType <> ''
                      End!Case CheckPricing('C')
      
                      If error# = 1
                          job:repair_type = tmp:RepairType
                      Else!If job:date_completed <> ''
                          tmp:RepairType = job:repair_type
                          Do pricing
                          Post(Event:accepted,?job:charge_type)
                      End!If job:date_completed <> ''
                  End!If job:repair_type <> tmp:RepairType
                  Display()
              End !do_lookup# = 1
      
          End !If PricingError# = 0
      End !0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Repair_Type, Accepted)
    OF ?LookupChargeableRepairType
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupChargeableRepairType, Accepted)
      If job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''    
          Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
          If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
              Case MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type, Unit Type and Repair Type.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Else !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
              job:Repair_Type = PickRepairTypes(job:Model_Number,job:Account_Number,job:Charge_Type,job:Unit_Type,'C')
              If job:Repair_Type = ''
                  job:Repair_Type = tmp:RepairType
              End !job:Repair_Type = ''
              Post(Event:Accepted,?job:Repair_Type)
      
          End !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
      End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupChargeableRepairType, Accepted)
    OF ?job:Repair_Type_Warranty
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Repair_Type_Warranty, Accepted)
      If ~0{prop:acceptall}
          do_lookup# = 1
      
          If job:date_completed <> ''
              If SecurityCheck('AMEND COMPLETED JOBS')
                  Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  do_lookup# = 0
              End
          End
          If job:invoice_Number <> '' and do_lookup# = 1
              If SecurityCheck('AMEND INVOICED JOBS')
                  Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  do_lookup# = 0
              End
          End
      
          If job:Model_Number = '' Or job:Warranty_Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
              Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              do_lookup# = 0
          End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''    
      
          If do_lookup# = 1
      
              IF job:Repair_Type_Warranty
                  Access:REPAIRTY.ClearKey(rep:Model_Chargeable_Key)
                  rep:Model_Number = job:Model_Number
                  rep:Warranty     = job:Warranty_Job
                  rep:Repair_Type  = job:Repair_Type_Warranty
                  If Access:REPAIRTY.TryFetch(rep:Model_Warranty_Key) = Level:Benign
                      !Found
                      
                  Else!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      Post(Event:Accepted,?LookupWarrantyRepairType)
      
                  End!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
              End!IF job:Repair_Type OR ?job:Repair_Type{Prop:Req}
      
              If job:Repair_Type_Warranty <> tmp:WarrantyRepairType
                  error# = 0
                  Case CheckPricing('W')
                      Of 1
                          error# = 1
                      Else
                          If tmp:WarrantyRepairType <> ''
                              Case MessageEx('Are you sure you want to change the repair type?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                  Of 2 ! &No Button
                                      error# = 1
                              End!Case MessageEx
                          End!If repair_type_temp <> ''
                  End!Case CheckPricing('C')
      
                  If error# = 1
                      job:repair_type_Warranty = tmp:WarrantyRepairType
                  Else!If job:date_completed <> ''
                      tmp:WarrantyRepairType = job:repair_type_Warranty
                      Do pricing
                      Post(Event:accepted,?job:Warranty_Charge_Type)
                  End!If job:date_completed <> ''
              End!If job:repair_type <> repair_type_temp
              Display()
          End !do_lookup# = 1
      End !0{prop:acceptall}
      IF job:Repair_Type_Warranty OR ?job:Repair_Type_Warranty{Prop:Req}
        rep:Repair_Type = job:Repair_Type_Warranty
        rep:Model_Number = job:model_number
        rep:Warranty = job:warranty_job
        GLO:Select1 = job:model_number
        !Save Lookup Field Incase Of error
        look:job:Repair_Type_Warranty        = job:Repair_Type_Warranty
        IF Access:REPAIRTY.TryFetch(rep:Model_Warranty_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            job:Repair_Type_Warranty = rep:Repair_Type
          ELSE
            CLEAR(rep:Model_Number)
            CLEAR(rep:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            job:Repair_Type_Warranty = look:job:Repair_Type_Warranty
            SELECT(?job:Repair_Type_Warranty)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Repair_Type_Warranty, Accepted)
    OF ?LookupWarrantyRepairType
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupWarrantyRepairType, Accepted)
      If job:Model_Number = '' Or job:Warranty_Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
          Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          do_lookup# = 0
      Else !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
          job:Repair_Type_Warranty = PickRepairTypes(job:Model_Number,job:Account_Number,job:Warranty_Charge_Type,job:Unit_Type,'W')
          If job:Repair_Type_Warranty = ''
              job:Repair_Type = tmp:WarrantyRepairType
          End !job:Repair_Type = ''
          Post(Event:Accepted,?job:Repair_Type_Warranty)
      End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''    
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupWarrantyRepairType, Accepted)
    OF ?Lookup_Invoice_Text
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Invoice_Text
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Invoice_Text, Accepted)
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If jbn:Invoice_Text = ''
                  jbn:Invoice_Text = Clip(glo:notes_pointer)
              Else !If job:engineers_notes = ''
                  jbn:Invoice_Text = Clip(jbn:Invoice_Text) & '. ' & Clip(glo:notes_pointer)
              End !If job:engineers_notes = ''
          End
          Display()
      End
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
      glo:select1 = ''
      
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Invoice_Text, Accepted)
    OF ?job:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Location, Accepted)
      ! Amend Location Levels
      If ~0{prop:acceptall}
          If job:location <> location_temp
              error# = 0
          
              If error# = 0
                  If location_temp <> ''
              !Add To Old Location
                      access:locinter.clearkey(loi:location_key)
                      loi:location = location_temp
                      If access:locinter.fetch(loi:location_key) = Level:Benign
                          If loi:allocate_spaces = 'YES'
                              loi:current_spaces+= 1
                              access:locinter.update()
                          End
                      end !if
                  End
              !Take From New Location
                  access:locinter.clearkey(loi:location_key)
                  loi:location = job:location
                  If access:locinter.fetch(loi:location_key) = Level:Benign
                      If loi:allocate_spaces = 'YES'
                          loi:current_spaces -= 1
                          If loi:current_spaces< 1
                              loi:location_available = 'NO'
                          End
                          access:locinter.update()
                      End
                  end !if
                  location_temp  = job:location
          
              End!If job:date_completed <> ''
          
          End
      End!If ~0{prop:acceptall}
      IF job:Location OR ?job:Location{Prop:Req}
        loi:Location = job:Location
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:job:Location        = job:Location
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            job:Location = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            job:Location = look:job:Location
            SELECT(?job:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Location, Accepted)
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = job:Location
      
      IF SELF.RUN(6,Selectrecord)  = RequestCompleted
          job:Location = loi:Location
          Select(?+1)
      ELSE
          Select(?job:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Location)
    OF ?PickNote
      ThisWindow.Update
      PickValidate
      ThisWindow.Reset
    OF ?Fault_Codes
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Codes, Accepted)
    Access:JOBNOTES.Update()

    glo:FaultCode1          = job:Fault_Code1
    glo:FaultCode2          = job:Fault_Code2
    glo:FaultCode3          = job:Fault_Code3
    glo:FaultCode4          = job:Fault_Code4
    glo:FaultCode5          = job:Fault_Code5
    glo:FaultCode6          = job:Fault_code6
    glo:FaultCode7          = job:Fault_Code7
    glo:FaultCode8          = job:Fault_code8
    glo:FaultCode9          = job:Fault_Code9
    glo:FaultCode10         = job:Fault_Code10
    glo:FaultCode11         = job:Fault_Code11
    glo:FaultCode12         = job:Fault_Code12
    glo:FaultCode13         = jobe:FaultCode13
    glo:FaultCode14         = jobe:FaultCode14
    glo:FaultCode15         = jobe:FaultCode15
    glo:FaultCode16         = jobe:FaultCode16
    glo:FaultCode17         = jobe:FaultCode17
    glo:FaultCode18         = jobe:FaultCode18
    glo:FaultCode19         = jobe:FaultCode19
    glo:FaultCode20         = jobe:FaultCode20
    glo:TradeFaultCode1     = jobe:TraFaultCode1
    glo:TradeFaultCode2     = jobe:TraFaultCode2
    glo:TradeFaultCode3     = jobe:TraFaultCode3
    glo:TradeFaultCOde4     = jobe:TraFaultCode4
    glo:TradeFaultCode5     = jobe:TraFaultCOde5
    glo:TradeFaultCode6     = jobe:TraFaultCode6
    glo:TradeFaultCode7     = jobe:TraFaultCode7
    glo:TradeFaultCode8     = jobe:TraFaultCode8
    glo:TradeFaultCode9     = jobe:TraFaultCode9
    glo:TradeFaultCode10    = jobe:TraFaultCode10
    glo:TradeFaultCode11    = jobe:TraFaultCode11
    glo:TradeFaultCode12    = jobe:TraFaultCode12


    GenericFaultCodes(job:Ref_Number,ThisWindow.Request,job:account_Number,job:Manufacturer,|
                        job:Warranty_Charge_Type,job:Repair_Type_Warranty,job:Date_Completed)

    job:Fault_Code1         =  glo:FaultCode1
    job:Fault_Code2         =  glo:FaultCode2
    job:Fault_Code3         =  glo:FaultCode3
    job:Fault_Code4         =  glo:FaultCode4
    job:Fault_Code5         =  glo:FaultCode5
    job:Fault_code6         =  glo:FaultCode6
    job:Fault_Code7         =  glo:FaultCode7
    job:Fault_code8         =  glo:FaultCode8
    job:Fault_Code9         =  glo:FaultCode9
    job:Fault_Code10        =  glo:FaultCode10
    job:Fault_Code11        =  glo:FaultCode11
    job:Fault_Code12        =  glo:FaultCode12
    jobe:FaultCode13        =  glo:FaultCode13
    jobe:FaultCode14        =  glo:FaultCode14
    jobe:FaultCode15        =  glo:FaultCode15
    jobe:FaultCode16        =  glo:FaultCode16
    jobe:FaultCode17        =  glo:FaultCode17
    jobe:FaultCode18        =  glo:FaultCode18
    jobe:FaultCode19        =  glo:FaultCode19
    jobe:FaultCode20        =  glo:FaultCode20
    jobe:TraFaultCode1      =  glo:TradeFaultCode1
    jobe:TraFaultCode2      =  glo:TradeFaultCode2
    jobe:TraFaultCode3      =  glo:TradeFaultCode3
    jobe:TraFaultCode4      =  glo:TradeFaultCOde4
    jobe:TraFaultCOde5      =  glo:TradeFaultCode5
    jobe:TraFaultCode6      =  glo:TradeFaultCode6
    jobe:TraFaultCode7      =  glo:TradeFaultCode7
    jobe:TraFaultCode8      =  glo:TradeFaultCode8
    jobe:TraFaultCode9      =  glo:TradeFaultCode9
    jobe:TraFaultCode10     =  glo:TradeFaultCode10
    jobe:TraFaultCode11     =  glo:TradeFaultCode11
    jobe:TraFaultCode12     =  glo:TradeFaultCode12

    Access:JOBSE.Update()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Codes, Accepted)
    OF ?ViewAccessories
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewAccessories, Accepted)
          glo:select1  = job:model_number
          glo:select2  = job:ref_number
      
          Browse_job_accessories
      
          glo:select1  = ''
          glo:select2  = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewAccessories, Accepted)
    OF ?engineers_notes
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Engineers_Notes
      ThisWindow.Reset
    OF ?Multi_Insert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Multi_Insert, Accepted)
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      glo:select11 = job:manufacturer
      glo:select12 = job:model_number
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      pick_model_stock
      glo:select11 = ''
      glo:select12 = ''
      if globalresponse = requestcompleted
          If Records(glo:Queue)
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  access:stomodel.clearkey(stm:model_number_key)
                  stm:ref_number   = glo:pointer
                  stm:manufacturer = job:manufacturer
                  stm:model_number = job:model_number
                  if access:stomodel.fetch(stm:model_number_key) = Level:Benign
                      access:stock.clearkey(sto:ref_number_key)
                      sto:ref_number = stm:ref_number
                      if access:stock.fetch(sto:ref_number_key) = Level:Benign
      
                          ! Start Change 1895 BE(15/05/03)
                          DO InitMaxParts
                          IF (tmpCheckChargeableParts) THEN
                              DO TotalChargeableParts
                              TotalPartsCost += sto:Purchase_cost ! Qty = 1
                              IF (TotalPartsCost > tra:MaxChargeablePartsCost) THEN
                                  MESSAGE('Part No.' & CLIP(sto:part_number) & ' cannot be attached to the job.<13,10>' & |
                                      'This would exceed the Maximum Chargeable Parts Cost of the Trade Account.', |
                                      'ServiceBase 2000', Icon:Question, 'OK', 1, 0)
                                  CYCLE
                              END
                          END
                          ! End Change 1895 BE(15/05/03)
      
                          If sto:sundry_item = 'YES'
                              access:parts.clearkey(par:part_number_key)
                              par:ref_number  = job:ref_number
                              par:part_number = sto:part_number
                              if access:parts.fetch(par:part_number_key)
                                  Do Add_Chargeable_Part
                                  ! Start Change BE030 BE(12/05/2004)
                                  !if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('chargeable').
                                  if def:PickNoteNormal or def:PickNoteMainStore then
                                      CreatePickingNote(sto:location)
                                      InsertPickingPart ('chargeable')
                                  end
                                  ! Start Change BE030 BE(12/05/2004)
                              End!if access:parts.fetch(par:part_number_key)
                          Else!If sto:sundry_item = 'YES'
                              If sto:quantity_stock < 1
                                  If SecurityCheck('JOBS - ORDER PARTS')
                                      Case MessageEx(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                      '<13,10>'&|
                                      '<13,10>This part is out of stock.','ServiceBase 2000',|
                                                 'Styles\hand.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                                          Of 1 ! &OK Button
                                      End !
                                  ! Start Change 216 BE(26/06/03)
                                  ELSIF (sto:RF_BOARD) THEN
                                      Beep(Beep:SystemHand)  ;  Yield()
                                      Message(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                              '||There are insufficient items in stock!' & |
                                              '||This part requires the entry of a new IMEI when attached to a job.' & |
                                              '||This part can therefore not be added at this time.', |
                                              'ServiceBase 2000', Icon:Hand, |
                                               '&OK', 1, 0)
                                  ! End Change 216 BE(26/06/03)
                                  Else !If SecurityCheck('JOBS - ORDER PARTS')
                                      Case MessageEx(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                      '<13,10>'&|
                                      '<13,10>This part is out of stock. Do you wish to place it on Back Order?','ServiceBase 2000',|
                                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                                          Of 1 ! &Yes Button
                                              Case def:SummaryOrders
                                                  Of 0 !Old Way
                                                      Do MakePartsOrder
                                                      Do Add_Chargeable_Part
                                                      par:Pending_Ref_number  = ope:Ref_Number
                                                      par:Date_Ordered    = ''
                                                      Access:PARTS.Update()
      
                                                      ! Start Change BE030 BE(12/05/2004)
                                                      !if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('chargeable').
                                                      if def:PickNoteNormal or def:PickNoteMainStore then
                                                          CreatePickingNote(sto:location)
                                                          InsertPickingPart ('chargeable')
                                                      end
                                                      ! Start Change BE030 BE(12/05/2004)
                                                  Of 1 !New Way
                                                      Do Add_Chargeable_Part
                                                      Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                                      ope:Supplier    = par:Supplier
                                                      ope:Part_Number = par:Part_Number
                                                      If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                          ope:Quantity    += 1
                                                          Access:ORDPEND.Update()
                                                      Else !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                          Do MakePartsOrder
                                                      End !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                      par:Pending_Ref_Number  = ope:Ref_Number
                                                      par:Requested   = True
                                                      par:Date_Ordered = ''
                                                      Access:PARTS.Update()
      
                                                      ! Start Change BE030 BE(12/05/2004)
                                                      !if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('chargeable').
                                                      if def:PickNoteNormal or def:PickNoteMainStore then
                                                          CreatePickingNote(sto:location)
                                                          InsertPickingPart ('chargeable')
                                                      end
                                                      ! Start Change BE030 BE(12/05/2004)
      
                                              End !Case def:SummaryOrders
      
                                          Of 2 ! &No Button
                                      End!Case MessageEx
                                  End !If SecurityCheck('JOBS - ORDER PARTS')
      
                              Else!If sto:quantity_stock < 1
                                  access:parts.clearkey(par:part_number_key)
                                  par:ref_number  = job:ref_number
                                  par:part_number = sto:part_number
                                  if access:parts.fetch(par:part_number_key)
                                      !TH 09/02/04
                                      Access:LOCATION.ClearKey(loc:ActiveLocationKey)
                                      loc:Active   = 1
                                      loc:Location = sto:Location
                                      If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.
      
                                      ! Start Change 4290 BE(18/05/04)
                                      !if loc:PickNoteEnable then
                                      IF NOT loc:PickNoteEnable THEN
                                      ! End Change 4290 BE(18/05/04)
                                          ! Start Change 2116 BE(26/06/03)
                                          IF (sto:RF_BOARD) THEN
                                              glo:select2 = ''
                                              glo:select3 = job:model_number
                                              glo:select4 = epr:part_number
                                              SETCURSOR()
                                              EnterNewIMEI
                                              SETCURSOR(cursor:wait)
                                              tmp_RF_Board_IMEI = CLIP(glo:select2)
                                              glo:select2 = ''
                                              glo:select3 = ''
                                              glo:select4 = ''
                                              IF (tmp_RF_Board_IMEI = '') THEN
                                                  CYCLE
                                              END
                                              GET(audit,0)
                                              IF (access:audit.primerecord() = level:benign) THEN
                                                  aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                                                      '<13,10>Old IMEI ' & Clip(job:esn)
                                                  aud:ref_number    = job:ref_number
                                                  aud:date          = Today()
                                                  aud:time          = Clock()
                                                  access:users.clearkey(use:password_key)
                                                  use:password =glo:password
                                                  access:users.fetch(use:password_key)
                                                  aud:user = use:user_code
                                                  aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                                                  IF (access:audit.insert() <> Level:benign) THEN
                                                      access:audit.cancelautoinc()
                                                  END
                                              END
                                              access:JOBSE.clearkey(jobe:RefNumberKey)
                                              jobe:RefNumber  = job:Ref_Number
                                              IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                                                  IF (access:JOBSE.primerecord() = Level:Benign) THEN
                                                      jobe:RefNumber  = job:Ref_Number
                                                      IF (access:JOBSE.tryinsert()) THEN
                                                          access:JOBSE.cancelautoinc()
                                                      END
                                                  END
                                              END
                                              IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                                                  jobe:PRE_RF_BOARD_IMEI = job:ESN
                                                  access:jobse.update()
                                              END
                                              job:ESN = tmp_RF_Board_IMEI
                                              access:jobs.update()
                                          END
                                          ! End Change 2116 BE(26/06/03)
                                      end
                                      Do Add_Chargeable_Part
      
                                      ! Start Change BE030 BE(12/05/2004)
                                      !if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('chargeable').
                                      if def:PickNoteNormal or def:PickNoteMainStore then
                                          CreatePickingNote(sto:location)
                                          InsertPickingPart ('chargeable')
                                      end
                                      ! Start Change BE030 BE(12/05/2004)
                                      sto:quantity_stock -= 1
                                      If sto:quantity_stock < 0
                                          sto:quantity_stock = 0
                                      End!If sto:quantity_stock < 0
                                      access:stock.update()
                                      get(stohist,0)
                                      if access:stohist.primerecord() = level:benign
                                          shi:ref_number           = sto:ref_number
                                          access:users.clearkey(use:password_key)
                                          use:password              =glo:password
                                          access:users.fetch(use:password_key)
                                          shi:user                  = use:user_code
                                          shi:date                 = today()
                                          shi:transaction_type     = 'DEC'
                                          shi:job_number           = job:ref_number
                                          shi:quantity             = 1
                                          shi:purchase_cost        = sto:purchase_cost
                                          shi:sale_cost            = sto:sale_cost
                                          shi:retail_cost          = sto:retail_cost
                                          shi:information          = ''
                                          shi:notes                = 'STOCK DECREMENTED'
                                          if access:stohist.insert()
                                              access:stohist.cancelautoinc()
                                          end
                                      end!if access:stohist.primerecord() = level:benign
                                  end!if access:parts.fetch(par:part_number_key)
                              End!If sto:quantity_stock < 1
                          End!If sto:sundry_item = 'YES'
                      end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                  end!if access:stomodel.fetch(stm:model_number_key) = Level:Benign
              End!Loop x# = 1 To Records(glo:Queue)
          End!If Records(glo:Queue)
      End!if globalresponse = requestcompleted
      globalrequest     = saverequest#
      BRW10.ResetSort(1)
      BRW9.ResetSort(1)
      Do CountParts
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Multi_Insert, Accepted)
    OF ?Add_Chargeable_Adjustment
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Chargeable_Adjustment, Accepted)
      Include('genjobs.inc','ChargeableAdjustment')
      BRW9.ResetSort(1)
      Do CountParts
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Chargeable_Adjustment, Accepted)
    OF ?Multi_Insert:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Multi_Insert:2, Accepted)
      !Are Parts Codes Required?
      Required# = 0
      If job:warranty_job = 'YES'
          access:chartype.clearkey(cha:charge_type_key)
          cha:charge_type = job:warranty_charge_type
          If access:chartype.fetch(cha:charge_type_key) = Level:Benign
              If cha:force_warranty = 'YES'
                  If job:Repair_Type_Warranty <> ''
                      Access:REPTYDEF.ClearKey(rtd:Warranty_Key)
                      rtd:Warranty    = 'YES'
                      rtd:Repair_Type = job:Repair_Type_Warranty
                      If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                          !Found
                          If rtd:CompFaultCoding
                              Required# = 1
                          End !If rtd:CompFaultCoding
                      Else!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
      
                  Else !If job:Repair_Type_Warranty <> ''
                      Required# = 1
                  End !If job:Repair_Type_Warranty <> ''
              End
          end !if
      
          If Required# = 1
              Required# = 0
              save_map_id = access:manfaupa.savefile()
              access:manfaupa.clearkey(map:field_number_key)
              map:manufacturer = job:manufacturer
              set(map:field_number_key,map:field_number_key)
              loop
                  if access:manfaupa.next()
                     break
                  end !if
                  if map:manufacturer <> job:manufacturer      |
                      then break.  ! end if
                  If MAP:Compulsory = 'YES'
                      required# = 1
                      Break
                  End!If MAP:Compulsory = 'YES'
              end !loop
              access:manfaupa.restorefile(save_map_id)
          End !If Required# = 1
      End!If job:warranty_job = 'YES'
      
      If required# = 1
          Post(event:accepted,?Insert:3)
      Else!If required# = 1
          glo:select11 = job:manufacturer
          glo:select12 = job:model_number
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          pick_model_stock
          glo:select11 = ''
          glo:select12 = ''
          if globalresponse = requestcompleted
              If Records(glo:Queue)
                  Loop x# = 1 To Records(glo:Queue)
                      Get(glo:Queue,x#)
                      access:stomodel.clearkey(stm:model_number_key)
                      stm:ref_number   = glo:pointer
                      stm:manufacturer = job:manufacturer
                      stm:model_number = job:model_number
                      if access:stomodel.fetch(stm:model_number_key) = Level:Benign
                          access:stock.clearkey(sto:ref_number_key)
                          sto:ref_number = stm:ref_number
                          if access:stock.fetch(sto:ref_number_key) = Level:Benign
                              If sto:sundry_item = 'YES'
                                  access:warparts.clearkey(wpr:part_number_key)
                                  wpr:ref_number  = job:ref_number
                                  wpr:part_number = sto:part_number
                                  if access:warparts.fetch(wpr:part_number_key)
                                      Do Add_Warranty_Part
                                      ! Start Change BE030 BE(12/05/2004)
                                      !if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('warranty').
                                      if def:PickNoteNormal or def:PickNoteMainStore then
                                          CreatePickingNote(sto:location)
                                          InsertPickingPart ('warranty')
                                      end
                                      ! Start Change BE030 BE(12/05/2004)
                                  End!if access:warparts.fetch(wpr:part_number_key)
                              Else!If sto:sundry_item <> 'YES'
                                  If sto:quantity_stock < 1
                                      If SecurityCheck('JOBS - ORDER PARTS')
                                          Case MessageEx(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                            '<13,10>'&|
                                            '<13,10>This part is out of stock.','ServiceBase 2000',|
                                                         'Styles\hand.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                                              Of 1 ! &OK Button
                                          End !
                                      ! Start Change 216 BE(26/06/03)
                                      ELSIF (sto:RF_BOARD) THEN
      
                                          Beep(Beep:SystemHand)  ;  Yield()
                                          Message(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                                  '||There are insufficient items in stock!' & |
                                                  '||This part requires the entry of a new IMEI when attached to a job.' & |
                                                  '||This part can therefore not be added at this time.', |
                                                  'ServiceBase 2000', Icon:Hand, |
                                                  '&OK', 1, 0)
                                      ! End Change 216 BE(26/06/03)
                                      Else !If SecurityCheck('JOBS - ORDER PARTS')
      
                                          Case MessageEx(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                            '<13,10>'&|
                                            '<13,10>This part is out of stock. Do you wish to place it on Back Order?','ServiceBase 2000',|
                                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                              Of 1 ! &Yes Button
                                                  
                                                  Case def:SummaryOrders
                                                      Of 0 !Old Way
                                                          Do MakeWarPartsOrder
                                                          Do Add_Warranty_Part
                                                          wpr:Pending_Ref_number  = ope:Ref_Number
                                                          wpr:Date_Ordered    = ''
                                                          Access:WARPARTS.Update()
                                                          ! Start Change BE030 BE(12/05/2004)
                                                          !if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('warranty').
                                                          if def:PickNoteNormal or def:PickNoteMainStore then
                                                              CreatePickingNote(sto:location)
                                                              InsertPickingPart ('warranty')
                                                          end
                                                          ! Start Change BE030 BE(12/05/2004)
                                                      Of 1 !New Way
                                                          Do Add_Warranty_Part
                                                          Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                                          ope:Supplier    = wpr:Supplier
                                                          ope:Part_Number = wpr:Part_Number
                                                          If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                              ope:Quantity    += 1
                                                              Access:ORDPEND.Update()
                                                          Else !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                              Do MakeWarPartsOrder
                                                          End !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                          wpr:Pending_Ref_Number  = ope:Ref_Number
                                                          wpr:Requested   = True
                                                          wpr:Date_Ordered = ''
                                                          Access:WARPARTS.Update()
      
                                                          ! Start Change BE030 BE(12/05/2004)
                                                          !if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('warranty').
                                                          if def:PickNoteNormal or def:PickNoteMainStore then
                                                              CreatePickingNote(sto:location)
                                                              InsertPickingPart ('warranty')
                                                          end
                                                          ! Start Change BE030 BE(12/05/2004)
      
                                                  End !Case def:SummaryOrders
                                                  
      
                                              Of 2 ! &No Button
      
                                          End!Case MessageEx
                                      End !If SecurityCheck('JOBS - ORDER PARTS')
      
                                  Else!If sto:quantity_stock < 1
                                      access:warparts.clearkey(wpr:part_number_key)
                                      wpr:ref_number  = job:ref_number
                                      wpr:part_number = sto:part_number
                                      if access:warparts.fetch(wpr:part_number_key)
      
                                          !TH 09/02/04
                                          Access:LOCATION.ClearKey(loc:ActiveLocationKey)
                                          loc:Active   = 1
                                          loc:Location = sto:Location
                                          If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.
      
                                          ! Start Change 4290 BE(18/05/04)
                                          !if loc:PickNoteEnable then
                                          IF NOT loc:PickNoteEnable THEN
                                          ! End Change 4290 BE(18/05/04)
                                              ! Start Change 2116 BE(26/06/03)
                                              IF (sto:RF_BOARD) THEN
                                                  glo:select2 = ''
                                                  glo:select3 = job:model_number
                                                  glo:select4 = epr:part_number
                                                  SETCURSOR()
                                                  EnterNewIMEI
                                                  SETCURSOR(cursor:wait)
                                                  tmp_RF_Board_IMEI = CLIP(glo:select2)
                                                  glo:select2 = ''
                                                  glo:select3 = ''
                                                  glo:select4 = ''
                                                  IF (tmp_RF_Board_IMEI = '') THEN
                                                      CYCLE
                                                  END
                                                  GET(audit,0)
                                                  IF (access:audit.primerecord() = level:benign) THEN
                                                      aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                                                          '<13,10>Old IMEI ' & Clip(job:esn)
                                                      aud:ref_number    = job:ref_number
                                                      aud:date          = Today()
                                                      aud:time          = Clock()
                                                      access:users.clearkey(use:password_key)
                                                      use:password =glo:password
                                                      access:users.fetch(use:password_key)
                                                      aud:user = use:user_code
                                                      aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                                                      IF (access:audit.insert() <> Level:benign) THEN
                                                          access:audit.cancelautoinc()
                                                      END
                                                  END
                                                  access:JOBSE.clearkey(jobe:RefNumberKey)
                                                  jobe:RefNumber  = job:Ref_Number
                                                  IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                                                      IF (access:JOBSE.primerecord() = Level:Benign) THEN
                                                          jobe:RefNumber  = job:Ref_Number
                                                          IF (access:JOBSE.tryinsert()) THEN
                                                              access:JOBSE.cancelautoinc()
                                                          END
                                                      END
                                                  END
                                                  IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                                                      jobe:PRE_RF_BOARD_IMEI = job:ESN
                                                      access:jobse.update()
                                                  END
                                                  job:ESN = tmp_RF_Board_IMEI
                                                  access:jobs.update()
                                              END
                                              ! End Change 2116 BE(26/06/03)
                                          end
                                          Do Add_Warranty_Part
      
                                          ! Start Change BE030 BE(12/05/2004)
                                          !if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('warranty').
                                          if def:PickNoteNormal or def:PickNoteMainStore then
                                              CreatePickingNote(sto:location)
                                              InsertPickingPart ('warranty')
                                          end
                                          ! Start Change BE030 BE(12/05/2004)
      
                                          sto:quantity_stock -= 1
                                          If sto:quantity_stock < 0
                                              sto:quantity_stock = 0
                                          End!If sto:quantity_stock < 0
                                          access:stock.update()
                                          get(stohist,0)
                                          if access:stohist.primerecord() = level:benign
                                              shi:ref_number           = sto:ref_number
                                              access:users.clearkey(use:password_key)
                                              use:password              =glo:password
                                              access:users.fetch(use:password_key)
                                              shi:user                  = use:user_code    
                                              shi:date                 = today()
                                              shi:transaction_type     = 'DEC'
                                              shi:job_number           = job:ref_number
                                              shi:quantity             = 1
                                              shi:purchase_cost        = sto:purchase_cost
                                              shi:sale_cost            = sto:sale_cost
                                              shi:retail_cost          = sto:retail_cost
                                              shi:information          = ''
                                              shi:notes                = 'STOCK DECREMENTED'
                                              if access:stohist.insert()
                                                 access:stohist.cancelautoinc()
                                              end
                                          end!if access:stohist.primerecord() = level:benign
                                      end!if access:warparts.fetch(wpr:part_number_key)
                                  End!If sto:quantity_stock < 1
                              End!If sto:sundry_item <> 'YES'
                          end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                      end!if access:stomodel.fetch(stm:model_number_key) = Level:Benign
                  End!Loop x# = 1 To Records(glo:Queue)
              End!If Records(glo:Queue)
          End!if globalresponse = requestcompleted
      End!If required# = 1
      BRW10.ResetSort(1)
      BRW9.ResetSort(1)
      Do CountParts
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Multi_Insert:2, Accepted)
    OF ?Add_Warranty_Adjustment
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Warranty_Adjustment, Accepted)
      Include('genjobs.inc','WarrantyAdjustment')
      BRW10.ResetSort(1)
      Do CountParts
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Warranty_Adjustment, Accepted)
    OF ?Costs
      ThisWindow.Update
      View_Costs
      ThisWindow.Reset
    OF ?CompleteButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CompleteButton, Accepted)
      ! Start Change 3922 BE(27/02/04)
      Total_Price('E', vat", total", balance")
      Total_Price('C', vat2", total2", balance2")
      total$ = total" + total2"
      ! End Change 3922 BE(27/02/04)
      
      ! Start Change 2838 BE(24/07/03)  ACCESS RIGHTS SECURITY CHECK
      IF ((job:date_completed <> '') AND SecurityCheck('JOBS - NO RECOMPLETE') = Level:Benign) THEN
          MessageEx('You do not have access to this option.','ServiceBase 2000',|
                    'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
       ! Start Change 4217 BE(29/04/04)
!      ! Start Change 3922 BE(27/02/04)
!      ELSIF ((job:Chargeable_Job = 'YES') AND |
!              (job:Estimate = 'YES') AND |
!              ((total$ = 0) OR (total$ > job:estimate_If_Over)) AND |
!              (job:Estimate_Accepted <> 'YES')) THEN
!            MessageEx('This job cannot be completed. An estimate is required.','ServiceBase 2000',|
!                      'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
!                      beep:systemhand,msgex:samewidths,84,26,0)
!      ! End Change 3922 BE(27/02/04)
      ELSIF ((job:Chargeable_Job = 'YES') AND |
              (job:Estimate = 'YES') AND |
              ((total$ = 0) OR (total$ > job:estimate_If_Over)) AND |
              (job:Estimate_Accepted = 'NO') AND (job:Estimate_Rejected = 'NO')) THEN
            MessageEx('This job cannot be completed. An estimate is required.','ServiceBase 2000',|
                      'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                      beep:systemhand,msgex:samewidths,84,26,0)
      ! End Change 4217 BE(29/04/04)
      ELSE
      ! End Change 2838 BE(24/07/03)
          ! Start Change 3830 BE(23/01/04)
          !Case MessageEx('You are about to completed this job.<13,10><13,10>(Warning! You will not be able to uncomplete it if you continue.)<13,10>Are you sure?','ServiceBase 2000',|
          !               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
          Case MessageEx('You are about to complete this job.<13,10><13,10>(Warning! You will not be able to uncomplete it if you continue.)<13,10>Are you sure?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
          ! End Change 3830 BE(23/01/04)
              Of 1 ! &Yes Button
                  ! Start Change 3804 BE(16/01/04)
                  IF (TempStatusCode <> job:Current_Status) THEN
                      GetStatus(SUB(TempStatusCode,1,3),1,'JOB')
                  END
                  ! End Change 3804 BE(16/01/04)
                  Set(DEFAULTS)
                  Access:DEFAULTS.Next()
                  !Save the notes field
                  Access:JOBNOTES.TryUpdate()
      
                  glo:errortext = ''
                  Complete# = 0
                  If job:date_completed = ''
                      !Check to see that all the required fields are filled in
                      CompulsoryFieldCheck('C')
                      If glo:ErrorText <> ''
                          glo:errortext = 'You cannot complete this job due to the following error(s): <13,10>' & Clip(glo:errortext)
                          Error_Text
                          glo:errortext = ''
                          !thismakeover.SetWindow(win:form)
                      Else!If glo:ErrorText <> ''
                          If GETINI('VALIDATE','ForceAccCheckComp',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                              If LocalValidateAccessories() = Level:Benign
                                  Complete# = 1
                              Else !If LocalValidateAccessories() = Level:Benign
                                  Access:JOBS.Update()
                              End !If LocalValidateAccessories() = Level:Benign
                          Else
                              Complete# = 1
                          End !If GETINI('VALIDATE','ForceAccCheckComp',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                      End!If glo:ErrorText <> ''
                  Else!If job:date_completed = ''
                      Case MessageEx('Are you sure you want to change the Completion Date?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              Complete# = 1
                          Of 2 ! &No Button
                      End!Case MessageEx
                  End!If job:date_completed = ''
      
                  If Complete# = 1
                      !**** TH 02/04 Time to deal with deferred IMEI changes
                      !if sts:PickNoteEnable then
                      ! Start Change xxxx BE(18/03/04)
                      IF (jobe:Pre_RF_Board_IMEI = '') THEN
                      ! End Change xxxx BE(18/03/04)
                          partpntstore = access:parts.savefile() !backup current pointer
                          warpartpntstore = access:warparts.savefile() !backup current pointer
      
                          access:PARTS.clearkey(par:Part_Number_Key) !check for existing picking record
                          par:Ref_Number = job:ref_number
                          set(par:Part_Number_Key,par:Part_Number_Key)
                          loop
                              if access:PARTS.next() then break. !no records
                              ! Start Change xxxx BE(18/03/04)
                              IF (par:Ref_Number <> job:ref_number) THEN
                                  BREAK
                              END
                              ! End Change xxxx BE(18/03/04)
                              !if rf_board then new imei
                              UpdateRFBoardIMEI(par:part_ref_number,par:part_number)
                          end
      
                          access:WARPARTS.clearkey(wpr:Part_Number_Key) !check for existing picking record
                          wpr:Ref_Number = job:ref_number
                          set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                          loop
                              if access:WARPARTS.next() then break. !no records
                              ! Start Change xxxx BE(18/03/04)
                              IF (wpr:Ref_Number <> job:ref_number) THEN
                                  BREAK
                              END
                              ! End Change xxxx BE(18/03/04)
                              !if rf_board then new imei
                              UpdateRFBoardIMEI(wpr:part_ref_number,wpr:part_number)
                          end
      
                          access:parts.restorefile(partpntstore)
                          access:warparts.restorefile(warpartpntstore)
                      ! Start Change xxxx BE(18/03/04)
                      END
                      ! End Change xxxx BE(18/03/04)
                      !end !if sts:PickNoteEnable
                      !**** TH 02/04
                      Print_Despatch_Note_Temp = ''
                      !Fill in the totals with the right costs from the
                      !Price Structure.
                      Do Pricing
                      Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                      man:Manufacturer = job:Manufacturer
                      If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                          !Found
                          If man:QAAtCompletion
                              !If QA at completion is ticked, then don't actually
                              !complete the job. That is done at QA
                              GetStatus(605,1,'JOB')
                              ! Start Change 3940 BE(02/03/04)
                              TempStatusCode = job:Current_Status
                              ! End Change 3940 BE(02/03/04)
      
                              Case MessageEx('The Repair has been completed. You must now QA this unit.','ServiceBase 2000',|
                                             'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              job:On_Test = 'YES'
                              job:Date_On_Test = Today()
                              job:Time_On_Test = Clock()
                              ! Start Change 3879 BE(26/02/2004)
                              Access:JOBSE.ClearKey(jobe:RefNumberKey)
                              jobe:RefNumber = job:Ref_number
                              IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
                                  ! Start Change 4053 BE(19/03/04)
                                  !jobe:CompleteRepairType = 1
                                  !jobe:CompleteRepairDate = TODAY()
                                  !jobe:CompleteRepairTime = CLOCK()
                                  !access:jobse.update()
                                  ! Start Change 4200 BE(04/05/04)
                                  !IF (jobe:CompleteRepairDate = '') THEN
                                  !    jobe:CompleteRepairType = 1
                                  !    jobe:CompleteRepairDate = TODAY()
                                  !    jobe:CompleteRepairTime = CLOCK()
                                  !    access:jobse.update()
                                  !END
                                  access:JOBSENG.clearkey(joe:JobNumberKey)
                                  joe:JobNumber = job:Ref_Number
                                  SET(joe:JobNumberKey, joe:JobNumberKey)
                                  IF (access:JOBSENG.next() = Level:Benign) THEN
                                      IF ((jobe:CompleteRepairDate = '') OR |
                                          (joe:DateAllocated > jobe:CompleteRepairDate) OR |
                                          ((joe:DateAllocated = jobe:CompleteRepairDate) AND |
                                           (joe:TimeAllocated > jobe:CompleteRepairTime))) THEN
                                          jobe:CompleteRepairType = 1
                                          jobe:CompleteRepairDate = TODAY()
                                          jobe:CompleteRepairTime = CLOCK()
                                          access:jobse.update()
                                      END
                                  Else
                                      !Start - Oop!. Forget to allow for there being no engineer history (DBH: 04-08-2005)
                                      If jobe:CompleteRepairDate = ''
                                          jobe:CompleteRepairType = 1
                                          jobe:CompleteRepairDate = Today()
                                          jobe:CompleteRepairTime = Clock()
                                          Access:JOBSE.Update()
                                      End ! If jobe:CompleteRepairDate = ''
                                  END
                                  ! End Change 4200 BE(04/05/04)
                                  ! End Change 4053 BE(19/03/04)
                              END
                              ! End Change 3879 BE(26/02/2004)
      
                              ! Start Change 3804 BE(23/01/04)
                              TempStatusCode = job:Current_Status
                              ! End Change 3804 BE(23/01/04)
                              Post(Event:Accepted,?OK)
                          Else !If man:QAAtCompletion
                              !To avoid not changing to the right status,
                              !change to completed, first, and then it should
                              !Change again to one of the below status's if necessary
                              GetStatus(705,1,'JOB')
                              ! Start Change 3940 BE(02/03/04)
                              TempStatusCode = job:Current_Status
                              ! End Change 3940 BE(02/03/04)
      
                              If ExchangeAccount(job:Account_Number)
                                  !Is the Trade Account used for Exchange Units
                                  !if so, then there's no need to despatch the
                                  !unit normal. Just auto fill the despatch fields
                                  !and mark the job as to return to exchange stock.
                                  ForceDespatch
                                  GetStatus(707,1,'JOB')
                                  ! Start Change 3940 BE(02/03/04)
                                  TempStatusCode = job:Current_Status
                                  ! End Change 3940 BE(02/03/04)
      
                              Else !If ExchangeAccount(job:Account_Number)
      
                                  !Will the job be restocked, i.e. has, or will have, an
                                  !Exchange Unit attached, or is it a normal despatch.
                                  restock# = 0
                                  If ExchangeAccount(job:Account_Number)
                                      restock# = 1
                                  Else !If ExchangeAccount(job:Account_Number)
                                      glo:select1  = job:ref_number
                                      If ToBeExchanged()
                                      !Has this unit got an Exchange, or is it expecting an exchange
                                          restock# = 1
                                      End!If job:exchange_unit_number <> ''
                                      !If there is an loan unit attached, assume that a despatch note is required.
                                      If job:loan_unit_number <> ''
                                          restock# = 0
                                      End!If job:loan_unit_number <> ''
                                  End !If ExchangeAccount(job:Account_Number)
      
                                  If CompletePrintDespatch(job:Account_Number) = Level:Benign
                                      Print_Despatch_Note_Temp = 'YES'
                                  End !CompletePrintDespatch(job:Account_Number) = Level:Benign
      
                                  If restock# = 0
      
                                      !Check the Accounts' "Despatch Paid/Invoiced" jobs ticks
                                      !If either of those are ticked then don't despatch yet
                                      !print_despatch_note_temp = ''
      
                                      If AccountAllowedToDespatch()
                                          !Do we need to despatch at completion?
                                          If DespatchAtClosing()
                                              !Does the Trade Account have "Skip Despatch" set?
                                              If AccountSkipDespatch(job:Account_Number) = Level:Benign
                                                  access:courier.clearkey(cou:courier_key)
                                                  cou:courier = job:courier
                                                  if access:courier.tryfetch(cou:courier_key)
                                                      Case MessageEx('Cannot find the Courier attached to this job.','ServiceBase 2000',|
                                                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                          Of 1 ! &OK Button
                                                      End!Case MessageEx
                                                  Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                                                      !Give the job a unique Despatch Number
                                                      !then call the despsing.inc.
                                                      !This calls the relevant exports etc, and different
                                                      !despatch procedures depending on the Courier.
                                                      !It will then print a Despatch Note and change
                                                      !The status as required.
                                                      If access:desbatch.primerecord() = Level:Benign
                                                          If access:desbatch.tryinsert()
                                                              access:desbatch.cancelautoinc()
                                                          Else!If access:despatch.tryinsert()
                                                              DespatchSingle()
                                                              Print_Despatch_Note_Temp = ''
                                                          End!If access:despatch.tryinsert()
                                                      End!If access:desbatch.primerecord() = Level:Benign
                                                  End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                                              Else
                                                  GetStatus(705,1,'JOB')
                                                  ! Start Change 3940 BE(02/03/04)
                                                  TempStatusCode = job:Current_Status
                                                  ! End Change 3940 BE(02/03/04)
      
                                              End !If AccountSkipDespatch = Level:Benign
                                          Else !If DespatchAtClosing()
                                              If job:Loan_Unit_Number <> ''
                                                  GetStatus(811,1,'JOB')
                                                  ! Start Change 3940 BE(02/03/04)
                                                  TempStatusCode = job:Current_Status
                                                  ! End Change 3940 BE(02/03/04)
                                              Else
                                                  GetStatus(705,1,'JOB')
                                                  ! Start Change 3940 BE(02/03/04)
                                                  TempStatusCode = job:Current_Status
                                                  ! End Change 3940 BE(02/03/04)
      
                                              End !If job:Loan_Unit_Number <> ''
                                          End !If DespatchAtClosing()
                                      Else !If AccountAllowedToDespatch
                                          GetStatus(705,1,'JOB')
                                          ! Start Change 3940 BE(02/03/04)
                                          TempStatusCode = job:Current_Status
                                          ! End Change 3940 BE(02/03/04)
      
                                      End !If AccountAllowedToDespatch
                                  Else !If restock# = 0
                                      If job:Exchange_Unit_Number <> ''
                                          !Are either of the Chargeable or Warranty repair types BERs?
                                          !If so, then don't change the status to 707 because it shouldn't
                                          !be returned to the Exchange Stock.
                                          Comp# = 0
                                          If job:Chargeable_Job = 'YES'
                                              If BERRepairType(job:Repair_Type) = Level:Benign
                                                   Comp# = 1
                                              End !If BERRepairType(job:Repair_Type) = Level:Benign
                                          End !If job:Chageable_Job = 'YES'
                                          If job:Warranty_Job = 'YES' and Comp# = 0
                                              If BERRepairType(job:Repair_Type_Warranty) = Level:Benign
                                                  Comp# = 1
                                              End !If BERRepairType(job:Repair_Type) = Level:Benign
                                          End !If job:Warranty_Job = 'YES'
                                          If Comp# = 1
                                              Case MessageEx('This unit has been exchanged but is a B.E.R. Repair. '&|
                                                '<13,10>DO NOT place this unit into Exchange Stock.','ServiceBase 2000',|
                                                             'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                                  Of 1 ! &OK Button
                                              End!Case MessageEx
                                              GetStatus(705,1,'JOB')
                                              ! Start Change 3940 BE(02/03/04)
                                              TempStatusCode = job:Current_Status
                                              ! End Change 3940 BE(02/03/04)
      
                                              Print_Despatch_Note_Temp = 'NO'
                                          Else !If Comp# = 1
                                              Case MessageEx('This unit has been exchanged. It should now be placed into Exchange Stock.','ServiceBase 2000',|
                                                             'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                                                  Of 1 ! &OK Button
                                              End!Case MessageEx
                                              GetStatus(707,1,'JOB')
                                              ! Start Change 3940 BE(02/03/04)
                                              TempStatusCode = job:Current_Status
                                              ! End Change 3940 BE(02/03/04)
      
                                          End !If Comp# = 1
                                      Else !If job:Exchange_Unit_Number <> ''
                                          GetStatus(705,1,'JOB')
                                          ! Start Change 3940 BE(02/03/04)
                                          TempStatusCode = job:Current_Status
                                          ! End Change 3940 BE(02/03/04)
      
                                      End !If job:Exchange_Unit_Number <> ''    
                                  End !If restock# = 0
                                  !Does the Account care about Bouncers?
                                  Check_For_Bouncers_Temp = 1
                                  If CompleteCheckForBouncer(job:Account_Number)
                                      Check_For_Bouncers_Temp = 0
                                  End !CompleteCheckForBouncer(job:Account_Number) = Level:Benign
      
                              End !If ExchangeAccount(job:Account_Number)
      
                              !Fill in the relevant fields
      
                              saved_ref_number_temp   = job:ref_number
                              saved_esn_temp  = job:esn
                              saved_msn_temp  = job:msn
                              job:date_completed = Today()
                              job:time_completed = Clock()
                              date_completed_temp = job:date_completed
                              time_completed_temp = job:time_completed
                              job:completed = 'YES'
                              ! Start Change 3879 BE(26/02/2004)
                              Access:JOBSE.ClearKey(jobe:RefNumberKey)
                              jobe:RefNumber = job:Ref_number
                              IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
                                  IF (jobe:CompleteRepairDate = '') THEN
                                      jobe:CompleteRepairType = 2
                                      jobe:CompleteRepairDate = TODAY()
                                      jobe:CompleteRepairTime = CLOCK()
                                      access:jobse.update()
                                  END
                              Else
                                  !Start - Oop!. Forget to allow for there being no engineer history (DBH: 04-08-2005)
                                  If jobe:CompleteRepairDate = ''
                                      jobe:CompleteRepairType = 2
                                      jobe:CompleteRepairDate = Today()
                                      jobe:CompleteRepairTime = Clock()
                                      Access:JOBSE.Update()
                                  End ! If jobe:CompleteRepairDate = ''
      
                              END
                              ! End Change 3879 BE(26/02/2004)
      
                              If Check_For_Bouncers_Temp
                                  !Check if the job is a Bouncer and add
                                  !it to the Bouncer List
                                  If CountBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
                                      !Bouncer
                                      AddBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
                                  Else !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
      
                                  End !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
      
                              End !If Check_For_Bouncers_Temp
      
                              !Check if this job is an EDI job
                              Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                              mod:Model_Number    = job:Model_Number
                              If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                  !Found
                                  job:edi = PendingJob(mod:Manufacturer)
                                  job:Manufacturer    = mod:Manufacturer
                              Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
                              !Mark the job's exchange unit as being available
                              CompleteDoExchange(job:Exchange_Unit_Number)
      
                              Do QA_Group
                              !ThisMakeover.SetWindow(win:form)
                              Access:JOBS.TryUpdate()
      
                              If ExchangeAccount(job:Account_Number)
                                  Case MessageEx('This is an Exchange Repair. Please re-stock the unit.','ServiceBase 2000',|
                                                 'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                              End !If ExchangeAccount(job:Account_Number)
      
                              If print_despatch_note_temp = 'YES'
                                  If restock# = 1
                                      restocking_note
                                  Else!If restock# = 1
                                      despatch_note
                                  End!If restock# = 1
                                  glo:select1  = ''
                              End!If print_despatch_note_temp = 'YES'
                              ! Start Change 3804 BE(23/01/04)
                              TempStatusCode = job:Current_Status
                              ! End Change 3804 BE(23/01/04)
                              Post(Event:Accepted,?OK)
                          End !If man:QAAtCompletion
                      Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      
                  End !Complete# = 1
      
      
              Of 2 ! &No Button
          End!Case MessageEx
      
          !Completed?
          If job:Date_Completed <> ''
              ?Completed{prop:hide} = 0
          End!If job:Date_Completed <> ''
          Display()
      ! Start Change 2838 BE(24/07/03)  ACCESS RIGHTS SECURITY CHECK
      END
      ! End Change 2838 BE(24/07/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CompleteButton, Accepted)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      ! Start Change 3804 BE(16/01/04)
      IF (TempStatusCode <> job:Current_Status) THEN
          GetStatus(SUB(TempStatusCode,1,3),1,'JOB')
      END
      ! End Change 3804 BE(16/01/04)
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?ContactHistory
      ThisWindow.Update
      Browse_Contact_History
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ContactHistory, Accepted)
      glo:select12    = ''
      Access:ContHist.ClearKey(cht:Ref_Number_Key)
      cht:Ref_Number = job:Ref_Number
      SET(cht:Ref_Number_Key,cht:Ref_Number_Key)
      LOOP
        IF Access:ContHist.Next()
          BREAK
        END
        IF cht:Ref_Number <> job:Ref_Number
          BREAK
        END
        ?ContactHistory{PROP:FONTSTYLE} = FONT:Bold
        BREAK
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ContactHistory, Accepted)
    OF ?Invoice
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Invoice, Accepted)
      Case MessageEx('Are you sure you want to create an Invoice?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Set(defaults)
              access:defaults.next()
              tmp:CreateInvoice = 0
              error# = 0
      
              If job:invoice_Number <> ''
                  error# = 1
                  Case MessageEx('This job has already been Invoiced.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              End!If job:invoice_Number <> ''
      
              If error# = 0 And job:bouncer = 'YES'
                  error# = 1
                  Case MessageEx('Cannot Invoice! This job has been marked as a Bouncer.<13,10><13,10>You must authorise this job before you can produce and Invoice.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              End!If error# = 0 And job:bouncer = 'YES'
      
              If error# = 0 And job:date_Completed = ''
                  error# = 1
                  Case MessageEx('Cannot Invoice! This job has not been completed.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              End!If error# = 0 And job:date_Completed = ''
      
              If error# = 0 and job:Chargeable_job <> 'YES'
                  error# = 1
                  Case MessageEx('Cannot Invoice! This is a Warranty Only Job.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              End!If error# = 0 and job:Chargeable_job <> 'YES'
      
              If error# = 0 And def:qa_required = 'YES'
                  error# = 1
                  IF job:qa_passed <> 'YES' And job:qa_second_passed <> 'YES'
                      Case MessageEx('Cannot Invoice! The selected job has not passed QA checks.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End!IF job:qa_passed <> 'YES' And job:qa_second_passed <> 'YES'
              End!If error# = 0 And def:qa_required = 'YES'
      
              If error# = 0
                  If job:ignore_chargeable_charges = 'YES'
                      If job:sub_total = 0
                          error# = 1
                          Case MessageEx('Cannot Invoice! This job has not been priced.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      End!If job:sub_total = 0
                  Else!If job:ignore_chargeable_charges = 'YES'
                      Pricing_Routine('C',labour",parts",pass",a")
                      If pass"    = False
                          error# = 1
                          Case MessageEx('Cannot Invoice! There is no Default Charge Structure setup.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      Else!If pass"    = False
                          job:labour_cost = labour"
                          job:parts_cost  = parts"
                          job:sub_total   = job:labour_cost + job:parts_cost + job:courier_cost
                      End!If pass"    = False
                  End!If job:ignore_chargeable_charges = 'YES'
              End!If error# = 0 And job:ignore_chargeable_charges = 'YES'
      
              IF error# = 0
                  access:subtracc.clearkey(sub:account_number_key)
                  sub:account_number = job:account_number
                  if access:subtracc.fetch(sub:account_number_key)
                      error# = 1
                      Case MessageEx('Cannot Invoice! Cannot find the Sub Account Details.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
                      access:tradeacc.clearkey(tra:account_number_key)
                      tra:account_number = sub:main_account_number
                      if access:tradeacc.fetch(tra:account_number_key)
                          error# = 1
                          Case MessageEx('Cannot Invoice! Cannot find the Trade Account Details.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      Else!if access:tradeacc.fetch(tra:account_number_key)
                          IF tra:invoice_sub_accounts = 'YES'
                              access:vatcode.clearkey(vat:vat_code_key)
                              vat:vat_code = sub:labour_vat_code
                              if access:vatcode.fetch(vat:vat_code_key)
                                  Case MessageEx('Cannot Invoice! A V.A.T. Rate has not been setup for this Sub Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                                  error# = 1
                              end!if access:vatcode.fetch(vat:vat_code_key)
                              If error# = 0
                                  access:vatcode.clearkey(vat:vat_code_key)
                                  vat:vat_code = sub:parts_vat_code
                                  if access:vatcode.fetch(vat:vat_code_key)
                                      error# = 1
                                      Case MessageEx('Cannot Invoice! A V.A.T. Rate has not been setup for this Trade Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                  end!if access:vatcode.fetch(vat:vat_code_key)
                              End!If error# = 0
                          Else!IF tra:invoice_sub_accounts = 'YES'
                              access:vatcode.clearkey(vat:vat_code_key)
                              vat:vat_code = tra:labour_vat_code
                              if access:vatcode.fetch(vat:vat_code_key)
                                  Case MessageEx('Cannot Invoice! A V.A.T. Rate has not been setup for this Sub Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                                  error# = 1
                              end!if access:vatcode.fetch(vat:vat_code_key)
                              If error# = 0
                                  access:vatcode.clearkey(vat:vat_code_key)
                                  vat:vat_code = tra:parts_vat_code
                                  if access:vatcode.fetch(vat:vat_code_key)
                                      error# = 1
                                      Case MessageEx('Cannot Invoice! A V.A.T. Rate has not been setup for this Trade Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                  end!if access:vatcode.fetch(vat:vat_code_key)
                              End!If error# = 0
                          End!IF tra:invoice_sub_accounts = 'YES'
                      End!if access:tradeacc.fetch(tra:account_number_key)
                  End!Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
              End!IF error# = 0
              IF error# = 0
                  tmp:CreateInvoice   = 1
                  Case MessageEx('An Invoice will be created with you complete editing this job.','ServiceBase 2000',|
                                 'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              End!IF error# = 0
      
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Invoice, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Start - Make sure the job is repriced on the way out - TrkBs: 5047 (DBH: 06-12-2004)
  Do Pricing
  !End   - Make sure the job is repriced on the way out - TrkBs: 5047 (DBH: 06-12-2004)
  Access:JOBNOTES.Update()
  jobe:Network    = tmp:Network
  jobe:SkillLevel= tmp:JobSkillLevel
  Access:JOBSE.Update()
  If job:Date_Completed <> ''
      CompulsoryFieldCheck('C')
  Else!If job:Date_Completed <> ''
      CompulsoryFieldCheck('B')
  End!If job:Date_Completed <> ''
  
  If glo:ErrorText <> ''
      glo:errortext = 'You cannot complete editing due to the following error(s): <13,10>' & Clip(glo:errortext)
      Error_Text
      glo:errortext = ''
      !thismakeover.SetWindow(win:form)
      Cycle
  Else!If error# = 
      ! Start Change 1744 BE(25/04/03)
      IF (MakeEstimateJobFlag = 0) THEN
      ! End Change 1744 BE(25/04/03)
          If job:date_completed = '' and Sub(job:Current_Status,1,3) <> '605'
              Case MessageEx('Warning! You have NOT marked this Repair as Completed.<13,10><13,10>Do you wish to go back and Completed it?','ServiceBase 2000',|
                         'Styles\warn.ico','|&Yes|&No',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      Cycle
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If job:date_completed = ''
      ! Start Change 1744 BE(25/04/03)
      END
      ! End Change 1744 BE(25/04/03)
  End
  
  !Did Anything Change?
  Include('JobChnge.inc','Audit')
  
  saved_esn_temp = job:esn
  saved_Ref_number_temp = job:Ref_Number
  
  
  Access:JOBNOTES.Update()
  
  !Check Accesories incase they change
  Free(SaveAccessoryQueue)
  Clear(SaveAccessoryQueue)
  
  Open(SaveAccessory)
  If ~Error()
      Save_jac_ID = Access:JOBACC.SaveFile()
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
             Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          Clear(savacc:Record)
          savacc:Accessory    = jac:Accessory
          Get(SaveAccessory,savacc:AccessoryKey)
          If Error()
              !New Accessory Added
              savaccq:Accessory   = jac:Accessory
              savaccq:Type        = 1
              Add(SaveAccessoryQueue)
          End !If Error()
      End !Loop
      Access:JOBACC.RestoreFile(Save_jac_ID)
  
      Clear(savacc:record)
      Set(SaveAccessory,0)
      Loop
          Next(SaveAccessory)
          If Error()
              Break
          End !If Error()
          Access:JOBACC.ClearKey(jac:Ref_Number_Key)
          jac:Ref_Number = job:Ref_Number
          jac:Accessory  = savacc:Accessory
          If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
              !Found
  
          Else!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              !Accessory Deleted
              savaccq:Accessory   = savacc:Accessory
              savaccq:Type        = 0
              Add(SaveAccessoryQueue)
          End!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
      End !Loop
  
      If Records(SaveAccessoryQueue)
          !Some accessories have been changed
          If Access:AUDIT.PrimeRecord() = Level:Benign
              Sort(SaveAccessoryQueue,savaccq:Accessory,savaccq:Type)
              Loop x# = 1 To Records(SaveAccessoryQueue)
                  Get(SaveAccessoryQueue,x#)
                  aud:Notes   = Clip(aud:Notes) & '<13,10>' & Clip(savaccq:Accessory)
                   Case savaccq:Type
                      Of 0
                          aud:Notes = Clip(aud:Notes) & ' -DELETED'
                      Of 1
                          aud:Notes = Clip(aud:Notes) & ' -ADDED'
                   End !Case savaccq:Type
              End !Loop x# = 1 To Records(SaveAccessoryQueue)
  
              !Cut out the line break at the beginning
              aud:Notes         = Sub(aud:Notes,3,Len(aud:notes))
              aud:Ref_Number    = job:ref_number
              aud:Date          = Today()
              aud:Time          = Clock()
              aud:Type          = 'JOB'
              Access:USERS.ClearKey(use:Password_Key)
              use:Password      = glo:Password
              Access:USERS.Fetch(use:Password_Key)
              aud:User          = use:User_Code
              aud:Action        = 'JOB ACCESSORIES AMENDED'
              Access:AUDIT.Insert()
          End!If Access:AUDIT.PrimeRecord() = Level:Benign
      End !If Records(SaveAccessoryQueue)
      Free(SaveAccessoryQueue)
  End !Error()
  Close(SaveAccessory)
  Remove(SaveAccessory)
  
  
  ReturnValue = PARENT.TakeCompleted()
  !!Do the automatic despatch
  !If tmp:DespatchClose    = 1
  !    despatch# = 1
  !    access:subtracc.clearkey(sub:account_number_key)
  !    sub:account_number  = job:account_number
  !    If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
  !        access:tradeacc.clearkey(tra:account_number_key)
  !        tra:account_number  = sub:main_account_number
  !        If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
  !            If tra:skip_despatch = 'YES'
  !                despatch# = 0
  !            End!If tra:skip_despatch = 'YES'
  !        End!If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
  !    End!If access;subtracc.tryfetch(sub:account_number_key) = Level:Benign
  !    If despatch# = 1
  !        access:courier.clearkey(cou:courier_key)
  !        cou:courier = job:courier
  !        if access:courier.tryfetch(cou:courier_key)
  !            Case MessageEx('Cannot find the Courier attached to this job.','ServiceBase 2000',|
  !                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                Of 1 ! &OK Button
  !            End!Case MessageEx
  !        Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
  !            If access:desbatch.primerecord() = Level:Benign
  !                If access:desbatch.tryinsert()
  !                    access:desbatch.cancelautoinc()
  !                Else!If access:despatch.tryinsert()
  !                    Include('despsing.inc')
  !                End!If access:despatch.tryinsert()
  !            End!If access:desbatch.primerecord() = Level:Benign
  !        End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
  !    End!If despatch# = 1
  !End!If tmp:DespatchClose    = 1
  !
  !If tmp:RestockingNote = 1
  !    glo:Select1 = job:ref_number
  !    restocking_note
  !    glo:Select1 = ''
  !End!If tmp:RestockingNote = 1
  !If print_despatch_note_temp = 'YES' and tmp:RestockingNote <> 1
  !!    
  !!    restock# = 0
  !    glo:select1  = job:ref_number
  !!    If ToBeExchanged()
  !!    !Has this unit got an Exchange, or is it expecting an exchange
  !!        restock# = 1
  !!    Else!If job:exchange_unit_number
  !!        !Does the transit type expect an exchange?
  !!        access:trantype.clearkey(trt:transit_type_key)
  !!        trt:transit_type = job:transit_type
  !!        if access:trantype.tryfetch(trt:transit_type_key) = Level:benign
  !!            if trt:exchange_unit = 'YES'
  !!                restock# = 1
  !!            End!if trt:exchange_unit = 'YES'
  !!        End!if access:trantype.tryfetch(trt:transit_type_key) = Level:benign
  !!    End!If job:exchange_unit_number <> ''
  !!    !If there is an loan unit attached, assume that a despatch note is required.
  !!    If job:loan_unit_number <> ''
  !!!        If job:despatched <> 'LAT'
  !!!            restock# = 1
  !!!        Else!If job:despatched <> 'LAT'
  !!            restock# = 0
  !!!        End!If job:despatched <> 'LAT'
  !!    End!If job:loan_unit_number <> ''
  !!
  !!    If restock# = 1
  !!        restocking_note
  !!    Else!If restock# = 1
  !        despatch_note
  !!    End!If restock# = 1
  !    glo:select1  = ''
  !End!If print_despatch_note_temp = 'YES'
  !!
  !!
  !Create Invoice
  If tmp:CreateInvoice = 1
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      if access:subtracc.fetch(sub:account_number_key) = Level:Benign
          access:tradeacc.clearkey(tra:account_number_key) 
          tra:account_number = sub:main_account_number
          if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
              If tra:use_sub_accounts = 'YES'
                  If sub:despatch_invoiced_jobs = 'YES'
                      If sub:despatch_paid_jobs = 'YES'
                          If job:paid = 'YES'
                              despatch# = 1
                          Else
                              despatch# = 2
                          End
                      Else
                          despatch# = 1
                      End!If sub:despatch_paid_jobs = 'YES' And job:paid = 'YES'
                      
                  End!If sub:despatch_invoiced_jobs = 'YES'
              End!If tra:use_sub_accounts = 'YES'
              if tra:invoice_sub_accounts = 'YES'
                  vat_number" = sub:vat_number
                  access:vatcode.clearkey(vat:vat_code_key)
                  vat:vat_code = sub:labour_vat_code
                  if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                     labour_rate$ = vat:vat_rate
                  end!if access:vatcode.fetch(vat:vat_code_key)
                  access:vatcode.clearkey(vat:vat_code_key)
                  vat:vat_code = sub:parts_vat_code
                  if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                     parts_rate$ = vat:vat_rate
                  end!if access:vatcode.fetch(vat:vat_code_key)
              else!if tra:use_sub_accounts = 'YES'
                  If tra:despatch_invoiced_jobs = 'YES'
                      If tra:despatch_paid_jobs = 'YES'
                          If job:paid = 'YES'
                              despatch# = 1
                          Else
                              despatch# = 2
                          End!If job:paid = 'YES'
                      Else
                          despatch# = 1
                      End!If tra:despatch_paid_jobs = 'YES' and job:paid = 'YES'
                  End!If tra:despatch_invoiced_jobs = 'YES'
                  vat_number" = tra:vat_number
                  access:vatcode.clearkey(vat:vat_code_key)
                  vat:vat_code = tra:labour_vat_code
                  if access:vatcode.fetch(vat:vat_code_key) = Level:benign
                     labour_rate$ = vat:vat_rate
                  end
                  access:vatcode.clearkey(vat:vat_code_key)
                  vat:vat_code = tra:parts_vat_code
                  if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                     parts_rate$ = vat:vat_rate
                  end
              end!if tra:use_sub_accounts = 'YES'
          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
      end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
  !SAGE BIT
      If def:use_sage = 'YES'
          glo:file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
          Remove(paramss)
          access:paramss.open()
          access:paramss.usefile()
  !LABOUR
          get(paramss,0)
          if access:paramss.primerecord() = Level:Benign
              prm:account_ref       = Stripcomma(job:Account_number)
              If tra:invoice_sub_accounts = 'YES'
                  If sub:invoice_customer_address <> 'YES'
                      prm:name              = Stripcomma(sub:company_name)
                      prm:address_1         = Stripcomma(sub:address_line1)
                      prm:address_2         = Stripcomma(sub:address_line2)
                      prm:address_3         = Stripcomma(sub:address_line3)
                      prm:address_5         = Stripcomma(sub:postcode)
                  Else!If tra:invoice_customer_address = 'YES'
                      prm:name              = Stripcomma(job:company_name)
                      prm:address_1         = Stripcomma(job:address_line1)
                      prm:address_2         = Stripcomma(job:address_line2)
                      prm:address_3         = Stripcomma(job:address_line3)
                      prm:address_5         = Stripcomma(job:postcode)
  
                  End!If tra:invoice_customer_address = 'YES'
              Else!If tra:invoice_sub_accounts = 'YES'
                  If tra:invoice_customer_address <> 'YES'
                      prm:name              = Stripcomma(tra:company_name)
                      prm:address_1         = Stripcomma(tra:address_line1)
                      prm:address_2         = Stripcomma(tra:address_line2)
                      prm:address_3         = Stripcomma(tra:address_line3)
                      prm:address_5         = Stripcomma(tra:postcode)
                  Else!If tra:invoice_customer_address = 'YES'
                      prm:name              = Stripcomma(job:company_name)
                      prm:address_1         = Stripcomma(job:address_line1)
                      prm:address_2         = Stripcomma(job:address_line2)
                      prm:address_3         = Stripcomma(job:address_line3)
                      prm:address_5         = Stripcomma(job:postcode)
                  End!If tra:invoice_customer_address = 'YES'
              End!If tra:invoice_sub_accounts = 'YES'
              prm:address_4         = ''
              prm:del_address_1     = Stripcomma(job:address_line1_delivery)
              prm:del_address_2     = Stripcomma(job:address_line2_delivery)
              prm:del_address_3     = Stripcomma(job:address_line3_delivery)
              prm:del_address_4     = ''
              prm:del_address_5     = Stripcomma(job:postcode_delivery)
              prm:cust_tel_number   = Stripcomma(job:telephone_number)
              If job:surname <> ''
                  if job:title = '' and job:initial = '' 
                      prm:contact_name = Stripcomma(clip(job:surname))
                  elsif job:title = '' and job:initial <> ''
                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                  elsif job:title <> '' and job:initial = ''
                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                  elsif job:title <> '' and job:initial <> ''
                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                  else
                      prm:contact_name = ''
                  end
              else
                  prm:contact_name = ''
              end
              prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
              prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
              prm:notes_3           = ''
              prm:taken_by          = Stripcomma(job:who_booked)
              prm:order_number      = Stripcomma(job:order_number)
              prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
              prm:payment_ref       = ''
              prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
              prm:global_details    = ''
              prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
              prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                          Round(job:parts_cost * (parts_rate$/100),.01) + |
                                          Round(job:courier_cost * (labour_rate$/100),.01))
              prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
              prm:description       = Stripcomma(DEF:Labour_Description)
              prm:nominal_code      = Stripcomma(DEF:Labour_Code)
              prm:qty_order         = '1'
              prm:unit_price        = '0'
              prm:net_amount        = Stripcomma(job:labour_cost)
              prm:tax_amount        = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01))
              prm:comment_1         = Stripcomma(job:charge_type)
              prm:comment_2         = Stripcomma(job:warranty_charge_type)
              prm:unit_of_sale      = '0'
              prm:full_net_amount   = '0'
              prm:invoice_date      = '*'
              prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
              prm:user_name         = Clip(DEF:User_Name_Sage)
              prm:password          = Clip(DEF:Password_Sage)
              prm:set_invoice_number= '*'
              prm:invoice_no        = '*'
              access:paramss.insert()
          end!if access:paramss.primerecord() = Level:Benign
  !PARTS
          get(paramss,0)
          if access:paramss.primerecord() = Level:Benign
              prm:account_ref       = Stripcomma(job:Account_number)
              If tra:invoice_sub_accounts = 'YES'
                  If sub:invoice_customer_address <> 'YES'
                      prm:name              = Stripcomma(sub:company_name)
                      prm:address_1         = Stripcomma(sub:address_line1)
                      prm:address_2         = Stripcomma(sub:address_line2)
                      prm:address_3         = Stripcomma(sub:address_line3)
                      prm:address_5         = Stripcomma(sub:postcode)
                  Else!If tra:invoice_customer_address = 'YES'
                      prm:name              = Stripcomma(job:company_name)
                      prm:address_1         = Stripcomma(job:address_line1)
                      prm:address_2         = Stripcomma(job:address_line2)
                      prm:address_3         = Stripcomma(job:address_line3)
                      prm:address_5         = Stripcomma(job:postcode)
  
                  End!If tra:invoice_customer_address = 'YES'
              Else!If tra:invoice_sub_accounts = 'YES'
                  If tra:invoice_customer_address <> 'YES'
                      prm:name              = Stripcomma(tra:company_name)
                      prm:address_1         = Stripcomma(tra:address_line1)
                      prm:address_2         = Stripcomma(tra:address_line2)
                      prm:address_3         = Stripcomma(tra:address_line3)
                      prm:address_5         = Stripcomma(tra:postcode)
                  Else!If tra:invoice_customer_address = 'YES'
                      prm:name              = Stripcomma(job:company_name)
                      prm:address_1         = Stripcomma(job:address_line1)
                      prm:address_2         = Stripcomma(job:address_line2)
                      prm:address_3         = Stripcomma(job:address_line3)
                      prm:address_5         = Stripcomma(job:postcode)
                  End!If tra:invoice_customer_address = 'YES'
              End!If tra:invoice_sub_accounts = 'YES'
              prm:address_4         = ''
              prm:del_address_1     = Stripcomma(job:address_line1_delivery)
              prm:del_address_2     = Stripcomma(job:address_line2_delivery)
              prm:del_address_3     = Stripcomma(job:address_line3_delivery)
              prm:del_address_4     = ''
              prm:del_address_5     = Stripcomma(job:postcode_delivery)
              prm:cust_tel_number   = Stripcomma(job:telephone_number)
              If job:surname <> ''
                  if job:title = '' and job:initial = '' 
                      prm:contact_name = Stripcomma(clip(job:surname))
                  elsif job:title = '' and job:initial <> ''
                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                  elsif job:title <> '' and job:initial = ''
                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                  elsif job:title <> '' and job:initial <> ''
                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                  else
                      prm:contact_name = ''
                  end
              else
                  prm:contact_name = ''
              end
              prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
              prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
              prm:notes_3           = ''
              prm:taken_by          = Stripcomma(job:who_booked)
              prm:order_number      = Stripcomma(job:order_number)
              prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
              prm:payment_ref       = ''
              prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
              prm:global_details    = ''
              prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
              prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                          Round(job:parts_cost * (parts_rate$/100),.01) + |
                                          Round(job:courier_cost * (labour_rate$/100),.01))
              prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
              prm:description       = Stripcomma(DEF:Parts_Description)
              prm:nominal_code      = Stripcomma(DEF:Parts_Code)
              prm:qty_order         = '1'
              prm:unit_price        = '0'
              prm:net_amount        = Stripcomma(job:parts_cost)
              prm:tax_amount        = Stripcomma(Round(job:parts_cost * (parts_rate$/100),.01))
              prm:comment_1         = Stripcomma(job:charge_type)
              prm:comment_2         = Stripcomma(job:warranty_charge_type)
              prm:unit_of_sale      = '0'
              prm:full_net_amount   = '0'
              prm:invoice_date      = '*'
              prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
              prm:user_name         = Clip(DEF:User_Name_Sage)
              prm:password          = Clip(DEF:Password_Sage)
              prm:set_invoice_number= '*'
              prm:invoice_no        = '*'
              access:paramss.insert()
          end!if access:paramss.primerecord() = Level:Benign
  !COURIER
          get(paramss,0)
          if access:paramss.primerecord() = Level:Benign
              prm:account_ref       = Stripcomma(job:Account_number)
              If tra:invoice_sub_accounts = 'YES'
                  If sub:invoice_customer_address <> 'YES'
                      prm:name              = Stripcomma(sub:company_name)
                      prm:address_1         = Stripcomma(sub:address_line1)
                      prm:address_2         = Stripcomma(sub:address_line2)
                      prm:address_3         = Stripcomma(sub:address_line3)
                      prm:address_5         = Stripcomma(sub:postcode)
                  Else!If tra:invoice_customer_address = 'YES'
                      prm:name              = Stripcomma(job:company_name)
                      prm:address_1         = Stripcomma(job:address_line1)
                      prm:address_2         = Stripcomma(job:address_line2)
                      prm:address_3         = Stripcomma(job:address_line3)
                      prm:address_5         = Stripcomma(job:postcode)
  
                  End!If tra:invoice_customer_address = 'YES'
              Else!If tra:invoice_sub_accounts = 'YES'
                  If tra:invoice_customer_address <> 'YES'
                      prm:name              = Stripcomma(tra:company_name)
                      prm:address_1         = Stripcomma(tra:address_line1)
                      prm:address_2         = Stripcomma(tra:address_line2)
                      prm:address_3         = Stripcomma(tra:address_line3)
                      prm:address_5         = Stripcomma(tra:postcode)
                  Else!If tra:invoice_customer_address = 'YES'
                      prm:name              = Stripcomma(job:company_name)
                      prm:address_1         = Stripcomma(job:address_line1)
                      prm:address_2         = Stripcomma(job:address_line2)
                      prm:address_3         = Stripcomma(job:address_line3)
                      prm:address_5         = Stripcomma(job:postcode)
                  End!If tra:invoice_customer_address = 'YES'
              End!If tra:invoice_sub_accounts = 'YES'
  
              prm:address_4         = ''
              prm:del_address_1     = Stripcomma(job:address_line1_delivery)
              prm:del_address_2     = Stripcomma(job:address_line2_delivery)
              prm:del_address_3     = Stripcomma(job:address_line3_delivery)
              prm:del_address_4     = ''
              prm:del_address_5     = Stripcomma(job:postcode_delivery)
              prm:cust_tel_number   = Stripcomma(job:telephone_number)
              If job:surname <> ''
                  if job:title = '' and job:initial = '' 
                      prm:contact_name = Stripcomma(clip(job:surname))
                  elsif job:title = '' and job:initial <> ''
                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                  elsif job:title <> '' and job:initial = ''
                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                  elsif job:title <> '' and job:initial <> ''
                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                  else
                      prm:contact_name = ''
                  end
              else
                  prm:contact_name = ''
              end
              prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
              prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
              prm:notes_3           = ''
              prm:taken_by          = Stripcomma(job:who_booked)
              prm:order_number      = Stripcomma(job:order_number)
              prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
              prm:payment_ref       = ''
              prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
              prm:global_details    = ''
              prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
              prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                          Round(job:parts_cost * (parts_rate$/100),.01) + |
                                          Round(job:courier_cost * (labour_rate$/100),.01))
              prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
              prm:description       = Stripcomma(DEF:Courier_Description)
              prm:nominal_code      = Stripcomma(DEF:Courier_Code)
              prm:qty_order         = '1'
              prm:unit_price        = '0'
              prm:net_amount        = Stripcomma(job:courier_cost)
              prm:tax_amount        = Stripcomma(Round(job:courier_cost * (labour_rate$/100),.01))
              prm:comment_1         = Stripcomma(job:charge_type)
              prm:comment_2         = Stripcomma(job:warranty_charge_type)
              prm:unit_of_sale      = '0'
              prm:full_net_amount   = '0'
              prm:invoice_date      = '*'
              prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
              prm:user_name         = Clip(DEF:User_Name_Sage)
              prm:password          = Clip(DEF:Password_Sage)
              prm:set_invoice_number= '*'
              prm:invoice_no        = '*'
              access:paramss.insert()
          end!if access:paramss.primerecord() = Level:Benign
          access:paramss.close()
          Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
          sage_error# = 0
          access:paramss.open()
          access:paramss.usefile()
          Set(paramss,0)
          If access:paramss.next()
              sage_error# = 1
          Else!If access:paramss.next()
              If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
                  sage_error# = 1
              Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
                  invoice_number_temp = prm:invoice_no
                  If invoice_number_temp = 0
                      sage_error# = 1
                  End!If invoice_number_temp = 0
              End!If prm:invoice_no = '-1'
          End!If access:paramss.next()
          access:paramss.close()
      End!If def:use_sage = 'YES'
      If sage_error# = 0
          invoice_error# = 1
          get(invoice,0)
          if access:invoice.primerecord() = level:benign
              If def:use_sage = 'YES'
                  inv:invoice_number = invoice_number_temp
              End!If def:use_sage = 'YES'
              inv:invoice_type       = 'SIN'
              inv:job_number         = job:ref_number
              inv:date_created       = Today()
              inv:account_number     = job:account_number
              If tra:invoice_sub_accounts = 'YES'
                  inv:AccountType = 'SUB'
              Else!If tra:invoice_sub_accounts = 'YES'
                  inv:AccountType = 'MAI'
              End!If tra:invoice_sub_accounts = 'YES'
              inv:total              = job:sub_total
              inv:vat_rate_labour    = labour_rate$
              inv:vat_rate_parts     = parts_rate$
              inv:vat_rate_retail    = labour_rate$
              inv:vat_number         = def:vat_number
              INV:Courier_Paid       = job:courier_cost
              inv:parts_paid         = job:parts_cost
              inv:labour_paid        = job:labour_cost
              inv:invoice_vat_number = vat_number"
              invoice_error# = 0
              If access:invoice.insert()
                  invoice_error# = 1
                  access:invoice.cancelautoinc()
              End!If access:invoice.insert()
          end!if access:invoice.primerecord() = level:benign
          If Invoice_error# = 0
              Case despatch#
                  Of 2
                      GetStatus(803,0,'JOB') !Ready To Despatch
                      ! Start Change 3940 BE(02/03/04)
                      TempStatusCode = job:Current_Status
                      ! End Change 3940 BE(02/03/04)
  
                  Else
                      If job:despatched = 'YES'
                          If job:paid = 'YES'
                              GetStatus(910,0,'JOB') !Despatched Paid
                              ! Start Change 3940 BE(02/03/04)
                              TempStatusCode = job:Current_Status
                              ! End Change 3940 BE(02/03/04)
  
                          Else!If job:paid = 'YES'
                              GetStatus(905,0,'JOB') !Despatch Unpaid
                              ! Start Change 3940 BE(02/03/04)
                              TempStatusCode = job:Current_Status
                              ! End Change 3940 BE(02/03/04)
  
                          End!If job:paid = 'YES'
                      Else!If job:despatched = 'YES'
                          GetStatus(810,0,'JOB') !Ready To Despatch
                          ! Start Change 3940 BE(02/03/04)
                          TempStatusCode = job:Current_Status
                          ! End Change 3940 BE(02/03/04)
  
                          job:despatched = 'REA'
                          job:despatch_type = 'JOB'
                          job:current_courier = job:courier
                      End!If job:despatched <> 'YES'
              End!Case despatch#
  
              job:invoice_number        = inv:invoice_number
              job:invoice_date          = Today()
              job:invoice_courier_cost  = job:courier_cost
              job:invoice_labour_cost   = job:labour_cost
              job:invoice_parts_cost    = job:parts_cost
              job:invoice_sub_total     = job:sub_total
              access:jobs.update()
              get(audit,0)
              if access:audit.primerecord() = level:benign
                  aud:notes         = 'INVOICE NUMBER: ' & inv:invoice_number
                  aud:ref_number    = job:ref_number
                  aud:date          = today()
                  aud:time          = clock()
                  aud:type          = 'JOB'
                  access:users.clearkey(use:password_key)
                  use:password = glo:password
                  access:users.fetch(use:password_key)
                  aud:user = use:user_code
                  aud:action        = 'INVOICE CREATED'
                  access:audit.insert()
              end!�if access:audit.primerecord() = level:benign
              
              glo:select1 = inv:invoice_number
              Single_Invoice
              glo:select1 = ''
          End!If Invoice_error# = 0
      End!If def:use_sage = 'YES' and sage_error# = 1
  
  End!If tmp:CreateInvoice = 1
  
  ! Start Change 4025 BE(28/04/04)
  !!Is this job an estimate, if so change the status
  !If job:Estimate = 'YES'
  !    GetStatus(505,1,'JOB')
  !    access:jobs.update()
  !End !If job:Estimate = 'YES'
  ! End Change 4025 BE(28/04/04)
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Jobs_Rapid')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:Network
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Network, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Networks')
             Post(Event:Accepted,?LookupNetwork)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupNetwork)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Network, AlertKey)
    END
  OF ?job:Repair_Type
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Repair_Type, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Repair Type')
             Post(Event:Accepted,?LookupChargeableRepairType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupChargeableRepairType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Repair_Type, AlertKey)
    END
  OF ?job:Repair_Type_Warranty
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Repair_Type_Warranty, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Repair Type')
             Post(Event:Accepted,?LookupWarrantyRepairType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupWarrantyRepairType)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Repair_Type_Warranty, AlertKey)
    END
  OF ?job:Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Location, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Location')
             Post(Event:Accepted,?LookupLocation)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupLocation)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Location, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F3Key
              Post(Event:Accepted,?AmendUnitType)
          Of F4Key
              If ?AllocateJob{prop:Disable} = 0
                  Post(Event:Accepted,?AllocateJob)
              End !If ?AllocateJob{prop:Disable} = 0
          Of F5Key
              Post(Event:Accepted,?Engineers_Notes)
          Of F9Key
              Post(Event:Accepted,?ViewAccessories)
          Of F7Key
              Post(Event:Accepted,?fault_Codes)
          Of F6Key
              Post(Event:Accepted,?Common_Faults)
          Of F8Key
              Post(Event:Accepted,?ChangeJobStatus)
          Of F10Key
              Post(Event:Accepted,?CompleteButton)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Set(defaults)
      access:defaults.next()
      
      DO enable_Disable
      Do lookup_engineer
      Do qa_group
      
      warranty_job_temp   = job:warranty_job
      chargeable_job_temp = job:chargeable_job
      date_completed_temp = job:Date_Completed
      time_completed_temp = job:Time_Completed
      engineer_temp       = job:Engineer
      
      !Is QA Rejection, then get and display the QA rejection reason from the AUDIT trail
      If job:qa_rejected = 'YES'
          setcursor(cursor:wait)
          save_aud_id = access:audit.savefile()
          access:audit.clearkey(aud:action_key)
          aud:ref_number = job:ref_number
          aud:action     = 'QA REJECTION'
          set(aud:action_key,aud:action_key)
          loop
              !omemo(audit)
              if access:audit.next()
                 break
              end !if
              if aud:ref_number <> job:ref_number      |
              or aud:action     <> 'QA REJECTION'      |
                  then break.  ! end if
              Unhide(?qa_rejection_reason_temp:prompt)
              Unhide(?qa_rejection_reason_temp)
              qa_rejection_reason_temp = aud:notes
              Break
          end !loop
          access:audit.restorefile(save_aud_id)
          setcursor()
      End!If job:qa_rejected = 'YES'
      
      despatch_by_text"   = ''
      access:courier.clearkey(cou:courier_key)
      cou:courier = job:courier
      if access:courier.tryfetch(cou:courier_key) = Level:Benign
         If cou:courier_Type = 'ANC' and Clock() > cou:last_despatch_time
              despatch_by_text" = 'DESPATCH TOMORROW!'
         End!If cou:courier_Type = 'ANC' and Clock() > cou:last_despatch_time
      end!if access:courier.tryfetch(cou:courier_key) = Level:Benign
      
      exchange_loan_temp = ''
      If job:exchange_unit_number <> ''
          exchange_loan_temp = 'This job has an EXCHANGE unit attached. ' 
      Elsif job:loan_unit_number <> ''
          exchange_loan_temp = 'This job has a LOAN unit attached!' & Clip(despath_by_text")
      Else
          exchange_loan_temp = despatch_by_text"
      End!If job:loan_unit_number <> ''
      
      If SecurityCheck('JOBS - SHOW COSTS')
          ?costs{prop:Hide} = 1
          ?Invoice{prop:Hide} = 1
      End !SecurityCheck('JOBS - COSTS')
      
      ! Start Change 1744 BE(15/04/03)
      IF (glo:select20 <> 'UPDATE_ENGINEER_JOB') THEN
          ?MakeEstimateJob{prop:Hide} = 1
          ?MakeEstimateJobPrompt{prop:Hide} = 1
      ELSE
          IF (SecurityCheck('JOBS - SHOW MAKE ESTIMATE') <> Level:Benign) THEN
              ?MakeEstimateJob{prop:Hide} = 1
              ?MakeEstimateJobPrompt{prop:Hide} = 1
          END
      END
      ! End Change 1744 BE(15/04/03)
      
      Display()
      !Cancelled?
      If job:cancelled = 'YES'
          Hide(?ok)
          Unhide(?canceltext)
          ?CancelText{prop:FontColor} = color:red
      Else !If job:cancelled = 'YES'
          If jobe:FailedDelivery
              ?CancelText{prop:Hide} = 0
              ?CancelText{prop:Text} = 'FAILED DELIVERY'
              ?CancelText{prop:FontColor} = color:green
          End !If jobe:FailedDelivery
      End!If job:cancelled = 'YES'
      
      !thismakeover.refresh()
        IF GetComputerName(JB:ComputerName,JB:ComputerLen).
        tmp:WorkStationName=JB:ComputerName
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      Do fill_lists
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
    Code

    local:ErrorCode  = 0
    If AccessoryCheck('JOB')
        If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 1
        Else!If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 2
        End !If AccessoryMismatch(job:Ref_Number,'JOB')
    End !If AccessoryCheck('JOB')
    If Access:AUDIT.PrimeRecord() = Level:Benign

        Case local:Errorcode
            Of 0 Orof 2
                aud:Notes         = 'ACCESSORIES:<13,10>'
            Of 1
                aud:Notes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
        End !Case local:Errorcode

        !Add the list of accessories to the Audit Trail
        If job:Despatch_Type <> 'LOAN'
            Save_jac_ID = Access:JOBACC.SaveFile()
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = job:Ref_Number
            Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
            Loop
                If Access:JOBACC.NEXT()
                   Break
                End !If
                If jac:Ref_Number <> job:Ref_Number      |
                    Then Break.  ! End If
                aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(jac:Accessory)
            End !Loop
            Access:JOBACC.RestoreFile(Save_jac_ID)
        Else !If job:Despatch_Type <> 'LOAN'
            Save_lac_ID = Access:LOANACC.SaveFile()
            Access:LOANACC.ClearKey(lac:Ref_Number_Key)
            lac:Ref_Number = job:Ref_Number
            Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
            Loop
                If Access:LOANACC.NEXT()
                   Break
                End !If
                If lac:Ref_Number <> job:Ref_Number      |
                    Then Break.  ! End If
                aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(lac:Accessory)
            End !Loop
            Access:LOANACC.RestoreFile(Save_lac_ID)
        End !If job:Despatch_Type <> 'LOAN'

        Case local:Errorcode
            Of 1
                !If Error show the Accessories that were actually tagged
                aud:Notes         = Clip(aud:Notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
                Clear(glo:Queue)
                Loop x# = 1 To Records(glo:Queue)
                    Get(glo:Queue,x#)
                    aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(glo:Pointer)
                End !Loop x# = 1 To Records(glo:Queue).
        End!Case local:Errorcode


        aud:Ref_Number    = job:ref_number
        aud:Date          = Today()
        aud:Time          = Clock()
        aud:Type          = 'JOB'
        Access:USERS.ClearKey(use:Password_Key)
        use:Password      = glo:Password
        Access:USERS.Fetch(use:Password_Key)
        aud:User          = use:User_Code
        Case local:ErrorCode
            Of 0
                aud:Action        = 'COMPLETION VALIDATION: PASSED ACCESSORIES'
            Of 1
                aud:Action        = 'COMPLETION VALIDATION: FAILED ACCESSORIES'
            Of 2
                aud:Action        = 'COMPLETION VALIDATION: PASSED ACCESSORIES (FORCED)'
        End !Case local:ErrorCode
        
        Access:AUDIT.Insert()

        !Do not complete
        If local:ErrorCode = 1
            Return Level:Fatal
        End !If local:ErrorCode = 1
    End!If Access:AUDIT.PrimeRecord() = Level:Benign

    Return Level:Benign
UpdateRFBoardIMEI PROCEDURE (LONG partrefnum, STRING partnum)
    code
    ! Start Change 2116 BE(26/06/03)
    access:stock.clearkey(sto:ref_number_key)
    sto:ref_number = partrefnum
    IF (access:stock.fetch(sto:ref_number_key) = Level:benign) THEN
        IF (sto:RF_BOARD) THEN
            glo:select2 = ''
            glo:select3 = job:model_number
            glo:select4 = partnum
            SETCURSOR()
            EnterNewIMEI
            SETCURSOR(cursor:wait)
            tmp_RF_Board_IMEI = CLIP(glo:select2)
            glo:select2 = ''
            glo:select3 = ''
            glo:select4 = ''
            IF (tmp_RF_Board_IMEI = '') THEN
                RETURN
            END
            GET(audit,0)
            IF (access:audit.primerecord() = level:benign) THEN
                aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                     '<13,10>Old IMEI ' & Clip(job:esn)
                aud:ref_number    = job:ref_number
                aud:date          = Today()
                aud:time          = Clock()
                access:users.clearkey(use:password_key)
                use:password =glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                IF (access:audit.insert() <> Level:benign) THEN
                    access:audit.cancelautoinc()
                END
            END
            access:JOBSE.clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                IF (access:JOBSE.primerecord() = Level:Benign) THEN
                    jobe:RefNumber  = job:Ref_Number
                    IF (access:JOBSE.tryinsert()) THEN
                        access:JOBSE.cancelautoinc()
                    END
                END
            END
            IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                jobe:PRE_RF_BOARD_IMEI = job:ESN
                access:jobse.update()
            END
            job:ESN = tmp_RF_Board_IMEI
            access:jobs.update()
        END
    END
    ! End Change 2116 BE(26/06/03)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.par:Part_Number_NormalFG = -1
  SELF.Q.par:Part_Number_NormalBG = -1
  SELF.Q.par:Part_Number_SelectedFG = -1
  SELF.Q.par:Part_Number_SelectedBG = -1
  SELF.Q.par:Description_NormalFG = -1
  SELF.Q.par:Description_NormalBG = -1
  SELF.Q.par:Description_SelectedFG = -1
  SELF.Q.par:Description_SelectedBG = -1
  SELF.Q.par:Quantity_NormalFG = -1
  SELF.Q.par:Quantity_NormalBG = -1
  SELF.Q.par:Quantity_SelectedFG = -1
  SELF.Q.par:Quantity_SelectedBG = -1
   
   
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Part_Number_NormalFG = 32768
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Part_Number_NormalFG = 255
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Part_Number_NormalFG = 8421504
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Part_Number_NormalFG = -1
     SELF.Q.par:Part_Number_NormalBG = -1
     SELF.Q.par:Part_Number_SelectedFG = -1
     SELF.Q.par:Part_Number_SelectedBG = -1
   END
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Description_NormalFG = 32768
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Description_NormalFG = 255
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Description_NormalFG = 8421504
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Description_NormalFG = -1
     SELF.Q.par:Description_NormalBG = -1
     SELF.Q.par:Description_SelectedFG = -1
     SELF.Q.par:Description_SelectedBG = -1
   END
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Quantity_NormalFG = 32768
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Quantity_NormalFG = 255
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Quantity_NormalFG = 8421504
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Quantity_NormalFG = -1
     SELF.Q.par:Quantity_NormalBG = -1
     SELF.Q.par:Quantity_SelectedFG = -1
     SELF.Q.par:Quantity_SelectedBG = -1
   END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW10.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.wpr:Part_Number_NormalFG = -1
  SELF.Q.wpr:Part_Number_NormalBG = -1
  SELF.Q.wpr:Part_Number_SelectedFG = -1
  SELF.Q.wpr:Part_Number_SelectedBG = -1
  SELF.Q.wpr:Description_NormalFG = -1
  SELF.Q.wpr:Description_NormalBG = -1
  SELF.Q.wpr:Description_SelectedFG = -1
  SELF.Q.wpr:Description_SelectedBG = -1
  SELF.Q.wpr:Quantity_NormalFG = -1
  SELF.Q.wpr:Quantity_NormalBG = -1
  SELF.Q.wpr:Quantity_SelectedFG = -1
  SELF.Q.wpr:Quantity_SelectedBG = -1
   
   
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Part_Number_NormalFG = 32768
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Part_Number_NormalFG = 255
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Part_Number_NormalFG = 8421504
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Part_Number_NormalFG = -1
     SELF.Q.wpr:Part_Number_NormalBG = -1
     SELF.Q.wpr:Part_Number_SelectedFG = -1
     SELF.Q.wpr:Part_Number_SelectedBG = -1
   END
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Description_NormalFG = 32768
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Description_NormalFG = 255
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Description_NormalFG = 8421504
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Description_NormalFG = -1
     SELF.Q.wpr:Description_NormalBG = -1
     SELF.Q.wpr:Description_SelectedFG = -1
     SELF.Q.wpr:Description_SelectedBG = -1
   END
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Quantity_NormalFG = 32768
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Quantity_NormalFG = 255
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Quantity_NormalFG = 8421504
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Quantity_NormalFG = -1
     SELF.Q.wpr:Quantity_NormalBG = -1
     SELF.Q.wpr:Quantity_SelectedFG = -1
     SELF.Q.wpr:Quantity_SelectedBG = -1
   END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
