

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01016.INC'),ONCE        !Local module procedure declarations
                     END


UpdateJOBPAYMT PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?jpt:Payment_Type
pay:Payment_Type       LIKE(pay:Payment_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(PAYTYPES)
                       PROJECT(pay:Payment_Type)
                     END
History::jpt:Record  LIKE(jpt:RECORD),STATIC
QuickWindow          WINDOW('Update the JOBPAYMT File'),AT(,,220,132),FONT('Tahoma',8,,),CENTER,IMM,HLP('UpdateJOBPAYMT'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,96),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Payment Type'),AT(8,36),USE(?jpt:payment_type:prompt)
                           COMBO(@s30),AT(84,36,124,10),USE(jpt:Payment_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Date'),AT(8,20),USE(?JPT:Date:Prompt),TRN
                           ENTRY(@d6b),AT(84,20,64,10),USE(jpt:Date),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           BUTTON,AT(152,20,10,10),USE(?PopCalendar),ICON('Calenda2.ico')
                           PROMPT('Payment Received'),AT(8,84),USE(?JPT:Amount:Prompt),TRN
                           GROUP,AT(4,48,208,36),USE(?Credit_Card_Group)
                             PROMPT('Credit Card Number'),AT(8,52),USE(?JPT:Credit_Card_Number:Prompt)
                             ENTRY(@s20),AT(84,52,124,10),USE(jpt:Credit_Card_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Expiry Date'),AT(8,68),USE(?JPT:Expiry_Date:Prompt)
                             ENTRY(@p##/##p),AT(84,68,28,10),USE(jpt:Expiry_Date),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Issue Number'),AT(120,68),USE(?JPT:Issue_Number:Prompt)
                             ENTRY(@s5),AT(168,68,40,10),USE(jpt:Issue_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           END
                           ENTRY(@n-14.2),AT(84,84,64,10),USE(jpt:Amount),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                         END
                       END
                       BUTTON('&OK'),AT(100,108,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,108,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,104,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_jpt_ali_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?jpt:payment_type:prompt{prop:FontColor} = -1
    ?jpt:payment_type:prompt{prop:Color} = 15066597
    If ?jpt:Payment_Type{prop:ReadOnly} = True
        ?jpt:Payment_Type{prop:FontColor} = 65793
        ?jpt:Payment_Type{prop:Color} = 15066597
    Elsif ?jpt:Payment_Type{prop:Req} = True
        ?jpt:Payment_Type{prop:FontColor} = 65793
        ?jpt:Payment_Type{prop:Color} = 8454143
    Else ! If ?jpt:Payment_Type{prop:Req} = True
        ?jpt:Payment_Type{prop:FontColor} = 65793
        ?jpt:Payment_Type{prop:Color} = 16777215
    End ! If ?jpt:Payment_Type{prop:Req} = True
    ?jpt:Payment_Type{prop:Trn} = 0
    ?jpt:Payment_Type{prop:FontStyle} = font:Bold
    ?JPT:Date:Prompt{prop:FontColor} = -1
    ?JPT:Date:Prompt{prop:Color} = 15066597
    If ?jpt:Date{prop:ReadOnly} = True
        ?jpt:Date{prop:FontColor} = 65793
        ?jpt:Date{prop:Color} = 15066597
    Elsif ?jpt:Date{prop:Req} = True
        ?jpt:Date{prop:FontColor} = 65793
        ?jpt:Date{prop:Color} = 8454143
    Else ! If ?jpt:Date{prop:Req} = True
        ?jpt:Date{prop:FontColor} = 65793
        ?jpt:Date{prop:Color} = 16777215
    End ! If ?jpt:Date{prop:Req} = True
    ?jpt:Date{prop:Trn} = 0
    ?jpt:Date{prop:FontStyle} = font:Bold
    ?JPT:Amount:Prompt{prop:FontColor} = -1
    ?JPT:Amount:Prompt{prop:Color} = 15066597
    ?Credit_Card_Group{prop:Font,3} = -1
    ?Credit_Card_Group{prop:Color} = 15066597
    ?Credit_Card_Group{prop:Trn} = 0
    ?JPT:Credit_Card_Number:Prompt{prop:FontColor} = -1
    ?JPT:Credit_Card_Number:Prompt{prop:Color} = 15066597
    If ?jpt:Credit_Card_Number{prop:ReadOnly} = True
        ?jpt:Credit_Card_Number{prop:FontColor} = 65793
        ?jpt:Credit_Card_Number{prop:Color} = 15066597
    Elsif ?jpt:Credit_Card_Number{prop:Req} = True
        ?jpt:Credit_Card_Number{prop:FontColor} = 65793
        ?jpt:Credit_Card_Number{prop:Color} = 8454143
    Else ! If ?jpt:Credit_Card_Number{prop:Req} = True
        ?jpt:Credit_Card_Number{prop:FontColor} = 65793
        ?jpt:Credit_Card_Number{prop:Color} = 16777215
    End ! If ?jpt:Credit_Card_Number{prop:Req} = True
    ?jpt:Credit_Card_Number{prop:Trn} = 0
    ?jpt:Credit_Card_Number{prop:FontStyle} = font:Bold
    ?JPT:Expiry_Date:Prompt{prop:FontColor} = -1
    ?JPT:Expiry_Date:Prompt{prop:Color} = 15066597
    If ?jpt:Expiry_Date{prop:ReadOnly} = True
        ?jpt:Expiry_Date{prop:FontColor} = 65793
        ?jpt:Expiry_Date{prop:Color} = 15066597
    Elsif ?jpt:Expiry_Date{prop:Req} = True
        ?jpt:Expiry_Date{prop:FontColor} = 65793
        ?jpt:Expiry_Date{prop:Color} = 8454143
    Else ! If ?jpt:Expiry_Date{prop:Req} = True
        ?jpt:Expiry_Date{prop:FontColor} = 65793
        ?jpt:Expiry_Date{prop:Color} = 16777215
    End ! If ?jpt:Expiry_Date{prop:Req} = True
    ?jpt:Expiry_Date{prop:Trn} = 0
    ?jpt:Expiry_Date{prop:FontStyle} = font:Bold
    ?JPT:Issue_Number:Prompt{prop:FontColor} = -1
    ?JPT:Issue_Number:Prompt{prop:Color} = 15066597
    If ?jpt:Issue_Number{prop:ReadOnly} = True
        ?jpt:Issue_Number{prop:FontColor} = 65793
        ?jpt:Issue_Number{prop:Color} = 15066597
    Elsif ?jpt:Issue_Number{prop:Req} = True
        ?jpt:Issue_Number{prop:FontColor} = 65793
        ?jpt:Issue_Number{prop:Color} = 8454143
    Else ! If ?jpt:Issue_Number{prop:Req} = True
        ?jpt:Issue_Number{prop:FontColor} = 65793
        ?jpt:Issue_Number{prop:Color} = 16777215
    End ! If ?jpt:Issue_Number{prop:Req} = True
    ?jpt:Issue_Number{prop:Trn} = 0
    ?jpt:Issue_Number{prop:FontStyle} = font:Bold
    If ?jpt:Amount{prop:ReadOnly} = True
        ?jpt:Amount{prop:FontColor} = 65793
        ?jpt:Amount{prop:Color} = 15066597
    Elsif ?jpt:Amount{prop:Req} = True
        ?jpt:Amount{prop:FontColor} = 65793
        ?jpt:Amount{prop:Color} = 8454143
    Else ! If ?jpt:Amount{prop:Req} = True
        ?jpt:Amount{prop:FontColor} = 65793
        ?jpt:Amount{prop:Color} = 16777215
    End ! If ?jpt:Amount{prop:Req} = True
    ?jpt:Amount{prop:Trn} = 0
    ?jpt:Amount{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Credit_Card_Bit     Routine
    access:paytypes.clearkey(pay:payment_type_key)
    pay:payment_type = jpt:payment_type
    if access:paytypes.fetch(pay:payment_type_key) = Level:Benign
        If pay:credit_card = 'YES'
            Enable(?credit_card_group)
            ?jpt:credit_card_number{prop:req} = 1
            ?jpt:expiry_date{prop:req} = 1
        Else
            Disable(?credit_card_group)
            ?jpt:credit_card_number{prop:req} = 0
            ?jpt:expiry_date{prop:req} = 0
        End
    end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateJOBPAYMT',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateJOBPAYMT',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateJOBPAYMT',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateJOBPAYMT',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jpt:payment_type:prompt;  SolaceCtrlName = '?jpt:payment_type:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jpt:Payment_Type;  SolaceCtrlName = '?jpt:Payment_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JPT:Date:Prompt;  SolaceCtrlName = '?JPT:Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jpt:Date;  SolaceCtrlName = '?jpt:Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JPT:Amount:Prompt;  SolaceCtrlName = '?JPT:Amount:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_Card_Group;  SolaceCtrlName = '?Credit_Card_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JPT:Credit_Card_Number:Prompt;  SolaceCtrlName = '?JPT:Credit_Card_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jpt:Credit_Card_Number;  SolaceCtrlName = '?jpt:Credit_Card_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JPT:Expiry_Date:Prompt;  SolaceCtrlName = '?JPT:Expiry_Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jpt:Expiry_Date;  SolaceCtrlName = '?jpt:Expiry_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JPT:Issue_Number:Prompt;  SolaceCtrlName = '?JPT:Issue_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jpt:Issue_Number;  SolaceCtrlName = '?jpt:Issue_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jpt:Amount;  SolaceCtrlName = '?jpt:Amount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Payment'
  OF ChangeRecord
    ActionMessage = 'Changing A Payment'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateJOBPAYMT')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateJOBPAYMT')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?jpt:payment_type:prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(jpt:Record,History::jpt:Record)
  SELF.AddHistoryField(?jpt:Payment_Type,4)
  SELF.AddHistoryField(?jpt:Date,3)
  SELF.AddHistoryField(?jpt:Credit_Card_Number,5)
  SELF.AddHistoryField(?jpt:Expiry_Date,6)
  SELF.AddHistoryField(?jpt:Issue_Number,7)
  SELF.AddHistoryField(?jpt:Amount,8)
  SELF.AddUpdateFile(Access:JOBPAYMT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:JOBPAYMT.Open
  Relate:PAYTYPES.Open
  Relate:VATCODE.Open
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBPAYMT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If thiswindow.request = Insertrecord
      access:users.clearkey(use:password_key)
      use:password =glo:password
      if access:users.fetch(use:password_key) = Level:Benign
         jpt:user_code = use:user_code
      end
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = glo:select1
      if access:jobs.fetch(job:ref_number_key)
          Return(Level:Fatal)
      end
      Total_Price('C',vat",total",balance")
      jpt:amount = balance"
  End!If thiswindow.request = Insertrecord
  Do RecolourWindow
  ?JPT:Date{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB7.Init(jpt:Payment_Type,?jpt:Payment_Type,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:PAYTYPES,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(pay:Payment_Type_Key)
  FDCB7.AddField(pay:Payment_Type,FDCB7.Q.pay:Payment_Type)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:JOBPAYMT.Close
    Relate:PAYTYPES.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateJOBPAYMT',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    jpt:Ref_Number = glo:select1
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?jpt:Payment_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jpt:Payment_Type, Accepted)
      Do credit_card_bit
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jpt:Payment_Type, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jpt:Date = TINCALENDARStyle1(jpt:Date)
          Display(?JPT:Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If ThisWindow.Request = InsertRecord
      If Access:AUDIT.PrimeRecord() = Level:Benign
          aud:Notes         = 'PAYMENT TYPE: ' & Clip(jpt:Payment_Type) & |
                              '<13,10>AMOUNT: ' & Format(jpt:Amount,@n14.2)
          aud:Ref_Number    = glo:Select1
          aud:Date          = Today()
          aud:Time          = Clock()
          aud:Type          = 'JOB'
          Access:USERS.ClearKey(use:Password_Key)
          use:Password      = glo:Password
          Access:USERS.Fetch(use:Password_Key)
          aud:User          = use:User_Code
          aud:Action        = 'PAYMENT RECEIVED'
          Access:AUDIT.Insert()
      End!If Access:AUDIT.PrimeRecord() = Level:Benign
  End !ThisWindow.Request = InsertRecord
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateJOBPAYMT')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?jpt:Date
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do credit_card_bit
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB7.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

