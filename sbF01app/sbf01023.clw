

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01023.INC'),ONCE        !Local module procedure declarations
                     END


Update_Common_Faults PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
save_ccp_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
chargeable_job_temp  STRING(3)
warranty_job_temp    STRING(3)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?com:Category
cmc:Category           LIKE(cmc:Category)             !List box control field - type derived from field
cmc:Record_Number      LIKE(cmc:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?com:Model_Number
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB8::View:FileDropCombo VIEW(COMMCAT)
                       PROJECT(cmc:Category)
                       PROJECT(cmc:Record_Number)
                     END
FDCB15::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
BRW11::View:Browse   VIEW(COMMONCP)
                       PROJECT(ccp:Description)
                       PROJECT(ccp:Part_Number)
                       PROJECT(ccp:Quantity)
                       PROJECT(ccp:Record_Number)
                       PROJECT(ccp:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
ccp:Description        LIKE(ccp:Description)          !List box control field - type derived from field
ccp:Part_Number        LIKE(ccp:Part_Number)          !List box control field - type derived from field
ccp:Quantity           LIKE(ccp:Quantity)             !List box control field - type derived from field
ccp:Record_Number      LIKE(ccp:Record_Number)        !Primary key field - type derived from field
ccp:Ref_Number         LIKE(ccp:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(COMMONWP)
                       PROJECT(cwp:Description)
                       PROJECT(cwp:Part_Number)
                       PROJECT(cwp:Quantity)
                       PROJECT(cwp:Record_Number)
                       PROJECT(cwp:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
cwp:Description        LIKE(cwp:Description)          !List box control field - type derived from field
cwp:Part_Number        LIKE(cwp:Part_Number)          !List box control field - type derived from field
cwp:Quantity           LIKE(cwp:Quantity)             !List box control field - type derived from field
cwp:Record_Number      LIKE(cwp:Record_Number)        !Primary key field - type derived from field
cwp:Ref_Number         LIKE(cwp:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::com:Record  LIKE(com:RECORD),STATIC
QuickWindow          WINDOW('Update the COMMONFA File'),AT(,,624,348),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Common_Faults'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,616,172),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Title Text'),AT(8,8),USE(?title_text),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Ref Number'),AT(8,20),USE(?COM:Ref_Number:Prompt),TRN
                           ENTRY(@s8),AT(84,20,64,10),USE(com:Ref_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(8,36),USE(?com:model_number:prompt)
                           PROMPT('Category'),AT(8,52),USE(?com:category:prompt)
                           COMBO(@s30),AT(84,52,124,10),USE(com:Category),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Description'),AT(8,68),USE(?COM:Description:Prompt),TRN
                           ENTRY(@s30),AT(84,68,124,10),USE(com:Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Chargeable Job'),AT(8,84),USE(com:Chargeable_Job),LEFT,VALUE('YES','NO')
                           GROUP,AT(280,28,60,32),USE(?Chargeable_Group)
                             BUTTON('Insert Adjustment'),AT(8,296,68,16),USE(?InsertAdjustmentChargeable),LEFT,ICON('insert.gif')
                             PROMPT('Charge Type'),AT(84,84),USE(?com:chargeable_charge_type:prompt)
                             BUTTON,AT(128,84,10,10),USE(?Lookup_Charge_Type),ICON('list3.ico')
                             ENTRY(@s30),AT(144,84,124,10),USE(com:Chargeable_Charge_Type),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Repair Type'),AT(84,96),USE(?com:chargeable_repair_type:2)
                             BUTTON,AT(128,96,10,10),USE(?Lookup_Repair_Type),ICON('list3.ico')
                             ENTRY(@s30),AT(144,96,124,10),USE(com:Chargeable_Repair_Type),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           END
                           CHECK('Warranty Job'),AT(8,112,66,10),USE(com:Warranty_Job),LEFT,VALUE('YES','NO')
                           BUTTON,AT(272,140,10,10),USE(?Lookup_Diagram_Path),ICON('list3.ico')
                           OPTION('Diagram Setting'),AT(84,152,184,20),USE(com:Diagram_Setting),BOXED
                             RADIO('Stretched'),AT(88,160),USE(?COM:Diagram_Setting:Radio1),VALUE('S')
                             RADIO('Centered'),AT(160,160),USE(?COM:Diagram_Setting:Radio2),VALUE('C')
                             RADIO('Tiled'),AT(228,160),USE(?COM:Diagram_Setting:Radio3),VALUE('T')
                           END
                           PROMPT('Diagram Path'),AT(84,140),USE(?COM:Diagram_Path:Prompt),TRN
                           ENTRY(@s255),AT(144,140,124,10),USE(com:Diagram_Path),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           GROUP,AT(276,76,60,32),USE(?warranty_Group)
                             BUTTON('Insert Adjustment'),AT(332,296,68,16),USE(?InsertAdjustmentWarranty),LEFT,ICON('insert.gif')
                             PROMPT('Charge Type'),AT(84,112),USE(?com:warranty_charge_type:prompt)
                             BUTTON,AT(128,124,10,10),USE(?Lookup_Warranty_Repair_Type),ICON('list3.ico')
                             ENTRY(@s30),AT(144,112,124,10),USE(com:Warranty_Charge_Type),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Repair Type'),AT(84,124),USE(?com:warranty_repair_type)
                             BUTTON,AT(128,112,10,10),USE(?Lookup_Warranty_Charge_Type),ICON('list3.ico')
                             ENTRY(@s30),AT(144,124,124,10),USE(com:Warranty_Repair_Type,,?COM:Warranty_Repair_Type:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           END
                           PROMPT('Engineers Notes'),AT(412,20),USE(?COM:Engineers_Notes:Prompt)
                           TEXT,AT(488,20,124,48),USE(com:Engineers_Notes),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           COMBO(@s30),AT(84,36,124,10),USE(com:Model_Number),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           BUTTON,AT(472,20,10,10),USE(?lookup_engineers_notes),ICON('List3.ico')
                           BUTTON,AT(472,84,10,10),USE(?Lookup_Invoice_Text),ICON('List3.ico')
                           PROMPT('Invoice Text'),AT(416,84),USE(?COM:Invoice_Text:Prompt)
                           TEXT,AT(488,84,124,48),USE(com:Invoice_Text),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       SHEET,AT(4,180,616,136),USE(?Sheet2),WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Chargeable Parts'),AT(8,184),USE(?Prompt10),FONT(,,COLOR:Navy,FONT:bold)
                           LIST,AT(8,200,284,88),USE(?List),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@120L(2)|M~Part Number~@s30@32L(2)|M~Quantity~@s8@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(116,296,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(176,296,56,16),USE(?Change),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(236,296,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                           PROMPT('Warranty Parts'),AT(332,184),USE(?Prompt10:2),FONT(,,COLOR:Navy,FONT:bold)
                           LIST,AT(332,200,284,88),USE(?List:2),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@120L(2)|M~Part Number~@s30@32L(2)|M~Quantity~@s8@'),FROM(Queue:Browse:1)
                           BUTTON('&Insert'),AT(440,296,56,16),USE(?Insert:2),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(500,296,56,16),USE(?Change:2),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(560,296,56,16),USE(?Delete:2),LEFT,ICON('delete.gif')
                         END
                       END
                       PANEL,AT(4,320,616,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Fault Codes'),AT(8,324,56,16),USE(?Lookup_Fault_Codes),LEFT,ICON('fault.gif')
                       BUTTON('View Diagram'),AT(68,324,56,16),USE(?View_Diagram),LEFT,ICON('Spy.gif')
                       BUTTON('&OK'),AT(504,324,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(560,324,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       CHECK('Attach Diagram'),AT(8,140),USE(com:Attach_Diagram),LEFT,VALUE('YES','NO')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB15               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

BRW11                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                 !Default Locator
BRW12                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?title_text{prop:FontColor} = -1
    ?title_text{prop:Color} = 15066597
    ?COM:Ref_Number:Prompt{prop:FontColor} = -1
    ?COM:Ref_Number:Prompt{prop:Color} = 15066597
    If ?com:Ref_Number{prop:ReadOnly} = True
        ?com:Ref_Number{prop:FontColor} = 65793
        ?com:Ref_Number{prop:Color} = 15066597
    Elsif ?com:Ref_Number{prop:Req} = True
        ?com:Ref_Number{prop:FontColor} = 65793
        ?com:Ref_Number{prop:Color} = 8454143
    Else ! If ?com:Ref_Number{prop:Req} = True
        ?com:Ref_Number{prop:FontColor} = 65793
        ?com:Ref_Number{prop:Color} = 16777215
    End ! If ?com:Ref_Number{prop:Req} = True
    ?com:Ref_Number{prop:Trn} = 0
    ?com:Ref_Number{prop:FontStyle} = font:Bold
    ?com:model_number:prompt{prop:FontColor} = -1
    ?com:model_number:prompt{prop:Color} = 15066597
    ?com:category:prompt{prop:FontColor} = -1
    ?com:category:prompt{prop:Color} = 15066597
    If ?com:Category{prop:ReadOnly} = True
        ?com:Category{prop:FontColor} = 65793
        ?com:Category{prop:Color} = 15066597
    Elsif ?com:Category{prop:Req} = True
        ?com:Category{prop:FontColor} = 65793
        ?com:Category{prop:Color} = 8454143
    Else ! If ?com:Category{prop:Req} = True
        ?com:Category{prop:FontColor} = 65793
        ?com:Category{prop:Color} = 16777215
    End ! If ?com:Category{prop:Req} = True
    ?com:Category{prop:Trn} = 0
    ?com:Category{prop:FontStyle} = font:Bold
    ?COM:Description:Prompt{prop:FontColor} = -1
    ?COM:Description:Prompt{prop:Color} = 15066597
    If ?com:Description{prop:ReadOnly} = True
        ?com:Description{prop:FontColor} = 65793
        ?com:Description{prop:Color} = 15066597
    Elsif ?com:Description{prop:Req} = True
        ?com:Description{prop:FontColor} = 65793
        ?com:Description{prop:Color} = 8454143
    Else ! If ?com:Description{prop:Req} = True
        ?com:Description{prop:FontColor} = 65793
        ?com:Description{prop:Color} = 16777215
    End ! If ?com:Description{prop:Req} = True
    ?com:Description{prop:Trn} = 0
    ?com:Description{prop:FontStyle} = font:Bold
    ?com:Chargeable_Job{prop:Font,3} = -1
    ?com:Chargeable_Job{prop:Color} = 15066597
    ?com:Chargeable_Job{prop:Trn} = 0
    ?Chargeable_Group{prop:Font,3} = -1
    ?Chargeable_Group{prop:Color} = 15066597
    ?Chargeable_Group{prop:Trn} = 0
    ?com:chargeable_charge_type:prompt{prop:FontColor} = -1
    ?com:chargeable_charge_type:prompt{prop:Color} = 15066597
    If ?com:Chargeable_Charge_Type{prop:ReadOnly} = True
        ?com:Chargeable_Charge_Type{prop:FontColor} = 65793
        ?com:Chargeable_Charge_Type{prop:Color} = 15066597
    Elsif ?com:Chargeable_Charge_Type{prop:Req} = True
        ?com:Chargeable_Charge_Type{prop:FontColor} = 65793
        ?com:Chargeable_Charge_Type{prop:Color} = 8454143
    Else ! If ?com:Chargeable_Charge_Type{prop:Req} = True
        ?com:Chargeable_Charge_Type{prop:FontColor} = 65793
        ?com:Chargeable_Charge_Type{prop:Color} = 16777215
    End ! If ?com:Chargeable_Charge_Type{prop:Req} = True
    ?com:Chargeable_Charge_Type{prop:Trn} = 0
    ?com:Chargeable_Charge_Type{prop:FontStyle} = font:Bold
    ?com:chargeable_repair_type:2{prop:FontColor} = -1
    ?com:chargeable_repair_type:2{prop:Color} = 15066597
    If ?com:Chargeable_Repair_Type{prop:ReadOnly} = True
        ?com:Chargeable_Repair_Type{prop:FontColor} = 65793
        ?com:Chargeable_Repair_Type{prop:Color} = 15066597
    Elsif ?com:Chargeable_Repair_Type{prop:Req} = True
        ?com:Chargeable_Repair_Type{prop:FontColor} = 65793
        ?com:Chargeable_Repair_Type{prop:Color} = 8454143
    Else ! If ?com:Chargeable_Repair_Type{prop:Req} = True
        ?com:Chargeable_Repair_Type{prop:FontColor} = 65793
        ?com:Chargeable_Repair_Type{prop:Color} = 16777215
    End ! If ?com:Chargeable_Repair_Type{prop:Req} = True
    ?com:Chargeable_Repair_Type{prop:Trn} = 0
    ?com:Chargeable_Repair_Type{prop:FontStyle} = font:Bold
    ?com:Warranty_Job{prop:Font,3} = -1
    ?com:Warranty_Job{prop:Color} = 15066597
    ?com:Warranty_Job{prop:Trn} = 0
    ?com:Diagram_Setting{prop:Font,3} = -1
    ?com:Diagram_Setting{prop:Color} = 15066597
    ?com:Diagram_Setting{prop:Trn} = 0
    ?COM:Diagram_Setting:Radio1{prop:Font,3} = -1
    ?COM:Diagram_Setting:Radio1{prop:Color} = 15066597
    ?COM:Diagram_Setting:Radio1{prop:Trn} = 0
    ?COM:Diagram_Setting:Radio2{prop:Font,3} = -1
    ?COM:Diagram_Setting:Radio2{prop:Color} = 15066597
    ?COM:Diagram_Setting:Radio2{prop:Trn} = 0
    ?COM:Diagram_Setting:Radio3{prop:Font,3} = -1
    ?COM:Diagram_Setting:Radio3{prop:Color} = 15066597
    ?COM:Diagram_Setting:Radio3{prop:Trn} = 0
    ?COM:Diagram_Path:Prompt{prop:FontColor} = -1
    ?COM:Diagram_Path:Prompt{prop:Color} = 15066597
    If ?com:Diagram_Path{prop:ReadOnly} = True
        ?com:Diagram_Path{prop:FontColor} = 65793
        ?com:Diagram_Path{prop:Color} = 15066597
    Elsif ?com:Diagram_Path{prop:Req} = True
        ?com:Diagram_Path{prop:FontColor} = 65793
        ?com:Diagram_Path{prop:Color} = 8454143
    Else ! If ?com:Diagram_Path{prop:Req} = True
        ?com:Diagram_Path{prop:FontColor} = 65793
        ?com:Diagram_Path{prop:Color} = 16777215
    End ! If ?com:Diagram_Path{prop:Req} = True
    ?com:Diagram_Path{prop:Trn} = 0
    ?com:Diagram_Path{prop:FontStyle} = font:Bold
    ?warranty_Group{prop:Font,3} = -1
    ?warranty_Group{prop:Color} = 15066597
    ?warranty_Group{prop:Trn} = 0
    ?com:warranty_charge_type:prompt{prop:FontColor} = -1
    ?com:warranty_charge_type:prompt{prop:Color} = 15066597
    If ?com:Warranty_Charge_Type{prop:ReadOnly} = True
        ?com:Warranty_Charge_Type{prop:FontColor} = 65793
        ?com:Warranty_Charge_Type{prop:Color} = 15066597
    Elsif ?com:Warranty_Charge_Type{prop:Req} = True
        ?com:Warranty_Charge_Type{prop:FontColor} = 65793
        ?com:Warranty_Charge_Type{prop:Color} = 8454143
    Else ! If ?com:Warranty_Charge_Type{prop:Req} = True
        ?com:Warranty_Charge_Type{prop:FontColor} = 65793
        ?com:Warranty_Charge_Type{prop:Color} = 16777215
    End ! If ?com:Warranty_Charge_Type{prop:Req} = True
    ?com:Warranty_Charge_Type{prop:Trn} = 0
    ?com:Warranty_Charge_Type{prop:FontStyle} = font:Bold
    ?com:warranty_repair_type{prop:FontColor} = -1
    ?com:warranty_repair_type{prop:Color} = 15066597
    If ?COM:Warranty_Repair_Type:2{prop:ReadOnly} = True
        ?COM:Warranty_Repair_Type:2{prop:FontColor} = 65793
        ?COM:Warranty_Repair_Type:2{prop:Color} = 15066597
    Elsif ?COM:Warranty_Repair_Type:2{prop:Req} = True
        ?COM:Warranty_Repair_Type:2{prop:FontColor} = 65793
        ?COM:Warranty_Repair_Type:2{prop:Color} = 8454143
    Else ! If ?COM:Warranty_Repair_Type:2{prop:Req} = True
        ?COM:Warranty_Repair_Type:2{prop:FontColor} = 65793
        ?COM:Warranty_Repair_Type:2{prop:Color} = 16777215
    End ! If ?COM:Warranty_Repair_Type:2{prop:Req} = True
    ?COM:Warranty_Repair_Type:2{prop:Trn} = 0
    ?COM:Warranty_Repair_Type:2{prop:FontStyle} = font:Bold
    ?COM:Engineers_Notes:Prompt{prop:FontColor} = -1
    ?COM:Engineers_Notes:Prompt{prop:Color} = 15066597
    If ?com:Engineers_Notes{prop:ReadOnly} = True
        ?com:Engineers_Notes{prop:FontColor} = 65793
        ?com:Engineers_Notes{prop:Color} = 15066597
    Elsif ?com:Engineers_Notes{prop:Req} = True
        ?com:Engineers_Notes{prop:FontColor} = 65793
        ?com:Engineers_Notes{prop:Color} = 8454143
    Else ! If ?com:Engineers_Notes{prop:Req} = True
        ?com:Engineers_Notes{prop:FontColor} = 65793
        ?com:Engineers_Notes{prop:Color} = 16777215
    End ! If ?com:Engineers_Notes{prop:Req} = True
    ?com:Engineers_Notes{prop:Trn} = 0
    ?com:Engineers_Notes{prop:FontStyle} = font:Bold
    If ?com:Model_Number{prop:ReadOnly} = True
        ?com:Model_Number{prop:FontColor} = 65793
        ?com:Model_Number{prop:Color} = 15066597
    Elsif ?com:Model_Number{prop:Req} = True
        ?com:Model_Number{prop:FontColor} = 65793
        ?com:Model_Number{prop:Color} = 8454143
    Else ! If ?com:Model_Number{prop:Req} = True
        ?com:Model_Number{prop:FontColor} = 65793
        ?com:Model_Number{prop:Color} = 16777215
    End ! If ?com:Model_Number{prop:Req} = True
    ?com:Model_Number{prop:Trn} = 0
    ?com:Model_Number{prop:FontStyle} = font:Bold
    ?COM:Invoice_Text:Prompt{prop:FontColor} = -1
    ?COM:Invoice_Text:Prompt{prop:Color} = 15066597
    If ?com:Invoice_Text{prop:ReadOnly} = True
        ?com:Invoice_Text{prop:FontColor} = 65793
        ?com:Invoice_Text{prop:Color} = 15066597
    Elsif ?com:Invoice_Text{prop:Req} = True
        ?com:Invoice_Text{prop:FontColor} = 65793
        ?com:Invoice_Text{prop:Color} = 8454143
    Else ! If ?com:Invoice_Text{prop:Req} = True
        ?com:Invoice_Text{prop:FontColor} = 65793
        ?com:Invoice_Text{prop:Color} = 16777215
    End ! If ?com:Invoice_Text{prop:Req} = True
    ?com:Invoice_Text{prop:Trn} = 0
    ?com:Invoice_Text{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Prompt10:2{prop:FontColor} = -1
    ?Prompt10:2{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    ?com:Attach_Diagram{prop:Font,3} = -1
    ?com:Attach_Diagram{prop:Color} = 15066597
    ?com:Attach_Diagram{prop:Trn} = 0

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Common_Faults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Common_Faults',1)
    SolaceViewVars('save_ccp_id',save_ccp_id,'Update_Common_Faults',1)
    SolaceViewVars('save_cwp_id',save_cwp_id,'Update_Common_Faults',1)
    SolaceViewVars('chargeable_job_temp',chargeable_job_temp,'Update_Common_Faults',1)
    SolaceViewVars('warranty_job_temp',warranty_job_temp,'Update_Common_Faults',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Common_Faults',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Common_Faults',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?title_text;  SolaceCtrlName = '?title_text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Ref_Number:Prompt;  SolaceCtrlName = '?COM:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Ref_Number;  SolaceCtrlName = '?com:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:model_number:prompt;  SolaceCtrlName = '?com:model_number:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:category:prompt;  SolaceCtrlName = '?com:category:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Category;  SolaceCtrlName = '?com:Category';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Description:Prompt;  SolaceCtrlName = '?COM:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Description;  SolaceCtrlName = '?com:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Chargeable_Job;  SolaceCtrlName = '?com:Chargeable_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Chargeable_Group;  SolaceCtrlName = '?Chargeable_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InsertAdjustmentChargeable;  SolaceCtrlName = '?InsertAdjustmentChargeable';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:chargeable_charge_type:prompt;  SolaceCtrlName = '?com:chargeable_charge_type:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Charge_Type;  SolaceCtrlName = '?Lookup_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Chargeable_Charge_Type;  SolaceCtrlName = '?com:Chargeable_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:chargeable_repair_type:2;  SolaceCtrlName = '?com:chargeable_repair_type:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Repair_Type;  SolaceCtrlName = '?Lookup_Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Chargeable_Repair_Type;  SolaceCtrlName = '?com:Chargeable_Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Warranty_Job;  SolaceCtrlName = '?com:Warranty_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Diagram_Path;  SolaceCtrlName = '?Lookup_Diagram_Path';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Diagram_Setting;  SolaceCtrlName = '?com:Diagram_Setting';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Diagram_Setting:Radio1;  SolaceCtrlName = '?COM:Diagram_Setting:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Diagram_Setting:Radio2;  SolaceCtrlName = '?COM:Diagram_Setting:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Diagram_Setting:Radio3;  SolaceCtrlName = '?COM:Diagram_Setting:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Diagram_Path:Prompt;  SolaceCtrlName = '?COM:Diagram_Path:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Diagram_Path;  SolaceCtrlName = '?com:Diagram_Path';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?warranty_Group;  SolaceCtrlName = '?warranty_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InsertAdjustmentWarranty;  SolaceCtrlName = '?InsertAdjustmentWarranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:warranty_charge_type:prompt;  SolaceCtrlName = '?com:warranty_charge_type:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Warranty_Repair_Type;  SolaceCtrlName = '?Lookup_Warranty_Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Warranty_Charge_Type;  SolaceCtrlName = '?com:Warranty_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:warranty_repair_type;  SolaceCtrlName = '?com:warranty_repair_type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Warranty_Charge_Type;  SolaceCtrlName = '?Lookup_Warranty_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Warranty_Repair_Type:2;  SolaceCtrlName = '?COM:Warranty_Repair_Type:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Engineers_Notes:Prompt;  SolaceCtrlName = '?COM:Engineers_Notes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Engineers_Notes;  SolaceCtrlName = '?com:Engineers_Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Model_Number;  SolaceCtrlName = '?com:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lookup_engineers_notes;  SolaceCtrlName = '?lookup_engineers_notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Invoice_Text;  SolaceCtrlName = '?Lookup_Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COM:Invoice_Text:Prompt;  SolaceCtrlName = '?COM:Invoice_Text:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Invoice_Text;  SolaceCtrlName = '?com:Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10:2;  SolaceCtrlName = '?Prompt10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Fault_Codes;  SolaceCtrlName = '?Lookup_Fault_Codes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?View_Diagram;  SolaceCtrlName = '?View_Diagram';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?com:Attach_Diagram;  SolaceCtrlName = '?com:Attach_Diagram';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Common Fault'
  OF ChangeRecord
    ActionMessage = 'Changing A Common Fault'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Common_Faults')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Common_Faults')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?title_text
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(com:Record,History::com:Record)
  SELF.AddHistoryField(?com:Ref_Number,1)
  SELF.AddHistoryField(?com:Category,2)
  SELF.AddHistoryField(?com:Description,7)
  SELF.AddHistoryField(?com:Chargeable_Job,8)
  SELF.AddHistoryField(?com:Chargeable_Charge_Type,9)
  SELF.AddHistoryField(?com:Chargeable_Repair_Type,10)
  SELF.AddHistoryField(?com:Warranty_Job,11)
  SELF.AddHistoryField(?com:Diagram_Setting,16)
  SELF.AddHistoryField(?com:Diagram_Path,17)
  SELF.AddHistoryField(?com:Warranty_Charge_Type,12)
  SELF.AddHistoryField(?COM:Warranty_Repair_Type:2,13)
  SELF.AddHistoryField(?com:Engineers_Notes,33)
  SELF.AddHistoryField(?com:Model_Number,6)
  SELF.AddHistoryField(?com:Invoice_Text,32)
  SELF.AddHistoryField(?com:Attach_Diagram,15)
  SELF.AddUpdateFile(Access:COMMONFA)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:COMMCAT.Open
  Relate:MODELNUM.Open
  Access:COMMONFA.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:COMMONFA
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW11.Init(?List,Queue:Browse.ViewPosition,BRW11::View:Browse,Queue:Browse,Relate:COMMONCP,SELF)
  BRW12.Init(?List:2,Queue:Browse:1.ViewPosition,BRW12::View:Browse,Queue:Browse:1,Relate:COMMONWP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If thiswindow.request = Insertrecord
      access:users.clearkey(use:password_key)
      use:password =glo:password
      access:users.fetch(use:password_key)
      com:who_booked = use:user_code
  End!If thiswindow.request = Insertrecord
  ?title_text{prop:text} = 'Fault Details.    Created By: ' & com:who_Booked & ' (' & CLip(Format(COM:date_booked,@d6))|
                             & ' ' & CLip(Format(com:time_booked,@t1)) & ')'
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW11.Q &= Queue:Browse
  BRW11.RetainRow = 0
  BRW11.AddSortOrder(,ccp:Description_Key)
  BRW11.AddRange(ccp:Ref_Number,Relate:COMMONCP,Relate:COMMONFA)
  BRW11.AddLocator(BRW11::Sort0:Locator)
  BRW11::Sort0:Locator.Init(,ccp:Description,1,BRW11)
  BRW11.AddField(ccp:Description,BRW11.Q.ccp:Description)
  BRW11.AddField(ccp:Part_Number,BRW11.Q.ccp:Part_Number)
  BRW11.AddField(ccp:Quantity,BRW11.Q.ccp:Quantity)
  BRW11.AddField(ccp:Record_Number,BRW11.Q.ccp:Record_Number)
  BRW11.AddField(ccp:Ref_Number,BRW11.Q.ccp:Ref_Number)
  BRW12.Q &= Queue:Browse:1
  BRW12.RetainRow = 0
  BRW12.AddSortOrder(,cwp:Description_Key)
  BRW12.AddRange(cwp:Ref_Number,Relate:COMMONWP,Relate:COMMONFA)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,cwp:Description,1,BRW12)
  BRW12.AddField(cwp:Description,BRW12.Q.cwp:Description)
  BRW12.AddField(cwp:Part_Number,BRW12.Q.cwp:Part_Number)
  BRW12.AddField(cwp:Quantity,BRW12.Q.cwp:Quantity)
  BRW12.AddField(cwp:Record_Number,BRW12.Q.cwp:Record_Number)
  BRW12.AddField(cwp:Ref_Number,BRW12.Q.cwp:Ref_Number)
  IF ?com:Chargeable_Job{Prop:Checked} = True
    UNHIDE(?Insert)
    UNHIDE(?Change)
    UNHIDE(?Delete)
    ENABLE(?Chargeable_Group)
    ENABLE(?List)
  END
  IF ?com:Chargeable_Job{Prop:Checked} = False
    com:Chargeable_Charge_Type = ''
    com:Chargeable_Repair_Type = ''
    HIDE(?Insert)
    HIDE(?Change)
    HIDE(?Delete)
    DISABLE(?Chargeable_Group)
    DISABLE(?List)
  END
  IF ?com:Warranty_Job{Prop:Checked} = True
    UNHIDE(?Insert:2)
    UNHIDE(?Change:2)
    UNHIDE(?Delete:2)
    ENABLE(?warranty_Group)
    ENABLE(?List:2)
  END
  IF ?com:Warranty_Job{Prop:Checked} = False
    com:Warranty_Charge_Type = ''
    com:Warranty_Repair_Type = ''
    HIDE(?Insert:2)
    HIDE(?Change:2)
    HIDE(?Delete:2)
    DISABLE(?warranty_Group)
    DISABLE(?List:2)
  END
  IF ?com:Attach_Diagram{Prop:Checked} = True
    UNHIDE(?View_Diagram)
    ENABLE(?View_Diagram)
    ENABLE(?Lookup_Diagram_Path)
    ENABLE(?COM:Diagram_Setting)
    ENABLE(?COM:Diagram_Path)
    ENABLE(?COM:Diagram_Path:Prompt)
  END
  IF ?com:Attach_Diagram{Prop:Checked} = False
    com:Diagram_Path = ''
    HIDE(?View_Diagram)
    DISABLE(?View_Diagram)
    DISABLE(?Lookup_Diagram_Path)
    DISABLE(?COM:Diagram_Setting)
    DISABLE(?COM:Diagram_Path:Prompt)
    DISABLE(?COM:Diagram_Path)
  END
  FDCB8.Init(com:Category,?com:Category,Queue:FileDropCombo:1.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:1,Relate:COMMCAT,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:1
  FDCB8.AddSortOrder(cmc:Category_Key)
  FDCB8.AddRange(cmc:Model_Number,com:Model_Number)
  FDCB8.AddField(cmc:Category,FDCB8.Q.cmc:Category)
  FDCB8.AddField(cmc:Record_Number,FDCB8.Q.cmc:Record_Number)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  FDCB15.Init(com:Model_Number,?com:Model_Number,Queue:FileDropCombo.ViewPosition,FDCB15::View:FileDropCombo,Queue:FileDropCombo,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB15.Q &= Queue:FileDropCombo
  FDCB15.AddSortOrder(mod:Model_Number_Key)
  FDCB15.AddField(mod:Model_Number,FDCB15.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB15.WindowComponent)
  FDCB15.DefaultFill = 0
  BRW11.AskProcedure = 1
  BRW12.AskProcedure = 2
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COMMCAT.Close
    Relate:MODELNUM.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Common_Faults',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  access:modelnum.clearkey(mod:model_number_key)
  mod:model_number = com:model_number
  if access:modelnum.fetch(mod:model_number_key) = Level:Benign
      glo:select11 = mod:manufacturer
  end!if access:modelnum.fetch(mod:model_number_key) = Level:Benign
  glo:select12 = com:model_number
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      UpdateCOMMONCP
      UpdateCOMMONWP
    END
    ReturnValue = GlobalResponse
  END
  glo:select11 = ''
  glo:select12 = ''
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?com:Chargeable_Job
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Chargeable_Job, Accepted)
      If ~0{prop:acceptall}
          If com:chargeable_job <> chargeable_job_temp
              If com:chargeable_job = 'NO'
                  beep(beep:systemquestion)  ;  yield()
                  case message('Warning! Removing the Chargeable aspect of this fault will '&|
                          'also remove any Chargeable Parts.'&|
                          '||Are you sure you want to continue?', |
                          'ServiceBase 2000', icon:question, |
                           button:yes+button:no, button:no, 0)
                  of button:yes
                      chargeable_job_temp = com:chargeable_job
                      setcursor(cursor:wait)                                                      !Delete the chargeable parts
                      save_ccp_id = access:commoncp.savefile()
                      access:commoncp.clearkey(ccp:ref_number_key)
                      ccp:ref_number = com:ref_number
                      set(ccp:ref_number_key,ccp:ref_number_key)
                      loop
                          if access:commoncp.next()
                             break
                          end !if
                          if ccp:ref_number <> com:ref_number      |
                              then break.  ! end if
                          Delete(commoncp)
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                      end !loop
                      access:commoncp.restorefile(save_ccp_id)
                      setcursor()
                  of button:no
                      com:chargeable_job = chargeable_job_temp                                    !Reset the tick box
                  end !case
              Else!If com:chargeable_job = 'NO'
                  chargeable_job_temp = com:chargeable_job                                        !Save the temporary value
              End!If com:chargeable_job = 'NO'
          End!If com:chargeable_job <> chargeable_job_temp
          !Thismakeover.setwindow(win:form)
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Chargeable_Job, Accepted)
    OF ?Lookup_Charge_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type, Accepted)
      glo:select1 = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type, Accepted)
    OF ?Lookup_Repair_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type, Accepted)
      glo:select1 = com:model_number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type, Accepted)
    OF ?com:Warranty_Job
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Warranty_Job, Accepted)
      If ~0{prop:acceptall}
          If com:warranty_job <> warranty_job_temp
              If com:warranty_job = 'NO'
                  beep(beep:systemquestion)  ;  yield()
                  case message('Warning! Removing the warranty aspect of this fault will '&|
                          'also remove any Warranty Parts.'&|
                          '||Are you sure you want to continue?', |
                          'ServiceBase 2000', icon:question, |
                           button:yes+button:no, button:no, 0)
                  of button:yes
                      warranty_job_temp = com:warranty_job
                      setcursor(cursor:wait)
                      save_cwp_id = access:commonwp.savefile()
                      access:commonwp.clearkey(cwp:ref_number_key)
                      cwp:ref_number = com:ref_number
                      set(cwp:ref_number_key,cwp:ref_number_key)
                      loop
                          if access:commonwp.next()
                             break
                          end !if
                          if cwp:ref_number <> com:ref_number      |
                              then break.  ! end if
                          Delete(commonwp)
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                      end !loop
                      access:commonwp.restorefile(save_cwp_id)
                      setcursor()
                  of button:no
                      com:warranty_job = warranty_job_temp                                    !Reset the tick box
                  end !case
              Else!If com:warranty_job = 'NO'
                  warranty_job_temp = com:warranty_job                                        !Save the temporary value
              End!If com:warranty_job = 'NO'
          End!If com:warranty_job <> warranty_job_temp
          !Thismakeover.setwindow(win:form)
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Warranty_Job, Accepted)
    OF ?Lookup_Warranty_Repair_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Repair_Type, Accepted)
      glo:select1 = com:model_number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Repair_Type, Accepted)
    OF ?Lookup_Warranty_Charge_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type, Accepted)
      glo:select1 = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type, Accepted)
    OF ?Lookup_Fault_Codes
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Fault_Codes, Accepted)
      access:modelnum.clearkey(mod:model_number_key)
      mod:model_number = com:model_number
      if access:modelnum.fetch(mod:model_number_key) = Level:Benign
          glo:select11 = mod:manufacturer
      end!if access:modelnum.fetch(mod:model_number_key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Fault_Codes, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?com:Chargeable_Job
      IF ?com:Chargeable_Job{Prop:Checked} = True
        UNHIDE(?Insert)
        UNHIDE(?Change)
        UNHIDE(?Delete)
        ENABLE(?Chargeable_Group)
        ENABLE(?List)
      END
      IF ?com:Chargeable_Job{Prop:Checked} = False
        com:Chargeable_Charge_Type = ''
        com:Chargeable_Repair_Type = ''
        HIDE(?Insert)
        HIDE(?Change)
        HIDE(?Delete)
        DISABLE(?Chargeable_Group)
        DISABLE(?List)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Chargeable_Job, Accepted)
      BRW11.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Chargeable_Job, Accepted)
    OF ?InsertAdjustmentChargeable
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InsertAdjustmentChargeable, Accepted)
      Case MessageEx('Are you sure you want to add a Chargeable Adjustment?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              If Access:COMMONCP.Primerecord() = Level:Benign
                  ccp:Ref_Number         = com:Ref_Number
                  ccp:Quantity           = 1
                  ccp:Adjustment         = 'YES'
                  ccp:Exclude_From_Order = 'YES'
                  ccp:Part_Number        = 'ADJUSTMENT'
                  ccp:Description        = 'ADJUSTMENT'
                  If Access:COMMONCP.Tryinsert()
                      Access:COMMONCP.Cancelautoinc()
                  End!If Access:COMMONCP.Tryinsert()
              End!If Access:COMMONCP.Primerecord() = Level:Benign
      
      
          Of 2 ! &No Button
      End!Case MessageEx
      BRW11.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InsertAdjustmentChargeable, Accepted)
    OF ?Lookup_Charge_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickChargeableChargeTypes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type, Accepted)
      case globalresponse
          of requestcompleted
              com:chargeable_charge_type = cha:charge_type
              select(?+2)
          of requestcancelled
      !        com: = ''
              select(?-1)
      end!case globalreponse
      display(?com:chargeable_charge_type)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Charge_Type, Accepted)
    OF ?Lookup_Repair_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickModelChargeableRepairTypes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type, Accepted)
      case globalresponse
          of requestcompleted
              com:chargeable_repair_type = rep:repair_type  
              select(?+2)
          of requestcancelled
      !        com:chargeable_repair_type = ''
              select(?-1)
      end!case globalreponse
      display(?com:chargeable_repair_type)
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Repair_Type, Accepted)
    OF ?com:Warranty_Job
      IF ?com:Warranty_Job{Prop:Checked} = True
        UNHIDE(?Insert:2)
        UNHIDE(?Change:2)
        UNHIDE(?Delete:2)
        ENABLE(?warranty_Group)
        ENABLE(?List:2)
      END
      IF ?com:Warranty_Job{Prop:Checked} = False
        com:Warranty_Charge_Type = ''
        com:Warranty_Repair_Type = ''
        HIDE(?Insert:2)
        HIDE(?Change:2)
        HIDE(?Delete:2)
        DISABLE(?warranty_Group)
        DISABLE(?List:2)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Warranty_Job, Accepted)
      BRW12.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Warranty_Job, Accepted)
    OF ?Lookup_Diagram_Path
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Diagram_Path, Accepted)
      filedialog ('Choose File',com:diagram_path,'GIF Files|*.GIF|BMP Files|*.BMP|JPEG Files|*.JPG|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
      com:diagram_path = upper(com:diagram_path)
      display(?com:diagram_path)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Diagram_Path, Accepted)
    OF ?InsertAdjustmentWarranty
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InsertAdjustmentWarranty, Accepted)
      Case MessageEx('Are you sure you want to add a Warranty Adjustment?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              If Access:COMMONWP.Primerecord() = Level:Benign
                  cwp:Ref_Number         = com:Ref_Number
                  cwp:Quantity           = 1
                  cwp:Adjustment         = 'YES'
                  cwp:Exclude_From_Order = 'YES'
                  cwp:Part_Number        = 'ADJUSTMENT'
                  cwp:Description        = 'ADJUSTMENT'
                  If Access:COMMONWP.Tryinsert()
                      Access:COMMONWP.Cancelautoinc()
                  End!If Access:COMMONCP.Tryinsert()
              End!If Access:COMMONCP.Primerecord() = Level:Benign
      
      
          Of 2 ! &No Button
      End!Case MessageEx
      BRW12.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InsertAdjustmentWarranty, Accepted)
    OF ?Lookup_Warranty_Repair_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickModelWarrantyRepairTypes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Repair_Type, Accepted)
      case globalresponse
          of requestcompleted
              com:warranty_repair_type = rep:repair_type
              select(?+2)
          of requestcancelled
      !        com:chargeable_repair_type = ''
              select(?-1)
      end!case globalreponse
      display()
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Repair_Type, Accepted)
    OF ?Lookup_Warranty_Charge_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickWarrantyChargeTypes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type, Accepted)
      case globalresponse
          of requestcompleted
              com:warranty_charge_type = cha:charge_type
              select(?+2)
          of requestcancelled
      !        com: = ''
              select(?-1)
      end!case globalreponse
      display(?com:warranty_charge_type)
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type, Accepted)
    OF ?com:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Model_Number, Accepted)
      FDCB8.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Model_Number, Accepted)
    OF ?lookup_engineers_notes
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Engineers_Notes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lookup_engineers_notes, Accepted)
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If com:engineers_notes = ''
                  com:engineers_notes = Clip(glo:notes_pointer)
              Else !If job:engineers_notes = ''
                  com:engineers_notes = Clip(com:engineers_notes) & '<13,10>' & Clip(glo:notes_pointer)
              End !If job:engineers_notes = ''
          End
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lookup_engineers_notes, Accepted)
    OF ?Lookup_Invoice_Text
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Invoice_Text
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Invoice_Text, Accepted)
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If com:invoice_text = ''
                  com:invoice_text    = Clip(glo:notes_pointer)
              Else !If job:invoice_text = ''
                  com:invoice_text = Clip(com:invoice_text) & '<13,10>' & Clip(glo:notes_pointer)
              End !If job:invoice_text = ''
          End
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Invoice_Text, Accepted)
    OF ?Lookup_Fault_Codes
      ThisWindow.Update
      Fault_Codes_Common_Faults
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Fault_Codes, Accepted)
      glo:select11 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Fault_Codes, Accepted)
    OF ?View_Diagram
      ThisWindow.Update
      View_Diagram
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?com:Attach_Diagram
      IF ?com:Attach_Diagram{Prop:Checked} = True
        UNHIDE(?View_Diagram)
        ENABLE(?View_Diagram)
        ENABLE(?Lookup_Diagram_Path)
        ENABLE(?COM:Diagram_Setting)
        ENABLE(?COM:Diagram_Path)
        ENABLE(?COM:Diagram_Path:Prompt)
      END
      IF ?com:Attach_Diagram{Prop:Checked} = False
        com:Diagram_Path = ''
        HIDE(?View_Diagram)
        DISABLE(?View_Diagram)
        DISABLE(?Lookup_Diagram_Path)
        DISABLE(?COM:Diagram_Setting)
        DISABLE(?COM:Diagram_Path:Prompt)
        DISABLE(?COM:Diagram_Path)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Attach_Diagram, Accepted)
      !Thismakeover.setwindow(win:form)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?com:Attach_Diagram, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Common_Faults')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      !Skip Model Number If New Record
      If thiswindow.request = Insertrecord
          If com:model_number <> ''
              Select(?com:description)
          End!If com:model_number <> ''
      End!If thiswindow.request = Insertrecord
      
      Post(event:accepted,?com:chargeable_job)
      Post(event:accepted,?com:warranty_job)
      Post(event:accepted,?com:attach_diagram)
      !Save Fields
      chargeable_job_temp = com:chargeable_job
      warranty_job_temp   = com:warranty_job
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW11.ResetSort(1)
      BRW12.ResetSort(1)
      FDCB15.ResetQueue(1)
      FDCB8.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW11.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW11.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

