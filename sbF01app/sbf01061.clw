

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01061.INC'),ONCE        !Local module procedure declarations
                     END


RoyalMail_Import_Routine PROCEDURE                    !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
start_consignment_number_temp LONG
current_consignment_number_temp LONG
sav:path             STRING(255)
path_temp            STRING(255)
save_job_id          USHORT,AUTO
save_cou_id          USHORT,AUTO
despatch_date_temp   DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Royal Mail Import Routine'),AT(,,227,75),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,220,40),USE(?Sheet1),SPREAD
                         TAB('Royal Mail Import Routine'),USE(?Tab1)
                           PROMPT('Import File'),AT(8,24),USE(?despatch_date_temp:Prompt)
                           ENTRY(@s255),AT(84,24,124,10),USE(filename3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(212,24,10,10),USE(?LookupImportFile),SKIP,ICON('List3.ico')
                         END
                       END
                       BUTTON('&Import File'),AT(8,52,56,16),USE(?OkButton),LEFT,ICON('desp_sm.gif'),DEFAULT
                       BUTTON('Close'),AT(164,52,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,48,220,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
RoyalMailFile   FILE,DRIVER('BASIC'),PRE(ROYM),NAME(filename3),CREATE,BINDABLE,THREAD ! Record length 1016
Record              Record
A                       String(30)
B                       String(30)
C                       String(30)
D                       String(30)
E                       String(30)
F                       String(30)
G                       String(30)
H                       String(30)
I                       String(30)
J                       String(30)
                    End
                End
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup2          SelectFileClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?despatch_date_temp:Prompt{prop:FontColor} = -1
    ?despatch_date_temp:Prompt{prop:Color} = 15066597
    If ?filename3{prop:ReadOnly} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 15066597
    Elsif ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 8454143
    Else ! If ?filename3{prop:Req} = True
        ?filename3{prop:FontColor} = 65793
        ?filename3{prop:Color} = 16777215
    End ! If ?filename3{prop:Req} = True
    ?filename3{prop:Trn} = 0
    ?filename3{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RoyalMail_Import_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('start_consignment_number_temp',start_consignment_number_temp,'RoyalMail_Import_Routine',1)
    SolaceViewVars('current_consignment_number_temp',current_consignment_number_temp,'RoyalMail_Import_Routine',1)
    SolaceViewVars('sav:path',sav:path,'RoyalMail_Import_Routine',1)
    SolaceViewVars('path_temp',path_temp,'RoyalMail_Import_Routine',1)
    SolaceViewVars('save_job_id',save_job_id,'RoyalMail_Import_Routine',1)
    SolaceViewVars('save_cou_id',save_cou_id,'RoyalMail_Import_Routine',1)
    SolaceViewVars('despatch_date_temp',despatch_date_temp,'RoyalMail_Import_Routine',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_date_temp:Prompt;  SolaceCtrlName = '?despatch_date_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?filename3;  SolaceCtrlName = '?filename3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupImportFile;  SolaceCtrlName = '?LookupImportFile';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RoyalMail_Import_Routine')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'RoyalMail_Import_Routine')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?despatch_date_temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STATUS.Open
  Access:COURIER.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:LOCINTER.UseFile
  Access:USERS.UseFile
  Access:EXCHHIST.UseFile
  Access:LOANHIST.UseFile
  Access:LOAN.UseFile
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FileLookup2.Init
  FileLookup2.Flags=BOR(FileLookup2.Flags,FILE:LongName)
  FileLookup2.SetMask('All Files','*.*')
  FileLookup2.DefaultDirectory='cou:Import_Path'
  FileLookup2.WindowTitle='Royal Mail Import File'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STATUS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'RoyalMail_Import_Routine',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupImportFile
      ThisWindow.Update
      filename3 = Upper(FileLookup2.Ask(1)  )
      DISPLAY
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      Case MessageEx('Are you sure you want to import the selected file?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
      
              error# = 0
              count# = 0
              Open(RoyalMailFile)
              If Error()
                  Stop(error())
                  error# = 1
              End!If Error()
          
              If error# = 0
          
                  first# = 1
                  recordspercycle     = 25
                  recordsprocessed    = 0
                  percentprogress     = 0
                  setcursor(cursor:wait)
                  open(progresswindow)
                  progress:thermometer    = 0
                  ?progress:pcttext{prop:text} = '0% Completed'
                  ?progress:userstring{prop:text} = 'Writing Output File...'
                  recordstoprocess = Records(RoyalMailFile)
          
                  Set(RoyalMailFile,0)
                  Loop
                      Next(RoyalMailFile)
                      If Error()
                          Break
                      End!If Error()
                      Do GetNextRecord2
                      Access:JOBS.Clearkey(job:Ref_Number_Key)
                      job:Ref_Number  = roym:F
                      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          !Found
                          count# += 1
                          If job:exchange_unit_number <> ''
                              If job:exchange_consignment_number = ''
                                  job:exchange_consignment_number = Clip(Upper(roym:J))
                                  If job:workshop <> 'YES' and job:third_party_site   = ''
                                      GetStatus(116,1,'JOB') !Awaiting Arrival
                                  End!If job:workshop <> 'YES'
                                  GetStatus(901,1,'EXC') !Despatched
          
                                  get(audit,0)
                                  if access:audit.primerecord() = level:benign
                                      aud:ref_number    = job:ref_number
                                      aud:date          = Today()
                                      aud:time          = Clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      aud:user = use:user_code
                                      aud:action        = 'ROYAL MAIL CONFIRMATION: EXCHANGE UNIT DESPATCHED'
                                      aud:notes         = 'DESPATCH NOTE NUMBER: ' & Clip(Upper(roym:J))
                                      aud:type          = 'EXC'
                                      if access:audit.insert()
                                          access:audit.cancelautoinc()
                                      end
                                  end!if access:audit.primerecord() = level:benign
                              End!IF job:exchange_consignment_number = ''
                          Else!If job:exchange_unit_number <> ''
                              If job:date_completed <> ''
                                  If job:consignment_number = ''
                                      job:consignment_number = Clip(Upper(roym:J))
                                      If job:paid = 'YES'
                                          GetStatus(910,1,'JOB') !Despatch Paid
                                      Else!If job:paid = 'YES'
                                          GetStatus(905,1,'JOB') !Despatch Unpaid
                                      End!If job:paid = 'YES'
      
                                      If def:RemoveWorkshopDespatch = 1
                                          job:Workshop = 'NO'
                                      End!If def:RemoveWorkshopDespatch = 1
      
                                      get(audit,0)
                                      if access:audit.primerecord() = level:benign
                                          aud:ref_number    = job:ref_number
                                          aud:date          = Today()
                                          aud:time          = Clock()
                                          access:users.clearkey(use:password_key)
                                          use:password =glo:password
                                          access:users.fetch(use:password_key)
                                          aud:user = use:user_code
                                          aud:action        = 'ROYAL MAIL CONFIRMATION: JOB DESPATCHED'
                                          aud:notes         = 'DESPATCH NOTE NUMBER: ' & Clip(Upper(roym:J))
                                          if access:audit.insert()
                                              access:audit.cancelautoinc()
                                          end
                                      end!if access:audit.primerecord() = level:benign
                                  End!If job:consignment_number = ''
                              Else
                                  If job:loan_unit_number <> ''
                                      If job:loan_consignment_number = ''
                                          job:loan_consignment_number = Clip(Upper(roym:J))
                                          !call the status routine
                                          If job:workshop <> 'YES' and job:third_party_site   = ''
                                              GetStatus(117,1,'JOB') !Awaiting Arrival
                                          End!If job:workshop <> 'YES'
                                          GetStatus(901,1,'LOA') !Loan Unit Despatched
                                          get(audit,0)
                                          if access:audit.primerecord() = level:benign
                                              aud:ref_number    = job:ref_number
                                              aud:date          = Today()
                                              aud:time          = Clock()
                                              access:users.clearkey(use:password_key)
                                              use:password =glo:password
                                              access:users.fetch(use:password_key)
                                              aud:user = use:user_code
                                              aud:action        = 'ROYAL MAIL CONFIRMATION: LOAN UNIT DESPATCHED'
                                              aud:notes         = 'DESPATCH NOTE NUMBER: ' & Clip(Upper(roym:J))
                                              aud:type          = 'LOA'
                                              if access:audit.insert()
                                                  access:audit.cancelautoinc()
                                              end
                                          end!if access:audit.primerecord() = level:benign
                                      End!If job:loan_unit_number = ''
          
                                  End!If job:loan_unit_number <> ''
                              End!If job:date_completed <> ''
                          End!If job:exchange_unit_number <> ''
                          access:jobs.update()
                      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                  End!Loop
          
                  setcursor()
                  close(progresswindow)
                  Close(RoyalMailFile)
                  If count# <> 0
                      Case MessageEx('Import Completed.','ServiceBase 2000','Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
                          Of 1 ! &OK Button
                      End!Case MessageEx
          
                  Else!If count# <> 0
                      Case MessageEx('No jobs were updated.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End!If count# <> 0
              End!If error# = 0
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'RoyalMail_Import_Routine')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Access:COURIER.Clearkey(cou:Courier_Type_Key)
      cou:Courier_Type = 'ROYAL MAIL'
      If Access:COURIER.Tryfetch(cou:Courier_Type_Key) = Level:Benign
          !Found
      Else! If Access:COURIER.Tryfetch(cou:Courier_Type_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:COURIER.Tryfetch(cou:Courier_Type_Key) = Level:Benign
      
      despatch_date_temp = Today()
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

