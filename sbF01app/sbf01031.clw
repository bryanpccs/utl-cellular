

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBF01031.INC'),ONCE        !Local module procedure declarations
                     END


Spares_Received_Export PROCEDURE                      ! Declare Procedure
save_sup_id          USHORT,AUTO
savepath             STRING(255)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
spares_queue         QUEUE,PRE(spaque)
supplier             STRING(30)
part_number          STRING(30)
description          STRING(30)
quantity             REAL
purchase_cost        REAL
total                REAL
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Spares_Received_Export')      !Add Procedure to Log
  end


   Relate:SUPPLIER.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:STOCK.Open
   Relate:DEFAULTS.Open
    savepath = path()
    set(defaults)
    access:defaults.next()
    If def:exportpath <> ''
        glo:file_name = Clip(def:exportpath) & '\EXP_REC.CSV'
    Else!If def:exportpath <> ''
        glo:file_name = 'C:\EXP_REC.CSV'
    End!If def:exportpath <> ''

    If not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                file:keepdir + file:noerror + file:longname)
        setpath(savepath)
    else
        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        setcursor(cursor:wait)
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = Records(supplier)
        ?progress:userstring{prop:text} = 'Compiling Data ...'
        count# = 0
        Remove(expspares)
        access:expspares.open()
        access:expspares.usefile()
        Clear(spares_queue)
        Free(spares_queue)
        setpath(savepath)
        setcursor(cursor:wait)
        save_sup_id = access:supplier.savefile()
        set(sup:company_name_key)
        loop
            if access:supplier.next()
               break
            end !if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            do getnextrecord2
            
            save_par_id = access:parts.savefile()
            access:parts.clearkey(par:SuppDateRecKey)
            par:supplier     = sup:company_name
            par:date_received = glo:select1
            set(par:SuppDateRecKey,par:SuppDateRecKey)
            loop
                if access:parts.next()
                   break
                end !if
                if par:supplier     <> sup:company_name      |
                or par:date_received >  glo:select2     |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                Sort(spares_queue,spaque:part_number)
                spaque:part_number  = par:part_number
                Get(spares_queue,spaque:part_number)
                If Error()
                    spaque:supplier     = sup:company_name
                    spaque:part_number  = par:part_number
                    spaque:description  = par:description
                    If par:part_ref_number <> ''
                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number = par:part_ref_number
                        if access:stock.fetch(sto:ref_number_key) = Level:Benign
                            If sto:location <> glo:select3
                                Cycle
                            End!If sto:location <> glo:select3
                            spaque:purchase_cost    = sto:purchase_cost
                        end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                    Else!If par:part_ref_number <> ''
                        spaque:purchase_cost    = par:purchase_cost
                    End!If par:part_ref_number <> ''
                    spaque:quantity     = par:quantity
                    Add(spares_queue)
                Else!If Error
                    spaque:quantity    += par:quantity
                    Put(spares_queue)
                End!If Error
            end !loop
            access:parts.restorefile(save_par_id)

            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:SuppDateRecKey)
            wpr:supplier     = sup:company_name
            wpr:date_received = glo:select1
            set(wpr:SuppDateRecKey,wpr:SuppDateRecKey)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:supplier     <> sup:company_name      |
                or wpr:date_received >  glo:select2      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                Sort(spares_queue,spaque:part_number)
                spaque:part_number  = wpr:part_number
                Get(spares_queue,spaque:part_number)
                If Error()
                    spaque:supplier     = sup:company_name
                    spaque:part_number  = wpr:part_number
                    spaque:description  = wpr:description
                    If wpr:part_ref_number <> ''
                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number = wpr:part_ref_number
                        if access:stock.fetch(sto:ref_number_key) = Level:Benign
                            If sto:location <> glo:select3
                                Cycle
                            End!If sto:location <> glo:select3
                            spaque:purchase_cost    = sto:purchase_cost
                        end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                    Else!If par:part_ref_number <> ''
                        spaque:purchase_cost    = wpr:purchase_cost
                    End!If par:part_ref_number <> ''
                    spaque:quantity     = wpr:quantity
                    Add(spares_queue)
                Else!If Error
                    spaque:quantity    += wpr:quantity
                    Put(spares_queue)
                End!If Error

            end !loop
            access:warparts.restorefile(save_wpr_id)

        end !loop
        access:supplier.restorefile(save_sup_id)
        setcursor()

        setcursor()
        close(progresswindow)


        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        setcursor(cursor:wait)
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = Records(spares_queue)

        ?progress:userstring{prop:text} = 'Creating File ...'
        Sort(spares_queue,spaque:supplier)
        Loop x# = 1 To Records(spares_queue)
            Do getnextrecord2
            Get(spares_queue,x#)
            get(expspares,0)
            if access:expspares.primerecord() = Level:Benign
                expspa:supplier    = Stripcomma(spaque:supplier)
                expspa:part_number = Stripcomma(spaque:part_number)
                expspa:description = Stripcomma(spaque:description)
                expspa:quantity    = Stripcomma(spaque:quantity)
                expspa:sale_cost   = Stripcomma(Format(spaque:purchase_cost,@n_14.2))
                expspa:total_cost  = Stripcomma(Format(spaque:purchase_cost * spaque:quantity,@n_14.2))
                access:expspares.insert()
                count# += 1
            end!if access:expspares.primerecord() = Level:Benign
        End!Loop x# = 1 To Records(spares_queue)
        access:expspares.close()
        setcursor()
        close(progresswindow)

        If count# = 0
            beep(beep:systemhand)  ;  yield()
            message('There are no records that match the selected criteria.', |
                    'ServiceBase 2000', icon:hand)
        Else!If count# = 0
            beep(beep:systemexclamation)  ;  yield()
            message('Export Completed.', |
                    'ServiceBase 2000', icon:exclamation)
        End!If count# = 0


    end
   Relate:SUPPLIER.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:STOCK.Close
   Relate:DEFAULTS.Close
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Spares_Received_Export',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_sup_id',save_sup_id,'Spares_Received_Export',1)
    SolaceViewVars('savepath',savepath,'Spares_Received_Export',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Spares_Received_Export',1)
    SolaceViewVars('save_par_id',save_par_id,'Spares_Received_Export',1)
    SolaceViewVars('spares_queue:supplier',spares_queue:supplier,'Spares_Received_Export',1)
    SolaceViewVars('spares_queue:part_number',spares_queue:part_number,'Spares_Received_Export',1)
    SolaceViewVars('spares_queue:description',spares_queue:description,'Spares_Received_Export',1)
    SolaceViewVars('spares_queue:quantity',spares_queue:quantity,'Spares_Received_Export',1)
    SolaceViewVars('spares_queue:purchase_cost',spares_queue:purchase_cost,'Spares_Received_Export',1)
    SolaceViewVars('spares_queue:total',spares_queue:total,'Spares_Received_Export',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
