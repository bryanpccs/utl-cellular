

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01053.INC'),ONCE        !Local module procedure declarations
                     END


Exchange_Loan_Unit PROCEDURE                          !Generated from procedure template - Window

tmp:ExchangeDetails  STRING(60)
print_label_temp     BYTE
save_xch_id          USHORT,AUTO
tmp:ExchangeAccessories STRING(60)
tmp:ExchangeUser     STRING(60)
tmp:LoanDetails      STRING(60)
tmp:LoanUser         STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?job:Exchange_Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?job:Loan_Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(JOBEXACC)
                       PROJECT(jea:Part_Number)
                       PROJECT(jea:Description)
                       PROJECT(jea:Record_Number)
                       PROJECT(jea:Job_Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
jea:Part_Number        LIKE(jea:Part_Number)          !List box control field - type derived from field
jea:Description        LIKE(jea:Description)          !List box control field - type derived from field
jea:Record_Number      LIKE(jea:Record_Number)        !Primary key field - type derived from field
jea:Job_Ref_Number     LIKE(jea:Job_Ref_Number)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB4::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Exchage/Loan Unit Details'),AT(,,516,248),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       BUTTON('Pick Exchange Unit'),AT(76,20,96,16),USE(?Pick_Exchange_Unit),LEFT,ICON('desp_sm.gif')
                       BUTTON('Pick Accessory'),AT(216,20,84,16),USE(?Pick_Accessory),LEFT,ICON('stock_sm.gif')
                       SHEET,AT(4,4,328,104),USE(?ExchangeSheet),DISABLE,SPREAD
                         TAB('Exchange Unit Details'),USE(?Tab1)
                           LINE,AT(204,10,0,90),USE(?Line1),COLOR(COLOR:Black),LINEWIDTH(2)
                           BUTTON('Pick Loan Unit'),AT(72,128,96,16),USE(?Pick_Exchange_Unit:2),LEFT,ICON('desp_sm.gif')
                           PROMPT('Exchange Unit No'),AT(8,44),USE(?ExchangeUnitNumber)
                           STRING(@s8),AT(72,44),USE(job:Exchange_Unit_Number),FONT(,,,FONT:bold,CHARSET:ANSI)
                           LIST,AT(216,40,112,60),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('109L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@'),FROM(Queue:Browse)
                           PROMPT('Unit Details'),AT(8,56),USE(?ExchangeUnitNumber:2)
                           STRING(@s60),AT(72,56,123,10),USE(tmp:ExchangeDetails),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(72,68),USE(xch:ESN),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(72,80),USE(xch:MSN),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s60),AT(72,92,123,10),USE(tmp:ExchangeUser),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('User'),AT(8,92),USE(?Prompt5:2)
                           PROMPT('E.S.N. / I.M.E.I.'),AT(8,68),USE(?ExchangeUnitNumber:3)
                           PROMPT('M.S.N.'),AT(8,80),USE(?ExchangeUnitNumber:4)
                         END
                       END
                       SHEET,AT(336,4,176,104),USE(?ExchangeDespatchSheet),DISABLE,SPREAD
                         TAB('Exchange Despatch Details'),USE(?Tab2)
                           PROMPT('Courier'),AT(340,20),USE(?Prompt5:3)
                           COMBO(@s30),AT(400,20,108,10),USE(job:Exchange_Courier),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Consignment No'),AT(340,36),USE(?JOB:Exchange_Consignment_Number:Prompt),TRN
                           ENTRY(@s30),AT(400,36,108,10),USE(job:Exchange_Consignment_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('User'),AT(340,52),USE(?JOB:Exchange_Despatched_User:Prompt),TRN
                           ENTRY(@s3),AT(400,52,64,10),USE(job:Exchange_Despatched_User),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Despatched'),AT(340,68),USE(?JOB:Exchange_Despatched:Prompt),TRN
                           ENTRY(@d6b),AT(400,68,64,10),USE(job:Exchange_Despatched),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Despatch Number'),AT(340,84),USE(?JOB:Exchange_Despatch_Number:Prompt),TRN
                           ENTRY(@p<<<<<<#pb),AT(400,84,64,10),USE(job:Exchange_Despatch_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       SHEET,AT(4,112,328,104),USE(?LoanSheet),DISABLE,SPREAD
                         TAB('Loan Unit Details'),USE(?LoanUnitTab)
                           LINE,AT(204,120,0,90),USE(?Line1:2),COLOR(COLOR:Black),LINEWIDTH(2)
                           PROMPT('Unit Details'),AT(8,164),USE(?ExchangeUnitNumber:6)
                           STRING(@s60),AT(72,164,123,10),USE(tmp:LoanDetails),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('E.S.N. / I.M.E.I.'),AT(8,176),USE(?ExchangeUnitNumber:7)
                           STRING(@s30),AT(72,176),USE(loa:ESN),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('M.S.N.'),AT(8,188),USE(?ExchangeUnitNumber:8)
                           STRING(@s30),AT(72,188),USE(loa:MSN),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Loan Unit Number'),AT(8,152),USE(?ExchangeUnitNumber:5)
                           STRING(@s8),AT(72,152),USE(job:Loan_Unit_Number),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('User'),AT(8,200),USE(?Prompt5:5)
                           STRING(@s60),AT(72,200,123,10),USE(tmp:LoanUser),FONT(,,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       SHEET,AT(336,112,176,104),USE(?LoanDespatchSheet),DISABLE,SPREAD
                         TAB('Loan Despatch Details'),USE(?Tab4)
                           PROMPT('Courier'),AT(340,128),USE(?Prompt5:4)
                           COMBO(@s30),AT(400,128,108,10),USE(job:Loan_Courier),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Consignment No'),AT(340,144),USE(?JOB:Loan_Consignment_Number:Prompt),TRN
                           ENTRY(@s30),AT(400,144,108,10),USE(job:Loan_Consignment_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('User'),AT(340,160),USE(?JOB:Loan_Despatched_User:Prompt),TRN
                           ENTRY(@s3),AT(400,160,64,10),USE(job:Loan_Despatched_User),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Despatched'),AT(340,176),USE(?JOB:Loan_Despatched:Prompt),TRN
                           ENTRY(@d6b),AT(400,176,64,10),USE(job:Loan_Despatched),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Despatch Number'),AT(340,192),USE(?JOB:Loan_Despatch_Number:Prompt),TRN
                           ENTRY(@p<<<<<<#pb),AT(400,192,64,10),USE(job:Loan_Despatch_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(4,220,508,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(452,224,56,16),USE(?Close),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?ExchangeSheet{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?ExchangeUnitNumber{prop:FontColor} = -1
    ?ExchangeUnitNumber{prop:Color} = 15066597
    ?job:Exchange_Unit_Number{prop:FontColor} = -1
    ?job:Exchange_Unit_Number{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?ExchangeUnitNumber:2{prop:FontColor} = -1
    ?ExchangeUnitNumber:2{prop:Color} = 15066597
    ?tmp:ExchangeDetails{prop:FontColor} = -1
    ?tmp:ExchangeDetails{prop:Color} = 15066597
    ?xch:ESN{prop:FontColor} = -1
    ?xch:ESN{prop:Color} = 15066597
    ?xch:MSN{prop:FontColor} = -1
    ?xch:MSN{prop:Color} = 15066597
    ?tmp:ExchangeUser{prop:FontColor} = -1
    ?tmp:ExchangeUser{prop:Color} = 15066597
    ?Prompt5:2{prop:FontColor} = -1
    ?Prompt5:2{prop:Color} = 15066597
    ?ExchangeUnitNumber:3{prop:FontColor} = -1
    ?ExchangeUnitNumber:3{prop:Color} = 15066597
    ?ExchangeUnitNumber:4{prop:FontColor} = -1
    ?ExchangeUnitNumber:4{prop:Color} = 15066597
    ?ExchangeDespatchSheet{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt5:3{prop:FontColor} = -1
    ?Prompt5:3{prop:Color} = 15066597
    If ?job:Exchange_Courier{prop:ReadOnly} = True
        ?job:Exchange_Courier{prop:FontColor} = 65793
        ?job:Exchange_Courier{prop:Color} = 15066597
    Elsif ?job:Exchange_Courier{prop:Req} = True
        ?job:Exchange_Courier{prop:FontColor} = 65793
        ?job:Exchange_Courier{prop:Color} = 8454143
    Else ! If ?job:Exchange_Courier{prop:Req} = True
        ?job:Exchange_Courier{prop:FontColor} = 65793
        ?job:Exchange_Courier{prop:Color} = 16777215
    End ! If ?job:Exchange_Courier{prop:Req} = True
    ?job:Exchange_Courier{prop:Trn} = 0
    ?job:Exchange_Courier{prop:FontStyle} = font:Bold
    ?JOB:Exchange_Consignment_Number:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Consignment_Number:Prompt{prop:Color} = 15066597
    If ?job:Exchange_Consignment_Number{prop:ReadOnly} = True
        ?job:Exchange_Consignment_Number{prop:FontColor} = 65793
        ?job:Exchange_Consignment_Number{prop:Color} = 15066597
    Elsif ?job:Exchange_Consignment_Number{prop:Req} = True
        ?job:Exchange_Consignment_Number{prop:FontColor} = 65793
        ?job:Exchange_Consignment_Number{prop:Color} = 8454143
    Else ! If ?job:Exchange_Consignment_Number{prop:Req} = True
        ?job:Exchange_Consignment_Number{prop:FontColor} = 65793
        ?job:Exchange_Consignment_Number{prop:Color} = 16777215
    End ! If ?job:Exchange_Consignment_Number{prop:Req} = True
    ?job:Exchange_Consignment_Number{prop:Trn} = 0
    ?job:Exchange_Consignment_Number{prop:FontStyle} = font:Bold
    ?JOB:Exchange_Despatched_User:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Despatched_User:Prompt{prop:Color} = 15066597
    If ?job:Exchange_Despatched_User{prop:ReadOnly} = True
        ?job:Exchange_Despatched_User{prop:FontColor} = 65793
        ?job:Exchange_Despatched_User{prop:Color} = 15066597
    Elsif ?job:Exchange_Despatched_User{prop:Req} = True
        ?job:Exchange_Despatched_User{prop:FontColor} = 65793
        ?job:Exchange_Despatched_User{prop:Color} = 8454143
    Else ! If ?job:Exchange_Despatched_User{prop:Req} = True
        ?job:Exchange_Despatched_User{prop:FontColor} = 65793
        ?job:Exchange_Despatched_User{prop:Color} = 16777215
    End ! If ?job:Exchange_Despatched_User{prop:Req} = True
    ?job:Exchange_Despatched_User{prop:Trn} = 0
    ?job:Exchange_Despatched_User{prop:FontStyle} = font:Bold
    ?JOB:Exchange_Despatched:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Despatched:Prompt{prop:Color} = 15066597
    If ?job:Exchange_Despatched{prop:ReadOnly} = True
        ?job:Exchange_Despatched{prop:FontColor} = 65793
        ?job:Exchange_Despatched{prop:Color} = 15066597
    Elsif ?job:Exchange_Despatched{prop:Req} = True
        ?job:Exchange_Despatched{prop:FontColor} = 65793
        ?job:Exchange_Despatched{prop:Color} = 8454143
    Else ! If ?job:Exchange_Despatched{prop:Req} = True
        ?job:Exchange_Despatched{prop:FontColor} = 65793
        ?job:Exchange_Despatched{prop:Color} = 16777215
    End ! If ?job:Exchange_Despatched{prop:Req} = True
    ?job:Exchange_Despatched{prop:Trn} = 0
    ?job:Exchange_Despatched{prop:FontStyle} = font:Bold
    ?JOB:Exchange_Despatch_Number:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Despatch_Number:Prompt{prop:Color} = 15066597
    If ?job:Exchange_Despatch_Number{prop:ReadOnly} = True
        ?job:Exchange_Despatch_Number{prop:FontColor} = 65793
        ?job:Exchange_Despatch_Number{prop:Color} = 15066597
    Elsif ?job:Exchange_Despatch_Number{prop:Req} = True
        ?job:Exchange_Despatch_Number{prop:FontColor} = 65793
        ?job:Exchange_Despatch_Number{prop:Color} = 8454143
    Else ! If ?job:Exchange_Despatch_Number{prop:Req} = True
        ?job:Exchange_Despatch_Number{prop:FontColor} = 65793
        ?job:Exchange_Despatch_Number{prop:Color} = 16777215
    End ! If ?job:Exchange_Despatch_Number{prop:Req} = True
    ?job:Exchange_Despatch_Number{prop:Trn} = 0
    ?job:Exchange_Despatch_Number{prop:FontStyle} = font:Bold
    ?LoanSheet{prop:Color} = 15066597
    ?LoanUnitTab{prop:Color} = 15066597
    ?ExchangeUnitNumber:6{prop:FontColor} = -1
    ?ExchangeUnitNumber:6{prop:Color} = 15066597
    ?tmp:LoanDetails{prop:FontColor} = -1
    ?tmp:LoanDetails{prop:Color} = 15066597
    ?ExchangeUnitNumber:7{prop:FontColor} = -1
    ?ExchangeUnitNumber:7{prop:Color} = 15066597
    ?loa:ESN{prop:FontColor} = -1
    ?loa:ESN{prop:Color} = 15066597
    ?ExchangeUnitNumber:8{prop:FontColor} = -1
    ?ExchangeUnitNumber:8{prop:Color} = 15066597
    ?loa:MSN{prop:FontColor} = -1
    ?loa:MSN{prop:Color} = 15066597
    ?ExchangeUnitNumber:5{prop:FontColor} = -1
    ?ExchangeUnitNumber:5{prop:Color} = 15066597
    ?job:Loan_Unit_Number{prop:FontColor} = -1
    ?job:Loan_Unit_Number{prop:Color} = 15066597
    ?Prompt5:5{prop:FontColor} = -1
    ?Prompt5:5{prop:Color} = 15066597
    ?tmp:LoanUser{prop:FontColor} = -1
    ?tmp:LoanUser{prop:Color} = 15066597
    ?LoanDespatchSheet{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    ?Prompt5:4{prop:FontColor} = -1
    ?Prompt5:4{prop:Color} = 15066597
    If ?job:Loan_Courier{prop:ReadOnly} = True
        ?job:Loan_Courier{prop:FontColor} = 65793
        ?job:Loan_Courier{prop:Color} = 15066597
    Elsif ?job:Loan_Courier{prop:Req} = True
        ?job:Loan_Courier{prop:FontColor} = 65793
        ?job:Loan_Courier{prop:Color} = 8454143
    Else ! If ?job:Loan_Courier{prop:Req} = True
        ?job:Loan_Courier{prop:FontColor} = 65793
        ?job:Loan_Courier{prop:Color} = 16777215
    End ! If ?job:Loan_Courier{prop:Req} = True
    ?job:Loan_Courier{prop:Trn} = 0
    ?job:Loan_Courier{prop:FontStyle} = font:Bold
    ?JOB:Loan_Consignment_Number:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Consignment_Number:Prompt{prop:Color} = 15066597
    If ?job:Loan_Consignment_Number{prop:ReadOnly} = True
        ?job:Loan_Consignment_Number{prop:FontColor} = 65793
        ?job:Loan_Consignment_Number{prop:Color} = 15066597
    Elsif ?job:Loan_Consignment_Number{prop:Req} = True
        ?job:Loan_Consignment_Number{prop:FontColor} = 65793
        ?job:Loan_Consignment_Number{prop:Color} = 8454143
    Else ! If ?job:Loan_Consignment_Number{prop:Req} = True
        ?job:Loan_Consignment_Number{prop:FontColor} = 65793
        ?job:Loan_Consignment_Number{prop:Color} = 16777215
    End ! If ?job:Loan_Consignment_Number{prop:Req} = True
    ?job:Loan_Consignment_Number{prop:Trn} = 0
    ?job:Loan_Consignment_Number{prop:FontStyle} = font:Bold
    ?JOB:Loan_Despatched_User:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Despatched_User:Prompt{prop:Color} = 15066597
    If ?job:Loan_Despatched_User{prop:ReadOnly} = True
        ?job:Loan_Despatched_User{prop:FontColor} = 65793
        ?job:Loan_Despatched_User{prop:Color} = 15066597
    Elsif ?job:Loan_Despatched_User{prop:Req} = True
        ?job:Loan_Despatched_User{prop:FontColor} = 65793
        ?job:Loan_Despatched_User{prop:Color} = 8454143
    Else ! If ?job:Loan_Despatched_User{prop:Req} = True
        ?job:Loan_Despatched_User{prop:FontColor} = 65793
        ?job:Loan_Despatched_User{prop:Color} = 16777215
    End ! If ?job:Loan_Despatched_User{prop:Req} = True
    ?job:Loan_Despatched_User{prop:Trn} = 0
    ?job:Loan_Despatched_User{prop:FontStyle} = font:Bold
    ?JOB:Loan_Despatched:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Despatched:Prompt{prop:Color} = 15066597
    If ?job:Loan_Despatched{prop:ReadOnly} = True
        ?job:Loan_Despatched{prop:FontColor} = 65793
        ?job:Loan_Despatched{prop:Color} = 15066597
    Elsif ?job:Loan_Despatched{prop:Req} = True
        ?job:Loan_Despatched{prop:FontColor} = 65793
        ?job:Loan_Despatched{prop:Color} = 8454143
    Else ! If ?job:Loan_Despatched{prop:Req} = True
        ?job:Loan_Despatched{prop:FontColor} = 65793
        ?job:Loan_Despatched{prop:Color} = 16777215
    End ! If ?job:Loan_Despatched{prop:Req} = True
    ?job:Loan_Despatched{prop:Trn} = 0
    ?job:Loan_Despatched{prop:FontStyle} = font:Bold
    ?JOB:Loan_Despatch_Number:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Despatch_Number:Prompt{prop:Color} = 15066597
    If ?job:Loan_Despatch_Number{prop:ReadOnly} = True
        ?job:Loan_Despatch_Number{prop:FontColor} = 65793
        ?job:Loan_Despatch_Number{prop:Color} = 15066597
    Elsif ?job:Loan_Despatch_Number{prop:Req} = True
        ?job:Loan_Despatch_Number{prop:FontColor} = 65793
        ?job:Loan_Despatch_Number{prop:Color} = 8454143
    Else ! If ?job:Loan_Despatch_Number{prop:Req} = True
        ?job:Loan_Despatch_Number{prop:FontColor} = 65793
        ?job:Loan_Despatch_Number{prop:Color} = 16777215
    End ! If ?job:Loan_Despatch_Number{prop:Req} = True
    ?job:Loan_Despatch_Number{prop:Trn} = 0
    ?job:Loan_Despatch_Number{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
show_hide       Routine
    If job:exchange_unit_number <> ''
        Enable(?exchangesheet)
        Enable(?exchangedespatchsheet)
    Else!If job:exchange_unit_number <> ''
        Disable(?exchangesheet)
        Disable(?exchangedespatchsheet)
    End!If job:exchange_unit_number <> ''


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Exchange_Loan_Unit',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:ExchangeDetails',tmp:ExchangeDetails,'Exchange_Loan_Unit',1)
    SolaceViewVars('print_label_temp',print_label_temp,'Exchange_Loan_Unit',1)
    SolaceViewVars('save_xch_id',save_xch_id,'Exchange_Loan_Unit',1)
    SolaceViewVars('tmp:ExchangeAccessories',tmp:ExchangeAccessories,'Exchange_Loan_Unit',1)
    SolaceViewVars('tmp:ExchangeUser',tmp:ExchangeUser,'Exchange_Loan_Unit',1)
    SolaceViewVars('tmp:LoanDetails',tmp:LoanDetails,'Exchange_Loan_Unit',1)
    SolaceViewVars('tmp:LoanUser',tmp:LoanUser,'Exchange_Loan_Unit',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Pick_Exchange_Unit;  SolaceCtrlName = '?Pick_Exchange_Unit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Pick_Accessory;  SolaceCtrlName = '?Pick_Accessory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeSheet;  SolaceCtrlName = '?ExchangeSheet';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Pick_Exchange_Unit:2;  SolaceCtrlName = '?Pick_Exchange_Unit:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnitNumber;  SolaceCtrlName = '?ExchangeUnitNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Unit_Number;  SolaceCtrlName = '?job:Exchange_Unit_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnitNumber:2;  SolaceCtrlName = '?ExchangeUnitNumber:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeDetails;  SolaceCtrlName = '?tmp:ExchangeDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:ESN;  SolaceCtrlName = '?xch:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:MSN;  SolaceCtrlName = '?xch:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeUser;  SolaceCtrlName = '?tmp:ExchangeUser';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:2;  SolaceCtrlName = '?Prompt5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnitNumber:3;  SolaceCtrlName = '?ExchangeUnitNumber:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnitNumber:4;  SolaceCtrlName = '?ExchangeUnitNumber:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeDespatchSheet;  SolaceCtrlName = '?ExchangeDespatchSheet';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:3;  SolaceCtrlName = '?Prompt5:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Courier;  SolaceCtrlName = '?job:Exchange_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Consignment_Number:Prompt;  SolaceCtrlName = '?JOB:Exchange_Consignment_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Consignment_Number;  SolaceCtrlName = '?job:Exchange_Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Despatched_User:Prompt;  SolaceCtrlName = '?JOB:Exchange_Despatched_User:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Despatched_User;  SolaceCtrlName = '?job:Exchange_Despatched_User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Despatched:Prompt;  SolaceCtrlName = '?JOB:Exchange_Despatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Despatched;  SolaceCtrlName = '?job:Exchange_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Despatch_Number:Prompt;  SolaceCtrlName = '?JOB:Exchange_Despatch_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Despatch_Number;  SolaceCtrlName = '?job:Exchange_Despatch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LoanSheet;  SolaceCtrlName = '?LoanSheet';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LoanUnitTab;  SolaceCtrlName = '?LoanUnitTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1:2;  SolaceCtrlName = '?Line1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnitNumber:6;  SolaceCtrlName = '?ExchangeUnitNumber:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LoanDetails;  SolaceCtrlName = '?tmp:LoanDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnitNumber:7;  SolaceCtrlName = '?ExchangeUnitNumber:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:ESN;  SolaceCtrlName = '?loa:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnitNumber:8;  SolaceCtrlName = '?ExchangeUnitNumber:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loa:MSN;  SolaceCtrlName = '?loa:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ExchangeUnitNumber:5;  SolaceCtrlName = '?ExchangeUnitNumber:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Unit_Number;  SolaceCtrlName = '?job:Loan_Unit_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:5;  SolaceCtrlName = '?Prompt5:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LoanUser;  SolaceCtrlName = '?tmp:LoanUser';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LoanDespatchSheet;  SolaceCtrlName = '?LoanDespatchSheet';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:4;  SolaceCtrlName = '?Prompt5:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Courier;  SolaceCtrlName = '?job:Loan_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Consignment_Number:Prompt;  SolaceCtrlName = '?JOB:Loan_Consignment_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Consignment_Number;  SolaceCtrlName = '?job:Loan_Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Despatched_User:Prompt;  SolaceCtrlName = '?JOB:Loan_Despatched_User:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Despatched_User;  SolaceCtrlName = '?job:Loan_Despatched_User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Despatched:Prompt;  SolaceCtrlName = '?JOB:Loan_Despatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Despatched;  SolaceCtrlName = '?job:Loan_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Despatch_Number:Prompt;  SolaceCtrlName = '?JOB:Loan_Despatch_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Despatch_Number;  SolaceCtrlName = '?job:Loan_Despatch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Exchange_Loan_Unit')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Exchange_Loan_Unit')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Pick_Exchange_Unit
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:JOBEXACC.Open
  Access:JOBS.UseFile
  Access:LOAN.UseFile
  Access:JOBEXHIS.UseFile
  Access:EXCHHIST.UseFile
  Access:JOBLOHIS.UseFile
  Access:LOANHIST.UseFile
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:JOBEXACC,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW2.Q &= Queue:Browse
  BRW2.RetainRow = 0
  BRW2.AddSortOrder(,jea:Part_Number_Key)
  BRW2.AddRange(jea:Job_Ref_Number,job:Ref_Number)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,jea:Part_Number,1,BRW2)
  BRW2.AddField(jea:Part_Number,BRW2.Q.jea:Part_Number)
  BRW2.AddField(jea:Description,BRW2.Q.jea:Description)
  BRW2.AddField(jea:Record_Number,BRW2.Q.jea:Record_Number)
  BRW2.AddField(jea:Job_Ref_Number,BRW2.Q.jea:Job_Ref_Number)
  FDCB3.Init(job:Exchange_Courier,?job:Exchange_Courier,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(cou:Courier_Key)
  FDCB3.AddField(cou:Courier,FDCB3.Q.cou:Courier)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  FDCB4.Init(job:Loan_Courier,?job:Loan_Courier,Queue:FileDropCombo:1.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo:1,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo:1
  FDCB4.AddSortOrder(cou:Courier_Key)
  FDCB4.AddField(cou:Courier,FDCB4.Q.cou:Courier)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:JOBEXACC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Exchange_Loan_Unit',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Pick_Exchange_Unit
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Pick_Exchange_Unit, Accepted)
      If JOB:Exchange_Consignment_Number <> ''
          Case MessageEx('An Exchange Unit / Accessory has already been despatched.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If JOB:Exchange_Consignment_Number <> ''
      
          glo:select12 = job:model_number
          Set(defaults)
          access:defaults.next()
          error# = 0
          If job:despatch_type = 'LOA' And job:despatched = 'REA' And error# = 0
              Case MessageEx('Cannot allocate an Exchange Unit until the Loan Unit has been despatched.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
          End
          If error# = 0
      
              do_exchange# = 1
              do_return# = 0
              old_exchange_number# = job:exchange_unit_number
      
              If job:exchange_unit_number <> ''
                  do_return# = 1
                  glo:select2 = 'E'
                  glo:select1 = ''
                  Loan_Changing_Screen
      
                  get(jobexhis,0)
                  if access:jobexhis.primerecord() = level:benign
                      jxh:ref_number       = job:ref_number
                      jxh:loan_unit_number = job:exchange_unit_number
                      jxh:date             = Today()
                      jxh:time             = Clock()
                      jxh:user             = use:user_code
                      Case glo:select1
                          Of 1
                              jxh:status    = 'REPLACED FAULTY EXCHANGE'
                          Of 2
                              jxh:status    = 'ALTERNATIVE MODEL REQUIRED'
                          Of 3
                              jxh:status     = 'EXCHANGE NOT REQUIRED: RE-STOCKED'
                              status_number# = 101
                              status_audit# = 1
                              Include('statusexc.inc')
                              job:exchange_unit_number = ''
                              job:exchange_accessory = ''
                                job:exchange_courier            = ''
                                job:exchange_consignment_number = ''
                                job:exchange_despatched         = ''
                                job:exchange_despatched_user    = ''
                                job:exchange_despatch_number    = ''
                              job:exchange_issued_date = ''
                              job:exchange_user        = ''
                              If job:despatch_type = 'EXC'
                                  job:despatched = 'NO'
                                  job:despatch_type = ''
                              End
                              do_exchange# = 0
                          Else
                              do_exchange# = 0
                              do_return# = 0
                      End!Case glo:select1
                      if access:jobexhis.insert()
                          access:jobexhis.cancelautoinc()     
                      end
                      glo:select1 = ''
                      glo:select2 = ''
                  End!if access:jobexhis.primerecord() = level:benign
      
              End!If job:exchange_unit_number <> ''
              If do_exchange# = 1
                  glo:select1 = ''
                  glo:select7 = ''
                  Select_Loan_Exchange('EXC',full_search",unit_number")
                      job:exchange_accessory = 'NO'
                      If full_search" = 'Y'
                          saverequest# = globalrequest
                          globalrequest = selectrecord
                          globalresponse = requestcancelled
      
                          access:subtracc.clearkey(sub:account_number_key)
                          sub:account_number = job:account_number
                          access:subtracc.fetch(sub:account_number_key)
                          access:tradeacc.clearkey(tra:account_number_key) 
                          tra:account_number = sub:main_account_number
                          access:tradeacc.fetch(tra:account_number_key)
                          exchange_stock_type"    = tra:exchange_stock_type
                          Browse_Exchange(exchange_stock_type")
                      Else
                          glo:select1 = unit_number"
                      End
                      if glo:select1 <> ''
                          job:exchange_unit_number = glo:select1
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number = job:exchange_unit_number
                          if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                              continue# = 1
                              If xch:model_number <> job:model_number
                                  Case MessageEx('The selected Exchange Unit has a Model Number of:<13,10><13,10>'&Clip(xch:model_number)&'.<13,10><13,10>Do you want to ignore this mismatch?','ServiceBase 2000',|
                                                 'Styles\warn.ico','|&Ignore Mismatch|&Cancel',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                      Of 1 ! &Ignore Mismatch Button
                                      Of 2 ! &Cancel Button
                                          job:exchange_unit_number = ''
                                          continue# = 0
                                  End!Case MessageEx
                              End!If xch:model_number <> job:model_number
                              If continue# = 1
                                  If def:qaexchloan = 'YES'
                                      xch:available = 'QA1'
                                  Else!If def:qaexchloan = 'YES'
                                      xch:available = 'EXC'
                                  End!If def:qaexchloan = 'YES'
      
                                  xch:job_number    = job:ref_number
                                  access:exchange.update()
      
                                  get(exchhist,0)
                                  if access:exchhist.primerecord() = level:benign
                                      exh:ref_number    = job:exchange_unit_number
                                      exh:date          = Today()
                                      exh:time          = Clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      exh:user = use:user_code
                                      If def:qaexchloan = 'YES'
                                          exh:status        = 'AWAITING QA. EXCHANGED ON JOB NO: ' & clip(job:ref_number)
                                      Else!If def:qaexchloan = 'YES'
                                          exh:status        = 'UNIT EXCHANGED ON JOB NO: ' & clip(job:ref_number)
                                      End!If def:qaexchloan = 'YES'
                                      if access:exchhist.insert()
                                          access:exchhist.cancelautoinc()
                                      end
                                  end!if access:exchhist.primerecord() = level:benign
      
      !Create a new exchange unit for the incoming unit (Made from the customer's unit).
      !If it has an audit number make the new unit the 'Replacement'
                                  
                                  access:exchange_alias.clearkey(xch_ali:ref_number_key)
                                  xch_ali:ref_number = job:exchange_unit_number
                                  if access:exchange_alias.fetch(xch_ali:ref_number_key) = Level:Benign
                                      get(exchange,0)
                                      if access:exchange.primerecord() = Level:benign
                                          ref_number$        = xch:ref_number
                                          xch:record        :=: xch_ali:record
                                          xch:ref_number     = ref_number$
                                          If job:date_Completed <> ''
                                              xch:available   = 'RTS'
                                          Else!If job:date_Completed <> ''
                                              If job:workshop = 'YES'
                                                  xch:available   = 'REP'
                                              Else!If job:workshop = 'YES'
                                                  xch:available   = 'INC'
                                              End!If job:workshop = 'YES'
      
                                          End!If job:date_Completed <> ''
                                          xch:job_number     = job:ref_number
                                          xch:esn            = job:esn
                                          xch:msn            = job:msn
                                          xch:model_number   = job:model_number
                                          xch:manufacturer   = job:manufacturer
                                          if access:exchange.insert()
                                             access:exchange.cancelautoinc()
                                          end
                                      end!if access:exchange.primerecord() = Level:benign
                                      If xch_ali:audit_number <> ''
                                          access:excaudit.clearkey(exa:audit_number_key)
                                          exa:stock_type   = xch:stock_type
                                          exa:audit_number = xch:audit_number
                                          if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                                              exa:replacement_unit_number = xch:ref_number
                                              access:excaudit.update()
                                          end
                                      End!If xch:audit_number <> ''
                                  end!if access:exchange_alias.fetch(xch_ali:ref_number_key) = Level:Benign
      
      !New Exchange Unit added and audit number file update according with the new replacement number.
                                  If def:qaexchloan = 'YES'
                                      job:despatched = ''
                                      job:despatch_type = ''
                                      job:exchange_status = 'QA REQUIRED'
                                      job:exchange_user   = exh:user
                                      status_number# = 605 !qa required
                                      status_audit# = thiswindow.request
                                      include('statusexc.inc')
                                  Else!If def:qaexchloan = 'YES'
                                      job:exchange_status = 'AWAITING DESPATCH'
                                      job:despatched = 'REA'
                                      job:despatch_type = 'EXC'
                                      job:exchange_user = exh:user
                                      job:current_courier = job:exchange_courier
                                      access:courier.clearkey(cou:courier_key)
                                      cou:courier = job:Exchange_courier
                                      Include('despxanc.inc')
                                      status_number# = 110 !depatch exchange unit
                                      status_audit# = thiswindow.request
                                      include('statusexc.inc')
                                  End!If def:qaexchloan = 'YES'
                                  job:status_end_date = end_date"
                                  job:status_end_time = end_time"
      
                                  If def:use_loan_exchange_label = 'YES'
                                      glo:select1 = job:ref_number
                                      glo:select2 = job:exchange_unit_number
                                      Set(defaults)
                                      access:defaults.next()
                                      Case def:label_printer_type
                                          Of 'TEC B-440 / B-442'
                                              Thermal_Labels_Exchange
                                          Of 'TEC B-452'
                                              Thermal_Labels_Exchange_B452
                                      End!Case def:themal_printer_type
                                      glo:select1 = ''
                                      glo:select2 = ''
                                  End!If def:use_loan_exchange_labels = 'YES'
                                  access:subtracc.clearkey(sub:account_number_key)
                                  sub:account_number = job:account_number
                                  if access:subtracc.fetch(sub:account_number_key) = level:benign
                                      access:tradeacc.clearkey(tra:account_number_key) 
                                      tra:account_number = sub:main_account_number
                                      if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                          If tra:refurbcharge = 'YES'
                                              Case MessageEx('This Repair Unit must be Refurbished.<13,10><13,10>Do you wish to print a Refurbishment Label?<13,10><13,10>(Note: It will be printed when you have completed editing this job)','ServiceBase 2000',|
                                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                                  Of 1 ! &Yes Button
                                                      print_label_temp = 'YES'
                                                  Of 2 ! &No Button
                                              End!Case MessageEx
                                          End!If tra:refurbcharge = 'YES'
                                      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                  end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
      
      
                              End!If continue# = 1
                          end
                      End!if glo:select1 <> ''
                      globalrequest = saverequest#
          !        End!If glo:select7 <> ''
      
              End!If do_exchange# = 1
              If do_return# = 2
                  access:stock.clearkey(sto:ref_number_key)
                  sto:ref_number = old_exchange_number#
                  if access:stock.fetch(sto:ref_number_key) = Level:Benign
                      sto:quantity_stock += 1
                      access:stock.update()
                  end
              End!If do_return# = 2
              If do_return# = 1
                  !Delete incoming unit
                  save_xch_id = access:exchange.savefile()
                  access:exchange.clearkey(xch:esn_only_key)
                  xch:esn = job:esn
                  set(xch:esn_only_key,xch:esn_only_key)
                  loop
                      if access:exchange.next()
                         break
                      end !if
                      if xch:esn <> job:esn      |
                          then break.  ! end if
                      If xch:job_number = job:ref_number
                          relate:exchange.Delete(0)
                      End!If xch:job_number = job:ref_number
                  end !loop
                  access:exchange.restorefile(save_xch_id)
      
                  access:exchange.clearkey(xch:ref_number_key)
                  xch:ref_number = old_exchange_number#
                  if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                      If xch:available <> 'QAF'
                          xch:available = 'AVL'
                      End!If xch:available <> 'QAF'
                      xch:job_number = ''
                      access:exchange.update()
      
                      get(exchhist,0)
                      if access:exchhist.primerecord() = level:benign
                          exh:ref_number    = old_exchange_number#
                          exh:date          = Today()
                          exh:time          = Clock()
                          exh:user          = use:user_code
                          exh:status        = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                          if access:exchhist.insert()
                              access:exchhist.cancelautoinc()
                          end
                      end!if access:exchhist.primerecord() = level:benign
                  end!if access:exchange.fetch(xch:ref_number_key)
              End!If do_return# = 1
      !        Do update_exchange
      !        Do exchange_box
      !
              Do show_hide
          End!If error# = 0
          glo:select12 = ''
      End!If JOB:Exchange_Consignment_Number <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Pick_Exchange_Unit, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Exchange_Loan_Unit')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do show_hide
      IF job:Loan_unit_number = ''
          Disable(?LoanSheet)
          Disable(?LoanDespatchSheet)
      Else!IF job:exchange_unit_number = ''
          Enable(?LoanSheet)
          Enable(?LoanDespatchSheet)
      End!IF job:exchange_unit_number = ''
      !ThisMakeOver.SetWindow(win:window)
      
      access:exchange.clearkey(xch:ref_number_key)
      xch:Ref_number  = job:Exchange_unit_number
      access:Exchange.tryfetch(xch:ref_number_key)
      tmp:ExchangeDetails = CLip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

