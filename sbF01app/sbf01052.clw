

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01052.INC'),ONCE        !Local module procedure declarations
                     END


Pick_Courier_Consignment PROCEDURE (f_courier,f_consignment_number,f_despatch_date) !Generated from procedure template - Window

tmp:courier          STRING(30)
tmp:ConsignmentNo    STRING(30)
tmp:DespatchDate     DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Batch Despatch'),AT(,,220,119),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,84),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Select the Courier, Consignment Number and Despatch Date for the Selected Batch'),AT(8,8,204,28),USE(?Prompt1),FONT(,10,COLOR:Navy,FONT:bold)
                           COMBO(@s30),AT(84,40,124,10),USE(tmp:courier),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Consignment Number'),AT(8,56),USE(?tmp:ConsignmentNo:Prompt)
                           ENTRY(@s30),AT(84,56,124,10),USE(tmp:ConsignmentNo),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Despatch Date'),AT(8,72),USE(?tmp:DespatchDate:Prompt)
                           ENTRY(@d6b),AT(84,72,64,10),USE(tmp:DespatchDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           BUTTON,AT(152,72,10,10),USE(?PopCalendar),SKIP,ICON('calenda2.ico')
                           PROMPT('Courier'),AT(8,40),USE(?Prompt2)
                         END
                       END
                       PANEL,AT(4,92,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,96,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(156,96,56,16),USE(?OkButton:2),LEFT,ICON('cancel.gif'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?tmp:courier{prop:ReadOnly} = True
        ?tmp:courier{prop:FontColor} = 65793
        ?tmp:courier{prop:Color} = 15066597
    Elsif ?tmp:courier{prop:Req} = True
        ?tmp:courier{prop:FontColor} = 65793
        ?tmp:courier{prop:Color} = 8454143
    Else ! If ?tmp:courier{prop:Req} = True
        ?tmp:courier{prop:FontColor} = 65793
        ?tmp:courier{prop:Color} = 16777215
    End ! If ?tmp:courier{prop:Req} = True
    ?tmp:courier{prop:Trn} = 0
    ?tmp:courier{prop:FontStyle} = font:Bold
    ?tmp:ConsignmentNo:Prompt{prop:FontColor} = -1
    ?tmp:ConsignmentNo:Prompt{prop:Color} = 15066597
    If ?tmp:ConsignmentNo{prop:ReadOnly} = True
        ?tmp:ConsignmentNo{prop:FontColor} = 65793
        ?tmp:ConsignmentNo{prop:Color} = 15066597
    Elsif ?tmp:ConsignmentNo{prop:Req} = True
        ?tmp:ConsignmentNo{prop:FontColor} = 65793
        ?tmp:ConsignmentNo{prop:Color} = 8454143
    Else ! If ?tmp:ConsignmentNo{prop:Req} = True
        ?tmp:ConsignmentNo{prop:FontColor} = 65793
        ?tmp:ConsignmentNo{prop:Color} = 16777215
    End ! If ?tmp:ConsignmentNo{prop:Req} = True
    ?tmp:ConsignmentNo{prop:Trn} = 0
    ?tmp:ConsignmentNo{prop:FontStyle} = font:Bold
    ?tmp:DespatchDate:Prompt{prop:FontColor} = -1
    ?tmp:DespatchDate:Prompt{prop:Color} = 15066597
    If ?tmp:DespatchDate{prop:ReadOnly} = True
        ?tmp:DespatchDate{prop:FontColor} = 65793
        ?tmp:DespatchDate{prop:Color} = 15066597
    Elsif ?tmp:DespatchDate{prop:Req} = True
        ?tmp:DespatchDate{prop:FontColor} = 65793
        ?tmp:DespatchDate{prop:Color} = 8454143
    Else ! If ?tmp:DespatchDate{prop:Req} = True
        ?tmp:DespatchDate{prop:FontColor} = 65793
        ?tmp:DespatchDate{prop:Color} = 16777215
    End ! If ?tmp:DespatchDate{prop:Req} = True
    ?tmp:DespatchDate{prop:Trn} = 0
    ?tmp:DespatchDate{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Pick_Courier_Consignment',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:courier',tmp:courier,'Pick_Courier_Consignment',1)
    SolaceViewVars('tmp:ConsignmentNo',tmp:ConsignmentNo,'Pick_Courier_Consignment',1)
    SolaceViewVars('tmp:DespatchDate',tmp:DespatchDate,'Pick_Courier_Consignment',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:courier;  SolaceCtrlName = '?tmp:courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ConsignmentNo:Prompt;  SolaceCtrlName = '?tmp:ConsignmentNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ConsignmentNo;  SolaceCtrlName = '?tmp:ConsignmentNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DespatchDate:Prompt;  SolaceCtrlName = '?tmp:DespatchDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DespatchDate;  SolaceCtrlName = '?tmp:DespatchDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton:2;  SolaceCtrlName = '?OkButton:2';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Pick_Courier_Consignment')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Pick_Courier_Consignment')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:COURIER.Open
  SELF.FilesOpened = True
  tmp:despatchdate = Today()
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?tmp:DespatchDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  FDCB2.Init(tmp:courier,?tmp:courier,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(cou:Courier_Key)
  FDCB2.AddField(cou:Courier,FDCB2.Q.cou:Courier)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Pick_Courier_Consignment',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:DespatchDate = TINCALENDARStyle1(tmp:DespatchDate)
          Display(?tmp:DespatchDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      error# = 0
      If tmp:courier = ''
          Select(?tmp:courier)
          error# = 1
      End!If tmp:courier = ''
      If tmp:consignmentno = '' And error# = 0
          Select(?tmp:consignmentno)
          error# = 1
      End
      If tmp:despatchdate = '' and error# = 0
          Select(?tmp:DespatchDate)
          error# = 1
      End!If tmp:despatchdate = '' and error# = 0
      If error# = 0
          f_courier   = tmp:courier
          f_consignment_number    = tmp:consignmentno
          f_despatch_date = tmp:despatchDate
          Post(event:closewindow)
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?OkButton:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton:2, Accepted)
      f_courier = ''
      Post(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton:2, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Pick_Courier_Consignment')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:DespatchDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

