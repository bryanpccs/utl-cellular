

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBF01020.INC'),ONCE        !Local module procedure declarations
                     END


Data_Export_Wizard PROCEDURE                          !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::3:TAGFLAG          BYTE(0)
DASBRW::3:TAGMOUSE         BYTE(0)
DASBRW::3:TAGDISPSTATUS    BYTE(0)
DASBRW::3:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer2                      LIKE(GLO:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGMOUSE         BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Pointer3                      LIKE(GLO:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer4                      LIKE(GLO:Pointer4)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::11:TAGFLAG         BYTE(0)
DASBRW::11:TAGMOUSE        BYTE(0)
DASBRW::11:TAGDISPSTATUS   BYTE(0)
DASBRW::11:QUEUE          QUEUE
Pointer5                      LIKE(GLO:Pointer5)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer6                      LIKE(GLO:Pointer6)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::15:TAGFLAG         BYTE(0)
DASBRW::15:TAGMOUSE        BYTE(0)
DASBRW::15:TAGDISPSTATUS   BYTE(0)
DASBRW::15:QUEUE          QUEUE
Pointer7                      LIKE(GLO:Pointer7)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
FilesOpened          BYTE
tmp:file1            STRING(255)
tmp:file2            STRING(255)
tmp:file3            STRING(255)
tmp:file4            STRING(255)
TabNumber            BYTE(1)
MaxTabs              BYTE(9)
account_tag_temp     STRING(1)
charge_type_tag_temp STRING(1)
warranty_type_tag_temp STRING(1)
status_tag_temp      STRING(1)
turnaround_tag_temp  STRING(1)
DisplayString        STRING(255)
engineer_tag_temp    STRING(1)
model_tag_temp       STRING(1)
Job_Batch_Number_Temp REAL
edi_batch_number_temp STRING(9)
booking_start_date_temp DATE
booking_end_date_temp DATE
completed_start_date_temp DATE
completed_end_date_temp DATE
export_order_temp    STRING(30)
export_jobs_temp     STRING('YES')
Export_Chargeable_Spares_Temp STRING('YES')
export_Warranty_Spares_temp STRING('YES')
Export_Audit_Temp    STRING('YES')
export_queue_temp    QUEUE,PRE()
order_temp           STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW2::View:Browse    VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
account_tag_temp       LIKE(account_tag_temp)         !List box control field - type derived from local data
account_tag_temp_Icon  LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                       PROJECT(cha:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
charge_type_tag_temp   LIKE(charge_type_tag_temp)     !List box control field - type derived from local data
charge_type_tag_temp_Icon LONG                        !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
cha:Ref_Number         LIKE(cha:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                       PROJECT(cha:Ref_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
warranty_type_tag_temp LIKE(warranty_type_tag_temp)   !List box control field - type derived from local data
warranty_type_tag_temp_Icon LONG                      !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
cha:Ref_Number         LIKE(cha:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
status_tag_temp        LIKE(status_tag_temp)          !List box control field - type derived from local data
status_tag_temp_Icon   LONG                           !Entry's icon ID
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                       PROJECT(tur:Days)
                       PROJECT(tur:Hours)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:5
turnaround_tag_temp    LIKE(turnaround_tag_temp)      !List box control field - type derived from local data
turnaround_tag_temp_Icon LONG                         !Entry's icon ID
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
tur:Days               LIKE(tur:Days)                 !List box control field - type derived from field
tur:Hours              LIKE(tur:Hours)                !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW12::View:Browse   VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:User_Code)
                       PROJECT(use:User_Type)
                     END
Queue:Browse:5       QUEUE                            !Queue declaration for browse/combo box using ?List:6
engineer_tag_temp      LIKE(engineer_tag_temp)        !List box control field - type derived from local data
engineer_tag_temp_Icon LONG                           !Entry's icon ID
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !List box control field - type derived from field
use:User_Type          LIKE(use:User_Type)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?List:7
model_tag_temp         LIKE(model_tag_temp)           !List box control field - type derived from local data
model_tag_temp_Icon    LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB16::View:FileDrop VIEW(JOBBATCH)
                       PROJECT(jbt:Batch_Number)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?Job_Batch_Number_Temp
jbt:Batch_Number       LIKE(jbt:Batch_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB17::View:FileDrop VIEW(EDIBATCH)
                       PROJECT(ebt:Batch_Number)
                       PROJECT(ebt:Manufacturer)
                     END
Queue:FileDrop:1     QUEUE                            !Queue declaration for browse/combo box using ?edi_batch_number_temp
ebt:Batch_Number       LIKE(ebt:Batch_Number)         !List box control field - type derived from field
ebt:Manufacturer       LIKE(ebt:Manufacturer)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Data Export Wizard'),AT(,,258,312),FONT('Tahoma',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       PROMPT('Welcome To The Data Export Wizard'),AT(8,8),USE(?Prompt1),FONT(,14,COLOR:Navy,FONT:bold)
                       SHEET,AT(4,4,252,276),USE(?Sheet1),BELOW,SPREAD
                         TAB('1'),USE(?Tab1)
                           PROMPT('Select Trade Account(s)'),AT(8,36),USE(?Prompt2),FONT(,12,,FONT:bold)
                           LIST,AT(28,60,204,180),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11LI@s1@63L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('sho&W tags'),AT(164,32,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag'),AT(28,244,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('&Rev tags'),AT(140,32,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('Tag &All'),AT(106,244,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(184,244,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                         END
                         TAB('2'),USE(?Tab2)
                           PROMPT('Select Chargeable Charge Type(s)'),AT(8,36),USE(?Prompt3),FONT(,12,,FONT:bold)
                           LIST,AT(52,60,152,180),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Tag'),AT(52,244,48,16),USE(?DASTAG:2),LEFT,ICON('tag.gif')
                           BUTTON('&Rev tags'),AT(160,24,56,16),USE(?DASREVTAG:2),HIDE
                           BUTTON('Tag &All'),AT(104,244,48,16),USE(?DASTAGAll:2),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(156,244,48,16),USE(?DASUNTAGALL:2),LEFT,ICON('untag.gif')
                           BUTTON('sho&W tags'),AT(76,24,70,13),USE(?DASSHOWTAG:2),HIDE
                         END
                         TAB('3'),USE(?Tab3)
                           PROMPT('Select Warranty Charge Type(s)'),AT(8,36),USE(?Prompt4),FONT(,12,,FONT:bold)
                           LIST,AT(52,60,152,180),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11LI@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Tag'),AT(52,244,48,16),USE(?DASTAG:3),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All'),AT(104,244,48,16),USE(?DASTAGAll:3),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(156,244,48,16),USE(?DASUNTAGALL:3),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(188,28,50,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(120,28,70,13),USE(?DASSHOWTAG:3),HIDE
                         END
                         TAB('4'),USE(?Tab4)
                           PROMPT('Select Status Type(s)'),AT(8,36),USE(?Prompt5),FONT(,12,,FONT:bold)
                           LIST,AT(52,60,152,180),USE(?List:4),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11LI@s1@120L(2)|M~Status~@s30@'),FROM(Queue:Browse:3)
                           BUTTON('&Tag'),AT(52,244,48,16),USE(?DASTAG:4),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All'),AT(104,244,48,16),USE(?DASTAGAll:4),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(156,244,48,16),USE(?DASUNTAGALL:4),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(148,28,50,13),USE(?DASREVTAG:4),HIDE
                           BUTTON('sho&W tags'),AT(172,28,70,13),USE(?DASSHOWTAG:4),HIDE
                         END
                         TAB('5'),USE(?Tab5)
                           PROMPT('Select Job Turnaround Time(s)'),AT(8,36),USE(?Prompt6),FONT(,12,,FONT:bold)
                           LIST,AT(36,60,192,180),USE(?List:5),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11LI@s1@120L(2)|M~Turnaround Time~@s30@23L(2)|M~Dys~@n3@8L(2)|M~Hrs~@n2@'),FROM(Queue:Browse:4)
                           BUTTON('&Tag'),AT(36,244,48,16),USE(?DASTAG:5),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All'),AT(106,244,48,16),USE(?DASTAGAll:5),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(180,244,48,16),USE(?DASUNTAGALL:5),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(116,20,50,13),USE(?DASREVTAG:5),HIDE
                           BUTTON('sho&W tags'),AT(176,24,70,13),USE(?DASSHOWTAG:5),HIDE
                         END
                         TAB('6'),USE(?Tab6)
                           PROMPT('Select Engineer(s)'),AT(8,36),USE(?Prompt7),FONT(,12,,FONT:bold)
                           LIST,AT(24,60,212,180),USE(?List:6),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@103L(2)|M~Surname~@s30@120L(2)|M~Forename~@s30@12L(2)|M~User Code~@s3' &|
   '@'),FROM(Queue:Browse:5)
                           BUTTON('&Tag'),AT(24,244,48,16),USE(?DASTAG:6),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All'),AT(106,244,48,16),USE(?DASTAGAll:6),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(188,244,48,16),USE(?DASUNTAGALL:6),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(144,24,50,13),USE(?DASREVTAG:6),HIDE
                           BUTTON('sho&W tags'),AT(144,36,70,13),USE(?DASSHOWTAG:6),HIDE
                         END
                         TAB('7'),USE(?Tab7)
                           PROMPT('Select Model Number(s)'),AT(8,36),USE(?Prompt8),FONT(,12,,FONT:bold)
                           LIST,AT(56,60,152,180),USE(?List:7),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse:6)
                           BUTTON('&Tag'),AT(56,244,48,16),USE(?DASTAG:7),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All'),AT(108,244,48,16),USE(?DASTAGAll:7),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(160,244,48,16),USE(?DASUNTAGALL:7),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(128,28,50,13),USE(?DASREVTAG:7),HIDE
                           BUTTON('sho&W tags'),AT(180,28,70,13),USE(?DASSHOWTAG:7),HIDE
                         END
                         TAB('8'),USE(?Tab8)
                           PROMPT('Other Fields'),AT(8,36),USE(?Prompt9),FONT(,12,,FONT:bold)
                           PROMPT('Job Batch Number'),AT(8,60),USE(?Prompt10)
                           LIST,AT(84,60,64,10),USE(Job_Batch_Number_Temp),VSCROLL,LEFT(2),FORMAT('24L(2)|M@s9@'),DROP(10),FROM(Queue:FileDrop)
                           PROMPT('EDI Batch Number'),AT(8,80),USE(?Prompt11)
                           LIST,AT(84,80,64,10),USE(edi_batch_number_temp),VSCROLL,LEFT(2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('43L(2)|M@s9@120L(2)|M@s30@'),DROP(10,100),FROM(Queue:FileDrop:1)
                           PROMPT('Date Ranges'),AT(8,104),USE(?Prompt12),FONT(,12,,FONT:bold)
                           PROMPT('Booking Start Date'),AT(8,124),USE(?booking_start_date_temp:Prompt)
                           ENTRY(@d6b),AT(84,124,64,10),USE(booking_start_date_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,124,10,10),USE(?PopCalendar),ICON('Calenda2.ico')
                           PROMPT('Booking End Date'),AT(8,140,59,10),USE(?booking_end_date_temp:Prompt)
                           ENTRY(@d6b),AT(84,140,64,10),USE(booking_end_date_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,140,10,10),USE(?PopCalendar:2),ICON('Calenda2.ico')
                           PROMPT('Completed Start Date'),AT(8,160),USE(?completed_start_date_temp:Prompt)
                           ENTRY(@d6b),AT(84,160,64,10),USE(completed_start_date_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,160,10,10),USE(?PopCalendar:3),ICON('Calenda2.ico')
                           PROMPT('Completed End Date'),AT(8,176,67,10),USE(?completed_end_date_temp:Prompt)
                           ENTRY(@d6b),AT(84,176,64,10),USE(completed_end_date_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,176,10,10),USE(?PopCalendar:4),ICON('Calenda2.ico')
                           PROMPT('Export Order'),AT(8,192),USE(?Prompt17),FONT(,12,,FONT:bold)
                           PROMPT('Export Order'),AT(8,216),USE(?Prompt18)
                           LIST,AT(84,216,124,10),USE(export_order_temp),VSCROLL,LEFT(2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),DROP(12),FROM(export_queue_temp)
                         END
                         TAB('9'),USE(?Tab9)
                           PROMPT('Export Files'),AT(8,36),USE(?Prompt19),FONT(,12,,FONT:bold)
                           PROMPT('This Export Wizard can now create 4 export files for Jobs, Parts (Chargeable And' &|
   ' Warranty) And Audit Trail.'),AT(8,56,244,20),USE(?Prompt20),FONT(,10,,)
                           PROMPT('Please select which files you want to create, their relative paths and filenames' &|
   '.'),AT(8,92,244,20),USE(?Prompt21)
                           CHECK('Export Jobs'),AT(8,124),USE(export_jobs_temp),VALUE('YES','NO')
                           PROMPT('Export Filename'),AT(8,140),USE(?file_name:Prompt)
                           ENTRY(@s255),AT(72,140,164,10),USE(tmp:file1),FONT(,,,FONT:bold),UPR
                           BUTTON,AT(240,140,10,10),USE(?Export_jobs_lookup),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(240,176,10,10),USE(?chargeable_spares_lookup),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(240,212,10,10),USE(?warranty_parts_lookup),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(240,252,10,10),USE(?audit_trail_lookup),SKIP,HIDE,ICON('list3.ico')
                           CHECK('Export Chargeable Parts'),AT(8,160),USE(Export_Chargeable_Spares_Temp),VALUE('YES','NO')
                           PROMPT('Export Filename'),AT(8,176),USE(?file_name2:Prompt)
                           ENTRY(@s255),AT(72,176,164,10),USE(tmp:file2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Export Warranty Parts'),AT(8,196),USE(export_Warranty_Spares_temp),VALUE('YES','NO')
                           PROMPT('Export Filename'),AT(8,212),USE(?file_name3:Prompt)
                           ENTRY(@s255),AT(72,212,164,10),USE(tmp:file3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Export Audit Trail'),AT(8,236),USE(Export_Audit_Temp),VALUE('YES','NO')
                           PROMPT('Export Filename'),AT(8,252),USE(?file_name4:Prompt)
                           ENTRY(@s255),AT(72,252,164,10),USE(tmp:file4),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       BUTTON('&Export'),AT(120,288,56,16),USE(?OkButton),LEFT,ICON('thumbs.gif')
                       BUTTON('Close'),AT(196,288,56,16),USE(?Close),LEFT,ICON('cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,284,252,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,288,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                       BUTTON('&Next'),AT(64,288,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,imm,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
       button('Cancel'),at(54,44,56,16),use(?cancel),left,icon('cancel.gif')
     end
Wizard31         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW4                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW8                 CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:5
Q                      &Queue:Browse:4                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
BRW12                CLASS(BrowseClass)               !Browse using ?List:6
Q                      &Queue:Browse:5                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
BRW14                CLASS(BrowseClass)               !Browse using ?List:7
Q                      &Queue:Browse:6                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
FDB16                CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

FDB17                CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop:1              !Reference to display queue
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_job_id         ushort,auto
save_aud_id   ushort,auto
save_par_id   ushort,auto
save_wpr_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Tab2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Tab3{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Tab4{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?List:4{prop:FontColor} = 65793
    ?List:4{prop:Color}= 16777215
    ?List:4{prop:Color,2} = 16777215
    ?List:4{prop:Color,3} = 12937777
    ?Tab5{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?List:5{prop:FontColor} = 65793
    ?List:5{prop:Color}= 16777215
    ?List:5{prop:Color,2} = 16777215
    ?List:5{prop:Color,3} = 12937777
    ?Tab6{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    ?List:6{prop:FontColor} = 65793
    ?List:6{prop:Color}= 16777215
    ?List:6{prop:Color,2} = 16777215
    ?List:6{prop:Color,3} = 12937777
    ?Tab7{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?List:7{prop:FontColor} = 65793
    ?List:7{prop:Color}= 16777215
    ?List:7{prop:Color,2} = 16777215
    ?List:7{prop:Color,3} = 12937777
    ?Tab8{prop:Color} = 15066597
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    ?Job_Batch_Number_Temp{prop:FontColor} = 65793
    ?Job_Batch_Number_Temp{prop:Color}= 16777215
    ?Job_Batch_Number_Temp{prop:Color,2} = 16777215
    ?Job_Batch_Number_Temp{prop:Color,3} = 12937777
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?edi_batch_number_temp{prop:FontColor} = 65793
    ?edi_batch_number_temp{prop:Color}= 16777215
    ?edi_batch_number_temp{prop:Color,2} = 16777215
    ?edi_batch_number_temp{prop:Color,3} = 12937777
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    ?booking_start_date_temp:Prompt{prop:FontColor} = -1
    ?booking_start_date_temp:Prompt{prop:Color} = 15066597
    If ?booking_start_date_temp{prop:ReadOnly} = True
        ?booking_start_date_temp{prop:FontColor} = 65793
        ?booking_start_date_temp{prop:Color} = 15066597
    Elsif ?booking_start_date_temp{prop:Req} = True
        ?booking_start_date_temp{prop:FontColor} = 65793
        ?booking_start_date_temp{prop:Color} = 8454143
    Else ! If ?booking_start_date_temp{prop:Req} = True
        ?booking_start_date_temp{prop:FontColor} = 65793
        ?booking_start_date_temp{prop:Color} = 16777215
    End ! If ?booking_start_date_temp{prop:Req} = True
    ?booking_start_date_temp{prop:Trn} = 0
    ?booking_start_date_temp{prop:FontStyle} = font:Bold
    ?booking_end_date_temp:Prompt{prop:FontColor} = -1
    ?booking_end_date_temp:Prompt{prop:Color} = 15066597
    If ?booking_end_date_temp{prop:ReadOnly} = True
        ?booking_end_date_temp{prop:FontColor} = 65793
        ?booking_end_date_temp{prop:Color} = 15066597
    Elsif ?booking_end_date_temp{prop:Req} = True
        ?booking_end_date_temp{prop:FontColor} = 65793
        ?booking_end_date_temp{prop:Color} = 8454143
    Else ! If ?booking_end_date_temp{prop:Req} = True
        ?booking_end_date_temp{prop:FontColor} = 65793
        ?booking_end_date_temp{prop:Color} = 16777215
    End ! If ?booking_end_date_temp{prop:Req} = True
    ?booking_end_date_temp{prop:Trn} = 0
    ?booking_end_date_temp{prop:FontStyle} = font:Bold
    ?completed_start_date_temp:Prompt{prop:FontColor} = -1
    ?completed_start_date_temp:Prompt{prop:Color} = 15066597
    If ?completed_start_date_temp{prop:ReadOnly} = True
        ?completed_start_date_temp{prop:FontColor} = 65793
        ?completed_start_date_temp{prop:Color} = 15066597
    Elsif ?completed_start_date_temp{prop:Req} = True
        ?completed_start_date_temp{prop:FontColor} = 65793
        ?completed_start_date_temp{prop:Color} = 8454143
    Else ! If ?completed_start_date_temp{prop:Req} = True
        ?completed_start_date_temp{prop:FontColor} = 65793
        ?completed_start_date_temp{prop:Color} = 16777215
    End ! If ?completed_start_date_temp{prop:Req} = True
    ?completed_start_date_temp{prop:Trn} = 0
    ?completed_start_date_temp{prop:FontStyle} = font:Bold
    ?completed_end_date_temp:Prompt{prop:FontColor} = -1
    ?completed_end_date_temp:Prompt{prop:Color} = 15066597
    If ?completed_end_date_temp{prop:ReadOnly} = True
        ?completed_end_date_temp{prop:FontColor} = 65793
        ?completed_end_date_temp{prop:Color} = 15066597
    Elsif ?completed_end_date_temp{prop:Req} = True
        ?completed_end_date_temp{prop:FontColor} = 65793
        ?completed_end_date_temp{prop:Color} = 8454143
    Else ! If ?completed_end_date_temp{prop:Req} = True
        ?completed_end_date_temp{prop:FontColor} = 65793
        ?completed_end_date_temp{prop:Color} = 16777215
    End ! If ?completed_end_date_temp{prop:Req} = True
    ?completed_end_date_temp{prop:Trn} = 0
    ?completed_end_date_temp{prop:FontStyle} = font:Bold
    ?Prompt17{prop:FontColor} = -1
    ?Prompt17{prop:Color} = 15066597
    ?Prompt18{prop:FontColor} = -1
    ?Prompt18{prop:Color} = 15066597
    ?export_order_temp{prop:FontColor} = 65793
    ?export_order_temp{prop:Color}= 16777215
    ?export_order_temp{prop:Color,2} = 16777215
    ?export_order_temp{prop:Color,3} = 12937777
    ?Tab9{prop:Color} = 15066597
    ?Prompt19{prop:FontColor} = -1
    ?Prompt19{prop:Color} = 15066597
    ?Prompt20{prop:FontColor} = -1
    ?Prompt20{prop:Color} = 15066597
    ?Prompt21{prop:FontColor} = -1
    ?Prompt21{prop:Color} = 15066597
    ?export_jobs_temp{prop:Font,3} = -1
    ?export_jobs_temp{prop:Color} = 15066597
    ?export_jobs_temp{prop:Trn} = 0
    ?file_name:Prompt{prop:FontColor} = -1
    ?file_name:Prompt{prop:Color} = 15066597
    If ?tmp:file1{prop:ReadOnly} = True
        ?tmp:file1{prop:FontColor} = 65793
        ?tmp:file1{prop:Color} = 15066597
    Elsif ?tmp:file1{prop:Req} = True
        ?tmp:file1{prop:FontColor} = 65793
        ?tmp:file1{prop:Color} = 8454143
    Else ! If ?tmp:file1{prop:Req} = True
        ?tmp:file1{prop:FontColor} = 65793
        ?tmp:file1{prop:Color} = 16777215
    End ! If ?tmp:file1{prop:Req} = True
    ?tmp:file1{prop:Trn} = 0
    ?tmp:file1{prop:FontStyle} = font:Bold
    ?Export_Chargeable_Spares_Temp{prop:Font,3} = -1
    ?Export_Chargeable_Spares_Temp{prop:Color} = 15066597
    ?Export_Chargeable_Spares_Temp{prop:Trn} = 0
    ?file_name2:Prompt{prop:FontColor} = -1
    ?file_name2:Prompt{prop:Color} = 15066597
    If ?tmp:file2{prop:ReadOnly} = True
        ?tmp:file2{prop:FontColor} = 65793
        ?tmp:file2{prop:Color} = 15066597
    Elsif ?tmp:file2{prop:Req} = True
        ?tmp:file2{prop:FontColor} = 65793
        ?tmp:file2{prop:Color} = 8454143
    Else ! If ?tmp:file2{prop:Req} = True
        ?tmp:file2{prop:FontColor} = 65793
        ?tmp:file2{prop:Color} = 16777215
    End ! If ?tmp:file2{prop:Req} = True
    ?tmp:file2{prop:Trn} = 0
    ?tmp:file2{prop:FontStyle} = font:Bold
    ?export_Warranty_Spares_temp{prop:Font,3} = -1
    ?export_Warranty_Spares_temp{prop:Color} = 15066597
    ?export_Warranty_Spares_temp{prop:Trn} = 0
    ?file_name3:Prompt{prop:FontColor} = -1
    ?file_name3:Prompt{prop:Color} = 15066597
    If ?tmp:file3{prop:ReadOnly} = True
        ?tmp:file3{prop:FontColor} = 65793
        ?tmp:file3{prop:Color} = 15066597
    Elsif ?tmp:file3{prop:Req} = True
        ?tmp:file3{prop:FontColor} = 65793
        ?tmp:file3{prop:Color} = 8454143
    Else ! If ?tmp:file3{prop:Req} = True
        ?tmp:file3{prop:FontColor} = 65793
        ?tmp:file3{prop:Color} = 16777215
    End ! If ?tmp:file3{prop:Req} = True
    ?tmp:file3{prop:Trn} = 0
    ?tmp:file3{prop:FontStyle} = font:Bold
    ?Export_Audit_Temp{prop:Font,3} = -1
    ?Export_Audit_Temp{prop:Color} = 15066597
    ?Export_Audit_Temp{prop:Trn} = 0
    ?file_name4:Prompt{prop:FontColor} = -1
    ?file_name4:Prompt{prop:Color} = 15066597
    If ?tmp:file4{prop:ReadOnly} = True
        ?tmp:file4{prop:FontColor} = 65793
        ?tmp:file4{prop:Color} = 15066597
    Elsif ?tmp:file4{prop:Req} = True
        ?tmp:file4{prop:FontColor} = 65793
        ?tmp:file4{prop:Color} = 8454143
    Else ! If ?tmp:file4{prop:Req} = True
        ?tmp:file4{prop:FontColor} = 65793
        ?tmp:file4{prop:Color} = 16777215
    End ! If ?tmp:file4{prop:Req} = True
    ?tmp:file4{prop:Trn} = 0
    ?tmp:file4{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::3:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW2.UpdateBuffer
   GLO:Queue.Pointer = sub:Account_Number
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = sub:Account_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
    account_tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    account_tag_temp = ''
  END
    Queue:Browse.account_tag_temp = account_tag_temp
  IF (account_tag_temp = '*')
    Queue:Browse.account_tag_temp_Icon = 2
  ELSE
    Queue:Browse.account_tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW2.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = sub:Account_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW2.Reset
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::3:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::3:QUEUE = GLO:Queue
    ADD(DASBRW::3:QUEUE)
  END
  FREE(GLO:Queue)
  BRW2.Reset
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::3:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::3:QUEUE,DASBRW::3:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = sub:Account_Number
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASSHOWTAG Routine
   CASE DASBRW::3:TAGDISPSTATUS
   OF 0
      DASBRW::3:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::3:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::3:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW2.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW4.UpdateBuffer
   GLO:Queue2.Pointer2 = cha:Charge_Type
   GET(GLO:Queue2,GLO:Queue2.Pointer2)
  IF ERRORCODE()
     GLO:Queue2.Pointer2 = cha:Charge_Type
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    charge_type_tag_temp = '*'
  ELSE
    DELETE(GLO:Queue2)
    charge_type_tag_temp = ''
  END
    Queue:Browse:1.charge_type_tag_temp = charge_type_tag_temp
  IF (charge_type_tag_temp = '*')
    Queue:Browse:1.charge_type_tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.charge_type_tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::5:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(GLO:Queue2)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue2.Pointer2 = cha:Charge_Type
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::5:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue2)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::5:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue2)
    GET(GLO:Queue2,QR#)
    DASBRW::5:QUEUE = GLO:Queue2
    ADD(DASBRW::5:QUEUE)
  END
  FREE(GLO:Queue2)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer2 = cha:Charge_Type
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer2)
    IF ERRORCODE()
       GLO:Queue2.Pointer2 = cha:Charge_Type
       ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW6.UpdateBuffer
   GLO:Queue3.Pointer3 = cha:Charge_Type
   GET(GLO:Queue3,GLO:Queue3.Pointer3)
  IF ERRORCODE()
     GLO:Queue3.Pointer3 = cha:Charge_Type
     ADD(GLO:Queue3,GLO:Queue3.Pointer3)
    warranty_type_tag_temp = '*'
  ELSE
    DELETE(GLO:Queue3)
    warranty_type_tag_temp = ''
  END
    Queue:Browse:2.warranty_type_tag_temp = warranty_type_tag_temp
  IF (warranty_type_tag_temp = '*')
    Queue:Browse:2.warranty_type_tag_temp_Icon = 2
  ELSE
    Queue:Browse:2.warranty_type_tag_temp_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(GLO:Queue3)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue3.Pointer3 = cha:Charge_Type
     ADD(GLO:Queue3,GLO:Queue3.Pointer3)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue3)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue3)
    GET(GLO:Queue3,QR#)
    DASBRW::7:QUEUE = GLO:Queue3
    ADD(DASBRW::7:QUEUE)
  END
  FREE(GLO:Queue3)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Pointer3 = cha:Charge_Type
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Pointer3)
    IF ERRORCODE()
       GLO:Queue3.Pointer3 = cha:Charge_Type
       ADD(GLO:Queue3,GLO:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse:3,CHOICE(?List:4))
  BRW8.UpdateBuffer
   GLO:Queue4.Pointer4 = sts:Status
   GET(GLO:Queue4,GLO:Queue4.Pointer4)
  IF ERRORCODE()
     GLO:Queue4.Pointer4 = sts:Status
     ADD(GLO:Queue4,GLO:Queue4.Pointer4)
    status_tag_temp = '*'
  ELSE
    DELETE(GLO:Queue4)
    status_tag_temp = ''
  END
    Queue:Browse:3.status_tag_temp = status_tag_temp
  IF (status_tag_temp = '*')
    Queue:Browse:3.status_tag_temp_Icon = 2
  ELSE
    Queue:Browse:3.status_tag_temp_Icon = 1
  END
  PUT(Queue:Browse:3)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::9:DASTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(GLO:Queue4)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue4.Pointer4 = sts:Status
     ADD(GLO:Queue4,GLO:Queue4.Pointer4)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::9:DASUNTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue4)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::9:DASREVTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue4)
    GET(GLO:Queue4,QR#)
    DASBRW::9:QUEUE = GLO:Queue4
    ADD(DASBRW::9:QUEUE)
  END
  FREE(GLO:Queue4)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer4 = sts:Status
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer4)
    IF ERRORCODE()
       GLO:Queue4.Pointer4 = sts:Status
       ADD(GLO:Queue4,GLO:Queue4.Pointer4)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:4{PROP:Text} = 'Show All'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:4{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List:4,CHOICE(?List:4))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::11:DASTAGONOFF Routine
  GET(Queue:Browse:4,CHOICE(?List:5))
  BRW10.UpdateBuffer
   GLO:Queue5.Pointer5 = tur:Turnaround_Time
   GET(GLO:Queue5,GLO:Queue5.Pointer5)
  IF ERRORCODE()
     GLO:Queue5.Pointer5 = tur:Turnaround_Time
     ADD(GLO:Queue5,GLO:Queue5.Pointer5)
    turnaround_tag_temp = '*'
  ELSE
    DELETE(GLO:Queue5)
    turnaround_tag_temp = ''
  END
    Queue:Browse:4.turnaround_tag_temp = turnaround_tag_temp
  IF (turnaround_tag_temp = '*')
    Queue:Browse:4.turnaround_tag_temp_Icon = 2
  ELSE
    Queue:Browse:4.turnaround_tag_temp_Icon = 1
  END
  PUT(Queue:Browse:4)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::11:DASTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW10.Reset
  FREE(GLO:Queue5)
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue5.Pointer5 = tur:Turnaround_Time
     ADD(GLO:Queue5,GLO:Queue5.Pointer5)
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::11:DASUNTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue5)
  BRW10.Reset
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::11:DASREVTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::11:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue5)
    GET(GLO:Queue5,QR#)
    DASBRW::11:QUEUE = GLO:Queue5
    ADD(DASBRW::11:QUEUE)
  END
  FREE(GLO:Queue5)
  BRW10.Reset
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::11:QUEUE.Pointer5 = tur:Turnaround_Time
     GET(DASBRW::11:QUEUE,DASBRW::11:QUEUE.Pointer5)
    IF ERRORCODE()
       GLO:Queue5.Pointer5 = tur:Turnaround_Time
       ADD(GLO:Queue5,GLO:Queue5.Pointer5)
    END
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::11:DASSHOWTAG Routine
   CASE DASBRW::11:TAGDISPSTATUS
   OF 0
      DASBRW::11:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::11:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::11:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:5{PROP:Text} = 'Show All'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:5{PROP:Text})
   BRW10.ResetSort(1)
   SELECT(?List:5,CHOICE(?List:5))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse:5,CHOICE(?List:6))
  BRW12.UpdateBuffer
   GLO:Queue6.Pointer6 = use:User_Code
   GET(GLO:Queue6,GLO:Queue6.Pointer6)
  IF ERRORCODE()
     GLO:Queue6.Pointer6 = use:User_Code
     ADD(GLO:Queue6,GLO:Queue6.Pointer6)
    engineer_tag_temp = '*'
  ELSE
    DELETE(GLO:Queue6)
    engineer_tag_temp = ''
  END
    Queue:Browse:5.engineer_tag_temp = engineer_tag_temp
  IF (engineer_tag_temp = '*')
    Queue:Browse:5.engineer_tag_temp_Icon = 2
  ELSE
    Queue:Browse:5.engineer_tag_temp_Icon = 1
  END
  PUT(Queue:Browse:5)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::13:DASTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(GLO:Queue6)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue6.Pointer6 = use:User_Code
     ADD(GLO:Queue6,GLO:Queue6.Pointer6)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::13:DASUNTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue6)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::13:DASREVTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue6)
    GET(GLO:Queue6,QR#)
    DASBRW::13:QUEUE = GLO:Queue6
    ADD(DASBRW::13:QUEUE)
  END
  FREE(GLO:Queue6)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer6 = use:User_Code
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer6)
    IF ERRORCODE()
       GLO:Queue6.Pointer6 = use:User_Code
       ADD(GLO:Queue6,GLO:Queue6.Pointer6)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:6{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:6{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:6{PROP:Text} = 'Show All'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:6{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List:6,CHOICE(?List:6))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::15:DASTAGONOFF Routine
  GET(Queue:Browse:6,CHOICE(?List:7))
  BRW14.UpdateBuffer
   GLO:Queue7.Pointer7 = mod:Model_Number
   GET(GLO:Queue7,GLO:Queue7.Pointer7)
  IF ERRORCODE()
     GLO:Queue7.Pointer7 = mod:Model_Number
     ADD(GLO:Queue7,GLO:Queue7.Pointer7)
    model_tag_temp = '*'
  ELSE
    DELETE(GLO:Queue7)
    model_tag_temp = ''
  END
    Queue:Browse:6.model_tag_temp = model_tag_temp
  IF (model_tag_temp = '*')
    Queue:Browse:6.model_tag_temp_Icon = 2
  ELSE
    Queue:Browse:6.model_tag_temp_Icon = 1
  END
  PUT(Queue:Browse:6)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::15:DASTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW14.Reset
  FREE(GLO:Queue7)
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue7.Pointer7 = mod:Model_Number
     ADD(GLO:Queue7,GLO:Queue7.Pointer7)
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::15:DASUNTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue7)
  BRW14.Reset
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::15:DASREVTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::15:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue7)
    GET(GLO:Queue7,QR#)
    DASBRW::15:QUEUE = GLO:Queue7
    ADD(DASBRW::15:QUEUE)
  END
  FREE(GLO:Queue7)
  BRW14.Reset
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::15:QUEUE.Pointer7 = mod:Model_Number
     GET(DASBRW::15:QUEUE,DASBRW::15:QUEUE.Pointer7)
    IF ERRORCODE()
       GLO:Queue7.Pointer7 = mod:Model_Number
       ADD(GLO:Queue7,GLO:Queue7.Pointer7)
    END
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::15:DASSHOWTAG Routine
   CASE DASBRW::15:TAGDISPSTATUS
   OF 0
      DASBRW::15:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:7{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::15:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:7{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::15:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:7{PROP:Text} = 'Show All'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:7{PROP:Text})
   BRW14.ResetSort(1)
   SELECT(?List:7,CHOICE(?List:7))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        display()
      end
    end

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        case event()
            of event:timer
                break
            of event:closewindow
                cancel# = 1
                break
            of event:accepted
                if field() = ?cancel
                    cancel# = 1
                    break
                end!if field() = ?button1
        end!case event()
    end!accept
    if cancel# = 1
        case messageex('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,charset:ansi,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            of 1 ! &yes button
                tmp:cancel = 1
            of 2 ! &no button
        end!case messageex
    end!if cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Data_Export_Wizard',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Data_Export_Wizard',1)
    SolaceViewVars('tmp:file1',tmp:file1,'Data_Export_Wizard',1)
    SolaceViewVars('tmp:file2',tmp:file2,'Data_Export_Wizard',1)
    SolaceViewVars('tmp:file3',tmp:file3,'Data_Export_Wizard',1)
    SolaceViewVars('tmp:file4',tmp:file4,'Data_Export_Wizard',1)
    SolaceViewVars('TabNumber',TabNumber,'Data_Export_Wizard',1)
    SolaceViewVars('MaxTabs',MaxTabs,'Data_Export_Wizard',1)
    SolaceViewVars('account_tag_temp',account_tag_temp,'Data_Export_Wizard',1)
    SolaceViewVars('charge_type_tag_temp',charge_type_tag_temp,'Data_Export_Wizard',1)
    SolaceViewVars('warranty_type_tag_temp',warranty_type_tag_temp,'Data_Export_Wizard',1)
    SolaceViewVars('status_tag_temp',status_tag_temp,'Data_Export_Wizard',1)
    SolaceViewVars('turnaround_tag_temp',turnaround_tag_temp,'Data_Export_Wizard',1)
    SolaceViewVars('DisplayString',DisplayString,'Data_Export_Wizard',1)
    SolaceViewVars('engineer_tag_temp',engineer_tag_temp,'Data_Export_Wizard',1)
    SolaceViewVars('model_tag_temp',model_tag_temp,'Data_Export_Wizard',1)
    SolaceViewVars('Job_Batch_Number_Temp',Job_Batch_Number_Temp,'Data_Export_Wizard',1)
    SolaceViewVars('edi_batch_number_temp',edi_batch_number_temp,'Data_Export_Wizard',1)
    SolaceViewVars('booking_start_date_temp',booking_start_date_temp,'Data_Export_Wizard',1)
    SolaceViewVars('booking_end_date_temp',booking_end_date_temp,'Data_Export_Wizard',1)
    SolaceViewVars('completed_start_date_temp',completed_start_date_temp,'Data_Export_Wizard',1)
    SolaceViewVars('completed_end_date_temp',completed_end_date_temp,'Data_Export_Wizard',1)
    SolaceViewVars('export_order_temp',export_order_temp,'Data_Export_Wizard',1)
    SolaceViewVars('export_jobs_temp',export_jobs_temp,'Data_Export_Wizard',1)
    SolaceViewVars('Export_Chargeable_Spares_Temp',Export_Chargeable_Spares_Temp,'Data_Export_Wizard',1)
    SolaceViewVars('export_Warranty_Spares_temp',export_Warranty_Spares_temp,'Data_Export_Wizard',1)
    SolaceViewVars('Export_Audit_Temp',Export_Audit_Temp,'Data_Export_Wizard',1)
    SolaceViewVars('export_queue_temp:order_temp',export_queue_temp:order_temp,'Data_Export_Wizard',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:2;  SolaceCtrlName = '?DASTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:2;  SolaceCtrlName = '?DASREVTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:2;  SolaceCtrlName = '?DASTAGAll:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:2;  SolaceCtrlName = '?DASUNTAGALL:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:2;  SolaceCtrlName = '?DASSHOWTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:3;  SolaceCtrlName = '?DASTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:3;  SolaceCtrlName = '?DASTAGAll:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:3;  SolaceCtrlName = '?DASUNTAGALL:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:3;  SolaceCtrlName = '?DASREVTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:3;  SolaceCtrlName = '?DASSHOWTAG:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:4;  SolaceCtrlName = '?List:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:4;  SolaceCtrlName = '?DASTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:4;  SolaceCtrlName = '?DASTAGAll:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:4;  SolaceCtrlName = '?DASUNTAGALL:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:4;  SolaceCtrlName = '?DASREVTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:4;  SolaceCtrlName = '?DASSHOWTAG:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:5;  SolaceCtrlName = '?List:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:5;  SolaceCtrlName = '?DASTAG:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:5;  SolaceCtrlName = '?DASTAGAll:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:5;  SolaceCtrlName = '?DASUNTAGALL:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:5;  SolaceCtrlName = '?DASREVTAG:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:5;  SolaceCtrlName = '?DASSHOWTAG:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:6;  SolaceCtrlName = '?List:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:6;  SolaceCtrlName = '?DASTAG:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:6;  SolaceCtrlName = '?DASTAGAll:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:6;  SolaceCtrlName = '?DASUNTAGALL:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:6;  SolaceCtrlName = '?DASREVTAG:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:6;  SolaceCtrlName = '?DASSHOWTAG:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab7;  SolaceCtrlName = '?Tab7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:7;  SolaceCtrlName = '?List:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:7;  SolaceCtrlName = '?DASTAG:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:7;  SolaceCtrlName = '?DASTAGAll:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:7;  SolaceCtrlName = '?DASUNTAGALL:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:7;  SolaceCtrlName = '?DASREVTAG:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:7;  SolaceCtrlName = '?DASSHOWTAG:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab8;  SolaceCtrlName = '?Tab8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job_Batch_Number_Temp;  SolaceCtrlName = '?Job_Batch_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?edi_batch_number_temp;  SolaceCtrlName = '?edi_batch_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12;  SolaceCtrlName = '?Prompt12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?booking_start_date_temp:Prompt;  SolaceCtrlName = '?booking_start_date_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?booking_start_date_temp;  SolaceCtrlName = '?booking_start_date_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?booking_end_date_temp:Prompt;  SolaceCtrlName = '?booking_end_date_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?booking_end_date_temp;  SolaceCtrlName = '?booking_end_date_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?completed_start_date_temp:Prompt;  SolaceCtrlName = '?completed_start_date_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?completed_start_date_temp;  SolaceCtrlName = '?completed_start_date_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?completed_end_date_temp:Prompt;  SolaceCtrlName = '?completed_end_date_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?completed_end_date_temp;  SolaceCtrlName = '?completed_end_date_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17;  SolaceCtrlName = '?Prompt17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt18;  SolaceCtrlName = '?Prompt18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?export_order_temp;  SolaceCtrlName = '?export_order_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab9;  SolaceCtrlName = '?Tab9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt19;  SolaceCtrlName = '?Prompt19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt20;  SolaceCtrlName = '?Prompt20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt21;  SolaceCtrlName = '?Prompt21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?export_jobs_temp;  SolaceCtrlName = '?export_jobs_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?file_name:Prompt;  SolaceCtrlName = '?file_name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:file1;  SolaceCtrlName = '?tmp:file1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Export_jobs_lookup;  SolaceCtrlName = '?Export_jobs_lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?chargeable_spares_lookup;  SolaceCtrlName = '?chargeable_spares_lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?warranty_parts_lookup;  SolaceCtrlName = '?warranty_parts_lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?audit_trail_lookup;  SolaceCtrlName = '?audit_trail_lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Export_Chargeable_Spares_Temp;  SolaceCtrlName = '?Export_Chargeable_Spares_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?file_name2:Prompt;  SolaceCtrlName = '?file_name2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:file2;  SolaceCtrlName = '?tmp:file2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?export_Warranty_Spares_temp;  SolaceCtrlName = '?export_Warranty_Spares_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?file_name3:Prompt;  SolaceCtrlName = '?file_name3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:file3;  SolaceCtrlName = '?tmp:file3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Export_Audit_Temp;  SolaceCtrlName = '?Export_Audit_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?file_name4:Prompt;  SolaceCtrlName = '?file_name4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:file4;  SolaceCtrlName = '?tmp:file4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSBackButton;  SolaceCtrlName = '?VSBackButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSNextButton;  SolaceCtrlName = '?VSNextButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Data_Export_Wizard')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Data_Export_Wizard')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:EDIBATCH.Open
  Relate:JOBBATCH.Open
  Relate:STATUS.Open
  Relate:TURNARND.Open
  Access:JOBS.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  tmp:file1 = Upper(Clip(Path()) & '\JOBSDATA.CSV')
  tmp:file2 = Upper(Clip(Path()) & '\CHARPART.CSV')
  tmp:file3 = Upper(Clip(Path()) & '\WARRPART.CSV')
  tmp:file4 = Upper(Clip(Path()) & '\AUDDATA.CSV')
  
  Clear(export_queue_temp)
  Free(export_queue_temp)
  order_temp = 'JOB NUMBER'
  Add(export_queue_temp)
  order_temp = 'E.S.N. / I.M.E.I.'
  Add(export_queue_temp)
  order_temp = 'ACCOUNT NUMBER'
  Add(export_queue_temp)
  order_temp = 'MODEL NUMBER'
  Add(export_queue_temp)
  order_temp = 'STATUS'
  Add(export_queue_temp)
  order_temp = 'MSN'
  Add(export_queue_temp)
  order_temp = 'SURNAME'
  Add(export_queue_temp)
  order_temp = 'ENGINEER'
  Add(export_queue_temp)
  order_temp = 'MOBILE NUMBER'
  Add(export_queue_temp)
  order_temp = 'DATE BOOKED'
  Add(export_queue_temp)
  order_temp = 'DATE COMPLETED'
  Add(export_queue_temp)
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  BRW4.Init(?List:2,Queue:Browse:1.ViewPosition,BRW4::View:Browse,Queue:Browse:1,Relate:CHARTYPE,SELF)
  BRW6.Init(?List:3,Queue:Browse:2.ViewPosition,BRW6::View:Browse,Queue:Browse:2,Relate:CHARTYPE,SELF)
  BRW8.Init(?List:4,Queue:Browse:3.ViewPosition,BRW8::View:Browse,Queue:Browse:3,Relate:STATUS,SELF)
  BRW10.Init(?List:5,Queue:Browse:4.ViewPosition,BRW10::View:Browse,Queue:Browse:4,Relate:TURNARND,SELF)
  BRW12.Init(?List:6,Queue:Browse:5.ViewPosition,BRW12::View:Browse,Queue:Browse:5,Relate:USERS,SELF)
  BRW14.Init(?List:7,Queue:Browse:6.ViewPosition,BRW14::View:Browse,Queue:Browse:6,Relate:MODELNUM,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:5{prop:vcr} = TRUE
  ?List:6{prop:vcr} = TRUE
  ?List:7{prop:vcr} = TRUE
  ?Job_Batch_Number_Temp{prop:vcr} = TRUE
  ?edi_batch_number_temp{prop:vcr} = TRUE
  ?export_order_temp{prop:vcr} = TRUE
    Wizard31.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?OkButton, |                     ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?booking_start_date_temp{Prop:Alrt,255} = MouseLeft2
  ?booking_end_date_temp{Prop:Alrt,255} = MouseLeft2
  ?completed_start_date_temp{Prop:Alrt,255} = MouseLeft2
  ?completed_end_date_temp{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW2.Q &= Queue:Browse
  BRW2.RetainRow = 0
  BRW2.AddSortOrder(,sub:Account_Number_Key)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,sub:Account_Number,1,BRW2)
  BIND('account_tag_temp',account_tag_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW2.AddField(account_tag_temp,BRW2.Q.account_tag_temp)
  BRW2.AddField(sub:Account_Number,BRW2.Q.sub:Account_Number)
  BRW2.AddField(sub:Company_Name,BRW2.Q.sub:Company_Name)
  BRW2.AddField(sub:RecordNumber,BRW2.Q.sub:RecordNumber)
  BRW4.Q &= Queue:Browse:1
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,cha:Warranty_Ref_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,cha:Warranty,1,BRW4)
  BRW4.SetFilter('(Upper(cha:warranty) = ''NO'')')
  BIND('charge_type_tag_temp',charge_type_tag_temp)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(charge_type_tag_temp,BRW4.Q.charge_type_tag_temp)
  BRW4.AddField(cha:Charge_Type,BRW4.Q.cha:Charge_Type)
  BRW4.AddField(cha:Warranty,BRW4.Q.cha:Warranty)
  BRW4.AddField(cha:Ref_Number,BRW4.Q.cha:Ref_Number)
  BRW6.Q &= Queue:Browse:2
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,cha:Warranty_Ref_Number_Key)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,cha:Warranty,1,BRW6)
  BRW6.SetFilter('(Upper(cha:Warranty) = ''YES'')')
  BIND('warranty_type_tag_temp',warranty_type_tag_temp)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(warranty_type_tag_temp,BRW6.Q.warranty_type_tag_temp)
  BRW6.AddField(cha:Charge_Type,BRW6.Q.cha:Charge_Type)
  BRW6.AddField(cha:Warranty,BRW6.Q.cha:Warranty)
  BRW6.AddField(cha:Ref_Number,BRW6.Q.cha:Ref_Number)
  BRW8.Q &= Queue:Browse:3
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,sts:Status_Key)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,sts:Status,1,BRW8)
  BIND('status_tag_temp',status_tag_temp)
  ?List:4{PROP:IconList,1} = '~notick1.ico'
  ?List:4{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(status_tag_temp,BRW8.Q.status_tag_temp)
  BRW8.AddField(sts:Status,BRW8.Q.sts:Status)
  BRW8.AddField(sts:Ref_Number,BRW8.Q.sts:Ref_Number)
  BRW10.Q &= Queue:Browse:4
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,tur:Turnaround_Time_Key)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,tur:Turnaround_Time,1,BRW10)
  BIND('turnaround_tag_temp',turnaround_tag_temp)
  ?List:5{PROP:IconList,1} = '~notick1.ico'
  ?List:5{PROP:IconList,2} = '~tick1.ico'
  BRW10.AddField(turnaround_tag_temp,BRW10.Q.turnaround_tag_temp)
  BRW10.AddField(tur:Turnaround_Time,BRW10.Q.tur:Turnaround_Time)
  BRW10.AddField(tur:Days,BRW10.Q.tur:Days)
  BRW10.AddField(tur:Hours,BRW10.Q.tur:Hours)
  BRW12.Q &= Queue:Browse:5
  BRW12.RetainRow = 0
  BRW12.AddSortOrder(,use:User_Type_Key)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,use:User_Type,1,BRW12)
  BIND('engineer_tag_temp',engineer_tag_temp)
  ?List:6{PROP:IconList,1} = '~notick1.ico'
  ?List:6{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(engineer_tag_temp,BRW12.Q.engineer_tag_temp)
  BRW12.AddField(use:Surname,BRW12.Q.use:Surname)
  BRW12.AddField(use:Forename,BRW12.Q.use:Forename)
  BRW12.AddField(use:User_Code,BRW12.Q.use:User_Code)
  BRW12.AddField(use:User_Type,BRW12.Q.use:User_Type)
  BRW14.Q &= Queue:Browse:6
  BRW14.RetainRow = 0
  BRW14.AddSortOrder(,mod:Model_Number_Key)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,mod:Model_Number,1,BRW14)
  BIND('model_tag_temp',model_tag_temp)
  ?List:7{PROP:IconList,1} = '~notick1.ico'
  ?List:7{PROP:IconList,2} = '~tick1.ico'
  BRW14.AddField(model_tag_temp,BRW14.Q.model_tag_temp)
  BRW14.AddField(mod:Model_Number,BRW14.Q.mod:Model_Number)
  IF ?export_jobs_temp{Prop:Checked} = True
    UNHIDE(?file_name:Prompt)
    UNHIDE(?tmp:file1)
    UNHIDE(?export_jobs_lookup)
  END
  IF ?export_jobs_temp{Prop:Checked} = False
    HIDE(?file_name:Prompt)
    HIDE(?tmp:file1)
    HIDE(?export_jobs_lookup)
  END
  IF ?Export_Chargeable_Spares_Temp{Prop:Checked} = True
    UNHIDE(?file_name2:Prompt)
    UNHIDE(?tmp:file2)
    UNHIDE(?chargeable_spares_lookup)
  END
  IF ?Export_Chargeable_Spares_Temp{Prop:Checked} = False
    HIDE(?file_name2:Prompt)
    HIDE(?tmp:file2)
    HIDE(?chargeable_spares_lookup)
  END
  IF ?export_Warranty_Spares_temp{Prop:Checked} = True
    UNHIDE(?file_name3:Prompt)
    UNHIDE(?tmp:file3)
    UNHIDE(?Warranty_Parts_Lookup)
  END
  IF ?export_Warranty_Spares_temp{Prop:Checked} = False
    HIDE(?file_name3:Prompt)
    HIDE(?tmp:file3)
    HIDE(?Warranty_Parts_Lookup)
  END
  IF ?Export_Audit_Temp{Prop:Checked} = True
    UNHIDE(?file_name4:Prompt)
    UNHIDE(?tmp:file4)
    UNHIDE(?audit_trail_lookup)
  END
  IF ?Export_Audit_Temp{Prop:Checked} = False
    HIDE(?file_name4:Prompt)
    HIDE(?tmp:file4)
    HIDE(?audit_trail_lookup)
  END
  FDB16.Init(?Job_Batch_Number_Temp,Queue:FileDrop.ViewPosition,FDB16::View:FileDrop,Queue:FileDrop,Relate:JOBBATCH,ThisWindow)
  FDB16.Q &= Queue:FileDrop
  FDB16.AddSortOrder(jbt:Batch_Number_Key)
  FDB16.AddField(jbt:Batch_Number,FDB16.Q.jbt:Batch_Number)
  ThisWindow.AddItem(FDB16.WindowComponent)
  FDB16.DefaultFill = 0
  FDB17.Init(?edi_batch_number_temp,Queue:FileDrop:1.ViewPosition,FDB17::View:FileDrop,Queue:FileDrop:1,Relate:EDIBATCH,ThisWindow)
  FDB17.Q &= Queue:FileDrop:1
  FDB17.AddSortOrder(ebt:Batch_Number_Key)
  FDB17.AddField(ebt:Batch_Number,FDB17.Q.ebt:Batch_Number)
  FDB17.AddField(ebt:Manufacturer,FDB17.Q.ebt:Manufacturer)
  ThisWindow.AddItem(FDB17.WindowComponent)
  FDB17.DefaultFill = 0
  BRW2.AddToolbarTarget(Toolbar)
  BRW4.AddToolbarTarget(Toolbar)
  BRW6.AddToolbarTarget(Toolbar)
  BRW8.AddToolbarTarget(Toolbar)
  BRW10.AddToolbarTarget(Toolbar)
  BRW12.AddToolbarTarget(Toolbar)
  BRW14.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue4)
  ?DASSHOWTAG:4{PROP:Text} = 'Show All'
  ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue5)
  ?DASSHOWTAG:5{PROP:Text} = 'Show All'
  ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue6)
  ?DASSHOWTAG:6{PROP:Text} = 'Show All'
  ?DASSHOWTAG:6{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:6{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue7)
  ?DASSHOWTAG:7{PROP:Text} = 'Show All'
  ?DASSHOWTAG:7{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:7{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue3)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue4)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue5)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue6)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue7)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:EDIBATCH.Close
    Relate:JOBBATCH.Close
    Relate:STATUS.Close
    Relate:TURNARND.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Data_Export_Wizard',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard31.Validate()
        DISABLE(Wizard31.NextControl())
     ELSE
        ENABLE(Wizard31.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          booking_start_date_temp = TINCALENDARStyle1(booking_start_date_temp)
          Display(?booking_start_date_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          booking_end_date_temp = TINCALENDARStyle1(booking_end_date_temp)
          Display(?booking_end_date_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          completed_start_date_temp = TINCALENDARStyle1(completed_start_date_temp)
          Display(?completed_start_date_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          completed_end_date_temp = TINCALENDARStyle1(completed_end_date_temp)
          Display(?completed_end_date_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?export_jobs_temp
      IF ?export_jobs_temp{Prop:Checked} = True
        UNHIDE(?file_name:Prompt)
        UNHIDE(?tmp:file1)
        UNHIDE(?export_jobs_lookup)
      END
      IF ?export_jobs_temp{Prop:Checked} = False
        HIDE(?file_name:Prompt)
        HIDE(?tmp:file1)
        HIDE(?export_jobs_lookup)
      END
      ThisWindow.Reset
    OF ?Export_jobs_lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export_jobs_lookup, Accepted)
      filedialog ('Choose File',tmp:file1,'CSV Files|*.*|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
      tmp:file1 = upper(tmp:file1)
      display(?tmp:file1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export_jobs_lookup, Accepted)
    OF ?chargeable_spares_lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?chargeable_spares_lookup, Accepted)
      filedialog ('Choose File',tmp:file2,'CSV Files|*.*|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
      tmp:file2 = upper(tmp:file2)
      display(?tmp:file2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?chargeable_spares_lookup, Accepted)
    OF ?warranty_parts_lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?warranty_parts_lookup, Accepted)
      filedialog ('Choose File',tmp:file3,'CSV Files|*.*|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
      tmp:file3 = upper(tmp:file3)
      display(?tmp:file3)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?warranty_parts_lookup, Accepted)
    OF ?audit_trail_lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?audit_trail_lookup, Accepted)
      filedialog ('Choose File',tmp:file4,'CSV Files|*.*|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
      tmp:file4 = upper(tmp:file4)
      display(?tmp:file4)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?audit_trail_lookup, Accepted)
    OF ?Export_Chargeable_Spares_Temp
      IF ?Export_Chargeable_Spares_Temp{Prop:Checked} = True
        UNHIDE(?file_name2:Prompt)
        UNHIDE(?tmp:file2)
        UNHIDE(?chargeable_spares_lookup)
      END
      IF ?Export_Chargeable_Spares_Temp{Prop:Checked} = False
        HIDE(?file_name2:Prompt)
        HIDE(?tmp:file2)
        HIDE(?chargeable_spares_lookup)
      END
      ThisWindow.Reset
    OF ?export_Warranty_Spares_temp
      IF ?export_Warranty_Spares_temp{Prop:Checked} = True
        UNHIDE(?file_name3:Prompt)
        UNHIDE(?tmp:file3)
        UNHIDE(?Warranty_Parts_Lookup)
      END
      IF ?export_Warranty_Spares_temp{Prop:Checked} = False
        HIDE(?file_name3:Prompt)
        HIDE(?tmp:file3)
        HIDE(?Warranty_Parts_Lookup)
      END
      ThisWindow.Reset
    OF ?Export_Audit_Temp
      IF ?Export_Audit_Temp{Prop:Checked} = True
        UNHIDE(?file_name4:Prompt)
        UNHIDE(?tmp:file4)
        UNHIDE(?audit_trail_lookup)
      END
      IF ?Export_Audit_Temp{Prop:Checked} = False
        HIDE(?file_name4:Prompt)
        HIDE(?tmp:file4)
        HIDE(?audit_trail_lookup)
      END
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      exit# = 0
      beep(beep:systemquestion)  ;  yield()
      case message('Are you sure you want to begin the Export Routine?', |
              'ServiceBase 2000', icon:question, |
               button:yes+button:no, button:no, 0)
      of button:yes
      of button:no
          exit# = 1
      end !case
      If exit# = 0
          error# = 0
          If export_jobs_temp <> 'YES' and export_chargeable_spares_temp <> 'YES' And export_warranty_spares_temp <> 'YES' And|
              export_audit_temp <> 'YES'
              beep(beep:systemhand)  ;  yield()
              message('You must select at least one file to export!', |
                      'ServiceBase 2000', icon:hand)
              error# = 1
          End!If export_jobs_temp <> 'YES' and export_chargeable_spares_temp <> 'YES' And export_warranty_spares_temp <> 'YES' |
      
          If error# = 0
              fileerror# = 0
              If export_jobs_temp = 'YES'
                  glo:file_name   = tmp:file1
                  Remove(glo:file_name)
                  If errorcode() = 05 Or errorcode() = 52
                      beep(beep:systemhand)  ;  yield()
                      message('The following error has occured with this file:'&|
                              '|'&clip(glo:file_name)&|
                              '||'&error(), |
                              'ServiceBase 2000', icon:hand)
                      fileerror# = 1
                  End!If errorcode() = 05 Or errorcode() = 52
                  If fileerror# = 0
                      access:expjobs.open()
                      access:expjobs.usefile()
                      get(expjobs,0)
                      expjob:ref_number                  = 'Job No'
                      expjob:batch_number                = 'Job Batch No'
                      expjob:who_booked                  = 'Who Booked'
                      expjob:date_booked                 = 'Date Booked'
                      expjob:time_booked                 = 'Time Booked'
                      expjob:current_status              = 'Status Type'
                      expjob:title                       = 'Title'
                      expjob:initial                     = 'Initial'
                      expjob:surname                     = 'Surname'
                      expjob:account_number              = 'Account No'
                      expjob:order_number                = 'Order No'
                      expjob:chargeable_job              = 'Chargeable Job'
                      expjob:charge_type                 = 'Chargeable Type'
                      expjob:repair_type                 = 'Chargeable Repair Type'
                      expjob:warranty_job                = 'Warranty Job'
                      expjob:warranty_charge_type        = 'Warranty Type'
                      expjob:repair_type_warranty        = 'Warranty Repair Type'
                      expjob:model_number                = 'Model No'
                      expjob:manufacturer                = 'Manufacturer'
                      expjob:esn                         = 'E.S.N. / I.M.E.I.'
                      expjob:msn                         = 'M.S.N.'
                      expjob:unit_type                   = 'Unit Type'
                      expjob:mobile_number               = 'Mobile No'
                      expjob:workshop                    = 'Workshop'
                      expjob:location                    = 'Location'
                      expjob:authority_number            = 'Authority No'
                      expjob:dop                         = 'D.O.P.'
                      expjob:physical_damage             = 'Physical Damage'
                      expjob:intermittent_fault          = 'Intermit. Fault'
                      expjob:transit_type                = 'Transit Type'
                      expjob:job_priority                = 'Turnaround Time'
                      expjob:engineer                    = 'Engineer'
                      expjob:postcode                    = 'Postcode'
                      expjob:company_name                = 'Company Name'
                      expjob:address_line1               = 'Address 1'
                      expjob:address_line2               = 'Address 2'
                      expjob:address_line3               = 'Address 3'
                      expjob:telephone_number            = 'Tel No'
                      expjob:fax_number                  = 'Fax No'
                      expjob:postcode_collection         = 'Collection Postcode'
                      expjob:company_name_collection     = 'Collection Company Name'
                      expjob:address_line1_collection    = 'Collection Address 1'
                      expjob:address_line2_collection    = 'Collection Address 2'
                      expjob:address_line3_collection    = 'Collection Address 3'
                      expjob:telephone_collection        = 'Collection Tel No'
                      expjob:postcode_delivery           = 'Delivery Postcode'
                      expjob:company_name_delivery       = 'Delivery Company Name'
                      expjob:address_line1_delivery      = 'Delivery Address 1'
                      expjob:address_line2_delivery      = 'Delivery Address 2'
                      expjob:address_line3_delivery      = 'Delivery Address 3'
                      expjob:telephone_delivery          = 'Delivery Tel No'
                      expjob:in_repair                   = 'In Repair'
                      expjob:date_in_repair              = 'In Repair Date'
                      expjob:time_in_repair              = 'In Repair Time'
                      expjob:on_test                     = 'On Test'
                      expjob:date_on_test                = 'On Test Date'
                      expjob:time_on_test                = 'On Test Time'
                      expjob:date_completed              = 'Date Completed'
                      expjob:time_completed              = 'Time Completed'
                      expjob:qa_passed                   = 'QA Passed'
                      expjob:date_qa_passed              = 'QA Passed Date'
                      expjob:time_qa_passed              = 'QA Passed Time'
                      expjob:qa_rejected                 = 'QA Rejected'
                      expjob:date_qa_rejected            = 'QA Rejected Date'
                      expjob:time_qa_rejected            = 'QA Rejected Time'
                      expjob:qa_second_passed            = 'QA 2nd Pass'
                      expjob:date_qa_second_passed       = 'QA 2nd Pass Date'
                      expjob:time_qa_second_passed       = 'QA 2nd Pass Time'
                      expjob:estimate_ready              = 'Estimate Ready'
                      expjob:estimate                    = 'Estimate'
                      expjob:estimate_if_over            = 'Estimate If Over'
                      expjob:estimate_accepted           = 'Estimate Accepted'
                      expjob:estimate_rejected           = 'Estimate Rejected'
                      expjob:courier_cost                = 'Chargeable Courier Cost'
                      expjob:labour_cost                 = 'Chargeable Labour Cost'
                      expjob:parts_cost                  = 'Chargeable Parts Cost'
                      expjob:sub_total                   = 'Chargeable Sub Total'
                      expjob:courier_cost_estimate       = 'Estimate Courier Cost'
                      expjob:labour_cost_estimate        = 'Estimate Labour Cost'
                      expjob:parts_cost_estimate         = 'Estimate Parts Cost'
                      expjob:sub_total_estimate          = 'Estimate Sub Total'
                      expjob:courier_cost_warranty       = 'Warranty Courier Cost'
                      expjob:labour_cost_warranty        = 'Warranty Labour Cost'
                      expjob:parts_cost_warranty         = 'Warranty Parts Cost'
                      expjob:sub_total_warranty          = 'Warranty Sub Total'
                      expjob:paid                        = 'Paid'
                      expjob:paid_warranty               = 'Warranty Paid'
                      expjob:date_paid                   = 'Date Paid'
                      expjob:paid_user                   = 'Paid By'
                      expjob:loan_status                 = 'Loan Status'
                      expjob:loan_issued_date            = 'Loan Issue Date'
                      expjob:loan_unit_number            = 'Loan Unit Number'
                      expjob:loan_user                   = 'Loan User'
                      expjob:loan_courier                = 'Loan Courier'
                      expjob:loan_consignment_number     = 'Loan Consignment No'
                      expjob:loan_despatched_user        = 'Loan Despatch User'
                      expjob:loan_despatch_number        = 'Loan Despatch No'
                      expjob:exchange_status             = 'Exchange Status'
                      expjob:exchange_unit_number        = 'Exchange Unit No'
                      expjob:exchange_issued_date        = 'Exchange Issue Date'
                      expjob:exchange_user               = 'Exchange User'
                      expjob:exchange_courier            = 'Exchange Courier'
                      expjob:exchange_consignment_number = 'Exchange Consignment No'
                      expjob:exchange_despatched_user    = 'Exchange Despatch User'
                      expjob:exchange_despatch_number    = 'Exchange Despatch No'
                      expjob:date_despatched             = 'Job Date Despatched'
                      expjob:despatch_number             = 'Job Despatch No'
                      expjob:despatch_user               = 'Job Despatch User'
                      expjob:courier                     = 'Job Courier'
                      expjob:consignment_number          = 'Job Consignment No'
                      expjob:incoming_courier            = 'Incoming Courier'
                      expjob:incoming_consignment_number = 'Incoming Consignment No'
                      expjob:incoming_date               = 'Incoming Date'
                      expjob:despatched                  = 'Job Despatched'
                      expjob:third_party_site            = 'Third Party Site'
                      expjob:fault_code1                 = 'Fault Code 1'
                      expjob:fault_code2                 = 'Fault Code 2'
                      expjob:fault_code3                 = 'Fault Code 3'
                      expjob:fault_code4                 = 'Fault Code 4'
                      expjob:fault_code5                 = 'Fault Code 5'
                      expjob:fault_code6                 = 'Fault Code 6'
                      expjob:fault_code7                 = 'Fault Code 7'
                      expjob:fault_code8                 = 'Fault Code 8'
                      expjob:fault_code9                 = 'Fault Code 9'
                      expjob:fault_code10                = 'Fault Code 10'
                      expjob:fault_code11                = 'Fault Code 11'
                      expjob:fault_code12                = 'Fault Code 12'
                      expjob:special_instructions        = 'Special Instructions'
                      expjob:edi_batch_number            = 'EDI Batch Number'
                      expjob:fault_description           = 'Fault Description'
                      expjob:notes                       = 'Notes'
                      expjob:engineers_notes             = 'Engineers Notes'
                      expjob:invoice_text                = 'Invoice Text'
                      expjob:fault_report                = 'Fault Report'
                      expjob:collection_text             = 'Collection Text'
                      expjob:delivery_text               = 'Delivery Text'
                      ! Start Change 3252 BE(29/01/04)
                      expjob:exchange_reason             = 'Exchange Reason'
                      ! End Change 3252 BE(29/01/04)
                      access:expjobs.insert()
                  End!If errorcode() = 05 Or errorcode() = 52
              End!If export_jobs_temp = 'YES'
      
              If export_chargeable_spares_temp = 'YES'
                  glo:file_name = tmp:file2
                  Remove(glo:file_name)
                  If errorcode() = 5 Or errorcode() = 52
                      beep(beep:systemhand)  ;  yield()
                      message('The following error has occured with this file:'&|
                              '|'&clip(glo:file_name)&|
                              '||'&error(), |
                              'ServiceBase 2000', icon:hand)
                      fileerror# = 1
                  End!If errorcode() = 5 Or errorcode() = 52
                  If fileerror# = 0
                      access:expparts.open()
                      access:expparts.usefile()
                      get(expparts,0)
                      exppar:ref_number           = 'Job No'
                      exppar:part_number          = 'Part No'
                      exppar:description          = 'Description'
                      exppar:supplier             = 'Supplier'
                      exppar:purchase_cost        = 'Purchase Cost'
                      exppar:sale_cost            = 'Sale Cost'
                      exppar:quantity             = 'Quantity'
                      exppar:exclude_from_order   = 'Exclude From Order'
                      exppar:despatch_note_number = 'Despatch Note No'
                      exppar:date_ordered         = 'Date Ordered'
                      exppar:order_number         = 'Order No'
                      exppar:date_received        = 'Date Received'
                      exppar:fault_code1          = 'Fault Code 1'
                      exppar:fault_code2          = 'Fault Code 2'
                      exppar:fault_code3          = 'Fault Code 3'
                      exppar:fault_code4          = 'Fault Code 4'
                      exppar:fault_code5          = 'Fault Code 5'
                      exppar:fault_code6          = 'Fault Code 6'
                      exppar:fault_code7          = 'Fault Code 7'
                      exppar:fault_code8          = 'Fault Code 8'
                      exppar:fault_code9          = 'Fault Code 9'
                      exppar:fault_code10         = 'Fault Code 10'
                      exppar:fault_code11         = 'Fault Code 11'
                      exppar:fault_code12         = 'Fault Code 12'
                      access:expparts.insert()
                  End!If error# = 0
              End!If export_chargeable_spares_temp = 'YES'
      
              If export_warranty_spares_temp = 'YES'
                  glo:file_name   = tmp:file3
                  Remove(glo:file_name)
                  If errorcode() = 05 Or errorcode() = 52
                      beep(beep:systemhand)  ;  yield()
                      message('The following error has occured with this file:'&|
                              '|'&clip(glo:file_name)&|
                              '||'&error(), |
                              'ServiceBase 2000', icon:hand)
                      fileerror# = 1
                  End!If errorcode() = 05 Or errorcode() = 52
                  IF fileerror# = 0
                      access:expwarparts.open()
                      access:expwarparts.usefile()
                      get(expwarparts,0)
                      expwar:ref_number           = 'Job No'
                      expwar:part_number          = 'Part No'
                      expwar:description          = 'Description'
                      expwar:supplier             = 'Supplier'
                      expwar:purchase_cost        = 'Purchase Cost'
                      expwar:sale_cost            = 'Sale Cost'
                      expwar:quantity             = 'Quantity'
                      expwar:exclude_from_order   = 'Exclude From Order'
                      expwar:despatch_note_number = 'Despatch Note No'
                      expwar:date_ordered         = 'Date Ordered'
                      expwar:order_number         = 'Order No'
                      expwar:date_received        = 'Date Received'
                      expwar:unittype             = 'Unit Type'
                      expwar:fault_code1          = 'Fault Code 1'
                      expwar:fault_code2          = 'Fault Code 2'
                      expwar:fault_code3          = 'Fault Code 3'
                      expwar:fault_code4          = 'Fault Code 4'
                      expwar:fault_code5          = 'Fault Code 5'
                      expwar:fault_code6          = 'Fault Code 6'
                      expwar:fault_code7          = 'Fault Code 7'
                      expwar:fault_code8          = 'Fault Code 8'
                      expwar:fault_code9          = 'Fault Code 9'
                      expwar:fault_code10         = 'Fault Code 10'
                      expwar:fault_code11         = 'Fault Code 11'
                      expwar:fault_code12         = 'Fault Code 12'
                      expwar:FirstJob             = 'First Job'
                      access:expwarparts.insert()
                  End !If error# = 0
              End!If export_warranty_spares_temp = 'YES'
      
              If export_audit_temp = 'YES'
                  glo:file_name4  = tmp:file4
                  Remove(glo:file_name4)
                  If errorcode() = 05 Or errorcode() = 52
                      beep(beep:systemhand)  ;  yield()
                      message('The following error has occured with this file:'&|
                              '|'&clip(glo:file_name4)&|
                              '||'&error(), |
                              'ServiceBase 2000', icon:hand)
                      fileerror# = 1
                  End!If errorcode() = 05 Or errorcode() = 52
                  IF fileerror# = 0
                      access:expaudit.open()
                      access:expaudit.usefile()
                      get(expaudit,0)
                      expaud:ref_number = 'Job No'
                      expaud:date       = 'Date'
                      expaud:time       = 'Time'
                      expaud:user       = 'User'
                      expaud:action     = 'Action'
                      expaud:notes      = 'Notes'
                      access:expaudit.insert()
                  End!If Error# = 0
              End!If export_audit_temp = 'YES'
      
      
              If fileerror# = 0
                  recordspercycle     = 25
                  recordsprocessed    = 0
                  percentprogress     = 0
                  setcursor(cursor:wait)
                  open(progresswindow)
                  progress:thermometer    = 0
                  ?progress:pcttext{prop:text} = '0% Completed'
      
                  recordstoprocess    = Records(jobs)
      
                  setcursor(cursor:wait)
                  countjobs# = 0
                  count_chargeable_parts# = 0
                  count_warranty_parts# = 0
                  count_audit# = 0
                  save_job_id = access:jobs.savefile()
                  FirstJob# = 0
      
          !Export Order
                  Case export_order_temp
                      Of 'JOB NUMBER'
                          set(job:ref_number_key,0)
                      Of 'E.S.N. / I.M.E.I.'
                          set(job:esn_key,0)
                      Of 'ACCOUNT NUMBER'
                          set(job:accountnumberkey,0)
                      Of 'MODEL NUMBER'
                          set(job:model_number_key,0)
                      Of 'STATUS'
                          set(job:by_status,0)
                      Of 'MSN'
                          set(job:msn_key,0)
                      Of 'SURNAME'
                          set(job:surname_key,0)
                      Of 'ENGINEER'
                          set(job:engineer_key,0)
                      Of 'MOBILE NUMBER'
                          set(job:mobilenumberkey,0)
                      Of 'DATE BOOKED'
                          If booking_start_date_temp <> '' And booking_end_date_temp <> ''
                              job:date_booked = booking_start_date_temp
                              set(job:date_booked_key,job:date_booked_key)
                          Else!If booking_start_date_temp <> '' And booking_end_date_temp <> ''
                              set(job:date_booked_key,0)
                          End!If booking_start_date_temp <> '' And booking_end_date_temp <> ''
                      Of 'DATE COMPLETED'
                          If completed_start_date_temp <> '' And completed_end_date_temp <> ''
                              job:date_completed = completed_start_date_temp
                              set(job:datecompletedkey,job:datecompletedkey)
                          Else!If booking_start_date_temp <> '' And booking_end_date_temp <> ''
                              set(job:datecompletedkey,0)
                          End!If booking_start_date_temp <> '' And booking_end_date_temp <> ''
      
                  End!Case glo:select30
      
      
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
      
              !---insert routine
                      do getnextrecord2
                      cancelcheck# += 1
                      if cancelcheck# > (recordstoprocess/100)
                          do cancelcheck
                          if tmp:cancel = 1
                              break
                          end!if tmp:cancel = 1
                          cancelcheck# = 0
                      end!if cancelcheck# > 50
      
      
      
                      If booking_start_date_temp <> '' And booking_end_date_temp <> '' And export_order_temp = 'DATE BOOKED'
                          If job:date_booked > booking_end_date_temp
                              break
                          End!If job:date_booked > booking_end_date_temp
                      Else!If booking_start_date_temp <> '' And booking_end_date_temp <> '' And export_order_temp = 'DATE BOOKED'
                          If booking_start_date_temp <> ''
                              If job:date_booked < booking_start_date_temp
                                  Cycle
                              End
                          End!If booking_start_date_temp <> ''
      
                          If booking_end_date_temp <> ''
                              If job:date_booked > booking_end_date_temp
                                  Cycle
                              End
                          End!If booking_end_date_temp <> ''
      
                      End!If booking_start_date_temp <> '' And booking_end_date_temp <> '' And export_order_temp = 'DATE BOOKED'
                      If completed_start_date_temp <> '' And completed_end_date_temp <> '' And export_order_temp = 'DATE COMPLETED'
                          If job:date_completed > completed_end_Date_temp
                              Break
                          End!If job:date_completed > completed_end_Date_temp
                      Else!If completed_start_date_temp <> '' And completed_end_date_temp <> '' And export_order_temp = 'DATE COMPLETED'
                          If completed_start_date_temp <> ''
                              If job:date_completed < completed_start_date_temp
                                  Cycle
                              End
                          End!If completed_start_date_temp <> ''
      
                          If completed_end_date_temp <> ''
                              If job:date_completed > completed_end_date_temp
                                  Cycle
                              End
                          End!If completed_end_date_temp <> ''
      
                      End!If completed_start_date_temp <> '' And completed_end_date_temp <> '' And export_order_temp = 'DATE COMPLETED'
      
      
                      If Records(glo:Queue) <> ''
                          Sort(glo:Queue,glo:pointer)
                          glo:pointer = job:account_number
                          Get(glo:Queue,glo:pointer)
                          If Error()
                              Cycle
                          End
                      End!If Records(glo:Queue) <> ''
      
                      If Records(glo:Queue2) <> ''
                          Sort(glo:Queue2,glo:pointer2)
                          glo:pointer2 = job:charge_type
                          Get(glo:Queue2,glo:pointer2)
                          If Error()
                              Cycle
                          End
                      End!If Records(glo:Queue2) <> ''
      
                      If Records(glo:Queue3) <> ''
                          Sort(glo:Queue3,glo:pointer3)
                          glo:pointer3 = job:warranty_charge_type
                          Get(glo:Queue3,glo:pointer3)
                          If Error()
                              Cycle
                          End
                      End!If Records(glo:Queue3) <> ''
      
                      If Records(glo:Queue4) <> ''
                          Sort(glo:Queue4,glo:pointer4)
                          glo:pointer4 = job:current_status
                          Get(glo:Queue4,glo:pointer4)
                          If Error()
                              Cycle
                          End
                      End!If Records(glo:Queue4) <> ''
      
                      If Records(glo:Queue5) <> ''
                          Sort(glo:Queue5,glo:pointer5)
                          glo:pointer5 = job:turnaround_time
                          Get(glo:Queue5,glo:pointer5)
                          If Error()
                              Cycle
                          End
                      End!If Records(glo:Queue5) <> ''
      
                      If Records(glo:Queue6) <> ''
                          Sort(glo:Queue6,glo:pointer6)
                          glo:pointer6 = job:engineer
                          Get(glo:Queue6,glo:pointer6)
                          If Error()
                              Cycle
                          End
                      End!If Records(glo:Queue6) <> ''
      
                      If Records(glo:Queue7) <> ''
                          Sort(glo:Queue7,glo:pointer7)
                          glo:pointer7 = job:model_number
                          Get(glo:Queue7,glo:pointer7)
                          If Error()
                              Cycle
                          End
                      End!If Records(glo:Queue7) <> ''
      
                      If job_batch_number_temp <> ''
                          If job:batch_number <> job_batch_number_temp
                              Cycle
                          End
                      End!If job_batch_number_temp <> ''
      
                      If edi_batch_number_temp <> ''
                          If job:edi_batch_number <> edi_batch_number_temp
                              Cycle
                          End
                      End!If edi_batch_number_temp <> ''
      
                      If export_jobs_temp = 'YES'
      
                          access:jobnotes.clearkey(jbn:RefNumberKey)
                          jbn:RefNUmber = job:ref_number
                          access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                          ! Start Change 3252 BE(29/01/04)
                          access:jobse.clearkey(jobe:RefNumberKey)
                          jobe:RefNumber = job:ref_number
                          access:jobse.fetch(jobe:RefNumberKey)
                          ! End Change 3252 BE(29/01/04)
      
                          glo:file_name   = tmp:file1
                          get(expjobs,0)
                          expjob:ref_number                  = job:ref_number
                          expjob:batch_number                = job:batch_number
                          expjob:who_booked                  = job:who_booked
                          expjob:date_booked                 = Format(job:date_booked,@d6)
                          expjob:time_booked                 = Format(job:time_booked,@t1)
                          expjob:current_status              = job:current_status
                          expjob:title                       = job:title
                          expjob:initial                     = job:initial
                          expjob:surname                     = job:surname
                          expjob:account_number              = job:account_number
                          expjob:order_number                = job:order_number
                          expjob:chargeable_job              = job:chargeable_job
                          expjob:charge_type                 = job:charge_type
                          expjob:repair_type                 = job:repair_type
                          expjob:warranty_job                = job:warranty_job
                          expjob:warranty_charge_type        = job:warranty_charge_type
                          expjob:repair_type_warranty        = job:repair_type_warranty
                          expjob:model_number                = job:model_number
                          expjob:manufacturer                = job:manufacturer
                          expjob:esn                         = job:esn
                          expjob:msn                         = job:msn
                          expjob:unit_type                   = job:unit_type
                          expjob:mobile_number               = job:mobile_number
                          expjob:workshop                    = job:workshop
                          expjob:location                    = job:location
                          expjob:authority_number            = job:authority_number
                          expjob:dop                         = Format(job:dop,@d6)
                          expjob:physical_damage             = job:physical_damage
                          expjob:intermittent_fault          = job:intermittent_fault
                          expjob:transit_type                = job:transit_type
                          expjob:job_priority                = job:job_priority
                          expjob:engineer                    = job:engineer
                          expjob:postcode                    = job:postcode
                          expjob:company_name                = job:company_name
                          expjob:address_line1               = job:address_line1
                          expjob:address_line2               = job:address_line2
                          expjob:address_line3               = job:address_line3
                          expjob:telephone_number            = job:telephone_number
                          expjob:fax_number                  = job:fax_number
                          expjob:postcode_collection         = job:postcode_collection
                          expjob:company_name_collection     = job:company_name_collection
                          expjob:address_line1_collection    = job:address_line1_collection
                          expjob:address_line2_collection    = job:address_line2_collection
                          expjob:address_line3_collection    = job:address_line3_collection
                          expjob:telephone_collection        = job:telephone_collection
                          expjob:postcode_delivery           = job:postcode_delivery
                          expjob:company_name_delivery       = job:company_name_delivery
                          expjob:address_line1_delivery      = job:address_line1_delivery
                          expjob:address_line2_delivery      = job:address_line2_delivery
                          expjob:address_line3_delivery      = job:address_line3_delivery
                          expjob:telephone_delivery          = job:telephone_delivery
                          expjob:in_repair                   = job:in_repair
                          expjob:date_in_repair              = Format(job:date_in_repair,@d6)
                          expjob:time_in_repair              = Format(job:time_in_repair,@t1)
                          expjob:on_test                     = job:on_test
                          expjob:date_on_test                = Format(job:date_on_test,@d6)
                          expjob:time_on_test                = Format(job:time_on_test,@t1)
                          expjob:date_completed              = Format(job:date_completed,@d6)
                          expjob:time_completed              = Format(job:time_completed,@t1)
                          expjob:qa_passed                   = job:qa_passed
                          expjob:date_qa_passed              = Format(job:date_qa_passed,@d6)
                          expjob:time_qa_passed              = Format(job:time_qa_passed,@t1)
                          expjob:qa_rejected                 = job:qa_rejected
                          expjob:date_qa_rejected            = Format(job:date_qa_rejected,@d6)
                          expjob:time_qa_rejected            = Format(job:time_qa_rejected,@t1)
                          expjob:qa_second_passed            = job:qa_second_passed
                          expjob:date_qa_second_passed       = Format(job:date_qa_second_passed,@d6)
                          expjob:time_qa_second_passed       = Format(job:time_qa_second_passed,@t1)
                          expjob:estimate_ready              = job:estimate_ready
                          expjob:estimate                    = job:estimate
                          expjob:estimate_if_over            = job:estimate_if_over
                          expjob:estimate_accepted           = job:estimate_accepted
                          expjob:estimate_rejected           = job:estimate_rejected
                          expjob:courier_cost                = job:courier_cost
                          expjob:labour_cost                 = job:labour_cost
                          expjob:parts_cost                  = job:parts_cost
                          expjob:sub_total                   = job:sub_total
                          expjob:courier_cost_estimate       = job:courier_cost_estimate
                          expjob:labour_cost_estimate        = job:labour_cost_estimate
                          expjob:parts_cost_estimate         = job:parts_cost_estimate
                          expjob:sub_total_estimate          = job:sub_total_estimate
                          expjob:courier_cost_warranty       = job:courier_cost_warranty
                          expjob:labour_cost_warranty        = job:labour_cost_warranty
                          expjob:parts_cost_warranty         = job:parts_cost_warranty
                          expjob:sub_total_warranty          = job:sub_total_warranty
                          expjob:paid                        = job:paid
                          expjob:paid_warranty               = job:paid_warranty
                          expjob:date_paid                   = job:date_paid
                          expjob:loan_status                 = job:loan_status
                          expjob:loan_issued_date            = Format(job:loan_issued_date,@d6)
                          expjob:loan_unit_number            = job:loan_unit_number
                          expjob:loan_user                   = job:loan_user
                          expjob:loan_courier                = job:loan_courier
                          expjob:loan_consignment_number     = job:loan_consignment_number
                          expjob:loan_despatched_user        = job:loan_despatched_user
                          expjob:loan_despatch_number        = job:loan_despatch_number
                          expjob:exchange_status             = job:exchange_status
                          expjob:exchange_unit_number        = job:exchange_unit_number
                          expjob:exchange_issued_date        = Format(job:exchange_issued_date,@d6)
                          expjob:exchange_user               = job:exchange_user
                          expjob:exchange_courier            = job:exchange_courier
                          expjob:exchange_consignment_number = job:exchange_consignment_number
                          expjob:exchange_despatched_user    = job:exchange_despatched_user
                          expjob:exchange_despatch_number    = job:exchange_despatch_number
                          expjob:date_despatched             = Format(job:date_despatched,@d6)
                          expjob:despatch_number             = job:despatch_number
                          expjob:despatch_user               = job:despatch_user
                          expjob:courier                     = job:courier
                          expjob:consignment_number          = job:consignment_number
                          expjob:incoming_courier            = job:incoming_courier
                          expjob:incoming_consignment_number = job:incoming_consignment_number
                          expjob:incoming_date               = Format(job:incoming_date,@d6)
                          expjob:despatched                  = job:despatched
                          expjob:third_party_site            = job:third_party_site
                          expjob:fault_code1                 = job:fault_code1
                          expjob:fault_code2                 = job:fault_code2
                          expjob:fault_code3                 = job:fault_code3
                          expjob:fault_code4                 = job:fault_code4
                          expjob:fault_code5                 = job:fault_code5
                          expjob:fault_code6                 = job:fault_code6
                          expjob:fault_code7                 = job:fault_code7
                          expjob:fault_code8                 = job:fault_code8
                          expjob:fault_code9                 = job:fault_code9
                          expjob:fault_code10                = job:fault_code10
                          expjob:fault_code11                = job:fault_code11
                          expjob:fault_code12                = job:fault_code12
                          expjob:special_instructions        = job:special_instructions
                          expjob:edi_batch_number            = job:edi_batch_number
                          expjob:fault_description           = StripReturn(jbn:fault_description)
                          expjob:notes                       = ''
                          expjob:engineers_notes             = StripReturn(jbn:engineers_notes)
                          expjob:invoice_text                = StripReturn(jbn:invoice_text)
                          expjob:collection_text             = StripReturn(jbn:collection_text)
                          expjob:delivery_text               = StripReturn(jbn:delivery_text)
                          ! Start Change 3252 BE(29/01/04)
                          expjob:exchange_reason             = StripReturn(jobe:ExchangeReason)
                          ! End Change 3252 BE(29/01/04)
                          access:expjobs.insert()
                          countjobs# = 1
                      End!If export_jobs_temp = 'YES'
      
                      If export_audit_temp = 'YES'
                          save_aud_id = access:audit.savefile()
                          access:audit.clearkey(aud:ref_number_key)
                          aud:ref_number = job:ref_number
                          set(aud:ref_number_key,aud:ref_number_key)
                          loop
                              if access:audit.next()
                                 break
                              end !if
                              if aud:ref_number <> job:ref_number      |
                                  then break.  ! end if
                              yldcnt# += 1
                              if yldcnt# > 25
                                 yield() ; yldcnt# = 0
                              end !if
                              glo:file_name4   = tmp:file4
                              get(expaudit,0)
                              expaud:ref_number = aud:ref_number
                              expaud:date       = Format(aud:date,@d6)
                              expaud:time       = Format(aud:time,@t1)
                              expaud:user       = aud:user
                              expaud:action     = aud:action
                              expaud:notes      = aud:notes
                              access:expaudit.insert()
                              count_audit# = 1
      
                          end !loop
                          access:audit.restorefile(save_aud_id)
                      End!If export_audit_temp = 'YES'
      
                      If Export_Chargeable_Spares_Temp = 'YES'
      
                          save_par_id = access:parts.savefile()
                          access:parts.clearkey(par:part_number_key)
                          par:ref_number  = job:ref_number
                          set(par:part_number_key,par:part_number_key)
                          loop
                              if access:parts.next()
                                 break
                              end !if
                              if par:ref_number  <> job:ref_number      |
                                  then break.  ! end if
                              yldcnt# += 1
                              if yldcnt# > 25
                                 yield() ; yldcnt# = 0
                              end !if
                              glo:file_name   = tmp:file2
                              get(expparts,0)
                              exppar:ref_number           = par:ref_number
                              exppar:part_number          = par:part_number
                              exppar:description          = par:description
                              exppar:supplier             = par:supplier
                              exppar:purchase_cost        = par:purchase_cost
                              exppar:sale_cost            = par:sale_cost
                              exppar:quantity             = par:quantity
                              exppar:exclude_from_order   = par:exclude_from_order
                              exppar:despatch_note_number = par:despatch_note_number
                              exppar:date_ordered         = Format(par:date_ordered,@d6)
                              exppar:order_number         = par:order_number
                              exppar:date_received        = Format(par:date_received,@d6)
                              exppar:fault_code1          = par:fault_code1
                              exppar:fault_code2          = par:fault_code2
                              exppar:fault_code3          = par:fault_code3
                              exppar:fault_code4          = par:fault_code4
                              exppar:fault_code5          = par:fault_code5
                              exppar:fault_code6          = par:fault_code6
                              exppar:fault_code7          = par:fault_code7
                              exppar:fault_code8          = par:fault_code8
                              exppar:fault_code9          = par:fault_code9
                              exppar:fault_code10         = par:fault_code10
                              exppar:fault_code11         = par:fault_code11
                              exppar:fault_code12         = par:fault_code12
                              access:expparts.insert()
                              count_chargeable_parts# = 1
                          end !loop
                          access:parts.restorefile(save_par_id)
                      End!If Export_Chargeable_Spares_Temp = 'YES'
      
                      If Export_Warranty_Spares_Temp = 'YES'
      
                          save_wpr_id = access:warparts.savefile()
                          access:warparts.clearkey(wpr:part_number_key)
                          wpr:ref_number  = job:ref_number
                          set(wpr:part_number_key,wpr:part_number_key)
                          loop
                              if access:warparts.next()
                                 break
                              end !if
                              if wpr:ref_number  <> job:ref_number      |
                                  then break.  ! end if
                              yldcnt# += 1
                              if yldcnt# > 25
                                 yield() ; yldcnt# = 0
                              end !if
                              glo:file_name  = tmp:file3
                              get(expwarparts,0)
                              expwar:ref_number           = wpr:ref_number
                              expwar:part_number          = wpr:part_number
                              expwar:description          = wpr:description
                              expwar:supplier             = wpr:supplier
                              expwar:purchase_cost        = wpr:purchase_cost
                              expwar:sale_cost            = wpr:sale_cost
                              expwar:quantity             = wpr:quantity
                              expwar:exclude_from_order   = wpr:exclude_from_order
                              expwar:despatch_note_number = wpr:despatch_note_number
                              expwar:date_ordered         = Format(wpr:date_ordered,@d6)
                              expwar:order_number         = wpr:order_number
                              expwar:date_received        = Format(wpr:date_received,@d6)
                              expwar:unittype             = job:unit_Type
                              expwar:fault_code1          = wpr:fault_code1
                              expwar:fault_code2          = wpr:fault_code2
                              expwar:fault_code3          = wpr:fault_code3
                              expwar:fault_code4          = wpr:fault_code4
                              expwar:fault_code5          = wpr:fault_code5
                              expwar:fault_code6          = wpr:fault_code6
                              expwar:fault_code7          = wpr:fault_code7
                              expwar:fault_code8          = wpr:fault_code8
                              expwar:fault_code9          = wpr:fault_code9
                              expwar:fault_code10         = wpr:fault_code10
                              expwar:fault_code11         = wpr:fault_code11
                              expwar:fault_code12         = wpr:fault_code12
                              If job:ref_number <> FirstJob#
                                  expwar:FirstJob = '1'
                                  FirstJob# = job:ref_number
                              Else!If job:ref_number <> FirstJob#
                                  expwar:FirstJob = ''
                              End!If job:ref_number <> FirstJob#
                              access:expwarparts.insert()
                              count_warranty_parts# = 1
                          end !loop
                          access:warparts.restorefile(save_wpr_id)
      
                      End!If Export_Chargeable_Spares_Temp = 'YES'
      
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
      
                  If export_jobs_temp = 'YES'
                      glo:file_name   = tmp:file1
                      If countjobs# = 0
                          
                          Clear(expjobs:record)
                          expjob:ref_number = 'No Records Match Criteria'
                          access:expjobs.insert()
                      End!If countjobs# = 0
                      access:expjobs.close()
                  End!If export_jobs_temp = 'YES'
      
                  If export_chargeable_spares_temp = 'YES'
                      glo:file_name   = tmp:file2
      
                      If count_chargeable_parts# = 0
                          Clear(expparts:record)
                          exppar:ref_number = 'No Records Match Criteria'
                          access:expparts.insert()
                      End!If countjobs# = 0
                      access:expparts.close()
                  End!If export_chargeable_spares_temp = 'YES'
      
                  If export_warranty_spares_temp = 'YES'
                      glo:file_name   = tmp:file3
      
                      If count_warranty_parts# = 0
                          Clear(expwarparts:record)
                          expwar:ref_number = 'No Records Match Criteria'
                          access:expwarparts.insert()
                      End!If countjobs# = 0
                      access:expwarparts.close()
                  End!If export_warranty_spares_temp = 'YES'
      
                  If export_audit_temp = 'YES'
                      glo:file_name4   = tmp:file4
      
                      If count_audit_parts# = 0
                          Clear(expaud:record)
                          expaud:ref_number = 'No Records Match Criteria'
                          access:expaudit.insert()
                      End!If countjobs# = 0
                      access:expaudit.close()
                  End!If export_audit_temp = 'YES'
      
                  setcursor()
                  close(progresswindow)
      
                  beep(beep:systemexclamation)  ;  yield()
                  message('Export File(s) Created.', |
                          'ServiceBase 2000', icon:exclamation)
              End!If fileerror# = 0
          End!If error# = 0
      End!If exit# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?VSBackButton
      ThisWindow.Update
         Wizard31.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
      error# = 0
      If Choice(?Sheet1) = 8
          If export_order_temp = ''
              error# = 1
              beep(beep:systemhand)  ;  yield()
              message('You must select an Export Order before you can continue.', |
                      'ServiceBase 2000', icon:hand)
          End!If export_order_temp = ''
      End!If TabNumber = 8
      If error# = 0
         Wizard31.TakeAccepted()
      End!If error# = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Data_Export_Wizard')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?booking_start_date_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?booking_end_date_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?completed_start_date_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?completed_end_date_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::3:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::7:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:3)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:4
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:4{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:4{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:4)
               ?List:4{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:5
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:5{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:5{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::11:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:5)
               ?List:5{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:6
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:6{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:6{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:6)
               ?List:6{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:7
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:7{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:7{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::15:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:7)
               ?List:7{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do dasbrw::3:dasuntagall
      Do dasbrw::5:dasuntagall
      Do dasbrw::7:dasuntagall
      Do dasbrw::9:dasuntagall
      Do dasbrw::11:dasuntagall
      Do dasbrw::13:dasuntagall
      Do dasbrw::15:dasuntagall
      Select(?sheet1,1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = sub:Account_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      account_tag_temp = ''
    ELSE
      account_tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (account_tag_temp = '*')
    SELF.Q.account_tag_temp_Icon = 2
  ELSE
    SELF.Q.account_tag_temp_Icon = 1
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW2.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW2::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW2::RecordStatus=ReturnValue
  IF BRW2::RecordStatus NOT=Record:OK THEN RETURN BRW2::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = sub:Account_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::3:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW2::RecordStatus
  RETURN ReturnValue


BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = cha:Charge_Type
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    IF ERRORCODE()
      charge_type_tag_temp = ''
    ELSE
      charge_type_tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (charge_type_tag_temp = '*')
    SELF.Q.charge_type_tag_temp_Icon = 2
  ELSE
    SELF.Q.charge_type_tag_temp_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = cha:Charge_Type
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue3.Pointer3 = cha:Charge_Type
     GET(GLO:Queue3,GLO:Queue3.Pointer3)
    IF ERRORCODE()
      warranty_type_tag_temp = ''
    ELSE
      warranty_type_tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (warranty_type_tag_temp = '*')
    SELF.Q.warranty_type_tag_temp_Icon = 2
  ELSE
    SELF.Q.warranty_type_tag_temp_Icon = 1
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue3.Pointer3 = cha:Charge_Type
     GET(GLO:Queue3,GLO:Queue3.Pointer3)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue


BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue4.Pointer4 = sts:Status
     GET(GLO:Queue4,GLO:Queue4.Pointer4)
    IF ERRORCODE()
      status_tag_temp = ''
    ELSE
      status_tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (status_tag_temp = '*')
    SELF.Q.status_tag_temp_Icon = 2
  ELSE
    SELF.Q.status_tag_temp_Icon = 1
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue4.Pointer4 = sts:Status
     GET(GLO:Queue4,GLO:Queue4.Pointer4)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue


BRW10.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue5.Pointer5 = tur:Turnaround_Time
     GET(GLO:Queue5,GLO:Queue5.Pointer5)
    IF ERRORCODE()
      turnaround_tag_temp = ''
    ELSE
      turnaround_tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (turnaround_tag_temp = '*')
    SELF.Q.turnaround_tag_temp_Icon = 2
  ELSE
    SELF.Q.turnaround_tag_temp_Icon = 1
  END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW10::RecordStatus=ReturnValue
  IF BRW10::RecordStatus NOT=Record:OK THEN RETURN BRW10::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue5.Pointer5 = tur:Turnaround_Time
     GET(GLO:Queue5,GLO:Queue5.Pointer5)
    EXECUTE DASBRW::11:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW10::RecordStatus
  RETURN ReturnValue


BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue6.Pointer6 = use:User_Code
     GET(GLO:Queue6,GLO:Queue6.Pointer6)
    IF ERRORCODE()
      engineer_tag_temp = ''
    ELSE
      engineer_tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (engineer_tag_temp = '*')
    SELF.Q.engineer_tag_temp_Icon = 2
  ELSE
    SELF.Q.engineer_tag_temp_Icon = 1
  END


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue6.Pointer6 = use:User_Code
     GET(GLO:Queue6,GLO:Queue6.Pointer6)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue


BRW14.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue7.Pointer7 = mod:Model_Number
     GET(GLO:Queue7,GLO:Queue7.Pointer7)
    IF ERRORCODE()
      model_tag_temp = ''
    ELSE
      model_tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (model_tag_temp = '*')
    SELF.Q.model_tag_temp_Icon = 2
  ELSE
    SELF.Q.model_tag_temp_Icon = 1
  END


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW14::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW14::RecordStatus=ReturnValue
  IF BRW14::RecordStatus NOT=Record:OK THEN RETURN BRW14::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue7.Pointer7 = mod:Model_Number
     GET(GLO:Queue7,GLO:Queue7.Pointer7)
    EXECUTE DASBRW::15:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW14::RecordStatus
  RETURN ReturnValue

Wizard31.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()


Wizard31.TakeBackEmbed PROCEDURE
   CODE

Wizard31.TakeNextEmbed PROCEDURE
   CODE

Wizard31.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
