

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01051.INC'),ONCE        !Local module procedure declarations
                     END


Credit_Part PROCEDURE (f_type,f_record_number)        !Generated from procedure template - Window

tmp:credit_quantity  LONG
tmp:part_number      STRING(30)
tmp:description      STRING(30)
tmp:quantity         LONG
tmp:item_cost        REAL
tmp:original_total   REAL
tmp:new_total        REAL
ActionMessage        CSTRING(40)
tmp:purchase_cost    REAL
tmp:trade_cost       REAL
tmp:retail_cost      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Credit Part'),AT(,,435,163),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),TIMER(50),GRAY,DOUBLE
                       SHEET,AT(4,4,212,128),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Original Part Details'),USE(?Tab1)
                           STRING('Part Number'),AT(8,20),USE(?String1),FONT(,10,,)
                           STRING(@s30),AT(84,20,128,12),USE(tmp:part_number),FONT(,10,,FONT:bold)
                           STRING('Original Part Details'),AT(8,8),USE(?String1:10),FONT(,10,COLOR:Navy,FONT:bold)
                           STRING(@s30),AT(84,36,128,12),USE(tmp:description),FONT(,10,,FONT:bold)
                           GROUP,AT(4,48,364,16),USE(?warranty_group),HIDE
                             STRING('Purchase Cost'),AT(8,52),USE(?String1:5),FONT(,8,,)
                             STRING(@n14.2),AT(84,52,52,12),USE(tmp:purchase_cost),RIGHT,FONT(,8,,FONT:bold)
                             PROMPT('Purchase Cost'),AT(224,52),USE(?IVPTMP:PurchaseCost:Prompt)
                             ENTRY(@n14.2),AT(300,52,64,10),USE(ivptmp:PurchaseCost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Purchase Cost'),TIP('Retail Stock Purchase Cost'),UPR
                           END
                           GROUP,AT(4,64,364,16),USE(?chargeable_group),HIDE
                             STRING('Trade Price'),AT(8,68),USE(?String1:6),FONT(,8,,)
                             STRING(@n14.2),AT(84,68,52,12),USE(tmp:trade_cost),RIGHT,FONT(,8,,FONT:bold)
                             PROMPT('Sale Cost'),AT(224,68),USE(?IVPTMP:SaleCost:Prompt)
                             ENTRY(@n14.2),AT(300,68,64,10),USE(ivptmp:SaleCost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Sale Cost'),TIP('Retail Stock Sale Cost'),UPR
                           END
                           GROUP,AT(4,80,364,16),USE(?Retail_Group),HIDE
                             STRING('Item Cost'),AT(8,84),USE(?String1:7),FONT(,8,,)
                             STRING(@n14.2),AT(84,84,52,12),USE(tmp:retail_cost),RIGHT,FONT(,8,,FONT:bold)
                             PROMPT('Item Cost'),AT(224,84),USE(?IVPTMP:RetailCost:Prompt)
                             ENTRY(@n14.2),AT(300,84,64,10),USE(ivptmp:RetailCost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Retail Cost'),TIP('Retail Stock Retail Cost'),UPR
                           END
                           STRING('Quantity'),AT(8,100),USE(?String1:8),FONT(,8,,)
                           STRING('Total Cost'),AT(8,116),USE(?String1:9),FONT(,8,,)
                           STRING(@s8),AT(84,100,52,12),USE(tmp:quantity),RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n14.2),AT(84,116,52,12),USE(tmp:original_total),RIGHT,FONT(,8,,FONT:bold)
                           STRING('Description'),AT(8,36),USE(?String1:2),FONT(,10,,)
                         END
                       END
                       SHEET,AT(220,4,212,128),USE(?Sheet1:2),WIZARD,SPREAD
                         TAB('Credit Part Details'),USE(?Tab2)
                           STRING('Credit Part Details'),AT(224,8),USE(?String1:11),FONT(,10,COLOR:Navy,FONT:bold)
                           STRING('Part Number'),AT(224,20),USE(?String1:3),FONT(,10,,)
                           STRING(@s30),AT(300,20,128,12),USE(ivptmp:PartNumber),FONT(,10,,FONT:bold)
                           STRING('Description'),AT(224,36),USE(?String1:4),FONT(,10,,)
                           STRING(@s30),AT(300,36,128,12),USE(ivptmp:Description),FONT(,10,,FONT:bold)
                           PROMPT('Credit Quantity'),AT(224,100),USE(?IVPTMP:CreditQuantity:Prompt)
                           SPIN(@n-14),AT(300,100,64,10),USE(ivptmp:CreditQuantity),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI),MSG('Quantity Credited'),TIP('Quantity Credited'),UPR,STEP(1)
                           PROMPT('Credited Total'),AT(224,116),USE(?tmp:new_total:Prompt)
                           ENTRY(@N14.2),AT(300,116,64,10),USE(tmp:new_total),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                         END
                       END
                       PANEL,AT(4,136,428,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(316,140,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(372,140,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?tmp:part_number{prop:FontColor} = -1
    ?tmp:part_number{prop:Color} = 15066597
    ?String1:10{prop:FontColor} = -1
    ?String1:10{prop:Color} = 15066597
    ?tmp:description{prop:FontColor} = -1
    ?tmp:description{prop:Color} = 15066597
    ?warranty_group{prop:Font,3} = -1
    ?warranty_group{prop:Color} = 15066597
    ?warranty_group{prop:Trn} = 0
    ?String1:5{prop:FontColor} = -1
    ?String1:5{prop:Color} = 15066597
    ?tmp:purchase_cost{prop:FontColor} = -1
    ?tmp:purchase_cost{prop:Color} = 15066597
    ?IVPTMP:PurchaseCost:Prompt{prop:FontColor} = -1
    ?IVPTMP:PurchaseCost:Prompt{prop:Color} = 15066597
    If ?ivptmp:PurchaseCost{prop:ReadOnly} = True
        ?ivptmp:PurchaseCost{prop:FontColor} = 65793
        ?ivptmp:PurchaseCost{prop:Color} = 15066597
    Elsif ?ivptmp:PurchaseCost{prop:Req} = True
        ?ivptmp:PurchaseCost{prop:FontColor} = 65793
        ?ivptmp:PurchaseCost{prop:Color} = 8454143
    Else ! If ?ivptmp:PurchaseCost{prop:Req} = True
        ?ivptmp:PurchaseCost{prop:FontColor} = 65793
        ?ivptmp:PurchaseCost{prop:Color} = 16777215
    End ! If ?ivptmp:PurchaseCost{prop:Req} = True
    ?ivptmp:PurchaseCost{prop:Trn} = 0
    ?ivptmp:PurchaseCost{prop:FontStyle} = font:Bold
    ?chargeable_group{prop:Font,3} = -1
    ?chargeable_group{prop:Color} = 15066597
    ?chargeable_group{prop:Trn} = 0
    ?String1:6{prop:FontColor} = -1
    ?String1:6{prop:Color} = 15066597
    ?tmp:trade_cost{prop:FontColor} = -1
    ?tmp:trade_cost{prop:Color} = 15066597
    ?IVPTMP:SaleCost:Prompt{prop:FontColor} = -1
    ?IVPTMP:SaleCost:Prompt{prop:Color} = 15066597
    If ?ivptmp:SaleCost{prop:ReadOnly} = True
        ?ivptmp:SaleCost{prop:FontColor} = 65793
        ?ivptmp:SaleCost{prop:Color} = 15066597
    Elsif ?ivptmp:SaleCost{prop:Req} = True
        ?ivptmp:SaleCost{prop:FontColor} = 65793
        ?ivptmp:SaleCost{prop:Color} = 8454143
    Else ! If ?ivptmp:SaleCost{prop:Req} = True
        ?ivptmp:SaleCost{prop:FontColor} = 65793
        ?ivptmp:SaleCost{prop:Color} = 16777215
    End ! If ?ivptmp:SaleCost{prop:Req} = True
    ?ivptmp:SaleCost{prop:Trn} = 0
    ?ivptmp:SaleCost{prop:FontStyle} = font:Bold
    ?Retail_Group{prop:Font,3} = -1
    ?Retail_Group{prop:Color} = 15066597
    ?Retail_Group{prop:Trn} = 0
    ?String1:7{prop:FontColor} = -1
    ?String1:7{prop:Color} = 15066597
    ?tmp:retail_cost{prop:FontColor} = -1
    ?tmp:retail_cost{prop:Color} = 15066597
    ?IVPTMP:RetailCost:Prompt{prop:FontColor} = -1
    ?IVPTMP:RetailCost:Prompt{prop:Color} = 15066597
    If ?ivptmp:RetailCost{prop:ReadOnly} = True
        ?ivptmp:RetailCost{prop:FontColor} = 65793
        ?ivptmp:RetailCost{prop:Color} = 15066597
    Elsif ?ivptmp:RetailCost{prop:Req} = True
        ?ivptmp:RetailCost{prop:FontColor} = 65793
        ?ivptmp:RetailCost{prop:Color} = 8454143
    Else ! If ?ivptmp:RetailCost{prop:Req} = True
        ?ivptmp:RetailCost{prop:FontColor} = 65793
        ?ivptmp:RetailCost{prop:Color} = 16777215
    End ! If ?ivptmp:RetailCost{prop:Req} = True
    ?ivptmp:RetailCost{prop:Trn} = 0
    ?ivptmp:RetailCost{prop:FontStyle} = font:Bold
    ?String1:8{prop:FontColor} = -1
    ?String1:8{prop:Color} = 15066597
    ?String1:9{prop:FontColor} = -1
    ?String1:9{prop:Color} = 15066597
    ?tmp:quantity{prop:FontColor} = -1
    ?tmp:quantity{prop:Color} = 15066597
    ?tmp:original_total{prop:FontColor} = -1
    ?tmp:original_total{prop:Color} = 15066597
    ?String1:2{prop:FontColor} = -1
    ?String1:2{prop:Color} = 15066597
    ?Sheet1:2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?String1:11{prop:FontColor} = -1
    ?String1:11{prop:Color} = 15066597
    ?String1:3{prop:FontColor} = -1
    ?String1:3{prop:Color} = 15066597
    ?ivptmp:PartNumber{prop:FontColor} = -1
    ?ivptmp:PartNumber{prop:Color} = 15066597
    ?String1:4{prop:FontColor} = -1
    ?String1:4{prop:Color} = 15066597
    ?ivptmp:Description{prop:FontColor} = -1
    ?ivptmp:Description{prop:Color} = 15066597
    ?IVPTMP:CreditQuantity:Prompt{prop:FontColor} = -1
    ?IVPTMP:CreditQuantity:Prompt{prop:Color} = 15066597
    If ?ivptmp:CreditQuantity{prop:ReadOnly} = True
        ?ivptmp:CreditQuantity{prop:FontColor} = 65793
        ?ivptmp:CreditQuantity{prop:Color} = 15066597
    Elsif ?ivptmp:CreditQuantity{prop:Req} = True
        ?ivptmp:CreditQuantity{prop:FontColor} = 65793
        ?ivptmp:CreditQuantity{prop:Color} = 8454143
    Else ! If ?ivptmp:CreditQuantity{prop:Req} = True
        ?ivptmp:CreditQuantity{prop:FontColor} = 65793
        ?ivptmp:CreditQuantity{prop:Color} = 16777215
    End ! If ?ivptmp:CreditQuantity{prop:Req} = True
    ?ivptmp:CreditQuantity{prop:Trn} = 0
    ?ivptmp:CreditQuantity{prop:FontStyle} = font:Bold
    ?tmp:new_total:Prompt{prop:FontColor} = -1
    ?tmp:new_total:Prompt{prop:Color} = 15066597
    If ?tmp:new_total{prop:ReadOnly} = True
        ?tmp:new_total{prop:FontColor} = 65793
        ?tmp:new_total{prop:Color} = 15066597
    Elsif ?tmp:new_total{prop:Req} = True
        ?tmp:new_total{prop:FontColor} = 65793
        ?tmp:new_total{prop:Color} = 8454143
    Else ! If ?tmp:new_total{prop:Req} = True
        ?tmp:new_total{prop:FontColor} = 65793
        ?tmp:new_total{prop:Color} = 16777215
    End ! If ?tmp:new_total{prop:Req} = True
    ?tmp:new_total{prop:Trn} = 0
    ?tmp:new_total{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Credit_Part',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:credit_quantity',tmp:credit_quantity,'Credit_Part',1)
    SolaceViewVars('tmp:part_number',tmp:part_number,'Credit_Part',1)
    SolaceViewVars('tmp:description',tmp:description,'Credit_Part',1)
    SolaceViewVars('tmp:quantity',tmp:quantity,'Credit_Part',1)
    SolaceViewVars('tmp:item_cost',tmp:item_cost,'Credit_Part',1)
    SolaceViewVars('tmp:original_total',tmp:original_total,'Credit_Part',1)
    SolaceViewVars('tmp:new_total',tmp:new_total,'Credit_Part',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Credit_Part',1)
    SolaceViewVars('tmp:purchase_cost',tmp:purchase_cost,'Credit_Part',1)
    SolaceViewVars('tmp:trade_cost',tmp:trade_cost,'Credit_Part',1)
    SolaceViewVars('tmp:retail_cost',tmp:retail_cost,'Credit_Part',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:part_number;  SolaceCtrlName = '?tmp:part_number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:10;  SolaceCtrlName = '?String1:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:description;  SolaceCtrlName = '?tmp:description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?warranty_group;  SolaceCtrlName = '?warranty_group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:5;  SolaceCtrlName = '?String1:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:purchase_cost;  SolaceCtrlName = '?tmp:purchase_cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:PurchaseCost:Prompt;  SolaceCtrlName = '?IVPTMP:PurchaseCost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:PurchaseCost;  SolaceCtrlName = '?ivptmp:PurchaseCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?chargeable_group;  SolaceCtrlName = '?chargeable_group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:6;  SolaceCtrlName = '?String1:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:trade_cost;  SolaceCtrlName = '?tmp:trade_cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:SaleCost:Prompt;  SolaceCtrlName = '?IVPTMP:SaleCost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:SaleCost;  SolaceCtrlName = '?ivptmp:SaleCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Retail_Group;  SolaceCtrlName = '?Retail_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:7;  SolaceCtrlName = '?String1:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:retail_cost;  SolaceCtrlName = '?tmp:retail_cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:RetailCost:Prompt;  SolaceCtrlName = '?IVPTMP:RetailCost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:RetailCost;  SolaceCtrlName = '?ivptmp:RetailCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:8;  SolaceCtrlName = '?String1:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:9;  SolaceCtrlName = '?String1:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:quantity;  SolaceCtrlName = '?tmp:quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:original_total;  SolaceCtrlName = '?tmp:original_total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:2;  SolaceCtrlName = '?String1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1:2;  SolaceCtrlName = '?Sheet1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:11;  SolaceCtrlName = '?String1:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:3;  SolaceCtrlName = '?String1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:PartNumber;  SolaceCtrlName = '?ivptmp:PartNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1:4;  SolaceCtrlName = '?String1:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:Description;  SolaceCtrlName = '?ivptmp:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:CreditQuantity:Prompt;  SolaceCtrlName = '?IVPTMP:CreditQuantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:CreditQuantity;  SolaceCtrlName = '?ivptmp:CreditQuantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:new_total:Prompt;  SolaceCtrlName = '?tmp:new_total:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:new_total;  SolaceCtrlName = '?tmp:new_total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Credit Part'
  OF ChangeRecord
    ActionMessage = 'Credit Part'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Credit_Part')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Credit_Part')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:INVPATMP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:INVPATMP.Open
  Relate:PARTS.Open
  Relate:RETSTOCK.Open
  Access:WARPARTS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:INVPATMP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Case f_type
      Of 'SIN'
          Unhide(?chargeable_group)
              access:parts.clearkey(par:recordnumberkey)
              par:record_number = f_record_number
              if access:parts.tryfetch(par:recordnumberkey)
                  Return Level:Fatal
              Else!if access:parts.tryfetch(par:record_number_key)
                  tmp:part_number     = par:part_number
                  tmp:description     = par:description
                  tmp:quantity        = par:quantity
                  tmp:purchase_cost   = par:purchase_cost
                  tmp:trade_cost      = par:Sale_Cost
                  tmp:retail_cost     = par:retail_cost
                  tmp:original_total  = 0
  
                  IVPTMP:PartNumber   = par:part_number
                  IVPTMP:Description  = par:description
                  IVPTMP:PurchaseCost = par:purchase_cost
                  IVPTMP:SaleCost     = par:sale_cost
                  IVPTMP:RetailCost   = par:Retail_cost
                  IVPTMP:RetstockNumber   = par:record_number
                  IVPTMP:CreditQuantity   = par:quantity
                  IVPTMP:Supplier         = par:supplier
                  IVPTMP:JobNumber        = par:ref_number
  
              End!if access:parts.tryfetch(par:record_number_key)
  
      Of 'WAR'
          Unhide(?Warranty_group)
      Of 'RET'
          Unhide(?retail_group)
          access:retstock.clearkey(res:record_number_key)
          res:record_number = f_record_number
          if access:retstock.tryfetch(res:record_number_key)
              return Level:fatal
          Else
              tmp:part_number     = res:part_number
              tmp:description     = res:description
              tmp:quantity        = res:quantity
              tmp:purchase_cost   = res:purchase_cost
              tmp:trade_cost      = RES:Sale_Cost
              tmp:retail_cost     = res:item_cost
              tmp:original_total  = 0
  
              IVPTMP:PartNumber   = res:part_number
              IVPTMP:Description  = res:description
              IVPTMP:PurchaseCost = res:purchase_cost
              IVPTMP:SaleCost     = res:sale_cost
              IVPTMP:RetailCost   = res:item_cost
              IVPTMP:RetstockNumber   = res:record_number
              IVPTMP:CreditQuantity   = res:quantity
              IVPTMP:Supplier         = res:supplier
              IVPTMP:JobNUmber        = res:ref_number
              ?ivptmp:CreditQuantity{prop:range,1} = 1
              ?ivptmp:CreditQuantity{prop:range,2} = tmp:quantity
          end!if access:retstock.tryfetch(res:record_number_key) = Level:Benign
  End!Case f_type
  ?ivptmp:CreditQuantity{prop:range,1} = 1
  ?ivptmp:CreditQuantity{prop:range,2} = tmp:quantity
  
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVPATMP.Close
    Relate:PARTS.Close
    Relate:RETSTOCK.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Credit_Part',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Error Checking
  If thiswindow.request = Insertrecord
      error# = 0
      Case f_type
          Of 'RET'
              If ivptmp:CreditQuantity < tmp:quantity and error# = 0
                  Case MessageEx('Are you sure you want to credit LESS than the original quantity?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          ivptmp:CreditQuantity   = tmp:Quantity
                          error# = 1
                  End!Case MessageEx
              End!If ivptmp:CreditQuantity < tmp:quantity
              IF ivptmp:RetailCost <> tmp:retail_cost And error# = 0
                  Case MessageEx('Are you sure you want to change the Credited Item Cost?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          ivptmp:RetailCost   = tmp:retail_cost
                          error# = 1
                  End!Case MessageEx
              End!IF ivptmp:RetailCost <> tmp:retail_cost And error# = 0
          Of 'CHA'
          Of 'WAR'
          Of 'SIN'
  
      End!Case f_type
  End!If thiswindow.request = Insertrecord
  If error# = 1
      Display()
      Cycle
  
  End!If error# = 1
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Credit_Part')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
      !Totals
      Case f_type
          Of 'CHA'
              tmp:original_total  = tmp:trade_cost * tmp:quantity
              tmp:new_total   = IVPTMP:SaleCost * IVPTMP:CreditQuantity
          Of 'WAR'
              tmp:original_total  = tmp:purchase_cost * tmp:quantity
              tmp:new_total   = IVPTMP:PurchaseCost * IVPTMP:CreditQuantity
          Of 'RET'
              tmp:original_total  = tmp:retail_cost * tmp:quantity
              tmp:new_total   = IVPTMP:RetailCost * IVPTMP:CreditQuantity
      End!Case f_type
      Display(?tmp:original_total)
      Display(?tmp:new_total)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

