

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01049.INC'),ONCE        !Local module procedure declarations
                     END


Delete_Invoice_Parts PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::ivptmp:Record LIKE(ivptmp:RECORD),STATIC
QuickWindow          WINDOW('Update the INVPATMP File'),AT(,,208,182),FONT('MS Sans Serif',8,,),IMM,HLP('Delete_Invoice_Parts'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,200,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number:'),AT(8,20),USE(?IVPTMP:RecordNumber:Prompt)
                           ENTRY(@n-14),AT(76,20,64,10),USE(ivptmp:RecordNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('AutoNumber Field'),TIP('AutoNumber Field'),UPR
                           PROMPT('Invoice Number:'),AT(8,34),USE(?IVPTMP:InvoiceNumber:Prompt)
                           ENTRY(@n-14),AT(76,34,64,10),USE(ivptmp:InvoiceNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Link To Invoice'),TIP('Link To Invoice'),UPR
                           PROMPT('Retstock Number:'),AT(8,48),USE(?IVPTMP:RetstockNumber:Prompt)
                           ENTRY(@n-14),AT(76,48,64,10),USE(ivptmp:RetstockNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Link to the Retail Item''s Record Number'),TIP('Link to the Retail Item''s Record Number'),UPR
                           PROMPT('Credit Quantity'),AT(8,62),USE(?IVPTMP:CreditQuantity:Prompt)
                           SPIN(@n-14),AT(76,62,79,10),USE(ivptmp:CreditQuantity),LEFT,MSG('Quantity Credited'),TIP('Quantity Credited'),UPR,STEP(1)
                           PROMPT('Part Number'),AT(8,76),USE(?IVPTMP:PartNumber:Prompt)
                           ENTRY(@s30),AT(76,76,124,10),USE(ivptmp:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Part Number'),TIP('Retail Stock Part Number'),UPR
                           PROMPT('Description'),AT(8,90),USE(?IVPTMP:Description:Prompt)
                           ENTRY(@s30),AT(76,90,124,10),USE(ivptmp:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Description'),TIP('Retail Stock Description'),UPR
                           PROMPT('Supplier'),AT(8,104),USE(?IVPTMP:Supplier:Prompt)
                           ENTRY(@s30),AT(76,104,124,10),USE(ivptmp:Supplier),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Supplier'),TIP('Retail Stock Supplier'),UPR
                           PROMPT('Purchase Cost'),AT(8,118),USE(?IVPTMP:PurchaseCost:Prompt)
                           ENTRY(@n14.2),AT(76,118,60,10),USE(ivptmp:PurchaseCost),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Purchase Cost'),TIP('Retail Stock Purchase Cost'),UPR
                           PROMPT('Sale Cost'),AT(8,132),USE(?IVPTMP:SaleCost:Prompt)
                           ENTRY(@n14.2),AT(76,132,60,10),USE(ivptmp:SaleCost),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Sale Cost'),TIP('Retail Stock Sale Cost'),UPR
                           PROMPT('Retail Cost'),AT(8,146),USE(?IVPTMP:RetailCost:Prompt)
                           ENTRY(@n14.2),AT(76,146,60,10),USE(ivptmp:RetailCost),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Retail Cost'),TIP('Retail Stock Retail Cost'),UPR
                         END
                       END
                       BUTTON('OK'),AT(110,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(159,164,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(159,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?IVPTMP:RecordNumber:Prompt{prop:FontColor} = -1
    ?IVPTMP:RecordNumber:Prompt{prop:Color} = 15066597
    If ?ivptmp:RecordNumber{prop:ReadOnly} = True
        ?ivptmp:RecordNumber{prop:FontColor} = 65793
        ?ivptmp:RecordNumber{prop:Color} = 15066597
    Elsif ?ivptmp:RecordNumber{prop:Req} = True
        ?ivptmp:RecordNumber{prop:FontColor} = 65793
        ?ivptmp:RecordNumber{prop:Color} = 8454143
    Else ! If ?ivptmp:RecordNumber{prop:Req} = True
        ?ivptmp:RecordNumber{prop:FontColor} = 65793
        ?ivptmp:RecordNumber{prop:Color} = 16777215
    End ! If ?ivptmp:RecordNumber{prop:Req} = True
    ?ivptmp:RecordNumber{prop:Trn} = 0
    ?ivptmp:RecordNumber{prop:FontStyle} = font:Bold
    ?IVPTMP:InvoiceNumber:Prompt{prop:FontColor} = -1
    ?IVPTMP:InvoiceNumber:Prompt{prop:Color} = 15066597
    If ?ivptmp:InvoiceNumber{prop:ReadOnly} = True
        ?ivptmp:InvoiceNumber{prop:FontColor} = 65793
        ?ivptmp:InvoiceNumber{prop:Color} = 15066597
    Elsif ?ivptmp:InvoiceNumber{prop:Req} = True
        ?ivptmp:InvoiceNumber{prop:FontColor} = 65793
        ?ivptmp:InvoiceNumber{prop:Color} = 8454143
    Else ! If ?ivptmp:InvoiceNumber{prop:Req} = True
        ?ivptmp:InvoiceNumber{prop:FontColor} = 65793
        ?ivptmp:InvoiceNumber{prop:Color} = 16777215
    End ! If ?ivptmp:InvoiceNumber{prop:Req} = True
    ?ivptmp:InvoiceNumber{prop:Trn} = 0
    ?ivptmp:InvoiceNumber{prop:FontStyle} = font:Bold
    ?IVPTMP:RetstockNumber:Prompt{prop:FontColor} = -1
    ?IVPTMP:RetstockNumber:Prompt{prop:Color} = 15066597
    If ?ivptmp:RetstockNumber{prop:ReadOnly} = True
        ?ivptmp:RetstockNumber{prop:FontColor} = 65793
        ?ivptmp:RetstockNumber{prop:Color} = 15066597
    Elsif ?ivptmp:RetstockNumber{prop:Req} = True
        ?ivptmp:RetstockNumber{prop:FontColor} = 65793
        ?ivptmp:RetstockNumber{prop:Color} = 8454143
    Else ! If ?ivptmp:RetstockNumber{prop:Req} = True
        ?ivptmp:RetstockNumber{prop:FontColor} = 65793
        ?ivptmp:RetstockNumber{prop:Color} = 16777215
    End ! If ?ivptmp:RetstockNumber{prop:Req} = True
    ?ivptmp:RetstockNumber{prop:Trn} = 0
    ?ivptmp:RetstockNumber{prop:FontStyle} = font:Bold
    ?IVPTMP:CreditQuantity:Prompt{prop:FontColor} = -1
    ?IVPTMP:CreditQuantity:Prompt{prop:Color} = 15066597
    If ?ivptmp:CreditQuantity{prop:ReadOnly} = True
        ?ivptmp:CreditQuantity{prop:FontColor} = 65793
        ?ivptmp:CreditQuantity{prop:Color} = 15066597
    Elsif ?ivptmp:CreditQuantity{prop:Req} = True
        ?ivptmp:CreditQuantity{prop:FontColor} = 65793
        ?ivptmp:CreditQuantity{prop:Color} = 8454143
    Else ! If ?ivptmp:CreditQuantity{prop:Req} = True
        ?ivptmp:CreditQuantity{prop:FontColor} = 65793
        ?ivptmp:CreditQuantity{prop:Color} = 16777215
    End ! If ?ivptmp:CreditQuantity{prop:Req} = True
    ?ivptmp:CreditQuantity{prop:Trn} = 0
    ?ivptmp:CreditQuantity{prop:FontStyle} = font:Bold
    ?IVPTMP:PartNumber:Prompt{prop:FontColor} = -1
    ?IVPTMP:PartNumber:Prompt{prop:Color} = 15066597
    If ?ivptmp:PartNumber{prop:ReadOnly} = True
        ?ivptmp:PartNumber{prop:FontColor} = 65793
        ?ivptmp:PartNumber{prop:Color} = 15066597
    Elsif ?ivptmp:PartNumber{prop:Req} = True
        ?ivptmp:PartNumber{prop:FontColor} = 65793
        ?ivptmp:PartNumber{prop:Color} = 8454143
    Else ! If ?ivptmp:PartNumber{prop:Req} = True
        ?ivptmp:PartNumber{prop:FontColor} = 65793
        ?ivptmp:PartNumber{prop:Color} = 16777215
    End ! If ?ivptmp:PartNumber{prop:Req} = True
    ?ivptmp:PartNumber{prop:Trn} = 0
    ?ivptmp:PartNumber{prop:FontStyle} = font:Bold
    ?IVPTMP:Description:Prompt{prop:FontColor} = -1
    ?IVPTMP:Description:Prompt{prop:Color} = 15066597
    If ?ivptmp:Description{prop:ReadOnly} = True
        ?ivptmp:Description{prop:FontColor} = 65793
        ?ivptmp:Description{prop:Color} = 15066597
    Elsif ?ivptmp:Description{prop:Req} = True
        ?ivptmp:Description{prop:FontColor} = 65793
        ?ivptmp:Description{prop:Color} = 8454143
    Else ! If ?ivptmp:Description{prop:Req} = True
        ?ivptmp:Description{prop:FontColor} = 65793
        ?ivptmp:Description{prop:Color} = 16777215
    End ! If ?ivptmp:Description{prop:Req} = True
    ?ivptmp:Description{prop:Trn} = 0
    ?ivptmp:Description{prop:FontStyle} = font:Bold
    ?IVPTMP:Supplier:Prompt{prop:FontColor} = -1
    ?IVPTMP:Supplier:Prompt{prop:Color} = 15066597
    If ?ivptmp:Supplier{prop:ReadOnly} = True
        ?ivptmp:Supplier{prop:FontColor} = 65793
        ?ivptmp:Supplier{prop:Color} = 15066597
    Elsif ?ivptmp:Supplier{prop:Req} = True
        ?ivptmp:Supplier{prop:FontColor} = 65793
        ?ivptmp:Supplier{prop:Color} = 8454143
    Else ! If ?ivptmp:Supplier{prop:Req} = True
        ?ivptmp:Supplier{prop:FontColor} = 65793
        ?ivptmp:Supplier{prop:Color} = 16777215
    End ! If ?ivptmp:Supplier{prop:Req} = True
    ?ivptmp:Supplier{prop:Trn} = 0
    ?ivptmp:Supplier{prop:FontStyle} = font:Bold
    ?IVPTMP:PurchaseCost:Prompt{prop:FontColor} = -1
    ?IVPTMP:PurchaseCost:Prompt{prop:Color} = 15066597
    If ?ivptmp:PurchaseCost{prop:ReadOnly} = True
        ?ivptmp:PurchaseCost{prop:FontColor} = 65793
        ?ivptmp:PurchaseCost{prop:Color} = 15066597
    Elsif ?ivptmp:PurchaseCost{prop:Req} = True
        ?ivptmp:PurchaseCost{prop:FontColor} = 65793
        ?ivptmp:PurchaseCost{prop:Color} = 8454143
    Else ! If ?ivptmp:PurchaseCost{prop:Req} = True
        ?ivptmp:PurchaseCost{prop:FontColor} = 65793
        ?ivptmp:PurchaseCost{prop:Color} = 16777215
    End ! If ?ivptmp:PurchaseCost{prop:Req} = True
    ?ivptmp:PurchaseCost{prop:Trn} = 0
    ?ivptmp:PurchaseCost{prop:FontStyle} = font:Bold
    ?IVPTMP:SaleCost:Prompt{prop:FontColor} = -1
    ?IVPTMP:SaleCost:Prompt{prop:Color} = 15066597
    If ?ivptmp:SaleCost{prop:ReadOnly} = True
        ?ivptmp:SaleCost{prop:FontColor} = 65793
        ?ivptmp:SaleCost{prop:Color} = 15066597
    Elsif ?ivptmp:SaleCost{prop:Req} = True
        ?ivptmp:SaleCost{prop:FontColor} = 65793
        ?ivptmp:SaleCost{prop:Color} = 8454143
    Else ! If ?ivptmp:SaleCost{prop:Req} = True
        ?ivptmp:SaleCost{prop:FontColor} = 65793
        ?ivptmp:SaleCost{prop:Color} = 16777215
    End ! If ?ivptmp:SaleCost{prop:Req} = True
    ?ivptmp:SaleCost{prop:Trn} = 0
    ?ivptmp:SaleCost{prop:FontStyle} = font:Bold
    ?IVPTMP:RetailCost:Prompt{prop:FontColor} = -1
    ?IVPTMP:RetailCost:Prompt{prop:Color} = 15066597
    If ?ivptmp:RetailCost{prop:ReadOnly} = True
        ?ivptmp:RetailCost{prop:FontColor} = 65793
        ?ivptmp:RetailCost{prop:Color} = 15066597
    Elsif ?ivptmp:RetailCost{prop:Req} = True
        ?ivptmp:RetailCost{prop:FontColor} = 65793
        ?ivptmp:RetailCost{prop:Color} = 8454143
    Else ! If ?ivptmp:RetailCost{prop:Req} = True
        ?ivptmp:RetailCost{prop:FontColor} = 65793
        ?ivptmp:RetailCost{prop:Color} = 16777215
    End ! If ?ivptmp:RetailCost{prop:Req} = True
    ?ivptmp:RetailCost{prop:Trn} = 0
    ?ivptmp:RetailCost{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Delete_Invoice_Parts',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Delete_Invoice_Parts',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Delete_Invoice_Parts',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:RecordNumber:Prompt;  SolaceCtrlName = '?IVPTMP:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:RecordNumber;  SolaceCtrlName = '?ivptmp:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:InvoiceNumber:Prompt;  SolaceCtrlName = '?IVPTMP:InvoiceNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:InvoiceNumber;  SolaceCtrlName = '?ivptmp:InvoiceNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:RetstockNumber:Prompt;  SolaceCtrlName = '?IVPTMP:RetstockNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:RetstockNumber;  SolaceCtrlName = '?ivptmp:RetstockNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:CreditQuantity:Prompt;  SolaceCtrlName = '?IVPTMP:CreditQuantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:CreditQuantity;  SolaceCtrlName = '?ivptmp:CreditQuantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:PartNumber:Prompt;  SolaceCtrlName = '?IVPTMP:PartNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:PartNumber;  SolaceCtrlName = '?ivptmp:PartNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:Description:Prompt;  SolaceCtrlName = '?IVPTMP:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:Description;  SolaceCtrlName = '?ivptmp:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:Supplier:Prompt;  SolaceCtrlName = '?IVPTMP:Supplier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:Supplier;  SolaceCtrlName = '?ivptmp:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:PurchaseCost:Prompt;  SolaceCtrlName = '?IVPTMP:PurchaseCost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:PurchaseCost;  SolaceCtrlName = '?ivptmp:PurchaseCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:SaleCost:Prompt;  SolaceCtrlName = '?IVPTMP:SaleCost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:SaleCost;  SolaceCtrlName = '?ivptmp:SaleCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IVPTMP:RetailCost:Prompt;  SolaceCtrlName = '?IVPTMP:RetailCost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ivptmp:RetailCost;  SolaceCtrlName = '?ivptmp:RetailCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Delete_Invoice_Parts')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Delete_Invoice_Parts')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?IVPTMP:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ivptmp:Record,History::ivptmp:Record)
  SELF.AddHistoryField(?ivptmp:RecordNumber,1)
  SELF.AddHistoryField(?ivptmp:InvoiceNumber,2)
  SELF.AddHistoryField(?ivptmp:RetstockNumber,3)
  SELF.AddHistoryField(?ivptmp:CreditQuantity,4)
  SELF.AddHistoryField(?ivptmp:PartNumber,5)
  SELF.AddHistoryField(?ivptmp:Description,6)
  SELF.AddHistoryField(?ivptmp:Supplier,7)
  SELF.AddHistoryField(?ivptmp:PurchaseCost,8)
  SELF.AddHistoryField(?ivptmp:SaleCost,9)
  SELF.AddHistoryField(?ivptmp:RetailCost,10)
  SELF.AddUpdateFile(Access:INVPATMP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:INVPATMP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:INVPATMP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVPATMP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Delete_Invoice_Parts',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Delete_Invoice_Parts')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

