

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01050.INC'),ONCE        !Local module procedure declarations
                     END


Create_Credit_Note PROCEDURE (f_invoice_number)       !Generated from procedure template - Form

ActionMessage        CSTRING(40)
save_ivptmp_id       USHORT,AUTO
invoice_total_temp   REAL
credit_parts_temp    REAL
Credit_labour_temp   REAL
Credit_Courier_Temp  REAL
Credit_Total_temp    REAL
Credit_Total_Inc_Temp REAL
tmp:parttotal        REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW5::View:Browse    VIEW(RETSTOCK)
                       PROJECT(res:Part_Number)
                       PROJECT(res:Description)
                       PROJECT(res:Quantity)
                       PROJECT(res:Credit)
                       PROJECT(res:Record_Number)
                       PROJECT(res:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
res:Part_Number        LIKE(res:Part_Number)          !List box control field - type derived from field
res:Part_Number_NormalFG LONG                         !Normal forground color
res:Part_Number_NormalBG LONG                         !Normal background color
res:Part_Number_SelectedFG LONG                       !Selected forground color
res:Part_Number_SelectedBG LONG                       !Selected background color
res:Description        LIKE(res:Description)          !List box control field - type derived from field
res:Description_NormalFG LONG                         !Normal forground color
res:Description_NormalBG LONG                         !Normal background color
res:Description_SelectedFG LONG                       !Selected forground color
res:Description_SelectedBG LONG                       !Selected background color
res:Quantity           LIKE(res:Quantity)             !List box control field - type derived from field
res:Quantity_NormalFG  LONG                           !Normal forground color
res:Quantity_NormalBG  LONG                           !Normal background color
res:Quantity_SelectedFG LONG                          !Selected forground color
res:Quantity_SelectedBG LONG                          !Selected background color
res:Credit             LIKE(res:Credit)               !List box control field - type derived from field
res:Credit_NormalFG    LONG                           !Normal forground color
res:Credit_NormalBG    LONG                           !Normal background color
res:Credit_SelectedFG  LONG                           !Selected forground color
res:Credit_SelectedBG  LONG                           !Selected background color
res:Record_Number      LIKE(res:Record_Number)        !List box control field - type derived from field
res:Record_Number_NormalFG LONG                       !Normal forground color
res:Record_Number_NormalBG LONG                       !Normal background color
res:Record_Number_SelectedFG LONG                     !Selected forground color
res:Record_Number_SelectedBG LONG                     !Selected background color
res:Ref_Number         LIKE(res:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(INVPATMP)
                       PROJECT(ivptmp:PartNumber)
                       PROJECT(ivptmp:Description)
                       PROJECT(ivptmp:CreditQuantity)
                       PROJECT(ivptmp:RecordNumber)
                       PROJECT(ivptmp:InvoiceNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
ivptmp:PartNumber      LIKE(ivptmp:PartNumber)        !List box control field - type derived from field
ivptmp:Description     LIKE(ivptmp:Description)       !List box control field - type derived from field
ivptmp:CreditQuantity  LIKE(ivptmp:CreditQuantity)    !List box control field - type derived from field
tmp:parttotal          LIKE(tmp:parttotal)            !List box control field - type derived from local data
ivptmp:RecordNumber    LIKE(ivptmp:RecordNumber)      !Primary key field - type derived from field
ivptmp:InvoiceNumber   LIKE(ivptmp:InvoiceNumber)     !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(PARTS)
                       PROJECT(par:Part_Number)
                       PROJECT(par:Description)
                       PROJECT(par:Quantity)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
par:Part_Number        LIKE(par:Part_Number)          !List box control field - type derived from field
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Quantity           LIKE(par:Quantity)             !List box control field - type derived from field
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Credit Note Production'),AT(,,411,371),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),TIMER(50),GRAY,DOUBLE
                       SHEET,AT(4,4,404,20),USE(?Sheet2),WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Created From Invoice Number:'),AT(8,8),USE(?Prompt1:2),FONT(,10,,)
                           STRING(@p<<<<<<<<#p),AT(132,8),USE(inv_ali:Invoice_Number),FONT(,10,,FONT:bold)
                         END
                       END
                       BUTTON('Credit Part'),AT(344,68,56,16),USE(?Credit_Part),HIDE,LEFT,ICON('money_sm.gif')
                       SHEET,AT(4,28,404,88),USE(?Sheet1),SPREAD
                         TAB('Retail Parts Used'),USE(?Retail_Tab),HIDE
                           LIST,AT(8,44,332,68),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M*~Part Number~@s30@120L(2)|M*~Description~@s30@41L(2)|M*~Quantity~@n8@7' &|
   '5L(2)|M*~Credited~@s3@56R(2)|M*~Record Number~L@n-14@'),FROM(Queue:Browse:1)
                         END
                         TAB('Chargeable Parts Used'),USE(?Chargeable_Tab),HIDE
                           LIST,AT(10,44,330,68),USE(?List:3),IMM,MSG('Browsing Records'),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@32D(2)|M~Quantity~L@n8@'),FROM(Queue:Browse:2)
                         END
                         TAB('Warranty'),USE(?Warranty_Tab),HIDE
                           PROMPT('You have selected to Credit A Warranty Invoice. Please note you can only do this' &|
   ' ONCE.'),AT(8,48,396,24),USE(?Prompt12),CENTER,FONT(,12,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Insert the values below that you wish to credit and then click ''Create Credit No' &|
   'te'''),AT(8,84,396,24),USE(?Prompt12:2),CENTER,FONT(,12,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       SHEET,AT(4,120,404,96),USE(?Sheet3),SPREAD
                         TAB('Parts Credited'),USE(?Credited_Tab)
                           LIST,AT(8,144,328,64),USE(?List),IMM,MSG('Browsing Records'),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@41L(2)|M~Credit Qty~@n-14@' &|
   '56R(2)|M~Total Cost~@n14.2@'),FROM(Queue:Browse)
                           BUTTON('&Change'),AT(175,183,42,12),USE(?Change),HIDE
                           BUTTON('&Delete'),AT(344,168,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                           BUTTON('&Insert'),AT(240,176,56,16),USE(?Insert),HIDE
                         END
                       END
                       SHEET,AT(4,220,404,120),USE(?Sheet4),WIZARD,SPREAD
                         TAB('Tab 4'),USE(?Tab4)
                           GROUP('Invoice Analysis'),AT(12,224,184,108),USE(?Group1),BOXED
                             PROMPT('Parts Cost'),AT(16,240),USE(?INV:Parts_Paid:Prompt)
                             ENTRY(@n14.2),AT(120,240,64,10),USE(inv_ali:Parts_Paid),SKIP,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI),READONLY
                             PROMPT('Labour Cost'),AT(16,256),USE(?INV:Labour_Paid:Prompt)
                             ENTRY(@n14.2),AT(120,256,64,10),USE(inv_ali:Labour_Paid),SKIP,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI),READONLY
                             PROMPT('Courier Cost'),AT(16,272),USE(?INV:Courier_Paid:Prompt)
                             ENTRY(@n14.2),AT(120,272,64,10),USE(inv_ali:Courier_Paid),SKIP,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI),READONLY
                             PROMPT('Invoice Total (Ex V.A.T)'),AT(16,292),USE(?INV:Total_Claimed:Prompt)
                             ENTRY(@n14.2),AT(120,292,64,10),USE(inv_ali:Total),SCROLL,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI),READONLY
                             PROMPT('Invoice Total (Inc V.A.T.)'),AT(16,316),USE(?invoice_total_temp:Prompt)
                             ENTRY(@n14.2),AT(120,316,64,10),USE(invoice_total_temp),SKIP,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI),READONLY
                           END
                           GROUP('Credit Analysis'),AT(220,224,184,108),USE(?Group2),BOXED
                             PROMPT('Parts Cost'),AT(224,240),USE(?credit_parts_temp:Prompt)
                             ENTRY(@n14.2),AT(336,240,64,10),USE(credit_parts_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Labour Cost'),AT(224,256),USE(?Credit_labour_temp:Prompt)
                             ENTRY(@n14.2),AT(336,256,64,10),USE(Credit_labour_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Courier Cost'),AT(224,272),USE(?Credit_Courier_Temp:Prompt)
                             ENTRY(@n14.2),AT(336,272,64,10),USE(Credit_Courier_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Credit Total (Ex V.A.T.)'),AT(224,292),USE(?Credit_Total_temp:Prompt)
                             ENTRY(@n14.2),AT(336,292,64,10),USE(Credit_Total_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             PROMPT('Credit Total (Inc V.A.T.)'),AT(224,316),USE(?Credit_Total_Inc_Temp:Prompt)
                             ENTRY(@n14.2),AT(336,316,64,10),USE(Credit_Total_Inc_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           END
                         END
                       END
                       PANEL,AT(4,344,404,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Create Credit Note'),AT(8,348,68,16),USE(?Create_Credit_Note),LEFT,ICON('ok.gif')
                       BUTTON('Close'),AT(348,348,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?inv_ali:Invoice_Number{prop:FontColor} = -1
    ?inv_ali:Invoice_Number{prop:Color} = 15066597
    ?Sheet1{prop:Color} = 15066597
    ?Retail_Tab{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Chargeable_Tab{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Warranty_Tab{prop:Color} = 15066597
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    ?Prompt12:2{prop:FontColor} = -1
    ?Prompt12:2{prop:Color} = 15066597
    ?Sheet3{prop:Color} = 15066597
    ?Credited_Tab{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Sheet4{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?INV:Parts_Paid:Prompt{prop:FontColor} = -1
    ?INV:Parts_Paid:Prompt{prop:Color} = 15066597
    If ?inv_ali:Parts_Paid{prop:ReadOnly} = True
        ?inv_ali:Parts_Paid{prop:FontColor} = 65793
        ?inv_ali:Parts_Paid{prop:Color} = 15066597
    Elsif ?inv_ali:Parts_Paid{prop:Req} = True
        ?inv_ali:Parts_Paid{prop:FontColor} = 65793
        ?inv_ali:Parts_Paid{prop:Color} = 8454143
    Else ! If ?inv_ali:Parts_Paid{prop:Req} = True
        ?inv_ali:Parts_Paid{prop:FontColor} = 65793
        ?inv_ali:Parts_Paid{prop:Color} = 16777215
    End ! If ?inv_ali:Parts_Paid{prop:Req} = True
    ?inv_ali:Parts_Paid{prop:Trn} = 0
    ?inv_ali:Parts_Paid{prop:FontStyle} = font:Bold
    ?INV:Labour_Paid:Prompt{prop:FontColor} = -1
    ?INV:Labour_Paid:Prompt{prop:Color} = 15066597
    If ?inv_ali:Labour_Paid{prop:ReadOnly} = True
        ?inv_ali:Labour_Paid{prop:FontColor} = 65793
        ?inv_ali:Labour_Paid{prop:Color} = 15066597
    Elsif ?inv_ali:Labour_Paid{prop:Req} = True
        ?inv_ali:Labour_Paid{prop:FontColor} = 65793
        ?inv_ali:Labour_Paid{prop:Color} = 8454143
    Else ! If ?inv_ali:Labour_Paid{prop:Req} = True
        ?inv_ali:Labour_Paid{prop:FontColor} = 65793
        ?inv_ali:Labour_Paid{prop:Color} = 16777215
    End ! If ?inv_ali:Labour_Paid{prop:Req} = True
    ?inv_ali:Labour_Paid{prop:Trn} = 0
    ?inv_ali:Labour_Paid{prop:FontStyle} = font:Bold
    ?INV:Courier_Paid:Prompt{prop:FontColor} = -1
    ?INV:Courier_Paid:Prompt{prop:Color} = 15066597
    If ?inv_ali:Courier_Paid{prop:ReadOnly} = True
        ?inv_ali:Courier_Paid{prop:FontColor} = 65793
        ?inv_ali:Courier_Paid{prop:Color} = 15066597
    Elsif ?inv_ali:Courier_Paid{prop:Req} = True
        ?inv_ali:Courier_Paid{prop:FontColor} = 65793
        ?inv_ali:Courier_Paid{prop:Color} = 8454143
    Else ! If ?inv_ali:Courier_Paid{prop:Req} = True
        ?inv_ali:Courier_Paid{prop:FontColor} = 65793
        ?inv_ali:Courier_Paid{prop:Color} = 16777215
    End ! If ?inv_ali:Courier_Paid{prop:Req} = True
    ?inv_ali:Courier_Paid{prop:Trn} = 0
    ?inv_ali:Courier_Paid{prop:FontStyle} = font:Bold
    ?INV:Total_Claimed:Prompt{prop:FontColor} = -1
    ?INV:Total_Claimed:Prompt{prop:Color} = 15066597
    If ?inv_ali:Total{prop:ReadOnly} = True
        ?inv_ali:Total{prop:FontColor} = 65793
        ?inv_ali:Total{prop:Color} = 15066597
    Elsif ?inv_ali:Total{prop:Req} = True
        ?inv_ali:Total{prop:FontColor} = 65793
        ?inv_ali:Total{prop:Color} = 8454143
    Else ! If ?inv_ali:Total{prop:Req} = True
        ?inv_ali:Total{prop:FontColor} = 65793
        ?inv_ali:Total{prop:Color} = 16777215
    End ! If ?inv_ali:Total{prop:Req} = True
    ?inv_ali:Total{prop:Trn} = 0
    ?inv_ali:Total{prop:FontStyle} = font:Bold
    ?invoice_total_temp:Prompt{prop:FontColor} = -1
    ?invoice_total_temp:Prompt{prop:Color} = 15066597
    If ?invoice_total_temp{prop:ReadOnly} = True
        ?invoice_total_temp{prop:FontColor} = 65793
        ?invoice_total_temp{prop:Color} = 15066597
    Elsif ?invoice_total_temp{prop:Req} = True
        ?invoice_total_temp{prop:FontColor} = 65793
        ?invoice_total_temp{prop:Color} = 8454143
    Else ! If ?invoice_total_temp{prop:Req} = True
        ?invoice_total_temp{prop:FontColor} = 65793
        ?invoice_total_temp{prop:Color} = 16777215
    End ! If ?invoice_total_temp{prop:Req} = True
    ?invoice_total_temp{prop:Trn} = 0
    ?invoice_total_temp{prop:FontStyle} = font:Bold
    ?Group2{prop:Font,3} = -1
    ?Group2{prop:Color} = 15066597
    ?Group2{prop:Trn} = 0
    ?credit_parts_temp:Prompt{prop:FontColor} = -1
    ?credit_parts_temp:Prompt{prop:Color} = 15066597
    If ?credit_parts_temp{prop:ReadOnly} = True
        ?credit_parts_temp{prop:FontColor} = 65793
        ?credit_parts_temp{prop:Color} = 15066597
    Elsif ?credit_parts_temp{prop:Req} = True
        ?credit_parts_temp{prop:FontColor} = 65793
        ?credit_parts_temp{prop:Color} = 8454143
    Else ! If ?credit_parts_temp{prop:Req} = True
        ?credit_parts_temp{prop:FontColor} = 65793
        ?credit_parts_temp{prop:Color} = 16777215
    End ! If ?credit_parts_temp{prop:Req} = True
    ?credit_parts_temp{prop:Trn} = 0
    ?credit_parts_temp{prop:FontStyle} = font:Bold
    ?Credit_labour_temp:Prompt{prop:FontColor} = -1
    ?Credit_labour_temp:Prompt{prop:Color} = 15066597
    If ?Credit_labour_temp{prop:ReadOnly} = True
        ?Credit_labour_temp{prop:FontColor} = 65793
        ?Credit_labour_temp{prop:Color} = 15066597
    Elsif ?Credit_labour_temp{prop:Req} = True
        ?Credit_labour_temp{prop:FontColor} = 65793
        ?Credit_labour_temp{prop:Color} = 8454143
    Else ! If ?Credit_labour_temp{prop:Req} = True
        ?Credit_labour_temp{prop:FontColor} = 65793
        ?Credit_labour_temp{prop:Color} = 16777215
    End ! If ?Credit_labour_temp{prop:Req} = True
    ?Credit_labour_temp{prop:Trn} = 0
    ?Credit_labour_temp{prop:FontStyle} = font:Bold
    ?Credit_Courier_Temp:Prompt{prop:FontColor} = -1
    ?Credit_Courier_Temp:Prompt{prop:Color} = 15066597
    If ?Credit_Courier_Temp{prop:ReadOnly} = True
        ?Credit_Courier_Temp{prop:FontColor} = 65793
        ?Credit_Courier_Temp{prop:Color} = 15066597
    Elsif ?Credit_Courier_Temp{prop:Req} = True
        ?Credit_Courier_Temp{prop:FontColor} = 65793
        ?Credit_Courier_Temp{prop:Color} = 8454143
    Else ! If ?Credit_Courier_Temp{prop:Req} = True
        ?Credit_Courier_Temp{prop:FontColor} = 65793
        ?Credit_Courier_Temp{prop:Color} = 16777215
    End ! If ?Credit_Courier_Temp{prop:Req} = True
    ?Credit_Courier_Temp{prop:Trn} = 0
    ?Credit_Courier_Temp{prop:FontStyle} = font:Bold
    ?Credit_Total_temp:Prompt{prop:FontColor} = -1
    ?Credit_Total_temp:Prompt{prop:Color} = 15066597
    If ?Credit_Total_temp{prop:ReadOnly} = True
        ?Credit_Total_temp{prop:FontColor} = 65793
        ?Credit_Total_temp{prop:Color} = 15066597
    Elsif ?Credit_Total_temp{prop:Req} = True
        ?Credit_Total_temp{prop:FontColor} = 65793
        ?Credit_Total_temp{prop:Color} = 8454143
    Else ! If ?Credit_Total_temp{prop:Req} = True
        ?Credit_Total_temp{prop:FontColor} = 65793
        ?Credit_Total_temp{prop:Color} = 16777215
    End ! If ?Credit_Total_temp{prop:Req} = True
    ?Credit_Total_temp{prop:Trn} = 0
    ?Credit_Total_temp{prop:FontStyle} = font:Bold
    ?Credit_Total_Inc_Temp:Prompt{prop:FontColor} = -1
    ?Credit_Total_Inc_Temp:Prompt{prop:Color} = 15066597
    If ?Credit_Total_Inc_Temp{prop:ReadOnly} = True
        ?Credit_Total_Inc_Temp{prop:FontColor} = 65793
        ?Credit_Total_Inc_Temp{prop:Color} = 15066597
    Elsif ?Credit_Total_Inc_Temp{prop:Req} = True
        ?Credit_Total_Inc_Temp{prop:FontColor} = 65793
        ?Credit_Total_Inc_Temp{prop:Color} = 8454143
    Else ! If ?Credit_Total_Inc_Temp{prop:Req} = True
        ?Credit_Total_Inc_Temp{prop:FontColor} = 65793
        ?Credit_Total_Inc_Temp{prop:Color} = 16777215
    End ! If ?Credit_Total_Inc_Temp{prop:Req} = True
    ?Credit_Total_Inc_Temp{prop:Trn} = 0
    ?Credit_Total_Inc_Temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Create_Credit_Note',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('ActionMessage',ActionMessage,'Create_Credit_Note',1)
    SolaceViewVars('save_ivptmp_id',save_ivptmp_id,'Create_Credit_Note',1)
    SolaceViewVars('invoice_total_temp',invoice_total_temp,'Create_Credit_Note',1)
    SolaceViewVars('credit_parts_temp',credit_parts_temp,'Create_Credit_Note',1)
    SolaceViewVars('Credit_labour_temp',Credit_labour_temp,'Create_Credit_Note',1)
    SolaceViewVars('Credit_Courier_Temp',Credit_Courier_Temp,'Create_Credit_Note',1)
    SolaceViewVars('Credit_Total_temp',Credit_Total_temp,'Create_Credit_Note',1)
    SolaceViewVars('Credit_Total_Inc_Temp',Credit_Total_Inc_Temp,'Create_Credit_Note',1)
    SolaceViewVars('tmp:parttotal',tmp:parttotal,'Create_Credit_Note',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv_ali:Invoice_Number;  SolaceCtrlName = '?inv_ali:Invoice_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_Part;  SolaceCtrlName = '?Credit_Part';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Retail_Tab;  SolaceCtrlName = '?Retail_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Chargeable_Tab;  SolaceCtrlName = '?Chargeable_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Warranty_Tab;  SolaceCtrlName = '?Warranty_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12;  SolaceCtrlName = '?Prompt12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12:2;  SolaceCtrlName = '?Prompt12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credited_Tab;  SolaceCtrlName = '?Credited_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Parts_Paid:Prompt;  SolaceCtrlName = '?INV:Parts_Paid:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv_ali:Parts_Paid;  SolaceCtrlName = '?inv_ali:Parts_Paid';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Labour_Paid:Prompt;  SolaceCtrlName = '?INV:Labour_Paid:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv_ali:Labour_Paid;  SolaceCtrlName = '?inv_ali:Labour_Paid';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Courier_Paid:Prompt;  SolaceCtrlName = '?INV:Courier_Paid:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv_ali:Courier_Paid;  SolaceCtrlName = '?inv_ali:Courier_Paid';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Total_Claimed:Prompt;  SolaceCtrlName = '?INV:Total_Claimed:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv_ali:Total;  SolaceCtrlName = '?inv_ali:Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?invoice_total_temp:Prompt;  SolaceCtrlName = '?invoice_total_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?invoice_total_temp;  SolaceCtrlName = '?invoice_total_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group2;  SolaceCtrlName = '?Group2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?credit_parts_temp:Prompt;  SolaceCtrlName = '?credit_parts_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?credit_parts_temp;  SolaceCtrlName = '?credit_parts_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_labour_temp:Prompt;  SolaceCtrlName = '?Credit_labour_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_labour_temp;  SolaceCtrlName = '?Credit_labour_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_Courier_Temp:Prompt;  SolaceCtrlName = '?Credit_Courier_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_Courier_Temp;  SolaceCtrlName = '?Credit_Courier_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_Total_temp:Prompt;  SolaceCtrlName = '?Credit_Total_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_Total_temp;  SolaceCtrlName = '?Credit_Total_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_Total_Inc_Temp:Prompt;  SolaceCtrlName = '?Credit_Total_Inc_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Credit_Total_Inc_Temp;  SolaceCtrlName = '?Credit_Total_Inc_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Create_Credit_Note;  SolaceCtrlName = '?Create_Credit_Note';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Create_Credit_Note')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Create_Credit_Note')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:INVOICE.Open
  Relate:INVOICE_ALIAS.Open
  Relate:INVPARTS.Open
  Relate:INVPATMP.Open
  Relate:RETSALES.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  access:invoice_alias.clearkey(inv_ali:invoice_number_key)
  inv_ali:invoice_number = f_invoice_number
  if access:invoice_alias.tryfetch(inv_ali:invoice_number_key)
      Return Level:fatal
  end
  
  Case inv_ali:invoice_type
      Of 'SIN' Orof 'CHA'
          access:jobs.clearkey(job:invoicenumberkey)
          job:invoice_number  = inv_ali:invoice_number
          If access:jobs.tryfetch(job:invoicenumberkey)
              Return Level:Fatal
          End!If access:jobs.tryfetch(job:invoice_number_key)
      OF 'WAR'
          access:jobs.clearkey(job:warinvoicenokey)
          job:invoice_number_warranty = inv_ali:invoice_number
          If access:jobs.tryfetch(job:warinvoicenokey)
              Return Level:Fatal
          End!If access:jobs.tryfetch(job:warranty_invoice_number_key)
  
      Of 'RET'
          access:retsales.clearkey(ret:invoice_number_key)
          ret:invoice_number = inv_ali:invoice_number
          if access:retsales.tryfetch(ret:invoice_number_key)
              Return Level:fatal
          end
          invoice_total_temp = inv_ali:total + (inv_ali:total * inv_ali:vat_rate_retail/100)
  End!Case inv:invoice_type
  
  
  
  BRW5.Init(?List:2,Queue:Browse:1.ViewPosition,BRW5::View:Browse,Queue:Browse:1,Relate:RETSTOCK,SELF)
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:INVPATMP,SELF)
  BRW9.Init(?List:3,Queue:Browse:2.ViewPosition,BRW9::View:Browse,Queue:Browse:2,Relate:PARTS,SELF)
  OPEN(window)
  SELF.Opened=True
  Case inv_ali:invoice_Type
      Of 'CHA' Orof 'SIN'
          Unhide(?chargeable_tab)
          Unhide(?Credit_Part)
      OF 'WAR'
          Unhide(?warranty_tab)
          Hide(?Credited_Tab)
      Of 'RET'
          Unhide(?retail_tab)
          Unhide(?Credit_Part)
  End!Case inv:invoice_Type
  Do RecolourWindow
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW5.Q &= Queue:Browse:1
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,res:Part_Number_Key)
  BRW5.AddRange(res:Ref_Number,ret:Ref_Number)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,res:Part_Number,1,BRW5)
  BRW5.AddField(res:Part_Number,BRW5.Q.res:Part_Number)
  BRW5.AddField(res:Description,BRW5.Q.res:Description)
  BRW5.AddField(res:Quantity,BRW5.Q.res:Quantity)
  BRW5.AddField(res:Credit,BRW5.Q.res:Credit)
  BRW5.AddField(res:Record_Number,BRW5.Q.res:Record_Number)
  BRW5.AddField(res:Ref_Number,BRW5.Q.res:Ref_Number)
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,ivptmp:InvoiceNoKey)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,ivptmp:InvoiceNumber,1,BRW4)
  BIND('tmp:parttotal',tmp:parttotal)
  BRW4.AddField(ivptmp:PartNumber,BRW4.Q.ivptmp:PartNumber)
  BRW4.AddField(ivptmp:Description,BRW4.Q.ivptmp:Description)
  BRW4.AddField(ivptmp:CreditQuantity,BRW4.Q.ivptmp:CreditQuantity)
  BRW4.AddField(tmp:parttotal,BRW4.Q.tmp:parttotal)
  BRW4.AddField(ivptmp:RecordNumber,BRW4.Q.ivptmp:RecordNumber)
  BRW4.AddField(ivptmp:InvoiceNumber,BRW4.Q.ivptmp:InvoiceNumber)
  BRW9.Q &= Queue:Browse:2
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,par:Part_Number_Key)
  BRW9.AddRange(par:Ref_Number,job:Ref_Number)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,par:Part_Number,1,BRW9)
  BRW9.AddField(par:Part_Number,BRW9.Q.par:Part_Number)
  BRW9.AddField(par:Description,BRW9.Q.par:Description)
  BRW9.AddField(par:Quantity,BRW9.Q.par:Quantity)
  BRW9.AddField(par:Record_Number,BRW9.Q.par:Record_Number)
  BRW9.AddField(par:Ref_Number,BRW9.Q.par:Ref_Number)
  BRW4.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
    Relate:INVOICE_ALIAS.Close
    Relate:INVPARTS.Close
    Relate:INVPATMP.Close
    Relate:RETSALES.Close
  END
  Remove(glo:file_name)
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Create_Credit_Note',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Delete_Invoice_Parts
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Credit_Part
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Credit_Part, Accepted)
      Thiswindow.reset(1)
      Case Choice(?Sheet1)
          Of 1 !Retail
              If res:credit = 'YES'
                  Case MessageEx('This part has previously been credited.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              Else!If res:credit = 'YES'
                  Globalrequest = Insertrecord
                  Credit_Part(inv_ali:invoice_type,res:record_number)
              End!If res:credit = 'YES'
      
          Of 2 !Chargeable
              If par:credit = 'YES'
                  Case MessageEx('This part has previously been credited.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
               Else!If par:credit = 'YES'
                  Globalrequest = Insertrecord
                  Credit_Part(inv_ali:invoice_type,par:record_number)
              End!If par:credit = 'YES'
      End!Case Choice(?CurrentTab)
      BRW4.ResetSort(1)
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Credit_Part, Accepted)
    OF ?Create_Credit_Note
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Create_Credit_Note, Accepted)
      Case MessageEx('You are about to create a Credit Note for this Invoice.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              get(invoice,0)
              if access:invoice.primerecord() = Level:Benign
                  inv:invoice_type       = inv_ali:invoice_type
                  inv:job_number         = ''
                  inv:date_created       = Today()
                  inv:account_number     = inv_ali:account_number
                  inv:total              = Credit_Total_temp
                  inv:vat_rate_labour    = inv_ali:vat_rate_labour
                  inv:vat_rate_parts     = inv_ali:vat_rate_parts
                  inv:vat_rate_retail    = inv_ali:vat_rate_retail
                  inv:vat_number         = inv_ali:vat_number
                  inv:invoice_vat_number = inv_ali:invoice_vat_number
                  inv:currency           = inv_ali:currency
                  inv:batch_number       = inv_ali:batch_number
                  inv:manufacturer       = inv_ali:manufacturer
                  inv:claim_reference    = inv_ali:claim_reference
                  inv:total_claimed      = inv_ali:total_claimed
                  inv:courier_paid       = Credit_Courier_Temp
                  inv:labour_paid        = Credit_labour_temp
                  inv:parts_paid         = credit_parts_temp
                  inv:reconciled_date    = inv_ali:Reconciled_date
                  inv:jobs_count         = inv_ali:jobs_count
                  inv:previnvoiceno      = inv_ali:invoice_number
                  inv:invoicecredit      = 'CRE'
                  if access:invoice.insert()
                     access:invoice.cancelautoinc()
                  Else
                      save_ivptmp_id = access:invpatmp.savefile()
                      set(ivptmp:recordnokey)
                      loop
                          if access:invpatmp.next()
                             break
                          end !if
                          get(invparts,0)
                          if access:invparts.primerecord() = Level:Benign
                              ivp:invoicenumber  = inv:invoice_number
                              ivp:retstocknumber = ivptmp:retstocknumber
                              ivp:creditquantity = ivptmp:CreditQuantity
                              ivp:partnumber     = ivptmp:PartNumber
                              ivp:description    = ivptmp:Description
                              ivp:supplier       = ivptmp:Supplier
                              ivp:purchasecost   = ivptmp:PurchaseCost
                              ivp:salecost       = ivptmp:SaleCost
                              ivp:retailcost     = ivptmp:RetailCost
                              ivp:jobnumber      = ivptmp:JobNUmber
                              Case inv:invoice_type
                                  Of 'RET'
                                      access:retstock.clearkey(res:record_number_key)
                                      res:record_number = ivp:retstocknumber
                                      if access:retstock.tryfetch(res:record_number_key) = Level:Benign
                                          res:invoicepart = ivp:recordnumber
                                          res:credit      = 'YES'
                                          access:retstock.update()
                                      end!if access:retstock.tryfetch(res:record_number_key) = Level:Benign
                                  Of 'SIN'
                                  Of 'CHA'
                                  Of 'WAR'
                              End!Case f_type
                              if access:invparts.insert()
                                 access:invparts.cancelautoinc()
                              end
                          end!if access:invparts.primerecord() = Level:Benign
                      end !loop
                      access:invpatmp.restorefile(save_ivptmp_id)
                      glo:select1  = inv:invoice_Number
                      Credit_Note
                      glo:select1  = ''
                  end
              end!if access:invoice.primerecord() = Level:Benign
      
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Create_Credit_Note, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Create_Credit_Note')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
      Credit_Total_temp = credit_parts_temp + Credit_labour_temp + Credit_Courier_Temp
      Credit_Total_Inc_Temp = Credit_Total_temp + (Credit_Total_temp * INV_ali:Vat_Rate_Retail/100)
      Display(?credit_total_temp)
      Display(?credit_Total_inc_Temp)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.res:Part_Number_NormalFG = -1
  SELF.Q.res:Part_Number_NormalBG = -1
  SELF.Q.res:Part_Number_SelectedFG = -1
  SELF.Q.res:Part_Number_SelectedBG = -1
  SELF.Q.res:Description_NormalFG = -1
  SELF.Q.res:Description_NormalBG = -1
  SELF.Q.res:Description_SelectedFG = -1
  SELF.Q.res:Description_SelectedBG = -1
  SELF.Q.res:Quantity_NormalFG = -1
  SELF.Q.res:Quantity_NormalBG = -1
  SELF.Q.res:Quantity_SelectedFG = -1
  SELF.Q.res:Quantity_SelectedBG = -1
  SELF.Q.res:Credit_NormalFG = -1
  SELF.Q.res:Credit_NormalBG = -1
  SELF.Q.res:Credit_SelectedFG = -1
  SELF.Q.res:Credit_SelectedBG = -1
  SELF.Q.res:Record_Number_NormalFG = -1
  SELF.Q.res:Record_Number_NormalBG = -1
  SELF.Q.res:Record_Number_SelectedFG = -1
  SELF.Q.res:Record_Number_SelectedBG = -1


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW4.ResetFromView PROCEDURE

credit_parts_temp:Sum REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:INVPATMP.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    credit_parts_temp:Sum += tmp:parttotal
  END
  credit_parts_temp = credit_parts_temp:Sum
  PARENT.ResetFromView
  Relate:INVPATMP.SetQuickScan(0)
  SETCURSOR()


BRW4.SetQueueRecord PROCEDURE

  CODE
  CASE (inv_ali:Invoice_Type)
  OF 'RET'
    tmp:parttotal = ivptmp:CreditQuantity * ivptmp:RetailCost
  OF 'SIN'
    tmp:parttotal = ivptmp:CreditQuantity * ivptmp:SaleCost
  OF 'CHA'
    tmp:parttotal = ivptmp:CreditQuantity * ivptmp:SaleCost
  ELSE
    tmp:parttotal = ivptmp:CreditQuantity * ivptmp:PurchaseCost
  END
  PARENT.SetQueueRecord
  SELF.Q.tmp:parttotal = tmp:parttotal                !Assign formula result to display queue


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

