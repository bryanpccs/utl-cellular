

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01048.INC'),ONCE        !Local module procedure declarations
                     END


Count_Stock_Export PROCEDURE                          !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
Location_Temp        STRING(30)
savepath             STRING(255)
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
save_sto_ali_id      USHORT,AUTO
total_temp           LONG
location_name_temp   STRING(30),DIM(100)
stock_amount_temp    LONG,DIM(100)
tmp:supplier         STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Location_Temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tmp:supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB5::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
window               WINDOW('All Stock Export'),AT(,,220,88),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,52),USE(?Sheet1),SPREAD
                         TAB('Select Site Location'),USE(?Tab1)
                           PROMPT('Location'),AT(8,20),USE(?Prompt1)
                           COMBO(@s30),AT(84,20,124,10),USE(Location_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Supplier'),AT(8,36),USE(?Prompt1:2)
                           COMBO(@s30),AT(84,36,124,10),USE(tmp:supplier),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                         END
                       END
                       PANEL,AT(4,60,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,64,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Close'),AT(156,64,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?Location_Temp{prop:ReadOnly} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 15066597
    Elsif ?Location_Temp{prop:Req} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 8454143
    Else ! If ?Location_Temp{prop:Req} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 16777215
    End ! If ?Location_Temp{prop:Req} = True
    ?Location_Temp{prop:Trn} = 0
    ?Location_Temp{prop:FontStyle} = font:Bold
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    If ?tmp:supplier{prop:ReadOnly} = True
        ?tmp:supplier{prop:FontColor} = 65793
        ?tmp:supplier{prop:Color} = 15066597
    Elsif ?tmp:supplier{prop:Req} = True
        ?tmp:supplier{prop:FontColor} = 65793
        ?tmp:supplier{prop:Color} = 8454143
    Else ! If ?tmp:supplier{prop:Req} = True
        ?tmp:supplier{prop:FontColor} = 65793
        ?tmp:supplier{prop:Color} = 16777215
    End ! If ?tmp:supplier{prop:Req} = True
    ?tmp:supplier{prop:Trn} = 0
    ?tmp:supplier{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Count_Stock_Export',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Location_Temp',Location_Temp,'Count_Stock_Export',1)
    SolaceViewVars('savepath',savepath,'Count_Stock_Export',1)
    SolaceViewVars('save_loc_id',save_loc_id,'Count_Stock_Export',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Count_Stock_Export',1)
    SolaceViewVars('save_sto_ali_id',save_sto_ali_id,'Count_Stock_Export',1)
    SolaceViewVars('total_temp',total_temp,'Count_Stock_Export',1)
    
      Loop SolaceDim1# = 1 to 100
        SolaceFieldName" = 'location_name_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",location_name_temp[SolaceDim1#],'Count_Stock_Export',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 100
        SolaceFieldName" = 'stock_amount_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",stock_amount_temp[SolaceDim1#],'Count_Stock_Export',1)
      End
    
    
    SolaceViewVars('tmp:supplier',tmp:supplier,'Count_Stock_Export',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_Temp;  SolaceCtrlName = '?Location_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:supplier;  SolaceCtrlName = '?tmp:supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Count_Stock_Export')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Count_Stock_Export')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Relate:STOCK_ALIAS.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FDCB3.Init(Location_Temp,?Location_Temp,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(loc:Location_Key)
  FDCB3.AddField(loc:Location,FDCB3.Q.loc:Location)
  FDCB3.AddField(loc:RecordNumber,FDCB3.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  FDCB5.Init(tmp:supplier,?tmp:supplier,Queue:FileDropCombo:1.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:1,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:1
  FDCB5.AddSortOrder(sup:Company_Name_Key)
  FDCB5.AddField(sup:Company_Name,FDCB5.Q.sup:Company_Name)
  FDCB5.AddField(sup:RecordNumber,FDCB5.Q.sup:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
    Relate:STOCK_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Count_Stock_Export',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      savepath = path()
      set(defaults)
      access:defaults.next()
      If def:exportpath <> ''
          glo:file_name = Clip(def:exportpath) & '\EXPSTOK.CSV'
      Else!If def:exportpath <> ''
          glo:file_name = 'C:\EXPSTOK.CSV'
      End!If def:exportpath <> ''
      
      if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !failed
          setpath(savepath)
      else!if not filedialog
          !found
          setpath(savepath)
      
          Remove(expgen)
      
          access:expgen.open()
          access:expgen.usefile()
      
          Clear(gen:record)
          gen:line1   = 'Stock Level Export'
          access:expgen.insert()
      
          Clear(gen:record)
          gen:line1   = 'Site Location:,' & CLip(location_temp) & ',Supplier:,' & Clip(tmp:Supplier)
          access:expgen.insert()
      
          Clear(gen:record)
          gen:line1   = 'Description,Purchase Cost,Trade Cost,'
      
          location_name_temp[1] = location_temp
      
          gen:line1   = Clip(gen:line1) & CLip(location_name_temp[1]) & ','
      
          count# = 1
          setcursor(cursor:wait)
          save_loc_id = access:location.savefile()
          set(loc:location_key)
          loop
              if access:location.next()
                 break
              end !if
              If loc:location <> location_temp
                  count# += 1
                  location_name_temp[count#] = loc:location
                  gen:line1   = Clip(gen:line1) & Clip(location_name_temp[count#]) & ','
              End!If loc:location <> location_temp
          end !loop
          access:location.restorefile(save_loc_id)
          setcursor()
      
          gen:line1   = Clip(gen:line1) & 'Total Qty'
          access:expgen.insert()
      
          recordspercycle     = 25
          recordsprocessed    = 0
          percentprogress     = 0
          setcursor(cursor:wait)
          open(progresswindow)
          progress:thermometer    = 0
          ?progress:pcttext{prop:text} = '0% Completed'
      
          recordstoprocess    = Records(Stock)
      
          Clear(gen:record)
      
          setcursor(cursor:wait)
          save_sto_id = access:stock.savefile()
          access:stock.clearkey(sto:description_key)
          sto:location    = location_temp
          set(sto:description_key,sto:description_key)
          loop
              if access:stock.next()
                 break
              end !if
              if sto:location    <> location_temp      |
                  then break.  ! end if
      
              Do GetNextRecord2
      
              If tmp:supplier <> ''
                  If sto:supplier <> tmp:supplier
                      Cycle
                  End!If sto:supplier <> tmp:supplier
              End!If tmp:supplier <> ''
      
              stock_amount_temp[1] = sto:quantity_stock
              total_temp  = stock_amount_temp[1]
              Clear(gen:Record)
              gen:line1   = Stripcomma(Clip(sto:description)) & ','
              gen:line1   = Clip(gen:line1) & Format(sto:purchase_Cost,@n14.2) & ','
              gen:line1   = Clip(gen:line1) & Format(sto:sale_cost,@n14.2) & ','
              gen:line1   = Clip(gen:line1) & Clip(stock_amount_temp[1]) & ','
      
              Loop x# = 2 to count#
                  stock_amount_temp[x#] = 0
      
                  save_sto_ali_id = access:stock_alias.savefile()
                  access:stock_alias.clearkey(sto_ali:location_part_description_key)
                  sto_ali:location    = location_name_temp[x#]
                  sto_ali:part_number = sto:part_number
                  sto_ali:description = sto:description
                  set(sto_ali:location_part_description_key,sto_ali:location_part_description_key)
                  loop
                      if access:stock_alias.next()
                         break
                      end !if
                      if sto_ali:location    <> location_name_temp[x#]      |
                      or sto_ali:part_number <> sto:part_number      |
                      or sto_ali:description <> sto:description      |
                          then break.  ! end if
                      stock_amount_temp[x#] += sto_ali:quantity_stock
                      total_temp  += sto_ali:quantity_stock
                  end !loop
                  access:stock_alias.restorefile(save_sto_ali_id)
      
                  gen:line1   = Clip(gen:line1) & Clip(stock_amount_temp[x#]) & ','
              End!Loop x# = 2 to count#
              gen:line1   = Clip(gen:line1) & Clip(total_temp)
              access:expgen.insert()
          end !loop
          access:stock.restorefile(save_sto_id)
          setcursor()
          access:expgen.close()
      
          setcursor()
          close(progresswindow)
      
          Case MessageEx('Export Completed.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      
      end!if not filedialog
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Count_Stock_Export')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


FDCB3.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

