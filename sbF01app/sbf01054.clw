

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01054.INC'),ONCE        !Local module procedure declarations
                     END


Pick_Exchange PROCEDURE (f_stock_type)                !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
all_temp             STRING('AVL')
available_temp       STRING('YES')
status_temp          STRING(40)
Stock_Type_Temp      STRING('AVL {27}')
Model_Number_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Model_Number_Temp
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Stock_Type_Temp
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(EXCHANGE)
                       PROJECT(xch:Ref_Number)
                       PROJECT(xch:Model_Number)
                       PROJECT(xch:Manufacturer)
                       PROJECT(xch:ESN)
                       PROJECT(xch:MSN)
                       PROJECT(xch:Job_Number)
                       PROJECT(xch:Available)
                       PROJECT(xch:Stock_Type)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
xch:Ref_Number         LIKE(xch:Ref_Number)           !List box control field - type derived from field
xch:Model_Number       LIKE(xch:Model_Number)         !List box control field - type derived from field
xch:Manufacturer       LIKE(xch:Manufacturer)         !List box control field - type derived from field
xch:ESN                LIKE(xch:ESN)                  !List box control field - type derived from field
status_temp            LIKE(status_temp)              !List box control field - type derived from local data
xch:MSN                LIKE(xch:MSN)                  !List box control field - type derived from field
xch:Job_Number         LIKE(xch:Job_Number)           !List box control field - type derived from field
xch:Available          LIKE(xch:Available)            !Browse key field - type derived from field
xch:Stock_Type         LIKE(xch:Stock_Type)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB13::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
FDCB12::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
QuickWindow          WINDOW('Browse The Exchange Units File'),AT(,,530,214),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Loan'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,64,432,144),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('34L(2)|M~Unit No~@p<<<<<<<<<<<<<<<<#p@95L(2)|M~Model Number~@s30@96L(2)|M~Manufacturer~@' &|
   's30@65L(2)|M~E.S.N. / I.M.E.I.~@s30@131L(2)|M~Status~@s40@65L(2)|M~M.S.N.~@s30@3' &|
   '2L(2)|M~Job Number~@p<<<<<<<<<<<<<<#pb@'),FROM(Queue:Browse:1)
                       PANEL,AT(4,4,440,24),USE(?Panel1),FILL(COLOR:Gray)
                       COMBO(@s30),AT(80,12,124,10),USE(Stock_Type_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       PROMPT('Stock Type'),AT(8,12),USE(?Prompt1),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                       SHEET,AT(4,32,440,180),USE(?CurrentTab),SPREAD
                         TAB('By E.S.N. / I.M.E.I. '),USE(?Tab:4)
                           ENTRY(@s30),AT(8,48,64,10),USE(xch:ESN),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Exchange Unit Number'),USE(?Tab:2)
                           ENTRY(@p<<<<<<<<#pb),AT(8,48,64,10),USE(xch:Ref_Number),FONT('Tahoma',8,,FONT:bold),UPR,MSG('Unit Number')
                         END
                         TAB('By Model Number'),USE(?Tab:3)
                           COMBO(@s30),AT(8,48,124,10),USE(Model_Number_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           ENTRY(@p<<<<<<<<#pb),AT(136,48,64,10),USE(xch:Ref_Number,,?XCH:Ref_Number:2),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY,MSG('Unit Number')
                         END
                         TAB('By M.S.N.'),USE(?Tab4)
                           ENTRY(@s30),AT(8,48,64,10),USE(xch:MSN),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('&Select'),AT(452,44,76,20),USE(?Select),HIDE,LEFT,ICON('select.ico')
                       BUTTON('Close'),AT(452,192,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort9:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW1::Sort4:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 1
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

BRW1::Sort2:StepClass StepLongClass                   !Conditional Step Manager - Choice(?CurrentTab) = 2
BRW1::Sort1:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 3
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

BRW1::Sort9:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 4
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB13               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB12               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    If ?Stock_Type_Temp{prop:ReadOnly} = True
        ?Stock_Type_Temp{prop:FontColor} = 65793
        ?Stock_Type_Temp{prop:Color} = 15066597
    Elsif ?Stock_Type_Temp{prop:Req} = True
        ?Stock_Type_Temp{prop:FontColor} = 65793
        ?Stock_Type_Temp{prop:Color} = 8454143
    Else ! If ?Stock_Type_Temp{prop:Req} = True
        ?Stock_Type_Temp{prop:FontColor} = 65793
        ?Stock_Type_Temp{prop:Color} = 16777215
    End ! If ?Stock_Type_Temp{prop:Req} = True
    ?Stock_Type_Temp{prop:Trn} = 0
    ?Stock_Type_Temp{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:4{prop:Color} = 15066597
    If ?xch:ESN{prop:ReadOnly} = True
        ?xch:ESN{prop:FontColor} = 65793
        ?xch:ESN{prop:Color} = 15066597
    Elsif ?xch:ESN{prop:Req} = True
        ?xch:ESN{prop:FontColor} = 65793
        ?xch:ESN{prop:Color} = 8454143
    Else ! If ?xch:ESN{prop:Req} = True
        ?xch:ESN{prop:FontColor} = 65793
        ?xch:ESN{prop:Color} = 16777215
    End ! If ?xch:ESN{prop:Req} = True
    ?xch:ESN{prop:Trn} = 0
    ?xch:ESN{prop:FontStyle} = font:Bold
    ?Tab:2{prop:Color} = 15066597
    If ?xch:Ref_Number{prop:ReadOnly} = True
        ?xch:Ref_Number{prop:FontColor} = 65793
        ?xch:Ref_Number{prop:Color} = 15066597
    Elsif ?xch:Ref_Number{prop:Req} = True
        ?xch:Ref_Number{prop:FontColor} = 65793
        ?xch:Ref_Number{prop:Color} = 8454143
    Else ! If ?xch:Ref_Number{prop:Req} = True
        ?xch:Ref_Number{prop:FontColor} = 65793
        ?xch:Ref_Number{prop:Color} = 16777215
    End ! If ?xch:Ref_Number{prop:Req} = True
    ?xch:Ref_Number{prop:Trn} = 0
    ?xch:Ref_Number{prop:FontStyle} = font:Bold
    ?Tab:3{prop:Color} = 15066597
    If ?Model_Number_Temp{prop:ReadOnly} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 15066597
    Elsif ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 8454143
    Else ! If ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 16777215
    End ! If ?Model_Number_Temp{prop:Req} = True
    ?Model_Number_Temp{prop:Trn} = 0
    ?Model_Number_Temp{prop:FontStyle} = font:Bold
    If ?XCH:Ref_Number:2{prop:ReadOnly} = True
        ?XCH:Ref_Number:2{prop:FontColor} = 65793
        ?XCH:Ref_Number:2{prop:Color} = 15066597
    Elsif ?XCH:Ref_Number:2{prop:Req} = True
        ?XCH:Ref_Number:2{prop:FontColor} = 65793
        ?XCH:Ref_Number:2{prop:Color} = 8454143
    Else ! If ?XCH:Ref_Number:2{prop:Req} = True
        ?XCH:Ref_Number:2{prop:FontColor} = 65793
        ?XCH:Ref_Number:2{prop:Color} = 16777215
    End ! If ?XCH:Ref_Number:2{prop:Req} = True
    ?XCH:Ref_Number:2{prop:Trn} = 0
    ?XCH:Ref_Number:2{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    If ?xch:MSN{prop:ReadOnly} = True
        ?xch:MSN{prop:FontColor} = 65793
        ?xch:MSN{prop:Color} = 15066597
    Elsif ?xch:MSN{prop:Req} = True
        ?xch:MSN{prop:FontColor} = 65793
        ?xch:MSN{prop:Color} = 8454143
    Else ! If ?xch:MSN{prop:Req} = True
        ?xch:MSN{prop:FontColor} = 65793
        ?xch:MSN{prop:Color} = 16777215
    End ! If ?xch:MSN{prop:Req} = True
    ?xch:MSN{prop:Trn} = 0
    ?xch:MSN{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Pick_Exchange',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Pick_Exchange',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Pick_Exchange',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Pick_Exchange',1)
    SolaceViewVars('all_temp',all_temp,'Pick_Exchange',1)
    SolaceViewVars('available_temp',available_temp,'Pick_Exchange',1)
    SolaceViewVars('status_temp',status_temp,'Pick_Exchange',1)
    SolaceViewVars('Stock_Type_Temp',Stock_Type_Temp,'Pick_Exchange',1)
    SolaceViewVars('Model_Number_Temp',Model_Number_Temp,'Pick_Exchange',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Stock_Type_Temp;  SolaceCtrlName = '?Stock_Type_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:ESN;  SolaceCtrlName = '?xch:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:Ref_Number;  SolaceCtrlName = '?xch:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_Number_Temp;  SolaceCtrlName = '?Model_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?XCH:Ref_Number:2;  SolaceCtrlName = '?XCH:Ref_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?xch:MSN;  SolaceCtrlName = '?xch:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select;  SolaceCtrlName = '?Select';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Pick_Exchange')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Pick_Exchange')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  If f_stock_Type <> ''
      stock_type_temp = f_stock_type
  End
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EXCHANGE,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort4:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort4:StepClass,xch:ESN_Available_Key)
  BRW1.AddRange(xch:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?XCH:ESN,xch:ESN,1,BRW1)
  BRW1.AddResetField(Stock_Type_Temp)
  BRW1.AddResetField(all_temp)
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,xch:Ref_Available_Key)
  BRW1.AddRange(xch:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?XCH:Ref_Number,xch:Ref_Number,1,BRW1)
  BRW1.AddResetField(Stock_Type_Temp)
  BRW1.AddResetField(all_temp)
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Alpha)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,xch:Model_Available_Key)
  BRW1.AddRange(xch:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?XCH:Ref_Number:2,xch:Model_Number,1,BRW1)
  BRW1.SetFilter('(Upper(xch:model_number) = Upper(model_number_temp))')
  BRW1.AddResetField(all_temp)
  BRW1::Sort9:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Alpha)
  BRW1.AddSortOrder(BRW1::Sort9:StepClass,xch:MSN_Available_Key)
  BRW1.AddRange(xch:Stock_Type,Stock_Type_Temp)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?XCH:MSN,xch:MSN,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,xch:Ref_Available_Key)
  BRW1.AddRange(xch:Available)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?XCH:Ref_Number,xch:Stock_Type,1,BRW1)
  BRW1.AddResetField(all_temp)
  BIND('status_temp',status_temp)
  BIND('Model_Number_Temp',Model_Number_Temp)
  BIND('Stock_Type_Temp',Stock_Type_Temp)
  BRW1.AddField(xch:Ref_Number,BRW1.Q.xch:Ref_Number)
  BRW1.AddField(xch:Model_Number,BRW1.Q.xch:Model_Number)
  BRW1.AddField(xch:Manufacturer,BRW1.Q.xch:Manufacturer)
  BRW1.AddField(xch:ESN,BRW1.Q.xch:ESN)
  BRW1.AddField(status_temp,BRW1.Q.status_temp)
  BRW1.AddField(xch:MSN,BRW1.Q.xch:MSN)
  BRW1.AddField(xch:Job_Number,BRW1.Q.xch:Job_Number)
  BRW1.AddField(xch:Available,BRW1.Q.xch:Available)
  BRW1.AddField(xch:Stock_Type,BRW1.Q.xch:Stock_Type)
  QuickWindow{PROP:MinWidth}=530
  QuickWindow{PROP:MinHeight}=214
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  FDCB13.Init(Model_Number_Temp,?Model_Number_Temp,Queue:FileDropCombo:1.ViewPosition,FDCB13::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB13.Q &= Queue:FileDropCombo:1
  FDCB13.AddSortOrder(mod:Model_Number_Key)
  FDCB13.AddField(mod:Model_Number,FDCB13.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB13.WindowComponent)
  FDCB13.DefaultFill = 0
  FDCB12.Init(Stock_Type_Temp,?Stock_Type_Temp,Queue:FileDropCombo.ViewPosition,FDCB12::View:FileDropCombo,Queue:FileDropCombo,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB12.Q &= Queue:FileDropCombo
  FDCB12.AddSortOrder(stp:Use_Loan_Key)
  FDCB12.AddField(stp:Stock_Type,FDCB12.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB12.WindowComponent)
  FDCB12.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:4{PROP:TEXT} = 'By E.S.N. / I.M.E.I. '
    ?Tab:2{PROP:TEXT} = 'By Exchange Unit Number'
    ?Tab:3{PROP:TEXT} = 'By Model Number'
    ?Tab4{PROP:TEXT} = 'By M.S.N.'
    ?Browse:1{PROP:FORMAT} ='65L(2)|M~E.S.N. / I.M.E.I.~@s30@#4#34L(2)|M~Unit No~@p<<<<<<<<#p@#1#95L(2)|M~Model Number~@s30@#2#96L(2)|M~Manufacturer~@s30@#3#131L(2)|M~Status~@s40@#5#65L(2)|M~M.S.N.~@s30@#6#32L(2)|M~Job Number~@p<<<<<<<#pb@#7#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Pick_Exchange',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('LOAN UNITS - INSERT',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of changerecord
            check_access('LOAN UNITS - CHANGE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of deleterecord
            check_access('LOAN UNITS - DELETE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Stock_Type_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
      BRW1.ResetSort(1)
      ThisWindow.Reset(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Type_Temp, Accepted)
    OF ?Model_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Model_Number_Temp, Accepted)
    OF ?Select
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select, Accepted)
      thiswindow.reset(1)
      error# = 0
      If xch:job_number <> 0
          Case MessageEx('This unit is already attached to this job.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If xch:job_number > 0
      If error# = 0
          glo:select1 = xch:ref_number
          Post(Event:closewindow)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Pick_Exchange')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='65L(2)|M~E.S.N. / I.M.E.I.~@s30@#4#34L(2)|M~Unit No~@p<<<<<<<<#p@#1#95L(2)|M~Model Number~@s30@#2#96L(2)|M~Manufacturer~@s30@#3#131L(2)|M~Status~@s40@#5#65L(2)|M~M.S.N.~@s30@#6#32L(2)|M~Job Number~@p<<<<<<<#pb@#7#'
          ?Tab:4{PROP:TEXT} = 'By E.S.N. / I.M.E.I. '
        OF 2
          ?Browse:1{PROP:FORMAT} ='34L(2)|M~Unit No~@p<<<<<<<<#p@#1#95L(2)|M~Model Number~@s30@#2#96L(2)|M~Manufacturer~@s30@#3#65L(2)|M~E.S.N. / I.M.E.I.~@s30@#4#131L(2)|M~Status~@s40@#5#65L(2)|M~M.S.N.~@s30@#6#32L(2)|M~Job Number~@p<<<<<<<#pb@#7#'
          ?Tab:2{PROP:TEXT} = 'By Exchange Unit Number'
        OF 3
          ?Browse:1{PROP:FORMAT} ='34L(2)|M~Unit No~@p<<<<<<<<#p@#1#95L(2)|M~Model Number~@s30@#2#96L(2)|M~Manufacturer~@s30@#3#65L(2)|M~E.S.N. / I.M.E.I.~@s30@#4#131L(2)|M~Status~@s40@#5#65L(2)|M~M.S.N.~@s30@#6#32L(2)|M~Job Number~@p<<<<<<<#pb@#7#'
          ?Tab:3{PROP:TEXT} = 'By Model Number'
        OF 4
          ?Browse:1{PROP:FORMAT} ='65L(2)|M~M.S.N.~@s30@#6#34L(2)|M~Unit No~@p<<<<<<<<#p@#1#95L(2)|M~Model Number~@s30@#2#96L(2)|M~Manufacturer~@s30@#3#65L(2)|M~E.S.N. / I.M.E.I.~@s30@#4#131L(2)|M~Status~@s40@#5#32L(2)|M~Job Number~@p<<<<<<<#pb@#7#'
          ?Tab4{PROP:TEXT} = 'By M.S.N.'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?xch:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:ESN, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:ESN, Selected)
    OF ?XCH:Ref_Number:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?XCH:Ref_Number:2, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?XCH:Ref_Number:2, Selected)
    OF ?xch:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:MSN, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:MSN, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      glo:select1 = ''
      If thiswindow.request = SelectRecord
          UnHide(?Select)
      End
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      FDCB13.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 4
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  CASE (xch:Available)
  OF 'AVL'
    status_temp = 'AVAILABLE'
  OF 'EXC'
    status_temp = 'EXCHANGED - JOB NO: ' & xch:Job_Number
  OF 'INC'
    status_temp = 'INCOMING TRANSIT - JOB NO: ' & xch:Job_Number
  OF 'FAU'
    status_temp = 'FAULTY'
  OF 'REP'
    status_temp = 'IN REPAIR - JOB NO: ' & xch:Job_Number
  OF 'SUS'
    status_temp = 'SUSPENDED'
  OF 'DES'
    status_temp = 'DESPATCHED - JOB NO: ' & xch:Job_Number
  OF 'QA1'
    status_temp = 'ELECTRONIC QA REQUIRED - JOB NO: ' & xch:Job_Number
  OF 'QA2'
    status_temp = 'MANUAL QA REQUIRED - JOB NO: ' & xch:Job_Number
  OF 'QAF'
    status_temp = 'QA FAILED'
  OF 'RTS'
    status_temp = 'RETURN TO STOCK'
  ELSE
    status_temp = 'IN REPAIR - JOB NO: ' & xch:Job_Number
  END
  PARENT.SetQueueRecord
  SELF.Q.status_temp = status_temp                    !Assign formula result to display queue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1::Sort4:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 4, Init, (BYTE Controls,BYTE Mode))
  xch:available = 'AVL'
  xch:stock_type = Upper(stock_type_temp)
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 4, Init, (BYTE Controls,BYTE Mode))


BRW1::Sort1:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 1, Init, (BYTE Controls,BYTE Mode))
  xch:available = 'AVL'
  xch:stock_type = Upper(stock_type_temp)
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 1, Init, (BYTE Controls,BYTE Mode))


BRW1::Sort9:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 9, Init, (BYTE Controls,BYTE Mode))
  xch:available = 'AVL'
  xch:stock_type  = Upper(stock_type_temp)
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 9, Init, (BYTE Controls,BYTE Mode))


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?Stock_Type_Temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?XCH:Ref_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Model_Number_Temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?XCH:Ref_Number:2, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?XCH:ESN, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?XCH:MSN, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

