

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01036.INC'),ONCE        !Local module procedure declarations
                     END


Fault_Codes_Window PROCEDURE (f_request)              !Generated from procedure template - Window

FilesOpened          BYTE
tmp:Required         BYTE(0)
save_taf_id          USHORT,AUTO
save_tfo_id          USHORT,AUTO
save_mfo_id          USHORT,AUTO
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Fault Codes'),AT(,,231,373),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,338),USE(?Sheet1),SPREAD
                         TAB('Manufacturers Fault Codes'),USE(?Tab1),HIDE
                           PROMPT('Fault Code 1:'),AT(8,20),USE(?JOB:Fault_Code1:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,20,124,10),USE(job:Fault_Code1),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2)
                           BUTTON,AT(212,20,10,10),USE(?Lookup),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(152,20,10,10),USE(?PopCalendar),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 2:'),AT(8,36),USE(?JOB:Fault_Code2:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,36,124,10),USE(job:Fault_Code2),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2)
                           BUTTON,AT(212,36,10,10),USE(?Lookup:2),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar:2),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 3:'),AT(8,52),USE(?JOB:Fault_Code3:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,52,124,10),USE(job:Fault_Code3),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(212,52,10,10),USE(?Lookup:3),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(152,52,10,10),USE(?PopCalendar:3),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 4:'),AT(8,68),USE(?JOB:Fault_Code4:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,68,124,10),USE(job:Fault_Code4),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,68,10,10),USE(?PopCalendar:4),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,68,10,10),USE(?Lookup:4),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 5:'),AT(8,84),USE(?JOB:Fault_Code5:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,84,124,10),USE(job:Fault_Code5),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,84,10,10),USE(?PopCalendar:5),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,84,10,10),USE(?Lookup:5),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 6:'),AT(8,100),USE(?JOB:Fault_Code6:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,100,124,10),USE(job:Fault_Code6),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,100,10,10),USE(?PopCalendar:6),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,100,10,10),USE(?Lookup:6),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 7:'),AT(8,116),USE(?JOB:Fault_Code7:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,116,124,10),USE(job:Fault_Code7),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,116,10,10),USE(?PopCalendar:7),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,116,10,10),USE(?Lookup:7),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 8:'),AT(8,132),USE(?JOB:Fault_Code8:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,132,124,10),USE(job:Fault_Code8),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,132,10,10),USE(?PopCalendar:8),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,132,10,10),USE(?Lookup:8),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 9:'),AT(8,148),USE(?JOB:Fault_Code9:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,148,124,10),USE(job:Fault_Code9),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,148,10,10),USE(?PopCalendar:9),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,148,10,10),USE(?Lookup:9),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 10:'),AT(8,164),USE(?JOB:Fault_Code10:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,164,124,10),USE(job:Fault_Code10),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,164,10,10),USE(?PopCalendar:10),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,164,10,10),USE(?Lookup:10),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 11:'),AT(8,180),USE(?JOB:Fault_Code11:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,180,124,10),USE(job:Fault_Code11),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,180,10,10),USE(?PopCalendar:11),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,180,10,10),USE(?Lookup:11),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 12:'),AT(8,196),USE(?JOB:Fault_Code12:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,196,124,10),USE(job:Fault_Code12),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,196,10,10),USE(?PopCalendar:12),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,196,10,10),USE(?Lookup:12),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 13'),AT(8,212),USE(?jobe:FaultCode13:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,212,124,10),USE(jobe:FaultCode13),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 13'),TIP('Fault Code 13'),UPR
                           BUTTON,AT(152,212,10,10),USE(?PopCalendar:25),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 14'),AT(8,228),USE(?jobe:FaultCode14:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,228,124,10),USE(jobe:FaultCode14),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 14'),TIP('Fault Code 14'),UPR
                           BUTTON,AT(152,229,10,10),USE(?PopCalendar:26),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,212,10,10),USE(?Lookup:13),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 15'),AT(8,244),USE(?jobe:FaultCode15:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,244,124,10),USE(jobe:FaultCode15),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 15'),TIP('Fault Code 15'),UPR
                           BUTTON,AT(152,244,10,10),USE(?PopCalendar:27),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,228,10,10),USE(?Lookup:14),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 16'),AT(8,260),USE(?jobe:FaultCode16:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,260,124,10),USE(jobe:FaultCode16),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 16'),TIP('Fault Code 16'),UPR
                           BUTTON,AT(152,260,10,10),USE(?PopCalendar:28),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,244,10,10),USE(?Lookup:15),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 17'),AT(8,276),USE(?jobe:FaultCode17:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,276,124,10),USE(jobe:FaultCode17),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 17'),TIP('Fault Code 17'),UPR
                           BUTTON,AT(152,276,10,10),USE(?PopCalendar:29),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,260,10,10),USE(?Lookup:16),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 18'),AT(8,292),USE(?jobe:FaultCode18:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,292,124,10),USE(jobe:FaultCode18),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 187'),TIP('Fault Code 187'),UPR
                           BUTTON,AT(152,292,10,10),USE(?PopCalendar:30),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,276,10,10),USE(?Lookup:17),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 19'),AT(8,308),USE(?jobe:FaultCode19:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,308,124,10),USE(jobe:FaultCode19),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 19'),TIP('Fault Code 19'),UPR
                           BUTTON,AT(152,308,10,10),USE(?PopCalendar:31),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,292,10,10),USE(?Lookup:18),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 20'),AT(8,324),USE(?jobe:FaultCode20:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,324,124,10),USE(jobe:FaultCode20),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 20'),TIP('Fault Code 20'),UPR
                           BUTTON,AT(152,324,10,10),USE(?PopCalendar:32),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,324,10,10),USE(?Lookup:20),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,308,10,10),USE(?Lookup:19),SKIP,HIDE,ICON('list3.ico')
                         END
                         TAB('Trade Fault Codes'),USE(?Tab2),HIDE
                           PROMPT('Fault Code 1'),AT(8,20),USE(?JOBE:TraFaultCode1:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(jobe:TraFaultCode1),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 1'),TIP('Fault Code 1'),UPR
                           BUTTON,AT(152,20,10,10),USE(?PopCalendar:13),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,20,10,10),USE(?tralookup:1),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 2'),AT(8,36),USE(?JOBE:TraFaultCode2:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(jobe:TraFaultCode2),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 2'),TIP('Fault Code 2'),UPR
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar:14),HIDE,ICON('CALENDA2.ICO')
                           PROMPT('Fault Code 3'),AT(8,52),USE(?JOBE:TraFaultCode3:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(jobe:TraFaultCode3),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 3'),TIP('Fault Code 3'),UPR
                           BUTTON,AT(152,52,10,10),USE(?PopCalendar:15),HIDE,ICON('CALENDA2.ICO')
                           PROMPT('Fault Code 4'),AT(8,68),USE(?JOBE:TraFaultCode4:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(jobe:TraFaultCode4),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 4'),TIP('Fault Code 4'),UPR
                           BUTTON,AT(152,68,10,10),USE(?PopCalendar:16),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,68,10,10),USE(?tralookup:4),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 5'),AT(8,84),USE(?JOBE:TraFaultCode5:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,124,10),USE(jobe:TraFaultCode5),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 5'),TIP('Fault Code 5'),UPR
                           BUTTON,AT(152,84,10,10),USE(?PopCalendar:17),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,84,10,10),USE(?tralookup:5),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 6'),AT(8,100),USE(?JOBE:TraFaultCode6:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,100,124,10),USE(jobe:TraFaultCode6),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 6'),TIP('Fault Code 6'),UPR
                           BUTTON,AT(152,100,10,10),USE(?PopCalendar:18),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,36,10,10),USE(?tralookup:2),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,52,10,10),USE(?tralookup:3),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 7'),AT(8,116),USE(?JOBE:TraFaultCode7:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,116,124,10),USE(jobe:TraFaultCode7),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 7'),TIP('Fault Code 7'),UPR
                           BUTTON,AT(152,116,10,10),USE(?PopCalendar:19),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,116,10,10),USE(?tralookup:7),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 8'),AT(8,132),USE(?JOBE:TraFaultCode8:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,132,124,10),USE(jobe:TraFaultCode8),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 8'),TIP('Fault Code 8'),UPR
                           BUTTON,AT(152,132,10,10),USE(?PopCalendar:20),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,132,10,10),USE(?tralookup:8),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 9'),AT(8,148),USE(?JOBE:TraFaultCode9:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,148,124,10),USE(jobe:TraFaultCode9),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 9'),TIP('Fault Code 9'),UPR
                           BUTTON,AT(152,148,10,10),USE(?PopCalendar:21),HIDE,ICON('CALENDA2.ICO')
                           PROMPT('Fault Code 10'),AT(8,164),USE(?JOBE:TraFaultCode10:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,164,124,10),USE(jobe:TraFaultCode10),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 10'),TIP('Fault Code 10'),UPR
                           BUTTON,AT(152,164,10,10),USE(?PopCalendar:22),HIDE,ICON('CALENDA2.ICO')
                           PROMPT('Fault Code 11'),AT(8,180),USE(?JOBE:TraFaultCode11:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,180,124,10),USE(jobe:TraFaultCode11),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 11'),TIP('Fault Code 11'),UPR
                           BUTTON,AT(152,180,10,10),USE(?PopCalendar:23),HIDE,ICON('CALENDA2.ICO')
                           PROMPT('Fault Code 12'),AT(8,196),USE(?JOBE:TraFaultCode12:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,196,124,10),USE(jobe:TraFaultCode12),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 12'),TIP('Fault Code 12'),UPR
                           BUTTON,AT(152,196,10,10),USE(?PopCalendar:24),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,148,10,10),USE(?tralookup:9),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,180,10,10),USE(?tralookup:11),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,100,10,10),USE(?tralookup:6),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,164,10,10),USE(?tralookup:10),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,196,10,10),USE(?tralookup:12),SKIP,HIDE,ICON('list3.ico')
                         END
                         TAB('Fault Codes'),USE(?Tab3),HIDE
                           PROMPT('NO Fault Codes are required for this job.'),AT(45,76,140,60),USE(?Prompt25),FONT(,22,COLOR:Navy,)
                         END
                       END
                       PANEL,AT(4,346,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(168,350,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_maf_id     ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?JOB:Fault_Code1:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code1:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code1{prop:ReadOnly} = True
        ?job:Fault_Code1{prop:FontColor} = 65793
        ?job:Fault_Code1{prop:Color} = 15066597
    Elsif ?job:Fault_Code1{prop:Req} = True
        ?job:Fault_Code1{prop:FontColor} = 65793
        ?job:Fault_Code1{prop:Color} = 8454143
    Else ! If ?job:Fault_Code1{prop:Req} = True
        ?job:Fault_Code1{prop:FontColor} = 65793
        ?job:Fault_Code1{prop:Color} = 16777215
    End ! If ?job:Fault_Code1{prop:Req} = True
    ?job:Fault_Code1{prop:Trn} = 0
    ?job:Fault_Code1{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code2:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code2:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code2{prop:ReadOnly} = True
        ?job:Fault_Code2{prop:FontColor} = 65793
        ?job:Fault_Code2{prop:Color} = 15066597
    Elsif ?job:Fault_Code2{prop:Req} = True
        ?job:Fault_Code2{prop:FontColor} = 65793
        ?job:Fault_Code2{prop:Color} = 8454143
    Else ! If ?job:Fault_Code2{prop:Req} = True
        ?job:Fault_Code2{prop:FontColor} = 65793
        ?job:Fault_Code2{prop:Color} = 16777215
    End ! If ?job:Fault_Code2{prop:Req} = True
    ?job:Fault_Code2{prop:Trn} = 0
    ?job:Fault_Code2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code3:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code3:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code3{prop:ReadOnly} = True
        ?job:Fault_Code3{prop:FontColor} = 65793
        ?job:Fault_Code3{prop:Color} = 15066597
    Elsif ?job:Fault_Code3{prop:Req} = True
        ?job:Fault_Code3{prop:FontColor} = 65793
        ?job:Fault_Code3{prop:Color} = 8454143
    Else ! If ?job:Fault_Code3{prop:Req} = True
        ?job:Fault_Code3{prop:FontColor} = 65793
        ?job:Fault_Code3{prop:Color} = 16777215
    End ! If ?job:Fault_Code3{prop:Req} = True
    ?job:Fault_Code3{prop:Trn} = 0
    ?job:Fault_Code3{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code4:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code4:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code4{prop:ReadOnly} = True
        ?job:Fault_Code4{prop:FontColor} = 65793
        ?job:Fault_Code4{prop:Color} = 15066597
    Elsif ?job:Fault_Code4{prop:Req} = True
        ?job:Fault_Code4{prop:FontColor} = 65793
        ?job:Fault_Code4{prop:Color} = 8454143
    Else ! If ?job:Fault_Code4{prop:Req} = True
        ?job:Fault_Code4{prop:FontColor} = 65793
        ?job:Fault_Code4{prop:Color} = 16777215
    End ! If ?job:Fault_Code4{prop:Req} = True
    ?job:Fault_Code4{prop:Trn} = 0
    ?job:Fault_Code4{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code5:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code5:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code5{prop:ReadOnly} = True
        ?job:Fault_Code5{prop:FontColor} = 65793
        ?job:Fault_Code5{prop:Color} = 15066597
    Elsif ?job:Fault_Code5{prop:Req} = True
        ?job:Fault_Code5{prop:FontColor} = 65793
        ?job:Fault_Code5{prop:Color} = 8454143
    Else ! If ?job:Fault_Code5{prop:Req} = True
        ?job:Fault_Code5{prop:FontColor} = 65793
        ?job:Fault_Code5{prop:Color} = 16777215
    End ! If ?job:Fault_Code5{prop:Req} = True
    ?job:Fault_Code5{prop:Trn} = 0
    ?job:Fault_Code5{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code6:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code6:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code6{prop:ReadOnly} = True
        ?job:Fault_Code6{prop:FontColor} = 65793
        ?job:Fault_Code6{prop:Color} = 15066597
    Elsif ?job:Fault_Code6{prop:Req} = True
        ?job:Fault_Code6{prop:FontColor} = 65793
        ?job:Fault_Code6{prop:Color} = 8454143
    Else ! If ?job:Fault_Code6{prop:Req} = True
        ?job:Fault_Code6{prop:FontColor} = 65793
        ?job:Fault_Code6{prop:Color} = 16777215
    End ! If ?job:Fault_Code6{prop:Req} = True
    ?job:Fault_Code6{prop:Trn} = 0
    ?job:Fault_Code6{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code7:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code7:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code7{prop:ReadOnly} = True
        ?job:Fault_Code7{prop:FontColor} = 65793
        ?job:Fault_Code7{prop:Color} = 15066597
    Elsif ?job:Fault_Code7{prop:Req} = True
        ?job:Fault_Code7{prop:FontColor} = 65793
        ?job:Fault_Code7{prop:Color} = 8454143
    Else ! If ?job:Fault_Code7{prop:Req} = True
        ?job:Fault_Code7{prop:FontColor} = 65793
        ?job:Fault_Code7{prop:Color} = 16777215
    End ! If ?job:Fault_Code7{prop:Req} = True
    ?job:Fault_Code7{prop:Trn} = 0
    ?job:Fault_Code7{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code8:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code8:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code8{prop:ReadOnly} = True
        ?job:Fault_Code8{prop:FontColor} = 65793
        ?job:Fault_Code8{prop:Color} = 15066597
    Elsif ?job:Fault_Code8{prop:Req} = True
        ?job:Fault_Code8{prop:FontColor} = 65793
        ?job:Fault_Code8{prop:Color} = 8454143
    Else ! If ?job:Fault_Code8{prop:Req} = True
        ?job:Fault_Code8{prop:FontColor} = 65793
        ?job:Fault_Code8{prop:Color} = 16777215
    End ! If ?job:Fault_Code8{prop:Req} = True
    ?job:Fault_Code8{prop:Trn} = 0
    ?job:Fault_Code8{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code9:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code9:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code9{prop:ReadOnly} = True
        ?job:Fault_Code9{prop:FontColor} = 65793
        ?job:Fault_Code9{prop:Color} = 15066597
    Elsif ?job:Fault_Code9{prop:Req} = True
        ?job:Fault_Code9{prop:FontColor} = 65793
        ?job:Fault_Code9{prop:Color} = 8454143
    Else ! If ?job:Fault_Code9{prop:Req} = True
        ?job:Fault_Code9{prop:FontColor} = 65793
        ?job:Fault_Code9{prop:Color} = 16777215
    End ! If ?job:Fault_Code9{prop:Req} = True
    ?job:Fault_Code9{prop:Trn} = 0
    ?job:Fault_Code9{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code10:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code10:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code10{prop:ReadOnly} = True
        ?job:Fault_Code10{prop:FontColor} = 65793
        ?job:Fault_Code10{prop:Color} = 15066597
    Elsif ?job:Fault_Code10{prop:Req} = True
        ?job:Fault_Code10{prop:FontColor} = 65793
        ?job:Fault_Code10{prop:Color} = 8454143
    Else ! If ?job:Fault_Code10{prop:Req} = True
        ?job:Fault_Code10{prop:FontColor} = 65793
        ?job:Fault_Code10{prop:Color} = 16777215
    End ! If ?job:Fault_Code10{prop:Req} = True
    ?job:Fault_Code10{prop:Trn} = 0
    ?job:Fault_Code10{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code11:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code11:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code11{prop:ReadOnly} = True
        ?job:Fault_Code11{prop:FontColor} = 65793
        ?job:Fault_Code11{prop:Color} = 15066597
    Elsif ?job:Fault_Code11{prop:Req} = True
        ?job:Fault_Code11{prop:FontColor} = 65793
        ?job:Fault_Code11{prop:Color} = 8454143
    Else ! If ?job:Fault_Code11{prop:Req} = True
        ?job:Fault_Code11{prop:FontColor} = 65793
        ?job:Fault_Code11{prop:Color} = 16777215
    End ! If ?job:Fault_Code11{prop:Req} = True
    ?job:Fault_Code11{prop:Trn} = 0
    ?job:Fault_Code11{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code12:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code12:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code12{prop:ReadOnly} = True
        ?job:Fault_Code12{prop:FontColor} = 65793
        ?job:Fault_Code12{prop:Color} = 15066597
    Elsif ?job:Fault_Code12{prop:Req} = True
        ?job:Fault_Code12{prop:FontColor} = 65793
        ?job:Fault_Code12{prop:Color} = 8454143
    Else ! If ?job:Fault_Code12{prop:Req} = True
        ?job:Fault_Code12{prop:FontColor} = 65793
        ?job:Fault_Code12{prop:Color} = 16777215
    End ! If ?job:Fault_Code12{prop:Req} = True
    ?job:Fault_Code12{prop:Trn} = 0
    ?job:Fault_Code12{prop:FontStyle} = font:Bold
    ?jobe:FaultCode13:Prompt{prop:FontColor} = -1
    ?jobe:FaultCode13:Prompt{prop:Color} = 15066597
    If ?jobe:FaultCode13{prop:ReadOnly} = True
        ?jobe:FaultCode13{prop:FontColor} = 65793
        ?jobe:FaultCode13{prop:Color} = 15066597
    Elsif ?jobe:FaultCode13{prop:Req} = True
        ?jobe:FaultCode13{prop:FontColor} = 65793
        ?jobe:FaultCode13{prop:Color} = 8454143
    Else ! If ?jobe:FaultCode13{prop:Req} = True
        ?jobe:FaultCode13{prop:FontColor} = 65793
        ?jobe:FaultCode13{prop:Color} = 16777215
    End ! If ?jobe:FaultCode13{prop:Req} = True
    ?jobe:FaultCode13{prop:Trn} = 0
    ?jobe:FaultCode13{prop:FontStyle} = font:Bold
    ?jobe:FaultCode14:Prompt{prop:FontColor} = -1
    ?jobe:FaultCode14:Prompt{prop:Color} = 15066597
    If ?jobe:FaultCode14{prop:ReadOnly} = True
        ?jobe:FaultCode14{prop:FontColor} = 65793
        ?jobe:FaultCode14{prop:Color} = 15066597
    Elsif ?jobe:FaultCode14{prop:Req} = True
        ?jobe:FaultCode14{prop:FontColor} = 65793
        ?jobe:FaultCode14{prop:Color} = 8454143
    Else ! If ?jobe:FaultCode14{prop:Req} = True
        ?jobe:FaultCode14{prop:FontColor} = 65793
        ?jobe:FaultCode14{prop:Color} = 16777215
    End ! If ?jobe:FaultCode14{prop:Req} = True
    ?jobe:FaultCode14{prop:Trn} = 0
    ?jobe:FaultCode14{prop:FontStyle} = font:Bold
    ?jobe:FaultCode15:Prompt{prop:FontColor} = -1
    ?jobe:FaultCode15:Prompt{prop:Color} = 15066597
    If ?jobe:FaultCode15{prop:ReadOnly} = True
        ?jobe:FaultCode15{prop:FontColor} = 65793
        ?jobe:FaultCode15{prop:Color} = 15066597
    Elsif ?jobe:FaultCode15{prop:Req} = True
        ?jobe:FaultCode15{prop:FontColor} = 65793
        ?jobe:FaultCode15{prop:Color} = 8454143
    Else ! If ?jobe:FaultCode15{prop:Req} = True
        ?jobe:FaultCode15{prop:FontColor} = 65793
        ?jobe:FaultCode15{prop:Color} = 16777215
    End ! If ?jobe:FaultCode15{prop:Req} = True
    ?jobe:FaultCode15{prop:Trn} = 0
    ?jobe:FaultCode15{prop:FontStyle} = font:Bold
    ?jobe:FaultCode16:Prompt{prop:FontColor} = -1
    ?jobe:FaultCode16:Prompt{prop:Color} = 15066597
    If ?jobe:FaultCode16{prop:ReadOnly} = True
        ?jobe:FaultCode16{prop:FontColor} = 65793
        ?jobe:FaultCode16{prop:Color} = 15066597
    Elsif ?jobe:FaultCode16{prop:Req} = True
        ?jobe:FaultCode16{prop:FontColor} = 65793
        ?jobe:FaultCode16{prop:Color} = 8454143
    Else ! If ?jobe:FaultCode16{prop:Req} = True
        ?jobe:FaultCode16{prop:FontColor} = 65793
        ?jobe:FaultCode16{prop:Color} = 16777215
    End ! If ?jobe:FaultCode16{prop:Req} = True
    ?jobe:FaultCode16{prop:Trn} = 0
    ?jobe:FaultCode16{prop:FontStyle} = font:Bold
    ?jobe:FaultCode17:Prompt{prop:FontColor} = -1
    ?jobe:FaultCode17:Prompt{prop:Color} = 15066597
    If ?jobe:FaultCode17{prop:ReadOnly} = True
        ?jobe:FaultCode17{prop:FontColor} = 65793
        ?jobe:FaultCode17{prop:Color} = 15066597
    Elsif ?jobe:FaultCode17{prop:Req} = True
        ?jobe:FaultCode17{prop:FontColor} = 65793
        ?jobe:FaultCode17{prop:Color} = 8454143
    Else ! If ?jobe:FaultCode17{prop:Req} = True
        ?jobe:FaultCode17{prop:FontColor} = 65793
        ?jobe:FaultCode17{prop:Color} = 16777215
    End ! If ?jobe:FaultCode17{prop:Req} = True
    ?jobe:FaultCode17{prop:Trn} = 0
    ?jobe:FaultCode17{prop:FontStyle} = font:Bold
    ?jobe:FaultCode18:Prompt{prop:FontColor} = -1
    ?jobe:FaultCode18:Prompt{prop:Color} = 15066597
    If ?jobe:FaultCode18{prop:ReadOnly} = True
        ?jobe:FaultCode18{prop:FontColor} = 65793
        ?jobe:FaultCode18{prop:Color} = 15066597
    Elsif ?jobe:FaultCode18{prop:Req} = True
        ?jobe:FaultCode18{prop:FontColor} = 65793
        ?jobe:FaultCode18{prop:Color} = 8454143
    Else ! If ?jobe:FaultCode18{prop:Req} = True
        ?jobe:FaultCode18{prop:FontColor} = 65793
        ?jobe:FaultCode18{prop:Color} = 16777215
    End ! If ?jobe:FaultCode18{prop:Req} = True
    ?jobe:FaultCode18{prop:Trn} = 0
    ?jobe:FaultCode18{prop:FontStyle} = font:Bold
    ?jobe:FaultCode19:Prompt{prop:FontColor} = -1
    ?jobe:FaultCode19:Prompt{prop:Color} = 15066597
    If ?jobe:FaultCode19{prop:ReadOnly} = True
        ?jobe:FaultCode19{prop:FontColor} = 65793
        ?jobe:FaultCode19{prop:Color} = 15066597
    Elsif ?jobe:FaultCode19{prop:Req} = True
        ?jobe:FaultCode19{prop:FontColor} = 65793
        ?jobe:FaultCode19{prop:Color} = 8454143
    Else ! If ?jobe:FaultCode19{prop:Req} = True
        ?jobe:FaultCode19{prop:FontColor} = 65793
        ?jobe:FaultCode19{prop:Color} = 16777215
    End ! If ?jobe:FaultCode19{prop:Req} = True
    ?jobe:FaultCode19{prop:Trn} = 0
    ?jobe:FaultCode19{prop:FontStyle} = font:Bold
    ?jobe:FaultCode20:Prompt{prop:FontColor} = -1
    ?jobe:FaultCode20:Prompt{prop:Color} = 15066597
    If ?jobe:FaultCode20{prop:ReadOnly} = True
        ?jobe:FaultCode20{prop:FontColor} = 65793
        ?jobe:FaultCode20{prop:Color} = 15066597
    Elsif ?jobe:FaultCode20{prop:Req} = True
        ?jobe:FaultCode20{prop:FontColor} = 65793
        ?jobe:FaultCode20{prop:Color} = 8454143
    Else ! If ?jobe:FaultCode20{prop:Req} = True
        ?jobe:FaultCode20{prop:FontColor} = 65793
        ?jobe:FaultCode20{prop:Color} = 16777215
    End ! If ?jobe:FaultCode20{prop:Req} = True
    ?jobe:FaultCode20{prop:Trn} = 0
    ?jobe:FaultCode20{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?JOBE:TraFaultCode1:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode1:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode1{prop:ReadOnly} = True
        ?jobe:TraFaultCode1{prop:FontColor} = 65793
        ?jobe:TraFaultCode1{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode1{prop:Req} = True
        ?jobe:TraFaultCode1{prop:FontColor} = 65793
        ?jobe:TraFaultCode1{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode1{prop:Req} = True
        ?jobe:TraFaultCode1{prop:FontColor} = 65793
        ?jobe:TraFaultCode1{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode1{prop:Req} = True
    ?jobe:TraFaultCode1{prop:Trn} = 0
    ?jobe:TraFaultCode1{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode2:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode2:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode2{prop:ReadOnly} = True
        ?jobe:TraFaultCode2{prop:FontColor} = 65793
        ?jobe:TraFaultCode2{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode2{prop:Req} = True
        ?jobe:TraFaultCode2{prop:FontColor} = 65793
        ?jobe:TraFaultCode2{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode2{prop:Req} = True
        ?jobe:TraFaultCode2{prop:FontColor} = 65793
        ?jobe:TraFaultCode2{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode2{prop:Req} = True
    ?jobe:TraFaultCode2{prop:Trn} = 0
    ?jobe:TraFaultCode2{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode3:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode3:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode3{prop:ReadOnly} = True
        ?jobe:TraFaultCode3{prop:FontColor} = 65793
        ?jobe:TraFaultCode3{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode3{prop:Req} = True
        ?jobe:TraFaultCode3{prop:FontColor} = 65793
        ?jobe:TraFaultCode3{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode3{prop:Req} = True
        ?jobe:TraFaultCode3{prop:FontColor} = 65793
        ?jobe:TraFaultCode3{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode3{prop:Req} = True
    ?jobe:TraFaultCode3{prop:Trn} = 0
    ?jobe:TraFaultCode3{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode4:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode4:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode4{prop:ReadOnly} = True
        ?jobe:TraFaultCode4{prop:FontColor} = 65793
        ?jobe:TraFaultCode4{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode4{prop:Req} = True
        ?jobe:TraFaultCode4{prop:FontColor} = 65793
        ?jobe:TraFaultCode4{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode4{prop:Req} = True
        ?jobe:TraFaultCode4{prop:FontColor} = 65793
        ?jobe:TraFaultCode4{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode4{prop:Req} = True
    ?jobe:TraFaultCode4{prop:Trn} = 0
    ?jobe:TraFaultCode4{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode5:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode5:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode5{prop:ReadOnly} = True
        ?jobe:TraFaultCode5{prop:FontColor} = 65793
        ?jobe:TraFaultCode5{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode5{prop:Req} = True
        ?jobe:TraFaultCode5{prop:FontColor} = 65793
        ?jobe:TraFaultCode5{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode5{prop:Req} = True
        ?jobe:TraFaultCode5{prop:FontColor} = 65793
        ?jobe:TraFaultCode5{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode5{prop:Req} = True
    ?jobe:TraFaultCode5{prop:Trn} = 0
    ?jobe:TraFaultCode5{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode6:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode6:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode6{prop:ReadOnly} = True
        ?jobe:TraFaultCode6{prop:FontColor} = 65793
        ?jobe:TraFaultCode6{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode6{prop:Req} = True
        ?jobe:TraFaultCode6{prop:FontColor} = 65793
        ?jobe:TraFaultCode6{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode6{prop:Req} = True
        ?jobe:TraFaultCode6{prop:FontColor} = 65793
        ?jobe:TraFaultCode6{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode6{prop:Req} = True
    ?jobe:TraFaultCode6{prop:Trn} = 0
    ?jobe:TraFaultCode6{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode7:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode7:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode7{prop:ReadOnly} = True
        ?jobe:TraFaultCode7{prop:FontColor} = 65793
        ?jobe:TraFaultCode7{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode7{prop:Req} = True
        ?jobe:TraFaultCode7{prop:FontColor} = 65793
        ?jobe:TraFaultCode7{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode7{prop:Req} = True
        ?jobe:TraFaultCode7{prop:FontColor} = 65793
        ?jobe:TraFaultCode7{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode7{prop:Req} = True
    ?jobe:TraFaultCode7{prop:Trn} = 0
    ?jobe:TraFaultCode7{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode8:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode8:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode8{prop:ReadOnly} = True
        ?jobe:TraFaultCode8{prop:FontColor} = 65793
        ?jobe:TraFaultCode8{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode8{prop:Req} = True
        ?jobe:TraFaultCode8{prop:FontColor} = 65793
        ?jobe:TraFaultCode8{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode8{prop:Req} = True
        ?jobe:TraFaultCode8{prop:FontColor} = 65793
        ?jobe:TraFaultCode8{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode8{prop:Req} = True
    ?jobe:TraFaultCode8{prop:Trn} = 0
    ?jobe:TraFaultCode8{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode9:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode9:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode9{prop:ReadOnly} = True
        ?jobe:TraFaultCode9{prop:FontColor} = 65793
        ?jobe:TraFaultCode9{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode9{prop:Req} = True
        ?jobe:TraFaultCode9{prop:FontColor} = 65793
        ?jobe:TraFaultCode9{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode9{prop:Req} = True
        ?jobe:TraFaultCode9{prop:FontColor} = 65793
        ?jobe:TraFaultCode9{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode9{prop:Req} = True
    ?jobe:TraFaultCode9{prop:Trn} = 0
    ?jobe:TraFaultCode9{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode10:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode10:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode10{prop:ReadOnly} = True
        ?jobe:TraFaultCode10{prop:FontColor} = 65793
        ?jobe:TraFaultCode10{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode10{prop:Req} = True
        ?jobe:TraFaultCode10{prop:FontColor} = 65793
        ?jobe:TraFaultCode10{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode10{prop:Req} = True
        ?jobe:TraFaultCode10{prop:FontColor} = 65793
        ?jobe:TraFaultCode10{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode10{prop:Req} = True
    ?jobe:TraFaultCode10{prop:Trn} = 0
    ?jobe:TraFaultCode10{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode11:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode11:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode11{prop:ReadOnly} = True
        ?jobe:TraFaultCode11{prop:FontColor} = 65793
        ?jobe:TraFaultCode11{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode11{prop:Req} = True
        ?jobe:TraFaultCode11{prop:FontColor} = 65793
        ?jobe:TraFaultCode11{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode11{prop:Req} = True
        ?jobe:TraFaultCode11{prop:FontColor} = 65793
        ?jobe:TraFaultCode11{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode11{prop:Req} = True
    ?jobe:TraFaultCode11{prop:Trn} = 0
    ?jobe:TraFaultCode11{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode12:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode12:Prompt{prop:Color} = 15066597
    If ?jobe:TraFaultCode12{prop:ReadOnly} = True
        ?jobe:TraFaultCode12{prop:FontColor} = 65793
        ?jobe:TraFaultCode12{prop:Color} = 15066597
    Elsif ?jobe:TraFaultCode12{prop:Req} = True
        ?jobe:TraFaultCode12{prop:FontColor} = 65793
        ?jobe:TraFaultCode12{prop:Color} = 8454143
    Else ! If ?jobe:TraFaultCode12{prop:Req} = True
        ?jobe:TraFaultCode12{prop:FontColor} = 65793
        ?jobe:TraFaultCode12{prop:Color} = 16777215
    End ! If ?jobe:TraFaultCode12{prop:Req} = True
    ?jobe:TraFaultCode12{prop:Trn} = 0
    ?jobe:TraFaultCode12{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Fault_Coding        Routine   ! Manufacturers Fault Codes

    tmp:Required = 0
    access:chartype.clearkey(cha:charge_type_key)
    cha:charge_type = job:Warranty_charge_type
    If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
        If cha:force_warranty = 'YES'
            If job:Repair_Type_Warranty <> ''
                Access:REPTYDEF.ClearKey(rtd:Warranty_Key)
                rtd:Warranty    = 'YES'
                rtd:Repair_Type = job:Repair_Type_Warranty
                If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                    !Found
                    If rtd:CompFaultCoding
                        tmp:Required = 1
                    End !If rtd:CompFaultCoding
                Else!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
            Else !If job:Repair_Type_Warranty <> ''
                tmp:Required = 1
            End !If job:Repair_Type_Warranty <> ''

        End
    end !if

    found# = 0
    setcursor(cursor:wait)
    save_maf_id = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = job:manufacturer
    set(maf:field_number_key,maf:field_number_key)
    loop
        if access:manfault.next()
           break
        end !if
        if maf:manufacturer <> job:manufacturer      |
            then break.  ! end if
        IF found# = 0
            found# = 1
            Unhide(?tab1)
        End!IF found# = 0
        hide# = 0
        req#  = 0

        If f_request = Insertrecord
            If Maf:compulsory_at_booking = 'YES' and tmp:Required
                req#    = 1
            Else!If Maf:compulsory_at_booking = 'YES'
                hide#   = 1
            End!If Maf:compulsory_at_booking = 'YES'
        Else!If f_request = Insertrecord
            If MAF:Compulsory = 'YES' and tmp:Required
                req# = 1
            End!If MAF:Compulsory = 'YES'
        End!If f_request = Insertrecord

        If maf:lookup = 'YES'
            field"  = ''
            records# = 0
            save_mfo_id = access:manfaulo.savefile()
            access:manfaulo.clearkey(mfo:field_key)
            mfo:manufacturer = maf:manufacturer
            mfo:field_number = maf:field_number
            set(mfo:field_key,mfo:field_key)
            loop
                if access:manfaulo.next()
                   break
                end !if
                if mfo:manufacturer <> maf:manufacturer      |
                or mfo:field_number <> maf:field_number      |
                    then break.  ! end if
                records# += 1
                If records# > 1
                    field" = ''
                    Break
                End!If records# > 1
                field"  = mfo:field
            end !loop
            access:manfaulo.restorefile(save_mfo_id)
            Case maf:field_number
                of 1
                    If job:fault_code1 = ''
                        job:fault_code1 = field"
                    End!If job:fault_code = ''
                of 2
                    If job:fault_code2 = ''
                        job:fault_code2 = field"
                    End!If job:fault_code = ''
                of 3
                    If job:fault_code3 = ''
                        job:fault_code3 = field"
                    End!If job:fault_code = ''
                of 4
                    If job:fault_code4 = ''
                        job:fault_code4 = field"
                    End!If job:fault_code = ''
                of 5
                    If job:fault_code5 = ''
                        job:fault_code5 = field"
                    End!If job:fault_code = ''
                of 6
                    If job:fault_code6 = ''
                        job:fault_code6 = field"
                    End!If job:fault_code = ''
                of 7
                    If job:fault_code7 = ''
                        job:fault_code7 = field"
                    End!If job:fault_code = ''
                of 8
                    If job:fault_code8 = ''
                        job:fault_code8 = field"
                    End!If job:fault_code = ''
                of 9
                    If job:fault_code9 = ''
                        job:fault_code9 = field"
                    End!If job:fault_code = ''
                of 10
                    If job:fault_code10 = ''
                        job:fault_code10 = field"
                    End!If job:fault_code = ''
                of 11
                    If job:fault_code11 = ''
                        job:fault_code11 = field"
                    End!If job:fault_code = ''
                of 12
                    If job:fault_code12 = ''
                        job:fault_code12 = field"
                    End!If job:fault_code = ''
                of 13
                    If jobe:FaultCode13 = ''
                        jobe:FaultCode13 = field"
                    End ! If jobe:FaultCode13 = ''
                Of 14
                    If jobe:FaultCode14 = ''
                        jobe:FaultCode14 = Field"
                    End ! If jobe:FaultCode14 = ''e
                Of 15
                    If jobe:FaultCode15 = ''
                        jobe:FaultCode15 = Field"
                    End ! If jobe:FaultCode15 = ''
                Of 16
                    If jobe:FaultCode16 = ''
                        jobe:FaultCode16 = Field"
                    End ! If jobe:FaultCode16 = ''
                Of 17
                    If jobe:FaultCode17 = ''
                        jobe:FaultCode17 = Field"
                    End ! If jobe:FaultCode17 = ''
                Of 18
                    If jobe:FaultCode18 = ''
                        jobe:FaultCode18 = Field"
                    End ! If jobe:FaultCode18 = ''
                Of 19
                    If jobe:FaultCode19 = ''
                        jobe:FaultCode19 = Field"
                    End ! If jobe:FaultCode19 = ''
                Of 20
                    If jobe:FaultCode20 = ''
                        jobe:FaultCode20 = Field"
                    End ! If jobe:FaultCode20 = ''

            End!Case maf:field_number
        End!If maf:lookup = 'YES'

        Case maf:field_number
            Of 1
                If hide# = 0
                    Unhide(?job:fault_code1:prompt)
                    Unhide(?job:fault_code1)
                    ?job:fault_code1:prompt{prop:text} = maf:field_name
                    If req# = 1
                        ?job:fault_code1{prop:req} = 1
                    End!If req# = 1

                    Case maf:field_type
                        Of 'DATE'
                            ?job:fault_code1{prop:text} = Clip(maf:DateType)
                            ?job:fault_code1{prop:width} = 64
                            Unhide(?popcalendar)

                        Of 'STRING'
                            ?job:fault_code1{prop:width} = 124
                            If maf:restrictlength
                                ?job:fault_code1{prop:text} = '@s' & maf:lengthto
                            Else!If maf:restrictlength
                                ?job:fault_code1{prop:text} = '@s30'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup)
                            End!If maf:lookup = 'YES'
                        Of 'NUMBER'
                            ?job:fault_code1{prop:width} = 64
                            If maf:restrictlength
                                ?job:fault_code1{prop:text} = '@n' & maf:lengthto
                            Else!If maf:restrictlength
                                ?job:fault_code1{prop:text} = '@n9'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup)
                            End!If maf:lookup = 'YES'
                    End!Case maf:field_type

                End!If hide# = 0
            Of 2
                If hide# = 0
                    Unhide(?job:fault_code2:prompt)
                    Unhide(?job:fault_code2)
                    ?job:fault_code2:prompt{prop:text} = maf:field_name
                    If req# = 1
                        ?job:fault_code2{prop:req} = 1
                    End!If req# = 1
                    Case maf:field_type
                        Of 'DATE'
                            ?job:fault_code2{prop:text} = Clip(maf:DateType)
                            ?job:fault_code2{prop:width} = 64
                            Unhide(?popcalendar:2)

                        Of 'STRING'
                            ?job:fault_code2{prop:width} = 124
                            If maf:restrictlength
                                ?job:fault_code2{prop:text} = '@s' & maf:lengthto
                            Else!If maf:restrictlength
                                ?job:fault_code2{prop:text} = '@s30'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup:2)
                            End!If maf:lookup = 'YES'
                        Of 'NUMBER'

                            ?job:fault_code2{prop:width} = 64
                            If maf:restrictlength
                                ?job:fault_code2{prop:text} = '@n' & maf:lengthto
                            Else!If maf:restrictlength
                                ?job:fault_code2{prop:text} = '@n9'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup:2)
                            End!If maf:lookup = 'YES'
                    End!Case maf:field_type
                End!If hide# = 0

            Of 3
                if hide# = 0
                    unhide(?job:fault_code3:prompt)
                    unhide(?job:fault_code3)
                    ?job:fault_code3:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?job:fault_code3{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?job:fault_code3{prop:text} = Clip(maf:DateType)
                            ?job:fault_code3{prop:width} = 64
                            unhide(?popcalendar:3)

                        of 'STRING'
                            ?job:fault_code3{prop:width} = 124
                            if maf:restrictlength
                                ?job:fault_code3{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code3{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:3)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?job:fault_code3{prop:width} = 64
                            if maf:restrictlength
                                ?job:fault_code3{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code3{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:3)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 4
                if hide# = 0
                    unhide(?job:fault_code4:prompt)
                    unhide(?job:fault_code4)
                    ?job:fault_code4:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?job:fault_code4{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?job:fault_code4{prop:text} = Clip(maf:DateType)
                            ?job:fault_code4{prop:width} = 64
                            unhide(?popcalendar:4)

                        of 'STRING'
                            ?job:fault_code4{prop:width} = 124
                            if maf:restrictlength
                                ?job:fault_code4{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code4{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:4)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?job:fault_code4{prop:width} = 64
                            if maf:restrictlength
                                ?job:fault_code4{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code4{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:4)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 5
                if hide# = 0
                    unhide(?job:fault_code5:prompt)
                    unhide(?job:fault_code5)
                    ?job:fault_code5:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?job:fault_code5{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?job:fault_code5{prop:text} = Clip(maf:DateType)
                            ?job:fault_code5{prop:width} = 64
                            unhide(?popcalendar:5)

                        of 'STRING'
                            ?job:fault_code5{prop:width} = 124
                            if maf:restrictlength
                                ?job:fault_code5{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code5{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:5)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?job:fault_code5{prop:width} = 64
                            if maf:restrictlength
                                ?job:fault_code5{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code5{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:5)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 6
                if hide# = 0
                    unhide(?job:fault_code6:prompt)
                    unhide(?job:fault_code6)
                    ?job:fault_code6:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?job:fault_code6{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?job:fault_code6{prop:text} = Clip(maf:DateType)
                            ?job:fault_code6{prop:width} = 64
                            unhide(?popcalendar:6)

                        of 'STRING'
                            ?job:fault_code6{prop:width} = 124
                            if maf:restrictlength
                                ?job:fault_code6{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code6{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:6)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?job:fault_code6{prop:width} = 64
                            if maf:restrictlength
                                ?job:fault_code6{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code6{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:6)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 7
                if hide# = 0
                    unhide(?job:fault_code7:prompt)
                    unhide(?job:fault_code7)
                    ?job:fault_code7:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?job:fault_code7{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?job:fault_code7{prop:text} = Clip(maf:DateType)
                            ?job:fault_code7{prop:width} = 64
                            unhide(?popcalendar:7)

                        of 'STRING'
                            ?job:fault_code7{prop:width} = 124
                            if maf:restrictlength
                                ?job:fault_code7{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code7{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:7)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?job:fault_code7{prop:width} = 64
                            if maf:restrictlength
                                ?job:fault_code7{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code7{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:7)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0

            Of 8
                if hide# = 0
                    unhide(?job:fault_code8:prompt)
                    unhide(?job:fault_code8)
                    ?job:fault_code8:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?job:fault_code8{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?job:fault_code8{prop:text} = Clip(maf:DateType)
                            ?job:fault_code8{prop:width} = 64
                            unhide(?popcalendar:8)

                        of 'STRING'
                            ?job:fault_code8{prop:width} = 124
                            if maf:restrictlength
                                ?job:fault_code8{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code8{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:8)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?job:fault_code8{prop:width} = 64
                            if maf:restrictlength
                                ?job:fault_code8{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code8{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:8)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 9
                if hide# = 0
                    unhide(?job:fault_code9:prompt)
                    unhide(?job:fault_code9)
                    ?job:fault_code9:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?job:fault_code9{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?job:fault_code9{prop:text} = Clip(maf:DateType)
                            ?job:fault_code9{prop:width} = 64
                            unhide(?popcalendar:9)

                        of 'STRING'
                            ?job:fault_code9{prop:width} = 124
                            if maf:restrictlength
                                ?job:fault_code9{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code9{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:9)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?job:fault_code9{prop:width} = 64
                            if maf:restrictlength
                                ?job:fault_code9{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code9{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:9)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 10
                if hide# = 0
                    unhide(?job:fault_code10:prompt)
                    unhide(?job:fault_code10)
                    ?job:fault_code10:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?job:fault_code10{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?job:fault_code10{prop:text} = Clip(maf:DateType)
                            ?job:fault_code10{prop:width} = 64
                            unhide(?popcalendar:10)

                        of 'STRING'
                            ?job:fault_code10{prop:width} = 124
                            if maf:restrictlength
                                ?job:fault_code10{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code10{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:10)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?job:fault_code10{prop:width} = 64
                            if maf:restrictlength
                                ?job:fault_code10{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code10{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:10)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 11
                if hide# = 0
                    unhide(?job:fault_code11:prompt)
                    unhide(?job:fault_code11)
                    ?job:fault_code11:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?job:fault_code11{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?job:fault_code11{prop:text} = Clip(maf:DateType)
                            ?job:fault_code11{prop:width} = 64
                            unhide(?popcalendar:11)

                        of 'STRING'
                            ?job:fault_code11{prop:width} = 124
                            if maf:restrictlength
                                ?job:fault_code11{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code11{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:11)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?job:fault_code11{prop:width} = 64
                            if maf:restrictlength
                                ?job:fault_code11{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code11{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:11)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 12
                if hide# = 0
                    unhide(?job:fault_code12:prompt)
                    unhide(?job:fault_code12)
                    ?job:fault_code12:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?job:fault_code12{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?job:fault_code12{prop:text} = Clip(maf:DateType)
                            ?job:fault_code12{prop:width} = 64
                            unhide(?popcalendar:12)

                        of 'STRING'
                            ?job:fault_code12{prop:width} = 124
                            if maf:restrictlength
                                ?job:fault_code12{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code12{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:12)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?job:fault_code12{prop:width} = 64
                            if maf:restrictlength
                                ?job:fault_code12{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?job:fault_code12{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:12)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 13
                if hide# = 0
                    unhide(?jobe:FaultCode13:prompt)
                    unhide(?jobe:FaultCode13)
                    ?jobe:FaultCode13:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jobe:FaultCode13{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jobe:FaultCode13{prop:text} = Clip(maf:DateType)
                            ?jobe:FaultCode13{prop:width} = 64
                            unhide(?popcalendar:25)

                        of 'STRING'
                            ?jobe:FaultCode13{prop:width} = 124
                            if maf:restrictlength
                                ?jobe:FaultCode13{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode13{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:13)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:FaultCode13{prop:width} = 64
                            if maf:restrictlength
                                ?jobe:FaultCode13{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode13{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:13)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 14
                if hide# = 0
                    unhide(?jobe:FaultCode14:prompt)
                    unhide(?jobe:FaultCode14)
                    ?jobe:FaultCode14:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jobe:FaultCode14{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jobe:FaultCode14{prop:text} = Clip(maf:DateType)
                            ?jobe:FaultCode14{prop:width} = 64
                            unhide(?popcalendar:26)

                        of 'STRING'
                            ?jobe:FaultCode14{prop:width} = 124
                            if maf:restrictlength
                                ?jobe:FaultCode14{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode14{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:14)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:FaultCode14{prop:width} = 64
                            if maf:restrictlength
                                ?jobe:FaultCode14{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode14{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:14)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 15
                if hide# = 0
                    unhide(?jobe:FaultCode15:prompt)
                    unhide(?jobe:FaultCode15)
                    ?jobe:FaultCode15:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jobe:FaultCode15{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jobe:FaultCode15{prop:text} = Clip(maf:DateType)
                            ?jobe:FaultCode15{prop:width} = 64
                            unhide(?popcalendar:27)

                        of 'STRING'
                            ?jobe:FaultCode15{prop:width} = 124
                            if maf:restrictlength
                                ?jobe:FaultCode15{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode15{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:15)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:FaultCode15{prop:width} = 64
                            if maf:restrictlength
                                ?jobe:FaultCode15{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode15{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:15)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 16
                if hide# = 0
                    unhide(?jobe:FaultCode16:prompt)
                    unhide(?jobe:FaultCode16)
                    ?jobe:FaultCode16:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jobe:FaultCode16{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jobe:FaultCode16{prop:text} = Clip(maf:DateType)
                            ?jobe:FaultCode16{prop:width} = 64
                            unhide(?popcalendar:28)

                        of 'STRING'
                            ?jobe:FaultCode16{prop:width} = 124
                            if maf:restrictlength
                                ?jobe:FaultCode16{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode16{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:16)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:FaultCode16{prop:width} = 64
                            if maf:restrictlength
                                ?jobe:FaultCode16{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode16{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:16)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 17
                if hide# = 0
                    unhide(?jobe:FaultCode17:prompt)
                    unhide(?jobe:FaultCode17)
                    ?jobe:FaultCode17:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jobe:FaultCode17{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jobe:FaultCode17{prop:text} = Clip(maf:DateType)
                            ?jobe:FaultCode17{prop:width} = 64
                            unhide(?popcalendar:29)

                        of 'STRING'
                            ?jobe:FaultCode17{prop:width} = 124
                            if maf:restrictlength
                                ?jobe:FaultCode17{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode17{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:17)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:FaultCode17{prop:width} = 64
                            if maf:restrictlength
                                ?jobe:FaultCode17{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode17{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:17)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 18
                if hide# = 0
                    unhide(?jobe:FaultCode18:prompt)
                    unhide(?jobe:FaultCode18)
                    ?jobe:FaultCode18:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jobe:FaultCode18{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jobe:FaultCode18{prop:text} = Clip(maf:DateType)
                            ?jobe:FaultCode18{prop:width} = 64
                            unhide(?popcalendar:30)

                        of 'STRING'
                            ?jobe:FaultCode18{prop:width} = 124
                            if maf:restrictlength
                                ?jobe:FaultCode18{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode18{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:18)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:FaultCode18{prop:width} = 64
                            if maf:restrictlength
                                ?jobe:FaultCode18{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode18{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:18)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 19
                if hide# = 0
                    unhide(?jobe:FaultCode19:prompt)
                    unhide(?jobe:FaultCode19)
                    ?jobe:FaultCode19:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jobe:FaultCode19{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jobe:FaultCode19{prop:text} = Clip(maf:DateType)
                            ?jobe:FaultCode19{prop:width} = 64
                            unhide(?popcalendar:31)

                        of 'STRING'
                            ?jobe:FaultCode19{prop:width} = 124
                            if maf:restrictlength
                                ?jobe:FaultCode19{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode19{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:19)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:FaultCode19{prop:width} = 64
                            if maf:restrictlength
                                ?jobe:FaultCode19{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode19{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:19)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 20
                if hide# = 0
                    unhide(?jobe:FaultCode20:prompt)
                    unhide(?jobe:FaultCode20)
                    ?jobe:FaultCode20:prompt{prop:text} = maf:field_name
                    if req# = 1
                        ?jobe:FaultCode20{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?jobe:FaultCode20{prop:text} = Clip(maf:DateType)
                            ?jobe:FaultCode20{prop:width} = 64
                            unhide(?popcalendar:32)

                        of 'STRING'
                            ?jobe:FaultCode20{prop:width} = 124
                            if maf:restrictlength
                                ?jobe:FaultCode20{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode20{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:20)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:FaultCode20{prop:width} = 64
                            if maf:restrictlength
                                ?jobe:FaultCode20{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?jobe:FaultCode20{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:20)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
        End
    end !loop
    access:manfault.restorefile(save_maf_id)
    setcursor()
    Display()
!continued.......
    found# = 0
    setcursor(cursor:wait)
    save_taf_id = access:trafault.savefile()
    access:trafault.clearkey(taf:field_number_key)
    taf:accountnumber = tra:account_number
    set(taf:field_number_key,taf:field_number_key)
    loop
        if access:trafault.next()
           break
        end !if
        if taf:accountnumber <> tra:account_number      |
            then break.  ! end if
        if found# = 0
            found# = 1
            unhide(?Tab2)
        End!if found# = 0
        hide# = 0
        req#  = 0
        If f_request = Insertrecord

            If taf:compulsory_at_booking = 'YES'
                req#    = 1
            Else!If taf:compulsory_at_booking = 'YES'
                hide#   = 1
            End!If taf:compulsory_at_booking = 'YES'
        Else!If f_request = Insertrecord
            If taf:Compulsory = 'YES'
                req# = 1
            End!If taf:Compulsory = 'YES'
        End!If f_request = Insertrecord

        If taf:lookup = 'YES'
            field"  = ''
            records# = 0
            save_tfo_id = access:trafaulo.savefile()
            access:trafaulo.clearkey(tfo:field_key)
            tfo:accountnumber = taf:accountnumber
            tfo:field_number = taf:field_number
            set(tfo:field_key,tfo:field_key)
            loop
                if access:trafaulo.next()
                   break
                end !if
                if tfo:accountnumber <> taf:accountnumber      |
                or tfo:field_number <> taf:field_number      |
                    then break.  ! end if
                records# += 1
                If records# > 1
                    field" = ''
                    Break
                End!If records# > 1
                field"  = tfo:field
            end !loop
            access:trafaulo.restorefile(save_tfo_id)
            Case taf:field_number
                of 1
                    If jobe:TraFaultCode1 = ''
                        jobe:TraFaultCode1 = field"
                    End!If jobe:TraFaultCode = ''
                of 2
                    If jobe:TraFaultCode2 = ''
                        jobe:TraFaultCode2 = field"
                    End!If jobe:TraFaultCode = ''
                of 3
                    If jobe:TraFaultCode3 = ''
                        jobe:TraFaultCode3 = field"
                    End!If jobe:TraFaultCode = ''
                of 4
                    If jobe:TraFaultCode4 = ''
                        jobe:TraFaultCode4 = field"
                    End!If jobe:TraFaultCode = ''
                of 5
                    If jobe:TraFaultCode5 = ''
                        jobe:TraFaultCode5 = field"
                    End!If jobe:TraFaultCode = ''
                of 6
                    If jobe:TraFaultCode6 = ''
                        jobe:TraFaultCode6 = field"
                    End!If jobe:TraFaultCode = ''
                of 7
                    If jobe:TraFaultCode7 = ''
                        jobe:TraFaultCode7 = field"
                    End!If jobe:TraFaultCode = ''
                of 8
                    If jobe:TraFaultCode8 = ''
                        jobe:TraFaultCode8 = field"
                    End!If jobe:TraFaultCode = ''
                of 9
                    If jobe:TraFaultCode9 = ''
                        jobe:TraFaultCode9 = field"
                    End!If jobe:TraFaultCode = ''
                of 10
                    If jobe:TraFaultCode10 = ''
                        jobe:TraFaultCode10 = field"
                    End!If jobe:TraFaultCode = ''
                of 11
                    If jobe:TraFaultCode11 = ''
                        jobe:TraFaultCode11 = field"
                    End!If jobe:TraFaultCode = ''
                of 12
                    If jobe:TraFaultCode12 = ''
                        jobe:TraFaultCode12 = field"
                    End!If jobe:TraFaultCode = ''
            End!Case taf:field_number
        End!If taf:lookup = 'YES'

        Case taf:field_number
            Of 1
                If hide# = 0
                    Unhide(?jobe:TraFaultCode1:prompt)
                    Unhide(?jobe:TraFaultCode1)
                    ?jobe:TraFaultCode1:prompt{prop:text} = taf:field_name
                    If req# = 1
                        ?jobe:TraFaultCode1{prop:req} = 1
                    End!If req# = 1

                    Case taf:field_type
                        Of 'DATE'
                            ?jobe:TraFaultCode1{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode1{prop:width} = 64
                            Unhide(?popcalendar:13)

                        Of 'STRING'
                            ?jobe:TraFaultCode1{prop:width} = 124
                            If taf:restrictlength
                                ?jobe:TraFaultCode1{prop:text} = '@s' & taf:lengthto
                            Else!If taf:restrictlength
                                ?jobe:TraFaultCode1{prop:text} = '@s30'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:1)
                            End!If taf:lookup = 'YES'
                        Of 'NUMBER'
                            ?jobe:TraFaultCode1{prop:width} = 64
                            If taf:restrictlength
                                ?jobe:TraFaultCode1{prop:text} = '@n' & taf:lengthto
                            Else!If taf:restrictlength
                                ?jobe:TraFaultCode1{prop:text} = '@n9'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:1)
                            End!If taf:lookup = 'YES'
                    End!Case taf:field_type

                End!If hide# = 0
            Of 2
                If hide# = 0
                    Unhide(?jobe:TraFaultCode2:prompt)
                    Unhide(?jobe:TraFaultCode2)
                    ?jobe:TraFaultCode2:prompt{prop:text} = taf:field_name
                    If req# = 1
                        ?jobe:TraFaultCode2{prop:req} = 1
                    End!If req# = 1
                    Case taf:field_type
                        Of 'DATE'
                            ?jobe:TraFaultCode2{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode2{prop:width} = 64
                            Unhide(?popcalendar:14)

                        Of 'STRING'
                            ?jobe:TraFaultCode2{prop:width} = 124
                            If taf:restrictlength
                                ?jobe:TraFaultCode2{prop:text} = '@s' & taf:lengthto
                            Else!If taf:restrictlength
                                ?jobe:TraFaultCode2{prop:text} = '@s30'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:2)
                            End!If taf:lookup = 'YES'
                        Of 'NUMBER'

                            ?jobe:TraFaultCode2{prop:width} = 64
                            If taf:restrictlength
                                ?jobe:TraFaultCode2{prop:text} = '@n' & taf:lengthto
                            Else!If taf:restrictlength
                                ?jobe:TraFaultCode2{prop:text} = '@n9'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:2)
                            End!If taf:lookup = 'YES'
                    End!Case taf:field_type
                End!If hide# = 0

            Of 3
                if hide# = 0
                    unhide(?jobe:TraFaultCode3:prompt)
                    unhide(?jobe:TraFaultCode3)
                    ?jobe:TraFaultCode3:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?jobe:TraFaultCode3{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?jobe:TraFaultCode3{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode3{prop:width} = 64
                            unhide(?popcalendar:15)

                        of 'STRING'
                            ?jobe:TraFaultCode3{prop:width} = 124
                            if taf:restrictlength
                                ?jobe:TraFaultCode3{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode3{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:3)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:TraFaultCode3{prop:width} = 64
                            if taf:restrictlength
                                ?jobe:TraFaultCode3{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode3{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:3)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 4
                if hide# = 0
                    unhide(?jobe:TraFaultCode4:prompt)
                    unhide(?jobe:TraFaultCode4)
                    ?jobe:TraFaultCode4:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?jobe:TraFaultCode4{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?jobe:TraFaultCode4{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode4{prop:width} = 64
                            unhide(?popcalendar:16)

                        of 'STRING'
                            ?jobe:TraFaultCode4{prop:width} = 124
                            if taf:restrictlength
                                ?jobe:TraFaultCode4{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode4{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:4)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:TraFaultCode4{prop:width} = 64
                            if taf:restrictlength
                                ?jobe:TraFaultCode4{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode4{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:4)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 5
                if hide# = 0
                    unhide(?jobe:TraFaultCode5:prompt)
                    unhide(?jobe:TraFaultCode5)
                    ?jobe:TraFaultCode5:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?jobe:TraFaultCode5{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?jobe:TraFaultCode5{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode5{prop:width} = 64
                            unhide(?popcalendar:17)

                        of 'STRING'
                            ?jobe:TraFaultCode5{prop:width} = 124
                            if taf:restrictlength
                                ?jobe:TraFaultCode5{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode5{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:5)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:TraFaultCode5{prop:width} = 64
                            if taf:restrictlength
                                ?jobe:TraFaultCode5{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode5{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:5)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 6
                if hide# = 0
                    unhide(?jobe:TraFaultCode6:prompt)
                    unhide(?jobe:TraFaultCode6)
                    ?jobe:TraFaultCode6:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?jobe:TraFaultCode6{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?jobe:TraFaultCode6{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode6{prop:width} = 64
                            unhide(?popcalendar:18)

                        of 'STRING'
                            ?jobe:TraFaultCode6{prop:width} = 124
                            if taf:restrictlength
                                ?jobe:TraFaultCode6{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode6{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:6)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:TraFaultCode6{prop:width} = 64
                            if taf:restrictlength
                                ?jobe:TraFaultCode6{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode6{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:6)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 7
                if hide# = 0
                    unhide(?jobe:TraFaultCode7:prompt)
                    unhide(?jobe:TraFaultCode7)
                    ?jobe:TraFaultCode7:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?jobe:TraFaultCode7{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?jobe:TraFaultCode7{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode7{prop:width} = 64
                            unhide(?popcalendar:19)

                        of 'STRING'
                            ?jobe:TraFaultCode7{prop:width} = 124
                            if taf:restrictlength
                                ?jobe:TraFaultCode7{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode7{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:7)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:TraFaultCode7{prop:width} = 64
                            if taf:restrictlength
                                ?jobe:TraFaultCode7{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode7{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:7)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0

            Of 8
                if hide# = 0
                    unhide(?jobe:TraFaultCode8:prompt)
                    unhide(?jobe:TraFaultCode8)
                    ?jobe:TraFaultCode8:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?jobe:TraFaultCode8{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?jobe:TraFaultCode8{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode8{prop:width} = 64
                            unhide(?popcalendar:20)

                        of 'STRING'
                            ?jobe:TraFaultCode8{prop:width} = 124
                            if taf:restrictlength
                                ?jobe:TraFaultCode8{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode8{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:8)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:TraFaultCode8{prop:width} = 64
                            if taf:restrictlength
                                ?jobe:TraFaultCode8{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode8{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:8)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 9
                if hide# = 0
                    unhide(?jobe:TraFaultCode9:prompt)
                    unhide(?jobe:TraFaultCode9)
                    ?jobe:TraFaultCode9:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?jobe:TraFaultCode9{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?jobe:TraFaultCode9{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode9{prop:width} = 64
                            unhide(?popcalendar:21)

                        of 'STRING'
                            ?jobe:TraFaultCode9{prop:width} = 124
                            if taf:restrictlength
                                ?jobe:TraFaultCode9{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode9{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:9)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:TraFaultCode9{prop:width} = 64
                            if taf:restrictlength
                                ?jobe:TraFaultCode9{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode9{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:9)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 10
                if hide# = 0
                    unhide(?jobe:TraFaultCode10:prompt)
                    unhide(?jobe:TraFaultCode10)
                    ?jobe:TraFaultCode10:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?jobe:TraFaultCode10{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?jobe:TraFaultCode10{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode10{prop:width} = 64
                            unhide(?popcalendar:22)

                        of 'STRING'
                            ?jobe:TraFaultCode10{prop:width} = 124
                            if taf:restrictlength
                                ?jobe:TraFaultCode10{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode10{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:10)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:TraFaultCode10{prop:width} = 64
                            if taf:restrictlength
                                ?jobe:TraFaultCode10{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode10{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:10)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 11
                if hide# = 0
                    unhide(?jobe:TraFaultCode11:prompt)
                    unhide(?jobe:TraFaultCode11)
                    ?jobe:TraFaultCode11:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?jobe:TraFaultCode11{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?jobe:TraFaultCode11{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode11{prop:width} = 64
                            unhide(?popcalendar:23)

                        of 'STRING'
                            ?jobe:TraFaultCode11{prop:width} = 124
                            if taf:restrictlength
                                ?jobe:TraFaultCode11{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode11{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:11)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:TraFaultCode11{prop:width} = 64
                            if taf:restrictlength
                                ?jobe:TraFaultCode11{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode11{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:11)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 12
                if hide# = 0
                    unhide(?jobe:TraFaultCode12:prompt)
                    unhide(?jobe:TraFaultCode12)
                    ?jobe:TraFaultCode12:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?jobe:TraFaultCode12{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?jobe:TraFaultCode12{prop:text} = Clip(maf:DateType)
                            ?jobe:TraFaultCode12{prop:width} = 64
                            unhide(?popcalendar:24)

                        of 'STRING'
                            ?jobe:TraFaultCode12{prop:width} = 124
                            if taf:restrictlength
                                ?jobe:TraFaultCode12{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode12{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:12)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?jobe:TraFaultCode12{prop:width} = 64
                            if taf:restrictlength
                                ?jobe:TraFaultCode12{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?jobe:TraFaultCode12{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:12)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
        End
    end !loop
    access:trafault.restorefile(save_taf_id)
    setcursor()

    If ?tab1{prop:hide} = 1 And ?tab2{prop:hide} = 1
        ?tab3{prop:hide} = 0
    End!If ?tab1{prop:hide} = 0 Or ?tab2{prop:hide} = 0
    !ThisMakeOver.SetWindow(Win:FORM)
    Display()
PutTradeNotes       Routine
    access:jobnotes.clearkey(jbn:refnumberkey)
    jbn:refnumber = job:ref_number
    if access:jobnotes.tryfetch(jbn:refnumberkey)
        access:jobnotes.primerecord()
        access:jobnotes.tryinsert()
        access:jobnotes.clearkey(jbn:refnumberkey)
        jbn:refnumber   = job:ref_number
        access:jobnotes.tryfetch(jbn:refnumberkey)
    end!if access:jobnotes.tryfetch(jbn:refnumberkey)

    if taf:replicatefault = 'YES'
        if jbn:fault_description = ''
            jbn:fault_description = clip(tfo:description)
        else!if jbn:fault_description = ''
            jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(tfo:description)
        end!if jbn:fault_description = ''
    end!if mfo:replicatefault = 'YES'
    if taf:replicateinvoice = 'YES'
        if jbn:invoice_text = ''
            jbn:invoice_text = clip(tfo:description)
        else!if jbn:fault_description = ''
            jbn:invoice_text = clip(jbn:invoice_text) & '<13,10>' & clip(tfo:description)
        end!if jbn:fault_description = ''
    end!if mfo:replicatefault = 'YES'

    access:jobnotes.update()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Fault_Codes_Window',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Fault_Codes_Window',1)
    SolaceViewVars('tmp:Required',tmp:Required,'Fault_Codes_Window',1)
    SolaceViewVars('save_taf_id',save_taf_id,'Fault_Codes_Window',1)
    SolaceViewVars('save_tfo_id',save_tfo_id,'Fault_Codes_Window',1)
    SolaceViewVars('save_mfo_id',save_mfo_id,'Fault_Codes_Window',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Fault_Codes_Window',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code1:Prompt;  SolaceCtrlName = '?JOB:Fault_Code1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code1;  SolaceCtrlName = '?job:Fault_Code1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup;  SolaceCtrlName = '?Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code2:Prompt;  SolaceCtrlName = '?JOB:Fault_Code2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code2;  SolaceCtrlName = '?job:Fault_Code2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:2;  SolaceCtrlName = '?Lookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code3:Prompt;  SolaceCtrlName = '?JOB:Fault_Code3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code3;  SolaceCtrlName = '?job:Fault_Code3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:3;  SolaceCtrlName = '?Lookup:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code4:Prompt;  SolaceCtrlName = '?JOB:Fault_Code4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code4;  SolaceCtrlName = '?job:Fault_Code4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:4;  SolaceCtrlName = '?Lookup:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code5:Prompt;  SolaceCtrlName = '?JOB:Fault_Code5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code5;  SolaceCtrlName = '?job:Fault_Code5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:5;  SolaceCtrlName = '?Lookup:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code6:Prompt;  SolaceCtrlName = '?JOB:Fault_Code6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code6;  SolaceCtrlName = '?job:Fault_Code6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:6;  SolaceCtrlName = '?Lookup:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code7:Prompt;  SolaceCtrlName = '?JOB:Fault_Code7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code7;  SolaceCtrlName = '?job:Fault_Code7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:7;  SolaceCtrlName = '?Lookup:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code8:Prompt;  SolaceCtrlName = '?JOB:Fault_Code8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code8;  SolaceCtrlName = '?job:Fault_Code8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:8;  SolaceCtrlName = '?Lookup:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code9:Prompt;  SolaceCtrlName = '?JOB:Fault_Code9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code9;  SolaceCtrlName = '?job:Fault_Code9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:9;  SolaceCtrlName = '?Lookup:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code10:Prompt;  SolaceCtrlName = '?JOB:Fault_Code10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code10;  SolaceCtrlName = '?job:Fault_Code10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:10;  SolaceCtrlName = '?Lookup:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code11:Prompt;  SolaceCtrlName = '?JOB:Fault_Code11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code11;  SolaceCtrlName = '?job:Fault_Code11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:11;  SolaceCtrlName = '?Lookup:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code12:Prompt;  SolaceCtrlName = '?JOB:Fault_Code12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code12;  SolaceCtrlName = '?job:Fault_Code12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:12;  SolaceCtrlName = '?Lookup:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode13:Prompt;  SolaceCtrlName = '?jobe:FaultCode13:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode13;  SolaceCtrlName = '?jobe:FaultCode13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:25;  SolaceCtrlName = '?PopCalendar:25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode14:Prompt;  SolaceCtrlName = '?jobe:FaultCode14:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode14;  SolaceCtrlName = '?jobe:FaultCode14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:26;  SolaceCtrlName = '?PopCalendar:26';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:13;  SolaceCtrlName = '?Lookup:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode15:Prompt;  SolaceCtrlName = '?jobe:FaultCode15:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode15;  SolaceCtrlName = '?jobe:FaultCode15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:27;  SolaceCtrlName = '?PopCalendar:27';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:14;  SolaceCtrlName = '?Lookup:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode16:Prompt;  SolaceCtrlName = '?jobe:FaultCode16:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode16;  SolaceCtrlName = '?jobe:FaultCode16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:28;  SolaceCtrlName = '?PopCalendar:28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:15;  SolaceCtrlName = '?Lookup:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode17:Prompt;  SolaceCtrlName = '?jobe:FaultCode17:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode17;  SolaceCtrlName = '?jobe:FaultCode17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:29;  SolaceCtrlName = '?PopCalendar:29';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:16;  SolaceCtrlName = '?Lookup:16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode18:Prompt;  SolaceCtrlName = '?jobe:FaultCode18:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode18;  SolaceCtrlName = '?jobe:FaultCode18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:30;  SolaceCtrlName = '?PopCalendar:30';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:17;  SolaceCtrlName = '?Lookup:17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode19:Prompt;  SolaceCtrlName = '?jobe:FaultCode19:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode19;  SolaceCtrlName = '?jobe:FaultCode19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:31;  SolaceCtrlName = '?PopCalendar:31';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:18;  SolaceCtrlName = '?Lookup:18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode20:Prompt;  SolaceCtrlName = '?jobe:FaultCode20:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:FaultCode20;  SolaceCtrlName = '?jobe:FaultCode20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:32;  SolaceCtrlName = '?PopCalendar:32';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:20;  SolaceCtrlName = '?Lookup:20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:19;  SolaceCtrlName = '?Lookup:19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode1:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode1;  SolaceCtrlName = '?jobe:TraFaultCode1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:13;  SolaceCtrlName = '?PopCalendar:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:1;  SolaceCtrlName = '?tralookup:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode2:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode2;  SolaceCtrlName = '?jobe:TraFaultCode2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:14;  SolaceCtrlName = '?PopCalendar:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode3:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode3;  SolaceCtrlName = '?jobe:TraFaultCode3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:15;  SolaceCtrlName = '?PopCalendar:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode4:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode4;  SolaceCtrlName = '?jobe:TraFaultCode4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:16;  SolaceCtrlName = '?PopCalendar:16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:4;  SolaceCtrlName = '?tralookup:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode5:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode5;  SolaceCtrlName = '?jobe:TraFaultCode5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:17;  SolaceCtrlName = '?PopCalendar:17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:5;  SolaceCtrlName = '?tralookup:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode6:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode6;  SolaceCtrlName = '?jobe:TraFaultCode6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:18;  SolaceCtrlName = '?PopCalendar:18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:2;  SolaceCtrlName = '?tralookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:3;  SolaceCtrlName = '?tralookup:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode7:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode7;  SolaceCtrlName = '?jobe:TraFaultCode7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:19;  SolaceCtrlName = '?PopCalendar:19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:7;  SolaceCtrlName = '?tralookup:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode8:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode8;  SolaceCtrlName = '?jobe:TraFaultCode8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:20;  SolaceCtrlName = '?PopCalendar:20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:8;  SolaceCtrlName = '?tralookup:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode9:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode9;  SolaceCtrlName = '?jobe:TraFaultCode9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:21;  SolaceCtrlName = '?PopCalendar:21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode10:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode10;  SolaceCtrlName = '?jobe:TraFaultCode10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:22;  SolaceCtrlName = '?PopCalendar:22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode11:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode11;  SolaceCtrlName = '?jobe:TraFaultCode11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:23;  SolaceCtrlName = '?PopCalendar:23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode12:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:TraFaultCode12;  SolaceCtrlName = '?jobe:TraFaultCode12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:24;  SolaceCtrlName = '?PopCalendar:24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:9;  SolaceCtrlName = '?tralookup:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:11;  SolaceCtrlName = '?tralookup:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:6;  SolaceCtrlName = '?tralookup:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:10;  SolaceCtrlName = '?tralookup:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:12;  SolaceCtrlName = '?tralookup:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt25;  SolaceCtrlName = '?Prompt25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Fault_Codes_Window')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Fault_Codes_Window')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?JOB:Fault_Code1:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:REPTYDEF.Open
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:JOBNOTES.UseFile
  Access:TRAFAULT.UseFile
  Access:TRAFAULO.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ?jobe:FaultCode13{Prop:Alrt,255} = MouseLeft2
  ?jobe:FaultCode14{Prop:Alrt,255} = MouseLeft2
  ?jobe:FaultCode15{Prop:Alrt,255} = MouseLeft2
  ?jobe:FaultCode16{Prop:Alrt,255} = MouseLeft2
  ?jobe:FaultCode17{Prop:Alrt,255} = MouseLeft2
  ?jobe:FaultCode18{Prop:Alrt,255} = MouseLeft2
  ?jobe:FaultCode19{Prop:Alrt,255} = MouseLeft2
  ?jobe:FaultCode20{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:REPTYDEF.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Fault_Codes_Window',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  Globalrequest = Request
  Browse_Manufacturer_Fault_Lookup
  ReturnValue   = Globalresponse
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?tralookup:1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:1, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:1, Accepted)
    OF ?tralookup:4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:4, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 4
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:4, Accepted)
    OF ?tralookup:5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:5, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 5
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:5, Accepted)
    OF ?tralookup:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:2, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:2, Accepted)
    OF ?tralookup:3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:3, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 3
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:3, Accepted)
    OF ?tralookup:7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:7, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 7
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:7, Accepted)
    OF ?tralookup:8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:8, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 8
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:8, Accepted)
    OF ?tralookup:9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:9, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 9
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:9, Accepted)
    OF ?tralookup:11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:11, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 11
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:11, Accepted)
    OF ?tralookup:6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:6, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 6
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:6, Accepted)
    OF ?tralookup:10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:10, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 10
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:10, Accepted)
    OF ?tralookup:12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:12, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 12
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:12, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?job:Fault_Code1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code1, Accepted)
      If job:fault_code1 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 1
          if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 1
              mfo:field        = job:fault_code1
              if access:manfaulo.tryfetch(mfo:field_key)
                  If maf:force_lookup = 'YES'
                      Post(event:accepted,?Lookup)
                  End!If maf:force_lookup = 'YES'
              Else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
                  If Maf:ReplicateFault = 'YES'
                      If jbn:fault_description = ''
                          jbn:fault_description = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_Text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_Text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
      
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code1, Accepted)
    OF ?Lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
      If ?lookup{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 1
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
              job:fault_code1 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  If Maf:ReplicateFault = 'YES'
                      If jbn:fault_description = ''
                          jbn:fault_description = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
              display()
          Else
      !        job:fault_code1 = ''
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code1 = TINCALENDARStyle1(job:Fault_Code1)
          Display(?JOB:Fault_Code1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?job:Fault_Code2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code2, Accepted)
      if job:fault_code2 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 2
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 2
              mfo:field        = job:fault_code2
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:2)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code2 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code2, Accepted)
    OF ?Lookup:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
      If ?lookup:2{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 2
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
              job:fault_code2 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
      !        job:fault_code2 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:2{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code2 = TINCALENDARStyle1(job:Fault_Code2)
          Display(?JOB:Fault_Code2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?job:Fault_Code3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code3, Accepted)
      if job:fault_code3 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 3
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 3
              mfo:field        = job:fault_code3
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:3)
                  end!if maf:force_lookup = 'YES'
              else
                   access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code3 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code3, Accepted)
    OF ?Lookup:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
      If ?lookup:3{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 3
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              job:fault_code3 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
      !        job:fault_code3 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:3{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code3 = TINCALENDARStyle1(job:Fault_Code3)
          Display(?JOB:Fault_Code3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?job:Fault_Code4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code4, Accepted)
      if job:fault_code4 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 4
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 4
              mfo:field        = job:fault_code4
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:4)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code4 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code4, Accepted)
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code4 = TINCALENDARStyle1(job:Fault_Code4)
          Display(?JOB:Fault_Code4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
      If ?lookup:4{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 4
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              job:fault_code4 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code4 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:4{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
    OF ?job:Fault_Code5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code5, Accepted)
      if job:fault_code5 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 5
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 5
              mfo:field        = job:fault_code5
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:5)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code5 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code5, Accepted)
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code5 = TINCALENDARStyle1(job:Fault_Code5)
          Display(?JOB:Fault_Code5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
      If ?lookup:5{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 5
          glo:select3  = ''
          If Self.Run(1,Selectrecord) = RequestCompleted
              job:fault_code5 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
            !  job:fault_code5 = ''
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:5{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
    OF ?job:Fault_Code6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code6, Accepted)
      if job:fault_code6 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 6
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 6
              mfo:field        = job:fault_code6
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:6)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code6 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code6, Accepted)
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code6 = TINCALENDARStyle1(job:Fault_Code6)
          Display(?JOB:Fault_Code6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
      If ?lookup:6{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 6
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              job:fault_code6 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
            !  job:fault_code6 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:6{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
    OF ?job:Fault_Code7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code7, Accepted)
      if job:fault_code7 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 7
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 7
              mfo:field        = job:fault_code7
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:7)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code7 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code7, Accepted)
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code7 = TINCALENDARStyle1(job:Fault_Code7)
          Display(?JOB:Fault_Code7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
      If ?lookup:7{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 7
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
              job:fault_code7 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
            !  job:fault_code7 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:7{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
    OF ?job:Fault_Code8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code8, Accepted)
      if job:fault_code8 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 8
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 8
              mfo:field        = job:fault_code8
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:8)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code8 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code8, Accepted)
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code8 = TINCALENDARStyle1(job:Fault_Code8)
          Display(?JOB:Fault_Code8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
      If ?lookup:8{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 8
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              job:fault_code8 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
            !  job:fault_code8  = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:8{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
    OF ?job:Fault_Code9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code9, Accepted)
      if job:fault_code9 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 9
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 9
              mfo:field        = job:fault_code9
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:9)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code9 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code9, Accepted)
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code9 = TINCALENDARStyle1(job:Fault_Code9)
          Display(?JOB:Fault_Code9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
      If ?lookup:9{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 9
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              job:fault_code9 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code9  = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:9{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
    OF ?job:Fault_Code10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code10, Accepted)
      if job:fault_code10 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 10
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 10
              mfo:field        = job:fault_code10
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:10)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code10 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code10, Accepted)
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code10 = TINCALENDARStyle1(job:Fault_Code10)
          Display(?JOB:Fault_Code10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
      If ?lookup:10{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 10
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              job:fault_code10 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
            !  job:fault_code10 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:10{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
    OF ?job:Fault_Code11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code11, Accepted)
      if job:fault_code11 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 11
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 11
              mfo:field        = job:fault_code11
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:11)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code11 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code11, Accepted)
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code11 = TINCALENDARStyle1(job:Fault_Code11)
          Display(?JOB:Fault_Code11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
      If ?lookup:11{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 11
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              job:fault_code11 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code11 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:11{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
    OF ?job:Fault_Code12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code12, Accepted)
      if job:fault_code12 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          maf:field_number = 12
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = job:manufacturer
              mfo:field_number = 12
              mfo:field        = job:fault_code12
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:12)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber   = job:Ref_Number
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = job:ref_number
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code12 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code12, Accepted)
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code12 = TINCALENDARStyle1(job:Fault_Code12)
          Display(?JOB:Fault_Code12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
      If ?lookup:12{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 12
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              job:fault_code12 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
    OF ?PopCalendar:25
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:FaultCode13 = TINCALENDARStyle1(jobe:FaultCode13)
          Display(?jobe:FaultCode13)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:26
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:FaultCode14 = TINCALENDARStyle1(jobe:FaultCode14)
          Display(?jobe:FaultCode14)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:13
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:13, Accepted)
      If ?lookup:13{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 13
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jobe:FaultCode13 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:13, Accepted)
    OF ?PopCalendar:27
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:FaultCode15 = TINCALENDARStyle1(jobe:FaultCode15)
          Display(?jobe:FaultCode15)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:14
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:14, Accepted)
      If ?lookup:14{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 14
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jobe:FaultCode14 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:14, Accepted)
    OF ?PopCalendar:28
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:FaultCode16 = TINCALENDARStyle1(jobe:FaultCode16)
          Display(?jobe:FaultCode16)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:15
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:15, Accepted)
      If ?lookup:15{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 15
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jobe:FaultCode15 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:15, Accepted)
    OF ?PopCalendar:29
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:FaultCode17 = TINCALENDARStyle1(jobe:FaultCode17)
          Display(?jobe:FaultCode17)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:16
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:16, Accepted)
      If ?lookup:16{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 16
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jobe:FaultCode16 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:16, Accepted)
    OF ?PopCalendar:30
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:FaultCode18 = TINCALENDARStyle1(jobe:FaultCode18)
          Display(?jobe:FaultCode18)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:17
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:17, Accepted)
      If ?lookup:17{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 17
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jobe:FaultCode17 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:17, Accepted)
    OF ?PopCalendar:31
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:FaultCode19 = TINCALENDARStyle1(jobe:FaultCode19)
          Display(?jobe:FaultCode19)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:18
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:18, Accepted)
      If ?lookup:18{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 18
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jobe:FaultCode18 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:18, Accepted)
    OF ?PopCalendar:32
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:FaultCode20 = TINCALENDARStyle1(jobe:FaultCode20)
          Display(?jobe:FaultCode20)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:20
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:20, Accepted)
      If ?lookup:20{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 20
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jobe:FaultCode20 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:20, Accepted)
    OF ?Lookup:19
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:19, Accepted)
      If ?lookup:19{prop:hide} = false
          glo:select1  = job:manufacturer
          glo:select2  = 19
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              jobe:FaultCode19 = mfo:field
              access:manfault.clearkey(maf:field_number_key)
              maf:manufacturer = job:manufacturer
              maf:field_number = mfo:field_number
              if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                      If Maf:ReplicateInvoice = 'YES'
                          If jbn:invoice_text = ''
                              jbn:invoice_text = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              display()
          Else
             ! job:fault_code12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:19, Accepted)
    OF ?jobe:TraFaultCode1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode1, Accepted)
      if jobe:trafaultcode1 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 1
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 1
              tfo:Field         = jobe:TraFaultCode1
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 1      |
                  Or tfo:Field         <> jobe:TraFaultCode1
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:1)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  !Message(taf:ReplicateFault,'Before Routine')
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode1, Accepted)
    OF ?PopCalendar:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode1 = TINCALENDARStyle1(jobe:TraFaultCode1)
          Display(?JOBE:TraFaultCode1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:1
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:1, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode1 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 1
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 1
              tfo:Field         = jobe:TraFaultCode1
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 1      |
                  Or tfo:Field         <> jobe:TraFaultCode1
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:1)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
      !        jobe:TraFaultCode1 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode1)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:1, Accepted)
    OF ?jobe:TraFaultCode2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode2, Accepted)
      if jobe:trafaultcode2 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 2
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 2
              tfo:Field         = jobe:TraFaultCode2
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 2      |
                  Or tfo:Field         <> jobe:TraFaultCode2
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:2)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode2, Accepted)
    OF ?PopCalendar:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode2 = TINCALENDARStyle1(jobe:TraFaultCode2)
          Display(?JOBE:TraFaultCode2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?jobe:TraFaultCode3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode3, Accepted)
      if jobe:trafaultcode3 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 3
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 3
              tfo:Field         = jobe:TraFaultCode3
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 3      |
                  Or tfo:Field         <> jobe:TraFaultCode3
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:3)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode3, Accepted)
    OF ?PopCalendar:15
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode3 = TINCALENDARStyle1(jobe:TraFaultCode3)
          Display(?JOBE:TraFaultCode3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?jobe:TraFaultCode4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode4, Accepted)
      if jobe:trafaultcode4 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 4
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 4
              tfo:Field         = jobe:TraFaultCode4
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 4      |
                  Or tfo:Field         <> jobe:TraFaultCode4
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:4)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode4, Accepted)
    OF ?PopCalendar:16
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode4 = TINCALENDARStyle1(jobe:TraFaultCode4)
          Display(?JOBE:TraFaultCode4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:4
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:4, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode4 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 4
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 4
              tfo:Field         = jobe:TraFaultCode4
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 4      |
                  Or tfo:Field         <> jobe:TraFaultCode4
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:4)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
      !        jobe:TraFaultCode4 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode4)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:4, Accepted)
    OF ?jobe:TraFaultCode5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode5, Accepted)
      if jobe:trafaultcode5 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 5
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 5
              tfo:Field         = jobe:TraFaultCode5
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 5      |
                  Or tfo:Field         <> jobe:TraFaultCode5
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:5)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode5, Accepted)
    OF ?PopCalendar:17
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode5 = TINCALENDARStyle1(jobe:TraFaultCode5)
          Display(?JOBE:TraFaultCode5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:5
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:5, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode5 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 5
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 5
              tfo:Field         = jobe:TraFaultCode5
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 5      |
                  Or tfo:Field         <> jobe:TraFaultCode5
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:5)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode5)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:5, Accepted)
    OF ?jobe:TraFaultCode6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode6, Accepted)
      if jobe:trafaultcode6 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 6
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 6
              tfo:Field         = jobe:TraFaultCode6
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 6      |
                  Or tfo:Field         <> jobe:TraFaultCode6
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:6)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode6, Accepted)
    OF ?PopCalendar:18
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode6 = TINCALENDARStyle1(jobe:TraFaultCode6)
          Display(?JOBE:TraFaultCode6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:2, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode2 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 2
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 2
              tfo:Field         = jobe:TraFaultCode2
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 2      |
                  Or tfo:Field         <> jobe:TraFaultCode2
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:2)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
      !        jobe:TraFaultCode2 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode2)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:2, Accepted)
    OF ?tralookup:3
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:3, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode3 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 3
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 3
              tfo:Field         = jobe:TraFaultCode3
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 3      |
                  Or tfo:Field         <> jobe:TraFaultCode3
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:3)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode3)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:3, Accepted)
    OF ?jobe:TraFaultCode7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode7, Accepted)
      if jobe:trafaultcode7 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 7
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 7
              tfo:Field         = jobe:TraFaultCode7
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 7      |
                  Or tfo:Field         <> jobe:TraFaultCode7
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:7)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode7, Accepted)
    OF ?PopCalendar:19
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode7 = TINCALENDARStyle1(jobe:TraFaultCode7)
          Display(?JOBE:TraFaultCode7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:7
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:7, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode7 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 7
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 7
              tfo:Field         = jobe:TraFaultCode7
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 7      |
                  Or tfo:Field         <> jobe:TraFaultCode7
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:7)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
      !        jobe:TraFaultCode7 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode7)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:7, Accepted)
    OF ?jobe:TraFaultCode8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode8, Accepted)
      if jobe:trafaultcode8 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 8
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 8
              tfo:Field         = jobe:TraFaultCode8
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 8      |
                  Or tfo:Field         <> jobe:TraFaultCode8
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:8)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode8, Accepted)
    OF ?PopCalendar:20
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode8 = TINCALENDARStyle1(jobe:TraFaultCode8)
          Display(?JOBE:TraFaultCode8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:8
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:8, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode8 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 8
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 8
              tfo:Field         = jobe:TraFaultCode8
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 8      |
                  Or tfo:Field         <> jobe:TraFaultCode8
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:8)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
      !        jobe:TraFaultCode8 = ''
      !        jobe:TraFaultCode9 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode8)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:8, Accepted)
    OF ?jobe:TraFaultCode9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode9, Accepted)
      if jobe:trafaultcode9 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 9
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 9
              tfo:Field         = jobe:TraFaultCode9
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 9      |
                  Or tfo:Field         <> jobe:TraFaultCode9
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:9)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode9, Accepted)
    OF ?PopCalendar:21
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Fault_Code9 = TINCALENDARStyle1(job:Fault_Code9)
          Display(?JOB:Fault_Code9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?jobe:TraFaultCode10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode10, Accepted)
      if jobe:trafaultcode10 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 10
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 10
              tfo:Field         = jobe:TraFaultCode10
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 10      |
                  Or tfo:Field         <> jobe:TraFaultCode10
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:10)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode10, Accepted)
    OF ?PopCalendar:22
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode10 = TINCALENDARStyle1(jobe:TraFaultCode10)
          Display(?JOBE:TraFaultCode10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?jobe:TraFaultCode11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode11, Accepted)
      if jobe:trafaultcode11 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 11
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 11
              tfo:Field         = jobe:TraFaultCode11
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 11      |
                  Or tfo:Field         <> jobe:TraFaultCode11
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:11)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode11, Accepted)
    OF ?PopCalendar:23
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode11 = TINCALENDARStyle1(jobe:TraFaultCode11)
          Display(?JOBE:TraFaultCode11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?jobe:TraFaultCode12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode12, Accepted)
      if jobe:trafaultcode12 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = job:account_number
          taf:field_number = 12
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 12
              tfo:Field         = jobe:TraFaultCode12
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 12      |
                  Or tfo:Field         <> jobe:TraFaultCode12
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:12)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jobe:TraFaultCode12, Accepted)
    OF ?PopCalendar:24
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jobe:TraFaultCode12 = TINCALENDARStyle1(jobe:TraFaultCode12)
          Display(?JOBE:TraFaultCode12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:9
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:9, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode9 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 9
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 9
              tfo:Field         = jobe:TraFaultCode9
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 9      |
                  Or tfo:Field         <> jobe:TraFaultCode9
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:9)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
      !        jobe:TraFaultCode9 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode9)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:9, Accepted)
    OF ?tralookup:11
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:11, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode11 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 11
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 11
              tfo:Field         = jobe:TraFaultCode11
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 11      |
                  Or tfo:Field         <> jobe:TraFaultCode11
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:1)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
      !        jobe:TraFaultCode1 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode11)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:11, Accepted)
    OF ?tralookup:6
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:6, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode6 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 6
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 6
              tfo:Field         = jobe:TraFaultCode6
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 6      |
                  Or tfo:Field         <> jobe:TraFaultCode6
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:6)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
      !        jobe:TraFaultCode6 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode6)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:6, Accepted)
    OF ?tralookup:10
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:10, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode10 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 10
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 10
              tfo:Field         = jobe:TraFaultCode10
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 10      |
                  Or tfo:Field         <> jobe:TraFaultCode10
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:10)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
      !        jobe:TraFaultCode10 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode10)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:10, Accepted)
    OF ?tralookup:12
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:12, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              jobe:TraFaultCode12 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = job:Account_Number
              taf:Field_Number  = 12
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = job:Account_Number
              tfo:Field_Number  = 12
              tfo:Field         = jobe:TraFaultCode12
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> job:Account_Number      |
                  Or tfo:Field_Number  <> 12      |
                  Or tfo:Field         <> jobe:TraFaultCode12
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:12)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> job:Account_Number      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> job:Account_Number      |
      
      
              Select(?+2)
          Of Requestcancelled
      !        jobe:TraFaultCode12 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:TraFaultCode12)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:12, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      continue# = 1
      If job:date_completed <> ''
          continue# = 0
          If ?job:fault_code1{prop:hide} = 0 and job:fault_code1 = ''
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 1 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?job:fault_code1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code2{prop:hide} = 0 and job:fault_code2 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 2 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_code1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code3{prop:hide} = 0 and job:fault_code3 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 3 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_ode1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code4{prop:hide} = 0 and job:fault_code4 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 4 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_code1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code5{prop:hide} = 0 and job:fault_code5 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 5 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_code1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code6{prop:hide} = 0 and job:fault_code6 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 6 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_code1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code7{prop:hide} = 0 and job:fault_code7 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 7 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_ode1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code8{prop:hide} = 0 and job:fault_code8 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 8 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_code1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code9{prop:hide} = 0 and job:fault_code9 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 9 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_code1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code10{prop:hide} = 0 and job:fault_code10 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 10 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_code1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code11{prop:hide} = 0 and job:fault_code11 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 11 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_ode1{prop:hide} = 0 and job:fault_code1 = ''
          If ?job:fault_code12{prop:hide} = 0 and job:fault_code12 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 12 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_code1{prop:hide} = 0 and job:fault_code1 = ''
          If error# = 0
              continue# = 1
          End!If error# = 0
      End!If job:date_completed <> ''
      If continue# = 1
          access:jobse.update()
          Post(event:closewindow)
      End
       
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Fault_Codes_Window')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?jobe:FaultCode13
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:25)
      CYCLE
    END
  OF ?jobe:FaultCode14
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:26)
      CYCLE
    END
  OF ?jobe:FaultCode15
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:27)
      CYCLE
    END
  OF ?jobe:FaultCode16
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:28)
      CYCLE
    END
  OF ?jobe:FaultCode17
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:29)
      CYCLE
    END
  OF ?jobe:FaultCode18
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:30)
      CYCLE
    END
  OF ?jobe:FaultCode19
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:31)
      CYCLE
    END
  OF ?jobe:FaultCode20
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:32)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?job:Fault_Code1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code1, AlertKey)
      If ?lookup{prop:hide} = 0
          Post(Event:Accepted,?Lookup)
      End!If ?lookup{prop:hide} = 0
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code1, AlertKey)
    END
  OF ?job:Fault_Code2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code2, AlertKey)
      If ?Lookup:2{prop:hide} = 0
          Post(Event:Accepted,?Lookup:2)
      End
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code2, AlertKey)
    END
  OF ?job:Fault_Code3
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code3, AlertKey)
      If ?Lookup:3{prop:hide} = 0
          Post(Event:Accepted,?Lookup:3)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code3, AlertKey)
    END
  OF ?job:Fault_Code4
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code4, AlertKey)
      If ?Lookup:4{prop:hide} = 0
          Post(Event:Accepted,?Lookup:4)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code4, AlertKey)
    END
  OF ?job:Fault_Code5
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code5, AlertKey)
      If ?Lookup:5{prop:hide} = 0
          Post(Event:Accepted,?Lookup:5)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code5, AlertKey)
    END
  OF ?job:Fault_Code6
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code6, AlertKey)
      If ?Lookup:6{prop:hide} = 0
          Post(Event:Accepted,?Lookup:6)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code6, AlertKey)
    END
  OF ?job:Fault_Code7
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code7, AlertKey)
      If ?Lookup:7{prop:hide} = 0
          Post(Event:Accepted,?Lookup:7)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code7, AlertKey)
    END
  OF ?job:Fault_Code8
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code8, AlertKey)
      If ?Lookup:8{prop:hide} = 0
          Post(Event:Accepted,?Lookup:8)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code8, AlertKey)
    END
  OF ?job:Fault_Code9
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code9, AlertKey)
      If ?Lookup:9{prop:hide} = 0
          Post(Event:Accepted,?Lookup:9)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code9, AlertKey)
    END
  OF ?job:Fault_Code10
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code10, AlertKey)
      If ?Lookup:10{prop:hide} = 0
          Post(Event:Accepted,?Lookup:10)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code10, AlertKey)
    END
  OF ?job:Fault_Code11
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code11, AlertKey)
      If ?Lookup:11{prop:hide} = 0
          Post(Event:Accepted,?Lookup:11)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code11, AlertKey)
    END
  OF ?job:Fault_Code12
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code12, AlertKey)
      If ?Lookup:12{prop:hide} = 0
          Post(Event:Accepted,?Lookup:12)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fault_Code12, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      access:subtracc.tryfetch(sub:account_number_key)
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.tryfetch(tra:account_number_key)
      access:jobse.clearkey(jobe:refnumberkey)
      jobe:refnumber  = job:Ref_number
      If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
          !Found
      Else! If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
          !Error
          If access:jobse.primerecord() = Level:Benign
              jobe:refnumber  = job:ref_number
              If access:jobse.tryinsert()
                  access:jobse.cancelautoinc()
              End!If access:jobse.tryinsert()
          End!If access:jobse.primerecord() = Level:Benign
      End! If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
      
      
      Do Fault_Coding
      Select(?+1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

