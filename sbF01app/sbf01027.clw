

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01027.INC'),ONCE        !Local module procedure declarations
                     END


UpdateCOMMONWP PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
save_cwp_ali_id      USHORT,AUTO
adjustment_temp      STRING(3)
quantity_temp        LONG
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?cwp:Supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
History::cwp:Record  LIKE(cwp:RECORD),STATIC
QuickWindow          WINDOW('Temporary Chargeable Part'),AT(,,289,247),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Warranty_Part'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,284,212),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Warranty Part'),AT(8,20),USE(?Prompt8),FONT('Tahoma',12,COLOR:Navy,FONT:bold)
                           PROMPT('Part Number'),AT(8,36),USE(?cwp:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(cwp:Part_Number),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           BUTTON('Browse Stock'),AT(212,36,68,10),USE(?browse_stock_button),SKIP,LEFT,ICON('book.ico')
                           PROMPT('Description'),AT(8,52),USE(?cwp:Description:Prompt),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(cwp:Description),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Purchase Cost'),AT(8,68),USE(?CWP:Purchase_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,68,64,10),USE(cwp:Purchase_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Sale Cost'),AT(8,84),USE(?CWP:Sale_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,84,64,10),USE(cwp:Sale_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Supplier'),AT(8,100),USE(?Prompt19)
                           COMBO(@s30),AT(84,100,124,10),USE(cwp:Supplier),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Quantity'),AT(8,116),USE(?Prompt7)
                           SPIN(@s8),AT(84,116,64,10),USE(cwp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold),REQ,RANGE(1,99999999),STEP(1)
                           CHECK('Exclude From Order'),AT(84,132),USE(cwp:Exclude_From_Order),VALUE('YES','NO')
                         END
                         TAB('Fault Coding'),USE(?Fault_Code_Tab),HIDE
                           BUTTON,AT(228,24,10,10),USE(?PopCalendar),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 1:'),AT(8,24),USE(?cwp:Fault_Code1:Prompt),HIDE
                           ENTRY(@s30),AT(84,24,124,10),USE(cwp:Fault_Code1),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,24,124,10),USE(cwp:Fault_Code1,,?cwp:Fault_Code1:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 2:'),AT(8,40),USE(?cwp:Fault_Code2:Prompt),HIDE
                           ENTRY(@s30),AT(84,40,124,10),USE(cwp:Fault_Code2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,40,10,10),USE(?Button4:2),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,40,10,10),USE(?PopCalendar:2),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,24,10,10),USE(?Button4),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,40,124,10),USE(cwp:Fault_Code2,,?cwp:Fault_Code2:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 3:'),AT(8,56),USE(?cwp:Fault_Code3:Prompt),HIDE
                           ENTRY(@s30),AT(84,56,124,10),USE(cwp:Fault_Code3),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,56,10,10),USE(?Button4:3),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,56,10,10),USE(?PopCalendar:3),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,72,10,10),USE(?Button4:4),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,56,124,10),USE(cwp:Fault_Code3,,?cwp:Fault_Code3:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 4:'),AT(8,72),USE(?cwp:Fault_Code4:Prompt),HIDE
                           ENTRY(@s30),AT(84,72,124,10),USE(cwp:Fault_Code4),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,88,10,10),USE(?Button4:5),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,72,10,10),USE(?PopCalendar:4),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,104,10,10),USE(?Button4:6),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,88,10,10),USE(?PopCalendar:5),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,120,10,10),USE(?Button4:7),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 7:'),AT(8,120),USE(?cwp:Fault_Code7:Prompt),HIDE
                           ENTRY(@s30),AT(84,120,124,10),USE(cwp:Fault_Code7),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,120,10,10),USE(?PopCalendar:7),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,136,10,10),USE(?Button4:8),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,120,124,10),USE(cwp:Fault_Code7,,?cwp:Fault_Code7:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 8:'),AT(8,136),USE(?cwp:Fault_Code8:Prompt),HIDE
                           ENTRY(@s30),AT(84,136,124,10),USE(cwp:Fault_Code8),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,136,10,10),USE(?PopCalendar:8),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(228,104,10,10),USE(?PopCalendar:6),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,152,10,10),USE(?Button4:9),HIDE,ICON('list3.ico')
                           BUTTON,AT(212,168,10,10),USE(?Button4:10),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,136,124,10),USE(cwp:Fault_Code8,,?cwp:Fault_Code8:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 9:'),AT(8,152),USE(?cwp:Fault_Code9:Prompt),HIDE
                           ENTRY(@s30),AT(84,152,124,10),USE(cwp:Fault_Code9),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,152,124,10),USE(cwp:Fault_Code9,,?cwp:Fault_Code9:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 10:'),AT(8,168),USE(?cwp:Fault_Code10:Prompt),HIDE
                           ENTRY(@s30),AT(84,168,124,10),USE(cwp:Fault_Code10),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,152,10,10),USE(?PopCalendar:9),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 11:'),AT(8,184),USE(?cwp:Fault_Code11:Prompt),HIDE
                           ENTRY(@s30),AT(84,184,124,10),USE(cwp:Fault_Code11),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,168,10,10),USE(?PopCalendar:10),HIDE,ICON('Calenda2.ico')
                           ENTRY(@d6b),AT(52,168,124,10),USE(cwp:Fault_Code10,,?cwp:Fault_Code10:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 12:'),AT(8,200),USE(?cwp:Fault_Code12:Prompt),HIDE
                           ENTRY(@s30),AT(84,200,124,10),USE(cwp:Fault_Code12),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,184,10,10),USE(?PopCalendar:11),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,184,10,10),USE(?Button4:11),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,200,10,10),USE(?PopCalendar:12),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,200,10,10),USE(?Button4:12),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,184,124,10),USE(cwp:Fault_Code11,,?cwp:Fault_Code11:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,200,124,10),USE(cwp:Fault_Code12,,?cwp:Fault_Code12:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,72,124,10),USE(cwp:Fault_Code4,,?cwp:Fault_Code4:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 5:'),AT(8,88),USE(?cwp:Fault_Code5:Prompt),HIDE
                           ENTRY(@s30),AT(84,88,124,10),USE(cwp:Fault_Code5),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,88,124,10),USE(cwp:Fault_Code5,,?cwp:Fault_Code5:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 6:'),AT(8,104),USE(?cwp:Fault_Code6:Prompt),HIDE
                           ENTRY(@s30),AT(84,104,124,10),USE(cwp:Fault_Code6),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,104,124,10),USE(cwp:Fault_Code6,,?cwp:Fault_Code6:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       PANEL,AT(4,220,284,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(172,224,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(228,224,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_map_id   ushort,auto
save_war_ali_id     ushort,auto
save_partmp_ali_id     ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?cwp:Part_Number:Prompt{prop:FontColor} = -1
    ?cwp:Part_Number:Prompt{prop:Color} = 15066597
    If ?cwp:Part_Number{prop:ReadOnly} = True
        ?cwp:Part_Number{prop:FontColor} = 65793
        ?cwp:Part_Number{prop:Color} = 15066597
    Elsif ?cwp:Part_Number{prop:Req} = True
        ?cwp:Part_Number{prop:FontColor} = 65793
        ?cwp:Part_Number{prop:Color} = 8454143
    Else ! If ?cwp:Part_Number{prop:Req} = True
        ?cwp:Part_Number{prop:FontColor} = 65793
        ?cwp:Part_Number{prop:Color} = 16777215
    End ! If ?cwp:Part_Number{prop:Req} = True
    ?cwp:Part_Number{prop:Trn} = 0
    ?cwp:Part_Number{prop:FontStyle} = font:Bold
    ?cwp:Description:Prompt{prop:FontColor} = -1
    ?cwp:Description:Prompt{prop:Color} = 15066597
    If ?cwp:Description{prop:ReadOnly} = True
        ?cwp:Description{prop:FontColor} = 65793
        ?cwp:Description{prop:Color} = 15066597
    Elsif ?cwp:Description{prop:Req} = True
        ?cwp:Description{prop:FontColor} = 65793
        ?cwp:Description{prop:Color} = 8454143
    Else ! If ?cwp:Description{prop:Req} = True
        ?cwp:Description{prop:FontColor} = 65793
        ?cwp:Description{prop:Color} = 16777215
    End ! If ?cwp:Description{prop:Req} = True
    ?cwp:Description{prop:Trn} = 0
    ?cwp:Description{prop:FontStyle} = font:Bold
    ?CWP:Purchase_Cost:Prompt{prop:FontColor} = -1
    ?CWP:Purchase_Cost:Prompt{prop:Color} = 15066597
    If ?cwp:Purchase_Cost{prop:ReadOnly} = True
        ?cwp:Purchase_Cost{prop:FontColor} = 65793
        ?cwp:Purchase_Cost{prop:Color} = 15066597
    Elsif ?cwp:Purchase_Cost{prop:Req} = True
        ?cwp:Purchase_Cost{prop:FontColor} = 65793
        ?cwp:Purchase_Cost{prop:Color} = 8454143
    Else ! If ?cwp:Purchase_Cost{prop:Req} = True
        ?cwp:Purchase_Cost{prop:FontColor} = 65793
        ?cwp:Purchase_Cost{prop:Color} = 16777215
    End ! If ?cwp:Purchase_Cost{prop:Req} = True
    ?cwp:Purchase_Cost{prop:Trn} = 0
    ?cwp:Purchase_Cost{prop:FontStyle} = font:Bold
    ?CWP:Sale_Cost:Prompt{prop:FontColor} = -1
    ?CWP:Sale_Cost:Prompt{prop:Color} = 15066597
    If ?cwp:Sale_Cost{prop:ReadOnly} = True
        ?cwp:Sale_Cost{prop:FontColor} = 65793
        ?cwp:Sale_Cost{prop:Color} = 15066597
    Elsif ?cwp:Sale_Cost{prop:Req} = True
        ?cwp:Sale_Cost{prop:FontColor} = 65793
        ?cwp:Sale_Cost{prop:Color} = 8454143
    Else ! If ?cwp:Sale_Cost{prop:Req} = True
        ?cwp:Sale_Cost{prop:FontColor} = 65793
        ?cwp:Sale_Cost{prop:Color} = 16777215
    End ! If ?cwp:Sale_Cost{prop:Req} = True
    ?cwp:Sale_Cost{prop:Trn} = 0
    ?cwp:Sale_Cost{prop:FontStyle} = font:Bold
    ?Prompt19{prop:FontColor} = -1
    ?Prompt19{prop:Color} = 15066597
    If ?cwp:Supplier{prop:ReadOnly} = True
        ?cwp:Supplier{prop:FontColor} = 65793
        ?cwp:Supplier{prop:Color} = 15066597
    Elsif ?cwp:Supplier{prop:Req} = True
        ?cwp:Supplier{prop:FontColor} = 65793
        ?cwp:Supplier{prop:Color} = 8454143
    Else ! If ?cwp:Supplier{prop:Req} = True
        ?cwp:Supplier{prop:FontColor} = 65793
        ?cwp:Supplier{prop:Color} = 16777215
    End ! If ?cwp:Supplier{prop:Req} = True
    ?cwp:Supplier{prop:Trn} = 0
    ?cwp:Supplier{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?cwp:Quantity{prop:ReadOnly} = True
        ?cwp:Quantity{prop:FontColor} = 65793
        ?cwp:Quantity{prop:Color} = 15066597
    Elsif ?cwp:Quantity{prop:Req} = True
        ?cwp:Quantity{prop:FontColor} = 65793
        ?cwp:Quantity{prop:Color} = 8454143
    Else ! If ?cwp:Quantity{prop:Req} = True
        ?cwp:Quantity{prop:FontColor} = 65793
        ?cwp:Quantity{prop:Color} = 16777215
    End ! If ?cwp:Quantity{prop:Req} = True
    ?cwp:Quantity{prop:Trn} = 0
    ?cwp:Quantity{prop:FontStyle} = font:Bold
    ?cwp:Exclude_From_Order{prop:Font,3} = -1
    ?cwp:Exclude_From_Order{prop:Color} = 15066597
    ?cwp:Exclude_From_Order{prop:Trn} = 0
    ?Fault_Code_Tab{prop:Color} = 15066597
    ?cwp:Fault_Code1:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code1:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code1{prop:ReadOnly} = True
        ?cwp:Fault_Code1{prop:FontColor} = 65793
        ?cwp:Fault_Code1{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code1{prop:Req} = True
        ?cwp:Fault_Code1{prop:FontColor} = 65793
        ?cwp:Fault_Code1{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code1{prop:Req} = True
        ?cwp:Fault_Code1{prop:FontColor} = 65793
        ?cwp:Fault_Code1{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code1{prop:Req} = True
    ?cwp:Fault_Code1{prop:Trn} = 0
    ?cwp:Fault_Code1{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code1:2{prop:ReadOnly} = True
        ?cwp:Fault_Code1:2{prop:FontColor} = 65793
        ?cwp:Fault_Code1:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code1:2{prop:Req} = True
        ?cwp:Fault_Code1:2{prop:FontColor} = 65793
        ?cwp:Fault_Code1:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code1:2{prop:Req} = True
        ?cwp:Fault_Code1:2{prop:FontColor} = 65793
        ?cwp:Fault_Code1:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code1:2{prop:Req} = True
    ?cwp:Fault_Code1:2{prop:Trn} = 0
    ?cwp:Fault_Code1:2{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code2:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code2:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code2{prop:ReadOnly} = True
        ?cwp:Fault_Code2{prop:FontColor} = 65793
        ?cwp:Fault_Code2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code2{prop:Req} = True
        ?cwp:Fault_Code2{prop:FontColor} = 65793
        ?cwp:Fault_Code2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code2{prop:Req} = True
        ?cwp:Fault_Code2{prop:FontColor} = 65793
        ?cwp:Fault_Code2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code2{prop:Req} = True
    ?cwp:Fault_Code2{prop:Trn} = 0
    ?cwp:Fault_Code2{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code2:2{prop:ReadOnly} = True
        ?cwp:Fault_Code2:2{prop:FontColor} = 65793
        ?cwp:Fault_Code2:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code2:2{prop:Req} = True
        ?cwp:Fault_Code2:2{prop:FontColor} = 65793
        ?cwp:Fault_Code2:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code2:2{prop:Req} = True
        ?cwp:Fault_Code2:2{prop:FontColor} = 65793
        ?cwp:Fault_Code2:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code2:2{prop:Req} = True
    ?cwp:Fault_Code2:2{prop:Trn} = 0
    ?cwp:Fault_Code2:2{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code3:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code3:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code3{prop:ReadOnly} = True
        ?cwp:Fault_Code3{prop:FontColor} = 65793
        ?cwp:Fault_Code3{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code3{prop:Req} = True
        ?cwp:Fault_Code3{prop:FontColor} = 65793
        ?cwp:Fault_Code3{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code3{prop:Req} = True
        ?cwp:Fault_Code3{prop:FontColor} = 65793
        ?cwp:Fault_Code3{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code3{prop:Req} = True
    ?cwp:Fault_Code3{prop:Trn} = 0
    ?cwp:Fault_Code3{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code3:2{prop:ReadOnly} = True
        ?cwp:Fault_Code3:2{prop:FontColor} = 65793
        ?cwp:Fault_Code3:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code3:2{prop:Req} = True
        ?cwp:Fault_Code3:2{prop:FontColor} = 65793
        ?cwp:Fault_Code3:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code3:2{prop:Req} = True
        ?cwp:Fault_Code3:2{prop:FontColor} = 65793
        ?cwp:Fault_Code3:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code3:2{prop:Req} = True
    ?cwp:Fault_Code3:2{prop:Trn} = 0
    ?cwp:Fault_Code3:2{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code4:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code4:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code4{prop:ReadOnly} = True
        ?cwp:Fault_Code4{prop:FontColor} = 65793
        ?cwp:Fault_Code4{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code4{prop:Req} = True
        ?cwp:Fault_Code4{prop:FontColor} = 65793
        ?cwp:Fault_Code4{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code4{prop:Req} = True
        ?cwp:Fault_Code4{prop:FontColor} = 65793
        ?cwp:Fault_Code4{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code4{prop:Req} = True
    ?cwp:Fault_Code4{prop:Trn} = 0
    ?cwp:Fault_Code4{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code7:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code7:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code7{prop:ReadOnly} = True
        ?cwp:Fault_Code7{prop:FontColor} = 65793
        ?cwp:Fault_Code7{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code7{prop:Req} = True
        ?cwp:Fault_Code7{prop:FontColor} = 65793
        ?cwp:Fault_Code7{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code7{prop:Req} = True
        ?cwp:Fault_Code7{prop:FontColor} = 65793
        ?cwp:Fault_Code7{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code7{prop:Req} = True
    ?cwp:Fault_Code7{prop:Trn} = 0
    ?cwp:Fault_Code7{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code7:2{prop:ReadOnly} = True
        ?cwp:Fault_Code7:2{prop:FontColor} = 65793
        ?cwp:Fault_Code7:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code7:2{prop:Req} = True
        ?cwp:Fault_Code7:2{prop:FontColor} = 65793
        ?cwp:Fault_Code7:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code7:2{prop:Req} = True
        ?cwp:Fault_Code7:2{prop:FontColor} = 65793
        ?cwp:Fault_Code7:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code7:2{prop:Req} = True
    ?cwp:Fault_Code7:2{prop:Trn} = 0
    ?cwp:Fault_Code7:2{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code8:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code8:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code8{prop:ReadOnly} = True
        ?cwp:Fault_Code8{prop:FontColor} = 65793
        ?cwp:Fault_Code8{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code8{prop:Req} = True
        ?cwp:Fault_Code8{prop:FontColor} = 65793
        ?cwp:Fault_Code8{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code8{prop:Req} = True
        ?cwp:Fault_Code8{prop:FontColor} = 65793
        ?cwp:Fault_Code8{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code8{prop:Req} = True
    ?cwp:Fault_Code8{prop:Trn} = 0
    ?cwp:Fault_Code8{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code8:2{prop:ReadOnly} = True
        ?cwp:Fault_Code8:2{prop:FontColor} = 65793
        ?cwp:Fault_Code8:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code8:2{prop:Req} = True
        ?cwp:Fault_Code8:2{prop:FontColor} = 65793
        ?cwp:Fault_Code8:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code8:2{prop:Req} = True
        ?cwp:Fault_Code8:2{prop:FontColor} = 65793
        ?cwp:Fault_Code8:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code8:2{prop:Req} = True
    ?cwp:Fault_Code8:2{prop:Trn} = 0
    ?cwp:Fault_Code8:2{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code9:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code9:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code9{prop:ReadOnly} = True
        ?cwp:Fault_Code9{prop:FontColor} = 65793
        ?cwp:Fault_Code9{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code9{prop:Req} = True
        ?cwp:Fault_Code9{prop:FontColor} = 65793
        ?cwp:Fault_Code9{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code9{prop:Req} = True
        ?cwp:Fault_Code9{prop:FontColor} = 65793
        ?cwp:Fault_Code9{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code9{prop:Req} = True
    ?cwp:Fault_Code9{prop:Trn} = 0
    ?cwp:Fault_Code9{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code9:2{prop:ReadOnly} = True
        ?cwp:Fault_Code9:2{prop:FontColor} = 65793
        ?cwp:Fault_Code9:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code9:2{prop:Req} = True
        ?cwp:Fault_Code9:2{prop:FontColor} = 65793
        ?cwp:Fault_Code9:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code9:2{prop:Req} = True
        ?cwp:Fault_Code9:2{prop:FontColor} = 65793
        ?cwp:Fault_Code9:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code9:2{prop:Req} = True
    ?cwp:Fault_Code9:2{prop:Trn} = 0
    ?cwp:Fault_Code9:2{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code10:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code10:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code10{prop:ReadOnly} = True
        ?cwp:Fault_Code10{prop:FontColor} = 65793
        ?cwp:Fault_Code10{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code10{prop:Req} = True
        ?cwp:Fault_Code10{prop:FontColor} = 65793
        ?cwp:Fault_Code10{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code10{prop:Req} = True
        ?cwp:Fault_Code10{prop:FontColor} = 65793
        ?cwp:Fault_Code10{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code10{prop:Req} = True
    ?cwp:Fault_Code10{prop:Trn} = 0
    ?cwp:Fault_Code10{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code11:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code11:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code11{prop:ReadOnly} = True
        ?cwp:Fault_Code11{prop:FontColor} = 65793
        ?cwp:Fault_Code11{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code11{prop:Req} = True
        ?cwp:Fault_Code11{prop:FontColor} = 65793
        ?cwp:Fault_Code11{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code11{prop:Req} = True
        ?cwp:Fault_Code11{prop:FontColor} = 65793
        ?cwp:Fault_Code11{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code11{prop:Req} = True
    ?cwp:Fault_Code11{prop:Trn} = 0
    ?cwp:Fault_Code11{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code10:2{prop:ReadOnly} = True
        ?cwp:Fault_Code10:2{prop:FontColor} = 65793
        ?cwp:Fault_Code10:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code10:2{prop:Req} = True
        ?cwp:Fault_Code10:2{prop:FontColor} = 65793
        ?cwp:Fault_Code10:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code10:2{prop:Req} = True
        ?cwp:Fault_Code10:2{prop:FontColor} = 65793
        ?cwp:Fault_Code10:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code10:2{prop:Req} = True
    ?cwp:Fault_Code10:2{prop:Trn} = 0
    ?cwp:Fault_Code10:2{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code12:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code12:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code12{prop:ReadOnly} = True
        ?cwp:Fault_Code12{prop:FontColor} = 65793
        ?cwp:Fault_Code12{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code12{prop:Req} = True
        ?cwp:Fault_Code12{prop:FontColor} = 65793
        ?cwp:Fault_Code12{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code12{prop:Req} = True
        ?cwp:Fault_Code12{prop:FontColor} = 65793
        ?cwp:Fault_Code12{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code12{prop:Req} = True
    ?cwp:Fault_Code12{prop:Trn} = 0
    ?cwp:Fault_Code12{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code11:2{prop:ReadOnly} = True
        ?cwp:Fault_Code11:2{prop:FontColor} = 65793
        ?cwp:Fault_Code11:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code11:2{prop:Req} = True
        ?cwp:Fault_Code11:2{prop:FontColor} = 65793
        ?cwp:Fault_Code11:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code11:2{prop:Req} = True
        ?cwp:Fault_Code11:2{prop:FontColor} = 65793
        ?cwp:Fault_Code11:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code11:2{prop:Req} = True
    ?cwp:Fault_Code11:2{prop:Trn} = 0
    ?cwp:Fault_Code11:2{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code12:2{prop:ReadOnly} = True
        ?cwp:Fault_Code12:2{prop:FontColor} = 65793
        ?cwp:Fault_Code12:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code12:2{prop:Req} = True
        ?cwp:Fault_Code12:2{prop:FontColor} = 65793
        ?cwp:Fault_Code12:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code12:2{prop:Req} = True
        ?cwp:Fault_Code12:2{prop:FontColor} = 65793
        ?cwp:Fault_Code12:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code12:2{prop:Req} = True
    ?cwp:Fault_Code12:2{prop:Trn} = 0
    ?cwp:Fault_Code12:2{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code4:2{prop:ReadOnly} = True
        ?cwp:Fault_Code4:2{prop:FontColor} = 65793
        ?cwp:Fault_Code4:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code4:2{prop:Req} = True
        ?cwp:Fault_Code4:2{prop:FontColor} = 65793
        ?cwp:Fault_Code4:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code4:2{prop:Req} = True
        ?cwp:Fault_Code4:2{prop:FontColor} = 65793
        ?cwp:Fault_Code4:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code4:2{prop:Req} = True
    ?cwp:Fault_Code4:2{prop:Trn} = 0
    ?cwp:Fault_Code4:2{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code5:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code5:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code5{prop:ReadOnly} = True
        ?cwp:Fault_Code5{prop:FontColor} = 65793
        ?cwp:Fault_Code5{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code5{prop:Req} = True
        ?cwp:Fault_Code5{prop:FontColor} = 65793
        ?cwp:Fault_Code5{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code5{prop:Req} = True
        ?cwp:Fault_Code5{prop:FontColor} = 65793
        ?cwp:Fault_Code5{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code5{prop:Req} = True
    ?cwp:Fault_Code5{prop:Trn} = 0
    ?cwp:Fault_Code5{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code5:2{prop:ReadOnly} = True
        ?cwp:Fault_Code5:2{prop:FontColor} = 65793
        ?cwp:Fault_Code5:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code5:2{prop:Req} = True
        ?cwp:Fault_Code5:2{prop:FontColor} = 65793
        ?cwp:Fault_Code5:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code5:2{prop:Req} = True
        ?cwp:Fault_Code5:2{prop:FontColor} = 65793
        ?cwp:Fault_Code5:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code5:2{prop:Req} = True
    ?cwp:Fault_Code5:2{prop:Trn} = 0
    ?cwp:Fault_Code5:2{prop:FontStyle} = font:Bold
    ?cwp:Fault_Code6:Prompt{prop:FontColor} = -1
    ?cwp:Fault_Code6:Prompt{prop:Color} = 15066597
    If ?cwp:Fault_Code6{prop:ReadOnly} = True
        ?cwp:Fault_Code6{prop:FontColor} = 65793
        ?cwp:Fault_Code6{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code6{prop:Req} = True
        ?cwp:Fault_Code6{prop:FontColor} = 65793
        ?cwp:Fault_Code6{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code6{prop:Req} = True
        ?cwp:Fault_Code6{prop:FontColor} = 65793
        ?cwp:Fault_Code6{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code6{prop:Req} = True
    ?cwp:Fault_Code6{prop:Trn} = 0
    ?cwp:Fault_Code6{prop:FontStyle} = font:Bold
    If ?cwp:Fault_Code6:2{prop:ReadOnly} = True
        ?cwp:Fault_Code6:2{prop:FontColor} = 65793
        ?cwp:Fault_Code6:2{prop:Color} = 15066597
    Elsif ?cwp:Fault_Code6:2{prop:Req} = True
        ?cwp:Fault_Code6:2{prop:FontColor} = 65793
        ?cwp:Fault_Code6:2{prop:Color} = 8454143
    Else ! If ?cwp:Fault_Code6:2{prop:Req} = True
        ?cwp:Fault_Code6:2{prop:FontColor} = 65793
        ?cwp:Fault_Code6:2{prop:Color} = 16777215
    End ! If ?cwp:Fault_Code6:2{prop:Req} = True
    ?cwp:Fault_Code6:2{prop:Trn} = 0
    ?cwp:Fault_Code6:2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateCOMMONWP',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateCOMMONWP',1)
    SolaceViewVars('save_cwp_ali_id',save_cwp_ali_id,'UpdateCOMMONWP',1)
    SolaceViewVars('adjustment_temp',adjustment_temp,'UpdateCOMMONWP',1)
    SolaceViewVars('quantity_temp',quantity_temp,'UpdateCOMMONWP',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateCOMMONWP',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateCOMMONWP',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateCOMMONWP',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateCOMMONWP',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'UpdateCOMMONWP',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Part_Number:Prompt;  SolaceCtrlName = '?cwp:Part_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Part_Number;  SolaceCtrlName = '?cwp:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?browse_stock_button;  SolaceCtrlName = '?browse_stock_button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Description:Prompt;  SolaceCtrlName = '?cwp:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Description;  SolaceCtrlName = '?cwp:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CWP:Purchase_Cost:Prompt;  SolaceCtrlName = '?CWP:Purchase_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Purchase_Cost;  SolaceCtrlName = '?cwp:Purchase_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CWP:Sale_Cost:Prompt;  SolaceCtrlName = '?CWP:Sale_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Sale_Cost;  SolaceCtrlName = '?cwp:Sale_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt19;  SolaceCtrlName = '?Prompt19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Supplier;  SolaceCtrlName = '?cwp:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Quantity;  SolaceCtrlName = '?cwp:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Exclude_From_Order;  SolaceCtrlName = '?cwp:Exclude_From_Order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Code_Tab;  SolaceCtrlName = '?Fault_Code_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code1:Prompt;  SolaceCtrlName = '?cwp:Fault_Code1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code1;  SolaceCtrlName = '?cwp:Fault_Code1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code1:2;  SolaceCtrlName = '?cwp:Fault_Code1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code2:Prompt;  SolaceCtrlName = '?cwp:Fault_Code2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code2;  SolaceCtrlName = '?cwp:Fault_Code2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:2;  SolaceCtrlName = '?Button4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code2:2;  SolaceCtrlName = '?cwp:Fault_Code2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code3:Prompt;  SolaceCtrlName = '?cwp:Fault_Code3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code3;  SolaceCtrlName = '?cwp:Fault_Code3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:3;  SolaceCtrlName = '?Button4:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:4;  SolaceCtrlName = '?Button4:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code3:2;  SolaceCtrlName = '?cwp:Fault_Code3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code4:Prompt;  SolaceCtrlName = '?cwp:Fault_Code4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code4;  SolaceCtrlName = '?cwp:Fault_Code4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:5;  SolaceCtrlName = '?Button4:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:6;  SolaceCtrlName = '?Button4:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:7;  SolaceCtrlName = '?Button4:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code7:Prompt;  SolaceCtrlName = '?cwp:Fault_Code7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code7;  SolaceCtrlName = '?cwp:Fault_Code7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:8;  SolaceCtrlName = '?Button4:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code7:2;  SolaceCtrlName = '?cwp:Fault_Code7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code8:Prompt;  SolaceCtrlName = '?cwp:Fault_Code8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code8;  SolaceCtrlName = '?cwp:Fault_Code8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:9;  SolaceCtrlName = '?Button4:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:10;  SolaceCtrlName = '?Button4:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code8:2;  SolaceCtrlName = '?cwp:Fault_Code8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code9:Prompt;  SolaceCtrlName = '?cwp:Fault_Code9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code9;  SolaceCtrlName = '?cwp:Fault_Code9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code9:2;  SolaceCtrlName = '?cwp:Fault_Code9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code10:Prompt;  SolaceCtrlName = '?cwp:Fault_Code10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code10;  SolaceCtrlName = '?cwp:Fault_Code10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code11:Prompt;  SolaceCtrlName = '?cwp:Fault_Code11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code11;  SolaceCtrlName = '?cwp:Fault_Code11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code10:2;  SolaceCtrlName = '?cwp:Fault_Code10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code12:Prompt;  SolaceCtrlName = '?cwp:Fault_Code12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code12;  SolaceCtrlName = '?cwp:Fault_Code12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:11;  SolaceCtrlName = '?Button4:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:12;  SolaceCtrlName = '?Button4:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code11:2;  SolaceCtrlName = '?cwp:Fault_Code11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code12:2;  SolaceCtrlName = '?cwp:Fault_Code12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code4:2;  SolaceCtrlName = '?cwp:Fault_Code4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code5:Prompt;  SolaceCtrlName = '?cwp:Fault_Code5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code5;  SolaceCtrlName = '?cwp:Fault_Code5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code5:2;  SolaceCtrlName = '?cwp:Fault_Code5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code6:Prompt;  SolaceCtrlName = '?cwp:Fault_Code6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code6;  SolaceCtrlName = '?cwp:Fault_Code6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cwp:Fault_Code6:2;  SolaceCtrlName = '?cwp:Fault_Code6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Warranty Part'
  OF ChangeRecord
    ActionMessage = 'Changing A Warranty Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateCOMMONWP')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateCOMMONWP')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt8
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(cwp:Record,History::cwp:Record)
  SELF.AddHistoryField(?cwp:Part_Number,7)
  SELF.AddHistoryField(?cwp:Description,8)
  SELF.AddHistoryField(?cwp:Purchase_Cost,10)
  SELF.AddHistoryField(?cwp:Sale_Cost,11)
  SELF.AddHistoryField(?cwp:Supplier,9)
  SELF.AddHistoryField(?cwp:Quantity,3)
  SELF.AddHistoryField(?cwp:Exclude_From_Order,6)
  SELF.AddHistoryField(?cwp:Fault_Code1,12)
  SELF.AddHistoryField(?cwp:Fault_Code1:2,12)
  SELF.AddHistoryField(?cwp:Fault_Code2,13)
  SELF.AddHistoryField(?cwp:Fault_Code2:2,13)
  SELF.AddHistoryField(?cwp:Fault_Code3,14)
  SELF.AddHistoryField(?cwp:Fault_Code3:2,14)
  SELF.AddHistoryField(?cwp:Fault_Code4,15)
  SELF.AddHistoryField(?cwp:Fault_Code7,18)
  SELF.AddHistoryField(?cwp:Fault_Code7:2,18)
  SELF.AddHistoryField(?cwp:Fault_Code8,19)
  SELF.AddHistoryField(?cwp:Fault_Code8:2,19)
  SELF.AddHistoryField(?cwp:Fault_Code9,20)
  SELF.AddHistoryField(?cwp:Fault_Code9:2,20)
  SELF.AddHistoryField(?cwp:Fault_Code10,21)
  SELF.AddHistoryField(?cwp:Fault_Code11,22)
  SELF.AddHistoryField(?cwp:Fault_Code10:2,21)
  SELF.AddHistoryField(?cwp:Fault_Code12,23)
  SELF.AddHistoryField(?cwp:Fault_Code11:2,22)
  SELF.AddHistoryField(?cwp:Fault_Code12:2,23)
  SELF.AddHistoryField(?cwp:Fault_Code4:2,15)
  SELF.AddHistoryField(?cwp:Fault_Code5,16)
  SELF.AddHistoryField(?cwp:Fault_Code5:2,16)
  SELF.AddHistoryField(?cwp:Fault_Code6,17)
  SELF.AddHistoryField(?cwp:Fault_Code6:2,17)
  SELF.AddUpdateFile(Access:COMMONWP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:COMMONCP.Open
  Relate:COMMONCP_ALIAS.Open
  Relate:COMMONWP_ALIAS.Open
  Access:MANFAUPA.UseFile
  Access:MANUFACT.UseFile
  Access:STOCK.UseFile
  Access:COMMONWP.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:COMMONWP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  !Is this an adjustment?
  If cwp:Adjustment = 'YES'
      ?cwp:Part_Number{prop:Disable} = 1
      ?cwp:Description{prop:Disable} = 1
      ?cwp:Purchase_Cost{prop:Disable} = 1
      ?cwp:Sale_Cost{prop:Disable} = 1
      ?cwp:Supplier{prop:Disable} = 1
      ?cwp:Quantity{prop:Disable} = 1
      ?cwp:Exclude_From_Order{prop:Disable} = 1
      ?Browse_Stock_Button{prop:Disable} = 1
  End!If cwp:Adjustment = 'YES'
  Do RecolourWindow
  ?cwp:Fault_Code7:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code8:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code9:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code10:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code11:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code12:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code1:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code2:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code3:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code4:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code5:2{Prop:Alrt,255} = MouseLeft2
  ?cwp:Fault_Code6:2{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(cwp:Supplier,?cwp:Supplier,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sup:Company_Name_Key)
  FDCB6.AddField(sup:Company_Name,FDCB6.Q.sup:Company_Name)
  FDCB6.AddField(sup:RecordNumber,FDCB6.Q.sup:RecordNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:COMMONCP.Close
    Relate:COMMONCP_ALIAS.Close
    Relate:COMMONWP_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateCOMMONWP',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?cwp:Part_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Part_Number, Accepted)
      If ~0{prop:acceptall}
      access:stock.clearkey(sto:manufacturer_key)
      sto:manufacturer = glo:select11
      sto:part_number  = cwp:part_number
      if access:stock.fetch(sto:manufacturer_key)
          beep(beep:systemexclamation)  ;  yield()
          case message('This Part Number does not exist in the Stock. '&|
                  '||Do you want to select another Model Number or continue.', |
                  'ServiceBase 2000', icon:exclamation, |
                   'Select|Continue', 1, 0)
          of 1  ! name: select  (default)
              post(event:accepted,?browse_stock_button)
          of 2  ! name: continue
              Select(?cwp:description)
          end !case
      
      Else!if access:stock.fetch(sto:manufacturer_key)
          CwP:Part_Number     = sto:part_number
          CwP:Description     = sto:description
          CwP:Supplier        = sto:supplier
          CwP:Purchase_Cost   = sto:Purchase_Cost
          CwP:Sale_Cost       = sto:sale_cost
      
          cwp:part_ref_number = sto:ref_number
          Select(?cwp:quantity)
          display()
      end
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Part_Number, Accepted)
    OF ?browse_stock_button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?browse_stock_button, Accepted)
      access:users.clearkey(use:password_key)
      use:password    =glo:password
      access:users.fetch(use:password_key)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      browse_model_stock(glo:select12,use:user_code)
      if globalresponse = requestcompleted
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = stm:ref_number
          if access:stock.fetch(sto:ref_number_key)
              beep(beep:systemhand)  ;  yield()
              message('Error! Cannot access the Stock File.', |
                      'ServiceBase 2000', icon:hand)
          Else!if access:stock.fetch(sto:ref_number_key)
              CwP:Part_Number     = sto:part_number
              CwP:Description     = sto:description
              CwP:Supplier        = sto:supplier
              CwP:Purchase_Cost   = sto:Purchase_Cost
              CwP:Sale_Cost       = sto:sale_cost
              cwp:part_ref_number = sto:ref_number
              Select(?cwp:quantity)
              display()
          end!if access:stock.fetch(sto:ref_number_key)
      end
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?browse_stock_button, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code1 = TINCALENDARStyle1(cwp:Fault_Code1)
          Display(?cwp:Fault_Code1:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?cwp:Fault_Code1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code1, Accepted)
      If cwp:fault_code1 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 1
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 1
                  mfp:field        = cwp:fault_code1
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code1 = mfp:field
                      else
                          cwp:fault_code1 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code1)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code1, Accepted)
    OF ?cwp:Fault_Code2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code2, Accepted)
      If cwp:fault_code2 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 2
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 2
                  mfp:field        = cwp:fault_code2
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code2 = mfp:field
                      else
                          cwp:fault_code2 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code2)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code2, Accepted)
    OF ?Button4:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:2, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 2
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code2 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:2, Accepted)
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code2 = TINCALENDARStyle1(cwp:Fault_Code2)
          Display(?cwp:Fault_Code2:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 1
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code1 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?cwp:Fault_Code3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code3, Accepted)
      If cwp:fault_code3 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 3
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 3
                  mfp:field        = cwp:fault_code3
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code3 = mfp:field
                      else
                          cwp:fault_code3 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code3)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code3, Accepted)
    OF ?Button4:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:3, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 3
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code3 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:3, Accepted)
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code3 = TINCALENDARStyle1(cwp:Fault_Code3)
          Display(?cwp:Fault_Code3:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:4, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 4
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code4 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:4, Accepted)
    OF ?cwp:Fault_Code4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code4, Accepted)
      If cwp:fault_code4 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 4
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 4
                  mfp:field        = cwp:fault_code4
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code4 = mfp:field
                      else
                          cwp:fault_code4 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code4)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code4, Accepted)
    OF ?Button4:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:5, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 5
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code5 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:5, Accepted)
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code4 = TINCALENDARStyle1(cwp:Fault_Code4)
          Display(?cwp:Fault_Code4:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:6, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 6
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code6 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:6, Accepted)
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code5 = TINCALENDARStyle1(cwp:Fault_Code5)
          Display(?cwp:Fault_Code5:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:7, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 7
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code7 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:7, Accepted)
    OF ?cwp:Fault_Code7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code7, Accepted)
      If cwp:fault_code7 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 7
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 7
                  mfp:field        = cwp:fault_code7
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code7 = mfp:field
                      else
                          cwp:fault_code7 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code7)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code7, Accepted)
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code7 = TINCALENDARStyle1(cwp:Fault_Code7)
          Display(?cwp:Fault_Code7:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:8, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 8
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code8 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:8, Accepted)
    OF ?cwp:Fault_Code8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code8, Accepted)
      If cwp:fault_code8 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 8
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 8
                  mfp:field        = cwp:fault_code8
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code8 = mfp:field
                      else
                          cwp:fault_code8 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code8)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code8, Accepted)
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code8 = TINCALENDARStyle1(cwp:Fault_Code8)
          Display(?cwp:Fault_Code8:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code6 = TINCALENDARStyle1(cwp:Fault_Code6)
          Display(?cwp:Fault_Code6:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:9, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 9
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code9 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:9, Accepted)
    OF ?Button4:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:10, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 10
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code10 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:10, Accepted)
    OF ?cwp:Fault_Code9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code9, Accepted)
      If cwp:fault_code9 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 9
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 9
                  mfp:field        = cwp:fault_code9
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code9 = mfp:field
                      else
                          cwp:fault_code9 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code9)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code9, Accepted)
    OF ?cwp:Fault_Code10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code10, Accepted)
      If cwp:fault_code10 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 10
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 10
                  mfp:field        = cwp:fault_code10
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code10 = mfp:field
                      else
                          cwp:fault_code10 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code10)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code10, Accepted)
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code9 = TINCALENDARStyle1(cwp:Fault_Code9)
          Display(?cwp:Fault_Code9:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?cwp:Fault_Code11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code11, Accepted)
      If cwp:fault_code11 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 11
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 11
                  mfp:field        = cwp:fault_code11
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code11 = mfp:field
                      else
                          cwp:fault_code11 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code11)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code11, Accepted)
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code10 = TINCALENDARStyle1(cwp:Fault_Code10)
          Display(?cwp:Fault_Code10:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?cwp:Fault_Code12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code12, Accepted)
      If cwp:fault_code12 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 12
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 12
                  mfp:field        = cwp:fault_code12
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code12 = mfp:field
                      else
                          cwp:fault_code12 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code12)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code12, Accepted)
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code11 = TINCALENDARStyle1(cwp:Fault_Code11)
          Display(?cwp:Fault_Code11:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:11, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 11
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code11 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:11, Accepted)
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          cwp:Fault_Code12 = TINCALENDARStyle1(cwp:Fault_Code12)
          Display(?cwp:Fault_Code12:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:12, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select11
      glo:select2  = 12
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          cwp:fault_code12 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4:12, Accepted)
    OF ?cwp:Fault_Code5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code5, Accepted)
      If cwp:fault_code5 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 5
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 5
                  mfp:field        = cwp:fault_code5
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code5 = mfp:field
                      else
                          cwp:fault_code5 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code5)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code5, Accepted)
    OF ?cwp:Fault_Code6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code6, Accepted)
      If cwp:fault_code6 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 6
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 6
                  mfp:field        = cwp:fault_code6
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          cwp:fault_code6 = mfp:field
                      else
                          cwp:fault_code6 = ''
                          select(?-1)
                      end
                      display(?cwp:fault_code6)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If cwp:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cwp:Fault_Code6, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  found# = 0
  setcursor(cursor:wait)
  
  save_cwp_ali_id = access:commonwp_alias.savefile()
  access:commonwp_alias.clearkey(cwp_alias:refpartnumberkey)
  cwp_alias:ref_number  = cwp:ref_number
  cwp_alias:part_number = cwp:part_number
  set(cwp_alias:refpartnumberkey,cwp_alias:refpartnumberkey)
  loop
      if access:commonwp_alias.next()
         break
      end !if
      if cwp_alias:ref_number  <> cwp:ref_number      |
      or cwp_alias:part_number <> cwp:part_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      If cwp_alias:record_number <> cwp:record_number
          found# = 1
          Break
      End!If ccp_ali:record_number <> cwp:record_number
  end !loop
  access:commonwp_alias.restorefile(save_cwp_ali_id)
  setcursor()
  
  If found# = 1
      beep(beep:systemhand)  ;  yield()
      message('This part has already been selected.', |
             'ServiceBase 2000', icon:hand)
      Select(?cwp:part_number)
      Cycle
  End !If found# = 1
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateCOMMONWP')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?cwp:Fault_Code1:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?cwp:Fault_Code2:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?cwp:Fault_Code3:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?cwp:Fault_Code7:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?cwp:Fault_Code8:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?cwp:Fault_Code9:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?cwp:Fault_Code10:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?cwp:Fault_Code11:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?cwp:Fault_Code12:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  OF ?cwp:Fault_Code4:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?cwp:Fault_Code5:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?cwp:Fault_Code6:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ! Fault Coding (Hopefully)
      required# = 0
      
      found# = 0
      setcursor(cursor:wait)
      save_map_id = access:manfaupa.savefile()
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = glo:select11
      set(map:field_number_key,map:field_number_key)
      loop
          if access:manfaupa.next()
             break
          end !if
          if map:manufacturer <> glo:select11 |
              then break.  ! end if
      
          Case map:field_number
              Of 1
                  found# = 1
                  Unhide(?cwp:fault_code1:prompt)
                  ?cwp:fault_code1:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar)
                      ?popcalendar{prop:xpos} = 212
                      Unhide(?cwp:fault_code1:2)
                      ?cwp:fault_code1:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code1)
                      If map:lookup = 'YES'
                          Unhide(?button4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code1{prop:req} = 1
                      ?cwp:fault_code1:2{prop:req} = 1
                  else
                      ?cwp:fault_code1{prop:req} = 0
                      ?cwp:fault_code1:2{prop:req} = 0
                  End
              Of 2
                  found# = 1
                  Unhide(?cwp:fault_code2:prompt)
                  ?cwp:fault_code2:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:2)
                      ?popcalendar:2{prop:xpos} = 212
                      Unhide(?cwp:fault_code2:2)
                      ?cwp:fault_code2:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code2)
                      If map:lookup = 'YES'
                          Unhide(?button4:2)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code2{prop:req} = 1
                      ?cwp:fault_code2:2{prop:req} = 1
                  else
                      ?cwp:fault_code2{prop:req} = 0
                      ?cwp:fault_code2:2{prop:req} = 0
                  End
              Of 3
                  found# = 1
                  Unhide(?cwp:fault_code3:prompt)
                  ?cwp:fault_code3:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:3)
                      ?popcalendar:3{prop:xpos} = 212
                      Unhide(?cwp:fault_code3:2)
                      ?cwp:fault_code3:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code3)
                      If map:lookup = 'YES'
                          Unhide(?button4:3)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code3{prop:req} = 1
                      ?cwp:fault_code3:2{prop:req} = 1
                  else
                      ?cwp:fault_code3{prop:req} = 0
                      ?cwp:fault_code3:2{prop:req} = 0
                  End
              Of 4
                  found# = 1
                  Unhide(?cwp:fault_code4:prompt)
                  ?cwp:fault_code4:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:4)
                      ?popcalendar:4{prop:xpos} = 212
                      Unhide(?cwp:fault_code4:2)
                      ?cwp:fault_code4:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code4)
                      If map:lookup = 'YES'
                          Unhide(?button4:4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code4{prop:req} = 1
                      ?cwp:fault_code4:2{prop:req} = 1
                  else
                      ?cwp:fault_code4{prop:req} = 0
                      ?cwp:fault_code4:2{prop:req} = 0
                  End
              Of 5
                  found# = 1
                  Unhide(?cwp:fault_code5:prompt)
                  ?cwp:fault_code5:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:5)
                      ?popcalendar:5{prop:xpos} = 212
                      Unhide(?cwp:fault_code5:2)
                      ?cwp:fault_code5:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code5)
                      If map:lookup = 'YES'
                          Unhide(?button4:5)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code5{prop:req} = 1
                      ?cwp:fault_code5:2{prop:req} = 1
                  else
                      ?cwp:fault_code5{prop:req} = 0
                      ?cwp:fault_code5:2{prop:req} = 0
                  End
              Of 6
                  found# = 1
                  Unhide(?cwp:fault_code6:prompt)
                  ?cwp:fault_code6:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:6)
                      ?popcalendar:6{prop:xpos} = 212
                      Unhide(?cwp:fault_code6:2)
                      ?cwp:fault_code6:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code6)
                      If map:lookup = 'YES'
                          Unhide(?button4:6)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code6{prop:req} = 1
                      ?cwp:fault_code6:2{prop:req} = 1
                  else
                      ?cwp:fault_code6{prop:req} = 0
                      ?cwp:fault_code6:2{prop:req} = 0
                  End
              Of 7
                  found# = 1
                  Unhide(?cwp:fault_code7:prompt)
                  ?cwp:fault_code7:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:7)
                      ?popcalendar:7{prop:xpos} = 212
                      Unhide(?cwp:fault_code7:2)
                      ?cwp:fault_code7:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code7)
                      If map:lookup = 'YES'
                          Unhide(?button4:7)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code7{prop:req} = 1
                      ?cwp:fault_code7:2{prop:req} = 1
                  else
                      ?cwp:fault_code7{prop:req} = 0
                      ?cwp:fault_code7:2{prop:req} = 0
                  End
      
              Of 8
                  found# = 1
                  Unhide(?cwp:fault_code8:prompt)
                  ?cwp:fault_code8:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:8)
                      ?popcalendar:8{prop:xpos} = 212
                      Unhide(?cwp:fault_code8:2)
                      ?cwp:fault_code8:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code8)
                      If map:lookup = 'YES'
                          Unhide(?button4:8)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code8{prop:req} = 1
                      ?cwp:fault_code8:2{prop:req} = 1
                  else
                      ?cwp:fault_code8{prop:req} = 0
                      ?cwp:fault_code8:2{prop:req} = 0
                  End
      
              Of 9
                  found# = 1
                  Unhide(?cwp:fault_code9:prompt)
                  ?cwp:fault_code9:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:9)
                      ?popcalendar:9{prop:xpos} = 212
                      Unhide(?cwp:fault_code9:2)
                      ?cwp:fault_code9:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code9)
                      If map:lookup = 'YES'
                          Unhide(?button4:9)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code9{prop:req} = 1
                      ?cwp:fault_code9:2{prop:req} = 1
                  else
                      ?cwp:fault_code9{prop:req} = 0
                      ?cwp:fault_code9:2{prop:req} = 0
                  End
      
              Of 10
                  found# = 1
                  Unhide(?cwp:fault_code10:prompt)
                  ?cwp:fault_code10:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:10)
                      ?popcalendar:10{prop:xpos} = 212
                      Unhide(?cwp:fault_code10:2)
                      ?cwp:fault_code10:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code10)
                      If map:lookup = 'YES'
                          Unhide(?button4:10)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code10{prop:req} = 1
                      ?cwp:fault_code10:2{prop:req} = 1
                  else
                      ?cwp:fault_code10{prop:req} = 0
                      ?cwp:fault_code10:2{prop:req} = 0
                  End
      
              Of 11
                  found# = 1
                  Unhide(?cwp:fault_code11:prompt)
                  ?cwp:fault_code11:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:11)
                      ?popcalendar:11{prop:xpos} = 212
                      Unhide(?cwp:fault_code11:2)
                      ?cwp:fault_code11:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code11)
                      If map:lookup = 'YES'
                          Unhide(?button4:11)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code11{prop:req} = 1
                      ?cwp:fault_code11:2{prop:req} = 1
                  else
                      ?cwp:fault_code11{prop:req} = 0
                      ?cwp:fault_code11:2{prop:req} = 0
                  End
      
              Of 12
                  found# = 1
                  Unhide(?cwp:fault_code12:prompt)
                  ?cwp:fault_code12:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:12)
                      ?popcalendar:12{prop:xpos} = 212
                      Unhide(?cwp:fault_code12:2)
                      ?cwp:fault_code12:2{prop:xpos} = 84
                  Else
                      Unhide(?cwp:fault_code12)
                      If map:lookup = 'YES'
                          Unhide(?button4:12)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?cwp:fault_code12{prop:req} = 1
                      ?cwp:fault_code12:2{prop:req} = 1
                  else
                      ?cwp:fault_code12{prop:req} = 0
                      ?cwp:fault_code12:2{prop:req} = 0
                  End
      
          End !Case map:field_number
      end !loop
      access:manfaupa.restorefile(save_map_id)
      setcursor()
      
      If found# = 1
          Unhide(?fault_Code_tab)
      End!If found# = 1
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

