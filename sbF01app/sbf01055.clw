

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01055.INC'),ONCE        !Local module procedure declarations
                     END


Force_Despatch PROCEDURE                              !Generated from procedure template - Window

job_number_temp      LONG
esn_temp             STRING(16)
tmp:consignmentno    STRING(30)
tmp:excconsignmentno STRING(30)
tmp:loaconsignmentno STRING(30)
tmp:selectjob        BYTE(0)
tmp:selectexc        BYTE(0)
tmp:selectloa        BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?job:Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?job:Exchange_Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?job:Loan_Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB5::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB6::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Manual Despatch Confirmation'),AT(,,606,199),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,600,56),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('WARNING! This procedure should only be used in the event of an error.'),AT(8,8,460,12),USE(?Prompt1),FONT(,10,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           PROMPT('This procedure will assign a Consignment Number / Courier / Date Despatched and ' &|
   'mark it as ''Despatched'''),AT(8,24,416,16),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('(Note: An entry will be recorded in the Audit Trail)'),AT(8,40),USE(?Prompt3)
                         END
                       END
                       SHEET,AT(4,64,600,104),USE(?Sheet2),SPREAD
                         TAB('Assign Consignment Number'),USE(?Tab2)
                           PROMPT('Job Number'),AT(8,84),USE(?job_number_temp:Prompt)
                           ENTRY(@s8),AT(76,84,64,10),USE(job_number_temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           GROUP,AT(8,96,592,64),USE(?Group1),DISABLE
                             PROMPT('Status'),AT(76,100),USE(?JOB:Courier:Prompt:2),TRN
                             PROMPT('Date Despatched'),AT(312,100),USE(?JOB:Date_Despatched:Prompt),TRN
                             PROMPT('Consignment Number'),AT(408,100),USE(?JOB:Date_Despatched:Prompt:2),TRN
                             PROMPT('Courier'),AT(204,100),USE(?JOB:Courier:Prompt:3),TRN
                             COMBO(@s30),AT(204,116,104,10),USE(job:Courier),IMM,DISABLE,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                             ENTRY(@d6b),AT(312,116,64,10),USE(job:Date_Despatched),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                             BUTTON('Today'),AT(380,116,29,11),USE(?Today),DISABLE
                             BUTTON('Today'),AT(380,132,29,11),USE(?Today1),DISABLE
                             ENTRY(@s30),AT(412,116,96,10),USE(tmp:consignmentno),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                             CHECK('Update Repair Unit'),AT(512,116),USE(tmp:selectjob)
                             PROMPT('Job Status'),AT(8,116),USE(?JOB:Current_Status:Prompt),TRN
                             ENTRY(@s30),AT(76,116,124,10),USE(job:Current_Status),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                             ENTRY(@s30),AT(76,132,124,10),USE(job:Exchange_Status),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                             COMBO(@s30),AT(204,132,104,10),USE(job:Exchange_Courier),IMM,DISABLE,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                             ENTRY(@d6b),AT(312,132,64,10),USE(job:Exchange_Despatched),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                             ENTRY(@s30),AT(412,132,96,10),USE(tmp:excconsignmentno),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                             CHECK('Update Exchange Unit'),AT(512,132),USE(tmp:selectexc),RIGHT,VALUE('1','0')
                             BUTTON('Today'),AT(380,148,29,11),USE(?Today2),DISABLE
                             ENTRY(@s30),AT(76,148,124,10),USE(job:Loan_Status),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                             COMBO(@s30),AT(203,148,104,10),USE(job:Loan_Courier),IMM,DISABLE,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                             ENTRY(@d6b),AT(312,148,64,10),USE(job:Loan_Despatched),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                             ENTRY(@s30),AT(412,148,96,10),USE(tmp:loaconsignmentno),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                             CHECK('Update Loan Unit'),AT(512,148),USE(tmp:selectloa),RIGHT,VALUE('1','0')
                           END
                           PROMPT('Exchange Unit'),AT(8,132),USE(?JOB:Exchange_Status:Prompt),TRN
                           PROMPT('Loan Unit'),AT(8,148,52,12),USE(?JOB:Loan_Status:Prompt),TRN
                         END
                       END
                       PANEL,AT(4,172,600,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Submit'),AT(8,176,56,16),USE(?Resubmit),LEFT,ICON('desp_sm.gif')
                       BUTTON('Finish'),AT(544,176,56,16),USE(?Close),LEFT,ICON('thumbs.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?job_number_temp:Prompt{prop:FontColor} = -1
    ?job_number_temp:Prompt{prop:Color} = 15066597
    If ?job_number_temp{prop:ReadOnly} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 15066597
    Elsif ?job_number_temp{prop:Req} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 8454143
    Else ! If ?job_number_temp{prop:Req} = True
        ?job_number_temp{prop:FontColor} = 65793
        ?job_number_temp{prop:Color} = 16777215
    End ! If ?job_number_temp{prop:Req} = True
    ?job_number_temp{prop:Trn} = 0
    ?job_number_temp{prop:FontStyle} = font:Bold
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?JOB:Courier:Prompt:2{prop:FontColor} = -1
    ?JOB:Courier:Prompt:2{prop:Color} = 15066597
    ?JOB:Date_Despatched:Prompt{prop:FontColor} = -1
    ?JOB:Date_Despatched:Prompt{prop:Color} = 15066597
    ?JOB:Date_Despatched:Prompt:2{prop:FontColor} = -1
    ?JOB:Date_Despatched:Prompt:2{prop:Color} = 15066597
    ?JOB:Courier:Prompt:3{prop:FontColor} = -1
    ?JOB:Courier:Prompt:3{prop:Color} = 15066597
    If ?job:Courier{prop:ReadOnly} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 15066597
    Elsif ?job:Courier{prop:Req} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 8454143
    Else ! If ?job:Courier{prop:Req} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 16777215
    End ! If ?job:Courier{prop:Req} = True
    ?job:Courier{prop:Trn} = 0
    ?job:Courier{prop:FontStyle} = font:Bold
    If ?job:Date_Despatched{prop:ReadOnly} = True
        ?job:Date_Despatched{prop:FontColor} = 65793
        ?job:Date_Despatched{prop:Color} = 15066597
    Elsif ?job:Date_Despatched{prop:Req} = True
        ?job:Date_Despatched{prop:FontColor} = 65793
        ?job:Date_Despatched{prop:Color} = 8454143
    Else ! If ?job:Date_Despatched{prop:Req} = True
        ?job:Date_Despatched{prop:FontColor} = 65793
        ?job:Date_Despatched{prop:Color} = 16777215
    End ! If ?job:Date_Despatched{prop:Req} = True
    ?job:Date_Despatched{prop:Trn} = 0
    ?job:Date_Despatched{prop:FontStyle} = font:Bold
    If ?tmp:consignmentno{prop:ReadOnly} = True
        ?tmp:consignmentno{prop:FontColor} = 65793
        ?tmp:consignmentno{prop:Color} = 15066597
    Elsif ?tmp:consignmentno{prop:Req} = True
        ?tmp:consignmentno{prop:FontColor} = 65793
        ?tmp:consignmentno{prop:Color} = 8454143
    Else ! If ?tmp:consignmentno{prop:Req} = True
        ?tmp:consignmentno{prop:FontColor} = 65793
        ?tmp:consignmentno{prop:Color} = 16777215
    End ! If ?tmp:consignmentno{prop:Req} = True
    ?tmp:consignmentno{prop:Trn} = 0
    ?tmp:consignmentno{prop:FontStyle} = font:Bold
    ?tmp:selectjob{prop:Font,3} = -1
    ?tmp:selectjob{prop:Color} = 15066597
    ?tmp:selectjob{prop:Trn} = 0
    ?JOB:Current_Status:Prompt{prop:FontColor} = -1
    ?JOB:Current_Status:Prompt{prop:Color} = 15066597
    If ?job:Current_Status{prop:ReadOnly} = True
        ?job:Current_Status{prop:FontColor} = 65793
        ?job:Current_Status{prop:Color} = 15066597
    Elsif ?job:Current_Status{prop:Req} = True
        ?job:Current_Status{prop:FontColor} = 65793
        ?job:Current_Status{prop:Color} = 8454143
    Else ! If ?job:Current_Status{prop:Req} = True
        ?job:Current_Status{prop:FontColor} = 65793
        ?job:Current_Status{prop:Color} = 16777215
    End ! If ?job:Current_Status{prop:Req} = True
    ?job:Current_Status{prop:Trn} = 0
    ?job:Current_Status{prop:FontStyle} = font:Bold
    If ?job:Exchange_Status{prop:ReadOnly} = True
        ?job:Exchange_Status{prop:FontColor} = 65793
        ?job:Exchange_Status{prop:Color} = 15066597
    Elsif ?job:Exchange_Status{prop:Req} = True
        ?job:Exchange_Status{prop:FontColor} = 65793
        ?job:Exchange_Status{prop:Color} = 8454143
    Else ! If ?job:Exchange_Status{prop:Req} = True
        ?job:Exchange_Status{prop:FontColor} = 65793
        ?job:Exchange_Status{prop:Color} = 16777215
    End ! If ?job:Exchange_Status{prop:Req} = True
    ?job:Exchange_Status{prop:Trn} = 0
    ?job:Exchange_Status{prop:FontStyle} = font:Bold
    If ?job:Exchange_Courier{prop:ReadOnly} = True
        ?job:Exchange_Courier{prop:FontColor} = 65793
        ?job:Exchange_Courier{prop:Color} = 15066597
    Elsif ?job:Exchange_Courier{prop:Req} = True
        ?job:Exchange_Courier{prop:FontColor} = 65793
        ?job:Exchange_Courier{prop:Color} = 8454143
    Else ! If ?job:Exchange_Courier{prop:Req} = True
        ?job:Exchange_Courier{prop:FontColor} = 65793
        ?job:Exchange_Courier{prop:Color} = 16777215
    End ! If ?job:Exchange_Courier{prop:Req} = True
    ?job:Exchange_Courier{prop:Trn} = 0
    ?job:Exchange_Courier{prop:FontStyle} = font:Bold
    If ?job:Exchange_Despatched{prop:ReadOnly} = True
        ?job:Exchange_Despatched{prop:FontColor} = 65793
        ?job:Exchange_Despatched{prop:Color} = 15066597
    Elsif ?job:Exchange_Despatched{prop:Req} = True
        ?job:Exchange_Despatched{prop:FontColor} = 65793
        ?job:Exchange_Despatched{prop:Color} = 8454143
    Else ! If ?job:Exchange_Despatched{prop:Req} = True
        ?job:Exchange_Despatched{prop:FontColor} = 65793
        ?job:Exchange_Despatched{prop:Color} = 16777215
    End ! If ?job:Exchange_Despatched{prop:Req} = True
    ?job:Exchange_Despatched{prop:Trn} = 0
    ?job:Exchange_Despatched{prop:FontStyle} = font:Bold
    If ?tmp:excconsignmentno{prop:ReadOnly} = True
        ?tmp:excconsignmentno{prop:FontColor} = 65793
        ?tmp:excconsignmentno{prop:Color} = 15066597
    Elsif ?tmp:excconsignmentno{prop:Req} = True
        ?tmp:excconsignmentno{prop:FontColor} = 65793
        ?tmp:excconsignmentno{prop:Color} = 8454143
    Else ! If ?tmp:excconsignmentno{prop:Req} = True
        ?tmp:excconsignmentno{prop:FontColor} = 65793
        ?tmp:excconsignmentno{prop:Color} = 16777215
    End ! If ?tmp:excconsignmentno{prop:Req} = True
    ?tmp:excconsignmentno{prop:Trn} = 0
    ?tmp:excconsignmentno{prop:FontStyle} = font:Bold
    ?tmp:selectexc{prop:Font,3} = -1
    ?tmp:selectexc{prop:Color} = 15066597
    ?tmp:selectexc{prop:Trn} = 0
    If ?job:Loan_Status{prop:ReadOnly} = True
        ?job:Loan_Status{prop:FontColor} = 65793
        ?job:Loan_Status{prop:Color} = 15066597
    Elsif ?job:Loan_Status{prop:Req} = True
        ?job:Loan_Status{prop:FontColor} = 65793
        ?job:Loan_Status{prop:Color} = 8454143
    Else ! If ?job:Loan_Status{prop:Req} = True
        ?job:Loan_Status{prop:FontColor} = 65793
        ?job:Loan_Status{prop:Color} = 16777215
    End ! If ?job:Loan_Status{prop:Req} = True
    ?job:Loan_Status{prop:Trn} = 0
    ?job:Loan_Status{prop:FontStyle} = font:Bold
    If ?job:Loan_Courier{prop:ReadOnly} = True
        ?job:Loan_Courier{prop:FontColor} = 65793
        ?job:Loan_Courier{prop:Color} = 15066597
    Elsif ?job:Loan_Courier{prop:Req} = True
        ?job:Loan_Courier{prop:FontColor} = 65793
        ?job:Loan_Courier{prop:Color} = 8454143
    Else ! If ?job:Loan_Courier{prop:Req} = True
        ?job:Loan_Courier{prop:FontColor} = 65793
        ?job:Loan_Courier{prop:Color} = 16777215
    End ! If ?job:Loan_Courier{prop:Req} = True
    ?job:Loan_Courier{prop:Trn} = 0
    ?job:Loan_Courier{prop:FontStyle} = font:Bold
    If ?job:Loan_Despatched{prop:ReadOnly} = True
        ?job:Loan_Despatched{prop:FontColor} = 65793
        ?job:Loan_Despatched{prop:Color} = 15066597
    Elsif ?job:Loan_Despatched{prop:Req} = True
        ?job:Loan_Despatched{prop:FontColor} = 65793
        ?job:Loan_Despatched{prop:Color} = 8454143
    Else ! If ?job:Loan_Despatched{prop:Req} = True
        ?job:Loan_Despatched{prop:FontColor} = 65793
        ?job:Loan_Despatched{prop:Color} = 16777215
    End ! If ?job:Loan_Despatched{prop:Req} = True
    ?job:Loan_Despatched{prop:Trn} = 0
    ?job:Loan_Despatched{prop:FontStyle} = font:Bold
    If ?tmp:loaconsignmentno{prop:ReadOnly} = True
        ?tmp:loaconsignmentno{prop:FontColor} = 65793
        ?tmp:loaconsignmentno{prop:Color} = 15066597
    Elsif ?tmp:loaconsignmentno{prop:Req} = True
        ?tmp:loaconsignmentno{prop:FontColor} = 65793
        ?tmp:loaconsignmentno{prop:Color} = 8454143
    Else ! If ?tmp:loaconsignmentno{prop:Req} = True
        ?tmp:loaconsignmentno{prop:FontColor} = 65793
        ?tmp:loaconsignmentno{prop:Color} = 16777215
    End ! If ?tmp:loaconsignmentno{prop:Req} = True
    ?tmp:loaconsignmentno{prop:Trn} = 0
    ?tmp:loaconsignmentno{prop:FontStyle} = font:Bold
    ?tmp:selectloa{prop:Font,3} = -1
    ?tmp:selectloa{prop:Color} = 15066597
    ?tmp:selectloa{prop:Trn} = 0
    ?JOB:Exchange_Status:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Status:Prompt{prop:Color} = 15066597
    ?JOB:Loan_Status:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Status:Prompt{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Force_Despatch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('job_number_temp',job_number_temp,'Force_Despatch',1)
    SolaceViewVars('esn_temp',esn_temp,'Force_Despatch',1)
    SolaceViewVars('tmp:consignmentno',tmp:consignmentno,'Force_Despatch',1)
    SolaceViewVars('tmp:excconsignmentno',tmp:excconsignmentno,'Force_Despatch',1)
    SolaceViewVars('tmp:loaconsignmentno',tmp:loaconsignmentno,'Force_Despatch',1)
    SolaceViewVars('tmp:selectjob',tmp:selectjob,'Force_Despatch',1)
    SolaceViewVars('tmp:selectexc',tmp:selectexc,'Force_Despatch',1)
    SolaceViewVars('tmp:selectloa',tmp:selectloa,'Force_Despatch',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_number_temp:Prompt;  SolaceCtrlName = '?job_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_number_temp;  SolaceCtrlName = '?job_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Courier:Prompt:2;  SolaceCtrlName = '?JOB:Courier:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Date_Despatched:Prompt;  SolaceCtrlName = '?JOB:Date_Despatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Date_Despatched:Prompt:2;  SolaceCtrlName = '?JOB:Date_Despatched:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Courier:Prompt:3;  SolaceCtrlName = '?JOB:Courier:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Courier;  SolaceCtrlName = '?job:Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Date_Despatched;  SolaceCtrlName = '?job:Date_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Today;  SolaceCtrlName = '?Today';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Today1;  SolaceCtrlName = '?Today1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:consignmentno;  SolaceCtrlName = '?tmp:consignmentno';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:selectjob;  SolaceCtrlName = '?tmp:selectjob';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Current_Status:Prompt;  SolaceCtrlName = '?JOB:Current_Status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Current_Status;  SolaceCtrlName = '?job:Current_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Status;  SolaceCtrlName = '?job:Exchange_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Courier;  SolaceCtrlName = '?job:Exchange_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Despatched;  SolaceCtrlName = '?job:Exchange_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:excconsignmentno;  SolaceCtrlName = '?tmp:excconsignmentno';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:selectexc;  SolaceCtrlName = '?tmp:selectexc';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Today2;  SolaceCtrlName = '?Today2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Status;  SolaceCtrlName = '?job:Loan_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Courier;  SolaceCtrlName = '?job:Loan_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Despatched;  SolaceCtrlName = '?job:Loan_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:loaconsignmentno;  SolaceCtrlName = '?tmp:loaconsignmentno';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:selectloa;  SolaceCtrlName = '?tmp:selectloa';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Status:Prompt;  SolaceCtrlName = '?JOB:Exchange_Status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Status:Prompt;  SolaceCtrlName = '?JOB:Loan_Status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Resubmit;  SolaceCtrlName = '?Resubmit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Force_Despatch')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Force_Despatch')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:STATUS.Open
  Access:JOBS.UseFile
  Access:USERS.UseFile
  Access:COURIER.UseFile
  Access:LOAN.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FDCB4.Init(job:Courier,?job:Courier,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder(cou:Courier_Key)
  FDCB4.AddField(cou:Courier,FDCB4.Q.cou:Courier)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB5.Init(job:Exchange_Courier,?job:Exchange_Courier,Queue:FileDropCombo:1.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:1,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:1
  FDCB5.AddSortOrder(cou:Courier_Key)
  FDCB5.AddField(cou:Courier,FDCB5.Q.cou:Courier)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB6.Init(job:Loan_Courier,?job:Loan_Courier,Queue:FileDropCombo:2.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo:2,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo:2
  FDCB6.AddSortOrder(cou:Courier_Key)
  FDCB6.AddField(cou:Courier,FDCB6.Q.cou:Courier)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:STATUS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Force_Despatch',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Courier
      Browse_Courier
      Browse_Courier
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?job_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job_number_temp, Accepted)
      Disable(?Group1)
      tmp:selectjob   = 0
      tmp:selectloa   = 0
      tmp:selectexc   = 0
      Post(Event:accepted,?tmp:selectjob)
      Post(Event:accepted,?tmp:selectexc)
      Post(Event:accepted,?tmp:selectloa)
      If job_number_temp <> 0
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number  = job_number_temp
          If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
              Enable(?Group1)
              tmp:consignmentno   = job:consignment_number
              tmp:excconsignmentno    = job:exchange_consignment_number
              tmp:loaconsignmentno    = job:loan_consignment_number
          Else!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
              Case MessageEx('Unable to find selected job!','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
                      job_number_temp = ''
                      Select(?job_number_temp)
              End!Case MessageEx
          End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
      End!If job_number_temp <> ''
      Display()
      !Thismakeover.setwindow(win:window)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job_number_temp, Accepted)
    OF ?Today
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Today, Accepted)
      job:date_Despatched = Today()
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Today, Accepted)
    OF ?Today1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Today1, Accepted)
      job:exchange_Despatched = Today()
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Today1, Accepted)
    OF ?tmp:selectjob
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:selectjob, Accepted)
      Case tmp:selectjob
          OF 0
              ?tmp:consignmentno{prop:readonly} = 1
              ?tmp:consignmentno{prop:skip} = 1
              ?tmp:consignmentno{prop:req} = 0
              ?job:courier{prop:readonly} = 1
              ?job:courier{prop:skip} = 1
              ?job:courier{prop:req} = 0
              ?job:date_despatched{prop:readonly} = 1
              ?job:date_despatched{prop:skip} = 1
              ?job:date_despatched{prop:req} = 0
              Disable(?today)
              Disable(?job:courier)
          OF 1
              ?tmp:consignmentno{prop:readonly} = 0
              ?tmp:consignmentno{prop:skip} = 0
              ?tmp:consignmentno{prop:req} = 1
              ?job:courier{prop:readonly} = 0
              ?job:courier{prop:skip} = 0
              ?job:courier{prop:req} = 1
              ?job:date_despatched{prop:readonly} = 0
              ?job:date_despatched{prop:skip} = 0
              ?job:date_despatched{prop:req} = 1
              Enable(?today)
              Enable(?job:courier)
      End!Case tmp:selectjob
      !ThisMakeOver.SetWindow(Win:Window)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:selectjob, Accepted)
    OF ?tmp:selectexc
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:selectexc, Accepted)
      Case tmp:selectexc
          OF 0
              ?tmp:excconsignmentno{prop:readonly} = 1
              ?tmp:excconsignmentno{prop:skip} = 1
              ?tmp:excconsignmentno{prop:req} = 0
              ?job:exchange_courier{prop:readonly} = 1
              ?job:exchange_courier{prop:skip} = 1
              ?job:exchange_courier{prop:req} = 0
              ?job:Exchange_despatched{prop:readonly} = 1
              ?job:exchange_despatched{prop:skip} = 1
              ?job:exchange_despatched{prop:req} = 0
              Disable(?today1)
              Disable(?job:exchange_courier)
          OF 1
              ?tmp:excconsignmentno{prop:readonly} = 0
              ?tmp:excconsignmentno{prop:skip} = 0
              ?tmp:excconsignmentno{prop:req} = 1
              ?job:exchange_courier{prop:readonly} = 0
              ?job:exchange_courier{prop:skip} = 0
              ?job:exchange_courier{prop:req} = 1
              ?job:exchange_despatched{prop:readonly} = 0
              ?job:exchange_despatched{prop:skip} = 0
              ?job:exchange_despatched{prop:req} = 1
              Enable(?today1)
              Enable(?job:exchange_courier)
      End!Case tmp:selectjob
      !ThisMakeOver.SetWindow(Win:Window)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:selectexc, Accepted)
    OF ?Today2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Today2, Accepted)
      job:loan_Despatched = Today()
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Today2, Accepted)
    OF ?tmp:selectloa
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:selectloa, Accepted)
      Case tmp:selectloa
          OF 0
              ?tmp:loaconsignmentno{prop:readonly} = 1
              ?tmp:loaconsignmentno{prop:skip} = 1
              ?tmp:loaconsignmentno{prop:req} = 0
              ?job:loan_courier{prop:readonly} = 1
              ?job:loan_courier{prop:skip} = 1
              ?job:loan_courier{prop:req} = 0
              ?job:loan_despatched{prop:readonly} = 1
              ?job:loan_despatched{prop:skip} = 1
              ?job:loan_despatched{prop:req} = 0
              Disable(?Today2)
              Disable(?job:loan_Courier)
          OF 1
              ?tmp:loaconsignmentno{prop:readonly} = 0
              ?tmp:loaconsignmentno{prop:skip} = 0
              ?tmp:loaconsignmentno{prop:req} = 1
              ?job:loan_courier{prop:readonly} = 0
              ?job:loan_courier{prop:skip} = 0
              ?job:loan_courier{prop:req} = 1
              ?job:loan_despatched{prop:readonly} = 0
              ?job:loan_despatched{prop:skip} = 0
              ?job:loan_despatched{prop:req} = 1
              Enable(?Today2)
              Enable(?job:loan_Courier)
      End!Case tmp:selectjob
      !ThisMakeOver.SetWindow(Win:Window)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:selectloa, Accepted)
    OF ?Resubmit
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Resubmit, Accepted)
    If tmp:selectjob = 1
        If tmp:consignmentno = ''
            Case MessageEx('You must insert a Consignment Number for the Repair Unit.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Else!If tmp:consignmentno = ''
          despatch# = 0
          job_number_temp = 0
          Select(?job_number_temp)
          Post(Event:accepted,?job_number_temp)

          despatch# = 1
          If job:date_despatched = ''
              Case MessageEx('This job is not awaiting despatch confirmation.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              despatch# = 0
              job_number_temp = 0
              Select(?job_number_temp)
              Post(Event:accepted,?job_number_temp)
          End!If job:despatched <> 'YES'
          If job:consignment_number <> '' and despatch# = 1
              Case MessageEx('Warning! This job already has a Consignment Number attached to it.<13,10><13,10>Are you sure you want to continue and change the Consignment Number of this job?','ServiceBase 2000',|
                             'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      
                  Of 2 ! &No Button
                      despatch# = 0
                      job_number_temp = 0
                      Select(?job_number_temp)
                      Post(Event:accepted,?job_number_temp)
              End!Case MessageEx
          End!If job:consignment_number <> ''
          If despatch# = 1
              get(audit,0)
              if access:audit.primerecord() = level:benign
                  aud:notes         = 'CONSIGNMENT NUMBER: ' & CLip(tmp:consignmentno)
                  aud:ref_number    = job:ref_number
                  aud:date          = today()
                  aud:time          = clock()
                  access:users.clearkey(use:password_key)
                  use:password =glo:password
                  access:users.fetch(use:password_key)
                  aud:user = use:user_code
                  aud:action        = 'MANUAL DESPATCH CONFIRMATION: JOB'
                  access:audit.insert()
              end!�if access:audit.primerecord() = level:benign

              If job:paid = 'YES'
                  !call the status routine
                  GetStatus(910,1,'JOB') !despatched paid

              Else!If job:paid = 'YES'
                  !call the status routine
                  GetStatus(905,1,'JOB') !despatched unpadi

              End!If job:paid = 'YES'
              JOB:Consignment_Number = tmp:consignmentno
              job:despatched = 'YES'
              access:jobs.update()


              despatch# = 0
              job_number_temp = 0
              Select(?job_number_temp)
              Post(Event:accepted,?job_number_temp)
              Case MessageEx('Job Updated.','ServiceBase 2000',|
                             'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End!If despatch# = 1
        End!If tmp:consignmentno = ''

    End!If tmp:selectjob = 1
    If tmp:selectexc = 1
        If job:exchange_unit_number = ''
            Case MessageEx('An Exchange Unit has NOT been attached to the selected job.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
            despatch# = 0
            job_number_temp = 0
            Select(?job_number_temp)
            Post(Event:accepted,?job_number_temp)

        Else!If job:exchange_unit_number = ''
            If tmp:excconsignmentno = ''
                Case MessageEx('You must insert a Consignment Number for the Exchange Unit.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                despatch# = 0
                job_number_temp = 0
                Select(?job_number_temp)
                Post(Event:accepted,?job_number_temp)

            Else!If tmp:consignmentno = ''
                despatch# = 1
                If job:exchange_despatched = ''
                    Case MessageEx('The Exchange Unit is not awaiting despatch confirmation.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    despatch# = 0
                    job_number_temp = 0
                    Select(?job_number_temp)
                    Post(Event:accepted,?job_number_temp)
                End!If job:despatched <> 'YES'
                If job:exchange_consignment_number <> '' and despatch# = 1
                    Case MessageEx('Warning! This Exchange Unit already has a Consignment Number attached to it.<13,10><13,10>Are you sure you want to continue and change the Consignment Number of this job?','ServiceBase 2000',|
                                   'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                        Of 1 ! &Yes Button
                            
                        Of 2 ! &No Button
                            despatch# = 0
                            job_number_temp = 0
                            Select(?job_number_temp)
                            Post(Event:accepted,?job_number_temp)
                    End!Case MessageEx
                End!If job:consignment_number <> ''
                If despatch# = 1
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:notes         = 'CONSIGNMENT NUMBER: ' & CLip(tmp:excconsignmentno)
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        access:users.clearkey(use:password_key)
                        use:password =glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:type = 'EXC'
                        aud:action        = 'MANUAL DESPATCH CONFIRMATION: EXCHANGE UNIT'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
                    If job:workshop <> 'YES' and job:third_party_site = ''
                        GetStatus(116,1,'JOB')

                    End!If job:workshop <> 'YES'
                    !call the exchange status routine
                    GetStatus(901,1,'EXC') !Despatched

                    JOB:Exchange_Consignment_Number = tmp:excconsignmentno
                    job:despatched = ''
                    access:jobs.update()
                    despatch# = 0
                    job_number_temp = 0
                    Select(?job_number_temp)
                    Post(Event:accepted,?job_number_temp)
                    Case MessageEx('Job Updated.','ServiceBase 2000',|
                                   'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                End! If despatch# = 1
            End!If tmp:consignmentno = ''
        End!If job:exchange_unit_number = ''
    End!If tmp:selectexc = 1

    If tmp:selectloa = 1
        If job:loan_unit_number = ''
            Case MessageEx('An Loan Unit has NOT been attached to the selected job.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
            despatch# = 0
            job_number_temp = 0
            Select(?job_number_temp)
            Post(Event:accepted,?job_number_temp)

        Else!If job:exchange_unit_number = ''
            If tmp:loaconsignmentno = ''
                Case MessageEx('You must insert a Consignment Number for the Loan Unit.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                despatch# = 0
                job_number_temp = 0
                Select(?job_number_temp)
                Post(Event:accepted,?job_number_temp)

            Else!If tmp:consignmentno = ''
            
                despatch# = 1
                If job:loan_despatched = ''
                    Case MessageEx('The Loan Unit is not awaiting despatch confirmation.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    despatch# = 0
                    job_number_temp = 0
                    Select(?job_number_temp)
                    Post(Event:accepted,?job_number_temp)
                End!If job:despatched <> 'YES'
                If job:loan_consignment_number <> '' and despatch# = 1
                    Case MessageEx('Warning! This Loan Unit already has a Consignment Number attached to it.<13,10><13,10>Are you sure you want to continue and change the Consignment Number of this job?','ServiceBase 2000',|
                                   'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                        Of 1 ! &Yes Button
                            
                        Of 2 ! &No Button
                            despatch# = 0
                            job_number_temp = 0
                            Select(?job_number_temp)
                            Post(Event:accepted,?job_number_temp)
                    End!Case MessageEx
                End!If job:consignment_number <> ''
                If despatch# = 1
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:notes         = 'CONSIGNMENT NUMBER: ' & CLip(tmp:loaconsignmentno)
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        access:users.clearkey(use:password_key)
                        use:password =glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:type = 'LOA'
                        aud:action        = 'MANUAL DESPATCH CONFIRMATION: LOAN UNIT'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
                    If job:workshop <> 'YES' and job:third_party_site = ''
                        GetStatus(117,1,'JOB') !awaiting arrival (loan)

                    End!If job:workshop <> 'YES'
                    GetStatus(120,1,'LOA') !Loan Unit Despatched

                    JOB:Loan_Consignment_Number = tmp:loaconsignmentno
                    job:despatched = ''
                    access:jobs.update()
                    despatch# = 0
                    job_number_temp = 0
                    Select(?job_number_temp)
                    Post(Event:accepted,?job_number_temp)
                    Case MessageEx('Job Updated.','ServiceBase 2000',|
                                   'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                End! If despatch# = 1
            End!Else!If tmp:consignmentno = ''
        End!Else!If job:exchange_unit_number = ''
    End!If tmp:selectloa = 1
    Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Resubmit, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Force_Despatch')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

