

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01034.INC'),ONCE        !Local module procedure declarations
                     END


View_Costs PROCEDURE                                  !Generated from procedure template - Window

FilesOpened          BYTE
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
total_temp           REAL
vat_chargeable_temp  REAL
vat_warranty_temp    REAL
total_warranty_temp  REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Chargeable Costs'),AT(,,160,148),FONT('Tahoma',8,,),CENTER,IMM,TIMER(50),GRAY,DOUBLE
                       SHEET,AT(4,4,152,112),USE(?Sheet1),SPREAD
                         TAB('Chargeable Costs'),USE(?Chargeable_Tab),HIDE
                           PROMPT('Chargeable Costs'),AT(8,24),USE(?Prompt1),FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Courier Cost'),AT(8,40),USE(?JOB:Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,40,64,10),USE(job:Courier_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR
                           STRING(@n14.2),AT(80,56),USE(job:Labour_Cost),RIGHT
                           PROMPT('Labour Cost'),AT(8,56),USE(?Prompt10)
                           STRING(@n14.2),AT(80,68),USE(job:Parts_Cost),RIGHT
                           LINE,AT(76,80,67,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Parts Cost'),AT(8,68),USE(?Prompt11)
                           STRING(@n14.2),AT(80,84),USE(vat_chargeable_temp),RIGHT
                           PROMPT('Total Cost'),AT(8,100),USE(?Prompt13)
                           STRING(@n14.2),AT(72,100),USE(total_temp),RIGHT,FONT(,,,FONT:bold)
                           PROMPT('V.A.T.'),AT(8,84),USE(?Prompt12)
                         END
                         TAB('Warranty Costs'),USE(?Warranty_Tab),HIDE
                           PROMPT('Warranty Costs'),AT(8,24),USE(?Prompt6),FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Courier Cost'),AT(8,40),USE(?JOB:Courier_Cost_Warranty:Prompt),TRN
                           ENTRY(@n14.2),AT(84,41,64,10),USE(job:Courier_Cost_Warranty),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Labour Cost'),AT(8,55),USE(?Prompt9)
                           STRING(@n14.2),AT(80,56),USE(job:Labour_Cost_Warranty),RIGHT
                           PROMPT('Parts Cost'),AT(8,68),USE(?Prompt14)
                           STRING(@n14.2),AT(80,68),USE(job:Parts_Cost_Warranty),RIGHT
                           LINE,AT(76,80,67,0),USE(?Line1:2),COLOR(COLOR:Black)
                           PROMPT('V.A.T.'),AT(8,84),USE(?Prompt15)
                           STRING(@n14.2),AT(80,84),USE(vat_warranty_temp),RIGHT
                           PROMPT('Total Cost'),AT(8,100,32,10),USE(?Prompt16)
                           STRING(@n14.2),AT(74,100),USE(total_warranty_temp),RIGHT,FONT(,,,FONT:bold)
                         END
                       END
                       BUTTON('&OK'),AT(96,124,56,16),USE(?OkButton),LEFT,ICON('OK.gif'),STD(STD:Close)
                       PANEL,AT(4,120,152,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Chargeable_Tab{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?JOB:Courier_Cost:Prompt{prop:FontColor} = -1
    ?JOB:Courier_Cost:Prompt{prop:Color} = 15066597
    If ?job:Courier_Cost{prop:ReadOnly} = True
        ?job:Courier_Cost{prop:FontColor} = 65793
        ?job:Courier_Cost{prop:Color} = 15066597
    Elsif ?job:Courier_Cost{prop:Req} = True
        ?job:Courier_Cost{prop:FontColor} = 65793
        ?job:Courier_Cost{prop:Color} = 8454143
    Else ! If ?job:Courier_Cost{prop:Req} = True
        ?job:Courier_Cost{prop:FontColor} = 65793
        ?job:Courier_Cost{prop:Color} = 16777215
    End ! If ?job:Courier_Cost{prop:Req} = True
    ?job:Courier_Cost{prop:Trn} = 0
    ?job:Courier_Cost{prop:FontStyle} = font:Bold
    ?job:Labour_Cost{prop:FontColor} = -1
    ?job:Labour_Cost{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    ?job:Parts_Cost{prop:FontColor} = -1
    ?job:Parts_Cost{prop:Color} = 15066597
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?vat_chargeable_temp{prop:FontColor} = -1
    ?vat_chargeable_temp{prop:Color} = 15066597
    ?Prompt13{prop:FontColor} = -1
    ?Prompt13{prop:Color} = 15066597
    ?total_temp{prop:FontColor} = -1
    ?total_temp{prop:Color} = 15066597
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    ?Warranty_Tab{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?JOB:Courier_Cost_Warranty:Prompt{prop:FontColor} = -1
    ?JOB:Courier_Cost_Warranty:Prompt{prop:Color} = 15066597
    If ?job:Courier_Cost_Warranty{prop:ReadOnly} = True
        ?job:Courier_Cost_Warranty{prop:FontColor} = 65793
        ?job:Courier_Cost_Warranty{prop:Color} = 15066597
    Elsif ?job:Courier_Cost_Warranty{prop:Req} = True
        ?job:Courier_Cost_Warranty{prop:FontColor} = 65793
        ?job:Courier_Cost_Warranty{prop:Color} = 8454143
    Else ! If ?job:Courier_Cost_Warranty{prop:Req} = True
        ?job:Courier_Cost_Warranty{prop:FontColor} = 65793
        ?job:Courier_Cost_Warranty{prop:Color} = 16777215
    End ! If ?job:Courier_Cost_Warranty{prop:Req} = True
    ?job:Courier_Cost_Warranty{prop:Trn} = 0
    ?job:Courier_Cost_Warranty{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?job:Labour_Cost_Warranty{prop:FontColor} = -1
    ?job:Labour_Cost_Warranty{prop:Color} = 15066597
    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    ?job:Parts_Cost_Warranty{prop:FontColor} = -1
    ?job:Parts_Cost_Warranty{prop:Color} = 15066597
    ?Prompt15{prop:FontColor} = -1
    ?Prompt15{prop:Color} = 15066597
    ?vat_warranty_temp{prop:FontColor} = -1
    ?vat_warranty_temp{prop:Color} = 15066597
    ?Prompt16{prop:FontColor} = -1
    ?Prompt16{prop:Color} = 15066597
    ?total_warranty_temp{prop:FontColor} = -1
    ?total_warranty_temp{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'View_Costs',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'View_Costs',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'View_Costs',1)
    SolaceViewVars('save_par_id',save_par_id,'View_Costs',1)
    SolaceViewVars('total_temp',total_temp,'View_Costs',1)
    SolaceViewVars('vat_chargeable_temp',vat_chargeable_temp,'View_Costs',1)
    SolaceViewVars('vat_warranty_temp',vat_warranty_temp,'View_Costs',1)
    SolaceViewVars('total_warranty_temp',total_warranty_temp,'View_Costs',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Chargeable_Tab;  SolaceCtrlName = '?Chargeable_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Courier_Cost:Prompt;  SolaceCtrlName = '?JOB:Courier_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Courier_Cost;  SolaceCtrlName = '?job:Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Labour_Cost;  SolaceCtrlName = '?job:Labour_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Parts_Cost;  SolaceCtrlName = '?job:Parts_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_chargeable_temp;  SolaceCtrlName = '?vat_chargeable_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt13;  SolaceCtrlName = '?Prompt13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_temp;  SolaceCtrlName = '?total_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12;  SolaceCtrlName = '?Prompt12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Warranty_Tab;  SolaceCtrlName = '?Warranty_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Courier_Cost_Warranty:Prompt;  SolaceCtrlName = '?JOB:Courier_Cost_Warranty:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Courier_Cost_Warranty;  SolaceCtrlName = '?job:Courier_Cost_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Labour_Cost_Warranty;  SolaceCtrlName = '?job:Labour_Cost_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14;  SolaceCtrlName = '?Prompt14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Parts_Cost_Warranty;  SolaceCtrlName = '?job:Parts_Cost_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1:2;  SolaceCtrlName = '?Line1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt15;  SolaceCtrlName = '?Prompt15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_warranty_temp;  SolaceCtrlName = '?vat_warranty_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16;  SolaceCtrlName = '?Prompt16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_warranty_temp;  SolaceCtrlName = '?total_warranty_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('View_Costs')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'View_Costs')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:DISCOUNT.Open
  Relate:VATCODE.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:WARPARTS.UseFile
  Access:PARTS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  IF job:chargeable_job = 'YES'
      Unhide(?chargeable_tab)
  End
  If job:warranty_job = 'YES'
      Unhide(?warranty_tab)
  End
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:DISCOUNT.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'View_Costs',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'View_Costs')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
          If job:invoice_number = ''
              If job:warranty_job = 'YES'
                  If job:ignore_warranty_charges <> 'YES'
                      Pricing_Routine('W',labour",parts",pass",discount")
                      If pass" = True
                          job:labour_cost_warranty    = labour"
                          job:parts_cost_warranty     = parts"
                      Else
                          job:labour_cost_warranty    = 0
                          job:parts_cost_warranty     = 0
                      End!If pass" = True
                  Else!If job:ignore_warranty_charges <> 'YES'
                      parts" = 0
                      access:chartype.clearkey(cha:charge_type_key)
                      cha:charge_type = job:warranty_charge_type
                      if access:chartype.fetch(cha:charge_type_key) = Level:Benign
                          If cha:no_charge <> 'YES'
                              save_wpr_id = access:warparts.savefile()
                              access:warparts.clearkey(wpr:part_number_key)
                              wpr:ref_number  = job:ref_number
                              set(wpr:part_number_key,wpr:part_number_key)
                              loop
                                  if access:warparts.next()
                                     break
                                  end !if
                                  if wpr:ref_number  <> job:ref_number      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  parts" += (wpr:quantity * wpr:purchase_cost)
                              end !loop
                              access:warparts.restorefile(save_wpr_id)
                              setcursor()
                          End!If cha:no_charge <> 'YES'
                      end!if access:chartype.fetch(cha:charge_type_key) = Level:Benign
                      job:parts_cost_warranty = parts"
                  End!If job:ignore_warranty_charges <> 'YES'
              End!If job:warranty_job = 'YES'
      
              If job:chargeable_job = 'YES'
                  If job:ignore_chargeable_charges <> 'YES'
                      Pricing_Routine('C',labour",parts",pass",discount")
                      If pass" = True
                          job:labour_cost = labour"
                          job:parts_cost  = parts"
                      Else
                          job:labour_cost = 0
                          job:parts_cost  = 0
                      End!If pass" = True
                  Else!If job:ignore_chargeable_charges <> 'YES'
                      parts" = 0
                      access:chartype.clearkey(cha:charge_type_key)
                      cha:charge_type = job:charge_type
                      if access:chartype.fetch(cha:charge_type_key) = Level:Benign
                          If cha:no_charge <> 'YES'
                              setcursor(cursor:wait)
                              save_par_id = access:parts.savefile()
                              access:parts.clearkey(par:part_number_key)
                              par:ref_number  = job:ref_number
                              set(par:part_number_key,par:part_number_key)
                              loop
                                  if access:parts.next()
                                     break
                                  end !if
                                  if par:ref_number  <> job:ref_number      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  parts" += (par:quantity * par:sale_cost)
                              end !loop
                              access:parts.restorefile(save_par_id)
                              setcursor()
                          End!If cha:no_charge <> 'YES'
                      end!if access:chartype.fetch(cha:charge_type_key) = Level:Benign
                      job:parts_cost = parts"
                  End!If job:ignore_chargeable_charges
              End!If job:chargeable_job = 'YES'
      
              Total_Price('C',vat",total",balance")
              job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
              total_temp = total"
              vat_chargeable_temp = vat"
      
              Total_Price('W',vat",total",balance")
              job:sub_total_warranty  = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
              vat_warranty_temp = vat"
              total_warranty_temp = total"
              access:jobs.update()
              Display()
          End!If job:invoice_number = ''
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

