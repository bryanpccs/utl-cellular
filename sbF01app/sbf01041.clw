

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01041.INC'),ONCE        !Local module procedure declarations
                     END


Scan_Batch_Despatch PROCEDURE (f_batch_Number)        !Generated from procedure template - Window

tmp:courier          STRING(30)
tmp:AccountNO        STRING(20)
tmp:ConsignmentNo    STRING(30)
tmp:DespatchDate     DATE
tmp:BeginDespatch    BYTE(0)
tmp:JobNumber        LONG
tmp:ESN              STRING(16)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Batch Despatch'),AT(,,220,204),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,108),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Select the Courier, Consignment Number and Despatch Date for the Selected Batch'),AT(8,8,204,24),USE(?Prompt1),FONT(,10,COLOR:Navy,FONT:bold)
                           PROMPT('Then click ''Begin Despatch'' to start scanning on the jobs to despatch.'),AT(8,84,204,24),USE(?Prompt7),FONT(,10,COLOR:Navy,FONT:bold)
                           COMBO(@s30),AT(84,32,124,10),USE(tmp:courier),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Consignment Number'),AT(8,48),USE(?tmp:ConsignmentNo:Prompt)
                           ENTRY(@s30),AT(84,48,124,10),USE(tmp:ConsignmentNo),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Despatch Date'),AT(8,64),USE(?tmp:DespatchDate:Prompt)
                           ENTRY(@d6b),AT(84,64,64,10),USE(tmp:DespatchDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           BUTTON,AT(152,64,10,10),USE(?PopCalendar),SKIP,ICON('calenda2.ico')
                           PROMPT('Courier'),AT(8,32),USE(?Prompt2)
                         END
                       END
                       CHECK('Begin Despatch'),AT(84,124),USE(tmp:BeginDespatch),VALUE('1','0')
                       SHEET,AT(4,116,212,56),USE(?Sheet2),DISABLE,WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Job Number'),AT(8,140),USE(?tmp:JobNumber:Prompt)
                           ENTRY(@s8),AT(84,140,64,10),USE(tmp:JobNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('E.S.N. / I.M.E.I.'),AT(8,156),USE(?tmp:ESN:Prompt)
                           ENTRY(@s16),AT(84,156,124,10),USE(tmp:ESN),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(4,176,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Finish'),AT(156,180,56,16),USE(?FInish),LEFT,ICON('thumbs.gif'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?tmp:courier{prop:ReadOnly} = True
        ?tmp:courier{prop:FontColor} = 65793
        ?tmp:courier{prop:Color} = 15066597
    Elsif ?tmp:courier{prop:Req} = True
        ?tmp:courier{prop:FontColor} = 65793
        ?tmp:courier{prop:Color} = 8454143
    Else ! If ?tmp:courier{prop:Req} = True
        ?tmp:courier{prop:FontColor} = 65793
        ?tmp:courier{prop:Color} = 16777215
    End ! If ?tmp:courier{prop:Req} = True
    ?tmp:courier{prop:Trn} = 0
    ?tmp:courier{prop:FontStyle} = font:Bold
    ?tmp:ConsignmentNo:Prompt{prop:FontColor} = -1
    ?tmp:ConsignmentNo:Prompt{prop:Color} = 15066597
    If ?tmp:ConsignmentNo{prop:ReadOnly} = True
        ?tmp:ConsignmentNo{prop:FontColor} = 65793
        ?tmp:ConsignmentNo{prop:Color} = 15066597
    Elsif ?tmp:ConsignmentNo{prop:Req} = True
        ?tmp:ConsignmentNo{prop:FontColor} = 65793
        ?tmp:ConsignmentNo{prop:Color} = 8454143
    Else ! If ?tmp:ConsignmentNo{prop:Req} = True
        ?tmp:ConsignmentNo{prop:FontColor} = 65793
        ?tmp:ConsignmentNo{prop:Color} = 16777215
    End ! If ?tmp:ConsignmentNo{prop:Req} = True
    ?tmp:ConsignmentNo{prop:Trn} = 0
    ?tmp:ConsignmentNo{prop:FontStyle} = font:Bold
    ?tmp:DespatchDate:Prompt{prop:FontColor} = -1
    ?tmp:DespatchDate:Prompt{prop:Color} = 15066597
    If ?tmp:DespatchDate{prop:ReadOnly} = True
        ?tmp:DespatchDate{prop:FontColor} = 65793
        ?tmp:DespatchDate{prop:Color} = 15066597
    Elsif ?tmp:DespatchDate{prop:Req} = True
        ?tmp:DespatchDate{prop:FontColor} = 65793
        ?tmp:DespatchDate{prop:Color} = 8454143
    Else ! If ?tmp:DespatchDate{prop:Req} = True
        ?tmp:DespatchDate{prop:FontColor} = 65793
        ?tmp:DespatchDate{prop:Color} = 16777215
    End ! If ?tmp:DespatchDate{prop:Req} = True
    ?tmp:DespatchDate{prop:Trn} = 0
    ?tmp:DespatchDate{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?tmp:BeginDespatch{prop:Font,3} = -1
    ?tmp:BeginDespatch{prop:Color} = 15066597
    ?tmp:BeginDespatch{prop:Trn} = 0
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?tmp:JobNumber:Prompt{prop:FontColor} = -1
    ?tmp:JobNumber:Prompt{prop:Color} = 15066597
    If ?tmp:JobNumber{prop:ReadOnly} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 15066597
    Elsif ?tmp:JobNumber{prop:Req} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 8454143
    Else ! If ?tmp:JobNumber{prop:Req} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 16777215
    End ! If ?tmp:JobNumber{prop:Req} = True
    ?tmp:JobNumber{prop:Trn} = 0
    ?tmp:JobNumber{prop:FontStyle} = font:Bold
    ?tmp:ESN:Prompt{prop:FontColor} = -1
    ?tmp:ESN:Prompt{prop:Color} = 15066597
    If ?tmp:ESN{prop:ReadOnly} = True
        ?tmp:ESN{prop:FontColor} = 65793
        ?tmp:ESN{prop:Color} = 15066597
    Elsif ?tmp:ESN{prop:Req} = True
        ?tmp:ESN{prop:FontColor} = 65793
        ?tmp:ESN{prop:Color} = 8454143
    Else ! If ?tmp:ESN{prop:Req} = True
        ?tmp:ESN{prop:FontColor} = 65793
        ?tmp:ESN{prop:Color} = 16777215
    End ! If ?tmp:ESN{prop:Req} = True
    ?tmp:ESN{prop:Trn} = 0
    ?tmp:ESN{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Scan_Batch_Despatch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:courier',tmp:courier,'Scan_Batch_Despatch',1)
    SolaceViewVars('tmp:AccountNO',tmp:AccountNO,'Scan_Batch_Despatch',1)
    SolaceViewVars('tmp:ConsignmentNo',tmp:ConsignmentNo,'Scan_Batch_Despatch',1)
    SolaceViewVars('tmp:DespatchDate',tmp:DespatchDate,'Scan_Batch_Despatch',1)
    SolaceViewVars('tmp:BeginDespatch',tmp:BeginDespatch,'Scan_Batch_Despatch',1)
    SolaceViewVars('tmp:JobNumber',tmp:JobNumber,'Scan_Batch_Despatch',1)
    SolaceViewVars('tmp:ESN',tmp:ESN,'Scan_Batch_Despatch',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:courier;  SolaceCtrlName = '?tmp:courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ConsignmentNo:Prompt;  SolaceCtrlName = '?tmp:ConsignmentNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ConsignmentNo;  SolaceCtrlName = '?tmp:ConsignmentNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DespatchDate:Prompt;  SolaceCtrlName = '?tmp:DespatchDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DespatchDate;  SolaceCtrlName = '?tmp:DespatchDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BeginDespatch;  SolaceCtrlName = '?tmp:BeginDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobNumber:Prompt;  SolaceCtrlName = '?tmp:JobNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobNumber;  SolaceCtrlName = '?tmp:JobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ESN:Prompt;  SolaceCtrlName = '?tmp:ESN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ESN;  SolaceCtrlName = '?tmp:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FInish;  SolaceCtrlName = '?FInish';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Scan_Batch_Despatch')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Scan_Batch_Despatch')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:EXCHANGE.Open
  Access:EXCHHIST.UseFile
  Access:LOCATION.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  tmp:despatchdate = Today()
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?tmp:DespatchDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  FDCB2.Init(tmp:courier,?tmp:courier,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(cou:Courier_Key)
  FDCB2.AddField(cou:Courier,FDCB2.Q.cou:Courier)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:EXCHANGE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Scan_Batch_Despatch',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?tmp:BeginDespatch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:BeginDespatch, Accepted)
      error# = 0
      If tmp:courier = ''
          error# = 1
          Select(?tmp:courier)
      End!If tmp:courier = ''
      If tmp:consignmentno = '' And error# = 0
          error# = 1
          Select(?tmp:ConsignmentNo)
      End!If tmp:consignmentno = '' And error# = 0
      If tmp:DespatchDate = '' and error# = 0
          error# = 1
          Select(?tmp:DespatchDate)
      End!If tmp:DespatchDate = '' and error# = 0
      If error# = 0
          Case MessageEx('Are you sure you are ready to begin scanning jobs for despatch?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
      
                  Disable(?Sheet1)
                  Enable(?Sheet2)
                  Disable(?tmp:BeginDespatch)
                  access:desbatch.primerecord()
                  access:desbatch.tryinsert()
                  !thismakeover.setwindow(win:window)
      
              Of 2 ! &No Button
          End!Case MessageEx
      
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:BeginDespatch, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:DespatchDate = TINCALENDARStyle1(tmp:DespatchDate)
          Display(?tmp:DespatchDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ESN, Accepted)
      error# = 0
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = tmp:JobNumber
      if access:jobs.tryfetch(job:ref_number_key)
          Case MessageEx('Unable to find selected Job Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
                  error# = 1
                  Select(?tmp:JobNumber)
          End!Case MessageEx
      Else!if access:jobs.tryfetch(job:ref_number_key)
          If job:batch_number <> f_batch_number
              error# = 1
              Case MessageEx('The selected Job Number is not part of the selected Batch.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Select(?tmp:JobNumber)
          Else!If job:batch_number <> f_batch_number
              If tmp:accountno = ''
                  tmp:accountno = job:account_number
              End!If tmp:accountno = ''
              If job:exchange_unit_number <> ''        
                  access:exchange.clearkey(xch:ref_number_key)
                  xch:ref_number = job:exchange_unit_number
                  if access:exchange.tryfetch(xch:ref_number_key)
                      Case MessageEx('The E.S.N. / I.M.E.I. does not match the selected Job Number.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                      Select(?tmp:esn)
                  Else!if access:exchange.tryfetch(xch:ref_number_key)
                      If xch:esn <> tmp:esn
                          Case MessageEx('The E.S.N. / I.M.E.I. does not match the selected Job Number.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          error# = 1
                          Select(?tmp:esn)
                      Else!If xch:esn <> tmp:esn
                          job:exchange_consignment_number = tmp:consignmentno
                          job:exchange_despatched = tmp:despatchdate
                          job:exchange_despatch_number    = dbt:batch_number
                          job:exchange_courier    = tmp:courier
                          job:despatched = ''
                          job:exchange_status = 'DESPATCHED'
                          access:users.clearkey(use:password_key)
                          use:password    =glo:password
                          access:users.tryfetch(use:password_key)
                          job:exchange_despatched_user    = use:user_code
                          !call the exchange status routine
                          GetStatus(125,1,'EXC') !exchange unit despatched
      
                          access:jobs.update()
      
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:notes         = 'BATCH NUMBER: ' & CLip(f_batch_number) & |
                                                  '<13,10>COURIER: ' & Clip(job:exchange_courier) &|
                                                  '<13,10>CONSIGNMENT NOTE NUMBER: ' & Clip(job:exchange_consignment_number)
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'BATCH DESPATCH: EXCHANGE UNIT'
                              aud:type          = 'EXC'
                              access:audit.insert()
                          end!�if access:audit.primerecord() = level:benign
      
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number  = job:Exchange_unit_number
                          If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                              xch:available = 'DES'
                              access:exchange.update()
                              Get(exchhist,0)
                              If access:exchhist.primerecord() = Level:Benign
                                  exh:ref_Number  = xch:ref_number
                                  exh:date        = Today()
                                  exh:time        = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password    =glo:password
                                  access:users.fetch(use:password_key)
                                  exh:user    = use:user_code
                                  If access:exchhist.insert()
                                      access:exchhist.cancelautoinc()
                                  End!If access:exchhist.insert()
                              End!If access:exchhist.primerecord() = Level:Benign
                          End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
      
                      End!If xch:esn <> tmp:esn
                  End!if access:exchange.tryfetch(xch:ref_number_key)
              Else!If job:exchange_unit_number <> ''
                  If job:esn <> tmp:esn
                      Case MessageEx('The E.S.N. / I.M.E.I. does not match the selected Job Number.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                      Select(?tmp:esn)
                  Else!If job:esn <> tmp:esn
                      job:Consignment_number  = tmp:ConsignmentNo
                      job:date_Despatched     = tmp:DespatchDate
                      job:courier             = tmp:Courier
                      job:despatch_number     = dbt:batch_number
                      job:despatched          = 'YES'
                      access:users.clearkey(use:password_key)
                      use:password    =glo:password
                      access:users.fetch(use:password_key)
                      job:despatch_user   = use:user_code
                      If job:paid = 'YES'
                          !call the status routine
                          GetStatus(910,1,'JOB') !despatched paid
      
                      Else!If job:paid = 'YES'
                          !call the status routine
                          GetStatus(905,1,'JOB') !despatched unpaid
      
                      End!If job:paid = 'YES'
                      access:jobs.update()
      
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:notes         = 'BATCH NUMBER: ' & CLip(f_batch_number) & |
                                              '<13,10>COURIER: ' & Clip(job:courier) &|
                                              '<13,10>CONSIGNMENT NOTE NUMBER: ' & Clip(job:consignment_number)
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'BATCH DESPATCH: JOB'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
      
                      access:locinter.clearkey(loi:location_key)
                      loi:location    = job:location
                      If access:locinter.tryfetch(loi:location_key)
                          If loi:allocate_spaces = 'YES'
                              loi:current_spaces += 1
                              loi:location_available = 'YES'
                              access:locinter.update()
                          End!If loi:allocate_spaces = 'YES'
                      End!If access:locinter.tryfetch(loi:location_key)
      
                  End!If job:esn <> tmp:esn
              End!If job:exchange_unit_number <> ''
          End!If job:batch_number <> f_batch_number
      End!if access:jobs.tryfetch(job:ref_number_key)
      If error# = 0
          tmp:JobNumber   = ''
          tmp:Esn         = ''
          Display()
          Select(?tmp:JobNumber)
      End!If error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ESN, Accepted)
    OF ?FInish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FInish, Accepted)
      glo:select1  = tmp:accountNo
      glo:select2  = f_batch_number
      glo:select3  = tmp:ConsignmentNo
      glo:select4  = tmp:Courier
      glo:select5  = dbt:batch_number
      Batch_Despatch_Note
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      glo:select4  = ''
      glo:select5  = ''
      Post(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FInish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Scan_Batch_Despatch')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:DespatchDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

