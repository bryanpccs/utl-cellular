

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01040.INC'),ONCE        !Local module procedure declarations
                     END


ANC_Despatch_Routine PROCEDURE (f_type)               !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
start_consignment_number_temp LONG
current_consignment_number_temp LONG
sav:path             STRING(255)
path_temp            STRING(255)
save_job_id          USHORT,AUTO
save_cou_id          USHORT,AUTO
despatch_date_temp   DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('ANC Despatch Routine'),AT(,,220,75),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,40),USE(?Sheet1),SPREAD
                         TAB('ANC Despatch Routine'),USE(?Tab1)
                           PROMPT('Despatch Date'),AT(8,24),USE(?despatch_date_temp:Prompt)
                           ENTRY(@d6),AT(84,24,64,10),USE(despatch_date_temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,24,10,10),USE(?PopCalendar),ICON('calenda2.ico')
                         END
                       END
                       BUTTON('&Create Export'),AT(8,52,56,16),USE(?OkButton),LEFT,ICON('desp_sm.gif'),DEFAULT
                       BUTTON('Close'),AT(156,52,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,48,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(filename3),CREATE,BINDABLE,THREAD ! Record length 1016
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(255)
Line2           STRING(213)
          . . .
in_File FILE,DRIVER('BASIC'),PRE(inf),NAME(filename4),CREATE,BINDABLE,THREAD ! Record length 1016
RECORD      RECORD
desp_no     STRING(11)
job_no      STRING(20)
          . .

Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
cont_no      STRING(8)
comma1       STRING(1)
acc_no       STRING(14)
comma2       STRING(1)
Name         STRING(25)
comma3       STRING(1)
Add1         STRING(25)
comma4       STRING(1)
Add2         STRING(25)
comma5       STRING(1)
Add3         STRING(25)
comma6       STRING(1)
location     STRING(25)
comma7       STRING(1)
Post_town    STRING(25)
comma8       STRING(1)
County       STRING(25)
comma9       STRING(1)
postcode     STRING(4)
comma10      STRING(1)
telephone    STRING(25)
comma11      STRING(1)
contact      STRING(25)
comma12      STRING(1)
Serv_code    STRING(3)
comma13      STRING(1)
date         STRING(8)
comma14      STRING(1)
items        STRING(3)
comma15      STRING(1)
weight       STRING(3)
comma16      STRING(1)
Desp_ref1    STRING(15)
comma17      STRING(1)
Notes1       STRING(32)
comma18      STRING(1)
Notes2       STRING(32)
comma19      STRING(1)
Notes3       STRING(32)
comma20      STRING(1)
carr_code    STRING(1)
comma21      STRING(1)
carr_desc    STRING(25)
comma22      STRING(1)
haz_flag     STRING(1)
comma23      STRING(1)
pack_group   STRING(10)
comma24      STRING(1)
haz_class    STRING(10)
comma25      STRING(1)
desp_ref2    STRING(15)
comma26      STRING(1)
          .
Out_Part   GROUP,OVER(Ouf:Out_Group),PRE(L2)
no_rec       STRING(4)
comma27      STRING(1)
batch        STRING(3)
comma28      STRING(1)
date1        STRING(8)
comma29      STRING(1)
pad          STRING(195)
pad1         STRING(255)
         .

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?despatch_date_temp:Prompt{prop:FontColor} = -1
    ?despatch_date_temp:Prompt{prop:Color} = 15066597
    If ?despatch_date_temp{prop:ReadOnly} = True
        ?despatch_date_temp{prop:FontColor} = 65793
        ?despatch_date_temp{prop:Color} = 15066597
    Elsif ?despatch_date_temp{prop:Req} = True
        ?despatch_date_temp{prop:FontColor} = 65793
        ?despatch_date_temp{prop:Color} = 8454143
    Else ! If ?despatch_date_temp{prop:Req} = True
        ?despatch_date_temp{prop:FontColor} = 65793
        ?despatch_date_temp{prop:Color} = 16777215
    End ! If ?despatch_date_temp{prop:Req} = True
    ?despatch_date_temp{prop:Trn} = 0
    ?despatch_date_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ANC_Despatch_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('start_consignment_number_temp',start_consignment_number_temp,'ANC_Despatch_Routine',1)
    SolaceViewVars('current_consignment_number_temp',current_consignment_number_temp,'ANC_Despatch_Routine',1)
    SolaceViewVars('sav:path',sav:path,'ANC_Despatch_Routine',1)
    SolaceViewVars('path_temp',path_temp,'ANC_Despatch_Routine',1)
    SolaceViewVars('save_job_id',save_job_id,'ANC_Despatch_Routine',1)
    SolaceViewVars('save_cou_id',save_cou_id,'ANC_Despatch_Routine',1)
    SolaceViewVars('despatch_date_temp',despatch_date_temp,'ANC_Despatch_Routine',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_date_temp:Prompt;  SolaceCtrlName = '?despatch_date_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_date_temp;  SolaceCtrlName = '?despatch_date_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ANC_Despatch_Routine')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ANC_Despatch_Routine')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?despatch_date_temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:STATUS.Open
  Access:COURIER.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:LOCINTER.UseFile
  Access:USERS.UseFile
  Access:EXCHHIST.UseFile
  Access:LOANHIST.UseFile
  Access:LOAN.UseFile
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?despatch_date_temp{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:STATUS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ANC_Despatch_Routine',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          despatch_date_temp = TINCALENDARStyle1(despatch_date_temp)
          Display(?despatch_date_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !current_Consignment_number_temp = start_consignment_number_temp
      !file_name = 'C:\ANC.TXT'
      !Remove(expgen)
      !If access:expgen.open()
      !    Stop(error())
      !    Return Level:Benign
      !End!If access:expgen.open()
      !access:expgen.usefile()
      
      count_import# = 0
      count# = 0
      setcursor(cursor:wait)
      save_cou_id = access:courier.savefile()
      access:courier.clearkey(cou:courier_type_key)
      cou:courier_type = 'ANC'
      set(cou:courier_type_key,cou:courier_type_key)
      loop
          if access:courier.next()
             break
          end !if
          if cou:courier_type <> 'ANC'      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
      
          filename3 = Clip(cou:export_path) & '\ANC' & Format(cou:ANCCount,@n03) & '.TXT'
      
          filename4   = CLip(cou:import_path) & '\CONS.TXT'
      
          file_error# = 0             !Can any of the import files be opened
          file_Type# = 0              !Which file is it? 1= CONS.TXT, 2= CONS.BAK
      
          Open(in_file)
          If Error()
              filename4   = Clip(cou:import_path) & '\CONS.BAK'
              Open(in_file)
              If Error()
                  file_error# = 1
              Else
                  file_type# = 2
              End!If Error()
          Else
              file_Type# = 1
          End
      
          IF file_error# = 0
              recordspercycle     = 25
              recordsprocessed    = 0
              percentprogress     = 0
              setcursor(cursor:wait)
              open(progresswindow)
              progress:thermometer    = 0
              ?progress:pcttext{prop:text} = '0% Completed'
              ?progress:userstring{prop:text} = 'Reading Import File...'
              count_records# = 0
      
              Set(in_file,0)
              Loop Until Eof(in_file)
                  Next(in_File)
                  If Error()
                      Break
                  End!If Error()
                  count_records# += 1
              End
      
              recordstoprocess    = count_records#
      
              job_no" = ''
              Set(in_file,0)
              Loop Until Eof(in_file)
                  Next(in_File)
                  If Error()
                      Break
                  End!If Error()
                  Do GetNextRecord2
      
                  If ISALPHA(Sub(inf:job_no,1,1))
                      job_no" = Clip(Sub(inf:job_no,2,Len(inf:job_no)))
                  Else!If ISALPHA(Sub(inf:job_no,1,1))
                      job_no" = Clip(inf:job_no)
                  End!If ISALPHA(Sub(inf:job_no,1,1))
      
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number = job_no"
                  if access:jobs.tryfetch(job:ref_number_key) = LeveL:Benign
                      count_import# += 1
                      If job:exchange_unit_number <> ''
                          If job:exchange_consignment_number = ''
                              job:exchange_consignment_number = Clip(Upper(inf:desp_no))
                              If job:workshop <> 'YES' and job:third_party_site   = ''
                                  GetStatus(116,1,'JOB')
      
                              End!If job:workshop <> 'YES'
                              !call the exchange status routine
                              GetStatus(901,1,'EXC') !Despatched
      
      
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                                  aud:ref_number    = job:ref_number
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  aud:action        = 'ANC EXPORT CONFIRMATION: EXCHANGE UNIT DESPATCHED'
                                  aud:notes         = 'DESPATCH NOTE NUMBER: ' & Clip(Upper(inf:desp_no))
                                  aud:type          = 'EXC'
                                  if access:audit.insert()
                                      access:audit.cancelautoinc()
                                  end
                              end!if access:audit.primerecord() = level:benign
                          End!IF job:exchange_consignment_number = ''
                      Else!If job:exchange_unit_number <> ''
                          If job:date_completed <> ''
                              If job:consignment_number = ''
                                  job:consignment_number = Clip(Upper(inf:desp_no))
                                  If job:paid = 'YES'
                                      !call the status routine
                                      GetStatus(910,1,'JOB') !despatched paid
      
                                  Else!If job:paid = 'YES'
                                      !call the status routine
                                      GetStatus(905,1,'JOB') !despatched unpadi
      
                                  End!If job:paid = 'YES'
      
                                  get(audit,0)
                                  if access:audit.primerecord() = level:benign
                                      aud:ref_number    = job:ref_number
                                      aud:date          = Today()
                                      aud:time          = Clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      aud:user = use:user_code
                                      aud:action        = 'ANC EXPORT CONFIRMATION: JOB DESPATCHED'
                                      aud:notes         = 'DESPATCH NOTE NUMBER: ' & Clip(Upper(inf:desp_no))
                                      if access:audit.insert()
                                          access:audit.cancelautoinc()
                                      end
                                  end!if access:audit.primerecord() = level:benign
                              End!If job:consignment_number = ''
                          Else
                              If job:loan_unit_number <> ''
                                  If job:loan_consignment_number = ''
                                      job:loan_consignment_number = Clip(Upper(inf:desp_no))
                                      !call the status routine
                                      If job:workshop <> 'YES' and job:third_party_site   = ''
                                          GetStatus(117,1,'JOB') !awaiting arrival (loan)
      
                                      End!If job:workshop <> 'YES'
                                      GetStatus(901,1,'LOA') !Loan Unit Despatched
      
                                      get(audit,0)
                                      if access:audit.primerecord() = level:benign
                                          aud:ref_number    = job:ref_number
                                          aud:date          = Today()
                                          aud:time          = Clock()
                                          access:users.clearkey(use:password_key)
                                          use:password =glo:password
                                          access:users.fetch(use:password_key)
                                          aud:user = use:user_code
                                          aud:action        = 'ANC EXPORT CONFIRMATION: LOAN UNIT DESPATCHED'
                                          aud:notes         = 'DESPATCH NOTE NUMBER: ' & Clip(Upper(inf:desp_no))
                                          aud:type          = 'LOA'
                                          if access:audit.insert()
                                              access:audit.cancelautoinc()
                                          end
                                      end!if access:audit.primerecord() = level:benign
                                  End!If job:loan_unit_number = ''
      
                              End!If job:loan_unit_number <> ''
                          End!If job:date_completed <> ''
                      End!If job:exchange_unit_number <> ''
      
                      access:jobs.update()
                      COU:ICOLSuffix += 1
                      If cou:ICOLSuffix = 999
                          cou:ICOLSuffix = 1
                      End!If cou:ICOLSuffix = 999
                      access:Courier.update()
                  end!if access:jobs.tryfetch(job:ref_number_key) = LeveL:Benign
              End!Loop Until Eof(in_file)
              Close(in_File)
              If file_Type# = 2
                  Rename(in_file,CLip(cou:import_path) & '\CONS.OLD')
              End
              setcursor()
              close(progresswindow)
          End!If ~Error()
      
          setcursor(cursor:wait)
          save_job_id = access:jobs.savefile()
          access:jobs.clearkey(job:DateDespatchKey)
      
          save_job_id = access:jobs.savefile()
          access:jobs.clearkey(job:ReadyToCouKey)
          job:despatched      = 'REA'
          job:current_courier = cou:courier
          set(job:ReadyToCouKey,job:ReadyToCouKey)
          loop
              if access:jobs.next()
                 break
              end !if
              if job:despatched      <> 'REA'      |
              or job:current_courier <> cou:courier      |
                  then break.  ! end if
              Case f_type
                  Of 'DELIVER'
                      If job:despatch_type = 'EXC' Or job:despatch_type = 'LOA'
                          If job:workshop <> 'YES'
                              If job:third_party_site = ''
                                  Cycle
                              End!If job:third_party_site <> ''
                          End!If job:workshop <> 'YES'
                      End!If job:despatch_type = 'EXC'
                      If job:despatch_Type = 'JOB'
                          If job:loan_unit_number <> ''
                              Cycle
                          End!If job:loan_unit_number <> ''
                      End!If job:despatch_Type = 'JOB'
                  Of 'COLLECT'
                      If job:despatch_type = 'EXC' Or job:despatch_type = 'LOA'
                          If job:workshop = 'YES'
                              Cycle
                          Else!If job:workshop = 'YES'
                              If job:third_party_site <> ''
                                  Cycle
                              End!If job:third_party_site <> ''
                          End!If job:workshop = 'YES'
                      End!If job:despatch_type = 'EXC'
                      If job:despatch_type = 'JOB'
                          If job:loan_unit_number = ''
                              Cycle
                          End!If job:loan_unit_number = ''
                      End!If job:despatch_type = 'JOB'
              End!Case f_type
              Case job:despatch_type
                  Of 'JOB'
                      If job:date_despatched = despatch_date_temp
                          count# += 1
                      End!If job:date_despatched <> date_despatched_temp
                  Of 'LOA'
                      If job:loan_despatched = despatch_date_temp
                          count# += 1
                      End!If job:date_despatched <> date_despatched_temp
      
                  Of 'EXC'
                      If job:exchange_despatched = despatch_date_temp
                          count# += 1
                      End!If job:date_despatched <> date_despatched_temp
      
              End!Case job:despatch_type
      
      
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
      end !loop
      access:courier.restorefile(save_cou_id)
      setcursor()
      
      Case MessageEx('The Import File has been read.<13,10><13,10>(This message will close automatically)','ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,400) 
          Of 1 ! &OK Button
      End!Case MessageEx
      
      
      If count# <> 0
      
          Remove(OUT_FILE)
          Open(out_file)
          If error()
              Create(out_file)
              Open(out_file)
          Else
              Empty(out_file)
          End!If error()
          first# = 1
          recordspercycle     = 25
          recordsprocessed    = 0
          percentprogress     = 0
          setcursor(cursor:wait)
          open(progresswindow)
          progress:thermometer    = 0
          ?progress:pcttext{prop:text} = '0% Completed'
          ?progress:userstring{prop:text} = 'Writing Output File...'
          recordstoprocess = count#
      
          save_cou_id = access:courier.savefile()
          access:courier.clearkey(cou:courier_type_key)
          cou:courier_type = 'ANC'
          set(cou:courier_type_key,cou:courier_type_key)
          loop
              if access:courier.next()
                 break
              end !if
              if cou:courier_type <> 'ANC'      |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              If first# = 1
                  Clear(ouf:record)
                  l2:no_rec = Format(count#,@n04)
                  l2:comma27 = ','
                  l2:batch  = Format(cou:ANCCount,@n03)
                  l2:comma28  = ','
                  l2:date1  = Format(Day(Today()),@n02) & '/' & Format(Month(Today()),@n02) & '/' & Format(Sub(Year(Today()),3,2),@n02)
                  l2:comma29 = ','
                  l2:pad      = ''
                  l2:pad1     = ''
                  Add(out_file)
                  first# = 0
              End!If first# = 1
      
              count# = 0
      
              save_job_id = access:jobs.savefile()
              access:jobs.clearkey(job:ReadyToCouKey)
              job:despatched      = 'REA'
              job:current_courier = cou:courier
              set(job:ReadyToCouKey,job:ReadyToCouKey)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  if job:despatched      <> 'REA'      |
                  or job:current_courier <> cou:courier      |
                      then break.  ! end if
                  Case f_type
                      Of 'DELIVER'
                          If job:despatch_type = 'EXC' Or job:despatch_type = 'LOA'
                              If job:workshop <> 'YES'
                                  If job:third_party_site = ''
                                      Cycle
                                  End!If job:third_party_site <> ''
                              End!If job:workshop <> 'YES'
                          End!If job:despatch_type = 'EXC'
                          If job:despatch_Type = 'JOB'
                              If job:loan_unit_number <> ''
                                  Cycle
                              End!If job:loan_unit_number <> ''
                          End!If job:despatch_Type = 'JOB'
                      Of 'COLLECT'
                          If job:despatch_type = 'EXC' Or job:despatch_type = 'LOA'
                              If job:workshop = 'YES'
                                  Cycle
                              Else!If job:workshop = 'YES'
                                  If job:third_party_site <> ''
                                      Cycle
                                  End!If job:third_party_site <> ''
                              End!If job:workshop = 'YES'
                          End!If job:despatch_type = 'EXC'
                          If job:despatch_type = 'JOB'
                              If job:loan_unit_number = ''
                                  Cycle
                              End!If job:loan_unit_number = ''
                          End!If job:despatch_type = 'JOB'
                  End!Case f_type
                  Case job:despatch_type
                      Of 'JOB'
                          If job:date_despatched = despatch_date_temp
                              count# += 1
                          Else
                              Cycle
                          End!If job:date_despatched <> date_despatched_temp
                      Of 'LOA'
                          If job:loan_despatched = despatch_date_temp
                              count# += 1
                          Else
                              Cycle
                          End!If job:date_despatched <> date_despatched_temp
      
                      Of 'EXC'
                          If job:exchange_despatched = despatch_date_temp
                              count# += 1
                          Else
                              Cycle
                          End!If job:date_despatched <> date_despatched_temp
      
                  End!Case job:despatch_type
      
                  Do GetNextRecord2
      
                  Clear(ouf:record)
                  l1:cont_no = Format(cou:ContractNumber,@s8)
                  l1:comma1  = ','
                  l1:acc_no  = Format(job:account_number,@s14)
                  l1:comma2  = ','
                  l1:name            = Format(job:company_name_delivery,@s25)
                  l1:comma3  = ','
                  l1:add1            = Format(job:address_line1_delivery,@s25)
                  l1:comma4  = ','
                  l1:add2            = Format(job:address_line2_delivery,@s25)
                  l1:comma5  = ','
                  l1:add3            = Format('',@s25)
                  l1:comma6  = ','
                  l1:location        = Format('',@s25)
                  l1:comma7  = ','
                  l1:post_town       = Format(job:address_line3_delivery,@s25)
                  l1:comma8  = ','
                  l1:county          = Format('',@s25)
                  l1:comma9  = ','
                  l1:postcode        = Format(job:postcode_delivery,@s4)
                  l1:comma10  = ','
                  l1:telephone       = Format(job:telephone_delivery,@s25)
                  l1:comma11  = ','
                  l1:contact         = Format('',@s25)
                  l1:comma12  = ','
                  l1:serv_code       = Format('A',@s3)
                  l1:comma13  = ','
                  l1:date            = Format(Format(Day(Today()),@n02) & '/' & Format(Month(Today()),@n02) & '/' & Format(Sub(Year(Today()),3,2),@n02),@s8)
                  l1:comma14  = ','
                  l1:items           = Format('001',@s3)
                  l1:comma15  = ','
                  l1:weight          = Format('001',@s3)
                  l1:comma16  = ','
                  in_string"       = Format(job:ref_number,@s15)
                  in_string" = CLIP(LEFT(in_string"))
      
                  STR_LEN#  = LEN(in_string")
                  STR_POS#  = 1
      
                  in_string" = UPPER(SUB(in_string",STR_POS#,1)) & SUB(in_string",STR_POS#+1,STR_LEN#-1)
      
                  LOOP STR_POS# = 1 TO STR_LEN#
      
                   IF SUB(in_string",STR_POS#,1) = '.'
                      in_string" = SUB(in_string",1,STR_POS#-1) & SUB(in_string",STR_POS#+1,STR_LEN#-1)
                   .
                  .
                  l1:desp_ref1    = Format(in_string",@s15)
      
                  l1:comma17  = ','
                  Case f_type
                      Of 'COLLECT'
                          l1:notes1   = Format('Collect When Delivery',@s32)
                      Of 'DELIVER'
                          l1:notes1          = Format('',@s32)
                  End!Case f_type
                  
                  l1:comma18  = ','
                  l1:notes2          = Format('',@s32)
                  l1:comma19  = ','
                  l1:notes3          = Format('',@s32)
                  l1:comma20  = ','
                  l1:carr_code       = Format('A',@s1)
                  l1:comma21  = ','
                  l1:carr_desc       = Format('',@s25)
                  l1:comma22  = ','
                  l1:haz_flag        = Format('',@s1)
                  l1:comma23  = ','
                  l1:pack_group      = Format('',@s10)
                  l1:comma24  = ','
                  l1:haz_class       = Format('',@s10)
                  l1:comma25  = ','
                  l1:desp_ref2       = Format('',@s15)
                  l1:comma26  = ','
                  Add(out_file)
                  Case job:despatch_type
                      Of 'JOB'
                          job:despatch_number = ''!dbt:batch_number
                          job:despatched = 'YES'
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          job:despatch_user = use:user_code
                          !call the status routine
                          GetStatus(820,1,'JOB') !awaiting desp. confirm
      
      
                      OF 'LOA'
                          job:loan_despatch_number = ''!dbt:batch_number
                          job:despatched = ''
                          job:loan_status = 'DESPATCHED'
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          job:loan_despatched_user = use:user_code
                          !call the loan status routine
                          GetStatus(820,1,'LOA') !Awaiting Despatch Confimation
      
                          access:loan.clearkey(loa:ref_number_key)
                          loa:ref_number = job:loan_unit_number
                          if access:loan.fetch(loa:ref_number_key) = Level:Benign
                              loa:available = 'DES'
                              access:loan.update()                                        !Make Exchange Unit Available
                              get(loanhist,0)
                              if access:loanhist.primerecord() = Level:Benign
                                  loh:ref_number    = loa:ref_number
                                  loh:date          = Today()
                                  loh:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  loh:user = use:user_code
                                  loh:status        = 'UNIT DESPATCHED ON JOB: ' & CLip(job:Ref_number)
                                  if access:loanhist.insert()
                                     access:loanhist.cancelautoinc()
                                  end
                              end!if access:exchhist.primerecord() = Level:Benign
                          end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
      
                      Of 'EXC'
                          job:exchange_despatch_number = ''!dbt:batch_number
                          job:despatched = ''
                          job:exchange_status = 'DESPATCHED'
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          job:exchange_despatched_user = use:user_code
                          !call the exchange status routine
                          GetStatus(820,1,'EXC') !Awaiting Despatch Confimation
      
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number = job:exchange_unit_number
                          if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                              xch:available = 'DES'
                              access:exchange.update()                                        !Make Exchange Unit Available
                              get(exchhist,0)
                              if access:exchhist.primerecord() = Level:Benign
                                  exh:ref_number    = xch:ref_number
                                  exh:date          = Today()
                                  exh:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  exh:user = use:user_code
                                  exh:status        = 'UNIT DESPATCHED ON JOB: ' & CLip(job:Ref_number)
                                  if access:exchhist.insert()
                                     access:exchhist.cancelautoinc()
                                  end
                              end!if access:exchhist.primerecord() = Level:Benign
                          end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
      
                  End!Case job:despatch_type
                  
                  access:jobs.update()
                  If job:despatch_type = 'JOB'
                  
                      !Return Location
                      access:locinter.clearkey(loi:location_key)
                      loi:location = job:location
                      if access:locinter.fetch(loi:location_key) = Level:Benign
                          If loi:allocate_spaces = 'YES'
                              loi:current_spaces += 1
                              loi:location_available = 'YES'
                              access:locinter.update()
                          End!If loi:allocate_spaces = 'YES'
                      end!if access:locinter.fetch(loi:location_key) = Level:Benign
      
                      print_despatch# = 0
                      access:subtracc.clearkey(sub:account_number_key)
                      sub:account_number = job:account_number
                      if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                          access:tradeacc.clearkey(tra:account_number_key)
                          tra:account_number = sub:main_account_number
                          if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                              If tra:use_sub_accounts = 'YES'
                                  If sub:print_despatch_despatch = 'YES'
                                      If SUB:despatch_note_per_item = 'YES'
                                          print_despatch# = 1
                                      End!If tra:print_despatch_notes = 'YES'
                                  End!If sub:print_despatch_despatch = 'YES'
                              Else!If tra:use_sub_accounts = 'YES'
                                  If tra:print_despatch_despatch = 'YES'
                                      If tra:despatch_note_per_item = 'YES'
                                          print_despatch# = 1
                                      End!If sub:print_despatch_notes = 'YES'
                                  End!If tra:print_despatch_despatch = 'YES'
                              End!If tra:use_sub_accounts = 'YES'
                          end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                      end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
          !            If print_despatch# = 1
          !                glo:select1 = job:ref_number
          !                setcursor()
          !                Despatch_Note
          !                setcursor(cursor:wait)
          !                glo:select1 = ''
          !            End
                  End!If job:despatch_type = 'JOB'
      
                  Case f_type
                      Of 'COLLECT'
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              
                              aud:ref_number    = job:ref_number
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              Case job:despatch_type
                                  Of 'JOB'
                                      aud:action        = 'ANC EXPORT COLL/DEL. ROUTINE: JOB EXPORTED'
                                  Of 'EXC'
                                      aud:action        = 'ANC EXPORT COLL/DEL. ROUTINE: EXCHANGE UNIT EXPORTED'
                                  Of 'LOA'
                                      aud:action        = 'ANC EXPORT COLL/DEL. ROUTINE: LOAN UNIT EXPORTED'
                              End!Case job:despatch_type
                              aud:notes         = 'AWAITING CONFIRMATION'
                              if access:audit.insert()
                                  access:audit.cancelautoinc()
                              end
                          end!if access:audit.primerecord() = level:benign
                      Of 'DELIVER'
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                                                      aud:ref_number    = job:ref_number
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              Case job:despatch_type
                                  Of 'JOB'
                                      aud:action        = 'ANC EXPORT DEL. ROUTINE: JOB EXPORTED'
                                  Of 'EXC'
                                      aud:action        = 'ANC EXPORT DEL. ROUTINE: EXCHANGE UNIT EXPORTED'
                                  Of 'LOA'
                                      aud:action        = 'ANC EXPORT DEL. ROUTINE: LOAN UNIT EXPORTED'
                              End!Case job:despatch_type
                              aud:notes         = 'AWAITING CONFIRMATION'
                              if access:audit.insert()
                                  access:audit.cancelautoinc()
                              end
                          end!if access:audit.primerecord() = level:benign
                  End!If f_type = 'COLLECT'
      
              end !loop
              access:jobs.restorefile(save_job_id)
      !        access:expgen.close()
              If count# <> 0
      !            sav:path    = path()
      !            setpath(Clip(cou:ancpath))
      !            Run('ancpaper.exe',1)
      !            Setpath(sav:path)
                  cou:anccount += 1
                  If cou:ANCCount = 999
                      cou:ANCCount = 1
                  End
                  access:courier.update()
              End!If count# <> 0
      
          end !loop
          access:courier.restorefile(save_cou_id)
          setcursor()
          close(progresswindow)
          Close(out_file)
          If count# <> 0
              Case MessageEx('Export Completed.','ServiceBase 2000','Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
      
          Else!If count# <> 0
              Case MessageEx('No jobs to export.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End!If count# <> 0
      Else!If count# <> 0
          Case MessageEx('There are no jobs to export.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End!If count# <> 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ANC_Despatch_Routine')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?despatch_date_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      despatch_date_temp = Today()
      Case f_Type
          Of 'DELIVER'
              0{prop:text} = 'ANC - Deliver Only'
          Of 'COLLECT'
              0{prop:text} = 'ANC - Deliver And Collect'
      End!Case f_Type
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

