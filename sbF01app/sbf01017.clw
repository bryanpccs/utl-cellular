

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01017.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Payments PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:LabourRate       REAL
tmp:PartsRate        REAL
save_jpt_id          USHORT,AUTO
FilesOpened          BYTE
Payment_Type_Temp    STRING('BOTH {8}')
grand_total          REAL
total_charge         REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBPAYMT)
                       PROJECT(jpt:Date)
                       PROJECT(jpt:Payment_Type)
                       PROJECT(jpt:User_Code)
                       PROJECT(jpt:Amount)
                       PROJECT(jpt:Record_Number)
                       PROJECT(jpt:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
jpt:Date               LIKE(jpt:Date)                 !List box control field - type derived from field
jpt:Payment_Type       LIKE(jpt:Payment_Type)         !List box control field - type derived from field
jpt:User_Code          LIKE(jpt:User_Code)            !List box control field - type derived from field
jpt:Amount             LIKE(jpt:Amount)               !List box control field - type derived from field
jpt:Record_Number      LIKE(jpt:Record_Number)        !Primary key field - type derived from field
jpt:Ref_Number         LIKE(jpt:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Payments'),AT(,,364,196),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Payments'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,36,264,152),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('48L(2)|M~Date~@d6b@104L(2)|M~Payment Type~@s30@23L(2)|M~User~@s3@56R(2)|M~Paymen' &|
   't Received~L@n-14.2@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(284,96,76,20),USE(?Insert:2),LEFT,ICON('insert.ico')
                       BUTTON('&Complete Process'),AT(284,4,76,20),USE(?Complete_Process),HIDE,LEFT,ICON('go.gif')
                       BUTTON('&Change'),AT(284,120,76,20),USE(?Change:2),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(284,144,76,20),USE(?Delete:2),LEFT,ICON('delete.ico')
                       SHEET,AT(4,4,272,188),USE(?CurrentTab),WIZARD,SPREAD
                         TAB('By Date'),USE(?Tab:3)
                           PROMPT('Payments Taken For Job Number:'),AT(8,8),USE(?Prompt2),FONT(,,COLOR:White,,CHARSET:ANSI)
                           STRING(@s8),AT(116,8),USE(GLO:Select1),FONT(,,COLOR:White,FONT:bold)
                           STRING('Charge'),AT(172,8),USE(?String2)
                           ENTRY(@n10.2),AT(208,8,64,10),USE(total_charge),SKIP,FONT(,,,FONT:bold),READONLY
                           PROMPT('Total Paid'),AT(172,20),USE(?grand_total:Prompt),TRN
                           ENTRY(@n-14.2),AT(208,20,64,10),USE(grand_total),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                         END
                       END
                       BUTTON('Close'),AT(284,172,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:3{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?GLO:Select1{prop:FontColor} = -1
    ?GLO:Select1{prop:Color} = 15066597
    ?String2{prop:FontColor} = -1
    ?String2{prop:Color} = 15066597
    If ?total_charge{prop:ReadOnly} = True
        ?total_charge{prop:FontColor} = 65793
        ?total_charge{prop:Color} = 15066597
    Elsif ?total_charge{prop:Req} = True
        ?total_charge{prop:FontColor} = 65793
        ?total_charge{prop:Color} = 8454143
    Else ! If ?total_charge{prop:Req} = True
        ?total_charge{prop:FontColor} = 65793
        ?total_charge{prop:Color} = 16777215
    End ! If ?total_charge{prop:Req} = True
    ?total_charge{prop:Trn} = 0
    ?total_charge{prop:FontStyle} = font:Bold
    ?grand_total:Prompt{prop:FontColor} = -1
    ?grand_total:Prompt{prop:Color} = 15066597
    If ?grand_total{prop:ReadOnly} = True
        ?grand_total{prop:FontColor} = 65793
        ?grand_total{prop:Color} = 15066597
    Elsif ?grand_total{prop:Req} = True
        ?grand_total{prop:FontColor} = 65793
        ?grand_total{prop:Color} = 8454143
    Else ! If ?grand_total{prop:Req} = True
        ?grand_total{prop:FontColor} = 65793
        ?grand_total{prop:Color} = 16777215
    End ! If ?grand_total{prop:Req} = True
    ?grand_total{prop:Trn} = 0
    ?grand_total{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Create_Invoice      ROutine
!        get(invoice,0)
!        if access:invoice.primerecord() = Level:Benign
!            inv:invoice_type       = 'SIN'
!            inv:job_number         = job:ref_number
!            inv:date_created       = Today()
!            inv:account_number     = job:account_number
!            If tra:invoice_sub_accounts = 'YES'
!                inv:AccountType        = 'SUB'
!            Else!If tra:invoice_sub_accounts = 'YES'
!                inv:accounttype        = 'MAI'
!            End!If tra:invoice_sub_accounts = 'YES'
!            inv:total              = job:sub_total
!            inv:vat_rate_labour    = tmp:LabourRate
!            inv:vat_rate_parts     = tmp:PartsRate
!            inv:vat_rate_retail    = ''
!            inv:vat_number         = Def:Vat_Number
!            inv:invoice_vat_number = ''!tmp:InvoiceVatNumber
!            inv:batch_number       = ''
!            inv:courier_paid       = job:Courier_Cost
!            inv:labour_paid        = job:Labour_Cost
!            inv:parts_paid         = job:Parts_Cost
!            inv:jobs_count         = 1
!            if access:invoice.insert()
!                access:invoice.cancelautoinc()
!
!            end
!
!            If access:desbatch.primerecord() = Level:Benign
!                access:desbatch.tryinsert()
!                job:date_despatched = Today()
!                job:despatch_number = dbt:batch_number
!                job:despatched = 'YES'
!                access:users.clearkey(use:password_key)
!                use:password =glo:password
!                access:users.fetch(use:password_key)
!                job:despatch_user = use:user_code
!                job:consignment_number = use:user_code
!                Include('ChkPaid.inc')
!
!                job:invoice_number  = inv:invoice_number
!                job:invoice_Date    = Today()
!                job:Invoice_Courier_Cost    = job:courier_Cost
!                job:invoice_labour_cost     = job:labour_cost
!                job:invoice_parts_cost      = job:parts_cost
!                job:invoice_sub_total       = job:sub_total
!
!                access:jobs.update()
!
!                get(audit,0)
!                if access:audit.primerecord() = level:benign
!                    aud:notes         = 'INVOICE CREATED: ' & Clip(job:invoice_number)
!                    aud:ref_number    = job:ref_number
!                    aud:date          = today()
!                    aud:time          = clock()
!                    aud:type          = 'JOB'
!                    access:users.clearkey(use:password_key)
!                    use:password =glo:password
!                    access:users.fetch(use:password_key)
!                    aud:user = use:user_code
!                    aud:action        = 'RECEPTION DESPATCH'
!                    access:audit.insert()
!                end!�if access:audit.primerecord() = level:benign
!
!                glo:select1  = inv:invoice_number
!                Single_Invoice
!                glo:select1  = ''
!            End!If access:desbatch.primerecord() = Level:Benign
!        End!if access:invoice.primerecord() = Level:Benign
!        Post(Event:Closewindow)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Payments',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Payments',1)
    SolaceViewVars('tmp:LabourRate',tmp:LabourRate,'Browse_Payments',1)
    SolaceViewVars('tmp:PartsRate',tmp:PartsRate,'Browse_Payments',1)
    SolaceViewVars('save_jpt_id',save_jpt_id,'Browse_Payments',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Payments',1)
    SolaceViewVars('Payment_Type_Temp',Payment_Type_Temp,'Browse_Payments',1)
    SolaceViewVars('grand_total',grand_total,'Browse_Payments',1)
    SolaceViewVars('total_charge',total_charge,'Browse_Payments',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Complete_Process;  SolaceCtrlName = '?Complete_Process';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select1;  SolaceCtrlName = '?GLO:Select1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_charge;  SolaceCtrlName = '?total_charge';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?grand_total:Prompt;  SolaceCtrlName = '?grand_total:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?grand_total;  SolaceCtrlName = '?grand_total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Payments')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Payments')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DESBATCH.Open
  Relate:JOBPAYMT.Open
  Relate:STAHEAD.Open
  Relate:VATCODE.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:INVOICE.UseFile
  Access:USERS.UseFile
  Access:STATUS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBPAYMT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,jpt:All_Date_Key)
  BRW1.AddRange(jpt:Ref_Number,GLO:Select1)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,jpt:Date,1,BRW1)
  BRW1.AddField(jpt:Date,BRW1.Q.jpt:Date)
  BRW1.AddField(jpt:Payment_Type,BRW1.Q.jpt:Payment_Type)
  BRW1.AddField(jpt:User_Code,BRW1.Q.jpt:User_Code)
  BRW1.AddField(jpt:Amount,BRW1.Q.jpt:Amount)
  BRW1.AddField(jpt:Record_Number,BRW1.Q.jpt:Record_Number)
  BRW1.AddField(jpt:Ref_Number,BRW1.Q.jpt:Ref_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DESBATCH.Close
    Relate:JOBPAYMT.Close
    Relate:STAHEAD.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Payments',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJOBPAYMT
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Complete_Process
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Complete_Process, Accepted)
      glo:Select2 = ''
      access:jobs.clearkey(job:Ref_number_key)
      job:ref_number  = glo:select1
      If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
          access:subtracc.clearkey(sub:account_number_key)
          sub:account_number = job:account_number
          if access:subtracc.fetch(sub:account_number_key) = level:benign
              access:tradeacc.clearkey(tra:account_number_key) 
              tra:account_number = sub:main_account_number
              if access:tradeacc.fetch(tra:account_number_key) = level:benign
                  if tra:invoice_sub_accounts = 'YES'
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = sub:labour_vat_code
                      if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                          tmp:LabourRate = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = sub:parts_vat_code
                      if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                          tmp:PartsRate = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                  else!if tra:invoice_sub_accounts = 'YES'
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = tra:labour_vat_code
                      if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                          tmp:LabourRate = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = tra:parts_vat_code
                      if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                          tmp:PartsRate = vat:vat_rate
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                  end!if tra:invoice_sub_accounts = 'YES'
              end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
          end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
      
          total$ = 0
          save_jpt_id = access:jobpaymt.savefile()
          access:jobpaymt.clearkey(jpt:all_date_key)
          jpt:ref_number = glo:select1
          set(jpt:all_date_key,jpt:all_date_key)
          loop
              if access:jobpaymt.next()
                 break
              end !if
              if jpt:ref_number <> glo:select1      |
                  then break.  ! end if
              total$  += jpt:amount
          end !loop
          access:jobpaymt.restorefile(save_jpt_id)
      
          If total$ => Round(job:courier_cost + job:labour_cost + job:parts_cost,.01) +|
                      Round(job:courier_cost * tmp:LabourRate/100,.01) +|
                      Round(job:labour_cost * tmp:LabourRate/100,.01) +|
                      Round(job:parts_cost * tmp:PartsRate/100,.01)
              glo:Select2 = 'COMPLETE'
              Post(Event:CloseWindow)
          Else
              Case MessageEx('Warning! You value paid is LESS than the total for this job. <13,10><13,10>Are you sure you want to continue to create the invoice and despatch this job?','ServiceBase 2000',|
                             'Styles\warn.ico','|&Continue|&Cancel',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &Continue Button
                      !Pass back the fact that the process will be completed
                      glo:Select2 = 'COMPLETE'
                      Post(Event:CloseWindow)
                  Of 2 ! &Cancel Button
              End!Case MessageEx
          End! If total$
      End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Complete_Process, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Payments')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      If glo:select2 = 'PROCESS'
          Unhide(?Complete_Process)
      End!If glo:select2 = 'PROCESS'
      
      ! Start Change 1553 BE(21/01/04)
      Total_Price('C',vat",total",balance")
      total_charge = total"
      DISPLAY()
      ! End Change 1553 BE(21/01/04)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.ResetFromView PROCEDURE

grand_total:Sum      REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:JOBPAYMT.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    grand_total:Sum += jpt:Amount
  END
  grand_total = grand_total:Sum
  PARENT.ResetFromView
  Relate:JOBPAYMT.SetQuickScan(0)
  SETCURSOR()


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)

