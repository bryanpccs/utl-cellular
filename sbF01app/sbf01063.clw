

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01063.INC'),ONCE        !Local module procedure declarations
                     END


GenericFaultCodes PROCEDURE (f:Job,f:Req,f:Acc,f:Man,f:WCrg,f:WRep,f:Comp) !Generated from procedure template - Window

FilesOpened          BYTE
tmp:Required         BYTE(0)
save_taf_id          USHORT,AUTO
save_tfo_id          USHORT,AUTO
save_mfo_id          USHORT,AUTO
ActionMessage        CSTRING(40)
sav:prompt           STRING(30),DIM(12)
tmp:prompt           STRING(30),DIM(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Fault Codes'),AT(,,231,373),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,338),USE(?Sheet1),SPREAD
                         TAB('Manufacturers Fault Codes'),USE(?Tab1),HIDE
                           STRING(@s30),AT(12,20),USE(tmp:prompt[1])
                           ENTRY(@s30),AT(84,20,124,10),USE(glo:FaultCode1),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2)
                           BUTTON,AT(212,20,10,10),USE(?Lookup),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,36),USE(tmp:prompt[2])
                           BUTTON,AT(152,20,10,10),USE(?PopCalendar),HIDE,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,36,124,10),USE(glo:FaultCode2),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2)
                           BUTTON,AT(212,36,10,10),USE(?Lookup:2),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar:2),HIDE,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,52,124,10),USE(glo:FaultCode3),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(212,52,10,10),USE(?Lookup:3),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,52),USE(tmp:prompt[3])
                           BUTTON,AT(152,52,10,10),USE(?PopCalendar:3),HIDE,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,68,124,10),USE(glo:FaultCode4),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,68,10,10),USE(?PopCalendar:4),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,68,10,10),USE(?Lookup:4),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,68),USE(tmp:prompt[4])
                           STRING(@s30),AT(12,84),USE(tmp:prompt[5])
                           ENTRY(@s30),AT(84,84,124,10),USE(glo:FaultCode5),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,84,10,10),USE(?PopCalendar:5),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,84,10,10),USE(?Lookup:5),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,100),USE(tmp:prompt[6])
                           ENTRY(@s30),AT(84,100,124,10),USE(glo:FaultCode6),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,100,10,10),USE(?PopCalendar:6),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,100,10,10),USE(?Lookup:6),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,116),USE(tmp:prompt[7])
                           ENTRY(@s30),AT(84,116,124,10),USE(glo:FaultCode7),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,116,10,10),USE(?PopCalendar:7),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,116,10,10),USE(?Lookup:7),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,132),USE(tmp:prompt[8])
                           ENTRY(@s30),AT(84,132,124,10),USE(glo:FaultCode8),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,132,10,10),USE(?PopCalendar:8),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,132,10,10),USE(?Lookup:8),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,148),USE(tmp:prompt[9])
                           ENTRY(@s30),AT(84,148,124,10),USE(glo:FaultCode9),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,148,10,10),USE(?PopCalendar:9),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,148,10,10),USE(?Lookup:9),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,164),USE(tmp:prompt[10])
                           ENTRY(@s255),AT(84,164,124,10),USE(glo:FaultCode10),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,164,10,10),USE(?PopCalendar:10),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,164,10,10),USE(?Lookup:10),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,180),USE(tmp:prompt[11])
                           ENTRY(@s255),AT(84,180,124,10),USE(glo:FaultCode11),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,180,10,10),USE(?PopCalendar:11),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,180,10,10),USE(?Lookup:11),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,196),USE(tmp:prompt[12])
                           ENTRY(@s255),AT(84,196,124,10),USE(glo:FaultCode12),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,196,10,10),USE(?PopCalendar:12),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,196,10,10),USE(?Lookup:12),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,212),USE(tmp:prompt[13])
                           ENTRY(@s30),AT(84,212,124,10),USE(glo:FaultCode13),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,212,10,10),USE(?PopCalendar:25),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,212,10,10),USE(?Lookup:13),SKIP,HIDE,ICON('list3.ico')
                           ENTRY(@s30),AT(84,228,124,10),USE(glo:FaultCode14),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,228,10,10),USE(?PopCalendar:26),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,228,10,10),USE(?Lookup:14),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,228),USE(tmp:prompt[14])
                           ENTRY(@s30),AT(84,244,124,10),USE(glo:FaultCode15),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,244,10,10),USE(?PopCalendar:27),HIDE,ICON('calenda2.ico')
                           STRING(@s30),AT(12,244),USE(tmp:prompt[15])
                           ENTRY(@s30),AT(84,260,124,10),USE(glo:FaultCode16),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,260,10,10),USE(?PopCalendar:28),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,260,10,10),USE(?Lookup:16),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,244,10,10),USE(?Lookup:15),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,260),USE(tmp:prompt[16])
                           ENTRY(@s30),AT(84,276,124,10),USE(glo:FaultCode17),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,276,10,10),USE(?PopCalendar:29),HIDE,ICON('calenda2.ico')
                           STRING(@s30),AT(12,276),USE(tmp:prompt[17])
                           ENTRY(@s30),AT(84,292,124,10),USE(glo:FaultCode18),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,292,10,10),USE(?PopCalendar:30),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,292,10,10),USE(?Lookup:18),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,276,10,10),USE(?Lookup:17),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,292),USE(tmp:prompt[18])
                           ENTRY(@s30),AT(84,308,124,10),USE(glo:FaultCode19),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,308,10,10),USE(?PopCalendar:31),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,308,10,10),USE(?Lookup:19),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,308),USE(tmp:prompt[19])
                           ENTRY(@s30),AT(84,324,124,10),USE(glo:FaultCode20),HIDE,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(152,324,10,10),USE(?PopCalendar:32),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,324,10,10),USE(?Lookup:20),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s30),AT(12,324),USE(tmp:prompt[20])
                         END
                         TAB('Trade Fault Codes'),USE(?Tab2),HIDE
                           PROMPT('Fault Code 1'),AT(8,20),USE(?glo:TradeFaultCode1:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(glo:TradeFaultCode1),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 1'),TIP('Fault Code 1'),UPR
                           BUTTON,AT(152,20,10,10),USE(?PopCalendar:13),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,20,10,10),USE(?tralookup:1),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 2'),AT(8,36),USE(?glo:TradeFaultCode2:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(glo:TradeFaultCode2),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 2'),TIP('Fault Code 2'),UPR
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar:14),HIDE,ICON('CALENDA2.ICO')
                           PROMPT('Fault Code 3'),AT(8,52),USE(?glo:TradeFaultCode3:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(glo:TradeFaultCode3),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 3'),TIP('Fault Code 3'),UPR
                           BUTTON,AT(152,52,10,10),USE(?PopCalendar:15),HIDE,ICON('CALENDA2.ICO')
                           PROMPT('Fault Code 4'),AT(8,68),USE(?glo:TradeFaultCode4:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(glo:TradeFaultCode4),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 4'),TIP('Fault Code 4'),UPR
                           BUTTON,AT(152,68,10,10),USE(?PopCalendar:16),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,68,10,10),USE(?tralookup:4),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 5'),AT(8,84),USE(?glo:TradeFaultCode5:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,124,10),USE(glo:TradeFaultCode5),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 5'),TIP('Fault Code 5'),UPR
                           BUTTON,AT(152,84,10,10),USE(?PopCalendar:17),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,84,10,10),USE(?tralookup:5),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 6'),AT(8,100),USE(?glo:TradeFaultCode6:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,100,124,10),USE(glo:TradeFaultCode6),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 6'),TIP('Fault Code 6'),UPR
                           BUTTON,AT(152,100,10,10),USE(?PopCalendar:18),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,36,10,10),USE(?tralookup:2),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,52,10,10),USE(?tralookup:3),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 7'),AT(8,116),USE(?glo:TradeFaultCode7:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,116,124,10),USE(glo:TradeFaultCode7),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 7'),TIP('Fault Code 7'),UPR
                           BUTTON,AT(152,116,10,10),USE(?PopCalendar:19),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,116,10,10),USE(?tralookup:7),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 8'),AT(8,132),USE(?glo:TradeFaultCode8:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,132,124,10),USE(glo:TradeFaultCode8),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 8'),TIP('Fault Code 8'),UPR
                           BUTTON,AT(152,132,10,10),USE(?PopCalendar:20),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,132,10,10),USE(?tralookup:8),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 9'),AT(8,148),USE(?glo:TradeFaultCode9:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,148,124,10),USE(glo:TradeFaultCode9),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 9'),TIP('Fault Code 9'),UPR
                           BUTTON,AT(152,148,10,10),USE(?PopCalendar:21),HIDE,ICON('CALENDA2.ICO')
                           PROMPT('Fault Code 10'),AT(8,164),USE(?glo:TradeFaultCode10:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,164,124,10),USE(glo:TradeFaultCode10),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 10'),TIP('Fault Code 10'),UPR
                           BUTTON,AT(152,164,10,10),USE(?PopCalendar:22),HIDE,ICON('CALENDA2.ICO')
                           PROMPT('Fault Code 11'),AT(8,180),USE(?glo:TradeFaultCode11:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,180,124,10),USE(glo:TradeFaultCode11),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 11'),TIP('Fault Code 11'),UPR
                           BUTTON,AT(152,180,10,10),USE(?PopCalendar:23),HIDE,ICON('CALENDA2.ICO')
                           PROMPT('Fault Code 12'),AT(8,196),USE(?glo:TradeFaultCode12:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,196,124,10),USE(glo:TradeFaultCode12),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 12'),TIP('Fault Code 12'),UPR
                           BUTTON,AT(152,196,10,10),USE(?PopCalendar:24),HIDE,ICON('CALENDA2.ICO')
                           BUTTON,AT(212,148,10,10),USE(?tralookup:9),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,180,10,10),USE(?tralookup:11),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,100,10,10),USE(?tralookup:6),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,164,10,10),USE(?tralookup:10),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(212,196,10,10),USE(?tralookup:12),SKIP,HIDE,ICON('list3.ico')
                         END
                         TAB('Fault Codes'),USE(?Tab3),HIDE
                           PROMPT('NO Fault Codes are required for this job.'),AT(45,156,140,60),USE(?Prompt25),FONT(,22,COLOR:Navy,)
                         END
                       END
                       PANEL,AT(4,346,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(168,350,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_maf_id     ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:prompt_1{prop:FontColor} = -1
    ?tmp:prompt_1{prop:Color} = 15066597
    If ?glo:FaultCode1{prop:ReadOnly} = True
        ?glo:FaultCode1{prop:FontColor} = 65793
        ?glo:FaultCode1{prop:Color} = 15066597
    Elsif ?glo:FaultCode1{prop:Req} = True
        ?glo:FaultCode1{prop:FontColor} = 65793
        ?glo:FaultCode1{prop:Color} = 8454143
    Else ! If ?glo:FaultCode1{prop:Req} = True
        ?glo:FaultCode1{prop:FontColor} = 65793
        ?glo:FaultCode1{prop:Color} = 16777215
    End ! If ?glo:FaultCode1{prop:Req} = True
    ?glo:FaultCode1{prop:Trn} = 0
    ?glo:FaultCode1{prop:FontStyle} = font:Bold
    ?tmp:prompt_2{prop:FontColor} = -1
    ?tmp:prompt_2{prop:Color} = 15066597
    If ?glo:FaultCode2{prop:ReadOnly} = True
        ?glo:FaultCode2{prop:FontColor} = 65793
        ?glo:FaultCode2{prop:Color} = 15066597
    Elsif ?glo:FaultCode2{prop:Req} = True
        ?glo:FaultCode2{prop:FontColor} = 65793
        ?glo:FaultCode2{prop:Color} = 8454143
    Else ! If ?glo:FaultCode2{prop:Req} = True
        ?glo:FaultCode2{prop:FontColor} = 65793
        ?glo:FaultCode2{prop:Color} = 16777215
    End ! If ?glo:FaultCode2{prop:Req} = True
    ?glo:FaultCode2{prop:Trn} = 0
    ?glo:FaultCode2{prop:FontStyle} = font:Bold
    If ?glo:FaultCode3{prop:ReadOnly} = True
        ?glo:FaultCode3{prop:FontColor} = 65793
        ?glo:FaultCode3{prop:Color} = 15066597
    Elsif ?glo:FaultCode3{prop:Req} = True
        ?glo:FaultCode3{prop:FontColor} = 65793
        ?glo:FaultCode3{prop:Color} = 8454143
    Else ! If ?glo:FaultCode3{prop:Req} = True
        ?glo:FaultCode3{prop:FontColor} = 65793
        ?glo:FaultCode3{prop:Color} = 16777215
    End ! If ?glo:FaultCode3{prop:Req} = True
    ?glo:FaultCode3{prop:Trn} = 0
    ?glo:FaultCode3{prop:FontStyle} = font:Bold
    ?tmp:prompt_3{prop:FontColor} = -1
    ?tmp:prompt_3{prop:Color} = 15066597
    If ?glo:FaultCode4{prop:ReadOnly} = True
        ?glo:FaultCode4{prop:FontColor} = 65793
        ?glo:FaultCode4{prop:Color} = 15066597
    Elsif ?glo:FaultCode4{prop:Req} = True
        ?glo:FaultCode4{prop:FontColor} = 65793
        ?glo:FaultCode4{prop:Color} = 8454143
    Else ! If ?glo:FaultCode4{prop:Req} = True
        ?glo:FaultCode4{prop:FontColor} = 65793
        ?glo:FaultCode4{prop:Color} = 16777215
    End ! If ?glo:FaultCode4{prop:Req} = True
    ?glo:FaultCode4{prop:Trn} = 0
    ?glo:FaultCode4{prop:FontStyle} = font:Bold
    ?tmp:prompt_4{prop:FontColor} = -1
    ?tmp:prompt_4{prop:Color} = 15066597
    ?tmp:prompt_5{prop:FontColor} = -1
    ?tmp:prompt_5{prop:Color} = 15066597
    If ?glo:FaultCode5{prop:ReadOnly} = True
        ?glo:FaultCode5{prop:FontColor} = 65793
        ?glo:FaultCode5{prop:Color} = 15066597
    Elsif ?glo:FaultCode5{prop:Req} = True
        ?glo:FaultCode5{prop:FontColor} = 65793
        ?glo:FaultCode5{prop:Color} = 8454143
    Else ! If ?glo:FaultCode5{prop:Req} = True
        ?glo:FaultCode5{prop:FontColor} = 65793
        ?glo:FaultCode5{prop:Color} = 16777215
    End ! If ?glo:FaultCode5{prop:Req} = True
    ?glo:FaultCode5{prop:Trn} = 0
    ?glo:FaultCode5{prop:FontStyle} = font:Bold
    ?tmp:prompt_6{prop:FontColor} = -1
    ?tmp:prompt_6{prop:Color} = 15066597
    If ?glo:FaultCode6{prop:ReadOnly} = True
        ?glo:FaultCode6{prop:FontColor} = 65793
        ?glo:FaultCode6{prop:Color} = 15066597
    Elsif ?glo:FaultCode6{prop:Req} = True
        ?glo:FaultCode6{prop:FontColor} = 65793
        ?glo:FaultCode6{prop:Color} = 8454143
    Else ! If ?glo:FaultCode6{prop:Req} = True
        ?glo:FaultCode6{prop:FontColor} = 65793
        ?glo:FaultCode6{prop:Color} = 16777215
    End ! If ?glo:FaultCode6{prop:Req} = True
    ?glo:FaultCode6{prop:Trn} = 0
    ?glo:FaultCode6{prop:FontStyle} = font:Bold
    ?tmp:prompt_7{prop:FontColor} = -1
    ?tmp:prompt_7{prop:Color} = 15066597
    If ?glo:FaultCode7{prop:ReadOnly} = True
        ?glo:FaultCode7{prop:FontColor} = 65793
        ?glo:FaultCode7{prop:Color} = 15066597
    Elsif ?glo:FaultCode7{prop:Req} = True
        ?glo:FaultCode7{prop:FontColor} = 65793
        ?glo:FaultCode7{prop:Color} = 8454143
    Else ! If ?glo:FaultCode7{prop:Req} = True
        ?glo:FaultCode7{prop:FontColor} = 65793
        ?glo:FaultCode7{prop:Color} = 16777215
    End ! If ?glo:FaultCode7{prop:Req} = True
    ?glo:FaultCode7{prop:Trn} = 0
    ?glo:FaultCode7{prop:FontStyle} = font:Bold
    ?tmp:prompt_8{prop:FontColor} = -1
    ?tmp:prompt_8{prop:Color} = 15066597
    If ?glo:FaultCode8{prop:ReadOnly} = True
        ?glo:FaultCode8{prop:FontColor} = 65793
        ?glo:FaultCode8{prop:Color} = 15066597
    Elsif ?glo:FaultCode8{prop:Req} = True
        ?glo:FaultCode8{prop:FontColor} = 65793
        ?glo:FaultCode8{prop:Color} = 8454143
    Else ! If ?glo:FaultCode8{prop:Req} = True
        ?glo:FaultCode8{prop:FontColor} = 65793
        ?glo:FaultCode8{prop:Color} = 16777215
    End ! If ?glo:FaultCode8{prop:Req} = True
    ?glo:FaultCode8{prop:Trn} = 0
    ?glo:FaultCode8{prop:FontStyle} = font:Bold
    ?tmp:prompt_9{prop:FontColor} = -1
    ?tmp:prompt_9{prop:Color} = 15066597
    If ?glo:FaultCode9{prop:ReadOnly} = True
        ?glo:FaultCode9{prop:FontColor} = 65793
        ?glo:FaultCode9{prop:Color} = 15066597
    Elsif ?glo:FaultCode9{prop:Req} = True
        ?glo:FaultCode9{prop:FontColor} = 65793
        ?glo:FaultCode9{prop:Color} = 8454143
    Else ! If ?glo:FaultCode9{prop:Req} = True
        ?glo:FaultCode9{prop:FontColor} = 65793
        ?glo:FaultCode9{prop:Color} = 16777215
    End ! If ?glo:FaultCode9{prop:Req} = True
    ?glo:FaultCode9{prop:Trn} = 0
    ?glo:FaultCode9{prop:FontStyle} = font:Bold
    ?tmp:prompt_10{prop:FontColor} = -1
    ?tmp:prompt_10{prop:Color} = 15066597
    If ?glo:FaultCode10{prop:ReadOnly} = True
        ?glo:FaultCode10{prop:FontColor} = 65793
        ?glo:FaultCode10{prop:Color} = 15066597
    Elsif ?glo:FaultCode10{prop:Req} = True
        ?glo:FaultCode10{prop:FontColor} = 65793
        ?glo:FaultCode10{prop:Color} = 8454143
    Else ! If ?glo:FaultCode10{prop:Req} = True
        ?glo:FaultCode10{prop:FontColor} = 65793
        ?glo:FaultCode10{prop:Color} = 16777215
    End ! If ?glo:FaultCode10{prop:Req} = True
    ?glo:FaultCode10{prop:Trn} = 0
    ?glo:FaultCode10{prop:FontStyle} = font:Bold
    ?tmp:prompt_11{prop:FontColor} = -1
    ?tmp:prompt_11{prop:Color} = 15066597
    If ?glo:FaultCode11{prop:ReadOnly} = True
        ?glo:FaultCode11{prop:FontColor} = 65793
        ?glo:FaultCode11{prop:Color} = 15066597
    Elsif ?glo:FaultCode11{prop:Req} = True
        ?glo:FaultCode11{prop:FontColor} = 65793
        ?glo:FaultCode11{prop:Color} = 8454143
    Else ! If ?glo:FaultCode11{prop:Req} = True
        ?glo:FaultCode11{prop:FontColor} = 65793
        ?glo:FaultCode11{prop:Color} = 16777215
    End ! If ?glo:FaultCode11{prop:Req} = True
    ?glo:FaultCode11{prop:Trn} = 0
    ?glo:FaultCode11{prop:FontStyle} = font:Bold
    ?tmp:prompt_12{prop:FontColor} = -1
    ?tmp:prompt_12{prop:Color} = 15066597
    If ?glo:FaultCode12{prop:ReadOnly} = True
        ?glo:FaultCode12{prop:FontColor} = 65793
        ?glo:FaultCode12{prop:Color} = 15066597
    Elsif ?glo:FaultCode12{prop:Req} = True
        ?glo:FaultCode12{prop:FontColor} = 65793
        ?glo:FaultCode12{prop:Color} = 8454143
    Else ! If ?glo:FaultCode12{prop:Req} = True
        ?glo:FaultCode12{prop:FontColor} = 65793
        ?glo:FaultCode12{prop:Color} = 16777215
    End ! If ?glo:FaultCode12{prop:Req} = True
    ?glo:FaultCode12{prop:Trn} = 0
    ?glo:FaultCode12{prop:FontStyle} = font:Bold
    ?tmp:prompt_13{prop:FontColor} = -1
    ?tmp:prompt_13{prop:Color} = 15066597
    If ?glo:FaultCode13{prop:ReadOnly} = True
        ?glo:FaultCode13{prop:FontColor} = 65793
        ?glo:FaultCode13{prop:Color} = 15066597
    Elsif ?glo:FaultCode13{prop:Req} = True
        ?glo:FaultCode13{prop:FontColor} = 65793
        ?glo:FaultCode13{prop:Color} = 8454143
    Else ! If ?glo:FaultCode13{prop:Req} = True
        ?glo:FaultCode13{prop:FontColor} = 65793
        ?glo:FaultCode13{prop:Color} = 16777215
    End ! If ?glo:FaultCode13{prop:Req} = True
    ?glo:FaultCode13{prop:Trn} = 0
    ?glo:FaultCode13{prop:FontStyle} = font:Bold
    If ?glo:FaultCode14{prop:ReadOnly} = True
        ?glo:FaultCode14{prop:FontColor} = 65793
        ?glo:FaultCode14{prop:Color} = 15066597
    Elsif ?glo:FaultCode14{prop:Req} = True
        ?glo:FaultCode14{prop:FontColor} = 65793
        ?glo:FaultCode14{prop:Color} = 8454143
    Else ! If ?glo:FaultCode14{prop:Req} = True
        ?glo:FaultCode14{prop:FontColor} = 65793
        ?glo:FaultCode14{prop:Color} = 16777215
    End ! If ?glo:FaultCode14{prop:Req} = True
    ?glo:FaultCode14{prop:Trn} = 0
    ?glo:FaultCode14{prop:FontStyle} = font:Bold
    ?tmp:prompt_14{prop:FontColor} = -1
    ?tmp:prompt_14{prop:Color} = 15066597
    If ?glo:FaultCode15{prop:ReadOnly} = True
        ?glo:FaultCode15{prop:FontColor} = 65793
        ?glo:FaultCode15{prop:Color} = 15066597
    Elsif ?glo:FaultCode15{prop:Req} = True
        ?glo:FaultCode15{prop:FontColor} = 65793
        ?glo:FaultCode15{prop:Color} = 8454143
    Else ! If ?glo:FaultCode15{prop:Req} = True
        ?glo:FaultCode15{prop:FontColor} = 65793
        ?glo:FaultCode15{prop:Color} = 16777215
    End ! If ?glo:FaultCode15{prop:Req} = True
    ?glo:FaultCode15{prop:Trn} = 0
    ?glo:FaultCode15{prop:FontStyle} = font:Bold
    ?tmp:prompt_15{prop:FontColor} = -1
    ?tmp:prompt_15{prop:Color} = 15066597
    If ?glo:FaultCode16{prop:ReadOnly} = True
        ?glo:FaultCode16{prop:FontColor} = 65793
        ?glo:FaultCode16{prop:Color} = 15066597
    Elsif ?glo:FaultCode16{prop:Req} = True
        ?glo:FaultCode16{prop:FontColor} = 65793
        ?glo:FaultCode16{prop:Color} = 8454143
    Else ! If ?glo:FaultCode16{prop:Req} = True
        ?glo:FaultCode16{prop:FontColor} = 65793
        ?glo:FaultCode16{prop:Color} = 16777215
    End ! If ?glo:FaultCode16{prop:Req} = True
    ?glo:FaultCode16{prop:Trn} = 0
    ?glo:FaultCode16{prop:FontStyle} = font:Bold
    ?tmp:prompt_16{prop:FontColor} = -1
    ?tmp:prompt_16{prop:Color} = 15066597
    If ?glo:FaultCode17{prop:ReadOnly} = True
        ?glo:FaultCode17{prop:FontColor} = 65793
        ?glo:FaultCode17{prop:Color} = 15066597
    Elsif ?glo:FaultCode17{prop:Req} = True
        ?glo:FaultCode17{prop:FontColor} = 65793
        ?glo:FaultCode17{prop:Color} = 8454143
    Else ! If ?glo:FaultCode17{prop:Req} = True
        ?glo:FaultCode17{prop:FontColor} = 65793
        ?glo:FaultCode17{prop:Color} = 16777215
    End ! If ?glo:FaultCode17{prop:Req} = True
    ?glo:FaultCode17{prop:Trn} = 0
    ?glo:FaultCode17{prop:FontStyle} = font:Bold
    ?tmp:prompt_17{prop:FontColor} = -1
    ?tmp:prompt_17{prop:Color} = 15066597
    If ?glo:FaultCode18{prop:ReadOnly} = True
        ?glo:FaultCode18{prop:FontColor} = 65793
        ?glo:FaultCode18{prop:Color} = 15066597
    Elsif ?glo:FaultCode18{prop:Req} = True
        ?glo:FaultCode18{prop:FontColor} = 65793
        ?glo:FaultCode18{prop:Color} = 8454143
    Else ! If ?glo:FaultCode18{prop:Req} = True
        ?glo:FaultCode18{prop:FontColor} = 65793
        ?glo:FaultCode18{prop:Color} = 16777215
    End ! If ?glo:FaultCode18{prop:Req} = True
    ?glo:FaultCode18{prop:Trn} = 0
    ?glo:FaultCode18{prop:FontStyle} = font:Bold
    ?tmp:prompt_18{prop:FontColor} = -1
    ?tmp:prompt_18{prop:Color} = 15066597
    If ?glo:FaultCode19{prop:ReadOnly} = True
        ?glo:FaultCode19{prop:FontColor} = 65793
        ?glo:FaultCode19{prop:Color} = 15066597
    Elsif ?glo:FaultCode19{prop:Req} = True
        ?glo:FaultCode19{prop:FontColor} = 65793
        ?glo:FaultCode19{prop:Color} = 8454143
    Else ! If ?glo:FaultCode19{prop:Req} = True
        ?glo:FaultCode19{prop:FontColor} = 65793
        ?glo:FaultCode19{prop:Color} = 16777215
    End ! If ?glo:FaultCode19{prop:Req} = True
    ?glo:FaultCode19{prop:Trn} = 0
    ?glo:FaultCode19{prop:FontStyle} = font:Bold
    ?tmp:prompt_19{prop:FontColor} = -1
    ?tmp:prompt_19{prop:Color} = 15066597
    If ?glo:FaultCode20{prop:ReadOnly} = True
        ?glo:FaultCode20{prop:FontColor} = 65793
        ?glo:FaultCode20{prop:Color} = 15066597
    Elsif ?glo:FaultCode20{prop:Req} = True
        ?glo:FaultCode20{prop:FontColor} = 65793
        ?glo:FaultCode20{prop:Color} = 8454143
    Else ! If ?glo:FaultCode20{prop:Req} = True
        ?glo:FaultCode20{prop:FontColor} = 65793
        ?glo:FaultCode20{prop:Color} = 16777215
    End ! If ?glo:FaultCode20{prop:Req} = True
    ?glo:FaultCode20{prop:Trn} = 0
    ?glo:FaultCode20{prop:FontStyle} = font:Bold
    ?tmp:prompt_20{prop:FontColor} = -1
    ?tmp:prompt_20{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?glo:TradeFaultCode1:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode1:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode1{prop:ReadOnly} = True
        ?glo:TradeFaultCode1{prop:FontColor} = 65793
        ?glo:TradeFaultCode1{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode1{prop:Req} = True
        ?glo:TradeFaultCode1{prop:FontColor} = 65793
        ?glo:TradeFaultCode1{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode1{prop:Req} = True
        ?glo:TradeFaultCode1{prop:FontColor} = 65793
        ?glo:TradeFaultCode1{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode1{prop:Req} = True
    ?glo:TradeFaultCode1{prop:Trn} = 0
    ?glo:TradeFaultCode1{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode2:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode2:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode2{prop:ReadOnly} = True
        ?glo:TradeFaultCode2{prop:FontColor} = 65793
        ?glo:TradeFaultCode2{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode2{prop:Req} = True
        ?glo:TradeFaultCode2{prop:FontColor} = 65793
        ?glo:TradeFaultCode2{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode2{prop:Req} = True
        ?glo:TradeFaultCode2{prop:FontColor} = 65793
        ?glo:TradeFaultCode2{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode2{prop:Req} = True
    ?glo:TradeFaultCode2{prop:Trn} = 0
    ?glo:TradeFaultCode2{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode3:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode3:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode3{prop:ReadOnly} = True
        ?glo:TradeFaultCode3{prop:FontColor} = 65793
        ?glo:TradeFaultCode3{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode3{prop:Req} = True
        ?glo:TradeFaultCode3{prop:FontColor} = 65793
        ?glo:TradeFaultCode3{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode3{prop:Req} = True
        ?glo:TradeFaultCode3{prop:FontColor} = 65793
        ?glo:TradeFaultCode3{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode3{prop:Req} = True
    ?glo:TradeFaultCode3{prop:Trn} = 0
    ?glo:TradeFaultCode3{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode4:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode4:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode4{prop:ReadOnly} = True
        ?glo:TradeFaultCode4{prop:FontColor} = 65793
        ?glo:TradeFaultCode4{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode4{prop:Req} = True
        ?glo:TradeFaultCode4{prop:FontColor} = 65793
        ?glo:TradeFaultCode4{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode4{prop:Req} = True
        ?glo:TradeFaultCode4{prop:FontColor} = 65793
        ?glo:TradeFaultCode4{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode4{prop:Req} = True
    ?glo:TradeFaultCode4{prop:Trn} = 0
    ?glo:TradeFaultCode4{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode5:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode5:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode5{prop:ReadOnly} = True
        ?glo:TradeFaultCode5{prop:FontColor} = 65793
        ?glo:TradeFaultCode5{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode5{prop:Req} = True
        ?glo:TradeFaultCode5{prop:FontColor} = 65793
        ?glo:TradeFaultCode5{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode5{prop:Req} = True
        ?glo:TradeFaultCode5{prop:FontColor} = 65793
        ?glo:TradeFaultCode5{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode5{prop:Req} = True
    ?glo:TradeFaultCode5{prop:Trn} = 0
    ?glo:TradeFaultCode5{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode6:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode6:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode6{prop:ReadOnly} = True
        ?glo:TradeFaultCode6{prop:FontColor} = 65793
        ?glo:TradeFaultCode6{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode6{prop:Req} = True
        ?glo:TradeFaultCode6{prop:FontColor} = 65793
        ?glo:TradeFaultCode6{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode6{prop:Req} = True
        ?glo:TradeFaultCode6{prop:FontColor} = 65793
        ?glo:TradeFaultCode6{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode6{prop:Req} = True
    ?glo:TradeFaultCode6{prop:Trn} = 0
    ?glo:TradeFaultCode6{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode7:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode7:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode7{prop:ReadOnly} = True
        ?glo:TradeFaultCode7{prop:FontColor} = 65793
        ?glo:TradeFaultCode7{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode7{prop:Req} = True
        ?glo:TradeFaultCode7{prop:FontColor} = 65793
        ?glo:TradeFaultCode7{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode7{prop:Req} = True
        ?glo:TradeFaultCode7{prop:FontColor} = 65793
        ?glo:TradeFaultCode7{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode7{prop:Req} = True
    ?glo:TradeFaultCode7{prop:Trn} = 0
    ?glo:TradeFaultCode7{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode8:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode8:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode8{prop:ReadOnly} = True
        ?glo:TradeFaultCode8{prop:FontColor} = 65793
        ?glo:TradeFaultCode8{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode8{prop:Req} = True
        ?glo:TradeFaultCode8{prop:FontColor} = 65793
        ?glo:TradeFaultCode8{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode8{prop:Req} = True
        ?glo:TradeFaultCode8{prop:FontColor} = 65793
        ?glo:TradeFaultCode8{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode8{prop:Req} = True
    ?glo:TradeFaultCode8{prop:Trn} = 0
    ?glo:TradeFaultCode8{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode9:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode9:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode9{prop:ReadOnly} = True
        ?glo:TradeFaultCode9{prop:FontColor} = 65793
        ?glo:TradeFaultCode9{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode9{prop:Req} = True
        ?glo:TradeFaultCode9{prop:FontColor} = 65793
        ?glo:TradeFaultCode9{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode9{prop:Req} = True
        ?glo:TradeFaultCode9{prop:FontColor} = 65793
        ?glo:TradeFaultCode9{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode9{prop:Req} = True
    ?glo:TradeFaultCode9{prop:Trn} = 0
    ?glo:TradeFaultCode9{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode10:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode10:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode10{prop:ReadOnly} = True
        ?glo:TradeFaultCode10{prop:FontColor} = 65793
        ?glo:TradeFaultCode10{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode10{prop:Req} = True
        ?glo:TradeFaultCode10{prop:FontColor} = 65793
        ?glo:TradeFaultCode10{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode10{prop:Req} = True
        ?glo:TradeFaultCode10{prop:FontColor} = 65793
        ?glo:TradeFaultCode10{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode10{prop:Req} = True
    ?glo:TradeFaultCode10{prop:Trn} = 0
    ?glo:TradeFaultCode10{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode11:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode11:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode11{prop:ReadOnly} = True
        ?glo:TradeFaultCode11{prop:FontColor} = 65793
        ?glo:TradeFaultCode11{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode11{prop:Req} = True
        ?glo:TradeFaultCode11{prop:FontColor} = 65793
        ?glo:TradeFaultCode11{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode11{prop:Req} = True
        ?glo:TradeFaultCode11{prop:FontColor} = 65793
        ?glo:TradeFaultCode11{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode11{prop:Req} = True
    ?glo:TradeFaultCode11{prop:Trn} = 0
    ?glo:TradeFaultCode11{prop:FontStyle} = font:Bold
    ?glo:TradeFaultCode12:Prompt{prop:FontColor} = -1
    ?glo:TradeFaultCode12:Prompt{prop:Color} = 15066597
    If ?glo:TradeFaultCode12{prop:ReadOnly} = True
        ?glo:TradeFaultCode12{prop:FontColor} = 65793
        ?glo:TradeFaultCode12{prop:Color} = 15066597
    Elsif ?glo:TradeFaultCode12{prop:Req} = True
        ?glo:TradeFaultCode12{prop:FontColor} = 65793
        ?glo:TradeFaultCode12{prop:Color} = 8454143
    Else ! If ?glo:TradeFaultCode12{prop:Req} = True
        ?glo:TradeFaultCode12{prop:FontColor} = 65793
        ?glo:TradeFaultCode12{prop:Color} = 16777215
    End ! If ?glo:TradeFaultCode12{prop:Req} = True
    ?glo:TradeFaultCode12{prop:Trn} = 0
    ?glo:TradeFaultCode12{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Fault_Coding        Routine   ! Manufacturers Fault Codes

    ! Start Change 2621 BE(15/05/2003)
    ! Not carried forward from V3.1
    Clear(tmp:Prompt)
    ! End Change 2621 BE(15/05/2003)

    tmp:Required = 0
    access:chartype.clearkey(cha:charge_type_key)
    cha:charge_type = f:WCrg
    If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
        If cha:force_warranty = 'YES'
            If f:WRep <> ''
                Access:REPTYDEF.ClearKey(rtd:Warranty_Key)
                rtd:Warranty    = 'YES'
                rtd:Repair_Type = f:WRep
                If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                    !Found
                    If rtd:CompFaultCoding
                        tmp:Required = 1
                    End !If rtd:CompFaultCoding
                Else!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
            Else !If job:Repair_Type_Warranty <> ''
                tmp:Required = 1
            End !If job:Repair_Type_Warranty <> ''

        End
    end !if

    found# = 0
    setcursor(cursor:wait)
    save_maf_id = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = f:Man
    set(maf:field_number_key,maf:field_number_key)
    loop
        if access:manfault.next()
           break
        end !if
        if maf:manufacturer <> f:Man      |
            then break.  ! end if
        IF found# = 0
            found# = 1
            Unhide(?tab1)
        End!IF found# = 0
        hide# = 0
        req#  = 0

        If maf:Compulsory_At_Booking = 'YES'
            If tmp:Required
                req# = 1
            End!If tmp:Required
        Else !If maf:Compulsory_At_Booking = 'YES' and tmp:Required
            If f:req = InsertRecord
                Hide# = 1
            End !If f:req = InsertRecord
        End !If maf:Compulsory_At_Booking = 'YES' and tmp:Required

        If maf:Compulsory = 'YES' and tmp:Required
            If f:Req <> InsertRecord
                req# = 1
            End !If f:Req <> InsertRecord
        End !If maf:Compulsory = 'YES' and tmp:Required
        SolaceViewVars('Field: ' & maf:Field_Number,'Hide#: ' & hide#,'Req#: ' & req#,6)

        If maf:lookup = 'YES'
            field"  = ''
            records# = 0
            save_mfo_id = access:manfaulo.savefile()
            access:manfaulo.clearkey(mfo:field_key)
            mfo:manufacturer = maf:manufacturer
            mfo:field_number = maf:field_number
            set(mfo:field_key,mfo:field_key)
            loop
                if access:manfaulo.next()
                   break
                end !if
                if mfo:manufacturer <> maf:manufacturer      |
                or mfo:field_number <> maf:field_number      |
                    then break.  ! end if
                records# += 1
                If records# > 1
                    field" = ''
                    Break
                End!If records# > 1
                field"  = mfo:field
            end !loop
            access:manfaulo.restorefile(save_mfo_id)
            Case maf:field_number
                of 1
                    If glo:FaultCode1 = ''
                        glo:FaultCode1 = field"
                    End!If glo:FaultCode = ''
                of 2
                    If glo:FaultCode2 = ''
                        glo:FaultCode2 = field"
                    End!If glo:FaultCode = ''
                of 3
                    If glo:FaultCode3 = ''
                        glo:FaultCode3 = field"
                    End!If glo:FaultCode = ''
                of 4
                    If glo:FaultCode4 = ''
                        glo:FaultCode4 = field"
                    End!If glo:FaultCode = ''
                of 5
                    If glo:FaultCode5 = ''
                        glo:FaultCode5 = field"
                    End!If glo:FaultCode = ''
                of 6
                    If glo:FaultCode6 = ''
                        glo:FaultCode6 = field"
                    End!If glo:FaultCode = ''
                of 7
                    If glo:FaultCode7 = ''
                        glo:FaultCode7 = field"
                    End!If glo:FaultCode = ''
                of 8
                    If glo:FaultCode8 = ''
                        glo:FaultCode8 = field"
                    End!If glo:FaultCode = ''
                of 9
                    If glo:FaultCode9 = ''
                        glo:FaultCode9 = field"
                    End!If glo:FaultCode = ''
                of 10
                    If glo:FaultCode10 = ''
                        glo:FaultCode10 = field"
                    End!If glo:FaultCode = ''
                of 11
                    If glo:FaultCode11 = ''
                        glo:FaultCode11 = field"
                    End!If glo:FaultCode = ''
                of 12
                    If glo:FaultCode12 = ''
                        glo:FaultCode12 = field"
                    End!If glo:FaultCode = ''
                Of 13
                    If glo:FaultCode13 = ''
                        glo:FaultCode13 = Field"
                    End ! If glo:FaultCode13 = ''
                Of 14
                    If glo:FaultCode14 = ''
                        glo:FaultCode14 = Field"
                    End ! If glo:FaultCode14 = ''
                Of 15
                    If glo:FaultCode15 = ''
                        glo:FaultCode15 = Field"
                    End ! If glo:FaultCode15 = ''
                Of 16
                    If glo:FaultCode16 = ''
                        glo:FaultCode16 = Field"
                    End ! If glo:FaultCode16 = ''
                Of 17
                    If glo:FaultCode17 = ''
                        glo:FaultCode17 = Field"
                    End ! If glo:FaultCode17 = ''
                Of 18
                    If glo:FaultCode18 = ''
                        glo:FaultCode18 = Field"
                    End ! If glo:FaultCode18 = ''
                Of 19
                    If glo:FaultCode19 = ''
                        glo:FaultCode19 = Field"
                    End ! If glo:FaultCode19 = ''
                Of 20
                    If glo:FaultCode20 = ''
                        glo:FaultCode20 = Field"
                    End ! If glo:FaultCode20 = ''
            End!Case maf:field_number
        End!If maf:lookup = 'YES'

        Case maf:field_number
            Of 1
                If hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                   ! Unhide(?glo:FaultCode1:prompt)
                    ! End Change 2621 BE(15/05/03)
                    Unhide(?glo:FaultCode1)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[1] = maf:field_name
                    !?glo:FaultCode1:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    If req# = 1
                        ?glo:FaultCode1{prop:req} = 1
                    End!If req# = 1

                    Case maf:field_type
                        Of 'DATE'
                            ?glo:FaultCode1{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode1{prop:width} = 64
                            Unhide(?popcalendar)

                        Of 'STRING'
                            ?glo:FaultCode1{prop:width} = 124
                            If maf:restrictlength
                                ?glo:FaultCode1{prop:text} = '@s' & maf:lengthto
                            Else!If maf:restrictlength
                                ?glo:FaultCode1{prop:text} = '@s30'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup)
                            End!If maf:lookup = 'YES'
                        Of 'NUMBER'
                            ?glo:FaultCode1{prop:width} = 64
                            If maf:restrictlength
                                ?glo:FaultCode1{prop:text} = '@n' & maf:lengthto
                            Else!If maf:restrictlength
                                ?glo:FaultCode1{prop:text} = '@n9'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup)
                            End!If maf:lookup = 'YES'
                    End!Case maf:field_type

                End!If hide# = 0
            Of 2
                If hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !Unhide(?glo:FaultCode2:prompt)
                    ! End Change 2621 BE(15/05/03)
                    Unhide(?glo:FaultCode2)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[2] = maf:field_name
                    !?glo:FaultCode2:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    If req# = 1
                        ?glo:FaultCode2{prop:req} = 1
                    End!If req# = 1
                    Case maf:field_type
                        Of 'DATE'
                            ?glo:FaultCode2{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode2{prop:width} = 64
                            Unhide(?popcalendar:2)

                        Of 'STRING'
                            ?glo:FaultCode2{prop:width} = 124
                            If maf:restrictlength
                                ?glo:FaultCode2{prop:text} = '@s' & maf:lengthto
                            Else!If maf:restrictlength
                                ?glo:FaultCode2{prop:text} = '@s30'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup:2)
                            End!If maf:lookup = 'YES'
                        Of 'NUMBER'

                            ?glo:FaultCode2{prop:width} = 64
                            If maf:restrictlength
                                ?glo:FaultCode2{prop:text} = '@n' & maf:lengthto
                            Else!If maf:restrictlength
                                ?glo:FaultCode2{prop:text} = '@n9'
                            End!If maf:restrictlength
                            If maf:lookup = 'YES'
                                Unhide(?lookup:2)
                            End!If maf:lookup = 'YES'
                    End!Case maf:field_type
                End!If hide# = 0

            Of 3
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode3:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode3)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[3] = maf:field_name
                    !?glo:FaultCode3:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode3{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode3{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode3{prop:width} = 64
                            unhide(?popcalendar:3)

                        of 'STRING'
                            ?glo:FaultCode3{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode3{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode3{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:3)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode3{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode3{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode3{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:3)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 4
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode4:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode4)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[4] = maf:field_name
                    !?glo:FaultCode4:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode4{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode4{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode4{prop:width} = 64
                            unhide(?popcalendar:4)

                        of 'STRING'
                            ?glo:FaultCode4{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode4{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode4{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:4)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode4{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode4{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode4{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:4)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 5
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode5:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode5)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[5] = maf:field_name
                    !?glo:FaultCode5:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode5{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode5{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode5{prop:width} = 64
                            unhide(?popcalendar:5)

                        of 'STRING'
                            ?glo:FaultCode5{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode5{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode5{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:5)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode5{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode5{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode5{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:5)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 6
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode6:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode6)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[6] = maf:field_name
                    !?glo:FaultCode6:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode6{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode6{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode6{prop:width} = 64
                            unhide(?popcalendar:6)

                        of 'STRING'
                            ?glo:FaultCode6{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode6{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode6{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:6)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode6{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode6{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode6{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:6)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 7
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode7:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode7)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[7] = maf:field_name
                    !?glo:FaultCode7:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode7{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode7{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode7{prop:width} = 64
                            unhide(?popcalendar:7)

                        of 'STRING'
                            ?glo:FaultCode7{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode7{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode7{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:7)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode7{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode7{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode7{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:7)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0

            Of 8
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode8:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode8)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[8] = maf:field_name
                    !?glo:FaultCode8:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode8{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode8{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode8{prop:width} = 64
                            unhide(?popcalendar:8)

                        of 'STRING'
                            ?glo:FaultCode8{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode8{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode8{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:8)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode8{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode8{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode8{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:8)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 9
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode9:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode9)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[9] = maf:field_name
                    !?glo:FaultCode9:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode9{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode9{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode9{prop:width} = 64
                            unhide(?popcalendar:9)

                        of 'STRING'
                            ?glo:FaultCode9{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode9{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode9{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:9)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode9{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode9{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode9{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:9)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 10
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode10:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode10)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[10] = maf:field_name
                    !?glo:FaultCode10:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode10{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode10{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode10{prop:width} = 64
                            unhide(?popcalendar:10)

                        of 'STRING'
                            ?glo:FaultCode10{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode10{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode10{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:10)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode10{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode10{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode10{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:10)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 11
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode11:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode11)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[11] = maf:field_name
                    !?glo:FaultCode11:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode11{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode11{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode11{prop:width} = 64
                            unhide(?popcalendar:11)

                        of 'STRING'
                            ?glo:FaultCode11{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode11{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode11{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:11)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode11{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode11{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode11{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:11)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 12
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode12:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode12)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[12] = maf:field_name
                    !?glo:FaultCode12:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode12{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode12{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode12{prop:width} = 64
                            unhide(?popcalendar:12)

                        of 'STRING'
                            ?glo:FaultCode12{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode12{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode12{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:12)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode12{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode12{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode12{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:12)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 13
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode12:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode13)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[13] = maf:field_name
                    !?glo:FaultCode12:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode13{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode13{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode13{prop:width} = 64
                            unhide(?popcalendar:13)

                        of 'STRING'
                            ?glo:FaultCode13{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode13{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode13{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:13)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode13{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode13{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode13{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:13)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 14
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode12:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode14)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[14] = maf:field_name
                    !?glo:FaultCode12:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode14{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode14{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode14{prop:width} = 64
                            unhide(?popcalendar:14)

                        of 'STRING'
                            ?glo:FaultCode14{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode14{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode14{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:14)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode14{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode14{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode14{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:14)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 15
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode12:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode15)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[15] = maf:field_name
                    !?glo:FaultCode12:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode15{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode15{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode15{prop:width} = 64
                            unhide(?popcalendar:15)

                        of 'STRING'
                            ?glo:FaultCode15{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode15{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode15{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:15)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode15{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode15{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode15{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:15)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 16
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode12:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode16)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[16] = maf:field_name
                    !?glo:FaultCode12:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode16{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode16{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode16{prop:width} = 64
                            unhide(?popcalendar:16)

                        of 'STRING'
                            ?glo:FaultCode16{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode16{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode16{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:16)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode16{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode16{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode16{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:16)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 17
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode12:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode17)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[17] = maf:field_name
                    !?glo:FaultCode12:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode17{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode17{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode17{prop:width} = 64
                            unhide(?popcalendar:17)

                        of 'STRING'
                            ?glo:FaultCode17{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode17{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode17{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:17)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode17{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode17{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode17{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:17)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 18
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode12:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode18)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[18] = maf:field_name
                    !?glo:FaultCode12:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode18{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode18{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode18{prop:width} = 64
                            unhide(?popcalendar:18)

                        of 'STRING'
                            ?glo:FaultCode18{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode18{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode18{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:18)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode18{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode18{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode18{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:18)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 19
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode12:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode19)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[19] = maf:field_name
                    !?glo:FaultCode12:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode19{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode19{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode19{prop:width} = 64
                            unhide(?popcalendar:19)

                        of 'STRING'
                            ?glo:FaultCode19{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode19{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode19{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:19)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode19{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode19{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode19{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:19)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
            Of 20
                if hide# = 0
                    ! Start Change 2621 BE(15/05/03)
                    !unhide(?glo:FaultCode12:prompt)
                    ! End Change 2621 BE(15/05/03)
                    unhide(?glo:FaultCode20)
                    !Start Change 2621 BE(15/05/03)
                    tmp:Prompt[20] = maf:field_name
                    !?glo:FaultCode12:prompt{prop:text} = maf:field_name
                    ! End Change 2621 BE(15/05/03)
                    if req# = 1
                        ?glo:FaultCode20{prop:req} = 1
                    end!if req# = 1

                    case maf:field_type
                        of 'DATE'
                            ?glo:FaultCode20{prop:text} = Clip(maf:DateType)
                            ?glo:FaultCode20{prop:width} = 64
                            unhide(?popcalendar:20)

                        of 'STRING'
                            ?glo:FaultCode20{prop:width} = 124
                            if maf:restrictlength
                                ?glo:FaultCode20{prop:text} = '@s' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode20{prop:text} = '@s30'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:20)
                            end!if maf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:FaultCode20{prop:width} = 64
                            if maf:restrictlength
                                ?glo:FaultCode20{prop:text} = '@n' & maf:lengthto
                            else!if maf:restrictlength
                                ?glo:FaultCode20{prop:text} = '@n9'
                            end!if maf:restrictlength
                            if maf:lookup = 'YES'
                                unhide(?lookup:20)
                            end!if maf:lookup = 'YES'
                    end!case maf:field_type
                end!if hide# = 0
        End
    end !loop
    access:manfault.restorefile(save_maf_id)
    setcursor()
    Display()
! Contined....
    found# = 0
    setcursor(cursor:wait)
    save_taf_id = access:trafault.savefile()
    access:trafault.clearkey(taf:field_number_key)
    taf:accountnumber = tra:Account_Number
    set(taf:field_number_key,taf:field_number_key)
    loop
        if access:trafault.next()
           break
        end !if
        if taf:accountnumber <> tra:Account_Number      |
            then break.  ! end if
        if found# = 0
            found# = 1
            unhide(?Tab2)
        End!if found# = 0
        hide# = 0
        req#  = 0

        If taf:Compulsory_At_Booking = 'YES'
            req# = 1
        Else !If taf:Compulsory_At_Booking = 'YES'
            If f:Req = InsertRecord
                Hide# = 1
            End !If f:Req = InsertRecord
        End !If taf:Compulsory_At_Booking = 'YES'

        If taf:Compulsory = 'YES' and f:Req <> InsertRecord
            req# = 1
        End !If taf:Compulsory = 'YES' and f:Req <> InsertRecord

        If taf:lookup = 'YES'
            field"  = ''
            records# = 0
            save_tfo_id = access:trafaulo.savefile()
            access:trafaulo.clearkey(tfo:field_key)
            tfo:accountnumber = taf:accountnumber
            tfo:field_number = taf:field_number
            set(tfo:field_key,tfo:field_key)
            loop
                if access:trafaulo.next()
                   break
                end !if
                if tfo:accountnumber <> taf:accountnumber      |
                or tfo:field_number <> taf:field_number      |
                    then break.  ! end if
                records# += 1
                If records# > 1
                    field" = ''
                    Break
                End!If records# > 1
                field"  = tfo:field
            end !loop
            access:trafaulo.restorefile(save_tfo_id)
            Case taf:field_number
                of 1
                    If glo:TradeFaultCode1 = ''
                        glo:TradeFaultCode1 = field"
                    End!If glo:TradeFaultCode = ''
                of 2
                    If glo:TradeFaultCode2 = ''
                        glo:TradeFaultCode2 = field"
                    End!If glo:TradeFaultCode = ''
                of 3
                    If glo:TradeFaultCode3 = ''
                        glo:TradeFaultCode3 = field"
                    End!If glo:TradeFaultCode = ''
                of 4
                    If glo:TradeFaultCode4 = ''
                        glo:TradeFaultCode4 = field"
                    End!If glo:TradeFaultCode = ''
                of 5
                    If glo:TradeFaultCode5 = ''
                        glo:TradeFaultCode5 = field"
                    End!If glo:TradeFaultCode = ''
                of 6
                    If glo:TradeFaultCode6 = ''
                        glo:TradeFaultCode6 = field"
                    End!If glo:TradeFaultCode = ''
                of 7
                    If glo:TradeFaultCode7 = ''
                        glo:TradeFaultCode7 = field"
                    End!If glo:TradeFaultCode = ''
                of 8
                    If glo:TradeFaultCode8 = ''
                        glo:TradeFaultCode8 = field"
                    End!If glo:TradeFaultCode = ''
                of 9
                    If glo:TradeFaultCode9 = ''
                        glo:TradeFaultCode9 = field"
                    End!If glo:TradeFaultCode = ''
                of 10
                    If glo:TradeFaultCode10 = ''
                        glo:TradeFaultCode10 = field"
                    End!If glo:TradeFaultCode = ''
                of 11
                    If glo:TradeFaultCode11 = ''
                        glo:TradeFaultCode11 = field"
                    End!If glo:TradeFaultCode = ''
                of 12
                    If glo:TradeFaultCode12 = ''
                        glo:TradeFaultCode12 = field"
                    End!If glo:TradeFaultCode = ''
            End!Case taf:field_number
        End!If taf:lookup = 'YES'

        Case taf:field_number
            Of 1
                If hide# = 0
                    Unhide(?glo:TradeFaultCode1:prompt)
                    Unhide(?glo:TradeFaultCode1)
                    ?glo:TradeFaultCode1:prompt{prop:text} = taf:field_name
                    If req# = 1
                        ?glo:TradeFaultCode1{prop:req} = 1
                    End!If req# = 1

                    Case taf:field_type
                        Of 'DATE'
                            ?glo:TradeFaultCode1{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode1{prop:width} = 64
                            Unhide(?popcalendar:13)

                        Of 'STRING'
                            ?glo:TradeFaultCode1{prop:width} = 124
                            If taf:restrictlength
                                ?glo:TradeFaultCode1{prop:text} = '@s' & taf:lengthto
                            Else!If taf:restrictlength
                                ?glo:TradeFaultCode1{prop:text} = '@s30'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:1)
                            End!If taf:lookup = 'YES'
                        Of 'NUMBER'
                            ?glo:TradeFaultCode1{prop:width} = 64
                            If taf:restrictlength
                                ?glo:TradeFaultCode1{prop:text} = '@n' & taf:lengthto
                            Else!If taf:restrictlength
                                ?glo:TradeFaultCode1{prop:text} = '@n9'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:1)
                            End!If taf:lookup = 'YES'
                    End!Case taf:field_type

                End!If hide# = 0
            Of 2
                If hide# = 0
                    Unhide(?glo:TradeFaultCode2:prompt)
                    Unhide(?glo:TradeFaultCode2)
                    ?glo:TradeFaultCode2:prompt{prop:text} = taf:field_name
                    If req# = 1
                        ?glo:TradeFaultCode2{prop:req} = 1
                    End!If req# = 1
                    Case taf:field_type
                        Of 'DATE'
                            ?glo:TradeFaultCode2{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode2{prop:width} = 64
                            Unhide(?popcalendar:14)

                        Of 'STRING'
                            ?glo:TradeFaultCode2{prop:width} = 124
                            If taf:restrictlength
                                ?glo:TradeFaultCode2{prop:text} = '@s' & taf:lengthto
                            Else!If taf:restrictlength
                                ?glo:TradeFaultCode2{prop:text} = '@s30'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:2)
                            End!If taf:lookup = 'YES'
                        Of 'NUMBER'

                            ?glo:TradeFaultCode2{prop:width} = 64
                            If taf:restrictlength
                                ?glo:TradeFaultCode2{prop:text} = '@n' & taf:lengthto
                            Else!If taf:restrictlength
                                ?glo:TradeFaultCode2{prop:text} = '@n9'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:2)
                            End!If taf:lookup = 'YES'
                    End!Case taf:field_type
                End!If hide# = 0

            Of 3
                if hide# = 0
                    unhide(?glo:TradeFaultCode3:prompt)
                    unhide(?glo:TradeFaultCode3)
                    ?glo:TradeFaultCode3:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?glo:TradeFaultCode3{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?glo:TradeFaultCode3{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode3{prop:width} = 64
                            unhide(?popcalendar:15)

                        of 'STRING'
                            ?glo:TradeFaultCode3{prop:width} = 124
                            if taf:restrictlength
                                ?glo:TradeFaultCode3{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode3{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:3)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:TradeFaultCode3{prop:width} = 64
                            if taf:restrictlength
                                ?glo:TradeFaultCode3{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode3{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:3)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 4
                if hide# = 0
                    unhide(?glo:TradeFaultCode4:prompt)
                    unhide(?glo:TradeFaultCode4)
                    ?glo:TradeFaultCode4:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?glo:TradeFaultCode4{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?glo:TradeFaultCode4{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode4{prop:width} = 64
                            unhide(?popcalendar:16)

                        of 'STRING'
                            ?glo:TradeFaultCode4{prop:width} = 124
                            if taf:restrictlength
                                ?glo:TradeFaultCode4{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode4{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:4)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:TradeFaultCode4{prop:width} = 64
                            if taf:restrictlength
                                ?glo:TradeFaultCode4{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode4{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:4)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 5
                if hide# = 0
                    unhide(?glo:TradeFaultCode5:prompt)
                    unhide(?glo:TradeFaultCode5)
                    ?glo:TradeFaultCode5:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?glo:TradeFaultCode5{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?glo:TradeFaultCode5{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode5{prop:width} = 64
                            unhide(?popcalendar:17)

                        of 'STRING'
                            ?glo:TradeFaultCode5{prop:width} = 124
                            if taf:restrictlength
                                ?glo:TradeFaultCode5{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode5{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:5)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:TradeFaultCode5{prop:width} = 64
                            if taf:restrictlength
                                ?glo:TradeFaultCode5{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode5{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:5)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 6
                if hide# = 0
                    unhide(?glo:TradeFaultCode6:prompt)
                    unhide(?glo:TradeFaultCode6)
                    ?glo:TradeFaultCode6:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?glo:TradeFaultCode6{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?glo:TradeFaultCode6{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode6{prop:width} = 64
                            unhide(?popcalendar:18)

                        of 'STRING'
                            ?glo:TradeFaultCode6{prop:width} = 124
                            if taf:restrictlength
                                ?glo:TradeFaultCode6{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode6{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:6)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:TradeFaultCode6{prop:width} = 64
                            if taf:restrictlength
                                ?glo:TradeFaultCode6{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode6{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:6)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 7
                if hide# = 0
                    unhide(?glo:TradeFaultCode7:prompt)
                    unhide(?glo:TradeFaultCode7)
                    ?glo:TradeFaultCode7:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?glo:TradeFaultCode7{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?glo:TradeFaultCode7{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode7{prop:width} = 64
                            unhide(?popcalendar:19)

                        of 'STRING'
                            ?glo:TradeFaultCode7{prop:width} = 124
                            if taf:restrictlength
                                ?glo:TradeFaultCode7{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode7{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:7)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:TradeFaultCode7{prop:width} = 64
                            if taf:restrictlength
                                ?glo:TradeFaultCode7{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode7{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:7)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0

            Of 8
                if hide# = 0
                    unhide(?glo:TradeFaultCode8:prompt)
                    unhide(?glo:TradeFaultCode8)
                    ?glo:TradeFaultCode8:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?glo:TradeFaultCode8{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?glo:TradeFaultCode8{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode8{prop:width} = 64
                            unhide(?popcalendar:20)

                        of 'STRING'
                            ?glo:TradeFaultCode8{prop:width} = 124
                            if taf:restrictlength
                                ?glo:TradeFaultCode8{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode8{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:8)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:TradeFaultCode8{prop:width} = 64
                            if taf:restrictlength
                                ?glo:TradeFaultCode8{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode8{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:8)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 9
                if hide# = 0
                    unhide(?glo:TradeFaultCode9:prompt)
                    unhide(?glo:TradeFaultCode9)
                    ?glo:TradeFaultCode9:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?glo:TradeFaultCode9{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?glo:TradeFaultCode9{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode9{prop:width} = 64
                            unhide(?popcalendar:21)

                        of 'STRING'
                            ?glo:TradeFaultCode9{prop:width} = 124
                            if taf:restrictlength
                                ?glo:TradeFaultCode9{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode9{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:9)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:TradeFaultCode9{prop:width} = 64
                            if taf:restrictlength
                                ?glo:TradeFaultCode9{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode9{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:9)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 10
                if hide# = 0
                    unhide(?glo:TradeFaultCode10:prompt)
                    unhide(?glo:TradeFaultCode10)
                    ?glo:TradeFaultCode10:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?glo:TradeFaultCode10{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?glo:TradeFaultCode10{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode10{prop:width} = 64
                            unhide(?popcalendar:22)

                        of 'STRING'
                            ?glo:TradeFaultCode10{prop:width} = 124
                            if taf:restrictlength
                                ?glo:TradeFaultCode10{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode10{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:10)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:TradeFaultCode10{prop:width} = 64
                            if taf:restrictlength
                                ?glo:TradeFaultCode10{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode10{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:10)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 11
                if hide# = 0
                    unhide(?glo:TradeFaultCode11:prompt)
                    unhide(?glo:TradeFaultCode11)
                    ?glo:TradeFaultCode11:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?glo:TradeFaultCode11{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?glo:TradeFaultCode11{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode11{prop:width} = 64
                            unhide(?popcalendar:23)

                        of 'STRING'
                            ?glo:TradeFaultCode11{prop:width} = 124
                            if taf:restrictlength
                                ?glo:TradeFaultCode11{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode11{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:11)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:TradeFaultCode11{prop:width} = 64
                            if taf:restrictlength
                                ?glo:TradeFaultCode11{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode11{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:11)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 12
                if hide# = 0
                    unhide(?glo:TradeFaultCode12:prompt)
                    unhide(?glo:TradeFaultCode12)
                    ?glo:TradeFaultCode12:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?glo:TradeFaultCode12{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?glo:TradeFaultCode12{prop:text} = Clip(maf:DateType)
                            ?glo:TradeFaultCode12{prop:width} = 64
                            unhide(?popcalendar:24)

                        of 'STRING'
                            ?glo:TradeFaultCode12{prop:width} = 124
                            if taf:restrictlength
                                ?glo:TradeFaultCode12{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode12{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:12)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?glo:TradeFaultCode12{prop:width} = 64
                            if taf:restrictlength
                                ?glo:TradeFaultCode12{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?glo:TradeFaultCode12{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:12)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
        End
    end !loop
    access:trafault.restorefile(save_taf_id)
    setcursor()

    If ?tab1{prop:hide} = 1 And ?tab2{prop:hide} = 1
        ?tab3{prop:hide} = 0
    End!If ?tab1{prop:hide} = 0 Or ?tab2{prop:hide} = 0

    Display()
! Start Change 2621 BE(15/05/2003
!
! Following code was not carried forward from V3.1.
! Have checked with Bryan Harrison that it shoukld have been.
! Replaced code for reference is commented out at the end of this section.
!
!Old Fault Code Names
!Old Fault Code Names

    ! Start Change 2621 BE(15/05/03)
    !
    ! Goodness knows what this code is supposed to do:
    ! Every time customer changes Fault Code 9 requires a code change here !!!!!!!!!
    ! Code has changed in V3.2 - or is it just that this V3.1 code change has not been carried forward ?????????????????
    !
    !If f:Man ='NOKIA' and glo:FaultCode9 <> '0' And glo:FaultCode9 <> '1' And glo:FaultCode9 <> ''
    If f:Man ='NOKIA' and glo:FaultCode9 <> '0' And glo:FaultCode9 <> '1' And glo:FaultCode9 <> '2' and glo:FaultCode9 <> '3' And glo:FaultCode9 <> '4' And glo:FaultCode9 <> ''
    ! End Change 2621 BE(15/05/03)
        ?Lookup{prop:Hide} = 1
        ?Lookup:2{prop:Hide} = 1
        ?Lookup:3{prop:Hide} = 1
        ?Lookup:4{prop:Hide} = 1
        ?Lookup:5{prop:Hide} = 1
        ?Lookup:6{prop:Hide} = 1
        ?Lookup:7{prop:Hide} = 1
        ?Lookup:8{prop:Hide} = 1
        ?Lookup:9{prop:Hide} = 1
        ?Lookup:10{prop:Hide} = 1
        ?Lookup:11{prop:Hide} = 1
        ?Lookup:12{prop:Hide} = 1
        If sav:Prompt[1] = ''
            ?glo:FaultCode1{prop:Hide} = 1
            tmp:Prompt[1] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode1{prop:Hide} = 0
            tmp:Prompt[1] = sav:Prompt[1]
        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[2] = ''
            ?glo:FaultCode2{prop:Hide} = 1
            tmp:Prompt[2] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode2{prop:Hide} = 0
            tmp:Prompt[2] = sav:Prompt[2]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[3] = ''
            ?glo:FaultCode3{prop:Hide} = 1
            tmp:Prompt[3] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode3{prop:Hide} = 0
            tmp:Prompt[3] = sav:Prompt[3]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[4] = ''
            ?glo:FaultCode4{prop:Hide} = 1
            tmp:Prompt[4] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode4{prop:Hide} = 0
            tmp:Prompt[4] = sav:Prompt[4]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[5] = ''
            ?glo:FaultCode5{prop:Hide} = 1
            tmp:Prompt[5] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode5{prop:Hide} = 0
            tmp:Prompt[5] = sav:Prompt[5]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[6] = ''
            ?glo:FaultCode6{prop:Hide} = 1
            tmp:Prompt[6] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode6{prop:Hide} = 0
            tmp:Prompt[6] = sav:Prompt[6]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[7] = ''
            ?glo:FaultCode7{prop:Hide} = 1
            tmp:Prompt[7] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode7{prop:Hide} = 0
            tmp:Prompt[7] = sav:Prompt[7]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[8] = ''
            ?glo:FaultCode8{prop:Hide} = 1
            tmp:Prompt[8] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode8{prop:Hide} = 0
            tmp:Prompt[8] = sav:Prompt[8]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[9] = ''
            ?glo:FaultCode9{prop:Hide} = 1
            tmp:Prompt[9] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode9{prop:Hide} = 0
            tmp:Prompt[9] = sav:Prompt[9]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[10] = ''
            ?glo:FaultCode10{prop:Hide} = 1
            tmp:Prompt[10] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode10{prop:Hide} = 0
            tmp:Prompt[10] = sav:Prompt[10]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[11] = ''
            ?glo:FaultCode11{prop:Hide} = 1
            tmp:Prompt[11] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode11{prop:Hide} = 0
            tmp:Prompt[11] = sav:Prompt[11]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

        If sav:Prompt[12] = ''
            ?glo:FaultCode12{prop:Hide} = 1
            tmp:Prompt[12] = ''
        Else !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''
            ?glo:FaultCode12{prop:Hide} = 0
            tmp:Prompt[12] = sav:Prompt[12]

        End !If GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI') <> ''

    End !If job:Date_Booked < (Deformat(GETINI('CUTOFF','Date',,CLIP(PATH())&'\NFAULTS.INI'),@d6))

    !ThisMakeOver.SetWindow(Win:FORM)
! End Change 2621 BE(15/05/2003)
PutTradeNotes       Routine
    If f:Job
        access:jobnotes.clearkey(jbn:refnumberkey)
        jbn:refnumber = f:Job
        if access:jobnotes.tryfetch(jbn:refnumberkey)
            access:jobnotes.primerecord()
            access:jobnotes.tryinsert()
            access:jobnotes.clearkey(jbn:refnumberkey)
            jbn:refnumber   = f:Job
            access:jobnotes.tryfetch(jbn:refnumberkey)
        end!if access:jobnotes.tryfetch(jbn:refnumberkey)

        if taf:replicatefault = 'YES'
            if jbn:fault_description = ''
                jbn:fault_description = clip(tfo:description)
            else!if jbn:fault_description = ''
                jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(tfo:description)
            end!if jbn:fault_description = ''
        end!if mfo:replicatefault = 'YES'
        if taf:replicateinvoice = 'YES'
            if jbn:invoice_text = ''
                jbn:invoice_text = clip(tfo:description)
            else!if jbn:fault_description = ''
                jbn:invoice_text = clip(jbn:invoice_text) & '<13,10>' & clip(tfo:description)
            end!if jbn:fault_description = ''
        end!if mfo:replicatefault = 'YES'

        access:jobnotes.update()
    End !If f:Job


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'GenericFaultCodes',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'GenericFaultCodes',1)
    SolaceViewVars('tmp:Required',tmp:Required,'GenericFaultCodes',1)
    SolaceViewVars('save_taf_id',save_taf_id,'GenericFaultCodes',1)
    SolaceViewVars('save_tfo_id',save_tfo_id,'GenericFaultCodes',1)
    SolaceViewVars('save_mfo_id',save_mfo_id,'GenericFaultCodes',1)
    SolaceViewVars('ActionMessage',ActionMessage,'GenericFaultCodes',1)
    
      Loop SolaceDim1# = 1 to 12
        SolaceFieldName" = 'sav:prompt' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",sav:prompt[SolaceDim1#],'GenericFaultCodes',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 20
        SolaceFieldName" = 'tmp:prompt' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tmp:prompt[SolaceDim1#],'GenericFaultCodes',1)
      End
    
    


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_1;  SolaceCtrlName = '?tmp:prompt_1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode1;  SolaceCtrlName = '?glo:FaultCode1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup;  SolaceCtrlName = '?Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_2;  SolaceCtrlName = '?tmp:prompt_2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode2;  SolaceCtrlName = '?glo:FaultCode2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:2;  SolaceCtrlName = '?Lookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode3;  SolaceCtrlName = '?glo:FaultCode3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:3;  SolaceCtrlName = '?Lookup:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_3;  SolaceCtrlName = '?tmp:prompt_3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode4;  SolaceCtrlName = '?glo:FaultCode4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:4;  SolaceCtrlName = '?Lookup:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_4;  SolaceCtrlName = '?tmp:prompt_4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_5;  SolaceCtrlName = '?tmp:prompt_5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode5;  SolaceCtrlName = '?glo:FaultCode5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:5;  SolaceCtrlName = '?Lookup:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_6;  SolaceCtrlName = '?tmp:prompt_6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode6;  SolaceCtrlName = '?glo:FaultCode6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:6;  SolaceCtrlName = '?Lookup:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_7;  SolaceCtrlName = '?tmp:prompt_7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode7;  SolaceCtrlName = '?glo:FaultCode7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:7;  SolaceCtrlName = '?Lookup:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_8;  SolaceCtrlName = '?tmp:prompt_8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode8;  SolaceCtrlName = '?glo:FaultCode8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:8;  SolaceCtrlName = '?Lookup:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_9;  SolaceCtrlName = '?tmp:prompt_9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode9;  SolaceCtrlName = '?glo:FaultCode9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:9;  SolaceCtrlName = '?Lookup:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_10;  SolaceCtrlName = '?tmp:prompt_10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode10;  SolaceCtrlName = '?glo:FaultCode10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:10;  SolaceCtrlName = '?Lookup:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_11;  SolaceCtrlName = '?tmp:prompt_11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode11;  SolaceCtrlName = '?glo:FaultCode11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:11;  SolaceCtrlName = '?Lookup:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_12;  SolaceCtrlName = '?tmp:prompt_12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode12;  SolaceCtrlName = '?glo:FaultCode12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:12;  SolaceCtrlName = '?Lookup:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_13;  SolaceCtrlName = '?tmp:prompt_13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode13;  SolaceCtrlName = '?glo:FaultCode13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:25;  SolaceCtrlName = '?PopCalendar:25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:13;  SolaceCtrlName = '?Lookup:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode14;  SolaceCtrlName = '?glo:FaultCode14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:26;  SolaceCtrlName = '?PopCalendar:26';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:14;  SolaceCtrlName = '?Lookup:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_14;  SolaceCtrlName = '?tmp:prompt_14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode15;  SolaceCtrlName = '?glo:FaultCode15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:27;  SolaceCtrlName = '?PopCalendar:27';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_15;  SolaceCtrlName = '?tmp:prompt_15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode16;  SolaceCtrlName = '?glo:FaultCode16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:28;  SolaceCtrlName = '?PopCalendar:28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:16;  SolaceCtrlName = '?Lookup:16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:15;  SolaceCtrlName = '?Lookup:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_16;  SolaceCtrlName = '?tmp:prompt_16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode17;  SolaceCtrlName = '?glo:FaultCode17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:29;  SolaceCtrlName = '?PopCalendar:29';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_17;  SolaceCtrlName = '?tmp:prompt_17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode18;  SolaceCtrlName = '?glo:FaultCode18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:30;  SolaceCtrlName = '?PopCalendar:30';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:18;  SolaceCtrlName = '?Lookup:18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:17;  SolaceCtrlName = '?Lookup:17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_18;  SolaceCtrlName = '?tmp:prompt_18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode19;  SolaceCtrlName = '?glo:FaultCode19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:31;  SolaceCtrlName = '?PopCalendar:31';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:19;  SolaceCtrlName = '?Lookup:19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_19;  SolaceCtrlName = '?tmp:prompt_19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:FaultCode20;  SolaceCtrlName = '?glo:FaultCode20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:32;  SolaceCtrlName = '?PopCalendar:32';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:20;  SolaceCtrlName = '?Lookup:20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:prompt_20;  SolaceCtrlName = '?tmp:prompt_20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode1:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode1;  SolaceCtrlName = '?glo:TradeFaultCode1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:13;  SolaceCtrlName = '?PopCalendar:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:1;  SolaceCtrlName = '?tralookup:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode2:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode2;  SolaceCtrlName = '?glo:TradeFaultCode2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:14;  SolaceCtrlName = '?PopCalendar:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode3:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode3;  SolaceCtrlName = '?glo:TradeFaultCode3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:15;  SolaceCtrlName = '?PopCalendar:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode4:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode4;  SolaceCtrlName = '?glo:TradeFaultCode4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:16;  SolaceCtrlName = '?PopCalendar:16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:4;  SolaceCtrlName = '?tralookup:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode5:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode5;  SolaceCtrlName = '?glo:TradeFaultCode5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:17;  SolaceCtrlName = '?PopCalendar:17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:5;  SolaceCtrlName = '?tralookup:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode6:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode6;  SolaceCtrlName = '?glo:TradeFaultCode6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:18;  SolaceCtrlName = '?PopCalendar:18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:2;  SolaceCtrlName = '?tralookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:3;  SolaceCtrlName = '?tralookup:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode7:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode7;  SolaceCtrlName = '?glo:TradeFaultCode7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:19;  SolaceCtrlName = '?PopCalendar:19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:7;  SolaceCtrlName = '?tralookup:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode8:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode8;  SolaceCtrlName = '?glo:TradeFaultCode8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:20;  SolaceCtrlName = '?PopCalendar:20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:8;  SolaceCtrlName = '?tralookup:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode9:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode9;  SolaceCtrlName = '?glo:TradeFaultCode9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:21;  SolaceCtrlName = '?PopCalendar:21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode10:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode10;  SolaceCtrlName = '?glo:TradeFaultCode10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:22;  SolaceCtrlName = '?PopCalendar:22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode11:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode11;  SolaceCtrlName = '?glo:TradeFaultCode11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:23;  SolaceCtrlName = '?PopCalendar:23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode12:Prompt;  SolaceCtrlName = '?glo:TradeFaultCode12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:TradeFaultCode12;  SolaceCtrlName = '?glo:TradeFaultCode12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:24;  SolaceCtrlName = '?PopCalendar:24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:9;  SolaceCtrlName = '?tralookup:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:11;  SolaceCtrlName = '?tralookup:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:6;  SolaceCtrlName = '?tralookup:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:10;  SolaceCtrlName = '?tralookup:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:12;  SolaceCtrlName = '?tralookup:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt25;  SolaceCtrlName = '?Prompt25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('GenericFaultCodes')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'GenericFaultCodes')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:prompt_1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:REPTYDEF.Open
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:JOBNOTES.UseFile
  Access:TRAFAULT.UseFile
  Access:TRAFAULO.UseFile
  Access:JOBSE.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
      ! Start Change 2621 BE(15/05/2003)
      !If f:Man ='NOKIA' and glo:FaultCode9 <> '0' And glo:FaultCode9 <> '1' And glo:FaultCode9 <> ''
      If f:Man ='NOKIA' and glo:FaultCode9 <> '0' And glo:FaultCode9 <> '1' And glo:FaultCode9 <> '2' and glo:FaultCode9 <> '3' And glo:FaultCode9 <> '4' And glo:FaultCode9 <> ''
      ! Start Change 2621 BE(15/05/2003)
          sav:Prompt[1]   = GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[2]   = GETINI('FAULTCODE2','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[3]   = GETINI('FAULTCODE3','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[4]   = GETINI('FAULTCODE4','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[5]   = GETINI('FAULTCODE5','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[6]   = GETINI('FAULTCODE6','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[7]   = GETINI('FAULTCODE7','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[8]   = GETINI('FAULTCODE8','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[9]   = GETINI('FAULTCODE9','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[10]   = GETINI('FAULTCODE10','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[11]   = GETINI('FAULTCODE11','Name',,CLIP(PATH())&'\NFAULTS.INI')
          sav:Prompt[12]   = GETINI('FAULTCODE12','Name',,CLIP(PATH())&'\NFAULTS.INI')
      End !If job:Manufacturer ='NOKIA' and glo:FaultCode9 <> '0' And glo:FaultCode9 <> '1' And glo:FaultCode9 <> ''
  OPEN(Window)
  SELF.Opened=True
  Do Fault_Coding
  Do RecolourWindow
  ?glo:FaultCode1{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode2{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode3{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode4{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode5{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode6{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode7{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode8{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode9{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode10{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode11{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode12{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode1{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode2{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode3{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode4{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode5{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode6{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode7{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode8{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode9{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode10{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode11{Prop:Alrt,255} = MouseLeft2
  ?glo:TradeFaultCode12{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode13{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode14{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode15{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode16{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode17{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode18{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode19{Prop:Alrt,255} = MouseLeft2
  ?glo:FaultCode20{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:REPTYDEF.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'GenericFaultCodes',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  Globalrequest = Request
  Browse_Manufacturer_Fault_Lookup
  ReturnValue   = Globalresponse
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?tralookup:1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:1, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:1, Accepted)
    OF ?tralookup:4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:4, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 4
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:4, Accepted)
    OF ?tralookup:5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:5, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 5
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:5, Accepted)
    OF ?tralookup:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:2, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:2, Accepted)
    OF ?tralookup:3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:3, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 3
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:3, Accepted)
    OF ?tralookup:7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:7, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 7
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:7, Accepted)
    OF ?tralookup:8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:8, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 8
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:8, Accepted)
    OF ?tralookup:9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:9, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 9
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:9, Accepted)
    OF ?tralookup:11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:11, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 11
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:11, Accepted)
    OF ?tralookup:6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:6, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 6
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:6, Accepted)
    OF ?tralookup:10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:10, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 10
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:10, Accepted)
    OF ?tralookup:12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:12, Accepted)
      glo:Select1 = tra:Account_Number
      glo:Select2 = 12
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:12, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?glo:FaultCode1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode1, Accepted)
      If glo:FaultCode1 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 1
          if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 1
              mfo:field        = glo:FaultCode1
              if access:manfaulo.tryfetch(mfo:field_key)
                  If maf:force_lookup = 'YES'
                      Post(event:accepted,?Lookup)
                  End!If maf:force_lookup = 'YES'
              Else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
                  If Maf:ReplicateFault = 'YES'
                      If jbn:fault_description = ''
                          jbn:fault_description = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_Text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_Text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
      
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
      End!If glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode1, Accepted)
    OF ?Lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
      If ?lookup{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 1
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
              glo:FaultCode1 = mfo:field
      
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      If Maf:ReplicateFault = 'YES'
                          If jbn:fault_description = ''
                              jbn:fault_description = Clip(mfo:description)
                          Else!If jbn:fault_description = ''
                              jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                          End!If jbn:fault_description = ''
                      End!If Mfo:ReplicateFault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
              End !If j:Job
              display()
          Else
              glo:FaultCode1 = ''
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode1 = TINCALENDARStyle1(glo:FaultCode1)
          Display(?glo:FaultCode1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?glo:FaultCode2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode2, Accepted)
      if glo:FaultCode2 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 2
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 2
              mfo:field        = glo:FaultCode2
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:2)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode2 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode2, Accepted)
    OF ?Lookup:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
      If ?lookup:2{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 2
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
              glo:FaultCode2 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      
              End !If f:Job
              display()
          Else
              glo:FaultCode2 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:2{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode2 = TINCALENDARStyle1(glo:FaultCode2)
          Display(?glo:FaultCode2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?glo:FaultCode3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode3, Accepted)
      if glo:FaultCode3 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 3
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 3
              mfo:field        = glo:FaultCode3
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:3)
                  end!if maf:force_lookup = 'YES'
              else
                   access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode3 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode3, Accepted)
    OF ?Lookup:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
      If ?lookup:3{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 3
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode3 = mfo:field
      
              If f:job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      
              End !If f:job
              display()
          Else
              glo:FaultCode3 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:3{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode3 = TINCALENDARStyle1(glo:FaultCode3)
          Display(?glo:FaultCode3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?glo:FaultCode4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode4, Accepted)
      if glo:FaultCode4 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 4
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 4
              mfo:field        = glo:FaultCode4
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:4)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode4 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode4, Accepted)
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode4 = TINCALENDARStyle1(glo:FaultCode4)
          Display(?glo:FaultCode4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
      If ?lookup:4{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 4
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode4 = mfo:field
      
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      
              End !If f:Job
              display()
          Else
              glo:FaultCode4 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:4{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
    OF ?glo:FaultCode5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode5, Accepted)
      if glo:FaultCode5 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 5
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 5
              mfo:field        = glo:FaultCode5
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:5)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode5 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode5, Accepted)
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode5 = TINCALENDARStyle1(glo:FaultCode5)
          Display(?glo:FaultCode5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
      If ?lookup:5{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 5
          glo:select3  = ''
          If Self.Run(1,Selectrecord) = RequestCompleted
              glo:FaultCode5 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
              End !If f:Job
              
              display()
          Else
              glo:FaultCode5 = ''
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:5{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
    OF ?glo:FaultCode6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode6, Accepted)
      if glo:FaultCode6 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 6
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 6
              mfo:field        = glo:FaultCode6
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:6)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode6 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode6, Accepted)
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode6 = TINCALENDARStyle1(glo:FaultCode6)
          Display(?glo:FaultCode6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
      If ?lookup:6{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 6
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode6 = mfo:field
      
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode6 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:6{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
    OF ?glo:FaultCode7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode7, Accepted)
      if glo:FaultCode7 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 7
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 7
              mfo:field        = glo:FaultCode7
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:7)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode7 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode7, Accepted)
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode7 = TINCALENDARStyle1(glo:FaultCode7)
          Display(?glo:FaultCode7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
      If ?lookup:7{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 7
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
              glo:FaultCode7 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode7 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:7{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
    OF ?glo:FaultCode8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode8, Accepted)
      if glo:FaultCode8 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 8
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 8
              mfo:field        = glo:FaultCode8
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:8)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode8 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode8, Accepted)
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode8 = TINCALENDARStyle1(glo:FaultCode8)
          Display(?glo:FaultCode8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
      If ?lookup:8{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 8
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode8 = mfo:field
      
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode8  = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:8{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
    OF ?glo:FaultCode9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode9, Accepted)
      if glo:FaultCode9 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 9
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 9
              mfo:field        = glo:FaultCode9
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:9)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode9 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode9, Accepted)
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode9 = TINCALENDARStyle1(glo:FaultCode9)
          Display(?glo:FaultCode9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
      If ?lookup:9{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 9
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode9 = mfo:field
      
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode9  = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:9{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
    OF ?glo:FaultCode10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode10, Accepted)
      if glo:FaultCode10 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 10
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 10
              mfo:field        = glo:FaultCode10
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:10)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode10 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode10, Accepted)
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode10 = TINCALENDARStyle1(glo:FaultCode10)
          Display(?glo:FaultCode10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
      If ?lookup:10{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 10
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode10 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode10 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:10{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
    OF ?glo:FaultCode11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode11, Accepted)
      if glo:FaultCode11 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 11
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 11
              mfo:field        = glo:FaultCode11
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:11)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode11 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode11, Accepted)
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode11 = TINCALENDARStyle1(glo:FaultCode11)
          Display(?glo:FaultCode11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
      If ?lookup:11{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 11
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode11 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode11 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:11{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
    OF ?glo:FaultCode12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode12, Accepted)
      if glo:FaultCode12 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = f:Man
          maf:field_number = 12
          if access:manfault.tryfetch(maf:field_number_key) = level:benign
              access:manfaulo.clearkey(mfo:field_key)
              mfo:manufacturer = f:Man
              mfo:field_number = 12
              mfo:field        = glo:FaultCode12
              if access:manfaulo.tryfetch(mfo:field_key)
                  if maf:force_lookup = 'YES'
                      post(event:accepted,?lookup:12)
                  end!if maf:force_lookup = 'YES'
              else
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = f:Job
                  If access:jobnotes.tryfetch(jbn:RefNumberKey)
                      access:jobnotes.primerecord()
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryinsert()
                      access:jobnotes.clearkey(jbn:RefNUmberKey)
                      jbn:RefNumber   = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
                  End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                  if maf:replicatefault = 'YES'
                      if jbn:fault_description = ''
                          jbn:fault_description = clip(mfo:description)
                      else!if jbn:fault_description = ''
                          jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                      end!if jbn:fault_description = ''
                  end!if mfo:replicatefault = 'YES'
                  If Maf:ReplicateInvoice = 'YES'
                      If jbn:invoice_text = ''
                          jbn:invoice_text = Clip(mfo:description)
                      Else!If jbn:fault_description = ''
                          jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                      End!If jbn:fault_description = ''
                  End!If Mfo:ReplicateFault = 'YES'
                  access:jobnotes.update()
              end!if access:manfaulo.tryfetch(mfo:field_key)
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode12 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode12, Accepted)
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode12 = TINCALENDARStyle1(glo:FaultCode12)
          Display(?glo:FaultCode12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
      If ?lookup:12{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 12
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode12 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode12 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:12{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
    OF ?PopCalendar:25
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode13 = TINCALENDARStyle1(glo:FaultCode13)
          Display(?glo:FaultCode13)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:13
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:13, Accepted)
      If ?lookup:13{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 13
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode13 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode13 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:13{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:13, Accepted)
    OF ?PopCalendar:26
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode14 = TINCALENDARStyle1(glo:FaultCode14)
          Display(?glo:FaultCode14)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:14
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:14, Accepted)
      If ?lookup:14{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 14
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode14 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode14 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:14{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:14, Accepted)
    OF ?PopCalendar:27
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode15 = TINCALENDARStyle1(glo:FaultCode15)
          Display(?glo:FaultCode15)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:28
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode16 = TINCALENDARStyle1(glo:FaultCode16)
          Display(?glo:FaultCode16)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:16
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:16, Accepted)
      If ?lookup:16{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 16
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode16 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode16 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:16{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:16, Accepted)
    OF ?Lookup:15
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:15, Accepted)
      If ?lookup:15{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 15
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode15 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode15 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:15{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:15, Accepted)
    OF ?PopCalendar:29
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode17 = TINCALENDARStyle1(glo:FaultCode17)
          Display(?glo:FaultCode17)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:30
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode18 = TINCALENDARStyle1(glo:FaultCode18)
          Display(?glo:FaultCode18)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:18
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:18, Accepted)
      If ?lookup:18{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 18
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode18 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode18 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:18{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:18, Accepted)
    OF ?Lookup:17
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:17, Accepted)
      If ?lookup:17{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 17
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode17 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode17 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:17{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:17, Accepted)
    OF ?PopCalendar:31
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode19 = TINCALENDARStyle1(glo:FaultCode19)
          Display(?glo:FaultCode19)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:19
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:19, Accepted)
      If ?lookup:19{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 19
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode19 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode19 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:19{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:19, Accepted)
    OF ?PopCalendar:32
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:FaultCode20 = TINCALENDARStyle1(glo:FaultCode20)
          Display(?glo:FaultCode20)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:20
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:20, Accepted)
      If ?lookup:20{prop:hide} = false
          glo:select1  = f:Man
          glo:select2  = 20
          glo:select3  = ''
          if Self.run(1,Selectrecord) = RequestCompleted
      
              glo:FaultCode20 = mfo:field
              If f:Job
                  access:manfault.clearkey(maf:field_number_key)
                  maf:manufacturer = f:Man
                  maf:field_number = mfo:field_number
                  if access:manfault.tryfetch(maf:field_number_key) = level:benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                      jbn:RefNumber = f:Job
                      access:jobnotes.tryfetch(jbn:RefNumberKey)
      
                      if maf:replicatefault = 'YES'
                          if jbn:fault_description = ''
                              jbn:fault_description = clip(mfo:description)
                          else!if jbn:fault_description = ''
                              jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(mfo:description)
                          end!if jbn:fault_description = ''
                      end!if mfo:replicatefault = 'YES'
                          If Maf:ReplicateInvoice = 'YES'
                              If jbn:invoice_text = ''
                                  jbn:invoice_text = Clip(mfo:description)
                              Else!If jbn:fault_description = ''
                                  jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                              End!If jbn:fault_description = ''
                          End!If Mfo:ReplicateFault = 'YES'
                      access:jobnotes.update()
                  end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
                  display()
      
              End !If f:Job
          Else
              glo:FaultCode20 = ''
      
          end
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
          globalrequest     = saverequest#
      End!If ?lookup:20{prop:hide} = false
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:20, Accepted)
    OF ?glo:TradeFaultCode1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode1, Accepted)
      if glo:TradeFaultCode1 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 1
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 1
              tfo:Field         = glo:TradeFaultCode1
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 1      |
                  Or tfo:Field         <> glo:TradeFaultCode1
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:1)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  !Message(taf:ReplicateFault,'Before Routine')
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode1, Accepted)
    OF ?PopCalendar:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode1 = TINCALENDARStyle1(glo:TradeFaultCode1)
          Display(?glo:TradeFaultCode1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:1
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:1, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode1 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 1
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 1
              tfo:Field         = glo:TradeFaultCode1
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 1      |
                  Or tfo:Field         <> glo:TradeFaultCode1
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:1)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode1 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode1)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:1, Accepted)
    OF ?glo:TradeFaultCode2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode2, Accepted)
      if glo:TradeFaultCode2 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 2
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 2
              tfo:Field         = glo:TradeFaultCode2
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 2      |
                  Or tfo:Field         <> glo:TradeFaultCode2
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:2)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode2, Accepted)
    OF ?PopCalendar:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode2 = TINCALENDARStyle1(glo:TradeFaultCode2)
          Display(?glo:TradeFaultCode2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?glo:TradeFaultCode3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode3, Accepted)
      if glo:TradeFaultCode3 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 3
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 3
              tfo:Field         = glo:TradeFaultCode3
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 3      |
                  Or tfo:Field         <> glo:TradeFaultCode3
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:3)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode3, Accepted)
    OF ?PopCalendar:15
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode3 = TINCALENDARStyle1(glo:TradeFaultCode3)
          Display(?glo:TradeFaultCode3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?glo:TradeFaultCode4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode4, Accepted)
      if glo:TradeFaultCode4 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 4
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 4
              tfo:Field         = glo:TradeFaultCode4
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 4      |
                  Or tfo:Field         <> glo:TradeFaultCode4
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:4)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode4, Accepted)
    OF ?PopCalendar:16
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode4 = TINCALENDARStyle1(glo:TradeFaultCode4)
          Display(?glo:TradeFaultCode4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:4
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:4, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode4 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 4
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 4
              tfo:Field         = glo:TradeFaultCode4
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 4      |
                  Or tfo:Field         <> glo:TradeFaultCode4
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:4)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode4 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode4)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:4, Accepted)
    OF ?glo:TradeFaultCode5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode5, Accepted)
      if glo:TradeFaultCode5 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 5
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 5
              tfo:Field         = glo:TradeFaultCode5
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 5      |
                  Or tfo:Field         <> glo:TradeFaultCode5
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:5)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode5, Accepted)
    OF ?PopCalendar:17
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode5 = TINCALENDARStyle1(glo:TradeFaultCode5)
          Display(?glo:TradeFaultCode5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:5
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:5, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode5 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 5
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 5
              tfo:Field         = glo:TradeFaultCode5
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 5      |
                  Or tfo:Field         <> glo:TradeFaultCode5
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:5)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode5 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode5)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:5, Accepted)
    OF ?glo:TradeFaultCode6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode6, Accepted)
      if glo:TradeFaultCode6 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 6
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 6
              tfo:Field         = glo:TradeFaultCode6
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 6      |
                  Or tfo:Field         <> glo:TradeFaultCode6
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:6)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode6, Accepted)
    OF ?PopCalendar:18
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode6 = TINCALENDARStyle1(glo:TradeFaultCode6)
          Display(?glo:TradeFaultCode6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:2
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:2, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode2 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 2
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 2
              tfo:Field         = glo:TradeFaultCode2
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 2      |
                  Or tfo:Field         <> glo:TradeFaultCode2
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:2)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode2 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode2)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:2, Accepted)
    OF ?tralookup:3
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:3, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode3 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 3
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 3
              tfo:Field         = glo:TradeFaultCode3
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 3      |
                  Or tfo:Field         <> glo:TradeFaultCode3
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:3)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode3 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode3)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:3, Accepted)
    OF ?glo:TradeFaultCode7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode7, Accepted)
      if glo:TradeFaultCode7 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 7
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 7
              tfo:Field         = glo:TradeFaultCode7
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 7      |
                  Or tfo:Field         <> glo:TradeFaultCode7
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:7)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode7, Accepted)
    OF ?PopCalendar:19
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode7 = TINCALENDARStyle1(glo:TradeFaultCode7)
          Display(?glo:TradeFaultCode7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:7
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:7, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode7 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 7
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 7
              tfo:Field         = glo:TradeFaultCode7
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 7      |
                  Or tfo:Field         <> glo:TradeFaultCode7
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:7)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode7 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode7)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:7, Accepted)
    OF ?glo:TradeFaultCode8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode8, Accepted)
      if glo:TradeFaultCode8 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 8
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 8
              tfo:Field         = glo:TradeFaultCode8
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 8      |
                  Or tfo:Field         <> glo:TradeFaultCode8
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:8)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode8, Accepted)
    OF ?PopCalendar:20
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode8 = TINCALENDARStyle1(glo:TradeFaultCode8)
          Display(?glo:TradeFaultCode8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:8
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:8, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode8 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 8
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 8
              tfo:Field         = glo:TradeFaultCode8
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 8      |
                  Or tfo:Field         <> glo:TradeFaultCode8
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:8)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode8 = ''
      !        glo:TradeFaultCode9 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode8)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:8, Accepted)
    OF ?glo:TradeFaultCode9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode9, Accepted)
      if glo:TradeFaultCode9 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 9
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 9
              tfo:Field         = glo:TradeFaultCode9
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 9      |
                  Or tfo:Field         <> glo:TradeFaultCode9
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:9)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode9, Accepted)
    OF ?PopCalendar:21
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode9 = TINCALENDARStyle1(glo:TradeFaultCode9)
          Display(?glo:TradeFaultCode9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?glo:TradeFaultCode10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode10, Accepted)
      if glo:TradeFaultCode10 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 10
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 10
              tfo:Field         = glo:TradeFaultCode10
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 10      |
                  Or tfo:Field         <> glo:TradeFaultCode10
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:10)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode10, Accepted)
    OF ?PopCalendar:22
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode10 = TINCALENDARStyle1(glo:TradeFaultCode10)
          Display(?glo:TradeFaultCode10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?glo:TradeFaultCode11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode11, Accepted)
      if glo:TradeFaultCode11 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 11
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 11
              tfo:Field         = glo:TradeFaultCode11
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 11      |
                  Or tfo:Field         <> glo:TradeFaultCode11
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:11)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode11, Accepted)
    OF ?PopCalendar:23
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode11 = TINCALENDARStyle1(glo:TradeFaultCode11)
          Display(?glo:TradeFaultCode11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?glo:TradeFaultCode12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode12, Accepted)
      if glo:TradeFaultCode12 <> ''
          access:trafault.clearkey(taf:field_number_key)
          taf:accountnumber = f:Acc
          taf:field_number = 12
          if access:trafault.tryfetch(taf:field_number_key) = level:benign
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 12
              tfo:Field         = glo:TradeFaultCode12
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 12      |
                  Or tfo:Field         <> glo:TradeFaultCode12
                  if taf:force_lookup = 'YES'
                      post(event:accepted,?tralookup:12)
                  end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
      
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
          end!if access:manfault.tryfetch(maf:field_number_key) = level:benign
      end!if glo:FaultCode1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:TradeFaultCode12, Accepted)
    OF ?PopCalendar:24
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          glo:TradeFaultCode12 = TINCALENDARStyle1(glo:TradeFaultCode12)
          Display(?glo:TradeFaultCode12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:9
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:9, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode9 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 9
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 9
              tfo:Field         = glo:TradeFaultCode9
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 9      |
                  Or tfo:Field         <> glo:TradeFaultCode9
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:9)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode9 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode9)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:9, Accepted)
    OF ?tralookup:11
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:11, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode11 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 11
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 11
              tfo:Field         = glo:TradeFaultCode11
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 11      |
                  Or tfo:Field         <> glo:TradeFaultCode11
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:1)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode11 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode11)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:11, Accepted)
    OF ?tralookup:6
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:6, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode6 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 6
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 6
              tfo:Field         = glo:TradeFaultCode6
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 6      |
                  Or tfo:Field         <> glo:TradeFaultCode6
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:6)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode6 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode6)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:6, Accepted)
    OF ?tralookup:10
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:10, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode10 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 10
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 10
              tfo:Field         = glo:TradeFaultCode10
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 10      |
                  Or tfo:Field         <> glo:TradeFaultCode10
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:10)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode10 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode10)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:10, Accepted)
    OF ?tralookup:12
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTradeFaultCodes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:12, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              glo:TradeFaultCode12 = tfo:Field
      
              Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
              taf:AccountNumber = f:Acc
              taf:Field_Number  = 12
              Access:TRAFAULT.TryFetch(taf:Field_Number_Key)
      
              Access:TRAFAULO.ClearKey(tfo:Field_Key)
              tfo:AccountNumber = f:Acc
              tfo:Field_Number  = 12
              tfo:Field         = glo:TradeFaultCode12
              Set(tfo:Field_Key,tfo:Field_Key)
              Access:TRAFAULO.NEXT()
              If tfo:AccountNumber <> f:Acc      |
                  Or tfo:Field_Number  <> 12      |
                  Or tfo:Field         <> glo:TradeFaultCode12
      !            if taf:force_lookup = 'YES'
      !                post(event:accepted,?tralookup:12)
      !            end!if maf:force_lookup = 'YES'
              Else !If tfo:AccountNumber <> f:Acc      |
                  Do PutTradeNotes
              End !If tfo:AccountNumber <> f:Acc      |
      
      
              Select(?+2)
          Of Requestcancelled
              glo:TradeFaultCode12 = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?glo:TradeFaultCode12)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:12, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      continue# = 1
      If f:comp <> ''
          continue# = 0
          If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 1 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode2{prop:hide} = 0 and glo:FaultCode2 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 2 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode3{prop:hide} = 0 and glo:FaultCode3 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 3 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_ode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode4{prop:hide} = 0 and glo:FaultCode4 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 4 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode5{prop:hide} = 0 and glo:FaultCode5 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 5 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode6{prop:hide} = 0 and glo:FaultCode6 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 6 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode7{prop:hide} = 0 and glo:FaultCode7 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 7 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_ode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode8{prop:hide} = 0 and glo:FaultCode8 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 8 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode9{prop:hide} = 0 and glo:FaultCode9 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 9 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode10{prop:hide} = 0 and glo:FaultCode10 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 10 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode11{prop:hide} = 0 and glo:FaultCode11 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 11 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If ?job:fault_ode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode12{prop:hide} = 0 and glo:FaultCode12 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 12 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode13{prop:hide} = 0 and glo:FaultCode13 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 13 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode14{prop:hide} = 0 and glo:FaultCode14 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 14 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode15{prop:hide} = 0 and glo:FaultCode15 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 15 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode16{prop:hide} = 0 and glo:FaultCode16 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 16 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode17{prop:hide} = 0 and glo:FaultCode17 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 17 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode18{prop:hide} = 0 and glo:FaultCode18 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 18 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode19{prop:hide} = 0 and glo:FaultCode19 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 19 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode20{prop:hide} = 0 and glo:FaultCode20 = '' and error# = 0
              error# = 1
              Case MessageEx('Warning! This job has been completed and fault code 20 is blank. <13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      continue# = 1
                  Of 2 ! &No Button
              End!Case MessageEx
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If error# = 0
              continue# = 1
          End!If error# = 0
      End!If job:date_completed <> ''
      If continue# = 1
          Post(event:closewindow)
      End
       
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'GenericFaultCodes')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?glo:FaultCode1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?glo:FaultCode2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?glo:FaultCode3
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?glo:FaultCode4
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?glo:FaultCode5
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?glo:FaultCode6
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  OF ?glo:FaultCode7
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?glo:FaultCode8
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?glo:FaultCode9
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?glo:FaultCode10
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?glo:FaultCode11
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?glo:FaultCode12
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  OF ?glo:FaultCode13
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:25)
      CYCLE
    END
  OF ?glo:FaultCode14
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:26)
      CYCLE
    END
  OF ?glo:FaultCode15
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:27)
      CYCLE
    END
  OF ?glo:FaultCode16
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:28)
      CYCLE
    END
  OF ?glo:FaultCode17
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:29)
      CYCLE
    END
  OF ?glo:FaultCode18
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:30)
      CYCLE
    END
  OF ?glo:FaultCode19
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:31)
      CYCLE
    END
  OF ?glo:FaultCode20
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:32)
      CYCLE
    END
  OF ?glo:TradeFaultCode1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:13)
      CYCLE
    END
  OF ?glo:TradeFaultCode2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:14)
      CYCLE
    END
  OF ?glo:TradeFaultCode3
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:15)
      CYCLE
    END
  OF ?glo:TradeFaultCode4
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:16)
      CYCLE
    END
  OF ?glo:TradeFaultCode5
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:17)
      CYCLE
    END
  OF ?glo:TradeFaultCode6
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:18)
      CYCLE
    END
  OF ?glo:TradeFaultCode7
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:19)
      CYCLE
    END
  OF ?glo:TradeFaultCode8
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:20)
      CYCLE
    END
  OF ?glo:TradeFaultCode9
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:21)
      CYCLE
    END
  OF ?glo:TradeFaultCode10
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:22)
      CYCLE
    END
  OF ?glo:TradeFaultCode11
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:23)
      CYCLE
    END
  OF ?glo:TradeFaultCode12
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:24)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?glo:FaultCode1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode1, AlertKey)
      If ?lookup{prop:hide} = 0
          Post(Event:Accepted,?Lookup)
      End!If ?lookup{prop:hide} = 0
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode1, AlertKey)
    END
  OF ?glo:FaultCode2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode2, AlertKey)
      If ?Lookup:2{prop:hide} = 0
          Post(Event:Accepted,?Lookup:2)
      End
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode2, AlertKey)
    END
  OF ?glo:FaultCode3
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode3, AlertKey)
      If ?Lookup:3{prop:hide} = 0
          Post(Event:Accepted,?Lookup:3)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode3, AlertKey)
    END
  OF ?glo:FaultCode4
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode4, AlertKey)
      If ?Lookup:4{prop:hide} = 0
          Post(Event:Accepted,?Lookup:4)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode4, AlertKey)
    END
  OF ?glo:FaultCode5
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode5, AlertKey)
      If ?Lookup:5{prop:hide} = 0
          Post(Event:Accepted,?Lookup:5)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode5, AlertKey)
    END
  OF ?glo:FaultCode6
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode6, AlertKey)
      If ?Lookup:6{prop:hide} = 0
          Post(Event:Accepted,?Lookup:6)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode6, AlertKey)
    END
  OF ?glo:FaultCode7
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode7, AlertKey)
      If ?Lookup:7{prop:hide} = 0
          Post(Event:Accepted,?Lookup:7)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode7, AlertKey)
    END
  OF ?glo:FaultCode8
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode8, AlertKey)
      If ?Lookup:8{prop:hide} = 0
          Post(Event:Accepted,?Lookup:8)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode8, AlertKey)
    END
  OF ?glo:FaultCode9
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode9, AlertKey)
      If ?Lookup:9{prop:hide} = 0
          Post(Event:Accepted,?Lookup:9)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode9, AlertKey)
    END
  OF ?glo:FaultCode10
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode10, AlertKey)
      If ?Lookup:10{prop:hide} = 0
          Post(Event:Accepted,?Lookup:10)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode10, AlertKey)
    END
  OF ?glo:FaultCode11
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode11, AlertKey)
      If ?Lookup:11{prop:hide} = 0
          Post(Event:Accepted,?Lookup:11)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode11, AlertKey)
    END
  OF ?glo:FaultCode12
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode12, AlertKey)
      If ?Lookup:12{prop:hide} = 0
          Post(Event:Accepted,?Lookup:12)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?glo:FaultCode12, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = f:Acc
      access:subtracc.tryfetch(sub:account_number_key)
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.tryfetch(tra:account_number_key)
      access:jobse.clearkey(jobe:refnumberkey)
      jobe:refnumber  = f:Job
      If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
          !Found
      Else! If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
          !Error
          If access:jobse.primerecord() = Level:Benign
              jobe:refnumber  = f:Job
              If access:jobse.tryinsert()
                  access:jobse.cancelautoinc()
              End!If access:jobse.tryinsert()
          End!If access:jobse.primerecord() = Level:Benign
      End! If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
      
      
      !Do Fault_Coding
      Select(?+1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

