

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01014.INC'),ONCE        !Local module procedure declarations
                     END


Internal_Mail PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
Sender_hold          STRING(40)
Recipient_Hold       STRING(20)
Email_Hold           STRING(20)
User_Name_Temp       STRING(60)
user_code_temp       STRING(3)
message_from_temp    STRING(40)
all_users_temp       STRING('NO {1}')
message_to_temp      STRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(MESSAGES)
                       PROJECT(mes:Date)
                       PROJECT(mes:Time)
                       PROJECT(mes:Comment)
                       PROJECT(mes:Message_Memo)
                       PROJECT(mes:Record_Number)
                       PROJECT(mes:Read)
                       PROJECT(mes:Message_For)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mes:Date               LIKE(mes:Date)                 !List box control field - type derived from field
mes:Time               LIKE(mes:Time)                 !List box control field - type derived from field
message_to_temp        LIKE(message_to_temp)          !List box control field - type derived from local data
message_from_temp      LIKE(message_from_temp)        !List box control field - type derived from local data
mes:Comment            LIKE(mes:Comment)              !List box control field - type derived from field
mes:Message_Memo       LIKE(mes:Message_Memo)         !Browse hot field - type derived from field
mes:Record_Number      LIKE(mes:Record_Number)        !Primary key field - type derived from field
mes:Read               LIKE(mes:Read)                 !Browse key field - type derived from field
mes:Message_For        LIKE(mes:Message_For)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Internal Mail'),AT(,,507,228),FONT('Tahoma',8,,,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('Internal_Mail'),SYSTEM,GRAY,RESIZE
                       BUTTON,AT(40,20,10,10),USE(?Lookup_Users),FLAT,ICON('list3.ico')
                       ENTRY(@s3),AT(8,20,28,10),USE(user_code_temp),FONT('Tahoma',8,,FONT:bold),UPR
                       ENTRY(@s60),AT(56,20,124,10),USE(User_Name_Temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                       CHECK('All Users'),AT(196,20),USE(all_users_temp),FONT(,,COLOR:White,,CHARSET:ANSI),VALUE('YES','NO')
                       LIST,AT(8,36,404,108),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('47R(2)|M~Date~L(0)@d6@27R(2)|M~Time~L(0)@t1@113L(2)|M~Message To~@s40@111L(2)|M~' &|
   'Message From~@s40@320L(2)|M~Comment~@s80@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(424,132,76,20),USE(?Insert:3),LEFT,ICON('insert.ico')
                       PANEL,AT(4,152,412,72),USE(?Panel1),FILL(COLOR:Silver)
                       TEXT,AT(8,156,404,64),USE(mes:Message_Memo),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),READONLY
                       BUTTON('&Change'),AT(424,156,76,20),USE(?Change:3),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(424,180,76,20),USE(?Delete:3),LEFT,ICON('delete.ico')
                       SHEET,AT(4,4,412,144),USE(?CurrentTab),SPREAD
                         TAB('New Messages'),USE(?Tab:1)
                         END
                         TAB('All Messages'),USE(?Tab:3)
                         END
                       END
                       BUTTON('Close'),AT(424,204,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort2:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'NO'
BRW1::Sort3:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'YES'
BRW1::Sort4:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(all_users_temp) = 'YES'
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW1::Sort2:StepClass CLASS(StepRealClass)            !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'NO'
Init                   PROCEDURE(BYTE Controls)
                     END

BRW1::Sort3:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'YES'
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(all_users_temp) = 'NO'
BRW1::Sort4:StepClass StepRealClass                   !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(all_users_temp) = 'YES'
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    If ?user_code_temp{prop:ReadOnly} = True
        ?user_code_temp{prop:FontColor} = 65793
        ?user_code_temp{prop:Color} = 15066597
    Elsif ?user_code_temp{prop:Req} = True
        ?user_code_temp{prop:FontColor} = 65793
        ?user_code_temp{prop:Color} = 8454143
    Else ! If ?user_code_temp{prop:Req} = True
        ?user_code_temp{prop:FontColor} = 65793
        ?user_code_temp{prop:Color} = 16777215
    End ! If ?user_code_temp{prop:Req} = True
    ?user_code_temp{prop:Trn} = 0
    ?user_code_temp{prop:FontStyle} = font:Bold
    If ?User_Name_Temp{prop:ReadOnly} = True
        ?User_Name_Temp{prop:FontColor} = 65793
        ?User_Name_Temp{prop:Color} = 15066597
    Elsif ?User_Name_Temp{prop:Req} = True
        ?User_Name_Temp{prop:FontColor} = 65793
        ?User_Name_Temp{prop:Color} = 8454143
    Else ! If ?User_Name_Temp{prop:Req} = True
        ?User_Name_Temp{prop:FontColor} = 65793
        ?User_Name_Temp{prop:Color} = 16777215
    End ! If ?User_Name_Temp{prop:Req} = True
    ?User_Name_Temp{prop:Trn} = 0
    ?User_Name_Temp{prop:FontStyle} = font:Bold
    ?all_users_temp{prop:Font,3} = -1
    ?all_users_temp{prop:Color} = 15066597
    ?all_users_temp{prop:Trn} = 0
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    If ?mes:Message_Memo{prop:ReadOnly} = True
        ?mes:Message_Memo{prop:FontColor} = 65793
        ?mes:Message_Memo{prop:Color} = 15066597
    Elsif ?mes:Message_Memo{prop:Req} = True
        ?mes:Message_Memo{prop:FontColor} = 65793
        ?mes:Message_Memo{prop:Color} = 8454143
    Else ! If ?mes:Message_Memo{prop:Req} = True
        ?mes:Message_Memo{prop:FontColor} = 65793
        ?mes:Message_Memo{prop:Color} = 16777215
    End ! If ?mes:Message_Memo{prop:Req} = True
    ?mes:Message_Memo{prop:Trn} = 0
    ?mes:Message_Memo{prop:FontStyle} = font:Bold
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?Tab:3{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Internal_Mail',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Internal_Mail',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Internal_Mail',1)
    SolaceViewVars('Sender_hold',Sender_hold,'Internal_Mail',1)
    SolaceViewVars('Recipient_Hold',Recipient_Hold,'Internal_Mail',1)
    SolaceViewVars('Email_Hold',Email_Hold,'Internal_Mail',1)
    SolaceViewVars('User_Name_Temp',User_Name_Temp,'Internal_Mail',1)
    SolaceViewVars('user_code_temp',user_code_temp,'Internal_Mail',1)
    SolaceViewVars('message_from_temp',message_from_temp,'Internal_Mail',1)
    SolaceViewVars('all_users_temp',all_users_temp,'Internal_Mail',1)
    SolaceViewVars('message_to_temp',message_to_temp,'Internal_Mail',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Lookup_Users;  SolaceCtrlName = '?Lookup_Users';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?user_code_temp;  SolaceCtrlName = '?user_code_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?User_Name_Temp;  SolaceCtrlName = '?User_Name_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?all_users_temp;  SolaceCtrlName = '?all_users_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mes:Message_Memo;  SolaceCtrlName = '?mes:Message_Memo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Internal_Mail')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Internal_Mail')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Lookup_Users
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MESSAGES.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MESSAGES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  access:users.clearkey(use:password_key)
  use:password =glo:password
  if access:users.fetch(use:password_key) = Level:Benign
     user_code_temp = use:user_code
     user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  end
  Display()
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,mes:Read_Key)
  BRW1.AddRange(mes:Message_For,user_code_temp)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(,mes:Date,1,BRW1)
  BRW1::Sort3:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Alpha)
  BRW1.AddSortOrder(BRW1::Sort3:StepClass,mes:Read_Only_Key)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(,mes:Read,1,BRW1)
  BRW1.SetFilter('(Upper(mes:read) = ''NO'')')
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Alpha)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,mes:Who_To_Key)
  BRW1.AddRange(mes:Message_For,user_code_temp)
  BRW1::Sort4:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort4:StepClass,mes:Date_Only_Key)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(,mes:Date,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,mes:Read_Key)
  BRW1.AddRange(mes:Message_For,user_code_temp)
  BIND('message_to_temp',message_to_temp)
  BIND('message_from_temp',message_from_temp)
  BRW1.AddField(mes:Date,BRW1.Q.mes:Date)
  BRW1.AddField(mes:Time,BRW1.Q.mes:Time)
  BRW1.AddField(message_to_temp,BRW1.Q.message_to_temp)
  BRW1.AddField(message_from_temp,BRW1.Q.message_from_temp)
  BRW1.AddField(mes:Comment,BRW1.Q.mes:Comment)
  BRW1.AddField(mes:Message_Memo,BRW1.Q.mes:Message_Memo)
  BRW1.AddField(mes:Record_Number,BRW1.Q.mes:Record_Number)
  BRW1.AddField(mes:Read,BRW1.Q.mes:Read)
  BRW1.AddField(mes:Message_For,BRW1.Q.mes:Message_For)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?all_users_temp{Prop:Checked} = True
    DISABLE(?Lookup_Users)
    DISABLE(?user_code_temp)
    DISABLE(?User_Name_Temp)
  END
  IF ?all_users_temp{Prop:Checked} = False
    ENABLE(?user_code_temp)
    ENABLE(?Lookup_Users)
    ENABLE(?User_Name_Temp)
  END
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MESSAGES.Close
    Relate:USERS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Internal_Mail',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of insertrecord
          check_access('INTERNAL EMAIL - INSERT',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
          end
      of changerecord
          check_access('INTERNAL EMAIL - CHANGE',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
          end
      of deleterecord
          check_access('INTERNAL EMAIL - DELETE',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
          end
  end !case request
  
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMessages
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Lookup_Users
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Users
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Users, Accepted)
      case globalresponse
          of requestcompleted
              user_code_temp = use:user_code
              user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
              select(?+2)
          of requestcancelled
              user_code_temp = ''
              user_name_temp = ''
              select(?-1)
      end!case globalreponse
      Display()
      Select(?Browse:1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Users, Accepted)
    OF ?user_code_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?user_code_temp, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      browse_users
      if globalresponse = requestcompleted
          user_code_temp = use:user_code
          user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
          Display()
      end
      globalrequest     = saverequest#
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?user_code_temp, Accepted)
    OF ?all_users_temp
      IF ?all_users_temp{Prop:Checked} = True
        DISABLE(?Lookup_Users)
        DISABLE(?user_code_temp)
        DISABLE(?User_Name_Temp)
      END
      IF ?all_users_temp{Prop:Checked} = False
        ENABLE(?user_code_temp)
        ENABLE(?Lookup_Users)
        ENABLE(?User_Name_Temp)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?all_users_temp, Accepted)
      thiswindow.reset(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?all_users_temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Internal_Mail')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'NO'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'YES'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_users_temp) = 'NO'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_users_temp) = 'YES'
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  access:users.clearkey(use:user_code_key)
  use:user_code = mes:message_from
  if access:users.fetch(use:user_code_key) = Level:Benign
     message_from_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  end
  access:users.clearkey(use:user_code_key)
  use:user_code = mes:message_for
  if access:users.fetch(use:user_code_key) = Level:Benign
     message_to_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  end
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW1::Sort2:StepClass.Init PROCEDURE(BYTE Controls)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 2, Init, (BYTE Controls))
  mes:read = 'NO'
  PARENT.Init(Controls)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 2, Init, (BYTE Controls))


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

