

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01033.INC'),ONCE        !Local module procedure declarations
                     END


Engineer_Status_Screen PROCEDURE                      !Generated from procedure template - Window

FilesOpened          BYTE
save_job_id          USHORT,AUTO
team_temp            STRING(30)
repaired_temp        REAL
achieved_temp        REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?team_temp
tea:Team               LIKE(tea:Team)                 !List box control field - type derived from field
tea:Record_Number      LIKE(tea:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(TEAMS)
                       PROJECT(tea:Team)
                       PROJECT(tea:Record_Number)
                     END
BRW3::View:Browse    VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:Repair_Target)
                       PROJECT(use:User_Code)
                       PROJECT(use:Team)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
repaired_temp          LIKE(repaired_temp)            !List box control field - type derived from local data
use:Repair_Target      LIKE(use:Repair_Target)        !List box control field - type derived from field
achieved_temp          LIKE(achieved_temp)            !List box control field - type derived from local data
use:User_Code          LIKE(use:User_Code)            !Primary key field - type derived from field
use:Team               LIKE(use:Team)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Engineer Status'),AT(,,411,278),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,TILED,GRAY,DOUBLE
                       PROMPT(''),AT(232,8,168,12),USE(?Date),TRN,FONT('Tahoma',10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                       PROMPT('Team'),AT(8,8),USE(?Prompt1),TRN
                       BUTTON('Cancel'),AT(348,256,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,4,404,20),USE(?Panel1:2),FILL(COLOR:Silver)
                       COMBO(@s30),AT(84,8,124,10),USE(team_temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                       SHEET,AT(4,28,404,220),USE(?Sheet1),SPREAD
                         TAB('By Engineer'),USE(?Tab1)
                           LIST,AT(8,44,396,200),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Surname~@s30@120L(2)|M~Forename~@s30@39R(2)|M~Repaired~L@s8@32R(2)|M~T' &|
   'arget~L@s8@40R(2)|M~Achieved (%)~L@n10@'),FROM(Queue:Browse)
                         END
                       END
                       PANEL,AT(4,252,404,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Date{prop:FontColor} = -1
    ?Date{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Panel1:2{prop:Fill} = 15066597

    If ?team_temp{prop:ReadOnly} = True
        ?team_temp{prop:FontColor} = 65793
        ?team_temp{prop:Color} = 15066597
    Elsif ?team_temp{prop:Req} = True
        ?team_temp{prop:FontColor} = 65793
        ?team_temp{prop:Color} = 8454143
    Else ! If ?team_temp{prop:Req} = True
        ?team_temp{prop:FontColor} = 65793
        ?team_temp{prop:Color} = 16777215
    End ! If ?team_temp{prop:Req} = True
    ?team_temp{prop:Trn} = 0
    ?team_temp{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Engineer_Status_Screen',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Engineer_Status_Screen',1)
    SolaceViewVars('save_job_id',save_job_id,'Engineer_Status_Screen',1)
    SolaceViewVars('team_temp',team_temp,'Engineer_Status_Screen',1)
    SolaceViewVars('repaired_temp',repaired_temp,'Engineer_Status_Screen',1)
    SolaceViewVars('achieved_temp',achieved_temp,'Engineer_Status_Screen',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Date;  SolaceCtrlName = '?Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1:2;  SolaceCtrlName = '?Panel1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?team_temp;  SolaceCtrlName = '?team_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Engineer_Status_Screen')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Engineer_Status_Screen')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Date
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS.Open
  SELF.FilesOpened = True
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:USERS,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,use:Team_Surname)
  BRW3.AddRange(use:Team,team_temp)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,use:Surname,1,BRW3)
  BIND('repaired_temp',repaired_temp)
  BIND('achieved_temp',achieved_temp)
  BRW3.AddField(use:Surname,BRW3.Q.use:Surname)
  BRW3.AddField(use:Forename,BRW3.Q.use:Forename)
  BRW3.AddField(repaired_temp,BRW3.Q.repaired_temp)
  BRW3.AddField(use:Repair_Target,BRW3.Q.use:Repair_Target)
  BRW3.AddField(achieved_temp,BRW3.Q.achieved_temp)
  BRW3.AddField(use:User_Code,BRW3.Q.use:User_Code)
  BRW3.AddField(use:Team,BRW3.Q.use:Team)
  FDCB2.Init(team_temp,?team_temp,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:TEAMS,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(tea:Team_Key)
  FDCB2.AddField(tea:Team,FDCB2.Q.tea:Team)
  FDCB2.AddField(tea:Record_Number,FDCB2.Q.tea:Record_Number)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Engineer_Status_Screen',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?team_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?team_temp, Accepted)
      BRW3.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?team_temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Engineer_Status_Screen')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Case Today() % 7
          Of 0
              day" = 'Sunday'
          Of 1
              day" = 'Monday'
          Of 2
              day" = 'Tuesday'
          Of 3
              day" = 'Wednesday'
          Of 4
              day" = 'Thursday'
          Of 5
              day" = 'Friday'
          Of 6
              day"    = 'Saturday'
      End!Case Today() % 7
      ?date{prop:text} = Clip(day") & ', ' & Format(Today(),@d18)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(3, SetQueueRecord, ())
  achieved_temp = ''
  repaired_temp = ''
  setcursor(cursor:wait)
  save_job_id = access:jobs.savefile()
  access:jobs.clearkey(job:EngDateCompKey)
  job:engineer       = use:user_code
  job:date_completed = Today()
  set(job:EngDateCompKey,job:EngDateCompKey)
  loop
      if access:jobs.next()
         break
      end !if
      if job:engineer       <> use:user_code      |
      or job:date_completed <> Today()      |
          then break.  ! end if
      repaired_temp += 1
  end !loop
  access:jobs.restorefile(save_job_id)
  setcursor()
  achieved_temp = (repaired_temp / use:repair_target) * 100
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(3, SetQueueRecord, ())


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

