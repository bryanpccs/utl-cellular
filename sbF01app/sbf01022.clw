

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01022.INC'),ONCE        !Local module procedure declarations
                     END


Fault_Codes_Temp_Window PROCEDURE                     !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Fault Codes'),AT(,,242,248),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,236,212),USE(?Sheet1),SPREAD
                         TAB('Fault Codes'),USE(?Tab1)
                           PROMPT('Fault Code 1:'),AT(8,20),USE(?glo:select1:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,20,124,10),USE(GLO:Select13),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,20,64,10),USE(GLO:Select13,,?glo:select13:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,20,10,10),USE(?Lookup),HIDE,ICON('list3.ico')
                           BUTTON,AT(224,20,10,10),USE(?PopCalendar),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 2:'),AT(8,36),USE(?glo:select2:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,36,124,10),USE(GLO:Select14),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,36,64,10),USE(GLO:Select14,,?glo:select14:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,36,10,10),USE(?Lookup:2),HIDE,ICON('list3.ico')
                           BUTTON,AT(224,36,10,10),USE(?PopCalendar:2),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 3:'),AT(8,52),USE(?glo:select3:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,52,124,10),USE(GLO:Select15),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,52,64,10),USE(GLO:Select15,,?glo:select15:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,52,10,10),USE(?Lookup:3),HIDE,ICON('list3.ico')
                           BUTTON,AT(224,52,10,10),USE(?PopCalendar:3),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 4:'),AT(8,68),USE(?glo:select4:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,68,124,10),USE(GLO:Select16),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,68,64,10),USE(GLO:Select16,,?glo:select16:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(224,68,10,10),USE(?PopCalendar:4),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,68,10,10),USE(?Lookup:4),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 5:'),AT(8,84),USE(?glo:select5:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,84,124,10),USE(GLO:Select17),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,84,64,10),USE(GLO:Select17,,?glo:select17:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(224,84,10,10),USE(?PopCalendar:5),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,84,10,10),USE(?Lookup:5),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 6:'),AT(8,100),USE(?glo:select6:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,100,124,10),USE(GLO:Select18),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,100,64,10),USE(GLO:Select18,,?glo:select18:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(224,100,10,10),USE(?PopCalendar:6),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,100,10,10),USE(?Lookup:6),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 7:'),AT(8,116),USE(?glo:select7:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,116,124,10),USE(GLO:Select19),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,116,64,10),USE(GLO:Select19,,?glo:select19:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(224,116,10,10),USE(?PopCalendar:7),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,116,10,10),USE(?Lookup:7),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 8:'),AT(8,132),USE(?glo:select8:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,132,124,10),USE(GLO:Select20),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,132,64,10),USE(GLO:Select20,,?glo:select20:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(224,132,10,10),USE(?PopCalendar:8),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,132,10,10),USE(?Lookup:8),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 9:'),AT(8,148),USE(?glo:select9:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,148,124,10),USE(GLO:Select21),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,148,64,10),USE(GLO:Select21,,?glo:select21:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(224,148,10,10),USE(?PopCalendar:9),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,148,10,10),USE(?Lookup:9),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 10:'),AT(8,164),USE(?glo:select10:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,164,124,10),USE(GLO:Select22),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,164,64,10),USE(GLO:Select22,,?glo:select22:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(224,164,10,10),USE(?PopCalendar:10),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,164,10,10),USE(?Lookup:10),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 11:'),AT(8,180),USE(?glo:select11:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,180,124,10),USE(GLO:Select23),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           ENTRY(@d6b),AT(56,180,64,10),USE(GLO:Select23,,?glo:select23:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(224,180,10,10),USE(?PopCalendar:11),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,180,10,10),USE(?Lookup:11),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 12:'),AT(8,196),USE(?glo:select12:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,196,124,10),USE(GLO:Select24),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(56,196,64,10),USE(GLO:Select24,,?glo:select24:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(224,196,10,10),USE(?PopCalendar:12),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,196,10,10),USE(?Lookup:12),HIDE,ICON('list3.ico')
                         END
                         TAB('Trade Fault Codes'),USE(?Tab2),HIDE
                           PROMPT('Fault Code 1'),AT(8,20),USE(?JOBE:TraFaultCode1:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,20,124,10),USE(GLO:Select31),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 1'),TIP('Fault Code 1'),UPR
                           BUTTON,AT(212,20,10,10),USE(?tralookup:1),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,20,10,10),USE(?PopCalendar:13),HIDE,LEFT,ICON('calenda2.ico')
                           PROMPT('Fault Code 2'),AT(8,36),USE(?JOBE:TraFaultCode2:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,36,124,10),USE(GLO:Select32),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 2'),TIP('Fault Code 2'),UPR
                           PROMPT('Fault Code 3'),AT(8,52),USE(?JOBE:TraFaultCode3:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,52,124,10),USE(GLO:Select33),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 3'),TIP('Fault Code 3'),UPR
                           PROMPT('Fault Code 4'),AT(8,68),USE(?JOBE:TraFaultCode4:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,68,124,10),USE(GLO:Select34),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 4'),TIP('Fault Code 4'),UPR
                           BUTTON,AT(212,68,10,10),USE(?tralookup:4),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,68,10,10),USE(?PopCalendar:16),HIDE,LEFT,ICON('calenda2.ico')
                           PROMPT('Fault Code 5'),AT(8,84),USE(?JOBE:TraFaultCode5:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,84,124,10),USE(GLO:Select35),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 5'),TIP('Fault Code 5'),UPR
                           BUTTON,AT(212,84,10,10),USE(?tralookup:5),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,84,10,10),USE(?PopCalendar:17),HIDE,LEFT,ICON('calenda2.ico')
                           PROMPT('Fault Code 6'),AT(8,100),USE(?JOBE:TraFaultCode6:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,100,124,10),USE(GLO:Select36),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 6'),TIP('Fault Code 6'),UPR
                           BUTTON,AT(212,36,10,10),USE(?tralookup:2),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,36,10,10),USE(?PopCalendar:14),HIDE,LEFT,ICON('calenda2.ico')
                           BUTTON,AT(212,52,10,10),USE(?tralookup:3),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,52,10,10),USE(?PopCalendar:15),HIDE,LEFT,ICON('calenda2.ico')
                           PROMPT('Fault Code 7'),AT(8,116),USE(?JOBE:TraFaultCode7:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,116,124,10),USE(GLO:Select37),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 7'),TIP('Fault Code 7'),UPR
                           BUTTON,AT(212,116,10,10),USE(?tralookup:7),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,116,10,10),USE(?PopCalendar:19),HIDE,LEFT,ICON('calenda2.ico')
                           PROMPT('Fault Code 8'),AT(8,132),USE(?JOBE:TraFaultCode8:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,132,124,10),USE(GLO:Select38),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 8'),TIP('Fault Code 8'),UPR
                           BUTTON,AT(212,132,10,10),USE(?tralookup:8),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,132,10,10),USE(?PopCalendar:20),HIDE,LEFT,ICON('calenda2.ico')
                           PROMPT('Fault Code 9'),AT(8,148),USE(?JOBE:TraFaultCode9:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,148,124,10),USE(GLO:Select39),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 9'),TIP('Fault Code 9'),UPR
                           PROMPT('Fault Code 10'),AT(8,164),USE(?JOBE:TraFaultCode10:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,164,124,10),USE(GLO:Select40),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 10'),TIP('Fault Code 10'),UPR
                           PROMPT('Fault Code 11'),AT(8,180),USE(?JOBE:TraFaultCode11:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,180,124,10),USE(GLO:Select41),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 11'),TIP('Fault Code 11'),UPR
                           PROMPT('Fault Code 12'),AT(8,196),USE(?JOBE:TraFaultCode12:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s40),AT(84,196,124,10),USE(GLO:Select42),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 12'),TIP('Fault Code 12'),UPR
                           BUTTON,AT(212,148,10,10),USE(?tralookup:9),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,148,10,10),USE(?PopCalendar:21),HIDE,LEFT,ICON('calenda2.ico')
                           BUTTON,AT(212,180,10,10),USE(?tralookup:11),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,180,10,10),USE(?PopCalendar:23),HIDE,LEFT,ICON('calenda2.ico')
                           BUTTON,AT(212,100,10,10),USE(?tralookup:6),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,100,10,10),USE(?PopCalendar:18),HIDE,LEFT,ICON('calenda2.ico')
                           BUTTON,AT(212,164,10,10),USE(?tralookup:10),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,164,10,10),USE(?PopCalendar:22),HIDE,LEFT,ICON('calenda2.ico')
                           BUTTON,AT(212,196,10,10),USE(?tralookup:12),SKIP,HIDE,ICON('list3.ico')
                           BUTTON,AT(224,196,10,10),USE(?PopCalendar:24),HIDE,LEFT,ICON('calenda2.ico')
                         END
                       END
                       PANEL,AT(4,220,236,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(180,224,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_maf_id     ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?glo:select1:Prompt{prop:FontColor} = -1
    ?glo:select1:Prompt{prop:Color} = 15066597
    If ?GLO:Select13{prop:ReadOnly} = True
        ?GLO:Select13{prop:FontColor} = 65793
        ?GLO:Select13{prop:Color} = 15066597
    Elsif ?GLO:Select13{prop:Req} = True
        ?GLO:Select13{prop:FontColor} = 65793
        ?GLO:Select13{prop:Color} = 8454143
    Else ! If ?GLO:Select13{prop:Req} = True
        ?GLO:Select13{prop:FontColor} = 65793
        ?GLO:Select13{prop:Color} = 16777215
    End ! If ?GLO:Select13{prop:Req} = True
    ?GLO:Select13{prop:Trn} = 0
    ?GLO:Select13{prop:FontStyle} = font:Bold
    If ?glo:select13:2{prop:ReadOnly} = True
        ?glo:select13:2{prop:FontColor} = 65793
        ?glo:select13:2{prop:Color} = 15066597
    Elsif ?glo:select13:2{prop:Req} = True
        ?glo:select13:2{prop:FontColor} = 65793
        ?glo:select13:2{prop:Color} = 8454143
    Else ! If ?glo:select13:2{prop:Req} = True
        ?glo:select13:2{prop:FontColor} = 65793
        ?glo:select13:2{prop:Color} = 16777215
    End ! If ?glo:select13:2{prop:Req} = True
    ?glo:select13:2{prop:Trn} = 0
    ?glo:select13:2{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?GLO:Select14{prop:ReadOnly} = True
        ?GLO:Select14{prop:FontColor} = 65793
        ?GLO:Select14{prop:Color} = 15066597
    Elsif ?GLO:Select14{prop:Req} = True
        ?GLO:Select14{prop:FontColor} = 65793
        ?GLO:Select14{prop:Color} = 8454143
    Else ! If ?GLO:Select14{prop:Req} = True
        ?GLO:Select14{prop:FontColor} = 65793
        ?GLO:Select14{prop:Color} = 16777215
    End ! If ?GLO:Select14{prop:Req} = True
    ?GLO:Select14{prop:Trn} = 0
    ?GLO:Select14{prop:FontStyle} = font:Bold
    If ?glo:select14:2{prop:ReadOnly} = True
        ?glo:select14:2{prop:FontColor} = 65793
        ?glo:select14:2{prop:Color} = 15066597
    Elsif ?glo:select14:2{prop:Req} = True
        ?glo:select14:2{prop:FontColor} = 65793
        ?glo:select14:2{prop:Color} = 8454143
    Else ! If ?glo:select14:2{prop:Req} = True
        ?glo:select14:2{prop:FontColor} = 65793
        ?glo:select14:2{prop:Color} = 16777215
    End ! If ?glo:select14:2{prop:Req} = True
    ?glo:select14:2{prop:Trn} = 0
    ?glo:select14:2{prop:FontStyle} = font:Bold
    ?glo:select3:Prompt{prop:FontColor} = -1
    ?glo:select3:Prompt{prop:Color} = 15066597
    If ?GLO:Select15{prop:ReadOnly} = True
        ?GLO:Select15{prop:FontColor} = 65793
        ?GLO:Select15{prop:Color} = 15066597
    Elsif ?GLO:Select15{prop:Req} = True
        ?GLO:Select15{prop:FontColor} = 65793
        ?GLO:Select15{prop:Color} = 8454143
    Else ! If ?GLO:Select15{prop:Req} = True
        ?GLO:Select15{prop:FontColor} = 65793
        ?GLO:Select15{prop:Color} = 16777215
    End ! If ?GLO:Select15{prop:Req} = True
    ?GLO:Select15{prop:Trn} = 0
    ?GLO:Select15{prop:FontStyle} = font:Bold
    If ?glo:select15:2{prop:ReadOnly} = True
        ?glo:select15:2{prop:FontColor} = 65793
        ?glo:select15:2{prop:Color} = 15066597
    Elsif ?glo:select15:2{prop:Req} = True
        ?glo:select15:2{prop:FontColor} = 65793
        ?glo:select15:2{prop:Color} = 8454143
    Else ! If ?glo:select15:2{prop:Req} = True
        ?glo:select15:2{prop:FontColor} = 65793
        ?glo:select15:2{prop:Color} = 16777215
    End ! If ?glo:select15:2{prop:Req} = True
    ?glo:select15:2{prop:Trn} = 0
    ?glo:select15:2{prop:FontStyle} = font:Bold
    ?glo:select4:Prompt{prop:FontColor} = -1
    ?glo:select4:Prompt{prop:Color} = 15066597
    If ?GLO:Select16{prop:ReadOnly} = True
        ?GLO:Select16{prop:FontColor} = 65793
        ?GLO:Select16{prop:Color} = 15066597
    Elsif ?GLO:Select16{prop:Req} = True
        ?GLO:Select16{prop:FontColor} = 65793
        ?GLO:Select16{prop:Color} = 8454143
    Else ! If ?GLO:Select16{prop:Req} = True
        ?GLO:Select16{prop:FontColor} = 65793
        ?GLO:Select16{prop:Color} = 16777215
    End ! If ?GLO:Select16{prop:Req} = True
    ?GLO:Select16{prop:Trn} = 0
    ?GLO:Select16{prop:FontStyle} = font:Bold
    If ?glo:select16:2{prop:ReadOnly} = True
        ?glo:select16:2{prop:FontColor} = 65793
        ?glo:select16:2{prop:Color} = 15066597
    Elsif ?glo:select16:2{prop:Req} = True
        ?glo:select16:2{prop:FontColor} = 65793
        ?glo:select16:2{prop:Color} = 8454143
    Else ! If ?glo:select16:2{prop:Req} = True
        ?glo:select16:2{prop:FontColor} = 65793
        ?glo:select16:2{prop:Color} = 16777215
    End ! If ?glo:select16:2{prop:Req} = True
    ?glo:select16:2{prop:Trn} = 0
    ?glo:select16:2{prop:FontStyle} = font:Bold
    ?glo:select5:Prompt{prop:FontColor} = -1
    ?glo:select5:Prompt{prop:Color} = 15066597
    If ?GLO:Select17{prop:ReadOnly} = True
        ?GLO:Select17{prop:FontColor} = 65793
        ?GLO:Select17{prop:Color} = 15066597
    Elsif ?GLO:Select17{prop:Req} = True
        ?GLO:Select17{prop:FontColor} = 65793
        ?GLO:Select17{prop:Color} = 8454143
    Else ! If ?GLO:Select17{prop:Req} = True
        ?GLO:Select17{prop:FontColor} = 65793
        ?GLO:Select17{prop:Color} = 16777215
    End ! If ?GLO:Select17{prop:Req} = True
    ?GLO:Select17{prop:Trn} = 0
    ?GLO:Select17{prop:FontStyle} = font:Bold
    If ?glo:select17:2{prop:ReadOnly} = True
        ?glo:select17:2{prop:FontColor} = 65793
        ?glo:select17:2{prop:Color} = 15066597
    Elsif ?glo:select17:2{prop:Req} = True
        ?glo:select17:2{prop:FontColor} = 65793
        ?glo:select17:2{prop:Color} = 8454143
    Else ! If ?glo:select17:2{prop:Req} = True
        ?glo:select17:2{prop:FontColor} = 65793
        ?glo:select17:2{prop:Color} = 16777215
    End ! If ?glo:select17:2{prop:Req} = True
    ?glo:select17:2{prop:Trn} = 0
    ?glo:select17:2{prop:FontStyle} = font:Bold
    ?glo:select6:Prompt{prop:FontColor} = -1
    ?glo:select6:Prompt{prop:Color} = 15066597
    If ?GLO:Select18{prop:ReadOnly} = True
        ?GLO:Select18{prop:FontColor} = 65793
        ?GLO:Select18{prop:Color} = 15066597
    Elsif ?GLO:Select18{prop:Req} = True
        ?GLO:Select18{prop:FontColor} = 65793
        ?GLO:Select18{prop:Color} = 8454143
    Else ! If ?GLO:Select18{prop:Req} = True
        ?GLO:Select18{prop:FontColor} = 65793
        ?GLO:Select18{prop:Color} = 16777215
    End ! If ?GLO:Select18{prop:Req} = True
    ?GLO:Select18{prop:Trn} = 0
    ?GLO:Select18{prop:FontStyle} = font:Bold
    If ?glo:select18:2{prop:ReadOnly} = True
        ?glo:select18:2{prop:FontColor} = 65793
        ?glo:select18:2{prop:Color} = 15066597
    Elsif ?glo:select18:2{prop:Req} = True
        ?glo:select18:2{prop:FontColor} = 65793
        ?glo:select18:2{prop:Color} = 8454143
    Else ! If ?glo:select18:2{prop:Req} = True
        ?glo:select18:2{prop:FontColor} = 65793
        ?glo:select18:2{prop:Color} = 16777215
    End ! If ?glo:select18:2{prop:Req} = True
    ?glo:select18:2{prop:Trn} = 0
    ?glo:select18:2{prop:FontStyle} = font:Bold
    ?glo:select7:Prompt{prop:FontColor} = -1
    ?glo:select7:Prompt{prop:Color} = 15066597
    If ?GLO:Select19{prop:ReadOnly} = True
        ?GLO:Select19{prop:FontColor} = 65793
        ?GLO:Select19{prop:Color} = 15066597
    Elsif ?GLO:Select19{prop:Req} = True
        ?GLO:Select19{prop:FontColor} = 65793
        ?GLO:Select19{prop:Color} = 8454143
    Else ! If ?GLO:Select19{prop:Req} = True
        ?GLO:Select19{prop:FontColor} = 65793
        ?GLO:Select19{prop:Color} = 16777215
    End ! If ?GLO:Select19{prop:Req} = True
    ?GLO:Select19{prop:Trn} = 0
    ?GLO:Select19{prop:FontStyle} = font:Bold
    If ?glo:select19:2{prop:ReadOnly} = True
        ?glo:select19:2{prop:FontColor} = 65793
        ?glo:select19:2{prop:Color} = 15066597
    Elsif ?glo:select19:2{prop:Req} = True
        ?glo:select19:2{prop:FontColor} = 65793
        ?glo:select19:2{prop:Color} = 8454143
    Else ! If ?glo:select19:2{prop:Req} = True
        ?glo:select19:2{prop:FontColor} = 65793
        ?glo:select19:2{prop:Color} = 16777215
    End ! If ?glo:select19:2{prop:Req} = True
    ?glo:select19:2{prop:Trn} = 0
    ?glo:select19:2{prop:FontStyle} = font:Bold
    ?glo:select8:Prompt{prop:FontColor} = -1
    ?glo:select8:Prompt{prop:Color} = 15066597
    If ?GLO:Select20{prop:ReadOnly} = True
        ?GLO:Select20{prop:FontColor} = 65793
        ?GLO:Select20{prop:Color} = 15066597
    Elsif ?GLO:Select20{prop:Req} = True
        ?GLO:Select20{prop:FontColor} = 65793
        ?GLO:Select20{prop:Color} = 8454143
    Else ! If ?GLO:Select20{prop:Req} = True
        ?GLO:Select20{prop:FontColor} = 65793
        ?GLO:Select20{prop:Color} = 16777215
    End ! If ?GLO:Select20{prop:Req} = True
    ?GLO:Select20{prop:Trn} = 0
    ?GLO:Select20{prop:FontStyle} = font:Bold
    If ?glo:select20:2{prop:ReadOnly} = True
        ?glo:select20:2{prop:FontColor} = 65793
        ?glo:select20:2{prop:Color} = 15066597
    Elsif ?glo:select20:2{prop:Req} = True
        ?glo:select20:2{prop:FontColor} = 65793
        ?glo:select20:2{prop:Color} = 8454143
    Else ! If ?glo:select20:2{prop:Req} = True
        ?glo:select20:2{prop:FontColor} = 65793
        ?glo:select20:2{prop:Color} = 16777215
    End ! If ?glo:select20:2{prop:Req} = True
    ?glo:select20:2{prop:Trn} = 0
    ?glo:select20:2{prop:FontStyle} = font:Bold
    ?glo:select9:Prompt{prop:FontColor} = -1
    ?glo:select9:Prompt{prop:Color} = 15066597
    If ?GLO:Select21{prop:ReadOnly} = True
        ?GLO:Select21{prop:FontColor} = 65793
        ?GLO:Select21{prop:Color} = 15066597
    Elsif ?GLO:Select21{prop:Req} = True
        ?GLO:Select21{prop:FontColor} = 65793
        ?GLO:Select21{prop:Color} = 8454143
    Else ! If ?GLO:Select21{prop:Req} = True
        ?GLO:Select21{prop:FontColor} = 65793
        ?GLO:Select21{prop:Color} = 16777215
    End ! If ?GLO:Select21{prop:Req} = True
    ?GLO:Select21{prop:Trn} = 0
    ?GLO:Select21{prop:FontStyle} = font:Bold
    If ?glo:select21:2{prop:ReadOnly} = True
        ?glo:select21:2{prop:FontColor} = 65793
        ?glo:select21:2{prop:Color} = 15066597
    Elsif ?glo:select21:2{prop:Req} = True
        ?glo:select21:2{prop:FontColor} = 65793
        ?glo:select21:2{prop:Color} = 8454143
    Else ! If ?glo:select21:2{prop:Req} = True
        ?glo:select21:2{prop:FontColor} = 65793
        ?glo:select21:2{prop:Color} = 16777215
    End ! If ?glo:select21:2{prop:Req} = True
    ?glo:select21:2{prop:Trn} = 0
    ?glo:select21:2{prop:FontStyle} = font:Bold
    ?glo:select10:Prompt{prop:FontColor} = -1
    ?glo:select10:Prompt{prop:Color} = 15066597
    If ?GLO:Select22{prop:ReadOnly} = True
        ?GLO:Select22{prop:FontColor} = 65793
        ?GLO:Select22{prop:Color} = 15066597
    Elsif ?GLO:Select22{prop:Req} = True
        ?GLO:Select22{prop:FontColor} = 65793
        ?GLO:Select22{prop:Color} = 8454143
    Else ! If ?GLO:Select22{prop:Req} = True
        ?GLO:Select22{prop:FontColor} = 65793
        ?GLO:Select22{prop:Color} = 16777215
    End ! If ?GLO:Select22{prop:Req} = True
    ?GLO:Select22{prop:Trn} = 0
    ?GLO:Select22{prop:FontStyle} = font:Bold
    If ?glo:select22:2{prop:ReadOnly} = True
        ?glo:select22:2{prop:FontColor} = 65793
        ?glo:select22:2{prop:Color} = 15066597
    Elsif ?glo:select22:2{prop:Req} = True
        ?glo:select22:2{prop:FontColor} = 65793
        ?glo:select22:2{prop:Color} = 8454143
    Else ! If ?glo:select22:2{prop:Req} = True
        ?glo:select22:2{prop:FontColor} = 65793
        ?glo:select22:2{prop:Color} = 16777215
    End ! If ?glo:select22:2{prop:Req} = True
    ?glo:select22:2{prop:Trn} = 0
    ?glo:select22:2{prop:FontStyle} = font:Bold
    ?glo:select11:Prompt{prop:FontColor} = -1
    ?glo:select11:Prompt{prop:Color} = 15066597
    If ?GLO:Select23{prop:ReadOnly} = True
        ?GLO:Select23{prop:FontColor} = 65793
        ?GLO:Select23{prop:Color} = 15066597
    Elsif ?GLO:Select23{prop:Req} = True
        ?GLO:Select23{prop:FontColor} = 65793
        ?GLO:Select23{prop:Color} = 8454143
    Else ! If ?GLO:Select23{prop:Req} = True
        ?GLO:Select23{prop:FontColor} = 65793
        ?GLO:Select23{prop:Color} = 16777215
    End ! If ?GLO:Select23{prop:Req} = True
    ?GLO:Select23{prop:Trn} = 0
    ?GLO:Select23{prop:FontStyle} = font:Bold
    If ?glo:select23:2{prop:ReadOnly} = True
        ?glo:select23:2{prop:FontColor} = 65793
        ?glo:select23:2{prop:Color} = 15066597
    Elsif ?glo:select23:2{prop:Req} = True
        ?glo:select23:2{prop:FontColor} = 65793
        ?glo:select23:2{prop:Color} = 8454143
    Else ! If ?glo:select23:2{prop:Req} = True
        ?glo:select23:2{prop:FontColor} = 65793
        ?glo:select23:2{prop:Color} = 16777215
    End ! If ?glo:select23:2{prop:Req} = True
    ?glo:select23:2{prop:Trn} = 0
    ?glo:select23:2{prop:FontStyle} = font:Bold
    ?glo:select12:Prompt{prop:FontColor} = -1
    ?glo:select12:Prompt{prop:Color} = 15066597
    If ?GLO:Select24{prop:ReadOnly} = True
        ?GLO:Select24{prop:FontColor} = 65793
        ?GLO:Select24{prop:Color} = 15066597
    Elsif ?GLO:Select24{prop:Req} = True
        ?GLO:Select24{prop:FontColor} = 65793
        ?GLO:Select24{prop:Color} = 8454143
    Else ! If ?GLO:Select24{prop:Req} = True
        ?GLO:Select24{prop:FontColor} = 65793
        ?GLO:Select24{prop:Color} = 16777215
    End ! If ?GLO:Select24{prop:Req} = True
    ?GLO:Select24{prop:Trn} = 0
    ?GLO:Select24{prop:FontStyle} = font:Bold
    If ?glo:select24:2{prop:ReadOnly} = True
        ?glo:select24:2{prop:FontColor} = 65793
        ?glo:select24:2{prop:Color} = 15066597
    Elsif ?glo:select24:2{prop:Req} = True
        ?glo:select24:2{prop:FontColor} = 65793
        ?glo:select24:2{prop:Color} = 8454143
    Else ! If ?glo:select24:2{prop:Req} = True
        ?glo:select24:2{prop:FontColor} = 65793
        ?glo:select24:2{prop:Color} = 16777215
    End ! If ?glo:select24:2{prop:Req} = True
    ?glo:select24:2{prop:Trn} = 0
    ?glo:select24:2{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?JOBE:TraFaultCode1:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode1:Prompt{prop:Color} = 15066597
    If ?GLO:Select31{prop:ReadOnly} = True
        ?GLO:Select31{prop:FontColor} = 65793
        ?GLO:Select31{prop:Color} = 15066597
    Elsif ?GLO:Select31{prop:Req} = True
        ?GLO:Select31{prop:FontColor} = 65793
        ?GLO:Select31{prop:Color} = 8454143
    Else ! If ?GLO:Select31{prop:Req} = True
        ?GLO:Select31{prop:FontColor} = 65793
        ?GLO:Select31{prop:Color} = 16777215
    End ! If ?GLO:Select31{prop:Req} = True
    ?GLO:Select31{prop:Trn} = 0
    ?GLO:Select31{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode2:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode2:Prompt{prop:Color} = 15066597
    If ?GLO:Select32{prop:ReadOnly} = True
        ?GLO:Select32{prop:FontColor} = 65793
        ?GLO:Select32{prop:Color} = 15066597
    Elsif ?GLO:Select32{prop:Req} = True
        ?GLO:Select32{prop:FontColor} = 65793
        ?GLO:Select32{prop:Color} = 8454143
    Else ! If ?GLO:Select32{prop:Req} = True
        ?GLO:Select32{prop:FontColor} = 65793
        ?GLO:Select32{prop:Color} = 16777215
    End ! If ?GLO:Select32{prop:Req} = True
    ?GLO:Select32{prop:Trn} = 0
    ?GLO:Select32{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode3:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode3:Prompt{prop:Color} = 15066597
    If ?GLO:Select33{prop:ReadOnly} = True
        ?GLO:Select33{prop:FontColor} = 65793
        ?GLO:Select33{prop:Color} = 15066597
    Elsif ?GLO:Select33{prop:Req} = True
        ?GLO:Select33{prop:FontColor} = 65793
        ?GLO:Select33{prop:Color} = 8454143
    Else ! If ?GLO:Select33{prop:Req} = True
        ?GLO:Select33{prop:FontColor} = 65793
        ?GLO:Select33{prop:Color} = 16777215
    End ! If ?GLO:Select33{prop:Req} = True
    ?GLO:Select33{prop:Trn} = 0
    ?GLO:Select33{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode4:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode4:Prompt{prop:Color} = 15066597
    If ?GLO:Select34{prop:ReadOnly} = True
        ?GLO:Select34{prop:FontColor} = 65793
        ?GLO:Select34{prop:Color} = 15066597
    Elsif ?GLO:Select34{prop:Req} = True
        ?GLO:Select34{prop:FontColor} = 65793
        ?GLO:Select34{prop:Color} = 8454143
    Else ! If ?GLO:Select34{prop:Req} = True
        ?GLO:Select34{prop:FontColor} = 65793
        ?GLO:Select34{prop:Color} = 16777215
    End ! If ?GLO:Select34{prop:Req} = True
    ?GLO:Select34{prop:Trn} = 0
    ?GLO:Select34{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode5:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode5:Prompt{prop:Color} = 15066597
    If ?GLO:Select35{prop:ReadOnly} = True
        ?GLO:Select35{prop:FontColor} = 65793
        ?GLO:Select35{prop:Color} = 15066597
    Elsif ?GLO:Select35{prop:Req} = True
        ?GLO:Select35{prop:FontColor} = 65793
        ?GLO:Select35{prop:Color} = 8454143
    Else ! If ?GLO:Select35{prop:Req} = True
        ?GLO:Select35{prop:FontColor} = 65793
        ?GLO:Select35{prop:Color} = 16777215
    End ! If ?GLO:Select35{prop:Req} = True
    ?GLO:Select35{prop:Trn} = 0
    ?GLO:Select35{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode6:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode6:Prompt{prop:Color} = 15066597
    If ?GLO:Select36{prop:ReadOnly} = True
        ?GLO:Select36{prop:FontColor} = 65793
        ?GLO:Select36{prop:Color} = 15066597
    Elsif ?GLO:Select36{prop:Req} = True
        ?GLO:Select36{prop:FontColor} = 65793
        ?GLO:Select36{prop:Color} = 8454143
    Else ! If ?GLO:Select36{prop:Req} = True
        ?GLO:Select36{prop:FontColor} = 65793
        ?GLO:Select36{prop:Color} = 16777215
    End ! If ?GLO:Select36{prop:Req} = True
    ?GLO:Select36{prop:Trn} = 0
    ?GLO:Select36{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode7:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode7:Prompt{prop:Color} = 15066597
    If ?GLO:Select37{prop:ReadOnly} = True
        ?GLO:Select37{prop:FontColor} = 65793
        ?GLO:Select37{prop:Color} = 15066597
    Elsif ?GLO:Select37{prop:Req} = True
        ?GLO:Select37{prop:FontColor} = 65793
        ?GLO:Select37{prop:Color} = 8454143
    Else ! If ?GLO:Select37{prop:Req} = True
        ?GLO:Select37{prop:FontColor} = 65793
        ?GLO:Select37{prop:Color} = 16777215
    End ! If ?GLO:Select37{prop:Req} = True
    ?GLO:Select37{prop:Trn} = 0
    ?GLO:Select37{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode8:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode8:Prompt{prop:Color} = 15066597
    If ?GLO:Select38{prop:ReadOnly} = True
        ?GLO:Select38{prop:FontColor} = 65793
        ?GLO:Select38{prop:Color} = 15066597
    Elsif ?GLO:Select38{prop:Req} = True
        ?GLO:Select38{prop:FontColor} = 65793
        ?GLO:Select38{prop:Color} = 8454143
    Else ! If ?GLO:Select38{prop:Req} = True
        ?GLO:Select38{prop:FontColor} = 65793
        ?GLO:Select38{prop:Color} = 16777215
    End ! If ?GLO:Select38{prop:Req} = True
    ?GLO:Select38{prop:Trn} = 0
    ?GLO:Select38{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode9:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode9:Prompt{prop:Color} = 15066597
    If ?GLO:Select39{prop:ReadOnly} = True
        ?GLO:Select39{prop:FontColor} = 65793
        ?GLO:Select39{prop:Color} = 15066597
    Elsif ?GLO:Select39{prop:Req} = True
        ?GLO:Select39{prop:FontColor} = 65793
        ?GLO:Select39{prop:Color} = 8454143
    Else ! If ?GLO:Select39{prop:Req} = True
        ?GLO:Select39{prop:FontColor} = 65793
        ?GLO:Select39{prop:Color} = 16777215
    End ! If ?GLO:Select39{prop:Req} = True
    ?GLO:Select39{prop:Trn} = 0
    ?GLO:Select39{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode10:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode10:Prompt{prop:Color} = 15066597
    If ?GLO:Select40{prop:ReadOnly} = True
        ?GLO:Select40{prop:FontColor} = 65793
        ?GLO:Select40{prop:Color} = 15066597
    Elsif ?GLO:Select40{prop:Req} = True
        ?GLO:Select40{prop:FontColor} = 65793
        ?GLO:Select40{prop:Color} = 8454143
    Else ! If ?GLO:Select40{prop:Req} = True
        ?GLO:Select40{prop:FontColor} = 65793
        ?GLO:Select40{prop:Color} = 16777215
    End ! If ?GLO:Select40{prop:Req} = True
    ?GLO:Select40{prop:Trn} = 0
    ?GLO:Select40{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode11:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode11:Prompt{prop:Color} = 15066597
    If ?GLO:Select41{prop:ReadOnly} = True
        ?GLO:Select41{prop:FontColor} = 65793
        ?GLO:Select41{prop:Color} = 15066597
    Elsif ?GLO:Select41{prop:Req} = True
        ?GLO:Select41{prop:FontColor} = 65793
        ?GLO:Select41{prop:Color} = 8454143
    Else ! If ?GLO:Select41{prop:Req} = True
        ?GLO:Select41{prop:FontColor} = 65793
        ?GLO:Select41{prop:Color} = 16777215
    End ! If ?GLO:Select41{prop:Req} = True
    ?GLO:Select41{prop:Trn} = 0
    ?GLO:Select41{prop:FontStyle} = font:Bold
    ?JOBE:TraFaultCode12:Prompt{prop:FontColor} = -1
    ?JOBE:TraFaultCode12:Prompt{prop:Color} = 15066597
    If ?GLO:Select42{prop:ReadOnly} = True
        ?GLO:Select42{prop:FontColor} = 65793
        ?GLO:Select42{prop:Color} = 15066597
    Elsif ?GLO:Select42{prop:Req} = True
        ?GLO:Select42{prop:FontColor} = 65793
        ?GLO:Select42{prop:Color} = 8454143
    Else ! If ?GLO:Select42{prop:Req} = True
        ?GLO:Select42{prop:FontColor} = 65793
        ?GLO:Select42{prop:Color} = 16777215
    End ! If ?GLO:Select42{prop:Req} = True
    ?GLO:Select42{prop:Trn} = 0
    ?GLO:Select42{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Fault_Coding        Routine   ! Manufacturers Fault Codes

    Hide(?glo:select13)
    Hide(?glo:select13:2)
    Hide(?glo:select14)
    Hide(?glo:select14:2)
    Hide(?glo:select15)
    Hide(?glo:select15:2)
    Hide(?glo:select16)
    Hide(?glo:select16:2)
    Hide(?glo:select17)
    Hide(?glo:select17:2)
    Hide(?glo:select18)
    Hide(?glo:select18:2)
    Hide(?glo:select19)
    Hide(?glo:select19:2)
    Hide(?glo:select20)
    Hide(?glo:select20:2)
    Hide(?glo:select21)
    Hide(?glo:select21:2)
    Hide(?glo:select22)
    Hide(?glo:select22:2)
    Hide(?glo:select23)
    Hide(?glo:select23:2)
    Hide(?glo:select24)
    Hide(?glo:select24:2)
    Hide(?glo:select1:prompt)
    Hide(?glo:select2:prompt)
    Hide(?glo:select3:prompt)
    Hide(?glo:select4:prompt)
    Hide(?glo:select5:prompt)
    Hide(?glo:select6:prompt)
    Hide(?glo:select7:prompt)
    Hide(?glo:select8:prompt)
    Hide(?glo:select9:prompt)
    Hide(?glo:select10:prompt)
    Hide(?glo:select11:prompt)
    Hide(?glo:select12:prompt)
    Hide(?popcalendar)
    Hide(?popcalendar:2)
    Hide(?popcalendar:3)
    Hide(?popcalendar:4)
    Hide(?popcalendar:5)
    Hide(?popcalendar:6)
    Hide(?popcalendar:7)
    Hide(?popcalendar:8)
    Hide(?popcalendar:9)
    Hide(?popcalendar:10)
    Hide(?popcalendar:11)
    Hide(?popcalendar:12)
    Hide(?lookup)
    Hide(?lookup:2)
    Hide(?lookup:3)
    Hide(?lookup:4)
    Hide(?lookup:5)
    Hide(?lookup:6)
    Hide(?lookup:7)
    Hide(?lookup:8)
    Hide(?lookup:9)
    Hide(?lookup:10)
    Hide(?lookup:11)
    Hide(?lookup:12)

    required# = 0
    access:chartype.clearkey(cha:charge_type_key)
    cha:charge_type = job:charge_type
    If access:chartype.fetch(cha:charge_type_key) = Level:Benign
        If cha:force_warranty = 'YES'
            required# = 1
        End
    end !if


    setcursor(cursor:wait)
    save_maf_id = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = glo:select4
    set(maf:field_number_key,maf:field_number_key)
    loop
        if access:manfault.next()
           break
        end !if
        if maf:manufacturer <> glo:select4      |
            then break.  ! end if
        Case maf:field_number
            Of 1
                Unhide(?glo:select1:prompt)
                ?glo:select1:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    ?glo:Select13:2{prop:text} = Clip(maf:DateType)
                    Unhide(?popcalendar)
                    ?popcalendar{prop:xpos} = 300
                    Unhide(?glo:select13:2)
                    ?glo:select13:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select13)
                    If maf:lookup = 'YES'
                        Unhide(?lookup)
                    End
                End
                If required# = 1
                    ?glo:select13{prop:req} = 1
                    ?glo:select13:2{prop:req} = 1
                Else
                    ?glo:select13{prop:req} = 0
                    ?glo:select13:2{prop:req} = 0
                End
            Of 2
                Unhide(?glo:select2:prompt)
                ?glo:select2:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    ?glo:Select14:2{prop:text} = Clip(maf:DateType)
                    Unhide(?popcalendar:2)
                    ?popcalendar:2{prop:xpos} = 300
                    Unhide(?glo:select14:2)
                    ?glo:select14:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select14)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:2)
                    End
                End
                If required# = 1
                    ?glo:select14{prop:req} = 1
                    ?glo:select14:2{prop:req} = 1
                Else
                    ?glo:select14{prop:req} = 0
                    ?glo:select14:2{prop:req} = 0
                End

            Of 3
                Unhide(?glo:select3:prompt)
                ?glo:select3:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    ?glo:Select15:2{prop:text} = Clip(maf:DateType)
                    Unhide(?popcalendar:3)
                    ?popcalendar:3{prop:xpos} = 300
                    Unhide(?glo:select15:2)
                    ?glo:select15:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select15)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:3)
                    End
                End
                If required# = 1
                    ?glo:select15{prop:req} = 1
                    ?glo:select15:2{prop:req} = 1
                Else
                    ?glo:select15{prop:req} = 0
                    ?glo:select15:2{prop:req} = 0
                End

            Of 4
                Unhide(?glo:select4:prompt)
                ?glo:select4:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:4)
                    ?popcalendar:4{prop:xpos} = 300
                    Unhide(?glo:select16:2)
                    ?glo:select16:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select16)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:4)
                    End
                End
                If required# = 1
                    ?glo:select16{prop:req} = 1
                    ?glo:select16:2{prop:req} = 1
                Else
                    ?glo:select16{prop:req} = 0
                    ?glo:select16:2{prop:req} = 0
                End

            Of 5
                Unhide(?glo:select5:prompt)
                ?glo:select5:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:5)
                    ?popcalendar:5{prop:xpos} = 300
                    Unhide(?glo:select17:2)
                    ?glo:select17:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select17)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:5)
                    End
                End
                If required# = 1
                    ?glo:select17{prop:req} = 1
                    ?glo:select17:2{prop:req} = 1
                Else
                    ?glo:select17{prop:req} = 0
                    ?glo:select17:2{prop:req} = 0
                End

            Of 6
                Unhide(?glo:select6:prompt)
                ?glo:select6:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:6)
                    ?popcalendar:6{prop:xpos} = 300
                    Unhide(?glo:select18:2)
                    ?glo:select18:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select18)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:6)
                    End
                End
                If required# = 1
                    ?glo:select18{prop:req} = 1
                    ?glo:select18:2{prop:req} = 1
                Else
                    ?glo:select18{prop:req} = 0
                    ?glo:select18:2{prop:req} = 0
                End

            Of 7
                Unhide(?glo:select7:prompt)
                ?glo:select7:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:7)
                    ?popcalendar:7{prop:xpos} = 300
                    Unhide(?glo:select19:2)
                    ?glo:select19:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select19)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:7)
                    End
                End
                If required# = 1
                    ?glo:select19{prop:req} = 1
                    ?glo:select19:2{prop:req} = 1
                Else
                    ?glo:select19{prop:req} = 0
                    ?glo:select19:2{prop:req} = 0
                End

            Of 8
                Unhide(?glo:select8:prompt)
                ?glo:select8:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:8)
                    ?popcalendar:8{prop:xpos} = 300
                    Unhide(?glo:select20:2)
                    ?glo:select20:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select20)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:8)
                    End
                End
                If required# = 1
                    ?glo:select20{prop:req} = 1
                    ?glo:select20:2{prop:req} = 1
                Else
                    ?glo:select20{prop:req} = 0
                    ?glo:select20:2{prop:req} = 0
                End

            Of 9
                Unhide(?glo:select9:prompt)
                ?glo:select9:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:9)
                    ?popcalendar:9{prop:xpos} = 300
                    Unhide(?glo:select21:2)
                    ?glo:select21:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select21)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:9)
                    End
                End
                If required# = 1
                    ?glo:select21{prop:req} = 1
                    ?glo:select21:2{prop:req} = 1
                Else
                    ?glo:select21{prop:req} = 0
                    ?glo:select21:2{prop:req} = 0
                End

            Of 10
                Unhide(?glo:select10:prompt)
                ?glo:select10:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:10)
                    ?popcalendar:10{prop:xpos} = 152
                    Unhide(?glo:select22:2)
                    ?glo:select22:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select22)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:10)
                    End
                End
                If required# = 1
                    ?glo:select22{prop:req} = 1
                    ?glo:select22:2{prop:req} = 1
                Else
                    ?glo:select22{prop:req} = 0
                    ?glo:select22:2{prop:req} = 0
                End

            Of 11
                Unhide(?glo:select11:prompt)
                ?glo:select11:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:11)
                    ?popcalendar:11{prop:xpos} = 152
                    Unhide(?glo:select23:2)
                    ?glo:select23:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select23)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:11)
                    End
                End
                If required# = 1
                    ?glo:select23{prop:req} = 1
                    ?glo:select23:2{prop:req} = 1
                Else
                    ?glo:select23{prop:req} = 0
                    ?glo:select23:2{prop:req} = 0
                End

            Of 12
                Unhide(?glo:select12:prompt)
                ?glo:select12:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:12)
                    ?popcalendar:12{prop:xpos} = 152
                    Unhide(?glo:select24:2)
                    ?glo:select24:2{prop:xpos} = 84
                Else
                    Unhide(?glo:select24)
                    If maf:lookup = 'YES'
                        Unhide(?lookup:12)
                    End
                End
                If required# = 1
                    ?glo:select24{prop:req} = 1
                    ?glo:select24:2{prop:req} = 1
                Else
                    ?glo:select24{prop:req} = 0
                    ?glo:select24:2{prop:req} = 0
                End

        End
    end !loop
    access:manfault.restorefile(save_maf_id)
    setcursor()
        found# = 0
    setcursor(cursor:wait)
    !save_taf_id = access:trafault.savefile()
    access:trafault.clearkey(taf:field_number_key)
    taf:accountnumber = GLO:Select43
    set(taf:field_number_key,taf:field_number_key)
    loop
        if access:trafault.next()
           break
        end !if
        if taf:accountnumber <> GLO:Select43      |
            then break.  ! end if
        if found# = 0    
            found# = 1
            unhide(?Tab2)        
        End!if found# = 0
        hide# = 0
        req#  = 0
        !If f_request = Insertrecord

        !    If taf:compulsory_at_booking = 'YES'
        !        req#    = 1
         !   Else!If taf:compulsory_at_booking = 'YES'
          !      hide#   = 1
        !    End!If taf:compulsory_at_booking = 'YES'
       ! Else!If f_request = Insertrecord
            If taf:Compulsory = 'YES'
                req# = 1
            End!If taf:Compulsory = 'YES'
        !End!If f_request = Insertrecord

        If taf:lookup = 'YES'
            field"  = ''
            records# = 0
            !save_tfo_id = access:trafaulo.savefile()
            access:trafaulo.clearkey(tfo:field_key)
            tfo:accountnumber = taf:accountnumber
            tfo:field_number = taf:field_number
            set(tfo:field_key,tfo:field_key)
            loop
                if access:trafaulo.next()
                   break
                end !if
                if tfo:accountnumber <> taf:accountnumber      |
                or tfo:field_number <> taf:field_number      |
                    then break.  ! end if
                records# += 1
                If records# > 1
                    field" = ''
                    Break
                End!If records# > 1
                field"  = tfo:field
            end !loop
            !access:trafaulo.restorefile(save_tfo_id)
            Case taf:field_number
                of 1
                    If GLO:Select31 = ''
                        GLO:Select31 = field"
                    End!If jobe:TraFaultCode = ''
                of 2
                    If GLO:Select32 = ''
                        GLO:Select32 = field"
                    End!If jobe:TraFaultCode = ''
                of 3
                    If GLO:Select33 = ''
                        GLO:Select33 = field"
                    End!If jobe:TraFaultCode = ''
                of 4
                    If GLO:Select34 = ''
                        GLO:Select34 = field"
                    End!If jobe:TraFaultCode = ''
                of 5
                    If GLO:Select35 = ''
                        GLO:Select35 = field"
                    End!If jobe:TraFaultCode = ''
                of 6
                    If GLO:Select36 = ''
                        GLO:Select36 = field"
                    End!If jobe:TraFaultCode = ''
                of 7
                    If GLO:Select37 = ''
                        GLO:Select37 = field"
                    End!If jobe:TraFaultCode = ''
                of 8
                    If GLO:Select38 = ''
                        GLO:Select38 = field"
                    End!If jobe:TraFaultCode = ''
                of 9
                    If GLO:Select39 = ''
                        GLO:Select39 = field"
                    End!If jobe:TraFaultCode = ''
                of 10
                    If GLO:Select40 = ''
                        GLO:Select40 = field"
                    End!If jobe:TraFaultCode = ''
                of 11
                    If GLO:Select41 = ''
                        GLO:Select41 = field"
                    End!If jobe:TraFaultCode = ''
                of 12
                    If GLO:Select42 = ''
                        GLO:Select42 = field"
                    End!If jobe:TraFaultCode = ''
            End!Case taf:field_number
        End!If taf:lookup = 'YES'

        Case taf:field_number
            Of 1
                If hide# = 0
                    Unhide(?jobe:TraFaultCode1:prompt)
                    Unhide(?GLO:Select31)
                    ?jobe:TraFaultCode1:prompt{prop:text} = taf:field_name
                    If req# = 1
                        ?GLO:Select31{prop:req} = 1
                    End!If req# = 1

                    Case taf:field_type
                        Of 'DATE'
                            ?GLO:Select31{prop:text} = '@d6b'
                            ?GLO:Select31{prop:width} = 64
                            Unhide(?popcalendar:13)

                        Of 'STRING'
                            ?GLO:Select31{prop:width} = 124
                            If taf:restrictlength
                                ?GLO:Select31{prop:text} = '@s' & taf:lengthto
                            Else!If taf:restrictlength
                                ?GLO:Select31{prop:text} = '@s30'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:1)
                            End!If taf:lookup = 'YES'
                        Of 'NUMBER'
                            ?GLO:Select31{prop:width} = 64
                            If taf:restrictlength
                                ?GLO:Select31{prop:text} = '@n' & taf:lengthto
                            Else!If taf:restrictlength
                                ?GLO:Select31{prop:text} = '@n9'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:1)
                            End!If taf:lookup = 'YES'
                    End!Case taf:field_type

                End!If hide# = 0
            Of 2
                If hide# = 0
                    Unhide(?jobe:TraFaultCode2:prompt)
                    Unhide(?GLO:Select32)
                    ?jobe:TraFaultCode2:prompt{prop:text} = taf:field_name
                    If req# = 1
                        ?GLO:Select32{prop:req} = 1
                    End!If req# = 1
                    Case taf:field_type
                        Of 'DATE'
                            ?GLO:Select32{prop:text} = '@d6b'
                            ?GLO:Select32{prop:width} = 64
                            Unhide(?popcalendar:14)

                        Of 'STRING'
                            ?GLO:Select32{prop:width} = 124
                            If taf:restrictlength
                                ?GLO:Select32{prop:text} = '@s' & taf:lengthto
                            Else!If taf:restrictlength
                                ?GLO:Select32{prop:text} = '@s30'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:2)
                            End!If taf:lookup = 'YES'
                        Of 'NUMBER'
                            
                            ?GLO:Select32{prop:width} = 64
                            If taf:restrictlength
                                ?GLO:Select32{prop:text} = '@n' & taf:lengthto
                            Else!If taf:restrictlength
                                ?GLO:Select32{prop:text} = '@n9'
                            End!If taf:restrictlength
                            If taf:lookup = 'YES'
                                Unhide(?tralookup:2)
                            End!If taf:lookup = 'YES'
                    End!Case taf:field_type
                End!If hide# = 0

            Of 3
                if hide# = 0
                    unhide(?jobe:TraFaultCode3:prompt)
                    unhide(?GLO:Select33)
                    ?jobe:TraFaultCode3:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?GLO:Select33{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?GLO:Select33{prop:text} = '@d6b'
                            ?GLO:Select33{prop:width} = 64
                            unhide(?popcalendar:15)

                        of 'STRING'
                            ?GLO:Select33{prop:width} = 124
                            if taf:restrictlength
                                ?GLO:Select33{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select33{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:3)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?GLO:Select33{prop:width} = 64
                            if taf:restrictlength
                                ?GLO:Select33{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select33{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:3)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 4
                if hide# = 0
                    unhide(?jobe:TraFaultCode4:prompt)
                    unhide(?GLO:Select34)
                    ?jobe:TraFaultCode4:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?GLO:Select34{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?GLO:Select34{prop:text} = '@d6b'
                            ?GLO:Select34{prop:width} = 64
                            unhide(?popcalendar:16)

                        of 'STRING'
                            ?GLO:Select34{prop:width} = 124
                            if taf:restrictlength
                                ?GLO:Select34{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select34{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:4)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?GLO:Select34{prop:width} = 64
                            if taf:restrictlength
                                ?GLO:Select34{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select34{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:4)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 5
                if hide# = 0
                    unhide(?jobe:TraFaultCode5:prompt)
                    unhide(?GLO:Select35)
                    ?jobe:TraFaultCode5:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?GLO:Select35{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?GLO:Select35{prop:text} = '@d6b'
                            ?GLO:Select35{prop:width} = 64
                            unhide(?popcalendar:17)

                        of 'STRING'
                            ?GLO:Select35{prop:width} = 124
                            if taf:restrictlength
                                ?GLO:Select35{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select35{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:5)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?GLO:Select35{prop:width} = 64
                            if taf:restrictlength
                                ?GLO:Select35{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select35{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:5)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 6
                if hide# = 0
                    unhide(?jobe:TraFaultCode6:prompt)
                    unhide(?GLO:Select36)
                    ?jobe:TraFaultCode6:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?GLO:Select36{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?GLO:Select36{prop:text} = '@d6b'
                            ?GLO:Select36{prop:width} = 64
                            unhide(?popcalendar:18)

                        of 'STRING'
                            ?GLO:Select36{prop:width} = 124
                            if taf:restrictlength
                                ?GLO:Select36{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select36{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:6)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?GLO:Select36{prop:width} = 64
                            if taf:restrictlength
                                ?GLO:Select36{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select36{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:6)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 7
                if hide# = 0
                    unhide(?jobe:TraFaultCode7:prompt)
                    unhide(?GLO:Select37)
                    ?jobe:TraFaultCode7:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?GLO:Select37{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?GLO:Select37{prop:text} = '@d6b'
                            ?GLO:Select37{prop:width} = 64
                            unhide(?popcalendar:19)

                        of 'STRING'
                            ?GLO:Select37{prop:width} = 124
                            if taf:restrictlength
                                ?GLO:Select37{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select37{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:7)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?GLO:Select37{prop:width} = 64
                            if taf:restrictlength
                                ?GLO:Select37{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select37{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:7)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0

            Of 8
                if hide# = 0
                    unhide(?jobe:TraFaultCode8:prompt)
                    unhide(?GLO:Select38)
                    ?jobe:TraFaultCode8:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?GLO:Select38{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?GLO:Select38{prop:text} = '@d6b'
                            ?GLO:Select38{prop:width} = 64
                            unhide(?popcalendar:20)

                        of 'STRING'
                            ?GLO:Select38{prop:width} = 124
                            if taf:restrictlength
                                ?GLO:Select38{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select38{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:8)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?GLO:Select38{prop:width} = 64
                            if taf:restrictlength
                                ?GLO:Select38{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select38{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:8)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 9
                if hide# = 0
                    unhide(?jobe:TraFaultCode9:prompt)
                    unhide(?GLO:Select39)
                    ?jobe:TraFaultCode9:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?GLO:Select39{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?GLO:Select39{prop:text} = '@d6b'
                            ?GLO:Select39{prop:width} = 64
                            unhide(?popcalendar:21)

                        of 'STRING'
                            ?GLO:Select39{prop:width} = 124
                            if taf:restrictlength
                                ?GLO:Select39{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select39{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:9)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?GLO:Select39{prop:width} = 64
                            if taf:restrictlength
                                ?GLO:Select39{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select39{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:9)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 10
                if hide# = 0
                    unhide(?jobe:TraFaultCode10:prompt)
                    unhide(?GLO:Select40)
                    ?jobe:TraFaultCode10:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?GLO:Select40{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?GLO:Select40{prop:text} = '@d6b'
                            ?GLO:Select40{prop:width} = 64
                            unhide(?popcalendar:22)

                        of 'STRING'
                            ?GLO:Select40{prop:width} = 124
                            if taf:restrictlength
                                ?GLO:Select40{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select40{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:10)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?GLO:Select40{prop:width} = 64
                            if taf:restrictlength
                                ?GLO:Select40{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select40{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:10)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 11
                if hide# = 0
                    unhide(?jobe:TraFaultCode11:prompt)
                    unhide(?GLO:Select41)
                    ?jobe:TraFaultCode11:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?GLO:Select41{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?GLO:Select41{prop:text} = '@d6b'
                            ?GLO:Select41{prop:width} = 64
                            unhide(?popcalendar:23)

                        of 'STRING'
                            ?GLO:Select41{prop:width} = 124
                            if taf:restrictlength
                                ?GLO:Select41{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select41{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:11)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?GLO:Select41{prop:width} = 64
                            if taf:restrictlength
                                ?GLO:Select41{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select41{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:11)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
            Of 12
                if hide# = 0
                    unhide(?jobe:TraFaultCode12:prompt)
                    unhide(?GLO:Select42)
                    ?jobe:TraFaultCode12:prompt{prop:text} = taf:field_name
                    if req# = 1
                        ?GLO:Select42{prop:req} = 1
                    end!if req# = 1

                    case taf:field_type
                        of 'DATE'
                            ?GLO:Select42{prop:text} = '@d6b'
                            ?GLO:Select42{prop:width} = 64
                            unhide(?popcalendar:24)

                        of 'STRING'
                            ?GLO:Select42{prop:width} = 124
                            if taf:restrictlength
                                ?GLO:Select42{prop:text} = '@s' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select42{prop:text} = '@s30'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:12)
                            end!if taf:lookup = 'YES'
                        of 'NUMBER'
                            ?GLO:Select42{prop:width} = 64
                            if taf:restrictlength
                                ?GLO:Select42{prop:text} = '@n' & taf:lengthto
                            else!if taf:restrictlength
                                ?GLO:Select42{prop:text} = '@n9'
                            end!if taf:restrictlength
                            if taf:lookup = 'YES'
                                unhide(?tralookup:12)
                            end!if taf:lookup = 'YES'
                    end!case taf:field_type
                end!if hide# = 0
        End
    end !loop
    !access:trafault.restorefile(save_taf_id)
    setcursor()
PutTradeNotes       Routine
    access:jobnotes.clearkey(jbn:refnumberkey)
    jbn:refnumber = job:ref_number
    if access:jobnotes.tryfetch(jbn:refnumberkey)
        access:jobnotes.primerecord()
        access:jobnotes.tryinsert()
        access:jobnotes.clearkey(jbn:refnumberkey)
        jbn:refnumber   = job:ref_number
        access:jobnotes.tryfetch(jbn:refnumberkey)
    end!if access:jobnotes.tryfetch(jbn:refnumberkey)

    if taf:replicatefault = 'YES'
        if jbn:fault_description = ''
            jbn:fault_description = clip(tfo:description)
        else!if jbn:fault_description = ''
            jbn:fault_description = clip(jbn:fault_description) & '<13,10>' & clip(tfo:description)
        end!if jbn:fault_description = ''
    end!if mfo:replicatefault = 'YES'
    if taf:replicateinvoice = 'YES'
        if jbn:invoice_text = ''
            jbn:invoice_text = clip(tfo:description)
        else!if jbn:fault_description = ''
            jbn:invoice_text = clip(jbn:invoice_text) & '<13,10>' & clip(tfo:description)
        end!if jbn:fault_description = ''
    end!if mfo:replicatefault = 'YES'

    access:jobnotes.update()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Fault_Codes_Temp_Window',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Fault_Codes_Temp_Window',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select1:Prompt;  SolaceCtrlName = '?glo:select1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select13;  SolaceCtrlName = '?GLO:Select13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select13:2;  SolaceCtrlName = '?glo:select13:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup;  SolaceCtrlName = '?Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select14;  SolaceCtrlName = '?GLO:Select14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select14:2;  SolaceCtrlName = '?glo:select14:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:2;  SolaceCtrlName = '?Lookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select3:Prompt;  SolaceCtrlName = '?glo:select3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select15;  SolaceCtrlName = '?GLO:Select15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select15:2;  SolaceCtrlName = '?glo:select15:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:3;  SolaceCtrlName = '?Lookup:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select4:Prompt;  SolaceCtrlName = '?glo:select4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select16;  SolaceCtrlName = '?GLO:Select16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select16:2;  SolaceCtrlName = '?glo:select16:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:4;  SolaceCtrlName = '?Lookup:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select5:Prompt;  SolaceCtrlName = '?glo:select5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select17;  SolaceCtrlName = '?GLO:Select17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select17:2;  SolaceCtrlName = '?glo:select17:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:5;  SolaceCtrlName = '?Lookup:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select6:Prompt;  SolaceCtrlName = '?glo:select6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select18;  SolaceCtrlName = '?GLO:Select18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select18:2;  SolaceCtrlName = '?glo:select18:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:6;  SolaceCtrlName = '?Lookup:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select7:Prompt;  SolaceCtrlName = '?glo:select7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select19;  SolaceCtrlName = '?GLO:Select19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select19:2;  SolaceCtrlName = '?glo:select19:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:7;  SolaceCtrlName = '?Lookup:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select8:Prompt;  SolaceCtrlName = '?glo:select8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select20;  SolaceCtrlName = '?GLO:Select20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select20:2;  SolaceCtrlName = '?glo:select20:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:8;  SolaceCtrlName = '?Lookup:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select9:Prompt;  SolaceCtrlName = '?glo:select9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select21;  SolaceCtrlName = '?GLO:Select21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select21:2;  SolaceCtrlName = '?glo:select21:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:9;  SolaceCtrlName = '?Lookup:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select10:Prompt;  SolaceCtrlName = '?glo:select10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select22;  SolaceCtrlName = '?GLO:Select22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select22:2;  SolaceCtrlName = '?glo:select22:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:10;  SolaceCtrlName = '?Lookup:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select11:Prompt;  SolaceCtrlName = '?glo:select11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select23;  SolaceCtrlName = '?GLO:Select23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select23:2;  SolaceCtrlName = '?glo:select23:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:11;  SolaceCtrlName = '?Lookup:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select12:Prompt;  SolaceCtrlName = '?glo:select12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select24;  SolaceCtrlName = '?GLO:Select24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select24:2;  SolaceCtrlName = '?glo:select24:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:12;  SolaceCtrlName = '?Lookup:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode1:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select31;  SolaceCtrlName = '?GLO:Select31';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:1;  SolaceCtrlName = '?tralookup:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:13;  SolaceCtrlName = '?PopCalendar:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode2:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select32;  SolaceCtrlName = '?GLO:Select32';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode3:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select33;  SolaceCtrlName = '?GLO:Select33';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode4:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select34;  SolaceCtrlName = '?GLO:Select34';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:4;  SolaceCtrlName = '?tralookup:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:16;  SolaceCtrlName = '?PopCalendar:16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode5:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select35;  SolaceCtrlName = '?GLO:Select35';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:5;  SolaceCtrlName = '?tralookup:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:17;  SolaceCtrlName = '?PopCalendar:17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode6:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select36;  SolaceCtrlName = '?GLO:Select36';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:2;  SolaceCtrlName = '?tralookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:14;  SolaceCtrlName = '?PopCalendar:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:3;  SolaceCtrlName = '?tralookup:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:15;  SolaceCtrlName = '?PopCalendar:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode7:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select37;  SolaceCtrlName = '?GLO:Select37';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:7;  SolaceCtrlName = '?tralookup:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:19;  SolaceCtrlName = '?PopCalendar:19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode8:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select38;  SolaceCtrlName = '?GLO:Select38';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:8;  SolaceCtrlName = '?tralookup:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:20;  SolaceCtrlName = '?PopCalendar:20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode9:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select39;  SolaceCtrlName = '?GLO:Select39';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode10:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select40;  SolaceCtrlName = '?GLO:Select40';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode11:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select41;  SolaceCtrlName = '?GLO:Select41';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOBE:TraFaultCode12:Prompt;  SolaceCtrlName = '?JOBE:TraFaultCode12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select42;  SolaceCtrlName = '?GLO:Select42';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:9;  SolaceCtrlName = '?tralookup:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:21;  SolaceCtrlName = '?PopCalendar:21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:11;  SolaceCtrlName = '?tralookup:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:23;  SolaceCtrlName = '?PopCalendar:23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:6;  SolaceCtrlName = '?tralookup:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:18;  SolaceCtrlName = '?PopCalendar:18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:10;  SolaceCtrlName = '?tralookup:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:22;  SolaceCtrlName = '?PopCalendar:22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tralookup:12;  SolaceCtrlName = '?tralookup:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:24;  SolaceCtrlName = '?PopCalendar:24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Fault_Codes_Temp_Window')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Fault_Codes_Temp_Window')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?glo:select1:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:REPTYDEF.Open
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAULT.UseFile
  Access:JOBSE.UseFile
  Access:TRAFAULO.UseFile
  Access:TRAFAULT.UseFile
  Access:JOBNOTES.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ?glo:select13:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select14:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select15:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select16:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select17:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select18:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select19:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select20:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select21:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select22:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select23:2{Prop:Alrt,255} = MouseLeft2
  ?glo:select24:2{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select31{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select32{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select33{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select34{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select35{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select36{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select37{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select38{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select39{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select40{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select41{Prop:Alrt,255} = MouseLeft2
  ?GLO:Select42{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:REPTYDEF.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Fault_Codes_Temp_Window',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?GLO:Select13
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select13, Accepted)
      If glo:select13 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 1
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 1
                  mfo:field        = glo:select13
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 1
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select13 = mfo:field
                      else
                          glo:select13 = ''
                          select(?-1)
                      end
                      display(?glo:select13)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select13, Accepted)
    OF ?Lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 1
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select13 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select13 = TINCALENDARStyle1(GLO:Select13)
          Display(?glo:select13:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?GLO:Select14
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select14, Accepted)
      If glo:select14 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 2
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 2
                  mfo:field        = glo:select14
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 2
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select14 = mfo:field
                      else
                          glo:select14 = ''
                          select(?-1)
                      end
                      display(?glo:select14)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select14, Accepted)
    OF ?Lookup:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 2
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select14 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select14 = TINCALENDARStyle1(GLO:Select14)
          Display(?glo:select14:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?GLO:Select15
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select15, Accepted)
      If glo:select15 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 3
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 3
                  mfo:field        = glo:select15
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 3
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select15 = mfo:field
                      else
                          glo:select15 = ''
                          select(?-1)
                      end
                      display(?glo:select15)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select15, Accepted)
    OF ?Lookup:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 3
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select15 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select15 = TINCALENDARStyle1(GLO:Select15)
          Display(?glo:select15:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?GLO:Select16
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select16, Accepted)
      If glo:select16 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 4
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 4
                  mfo:field        = glo:select16
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 4
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select16 = mfo:field
                      else
                          glo:select16 = ''
                          select(?-1)
                      end
                      display(?glo:select16)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select16, Accepted)
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select16 = TINCALENDARStyle1(GLO:Select16)
          Display(?glo:select16:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 4
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select16 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
    OF ?GLO:Select17
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select17, Accepted)
      If glo:select17 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 5
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 5
                  mfo:field        = glo:select17
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 5
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select17 = mfo:field
                      else
                          glo:select17 = ''
                          select(?-1)
                      end
                      display(?glo:select17)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select17, Accepted)
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select17 = TINCALENDARStyle1(GLO:Select17)
          Display(?glo:select17:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 5
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select17 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
    OF ?GLO:Select18
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select18, Accepted)
      If glo:select18 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 6
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 6
                  mfo:field        = glo:select18
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 6
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select18 = mfo:field
                      else
                          glo:select18 = ''
                          select(?-1)
                      end
                      display(?glo:select18)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select18, Accepted)
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select18 = TINCALENDARStyle1(GLO:Select18)
          Display(?glo:select18:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 6
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select18 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
    OF ?GLO:Select19
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select19, Accepted)
      If glo:select19 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 7
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 7
                  mfo:field        = glo:select19
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 7
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select19 = mfo:field
                      else
                          glo:select19 = ''
                          select(?-1)
                      end
                      display(?glo:select19)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select19, Accepted)
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select19 = TINCALENDARStyle1(GLO:Select19)
          Display(?glo:select19:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 7
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select19 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
    OF ?GLO:Select20
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select20, Accepted)
      If glo:select20 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 8
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 8
                  mfo:field        = glo:select20
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 8
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select20 = mfo:field
                      else
                          glo:select20 = ''
                          select(?-1)
                      end
                      display(?glo:select20)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select20, Accepted)
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select20 = TINCALENDARStyle1(GLO:Select20)
          Display(?glo:select20:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 8
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select20 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
    OF ?GLO:Select21
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select21, Accepted)
      If glo:select21 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 9
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 9
                  mfo:field        = glo:select21
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 9
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select21 = mfo:field
                      else
                          glo:select21 = ''
                          select(?-1)
                      end
                      display(?glo:select21)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select21, Accepted)
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select21 = TINCALENDARStyle1(GLO:Select21)
          Display(?glo:select21:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 9
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select21 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
    OF ?GLO:Select22
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select22, Accepted)
      If glo:select22<> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 10
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 10
                  mfo:field        = job:fault_code1
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 10
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select22= mfo:field
                      else
                          glo:select22= ''
                          select(?-1)
                      end
                      display(?glo:select22)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select22, Accepted)
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select22 = TINCALENDARStyle1(GLO:Select22)
          Display(?glo:select22:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 10
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select22 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
    OF ?GLO:Select23
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select23, Accepted)
      If glo:select23<> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 11
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 11
                  mfo:field        = glo:select23
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 11
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select23= mfo:field
                      else
                          glo:select23= ''
                          select(?-1)
                      end
                      display(?glo:select23)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select23, Accepted)
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select23 = TINCALENDARStyle1(GLO:Select23)
          Display(?glo:select23:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 11
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select23 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
    OF ?GLO:Select24
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select24, Accepted)
      If glo:select24 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = glo:select4
          maf:field_number = 12
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = glo:select4
                  mfo:field_number = 12
                  mfo:field        = glo:select24
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select4
                      glo:select2  = 12
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          glo:select24 = mfo:field
                      else
                          glo:select24 = ''
                          select(?-1)
                      end
                      display(?glo:select24)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If job:fault_code1 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select24, Accepted)
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select24 = TINCALENDARStyle1(GLO:Select24)
          Display(?glo:select24:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select4
      glo:select2  = 12
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          glo:select24 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
    OF ?GLO:Select31
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select31, Accepted)
      If glo:select31 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select31
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select31 = tfo:field
                      else
                          glo:select31 = ''
                          select(?-1)
                      end
                      display(?glo:select31)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select31, Accepted)
    OF ?tralookup:1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:1, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 1
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select31 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:1, Accepted)
    OF ?PopCalendar:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select31 = TINCALENDARStyle1(GLO:Select31)
          Display(?GLO:Select31)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?GLO:Select32
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select32, Accepted)
      If glo:select32 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select32
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select32 = tfo:field
                      else
                          glo:select32 = ''
                          select(?-1)
                      end
                      display(?glo:select32)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select32, Accepted)
    OF ?GLO:Select33
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select33, Accepted)
      If glo:select33 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select33
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select33 = tfo:field
                      else
                          glo:select33 = ''
                          select(?-1)
                      end
                      display(?glo:select33)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select33, Accepted)
    OF ?GLO:Select34
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select34, Accepted)
      If glo:select34 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select34
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select34 = tfo:field
                      else
                          glo:select34 = ''
                          select(?-1)
                      end
                      display(?glo:select34)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select34, Accepted)
    OF ?tralookup:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:4, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 4
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select34 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:4, Accepted)
    OF ?PopCalendar:16
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select34 = TINCALENDARStyle1(GLO:Select34)
          Display(?GLO:Select34)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?GLO:Select35
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select35, Accepted)
      If glo:select35 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select35
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select35 = tfo:field
                      else
                          glo:select35 = ''
                          select(?-1)
                      end
                      display(?glo:select35)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select35, Accepted)
    OF ?tralookup:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:5, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 5
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select35 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:5, Accepted)
    OF ?PopCalendar:17
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select35 = TINCALENDARStyle1(GLO:Select35)
          Display(?GLO:Select35)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?GLO:Select36
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select36, Accepted)
      If glo:select36 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select36
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select36 = tfo:field
                      else
                          glo:select36 = ''
                          select(?-1)
                      end
                      display(?glo:select36)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select36, Accepted)
    OF ?tralookup:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:2, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 2
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select32 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:2, Accepted)
    OF ?PopCalendar:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select32 = TINCALENDARStyle1(GLO:Select32)
          Display(?GLO:Select32)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:3, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 3
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select33 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:3, Accepted)
    OF ?PopCalendar:15
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select33 = TINCALENDARStyle1(GLO:Select33)
          Display(?GLO:Select33)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?GLO:Select37
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select37, Accepted)
      If glo:select37 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select37
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select37 = tfo:field
                      else
                          glo:select37 = ''
                          select(?-1)
                      end
                      display(?glo:select37)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select37, Accepted)
    OF ?tralookup:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:7, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 7
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select37 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:7, Accepted)
    OF ?PopCalendar:19
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select37 = TINCALENDARStyle1(GLO:Select37)
          Display(?GLO:Select37)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?GLO:Select38
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select38, Accepted)
      If glo:select38 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select38
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select38 = tfo:field
                      else
                          glo:select38 = ''
                          select(?-1)
                      end
                      display(?glo:select38)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select38, Accepted)
    OF ?tralookup:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:8, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 8
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select38 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:8, Accepted)
    OF ?PopCalendar:20
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select38 = TINCALENDARStyle1(GLO:Select38)
          Display(?GLO:Select38)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?GLO:Select39
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select39, Accepted)
      If glo:select39 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select39
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select39 = tfo:field
                      else
                          glo:select39 = ''
                          select(?-1)
                      end
                      display(?glo:select39)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select39, Accepted)
    OF ?GLO:Select40
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select40, Accepted)
      If glo:select40 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select40
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select40 = tfo:field
                      else
                          glo:select40 = ''
                          select(?-1)
                      end
                      display(?glo:select40)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select40, Accepted)
    OF ?GLO:Select41
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select41, Accepted)
      If glo:select41 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select41
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select41 = tfo:field
                      else
                          glo:select41 = ''
                          select(?-1)
                      end
                      display(?glo:select41)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select41, Accepted)
    OF ?GLO:Select42
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select42, Accepted)
      If glo:select42 <> ''
          access:manfault.clearkey(maf:field_number_key)
          taf:AccountNumber = glo:select43
          taf:Field_Number = 1
          if access:trafault.fetch(taf:Field_Number_Key) = Level:Benign
              If taf:Force_Lookup = 'YES'
                  access:trafaulo.clearkey(tfo:field_key)
                  tfo:AccountNumber = glo:select43
                  tfo:Field_Number = 1
                  tfo:Field        = glo:select42
                  if access:trafaulo.fetch(tfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = glo:select43
                      glo:select2  = 1
                      glo:select3  = ''
      
                      PickTradeFaultCodes
                      if globalresponse = requestcompleted
                          glo:select42 = tfo:field
                      else
                          glo:select42 = ''
                          select(?-1)
                      end
                      display(?glo:select42)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If glo:select13 <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select42, Accepted)
    OF ?tralookup:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:9, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 9
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select39 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:9, Accepted)
    OF ?PopCalendar:21
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select39 = TINCALENDARStyle1(GLO:Select39)
          Display(?GLO:Select39)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:11, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 11
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select41 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:11, Accepted)
    OF ?PopCalendar:23
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select41 = TINCALENDARStyle1(GLO:Select41)
          Display(?GLO:Select41)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:6, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 6
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select36 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:6, Accepted)
    OF ?PopCalendar:18
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select36 = TINCALENDARStyle1(GLO:Select36)
          Display(?GLO:Select36)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:10, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 10
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select40 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:10, Accepted)
    OF ?PopCalendar:22
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select40 = TINCALENDARStyle1(GLO:Select40)
          Display(?GLO:Select40)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tralookup:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:12, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select43
      glo:select2  = 12
      glo:select3  = ''
      PickTradeFaultCodes
      if globalresponse = requestcompleted
          glo:select42 = tfo:Field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tralookup:12, Accepted)
    OF ?PopCalendar:24
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select42 = TINCALENDARStyle1(GLO:Select42)
          Display(?GLO:Select42)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Fault_Codes_Temp_Window')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?glo:select13:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?glo:select14:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?glo:select15:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?glo:select16:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?glo:select17:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?glo:select18:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  OF ?glo:select19:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?glo:select20:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?glo:select21:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?glo:select22:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?glo:select23:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?glo:select24:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  OF ?GLO:Select31
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:13)
      CYCLE
    END
  OF ?GLO:Select32
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:14)
      CYCLE
    END
  OF ?GLO:Select33
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:15)
      CYCLE
    END
  OF ?GLO:Select34
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:16)
      CYCLE
    END
  OF ?GLO:Select35
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:17)
      CYCLE
    END
  OF ?GLO:Select36
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:18)
      CYCLE
    END
  OF ?GLO:Select37
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:19)
      CYCLE
    END
  OF ?GLO:Select38
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:20)
      CYCLE
    END
  OF ?GLO:Select39
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:21)
      CYCLE
    END
  OF ?GLO:Select40
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:22)
      CYCLE
    END
  OF ?GLO:Select41
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:23)
      CYCLE
    END
  OF ?GLO:Select42
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:24)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do Fault_Coding
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

