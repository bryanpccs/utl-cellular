

   MEMBER('celrapvo.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA011.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA012.INC'),ONCE        !Req'd for module callout resolution
                     END


Voda_ONR_Process PROCEDURE                            !Generated from procedure template - Window

IsOnTest             BYTE(0)
MSN_Field_Mask       STRING(30)
technician_string    STRING(60)
TotalPartsCost       REAL
tmpCheckChargeableParts BYTE
tmp_RF_Board_IMEI    STRING(20)
IsPass               BYTE(0)
IsFail               BYTE(0)
QA_Type              STRING(3)
handset_type_temp    STRING(30)
Failure_Reason       STRING(30)
ActivePanel          SHORT(0)
RecordState          SHORT(0)
Temp_IMEI_Number     STRING(20)
Temp_Usercode        STRING(3)
savejobs             USHORT
savejobse            USHORT
savejobnotes         USHORT
save_manfault        USHORT
save_manfaulo        USHORT
save_trafault        USHORT
save_trafaulo        USHORT
save_map_id          USHORT
save_jac_id          USHORT
save_lac_id          USHORT
save_par_id          USHORT
save_par_id2         USHORT
save_wpr_id          USHORT
SaveBookmark         USHORT
warpartpntstore      USHORT
partpntstore         USHORT
old_prompt           STRING(30),DIM(12)
jfc_prompt           STRING(30),DIM(12)
jfc_value            STRING(30),DIM(12)
jfc_force_lookup     SHORT,DIM(12)
tfc_prompt           STRING(30),DIM(12)
tfc_value            STRING(30),DIM(12)
tfc_force_lookup     SHORT,DIM(12)
save_charge_type     STRING(30)
save_repair_type     STRING(30)
tmp:End_Days         LONG
tmp:End_Hours        LONG
tmp:PreviousStatus   STRING(30)
PartFaultCodes       GROUP,PRE(pfc)
FaultCode1           STRING(30)
FaultCode2           STRING(30)
FaultCode3           STRING(30)
FaultCode4           STRING(30)
FaultCode5           STRING(30)
FaultCode6           STRING(30)
FaultCode7           STRING(30)
FaultCode8           STRING(30)
FaultCode9           STRING(30)
FaultCode10          STRING(30)
FaultCode11          STRING(30)
FaultCode12          STRING(30)
                     END
temprec              GROUP,PRE(temp)
Account_Number       STRING(15)
Order_Number         STRING(30)
job_reference        STRING(8)
Charge_Part_Number   STRING(30)
Warranty_Part_Number STRING(30)
charge_type          STRING(30)
repair_type          STRING(30)
warranty_charge_type STRING(30)
repair_type_warranty STRING(30)
                     END
ONR_Defaults         GROUP,PRE(ONR)
location             STRING(30)
account_number       STRING(15)
transit_type         STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW4::View:Browse    VIEW(PARTS)
                       PROJECT(par:Part_Number)
                       PROJECT(par:Description)
                       PROJECT(par:Quantity)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
par:Part_Number        LIKE(par:Part_Number)          !List box control field - type derived from field
par:Part_Number_NormalFG LONG                         !Normal forground color
par:Part_Number_NormalBG LONG                         !Normal background color
par:Part_Number_SelectedFG LONG                       !Selected forground color
par:Part_Number_SelectedBG LONG                       !Selected background color
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Description_NormalFG LONG                         !Normal forground color
par:Description_NormalBG LONG                         !Normal background color
par:Description_SelectedFG LONG                       !Selected forground color
par:Description_SelectedBG LONG                       !Selected background color
par:Quantity           LIKE(par:Quantity)             !List box control field - type derived from field
par:Quantity_NormalFG  LONG                           !Normal forground color
par:Quantity_NormalBG  LONG                           !Normal background color
par:Quantity_SelectedFG LONG                          !Selected forground color
par:Quantity_SelectedBG LONG                          !Selected background color
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW19::View:Browse   VIEW(WARPARTS)
                       PROJECT(wpr:Part_Number)
                       PROJECT(wpr:Description)
                       PROJECT(wpr:Quantity)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
wpr:Part_Number        LIKE(wpr:Part_Number)          !List box control field - type derived from field
wpr:Part_Number_NormalFG LONG                         !Normal forground color
wpr:Part_Number_NormalBG LONG                         !Normal background color
wpr:Part_Number_SelectedFG LONG                       !Selected forground color
wpr:Part_Number_SelectedBG LONG                       !Selected background color
wpr:Description        LIKE(wpr:Description)          !List box control field - type derived from field
wpr:Description_NormalFG LONG                         !Normal forground color
wpr:Description_NormalBG LONG                         !Normal background color
wpr:Description_SelectedFG LONG                       !Selected forground color
wpr:Description_SelectedBG LONG                       !Selected background color
wpr:Quantity           LIKE(wpr:Quantity)             !List box control field - type derived from field
wpr:Quantity_NormalFG  LONG                           !Normal forground color
wpr:Quantity_NormalBG  LONG                           !Normal background color
wpr:Quantity_SelectedFG LONG                          !Selected forground color
wpr:Quantity_SelectedBG LONG                          !Selected background color
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Rapid Repair Process'),AT(,,636,308),FONT('Tahoma',8,,),CENTER,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(156,4,476,252),USE(?TestAndRepairSheet),WIZARD,SPREAD
                         TAB,USE(?TestAndRepairTab)
                           TEXT,AT(376,24,120,44),USE(jbn:Fault_Description),VSCROLL,UPR
                           PROMPT('Fault Description'),AT(376,12),USE(?FaultDescriptionPrompt),TRN
                           BUTTON('...'),AT(432,12,10,10),USE(?PickFaultDescriptionButton),SKIP,FLAT,LEFT,ICON('list3.ico')
                           PROMPT('Engineer''s Notes'),AT(376,72),USE(?EngineersNotesPrompt),TRN
                           BUTTON('...'),AT(432,72,10,10),USE(?PickEngineersNotesButton),SKIP,FLAT,LEFT,ICON('list3.ico')
                           PROMPT('Invoice Text'),AT(504,72),USE(?InvoiceTextPrompt),TRN
                           BUTTON('...'),AT(544,72,10,10),USE(?PickInvoiceTextButton),SKIP,FLAT,LEFT,ICON('list3.ico')
                           TEXT,AT(376,84,120,44),USE(jbn:Engineers_Notes),VSCROLL,UPR
                           TEXT,AT(504,84,120,44),USE(jbn:Invoice_Text),VSCROLL
                           CHECK('Chargeable Job'),AT(161,192),USE(job:Chargeable_Job),TRN,LEFT,VALUE('YES','NO')
                           ENTRY(@s30),AT(232,192,120,10),USE(temp:charge_type),UPR
                           BUTTON('...'),AT(356,192,10,10),USE(?ChargeButton),SKIP,FLAT,LEFT,ICON('list3.ico')
                           ENTRY(@s30),AT(232,228,120,10),USE(temp:repair_type_warranty),UPR
                           BUTTON('...'),AT(356,228,10,10),USE(?WarrantyTypeButton),SKIP,FLAT,LEFT,ICON('list3.ico')
                           CHECK('On Test'),AT(508,58),USE(IsOnTest),TRN,VALUE('1','0')
                           CHECK('Pass'),AT(556,58),USE(IsPass),TRN,VALUE('1','0')
                           CHECK('Fail'),AT(600,58),USE(IsFail),TRN,VALUE('1','0')
                           GROUP('QA'),AT(504,12,124,36),USE(?QAGroup),BOXED
                             BUTTON('QA &Fail [F10]'),AT(568,28,56,16),USE(?QAFailButton),FLAT,LEFT,KEY(F10Key),ICON('cancel.gif')
                             BUTTON('QA &Pass [F9]'),AT(508,28,56,16),USE(?QAPassButton),FLAT,LEFT,KEY(F9Key),ICON('ok.gif')
                           END
                           PROMPT('Test & Repair'),AT(164,6),USE(?Prompt23),TRN,FONT(,,,FONT:bold)
                           ENTRY(@s30),AT(232,216,120,10),USE(temp:warranty_charge_type),UPR
                           PROMPT('Charge Type'),AT(172,204),USE(?ChargeTypePrompt),TRN
                           ENTRY(@s30),AT(232,204,120,10),USE(temp:repair_type),UPR
                           BUTTON('...'),AT(356,204,10,10),USE(?ChargeTypeButton),SKIP,FLAT,LEFT,ICON('list3.ico')
                           CHECK('Warranty Job'),AT(168,216),USE(job:Warranty_Job),TRN,LEFT,VALUE('YES','NO')
                           BUTTON('...'),AT(356,216,10,10),USE(?warrantyButton),SKIP,FLAT,LEFT,ICON('list3.ico')
                           PROMPT('Warranty Type'),AT(168,228),USE(?WarrantyTypePrompt),TRN
                           SHEET,AT(160,16,212,168),USE(?Sheet4),SPREAD
                             TAB('Manufacturer''s Fault Codes'),USE(?MFCTab)
                               STRING(@s20),AT(164,36),USE(jfc_prompt[1]),TRN,HIDE
                               ENTRY(@s30),AT(248,36,64,10),USE(jfc_value[1]),HIDE
                               BUTTON('...'),AT(316,36,10,10),USE(?PopCalendar),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,36,10,10),USE(?JFC1_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,48),USE(jfc_prompt[2]),TRN,HIDE
                               ENTRY(@s30),AT(248,48,64,10),USE(jfc_value[2]),HIDE
                               BUTTON('...'),AT(316,48,10,10),USE(?PopCalendar:2),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,48,10,10),USE(?JFC2_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,60),USE(jfc_prompt[3]),TRN,HIDE
                               ENTRY(@s30),AT(248,60,64,10),USE(jfc_value[3]),HIDE
                               BUTTON('...'),AT(316,60,10,10),USE(?PopCalendar:3),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,60,10,10),USE(?JFC3_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,72),USE(jfc_prompt[4]),TRN,HIDE
                               ENTRY(@s30),AT(248,72,64,10),USE(jfc_value[4]),HIDE
                               BUTTON('...'),AT(316,72,10,10),USE(?PopCalendar:4),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,72,10,10),USE(?JFC4_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,84),USE(jfc_prompt[5]),TRN,HIDE
                               ENTRY(@s30),AT(248,84,64,10),USE(jfc_value[5]),HIDE
                               BUTTON('...'),AT(316,84,10,10),USE(?PopCalendar:5),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,84,10,10),USE(?JFC5_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,96),USE(jfc_prompt[6]),TRN,HIDE
                               ENTRY(@s30),AT(248,96,64,10),USE(jfc_value[6]),HIDE
                               BUTTON('...'),AT(316,96,10,10),USE(?PopCalendar:6),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,96,10,10),USE(?JFC6_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,108),USE(jfc_prompt[7]),TRN,HIDE
                               ENTRY(@s30),AT(248,108,64,10),USE(jfc_value[7]),HIDE
                               BUTTON('...'),AT(316,108,10,10),USE(?PopCalendar:7),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,108,10,10),USE(?JFC7_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,120),USE(jfc_prompt[8]),TRN,HIDE
                               ENTRY(@s30),AT(248,120,64,10),USE(jfc_value[8]),HIDE
                               BUTTON('...'),AT(316,120,10,10),USE(?PopCalendar:8),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,120,10,10),USE(?JFC8_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,132),USE(jfc_prompt[9]),TRN,HIDE
                               ENTRY(@s30),AT(248,132,64,10),USE(jfc_value[9]),HIDE
                               BUTTON('...'),AT(316,132,10,10),USE(?PopCalendar:9),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,132,10,10),USE(?JFC9_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,144),USE(jfc_prompt[10]),TRN,HIDE
                               ENTRY(@s30),AT(248,144,64,10),USE(jfc_value[10]),HIDE
                               BUTTON('...'),AT(316,144,10,10),USE(?PopCalendar:10),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,144,10,10),USE(?JFC10_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,156),USE(jfc_prompt[11]),TRN,HIDE
                               ENTRY(@s30),AT(248,156,64,10),USE(jfc_value[11]),HIDE
                               BUTTON('...'),AT(316,156,10,10),USE(?PopCalendar:11),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,156,10,10),USE(?JFC11_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,168),USE(jfc_prompt[12]),TRN,HIDE
                               ENTRY(@s30),AT(248,168,64,10),USE(jfc_value[12]),HIDE
                               BUTTON('...'),AT(316,168,10,10),USE(?PopCalendar:12),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,168,10,10),USE(?JFC12_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                             END
                             TAB('Trade Fault Codes'),USE(?TFCTab)
                               BUTTON('...'),AT(356,36,10,10),USE(?TFC1_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               BUTTON('...'),AT(316,168,10,10),USE(?PopCalendar:24),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               ENTRY(@s30),AT(248,36,64,10),USE(tfc_value[1]),HIDE
                               BUTTON('...'),AT(316,36,10,10),USE(?PopCalendar:13),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               STRING(@s20),AT(164,36),USE(tfc_prompt[1]),TRN,HIDE
                               STRING(@s20),AT(164,48),USE(tfc_prompt[2]),TRN,HIDE
                               ENTRY(@s30),AT(248,48,64,10),USE(tfc_value[2]),HIDE
                               BUTTON('...'),AT(356,48,10,10),USE(?TFC2_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               BUTTON('...'),AT(316,48,10,10),USE(?PopCalendar:14),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               STRING(@s20),AT(164,60),USE(tfc_prompt[3]),TRN,HIDE
                               ENTRY(@s30),AT(248,60,64,10),USE(tfc_value[3]),HIDE
                               BUTTON('...'),AT(356,60,10,10),USE(?TFC3_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               BUTTON('...'),AT(316,60,10,10),USE(?PopCalendar:15),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               STRING(@s20),AT(164,120),USE(tfc_prompt[8]),TRN,HIDE
                               ENTRY(@s30),AT(248,120,64,10),USE(tfc_value[8]),HIDE
                               BUTTON('...'),AT(356,108,10,10),USE(?TFC7_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,72),USE(tfc_prompt[4]),TRN,HIDE
                               ENTRY(@s30),AT(248,72,64,10),USE(tfc_value[4]),HIDE
                               BUTTON('...'),AT(356,72,10,10),USE(?TFC4_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,132),USE(tfc_prompt[9]),TRN,HIDE
                               ENTRY(@s30),AT(248,132,64,10),USE(tfc_value[9]),HIDE
                               BUTTON('...'),AT(356,120,10,10),USE(?TFC8_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               BUTTON('...'),AT(316,132,10,10),USE(?PopCalendar:21),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(316,96,10,10),USE(?PopCalendar:18),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               STRING(@s20),AT(164,84),USE(tfc_prompt[5]),TRN,HIDE
                               ENTRY(@s30),AT(248,84,64,10),USE(tfc_value[5]),HIDE
                               BUTTON('...'),AT(356,84,10,10),USE(?TFC5_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               BUTTON('...'),AT(316,72,10,10),USE(?PopCalendar:16),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               STRING(@s20),AT(164,144),USE(tfc_prompt[10]),TRN,HIDE
                               ENTRY(@s30),AT(248,144,64,10),USE(tfc_value[10]),HIDE
                               BUTTON('...'),AT(316,144,10,10),USE(?PopCalendar:22),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(316,108,10,10),USE(?PopCalendar:19),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,132,10,10),USE(?TFC9_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               BUTTON('...'),AT(356,156,10,10),USE(?TFC11_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               BUTTON('...'),AT(316,156,10,10),USE(?PopCalendar:23),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(316,120,10,10),USE(?PopCalendar:20),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               STRING(@s20),AT(164,96),USE(tfc_prompt[6]),TRN,HIDE
                               ENTRY(@s30),AT(248,96,64,10),USE(tfc_value[6]),HIDE
                               STRING(@s20),AT(164,156),USE(tfc_prompt[11]),TRN,HIDE
                               ENTRY(@s30),AT(248,156,64,10),USE(tfc_value[11]),HIDE
                               BUTTON('...'),AT(356,144,10,10),USE(?TFC10_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,108),USE(tfc_prompt[7]),TRN,HIDE
                               ENTRY(@s30),AT(248,108,64,10),USE(tfc_value[7]),HIDE
                               BUTTON('...'),AT(316,84,10,10),USE(?PopCalendar:17),SKIP,HIDE,LEFT,ICON('calenda2.ico')
                               BUTTON('...'),AT(356,96,10,10),USE(?TFC6_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                               STRING(@s20),AT(164,168),USE(tfc_prompt[12]),TRN,HIDE
                               ENTRY(@s30),AT(248,168,64,10),USE(tfc_value[12]),HIDE
                               BUTTON('...'),AT(356,168,10,10),USE(?TFC12_Button),SKIP,HIDE,LEFT,ICON('list3.ico')
                             END
                           END
                         END
                       END
                       SHEET,AT(376,132,252,120),USE(?Sheet2),SPREAD
                         TAB('Chargeable Parts'),USE(?ChargeablePartsTab)
                           LIST,AT(380,148,244,68),USE(?List),IMM,MSG('Browsing Records'),FORMAT('78L(2)|M*~Part Number~@s30@121L(2)|M*~Description~@s30@32D(2)|M*~Quantity~L@n8@'),FROM(Queue:Browse)
                           PROMPT('Add Part No'),AT(382,220),USE(?AddPartPrompt),TRN
                           ENTRY(@s30),AT(425,220,115,11),USE(temp:Charge_Part_Number),ALRT(TabKey),ALRT(EnterKey),UPR
                           BUTTON('&Insert'),AT(381,236,58,12),USE(?PartInsert),LEFT,ICON('insert.ico')
                           BUTTON('Adjustment'),AT(441,236,58,12),USE(?PartAdjustment),LEFT,ICON('insert.ico')
                           BUTTON('&Change'),AT(501,236,58,12),USE(?PartChange),LEFT,ICON('edit.ico')
                           BUTTON('&Delete'),AT(561,236,58,12),USE(?PartDelete),LEFT,ICON('delete.ico')
                         END
                         TAB('Warranty Parts'),USE(?WarrantyPartsTab)
                           LIST,AT(380,148,244,68),USE(?List:2),IMM,MSG('Browsing Records'),FORMAT('73L(2)|M*~Part Number~@s30@105L(2)|M*~Description~@s30@32D(2)|M*~Quantity~L@n8@'),FROM(Queue:Browse:1)
                           PROMPT('Add Part No'),AT(382,220),USE(?AddWarrantyPrompt),TRN
                           ENTRY(@s30),AT(425,220,115,10),USE(temp:Warranty_Part_Number),ALRT(TabKey),ALRT(EnterKey),UPR
                           BUTTON('&Insert'),AT(381,236,58,12),USE(?WarInsert),LEFT,ICON('insert.ico')
                           BUTTON('Adjustment'),AT(441,236,58,12),USE(?WarAdjustment),LEFT,ICON('insert.ico')
                           BUTTON('&Change'),AT(501,236,58,12),USE(?WarChange),LEFT,ICON('edit.ico')
                           BUTTON('&Delete'),AT(561,236,58,12),USE(?WarDelete),LEFT,ICON('delete.ico')
                         END
                       END
                       SHEET,AT(4,4,148,252),USE(?BookInSheet),WIZARD,SPREAD
                         TAB('Tab 4'),USE(?BookInTab)
                           PROMPT('Book In'),AT(10,6),USE(?Prompt22),TRN,FONT(,,,FONT:bold)
                           PROMPT('Account'),AT(10,20),USE(?Prompt1),TRN
                           ENTRY(@s20),AT(48,20,88,10),USE(temp:Account_Number),UPR
                           BUTTON('...'),AT(138,20,10,10),USE(?PickAccountButton),SKIP,FLAT,LEFT,ICON('list3.ico')
                           PROMPT('Order No.'),AT(10,32),USE(?Prompt2),TRN
                           ENTRY(@s30),AT(48,32,88,10),USE(temp:Order_Number),ALRT(EnterKey),ALRT(TabKey),UPR
                           PROMPT('Job No'),AT(16,49),USE(?Prompt25),TRN
                           ENTRY(@s8),AT(47,49,89,11),USE(temp:job_reference),ALRT(TabKey),ALRT(EnterKey),UPR
                           PROMPT('IMEI Number'),AT(10,68),USE(?Prompt3),TRN
                           ENTRY(@s20),AT(64,68,72,10),USE(job:ESN),UPR
                           PROMPT('MSN'),AT(10,80),USE(?Prompt4),TRN
                           ENTRY(@s20),AT(64,80,72,10),USE(job:MSN),UPR
                           PROMPT('Model Number'),AT(10,92),USE(?Prompt5),TRN
                           ENTRY(@s30),AT(64,92,72,10),USE(job:Model_Number),UPR
                           BUTTON('...'),AT(138,92,10,10),USE(?PickModelButton),SKIP,FLAT,LEFT,ICON('list3.ico')
                           PROMPT('Customer Name'),AT(10,104),USE(?Prompt6),TRN
                           ENTRY(@s30),AT(64,104,72,10),USE(job:Surname),UPR
                           PROMPT('Delivery Address'),AT(10,120),USE(?Prompt7),TRN
                           ENTRY(@s30),AT(10,128,135,10),USE(job:Company_Name_Delivery),UPR
                           ENTRY(@s30),AT(10,140,135,10),USE(job:Address_Line1_Delivery),UPR
                           ENTRY(@s30),AT(10,152,135,10),USE(job:Address_Line2_Delivery),UPR
                           ENTRY(@s30),AT(10,164,135,10),USE(job:Address_Line3_Delivery),UPR
                           ENTRY(@s10),AT(10,176,72,10),USE(job:Postcode_Delivery),UPR
                           CHECK('Workshop'),AT(10,192),USE(job:Workshop),TRN,VALUE('YES','NO')
                           STRING(@d8B),AT(58,192),USE(jobe:InWorkshopDate),TRN
                           STRING(@t1b),AT(58,200),USE(jobe:InWorkshopTime),TRN
                           PROMPT('Location'),AT(10,208),USE(?Prompt8),TRN
                           ENTRY(@s30),AT(10,216,126,10),USE(job:Location),UPR
                           BUTTON('...'),AT(138,216,10,10),USE(?PickLocationButton),SKIP,FLAT,LEFT,ICON('list3.ico')
                           BUTTON('OK'),AT(48,232,56,16),USE(?BookOKButton),FLAT,LEFT,ICON('ok.gif')
                         END
                       END
                       PANEL,AT(4,260,628,44),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON,AT(227,264,24,16),USE(?AllocateCommonFaultsButton),FLAT,LEFT,KEY(F7Key),ICON('fault.gif')
                       PROMPT('Allocate Common'),AT(255,264),USE(?Prompt14),TRN
                       BUTTON,AT(195,284,24,16),USE(?ChangeStatusButton),FLAT,LEFT,KEY(F8Key),ICON('1.ico')
                       PROMPT('Faults [F7]'),AT(255,272),USE(?Prompt15),TRN
                       BUTTON('Complete Job [F10]'),AT(548,264,72,16),USE(?CompleteJobButton),TRN,FLAT,LEFT,KEY(F10Key),ICON('fixer.gif')
                       BUTTON,AT(404,264,24,16),USE(?PickButton),FLAT,ICON('pick.gif')
                       BUTTON,AT(103,284,24,16),USE(?ViewJobButton),FLAT,KEY(F6Key),ICON('spy.gif')
                       BUTTON,AT(315,264,24,16),USE(?RemovePartsButton),FLAT,LEFT,ICON('bin.gif')
                       PROMPT('Remove Parts'),AT(343,264),USE(?Prompt16),TRN
                       PROMPT('Serial Number'),AT(16,264),USE(?SerialNumberPrompt),TRN
                       ENTRY(@s20),AT(73,264,135,10),USE(Temp_IMEI_Number),ALRT(EnterKey),ALRT(TabKey),UPR
                       BUTTON,AT(15,284,24,16),USE(?AllocateJobButton),FLAT,KEY(F5Key),ICON('audit.gif')
                       PROMPT('Allocate Job [F5]'),AT(43,288),USE(?Prompt18),TRN
                       PROMPT('View Job [F6]'),AT(131,288),USE(?Prompt19),TRN
                       PROMPT('Print'),AT(396,284),USE(?Prompt30),TRN
                       BUTTON('CompletedLabelButton'),AT(368,284,24,16),USE(?CompletedLabelButton),FLAT,LEFT,ICON(ICON:Print)
                       BUTTON('Cancel'),AT(568,284,56,16),USE(?CancelButton),FLAT,LEFT,ICON('cancel.gif')
                       PROMPT('[F8]'),AT(223,292),USE(?Prompt21),TRN
                       BUTTON,AT(284,284,24,16),USE(?BookinLabelButton),FLAT,LEFT,ICON(ICON:Print)
                       PROMPT('Book In Label'),AT(312,292),USE(?Prompt29),TRN
                       PROMPT('Completed Label'),AT(396,292),USE(?Prompt31),TRN
                       PROMPT('Change Status'),AT(223,284),USE(?Prompt20),TRN
                       PROMPT('Print'),AT(312,284),USE(?Prompt28),TRN
                       PROMPT('And Invoice Text'),AT(343,272),USE(?Prompt17),TRN
                       PROMPT('Print Pick Note'),AT(432,268),USE(?Prompt32),TRN
                       BUTTON('Close'),AT(508,284,56,16),USE(?OKButton),FLAT,LEFT,ICON('ok.gif')
                       PANEL,AT(548,264,72,16),USE(?JobCompletePanel),FILL(COLOR:Lime)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW19                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW19::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
    MAP
Show_Edit                   PROCEDURE(long, string, long, long, string, long, long, string, string)
Show_Edit2                  PROCEDURE(long, string, long, long, string, long, long, string, string)
Lookup_Fault_Code           PROCEDURE(long)
Lookup_Trade_Fault_Code     PROCEDURE(long)
Edit_Fault_Code             PROCEDURE(long),Byte
Edit_Trade_Fault_Code       PROCEDURE(long),Byte
LocalValidateAccessories    PROCEDURE(),Byte
LocalValidateNetwork        PROCEDURE(),Byte
LocalValidateParts          PROCEDURE(),Byte
CheckPartNo                 PROCEDURE(String,*Long),Byte
CheckMandatoryFaultCodes    PROCEDURE(String, String, Long),Byte
CheckWarrantyPartExists     PROCEDURE(String),Byte
CheckChargeablePartExists   PROCEDURE(String),Byte
Add_Chargeable              PROCEDURE(Long)
Add_Warranty                PROCEDURE(Long)
ChangeChargeableJob         PROCEDURE()
ChangeWarrantyJob           PROCEDURE()
UpdateRFBoardIMEI PROCEDURE (LONG,STRING)
    END
rs ITEMIZE(0), PRE(rs)
NoRecord        EQUATE  ! rs:NoRecord = 0
InsertRecord    EQUATE  ! rs:InsertRecord = 1
UpdateRecord    EQUATE  ! rs:UpdateRecord = 2
END

ap ITEMIZE(0), PRE(ap)
NoPanel             EQUATE  ! ap:NoPanel = 0
BookinPanel         EQUATE  ! ap:BookinPanel = 1
TestAndRepairPanel  EQUATE  ! ap:TestAndRepairPanel = 2
QAPanel             EQUATE  ! ap:QAPanel = 3
END

TempFilePath         CSTRING(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?TestAndRepairSheet{prop:Color} = 15066597
    ?TestAndRepairTab{prop:Color} = 15066597
    If ?jbn:Fault_Description{prop:ReadOnly} = True
        ?jbn:Fault_Description{prop:FontColor} = 65793
        ?jbn:Fault_Description{prop:Color} = 15066597
    Elsif ?jbn:Fault_Description{prop:Req} = True
        ?jbn:Fault_Description{prop:FontColor} = 65793
        ?jbn:Fault_Description{prop:Color} = 8454143
    Else ! If ?jbn:Fault_Description{prop:Req} = True
        ?jbn:Fault_Description{prop:FontColor} = 65793
        ?jbn:Fault_Description{prop:Color} = 16777215
    End ! If ?jbn:Fault_Description{prop:Req} = True
    ?jbn:Fault_Description{prop:Trn} = 0
    ?jbn:Fault_Description{prop:FontStyle} = font:Bold
    ?FaultDescriptionPrompt{prop:FontColor} = -1
    ?FaultDescriptionPrompt{prop:Color} = 15066597
    ?EngineersNotesPrompt{prop:FontColor} = -1
    ?EngineersNotesPrompt{prop:Color} = 15066597
    ?InvoiceTextPrompt{prop:FontColor} = -1
    ?InvoiceTextPrompt{prop:Color} = 15066597
    If ?jbn:Engineers_Notes{prop:ReadOnly} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 15066597
    Elsif ?jbn:Engineers_Notes{prop:Req} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 8454143
    Else ! If ?jbn:Engineers_Notes{prop:Req} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 16777215
    End ! If ?jbn:Engineers_Notes{prop:Req} = True
    ?jbn:Engineers_Notes{prop:Trn} = 0
    ?jbn:Engineers_Notes{prop:FontStyle} = font:Bold
    If ?jbn:Invoice_Text{prop:ReadOnly} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 15066597
    Elsif ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 8454143
    Else ! If ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 16777215
    End ! If ?jbn:Invoice_Text{prop:Req} = True
    ?jbn:Invoice_Text{prop:Trn} = 0
    ?jbn:Invoice_Text{prop:FontStyle} = font:Bold
    ?job:Chargeable_Job{prop:Font,3} = -1
    ?job:Chargeable_Job{prop:Color} = 15066597
    ?job:Chargeable_Job{prop:Trn} = 0
    If ?temp:charge_type{prop:ReadOnly} = True
        ?temp:charge_type{prop:FontColor} = 65793
        ?temp:charge_type{prop:Color} = 15066597
    Elsif ?temp:charge_type{prop:Req} = True
        ?temp:charge_type{prop:FontColor} = 65793
        ?temp:charge_type{prop:Color} = 8454143
    Else ! If ?temp:charge_type{prop:Req} = True
        ?temp:charge_type{prop:FontColor} = 65793
        ?temp:charge_type{prop:Color} = 16777215
    End ! If ?temp:charge_type{prop:Req} = True
    ?temp:charge_type{prop:Trn} = 0
    ?temp:charge_type{prop:FontStyle} = font:Bold
    If ?temp:repair_type_warranty{prop:ReadOnly} = True
        ?temp:repair_type_warranty{prop:FontColor} = 65793
        ?temp:repair_type_warranty{prop:Color} = 15066597
    Elsif ?temp:repair_type_warranty{prop:Req} = True
        ?temp:repair_type_warranty{prop:FontColor} = 65793
        ?temp:repair_type_warranty{prop:Color} = 8454143
    Else ! If ?temp:repair_type_warranty{prop:Req} = True
        ?temp:repair_type_warranty{prop:FontColor} = 65793
        ?temp:repair_type_warranty{prop:Color} = 16777215
    End ! If ?temp:repair_type_warranty{prop:Req} = True
    ?temp:repair_type_warranty{prop:Trn} = 0
    ?temp:repair_type_warranty{prop:FontStyle} = font:Bold
    ?IsOnTest{prop:Font,3} = -1
    ?IsOnTest{prop:Color} = 15066597
    ?IsOnTest{prop:Trn} = 0
    ?IsPass{prop:Font,3} = -1
    ?IsPass{prop:Color} = 15066597
    ?IsPass{prop:Trn} = 0
    ?IsFail{prop:Font,3} = -1
    ?IsFail{prop:Color} = 15066597
    ?IsFail{prop:Trn} = 0
    ?QAGroup{prop:Font,3} = -1
    ?QAGroup{prop:Color} = 15066597
    ?QAGroup{prop:Trn} = 0
    ?Prompt23{prop:FontColor} = -1
    ?Prompt23{prop:Color} = 15066597
    If ?temp:warranty_charge_type{prop:ReadOnly} = True
        ?temp:warranty_charge_type{prop:FontColor} = 65793
        ?temp:warranty_charge_type{prop:Color} = 15066597
    Elsif ?temp:warranty_charge_type{prop:Req} = True
        ?temp:warranty_charge_type{prop:FontColor} = 65793
        ?temp:warranty_charge_type{prop:Color} = 8454143
    Else ! If ?temp:warranty_charge_type{prop:Req} = True
        ?temp:warranty_charge_type{prop:FontColor} = 65793
        ?temp:warranty_charge_type{prop:Color} = 16777215
    End ! If ?temp:warranty_charge_type{prop:Req} = True
    ?temp:warranty_charge_type{prop:Trn} = 0
    ?temp:warranty_charge_type{prop:FontStyle} = font:Bold
    ?ChargeTypePrompt{prop:FontColor} = -1
    ?ChargeTypePrompt{prop:Color} = 15066597
    If ?temp:repair_type{prop:ReadOnly} = True
        ?temp:repair_type{prop:FontColor} = 65793
        ?temp:repair_type{prop:Color} = 15066597
    Elsif ?temp:repair_type{prop:Req} = True
        ?temp:repair_type{prop:FontColor} = 65793
        ?temp:repair_type{prop:Color} = 8454143
    Else ! If ?temp:repair_type{prop:Req} = True
        ?temp:repair_type{prop:FontColor} = 65793
        ?temp:repair_type{prop:Color} = 16777215
    End ! If ?temp:repair_type{prop:Req} = True
    ?temp:repair_type{prop:Trn} = 0
    ?temp:repair_type{prop:FontStyle} = font:Bold
    ?job:Warranty_Job{prop:Font,3} = -1
    ?job:Warranty_Job{prop:Color} = 15066597
    ?job:Warranty_Job{prop:Trn} = 0
    ?WarrantyTypePrompt{prop:FontColor} = -1
    ?WarrantyTypePrompt{prop:Color} = 15066597
    ?Sheet4{prop:Color} = 15066597
    ?MFCTab{prop:Color} = 15066597
    ?jfc_prompt_1{prop:FontColor} = -1
    ?jfc_prompt_1{prop:Color} = 15066597
    If ?jfc_value_1{prop:ReadOnly} = True
        ?jfc_value_1{prop:FontColor} = 65793
        ?jfc_value_1{prop:Color} = 15066597
    Elsif ?jfc_value_1{prop:Req} = True
        ?jfc_value_1{prop:FontColor} = 65793
        ?jfc_value_1{prop:Color} = 8454143
    Else ! If ?jfc_value_1{prop:Req} = True
        ?jfc_value_1{prop:FontColor} = 65793
        ?jfc_value_1{prop:Color} = 16777215
    End ! If ?jfc_value_1{prop:Req} = True
    ?jfc_value_1{prop:Trn} = 0
    ?jfc_value_1{prop:FontStyle} = font:Bold
    ?jfc_prompt_2{prop:FontColor} = -1
    ?jfc_prompt_2{prop:Color} = 15066597
    If ?jfc_value_2{prop:ReadOnly} = True
        ?jfc_value_2{prop:FontColor} = 65793
        ?jfc_value_2{prop:Color} = 15066597
    Elsif ?jfc_value_2{prop:Req} = True
        ?jfc_value_2{prop:FontColor} = 65793
        ?jfc_value_2{prop:Color} = 8454143
    Else ! If ?jfc_value_2{prop:Req} = True
        ?jfc_value_2{prop:FontColor} = 65793
        ?jfc_value_2{prop:Color} = 16777215
    End ! If ?jfc_value_2{prop:Req} = True
    ?jfc_value_2{prop:Trn} = 0
    ?jfc_value_2{prop:FontStyle} = font:Bold
    ?jfc_prompt_3{prop:FontColor} = -1
    ?jfc_prompt_3{prop:Color} = 15066597
    If ?jfc_value_3{prop:ReadOnly} = True
        ?jfc_value_3{prop:FontColor} = 65793
        ?jfc_value_3{prop:Color} = 15066597
    Elsif ?jfc_value_3{prop:Req} = True
        ?jfc_value_3{prop:FontColor} = 65793
        ?jfc_value_3{prop:Color} = 8454143
    Else ! If ?jfc_value_3{prop:Req} = True
        ?jfc_value_3{prop:FontColor} = 65793
        ?jfc_value_3{prop:Color} = 16777215
    End ! If ?jfc_value_3{prop:Req} = True
    ?jfc_value_3{prop:Trn} = 0
    ?jfc_value_3{prop:FontStyle} = font:Bold
    ?jfc_prompt_4{prop:FontColor} = -1
    ?jfc_prompt_4{prop:Color} = 15066597
    If ?jfc_value_4{prop:ReadOnly} = True
        ?jfc_value_4{prop:FontColor} = 65793
        ?jfc_value_4{prop:Color} = 15066597
    Elsif ?jfc_value_4{prop:Req} = True
        ?jfc_value_4{prop:FontColor} = 65793
        ?jfc_value_4{prop:Color} = 8454143
    Else ! If ?jfc_value_4{prop:Req} = True
        ?jfc_value_4{prop:FontColor} = 65793
        ?jfc_value_4{prop:Color} = 16777215
    End ! If ?jfc_value_4{prop:Req} = True
    ?jfc_value_4{prop:Trn} = 0
    ?jfc_value_4{prop:FontStyle} = font:Bold
    ?jfc_prompt_5{prop:FontColor} = -1
    ?jfc_prompt_5{prop:Color} = 15066597
    If ?jfc_value_5{prop:ReadOnly} = True
        ?jfc_value_5{prop:FontColor} = 65793
        ?jfc_value_5{prop:Color} = 15066597
    Elsif ?jfc_value_5{prop:Req} = True
        ?jfc_value_5{prop:FontColor} = 65793
        ?jfc_value_5{prop:Color} = 8454143
    Else ! If ?jfc_value_5{prop:Req} = True
        ?jfc_value_5{prop:FontColor} = 65793
        ?jfc_value_5{prop:Color} = 16777215
    End ! If ?jfc_value_5{prop:Req} = True
    ?jfc_value_5{prop:Trn} = 0
    ?jfc_value_5{prop:FontStyle} = font:Bold
    ?jfc_prompt_6{prop:FontColor} = -1
    ?jfc_prompt_6{prop:Color} = 15066597
    If ?jfc_value_6{prop:ReadOnly} = True
        ?jfc_value_6{prop:FontColor} = 65793
        ?jfc_value_6{prop:Color} = 15066597
    Elsif ?jfc_value_6{prop:Req} = True
        ?jfc_value_6{prop:FontColor} = 65793
        ?jfc_value_6{prop:Color} = 8454143
    Else ! If ?jfc_value_6{prop:Req} = True
        ?jfc_value_6{prop:FontColor} = 65793
        ?jfc_value_6{prop:Color} = 16777215
    End ! If ?jfc_value_6{prop:Req} = True
    ?jfc_value_6{prop:Trn} = 0
    ?jfc_value_6{prop:FontStyle} = font:Bold
    ?jfc_prompt_7{prop:FontColor} = -1
    ?jfc_prompt_7{prop:Color} = 15066597
    If ?jfc_value_7{prop:ReadOnly} = True
        ?jfc_value_7{prop:FontColor} = 65793
        ?jfc_value_7{prop:Color} = 15066597
    Elsif ?jfc_value_7{prop:Req} = True
        ?jfc_value_7{prop:FontColor} = 65793
        ?jfc_value_7{prop:Color} = 8454143
    Else ! If ?jfc_value_7{prop:Req} = True
        ?jfc_value_7{prop:FontColor} = 65793
        ?jfc_value_7{prop:Color} = 16777215
    End ! If ?jfc_value_7{prop:Req} = True
    ?jfc_value_7{prop:Trn} = 0
    ?jfc_value_7{prop:FontStyle} = font:Bold
    ?jfc_prompt_8{prop:FontColor} = -1
    ?jfc_prompt_8{prop:Color} = 15066597
    If ?jfc_value_8{prop:ReadOnly} = True
        ?jfc_value_8{prop:FontColor} = 65793
        ?jfc_value_8{prop:Color} = 15066597
    Elsif ?jfc_value_8{prop:Req} = True
        ?jfc_value_8{prop:FontColor} = 65793
        ?jfc_value_8{prop:Color} = 8454143
    Else ! If ?jfc_value_8{prop:Req} = True
        ?jfc_value_8{prop:FontColor} = 65793
        ?jfc_value_8{prop:Color} = 16777215
    End ! If ?jfc_value_8{prop:Req} = True
    ?jfc_value_8{prop:Trn} = 0
    ?jfc_value_8{prop:FontStyle} = font:Bold
    ?jfc_prompt_9{prop:FontColor} = -1
    ?jfc_prompt_9{prop:Color} = 15066597
    If ?jfc_value_9{prop:ReadOnly} = True
        ?jfc_value_9{prop:FontColor} = 65793
        ?jfc_value_9{prop:Color} = 15066597
    Elsif ?jfc_value_9{prop:Req} = True
        ?jfc_value_9{prop:FontColor} = 65793
        ?jfc_value_9{prop:Color} = 8454143
    Else ! If ?jfc_value_9{prop:Req} = True
        ?jfc_value_9{prop:FontColor} = 65793
        ?jfc_value_9{prop:Color} = 16777215
    End ! If ?jfc_value_9{prop:Req} = True
    ?jfc_value_9{prop:Trn} = 0
    ?jfc_value_9{prop:FontStyle} = font:Bold
    ?jfc_prompt_10{prop:FontColor} = -1
    ?jfc_prompt_10{prop:Color} = 15066597
    If ?jfc_value_10{prop:ReadOnly} = True
        ?jfc_value_10{prop:FontColor} = 65793
        ?jfc_value_10{prop:Color} = 15066597
    Elsif ?jfc_value_10{prop:Req} = True
        ?jfc_value_10{prop:FontColor} = 65793
        ?jfc_value_10{prop:Color} = 8454143
    Else ! If ?jfc_value_10{prop:Req} = True
        ?jfc_value_10{prop:FontColor} = 65793
        ?jfc_value_10{prop:Color} = 16777215
    End ! If ?jfc_value_10{prop:Req} = True
    ?jfc_value_10{prop:Trn} = 0
    ?jfc_value_10{prop:FontStyle} = font:Bold
    ?jfc_prompt_11{prop:FontColor} = -1
    ?jfc_prompt_11{prop:Color} = 15066597
    If ?jfc_value_11{prop:ReadOnly} = True
        ?jfc_value_11{prop:FontColor} = 65793
        ?jfc_value_11{prop:Color} = 15066597
    Elsif ?jfc_value_11{prop:Req} = True
        ?jfc_value_11{prop:FontColor} = 65793
        ?jfc_value_11{prop:Color} = 8454143
    Else ! If ?jfc_value_11{prop:Req} = True
        ?jfc_value_11{prop:FontColor} = 65793
        ?jfc_value_11{prop:Color} = 16777215
    End ! If ?jfc_value_11{prop:Req} = True
    ?jfc_value_11{prop:Trn} = 0
    ?jfc_value_11{prop:FontStyle} = font:Bold
    ?jfc_prompt_12{prop:FontColor} = -1
    ?jfc_prompt_12{prop:Color} = 15066597
    If ?jfc_value_12{prop:ReadOnly} = True
        ?jfc_value_12{prop:FontColor} = 65793
        ?jfc_value_12{prop:Color} = 15066597
    Elsif ?jfc_value_12{prop:Req} = True
        ?jfc_value_12{prop:FontColor} = 65793
        ?jfc_value_12{prop:Color} = 8454143
    Else ! If ?jfc_value_12{prop:Req} = True
        ?jfc_value_12{prop:FontColor} = 65793
        ?jfc_value_12{prop:Color} = 16777215
    End ! If ?jfc_value_12{prop:Req} = True
    ?jfc_value_12{prop:Trn} = 0
    ?jfc_value_12{prop:FontStyle} = font:Bold
    ?TFCTab{prop:Color} = 15066597
    If ?tfc_value_1{prop:ReadOnly} = True
        ?tfc_value_1{prop:FontColor} = 65793
        ?tfc_value_1{prop:Color} = 15066597
    Elsif ?tfc_value_1{prop:Req} = True
        ?tfc_value_1{prop:FontColor} = 65793
        ?tfc_value_1{prop:Color} = 8454143
    Else ! If ?tfc_value_1{prop:Req} = True
        ?tfc_value_1{prop:FontColor} = 65793
        ?tfc_value_1{prop:Color} = 16777215
    End ! If ?tfc_value_1{prop:Req} = True
    ?tfc_value_1{prop:Trn} = 0
    ?tfc_value_1{prop:FontStyle} = font:Bold
    ?tfc_prompt_1{prop:FontColor} = -1
    ?tfc_prompt_1{prop:Color} = 15066597
    ?tfc_prompt_2{prop:FontColor} = -1
    ?tfc_prompt_2{prop:Color} = 15066597
    If ?tfc_value_2{prop:ReadOnly} = True
        ?tfc_value_2{prop:FontColor} = 65793
        ?tfc_value_2{prop:Color} = 15066597
    Elsif ?tfc_value_2{prop:Req} = True
        ?tfc_value_2{prop:FontColor} = 65793
        ?tfc_value_2{prop:Color} = 8454143
    Else ! If ?tfc_value_2{prop:Req} = True
        ?tfc_value_2{prop:FontColor} = 65793
        ?tfc_value_2{prop:Color} = 16777215
    End ! If ?tfc_value_2{prop:Req} = True
    ?tfc_value_2{prop:Trn} = 0
    ?tfc_value_2{prop:FontStyle} = font:Bold
    ?tfc_prompt_3{prop:FontColor} = -1
    ?tfc_prompt_3{prop:Color} = 15066597
    If ?tfc_value_3{prop:ReadOnly} = True
        ?tfc_value_3{prop:FontColor} = 65793
        ?tfc_value_3{prop:Color} = 15066597
    Elsif ?tfc_value_3{prop:Req} = True
        ?tfc_value_3{prop:FontColor} = 65793
        ?tfc_value_3{prop:Color} = 8454143
    Else ! If ?tfc_value_3{prop:Req} = True
        ?tfc_value_3{prop:FontColor} = 65793
        ?tfc_value_3{prop:Color} = 16777215
    End ! If ?tfc_value_3{prop:Req} = True
    ?tfc_value_3{prop:Trn} = 0
    ?tfc_value_3{prop:FontStyle} = font:Bold
    ?tfc_prompt_8{prop:FontColor} = -1
    ?tfc_prompt_8{prop:Color} = 15066597
    If ?tfc_value_8{prop:ReadOnly} = True
        ?tfc_value_8{prop:FontColor} = 65793
        ?tfc_value_8{prop:Color} = 15066597
    Elsif ?tfc_value_8{prop:Req} = True
        ?tfc_value_8{prop:FontColor} = 65793
        ?tfc_value_8{prop:Color} = 8454143
    Else ! If ?tfc_value_8{prop:Req} = True
        ?tfc_value_8{prop:FontColor} = 65793
        ?tfc_value_8{prop:Color} = 16777215
    End ! If ?tfc_value_8{prop:Req} = True
    ?tfc_value_8{prop:Trn} = 0
    ?tfc_value_8{prop:FontStyle} = font:Bold
    ?tfc_prompt_4{prop:FontColor} = -1
    ?tfc_prompt_4{prop:Color} = 15066597
    If ?tfc_value_4{prop:ReadOnly} = True
        ?tfc_value_4{prop:FontColor} = 65793
        ?tfc_value_4{prop:Color} = 15066597
    Elsif ?tfc_value_4{prop:Req} = True
        ?tfc_value_4{prop:FontColor} = 65793
        ?tfc_value_4{prop:Color} = 8454143
    Else ! If ?tfc_value_4{prop:Req} = True
        ?tfc_value_4{prop:FontColor} = 65793
        ?tfc_value_4{prop:Color} = 16777215
    End ! If ?tfc_value_4{prop:Req} = True
    ?tfc_value_4{prop:Trn} = 0
    ?tfc_value_4{prop:FontStyle} = font:Bold
    ?tfc_prompt_9{prop:FontColor} = -1
    ?tfc_prompt_9{prop:Color} = 15066597
    If ?tfc_value_9{prop:ReadOnly} = True
        ?tfc_value_9{prop:FontColor} = 65793
        ?tfc_value_9{prop:Color} = 15066597
    Elsif ?tfc_value_9{prop:Req} = True
        ?tfc_value_9{prop:FontColor} = 65793
        ?tfc_value_9{prop:Color} = 8454143
    Else ! If ?tfc_value_9{prop:Req} = True
        ?tfc_value_9{prop:FontColor} = 65793
        ?tfc_value_9{prop:Color} = 16777215
    End ! If ?tfc_value_9{prop:Req} = True
    ?tfc_value_9{prop:Trn} = 0
    ?tfc_value_9{prop:FontStyle} = font:Bold
    ?tfc_prompt_5{prop:FontColor} = -1
    ?tfc_prompt_5{prop:Color} = 15066597
    If ?tfc_value_5{prop:ReadOnly} = True
        ?tfc_value_5{prop:FontColor} = 65793
        ?tfc_value_5{prop:Color} = 15066597
    Elsif ?tfc_value_5{prop:Req} = True
        ?tfc_value_5{prop:FontColor} = 65793
        ?tfc_value_5{prop:Color} = 8454143
    Else ! If ?tfc_value_5{prop:Req} = True
        ?tfc_value_5{prop:FontColor} = 65793
        ?tfc_value_5{prop:Color} = 16777215
    End ! If ?tfc_value_5{prop:Req} = True
    ?tfc_value_5{prop:Trn} = 0
    ?tfc_value_5{prop:FontStyle} = font:Bold
    ?tfc_prompt_10{prop:FontColor} = -1
    ?tfc_prompt_10{prop:Color} = 15066597
    If ?tfc_value_10{prop:ReadOnly} = True
        ?tfc_value_10{prop:FontColor} = 65793
        ?tfc_value_10{prop:Color} = 15066597
    Elsif ?tfc_value_10{prop:Req} = True
        ?tfc_value_10{prop:FontColor} = 65793
        ?tfc_value_10{prop:Color} = 8454143
    Else ! If ?tfc_value_10{prop:Req} = True
        ?tfc_value_10{prop:FontColor} = 65793
        ?tfc_value_10{prop:Color} = 16777215
    End ! If ?tfc_value_10{prop:Req} = True
    ?tfc_value_10{prop:Trn} = 0
    ?tfc_value_10{prop:FontStyle} = font:Bold
    ?tfc_prompt_6{prop:FontColor} = -1
    ?tfc_prompt_6{prop:Color} = 15066597
    If ?tfc_value_6{prop:ReadOnly} = True
        ?tfc_value_6{prop:FontColor} = 65793
        ?tfc_value_6{prop:Color} = 15066597
    Elsif ?tfc_value_6{prop:Req} = True
        ?tfc_value_6{prop:FontColor} = 65793
        ?tfc_value_6{prop:Color} = 8454143
    Else ! If ?tfc_value_6{prop:Req} = True
        ?tfc_value_6{prop:FontColor} = 65793
        ?tfc_value_6{prop:Color} = 16777215
    End ! If ?tfc_value_6{prop:Req} = True
    ?tfc_value_6{prop:Trn} = 0
    ?tfc_value_6{prop:FontStyle} = font:Bold
    ?tfc_prompt_11{prop:FontColor} = -1
    ?tfc_prompt_11{prop:Color} = 15066597
    If ?tfc_value_11{prop:ReadOnly} = True
        ?tfc_value_11{prop:FontColor} = 65793
        ?tfc_value_11{prop:Color} = 15066597
    Elsif ?tfc_value_11{prop:Req} = True
        ?tfc_value_11{prop:FontColor} = 65793
        ?tfc_value_11{prop:Color} = 8454143
    Else ! If ?tfc_value_11{prop:Req} = True
        ?tfc_value_11{prop:FontColor} = 65793
        ?tfc_value_11{prop:Color} = 16777215
    End ! If ?tfc_value_11{prop:Req} = True
    ?tfc_value_11{prop:Trn} = 0
    ?tfc_value_11{prop:FontStyle} = font:Bold
    ?tfc_prompt_7{prop:FontColor} = -1
    ?tfc_prompt_7{prop:Color} = 15066597
    If ?tfc_value_7{prop:ReadOnly} = True
        ?tfc_value_7{prop:FontColor} = 65793
        ?tfc_value_7{prop:Color} = 15066597
    Elsif ?tfc_value_7{prop:Req} = True
        ?tfc_value_7{prop:FontColor} = 65793
        ?tfc_value_7{prop:Color} = 8454143
    Else ! If ?tfc_value_7{prop:Req} = True
        ?tfc_value_7{prop:FontColor} = 65793
        ?tfc_value_7{prop:Color} = 16777215
    End ! If ?tfc_value_7{prop:Req} = True
    ?tfc_value_7{prop:Trn} = 0
    ?tfc_value_7{prop:FontStyle} = font:Bold
    ?tfc_prompt_12{prop:FontColor} = -1
    ?tfc_prompt_12{prop:Color} = 15066597
    If ?tfc_value_12{prop:ReadOnly} = True
        ?tfc_value_12{prop:FontColor} = 65793
        ?tfc_value_12{prop:Color} = 15066597
    Elsif ?tfc_value_12{prop:Req} = True
        ?tfc_value_12{prop:FontColor} = 65793
        ?tfc_value_12{prop:Color} = 8454143
    Else ! If ?tfc_value_12{prop:Req} = True
        ?tfc_value_12{prop:FontColor} = 65793
        ?tfc_value_12{prop:Color} = 16777215
    End ! If ?tfc_value_12{prop:Req} = True
    ?tfc_value_12{prop:Trn} = 0
    ?tfc_value_12{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?ChargeablePartsTab{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?AddPartPrompt{prop:FontColor} = -1
    ?AddPartPrompt{prop:Color} = 15066597
    If ?temp:Charge_Part_Number{prop:ReadOnly} = True
        ?temp:Charge_Part_Number{prop:FontColor} = 65793
        ?temp:Charge_Part_Number{prop:Color} = 15066597
    Elsif ?temp:Charge_Part_Number{prop:Req} = True
        ?temp:Charge_Part_Number{prop:FontColor} = 65793
        ?temp:Charge_Part_Number{prop:Color} = 8454143
    Else ! If ?temp:Charge_Part_Number{prop:Req} = True
        ?temp:Charge_Part_Number{prop:FontColor} = 65793
        ?temp:Charge_Part_Number{prop:Color} = 16777215
    End ! If ?temp:Charge_Part_Number{prop:Req} = True
    ?temp:Charge_Part_Number{prop:Trn} = 0
    ?temp:Charge_Part_Number{prop:FontStyle} = font:Bold
    ?WarrantyPartsTab{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?AddWarrantyPrompt{prop:FontColor} = -1
    ?AddWarrantyPrompt{prop:Color} = 15066597
    If ?temp:Warranty_Part_Number{prop:ReadOnly} = True
        ?temp:Warranty_Part_Number{prop:FontColor} = 65793
        ?temp:Warranty_Part_Number{prop:Color} = 15066597
    Elsif ?temp:Warranty_Part_Number{prop:Req} = True
        ?temp:Warranty_Part_Number{prop:FontColor} = 65793
        ?temp:Warranty_Part_Number{prop:Color} = 8454143
    Else ! If ?temp:Warranty_Part_Number{prop:Req} = True
        ?temp:Warranty_Part_Number{prop:FontColor} = 65793
        ?temp:Warranty_Part_Number{prop:Color} = 16777215
    End ! If ?temp:Warranty_Part_Number{prop:Req} = True
    ?temp:Warranty_Part_Number{prop:Trn} = 0
    ?temp:Warranty_Part_Number{prop:FontStyle} = font:Bold
    ?BookInSheet{prop:Color} = 15066597
    ?BookInTab{prop:Color} = 15066597
    ?Prompt22{prop:FontColor} = -1
    ?Prompt22{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?temp:Account_Number{prop:ReadOnly} = True
        ?temp:Account_Number{prop:FontColor} = 65793
        ?temp:Account_Number{prop:Color} = 15066597
    Elsif ?temp:Account_Number{prop:Req} = True
        ?temp:Account_Number{prop:FontColor} = 65793
        ?temp:Account_Number{prop:Color} = 8454143
    Else ! If ?temp:Account_Number{prop:Req} = True
        ?temp:Account_Number{prop:FontColor} = 65793
        ?temp:Account_Number{prop:Color} = 16777215
    End ! If ?temp:Account_Number{prop:Req} = True
    ?temp:Account_Number{prop:Trn} = 0
    ?temp:Account_Number{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?temp:Order_Number{prop:ReadOnly} = True
        ?temp:Order_Number{prop:FontColor} = 65793
        ?temp:Order_Number{prop:Color} = 15066597
    Elsif ?temp:Order_Number{prop:Req} = True
        ?temp:Order_Number{prop:FontColor} = 65793
        ?temp:Order_Number{prop:Color} = 8454143
    Else ! If ?temp:Order_Number{prop:Req} = True
        ?temp:Order_Number{prop:FontColor} = 65793
        ?temp:Order_Number{prop:Color} = 16777215
    End ! If ?temp:Order_Number{prop:Req} = True
    ?temp:Order_Number{prop:Trn} = 0
    ?temp:Order_Number{prop:FontStyle} = font:Bold
    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    If ?temp:job_reference{prop:ReadOnly} = True
        ?temp:job_reference{prop:FontColor} = 65793
        ?temp:job_reference{prop:Color} = 15066597
    Elsif ?temp:job_reference{prop:Req} = True
        ?temp:job_reference{prop:FontColor} = 65793
        ?temp:job_reference{prop:Color} = 8454143
    Else ! If ?temp:job_reference{prop:Req} = True
        ?temp:job_reference{prop:FontColor} = 65793
        ?temp:job_reference{prop:Color} = 16777215
    End ! If ?temp:job_reference{prop:Req} = True
    ?temp:job_reference{prop:Trn} = 0
    ?temp:job_reference{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?job:ESN{prop:ReadOnly} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 15066597
    Elsif ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 8454143
    Else ! If ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 16777215
    End ! If ?job:ESN{prop:Req} = True
    ?job:ESN{prop:Trn} = 0
    ?job:ESN{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?job:MSN{prop:ReadOnly} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 15066597
    Elsif ?job:MSN{prop:Req} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 8454143
    Else ! If ?job:MSN{prop:Req} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 16777215
    End ! If ?job:MSN{prop:Req} = True
    ?job:MSN{prop:Trn} = 0
    ?job:MSN{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?job:Model_Number{prop:ReadOnly} = True
        ?job:Model_Number{prop:FontColor} = 65793
        ?job:Model_Number{prop:Color} = 15066597
    Elsif ?job:Model_Number{prop:Req} = True
        ?job:Model_Number{prop:FontColor} = 65793
        ?job:Model_Number{prop:Color} = 8454143
    Else ! If ?job:Model_Number{prop:Req} = True
        ?job:Model_Number{prop:FontColor} = 65793
        ?job:Model_Number{prop:Color} = 16777215
    End ! If ?job:Model_Number{prop:Req} = True
    ?job:Model_Number{prop:Trn} = 0
    ?job:Model_Number{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    If ?job:Surname{prop:ReadOnly} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 15066597
    Elsif ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 8454143
    Else ! If ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 16777215
    End ! If ?job:Surname{prop:Req} = True
    ?job:Surname{prop:Trn} = 0
    ?job:Surname{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?job:Company_Name_Delivery{prop:ReadOnly} = True
        ?job:Company_Name_Delivery{prop:FontColor} = 65793
        ?job:Company_Name_Delivery{prop:Color} = 15066597
    Elsif ?job:Company_Name_Delivery{prop:Req} = True
        ?job:Company_Name_Delivery{prop:FontColor} = 65793
        ?job:Company_Name_Delivery{prop:Color} = 8454143
    Else ! If ?job:Company_Name_Delivery{prop:Req} = True
        ?job:Company_Name_Delivery{prop:FontColor} = 65793
        ?job:Company_Name_Delivery{prop:Color} = 16777215
    End ! If ?job:Company_Name_Delivery{prop:Req} = True
    ?job:Company_Name_Delivery{prop:Trn} = 0
    ?job:Company_Name_Delivery{prop:FontStyle} = font:Bold
    If ?job:Address_Line1_Delivery{prop:ReadOnly} = True
        ?job:Address_Line1_Delivery{prop:FontColor} = 65793
        ?job:Address_Line1_Delivery{prop:Color} = 15066597
    Elsif ?job:Address_Line1_Delivery{prop:Req} = True
        ?job:Address_Line1_Delivery{prop:FontColor} = 65793
        ?job:Address_Line1_Delivery{prop:Color} = 8454143
    Else ! If ?job:Address_Line1_Delivery{prop:Req} = True
        ?job:Address_Line1_Delivery{prop:FontColor} = 65793
        ?job:Address_Line1_Delivery{prop:Color} = 16777215
    End ! If ?job:Address_Line1_Delivery{prop:Req} = True
    ?job:Address_Line1_Delivery{prop:Trn} = 0
    ?job:Address_Line1_Delivery{prop:FontStyle} = font:Bold
    If ?job:Address_Line2_Delivery{prop:ReadOnly} = True
        ?job:Address_Line2_Delivery{prop:FontColor} = 65793
        ?job:Address_Line2_Delivery{prop:Color} = 15066597
    Elsif ?job:Address_Line2_Delivery{prop:Req} = True
        ?job:Address_Line2_Delivery{prop:FontColor} = 65793
        ?job:Address_Line2_Delivery{prop:Color} = 8454143
    Else ! If ?job:Address_Line2_Delivery{prop:Req} = True
        ?job:Address_Line2_Delivery{prop:FontColor} = 65793
        ?job:Address_Line2_Delivery{prop:Color} = 16777215
    End ! If ?job:Address_Line2_Delivery{prop:Req} = True
    ?job:Address_Line2_Delivery{prop:Trn} = 0
    ?job:Address_Line2_Delivery{prop:FontStyle} = font:Bold
    If ?job:Address_Line3_Delivery{prop:ReadOnly} = True
        ?job:Address_Line3_Delivery{prop:FontColor} = 65793
        ?job:Address_Line3_Delivery{prop:Color} = 15066597
    Elsif ?job:Address_Line3_Delivery{prop:Req} = True
        ?job:Address_Line3_Delivery{prop:FontColor} = 65793
        ?job:Address_Line3_Delivery{prop:Color} = 8454143
    Else ! If ?job:Address_Line3_Delivery{prop:Req} = True
        ?job:Address_Line3_Delivery{prop:FontColor} = 65793
        ?job:Address_Line3_Delivery{prop:Color} = 16777215
    End ! If ?job:Address_Line3_Delivery{prop:Req} = True
    ?job:Address_Line3_Delivery{prop:Trn} = 0
    ?job:Address_Line3_Delivery{prop:FontStyle} = font:Bold
    If ?job:Postcode_Delivery{prop:ReadOnly} = True
        ?job:Postcode_Delivery{prop:FontColor} = 65793
        ?job:Postcode_Delivery{prop:Color} = 15066597
    Elsif ?job:Postcode_Delivery{prop:Req} = True
        ?job:Postcode_Delivery{prop:FontColor} = 65793
        ?job:Postcode_Delivery{prop:Color} = 8454143
    Else ! If ?job:Postcode_Delivery{prop:Req} = True
        ?job:Postcode_Delivery{prop:FontColor} = 65793
        ?job:Postcode_Delivery{prop:Color} = 16777215
    End ! If ?job:Postcode_Delivery{prop:Req} = True
    ?job:Postcode_Delivery{prop:Trn} = 0
    ?job:Postcode_Delivery{prop:FontStyle} = font:Bold
    ?job:Workshop{prop:Font,3} = -1
    ?job:Workshop{prop:Color} = 15066597
    ?job:Workshop{prop:Trn} = 0
    ?jobe:InWorkshopDate{prop:FontColor} = -1
    ?jobe:InWorkshopDate{prop:Color} = 15066597
    ?jobe:InWorkshopTime{prop:FontColor} = -1
    ?jobe:InWorkshopTime{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    If ?job:Location{prop:ReadOnly} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 15066597
    Elsif ?job:Location{prop:Req} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 8454143
    Else ! If ?job:Location{prop:Req} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 16777215
    End ! If ?job:Location{prop:Req} = True
    ?job:Location{prop:Trn} = 0
    ?job:Location{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597

    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    ?Prompt15{prop:FontColor} = -1
    ?Prompt15{prop:Color} = 15066597
    ?Prompt16{prop:FontColor} = -1
    ?Prompt16{prop:Color} = 15066597
    ?SerialNumberPrompt{prop:FontColor} = -1
    ?SerialNumberPrompt{prop:Color} = 15066597
    If ?Temp_IMEI_Number{prop:ReadOnly} = True
        ?Temp_IMEI_Number{prop:FontColor} = 65793
        ?Temp_IMEI_Number{prop:Color} = 15066597
    Elsif ?Temp_IMEI_Number{prop:Req} = True
        ?Temp_IMEI_Number{prop:FontColor} = 65793
        ?Temp_IMEI_Number{prop:Color} = 8454143
    Else ! If ?Temp_IMEI_Number{prop:Req} = True
        ?Temp_IMEI_Number{prop:FontColor} = 65793
        ?Temp_IMEI_Number{prop:Color} = 16777215
    End ! If ?Temp_IMEI_Number{prop:Req} = True
    ?Temp_IMEI_Number{prop:Trn} = 0
    ?Temp_IMEI_Number{prop:FontStyle} = font:Bold
    ?Prompt18{prop:FontColor} = -1
    ?Prompt18{prop:Color} = 15066597
    ?Prompt19{prop:FontColor} = -1
    ?Prompt19{prop:Color} = 15066597
    ?Prompt30{prop:FontColor} = -1
    ?Prompt30{prop:Color} = 15066597
    ?Prompt21{prop:FontColor} = -1
    ?Prompt21{prop:Color} = 15066597
    ?Prompt29{prop:FontColor} = -1
    ?Prompt29{prop:Color} = 15066597
    ?Prompt31{prop:FontColor} = -1
    ?Prompt31{prop:Color} = 15066597
    ?Prompt20{prop:FontColor} = -1
    ?Prompt20{prop:Color} = 15066597
    ?Prompt28{prop:FontColor} = -1
    ?Prompt28{prop:Color} = 15066597
    ?Prompt17{prop:FontColor} = -1
    ?Prompt17{prop:Color} = 15066597
    ?Prompt32{prop:FontColor} = -1
    ?Prompt32{prop:Color} = 15066597
    ?JobCompletePanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Update_Record   ROUTINE
    IF (RecordState = rs:UpdateRecord) THEN
        IF (?jfc_value_1{PROP:Hide} = 0) THEN
            job:Fault_Code1 = jfc_value[1]
        END
        IF (?jfc_value_2{PROP:Hide} = 0) THEN
            job:Fault_Code2 = jfc_value[2]
        END

        IF (?jfc_value_3{PROP:Hide} = 0) THEN
            job:Fault_Code3 = jfc_value[3]
        END

        IF (?jfc_value_4{PROP:Hide} = 0) THEN
            job:Fault_Code4 = jfc_value[4]
        END
        IF (?jfc_value_5{PROP:Hide} = 0) THEN
            job:Fault_Code5 = jfc_value[5]
        END

        IF (?jfc_value_6{PROP:Hide} = 0) THEN
            job:Fault_Code6 = jfc_value[6]
        END

        IF (?jfc_value_7{PROP:Hide} = 0) THEN
            job:Fault_Code7 = jfc_value[7]
        END

        IF (?jfc_value_8{PROP:Hide} = 0) THEN
            job:Fault_Code8 = jfc_value[8]
        END

        IF (?jfc_value_9{PROP:Hide} = 0) THEN
            job:Fault_Code9 = jfc_value[9]
        END

        IF (?jfc_value_10{PROP:Hide} = 0) THEN
            job:Fault_Code10 = jfc_value[10]
        END

        IF (?jfc_value_11{PROP:Hide} = 0) THEN
            job:Fault_Code11 = jfc_value[11]
        END

        IF (?jfc_value_12{PROP:Hide} = 0) THEN
            job:Fault_Code12 = jfc_value[12]
        END

        IF (?tfc_value_1{PROP:Hide} = 0) THEN
            jobe:TraFaultCode1 = tfc_value[1]
        END
        IF (?tfc_value_2{PROP:Hide} = 0) THEN
            jobe:TraFaultCode2 = tfc_value[2]
        END

        IF (?tfc_value_3{PROP:Hide} = 0) THEN
            jobe:TraFaultCode3 = tfc_value[3]
        END

        IF (?tfc_value_4{PROP:Hide} = 0) THEN
            jobe:TraFaultCode4 = tfc_value[4]
        END
        IF (?tfc_value_5{PROP:Hide} = 0) THEN
            jobe:TraFaultCode5 = tfc_value[5]
        END

        IF (?tfc_value_6{PROP:Hide} = 0) THEN
            jobe:TraFaultCode6 = tfc_value[6]
        END

        IF (?tfc_value_7{PROP:Hide} = 0) THEN
            jobe:TraFaultCode7 = tfc_value[7]
        END

        IF (?tfc_value_8{PROP:Hide} = 0) THEN
            jobe:TraFaultCode8 = tfc_value[8]
        END

        IF (?tfc_value_9{PROP:Hide} = 0) THEN
            jobe:TraFaultCode9 = tfc_value[9]
        END

        IF (?tfc_value_10{PROP:Hide} = 0) THEN
            jobe:TraFaultCode10 = tfc_value[10]
        END

        IF (?tfc_value_11{PROP:Hide} = 0) THEN
            jobe:TraFaultCode11 = tfc_value[11]
        END

        IF (?tfc_value_12{PROP:Hide} = 0) THEN
            jobe:TraFaultCode12 = tfc_value[12]
        END

        access:jobs.update()
        access:jobse.update()
        access:jobnotes.update()
    END


Cancel_Record   ROUTINE
    RecordState = rs:NoRecord
Set_Checkboxes  ROUTINE
    IsOnTest = 0
    IsPass = 0
    IsFail = 0
    IF (job:current_status[1 : 3] = '320') THEN
        IsOnTest = 1
    ELSIF (job:current_status[1 : 3] = '605') THEN
        IsPass =1
    ELSIF (job:current_status[1 : 3] = '315') THEN
        IsFail = 1
    END
Activate_Bookin_Panel   ROUTINE
    DO Enable_Bookin_Panel
    DO Disable_TestAndRepair_Panel
    DO Disable_Bottom_Panel
    DO Disable_QA_Panel
    !Enable(?OKButton)
    Enable(?CancelButton)
    ActivePanel = ap:BookinPanel
    DISPLAY()
    SELECT(?job:ESN)
Activate_TestAndRepair_Panel    ROUTINE
    DO Disable_Bookin_Panel
    DO Enable_TestAndRepair_Panel
    DO Enable_Bottom_Panel
    !Enable(?OKButton)
    Enable(?CancelButton)
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = job:Manufacturer
    IF ((Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign) AND (man:QAAtCompletion)) THEN
        ?CompleteJobButton{PROP:Text} = 'Complete Repair [F10]'
    ELSE
        ?CompleteJobButton{PROP:Text} = 'Complete Job [F10]'
    END
    DO Disable_QA_Panel
    ActivePanel = ap:TestAndRepairPanel
    DISPLAY()

Activate_QA_Panel   ROUTINE
    DO Disable_Bookin_Panel
    DO Disable_TestAndRepair_Panel
    DO Disable_Bottom_Panel
    !Disable(?OKButton)
    Disable(?CancelButton)
    DO Enable_QA_Panel
    ActivePanel = ap:QAPanel
    DO Auto_Allocate_Job
    DISPLAY()
Activate_No_Panel   ROUTINE
    DO Disable_Bookin_Panel
    DO Disable_TestAndRepair_Panel
    DO Disable_Bottom_Panel
    !Disable(?OKButton)
    Disable(?CancelButton)
    DO Disable_QA_Panel
    IF (ActivePanel = ap:BookinPanel) THEN
        SELECT(?temp:Account_Number)
    ELSE
        temp_IMEI_Number = ''
        SELECT(?temp_IMEI_Number)
    END
    ActivePanel = ap:NoPanel
    DISPLAY()
Enable_Bookin_Panel ROUTINE
    Enable(?Prompt3)
    Enable(?Prompt4)
    Enable(?Prompt5)
    Enable(?Prompt6)
    Enable(?Prompt7)
    Enable(?Prompt8)
    Enable(?job:ESN)
    Enable(?job:MSN)
    Enable(?job:Model_Number)
    Enable(?job:Surname)
    Enable(?job:Company_Name_Delivery)
    Enable(?job:Address_Line1_Delivery)
    Enable(?job:Address_Line2_Delivery)
    Enable(?job:Address_Line3_Delivery)
    Enable(?job:Postcode_Delivery)
    Enable(?job:Workshop)
    Enable(?jobe:InWorkshopDate)
    Enable(?jobe:InWorkshopTime)
    Enable(?job:Location)
    Enable(?PickLocationButton)
    Enable(?PickModelButton)
    Enable(?BookOKButton)
Disable_Bookin_Panel    ROUTINE
    Disable(?Prompt3)
    Disable(?Prompt4)
    Disable(?Prompt5)
    Disable(?Prompt6)
    Disable(?Prompt7)
    Disable(?Prompt8)
    Disable(?job:ESN)
    Disable(?job:MSN)
    Disable(?job:Model_Number)
    Disable(?job:Surname)
    Disable(?job:Company_Name_Delivery)
    Disable(?job:Address_Line1_Delivery)
    Disable(?job:Address_Line2_Delivery)
    Disable(?job:Address_Line3_Delivery)
    Disable(?job:Postcode_Delivery)
    Disable(?job:Workshop)
    Disable(?jobe:InWorkshopDate)
    Disable(?jobe:InWorkshopTime)
    Disable(?job:Location)
    Disable(?PickLocationButton)
    Disable(?PickModelButton)
    Disable(?BookOKButton)
Enable_TestAndRepair_Panel  ROUTINE
    Enable(?FaultDescriptionPrompt)
    Enable(?jbn:Fault_Description)
    Enable(?PickFaultDescriptionButton)
    Enable(?EngineersNotesPrompt)
    Enable(?jbn:Engineers_Notes)
    Enable(?PickEngineersNotesButton)
    Enable(?InvoiceTextPrompt)
    Enable(?jbn:Invoice_Text)
    Enable(?PickInvoiceTextButton)
    Enable(?IsOnTest)
    Enable(?IsPass)
    Enable(?IsFail)

    Enable(?job:chargeable_job)
    !Enable(?job:charge_type)
    Enable(?temp:charge_type)
    Enable(?ChargeButton)
    Enable(?ChargeTypePrompt)
    !Enable(?job:repair_type)
    Enable(?temp:repair_type)
    Enable(?ChargeTypeButton)
    POST(EVENT:Accepted, ?job:chargeable_job)

    Enable(?job:warranty_job)
    !Enable(?job:warranty_charge_type)
    Enable(?temp:warranty_charge_type)
    Enable(?WarrantyButton)
    Enable(?WarrantyTypePrompt)
    !Enable(?job:repair_type_warranty)
    Enable(?temp:repair_type_warranty)
    Enable(?WarrantyTypeButton)
    POST(EVENT:Accepted, ?job:warranty_job)

    DO Enable_Parts
    !Enable(?ChargeablePartsTab)
    !Enable(?List)
    !Unhide(?PartInsert)
    !Unhide(?PartAdjustment)
    !Unhide(?PartChange)
    !Unhide(?PartDelete)
    !Enable(?WarrantyPartsTab)
    !Enable(?List:2)
    !Unhide(?WarInsert)
    !Unhide(?WarAdjustment)
    !Unhide(?WarChange)
    !Unhide(?WarDelete)

    Enable(?MFCTab)
    Enable(?TFCTab)

    IF (?jfc_prompt_1{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_1)
    END
    IF (?jfc_value_1{PROP:Hide} = 0) THEN
        Enable(?jfc_value_1)
    END
    IF (?popcalendar{PROP:Hide} = 0) THEN
        Enable(?popcalendar)
    END
    IF (?JFC1_Button{PROP:Hide} = 0) THEN
        Enable(?JFC1_Button)
    END

    IF (?jfc_prompt_2{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_2)
    END
    IF (?jfc_value_2{PROP:Hide} = 0) THEN
        Enable(?jfc_value_2)
    END
    IF (?popcalendar:2{PROP:Hide} = 0) THEN
        Enable(?popcalendar:2)
    END
    IF (?JFC2_Button{PROP:Hide} = 0) THEN
        Enable(?JFC2_Button)
    END

    IF (?jfc_prompt_3{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_3)
    END
    IF (?jfc_value_3{PROP:Hide} = 0) THEN
        Enable(?jfc_value_3)
    END
    IF (?popcalendar:3{PROP:Hide} = 0) THEN
        Enable(?popcalendar:3)
    END
    IF (?JFC3_Button{PROP:Hide} = 0) THEN
        Enable(?JFC3_Button)
    END

    IF (?jfc_prompt_4{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_4)
    END
    IF (?jfc_value_4{PROP:Hide} = 0) THEN
        Enable(?jfc_value_4)
    END
    IF (?popcalendar:4{PROP:Hide} = 0) THEN
        Enable(?popcalendar:4)
    END
    IF (?JFC4_Button{PROP:Hide} = 0) THEN
        Enable(?JFC4_Button)
    END

    IF (?jfc_prompt_5{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_5)
    END
    IF (?jfc_value_5{PROP:Hide} = 0) THEN
        Enable(?jfc_value_5)
    END
    IF (?popcalendar:5{PROP:Hide} = 0) THEN
        Enable(?popcalendar:5)
    END
    IF (?JFC5_Button{PROP:Hide} = 0) THEN
        Enable(?JFC5_Button)
    END

    IF (?jfc_prompt_6{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_6)
    END
    IF (?jfc_value_6{PROP:Hide} = 0) THEN
        Enable(?jfc_value_6)
    END
    IF (?popcalendar:6{PROP:Hide} = 0) THEN
        Enable(?popcalendar:6)
    END
    IF (?JFC6_Button{PROP:Hide} = 0) THEN
        Enable(?JFC6_Button)
    END

    IF (?jfc_prompt_7{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_7)
    END
    IF (?jfc_value_7{PROP:Hide} = 0) THEN
        Enable(?jfc_value_7)
    END
    IF (?popcalendar:7{PROP:Hide} = 0) THEN
        Enable(?popcalendar:7)
    END
    IF (?JFC7_Button{PROP:Hide} = 0) THEN
        Enable(?JFC7_Button)
    END

    IF (?jfc_prompt_8{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_8)
    END
    IF (?jfc_value_8{PROP:Hide} = 0) THEN
        Enable(?jfc_value_8)
    END
    IF (?popcalendar:8{PROP:Hide} = 0) THEN
        Enable(?popcalendar:8)
    END
    IF (?JFC8_Button{PROP:Hide} = 0) THEN
        Enable(?JFC8_Button)
    END

    IF (?jfc_prompt_9{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_9)
    END
    IF (?jfc_value_9{PROP:Hide} = 0) THEN
        Enable(?jfc_value_9)
    END
    IF (?popcalendar:9{PROP:Hide} = 0) THEN
        Enable(?popcalendar:9)
    END
    IF (?JFC9_Button{PROP:Hide} = 0) THEN
        Enable(?JFC9_Button)
    END

    IF (?jfc_prompt_10{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_10)
    END
    IF (?jfc_value_10{PROP:Hide} = 0) THEN
        Enable(?jfc_value_10)
    END
    IF (?popcalendar:10{PROP:Hide} = 0) THEN
        Enable(?popcalendar:10)
    END
    IF (?JFC10_Button{PROP:Hide} = 0) THEN
        Enable(?JFC10_Button)
    END

    IF (?jfc_prompt_11{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_11)
    END
    IF (?jfc_value_11{PROP:Hide} = 0) THEN
        Enable(?jfc_value_11)
    END
    IF (?popcalendar:11{PROP:Hide} = 0) THEN
        Enable(?popcalendar:11)
    END
    IF (?JFC11_Button{PROP:Hide} = 0) THEN
        Enable(?JFC11_Button)
    END

    IF (?jfc_prompt_12{PROP:Hide} = 0) THEN
        Enable(?jfc_prompt_12)
    END
    IF (?jfc_value_12{PROP:Hide} = 0) THEN
        Enable(?jfc_value_12)
    END
    IF (?popcalendar:12{PROP:Hide} = 0) THEN
        Enable(?popcalendar:12)
    END
    IF (?JFC12_Button{PROP:Hide} = 0) THEN
        Enable(?JFC12_Button)
    END

Disable_TestAndRepair_Panel ROUTINE
    Disable(?FaultDescriptionPrompt)
    Disable(?jbn:Fault_Description)
    Disable(?PickFaultDescriptionButton)
    Disable(?EngineersNotesPrompt)
    Disable(?PickEngineersNotesButton)
    Disable(?jbn:Engineers_Notes)
    Disable(?InvoiceTextPrompt)
    Disable(?jbn:Invoice_Text)
    Disable(?PickInvoiceTextButton)
    Disable(?IsOnTest)
    Disable(?IsPass)
    Disable(?IsFail)

    DO Disable_Parts

    Disable(?job:chargeable_job)
    !Disable(?job:charge_type)
    Disable(?temp:charge_type)
    Disable(?ChargeButton)

    Disable(?ChargeTypePrompt)
    !Disable(?job:repair_type)
    Disable(?temp:repair_type)
    Disable(?ChargeTypeButton)

    Disable(?job:warranty_job)
    !Disable(?job:warranty_charge_type)
    Disable(?temp:warranty_charge_type)
    Disable(?WarrantyButton)

    Disable(?WarrantyTypePrompt)
    !Disable(?job:repair_type_warranty)
    Disable(?temp:repair_type_warranty)
    Disable(?WarrantyTypeButton)

    !Disable(?ChargeablePartsTab)
    !Disable(?List)
    !Hide(?PartInsert)
    !Hide(?PartAdjustment)
    !Hide(?PartChange)
    !Hide(?PartDelete)
    !Disable(?WarrantyPartsTab)
    !isable(?List:2)
    !Hide(?WarInsert)
    !Hide(?WarAdjustment)
    !Hide(?WarChange)
    !Hide(?WarDelete)

    Disable(?MFCTab)
    Disable(?TFCTab)

    IF (?jfc_prompt_1{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_1)
    END
    IF (?jfc_value_1{PROP:Hide} = 0) THEN
        Disable(?jfc_value_1)
    END
    IF (?popcalendar{PROP:Hide} = 0) THEN
        Disable(?popcalendar)
    END
    IF (?JFC1_Button{PROP:Hide} = 0) THEN
        Disable(?jfc1_Button)
    END

    IF (?jfc_prompt_2{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_2)
    END
    IF (?jfc_value_2{PROP:Hide} = 0) THEN
        Disable(?jfc_value_2)
    END
    IF (?popcalendar:2{PROP:Hide} = 0) THEN
        Disable(?popcalendar:2)
    END
    IF (?JFC2_Button{PROP:Hide} = 0) THEN
        Disable(?jfc2_Button)
    END

    IF (?jfc_prompt_3{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_3)
    END
    IF (?jfc_value_3{PROP:Hide} = 0) THEN
        Disable(?jfc_value_3)
    END
    IF (?popcalendar:3{PROP:Hide} = 0) THEN
        Disable(?popcalendar:3)
    END
    IF (?JFC3_Button{PROP:Hide} = 0) THEN
        Disable(?JFC3_Button)
    END

    IF (?jfc_prompt_4{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_4)
    END
    IF (?jfc_value_4{PROP:Hide} = 0) THEN
        Disable(?jfc_value_4)
    END
    IF (?popcalendar:4{PROP:Hide} = 0) THEN
        Disable(?popcalendar:4)
    END
    IF (?JFC4_Button{PROP:Hide} = 0) THEN
        Disable(?jfc4_Button)
    END

    IF (?jfc_prompt_5{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_5)
    END
    IF (?jfc_value_5{PROP:Hide} = 0) THEN
        Disable(?jfc_value_5)
    END
    IF (?popcalendar:5{PROP:Hide} = 0) THEN
        Disable(?popcalendar:5)
    END
    IF (?JFC5_Button{PROP:Hide} = 0) THEN
        Disable(?jfc5_Button)
    END

    IF (?jfc_prompt_6{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_6)
    END
    IF (?jfc_value_6{PROP:Hide} = 0) THEN
        Disable(?jfc_value_6)
    END
    IF (?popcalendar:6{PROP:Hide} = 0) THEN
        Disable(?popcalendar:6)
    END
    IF (?JFC6_Button{PROP:Hide} = 0) THEN
        Disable(?jfc6_Button)
    END

    IF (?jfc_prompt_7{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_7)
    END
    IF (?jfc_value_7{PROP:Hide} = 0) THEN
        Disable(?jfc_value_7)
    END
    IF (?popcalendar:7{PROP:Hide} = 0) THEN
        Disable(?popcalendar:7)
    END
    IF (?JFC7_Button{PROP:Hide} = 0) THEN
        Disable(?jfc7_Button)
    END

    IF (?jfc_prompt_8{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_8)
    END
    IF (?jfc_value_8{PROP:Hide} = 0) THEN
        Disable(?jfc_value_8)
    END
    IF (?popcalendar:8{PROP:Hide} = 0) THEN
        Disable(?popcalendar:8)
    END
    IF (?JFC8_Button{PROP:Hide} = 0) THEN
        Disable(?jfc8_Button)
    END

    IF (?jfc_prompt_9{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_9)
    END
    IF (?jfc_value_9{PROP:Hide} = 0) THEN
        Disable(?jfc_value_9)
    END
    IF (?popcalendar:9{PROP:Hide} = 0) THEN
        Disable(?popcalendar:9)
    END
    IF (?JFC9_Button{PROP:Hide} = 0) THEN
        Disable(?jfc9_Button)
    END

    IF (?jfc_prompt_10{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_10)
    END
    IF (?jfc_value_10{PROP:Hide} = 0) THEN
        Disable(?jfc_value_10)
    END
    IF (?popcalendar:10{PROP:Hide} = 0) THEN
        Disable(?popcalendar:10)
    END
    IF (?JFC10_Button{PROP:Hide} = 0) THEN
        Disable(?jfc10_Button)
    END

    IF (?jfc_prompt_11{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_11)
    END
    IF (?jfc_value_11{PROP:Hide} = 0) THEN
        Disable(?jfc_value_11)
    END
    IF (?popcalendar:11{PROP:Hide} = 0) THEN
        Disable(?popcalendar:11)
    END
    IF (?JFC11_Button{PROP:Hide} = 0) THEN
        Disable(?jfc11_Button)
    END

    IF (?jfc_prompt_12{PROP:Hide} = 0) THEN
        Disable(?jfc_prompt_12)
    END
    IF (?jfc_value_12{PROP:Hide} = 0) THEN
        Disable(?jfc_value_12)
    END
    IF (?popcalendar:12{PROP:Hide} = 0) THEN
        Disable(?popcalendar:12)
    END
    IF (?JFC12_Button{PROP:Hide} = 0) THEN
        Disable(?jfc12_Button)
    END
Enable_QA_Panel ROUTINE
    Enable(?QAGroup)
Disable_QA_Panel    ROUTINE
    Disable(?QAGroup)
Enable_Bottom_Panel ROUTINE
    Enable(?AllocateCommonFaultsButton)
    Enable(?RemovePartsButton)
    Enable(?CompleteJobButton)
    Enable(?ChangeStatusButton)
    Enable(?ViewJobButton)
    Enable(?AllocateJobButton)
    ! Start Change 3288 BE(23/09/03)
    Enable(?BookinLabelButton)
    ! Start Change 3689 BE(12/12/03)
    Enable(?PickButton)
    IF (job:completed = 'YES') THEN
        Enable(?CompletedLabelButton)
        Enable(?Prompt30)
        Enable(?Prompt31)
    END
    ! End Change 3689 BE(12/12/03)
    Enable(?Prompt28)
    Enable(?Prompt29)
    ! Start Change 3689 BE(12/12/03)
    !Enable(?Prompt30)
    !Enable(?Prompt31)
    ! End Change 3689 BE(12/12/03)
    ! End Change 3288 BE(23/09/03)
    !Enable(?OKButton)
    !Enable(?CancelButton)
    Enable(?Prompt14)
    Enable(?Prompt15)
    Enable(?Prompt16)
    Enable(?Prompt17)
    Enable(?Prompt18)
    Enable(?Prompt19)
    Enable(?Prompt20)
    Enable(?Prompt21)
    Enable(?Prompt32)

Disable_Bottom_Panel    ROUTINE
    Disable(?AllocateCommonFaultsButton)
    Disable(?RemovePartsButton)
    Disable(?CompleteJobButton)
    Disable(?ChangeStatusButton)
    Disable(?ViewJobButton)
    Disable(?AllocateJobButton)
    ! Start Change 3288 BE(23/09/03)
    Disable(?BookinLabelButton)
    Disable(?CompletedLabelButton)
    Disable(?PickButton)
    Disable(?Prompt28)
    Disable(?Prompt29)
    Disable(?Prompt30)
    Disable(?Prompt31)
    ! End Change 3288 BE(23/09/03)
    !Disable(?OKButton)
    Disable(?Prompt14)
    Disable(?Prompt15)
    Disable(?Prompt16)
    Disable(?Prompt17)
    Disable(?Prompt18)
    Disable(?Prompt19)
    Disable(?Prompt20)
    Disable(?Prompt21)
    Disable(?Prompt32)
Enable_Parts    ROUTINE
    IF (job:Current_Status[1:3] = '320') THEN
        IF ((job:chargeable_job = 'YES') AND(job:charge_type <> '')) THEN
            Enable(?ChargeablePartsTab)
            Enable(?temp:Charge_Part_Number)
            Enable(?AddPartPrompt)
            Unhide(?PartInsert)
            Unhide(?PartAdjustment)
            Unhide(?PartChange)
            Unhide(?PartDelete)

            Enable(?List)        ! crc bug 13/10/03
            BRW4.ResetSort(1)     ! crc bug 13/10/03
        END
        IF ((job:warranty_job = 'YES') AND (job:warranty_charge_type <> '')) THEN
            Enable(?WarrantyPartsTab)
            Enable(?temp:Warranty_Part_Number)
            Enable(?AddWarrantyPrompt)
            Unhide(?WarInsert)
            Unhide(?WarAdjustment)
            Unhide(?WarChange)
            Unhide(?WarDelete)

            Enable(?List:2)        ! crc bug 13/10/03
            BRW19.ResetSort(1)      ! crc bug 13/10/03
        END
    END
Disable_Parts   ROUTINE
    !Disable(?ChargeablePartsTab)
    !Disable(?temp:Charge_Part_Number)
    !Disable(?List)
    !Disable(?AddPartPrompt)
    !ide(?PartInsert)
    !Hide(?PartAdjustment)
    !Hide(?PartChange)
    !ide(?PartDelete)
    !isable(?WarrantyPartsTab)
    !Disable(?temp:Warranty_Part_Number)
    !isable(?List:2)
    !Hide(?WarInsert)
    !Hide(?WarAdjustment)
    !Hide(?WarChange)
    !Hide(?WarDelete)

    DO Disable_Chargeable_Parts 
    DO Disable_Warranty_Parts 
Disable_Chargeable_Parts   ROUTINE
    Disable(?ChargeablePartsTab)
    Disable(?temp:Charge_Part_Number)
    Disable(?List)
    Disable(?AddPartPrompt)
    Hide(?PartInsert)
    Hide(?PartAdjustment)
    Hide(?PartChange)
    Hide(?PartDelete)
Disable_Warranty_Parts   ROUTINE
    Disable(?List:2)
    Disable(?WarrantyPartsTab)
    Disable(?temp:Warranty_Part_Number)
    Disable(?AddWarrantyPrompt)
    Hide(?WarInsert)
    Hide(?WarAdjustment)
    Hide(?WarChange)
    Hide(?WarDelete)
Show_Job_Fault_Codes   ROUTINE

    HIDE(?jfc_prompt_1)
    HIDE(?jfc_value_1)
    HIDE(?popcalendar)
    HIDE(?JFC1_Button)

    HIDE(?jfc_prompt_2)
    HIDE(?jfc_value_2)
    HIDE(?popcalendar:2)
    HIDE(?JFC2_Button)

    HIDE(?jfc_prompt_3)
    HIDE(?jfc_value_3)
    HIDE(?popcalendar:3)
    HIDE(?JFC3_Button)

    HIDE(?jfc_prompt_4)
    HIDE(?jfc_value_4)
    HIDE(?popcalendar:4)
    HIDE(?JFC4_Button)

    HIDE(?jfc_prompt_5)
    HIDE(?jfc_value_5)
    HIDE(?popcalendar:5)
    HIDE(?JFC5_Button)

    HIDE(?jfc_prompt_6)
    HIDE(?jfc_value_6)
    HIDE(?popcalendar:6)
    HIDE(?JFC6_Button)

    HIDE(?jfc_prompt_7)
    HIDE(?jfc_value_7)
    HIDE(?popcalendar:7)
    HIDE(?JFC7_Button)

    HIDE(?jfc_prompt_8)
    HIDE(?jfc_value_8)
    HIDE(?popcalendar:8)
    HIDE(?JFC8_Button)

    HIDE(?jfc_prompt_9)
    HIDE(?jfc_value_9)
    HIDE(?popcalendar:9)
    HIDE(?JFC9_Button)

    HIDE(?jfc_prompt_10)
    HIDE(?jfc_value_10)
    HIDE(?popcalendar:10)
    HIDE(?JFC10_Button)

    HIDE(?jfc_prompt_11)
    HIDE(?jfc_value_11)
    HIDE(?popcalendar:11)
    HIDE(?JFC11_Button)

    HIDE(?jfc_prompt_12)
    HIDE(?jfc_value_12)
    HIDE(?popcalendar:12)
    HIDE(?JFC12_Button)

    ?jfc_value_1{PROP:Req} = 0
    ?jfc_value_2{PROP:Req} = 0
    ?jfc_value_3{PROP:Req} = 0
    ?jfc_value_4{PROP:Req} = 0
    ?jfc_value_5{PROP:Req} = 0
    ?jfc_value_6{PROP:Req} = 0
    ?jfc_value_7{PROP:Req} = 0
    ?jfc_value_8{PROP:Req} = 0
    ?jfc_value_9{PROP:Req} = 0
    ?jfc_value_10{PROP:Req} = 0
    ?jfc_value_11{PROP:Req} = 0
    ?jfc_value_12{PROP:Req} = 0

    ?jfc_value_1{PROP:READONLY} = 0
    ?jfc_value_2{PROP:READONLY} = 0
    ?jfc_value_3{PROP:READONLY} = 0
    ?jfc_value_4{PROP:READONLY} = 0
    ?jfc_value_5{PROP:READONLY} = 0
    ?jfc_value_6{PROP:READONLY} = 0
    ?jfc_value_7{PROP:READONLY} = 0
    ?jfc_value_8{PROP:READONLY} = 0
    ?jfc_value_9{PROP:READONLY} = 0
    ?jfc_value_10{PROP:READONLY} = 0
    ?jfc_value_11{PROP:READONLY} = 0
    ?jfc_value_11{PROP:READONLY} = 0

    ?jfc_value_1{PROP:COLOR} = COLOR:White
    ?jfc_value_2{PROP:COLOR} = COLOR:White
    ?jfc_value_3{PROP:COLOR} = COLOR:White
    ?jfc_value_4{PROP:COLOR} = COLOR:White
    ?jfc_value_5{PROP:COLOR} = COLOR:White
    ?jfc_value_6{PROP:COLOR} = COLOR:White
    ?jfc_value_7{PROP:COLOR} = COLOR:White
    ?jfc_value_8{PROP:COLOR} = COLOR:White
    ?jfc_value_9{PROP:COLOR} = COLOR:White
    ?jfc_value_10{PROP:COLOR} = COLOR:White
    ?jfc_value_11{PROP:COLOR} = COLOR:White
    ?jfc_value_11{PROP:COLOR} = COLOR:White

    jfc_value[1]     = CLIP(job:Fault_Code1)
    jfc_value[2]     = CLIP(job:Fault_Code2)
    jfc_value[3]     = CLIP(job:Fault_Code3)
    jfc_value[4]     = CLIP(job:Fault_Code4)
    jfc_value[5]     = CLIP(job:Fault_Code5)
    jfc_value[6]     = CLIP(job:Fault_Code6)
    jfc_value[7]     = CLIP(job:Fault_Code7)
    jfc_value[8]     = CLIP(job:Fault_Code8)
    jfc_value[9]     = CLIP(job:Fault_Code9)
    jfc_value[10]    = CLIP(job:Fault_Code10)
    jfc_value[11]    = CLIP(job:Fault_Code11)
    jfc_value[12]    = CLIP(job:Fault_Code12)

    Clear(jfc_prompt)
    Clear(jfc_force_lookup)

    CompFaultCoding# = 0
    access:chartype.clearkey(cha:charge_type_key)
    cha:charge_type = job:Warranty_Charge_Type
    IF (access:chartype.tryfetch(cha:charge_type_key) = Level:Benign) THEN
        IF (cha:force_warranty = 'YES') THEN
            IF (job:Repair_Type_Warranty <> '') THEN
                Access:REPTYDEF.ClearKey(rtd:Warranty_Key)
                rtd:Warranty    = 'YES'
                rtd:Repair_Type = job:Repair_Type_Warranty
                IF ((Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign) AND rtd:CompFaultCoding) THEN
                    CompFaultCoding# = 1
                END
            ELSE
                CompFaultCoding# = 1
            END
        END
    END

    save_manfault = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = job:Manufacturer
    SET(maf:field_number_key,maf:field_number_key)
    LOOP
        IF ((access:manfault.next() <> Level:Benign) OR (maf:manufacturer <> job:Manufacturer)) THEN
           BREAK
        END

        req#  = 0
        reqbook# = 0

        IF ((maf:Compulsory_At_Booking = 'YES') AND CompFaultCoding#) THEN
            req# = 1
            reqbook# = 1
        END

        IF ((maf:Compulsory = 'YES') and CompFaultCoding#) THEN
            req# = 1
        END

        IF (maf:lookup = 'YES') THEN
            field"  = ''
            records# = 0
            save_manfaulo = access:manfaulo.savefile()
            access:manfaulo.clearkey(mfo:field_key)
            mfo:manufacturer = maf:manufacturer
            mfo:field_number = maf:field_number
            SET(mfo:field_key,mfo:field_key)
            LOOP
                IF ((access:manfaulo.next() <> Level:Benign) OR |
                     (mfo:manufacturer <> maf:manufacturer)   OR   |
                     (mfo:field_number <> maf:field_number))  THEN
                    BREAK
                END
                records# += 1
                IF (records# > 1) THEN
                    field" = ''
                    BREAK
                END
                field"  = mfo:field
            END
            access:manfaulo.restorefile(save_manfaulo)
            IF (jfc_value[maf:field_number] = '') THEN
                jfc_value[maf:field_number] = field"
            END
        END

        CASE maf:field_type
        OF 'DATE'
            Show_Edit(maf:field_number, maf:field_name, req#, reqbook#, maf:DateType, 64, 1, 'NO', 'NO')
        OF 'STRING'
            IF (maf:restrictlength) THEN
                Show_Edit(maf:field_number, maf:field_name, req#, reqbook#, '@s' & maf:lengthto, 104, 0, maf:lookup, maf:force_lookup)
            ELSE
                Show_Edit(maf:field_number, maf:field_name, req#, reqbook#, '@s30', 104, 0, maf:lookup, maf:force_lookup)
            END
        OF 'NUMBER'
            IF (maf:restrictlength) THEN
                Show_Edit(maf:field_number, maf:field_name, req#, reqbook#, '@n' & maf:lengthto, 64, 0, maf:lookup, maf:force_lookup)
            ELSE
                Show_Edit(maf:field_number, maf:field_name, req#, reqbook#, '@n9', 64, 0, maf:lookup, maf:force_lookup)
            END
        ELSE   ! Assume String
            IF (maf:restrictlength) THEN
                Show_Edit(maf:field_number, maf:field_name, req#, reqbook#, '@s' & maf:lengthto, 104, 0, maf:lookup, maf:force_lookup)
            ELSE
                Show_Edit(maf:field_number, maf:field_name, req#, reqbook#, '@s30', 104, 0, maf:lookup, maf:force_lookup)
            END
        END

    END
    access:manfault.restorefile(save_manfault)

    IF (job:manufacturer ='NOKIA' AND jfc_value[9] <> '0' AND jfc_value[9] <> '1' AND |
        jfc_value[9] <> '2' AND jfc_value[9] <> '3' AND jfc_value[9] <> '4' AND jfc_value[9] <> '') THEN

        HIDE(?JFC1_Button)
        HIDE(?JFC2_Button)
        HIDE(?JFC3_Button)
        HIDE(?JFC4_Button)
        HIDE(?JFC5_Button)
        HIDE(?JFC6_Button)
        HIDE(?JFC7_Button)
        HIDE(?JFC8_Button)
        HIDE(?JFC9_Button)
        HIDE(?JFC11_Button)
        HIDE(?JFC11_Button)
        HIDE(?JFC11_Button)

        ?jfc_value_1{PROP:READONLY} = 0
        ?jfc_value_2{PROP:READONLY} = 0
        ?jfc_value_3{PROP:READONLY} = 0
        ?jfc_value_4{PROP:READONLY} = 0
        ?jfc_value_5{PROP:READONLY} = 0
        ?jfc_value_6{PROP:READONLY} = 0
        ?jfc_value_7{PROP:READONLY} = 0
        ?jfc_value_8{PROP:READONLY} = 0
        ?jfc_value_9{PROP:READONLY} = 0
        ?jfc_value_10{PROP:READONLY} = 0
        ?jfc_value_11{PROP:READONLY} = 0
        ?jfc_value_11{PROP:READONLY} = 0

        ?jfc_value_1{PROP:COLOR} = COLOR:White
        ?jfc_value_2{PROP:COLOR} = COLOR:White
        ?jfc_value_3{PROP:COLOR} = COLOR:White
        ?jfc_value_4{PROP:COLOR} = COLOR:White
        ?jfc_value_5{PROP:COLOR} = COLOR:White
        ?jfc_value_6{PROP:COLOR} = COLOR:White
        ?jfc_value_7{PROP:COLOR} = COLOR:White
        ?jfc_value_8{PROP:COLOR} = COLOR:White
        ?jfc_value_9{PROP:COLOR} = COLOR:White
        ?jfc_value_10{PROP:COLOR} = COLOR:White
        ?jfc_value_11{PROP:COLOR} = COLOR:White
        ?jfc_value_11{PROP:COLOR} = COLOR:White

        IF (old_prompt[1] = '') THEN
            HIDE(?jfc_value_1)
            jfc_prompt[1] = ''
        ELSE
            UNHIDE(?jfc_value_1)
            jfc_prompt[1] = old_prompt[1]
        END

        IF (old_prompt[2] = '') THEN
            HIDE(?jfc_value_2)
            jfc_prompt[2] = ''
        ELSE
            UNHIDE(?jfc_value_2)
            jfc_prompt[2] = old_prompt[2]
        END

        IF (old_prompt[3] = '') THEN
            HIDE(?jfc_value_3)
            jfc_prompt[3] = ''
        ELSE
            UNHIDE(?jfc_value_3)
            jfc_prompt[3] = old_prompt[3]
        END

        IF (old_prompt[4] = '') THEN
            HIDE(?jfc_value_4)
            jfc_prompt[4] = ''
        ELSE
            UNHIDE(?jfc_value_4)
            jfc_prompt[4] = old_prompt[4]
        END

        IF (old_prompt[5] = '') THEN
            HIDE(?jfc_value_5)
            jfc_prompt[5] = ''
        ELSE
            UNHIDE(?jfc_value_5)
            jfc_prompt[5] = old_prompt[5]
        END

        IF (old_prompt[6] = '') THEN
            HIDE(?jfc_value_6)
            jfc_prompt[6] = ''
        ELSE
            UNHIDE(?jfc_value_6)
            jfc_prompt[6] = old_prompt[6]
        END

        IF (old_prompt[7] = '') THEN
            HIDE(?jfc_value_7)
            jfc_prompt[7] = ''
        ELSE
            UNHIDE(?jfc_value_7)
            jfc_prompt[7] = old_prompt[7]
        END

        IF (old_prompt[8] = '') THEN
            HIDE(?jfc_value_8)
            jfc_prompt[8] = ''
        ELSE
            UNHIDE(?jfc_value_8)
            jfc_prompt[8] = old_prompt[8]
        END

        IF (old_prompt[9] = '') THEN
            HIDE(?jfc_value_9)
            jfc_prompt[9] = ''
        ELSE
            UNHIDE(?jfc_value_9)
            jfc_prompt[9] = old_prompt[9]
        END

        IF (old_prompt[10] = '') THEN
            HIDE(?jfc_value_10)
            jfc_prompt[10] = ''
        ELSE
            UNHIDE(?jfc_value_10)
            jfc_prompt[10] = old_prompt[10]
        END

        IF (old_prompt[11] = '') THEN
            HIDE(?jfc_value_11)
            jfc_prompt[11] = ''
        ELSE
            UNHIDE(?jfc_value_11)
            jfc_prompt[11] = old_prompt[11]
        END

        IF (old_prompt[12] = '') THEN
            HIDE(?jfc_value_12)
            jfc_prompt[12] = ''
        ELSE
            UNHIDE(?jfc_value_12)
            jfc_prompt[12] = old_prompt[12]
        END

    END

    DO Show_Trade_Fault_Codes

    temp:charge_type = job:charge_type
    temp:repair_type = job:repair_type
    temp:warranty_charge_type =  job:warranty_charge_type
    temp:repair_type_warranty = job:repair_type_warranty






Show_Trade_Fault_Codes  ROUTINE

    HIDE(?tfc_prompt_1)
    HIDE(?tfc_value_1)
    HIDE(?popcalendar:13)
    HIDE(?tfc1_Button)

    HIDE(?tfc_prompt_2)
    HIDE(?tfc_value_2)
    HIDE(?popcalendar:14)
    HIDE(?tfc2_Button)

    HIDE(?tfc_prompt_3)
    HIDE(?tfc_value_3)
    HIDE(?popcalendar:15)
    HIDE(?tfc3_Button)

    HIDE(?tfc_prompt_4)
    HIDE(?tfc_value_4)
    HIDE(?popcalendar:16)
    HIDE(?tfc4_Button)

    HIDE(?tfc_prompt_5)
    HIDE(?tfc_value_5)
    HIDE(?popcalendar:17)
    HIDE(?tfc5_Button)

    HIDE(?tfc_prompt_6)
    HIDE(?tfc_value_6)
    HIDE(?popcalendar:18)
    HIDE(?tfc6_Button)

    HIDE(?tfc_prompt_7)
    HIDE(?tfc_value_7)
    HIDE(?popcalendar:19)
    HIDE(?tfc7_Button)

    HIDE(?tfc_prompt_8)
    HIDE(?tfc_value_8)
    HIDE(?popcalendar:20)
    HIDE(?tfc8_Button)

    HIDE(?tfc_prompt_9)
    HIDE(?tfc_value_9)
    HIDE(?popcalendar:21)
    HIDE(?tfc9_Button)

    HIDE(?tfc_prompt_10)
    HIDE(?tfc_value_10)
    HIDE(?popcalendar:22)
    HIDE(?tfc10_Button)

    HIDE(?tfc_prompt_11)
    HIDE(?tfc_value_11)
    HIDE(?popcalendar:23)
    HIDE(?tfc11_Button)

    HIDE(?tfc_prompt_12)
    HIDE(?tfc_value_12)
    HIDE(?popcalendar:24)
    HIDE(?tfc12_Button)

    ?tfc_value_1{PROP:Req} = 0
    ?tfc_value_2{PROP:Req} = 0
    ?tfc_value_3{PROP:Req} = 0
    ?tfc_value_4{PROP:Req} = 0
    ?tfc_value_5{PROP:Req} = 0
    ?tfc_value_6{PROP:Req} = 0
    ?tfc_value_7{PROP:Req} = 0
    ?tfc_value_8{PROP:Req} = 0
    ?tfc_value_9{PROP:Req} = 0
    ?tfc_value_10{PROP:Req} = 0
    ?tfc_value_11{PROP:Req} = 0
    ?tfc_value_12{PROP:Req} = 0

    ?tfc_value_1{PROP:READONLY} = 0
    ?tfc_value_2{PROP:READONLY} = 0
    ?tfc_value_3{PROP:READONLY} = 0
    ?tfc_value_4{PROP:READONLY} = 0
    ?tfc_value_5{PROP:READONLY} = 0
    ?tfc_value_6{PROP:READONLY} = 0
    ?tfc_value_7{PROP:READONLY} = 0
    ?tfc_value_8{PROP:READONLY} = 0
    ?tfc_value_9{PROP:READONLY} = 0
    ?tfc_value_10{PROP:READONLY} = 0
    ?tfc_value_11{PROP:READONLY} = 0
    ?tfc_value_11{PROP:READONLY} = 0

    ?tfc_value_1{PROP:COLOR} = COLOR:White
    ?tfc_value_2{PROP:COLOR} = COLOR:White
    ?tfc_value_3{PROP:COLOR} = COLOR:White
    ?tfc_value_4{PROP:COLOR} = COLOR:White
    ?tfc_value_5{PROP:COLOR} = COLOR:White
    ?tfc_value_6{PROP:COLOR} = COLOR:White
    ?tfc_value_7{PROP:COLOR} = COLOR:White
    ?tfc_value_8{PROP:COLOR} = COLOR:White
    ?tfc_value_9{PROP:COLOR} = COLOR:White
    ?tfc_value_10{PROP:COLOR} = COLOR:White
    ?tfc_value_11{PROP:COLOR} = COLOR:White
    ?tfc_value_11{PROP:COLOR} = COLOR:White

    tfc_value[1]     = CLIP(jobe:TraFaultCode1)
    tfc_value[2]     = CLIP(jobe:TraFaultCode2)
    tfc_value[3]     = CLIP(jobe:TraFaultCode3)
    tfc_value[4]     = CLIP(jobe:TraFaultCode4)
    tfc_value[5]     = CLIP(jobe:TraFaultCode5)
    tfc_value[6]     = CLIP(jobe:TraFaultCode6)
    tfc_value[7]     = CLIP(jobe:TraFaultCode7)
    tfc_value[8]     = CLIP(jobe:TraFaultCode8)
    tfc_value[9]     = CLIP(jobe:TraFaultCode9)
    tfc_value[10]    = CLIP(jobe:TraFaultCode10)
    tfc_value[11]    = CLIP(jobe:TraFaultCode11)
    tfc_value[12]    = CLIP(jobe:TraFaultCode12)

    Clear(tfc_prompt)
    Clear(tfc_force_lookup)

    access:subtracc.clearkey(sub:account_number_key)
    sub:account_number = job:account_number
    access:subtracc.fetch(sub:account_number_key)
    access:tradeacc.clearkey(tra:account_number_key)
    tra:account_number = sub:main_account_number
    access:tradeacc.fetch(tra:account_number_key)

    save_trafault = access:trafault.savefile()
    access:trafault.clearkey(taf:field_number_key)
    taf:accountnumber = tra:Account_Number
    SET(taf:field_number_key,taf:field_number_key)
    LOOP
        IF ((access:trafault.next() <> Level:Benign) OR (taf:accountnumber <> tra:Account_Number)) THEN
           BREAK
        END

        req#  = 0
        reqbook# = 0

        IF (taf:Compulsory_At_Booking = 'YES') THEN
            req# = 1
            reqbook# = 1
        END

        IF (taf:Compulsory = 'YES') THEN
            req# = 1
        END

        IF (taf:lookup = 'YES') THEN
            field"  = ''
            records# = 0
            save_trafaulo = access:trafaulo.savefile()
            access:trafaulo.clearkey(tfo:field_key)
            tfo:accountnumber = taf:accountnumber
            tfo:field_number = taf:field_number
            SET(tfo:field_key,tfo:field_key)
            LOOP
                IF ((access:trafaulo.next() <> Level:Benign) OR |
                     (tfo:accountnumber <> taf:accountnumber)   OR   |
                     (tfo:field_number <> taf:field_number))  THEN
                    BREAK
                END
                records# += 1
                IF (records# > 1) THEN
                    field" = ''
                    BREAK
                END
                field"  = mfo:field
            END
            access:trafaulo.restorefile(save_trafaulo)
            IF (tfc_value[taf:field_number] = '') THEN
                tfc_value[taf:field_number] = field"
            END
        END

        CASE taf:field_type
        OF 'DATE'
            ! Note: Trade Account Fault Codes do not have a value for the Date Format as manufacturer Fault Codes do.
            ! The code in GenericFaultCodes (SBF01APP) uses the man:datetype here but the value of this field depends
            ! upon the last record read and is likely to be spurious.  Have adopted an immutable value of @d6 (dd/mm/yyyy) here
            ! to get around this problem.  Note, however, that is very likely to produce a different result than that
            ! generated by GenericfaultCodes (whicj itself is very unrelible not to say unpredictable)..
            Show_Edit2(taf:field_number, taf:field_name, req#, reqbook#, '@d6', 64, 1, 'NO', 'NO')
        OF 'STRING'
            IF (taf:restrictlength) THEN
                Show_Edit2(taf:field_number, taf:field_name, req#, reqbook#, '@s' & taf:lengthto, 104, 0, taf:lookup, taf:force_lookup)
            ELSE
                Show_Edit2(taf:field_number, taf:field_name, req#, reqbook#, '@s30', 104, 0, taf:lookup, taf:force_lookup)
            END
        OF 'NUMBER'
            IF (taf:restrictlength) THEN
                Show_Edit2(taf:field_number, taf:field_name, req#, reqbook#, '@n' & taf:lengthto, 64, 0, taf:lookup, taf:force_lookup)
            ELSE
                Show_Edit2(taf:field_number, taf:field_name, req#, reqbook#, '@n9', 64, 0, taf:lookup, taf:force_lookup)
            END
        ELSE   ! Assume String
            IF (taf:restrictlength) THEN
                Show_Edit2(taf:field_number, taf:field_name, req#, reqbook#, '@s' & taf:lengthto, 104, 0, taf:lookup, taf:force_lookup)
            ELSE
                Show_Edit2(taf:field_number, taf:field_name, req#, reqbook#, '@s30', 104, 0, taf:lookup, taf:force_lookup)
            END
        END

    END
    access:trafault.restorefile(save_trafault)
Pricing     ROUTINE
    IF (job:invoice_number <> '') THEN
        job:labour_cost = job:invoice_labour_cost
        job:parts_cost  = job:invoice_parts_cost
    ELSE
        Pricing_Routine('C',labour",parts",pass",a")
        IF (pass" = True) THEN
            job:labour_cost = labour"
            job:parts_cost  = parts"
        ELSE
            job:labour_cost = 0
            job:parts_cost  = 0
        END
    END
    IF (job:invoice_number_warranty <> '') THEN
        job:labour_cost_warranty = JOB:WInvoice_Labour_Cost
        job:parts_cost_warranty  = JOB:WInvoice_Parts_Cost
    ELSE
        Pricing_Routine('W',labour",parts",pass",a")
        IF (pass" = True) THEN
            job:labour_cost_warranty    = labour"
            job:parts_cost_warranty     = parts"
        ELSE
            job:labour_cost_warranty = 0
            job:parts_cost_warranty = 0
        END
    END
CompleteJob  ROUTINE
     !To avoid not changing to the right status,
     !change to completed, first, and then it should
     !Change again to one of the below status's if necessary
     GetStatus(705,1,'JOB')
     IF (ExchangeAccount(job:Account_Number)) THEN
        !Is the Trade Account used for Exchange Units
        !if so, then there's no need to despatch the
        !unit normal. Just auto fill the despatch fields
        !and mark the job as to return to exchange stock.
        ForceDespatch
        GetStatus(707,1,'JOB')
    ELSE
        !Will the job be restocked, i.e. has, or will have, an
        !Exchange Unit attached, or is it a normal despatch.
        restock# = 0
        IF (ExchangeAccount(job:Account_Number)) THEN
            restock# = 1
        ELSE
            glo:select1  = job:ref_number
            IF (ToBeExchanged()) THEN
                !Has this unit got an Exchange, or is it expecting an exchange
                restock# = 1
            END
            !If there is an loan unit attached, assume that a despatch note is required.
            IF (job:loan_unit_number <> '') THEN
                restock# = 0
            END
        END

        IF (CompletePrintDespatch(job:Account_Number) = Level:Benign) THEN
            Print_Despatch_Note_Temp# = 1
        END

        IF (restock# = 0) THEN
            !Check the Accounts' "Despatch Paid/Invoiced" jobs ticks
            !If either of those are ticked then don't despatch yet
            !print_despatch_note_temp = ''
            IF (AccountAllowedToDespatch()) THEN
                !Do we need to despatch at completion?
                IF (DespatchAtClosing()) THEN
                    !Does the Trade Account have "Skip Despatch" set?
                    IF (AccountSkipDespatch(job:Account_Number) = Level:Benign) THEN
                        access:courier.clearkey(cou:courier_key)
                        cou:courier = job:courier
                        IF (access:courier.tryfetch(cou:courier_key)) THEN
                            MessageEx('Cannot find the Courier attached to this job.','ServiceBase 2000',|
                                      'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                       beep:systemhand,msgex:samewidths,84,26,0)
                        ELSE
                            !Give the job a unique Despatch Number
                            !then call the despsing.inc.
                            !This calls the relevant exports etc, and different
                            !despatch procedures depending on the Courier.
                            !It will then print a Despatch Note and change
                            !The status as required.
                            IF (access:desbatch.primerecord() = Level:Benign) THEN
                                IF (access:desbatch.tryinsert() <> Level:Benign) THEN
                                    access:desbatch.cancelautoinc()
                                ELSE
                                    DespatchSingle()
                                    Print_Despatch_Note_Temp# = 0
                                END
                            END
                        END
                    ELSE
                        GetStatus(705,1,'JOB')
                    END
                ELSE
                    IF (job:Loan_Unit_Number <> '') THEN
                        GetStatus(811,1,'JOB')
                    ELSE
                        GetStatus(705,1,'JOB')
                    END
                END
            ELSE
                GetStatus(705,1,'JOB')
            END
        ELSE
            IF (job:Exchange_Unit_Number <> '') THEN
                !Are either of the Chargeable or Warranty repair types BERs?
                !If so, then don't change the status to 707 because it shouldn't
                !be returned to the Exchange Stock.
                Comp# = 0
                IF (job:Chargeable_Job = 'YES') THEN
                    IF (BERRepairType(job:Repair_Type) = Level:Benign) THEN
                        Comp# = 1
                    END
                END
                IF (job:Warranty_Job = 'YES' and Comp# = 0) THEN
                    IF (BERRepairType(job:Repair_Type_Warranty) = Level:Benign) THEN
                        Comp# = 1
                    END
                END
                IF (Comp# = 1) THEN
                    MessageEx('This unit has been exchanged but is a B.E.R. Repair.<13,10>DO NOT place this unit into Exchange Stock.','ServiceBase 2000',|
                              'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                               beep:systemexclamation,msgex:samewidths,84,26,0)
                    GetStatus(705,1,'JOB')
                    Print_Despatch_Note_Temp# = 0
                ELSE
                    MessageEx('This unit has been exchanged. It should now be placed into Exchange Stock.','ServiceBase 2000',|
                              'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                              beep:systemasterisk,msgex:samewidths,84,26,0)
                    GetStatus(707,1,'JOB')
                END
            ELSE
                GetStatus(705,1,'JOB')
            END
        END

        !Does the Account care about Bouncers?
        Check_For_Bouncers_Temp# = 1
        IF (CompleteCheckForBouncer(job:Account_Number)) THEN
            Check_For_Bouncers_Temp# = 0
        END

    END

    job:date_completed = Today()
    job:time_completed = Clock()
    job:completed = 'YES'

    ! Start Change 3689 BE(12/12/03)
    Enable(?CompletedLabelButton)
    ! End Change 3689 BE(12/12/03)

    IF (Check_For_Bouncers_Temp#) THEN
        !Check if the job is a Bouncer and add
        !it to the Bouncer List
        IF (CountBouncer(job:Ref_Number,job:Date_Booked,job:ESN)) THEN
            AddBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
        END
    END

    !Check if this job is an EDI job
    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    Mod:Model_Number    = job:Model_Number
    IF (Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign) THEN
        job:edi = PendingJob(mod:Manufacturer)
        job:Manufacturer    = mod:Manufacturer
    !ELSE
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    END

    !Mark the job's exchange unit as being available
    CompleteDoExchange(job:Exchange_Unit_Number)

    DO Update_Record

    IF (ExchangeAccount(job:Account_Number)) THEN
        MessageEx('This is an Exchange Repair. Please re-stock the unit.','ServiceBase 2000',|
                  'Styles\warn.ico','&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                   beep:systemexclamation,msgex:samewidths,84,26,0)
    END

    IF (print_despatch_note_temp#) THEN
        IF (restock# = 1) THEN
            restocking_note
        ELSE
            despatch_note
        END
        glo:select1  = ''
    END

    Complete_JobLabel(job:ref_number)
Auto_Allocate_Job    ROUTINE
    IF (job:Engineer <> temp_usercode) THEN
        job:Engineer = temp_usercode
        GetStatus(310,1,'JOB')    ! Allocated to Engineer

        ! Start Change 3602 BE(26/11/03)
        IF (Access:JOBSENG.PrimeRecord() = Level:Benign) THEN
            joe:JobNumber     = job:Ref_Number
            joe:UserCode      = job:Engineer
            joe:DateAllocated = Today()
            joe:TimeAllocated = Clock()
            joe:AllocatedBy   = temp_usercode
            access:users.clearkey(use:User_Code_Key)
            use:User_Code   = job:Engineer
            access:users.fetch(use:User_Code_Key)

            joe:EngSkillLevel = use:SkillLevel
            joe:JobSkillLevel = jobe:SkillLevel

            IF (Access:JOBSENG.TryInsert() <> Level:Benign) THEN
                access:JOBSENG.cancelautoinc()
            END
        END
        ! Start Change 3602 BE(26/11/03)

    END
InitMaxParts    ROUTINE    !Start Change 1895 BE(15/05/03)
    tmpCheckChargeableParts = 0
    TotalPartsCost = 0.0
    access:SUBTRACC.clearkey(sub:account_number_key)
    sub:Account_Number  = job:Account_Number
    IF (access:SUBTRACC.tryfetch(sub:account_number_key) = Level:Benign) THEN
        access:TRADEACC.clearkey(tra:account_number_key)
        tra:Account_Number  = sub:Main_Account_Number
        IF (access:TRADEACC.tryfetch(tra:account_number_key) = Level:Benign) THEN
            TmpCheckChargeableParts = tra:CheckChargeablePartsCost
        END
    END
!End Change 1895 BE(15/05/03)
TotalChargeableParts    ROUTINE ! Start Change 1895 BE(15/05/03)
    TotalPartsCost = 0
    save_par_id2 = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    SET(par:Part_Number_Key,par:Part_Number_Key)
    LOOP
        IF ((Access:PARTS.NEXT() <> Level:Benign) OR |
            (par:Ref_Number  <> job:Ref_Number)) THEN
            BREAK
        END
        IF (par:Adjustment <> 'YES') THEN
            TotalPartsCost += par:Purchase_Cost * par:Quantity
        END
    END
    Access:PARTS_ALIAS.RestoreFile(save_par_id2)
! End Change 1895 BE(15/05/03)
MakePartsOrder      Routine
    If Access:ORDPEND.PrimeRecord() = Level:Benign
        ope:Part_Ref_Number = sto:Ref_number
        ope:Job_Number      = job:Ref_number
        ope:Part_Type       = 'JOB'
        ope:Supplier        = sto:Supplier
        ope:Part_Number     = sto:Part_Number
        ope:Description     = sto:Description
        ope:Quantity        = 1
        ope:Account_Number  = job:Account_Number
        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !If Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
MakeWarPartsOrder      Routine
    If Access:ORDPEND.PrimeRecord() = Level:Benign
        ope:Part_Ref_Number = sto:Ref_number
        ope:Job_Number      = job:Ref_number
        ope:Part_Type       = 'WAR'
        ope:Supplier        = sto:Supplier
        ope:Part_Number     = sto:Part_Number
        ope:Description     = sto:Description
        ope:Quantity        = 1
        ope:Account_Number  = job:Account_Number
        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !If Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
Add_Warranty_Part     Routine
    Clear(wpr:Record)
    get(warparts,0)
    if access:warparts.primerecord() = level:benign
        wpr:ref_number           = job:ref_number
        wpr:adjustment           = 'NO'
        wpr:part_number     = sto:part_number
        wpr:description     = sto:description
        wpr:supplier        = sto:supplier
        wpr:purchase_cost   = sto:purchase_cost
        wpr:sale_cost       = sto:sale_cost
        wpr:retail_cost     = sto:retail_cost
        wpr:quantity             = 1
        wpr:warranty_part        = 'NO'
        wpr:exclude_from_order   = 'NO'
        wpr:fault_codes_checked  = 'YES'
        wpr:part_ref_Number      = sto:ref_number
        wpr:date_ordered         = Today()
        IF (sto:assign_fault_codes = 'YES') THEN
            wpr:fault_code1    = sto:fault_code1
            wpr:fault_code2    = sto:fault_code2
            wpr:fault_code3    = sto:fault_code3
            wpr:fault_code4    = sto:fault_code4
            wpr:fault_code5    = sto:fault_code5
            wpr:fault_code6    = sto:fault_code6
            wpr:fault_code7    = sto:fault_code7
            wpr:fault_code8    = sto:fault_code8
            wpr:fault_code9    = sto:fault_code9
            wpr:fault_code10   = sto:fault_code10
            wpr:fault_code11   = sto:fault_code11
            wpr:fault_code12   = sto:fault_code12
        ELSE
            wpr:fault_code1    = pfc:FaultCode1
            wpr:fault_code2    = pfc:FaultCode2
            wpr:fault_code3    = pfc:FaultCode3
            wpr:fault_code4    = pfc:FaultCode4
            wpr:fault_code5    = pfc:FaultCode5
            wpr:fault_code6    = pfc:FaultCode6
            wpr:fault_code7    = pfc:FaultCode7
            wpr:fault_code8    = pfc:FaultCode8
            wpr:fault_code9    = pfc:FaultCode9
            wpr:fault_code10   = pfc:FaultCode10
            wpr:fault_code11   = pfc:FaultCode11
            wpr:fault_code12   = pfc:FaultCode12
        END
        if access:warparts.insert()
            access:warparts.cancelautoinc()
        end
    end!if access:warparts.primerecord() = level:benign

Add_Chargeable_Part     Routine
    Clear(par:record)
    get(parts,0)
    if access:parts.primerecord() = level:benign
        par:ref_number           = job:ref_number
        par:adjustment           = 'NO'
        par:part_number     = sto:part_number
        par:description     = sto:description
        par:supplier        = sto:supplier
        par:purchase_cost   = sto:purchase_cost
        par:sale_cost       = sto:sale_cost
        par:retail_cost     = sto:retail_cost
        par:quantity             = 1
        par:warranty_part        = 'NO'
        par:exclude_from_order   = 'NO'
        par:fault_codes_checked  = 'YES'
        par:part_ref_Number      = sto:ref_number
        par:date_ordered         = Today()
        IF (sto:assign_fault_codes = 'YES') THEN
            par:fault_code1    = sto:fault_code1
            par:fault_code2    = sto:fault_code2
            par:fault_code3    = sto:fault_code3
            par:fault_code4    = sto:fault_code4
            par:fault_code5    = sto:fault_code5
            par:fault_code6    = sto:fault_code6
            par:fault_code7    = sto:fault_code7
            par:fault_code8    = sto:fault_code8
            par:fault_code9    = sto:fault_code9
            par:fault_code10   = sto:fault_code10
            par:fault_code11   = sto:fault_code11
            par:fault_code12   = sto:fault_code12
        ELSE
            par:fault_code1    = pfc:FaultCode1
            par:fault_code2    = pfc:FaultCode2
            par:fault_code3    = pfc:FaultCode3
            par:fault_code4    = pfc:FaultCode4
            par:fault_code5    = pfc:FaultCode5
            par:fault_code6    = pfc:FaultCode6
            par:fault_code7    = pfc:FaultCode7
            par:fault_code8    = pfc:FaultCode8
            par:fault_code9    = pfc:FaultCode9
            par:fault_code10   = pfc:FaultCode10
            par:fault_code11   = pfc:FaultCode11
            par:fault_code12   = pfc:FaultCode12
        END
        if access:parts.insert()
            access:parts.cancelautoinc()
        end
    end!if access:warparts.primerecord() = level:benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Voda_ONR_Process',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('IsOnTest',IsOnTest,'Voda_ONR_Process',1)
    SolaceViewVars('MSN_Field_Mask',MSN_Field_Mask,'Voda_ONR_Process',1)
    SolaceViewVars('technician_string',technician_string,'Voda_ONR_Process',1)
    SolaceViewVars('TotalPartsCost',TotalPartsCost,'Voda_ONR_Process',1)
    SolaceViewVars('tmpCheckChargeableParts',tmpCheckChargeableParts,'Voda_ONR_Process',1)
    SolaceViewVars('tmp_RF_Board_IMEI',tmp_RF_Board_IMEI,'Voda_ONR_Process',1)
    SolaceViewVars('IsPass',IsPass,'Voda_ONR_Process',1)
    SolaceViewVars('IsFail',IsFail,'Voda_ONR_Process',1)
    SolaceViewVars('QA_Type',QA_Type,'Voda_ONR_Process',1)
    SolaceViewVars('handset_type_temp',handset_type_temp,'Voda_ONR_Process',1)
    SolaceViewVars('Failure_Reason',Failure_Reason,'Voda_ONR_Process',1)
    SolaceViewVars('ActivePanel',ActivePanel,'Voda_ONR_Process',1)
    SolaceViewVars('RecordState',RecordState,'Voda_ONR_Process',1)
    SolaceViewVars('Temp_IMEI_Number',Temp_IMEI_Number,'Voda_ONR_Process',1)
    SolaceViewVars('Temp_Usercode',Temp_Usercode,'Voda_ONR_Process',1)
    SolaceViewVars('savejobs',savejobs,'Voda_ONR_Process',1)
    SolaceViewVars('savejobse',savejobse,'Voda_ONR_Process',1)
    SolaceViewVars('savejobnotes',savejobnotes,'Voda_ONR_Process',1)
    SolaceViewVars('save_manfault',save_manfault,'Voda_ONR_Process',1)
    SolaceViewVars('save_manfaulo',save_manfaulo,'Voda_ONR_Process',1)
    SolaceViewVars('save_trafault',save_trafault,'Voda_ONR_Process',1)
    SolaceViewVars('save_trafaulo',save_trafaulo,'Voda_ONR_Process',1)
    SolaceViewVars('save_map_id',save_map_id,'Voda_ONR_Process',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Voda_ONR_Process',1)
    SolaceViewVars('save_lac_id',save_lac_id,'Voda_ONR_Process',1)
    SolaceViewVars('save_par_id',save_par_id,'Voda_ONR_Process',1)
    SolaceViewVars('save_par_id2',save_par_id2,'Voda_ONR_Process',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Voda_ONR_Process',1)
    SolaceViewVars('SaveBookmark',SaveBookmark,'Voda_ONR_Process',1)
    SolaceViewVars('warpartpntstore',warpartpntstore,'Voda_ONR_Process',1)
    SolaceViewVars('partpntstore',partpntstore,'Voda_ONR_Process',1)
    
      Loop SolaceDim1# = 1 to 12
        SolaceFieldName" = 'old_prompt' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",old_prompt[SolaceDim1#],'Voda_ONR_Process',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 12
        SolaceFieldName" = 'jfc_prompt' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",jfc_prompt[SolaceDim1#],'Voda_ONR_Process',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 12
        SolaceFieldName" = 'jfc_value' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",jfc_value[SolaceDim1#],'Voda_ONR_Process',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 12
        SolaceFieldName" = 'jfc_force_lookup' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",jfc_force_lookup[SolaceDim1#],'Voda_ONR_Process',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 12
        SolaceFieldName" = 'tfc_prompt' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tfc_prompt[SolaceDim1#],'Voda_ONR_Process',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 12
        SolaceFieldName" = 'tfc_value' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tfc_value[SolaceDim1#],'Voda_ONR_Process',1)
      End
    
    
    
      Loop SolaceDim1# = 1 to 12
        SolaceFieldName" = 'tfc_force_lookup' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",tfc_force_lookup[SolaceDim1#],'Voda_ONR_Process',1)
      End
    
    
    SolaceViewVars('save_charge_type',save_charge_type,'Voda_ONR_Process',1)
    SolaceViewVars('save_repair_type',save_repair_type,'Voda_ONR_Process',1)
    SolaceViewVars('tmp:End_Days',tmp:End_Days,'Voda_ONR_Process',1)
    SolaceViewVars('tmp:End_Hours',tmp:End_Hours,'Voda_ONR_Process',1)
    SolaceViewVars('tmp:PreviousStatus',tmp:PreviousStatus,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode1',PartFaultCodes:FaultCode1,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode2',PartFaultCodes:FaultCode2,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode3',PartFaultCodes:FaultCode3,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode4',PartFaultCodes:FaultCode4,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode5',PartFaultCodes:FaultCode5,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode6',PartFaultCodes:FaultCode6,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode7',PartFaultCodes:FaultCode7,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode8',PartFaultCodes:FaultCode8,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode9',PartFaultCodes:FaultCode9,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode10',PartFaultCodes:FaultCode10,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode11',PartFaultCodes:FaultCode11,'Voda_ONR_Process',1)
    SolaceViewVars('PartFaultCodes:FaultCode12',PartFaultCodes:FaultCode12,'Voda_ONR_Process',1)
    SolaceViewVars('temprec:Account_Number',temprec:Account_Number,'Voda_ONR_Process',1)
    SolaceViewVars('temprec:Order_Number',temprec:Order_Number,'Voda_ONR_Process',1)
    SolaceViewVars('temprec:job_reference',temprec:job_reference,'Voda_ONR_Process',1)
    SolaceViewVars('temprec:Charge_Part_Number',temprec:Charge_Part_Number,'Voda_ONR_Process',1)
    SolaceViewVars('temprec:Warranty_Part_Number',temprec:Warranty_Part_Number,'Voda_ONR_Process',1)
    SolaceViewVars('temprec:charge_type',temprec:charge_type,'Voda_ONR_Process',1)
    SolaceViewVars('temprec:repair_type',temprec:repair_type,'Voda_ONR_Process',1)
    SolaceViewVars('temprec:warranty_charge_type',temprec:warranty_charge_type,'Voda_ONR_Process',1)
    SolaceViewVars('temprec:repair_type_warranty',temprec:repair_type_warranty,'Voda_ONR_Process',1)
    SolaceViewVars('ONR_Defaults:location',ONR_Defaults:location,'Voda_ONR_Process',1)
    SolaceViewVars('ONR_Defaults:account_number',ONR_Defaults:account_number,'Voda_ONR_Process',1)
    SolaceViewVars('ONR_Defaults:transit_type',ONR_Defaults:transit_type,'Voda_ONR_Process',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?TestAndRepairSheet;  SolaceCtrlName = '?TestAndRepairSheet';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TestAndRepairTab;  SolaceCtrlName = '?TestAndRepairTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Fault_Description;  SolaceCtrlName = '?jbn:Fault_Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FaultDescriptionPrompt;  SolaceCtrlName = '?FaultDescriptionPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PickFaultDescriptionButton;  SolaceCtrlName = '?PickFaultDescriptionButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EngineersNotesPrompt;  SolaceCtrlName = '?EngineersNotesPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PickEngineersNotesButton;  SolaceCtrlName = '?PickEngineersNotesButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InvoiceTextPrompt;  SolaceCtrlName = '?InvoiceTextPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PickInvoiceTextButton;  SolaceCtrlName = '?PickInvoiceTextButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Engineers_Notes;  SolaceCtrlName = '?jbn:Engineers_Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Invoice_Text;  SolaceCtrlName = '?jbn:Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Chargeable_Job;  SolaceCtrlName = '?job:Chargeable_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?temp:charge_type;  SolaceCtrlName = '?temp:charge_type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChargeButton;  SolaceCtrlName = '?ChargeButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?temp:repair_type_warranty;  SolaceCtrlName = '?temp:repair_type_warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarrantyTypeButton;  SolaceCtrlName = '?WarrantyTypeButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IsOnTest;  SolaceCtrlName = '?IsOnTest';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IsPass;  SolaceCtrlName = '?IsPass';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IsFail;  SolaceCtrlName = '?IsFail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QAGroup;  SolaceCtrlName = '?QAGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QAFailButton;  SolaceCtrlName = '?QAFailButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QAPassButton;  SolaceCtrlName = '?QAPassButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt23;  SolaceCtrlName = '?Prompt23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?temp:warranty_charge_type;  SolaceCtrlName = '?temp:warranty_charge_type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChargeTypePrompt;  SolaceCtrlName = '?ChargeTypePrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?temp:repair_type;  SolaceCtrlName = '?temp:repair_type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChargeTypeButton;  SolaceCtrlName = '?ChargeTypeButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Warranty_Job;  SolaceCtrlName = '?job:Warranty_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?warrantyButton;  SolaceCtrlName = '?warrantyButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarrantyTypePrompt;  SolaceCtrlName = '?WarrantyTypePrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MFCTab;  SolaceCtrlName = '?MFCTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_1;  SolaceCtrlName = '?jfc_prompt_1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_1;  SolaceCtrlName = '?jfc_value_1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC1_Button;  SolaceCtrlName = '?JFC1_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_2;  SolaceCtrlName = '?jfc_prompt_2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_2;  SolaceCtrlName = '?jfc_value_2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC2_Button;  SolaceCtrlName = '?JFC2_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_3;  SolaceCtrlName = '?jfc_prompt_3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_3;  SolaceCtrlName = '?jfc_value_3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC3_Button;  SolaceCtrlName = '?JFC3_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_4;  SolaceCtrlName = '?jfc_prompt_4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_4;  SolaceCtrlName = '?jfc_value_4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC4_Button;  SolaceCtrlName = '?JFC4_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_5;  SolaceCtrlName = '?jfc_prompt_5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_5;  SolaceCtrlName = '?jfc_value_5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC5_Button;  SolaceCtrlName = '?JFC5_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_6;  SolaceCtrlName = '?jfc_prompt_6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_6;  SolaceCtrlName = '?jfc_value_6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC6_Button;  SolaceCtrlName = '?JFC6_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_7;  SolaceCtrlName = '?jfc_prompt_7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_7;  SolaceCtrlName = '?jfc_value_7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC7_Button;  SolaceCtrlName = '?JFC7_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_8;  SolaceCtrlName = '?jfc_prompt_8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_8;  SolaceCtrlName = '?jfc_value_8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC8_Button;  SolaceCtrlName = '?JFC8_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_9;  SolaceCtrlName = '?jfc_prompt_9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_9;  SolaceCtrlName = '?jfc_value_9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC9_Button;  SolaceCtrlName = '?JFC9_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_10;  SolaceCtrlName = '?jfc_prompt_10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_10;  SolaceCtrlName = '?jfc_value_10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC10_Button;  SolaceCtrlName = '?JFC10_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_11;  SolaceCtrlName = '?jfc_prompt_11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_11;  SolaceCtrlName = '?jfc_value_11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC11_Button;  SolaceCtrlName = '?JFC11_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_prompt_12;  SolaceCtrlName = '?jfc_prompt_12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jfc_value_12;  SolaceCtrlName = '?jfc_value_12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JFC12_Button;  SolaceCtrlName = '?JFC12_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFCTab;  SolaceCtrlName = '?TFCTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC1_Button;  SolaceCtrlName = '?TFC1_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:24;  SolaceCtrlName = '?PopCalendar:24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_1;  SolaceCtrlName = '?tfc_value_1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:13;  SolaceCtrlName = '?PopCalendar:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_1;  SolaceCtrlName = '?tfc_prompt_1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_2;  SolaceCtrlName = '?tfc_prompt_2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_2;  SolaceCtrlName = '?tfc_value_2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC2_Button;  SolaceCtrlName = '?TFC2_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:14;  SolaceCtrlName = '?PopCalendar:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_3;  SolaceCtrlName = '?tfc_prompt_3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_3;  SolaceCtrlName = '?tfc_value_3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC3_Button;  SolaceCtrlName = '?TFC3_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:15;  SolaceCtrlName = '?PopCalendar:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_8;  SolaceCtrlName = '?tfc_prompt_8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_8;  SolaceCtrlName = '?tfc_value_8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC7_Button;  SolaceCtrlName = '?TFC7_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_4;  SolaceCtrlName = '?tfc_prompt_4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_4;  SolaceCtrlName = '?tfc_value_4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC4_Button;  SolaceCtrlName = '?TFC4_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_9;  SolaceCtrlName = '?tfc_prompt_9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_9;  SolaceCtrlName = '?tfc_value_9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC8_Button;  SolaceCtrlName = '?TFC8_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:21;  SolaceCtrlName = '?PopCalendar:21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:18;  SolaceCtrlName = '?PopCalendar:18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_5;  SolaceCtrlName = '?tfc_prompt_5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_5;  SolaceCtrlName = '?tfc_value_5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC5_Button;  SolaceCtrlName = '?TFC5_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:16;  SolaceCtrlName = '?PopCalendar:16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_10;  SolaceCtrlName = '?tfc_prompt_10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_10;  SolaceCtrlName = '?tfc_value_10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:22;  SolaceCtrlName = '?PopCalendar:22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:19;  SolaceCtrlName = '?PopCalendar:19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC9_Button;  SolaceCtrlName = '?TFC9_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC11_Button;  SolaceCtrlName = '?TFC11_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:23;  SolaceCtrlName = '?PopCalendar:23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:20;  SolaceCtrlName = '?PopCalendar:20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_6;  SolaceCtrlName = '?tfc_prompt_6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_6;  SolaceCtrlName = '?tfc_value_6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_11;  SolaceCtrlName = '?tfc_prompt_11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_11;  SolaceCtrlName = '?tfc_value_11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC10_Button;  SolaceCtrlName = '?TFC10_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_7;  SolaceCtrlName = '?tfc_prompt_7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_7;  SolaceCtrlName = '?tfc_value_7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:17;  SolaceCtrlName = '?PopCalendar:17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC6_Button;  SolaceCtrlName = '?TFC6_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_prompt_12;  SolaceCtrlName = '?tfc_prompt_12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfc_value_12;  SolaceCtrlName = '?tfc_value_12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFC12_Button;  SolaceCtrlName = '?TFC12_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChargeablePartsTab;  SolaceCtrlName = '?ChargeablePartsTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AddPartPrompt;  SolaceCtrlName = '?AddPartPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?temp:Charge_Part_Number;  SolaceCtrlName = '?temp:Charge_Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PartInsert;  SolaceCtrlName = '?PartInsert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PartAdjustment;  SolaceCtrlName = '?PartAdjustment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PartChange;  SolaceCtrlName = '?PartChange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PartDelete;  SolaceCtrlName = '?PartDelete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarrantyPartsTab;  SolaceCtrlName = '?WarrantyPartsTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AddWarrantyPrompt;  SolaceCtrlName = '?AddWarrantyPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?temp:Warranty_Part_Number;  SolaceCtrlName = '?temp:Warranty_Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarInsert;  SolaceCtrlName = '?WarInsert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarAdjustment;  SolaceCtrlName = '?WarAdjustment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarChange;  SolaceCtrlName = '?WarChange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarDelete;  SolaceCtrlName = '?WarDelete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BookInSheet;  SolaceCtrlName = '?BookInSheet';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BookInTab;  SolaceCtrlName = '?BookInTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22;  SolaceCtrlName = '?Prompt22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?temp:Account_Number;  SolaceCtrlName = '?temp:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PickAccountButton;  SolaceCtrlName = '?PickAccountButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?temp:Order_Number;  SolaceCtrlName = '?temp:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt25;  SolaceCtrlName = '?Prompt25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?temp:job_reference;  SolaceCtrlName = '?temp:job_reference';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ESN;  SolaceCtrlName = '?job:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:MSN;  SolaceCtrlName = '?job:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Model_Number;  SolaceCtrlName = '?job:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PickModelButton;  SolaceCtrlName = '?PickModelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Surname;  SolaceCtrlName = '?job:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name_Delivery;  SolaceCtrlName = '?job:Company_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1_Delivery;  SolaceCtrlName = '?job:Address_Line1_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2_Delivery;  SolaceCtrlName = '?job:Address_Line2_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3_Delivery;  SolaceCtrlName = '?job:Address_Line3_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode_Delivery;  SolaceCtrlName = '?job:Postcode_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Workshop;  SolaceCtrlName = '?job:Workshop';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:InWorkshopDate;  SolaceCtrlName = '?jobe:InWorkshopDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:InWorkshopTime;  SolaceCtrlName = '?jobe:InWorkshopTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Location;  SolaceCtrlName = '?job:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PickLocationButton;  SolaceCtrlName = '?PickLocationButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BookOKButton;  SolaceCtrlName = '?BookOKButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AllocateCommonFaultsButton;  SolaceCtrlName = '?AllocateCommonFaultsButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14;  SolaceCtrlName = '?Prompt14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChangeStatusButton;  SolaceCtrlName = '?ChangeStatusButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt15;  SolaceCtrlName = '?Prompt15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CompleteJobButton;  SolaceCtrlName = '?CompleteJobButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PickButton;  SolaceCtrlName = '?PickButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ViewJobButton;  SolaceCtrlName = '?ViewJobButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RemovePartsButton;  SolaceCtrlName = '?RemovePartsButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16;  SolaceCtrlName = '?Prompt16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SerialNumberPrompt;  SolaceCtrlName = '?SerialNumberPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Temp_IMEI_Number;  SolaceCtrlName = '?Temp_IMEI_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AllocateJobButton;  SolaceCtrlName = '?AllocateJobButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt18;  SolaceCtrlName = '?Prompt18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt19;  SolaceCtrlName = '?Prompt19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt30;  SolaceCtrlName = '?Prompt30';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CompletedLabelButton;  SolaceCtrlName = '?CompletedLabelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt21;  SolaceCtrlName = '?Prompt21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BookinLabelButton;  SolaceCtrlName = '?BookinLabelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt29;  SolaceCtrlName = '?Prompt29';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt31;  SolaceCtrlName = '?Prompt31';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt20;  SolaceCtrlName = '?Prompt20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt28;  SolaceCtrlName = '?Prompt28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17;  SolaceCtrlName = '?Prompt17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt32;  SolaceCtrlName = '?Prompt32';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OKButton;  SolaceCtrlName = '?OKButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobCompletePanel;  SolaceCtrlName = '?JobCompletePanel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Voda_ONR_Process')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Voda_ONR_Process')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?jbn:Fault_Description
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCESDEF.Open
  Relate:COMMCAT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:DEFSTOCK.Open
  Relate:DESBATCH.Open
  Relate:DISCOUNT.Open
  Relate:ESNMODEL.Open
  Relate:EXCHANGE.Open
  Relate:JOBS2_ALIAS.Open
  Relate:NETWORKS.Open
  Relate:PARTS_ALIAS.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Relate:REPTYDEF.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WARPARTS_ALIAS.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:SUBTRACC.UseFile
  Access:MODELNUM.UseFile
  Access:JOBNOTES.UseFile
  Access:ESTPARTS.UseFile
  Access:LOCINTER.UseFile
  Access:AUDIT.UseFile
  Access:TRADEACC.UseFile
  Access:UNITTYPE.UseFile
  Access:COURIER.UseFile
  Access:JOBSTAGE.UseFile
  Access:COMMONFA.UseFile
  Access:JOBSENG.UseFile
  Access:CHARTYPE.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAUPA.UseFile
  Access:LOANACC.UseFile
  Access:JOBACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:EXCHHIST.UseFile
  Access:LOAN.UseFile
  Access:LOANHIST.UseFile
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:COMMONCP.UseFile
  Access:COMMONWP.UseFile
  Access:STOMODEL.UseFile
  Access:LOCATION.UseFile
  Access:MODELCOL.UseFile
  Access:TRAFAULT.UseFile
  Access:TRAFAULO.UseFile
  Access:REPAIRTY.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHAR.UseFile
  Access:ORDPEND.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:PARTS,SELF)
  BRW19.Init(?List:2,Queue:Browse:1.ViewPosition,BRW19::View:Browse,Queue:Browse:1,Relate:WARPARTS,SELF)
  OPEN(window)
  SELF.Opened=True
  DO Disable_Bookin_Panel
  DO Disable_TestAndRepair_Panel
  DO Disable_QA_Panel
  DO Disable_Bottom_Panel
  Disable(?CancelButton)
  
  RecordState = rs:NoRecord
  ActivePanel = ap:NoPanel
  
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  temp_usercode = use:user_code
  
  ! Start Change xxxx BE(22/03/04)
  ! added for Picking Note calls bug
  Set(Defaults)
  access:Defaults.next()
  ! End Change xxxx BE(22/03/04)
  
  old_prompt[1]   = GETINI('FAULTCODE1','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[2]   = GETINI('FAULTCODE2','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[3]   = GETINI('FAULTCODE3','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[4]   = GETINI('FAULTCODE4','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[5]   = GETINI('FAULTCODE5','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[6]   = GETINI('FAULTCODE6','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[7]   = GETINI('FAULTCODE7','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[8]   = GETINI('FAULTCODE8','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[9]   = GETINI('FAULTCODE9','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[10]   = GETINI('FAULTCODE10','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[11]   = GETINI('FAULTCODE11','Name',,CLIP(PATH())&'\NFAULTS.INI')
  old_prompt[12]   = GETINI('FAULTCODE12','Name',,CLIP(PATH())&'\NFAULTS.INI')
  
  
  
  
  
  
  
  
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  BRW4.Q &= Queue:Browse
  BRW4.AddSortOrder(,par:Part_Number_Key)
  BRW4.AddRange(par:Ref_Number,Relate:PARTS,Relate:JOBS)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,par:Part_Number,1,BRW4)
  BRW4.AddField(par:Part_Number,BRW4.Q.par:Part_Number)
  BRW4.AddField(par:Description,BRW4.Q.par:Description)
  BRW4.AddField(par:Quantity,BRW4.Q.par:Quantity)
  BRW4.AddField(par:Record_Number,BRW4.Q.par:Record_Number)
  BRW4.AddField(par:Ref_Number,BRW4.Q.par:Ref_Number)
  BRW19.Q &= Queue:Browse:1
  BRW19.AddSortOrder(,wpr:Part_Number_Key)
  BRW19.AddRange(wpr:Ref_Number,Relate:WARPARTS,Relate:JOBS)
  BRW19.AddLocator(BRW19::Sort0:Locator)
  BRW19::Sort0:Locator.Init(,wpr:Part_Number,1,BRW19)
  BRW19.AddField(wpr:Part_Number,BRW19.Q.wpr:Part_Number)
  BRW19.AddField(wpr:Description,BRW19.Q.wpr:Description)
  BRW19.AddField(wpr:Quantity,BRW19.Q.wpr:Quantity)
  BRW19.AddField(wpr:Record_Number,BRW19.Q.wpr:Record_Number)
  BRW19.AddField(wpr:Ref_Number,BRW19.Q.wpr:Ref_Number)
  BRW4.AskProcedure = 1
  BRW19.AskProcedure = 2
  BRW4.AddToolbarTarget(Toolbar)
  BRW19.AddToolbarTarget(Toolbar)
  SELECT(?ChargeablePartsTab)
  SELECT(?MFCTab)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCESDEF.Close
    Relate:COMMCAT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:DEFSTOCK.Close
    Relate:DESBATCH.Close
    Relate:DISCOUNT.Close
    Relate:ESNMODEL.Close
    Relate:EXCHANGE.Close
    Relate:JOBS2_ALIAS.Close
    Relate:NETWORKS.Close
    Relate:PARTS_ALIAS.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
    Relate:REPTYDEF.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WARPARTS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Voda_ONR_Process',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
      continue# = 1
      If job:model_number = ''
          MessageEx('You must select a Model Number before you can insert any parts.','ServiceBase 2000',|
                         'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
          continue# = 0
      Else !If job:model_number = ''
          If job:charge_type = '' And (Field() = ?PartInsert Or Field() = ?PartChange Or Field() = ?PartDelete)
              MessageEx('You must select a Charge Type before you can insert any parts.','ServiceBase 2000',|
                             'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                             beep:systemhand,msgex:samewidths,84,26,0)
              continue# = 0
          End
          If job:warranty_charge_type = '' And continue# = 1 And (Field() = ?WarInsert Or Field() = ?WarChange Or Field() = ?WarDelete)
              MessageEx('You must select a Warranty Charge Type before you can insert any parts.','ServiceBase 2000',|
                             'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                             beep:systemhand,msgex:samewidths,84,26,0)
              continue# = 0
          End
  
      End !If job:model_number = ''
  
      If Field() = ?PartInsert Or Field() = ?WarInsert Or Field() = ?PartDelete or Field() = ?WarDelete
          If job:date_completed <> ''
              Check_Access('AMEND COMPLETED JOBS',x")
              If x" = False
                  MessageEx('Cannot Insert/Delete parts!<13,10><13,10>This job has been completed.','ServiceBase 2000',|
                                 'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                 beep:systemhand,msgex:samewidths,84,26,0)
                  continue# = 0
              End
          End
          If job:invoice_Number <> ''
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  If request = InsertRecord Or request = DeleteRecord
                      continue# = 0
                      If Field() = ?PartInsert
                          MessageEx('Cannot Insert/Delete parts!<13,10><13,10>The Chargeable part of this job has been invoiced.','ServiceBase 2000',|
                                         'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                         beep:systemhand,msgex:samewidths,84,26,0)
                      End!If number = 1
                      If Field() = ?WarInsert
                          MessageEx('Cannot Insert/Delete parts!<13,10><13,10>The Warranty part of this job has been invoiced.','ServiceBase 2000',|
                                         'Styles\stop.ico','',0,0,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                         beep:systemhand,msgex:samewidths,84,26,0)
                      End!If number = 1
  
                  End!If request = InsertRecord Or request = DeleteRecord
  
              End
          End
  
      End !If Field() = ?Insert Or Field() = ?Insert:3 Or Field() = ?Delete or Field() = ?Delete:3
  
  
      If continue# = 1
  
          If Field() = ?PartInsert
              glo:select11 = job:manufacturer
              glo:select12 = job:model_number
          End !If Field() = ?Insert
          If Field() = ?PartDelete
              !Stock Order
              If DeleteChargeablePart()
                  !Begin deletion of pick detail entry TH
                  par:Quantity = 0 !reset since it is about to be deleted - can utilise
                  if def:PickNoteNormal or def:PickNoteMainStore then
                      ChangePickingPart('chargeable')
                      DeletePickingParts('chargeable',0)
                  end
                  ! Start Change 2116 BE(26/06/03)
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = par:Part_Ref_Number
                  IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign) THEN
                       !access:JOBSE.clearkey(jobe:RefNumberKey)
                       !jobe:RefNumber  = job:Ref_Number
                       !IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                       !    IF (access:JOBSE.primerecord() = Level:Benign) THEN
                       !        jobe:RefNumber  = job:Ref_Number
                       !        IF (access:JOBSE.tryinsert()) THEN
                       !            access:JOBSE.cancelautoinc()
                       !        END
                       !    END
                       !END
                       IF ((sto:RF_BOARD) AND (jobe:Pre_RF_Board_IMEI <> '')) THEN
                          GET(audit,0)
                          IF (access:audit.primerecord() = level:benign) THEN
                              aud:notes         = 'RF Board deleted from job.<13,10>New IMEI ' & Clip(jobe:Pre_RF_Board_IMEI) & |
                                                  '<13,10>Old IMEI ' & Clip(job:esn)
                              aud:ref_number    = job:ref_number
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(jobe:Pre_RF_Board_IMEI)
                              if access:audit.insert()
                                  access:audit.cancelautoinc()
                              end
                          END
                          job:ESN = jobe:Pre_RF_Board_IMEI
                          jobe:Pre_RF_Board_IMEI = ''
                          access:jobse.update()
                          access:jobs.update()
                          DISPLAY(?job:ESN)
                      END
                  END
                  ! End Change 2116 BE(26/06/03)
                  Delete(PARTS)
              End !If DeleteChargeablePart()
          End!If Field() = ?Delete
  
          If Field() = ?WarInsert
              glo:select11 = job:manufacturer
              glo:select12 = job:model_number
          End !If Field() = ?Insert:3
          If Field() = ?WarDelete
              If DeleteWarrantyPart()
                  !Begin deletion of pick detail entry TH
                  wpr:Quantity = 0 !reset since it is about to be deleted - can utilise
                  if def:PickNoteNormal or def:PickNoteMainStore then
                      ChangePickingPart('warranty')
                      DeletePickingParts('warranty',0)
                  end
                  ! Start Change 2116 BE(26/06/03)
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = wpr:Part_Ref_Number
                  IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign) THEN
                       !access:JOBSE.clearkey(jobe:RefNumberKey)
                       !jobe:RefNumber  = job:Ref_Number
                       !IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                       !    IF (access:JOBSE.primerecord() = Level:Benign) THEN
                       !        jobe:RefNumber  = job:Ref_Number
                       !        IF (access:JOBSE.tryinsert()) THEN
                       !            access:JOBSE.cancelautoinc()
                       !        END
                       !   END
                       !ND
                       IF ((sto:RF_BOARD) AND (jobe:Pre_RF_Board_IMEI <> '')) THEN
                          GET(audit,0)
                          IF (access:audit.primerecord() = level:benign) THEN
                              aud:notes         = 'RF Board deleted from job.<13,10>New IMEI ' & Clip(jobe:Pre_RF_Board_IMEI) & |
                                                  '<13,10>Old IMEI ' & Clip(job:esn)
                              aud:ref_number    = job:ref_number
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(jobe:Pre_RF_Board_IMEI)
                              if access:audit.insert()
                                  access:audit.cancelautoinc()
                              end
                          END
                          job:ESN = jobe:Pre_RF_Board_IMEI
                          jobe:Pre_RF_Board_IMEI = ''
                          access:jobse.update()
                          access:jobs.update()
                          DISPLAY(?job:ESN)
                      END
                  END
                  ! End Change 2116 BE(26/06/03)
                  Delete(WARPARTS)
              End !If DeleteWarrantyPart()
          End !If Field() = Delete:3
  
          If request <> DeleteRecord
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_Parts
      Update_Warranty_Part
    END
    ReturnValue = GlobalResponse
  END
        End !If request <> DeleteRecord
  
        If (Field() = ?PartInsert Or Field() = ?PartChange Or Field() = ?PartDelete)
  
            if def:PickNoteNormal or def:PickNoteMainStore then
                if request = insertrecord and returnvalue = requestcompleted then
                    CreatePickingNote (sto:Location)
                    InsertPickingPart ('chargeable')
                end
                if request = changerecord and returnvalue = requestcompleted then
                    ChangePickingPart ('chargeable')
                end
            end
  
            If glo:select1 = 'NEW PENDING'
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = par:part_ref_number
                If access:stock.fetch(sto:ref_number_key)
                    beep(beep:systemhand)  ;  yield()
                    message('An Error has occured retrieving the details of this Stock Part.', |
                            'ServiceBase 2000', icon:hand)
                Else!If access:stock.fetch(sto:ref_number_key)
                    If Access:STOHIST.Primerecord() = Level:Benign
                        shi:Ref_Number              = sto:Ref_Number
                        shi:Transaction_Type        = 'DEC'
                        shi:Despatch_Note_Number    = par:Despatch_Note_Number
                        shi:Quantity                = sto:Quantity_Stock
                        shi:Date                    = Today()
                        shi:Purchase_Cost           = par:Purchase_Cost
                        shi:Sale_Cost               = par:Sale_Cost
                        shi:Job_Number              = job:Ref_Number
                        Access:USERS.Clearkey(use:Password_Key)
                        use:Password                = glo:Password
                        Access:USERS.Tryfetch(use:Password_Key)
                        shi:User                    = use:User_Code
                        shi:Notes                   = 'STOCK DECREMENTED'
                        shi:Information             = ''
                        If Access:STOHIST.Tryinsert()
                            Access:STOHIST.Cancelautoinc()
                        End!If Access:STOHIST.Tryinsert()
                    End!If Access:STOHIST.Primerecord() = Level:Benign
    
                    sto:quantity_stock     = 0
                    access:stock.update()
    
                    access:parts_alias.clearkey(par_ali:RefPartRefNoKey)
                    par_ali:ref_number      = job:ref_number
                    par_ali:part_ref_number = glo:select2
                    If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
                        par:ref_number           = par_ali:ref_number
                        par:adjustment           = par_ali:adjustment
                        par:part_ref_number      = par_ali:part_ref_number
    
                        par:part_number     = par_ali:part_number
                        par:description     = par_ali:description
                        par:supplier        = par_ali:supplier
                        par:purchase_cost   = par_ali:purchase_cost
                        par:sale_cost       = par_ali:sale_cost
                        par:retail_cost     = par_ali:retail_cost
                        par:quantity             = glo:select3
                        par:warranty_part        = 'NO'
                        par:exclude_from_order   = 'NO'
                        par:despatch_note_number = ''
                        par:date_ordered         = ''
                        par:date_received        = ''
                        par:pending_ref_number   = glo:select4
                        par:order_number         = ''
                        par:fault_code1    = par_ali:fault_code1
                        par:fault_code2    = par_ali:fault_code2
                        par:fault_code3    = par_ali:fault_code3
                        par:fault_code4    = par_ali:fault_code4
                        par:fault_code5    = par_ali:fault_code5
                        par:fault_code6    = par_ali:fault_code6
                        par:fault_code7    = par_ali:fault_code7
                        par:fault_code8    = par_ali:fault_code8
                        par:fault_code9    = par_ali:fault_code9
                        par:fault_code10   = par_ali:fault_code10
                        par:fault_code11   = par_ali:fault_code11
                        par:fault_code12   = par_ali:fault_code12
                        access:parts.insert()
                    end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign
                end !if access:stock.fetch(sto:ref_number_key = Level:Benign
            End!If glo:select1 = 'NEW PENDING'
        End
        If (Field() = ?WarInsert Or Field() = ?WarChange Or Field() = ?WarDelete)
  
            if def:PickNoteNormal or def:PickNoteMainStore then
                if request = insertrecord and returnvalue = requestcompleted then
                    CreatePickingNote (sto:Location)
                    InsertPickingPart ('warranty')
                end
                if request = changerecord and returnvalue = requestcompleted then
                    ChangePickingPart ('warranty')
                end
            end
  
            If glo:select1 = 'NEW PENDING'
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = wpr:part_ref_number
                If access:stock.fetch(sto:ref_number_key)
                    beep(beep:systemhand)  ;  yield()
                    message('An Error has occured retrieving the details of this Stock Part.', |
                            'ServiceBase 2000', icon:hand)
                Else!If access:stock.fetch(sto:ref_number_key)
                    If Access:STOHIST.Primerecord() = Level:Benign
                        shi:Ref_Number              = sto:Ref_Number
                        shi:Transaction_Type        = 'DEC'
                        shi:Despatch_Note_Number    = wpr:Despatch_Note_Number
                        shi:Quantity                = sto:Quantity_Stock
                        shi:Date                    = Today()
                        shi:Purchase_Cost           = wpr:Purchase_Cost
                        shi:Sale_Cost               = wpr:Sale_Cost
                        shi:Job_Number              = job:Ref_Number
                        Access:USERS.Clearkey(use:Password_Key)
                        use:Password                = glo:Password
                        Access:USERS.Tryfetch(use:Password_Key)
                        shi:User                    = use:User_Code
                        shi:Notes                   = 'STOCK DECREMENTED'
                        shi:Information             = ''
                        If Access:STOHIST.Tryinsert()
                            Access:STOHIST.Cancelautoinc()
                        End!If Access:STOHIST.Tryinsert()
                    End!If Access:STOHIST.Primerecord() = Level:Benign
                    sto:quantity_stock     = 0
                    access:stock.update()
                    access:warparts_alias.clearkey(war_ali:RefPartRefNoKey)
                    war_ali:ref_number      = job:ref_number
                    war_ali:part_ref_number = glo:select2
                    If access:warparts_alias.fetch(war_ali:RefPartRefNoKey) = Level:Benign
                        wpr:ref_number           = war_ali:ref_number
                        wpr:adjustment           = war_ali:adjustment
                        wpr:part_ref_number      = war_ali:part_ref_number
                        wpr:part_number     = war_ali:part_number
                        wpr:description     = war_ali:description
                        wpr:supplier        = war_ali:supplier
                        wpr:purchase_cost   = war_ali:purchase_cost
                        wpr:sale_cost       = war_ali:sale_cost
                        wpr:retail_cost     = war_ali:retail_cost
                        wpr:quantity             = glo:select3
                        wpr:warranty_part        = 'YES'
                        wpr:exclude_from_order   = 'NO'
                        wpr:despatch_note_number = ''
                        wpr:date_ordered         = ''
                        wpr:date_received        = ''
                        wpr:pending_ref_number   = glo:select4
                        wpr:order_number         = ''
                        wpr:fault_code1    = war_ali:fault_code1
                        wpr:fault_code2    = war_ali:fault_code2
                        wpr:fault_code3    = war_ali:fault_code3
                        wpr:fault_code4    = war_ali:fault_code4
                        wpr:fault_code5    = war_ali:fault_code5
                        wpr:fault_code6    = war_ali:fault_code6
                        wpr:fault_code7    = war_ali:fault_code7
                        wpr:fault_code8    = war_ali:fault_code8
                        wpr:fault_code9    = war_ali:fault_code9
                        wpr:fault_code10   = war_ali:fault_code10
                        wpr:fault_code11   = war_ali:fault_code11
                        wpr:fault_code12   = war_ali:fault_code12
  
                        access:warparts.insert()
                    end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign
                end !if access:stock.fetch(sto:ref_number_key = Level:Benign
            End!If glo:select1 = 'NEW PENDING'
        End!If (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
        Else!!If continue# = 1
            If (Field() = ?PartInsert Or Field() = ?PartChange Or Field() = ?PartDelete)
                access:parts.cancelautoinc()
            End!If (Field() = ?Insert Or Field() = ?Change Or Field() = ?Delete)
            If (Field() = ?WarInsert Or Field() = ?WarChange Or Field() = ?WarDelete)
                access:warparts.cancelautoinc()
            End!If (Field() = ?Insert:3 Or Field() = ?Change:3 Or Field() = ?Delete:3)
        End !If continue# = 1
  
        !Clear(glo:G_Select1)
        !Clear(glo:G_Select1)
        !Do check_parts
        !Do Pricing
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?job:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Accepted)
      ! Start Change 3157 BE(29/08/03)
      IF (~0{prop:acceptall}) THEN
      IF (job:Model_Number <> '') THEN
          POST(Event:Accepted, ?job:Model_Number)
      END
      END !If ~0{prop:acceptall}
      ! End Change 3157 BE(29/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Accepted)
    OF ?job:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Model_Number, Accepted)
      ! Start Change 3157 BE(29/08/03)
      IF (~0{prop:acceptall}) THEN
      IF (job:Model_Number <> '') THEN
          Access:MODELNUM.Clearkey(mod:Model_Number_Key)
          mod:Model_Number    = job:Model_Number
          IF (Access:MODELNUM.Tryfetch(mod:Model_Number_Key) <> Level:Benign) THEN
              POST(Event:Accepted, ?PickModelButton)
          ELSE
              job:Model_Number = IMEIModelRoutine(job:ESN,job:Model_Number)
      
              access:modelnum.clearkey(mod:model_number_key)
              mod:model_number = job:model_number
              IF ((access:modelnum.fetch(mod:model_number_key) = Level:Benign) AND (job:manufacturer <> mod:manufacturer)) THEN
                  job:manufacturer = mod:manufacturer
                  IF (job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA'  Or job:manufacturer = 'MOTOROLA') THEN
                      job:fault_code1 = mod:product_type
                  END
                  DO Show_Job_Fault_Codes
                  DO Disable_TestAndRepair_Panel
              END
      
              access:modelcol.clearkey(moc:colour_key)
              moc:model_number = job:model_number
              SET(moc:colour_key, moc:colour_key)
              IF ((access:modelcol.next() = Level:Benign) AND (moc:model_number = job:model_number)) THEN
                  job:colour  = moc:colour
              END
      
              !job:Model_Number = IMEIModelRoutine(job:ESN,job:Model_Number)
      
              IF (mod:specify_unit_type = 'YES') THEN
                  job:unit_type   = mod:unit_type
              END
      
              ! Start Change 4239 BE(24/05/04)
              Access:MANUFACT.ClearKey(man:Manufacturer_Key)
              man:Manufacturer = job:Manufacturer
              IF (Access:MANUFACT.Fetch(man:Manufacturer_Key) = Level:Benign) THEN
                  MSN_Field_Mask = man:MSNFieldMask
              ELSE
                  MSN_Field_Mask = ''
              END
              ! End Change 4239 BE(24/05/04)
      
              IF (CheckLength('IMEI',job:Model_Number,job:ESN)) THEN
                  job:ESN = ''
                  Select(?job:ESN)
              END
      
              DISPLAY()
          END
      END
      END !If ~0{prop:acceptall}
      ! End Change 3157 BE(29/08/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Model_Number, Accepted)
    OF ?ChangeStatusButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChangeStatusButton, Accepted)
      DO Update_Record
      SaveRequest# = GlobalRequest
      GlobalRequest = ChangeRecord
      Change_Status(job:Ref_Number)
      GlobalRequest = SaveRequest#
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChangeStatusButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PickFaultDescriptionButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickFaultDescriptionButton, Accepted)
      GlobalRequest = SelectRecord!
      Browse_Fault_Description_Text
      rc# = Records(glo:Q_Notes)
      IF (rc# > 0) THEN
          LOOP x# = 1 TO rc#
              GET(glo:Q_Notes, x#)
              IF (jbn:fault_description = '') THEN
                  jbn:fault_description = Clip(glo:notes_pointer)
              ELSE
                  jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(glo:notes_pointer)
              END
          END
          Display(?jbn:fault_description)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickFaultDescriptionButton, Accepted)
    OF ?PickEngineersNotesButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickEngineersNotesButton, Accepted)
      GlobalRequest = SelectRecord
      Browse_Engineers_Notes
      rc# = Records(glo:Q_Notes)
      IF (rc# > 0) THEN
          LOOP x# = 1 TO rc#
              GET(glo:Q_Notes, x#)
              IF (jbn:engineers_notes = '') THEN
                  jbn:engineers_notes = Clip(glo:notes_pointer)
              ELSE
                  jbn:engineers_notes = Clip(jbn:engineers_notes) & '<13,10>' & Clip(glo:notes_pointer)
              END
          END
          Display(?jbn:Engineers_Notes)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickEngineersNotesButton, Accepted)
    OF ?PickInvoiceTextButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickInvoiceTextButton, Accepted)
      GlobalRequest = SelectRecord!
      Browse_Invoice_Text
      rc# = Records(glo:Q_Notes)
      IF (rc# > 0) THEN
          LOOP x# = 1 TO rc#
              GET(glo:Q_Notes, x#)
              IF (jbn:invoice_text = '') THEN
                  jbn:invoice_text = Clip(glo:notes_pointer)
              ELSE
                  jbn:invoice_text = Clip(jbn:invoice_text) & '. ' & Clip(glo:notes_pointer)
              END
          END
          Display(?jbn:Invoice_Text)
      END
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickInvoiceTextButton, Accepted)
    OF ?job:Chargeable_Job
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Chargeable_Job, Accepted)
      ! Start Change BE024 BE(17/12/03)
      If ~0{prop:acceptall}
          ChangeChargeableJob()
      ! End Change BE024 BE(17/12/03)
      
          IF (job:Chargeable_Job = 'YES') THEN
              !Enable(?job:Charge_Type)
              !Enable(?ChargeButton)
              !Enable(?ChargeTypePrompt)
              !Enable(?job:Repair_Type)
              !Enable(?ChargeTypeButton)
              !UNHIDE(?job:Charge_Type)
              UNHIDE(?temp:Charge_Type)
              UNHIDE(?ChargeButton)
              UNHIDE(?ChargeTypePrompt)
              !UNHIDE(?job:Repair_Type)
              UNHIDE(?temp:Repair_Type)
              UNHIDE(?ChargeTypeButton)
              DO Enable_Parts
          ELSE
              !Disable(?job:Charge_Type)
              !Disable(?ChargeButton)
              !Disable(?ChargeTypePrompt)
              !Disable(?job:Repair_Type)
              !Disable(?ChargeTypeButton)
              !HIDE(?job:Charge_Type)
              HIDE(?temp:Charge_Type)
              HIDE(?ChargeButton)
              HIDE(?ChargeTypePrompt)
              !HIDE(?job:Repair_Type)
              HIDE(?temp:Repair_Type)
              HIDE(?ChargeTypeButton)
              DO Disable_Chargeable_Parts
          END
          DISPLAY()
      ! Start Change BE024 BE(17/12/03)
      END
      ! End Change BE024 BE(17/12/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Chargeable_Job, Accepted)
    OF ?temp:charge_type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:charge_type, Accepted)
      IF (~0{prop:acceptall})   THEN
          IF (temp:charge_type = '') THEN
              job:charge_type = temp:charge_type
              DO Disable_Chargeable_Parts
          ELSE
              save_charge_type = job:charge_type
              job:charge_type = temp:charge_type
              IF (CheckPricing('C') = Level:Benign) THEN
                  DO Enable_Parts
              ELSE
                  job:Charge_Type = save_charge_type
                  temp:charge_type  = job:Charge_Type
              END
          END
          DISPLAY()
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:charge_type, Accepted)
    OF ?ChargeButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChargeButton, Accepted)
      glo:select1 = 'NO'
      GlobalRequest = SelectRecord
      Browse_Warranty_Charge_Types
      IF (GlobalResponse = RequestCompleted) THEN
          temp:charge_type = cha:charge_type
          !Do Estimate_Check
          DISPLAY(?temp:charge_type)
          POST(EVENT:Accepted, ?temp:charge_type)
      END
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChargeButton, Accepted)
    OF ?temp:repair_type_warranty
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:repair_type_warranty, Accepted)
      IF (~0{prop:acceptall}) THEN
          IF (job:repair_type_warranty <> temp:repair_type_warranty) THEN
              IF ((job:date_completed <> '') AND (SecurityCheck('AMEND COMPLETED JOBS'))) THEN
                  MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                            'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemhand,msgex:samewidths,84,26,0)
              ELSIF ((job:invoice_Number <> '') AND (SecurityCheck('AMEND INVOICED JOBS'))) THEN
                  MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                            'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemhand,msgex:samewidths,84,26,0)
              ELSIF (job:Model_Number = '' Or job:Warranty_Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = '') THEN
                  MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                            'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemhand,msgex:samewidths,84,26,0)
              ELSE
                  IF (temp:repair_type_warranty = '')
                      job:repair_type_warranty = temp:repair_type_warranty
                  ELSE
                      Access:REPAIRTY.ClearKey(rep:Model_Warranty_Key)
                      rep:Model_Number = job:Model_Number
                      rep:warranty  = job:Warranty_Job
                      rep:repair_type  = temp:repair_type_warranty
                      IF (Access:REPAIRTY.TryFetch(rep:Model_Warranty_Key) <> Level:Benign) THEN
                          POST(Event:Accepted, ?WarrantyTypeButton)
                      ELSE
                          save_repair_type = job:repair_type_warranty
                          job:repair_type_warranty =  temp:repair_type_warranty
                          IF (CheckPricing('W') <> 1) THEN
                              !IF (job:repair_type_warranty = '') THEN
                              IF (save_repair_type = '') THEN
                                  !job:repair_type_warranty = temp:repair_type_warranty
                                  Do pricing
                              ELSE
                                  CASE MessageEx('Are you sure you want to change the repair type?','ServiceBase 2000',|
                                                  'Styles\question.ico','&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                                  beep:systemquestion,msgex:samewidths,84,26,0)
                                      OF 1 ! &Yes Button
                                          !job:repair_type_warranty = temp:repair_type_warranty
                                          Do pricing
                                      OF 2 ! &No Button
                                          job:repair_type_warranty = save_repair_type
                                          temp:repair_type_warranty = job:repair_type_warranty
                                  END
                              END
                          ELSE
                              job:repair_type_warranty = save_repair_type
                              temp:repair_type_warranty = job:repair_type_warranty
                          END
                      END
                  END
                  DISPLAY()
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:repair_type_warranty, Accepted)
    OF ?WarrantyTypeButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?WarrantyTypeButton, Accepted)
      If (job:Model_Number = '' OR job:warranty_Charge_Type = '' OR job:Account_Number = '' OR job:Unit_Type = '' OR |
          PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Warranty_Charge_Type,temp:Repair_Type_warranty))  THEN
          MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type, and Unit Type.',|
                    'ServiceBase 2000','Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
          RepairType" = PickRepairTypes(job:Model_Number,job:Account_Number,job:Warranty_Charge_Type,job:Unit_Type,'W')
          IF (RepairType" <> '') THEN
              temp:Repair_Type_Warranty = RepairType"
              DISPLAY(?temp:Repair_Type_Warranty)
              POST(Event:Accepted,?temp:Repair_Type_Warranty)
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?WarrantyTypeButton, Accepted)
    OF ?IsOnTest
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?IsOnTest, Accepted)
      IF (IsOnTest) THEN
          IsPass = 0
          IsFail = 0
          DISPLAY(?IsFail)
          DISPLAY(?IsPass)
          DO Auto_Allocate_Job
          GetStatus(320,1,'JOB')    ! On Test
          DO Update_Record
          DO Enable_Parts
      END
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?IsOnTest, Accepted)
    OF ?IsPass
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?IsPass, Accepted)
      IF (IsPass) THEN
          IsOnTest = 0
          IsFail = 0
          DISPLAY(?IsFail)
          DISPLAY(?IsOnTest)
          GetStatus(605,1,'JOB')  ! Awaiting QA
          DO Update_Record
      END
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?IsPass, Accepted)
    OF ?IsFail
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?IsFail, Accepted)
      IF (IsFail) THEN
          IsPass = 0
          IsOnTest = 0
          DISPLAY(?IsOnTest)
          DISPLAY(?IsPass)
          GetStatus(315,1,'JOB')  ! In Repair
          DO Update_Record
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?IsFail, Accepted)
    OF ?QAFailButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?QAFailButton, Accepted)
      IF (job:third_party_site <> '') THEN
          handset_type_temp  = 'Thid Party Repair'
      ELSE
          handset_type_temp  = 'Repair'
      END
      
      Set(Defaults)
      access:Defaults.next()
      
      Access:MANUFACT.ClearKey(man:Manufacturer_Key)
      man:Manufacturer = job:Manufacturer
      IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign) THEN
      
          IF (man:UseElectronicQA) THEN
              QA_Type = 'PRE'
          ELSE
              QA_Type = 'PRI'
          END
      
          IF (handset_type_temp = 'Exchange' OR handset_type_temp = 'Loan') THEN
              IF (~man:QALoanExchange) THEN
                  MessageEx('This Manufacturer is not setup to QA Loan/Exchange Units.','ServiceBase 2000',|
                             'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                             beep:systemhand,msgex:samewidths,84,26,0)
              ELSE
                  IF (handset_type_temp = 'Exchange') THEN                                               !Is it an Exchange that was scanned?
                      IF (job:exchange_consignment_number = '') THEN                                     !Check that is isn't already ready
                          !Do passed_fail_update
                          Failure_Reason = ''
                          qa_failure_reason(Failure_Reason)
                          glo:notes_global = Clip(Failure_Reason)
                          IF (def:qa_failed_label = 'YES') THEN
                              glo:select1  = job:exchange_unit_number
                              QA_Failed_Label_Exchange
                              glo:select1  = ''
                          END
                          IF (def:qa_failed_report = 'YES') THEN
                              glo:select1 = job:exchange_unit_number
                              QA_Failed_Exchange
                              glo:select1 = ''
                          END
                          glo:notes_global = ''
                          job:exchange_status = 'QA FAILED'
                          job:despatched = ''
                          job:despatch_type = ''
                          CASE QA_Type
                              OF 'PRE'
                                  GetStatus(625,1,'EXC') !electronic qa rejected
                              OF 'PRI'
                                  GetStatus(615,1,'EXC') !manual qa rejected
                          END
                          GET(audit,0)
                          IF (access:audit.primerecord() = level:benign) THEN
                              aud:notes         = 'EXCHANGE UNIT NUMBER: ' & CLIP(job:Exchange_unit_number) &|
                                                  '<13,10>QA FAILURE REASON: ' & CLIP(Failure_Reason)
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              CASE QA_Type
                                  OF 'PRE'
                                      aud:action        = 'RAPID QA UPDATE: ELECTRONIC QA FAILED (EXC)'
                                  OF 'PRI'
                                      aud:action        = 'RAPID QA UPDATE: MANUAL QA FAILED (EXC)'
                              END
                              aud:type      = 'EXC'
                              access:audit.insert()
                          END
                          access:jobs.update()
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number = job:exchange_unit_number
                          IF (access:exchange.tryfetch(xch:ref_number_key) = Level:benign) THEN
                              xch:available = 'QAF'
                              access:exchange.update()
                              GET(exchhist,0)
                              IF (access:exchhist.primerecord() = level:benign) THEN
                                  exh:ref_number   = xch:ref_number
                                  exh:date          = today()
                                  exh:time          = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  exh:user = use:user_code
                                  exh:status        = 'RAPID QA UPDATE: QA FAILED - ' & CLIP(Failure_Reason)
                                  access:exchhist.insert()
                              END
                          END
                          DO Activate_No_Panel
                      END
                  END
      
                  IF (handset_type_temp = 'Loan') THEN
                      IF (job:loan_consignment_number = '') THEN
                          !Do passed_fail_update
                          Failure_Reason = ''
                          qa_failure_reason(Failure_Reason)
                          glo:notes_global = Clip(Failure_Reason)
                          IF (def:qa_failed_label = 'YES') THEN
                              glo:select1  = job:loan_unit_number
                              QA_Failed_Label_Loan
                              glo:select1  = ''
                          END
                          IF (def:qa_failed_report = 'YES') THEN
                             glo:select1 = job:loan_unit_number
                              QA_Failed_Loan
                              glo:select1 = ''
                          END
                          glo:notes_global = ''
                          job:loan_status = 'QA FAILED'
                          job:despatched = ''
                          job:despatch_type = ''
                          CASE QA_Type
                              OF 'PRE'
                                  GetStatus(625,1,'LOA') !electronic qa rejected
                              OF 'PRI'
                                  GetStatus(615,1,'LOA') !electronic qa rejected
                          END
                          job:status_end_date = ''
                          job:status_end_time = ''
                          GET(audit,0)
                          IF (access:audit.primerecord() = level:benign) THEN
                              aud:notes         = 'LOAN UNIT NUMBER: ' & Clip(job:Loan_unit_number) &|
                                                  '<13,10>QA FAILURE REASON: ' & CLip(Failure_Reason)
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              CASE QA_Type
                                  OF 'PRE'
                                      aud:action        = 'RAPID QA UPDATE: ELECTRONIC QA FAILED (LOA)'
                                  OF 'PRI'
                                      aud:action        = 'RAPID QA UPDATE: MANUAL QA FAILED (LOA)'
                              END
                              aud:type = 'LOA'
                              access:audit.insert()
                          END
                          access:jobs.update()
                          access:loan.clearkey(loa:ref_number_key)
                          loa:ref_number = job:loan_unit_number
                          IF (access:loan.tryfetch(loa:ref_number_key) = Level:benign) THEN
                              loa:available = 'QAF'
                              access:loan.update()
                              GET(loanhist,0)
                              IF (access:loanhist.primerecord() = level:benign) THEN
                                  loh:ref_number   = xch:ref_number
                                  loh:date          = today()
                                 loh:time          = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  loh:user = use:user_code
                                  loh:status        = 'RAPID QA UPDATE: QA FAILED - ' & CLIP(Failure_Reason)
                                  access:loanhist.insert()
                              END
                          END
                          DO Activate_No_Panel
                      END
                  END
              END
          ELSE
              IF (job:date_completed <> '') THEN
                  MessageEx('Cannot QA! The selected job has been completed.','ServiceBase 2000',|
                             'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                             beep:systemhand,msgex:samewidths,84,26,0)
              ELSE
                  job:qa_rejected      = 'YES'
                  job:date_qa_rejected = Today()
                  job:time_qa_rejected = Clock()
                  CASE QA_Type
                      OF 'PRE'
                          GetStatus(625,1,'JOB') !electronic qa rejected
                      OF 'PRI'
                          GetStatus(615,1,'JOB') !manual qa rejected
                  END
      
                  access:jobs.update()
                  !Do passed_fail_update
                  beep(beep:systemasterisk)
                  Failure_Reason = ''
                  qa_failure_reason(Failure_Reason)
                  GET(audit,0)
                  IF (access:audit.primerecord() = level:benign) THEN
                      aud:notes         = ''
                      aud:ref_number    = job:ref_number
                      aud:date          = today()
                      aud:time          = clock()
                      access:users.clearkey(use:password_key)
                      use:password =glo:password
                      access:users.fetch(use:password_key)
                      aud:user = use:user_code
                      CASE QA_Type
                         OF 'PRE'
                             aud:action        = 'QA REJECTION ELECTRONIC: ' & Clip(Failure_Reason)
                         OF 'PRI'
                             aud:action        = 'QA REJECTION MANUAL: ' & Clip(Failure_Reason)
                     END
                     access:audit.insert()
                  END
                  glo:notes_global = CLIP(Failure_Reason)
                  IF (def:qa_failed_label = 'YES') THEN
                      glo:select1  = job:ref_number
                      QA_Failed_Label
                      glo:select1  = ''
                  END
                  IF (def:qa_failed_report = 'YES') THEN
                      glo:select1 = job:ref_number
                      QA_Failed
                      glo:select1 = ''
                  END
                  glo:notes_global = ''
                  DO Activate_No_Panel
              END
          END
      ELSE
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          MessageEx('Error! Cannot find the Manufacturer attached to this job.','ServiceBase 2000',|
                     'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                     beep:systemhand,msgex:samewidths,84,26,0)
      END
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?QAFailButton, Accepted)
    OF ?QAPassButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?QAPassButton, Accepted)
      IF (job:third_party_site <> '') THEN
          handset_type_temp  = 'Thid Party Repair'
      ELSE
          handset_type_temp  = 'Repair'
      END
      
      SET(Defaults)
      access:Defaults.next()
      Access:MANUFACT.ClearKey(man:Manufacturer_Key)
      man:Manufacturer = job:Manufacturer
      IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign) THEN
          IF (man:UseElectronicQA) THEN
              QA_Type = 'PRE'
          ELSE
              QA_Type = 'PRI'
          END
          !IF (QA_Type = 'PRE' And ~man:UseElectronicQA)
          !        MessageEx('The Manufacturer of the selected job is not setup to do Electronic QA.','ServiceBase 2000',|
          !                   'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
          !                   beep:systemhand,msgex:samewidths,84,26,0)
          !ELSE
              error# = 0
              IF (handset_type_temp = 'Exchange' OR handset_type_temp = 'Loan') THEN
                  If ~man:QALoanExchange
                      error# = 1
                      MessageEx('This Manufacturer is not setup to QA Loan/Exchange Units.','ServiceBase 2000',|
                                'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                beep:systemhand,msgex:samewidths,84,26,0)
                  ELSE
                      error# = 1                                                              !Stop from doing the job check
                      IF (handset_type_temp = 'Exchange') THEN                                !Is it an Exchange that was scanned?
                          IF (job:exchange_consignment_number = '') THEN                      !Check that is isn't already ready
                              type# = 2 !1 - Job, 2 - Exchange, 3 - Loan
                              CASE AccessoryCheck('EXCHANGE')
                                  OF 1 OROF 2
                                      Failure_Reason = ''
                                      QA_Failure_Reason(Failure_Reason)
                                      GET(audit,0)
                                      IF (access:audit.primerecord() = level:benign) THEN
                                          aud:notes         = 'ACCESSORIES BOOKED IN:-'
                                          save_jac_id = access:jobacc.savefile()
                                          access:jobacc.clearkey(jac:ref_number_key)
                                          jac:ref_number = job:Ref_number
                                          SET(jac:ref_number_key,jac:ref_number_key)
                                          LOOP
                                              IF ((access:jobacc.next() <> Level:Benign) OR (jac:ref_number <> job:Ref_number)) THEN
                                                 BREAK
                                              END
                                               aud:notes   = CLIP(aud:notes) & '<13,10>' & CLIP(jac:accessory)
                                          END
                                          access:jobacc.restorefile(save_jac_id)
      
                                          aud:notes       = CLIP(aud:notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                          LOOP x# = 1 TO RECORDS(glo:queue)
                                              GET(glo:queue,x#)
                                              aud:notes       = CLIP(aud:notes) & '<13,10>' & CLIP(glo:pointer)
                                          END
                                          !Save Notes For Report
                                          glo:notes_global    = CLIP(aud:notes)
      
                                          aud:ref_number    = job:ref_number
                                          aud:date          = today()
                                          aud:time          = clock()
                                          aud:type          = 'EXC'
                                          access:users.clearkey(use:password_key)
                                          use:password = glo:password
                                          access:users.fetch(use:password_key)
                                          aud:user = use:user_code
                                          CASE QA_Type
                                              Of 'PRE'
                                                  aud:action        = 'QA REJECTION ELECTRONIC: ' & CLIP(Failure_Reason)
                                              Of 'PRI'
                                                  aud:action        = 'QA REJECTION MANUAL: ' & CLIP(Failure_Reason)
                                          END
                                          access:audit.insert()
                                      END
                                      glo:select1 = job:exchange_unit_number
                                      IF (def:qa_failed_label = 'YES') THEN
                                          QA_Failed_Label_Exchange
                                      END
                                      IF (def:qa_failed_report = 'YES') THEN
                                          QA_Failed_Exchange
                                      END
                                      glo:select1 = ''
                                      glo:notes_global    = ''
                                      CASE QA_Type
                                          OF 'PRE'
                                              GetStatus(625,1,'EXC')
                                          ELSE
                                              GetStatus(615,1,'EXC')
                                      END
      
                                      job:qa_rejected = 'YES'
                                      job:date_qa_rejected    = Today()
                                      job:time_qa_rejected    = Clock()
                                      access:jobs.update()
                                      !DO passed_fail_update
                                      !DO Update_record
                                      !DO Activate_No_Panel
                                  OF 3
                                  OF 0
                                      CASE QA_Type                                                             !to be despatched.
                                          OF 'PRE'
                                              !DO passed_update
                                              GET(audit,0)
                                              IF (access:audit.primerecord() = level:benign) THEN
                                                  aud:notes         = 'EXCHANGE UNIT NUMBER: ' & Clip(job:Exchange_unit_number)
                                                  aud:ref_number    = job:ref_number
                                                  aud:date          = today()
                                                  aud:time          = clock()
                                                  access:users.clearkey(use:password_key)
                                                  use:password =glo:password
                                                  access:users.fetch(use:password_key)
                                                  aud:user = use:user_code
                                                  aud:action        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED (EXC)'
                                                  aud:type          = 'EXC'
                                                  access:audit.insert()
                                              END
                                              GetStatus(620,1,'EXC') !Electronic QA passed
                                              access:jobs.update()
                                              access:exchange.clearkey(xch:ref_number_key)
                                              xch:ref_number = job:exchange_unit_number
                                              IF (access:exchange.tryfetch(xch:ref_number_key) = Level:benign) THEN
                                                  xch:available = 'QA2'
                                                  access:exchange.update()
                                                  GET(exchhist,0)
                                                  IF (access:exchhist.primerecord() = level:benign) THEN
                                                      exh:ref_number   = xch:ref_number
                                                      exh:date          = today()
                                                      exh:time          = clock()
                                                      access:users.clearkey(use:password_key)
                                                      use:password =glo:password
                                                      access:users.fetch(use:password_key)
                                                      exh:user = use:user_code
                                                      exh:status        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED'
                                                      access:exchhist.insert()
                                                  END
                                              END
                                              IF (def:QAPassLabel = 1) THEN
                                                  glo:Select1  = job:Exchange_Unit_Number
                                                  QA_Passed_Label_Exchange
                                                  glo:Select1  = ''
                                              END
                                          OF 'PRI'
                                              error_courier# = 0
                                              IF (job:exchange_courier = '')
                                                  CASE MessageEx('There is NO Courier attached to this Exchange Unit!<13,10><13,10>You will not be able to QA this unit until you have attached one, as it will not be able to be despatched correctly.<13,10><13,10>Do you wish to select a Courier Now?','ServiceBase 2000',|
                                                                 'Styles\stop.ico','&Select Courier|&Cancel QA',2,2,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                                                 beep:systemhand,msgex:samewidths,84,26,0)
                                                      OF 1 ! &Select Courier Button
                                                          saverequest#      = globalrequest
                                                          globalresponse    = requestcancelled
                                                          globalrequest     = selectrecord
                                                          browse_courier
                                                          IF (globalresponse = requestcompleted)
                                                              job:exchange_courier = cou:courier
                                                          ELSE
                                                              error_courier# = 1
                                                              MessageEx('This unit has NOT been QA''d at this time.','ServiceBase 2000',|
                                                                        'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                                                        beep:systemhand,msgex:samewidths,84,26,0)
                                                          END
                                                          globalrequest     = saverequest#
                                                      OF 2 ! &Cancel QA Button
                                                          error_courier# = 1
                                                  END
                                              END
                                              IF (error_courier# = 1) THEN
                                                  !DO failed_update
                                              ELSE
                                                  !DO passed_update
                                                  job:Exchange_Despatched = DespatchANC(job:Exchange_Courier,'EXC')
                                                  access:users.clearkey(use:password_key)
                                                  use:password =glo:password
                                                  access:users.fetch(use:password_key)
                                                  job:current_courier = job:exchange_courier
                                                  Access:courier.clearkey(cou:courier_key)
                                                  cou:courier = job:exchange_courier
                                                  access:courier.tryfetch(cou:courier_key)
                                                  IF (cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G') THEN
                                                      job:excservice = cou:service
                                                  ELSE
                                                      job:excservice = ''
                                                  END
      
                                                  GetStatus(610,1,'EXC') !Despatch Exchange Unit
      
                                                  GET(audit,0)
                                                  IF (access:audit.primerecord() = level:benign) THEN
                                                      aud:notes         = 'EXCHANGE UNIT NUMBER: ' & CLIP(job:Exchange_unit_number)
                                                      aud:ref_number    = job:ref_number
                                                      aud:date          = today()
                                                      aud:time          = clock()
                                                      access:users.clearkey(use:password_key)
                                                      use:password =glo:password
                                                      access:users.fetch(use:password_key)
                                                      aud:user = use:user_code
                                                      aud:action        = 'RAPID QA UPDATE: MANUAL QA PASSED (EXC)'
                                                      aud:type          = 'EXC'
                                                      access:audit.insert()
                                                  END
                                                  access:jobs.update()
                                                  access:exchange.clearkey(xch:ref_number_key)
                                                  xch:ref_number = job:exchange_unit_number
                                                  IF (access:exchange.tryfetch(xch:ref_number_key) = Level:benign) THEN
                                                      xch:available = 'EXC'
                                                      access:exchange.update()
                                                      GET(exchhist,0)
                                                      IF (access:exchhist.primerecord() = level:benign) THEN
                                                          exh:ref_number   = xch:ref_number
                                                          exh:date          = today()
                                                          exh:time          = clock()
                                                          access:users.clearkey(use:password_key)
                                                          use:password =glo:password
                                                          access:users.fetch(use:password_key)
                                                          exh:user = use:user_code
                                                          exh:status        = 'UNIT EXCHANGED ON JOB NO: ' & CLip(job:Ref_number)
                                                          access:exchhist.insert()
                                                      END
                                                  END
                                                  IF (def:QAPassLabel = 1) THEN
                                                      glo:Select1  = job:Exchange_Unit_Number
                                                      QA_Passed_Label_Exchange
                                                      glo:Select1  = ''
                                                  END
                                              END
                                      END
                              END
                          END
                      END
                      IF (handset_type_temp = 'Loan') THEN
                          IF (job:loan_consignment_number = '' And job:despatched <> 'LOA') THEN    !Check that is isn't already ready
                              CASE AccessoryCheck('LOAN')
                                  OF 1 OROF 2 !Failed QA
                                      Failure_Reason = ''
                                      QA_Failure_Reason(Failure_Reason)
                                      GET(audit,0)
                                      IF (access:audit.primerecord() = level:benign) THEN
                                          aud:notes         = 'ACCESSORIES BOOKED IN:-'
                                          save_lac_id = access:loanacc.savefile()
                                          access:loanacc.clearkey(lac:ref_number_key)
                                          lac:ref_number = job:Loan_unit_Number
                                          SET(lac:ref_number_key,lac:ref_number_key)
                                          LOOP
                                              IF ((access:loanacc.next() <> Level:benign) OR (lac:ref_number <> job:loan_unit_number)) THEN
                                                 Break
                                              END
                                              aud:notes   = CLIP(aud:notes) & '<13,10>' & CLIP(lac:accessory)
                                          END
                                          access:loanacc.restorefile(save_lac_id)
      
                                          aud:notes       = CLIP(aud:notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                          LOOP x# = 1 TO RECORDS(glo:queue)
                                              GET(glo:queue,x#)
                                              aud:notes       = CLIP(aud:notes) & '<13,10>' & CLIP(glo:pointer)
                                          END
                                          !Save Notes For Report
                                          glo:notes_global    = CLIP(aud:notes)
                                          aud:ref_number    = job:ref_number
                                          aud:date          = today()
                                          aud:time          = clock()
                                          aud:type          = 'LOA'
                                          access:users.clearkey(use:password_key)
                                          use:password = glo:password
                                          access:users.fetch(use:password_key)
                                          aud:user = use:user_code
                                          CASE QA_Type
                                              OF 'PRE'
                                                  aud:action        = 'QA REJECTION ELECTRONIC: ' & CLIP(Failure_Reason)
                                              OF 'PRI'
                                                  aud:action        = 'QA REJECTION MANUAL: ' & CLIP(Failure_Reason)
                                          END
                                          access:audit.insert()
                                      END
                                      glo:select1 = job:loan_unit_number
                                      IF (def:qa_failed_label = 'YES') THEN
                                          QA_Failed_Label_Loan
                                      END
                                      IF (def:qa_failed_report = 'YES') THEN
                                          QA_Failed_Loan
                                      END
                                      glo:select1 = ''
                                      glo:notes_global    = ''
                                      CASE QA_Type
                                          OF 'PRE'
                                              GetStatus(625,1,'LOA')
                                          ELSE
                                              GetStatus(615,1,'LOA')
                                      END
                                      job:qa_rejected = 'YES'
                                      job:date_qa_rejected    = Today()
                                      job:time_qa_rejected    = Clock()
                                      access:jobs.update()
                                      !DO passed_fail_update
                                      !DO Update_Record
                                      !DO Activate_No_Panel
                                  OF 3   !Cancelled
      
                                  OF 0 !Passed
                                      CASE QA_Type                                                             !to be despatched.
                                          OF 'PRE'
                                              !DO passed_update
                                              GET(audit,0)
                                              IF (access:audit.primerecord() = level:benign) THEN
                                                  aud:notes         = 'LOAN UNIT NUMBER: ' & CLIP(job:Loan_unit_number)
                                                  aud:ref_number    = job:ref_number
                                                  aud:date          = today()
                                                  aud:time          = clock()
                                                  access:users.clearkey(use:password_key)
                                                  use:password =glo:password
                                                  access:users.fetch(use:password_key)
                                                  aud:user = use:user_code
                                                  aud:action        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED (LOA)'
                                                  aud:type          = 'LOA'
                                                  access:audit.insert()
                                              END
                                              GetStatus(620,1,'LOA')
                                              access:jobs.update()
                                              access:loan.clearkey(loa:ref_number_key)
                                              loa:ref_number = job:loan_unit_number
                                              IF (access:loan.tryfetch(loa:ref_number_key) = Level:benign) THEN
                                                  loa:available = 'QA2'
                                                  access:loan.update()
                                                  GET(loanhist,0)
                                                  IF (access:loanhist.primerecord() = level:benign) THEN
                                                      loh:ref_number   = xch:ref_number
                                                      loh:date          = today()
                                                      loh:time          = clock()
                                                      access:users.clearkey(use:password_key)
                                                      use:password =glo:password
                                                      access:users.fetch(use:password_key)
                                                      loh:user = use:user_code
                                                      loh:status        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED'
                                                      access:loanhist.insert()
                                                  END
                                              END
                                              IF (def:QAPassLabel = 1) THEN
                                                  glo:Select1  = job:Loan_Unit_Number
                                                  QA_Passed_Label_Loan
                                                  glo:Select1  = ''
                                              END
                                          OF 'PRI'
                                              error_courier# = 0
                                              IF (job:loan_courier = '') THEN
                                                  Case MessageEx('There is NO Courier attached to this Loan Unit!<13,10><13,10>You will not be able to QA this unit until you have attached one, as it will not be able to be despatched correctly.<13,10><13,10>Do you wish to select a Courier Now?','ServiceBase 2000',|
                                                                 'Styles\stop.ico','&Select Courier|&Cancel QA',2,2,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                                                 beep:systemhand,msgex:samewidths,84,26,0)
                                                      OF 1 ! &Select Courier Button
                                                          saverequest#      = globalrequest
                                                          globalresponse    = requestcancelled
                                                          globalrequest     = selectrecord
                                                          browse_courier
                                                          IF (globalresponse = requestcompleted) THEN
                                                              job:loan_courier = cou:courier
                                                          ELSE
                                                              error_courier# = 1
                                                              MessageEx('This unit has NOT been QA''d at this time.','ServiceBase 2000',|
                                                                        'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                                                        beep:systemhand,msgex:samewidths,84,26,0)
                                                          END
                                                          globalrequest     = saverequest#
                                                      OF 2 ! &Cancel QA Button
                                                          error_courier# = 1
                                                  END
                                              END
                                              IF (job:error_Courier# = 1) THEN
                                                  !DO failed_update
                                              ELSE
                                                  !DO passed_update
                                                  job:Loan_Despatched = DespatchANC(job:Loan_Courier,'LOA')
                                                  job:current_courier = job:loan_courier
                                                  Access:courier.clearkey(cou:courier_key)
                                                  cou:courier = job:loan_courier
                                                  access:courier.tryfetch(cou:courier_key)
                                                  IF (cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G') THEN
                                                      job:loaservice = cou:service
                                                  ELSE
                                                      job:loaservice = ''
                                                  END
      
                                                  GetStatus(610,1,'LOA')
      
                                                  GET(audit,0)
                                                  IF (access:audit.primerecord() = level:benign) THEN
                                                      aud:notes         = 'LOAN UNIT NUMBER: ' & CLIP(job:Loan_unit_number)
                                                      aud:ref_number    = job:ref_number
                                                      aud:date          = today()
                                                      aud:time          = clock()
                                                      access:users.clearkey(use:password_key)
                                                      use:password =glo:password
                                                      access:users.fetch(use:password_key)
                                                      aud:user = use:user_code
                                                      aud:action        = 'RAPID QA UPDATE: MANUAL QA PASSED (LOA)'
                                                      aud:type          = 'LOA'
                                                      access:audit.insert()
                                                  END
                                                  access:jobs.update()
                                                  access:loan.clearkey(loa:ref_number_key)
                                                  loa:ref_number = job:loan_unit_number
                                                  IF (access:loan.tryfetch(loa:ref_number_key) = Level:benign) THEN
                                                      loa:available = 'LOA'
                                                      access:loan.update()
                                                      GET(loanhist,0)
                                                      IF (access:loanhist.primerecord() = level:benign) THEN
                                                          loh:ref_number   = xch:ref_number
                                                          loh:date          = today()
                                                          loh:time          = clock()
                                                          access:users.clearkey(use:password_key)
                                                          use:password =glo:password
                                                          access:users.fetch(use:password_key)
                                                          loh:user = use:user_code
                                                          loh:status        = 'UNIT LOANED ON JOB NO: ' & CLip(job:Ref_number)
                                                          access:loanhist.insert()
                                                      END
                                                  END
                                              END
                                              IF (def:QAPassLabel = 1) THEN
                                                  glo:Select1  = job:Loan_Unit_Number
                                                  QA_Passed_Label_Loan
                                                  glo:Select1  = ''
                                              END
                                      END
                              END
                          END
                      END
                  END
              ELSE
                  IF (~man:UseQA) THEN
                      MessageEx('This Manufacturer is not setup to QA Units.','ServiceBase 2000',|
                                     'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                     beep:systemhand,msgex:samewidths,84,26,0)
                  ELSE
                      IF (handset_type_temp = 'Repair' Or handset_type_temp = 'Third Party Repair') THEN
                          IF (job:date_Completed <> '') THEN
                              MessageEx('Cannot QA! The selected job has been completed!','ServiceBase 2000',|
                                        'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                         beep:systemhand,msgex:samewidths,84,26,0)
                              error# = 1
                          END
                      END
                      IF (error# = 0) THEN
                          IF (CheckRefurb()) THEN
                              CASE MessageEx('This unit is authorised for Refurbishment.<13,10><13,10>Is the Refurbishment Complete?','ServiceBase 2000',|
                                             'Styles\question.ico','&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                             beep:systemquestion,msgex:samewidths,84,26,0)
                                  OF 1 ! &Yes Button
                                  OF 2 ! &No Button
                                      error# = 1
                                      job:qa_rejected = 'YES'
                                      job:date_qa_rejected    = Today()
                                      job:time_qa_Rejected    = Clock()
                                      status_audit# = 1
                                      CASE QA_Type
                                          OF 'PRE'
                                              GetStatus(625,1,'JOB')
                                          ELSE
                                              GetStatus(615,1,'JOB')
                                      END
                                      access:jobs.update()
                                      Failure_Reason = ''
                                      QA_Failure_Reason(Failure_Reason)
                                      GET(audit,0)
                                      IF (access:audit.primerecord() = level:benign) THEN
                                          aud:notes         = 'REFURBISHMENT NOT COMPLETE'
                                          aud:ref_number    = job:ref_number
                                          aud:date          = today()
                                          aud:time          = clock()
                                          aud:type          = 'JOB'
                                          access:users.clearkey(use:password_key)
                                          use:password = glo:password
                                          access:users.fetch(use:password_key)
                                          aud:user = use:user_code
                                          CASE QA_Type
                                              OF 'PRE'
                                                  aud:action        = 'QA REJECTION ELECTRONIC: ' & CLIP(Failure_Reason)
                                              OF 'PRI'
                                                  aud:action        = 'QA REJECTION MANUAL: ' & CLIP(Failure_Reason)
                                          END
                                          access:audit.insert()
                                      END
                                      glo:notes_global    = CLIP(Failure_Reason)
                                      glo:select1 = job:ref_number
                                      IF (def:qa_failed_label = 'YES') THEN
                                          QA_Failed_Label
                                      END
                                      IF (def:qa_failed_report = 'YES') THEN
                                          QA_Failed
                                      END
                                      glo:select1 = ''
                                      glo:notes_global    = ''
                              END
                          END
                          IF (error# = 0) THEN
                              IF (job:exchange_unit_number = '') THEN
                                  type# = 1 !Job
                                  CASE AccessoryCheck('JOB')
                                      OF 1 OROF 2
                                          Failure_Reason = ''
                                          QA_Failure_Reason(Failure_Reason)
                                          get(audit,0)
                                          IF (access:audit.primerecord() = level:benign) THEN
                                              aud:notes         = 'ACCESSORIES BOOKED IN:-'
                                              save_jac_id = access:jobacc.savefile()
                                              access:jobacc.clearkey(jac:ref_number_key)
                                              jac:ref_number = job:Ref_number
                                              SET(jac:ref_number_key,jac:ref_number_key)
                                              LOOP
                                                  IF ((access:jobacc.next() <> Level:Benign) OR (jac:ref_number <> job:Ref_number )) THEN
                                                     BREAK
                                                  END
                                                  aud:notes   = CLIP(aud:notes) & '<13,10>' & CLIP(jac:accessory)
                                              END
                                              access:jobacc.restorefile(save_jac_id)
      
                                              aud:notes       = CLIP(aud:notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                              LOOP x# = 1 TO RECORDS(glo:queue)
                                                  GET(glo:queue,x#)
                                                  aud:notes       = CLIP(aud:notes) & '<13,10>' & CLIP(glo:pointer)
                                              END
                                              !Save Notes For Report
                                              glo:notes_global    = Clip(aud:notes)
                                              aud:ref_number    = job:ref_number
                                              aud:date          = today()
                                              aud:time          = clock()
                                              aud:type          = 'JOB'
                                              access:users.clearkey(use:password_key)
                                              use:password = glo:password
                                              access:users.fetch(use:password_key)
                                              aud:user = use:user_code
                                              CASE QA_Type
                                                  OF 'PRE'
                                                      aud:action        = 'QA REJECTION ELECTRONIC: ' & CLIP(Failure_Reason)
                                                  OF 'PRI'
                                                      aud:action        = 'QA REJECTION MANUAL: ' & CLIP(Failure_Reason)
                                              END
                                              access:audit.insert()
                                          END
                                          glo:select1 = job:ref_number
                                          IF (def:qa_failed_label = 'YES') THEN
                                              QA_Failed_Label
                                          END
                                          IF (def:qa_failed_report = 'YES') THEN
                                              QA_Failed
                                          END
                                          glo:select1 = ''
                                          glo:notes_global    = ''
                                          CASE QA_Type
                                              OF 'PRE'
                                                  GetStatus(625,1,'JOB')
                                              ELSE
                                                  GetStatus(615,1,'JOB')
                                          END
                                          job:qa_rejected = 'YES'
                                          job:date_qa_rejected    = Today()
                                          job:time_qa_rejected    = Clock()
                                          access:jobs.update()
                                          !DO Update_Record
                                          !DO Activate_No_Panel
                                          !DO passed_fail_update
      
                                  OF 3
                                      pass# = 0
                                  OF 0
                                      pass# = 1
                                  END
                              ELSE
                                  pass# = 1
                              END
                          END
      
                          IF (pass# = 1) THEN
                              pass# = LocalValidateParts()
                          END
      
                          IF (pass# = 1) THEN
                              pass# = LocalValidateNetwork()
                          END
      
                          IF (pass# = 1)                                                                    !If passed fill in all the relevant
                              CASE QA_Type
                                  OF 'PRE'
                                      GetStatus(620,1,'JOB')
                                      GET(audit,0)
                                      IF (access:audit.primerecord() = level:benign) THEN
                                          aud:notes         = ''
                                          aud:ref_number    = job:ref_number
                                          aud:date          = today()
                                          aud:time          = clock()
                                          access:users.clearkey(use:password_key)
                                          use:password =glo:password
                                          access:users.fetch(use:password_key)
                                          aud:user = use:user_code
                                          aud:action        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED'
                                          access:audit.insert()
                                      END
                                      IF (def:QAPassLabel = 1) THEN
                                          glo:Select1  = job:Ref_Number
                                          QA_Passed_Label
                                          glo:Select1  = ''
                                      END
                                      access:jobs.update()
                                      !DO Update_Record
                                      !DO Activate_No_Panel
                                      !DO passed_update
      
                                  OF 'PRI'
                                      !Auto complete the job... will need to default this later
                                      Complete# = 0
                                      CompleteError# = 0
                                      IF (man:UseQA)
                                          IF (man:QAAtCompletion) THEN
                                              glo:ErrorText = ''
                                              CompulsoryFieldCheck('C')
                                              IF (glo:ErrorText <> '') THEN
                                                  glo:ErrorText = 'QA FAILURE! You cannot QA because of the following error(s): <13,10>' & CLIP(glo:ErrorText)
                                                  Error_Text
                                                  glo:errortext = ''
      
                                                  Failure_Reason = ''
                                                  QA_Failure_Reason(Failure_Reason)
                                                  CompleteError# = 1
                                                      
                                                  GET(audit,0)
                                                  IF (access:audit.primerecord() = level:benign) THEN
                                                      aud:Notes         = 'FAILED COMPLETION CHECK'
                                                      aud:ref_number    = job:ref_number
                                                      aud:date          = today()
                                                      aud:time          = clock()
                                                      aud:type          = 'JOB'
                                                      access:users.clearkey(use:password_key)
                                                      use:password = glo:password
                                                      access:users.fetch(use:password_key)
                                                      aud:user = use:user_code
                                                      CASE QA_Type
                                                          OF 'PRE'
                                                              aud:action        = 'QA REJECTION ELECTRONIC: ' & CLIP(Failure_Reason)
                                                          OF 'PRI'
                                                              aud:action        = 'QA REJECTION MANUAL: ' & CLIP(Failure_Reason)
                                                      END
                                                      access:audit.insert()
                                                  END
                                                  glo:select1 = job:ref_number
                                                  IF (def:qa_failed_label = 'YES') THEN
                                                      QA_Failed_Label
                                                  END
                                                  IF (def:qa_failed_report = 'YES') THEN
                                                      QA_Failed
                                                  END
                                                  glo:select1 = ''
                                                  glo:notes_global    = ''
                                                  GetStatus(615,1,'JOB')
                                                  job:qa_rejected = 'YES'
                                                  job:date_qa_rejected    = Today()
                                                  job:time_qa_rejected    = Clock()
                                                  access:jobs.update()
                                                  !DO passed_fail_update
                                              ELSE
                                                  Complete# = 1
                                              END
                                          ELSE
                                              GetStatus(610,1,'JOB')
                                          END
                                          IF (CompleteError# = 0) THEN
                                              job:qa_passed      = 'YES'
                                              job:date_qa_passed = Today()
                                              job:time_qa_passed = Clock()
      
                                              GET(audit,0)
                                              IF (access:audit.primerecord() = level:benign) THEN
                                                  aud:notes         = ''
                                                  aud:ref_number    = job:ref_number
                                                  aud:date          = today()
                                                  aud:time          = clock()
                                                  access:users.clearkey(use:password_key)
                                                  use:password =glo:password
                                                  access:users.fetch(use:password_key)
                                                  aud:user = use:user_code
                                                  aud:action        = 'RAPID QA UPDATE: MANUAL QA PASSED'
                                                  access:audit.insert()
                                              END
                                              IF (def:QAPassLabel = 1) THEN
                                                  glo:Select1  = job:Ref_Number
                                                  QA_Passed_Label
                                                  glo:Select1  = ''
                                              END
      
                                              IF (Complete# = 1) THEN
                                                  IF (job:ignore_chargeable_charges <> 'YES') THEN
                                                      Pricing_Routine('C',labour",parts",pass",discount")
                                                      IF (pass" = True) THEN
                                                          job:labour_cost = labour"
                                                          job:parts_cost  = parts"
                                                      ELSE
                                                          job:labour_cost = 0
                                                          job:parts_cost  = 0
                                                      END
                                                  END
                                                  
                                                  IF (job:ignore_warranty_charges <> 'YES') THEN
                                                      Pricing_Routine('W',labour",parts",pass",discount")
                                                      IF (pass" = True) THEN
                                                          job:labour_cost_warranty    = labour"
                                                          job:parts_cost_warranty     = parts"
                                                      ELSE
                                                          job:labour_cost_warranty = 0
                                                          job:parts_cost_warranty = 0
                                                      END
                                                  END
      
                                                  DO CompleteJob
      
                                              END
                                              Access:JOBS.Update()
                                              !DO Update_Record
                                              !DO Activate_No_Panel
                                              !Do closing
                                              !DO Passed_Update
                                          END
                                      END
                              END
                          END
                      ELSE
                          !Do failed_update
                      END
                  END
              END
              DO Activate_No_Panel
          !END
      ELSE
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          MessageEx('Error! Cannot find the Manufacturer attached to this job!','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
      END
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?QAPassButton, Accepted)
    OF ?temp:warranty_charge_type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:warranty_charge_type, Accepted)
      IF (~0{prop:acceptall}) THEN
          IF (temp:Warranty_Charge_Type = '') THEN
              job:Warranty_Charge_Type = temp:Warranty_Charge_Type
              DO Disable_Warranty_Parts
          ELSE
              save_charge_type = job:Warranty_Charge_Type
              job:Warranty_Charge_Type = temp:Warranty_Charge_Type
              IF (CheckPricing('W') = Level:Benign) THEN
                  DO Enable_Parts
              ELSE
                  job:Warranty_Charge_Type = save_charge_type
                  temp:Warranty_Charge_Type  = job:Warranty_Charge_Type
              END
          END
          DISPLAY()
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:warranty_charge_type, Accepted)
    OF ?temp:repair_type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:repair_type, Accepted)
      IF (~0{prop:acceptall}) THEN
          IF (job:Repair_Type <> temp:Repair_Type) THEN
              IF (PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)) THEN
                  MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type, Unit Type and Repair Type.','ServiceBase 2000',|
                            'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemhand,msgex:samewidths,84,26,0)
              ELSIF ((job:date_completed <> '') AND (SecurityCheck('AMEND COMPLETED JOBS'))) THEN
                  MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                            'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemhand,msgex:samewidths,84,26,0)
              ELSIF ((job:invoice_Number <> '') AND (SecurityCheck('AMEND INVOICED JOBS'))) THEN
                  MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                            'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemhand,msgex:samewidths,84,26,0)
              ELSIF (job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = '') THEN
                  MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                            'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemhand,msgex:samewidths,84,26,0)
              ELSE
                  IF (temp:Repair_Type = '')
                      job:repair_type = temp:Repair_Type
                  ELSE
                      Access:REPAIRTY.ClearKey(rep:Model_Chargeable_Key)
                      rep:Model_Number = job:Model_Number
                      rep:Chargeable   = job:Chargeable_Job
                      rep:Repair_Type  = temp:Repair_Type
                      IF (Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) <> Level:Benign) THEN
                          POST(Event:Accepted, ?ChargeTypeButton)
                      ELSE
                          save_repair_type  = job:repair_type
                          job:repair_type =  temp:Repair_Type
                          IF (CheckPricing('C') <> 1) THEN
                              !IF (job:Repair_type = '') THEN
                              IF (save_repair_type = '') THEN
                                  !job:repair_type = temp:Repair_Type
                                  Do pricing
                              ELSE
                                  CASE MessageEx('Are you sure you want to change the repair type?','ServiceBase 2000',|
                                                  'Styles\question.ico','&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                                  beep:systemquestion,msgex:samewidths,84,26,0)
                                      OF 1 ! &Yes Button
                                          !job:repair_type = temp:Repair_Type
                                          Do pricing
                                      OF 2 ! &No Button
                                          job:repair_type = save_repair_type
                                          temp:Repair_Type = job:repair_type
                                  END
                              END
                          ELSE
                              job:repair_type = save_repair_type
                              temp:Repair_Type = job:repair_type
                          END
                      END
                  END
                  DISPLAY()
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:repair_type, Accepted)
    OF ?ChargeTypeButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChargeTypeButton, Accepted)
      If (job:Model_Number = '' OR job:Charge_Type = '' OR job:Account_Number = '' OR job:Unit_Type = '' OR |
          PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,temp:Repair_Type))  THEN
          MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type, and Unit Type.',|
                    'ServiceBase 2000','Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
          RepairType" = PickRepairTypes(job:Model_Number,job:Account_Number,job:Charge_Type,job:Unit_Type,'C')
          IF (RepairType" <> '') THEN
              temp:Repair_Type = RepairType"
              DISPLAY(?temp:Repair_Type)
              POST(Event:Accepted,?temp:Repair_Type)
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChargeTypeButton, Accepted)
    OF ?job:Warranty_Job
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Warranty_Job, Accepted)
      ! Start Change BE024 BE(17/12/03)
      If ~0{prop:acceptall}
          ChangeWarrantyJob()
      ! End Change BE024 BE(17/12/03)
      
          IF (job:warranty_Job = 'YES') THEN
              !Enable(?job:Warranty_Charge_Type)
              !Enable(?WarrantyButton)
              !Enable(?WarrantyTypePrompt)
              !Enable(?job:Repair_Type_Warranty)
              !Enable(?WarrantyTypeButton)
              !UNHIDE(?job:Warranty_Charge_Type)
              UNHIDE(?temp:Warranty_Charge_Type)
              UNHIDE(?WarrantyButton)
              UNHIDE(?WarrantyTypePrompt)
              !UNHIDE(?job:Repair_Type_Warranty)
              UNHIDE(?temp:Repair_Type_Warranty)
              UNHIDE(?WarrantyTypeButton)
              DO Enable_Parts
          ELSE
              !Disable(?job:Warranty_Charge_Type)
              !Disable(?WarrantyButton)
              !Disable(?WarrantyTypePrompt)
              !Disable(?job:Repair_Type_Warranty)
              !Disable(?WarrantyTypeButton)
              !HIDE(?job:Warranty_Charge_Type)
              HIDE(?temp:Warranty_Charge_Type)
              HIDE(?WarrantyButton)
              HIDE(?WarrantyTypePrompt)
              !HIDE(?job:Repair_Type_Warranty)
              HIDE(?temp:Repair_Type_Warranty)
              HIDE(?WarrantyTypeButton)
              DO Disable_Warranty_Parts
          END
          DISPLAY()
      ! Start Change BE024 BE(17/12/03)
      END
      ! End Change BE024 BE(17/12/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Warranty_Job, Accepted)
    OF ?warrantyButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?warrantyButton, Accepted)
      glo:select1 = 'YES'
      GlobalRequest = SelectRecord
      Browse_Warranty_Charge_Types()
      IF (globalresponse = RequestCompleted) THEN
          temp:warranty_charge_type = cha:charge_type
          DISPLAY(?temp:warranty_charge_type)
          POST(EVENT:Accepted, ?temp:warranty_charge_type)
      END
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?warrantyButton, Accepted)
    OF ?jfc_value_1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_1, Accepted)
      IF (Edit_Fault_Code(1) = 0) THEN
          POST(Event:Accepted, ?JFC1_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_1, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[1] = TINCALENDARStyle1(jfc_value[1])
          Display(?jfc_value_1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC1_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC1_Button, Accepted)
      !IF (?JFC1_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(1)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC1_Button, Accepted)
    OF ?jfc_value_2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_2, Accepted)
      IF (Edit_Fault_Code(2) = 0) THEN
          POST(Event:Accepted, ?JFC2_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_2, Accepted)
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[2] = TINCALENDARStyle1(jfc_value[2])
          Display(?jfc_value_2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC2_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC2_Button, Accepted)
      !IF (?JFC2_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(2)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC2_Button, Accepted)
    OF ?jfc_value_3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_3, Accepted)
      IF (Edit_Fault_Code(3) = 0) THEN
          POST(Event:Accepted, ?JFC3_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_3, Accepted)
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[3] = TINCALENDARStyle1(jfc_value[3])
          Display(?jfc_value_3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC3_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC3_Button, Accepted)
      !IF (?JFC3_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(3)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC3_Button, Accepted)
    OF ?jfc_value_4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_4, Accepted)
      IF (Edit_Fault_Code(4) = 0) THEN
          POST(Event:Accepted, ?JFC4_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_4, Accepted)
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[4] = TINCALENDARStyle1(jfc_value[4])
          Display(?jfc_value_4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC4_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC4_Button, Accepted)
      !IF (?JFC4_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(4)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC4_Button, Accepted)
    OF ?jfc_value_5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_5, Accepted)
      IF (Edit_Fault_Code(5) = 0) THEN
          POST(Event:Accepted, ?JFC5_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_5, Accepted)
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[5] = TINCALENDARStyle1(jfc_value[5])
          Display(?jfc_value_5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC5_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC5_Button, Accepted)
      !IF (?JFC5_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(5)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC5_Button, Accepted)
    OF ?jfc_value_6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_6, Accepted)
      IF (Edit_Fault_Code(6) = 0) THEN
          POST(Event:Accepted, ?JFC6_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_6, Accepted)
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[6] = TINCALENDARStyle1(jfc_value[6])
          Display(?jfc_value_6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC6_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC6_Button, Accepted)
      !IF (?JFC6_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(6)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC6_Button, Accepted)
    OF ?jfc_value_7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_7, Accepted)
      IF (Edit_Fault_Code(7) = 0) THEN
          POST(Event:Accepted, ?JFC7_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_7, Accepted)
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[7] = TINCALENDARStyle1(jfc_value[7])
          Display(?jfc_value_7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC7_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC7_Button, Accepted)
      !IF (?JFC7_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(7)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC7_Button, Accepted)
    OF ?jfc_value_8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_8, Accepted)
      IF (Edit_Fault_Code(9) = 0) THEN
          POST(Event:Accepted, ?JFC9_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_8, Accepted)
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[8] = TINCALENDARStyle1(jfc_value[8])
          Display(?jfc_value_8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC8_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC8_Button, Accepted)
      !IF (?JFC8_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(8)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC8_Button, Accepted)
    OF ?jfc_value_9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_9, Accepted)
      IF (Edit_Fault_Code(9) = 0) THEN
          POST(Event:Accepted, ?JFC9_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_9, Accepted)
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[9] = TINCALENDARStyle1(jfc_value[9])
          Display(?jfc_value_9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC9_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC9_Button, Accepted)
      !IF (?JFC9_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(9)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC9_Button, Accepted)
    OF ?jfc_value_10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_10, Accepted)
      IF (Edit_Fault_Code(10) = 0) THEN
          POST(Event:Accepted, ?JFC10_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_10, Accepted)
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[10] = TINCALENDARStyle1(jfc_value[10])
          Display(?jfc_value_10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC10_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC10_Button, Accepted)
      !IF (?JFC10_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(10)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC10_Button, Accepted)
    OF ?jfc_value_11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_11, Accepted)
      IF (Edit_Fault_Code(11) = 0) THEN
          POST(Event:Accepted, ?JFC11_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_11, Accepted)
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[11] = TINCALENDARStyle1(jfc_value[11])
          Display(?jfc_value_11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC11_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC11_Button, Accepted)
      !IF (?JFC11_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(11)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC11_Button, Accepted)
    OF ?jfc_value_12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_12, Accepted)
      IF (Edit_Fault_Code(12) = 0) THEN
          POST(Event:Accepted, ?JFC12_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jfc_value_12, Accepted)
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jfc_value[12] = TINCALENDARStyle1(jfc_value[12])
          Display(?jfc_value_12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?JFC12_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC12_Button, Accepted)
      !IF (?JFC12_Button{prop:hide} = 0) THEN
          Lookup_Fault_Code(12)
      !END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JFC12_Button, Accepted)
    OF ?TFC1_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC1_Button, Accepted)
      Lookup_Trade_Fault_Code(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC1_Button, Accepted)
    OF ?PopCalendar:24
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[12] = TINCALENDARStyle1(tfc_value[12])
          Display(?tfc_value_12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc_value_1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_1, Accepted)
      IF (Edit_Trade_Fault_Code(1) = 0) THEN
          POST(Event:Accepted, ?TFC1_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_1, Accepted)
    OF ?PopCalendar:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[1] = TINCALENDARStyle1(tfc_value[1])
          Display(?tfc_value_1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc_value_2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_2, Accepted)
      IF (Edit_Trade_Fault_Code(2) = 0) THEN
          POST(Event:Accepted, ?TFC2_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_2, Accepted)
    OF ?TFC2_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC2_Button, Accepted)
      Lookup_Trade_Fault_Code(2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC2_Button, Accepted)
    OF ?PopCalendar:14
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[2] = TINCALENDARStyle1(tfc_value[2])
          Display(?tfc_value_2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc_value_3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_3, Accepted)
      IF (Edit_Trade_Fault_Code(3) = 0) THEN
          POST(Event:Accepted, ?TFC3_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_3, Accepted)
    OF ?TFC3_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC3_Button, Accepted)
      Lookup_Trade_Fault_Code(3)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC3_Button, Accepted)
    OF ?PopCalendar:15
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[3] = TINCALENDARStyle1(tfc_value[3])
          Display(?tfc_value_3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc_value_8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_8, Accepted)
      IF (Edit_Trade_Fault_Code(8) = 0) THEN
          POST(Event:Accepted, ?TFC8_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_8, Accepted)
    OF ?TFC7_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC7_Button, Accepted)
      Lookup_Trade_Fault_Code(7)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC7_Button, Accepted)
    OF ?tfc_value_4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_4, Accepted)
      IF (Edit_Trade_Fault_Code(4) = 0) THEN
          POST(Event:Accepted, ?TFC4_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_4, Accepted)
    OF ?TFC4_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC4_Button, Accepted)
      Lookup_Trade_Fault_Code(4)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC4_Button, Accepted)
    OF ?tfc_value_9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_9, Accepted)
      IF (Edit_Trade_Fault_Code(9) = 0) THEN
          POST(Event:Accepted, ?TFC9_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_9, Accepted)
    OF ?TFC8_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC8_Button, Accepted)
      Lookup_Trade_Fault_Code(8)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC8_Button, Accepted)
    OF ?PopCalendar:21
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[9] = TINCALENDARStyle1(tfc_value[9])
          Display(?tfc_value_9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:18
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[6] = TINCALENDARStyle1(tfc_value[6])
          Display(?tfc_value_6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc_value_5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_5, Accepted)
      IF (Edit_Trade_Fault_Code(5) = 0) THEN
          POST(Event:Accepted, ?TFC5_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_5, Accepted)
    OF ?TFC5_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC5_Button, Accepted)
      Lookup_Trade_Fault_Code(5)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC5_Button, Accepted)
    OF ?PopCalendar:16
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[4] = TINCALENDARStyle1(tfc_value[4])
          Display(?tfc_value_4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc_value_10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_10, Accepted)
      IF (Edit_Trade_Fault_Code(10) = 0) THEN
          POST(Event:Accepted, ?TFC10_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_10, Accepted)
    OF ?PopCalendar:22
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[10] = TINCALENDARStyle1(tfc_value[10])
          Display(?tfc_value_10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:19
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[7] = TINCALENDARStyle1(tfc_value[7])
          Display(?tfc_value_7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?TFC9_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC9_Button, Accepted)
      Lookup_Trade_Fault_Code(9)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC9_Button, Accepted)
    OF ?TFC11_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC11_Button, Accepted)
      Lookup_Trade_Fault_Code(11)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC11_Button, Accepted)
    OF ?PopCalendar:23
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[11] = TINCALENDARStyle1(tfc_value[11])
          Display(?tfc_value_11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:20
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[8] = TINCALENDARStyle1(tfc_value[8])
          Display(?tfc_value_8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tfc_value_6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_6, Accepted)
      IF (Edit_Trade_Fault_Code(6) = 0) THEN
          POST(Event:Accepted, ?TFC6_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_6, Accepted)
    OF ?tfc_value_11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_11, Accepted)
      IF (Edit_Trade_Fault_Code(11) = 0) THEN
          POST(Event:Accepted, ?TFC11_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_11, Accepted)
    OF ?TFC10_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC10_Button, Accepted)
      Lookup_Trade_Fault_Code(10)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC10_Button, Accepted)
    OF ?tfc_value_7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_7, Accepted)
      IF (Edit_Trade_Fault_Code(7) = 0) THEN
          POST(Event:Accepted, ?TFC7_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_7, Accepted)
    OF ?PopCalendar:17
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tfc_value[5] = TINCALENDARStyle1(tfc_value[5])
          Display(?tfc_value_5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?TFC6_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC6_Button, Accepted)
      Lookup_Trade_Fault_Code(6)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC6_Button, Accepted)
    OF ?tfc_value_12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_12, Accepted)
      IF (Edit_Trade_Fault_Code(12) = 0) THEN
          POST(Event:Accepted, ?TFC12_Button)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfc_value_12, Accepted)
    OF ?TFC12_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC12_Button, Accepted)
      Lookup_Trade_Fault_Code(12)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TFC12_Button, Accepted)
    OF ?temp:Charge_Part_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:Charge_Part_Number, Accepted)
      PartRef# = 0
      IF ((job:chargeable_job = 'NO') OR (job:charge_type = '')) THEN
          MessageEx('You must select a Charge Type before you can insert any parts','ServiceBase 2000',|
                    'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?temp:Charge_Part_Number)
      ELSIF (temp:Charge_Part_Number = 'ADJUSTMENT') THEN
          POST(Event:Accepted, ?PartAdjustment)
      ELSIF (CheckPartNo(temp:Charge_Part_Number,PartRef#) = 0) THEN
          MessageEx('Invalid Part No.','ServiceBase 2000',|
                    'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?temp:Charge_Part_Number)
      ELSIF (CheckChargeablePartExists(temp:Charge_Part_Number) = 0) THEN
          MessageEx('Part No. already exists','ServiceBase 2000',|
                    'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?temp:Charge_Part_Number)
      ELSE
          !POST(Event:Accepted, ?PartInsert)
          !GET(parts,0)
          !IF (access:parts.primerecord() = level:benign) THEN
          !    GlobalRequest = InsertRecord
          !    AddChargeParts
          !    GlobalRequest = ''
          !    MESSAGE('DEBUG 1')
          !    IF (GlobalResponse = RequestCompleted) THEN
          !        MESSAGE('DEBUG 2')
          !        IF (access:parts.insert() <> Level:Benign) THEN
          !            MESSAGE('DEBUG 3')
          !            access:parts.cancelautoinc()
          !        END
          !        MESSAGE('DEBUG 4')
          !       BRW4.ResetFromFile()
          !       BRW4.ResetSort(1)
          !   END
          !END
          Add_Chargeable(PartRef#)
          BRW4.ResetFromFile()
          ! Start change 3288 BE(23/09/03)
          temp:Charge_part_Number = ''
          DISPLAY(?temp:Charge_Part_Number)
          ! End Change 3288 BE(23/09/03)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:Charge_Part_Number, Accepted)
    OF ?PartInsert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PartInsert, Accepted)
      DO Auto_Allocate_Job
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PartInsert, Accepted)
    OF ?PartAdjustment
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PartAdjustment, Accepted)
      GET(parts,0)
      IF (access:parts.primerecord() = level:benign) THEN
          par:ref_number            = job:ref_number
          par:adjustment            = 'YES'
          par:part_number           = 'ADJUSTMENT'
          par:description           = 'ADJUSTMENT'
          par:quantity              = 1
          par:warranty_part         = 'NO'
          par:exclude_from_order    = 'YES'
          IF (access:parts.insert()) THEN
              access:parts.cancelautoinc()
          END
      END
      !BRW4.ResetSort(1)
      BRW4.ResetFromFile()
      
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PartAdjustment, Accepted)
    OF ?temp:Warranty_Part_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:Warranty_Part_Number, Accepted)
      PartRef# = 0
      IF ((job:warranty_job = 'NO') OR (job:warranty_charge_type = '')) THEN
          MessageEx('You must select a Charge Type before you can insert any parts','ServiceBase 2000',|
                    'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?temp:Warranty_Part_Number)
      ELSIF (temp:Warranty_Part_Number = 'ADJUSTMENT') THEN
          POST(Event:Accepted, ?WarAdjustment)
      ELSIF (CheckPartNo(temp:Warranty_Part_Number,PartRef#) = 0) THEN
          MessageEx('Invalid Part No.','ServiceBase 2000',|
                    'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?temp:Warranty_Part_Number)
      ELSIF (CheckWarrantyPartExists(temp:Warranty_Part_Number) = 0) THEN
          MessageEx('Part No. already exists','ServiceBase 2000',|
                    'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?temp:Charge_Part_Number)
      ELSE
          !POST(Event:Accepted, ?WarInsert)
          !GET(warparts,0)
          !IF (access:warparts.primerecord() = level:benign) THEN
          !    GlobalRequest = InsertRecord
          !    AddWarrantyParts
          !    IF (GlobalResponse = RequestCompleted) THEN
          !        IF (access:warparts.insert() = Level:Benign) THEN
          !            BRW19.ResetFromFile()
          !        ELSE
          !            access:warparts.cancelautoinc()
          !        END
          !    END
          !END
          Add_Warranty(PartRef#)
          BRW19.ResetFromFile()
          ! Start change 3288 BE(23/09/03)
          temp:Warranty_part_Number = ''
          DISPLAY(?temp:Warranty_Part_Number)
          ! End Change 3288 BE(23/09/03)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:Warranty_Part_Number, Accepted)
    OF ?WarInsert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?WarInsert, Accepted)
      DO Auto_Allocate_Job
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?WarInsert, Accepted)
    OF ?WarAdjustment
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?WarAdjustment, Accepted)
      found# = 0
      access:manufact.clearkey(man:manufacturer_key)
      man:manufacturer    = job:manufacturer
      IF (access:manufact.tryfetch(man:manufacturer_key) = Level:benign) THEN
          IF (man:includeadjustment = 'YES' AND man:adjustpart = 1) THEN
              found# = 1
          END
      END
      IF (found# = 0) THEN
          access:chartype.clearkey(cha:charge_type_key)
          cha:charge_type = job:warranty_charge_type
          IF (access:chartype.fetch(cha:charge_type_key) = Level:Benign) THEN
              IF (cha:force_warranty = 'YES') THEN
                  save_map_id = access:manfaupa.savefile()
                  access:manfaupa.clearkey(map:field_number_key)
                  map:manufacturer = job:manufacturer
                  SET(map:field_number_key,map:field_number_key)
                  LOOP
                      IF ((access:manfaupa.next() <> Level:Benign) OR (map:manufacturer <> job:manufacturer )) THEN
                         BREAK
                      END
                      IF (map:compulsory = 'YES') THEN
                          found# = 1
                          BREAK
                      END
                  END
                  access:manfaupa.restorefile(save_map_id)
              END
          END
      END
      IF (found# = 1) THEN
          CLEAR(wpr:Record)
          Globalrequest = Insertrecord
          glo:select1 = 'ADJUSTMENT'
          glo:select2 = job:ref_number
          Update_Warranty_Part
          glo:select1 = ''
          glo:select2 = ''
      ELSE
          GET(warparts,0)
          IF (access:warparts.primerecord() = level:benign) THEN
              wpr:ref_number           = job:ref_number
              wpr:adjustment           = 'YES'
              wpr:part_number          = 'ADJUSTMENT'
              wpr:description          = 'ADJUSTMENT'
              wpr:quantity             = 1
              wpr:warranty_part        = 'YES'
              wpr:exclude_from_order   = 'YES'
              IF (access:warparts.insert()) THEN
                  access:warparts.cancelautoinc()
              END
          END
      END
      !BRW19.ResetSort(1)
      BRW19.ResetFromFile()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?WarAdjustment, Accepted)
    OF ?temp:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:Account_Number, Accepted)
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = temp:account_number
      IF (access:subtracc.fetch(sub:account_number_key) <> level:benign) THEN
          temp:account_number = ''
          POST(Event:Accepted, ?PickAccountButton)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:Account_Number, Accepted)
    OF ?PickAccountButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickAccountButton, Accepted)
      GlobalRequest = SelectRecord
      PickSubAccounts
      temp:Account_Number = sub:Account_Number
      DISPLAY(?temp:Account_Number)
      IF (temp:Account_Number = '') THEN
          SELECT(?temp:Account_Number)
      ELSE
          SELECT(?temp:Order_Number)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickAccountButton, Accepted)
    OF ?temp:Order_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:Order_Number, Accepted)
      IF (temp:Account_Number = '') THEN
          MessageEx('Please Choose an Account Number.','ServiceBase 2000',|
                         'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?temp:Account_Number)
      ELSIF (INCOMPLETE()) THEN
          temp:Account_Number = job:account_number
          DISPLAY(?temp:Account_Number)
          temp:Order_Number = job:order_number
          DISPLAY(?temp:Order_Number)
          temp:job_reference = job:ref_number
          DISPLAY(?temp:job_reference)
          SELECT(INCOMPLETE())
      ELSE
          DO Update_Record
      
          savejobs = access:jobs.savefile()
          savejobse = access:jobse.savefile()
          savejobnotes = access:jobnotes.savefile()
          Access:JOBS.ClearKey(job:AccOrdNoKey)
          job:Account_Number = temp:Account_Number
          job:Order_Number = temp:Order_Number
          SET(job:AccOrdNoKey, job:AccOrdNoKey)
          count# = 0
          iwcount# = 0
          LOOP
              IF ((access:jobs.next() <> Level:Benign) OR |
                   (job:Account_Number <> temp:Account_Number) OR |
                   (job:Order_Number <> temp:Order_Number)) THEN
                  BREAK
              END
              ! Start Change 3288 BE(23/09/03)
              !IF ((job:Workshop = 'YES') AND (job:Location <> '')) THEN
              !    iwcount# += 1
              !    CYCLE
              !END
              ! Start Change 3689 BE(12/12/03)
              !IF ((job:Workshop = 'YES') AND (job:Location <> '') AND (job:date_completed <> '')) THEN
              IF ((job:Workshop = 'YES' AND job:Location <> '') OR (job:date_completed <> '')) THEN
              ! End Change 3689 BE(12/12/03)
                  iwcount# += 1
                  CYCLE
              END
              ! End Change 3288 BE(23/09/03)
              count# += 1
              IF (count# > 1) THEN
                  IF (PickJob_Order(temp:Account_Number, temp:Order_Number) = 0) THEN
                      count# = -1
                  END
                  BREAK
              END
          END
      
          IF ((count# = 0) AND(iwcount# > 0)) THEN
              MessageEx('Record is already marked as In Workshop','ServiceBase 2000',|
                         'Styles\warn.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
              count# = -1
          END
      
          IF (count# > 0) THEN
              IF (count# = 1) THEN
                  IF (iwcount# = 0) THEN
                      Access:JOBS.ClearKey(job:AccOrdNoKey)
                      job:Account_Number = temp:Account_Number
                      job:Order_Number = temp:Order_Number
                      access:jobs.fetch(job:AccOrdNoKey)
                  ELSE
                      Access:JOBS.ClearKey(job:AccOrdNoKey)
                      job:Account_Number = temp:Account_Number
                      job:Order_Number = temp:Order_Number
                      SET(job:AccOrdNoKey, job:AccOrdNoKey)
                      LOOP
                          IF ((access:jobs.next() <> Level:Benign) OR |
                              (job:Account_Number <> temp:Account_Number) OR |
                              (job:Order_Number <> temp:Order_Number)) THEN
                              BREAK
                          END
                          IF ((job:Workshop = 'YES') AND (job:Location <> '')) THEN
                              CYCLE
                          END
                          BREAK
                      END
                  END
              END
              RecordState = rs:UpdateRecord
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey) <> Level:Benign) THEN
                  IF (Access:JOBSE.Primerecord() = Level:Benign) THEN
                      jobe:RefNumber  = job:Ref_Number
                      IF (Access:JOBSE.Tryinsert() <> Level:Benign) THEN
                          Access:JOBSE.Cancelautoinc()
                      END
                  END
              END
              access:jobnotes.clearkey(JBN:RefNumberKey)
              jbn:refnumber   = job:ref_number
              IF (access:jobnotes.tryfetch(JBN:RefNumberKey) <> Level:benign) THEN
                  IF (access:jobnotes.primerecord() = Level:Benign) THEN
                      jbn:refnumber   = job:ref_number
                      IF (access:jobnotes.tryinsert() <> Level:Benign) THEN
                          access:jobnotes.cancelautoinc()
                      END
                  END
              END
              temp_IMEI_Number = ''
              temp:job_reference = job:ref_number
              !temp:charge_type = job:charge_type
              !temp:repair_type = job:repair_type
              !temp:warranty_charge_type = job:warranty_charge_type
              !temp:repair_type_warranty = job:repair_type_warranty
      
              ! Start Change 4239 BE(24/05/04)
              Access:MANUFACT.ClearKey(man:Manufacturer_Key)
              man:Manufacturer = job:Manufacturer
              IF (Access:MANUFACT.Fetch(man:Manufacturer_Key) = Level:Benign) THEN
                  MSN_Field_Mask = man:MSNFieldMask
              ELSE
                  MSN_Field_Mask = ''
              END
              ! End Change 4239 BE(24/05/04)
      
              DISPLAY(?temp:job_reference)
              DO Show_Job_Fault_Codes
              DO Set_Checkboxes
              DO Activate_Bookin_Panel
              SELECT(?WarrantyPartsTab)
              SELECT(?ChargeablePartsTab)
              SELECT(?TFCTab)
              SELECT(?MFCTab)
          ELSIF (count# = 0) THEN
              CASE MessageEx('Record Not Found.  Do you want to create a new entry ?','ServiceBase 2000',|
                         'Styles\warn.ico','&OK|&No',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
              OF 1 ! YES
                  IF (access:JOBS.primerecord() = Level:Benign) THEN
                      job:Account_Number = temp:Account_Number
                      job:Order_Number = temp:Order_Number
                      SaveRequest# = GlobalRequest
                      GlobalRequest = InsertRecord
                      UpdateJOBS
                      GlobalRequest = SaveRequest#
                      IF (GlobalResponse = RequestCompleted) THEN
                          RecordState = rs:UpdateRecord
                          temp_IMEI_Number = ''
                          !temp:charge_type = job:charge_type
                          !temp:repair_type = job:repair_type
                          !temp:warranty_charge_type = job:warranty_charge_type
                          !temp:repair_type_warranty = job:repair_type_warranty
                          DO Show_Job_Fault_Codes
                          DO Set_Checkboxes
                          DO Activate_Bookin_Panel
                          SELECT(?WarrantyPartsTab)
                          SELECT(?ChargeablePartsTab)
                          SELECT(?TFCTab)
                          SELECT(?MFCTab)
                      ELSE
                          access:jobs.cancelautoinc()
                          SELECT(?temp:Order_Number)
                          access:jobs.restorefile(savejobs)
                          access:jobse.restorefile(savejobse)
                          access:jobnotes.restorefile(savejobnotes)
                      END
                  ELSE
                      temp:Order_Number = ''
                      DISPLAY(?temp:Order_Number)
                      SELECT(?temp:Order_Number)
                      access:jobs.restorefile(savejobs)
                      access:jobse.restorefile(savejobse)
                      access:jobnotes.restorefile(savejobnotes)
                  END
              ELSE
                  temp:Order_Number = ''
                  DISPLAY(?temp:Order_Number)
                  SELECT(?temp:Order_Number)
                  access:jobs.restorefile(savejobs)
                  access:jobse.restorefile(savejobse)
                  access:jobnotes.restorefile(savejobnotes)
              END
          ELSE
              temp:Order_Number = ''
              DISPLAY(?temp:Order_Number)
              SELECT(?temp:Order_Number)
              access:jobs.restorefile(savejobs)
              access:jobse.restorefile(savejobse)
              access:jobnotes.restorefile(savejobnotes)
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:Order_Number, Accepted)
    OF ?temp:job_reference
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:job_reference, Accepted)
      IF (temp:Job_Reference = '') THEN
          MessageEx('Please Choose a Job Number.','ServiceBase 2000',|
                         'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?temp:Job_Reference)
      ELSIF (INCOMPLETE()) THEN
          temp:Account_Number = job:account_number
          DISPLAY(?temp:Account_Number)
          temp:Order_Number = job:order_number
          DISPLAY(?temp:Order_Number)
          temp:Job_Reference = job:ref_number
          DISPLAY(?temp:Job_Reference)
          SELECT(INCOMPLETE())
      ELSE
          DO Update_Record
      
          savejobs = access:jobs.savefile()
          savejobse = access:jobse.savefile()
          savejobnotes = access:jobnotes.savefile()
          count# = 0
          Access:JOBS.ClearKey(job:Ref_Number_Key)
          job:Ref_Number = temp:job_reference
          IF (access:jobs.fetch(job:Ref_Number_Key) = Level:Benign) THEN
              IF ((job:Workshop = 'YES') AND (job:Location <> '')) THEN
                  MessageEx('Record is already marked as In Workshop','ServiceBase 2000',|
                            'Styles\warn.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemhand,msgex:samewidths,84,26,0)
                  Count# = -1
              ELSIF (job:date_completed <> '') THEN
                  MessageEx('Record is a completed job','ServiceBase 2000',|
                            'Styles\warn.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                            beep:systemhand,msgex:samewidths,84,26,0)
                  Count# = -1
              ELSE
                  Count# = 1
              END
          END
      
          IF (count# > 0) THEN
              RecordState = rs:UpdateRecord
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey) <> Level:Benign) THEN
                  IF (Access:JOBSE.Primerecord() = Level:Benign) THEN
                      jobe:RefNumber  = job:Ref_Number
                      IF (Access:JOBSE.Tryinsert() <> Level:Benign) THEN
                          Access:JOBSE.Cancelautoinc()
                      END
                  END
              END
              access:jobnotes.clearkey(JBN:RefNumberKey)
              jbn:refnumber   = job:ref_number
              IF (access:jobnotes.tryfetch(JBN:RefNumberKey) <> Level:benign) THEN
                  IF (access:jobnotes.primerecord() = Level:Benign) THEN
                      jbn:refnumber   = job:ref_number
                      IF (access:jobnotes.tryinsert() <> Level:Benign) THEN
                          access:jobnotes.cancelautoinc()
                      END
                  END
              END
              temp_IMEI_Number = ''
              temp:Account_Number = job:account_number
              DISPLAY(?temp:Account_Number)
              temp:Order_Number = job:order_number
              DISPLAY(?temp:Order_Number)
              !temp:charge_type = job:charge_type
              !temp:repair_type = job:repair_type
              !temp:warranty_charge_type = job:warranty_charge_type
              !temp:repair_type_warranty = job:repair_type_warranty
      
              ! Start Change 4239 BE(24/05/04)
              Access:MANUFACT.ClearKey(man:Manufacturer_Key)
              man:Manufacturer = job:Manufacturer
              IF (Access:MANUFACT.Fetch(man:Manufacturer_Key) = Level:Benign) THEN
                  MSN_Field_Mask = man:MSNFieldMask
              ELSE
                  MSN_Field_Mask = ''
              END
              ! End Change 4239 BE(24/05/04)
      
              DO Show_Job_Fault_Codes
              DO Set_Checkboxes
              DO Activate_Bookin_Panel
              SELECT(?WarrantyPartsTab)
              SELECT(?ChargeablePartsTab)
              SELECT(?TFCTab)
              SELECT(?MFCTab)
          ELSIF (count# = 0) THEN
              CASE MessageEx('Record Not Found.  Do you want to create a new entry ?','ServiceBase 2000',|
                         'Styles\warn.ico','&OK|&No',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
              OF 1 ! YES
                  IF (access:JOBS.primerecord() = Level:Benign) THEN
                      job:Account_Number = temp:Account_Number
                      job:Order_Number = temp:Order_Number
                      SaveRequest# = GlobalRequest
                      GlobalRequest = InsertRecord
                      UpdateJOBS
                      GlobalRequest = SaveRequest#
                      IF (GlobalResponse = RequestCompleted) THEN
                          RecordState = rs:UpdateRecord
                          temp_IMEI_Number = ''
                          !temp:charge_type = job:charge_type
                          !temp:repair_type = job:repair_type
                          !temp:warranty_charge_type = job:warranty_charge_type
                          !temp:repair_type_warranty = job:repair_type_warranty
                          DO Show_Job_Fault_Codes
                          DO Set_Checkboxes
                          DO Activate_Bookin_Panel
                          SELECT(?WarrantyPartsTab)
                          SELECT(?ChargeablePartsTab)
                          SELECT(?TFCTab)
                          SELECT(?MFCTab)
                      ELSE
                          access:jobs.cancelautoinc()
                          SELECT(?temp:job_reference)
                          access:jobs.restorefile(savejobs)
                          access:jobse.restorefile(savejobse)
                          access:jobnotes.restorefile(savejobnotes)
                      END
                  ELSE
                      temp:Order_Number = ''
                      DISPLAY(?temp:job_reference)
                      SELECT(?temp:job_reference)
                      access:jobs.restorefile(savejobs)
                      access:jobse.restorefile(savejobse)
                      access:jobnotes.restorefile(savejobnotes)
                  END
              ELSE
                  temp:Order_Number = ''
                  DISPLAY(?temp:job_reference)
                  SELECT(?temp:job_reference)
                  access:jobs.restorefile(savejobs)
                  access:jobse.restorefile(savejobse)
                  access:jobnotes.restorefile(savejobnotes)
              END
          ELSE
              temp:job_reference = ''
              DISPLAY(?temp:job_reference)
              SELECT(?temp:job_reference)
              access:jobs.restorefile(savejobs)
              access:jobse.restorefile(savejobse)
              access:jobnotes.restorefile(savejobnotes)
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?temp:job_reference, Accepted)
    OF ?job:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:MSN, Accepted)
      ! Start Change 4239 BE(24/05/04)
      !IF (MSN_Field_Mask <> '') THEN
      IF (job:warranty_job = 'YES' AND MSN_Field_Mask <> '') THEN
          IF ((job:MSN <> '') AND (CheckFaultFormat(job:MSN, MSN_Field_Mask))) THEN
              BEEP()
              MessageEx('MSN Failed Format Validation',|
                        'ServiceBase 2000','Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,|
                        CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
              job:MSN = ''
              SELECT(?job:MSN)
              CYCLE
          END
      END
      ! End Change 4239 BE(24/05/04)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:MSN, Accepted)
    OF ?PickModelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickModelButton, Accepted)
      GlobalRequest = SelectRecord
      Browse_Model_Numbers
      IF ((GlobalResponse = RequestCompleted) AND (mod:Model_Number <> '')) THEN
          job:Model_Number = mod:Model_Number
          ! Start Change 3157 BE(29/08/03)
          !job:Manufacturer = mod:Manufacturer
          !job:fault_code1 = mod:product_type
          !job:unit_type = mod:unit_type
          !job:edi = PendingJob(job:manufacturer)
          !DISPLAY(?job:model_number)
          POST(EVENT:ACCEPTED, ?job:Model_Number)
          ! Ebd Change 3157 BE(29/08/03)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickModelButton, Accepted)
    OF ?job:Workshop
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Workshop, Accepted)
      IF (job:Workshop) THEN
          jobe:InWorkshopDate = Today()
          jobe:InWorkShopTime = CLOCK()
      ELSE
          jobe:InWorkshopDate = ''
          jobe:InWorkShopTime = ''
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Workshop, Accepted)
    OF ?job:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Location, Accepted)
      IF (job:location <> '') THEN
          access:locinter.clearkey(loi:location_key)
          loi:location = job:location
          IF (access:locinter.fetch(loi:location_key) <> Level:Benign) THEN
              POST(Event:Accepted, ?PickLocationButton)
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Location, Accepted)
    OF ?PickLocationButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickLocationButton, Accepted)
      GlobalRequest = SelectRecord
      Browse_Locations
      IF ((GlobalResponse = RequestCompleted) AND (loi:Location <> '')) THEN
          job:location = loi:Location
          DISPLAY(?job:location)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickLocationButton, Accepted)
    OF ?BookOKButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BookOKButton, Accepted)
      IF (job:Workshop = 'NO') THEN
          MessageEx('Please Mark job as In Workshop.','ServiceBase 2000',|
                         'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?job:Workshop)
      ELSIF (job:location = '') THEN
          MessageEx('Please Choose a Location.','ServiceBase 2000',|
                         'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?job:location)
      ELSE
          GetStatus(305,1,'JOB')  ! Awaiting Allocation
          DO Update_Record
      
          Bookin_JobLabel(job:ref_number)
      
          ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          ! Add Call to print Job Card
          ! temporary Requirement re telephone call
          ! Tony Dow 26/08/2003
          !
          ! Removed at request of Tony Dow
          ! telephone call 05/09/2003
          !glo:select1 = job:ref_number
          !Job_Card
          ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
          DO Activate_TestAndRepair_Panel
      
          ! Start change 3288 BE(23/09/03)
          SELECT(?temp:Account_Number)
          ! End Change 3288 BE(23/09/03)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BookOKButton, Accepted)
    OF ?AllocateCommonFaultsButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllocateCommonFaultsButton, Accepted)
      IF (SecurityCheck('COMMON FAULTS - APPLY TO JOBS') <> Level:Benign) THEN
          MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
          DO Update_Record
          glo:select1 = job:model_number
          SaveRequest# = GlobalRequest
          GlobalRequest = SelectRecord
          Browse_Common_faults
          GlobalRequest = SaveRequest#
      
          IF (GlobalResponse = RequestCompleted) THEN
      
              CommonFaults(com:ref_number)
      
              IF (job:ignore_warranty_charges <> 'YES') THEN
                  Pricing_Routine('W',labour",parts",pass",a")
                  IF (pass" = True) THEN
                      job:labour_cost_warranty = labour"
                      job:parts_cost_warranty  = parts"
                      job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                  END
              END
      
              IF (job:ignore_Chargeable_charges <> 'YES') THEN
                  Pricing_Routine('C',labour",parts",pass",a")
                  IF (pass" = True) THEN
                      job:labour_cost = labour"
                      job:parts_cost  = parts"
                      job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
                  END
              END
              DO Auto_Allocate_Job
              DO Update_Record
              !access:jobs.update()
              BRW4.ResetSort(1)
              BRW19.ResetSort(1)
              DISPLAY()
          END
          glo:select1 = ''
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllocateCommonFaultsButton, Accepted)
    OF ?CompleteJobButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CompleteJobButton, Accepted)
      IF ((job:date_completed <> '') AND SecurityCheck('JOBS - NO RECOMPLETE') = Level:Benign) THEN
          MessageEx('You do not have access to this option.','ServiceBase 2000',|
                    'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
          CASE MessageEx('You are about to complete this job.<13,10><13,10>(Warning! You will not be able to uncomplete it if you continue.)<13,10>Are you sure?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                         beep:systemquestion,msgex:samewidths,84,26,0)
              OF 1 ! &Yes Button
                  SET(DEFAULTS)
                  Access:DEFAULTS.Next()
                  !Save the notes field
                  !Access:JOBNOTES.TryUpdate()
                  DO Update_Record
      
                  glo:errortext = ''
                  Complete# = 0
                  IF (job:date_completed = '') THEN
                      !Check to see that all the required fields are filled in
                      CompulsoryFieldCheck('C')
                      IF (glo:ErrorText <> '')
                          glo:errortext = 'You cannot complete this job due to the following error(s): <13,10>' & Clip(glo:errortext)
                          Error_Text
                          glo:errortext = ''
                      ELSE
                          IF (GETINI('VALIDATE','ForceAccCheckComp',,CLIP(PATH())&'\SB2KDEF.INI') = 1) THEN
                              IF (LocalValidateAccessories() = Level:Benign) THEN
                                  Complete# = 1
                              !ELSE
                                  !Access:JOBS.Update()
                              END
                          ELSE
                              Complete# = 1
                          END
                      END
                  ELSE
                      CASE MessageEx('Are you sure you want to change the Completion Date?','ServiceBase 2000',|
                                     'Styles\question.ico','&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                     beep:systemquestion,msgex:samewidths,84,26,0)
                          OF 1 ! &Yes Button
                              Complete# = 1
                          OF 2 ! &No Button
                      END
                  END
      
                  IF (Complete# = 1) THEN
                      !**** TH 02/04 Time to deal with deferred IMEI changes
                      !if sts:PickNoteEnable then
                      ! Start Change xxxx BE(18/03/04)
                      IF (jobe:Pre_RF_Board_IMEI = '') THEN
                      ! End Change xxxx BE(18/03/04)
                          partpntstore = access:parts.savefile() !backup current pointer
                          warpartpntstore = access:warparts.savefile() !backup current pointer
      
                          access:PARTS.clearkey(par:Part_Number_Key) !check for existing picking record
                          par:Ref_Number = job:ref_number
                          set(par:Part_Number_Key,par:Part_Number_Key)
                          loop
                              if access:PARTS.next() then break. !no records
                              ! Start Change xxxx BE(18/03/04)
                              IF (par:Ref_Number <> job:ref_number) THEN
                                  BREAK
                              END
                              ! End Change xxxx BE(18/03/04)
                              !if rf_board then new imei
                              UpdateRFBoardIMEI(par:part_ref_number,par:part_number)
                          end
      
                          access:WARPARTS.clearkey(wpr:Part_Number_Key) !check for existing picking record
                          wpr:Ref_Number = job:ref_number
                          set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                          loop
                              if access:WARPARTS.next() then break. !no records
                              ! Start Change xxxx BE(18/03/04)
                              IF (wpr:Ref_Number <> job:ref_number) THEN
                                  BREAK
                              END
                              ! End Change xxxx BE(18/03/04)
                              !if rf_board then new imei
                              UpdateRFBoardIMEI(wpr:part_ref_number,wpr:part_number)
                          end
      
                          access:parts.restorefile(partpntstore)
                          access:warparts.restorefile(warpartpntstore)
                      ! Start Change xxxx BE(18/03/04)
                      END
                      ! End Change xxxx BE(18/03/04)
                      !end !if sts:PickNoteEnable
                      !**** TH 02/04
                      Print_Despatch_Note_Temp# = 0
                      !Fill in the totals with the right costs from the
                      !Price Structure.
                      Do Pricing
                      Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                      man:Manufacturer = job:Manufacturer
                      IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign) THEN
                          !Found
                          If man:QAAtCompletion
                              !If QA at completion is ticked, then don't actually
                              !complete the job. That is done at QA
                              GetStatus(605,1,'JOB')
                              MessageEx('The Repair has been completed. You must now QA this unit.','ServiceBase 2000',|
                                             'Styles\idea.ico','&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                             beep:systemasterisk,msgex:samewidths,84,26,0)
                              job:On_Test = 'YES'
                              job:Date_On_Test = Today()
                              job:Time_On_Test = Clock()
      
                              ! Start Change 3879 BE(26/02/2004)
                              Access:JOBSE.ClearKey(jobe:RefNumberKey)
                              jobe:RefNumber = job:Ref_number
                              IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
                                  ! Start Change 4053 BE(19/03/04)
                                  !jobe:CompleteRepairType = 1
                                  !jobe:CompleteRepairDate = TODAY()
                                  !jobe:CompleteRepairTime = CLOCK()
                                  !access:jobse.update()
                                  ! Start Change 4200 BE(04/05/04)
                                  !IF (jobe:CompleteRepairDate = '') THEN
                                  !    jobe:CompleteRepairType = 1
                                  !    jobe:CompleteRepairDate = TODAY()
                                  !    jobe:CompleteRepairTime = CLOCK()
                                  !    access:jobse.update()
                                  !END
                                  access:JOBSENG.clearkey(joe:JobNumberKey)
                                  joe:JobNumber = job:Ref_Number
                                  SET(joe:JobNumberKey, joe:JobNumberKey)
                                  IF (access:JOBSENG.next() = Level:Benign) THEN
                                      IF ((jobe:CompleteRepairDate = '') OR |
                                          (joe:DateAllocated > jobe:CompleteRepairDate) OR |
                                          ((joe:DateAllocated = jobe:CompleteRepairDate) AND |
                                           (joe:TimeAllocated > jobe:CompleteRepairTime))) THEN
                                          jobe:CompleteRepairType = 1
                                          jobe:CompleteRepairDate = TODAY()
                                          jobe:CompleteRepairTime = CLOCK()
                                          access:jobse.update()
                                      END
                                  END
                                  ! End Change 4200 BE(04/05/04)
                                  ! End Change 4053 BE(19/03/04)
                              END
                              ! End Change 3879 BE(26/02/2004)
      
                              DO Update_Record
                              DO Activate_QA_Panel
                          ELSE
                              ! Start Change 3879 BE(26/02/2004)
                              Access:JOBSE.ClearKey(jobe:RefNumberKey)
                              jobe:RefNumber = job:Ref_number
                              IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
                                  IF (jobe:CompleteRepairDate = '') THEN
                                      jobe:CompleteRepairType = 2
                                      jobe:CompleteRepairDate = TODAY()
                                      jobe:CompleteRepairTime = CLOCK()
                                      access:jobse.update()
                                  END
                              END
                              ! End Change 3879 BE(26/02/2004)
      
                              DO CompleteJob
                              DO Activate_No_Panel
                          END
                      !ELSE
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      END
                  END
              OF 2 ! &No Button
          END
      END
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CompleteJobButton, Accepted)
    OF ?PickButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickButton, Accepted)
      PickValidate
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PickButton, Accepted)
    OF ?ViewJobButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewJobButton, Accepted)
      DO Update_Record
      SaveRequest# = GlobalRequest
      GlobalRequest = ChangeRecord
      UpdateJOBS
      GlobalRequest = SaveRequest#
      ! Start Change 3689 BE(12/12/03)
      IF (job:completed = 'YES') THEN
          Enable(?CompletedLabelButton)
          Enable(?Prompt30)
          Enable(?Prompt31)
      END
      ! End Change 3689 BE(12/12/03)
      DO Show_Job_fault_Codes
      POST(EVENT:Accepted, ?job:chargeable_job)
      POST(EVENT:Accepted, ?job:warranty_job)
      BRW4.ResetSort(1)
      BRW19.ResetSort(1)
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewJobButton, Accepted)
    OF ?RemovePartsButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RemovePartsButton, Accepted)
      IF (SecurityCheck('REMOVE PARTS AND INVOICE TEXT') <> Level:Benign) THEN
          MessageEx('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                             beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
          Do Update_Record
          RemovePartsAndInvoiceText()
          BRW4.ResetSort(1)
          BRW19.ResetSort(1)
          DISPLAY()
          MessageEx('Parts and text removed.','ServiceBase 2000',|
                         'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                         beep:systemexclamation,msgex:samewidths,84,26,0)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RemovePartsButton, Accepted)
    OF ?Temp_IMEI_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Temp_IMEI_Number, Accepted)
      IF (temp_IMEI_Number = '') THEN
          MessageEx('Please Choose a Serial Number.','ServiceBase 2000',|
                         'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
          SELECT(?temp_IMEI_Number)
      ELSIF (INCOMPLETE()) THEN
          temp_IMEI_Number = job:ESN
          DISPLAY(?temp_IMEI_Number)
          SELECT(INCOMPLETE())
      ELSE
          DO  Update_Record
      
          savejobs = access:jobs.savefile()
          savejobse = access:jobse.savefile()
          savejobnotes = access:jobnotes.savefile()
          Access:JOBS.ClearKey(job:ESN_Key)
          job:ESN = temp_IMEI_Number
          SET(job:ESN_Key, job:ESN_Key)
          count# = 0
          LOOP
              IF ((access:jobs.next() <> Level:Benign) OR |
                   (job:ESN <> temp_IMEI_Number)) THEN
                  BREAK
              END
              count# += 1
              IF (count# > 1) THEN
                  IF (PickJob_IMEI(temp_IMEI_Number) = 0) THEN
                      count# = -1
                  END
                  BREAK
              END
          END
      
          IF (count# > 0) THEN
              IF (count# = 1) THEN
                  Access:JOBS.ClearKey(job:ESN_Key)
                  job:ESN = temp_IMEI_Number
                  access:jobs.fetch(job:ESN_Key)
              END
      
              RecordState = rs:UpdateRecord
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey) <> Level:Benign) THEN
                  IF (Access:JOBSE.Primerecord() = Level:Benign) THEN
                      jobe:RefNumber  = job:Ref_Number
                      IF (Access:JOBSE.Tryinsert() <> Level:Benign) THEN
                          Access:JOBSE.Cancelautoinc()
                      END
                  END
              END
              access:jobnotes.clearkey(JBN:RefNumberKey)
              jbn:refnumber   = job:ref_number
              IF (access:jobnotes.tryfetch(JBN:RefNumberKey) <> Level:benign) THEN
                  IF (access:jobnotes.primerecord() = Level:Benign) THEN
                      jbn:refnumber   = job:ref_number
                      IF (access:jobnotes.tryinsert() <> Level:Benign) THEN
                          access:jobnotes.cancelautoinc()
                      END
                  END
              END
      
              temp:Account_Number = job:Account_Number
              temp:Order_Number = job:Order_Number
              temp:job_reference = job:ref_number
              !temp:charge_type = job:charge_type
              !temp:repair_type = job:repair_type
              !temp:warranty_charge_type = job:warranty_charge_type
              !temp:repair_type_warranty = job:repair_type_warranty
              DO Show_Job_Fault_Codes
              DO Set_Checkboxes
              IF (job:current_status[1 : 3] = '605') THEN
                  DO Activate_QA_Panel
              ELSE
                  DO Activate_TestAndRepair_Panel
              END
              SELECT(?WarrantyPartsTab)
              SELECT(?ChargeablePartsTab)
              SELECT(?TFCTab)
              SELECT(?MFCTab)
              !IF (job:date_completed <> '') THEN
              IF (job:current_status[1 : 3] = '605') THEN
                  Select(?QAGroup)
              ELSE
                  Select(?TestAndRepairSheet)
              END
              ! Start Change 3288 BE(23/09/03)
              IF ((job:current_status[1 : 3] = '615') OR (job:current_status[1 : 3] = '625')) THEN
                  technician_string = ''
                  Access:USERS.Clearkey(use:User_Code_Key)
                  use:User_Code   = job:Engineer
                  IF (Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign) THEN
                      technician_string = CLIP(use:forename)
                      IF (CLIP(technician_string) = '') THEN
                          technician_string = CLIP(use:surname)
                      ELSE
                          technician_string = CLIP(technician_string) & ' ' & CLIP(use:surname)
                      END
                      IF (CLIP(technician_string) = '') THEN
                          technician_string = job:engineer
                      END
                  ELSE
                      technician_string = job:engineer
                  END
                  MESSAGE('QA Failed: Technician ' & CLIP(technician_string))
              END
              ! End Change 3288 BE(23/09/03)
          ELSIF (count# = 0) THEN
              temp_IMEI_Number = ''
              DISPLAY(?temp_IMEI_Number)
              SELECT(?temp_IMEI_Number)
              access:jobs.restorefile(savejobs)
              access:jobse.restorefile(savejobse)
              access:jobnotes.restorefile(savejobnotes)
              MessageEx('Record Not Found.','ServiceBase 2000',|
                         'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                         beep:systemhand,msgex:samewidths,84,26,0)
          ELSE
              temp_IMEI_Number = ''
              DISPLAY(?temp_IMEI_Number)
              SELECT(?temp_IMEI_Number)
              access:jobs.restorefile(savejobs)
              access:jobse.restorefile(savejobse)
              access:jobnotes.restorefile(savejobnotes)
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Temp_IMEI_Number, Accepted)
    OF ?AllocateJobButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllocateJobButton, Accepted)
      DO Update_Record
      AllocateJob(job:ref_number)
      DISPLAY()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AllocateJobButton, Accepted)
    OF ?CompletedLabelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CompletedLabelButton, Accepted)
      Complete_JobLabel(job:ref_number)
      MESSAGE('Completed Label Printed OK')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CompletedLabelButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      DO Cancel_Record
      DO Activate_No_Panel
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    OF ?BookinLabelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BookinLabelButton, Accepted)
      Bookin_JobLabel(job:ref_number)
      MESSAGE('Bookin Label Printed OK')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BookinLabelButton, Accepted)
    OF ?OKButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OKButton, Accepted)
      DO Update_Record
      !DO Activate_No_Panel
      POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OKButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Voda_ONR_Process')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?temp:Charge_Part_Number
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?temp:Charge_Part_Number)
        CASE EVENT()
        OF EVENT:AlertKey
             CASE KEYCODE()
             OF EnterKey  OROF TabKey
                LOOP UNTIL KEYBOARD()=0
                   ASK
                END
                UPDATE(?temp:Charge_Part_Number)
                POST(EVENT:Accepted,?temp:Charge_Part_Number)
                CYCLE
             END
        END
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?temp:Charge_Part_Number)
  OF ?temp:Warranty_Part_Number
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?temp:Warranty_Part_Number)
        CASE EVENT()
        OF EVENT:AlertKey
             CASE KEYCODE()
             OF EnterKey  OROF TabKey
                LOOP UNTIL KEYBOARD()=0
                   ASK
                END
                UPDATE(?temp:Warranty_Part_Number)
                POST(EVENT:Accepted,?temp:Warranty_Part_Number)
                CYCLE
             END
        END
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?temp:Warranty_Part_Number)
  OF ?temp:Order_Number
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?temp:Order_Number)
        CASE EVENT()
        OF EVENT:AlertKey
             CASE KEYCODE()
             OF EnterKey  OROF TabKey
                LOOP UNTIL KEYBOARD()=0
                   ASK
                END
                UPDATE(?temp:Order_Number)
                POST(EVENT:Accepted,?temp:Order_Number)
                CYCLE
             END
        END
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?temp:Order_Number)
  OF ?temp:job_reference
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?temp:job_reference)
        CASE EVENT()
        OF EVENT:AlertKey
             CASE KEYCODE()
             OF EnterKey  OROF TabKey
                LOOP UNTIL KEYBOARD()=0
                   ASK
                END
                UPDATE(?temp:Job_Reference)
                POST(EVENT:Accepted,?temp:Job_Reference)
                CYCLE
             END
        END
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?temp:job_reference)
  OF ?Temp_IMEI_Number
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Temp_IMEI_Number)
        CASE EVENT()
        OF EVENT:AlertKey
             CASE KEYCODE()
             OF EnterKey  OROF TabKey
                LOOP UNTIL KEYBOARD()=0
                   ASK
                END
                UPDATE(?temp_IMEI_Number)
                POST(EVENT:Accepted,?temp_IMEI_Number)
                CYCLE
             END
        END
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Temp_IMEI_Number)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
UpdateRFBoardIMEI PROCEDURE (LONG partrefnum, STRING partnum)
    code
    ! Start Change 2116 BE(26/06/03)
    access:stock.clearkey(sto:ref_number_key)
    sto:ref_number = partrefnum
    IF (access:stock.fetch(sto:ref_number_key) = Level:benign) THEN
        IF (sto:RF_BOARD) THEN
            glo:select2 = ''
            glo:select3 = job:model_number
            glo:select4 = partnum
            SETCURSOR()
            EnterNewIMEI
            SETCURSOR(cursor:wait)
            tmp_RF_Board_IMEI = CLIP(glo:select2)
            glo:select2 = ''
            glo:select3 = ''
            glo:select4 = ''
            IF (tmp_RF_Board_IMEI = '') THEN
                RETURN
            END
            GET(audit,0)
            IF (access:audit.primerecord() = level:benign) THEN
                aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                     '<13,10>Old IMEI ' & Clip(job:esn)
                aud:ref_number    = job:ref_number
                aud:date          = Today()
                aud:time          = Clock()
                access:users.clearkey(use:password_key)
                use:password =glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                IF (access:audit.insert() <> Level:benign) THEN
                    access:audit.cancelautoinc()
                END
            END
            access:JOBSE.clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                IF (access:JOBSE.primerecord() = Level:Benign) THEN
                    jobe:RefNumber  = job:Ref_Number
                    IF (access:JOBSE.tryinsert()) THEN
                        access:JOBSE.cancelautoinc()
                    END
                END
            END
            IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                jobe:PRE_RF_BOARD_IMEI = job:ESN
                access:jobse.update()
            END
            job:ESN = tmp_RF_Board_IMEI
            access:jobs.update()
        END
    END
    ! End Change 2116 BE(26/06/03)
ChangeChargeableJob     PROCEDURE()
    CODE

    IF (job:chargeable_job = 'NO') THEN
        found# = 0
        IF (job:estimate = 'YES') THEN
            !found# = 2
            found# = 0
        ELSE
            SaveBookmark = access:parts.savefile()
            access:parts.clearkey(par:part_number_key)
            par:ref_number  = job:ref_number
            SET(par:part_number_key,par:part_number_key)
            LOOP
                IF ((access:parts.next() <> Level:Benign) OR (par:ref_number  <> job:ref_number)) THEN
                    BREAK
                END
                found# = 1
                BREAK
            END
            access:parts.restorefile(SaveBookmark)
        END

        CASE found#
            OF 1    !Parts
                MessageEx('Chargeable parts have been added to this job.<13,10><13,10>You must remove these before you can mark this job as Warranty Only.', |
                          'ServiceBase 2000',|
                          'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0, |
                          beep:systemhand,msgex:samewidths,84,26,0)
                job:chargeable_job = 'YES'

            OF 2    !Estimate
                MessageEx('This job has been marked as an Estimate.<13,10><13,10>You cannot make this a Warranty Only job.',|
                          'ServiceBase 2000',|
                          'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0, |
                          beep:systemhand,msgex:samewidths,84,26,0)
                job:chargeable_job  = 'YES'
        END
    END

    RETURN
ChangeWarrantyJob     PROCEDURE()
    CODE

    IF (job:warranty_job = 'NO') THEN
        found# = 0
        SaveBookmark = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        SET(wpr:part_number_key,wpr:part_number_key)
        LOOP
            IF ((access:warparts.next() <> Level:Benign) OR (wpr:ref_number  <> job:ref_number)) THEN
               break
            END
            found# = 1
            BREAK
        END
        access:warparts.restorefile(SaveBookmark)

        IF (found# <> 0) THEN
            MessageEx('Warranty parts have been added to this job.<13,10><13,10>You must remove these before you can mark this job as Chargeable Only.', |
                           'ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0, |
                           beep:systemhand,msgex:samewidths,84,26,0)
            job:warranty_job  = 'YES'
        END
    END

    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = job:Model_Number
    IF (Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign) THEN
        job:EDI = PendingJob(mod:Manufacturer)
        job:Manufacturer    = mod:Manufacturer
    END

    RETURN
Show_Edit   PROCEDURE(EditNo, EditPrompt, EditReq, EditReqBook, EditValue, EditWidth, ShowCalendar, ShowLookup, forcelookup)
    CODE
    CASE EditNo
        OF 1
            !?jfc_value_1{prop:req} = EditReqBook
            jfc_prompt[1] = CLIP(EditPrompt)
            ?jfc_value_1{prop:text} = Clip(EditValue)
            ?jfc_value_1{prop:width} = EditWidth
            UNHIDE(?jfc_value_1)
            UNHIDE(?jfc_prompt_1)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC1_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_1{PROP:READONLY} = 1
                    jfc_force_lookup[1] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_1{PROP:COLOR} = 0BDFFFFH
            END
        OF 2
            !?jfc_value_2{prop:req} = EditReqBook
            jfc_prompt[2] = CLIP(EditPrompt)
            ?jfc_value_2{prop:text} = Clip(EditValue)
            ?jfc_value_2{prop:width} = EditWidth
            UNHIDE(?jfc_value_2)
            UNHIDE(?jfc_prompt_2)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:2)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC2_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_2{PROP:READONLY} = 1
                    jfc_force_lookup[2] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_2{PROP:COLOR} = 0BDFFFFH
            END
        OF 3
            !?jfc_value_3{prop:req} = EditReqBook
            jfc_prompt[3] = CLIP(EditPrompt)
            ?jfc_value_3{prop:text} = Clip(EditValue)
            ?jfc_value_3{prop:width} = EditWidth
            UNHIDE(?jfc_value_3)
            UNHIDE(?jfc_prompt_3)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:3)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC3_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_3{PROP:READONLY} = 1
                    jfc_force_lookup[3] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_3{PROP:COLOR} = 0BDFFFFH
            END
        OF 4
            !?jfc_value_4{prop:req} = EditReqBook
            jfc_prompt[4] = CLIP(EditPrompt)
            ?jfc_value_4{prop:text} = Clip(EditValue)
            ?jfc_value_4{prop:width} = EditWidth
            UNHIDE(?jfc_value_4)
            UNHIDE(?jfc_prompt_4)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:4)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC4_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_4{PROP:READONLY} = 1
                    jfc_force_lookup[4] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_4{PROP:COLOR} = 0BDFFFFH
            END
        OF 5
            !?jfc_value_5{prop:req} = EditReqBook
            jfc_prompt[5] = CLIP(EditPrompt)
            ?jfc_value_5{prop:text} = Clip(EditValue)
            ?jfc_value_5{prop:width} = EditWidth
            UNHIDE(?jfc_value_5)
            UNHIDE(?jfc_prompt_5)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:5)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC5_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_5{PROP:READONLY} = 1
                    jfc_force_lookup[5] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_5{PROP:COLOR} = 0BDFFFFH
            END
        OF 6
            !?jfc_value_6{prop:req} = EditReqBook
            jfc_prompt[6] = CLIP(EditPrompt)
            ?jfc_value_6{prop:text} = Clip(EditValue)
            ?jfc_value_6{prop:width} = EditWidth
            UNHIDE(?jfc_value_6)
            UNHIDE(?jfc_prompt_6)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:6)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC6_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_6{PROP:READONLY} = 1
                    jfc_force_lookup[6] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_6{PROP:COLOR} = 0BDFFFFH
            END
        OF 7
            !?jfc_value_7{prop:req} = EditReqBook
            jfc_prompt[7] = CLIP(EditPrompt)
            ?jfc_value_7{prop:text} = Clip(EditValue)
            ?jfc_value_7{prop:width} = EditWidth
            UNHIDE(?jfc_value_7)
            UNHIDE(?jfc_prompt_7)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:7)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC7_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_7{PROP:READONLY} = 1
                    jfc_force_lookup[7] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_7{PROP:COLOR} = 0BDFFFFH
            END
        OF 8
            !?jfc_value_8{prop:req} = EditReqBook
            jfc_prompt[8] = CLIP(EditPrompt)
            ?jfc_value_8{prop:text} = Clip(EditValue)
            ?jfc_value_8{prop:width} = EditWidth
            UNHIDE(?jfc_value_8)
            UNHIDE(?jfc_prompt_8)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:8)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC8_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_8{PROP:READONLY} = 1
                    jfc_force_lookup[8] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_8{PROP:COLOR} = 0BDFFFFH
            END
        OF 9
            !?jfc_value_9{prop:req} = EditReqBook
            jfc_prompt[9] = CLIP(EditPrompt)
            ?jfc_value_9{prop:text} = Clip(EditValue)
            ?jfc_value_9{prop:width} = EditWidth
            UNHIDE(?jfc_value_9)
            UNHIDE(?jfc_prompt_9)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:9)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC9_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_9{PROP:READONLY} = 1
                    jfc_force_lookup[9] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_9{PROP:COLOR} = 0BDFFFFH
            END
        OF 10
            !?jfc_value_10{prop:req} = EditReqBook
            jfc_prompt[10] = CLIP(EditPrompt)
            ?jfc_value_10{prop:text} = Clip(EditValue)
            ?jfc_value_10{prop:width} = EditWidth
            UNHIDE(?jfc_value_10)
            UNHIDE(?jfc_prompt_10)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:10)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC10_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_10{PROP:READONLY} = 1
                    jfc_force_lookup[10] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_10{PROP:COLOR} = 0BDFFFFH
            END
        OF 11
            !?jfc_value_11{prop:req} = EditReqBook
            jfc_prompt[11] = CLIP(EditPrompt)
            ?jfc_value_11{prop:text} = Clip(EditValue)
            ?jfc_value_11{prop:width} = EditWidth
            UNHIDE(?jfc_value_11)
            UNHIDE(?jfc_prompt_11)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:11)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC11_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_11{PROP:READONLY} = 1
                    jfc_force_lookup[11] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_11{PROP:COLOR} = 0BDFFFFH
            END
        OF 12
            !?jfc_value_12{prop:req} = EditReqBook
            jfc_prompt[12] = CLIP(EditPrompt)
            ?jfc_value_12{prop:text} = Clip(EditValue)
            ?jfc_value_12{prop:width} = EditWidth
            UNHIDE(?jfc_value_12)
            UNHIDE(?jfc_prompt_12)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:12)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?JFC12_Button)
                IF (forcelookup = 'YES') THEN
                    !?jfc_value_12{PROP:READONLY} = 1
                    jfc_force_lookup[12] = 1
                END
            END
            IF (EditReq) THEN
                ?jfc_value_12{PROP:COLOR} = 0BDFFFFH
            END
    END
    RETURN
Show_Edit2   PROCEDURE(EditNo, EditPrompt, EditReq, EditReqBook, EditValue, EditWidth, ShowCalendar, ShowLookup, forcelookup)
    CODE
    CASE EditNo
        OF 1
            !?tfc_value_1{prop:req} = EditReqBook
            tfc_prompt[1] = CLIP(EditPrompt)
            ?tfc_value_1{prop:text} = Clip(EditValue)
            ?tfc_value_1{prop:width} = EditWidth
            UNHIDE(?tfc_value_1)
            UNHIDE(?tfc_prompt_1)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:13)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc1_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_1{PROP:READONLY} = 1
                    tfc_force_lookup[1] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_1{PROP:COLOR} = 0BDFFFFH
            END
        OF 2
            !?tfc_value_2{prop:req} = EditReqBook
            tfc_prompt[2] = CLIP(EditPrompt)
            ?tfc_value_2{prop:text} = Clip(EditValue)
            ?tfc_value_2{prop:width} = EditWidth
            UNHIDE(?tfc_value_2)
            UNHIDE(?tfc_prompt_2)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:14)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc2_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_2{PROP:READONLY} = 1
                    tfc_force_lookup[2] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_2{PROP:COLOR} = 0BDFFFFH
            END
        OF 3
            !?tfc_value_3{prop:req} = EditReqBook
            tfc_prompt[3] = CLIP(EditPrompt)
            ?tfc_value_3{prop:text} = Clip(EditValue)
            ?tfc_value_3{prop:width} = EditWidth
            UNHIDE(?tfc_value_3)
            UNHIDE(?tfc_prompt_3)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:15)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc3_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_3{PROP:READONLY} = 1
                    tfc_force_lookup[3] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_3{PROP:COLOR} = 0BDFFFFH
            END
        OF 4
            !?tfc_value_4{prop:req} = EditReqBook
            tfc_prompt[4] = CLIP(EditPrompt)
            ?tfc_value_4{prop:text} = Clip(EditValue)
            ?tfc_value_4{prop:width} = EditWidth
            UNHIDE(?tfc_value_4)
            UNHIDE(?tfc_prompt_4)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:16)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc4_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_4{PROP:READONLY} = 1
                    tfc_force_lookup[4] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_4{PROP:COLOR} = 0BDFFFFH
            END
        OF 5
            !?tfc_value_5{prop:req} = EditReqBook
            tfc_prompt[5] = CLIP(EditPrompt)
            ?tfc_value_5{prop:text} = Clip(EditValue)
            ?tfc_value_5{prop:width} = EditWidth
            UNHIDE(?tfc_value_5)
            UNHIDE(?tfc_prompt_5)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:17)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc5_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_5{PROP:READONLY} = 1
                    tfc_force_lookup[5] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_5{PROP:COLOR} = 0BDFFFFH
            END
        OF 6
            !?tfc_value_6{prop:req} = EditReqBook
            tfc_prompt[6] = CLIP(EditPrompt)
            ?tfc_value_6{prop:text} = Clip(EditValue)
            ?tfc_value_6{prop:width} = EditWidth
            UNHIDE(?tfc_value_6)
            UNHIDE(?tfc_prompt_6)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:18)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc6_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_6{PROP:READONLY} = 1
                    tfc_force_lookup[6] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_6{PROP:COLOR} = 0BDFFFFH
            END
        OF 7
            !?tfc_value_7{prop:req} = EditReqBook
            tfc_prompt[7] = CLIP(EditPrompt)
            ?tfc_value_7{prop:text} = Clip(EditValue)
            ?tfc_value_7{prop:width} = EditWidth
            UNHIDE(?tfc_value_7)
            UNHIDE(?tfc_prompt_7)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:19)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc7_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_7{PROP:READONLY} = 1
                    tfc_force_lookup[7] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_7{PROP:COLOR} = 0BDFFFFH
            END
        OF 8
            !?tfc_value_8{prop:req} = EditReqBook
            tfc_prompt[8] = CLIP(EditPrompt)
            ?tfc_value_8{prop:text} = Clip(EditValue)
            ?tfc_value_8{prop:width} = EditWidth
            UNHIDE(?tfc_value_8)
            UNHIDE(?tfc_prompt_8)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:20)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc8_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_8{PROP:READONLY} = 1
                    tfc_force_lookup[8] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_8{PROP:COLOR} = 0BDFFFFH
            END
        OF 9
            !?tfc_value_9{prop:req} = EditReqBook
            tfc_prompt[9] = CLIP(EditPrompt)
            ?tfc_value_9{prop:text} = Clip(EditValue)
            ?tfc_value_9{prop:width} = EditWidth
            UNHIDE(?tfc_value_9)
            UNHIDE(?tfc_prompt_9)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:21)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc9_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_9{PROP:READONLY} = 1
                    tfc_force_lookup[9] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_9{PROP:COLOR} = 0BDFFFFH
            END
        OF 10
            !?tfc_value_10{prop:req} = EditReqBook
            tfc_prompt[10] = CLIP(EditPrompt)
            ?tfc_value_10{prop:text} = Clip(EditValue)
            ?tfc_value_10{prop:width} = EditWidth
            UNHIDE(?tfc_value_10)
            UNHIDE(?tfc_prompt_10)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:22)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc10_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_10{PROP:READONLY} = 1
                    tfc_force_lookup[10] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_10{PROP:COLOR} = 0BDFFFFH
            END
        OF 11
            !?tfc_value_11{prop:req} = EditReqBook
            tfc_prompt[11] = CLIP(EditPrompt)
            ?tfc_value_11{prop:text} = Clip(EditValue)
            ?tfc_value_11{prop:width} = EditWidth
            UNHIDE(?tfc_value_11)
            UNHIDE(?tfc_prompt_11)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:23)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc11_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_11{PROP:READONLY} = 1
                    tfc_force_lookup[11] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_11{PROP:COLOR} = 0BDFFFFH
            END
        OF 12
            !?tfc_value_12{prop:req} = EditReqBook
            tfc_prompt[12] = CLIP(EditPrompt)
            ?tfc_value_12{prop:text} = Clip(EditValue)
            ?tfc_value_12{prop:width} = EditWidth
            UNHIDE(?tfc_value_12)
            UNHIDE(?tfc_prompt_12)
            IF (ShowCalendar) THEN
                UNHIDE(?PopCalendar:24)
            END
            IF (ShowLookup = 'YES') THEN
                UNHIDE(?tfc12_Button)
                IF (forcelookup = 'YES') THEN
                    !?tfc_value_12{PROP:READONLY} = 1
                    tfc_force_lookup[12] = 1
                END
            END
            IF (EditReq) THEN
                ?tfc_value_12{PROP:COLOR} = 0BDFFFFH
            END
    END
    RETURN
Lookup_Fault_Code   PROCEDURE(EditNo)
    CODE
    glo:select1  = job:manufacturer
    glo:select2  = EditNo
    glo:select3  = ''
    SaveRequest# = GlobalRequest
    GlobalRequest = SelectRecord
    Browse_Manufacturer_Fault_Lookup
    GlobalRequest = SaveRequest#
    IF (GlobalResponse = RequestCompleted) THEN
        jfc_value[EditNo] = mfo:field
        CASE EditNo
        OF 1
            POST(Event:Accepted, ?jfc_value_1)
        OF 2
            POST(Event:Accepted, ?jfc_value_2)
        OF 3
            POST(Event:Accepted, ?jfc_value_3)
        OF 4
            POST(Event:Accepted, ?jfc_value_4)
        OF 5
            POST(Event:Accepted, ?jfc_value_5)
        OF 6
            POST(Event:Accepted, ?jfc_value_6)
        OF 7
            POST(Event:Accepted, ?jfc_value_7)
        OF 8
            POST(Event:Accepted, ?jfc_value_8)
        OF 9
            POST(Event:Accepted, ?jfc_value_9)
        OF 10
            POST(Event:Accepted, ?jfc_value_10)
        OF 11
            POST(Event:Accepted, ?jfc_value_11)
        OF 12
            POST(Event:Accepted, ?jfc_value_12)
        END
    END
    DISPLAY()
    glo:select1  = ''
    glo:select2  = ''
    glo:select3  = ''
    RETURN
Lookup_Trade_Fault_Code   PROCEDURE(EditNo)
    CODE

    access:subtracc.clearkey(sub:account_number_key)
    sub:account_number = job:account_number
    access:subtracc.fetch(sub:account_number_key)
    access:tradeacc.clearkey(tra:account_number_key)
    tra:account_number = sub:main_account_number
    access:tradeacc.fetch(tra:account_number_key)

    glo:Select1 = tra:Account_Number
    glo:Select2 = EditNo
    SaveRequest# = GlobalRequest
    GlobalRequest = SelectRecord
    PickTradeFaultCodes
    GlobalRequest = SaveRequest#

    IF (GlobalResponse = RequestCompleted) THEN
        tfc_value[EditNo] = tfo:Field
        CASE EditNo
        OF 1
            POST(Event:Accepted, ?tfc_value_1)
        OF 2
            POST(Event:Accepted, ?tfc_value_2)
        OF 3
            POST(Event:Accepted, ?tfc_value_3)
        OF 4
            POST(Event:Accepted, ?tfc_value_4)
        OF 5
            POST(Event:Accepted, ?tfc_value_5)
        OF 6
            POST(Event:Accepted, ?tfc_value_6)
        OF 7
            POST(Event:Accepted, ?tfc_value_7)
        OF 8
            POST(Event:Accepted, ?tfc_value_8)
        OF 9
            POST(Event:Accepted, ?tfc_value_9)
        OF 10
            POST(Event:Accepted, ?tfc_value_10)
        OF 11
            POST(Event:Accepted, ?tfc_value_11)
        OF 12
            POST(Event:Accepted, ?tfc_value_12)
        END
    END
    DISPLAY()
    glo:select1  = ''
    glo:select2  = ''
    RETURN

Edit_Fault_Code PROCEDURE(EditNo)
result  Byte
    CODE
    result = 1
    IF (jfc_value[EditNo] <> '') THEN
        access:manfault.clearkey(maf:field_number_key)
        maf:manufacturer = job:manufacturer
        maf:field_number = EditNo
        IF (access:manfault.tryfetch(maf:field_number_key) = Level:Benign) THEN
            IF (maf:lookup = 'YES') THEN
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:manufacturer = job:manufacturer
                mfo:Field_Number  = EditNo
                mfo:Field         = jfc_value[EditNo]
                IF (Access:MANFAULO.Fetch(mfo:Field_Key) = Level:Benign) THEN
                    !access:jobnotes.clearkey(jbn:RefNumberKey)
                    !jbn:RefNumber = job:ref_number
                    !IF (access:jobnotes.tryfetch(jbn:RefNumberKey) = Level:Benign) THEN
                    IF (Maf:ReplicateFault = 'YES') THEN
                        IF (jbn:fault_description = '') THEN
                            jbn:fault_description = Clip(mfo:description)
                        ELSE
                            jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(mfo:description)
                        END
                    END
                    IF (Maf:ReplicateInvoice = 'YES') THEN
                        IF (jbn:invoice_text = '') THEN
                            jbn:invoice_text = Clip(mfo:description)
                        ELSE
                            jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(mfo:description)
                        END
                    END
                    !access:jobnotes.update()
                    DISPLAY()
                    !END
                ELSE
                    IF (jfc_force_lookup[EditNo] = 1) THEN
                        result = 0
                    END
                END
            END
        END
    END
    RETURN(result)
Edit_Trade_Fault_Code   PROCEDURE(EditNo)
result  Byte
    CODE
    result = 1
    IF (tfc_value[EditNo] <> '') THEN
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        access:subtracc.fetch(sub:account_number_key)
        access:tradeacc.clearkey(tra:account_number_key)
        tra:account_number = sub:main_account_number
        access:tradeacc.fetch(tra:account_number_key)

        Access:TRAFAULT.ClearKey(taf:Field_Number_Key)
        taf:AccountNumber = tra:Account_Number
        taf:Field_Number  = EditNo
        IF (Access:TRAFAULT.TryFetch(taf:Field_Number_Key) = Level:Benign) THEN
            IF (taf:lookup = 'YES') THEN
                Access:TRAFAULO.ClearKey(tfo:Field_Key)
                tfo:AccountNumber = tra:account_number
                tfo:Field_Number  = EditNo
                tfo:Field         = tfc_value[EditNo]
                SET(tfo:Field_Key, tfo:Field_Key)
                IF ((Access:TRAFAULO.next() = Level:Benign) AND |
                    (tfo:AccountNumber = tra:account_number) AND |
                    (tfo:Field_Number  = EditNo) AND |
                    (tfo:Field  = tfc_value[EditNo])) THEN
                    !access:jobnotes.clearkey(jbn:RefNumberKey)
                    !jbn:RefNumber = job:ref_number
                    !IF (access:jobnotes.tryfetch(jbn:RefNumberKey) = Level:Benign) THEN
                    IF (taf:ReplicateFault = 'YES') THEN
                        IF (jbn:fault_description = '') THEN
                            jbn:fault_description = Clip(tfo:description)
                        ELSE
                            jbn:fault_description = Clip(jbn:fault_description) & '<13,10>' & Clip(tfo:description)
                        END
                    END
                    IF (taf:ReplicateInvoice = 'YES') THEN
                        IF (jbn:invoice_text = '') THEN
                            jbn:invoice_text = Clip(tfo:description)
                        ELSE
                            jbn:invoice_text = Clip(jbn:invoice_text) & '<13,10>' & Clip(tfo:description)
                        END
                    END
                    !access:jobnotes.update()
                    DISPLAY()
                    !END
                ELSE
                    IF (tfc_force_lookup[EditNo] = 1) THEN
                        result = 0
                    END
                END
            END
        END
    END
    RETURN(result)
LocalValidateAccessories    PROCEDURE()
local:ErrorCode             Byte
    CODE

    local:ErrorCode  = 0
    IF (AccessoryCheck('JOB')) THEN
        IF (AccessoryMismatch(job:Ref_Number,'JOB')) THEN
            local:Errorcode = 1
        ELSE
            local:Errorcode = 2
        END
    END

    IF (Access:AUDIT.PrimeRecord() = Level:Benign) THEN
        CASE local:Errorcode
            OF 0 OROF 2
                aud:Notes         = 'ACCESSORIES:<13,10>'
            OF 1
                aud:Notes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
        END

        !Add the list of accessories to the Audit Trail
        IF (job:Despatch_Type <> 'LOAN') THEN
            Save_jac_ID = Access:JOBACC.SaveFile()
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = job:Ref_Number
            SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
            LOOP
                IF ((Access:JOBACC.NEXT() <> Level:Benign) OR (jac:Ref_Number <> job:Ref_Number)) THEN
                   BREAK
                END
                aud:Notes = CLIP(aud:Notes) & '<13,10>' & CLIP(jac:Accessory)
            END
            Access:JOBACC.RestoreFile(Save_jac_ID)
        ELSE
            Save_lac_ID = Access:LOANACC.SaveFile()
            Access:LOANACC.ClearKey(lac:Ref_Number_Key)
            lac:Ref_Number = job:Ref_Number
            SET(lac:Ref_Number_Key,lac:Ref_Number_Key)
            LOOP
                IF ((Access:LOANACC.NEXT() <> Level:Benign) OR(lac:Ref_Number <> job:Ref_Number)) THEN
                   BREAK
                END
                aud:Notes = CLIP(aud:Notes) & '<13,10>' & CLIP(lac:Accessory)
            End !Loop
            Access:LOANACC.RestoreFile(Save_lac_ID)
        END

        CASE local:Errorcode
            OF 1
                !If Error show the Accessories that were actually tagged
                aud:Notes         = CLIP(aud:Notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
                CLEAR(glo:Queue)
                LOOP x# = 1 TO RECORDS(glo:Queue)
                    GET(glo:Queue,x#)
                    aud:Notes = CLIP(aud:Notes) & '<13,10>' & CLIP(glo:Pointer)
                END
        END

        aud:Ref_Number    = job:ref_number
        aud:Date          = Today()
        aud:Time          = Clock()
        aud:Type          = 'JOB'
        Access:USERS.ClearKey(use:Password_Key)
        use:Password      = glo:Password
        Access:USERS.Fetch(use:Password_Key)
        aud:User          = use:User_Code
        CASE local:ErrorCode
            OF 0
                aud:Action        = 'COMPLETION VALIDATION: PASSED ACCESSORIES'
            OF 1
                aud:Action        = 'COMPLETION VALIDATION: FAILED ACCESSORIES'
            OF 2
                aud:Action        = 'COMPLETION VALIDATION: PASSED ACCESSORIES (FORCED)'
        END
        
        Access:AUDIT.Insert()

        !Do not complete
        IF (local:ErrorCode = 1) THEN
            RETURN Level:Fatal
        END
    END

    RETURN Level:Benign
LocalValidateParts     PROCEDURE()
    CODE
      !OK, not do Parts/Network check
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = job:Manufacturer
      IF (Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign) THEN
          IF (man:QAParts) THEN
              IF (GetTempPathA(255,TempFilePath)) THEN
                  IF (Sub(TempFilePath,-1,1) = '\') THEN
                      glo:FileName = CLIP(TempFilePath) & ''
                  ELSE
                      glo:FileName = Clip(TempFilePath) & '\'
                  END
              END

              glo:FileName = CLIP(glo:FileName) & 'QAPARTS' & Clock() & '.TMP'

              REMOVE(glo:FileName)
              Access:QAPARTSTEMP.Open()
              Access:QAPARTSTEMP.UseFile()
              IF (job:Chargeable_Job = 'YES') THEN
                  Save_par_ID = Access:PARTS.SaveFile()
                  Access:PARTS.ClearKey(par:Part_Number_Key)
                  par:Ref_Number  = job:Ref_Number
                  SET(par:Part_Number_Key,par:Part_Number_Key)
                  LOOP
                      IF ((Access:PARTS.NEXT() <> Level:benign) OR (par:Ref_Number  <> job:Ref_Number)) THEN
                         BREAK
                      END
                      IF (par:Part_Number = 'ADJUSTMENT' or par:Adjustment = 'YES') THEN
                        CYCLE
                      END

                      IF (Access:QAPARTSTEMP.PrimeRecord() = Level:Benign) THEN
                          qap:PartNumber       = par:Part_Number
                          qap:Description      = par:Description
                          qap:PartRecordNumber = par:Record_Number
                          qap:PartType         = 'CHA'
                          Access:QAPARTSTEMP.TryInsert()
                      END
                  END
                  Access:PARTS.RestoreFile(Save_par_ID)
              END

              IF (job:Warranty_Job = 'YES') THEN
                  Save_wpr_ID = Access:WARPARTS.SaveFile()
                  Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                  wpr:Ref_Number  = job:Ref_Number
                  SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
                  LOOP
                      IF ((Access:WARPARTS.NEXT() <> Level:Benign) OR (wpr:Ref_Number  <> job:Ref_Number)) THEN
                         BREAK
                      END
                      IF (wpr:Part_Number = 'ADJUSTMENT' or wpr:Adjustment = 'YES') THEN
                        CYCLE
                      END

                      IF (Access:QAPARTSTEMP.PrimeRecord() = Level:Benign) THEN
                          qap:PartNumber       = wpr:Part_Number
                          qap:Description      = wpr:Description
                          qap:PartRecordNumber = wpr:Record_Number
                          qap:PartType         = 'WAR'
                          Access:QAPARTSTEMP.TryInsert()
                      END
                  END
                  Access:WARPARTS.RestoreFile(Save_wpr_ID)
              END

              CASE ValidateParts()
                 OF 0
                     Access:QAPARTSTEMP.Close()
                     REMOVE(glo:FileName)
                     RETURN 0
                 OF 1 !Passed
                     Access:QAPARTSTEMP.Close()
                     REMOVE(glo:FileName)
                     RETURN 1
                 OF 2 !Failed
                     !DO FailParts
                     Access:QAPARTSTEMP.Close()
                     REMOVE(glo:FileName)
                     RETURN 0
              END
          ELSE
                RETURN 1
          END
      ELSE
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          RETURN 1
      END

LocalValidateNetwork     PROCEDURE()
    CODE
      !OK, not do Parts/Network check
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = job:Manufacturer
    IF (Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign) THEN
        IF (man:QANetwork) THEN
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign) THEN
                SaveRequest#      = GlobalRequest
                GlobalResponse    = RequestCancelled
                GlobalRequest     = SelectRecord
                PickNetworks
                GlobalRequest     = SaveRequest#
                IF (Globalresponse = RequestCompleted) THEN
                    IF (jobe:Network = net:Network) THEN
                        RETURN 1
                    ELSE
                        Beep(Beep:SystemHand)  ;  Yield()
                        Message('Mismatch Network.'&|
                                '||This job will now fail QA.', |
                                'ServiceBase 2000', Icon:Hand, |
                                 Button:OK, Button:OK, 0)
                        reason" = ''
                        QA_Failure_Reason(reason")
                        GET(audit,0)
                        IF (access:audit.primerecord() = level:benign) THEN
                            aud:notes         = 'NETWORK RECORDED: ' & CLIP(jobe:Network) & |
                                                '<13,10,13,10>NETWORK VALIDATED: ' & CLIP(net:Network)
                            glo:notes_global    = Clip(aud:notes)

                            aud:ref_number    = job:ref_number
                            aud:date          = today()
                            aud:time          = clock()
                            aud:type          = 'JOB'
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            aud:user = use:user_code
                            CASE QA_Type
                                OF 'PRE'
                                    aud:action        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                OF 'PRI'
                                    aud:action        = 'QA REJECTION MANUAL: ' & Clip(reason")
                            END
                            access:audit.insert()
                        END
                        glo:select1 = job:ref_number
                        IF (def:qa_failed_label = 'YES') THEN
                            QA_Failed_Label
                        END
                        IF (def:qa_failed_report = 'YES') THEN
                            QA_Failed
                        END
                        glo:select1 = ''
                        glo:notes_global    = ''
                        CASE QA_Type
                            OF 'PRE'
                                GetStatus(625,1,'JOB')
                            ELSE
                                GetStatus(615,1,'JOB')
                        END

                        job:qa_rejected = 'YES'
                        job:date_qa_rejected    = Today()
                        job:time_qa_rejected    = Clock()
                        access:jobs.update()
                        !Do passed_fail_update
                        RETURN 0
                    END
                ELSE
                    RETURN 0
                END
            ELSE
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
                RETURN 1
            END
        ELSE
            RETURN 1
        END
    ELSE
        RETURN 1
    END


CheckPartNo        PROCEDURE(PartNo,PartRefNo)
result              Byte
temp_user_code      String(3)
temp_location       String(30)
    CODE

    result = 0

    !access:stock.clearkey(sto:manufacturer_key)
    !sto:manufacturer = job:manufacturer
    !sto:part_number = PartNo
    !IF (access:stock.fetch(sto:manufacturer_key) = Level:Benign) THEN
    !    result = 1
    !END
    temp_user_code  = ''
    temp_location = ''
    PartRefNo = ''
    IF (job:engineer <> '') THEN
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code   = job:Engineer
        IF (Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign) THEN
            temp_user_code  = use:User_Code
        END
    ELSE
        Access:USERS.Clearkey(use:Password_Key)
        use:Password    = glo:Password
        IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign) THEN
            temp_user_code  = use:User_Code
        END
    END


    IF (temp_user_code <> '') THEN
        Access:LOCATION.ClearKey(loc:ActiveLocationKey)
        loc:Active   = 1
        loc:Location = use:Location
        IF (Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign) THEN
            temp_location = loc:Location
        ELSE
            IF (~use:StockFromLocationOnly) THEN
                SET(defstock)
                Access:defstock.next()
                IF (dst:site_location <> '') THEN
                    temp_location = dst:site_location
                END
            END
        END
        IF (temp_location <> '') THEN
            access:stomodel.clearkey(stm:location_part_number_key)
            stm:model_number = job:model_number
            stm:location = temp_location
            stm:part_number = PartNo
            IF (access:stomodel.fetch(stm:location_part_number_key) = Level:Benign) THEN
                result = 1
                PartRefNo = stm:ref_number
            END
        END
    END

    RETURN(result)
CheckMandatoryFaultCodes    PROCEDURE(charge_type, repair_type, isWarranty)
result             Byte
reqflag            Long
    CODE

    result = 0
    reqflag = 0
    IF (charge_type = '') THEN
        reqflag = 0
    ELSE
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = charge_type
        IF (access:chartype.fetch(cha:charge_type_key) = Level:Benign) THEN
            IF (cha:force_warranty = 'YES') THEN
                IF (repair_type = '') THEN
                    reqflag = 1
                ELSE
                    IF (isWarranty = 1) THEN
                        Access:REPTYDEF.ClearKey(rtd:Warranty_Key)
                        rtd:Warranty    = 'YES'
                        rtd:Repair_Type = repair_type
                        IF (Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign) THEN
                            IF (rtd:CompFaultCoding) THEN
                                reqflag = 1
                            END
                        ELSE
                            reqflag = 1
                        END
                    ELSE
                        Access:REPTYDEF.ClearKey(rtd:Chargeable_Key)
                        rtd:Chargeable    = 'YES'
                        rtd:Repair_Type = repair_type
                        IF (Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign) THEN
                            IF (rtd:CompFaultCoding) THEN
                                reqflag = 1
                            END
                        ELSE
                            reqflag = 1
                        END
                    END
                END
            END
        END
    END

    IF (reqflag = 1) THEN
        access:manfaupa.clearkey(map:field_number_key)
        map:manufacturer = job:manufacturer
        SET(map:field_number_key,map:field_number_key)
        LOOP
            IF ((access:manfaupa.next() <> Level:Benign) OR (map:manufacturer <> job:manufacturer)) THEN
                BREAK
            END
            IF (MAP:Compulsory = 'YES') THEN
                result = 1
                BREAK
            END
        END
    END

    RETURN(result)
CheckWarrantyPartExists PROCEDURE(PartNo)
result  Byte
    CODE
    result = 0
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    wpr:part_number = PartNo
    IF (access:warparts.fetch(wpr:part_number_key) <> Level:benign) THEN
        result = 1
    END
    RETURN(result)
CheckChargeablePartExists PROCEDURE(PartNo)
result  Byte
    CODE
    result = 0
    access:parts.clearkey(par:part_number_key)
    par:ref_number  = job:ref_number
    par:part_number = PartNo
    IF (access:parts.fetch(par:part_number_key) <> Level:benign) THEN
        result = 1
    END
    RETURN(result)
Add_Chargeable PROCEDURE(PartRefNo)
    CODE

    IF (CheckMandatoryFaultCodes(job:charge_type, job:Repair_Type, 0) = 1) THEN
        !Post(event:accepted,?WarInsert)
        IF (SetPartFaultCodes(job:charge_type,            |
                                pfc:FaultCode1,           |
                                pfc:FaultCode2,           |
                                pfc:FaultCode3,           |
                                pfc:FaultCode4,           |
                                pfc:FaultCode5,           |
                                pfc:FaultCode6,           |
                                pfc:FaultCode7,           |
                                pfc:FaultCode8,           |
                                pfc:FaultCode9,           |
                                pfc:FaultCode10,          |
                                pfc:FaultCode11,          |
                                pfc:FaultCode12) = 0) THEN
            RETURN
        END
    ELSE
        pfc:FaultCode1 = ''
        pfc:FaultCode2 = ''
        pfc:FaultCode3 = ''
        pfc:FaultCode4 = ''
        pfc:FaultCode5 = ''
        pfc:FaultCode6 = ''
        pfc:FaultCode7 = ''
        pfc:FaultCode8 = ''
        pfc:FaultCode9 = ''
        pfc:FaultCode10 = ''
        pfc:FaultCode11 = ''
        pfc:FaultCode12 = ''
    END

    access:stomodel.clearkey(stm:model_number_key)
    stm:ref_number   = PartRefNo
    stm:manufacturer = job:manufacturer
    stm:model_number = job:model_number
    if access:stomodel.fetch(stm:model_number_key) = Level:Benign
        access:stock.clearkey(sto:ref_number_key)
        sto:ref_number = stm:ref_number
        if access:stock.fetch(sto:ref_number_key) = Level:Benign

            ! Start Change 1895 BE(15/05/03)
            DO InitMaxParts
            IF (tmpCheckChargeableParts) THEN
                DO TotalChargeableParts
                TotalPartsCost += sto:Purchase_cost ! Qty = 1
                IF (TotalPartsCost > tra:MaxChargeablePartsCost) THEN
                    MESSAGE('Part No.' & CLIP(sto:part_number) & ' cannot be attached to the job.<13,10>' & |
                            'This would exceed the Maximum Chargeable Parts Cost of the Trade Account.', |
                            'ServiceBase 2000', Icon:Question, 'OK', 1, 0)
                    RETURN
                END
            END
            ! End Change 1895 BE(15/05/03)

            If sto:sundry_item = 'YES'
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = job:ref_number
                par:part_number = sto:part_number
                if access:parts.fetch(par:part_number_key)
                    Do Add_Chargeable_Part
                    if def:PickNoteNormal or def:PickNoteMainStore then
                        CreatePickingNote (sto:Location)
                        InsertPickingPart ('chargeable')
                    end
                End!if access:parts.fetch(par:part_number_key)
            Else!If sto:sundry_item = 'YES'
                If sto:quantity_stock < 1
                    If SecurityCheck('JOBS - ORDER PARTS')
                        Case MessageEx(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                       '<13,10>'&|
                                       '<13,10>This part is out of stock.','ServiceBase 2000',|
                                       'Styles\hand.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                            Of 1 ! &OK Button
                        End !
                    ! Start Change 216 BE(26/06/03)
                    ELSIF (sto:RF_BOARD) THEN
                        Beep(Beep:SystemHand)  ;  Yield()
                        Message(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                '||There are insufficient items in stock!' & |
                                '||This part requires the entry of a new IMEI when attached to a job.' & |
                                '||This part can therefore not be added at this time.', |
                                'ServiceBase 2000', Icon:Hand, |
                                '&OK', 1, 0)
                    ! End Change 216 BE(26/06/03)
                    Else !If SecurityCheck('JOBS - ORDER PARTS')
                        Case MessageEx(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                       '<13,10>'&|
                                       '<13,10>This part is out of stock. Do you wish to place it on Back Order?','ServiceBase 2000',|
                                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                            Of 1 ! &Yes Button
                                Case def:SummaryOrders
                                    Of 0 !Old Way
                                        Do MakePartsOrder
                                        Do Add_Chargeable_Part
                                        par:Pending_Ref_number  = ope:Ref_Number
                                        par:Date_Ordered    = ''
                                        Access:PARTS.Update()
                                        if def:PickNoteNormal or def:PickNoteMainStore then
                                            CreatePickingNote (sto:Location)
                                            InsertPickingPart ('chargeable')
                                        end
                                    Of 1 !New Way
                                        Do Add_Chargeable_Part
                                        Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                        ope:Supplier    = par:Supplier
                                        ope:Part_Number = par:Part_Number
                                        If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                            ope:Quantity    += 1
                                            Access:ORDPEND.Update()
                                        Else !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                            Do MakePartsOrder
                                        End !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                        par:Pending_Ref_Number  = ope:Ref_Number
                                        par:Requested   = True
                                        par:Date_Ordered = ''
                                        Access:PARTS.Update()
                                        if def:PickNoteNormal or def:PickNoteMainStore then
                                            CreatePickingNote (sto:Location)
                                            InsertPickingPart ('chargeable')
                                        end

                                End !Case def:SummaryOrders
                           Of 2 ! &No Button
                        End!Case MessageEx
                    End !If SecurityCheck('JOBS - ORDER PARTS')

                Else!If sto:quantity_stock < 1
                    access:parts.clearkey(par:part_number_key)
                    par:ref_number  = job:ref_number
                    par:part_number = sto:part_number
                    if access:parts.fetch(par:part_number_key)
                        !TH 09/02/04
                        Access:LOCATION.ClearKey(loc:ActiveLocationKey)
                        loc:Active   = 1
                        loc:Location = sto:Location
                        If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.

                        if (~loc:PickNoteEnable) then
                            ! Start Change 2116 BE(26/06/03)
                            IF (sto:RF_BOARD) THEN
                                 glo:select2 = ''
                                 glo:select3 = job:model_number
                                 glo:select4 = epr:part_number
                                 EnterNewIMEI
                                 tmp_RF_Board_IMEI = CLIP(glo:select2)
                                 glo:select2 = ''
                                 glo:select3 = ''
                                 glo:select4 = ''
                                 IF (tmp_RF_Board_IMEI = '') THEN
                                     RETURN
                                 END
                                 GET(audit,0)
                                 IF (access:audit.primerecord() = level:benign) THEN
                                     aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                                         '<13,10>Old IMEI ' & Clip(job:esn)
                                     aud:ref_number    = job:ref_number
                                     aud:date          = Today()
                                     aud:time          = Clock()
                                     access:users.clearkey(use:password_key)
                                     use:password =glo:password
                                     access:users.fetch(use:password_key)
                                     aud:user = use:user_code
                                     aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                                     IF (access:audit.insert() <> Level:benign) THEN
                                         access:audit.cancelautoinc()
                                     END
                                 END
                                 !access:JOBSE.clearkey(jobe:RefNumberKey)
                                 !jobe:RefNumber  = job:Ref_Number
                                 !IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                                 !    IF (access:JOBSE.primerecord() = Level:Benign) THEN
                                 !        jobe:RefNumber  = job:Ref_Number
                                 !        IF (access:JOBSE.tryinsert()) THEN
                                 !            access:JOBSE.cancelautoinc()
                                 !        END
                                 !    END
                                 !END
                                 IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                                     jobe:PRE_RF_BOARD_IMEI = job:ESN
                                     access:jobse.update()
                                 END
                                 job:ESN = tmp_RF_Board_IMEI
                                 access:jobs.update()
                            END
                            ! End Change 2116 BE(26/06/03)
                       end
                       Do Add_Chargeable_Part
                       if def:PickNoteNormal or def:PickNoteMainStore then
                           CreatePickingNote (sto:Location)
                           InsertPickingPart ('chargeable')
                       end
                       sto:quantity_stock -= 1
                       If sto:quantity_stock < 0
                              sto:quantity_stock = 0
                       End!If sto:quantity_stock < 0
                       access:stock.update()
                       get(stohist,0)
                       if access:stohist.primerecord() = level:benign
                           shi:ref_number           = sto:ref_number
                           access:users.clearkey(use:password_key)
                           use:password              =glo:password
                           access:users.fetch(use:password_key)
                           shi:user                  = use:user_code
                           shi:date                 = today()
                           shi:transaction_type     = 'DEC'
                           shi:job_number           = job:ref_number
                           shi:quantity             = 1
                           shi:purchase_cost        = sto:purchase_cost
                           shi:sale_cost            = sto:sale_cost
                           shi:retail_cost          = sto:retail_cost
                           shi:information          = ''
                           shi:notes                = 'STOCK DECREMENTED'
                           if access:stohist.insert()
                                access:stohist.cancelautoinc()
                            end
                       end!if access:stohist.primerecord() = level:benign
                   end!if access:parts.fetch(par:part_number_key)
               End!If sto:quantity_stock < 1
           End!If sto:sundry_item = 'YES'
       end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
    end!if access:stomodel.fetch(stm:model_number_key) = Level:Benign

    RETURN
Add_Warranty   PROCEDURE(PartRefNo)
    CODE

    IF (CheckMandatoryFaultCodes(job:warranty_charge_type, job:Repair_Type_Warranty, 1) = 1) THEN
        !Post(event:accepted,?WarInsert)
        IF (SetPartFaultCodes(job:warranty_charge_type, |
                          pfc:FaultCode1,           |
                          pfc:FaultCode2,           |
                          pfc:FaultCode3,           |
                          pfc:FaultCode4,           |
                          pfc:FaultCode5,           |
                          pfc:FaultCode6,           |
                          pfc:FaultCode7,           |
                          pfc:FaultCode8,           |
                          pfc:FaultCode9,           |
                          pfc:FaultCode10,          |
                          pfc:FaultCode11,          |
                          pfc:FaultCode12) = 0) THEN
            RETURN
        END
    ELSE
        pfc:FaultCode1 = ''
        pfc:FaultCode2 = ''
        pfc:FaultCode3 = ''
        pfc:FaultCode4 = ''
        pfc:FaultCode5 = ''
        pfc:FaultCode6 = ''
        pfc:FaultCode7 = ''
        pfc:FaultCode8 = ''
        pfc:FaultCode9 = ''
        pfc:FaultCode10 = ''
        pfc:FaultCode11 = ''
        pfc:FaultCode12 = ''
    END

        access:stomodel.clearkey(stm:model_number_key)
        stm:ref_number   = PartRefNo
        stm:manufacturer = job:manufacturer
        stm:model_number = job:model_number
        IF (access:stomodel.fetch(stm:model_number_key) = Level:Benign) THEN
            access:stock.clearkey(sto:ref_number_key)
            sto:ref_number = stm:ref_number
            IF (access:stock.fetch(sto:ref_number_key) = Level:Benign) THEN
                IF (sto:sundry_item = 'YES') THEN
                    access:warparts.clearkey(wpr:part_number_key)
                    wpr:ref_number  = job:ref_number
                    wpr:part_number = sto:part_number
                    IF (access:warparts.fetch(wpr:part_number_key) <> Level:Benign) THEN
                        Do Add_Warranty_Part
                        if def:PickNoteNormal or def:PickNoteMainStore then
                             CreatePickingNote (sto:Location)
                             InsertPickingPart ('warranty')
                        end
                    END
                ELSE
                    IF (sto:quantity_stock < 1) THEN
                        IF (SecurityCheck('JOBS - ORDER PARTS')) THEN
                            MessageEx(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                           '<13,10>'&|
                                           '<13,10>This part is out of stock.','ServiceBase 2000',|
                                           'Styles\hand.ico','&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                           beep:systemquestion,msgex:samewidths,84,26,0)
                        ! Start Change 216 BE(26/06/03)
                        ELSIF (sto:RF_BOARD) THEN
                            Beep(Beep:SystemHand)  ;  Yield()
                            Message(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                    '||There are insufficient items in stock!' & |
                                    '||This part requires the entry of a new IMEI when attached to a job.' & |
                                    '||This part can therefore not be added at this time.', |
                                    'ServiceBase 2000', Icon:Hand, |
                                    '&OK', 1, 0)
                        ! End Change 216 BE(26/06/03)
                        ELSE
                            CASE MessageEx(Clip(sto:Part_number) & ' - ' & Clip(sto:Description) & '.'&|
                                           '<13,10>'&|
                                           '<13,10>This part is out of stock. Do you wish to place it on Back Order?','ServiceBase 2000',|
                                           'Styles\question.ico','&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                           beep:systemquestion,msgex:samewidths,84,26,0)
                               OF 1 ! &Yes Button
                                            
                                    CASE def:SummaryOrders
                                        OF 0 !Old Way
                                            DO MakeWarPartsOrder
                                            DO Add_Warranty_Part
                                            wpr:Pending_Ref_number  = ope:Ref_Number
                                            wpr:Date_Ordered    = ''
                                            Access:WARPARTS.Update()
                                            if def:PickNoteNormal or def:PickNoteMainStore then
                                                 CreatePickingNote (sto:Location)
                                                 InsertPickingPart ('warranty')
                                            end
                                        OF 1 !New Way
                                            DO Add_Warranty_Part
                                            Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                            ope:Supplier    = wpr:Supplier
                                            ope:Part_Number = wpr:Part_Number
                                            IF (Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign) THEN
                                                ope:Quantity    += 1
                                                Access:ORDPEND.Update()
                                            ELSE
                                                DO MakeWarPartsOrder
                                            END
                                            wpr:Pending_Ref_Number  = ope:Ref_Number
                                            wpr:Requested   = True
                                            wpr:Date_Ordered = ''
                                            Access:WARPARTS.Update()
                                            if def:PickNoteNormal or def:PickNoteMainStore then
                                                 CreatePickingNote (sto:Location)
                                                 InsertPickingPart ('warranty')
                                            end
                                    END
                                OF 2 ! &No Button
                            END
                       END
                   ELSE !If sto:quantity_stock < 1
                       access:warparts.clearkey(wpr:part_number_key)
                       wpr:ref_number  = job:ref_number
                       wpr:part_number = sto:part_number
                       IF (access:warparts.fetch(wpr:part_number_key)) THEN
                            !TH 09/02/04
                            Access:LOCATION.ClearKey(loc:ActiveLocationKey)
                            loc:Active   = 1
                            loc:Location = sto:Location
                            If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.

                            if (~loc:PickNoteEnable) then
                                ! Start Change 2116 BE(26/06/03)
                                IF (sto:RF_BOARD) THEN
                                    glo:select2 = ''
                                    glo:select3 = job:model_number
                                    glo:select4 = epr:part_number
                                    EnterNewIMEI
                                    tmp_RF_Board_IMEI = CLIP(glo:select2)
                                    glo:select2 = ''
                                    glo:select3 = ''
                                    glo:select4 = ''
                                    IF (tmp_RF_Board_IMEI = '') THEN
                                         RETURN
                                    END
                                    GET(audit,0)
                                    IF (access:audit.primerecord() = level:benign) THEN
                                        aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                                            '<13,10>Old IMEI ' & Clip(job:esn)
                                        aud:ref_number    = job:ref_number
                                        aud:date          = Today()
                                        aud:time          = Clock()
                                        access:users.clearkey(use:password_key)
                                        use:password =glo:password
                                        access:users.fetch(use:password_key)
                                        aud:user = use:user_code
                                        aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                                        IF (access:audit.insert() <> Level:benign) THEN
                                            access:audit.cancelautoinc()
                                        END
                                    END
                                    !access:JOBSE.clearkey(jobe:RefNumberKey)
                                    !jobe:RefNumber  = job:Ref_Number
                                    !IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                                    !    IF (access:JOBSE.primerecord() = Level:Benign) THEN
                                    !        jobe:RefNumber  = job:Ref_Number
                                    !        IF (access:JOBSE.tryinsert()) THEN
                                    !            access:JOBSE.cancelautoinc()
                                    !        END
                                    !    END
                                    !END
                                    IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                                        jobe:PRE_RF_BOARD_IMEI = job:ESN
                                        access:jobse.update()
                                    END
                                    job:ESN = tmp_RF_Board_IMEI
                                    access:jobs.update()
                                END
                                ! End Change 2116 BE(26/06/03)
                           end
                           DO Add_Warranty_Part
                           if def:PickNoteNormal or def:PickNoteMainStore then
                               CreatePickingNote (sto:Location)
                               InsertPickingPart ('warranty')
                           end
                           sto:quantity_stock -= 1
                           IF (sto:quantity_stock < 0) THEN
                               sto:quantity_stock = 0
                           END
                           access:stock.update()
                           GET(stohist,0)
                           IF (access:stohist.primerecord() = level:benign) THEN
                               shi:ref_number           = sto:ref_number
                               access:users.clearkey(use:password_key)
                               use:password              =glo:password
                               access:users.fetch(use:password_key)
                               shi:user                  = use:user_code
                               shi:date                 = today()
                               shi:transaction_type     = 'DEC'
                               shi:job_number           = job:ref_number
                               shi:quantity             = 1
                               shi:purchase_cost        = sto:purchase_cost
                               shi:sale_cost            = sto:sale_cost
                               shi:retail_cost          = sto:retail_cost
                               shi:information          = ''
                               shi:notes                = 'STOCK DECREMENTED'
                               IF (access:stohist.insert()) THEN
                                   access:stohist.cancelautoinc()
                               END
                           END
                       END !if access:warparts.fetch(wpr:part_number_key)
                   END !If sto:quantity_stock < 1
               END !If sto:sundry_item <> 'YES'
           END !if access:stock.fetch(sto:ref_number_key) = Level:Benign
        END !access:stomodel.fetch(stm:model_number_key) = Level:Benign
    RETURN

BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?PartInsert
    SELF.ChangeControl=?PartChange
    SELF.DeleteControl=?PartDelete
  END


BRW4.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.par:Part_Number_NormalFG = -1
  SELF.Q.par:Part_Number_NormalBG = -1
  SELF.Q.par:Part_Number_SelectedFG = -1
  SELF.Q.par:Part_Number_SelectedBG = -1
  SELF.Q.par:Description_NormalFG = -1
  SELF.Q.par:Description_NormalBG = -1
  SELF.Q.par:Description_SelectedFG = -1
  SELF.Q.par:Description_SelectedBG = -1
  SELF.Q.par:Quantity_NormalFG = -1
  SELF.Q.par:Quantity_NormalBG = -1
  SELF.Q.par:Quantity_SelectedFG = -1
  SELF.Q.par:Quantity_SelectedBG = -1
   
   
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Part_Number_NormalFG = 32768
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Part_Number_NormalFG = 255
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Part_Number_NormalFG = 8421504
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Part_Number_NormalFG = -1
     SELF.Q.par:Part_Number_NormalBG = -1
     SELF.Q.par:Part_Number_SelectedFG = -1
     SELF.Q.par:Part_Number_SelectedBG = -1
   END
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Description_NormalFG = 32768
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Description_NormalFG = 255
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Description_NormalFG = 8421504
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Description_NormalFG = -1
     SELF.Q.par:Description_NormalBG = -1
     SELF.Q.par:Description_SelectedFG = -1
     SELF.Q.par:Description_SelectedBG = -1
   END
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Quantity_NormalFG = 32768
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Quantity_NormalFG = 255
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Quantity_NormalFG = 8421504
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Quantity_NormalFG = -1
     SELF.Q.par:Quantity_NormalBG = -1
     SELF.Q.par:Quantity_SelectedFG = -1
     SELF.Q.par:Quantity_SelectedBG = -1
   END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW19.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?WarInsert
    SELF.ChangeControl=?WarChange
    SELF.DeleteControl=?WarDelete
  END


BRW19.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.wpr:Part_Number_NormalFG = -1
  SELF.Q.wpr:Part_Number_NormalBG = -1
  SELF.Q.wpr:Part_Number_SelectedFG = -1
  SELF.Q.wpr:Part_Number_SelectedBG = -1
  SELF.Q.wpr:Description_NormalFG = -1
  SELF.Q.wpr:Description_NormalBG = -1
  SELF.Q.wpr:Description_SelectedFG = -1
  SELF.Q.wpr:Description_SelectedBG = -1
  SELF.Q.wpr:Quantity_NormalFG = -1
  SELF.Q.wpr:Quantity_NormalBG = -1
  SELF.Q.wpr:Quantity_SelectedFG = -1
  SELF.Q.wpr:Quantity_SelectedBG = -1
   
   
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Part_Number_NormalFG = 32768
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Part_Number_NormalFG = 255
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Part_Number_NormalFG = 8421504
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Part_Number_NormalFG = -1
     SELF.Q.wpr:Part_Number_NormalBG = -1
     SELF.Q.wpr:Part_Number_SelectedFG = -1
     SELF.Q.wpr:Part_Number_SelectedBG = -1
   END
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Description_NormalFG = 32768
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Description_NormalFG = 255
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Description_NormalFG = 8421504
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Description_NormalFG = -1
     SELF.Q.wpr:Description_NormalBG = -1
     SELF.Q.wpr:Description_SelectedFG = -1
     SELF.Q.wpr:Description_SelectedBG = -1
   END
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Quantity_NormalFG = 32768
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Quantity_NormalFG = 255
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Quantity_NormalFG = 8421504
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Quantity_NormalFG = -1
     SELF.Q.wpr:Quantity_NormalBG = -1
     SELF.Q.wpr:Quantity_SelectedFG = -1
     SELF.Q.wpr:Quantity_SelectedBG = -1
   END


BRW19.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
