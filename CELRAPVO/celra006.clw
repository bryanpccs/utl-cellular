

   MEMBER('celrapvo.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA010.INC'),ONCE        !Req'd for module callout resolution
                     END


AllocateJob PROCEDURE (f_refNumber)                   !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:engineer         STRING(30)
tmp:Location         STRING(30)
tmp:status           STRING(30)
tmp:usercode         STRING(3)
window               WINDOW('Allocate Job'),AT(,,230,103),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,68),USE(?Sheet1),SPREAD
                         TAB('Allocate Job'),USE(?Tab1)
                           PROMPT('Engineer'),AT(8,20),USE(?tmp:engineer:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,64,10),USE(tmp:engineer),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Engineers'),TIP('Engineers'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(152,20,10,10),USE(?LookupEngineer),SKIP,ICON('List3.ico')
                           PROMPT('Location'),AT(8,36),USE(?tmp:Location:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(tmp:Location),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,36,10,10),USE(?LookupLocation),SKIP,ICON('List3.ico')
                           PROMPT('Status'),AT(8,52),USE(?tmp:status:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(tmp:status),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Status'),TIP('Status'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupStatus),SKIP,ICON('List3.ico')
                         END
                       END
                       PANEL,AT(4,76,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(168,80,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       BUTTON('&OK'),AT(112,80,56,16),USE(?Button4),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
look:tmp:engineer                Like(tmp:engineer)
look:tmp:Location                Like(tmp:Location)
look:tmp:status                Like(tmp:status)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:engineer:Prompt{prop:FontColor} = -1
    ?tmp:engineer:Prompt{prop:Color} = 15066597
    If ?tmp:engineer{prop:ReadOnly} = True
        ?tmp:engineer{prop:FontColor} = 65793
        ?tmp:engineer{prop:Color} = 15066597
    Elsif ?tmp:engineer{prop:Req} = True
        ?tmp:engineer{prop:FontColor} = 65793
        ?tmp:engineer{prop:Color} = 8454143
    Else ! If ?tmp:engineer{prop:Req} = True
        ?tmp:engineer{prop:FontColor} = 65793
        ?tmp:engineer{prop:Color} = 16777215
    End ! If ?tmp:engineer{prop:Req} = True
    ?tmp:engineer{prop:Trn} = 0
    ?tmp:engineer{prop:FontStyle} = font:Bold
    ?tmp:Location:Prompt{prop:FontColor} = -1
    ?tmp:Location:Prompt{prop:Color} = 15066597
    If ?tmp:Location{prop:ReadOnly} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 15066597
    Elsif ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 8454143
    Else ! If ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 16777215
    End ! If ?tmp:Location{prop:Req} = True
    ?tmp:Location{prop:Trn} = 0
    ?tmp:Location{prop:FontStyle} = font:Bold
    ?tmp:status:Prompt{prop:FontColor} = -1
    ?tmp:status:Prompt{prop:Color} = 15066597
    If ?tmp:status{prop:ReadOnly} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 15066597
    Elsif ?tmp:status{prop:Req} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 8454143
    Else ! If ?tmp:status{prop:Req} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 16777215
    End ! If ?tmp:status{prop:Req} = True
    ?tmp:status{prop:Trn} = 0
    ?tmp:status{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AllocateJob',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:engineer',tmp:engineer,'AllocateJob',1)
    SolaceViewVars('tmp:Location',tmp:Location,'AllocateJob',1)
    SolaceViewVars('tmp:status',tmp:status,'AllocateJob',1)
    SolaceViewVars('tmp:usercode',tmp:usercode,'AllocateJob',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:engineer:Prompt;  SolaceCtrlName = '?tmp:engineer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:engineer;  SolaceCtrlName = '?tmp:engineer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEngineer;  SolaceCtrlName = '?LookupEngineer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location:Prompt;  SolaceCtrlName = '?tmp:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location;  SolaceCtrlName = '?tmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:status:Prompt;  SolaceCtrlName = '?tmp:status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:status;  SolaceCtrlName = '?tmp:status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStatus;  SolaceCtrlName = '?LookupStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AllocateJob')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'AllocateJob')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:engineer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:STAHEAD.Open
  Access:USERS.UseFile
  Access:LOCINTER.UseFile
  Access:STATUS.UseFile
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBSE.UseFile
  Access:JOBSENG.UseFile
  SELF.FilesOpened = True
  tmp:engineer    = Clip(GETINI('ALLOCATEJOB','Engineer',,CLIP(PATH())&'\CELRAPEN.INI'))
  tmp:location    = Clip(GETINI('ALLOCATEJOB','Location',,CLIP(PATH())&'\CELRAPEN.INI'))
  tmp:status      = Clip(GETINI('ALLOCATEJOB','Status',,CLIP(PATH())&'\CELRAPEN.INI'))
  tmp:usercode    = Clip(GETINI('ALLOCATEJOB','UserCode',,CLIP(PATH())&'\CELRAPEN.INI'))
  
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      ?tmp:Location{prop:Hide} = 1
      ?tmp:Location:Prompt{prop:Hide} = 1
      ?LookupLocation{prop:Hide} = 1
  End !def:HideLocation
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  IF ?tmp:status{Prop:Tip} AND ~?LookupStatus{Prop:Tip}
     ?LookupStatus{Prop:Tip} = 'Select ' & ?tmp:status{Prop:Tip}
  END
  IF ?tmp:status{Prop:Msg} AND ~?LookupStatus{Prop:Msg}
     ?LookupStatus{Prop:Msg} = 'Select ' & ?tmp:status{Prop:Msg}
  END
  IF ?tmp:Location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?tmp:Location{Prop:Tip}
  END
  IF ?tmp:Location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?tmp:Location{Prop:Msg}
  END
  IF ?tmp:engineer{Prop:Tip} AND ~?LookupEngineer{Prop:Tip}
     ?LookupEngineer{Prop:Tip} = 'Select ' & ?tmp:engineer{Prop:Tip}
  END
  IF ?tmp:engineer{Prop:Msg} AND ~?LookupEngineer{Prop:Msg}
     ?LookupEngineer{Prop:Msg} = 'Select ' & ?tmp:engineer{Prop:Msg}
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:STAHEAD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'AllocateJob',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Users_Job_Assignment
      Browse_Available_Locations
      Browse_Status
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:engineer
      IF tmp:engineer OR ?tmp:engineer{Prop:Req}
        use:Surname = tmp:engineer
        use:User_Type = 'ENGINEER'
        use:Active = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:engineer        = tmp:engineer
        IF Access:USERS.TryFetch(use:User_Type_Active_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:engineer = use:Surname
            tmp:usercode = USE:User_Code
          ELSE
            CLEAR(tmp:usercode)
            CLEAR(use:User_Type)
            CLEAR(use:Active)
            !Restore Lookup On Error
            tmp:engineer = look:tmp:engineer
            SELECT(?tmp:engineer)
            CYCLE
          END
        ELSE
          tmp:usercode = USE:User_Code
        END
      END
      ThisWindow.Reset()
    OF ?LookupEngineer
      ThisWindow.Update
      use:Surname = tmp:engineer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:engineer = use:Surname
          Select(?+1)
      ELSE
          Select(?tmp:engineer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:engineer)
    OF ?tmp:Location
      IF tmp:Location OR ?tmp:Location{Prop:Req}
        loi:Location = tmp:Location
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:Location        = tmp:Location
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Location = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            tmp:Location = look:tmp:Location
            SELECT(?tmp:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = tmp:Location
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Location = loi:Location
          Select(?+1)
      ELSE
          Select(?tmp:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Location)
    OF ?tmp:status
      IF tmp:status OR ?tmp:status{Prop:Req}
        sts:Status = tmp:status
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:status        = tmp:status
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:status = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:status = look:tmp:status
            SELECT(?tmp:status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupStatus
      ThisWindow.Update
      sts:Status = tmp:status
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:status = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:status)
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = f_refnumber
      If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
          !Found
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_number
          If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
              !Found
      
          Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
          If tmp:Status <> ''
              GetStatus(Sub(Clip(tmp:status),1,3),1,'JOB')
          End!If tmp:Status <> ''
          If tmp:Location <> ''
              !Add To Old Location
                  If job:location <> ''
                      access:locinter.clearkey(loi:location_key)
                      loi:location = job:location
                      If access:locinter.fetch(loi:location_key) = Level:Benign
                          If loi:allocate_spaces = 'YES'
                              loi:current_spaces+= 1
                              loi:location_available = 'YES'
                              access:locinter.update()
                          End
                      end !if
                  End!If job:location <> ''
              !Take From New Location
                  job:location = tmp:Location
      
                  access:locinter.clearkey(loi:location_key)
                  loi:location = job:location
                  If access:locinter.fetch(loi:location_key) = Level:Benign
                      If loi:allocate_spaces = 'YES'
                          loi:current_spaces -= 1
                          If loi:current_spaces< 1
                              loi:current_spaces = 0
                              loi:location_available = 'NO'
                          End
                          access:locinter.update()
                      End
                  end !if
      
          End!If tmp:Location <> ''
              
          IF tmp:usercode = ''
            IF job:engineer <> ''
               Case MessageEx('You have not entered a usercode. There is currently an engineer allocated to this job. Do you wish to remove the engineer from this job?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
               OF 1 ! &Yes Button
                 job:engineer    = tmp:usercode
               Of 2 ! &No Button
                 !Nothing!
      
              End!Case MessageEx
            END
          ELSE
            job:engineer    = tmp:usercode
              Access:USERS.Clearkey(use:User_Code_Key)
              use:User_Code   = job:Engineer
              If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                  !Found
      
              Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              
          END
          access:jobs.update()
          If tmp:Usercode <> '' Or tmp:Location <> '' Or tmp:Status <> ''
              get(audit,0)
              if access:audit.primerecord() = level:benign
                  aud:notes         = 'FIELDS UPDATED:'
                  If tmp:Usercode <> ''
                      aud:Notes       = Clip(aud:Notes) & '<13,10>ENGINEER: ' & CLIP(tmp:Usercode) & |
                                          '<13,10>SKILL LEVEL: ' & Clip(use:SkillLevel)
                  End!If tmp:Usercode <> ''
                  If tmp:Location <> ''
                      aud:Notes       = Clip(aud:Notes) & '<13,10>LOCATION: ' & Clip(tmp:Location)
                  End!If tmp:Location <> ''
                  If tmp:Status <> ''
                      aud:Notes       = Clip(aud:Notes) & '<13,10>STATUS: ' & CLip(tmp:Status)
                  End!If tmp:Status <> ''
      
                  aud:ref_number    = job:ref_number
                  aud:date          = today()
                  aud:time          = clock()
                  aud:type          = 'JOB'
                  access:users.clearkey(use:password_key)
                  use:password = glo:password
                  access:users.fetch(use:password_key)
                  aud:user = use:user_code
                  ! Start Change 3602 BE(26/11/03)
                  !aud:action        = 'RAPID ENGINEER UPDATE: JOB ALLOCATED'
                  aud:action        = 'RAPID REPAIR PROCESS: JOB ALLOCATED'
                  ! End Change 3602 BE(26/11/03)
                  access:audit.insert()
              end!�if access:audit.primerecord() = level:benign
          End!If tmp:Usercode <> '' Or tmp:Location <> '' Or tmp:Status <> ''
      
          If Access:JOBSENG.PrimeRecord() = Level:Benign
              joe:JobNumber     = job:Ref_Number
              joe:UserCode      = job:Engineer
              joe:DateAllocated = Today()
              joe:TimeAllocated = Clock()
              joe:AllocatedBy   = use:User_Code
              access:users.clearkey(use:User_Code_Key)
              use:User_Code   = job:Engineer
              access:users.fetch(use:User_Code_Key)
      
              joe:EngSkillLevel = use:SkillLevel
              joe:JobSkillLevel = jobe:SkillLevel
      
              If Access:JOBSENG.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:JOBSENG.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:JOBSENG.TryInsert() = Level:Benign
          End !If Access:JOBSENG.PrimeRecord() = Level:Benign
      
      
          PUTINI('ALLOCATEJOB','Engineer',tmp:Engineer,CLIP(PATH()) & '\CELRAPEN.INI')
          PUTINI('ALLOCATEJOB','Location',tmp:Location,CLIP(PATH()) & '\CELRAPEN.INI')
          PUTINI('ALLOCATEJOB','Status',tmp:Status,CLIP(PATH()) & '\CELRAPEN.INI')
          PUTINI('ALLOCATEJOB','UserCode',tmp:UserCode,CLIP(PATH()) & '\CELRAPEN.INI')
          Post(event:closewindow)
      Else! If access:.tryfetch(job:ref_number_key) = Level:Benign
          !Error
      End! If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'AllocateJob')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:engineer
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:engineer, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Engineer')
             Post(Event:Accepted,?LookupEngineer)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupEngineer)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:engineer, AlertKey)
    END
  OF ?tmp:Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Location')
             Post(Event:Accepted,?tmp:Location)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?tmp:Location)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, AlertKey)
    END
  OF ?tmp:status
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:status, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Status')
             Post(Event:Accepted,?tmp:status)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?tmp:status)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:status, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

