

   MEMBER('celrapvo.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA012.INC'),ONCE        !Local module procedure declarations
                     END


SetPartFaultCodes PROCEDURE (f_Charge_Type,fc1,fc2,fc3,fc4,fc5,fc6,fc7,fc8,fc9,fc10,fc11,fc12) !Generated from procedure template - Window

PartFaultCodes       GROUP,PRE(pfc)
FaultCode1           STRING(30)
FaultCode2           STRING(30)
FaultCode3           STRING(30)
FaultCode4           STRING(30)
FaultCode5           STRING(30)
FaultCode6           STRING(30)
FaultCode7           STRING(30)
FaultCode8           STRING(30)
FaultCode9           STRING(30)
FaultCode10          STRING(30)
FaultCode11          STRING(30)
FaultCode12          STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
result               BYTE
window               WINDOW('Part Fault Codes'),AT(,,247,202),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,240,168),USE(?Sheet1),SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('Fault Code 1'),AT(8,20),USE(?Prompt1),TRN,HIDE
                           ENTRY(@d6b),AT(52,20,124,10),USE(pfc:FaultCode1),HIDE,UPR
                           BUTTON('...'),AT(212,20,10,10),USE(?Button1),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,20,10,10),USE(?PopCalendar:1),HIDE,LEFT,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,20,124,10),USE(pfc:FaultCode1,,?pfc:FaultCode1:2),HIDE,UPR
                           PROMPT('Fault Code 2'),AT(8,32),USE(?Prompt2),TRN,HIDE
                           ENTRY(@d6b),AT(52,32,124,10),USE(pfc:FaultCode2),HIDE,UPR
                           ENTRY(@s30),AT(84,32,124,10),USE(pfc:FaultCode2,,?pfc:FaultCode2:2),HIDE,UPR
                           PROMPT('Fault Code 3'),AT(8,44),USE(?Prompt3),TRN,HIDE
                           ENTRY(@d6b),AT(52,44,124,10),USE(pfc:FaultCode3),HIDE,UPR
                           PROMPT('Fault Code 10'),AT(8,128),USE(?Prompt10),TRN,HIDE
                           ENTRY(@d6b),AT(52,128,124,10),USE(pfc:FaultCode10),HIDE,UPR
                           PROMPT('Fault Code 4'),AT(8,56),USE(?Prompt4),TRN,HIDE
                           PROMPT('Fault Code 11'),AT(8,140),USE(?Prompt11),TRN,HIDE
                           ENTRY(@d6b),AT(52,140,124,10),USE(pfc:FaultCode11),HIDE,UPR
                           PROMPT('Fault Code 5'),AT(8,68),USE(?Prompt5),TRN,HIDE
                           ENTRY(@d6b),AT(52,56,124,10),USE(pfc:FaultCode4),HIDE,UPR
                           PROMPT('Fault Code 12'),AT(8,152),USE(?Prompt12),TRN,HIDE
                           ENTRY(@d6b),AT(52,152,124,10),USE(pfc:FaultCode12),HIDE,UPR
                           BUTTON('...'),AT(212,68,10,10),USE(?Button5),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,68,10,10),USE(?PopCalendar:5),HIDE,LEFT,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,68,124,10),USE(pfc:FaultCode5,,?pfc:FaultCode5:2),HIDE,UPR
                           PROMPT('Fault Code 6'),AT(8,80),USE(?Prompt6),TRN,HIDE
                           ENTRY(@d6b),AT(52,80,124,10),USE(pfc:FaultCode6),HIDE,UPR
                           ENTRY(@s30),AT(84,80,124,10),USE(pfc:FaultCode6,,?pfc:FaultCode6:2),HIDE,UPR
                           BUTTON('...'),AT(212,80,10,10),USE(?Button6),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,80,10,10),USE(?PopCalendar:6),HIDE,LEFT,ICON('Calenda2.ico')
                           ENTRY(@d6b),AT(52,68,124,10),USE(pfc:FaultCode5),HIDE,UPR
                           PROMPT('Fault Code 7'),AT(8,92),USE(?Prompt7),TRN,HIDE
                           ENTRY(@d6b),AT(52,92,124,10),USE(pfc:FaultCode7),HIDE,UPR
                           PROMPT('Fault Code 8'),AT(8,104),USE(?Prompt8),TRN,HIDE
                           PROMPT('Fault Code 9'),AT(8,116),USE(?Prompt9),TRN,HIDE
                           ENTRY(@d6b),AT(52,104,124,10),USE(pfc:FaultCode8),HIDE,UPR
                           ENTRY(@s30),AT(84,104,124,10),USE(pfc:FaultCode8,,?pfc:FaultCode8:2),HIDE,UPR
                           ENTRY(@d6b),AT(52,116,124,10),USE(pfc:FaultCode9),HIDE,UPR
                           BUTTON('...'),AT(212,116,10,10),USE(?Button9),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,116,10,10),USE(?PopCalendar:9),HIDE,LEFT,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,116,124,10),USE(pfc:FaultCode9,,?pfc:FaultCode9:2),HIDE,UPR
                           BUTTON('...'),AT(212,152,10,10),USE(?Button12),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,152,10,10),USE(?PopCalendar:12),HIDE,LEFT,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,152,124,10),USE(pfc:FaultCode12,,?pfc:FaultCode12:2),HIDE,UPR
                           BUTTON('...'),AT(212,140,10,10),USE(?Button11),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,140,10,10),USE(?PopCalendar:11),HIDE,LEFT,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,140,124,10),USE(pfc:FaultCode11,,?pfc:FaultCode11:2),HIDE,UPR
                           BUTTON('...'),AT(212,128,10,10),USE(?Button10),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,128,10,10),USE(?PopCalendar:10),HIDE,LEFT,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,128,124,10),USE(pfc:FaultCode10,,?pfc:FaultCode10:2),HIDE,UPR
                           BUTTON('...'),AT(212,104,10,10),USE(?Button8),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,104,10,10),USE(?PopCalendar:8),HIDE,LEFT,ICON('Calenda2.ico')
                           BUTTON('...'),AT(212,92,10,10),USE(?Button7),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,92,10,10),USE(?PopCalendar:7),HIDE,LEFT,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,92,124,10),USE(pfc:FaultCode7,,?pfc:FaultCode7:2),HIDE,UPR
                           BUTTON('...'),AT(212,56,10,10),USE(?Button4),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,56,10,10),USE(?PopCalendar:4),HIDE,LEFT,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,56,124,10),USE(pfc:FaultCode4,,?pfc:FaultCode4:2),HIDE,UPR
                           BUTTON('...'),AT(212,44,10,10),USE(?Button3),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,44,10,10),USE(?PopCalendar:3),HIDE,LEFT,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,44,124,10),USE(pfc:FaultCode3,,?pfc:FaultCode3:2),HIDE,UPR
                           BUTTON('...'),AT(212,32,10,10),USE(?Button2),HIDE,LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(228,32,10,10),USE(?PopCalendar:2),HIDE,LEFT,ICON('Calenda2.ico')
                         END
                       END
                       PANEL,AT(4,176,240,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(184,180,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif')
                       BUTTON('&OK'),AT(128,180,56,16),USE(?OKButton),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(result)

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?pfc:FaultCode1{prop:ReadOnly} = True
        ?pfc:FaultCode1{prop:FontColor} = 65793
        ?pfc:FaultCode1{prop:Color} = 15066597
    Elsif ?pfc:FaultCode1{prop:Req} = True
        ?pfc:FaultCode1{prop:FontColor} = 65793
        ?pfc:FaultCode1{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode1{prop:Req} = True
        ?pfc:FaultCode1{prop:FontColor} = 65793
        ?pfc:FaultCode1{prop:Color} = 16777215
    End ! If ?pfc:FaultCode1{prop:Req} = True
    ?pfc:FaultCode1{prop:Trn} = 0
    ?pfc:FaultCode1{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode1:2{prop:ReadOnly} = True
        ?pfc:FaultCode1:2{prop:FontColor} = 65793
        ?pfc:FaultCode1:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode1:2{prop:Req} = True
        ?pfc:FaultCode1:2{prop:FontColor} = 65793
        ?pfc:FaultCode1:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode1:2{prop:Req} = True
        ?pfc:FaultCode1:2{prop:FontColor} = 65793
        ?pfc:FaultCode1:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode1:2{prop:Req} = True
    ?pfc:FaultCode1:2{prop:Trn} = 0
    ?pfc:FaultCode1:2{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?pfc:FaultCode2{prop:ReadOnly} = True
        ?pfc:FaultCode2{prop:FontColor} = 65793
        ?pfc:FaultCode2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode2{prop:Req} = True
        ?pfc:FaultCode2{prop:FontColor} = 65793
        ?pfc:FaultCode2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode2{prop:Req} = True
        ?pfc:FaultCode2{prop:FontColor} = 65793
        ?pfc:FaultCode2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode2{prop:Req} = True
    ?pfc:FaultCode2{prop:Trn} = 0
    ?pfc:FaultCode2{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode2:2{prop:ReadOnly} = True
        ?pfc:FaultCode2:2{prop:FontColor} = 65793
        ?pfc:FaultCode2:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode2:2{prop:Req} = True
        ?pfc:FaultCode2:2{prop:FontColor} = 65793
        ?pfc:FaultCode2:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode2:2{prop:Req} = True
        ?pfc:FaultCode2:2{prop:FontColor} = 65793
        ?pfc:FaultCode2:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode2:2{prop:Req} = True
    ?pfc:FaultCode2:2{prop:Trn} = 0
    ?pfc:FaultCode2:2{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?pfc:FaultCode3{prop:ReadOnly} = True
        ?pfc:FaultCode3{prop:FontColor} = 65793
        ?pfc:FaultCode3{prop:Color} = 15066597
    Elsif ?pfc:FaultCode3{prop:Req} = True
        ?pfc:FaultCode3{prop:FontColor} = 65793
        ?pfc:FaultCode3{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode3{prop:Req} = True
        ?pfc:FaultCode3{prop:FontColor} = 65793
        ?pfc:FaultCode3{prop:Color} = 16777215
    End ! If ?pfc:FaultCode3{prop:Req} = True
    ?pfc:FaultCode3{prop:Trn} = 0
    ?pfc:FaultCode3{prop:FontStyle} = font:Bold
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    If ?pfc:FaultCode10{prop:ReadOnly} = True
        ?pfc:FaultCode10{prop:FontColor} = 65793
        ?pfc:FaultCode10{prop:Color} = 15066597
    Elsif ?pfc:FaultCode10{prop:Req} = True
        ?pfc:FaultCode10{prop:FontColor} = 65793
        ?pfc:FaultCode10{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode10{prop:Req} = True
        ?pfc:FaultCode10{prop:FontColor} = 65793
        ?pfc:FaultCode10{prop:Color} = 16777215
    End ! If ?pfc:FaultCode10{prop:Req} = True
    ?pfc:FaultCode10{prop:Trn} = 0
    ?pfc:FaultCode10{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    If ?pfc:FaultCode11{prop:ReadOnly} = True
        ?pfc:FaultCode11{prop:FontColor} = 65793
        ?pfc:FaultCode11{prop:Color} = 15066597
    Elsif ?pfc:FaultCode11{prop:Req} = True
        ?pfc:FaultCode11{prop:FontColor} = 65793
        ?pfc:FaultCode11{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode11{prop:Req} = True
        ?pfc:FaultCode11{prop:FontColor} = 65793
        ?pfc:FaultCode11{prop:Color} = 16777215
    End ! If ?pfc:FaultCode11{prop:Req} = True
    ?pfc:FaultCode11{prop:Trn} = 0
    ?pfc:FaultCode11{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?pfc:FaultCode4{prop:ReadOnly} = True
        ?pfc:FaultCode4{prop:FontColor} = 65793
        ?pfc:FaultCode4{prop:Color} = 15066597
    Elsif ?pfc:FaultCode4{prop:Req} = True
        ?pfc:FaultCode4{prop:FontColor} = 65793
        ?pfc:FaultCode4{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode4{prop:Req} = True
        ?pfc:FaultCode4{prop:FontColor} = 65793
        ?pfc:FaultCode4{prop:Color} = 16777215
    End ! If ?pfc:FaultCode4{prop:Req} = True
    ?pfc:FaultCode4{prop:Trn} = 0
    ?pfc:FaultCode4{prop:FontStyle} = font:Bold
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    If ?pfc:FaultCode12{prop:ReadOnly} = True
        ?pfc:FaultCode12{prop:FontColor} = 65793
        ?pfc:FaultCode12{prop:Color} = 15066597
    Elsif ?pfc:FaultCode12{prop:Req} = True
        ?pfc:FaultCode12{prop:FontColor} = 65793
        ?pfc:FaultCode12{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode12{prop:Req} = True
        ?pfc:FaultCode12{prop:FontColor} = 65793
        ?pfc:FaultCode12{prop:Color} = 16777215
    End ! If ?pfc:FaultCode12{prop:Req} = True
    ?pfc:FaultCode12{prop:Trn} = 0
    ?pfc:FaultCode12{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode5:2{prop:ReadOnly} = True
        ?pfc:FaultCode5:2{prop:FontColor} = 65793
        ?pfc:FaultCode5:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode5:2{prop:Req} = True
        ?pfc:FaultCode5:2{prop:FontColor} = 65793
        ?pfc:FaultCode5:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode5:2{prop:Req} = True
        ?pfc:FaultCode5:2{prop:FontColor} = 65793
        ?pfc:FaultCode5:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode5:2{prop:Req} = True
    ?pfc:FaultCode5:2{prop:Trn} = 0
    ?pfc:FaultCode5:2{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    If ?pfc:FaultCode6{prop:ReadOnly} = True
        ?pfc:FaultCode6{prop:FontColor} = 65793
        ?pfc:FaultCode6{prop:Color} = 15066597
    Elsif ?pfc:FaultCode6{prop:Req} = True
        ?pfc:FaultCode6{prop:FontColor} = 65793
        ?pfc:FaultCode6{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode6{prop:Req} = True
        ?pfc:FaultCode6{prop:FontColor} = 65793
        ?pfc:FaultCode6{prop:Color} = 16777215
    End ! If ?pfc:FaultCode6{prop:Req} = True
    ?pfc:FaultCode6{prop:Trn} = 0
    ?pfc:FaultCode6{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode6:2{prop:ReadOnly} = True
        ?pfc:FaultCode6:2{prop:FontColor} = 65793
        ?pfc:FaultCode6:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode6:2{prop:Req} = True
        ?pfc:FaultCode6:2{prop:FontColor} = 65793
        ?pfc:FaultCode6:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode6:2{prop:Req} = True
        ?pfc:FaultCode6:2{prop:FontColor} = 65793
        ?pfc:FaultCode6:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode6:2{prop:Req} = True
    ?pfc:FaultCode6:2{prop:Trn} = 0
    ?pfc:FaultCode6:2{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode5{prop:ReadOnly} = True
        ?pfc:FaultCode5{prop:FontColor} = 65793
        ?pfc:FaultCode5{prop:Color} = 15066597
    Elsif ?pfc:FaultCode5{prop:Req} = True
        ?pfc:FaultCode5{prop:FontColor} = 65793
        ?pfc:FaultCode5{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode5{prop:Req} = True
        ?pfc:FaultCode5{prop:FontColor} = 65793
        ?pfc:FaultCode5{prop:Color} = 16777215
    End ! If ?pfc:FaultCode5{prop:Req} = True
    ?pfc:FaultCode5{prop:Trn} = 0
    ?pfc:FaultCode5{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?pfc:FaultCode7{prop:ReadOnly} = True
        ?pfc:FaultCode7{prop:FontColor} = 65793
        ?pfc:FaultCode7{prop:Color} = 15066597
    Elsif ?pfc:FaultCode7{prop:Req} = True
        ?pfc:FaultCode7{prop:FontColor} = 65793
        ?pfc:FaultCode7{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode7{prop:Req} = True
        ?pfc:FaultCode7{prop:FontColor} = 65793
        ?pfc:FaultCode7{prop:Color} = 16777215
    End ! If ?pfc:FaultCode7{prop:Req} = True
    ?pfc:FaultCode7{prop:Trn} = 0
    ?pfc:FaultCode7{prop:FontStyle} = font:Bold
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    If ?pfc:FaultCode8{prop:ReadOnly} = True
        ?pfc:FaultCode8{prop:FontColor} = 65793
        ?pfc:FaultCode8{prop:Color} = 15066597
    Elsif ?pfc:FaultCode8{prop:Req} = True
        ?pfc:FaultCode8{prop:FontColor} = 65793
        ?pfc:FaultCode8{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode8{prop:Req} = True
        ?pfc:FaultCode8{prop:FontColor} = 65793
        ?pfc:FaultCode8{prop:Color} = 16777215
    End ! If ?pfc:FaultCode8{prop:Req} = True
    ?pfc:FaultCode8{prop:Trn} = 0
    ?pfc:FaultCode8{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode8:2{prop:ReadOnly} = True
        ?pfc:FaultCode8:2{prop:FontColor} = 65793
        ?pfc:FaultCode8:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode8:2{prop:Req} = True
        ?pfc:FaultCode8:2{prop:FontColor} = 65793
        ?pfc:FaultCode8:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode8:2{prop:Req} = True
        ?pfc:FaultCode8:2{prop:FontColor} = 65793
        ?pfc:FaultCode8:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode8:2{prop:Req} = True
    ?pfc:FaultCode8:2{prop:Trn} = 0
    ?pfc:FaultCode8:2{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode9{prop:ReadOnly} = True
        ?pfc:FaultCode9{prop:FontColor} = 65793
        ?pfc:FaultCode9{prop:Color} = 15066597
    Elsif ?pfc:FaultCode9{prop:Req} = True
        ?pfc:FaultCode9{prop:FontColor} = 65793
        ?pfc:FaultCode9{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode9{prop:Req} = True
        ?pfc:FaultCode9{prop:FontColor} = 65793
        ?pfc:FaultCode9{prop:Color} = 16777215
    End ! If ?pfc:FaultCode9{prop:Req} = True
    ?pfc:FaultCode9{prop:Trn} = 0
    ?pfc:FaultCode9{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode9:2{prop:ReadOnly} = True
        ?pfc:FaultCode9:2{prop:FontColor} = 65793
        ?pfc:FaultCode9:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode9:2{prop:Req} = True
        ?pfc:FaultCode9:2{prop:FontColor} = 65793
        ?pfc:FaultCode9:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode9:2{prop:Req} = True
        ?pfc:FaultCode9:2{prop:FontColor} = 65793
        ?pfc:FaultCode9:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode9:2{prop:Req} = True
    ?pfc:FaultCode9:2{prop:Trn} = 0
    ?pfc:FaultCode9:2{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode12:2{prop:ReadOnly} = True
        ?pfc:FaultCode12:2{prop:FontColor} = 65793
        ?pfc:FaultCode12:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode12:2{prop:Req} = True
        ?pfc:FaultCode12:2{prop:FontColor} = 65793
        ?pfc:FaultCode12:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode12:2{prop:Req} = True
        ?pfc:FaultCode12:2{prop:FontColor} = 65793
        ?pfc:FaultCode12:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode12:2{prop:Req} = True
    ?pfc:FaultCode12:2{prop:Trn} = 0
    ?pfc:FaultCode12:2{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode11:2{prop:ReadOnly} = True
        ?pfc:FaultCode11:2{prop:FontColor} = 65793
        ?pfc:FaultCode11:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode11:2{prop:Req} = True
        ?pfc:FaultCode11:2{prop:FontColor} = 65793
        ?pfc:FaultCode11:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode11:2{prop:Req} = True
        ?pfc:FaultCode11:2{prop:FontColor} = 65793
        ?pfc:FaultCode11:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode11:2{prop:Req} = True
    ?pfc:FaultCode11:2{prop:Trn} = 0
    ?pfc:FaultCode11:2{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode10:2{prop:ReadOnly} = True
        ?pfc:FaultCode10:2{prop:FontColor} = 65793
        ?pfc:FaultCode10:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode10:2{prop:Req} = True
        ?pfc:FaultCode10:2{prop:FontColor} = 65793
        ?pfc:FaultCode10:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode10:2{prop:Req} = True
        ?pfc:FaultCode10:2{prop:FontColor} = 65793
        ?pfc:FaultCode10:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode10:2{prop:Req} = True
    ?pfc:FaultCode10:2{prop:Trn} = 0
    ?pfc:FaultCode10:2{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode7:2{prop:ReadOnly} = True
        ?pfc:FaultCode7:2{prop:FontColor} = 65793
        ?pfc:FaultCode7:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode7:2{prop:Req} = True
        ?pfc:FaultCode7:2{prop:FontColor} = 65793
        ?pfc:FaultCode7:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode7:2{prop:Req} = True
        ?pfc:FaultCode7:2{prop:FontColor} = 65793
        ?pfc:FaultCode7:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode7:2{prop:Req} = True
    ?pfc:FaultCode7:2{prop:Trn} = 0
    ?pfc:FaultCode7:2{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode4:2{prop:ReadOnly} = True
        ?pfc:FaultCode4:2{prop:FontColor} = 65793
        ?pfc:FaultCode4:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode4:2{prop:Req} = True
        ?pfc:FaultCode4:2{prop:FontColor} = 65793
        ?pfc:FaultCode4:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode4:2{prop:Req} = True
        ?pfc:FaultCode4:2{prop:FontColor} = 65793
        ?pfc:FaultCode4:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode4:2{prop:Req} = True
    ?pfc:FaultCode4:2{prop:Trn} = 0
    ?pfc:FaultCode4:2{prop:FontStyle} = font:Bold
    If ?pfc:FaultCode3:2{prop:ReadOnly} = True
        ?pfc:FaultCode3:2{prop:FontColor} = 65793
        ?pfc:FaultCode3:2{prop:Color} = 15066597
    Elsif ?pfc:FaultCode3:2{prop:Req} = True
        ?pfc:FaultCode3:2{prop:FontColor} = 65793
        ?pfc:FaultCode3:2{prop:Color} = 8454143
    Else ! If ?pfc:FaultCode3:2{prop:Req} = True
        ?pfc:FaultCode3:2{prop:FontColor} = 65793
        ?pfc:FaultCode3:2{prop:Color} = 16777215
    End ! If ?pfc:FaultCode3:2{prop:Req} = True
    ?pfc:FaultCode3:2{prop:Trn} = 0
    ?pfc:FaultCode3:2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'SetPartFaultCodes',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('PartFaultCodes:FaultCode1',PartFaultCodes:FaultCode1,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode2',PartFaultCodes:FaultCode2,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode3',PartFaultCodes:FaultCode3,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode4',PartFaultCodes:FaultCode4,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode5',PartFaultCodes:FaultCode5,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode6',PartFaultCodes:FaultCode6,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode7',PartFaultCodes:FaultCode7,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode8',PartFaultCodes:FaultCode8,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode9',PartFaultCodes:FaultCode9,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode10',PartFaultCodes:FaultCode10,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode11',PartFaultCodes:FaultCode11,'SetPartFaultCodes',1)
    SolaceViewVars('PartFaultCodes:FaultCode12',PartFaultCodes:FaultCode12,'SetPartFaultCodes',1)
    SolaceViewVars('result',result,'SetPartFaultCodes',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode1;  SolaceCtrlName = '?pfc:FaultCode1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button1;  SolaceCtrlName = '?Button1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:1;  SolaceCtrlName = '?PopCalendar:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode1:2;  SolaceCtrlName = '?pfc:FaultCode1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode2;  SolaceCtrlName = '?pfc:FaultCode2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode2:2;  SolaceCtrlName = '?pfc:FaultCode2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode3;  SolaceCtrlName = '?pfc:FaultCode3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode10;  SolaceCtrlName = '?pfc:FaultCode10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode11;  SolaceCtrlName = '?pfc:FaultCode11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode4;  SolaceCtrlName = '?pfc:FaultCode4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt12;  SolaceCtrlName = '?Prompt12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode12;  SolaceCtrlName = '?pfc:FaultCode12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button5;  SolaceCtrlName = '?Button5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode5:2;  SolaceCtrlName = '?pfc:FaultCode5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode6;  SolaceCtrlName = '?pfc:FaultCode6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode6:2;  SolaceCtrlName = '?pfc:FaultCode6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button6;  SolaceCtrlName = '?Button6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode5;  SolaceCtrlName = '?pfc:FaultCode5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode7;  SolaceCtrlName = '?pfc:FaultCode7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode8;  SolaceCtrlName = '?pfc:FaultCode8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode8:2;  SolaceCtrlName = '?pfc:FaultCode8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode9;  SolaceCtrlName = '?pfc:FaultCode9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button9;  SolaceCtrlName = '?Button9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode9:2;  SolaceCtrlName = '?pfc:FaultCode9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button12;  SolaceCtrlName = '?Button12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode12:2;  SolaceCtrlName = '?pfc:FaultCode12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button11;  SolaceCtrlName = '?Button11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode11:2;  SolaceCtrlName = '?pfc:FaultCode11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button10;  SolaceCtrlName = '?Button10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode10:2;  SolaceCtrlName = '?pfc:FaultCode10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button8;  SolaceCtrlName = '?Button8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button7;  SolaceCtrlName = '?Button7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode7:2;  SolaceCtrlName = '?pfc:FaultCode7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode4:2;  SolaceCtrlName = '?pfc:FaultCode4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pfc:FaultCode3:2;  SolaceCtrlName = '?pfc:FaultCode3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button2;  SolaceCtrlName = '?Button2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OKButton;  SolaceCtrlName = '?OKButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SetPartFaultCodes')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'SetPartFaultCodes')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?CancelButton,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?pfc:FaultCode1{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode2{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode3{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode4{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode5{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode6{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode7{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode8{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode9{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode10{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode11{Prop:Alrt,255} = MouseLeft2
  ?pfc:FaultCode12{Prop:Alrt,255} = MouseLeft2
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'SetPartFaultCodes',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OKButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OKButton, Accepted)
      IF (INCOMPLETE()) THEN
          BEEP(BEEP:SystemExclamation)
         SELECT(INCOMPLETE())
      ELSE
          fc1 = pfc:FaultCode1
          fc2 = pfc:FaultCode2
          fc3 = pfc:FaultCode3
          fc4 = pfc:FaultCode4
          fc5 = pfc:FaultCode5
          fc6 = pfc:FaultCode6
          fc7 = pfc:FaultCode7
          fc8 = pfc:FaultCode8
          fc9 = pfc:FaultCode9
          fc10 = pfc:FaultCode10
          fc11 = pfc:FaultCode11
          fc12 = pfc:FaultCode12
          result = 1
          POST(Event:CloseWindow)
      END
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OKButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 1
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode1 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
    OF ?PopCalendar:1
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode1 = TINCALENDARStyle1(pfc:FaultCode1)
          Display(?pfc:FaultCode1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?pfc:FaultCode1:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode1:2, Accepted)
      IF (pfc:faultcode1 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 1
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 1
                  mfp:field        = pfc:faultcode1
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode1 = mfp:field
                      ELSE
                          pfc:faultcode1 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode1:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode1:2, Accepted)
    OF ?pfc:FaultCode2:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode2:2, Accepted)
      IF (pfc:faultcode2 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 2
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 2
                  mfp:field        = pfc:faultcode2
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode2 = mfp:field
                      ELSE
                          pfc:faultcode2 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode2:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode2:2, Accepted)
    OF ?Button5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 5
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode5 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode5 = TINCALENDARStyle1(pfc:FaultCode5)
          Display(?pfc:FaultCode5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?pfc:FaultCode5:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode5:2, Accepted)
      IF (pfc:faultcode5 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 5
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 5
                  mfp:field        = pfc:faultcode5
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode5 = mfp:field
                      ELSE
                          pfc:faultcode5 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode5:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode5:2, Accepted)
    OF ?pfc:FaultCode6:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode6:2, Accepted)
      IF (pfc:faultcode6 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 6
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 6
                  mfp:field        = pfc:faultcode6
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode6 = mfp:field
                      ELSE
                          pfc:faultcode6 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode6:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode6:2, Accepted)
    OF ?Button6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 6
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode6 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode6 = TINCALENDARStyle1(pfc:FaultCode6)
          Display(?pfc:FaultCode6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?pfc:FaultCode8:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode8:2, Accepted)
      IF (pfc:faultcode8 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 8
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 8
                  mfp:field        = pfc:faultcode8
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode8 = mfp:field
                      ELSE
                          pfc:faultcode8 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode8:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode8:2, Accepted)
    OF ?Button9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button9, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 9
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode9 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button9, Accepted)
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode9 = TINCALENDARStyle1(pfc:FaultCode9)
          Display(?pfc:FaultCode9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?pfc:FaultCode9:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode9:2, Accepted)
      IF (pfc:faultcode9 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 9
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 9
                  mfp:field        = pfc:faultcode9
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode9 = mfp:field
                      ELSE
                          pfc:faultcode9 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode9:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode9:2, Accepted)
    OF ?Button12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button12, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 12
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode12 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button12, Accepted)
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode12 = TINCALENDARStyle1(pfc:FaultCode12)
          Display(?pfc:FaultCode12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?pfc:FaultCode12:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode12:2, Accepted)
      IF (pfc:faultcode12 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 12
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 12
                  mfp:field        = pfc:faultcode12
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode12 = mfp:field
                      ELSE
                          pfc:faultcode12 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode12:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode12:2, Accepted)
    OF ?Button11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button11, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 11
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode11 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button11, Accepted)
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode11 = TINCALENDARStyle1(pfc:FaultCode11)
          Display(?pfc:FaultCode11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?pfc:FaultCode11:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode11:2, Accepted)
      IF (pfc:faultcode11 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 11
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 11
                  mfp:field        = pfc:faultcode11
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode11 = mfp:field
                      ELSE
                          pfc:faultcode11 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode11:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode11:2, Accepted)
    OF ?Button10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button10, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 10
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode10 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button10, Accepted)
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode10 = TINCALENDARStyle1(pfc:FaultCode10)
          Display(?pfc:FaultCode10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?pfc:FaultCode10:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode10:2, Accepted)
      IF (pfc:faultcode10 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 10
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 10
                  mfp:field        = pfc:faultcode10
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode10 = mfp:field
                      ELSE
                          pfc:faultcode10 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode10:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode10:2, Accepted)
    OF ?Button8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button8, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 8
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode8 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button8, Accepted)
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode8 = TINCALENDARStyle1(pfc:FaultCode8)
          Display(?pfc:FaultCode8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button7, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 7
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode7 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button7, Accepted)
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode7 = TINCALENDARStyle1(pfc:FaultCode7)
          Display(?pfc:FaultCode7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?pfc:FaultCode7:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode7:2, Accepted)
      IF (pfc:faultcode7 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 7
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 7
                  mfp:field        = pfc:faultcode7
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode7 = mfp:field
                      ELSE
                          pfc:faultcode7 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode7:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode7:2, Accepted)
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 4
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode4 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode4 = TINCALENDARStyle1(pfc:FaultCode4)
          Display(?pfc:FaultCode4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?pfc:FaultCode4:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode4:2, Accepted)
      IF (pfc:faultcode4 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 4
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 4
                  mfp:field        = pfc:faultcode4
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode4 = mfp:field
                      ELSE
                          pfc:faultcode4 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode4:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode4:2, Accepted)
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 3
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode3 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    OF ?pfc:FaultCode3:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode3:2, Accepted)
      IF (pfc:faultcode3 <> '') THEN
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 3
          IF (access:manfaupa.fetch(map:field_number_key)) THEN
              IF (map:force_lookup = 'YES') THEN
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 3
                  mfp:field        = pfc:faultcode3
                  IF (access:manfpalo.fetch(mfp:field_key)) THEN
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      IF (globalresponse = requestcompleted) THEN
                          pfc:faultcode3 = mfp:field
                      ELSE
                          pfc:faultcode3 = ''
                          select(?-1)
                      END
                      DISPLAY(?pfc:faultcode3:2)
                      globalrequest     = saverequest#
                  END
              END
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pfc:FaultCode3:2, Accepted)
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 2
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          pfc:faultcode2 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          pfc:FaultCode2 = TINCALENDARStyle1(pfc:FaultCode2)
          Display(?pfc:FaultCode2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'SetPartFaultCodes')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?pfc:FaultCode1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:1)
      CYCLE
    END
  OF ?pfc:FaultCode2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?pfc:FaultCode3
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?Panel1)
      CYCLE
    END
  OF ?pfc:FaultCode10
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?pfc:FaultCode11
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?pfc:FaultCode4
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?pfc:FaultCode12
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  OF ?pfc:FaultCode6
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  OF ?pfc:FaultCode5
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?pfc:FaultCode7
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?pfc:FaultCode8
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?pfc:FaultCode9
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      result = 0
      access:chartype.clearkey(cha:charge_type_key)
      cha:charge_type = f_charge_type
      IF ((access:chartype.fetch(cha:charge_type_key) = Level:Benign) AND (cha:force_warranty = 'YES')) THEN
          SETCURSOR(CURSOR:WAIT)
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          SET(map:field_number_key,map:field_number_key)
          LOOP
              IF ((access:manfaupa.next() <> Level:benign) OR (map:manufacturer <> job:manufacturer)) THEN
                  BREAK
              END
      
              IF (map:compulsory = 'YES') THEN
                  CASE map:field_number
                      OF 1
                          Unhide(?prompt1)
                          ?prompt1{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:1)
                              ?popcalendar:1{prop:xpos} = 212
                              Unhide(?pfc:faultcode1)
                              ?pfc:faultcode1{prop:xpos} = 84
                              ?pfc:faultcode1{prop:req} = 1
                              ?pfc:faultcode1{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode1:2)
                              If map:lookup = 'YES'
                                  Unhide(?button1)
                              End
                              ?pfc:faultcode1:2{prop:req} = 1
                              ?pfc:faultcode1:2{PROP:COLOR} = 0BDFFFFH
                          End
                      OF 2
                          Unhide(?prompt2)
                          ?prompt2{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:2)
                              ?popcalendar:2{prop:xpos} = 212
                              Unhide(?pfc:faultcode2)
                              ?pfc:faultcode2{prop:xpos} = 84
                              ?pfc:faultcode2{prop:req} = 1
                              ?pfc:faultcode2{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode2:2)
                              If map:lookup = 'YES'
                                  Unhide(?button2)
                              End
                              ?pfc:faultcode2:2{prop:req} = 1
                              ?pfc:faultcode2:2{PROP:COLOR} = 0BDFFFFH
                          End
                      OF 3
                          Unhide(?prompt3)
                          ?prompt3{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:3)
                              ?popcalendar:3{prop:xpos} = 212
                              Unhide(?pfc:faultcode3)
                              ?pfc:faultcode3{prop:xpos} = 84
                              ?pfc:faultcode3{prop:req} = 1
                              ?pfc:faultcode3{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode3:2)
                              If map:lookup = 'YES'
                                  Unhide(?button3)
                              End
                              ?pfc:faultcode3:2{prop:req} = 1
                              ?pfc:faultcode3:2{PROP:COLOR} = 0BDFFFFH
                          End
                      OF 4
                          Unhide(?prompt4)
                          ?prompt4{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:4)
                              ?popcalendar:4{prop:xpos} = 212
                              Unhide(?pfc:faultcode4)
                              ?pfc:faultcode4{prop:xpos} = 84
                              ?pfc:faultcode4{prop:req} = 1
                              ?pfc:faultcode4{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode4:2)
                              If map:lookup = 'YES'
                                  Unhide(?button4)
                              End
                              ?pfc:faultcode4:2{prop:req} = 1
                              ?pfc:faultcode4:2{PROP:COLOR} = 0BDFFFFH
                          End
                      Of 5
                          Unhide(?prompt5)
                          ?prompt5{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:5)
                              ?popcalendar:5{prop:xpos} = 212
                              Unhide(?pfc:faultcode5)
                              ?pfc:faultcode5{prop:xpos} = 84
                              ?pfc:faultcode5{prop:req} = 1
                              ?pfc:faultcode5{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode5:2)
                              If map:lookup = 'YES'
                                  Unhide(?button5)
                              End
                              ?pfc:faultcode5:2{prop:req} = 1
                              ?pfc:faultcode5:2{PROP:COLOR} = 0BDFFFFH
                          End
                      OF 6
                          Unhide(?prompt6)
                          ?prompt6{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:6)
                              ?popcalendar:6{prop:xpos} = 212
                              Unhide(?pfc:faultcode6)
                              ?pfc:faultcode6{prop:xpos} = 84
                              ?pfc:faultcode6{prop:req} = 1
                              ?pfc:faultcode6{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode6:2)
                              If map:lookup = 'YES'
                                  Unhide(?button6)
                              End
                              ?pfc:faultcode6:2{prop:req} = 1
                              ?pfc:faultcode6:2{PROP:COLOR} = 0BDFFFFH
                          End
                      OF 7
                          Unhide(?prompt7)
                          ?prompt7{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:7)
                              ?popcalendar:7{prop:xpos} = 212
                              Unhide(?pfc:faultcode7)
                              ?pfc:faultcode7{prop:xpos} = 84
                              ?pfc:faultcode7{prop:req} = 1
                              ?pfc:faultcode7{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode7:2)
                              If map:lookup = 'YES'
                                  Unhide(?button7)
                              End
                              ?pfc:faultcode7:2{prop:req} = 1
                              ?pfc:faultcode7:2{PROP:COLOR} = 0BDFFFFH
                          End
                      OF 8
                          Unhide(?prompt8)
                          ?prompt8{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:8)
                              ?popcalendar:8{prop:xpos} = 212
                              Unhide(?pfc:faultcode8)
                              ?pfc:faultcode8{prop:xpos} = 84
                              ?pfc:faultcode8{prop:req} = 1
                              ?pfc:faultcode8{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode8:2)
                              If map:lookup = 'YES'
                                  Unhide(?button8)
                              End
                              ?pfc:faultcode8:2{prop:req} = 1
                              ?pfc:faultcode8:2{PROP:COLOR} = 0BDFFFFH
                          End
                      OF 9
                          Unhide(?prompt9)
                          ?prompt9{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:9)
                              ?popcalendar:9{prop:xpos} = 212
                              Unhide(?pfc:faultcode9)
                              ?pfc:faultcode9{prop:xpos} = 84
                              ?pfc:faultcode9{prop:req} = 1
                              ?pfc:faultcode9{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode9:2)
                              If map:lookup = 'YES'
                                  Unhide(?button9)
                              End
                              ?pfc:faultcode9:2{prop:req} = 1
                              ?pfc:faultcode9:2{PROP:COLOR} = 0BDFFFFH
                          End
                      OF 10
                          Unhide(?prompt10)
                          ?prompt10{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:10)
                              ?popcalendar:10{prop:xpos} = 212
                              Unhide(?pfc:faultcode10)
                              ?pfc:faultcode10{prop:xpos} = 84
                              ?pfc:faultcode10{prop:req} = 1
                              ?pfc:faultcode10{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode10:2)
                              If map:lookup = 'YES'
                                  Unhide(?button10)
                              End
                              ?pfc:faultcode10:2{prop:req} = 1
                              ?pfc:faultcode10:2{PROP:COLOR} = 0BDFFFFH
                          End
                      OF 11
                          Unhide(?prompt11)
                          ?prompt11{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:11)
                              ?popcalendar:11{prop:xpos} = 212
                              Unhide(?pfc:faultcode11)
                              ?pfc:faultcode11{prop:xpos} = 84
                              ?pfc:faultcode11{prop:req} = 1
                              ?pfc:faultcode11{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode11:2)
                              If map:lookup = 'YES'
                                  Unhide(?button11)
                              End
                              ?pfc:faultcode11:2{prop:req} = 1
                              ?pfc:faultcode11:2{PROP:COLOR} = 0BDFFFFH
                          End
                      OF 12
                          Unhide(?prompt12)
                          ?prompt12{prop:text} = map:field_name
                          If map:field_type = 'DATE'
                              Unhide(?popcalendar:12)
                              ?popcalendar:12{prop:xpos} = 212
                              Unhide(?pfc:faultcode12)
                              ?pfc:faultcode12{prop:xpos} = 84
                              ?pfc:faultcode12{prop:req} = 1
                              ?pfc:faultcode12{PROP:COLOR} = 0BDFFFFH
                          Else
                              Unhide(?pfc:faultcode12:2)
                              If map:lookup = 'YES'
                                  Unhide(?button12)
                              End
                              ?pfc:faultcode12:2{prop:req} = 1
                              ?pfc:faultcode12:2{PROP:COLOR} = 0BDFFFFH
                          End
                  END !Case map:field_number
               END
          END !loop
          SETCURSOR()
      END
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

