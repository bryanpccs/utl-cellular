

   MEMBER('sbrsigom.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBRSI001.INC'),ONCE        !Local module procedure declarations
                     END


Date_Range PROCEDURE                                  !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_inv_id          USHORT,AUTO
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:Trade_Acc        STRING(15)
tmp:Net_Total        REAL
tmp:VAT_Total        REAL
savepath             STRING(255)
local:FileName       STRING(255),STATIC
window               WINDOW('Sigma Omicron Export'),AT(,,232,84),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,48),USE(?Sheet1),SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           PROMPT('Start Date'),AT(8,20),USE(?glo:select1:Prompt)
                           ENTRY(@d6),AT(84,20,64,10),USE(tmp:StartDate),FONT(,8,,FONT:bold),CAP
                           BUTTON,AT(152,20,10,10),USE(?PopCalendar),ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(8,36),USE(?glo:select2:Prompt)
                           ENTRY(@d6),AT(84,36,64,10),USE(tmp:EndDate),FONT(,8,,FONT:bold),UPR
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar:2),ICON('Calenda2.ico')
                         END
                       END
                       PANEL,AT(4,56,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(112,60,56,16),USE(?OkButton),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(168,60,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
OmicronExport    File,Driver('ASCII'),Pre(omexp),Name(local:FileName),Create,Bindable,Thread
Record                  Record
Line1                   String(255)
                        End
                    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?glo:select1:Prompt{prop:FontColor} = -1
    ?glo:select1:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Date_Range')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?glo:select1:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:StartDate = Deformat('1/1/1990',@d6)
  tmp:EndDate   = Today()
  Relate:INVOICE.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      savepath = path()
      local:FileName = 'OMICRON.TXT'
      If not filedialog ('Choose File',local:FileName,'TXT Files|*.TXT', |
                  file:keepdir + file:noerror + file:longname)
          !Failed
          setpath(savepath)
      else!If not filedialog
          !Found
          setpath(savepath)
      
          Error# = 0
          Remove(local:FileName)
      
          Open(OmicronExport)
          If Error()
              Create(OmicronExport)
              Open(OmicronExport)
              If Error()
                  Stop(Error())
                  Error# = 1
              End !If Error()
          End !If Error()
      
          If Error# = 0
              recordspercycle         = 25
              recordsprocessed        = 0
              percentprogress         = 0
              progress:thermometer    = 0
              thiswindow.reset(1)
              open(progresswindow)
      
              ?progress:userstring{prop:text} = 'Running...'
              ?progress:pcttext{prop:text} = '0% Completed'
      
              recordstoprocess    = Records(Invoice)
              Count# = 0
              Save_inv_ID = Access:INVOICE.SaveFile()
              Access:INVOICE.ClearKey(inv:Date_Created_Key)
              inv:Date_Created = tmp:StartDate
              Set(inv:Date_Created_Key,inv:Date_Created_Key)
              Loop
                  If Access:INVOICE.NEXT()
                     Break
                  End !If
                  If inv:Date_Created > tmp:EndDate      |
                      Then Break.  ! End If
      
                  Do GetNextRecord2
                  Do cancelcheck
                  If tmp:cancel = 1
                      Break
                  End!If tmp:cancel = 1
      
      
                  If INV:Invoice_Type = 'SIN' AND INV:InvoiceCredit = 'INV'
                      Access:JOBS.ClearKey(job:InvoiceNumberKey)
                      job:Invoice_Number = inv:Invoice_Number
                      If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
                          !Found
                          !Work Out costs!
                          ! Start Change 2997 BE(06/08/03)
                          !tmp:net_total = job:Invoice_Sub_Total
                          tmp:net_total = job:Invoice_Courier_Cost + job:Invoice_Labour_Cost + job:Invoice_Parts_Cost
                          ! End Change 2997 BE(06/08/03)
      
                          !Exclude Zero Value Invoices
                          If tmp:net_total = 0
                              Cycle
                          End !If tmp:net_total = 0
      
                          !Need to get vat costs!
                          tmp:vat_total = ((job:Invoice_Courier_Cost * inv:Vat_Rate_Labour)/100) + ((job:Invoice_Labour_Cost * inv:Vat_Rate_Labour)/100) + ((job:Invoice_Parts_Cost * inv:Vat_Rate_Parts)/100)
                          Count# += 1
                          ?progress:userstring{prop:text} = 'Records Found: ' & Count#
      
                          AccountNumber" = inv:Account_Number
      
                          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                          sub:Account_Number  = job:Account_Number
                          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                              !Found
                              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                              tra:Account_Number  = sub:Main_Account_Number
                              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                  !Found
                                  If tra:Invoice_Sub_Accounts <> 'YES' and tra:Use_Sub_Accounts = 'YES'
                                      AccountNumber"  = tra:Account_Number
                                  End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                  !Error
                              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                              !Error
                          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                          Clear(omexp:Record)
                          ! Start Change 2997 BE(06/08/03)
                          !omexp:Line1 = '"' & Format(AccountNumber",@s10) & '","S' & |
                          !                Format(inv:Invoice_Number,@n09) & '","' & |
                          !                Format(inv:Date_Created,@d06) & '","' & |
                          !                Format(tmp:Net_Total,@n15.2) & '","' & |
                          !                Format(tmp:Vat_Total,@n15.2) & '"'
                          omexp:Line1 = '"' & Format(AccountNumber",@s10) & '","S1' & |
                                          Format(inv:Invoice_Number,@n08) & '","' & |
                                          Format(inv:Date_Created,@d06) & '","' & |
                                          Format(tmp:Net_Total,@n15.2) & '","' & |
                                          Format(tmp:Vat_Total,@n15.2) & '"'
                          ! End Change 2997 BE(06/08/03)
                          Add(OmicronExport)
                      Else!If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
      
                  End !If INV:Invoice_Type = 'SIN' AND INV:InvoiceCredit = 'INV'
              End !Loop
              Access:INVOICE.RestoreFile(Save_inv_ID)
      
              Do EndPrintRun
              close(progresswindow)
      
              Close(OmicronExport)
              Case MessageEx('Export Complete.','ServiceBase 2000',|
                             'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End !If Error# = 0
      End!If not filedialog
      
    OF ?CancelButton
      ThisWindow.Update
      glo:select1 = ''
      glo:select2 = ''
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

