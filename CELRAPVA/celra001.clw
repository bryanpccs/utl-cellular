

   MEMBER('celrapva.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA004.INC'),ONCE        !Req'd for module callout resolution
                     END


AdvancedExchangedValidation PROCEDURE                 !Generated from procedure template - Window

FilesOpened          BYTE
Local_Yes            STRING('YES')
save_xch_id          USHORT,AUTO
save_lac_id          USHORT,AUTO
job_queue_temp       QUEUE,PRE(jobque)
job_number           LONG
record_number        LONG
                     END
error_temp           BYTE(1)
Job_Number_Temp      LONG
serial_number_temp   STRING(20)
model_details_temp   STRING(60)
handset_type_temp    STRING(30)
date_booked_temp     DATE
save_jac_id          USHORT,AUTO
update_text_temp     STRING(100)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
status_temp          STRING(30)
FieldsGroup          GROUP,PRE()
tmp:ModelNumber      STRING(30)
tmp:JobNumber        LONG
tmp:IMEINumber       STRING(30)
                     END
tmp:NewStatus        STRING(30)
tmp:Workshop         STRING('NO {1}')
tmp:Location         STRING(30)
window               WINDOW('Job Validation'),AT(,,367,172),FONT('Tahoma',8,,),CENTER,ALRT(F10Key),GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,232,136),USE(?Sheet1),SPREAD
                         TAB('Rapid Advanced Exchanges Validation'),USE(?Tab1)
                           PROMPT('Job Number'),AT(12,20),USE(?Job_Number_Temp:Prompt),TRN
                           ENTRY(@s8),AT(84,20,68,10),USE(tmp:JobNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Unit Serial Number'),AT(12,36),USE(?serial_number_temp:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(tmp:IMEINumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Model Number'),AT(12,52),USE(?tmp:ModelNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(tmp:ModelNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Model Number'),TIP('Model Number'),REQ,UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupModelNumber),SKIP,ICON('List3.ico')
                           GROUP('Unit Details'),AT(8,64,220,60),USE(?Group1),DISABLE,BOXED
                             PROMPT('New Status'),AT(12,76),USE(?tmp:NewStatus:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(84,76,124,10),USE(tmp:NewStatus),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('New Status'),TIP('New Status'),UPR
                             BUTTON,AT(212,76,10,10),USE(?LookupNewStatus),SKIP,ICON('List3.ico')
                             CHECK('Workshop'),AT(84,92),USE(tmp:Workshop),MSG('Workshop'),TIP('Workshop'),VALUE('YES','NO')
                             PROMPT('Location'),AT(12,108),USE(?tmp:Location:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(84,108,124,10),USE(tmp:Location),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),UPR
                             BUTTON,AT(212,108,10,10),USE(?LookupLocation),SKIP,ICON('List3.ico')
                           END
                           STRING(@s100),AT(8,128,224,8),USE(update_text_temp),FONT(,9,,FONT:bold)
                         END
                       END
                       SHEET,AT(240,4,124,136),USE(?Sheet2),SPREAD
                         TAB('Jobs Successfully Updated'),USE(?Tab2)
                           LIST,AT(244,20,116,116),USE(?List1),VSCROLL,FORMAT('56L(2)|M~Job Number~@s8@'),FROM(job_queue_temp)
                         END
                       END
                       PANEL,AT(4,144,360,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Finish [ESC]'),AT(304,148,56,16),USE(?Close),LEFT,ICON('thumbs.gif'),STD(STD:Close)
                       BUTTON('Update Job [F10]'),AT(8,148,56,16),USE(?UpdateJob),DISABLE,LEFT,ICON('FauCode.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
look:tmp:ModelNumber                Like(tmp:ModelNumber)
look:tmp:NewStatus                Like(tmp:NewStatus)
look:tmp:Location                Like(tmp:Location)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Job_Number_Temp:Prompt{prop:FontColor} = -1
    ?Job_Number_Temp:Prompt{prop:Color} = 15066597
    If ?tmp:JobNumber{prop:ReadOnly} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 15066597
    Elsif ?tmp:JobNumber{prop:Req} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 8454143
    Else ! If ?tmp:JobNumber{prop:Req} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 16777215
    End ! If ?tmp:JobNumber{prop:Req} = True
    ?tmp:JobNumber{prop:Trn} = 0
    ?tmp:JobNumber{prop:FontStyle} = font:Bold
    ?serial_number_temp:Prompt{prop:FontColor} = -1
    ?serial_number_temp:Prompt{prop:Color} = 15066597
    If ?tmp:IMEINumber{prop:ReadOnly} = True
        ?tmp:IMEINumber{prop:FontColor} = 65793
        ?tmp:IMEINumber{prop:Color} = 15066597
    Elsif ?tmp:IMEINumber{prop:Req} = True
        ?tmp:IMEINumber{prop:FontColor} = 65793
        ?tmp:IMEINumber{prop:Color} = 8454143
    Else ! If ?tmp:IMEINumber{prop:Req} = True
        ?tmp:IMEINumber{prop:FontColor} = 65793
        ?tmp:IMEINumber{prop:Color} = 16777215
    End ! If ?tmp:IMEINumber{prop:Req} = True
    ?tmp:IMEINumber{prop:Trn} = 0
    ?tmp:IMEINumber{prop:FontStyle} = font:Bold
    ?tmp:ModelNumber:Prompt{prop:FontColor} = -1
    ?tmp:ModelNumber:Prompt{prop:Color} = 15066597
    If ?tmp:ModelNumber{prop:ReadOnly} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 15066597
    Elsif ?tmp:ModelNumber{prop:Req} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 8454143
    Else ! If ?tmp:ModelNumber{prop:Req} = True
        ?tmp:ModelNumber{prop:FontColor} = 65793
        ?tmp:ModelNumber{prop:Color} = 16777215
    End ! If ?tmp:ModelNumber{prop:Req} = True
    ?tmp:ModelNumber{prop:Trn} = 0
    ?tmp:ModelNumber{prop:FontStyle} = font:Bold
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?tmp:NewStatus:Prompt{prop:FontColor} = -1
    ?tmp:NewStatus:Prompt{prop:Color} = 15066597
    If ?tmp:NewStatus{prop:ReadOnly} = True
        ?tmp:NewStatus{prop:FontColor} = 65793
        ?tmp:NewStatus{prop:Color} = 15066597
    Elsif ?tmp:NewStatus{prop:Req} = True
        ?tmp:NewStatus{prop:FontColor} = 65793
        ?tmp:NewStatus{prop:Color} = 8454143
    Else ! If ?tmp:NewStatus{prop:Req} = True
        ?tmp:NewStatus{prop:FontColor} = 65793
        ?tmp:NewStatus{prop:Color} = 16777215
    End ! If ?tmp:NewStatus{prop:Req} = True
    ?tmp:NewStatus{prop:Trn} = 0
    ?tmp:NewStatus{prop:FontStyle} = font:Bold
    ?tmp:Workshop{prop:Font,3} = -1
    ?tmp:Workshop{prop:Color} = 15066597
    ?tmp:Workshop{prop:Trn} = 0
    ?tmp:Location:Prompt{prop:FontColor} = -1
    ?tmp:Location:Prompt{prop:Color} = 15066597
    If ?tmp:Location{prop:ReadOnly} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 15066597
    Elsif ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 8454143
    Else ! If ?tmp:Location{prop:Req} = True
        ?tmp:Location{prop:FontColor} = 65793
        ?tmp:Location{prop:Color} = 16777215
    End ! If ?tmp:Location{prop:Req} = True
    ?tmp:Location{prop:Trn} = 0
    ?tmp:Location{prop:FontStyle} = font:Bold
    ?update_text_temp{prop:FontColor} = -1
    ?update_text_temp{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AdvancedExchangedValidation',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'AdvancedExchangedValidation',1)
    SolaceViewVars('Local_Yes',Local_Yes,'AdvancedExchangedValidation',1)
    SolaceViewVars('save_xch_id',save_xch_id,'AdvancedExchangedValidation',1)
    SolaceViewVars('save_lac_id',save_lac_id,'AdvancedExchangedValidation',1)
    SolaceViewVars('job_queue_temp:job_number',job_queue_temp:job_number,'AdvancedExchangedValidation',1)
    SolaceViewVars('job_queue_temp:record_number',job_queue_temp:record_number,'AdvancedExchangedValidation',1)
    SolaceViewVars('error_temp',error_temp,'AdvancedExchangedValidation',1)
    SolaceViewVars('Job_Number_Temp',Job_Number_Temp,'AdvancedExchangedValidation',1)
    SolaceViewVars('serial_number_temp',serial_number_temp,'AdvancedExchangedValidation',1)
    SolaceViewVars('model_details_temp',model_details_temp,'AdvancedExchangedValidation',1)
    SolaceViewVars('handset_type_temp',handset_type_temp,'AdvancedExchangedValidation',1)
    SolaceViewVars('date_booked_temp',date_booked_temp,'AdvancedExchangedValidation',1)
    SolaceViewVars('save_jac_id',save_jac_id,'AdvancedExchangedValidation',1)
    SolaceViewVars('update_text_temp',update_text_temp,'AdvancedExchangedValidation',1)
    SolaceViewVars('status_temp',status_temp,'AdvancedExchangedValidation',1)
    SolaceViewVars('FieldsGroup:tmp:ModelNumber',FieldsGroup:tmp:ModelNumber,'AdvancedExchangedValidation',1)
    SolaceViewVars('FieldsGroup:tmp:JobNumber',FieldsGroup:tmp:JobNumber,'AdvancedExchangedValidation',1)
    SolaceViewVars('FieldsGroup:tmp:IMEINumber',FieldsGroup:tmp:IMEINumber,'AdvancedExchangedValidation',1)
    SolaceViewVars('tmp:NewStatus',tmp:NewStatus,'AdvancedExchangedValidation',1)
    SolaceViewVars('tmp:Workshop',tmp:Workshop,'AdvancedExchangedValidation',1)
    SolaceViewVars('tmp:Location',tmp:Location,'AdvancedExchangedValidation',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job_Number_Temp:Prompt;  SolaceCtrlName = '?Job_Number_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobNumber;  SolaceCtrlName = '?tmp:JobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?serial_number_temp:Prompt;  SolaceCtrlName = '?serial_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:IMEINumber;  SolaceCtrlName = '?tmp:IMEINumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber:Prompt;  SolaceCtrlName = '?tmp:ModelNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ModelNumber;  SolaceCtrlName = '?tmp:ModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupModelNumber;  SolaceCtrlName = '?LookupModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:NewStatus:Prompt;  SolaceCtrlName = '?tmp:NewStatus:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:NewStatus;  SolaceCtrlName = '?tmp:NewStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupNewStatus;  SolaceCtrlName = '?LookupNewStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Workshop;  SolaceCtrlName = '?tmp:Workshop';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location:Prompt;  SolaceCtrlName = '?tmp:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location;  SolaceCtrlName = '?tmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?update_text_temp;  SolaceCtrlName = '?update_text_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List1;  SolaceCtrlName = '?List1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UpdateJob;  SolaceCtrlName = '?UpdateJob';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AdvancedExchangedValidation')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'AdvancedExchangedValidation')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Job_Number_Temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Clear(job_queue_temp)
  Free(job_queue_temp)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:LOAN.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:COURIER.UseFile
  Access:LOANACC.UseFile
  Access:MODELNUM.UseFile
  Access:LOCINTER.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  IF ?tmp:ModelNumber{Prop:Tip} AND ~?LookupModelNumber{Prop:Tip}
     ?LookupModelNumber{Prop:Tip} = 'Select ' & ?tmp:ModelNumber{Prop:Tip}
  END
  IF ?tmp:ModelNumber{Prop:Msg} AND ~?LookupModelNumber{Prop:Msg}
     ?LookupModelNumber{Prop:Msg} = 'Select ' & ?tmp:ModelNumber{Prop:Msg}
  END
  IF ?tmp:NewStatus{Prop:Tip} AND ~?LookupNewStatus{Prop:Tip}
     ?LookupNewStatus{Prop:Tip} = 'Select ' & ?tmp:NewStatus{Prop:Tip}
  END
  IF ?tmp:NewStatus{Prop:Msg} AND ~?LookupNewStatus{Prop:Msg}
     ?LookupNewStatus{Prop:Msg} = 'Select ' & ?tmp:NewStatus{Prop:Msg}
  END
  IF ?tmp:Location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?tmp:Location{Prop:Tip}
  END
  IF ?tmp:Location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?tmp:Location{Prop:Msg}
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'AdvancedExchangedValidation',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Model_Numbers
      Browse_Status
      Browse_Locations
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Close
      POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:ModelNumber
      IF tmp:ModelNumber OR ?tmp:ModelNumber{Prop:Req}
        mod:Model_Number = tmp:ModelNumber
        !Save Lookup Field Incase Of error
        look:tmp:ModelNumber        = tmp:ModelNumber
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:ModelNumber = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            tmp:ModelNumber = look:tmp:ModelNumber
            SELECT(?tmp:ModelNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      !Validate
      Error# = 0
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = tmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          If job:ESN <> tmp:IMEINumber
              Case MessageEx('The entered I.M.E.I. Number does not match that of the selected Job Number.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Error# = 1
          Else !If job:ESN <> tmp:IMEINumber
              If job:Model_Number <> tmp:ModelNumber
                  Case MessageEx('The entered Model Number does not match that of the selected Job Number.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Error# = 1
              Else !If job:Model_Number <> tmp:ModelNumber
                  If AccessoryCheck('JOB')
                      Error# = 1
                  Else !If AccessoryCheck('JOB')
                      tmp:NewStatus   = job:Current_Status
                      tmp:Workshop    = job:Workshop
                      tmp:Location    = job:Location
                      Display()
                      ?UpdateJob{prop:Disable} = 0
                      ?Group1{prop:Disable} = 0
                      Select(?tmp:NewStatus)
                  End !If AccessoryCheck('JOB')
              End !If job:Model_Number <> tmp:ModelNumber
          End !If job:ESN <> tmp:IMEINumber
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          Case MessageEx('Unable to find the selected Job Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Error# = 1
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      If Error# = 1
          Clear(FieldsGroup)
          Select(?tmp:JobNumber)
      End !Error# = 1
    OF ?LookupModelNumber
      ThisWindow.Update
      mod:Model_Number = tmp:ModelNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:ModelNumber = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?tmp:ModelNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ModelNumber)
    OF ?tmp:NewStatus
      IF tmp:NewStatus OR ?tmp:NewStatus{Prop:Req}
        sts:Status = tmp:NewStatus
        sts:Job = 'YES'
        GLO:Select1 = 'JOB'
        !Save Lookup Field Incase Of error
        look:tmp:NewStatus        = tmp:NewStatus
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:NewStatus = sts:Status
          ELSE
            CLEAR(sts:Job)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            tmp:NewStatus = look:tmp:NewStatus
            SELECT(?tmp:NewStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupNewStatus
      ThisWindow.Update
      sts:Status = tmp:NewStatus
      GLO:Select1 = 'JOB'
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:NewStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:NewStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:NewStatus)
    OF ?tmp:Location
      IF tmp:Location OR ?tmp:Location{Prop:Req}
        loi:Location = tmp:Location
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:Location        = tmp:Location
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:Location = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            tmp:Location = look:tmp:Location
            SELECT(?tmp:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = tmp:Location
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:Location = loi:Location
          Select(?+1)
      ELSE
          Select(?tmp:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Location)
    OF ?UpdateJob
      ThisWindow.Update
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = tmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          job:Location    = tmp:Location
          job:Workshop    = tmp:Workshop
          If tmp:NewStatus <> job:Current_Status
              GetStatus(Sub(tmp:NewStatus,1,3),1,'JOB')
          End !If tmp:Status <> job:Current_Status
      
          If Access:JOBS.Update() = Level:Benign
              If Access:AUDIT.PrimeRecord() = Level:Benign
                  aud:Notes         = 'STATUS: ' & Clip(tmp:NewStatus) & |
                                      '<13,10>WORKSHOP: ' & Clip(tmp:Workshop) &|
                                      '<13,10>LOCATION: ' & Clip(tmp:Location)
                  aud:Ref_Number    = job:ref_number
                  aud:Date          = Today()
                  aud:Time          = Clock()
                  aud:Type          = 'JOB'
                  Access:USERS.ClearKey(use:Password_Key)
                  use:Password      = glo:Password
                  Access:USERS.Fetch(use:Password_Key)
                  aud:User          = use:User_Code
                  aud:Action        = 'ADVANCE EXCHANGE VALIDATION'  !A Brief Description UPPERCASE
                  Access:AUDIT.Insert()
              End!If Access:AUDIT.PrimeRecord() = Level:Benign
      
          End !If Access:JOBS.Update() = Level:Benign
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      ?Group1{prop:Disable} = 1
      ?UpdateJob{prop:Disable} = 1
      
      beep(beep:systemasterisk)
      jobque:job_number = tmp:JobNumber
      jobque:record_number += 1
      Add(job_queue_temp)
      Sort(job_queue_temp,-jobque:record_number)
      
      update_text_temp = 'Job Number: ' & Clip(tmp:JobNumber) & ' Updated Successfully'
      ?update_text_temp{prop:fontcolor} = color:navy
      
      Clear(FieldsGroup)
      tmp:NewStatus   = ''
      tmp:Workshop    = 'NO'
      tmp:Location    = ''
      Select(tmp:JobNumber)
      
      Display()
      
      !If Access:AUDIT.PrimeRecord() = Level:Benign
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'AdvancedExchangedValidation')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F10Key
              If ?UpdateJob{prop:Disable} = 0
                  Post(Event:Accepted,?UpdateJob)
              End !If ?UpdateJob{prop:Disable} = 0
      End !KeyCode()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

