

   MEMBER('sba04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA04010.INC'),ONCE        !Local module procedure declarations
                     END


DespatchUPSAlt       PROCEDURE  (func:Multiple,func:AccountNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
UPSFile     File,Driver('BASIC','/ALWAYSQUOTE=off'),Name(local:FileName),Pre(upsfil),Create,Bindable,Thread
Record          Record
CompanyName         String(35)
ContactName         String(35)
Address1            String(35)
Address2            String(35)
Address3            String(35)
City                String(35)
Country             String(35)
Postcode            String(9)
Telephone           String(15)
Description         String(35)
Ref1                String(35)
Ref2                String(35)
Legs                String(1)
                End
            End
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'DespatchUPSAlt')      !Add Procedure to Log
  end


    Open(UPSFILE)

    dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
    If func:Multiple
        dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
        upsfil:Description   = '.'
        upsfil:Ref1         = 'DB' & StripPoint(Clip(dbt:batch_number))
        upsfil:Ref2         = '.'
    Else
        upsfil:Description = 'J' & StripPoint(Clip(job:ref_number))
        upsfil:Ref1        = 'DB' & StripPoint(Clip(dbt:batch_number))
        upsfil:Ref2        = Clip(StripComma(job:order_number))
    End
    If func:Multiple
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = func:AccountNumber
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key) 
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                if tra:use_sub_accounts = 'YES'
                    upsfil:ContactName  = Left(StripComma(sub:contact_name))
                    upsfil:CompanyName  = Left(StripComma(sub:company_name))
                    upsfil:Address1     = Left(StripComma(sub:address_line1))
                    upsfil:Address2     = Left(StripComma(sub:address_line2))
                    upsfil:Address3     = Left(StripComma(sub:address_line3))
                    upsfil:City         = Left(StripComma(sub:address_line3))
                    upsfil:Country      = 'United Kingdom'
                    upsfil:Postcode = Left(StripComma(sub:postcode))
                    upsfil:Telephone    = Left(StripComma(sub:Telephone_Number))
                else!if tra:use_sub_accounts = 'YES'
                    upsfil:Contactname  = Left(StripComma(tra:contact_name))
                    upsfil:CompanyName  = Left(StripComma(tra:company_name))
                    upsfil:Address1     = Left(StripComma(tra:address_line1))
                    upsfil:Address2     = Left(StripComma(tra:address_line2))
                    upsfil:Address3     = Left(StripComma(tra:address_line3))
                    upsfil:City         = Left(StripComma(tra:address_line3))
                    upsfil:Country      = 'United Kingdom'
                    upsfil:Postcode     = Left(StripComma(tra:postcode))
                    upsfil:Telephone    = Left(StripComma(tra:Telephone_Number))
                end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
    Else!If func:Multiple = 'YES'
        upsfil:ContactName  = Left(StripComma(Clip(job:title)) & ' ' & Clip(StripComma(job:initial)) & ' ' & Clip(StripComma(job:surname)))
        upsfil:CompanyName  = Left(StripComma(job:company_name_delivery))
        upsfil:Address1     = Left(StripComma(job:address_line1_delivery))
        upsfil:Address2     = Left(StripComma(job:address_line2_delivery))
        upsfil:Address3     = Left(StripComma(job:address_line3_delivery))
        upsfil:City         = Left(StripComma(job:address_line3_delivery))
        upsfil:Country      = 'United Kingdom'
        upsfil:Postcode     = Left(StripComma(job:postcode_delivery))
        upsfil:Telephone    = Left(StripComma(job:telephone_delivery))
    End!If func:Multiple = 'YES'
    If job:Despatch_Type = 'EXC' or job:Despatch_Type = 'LOA'
        upsfil:Legs = '3'
    Else!If job:Despatch_Type = 'EXC' or job:Desaptch_Type = 'LOA'
        upsfil:Legs = '1'
    End!If job:Despatch_Type = 'EXC' or job:Desaptch_Type = 'LOA'
    If upsfil:CompanyName = ''
        upsfil:CompanyName = '.'
    End!If upsfil:CompanyName = ''
    Close(UPSFILE)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchUPSAlt',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
