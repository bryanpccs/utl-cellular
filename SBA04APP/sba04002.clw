

   MEMBER('sba04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA04002.INC'),ONCE        !Local module procedure declarations
                     END


Despatch             PROCEDURE  (func:Type,func:ConsignmentNumber,func:Multiple) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:PrintDespatch    BYTE(0)
tmp:PrintSummaryDespatch BYTE(0)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Despatch')      !Add Procedure to Log
  end


    Case func:Type
        Of 'JOB'
            job:consignment_number  = func:ConsignmentNumber
            Access:COURIER.ClearKey(cou:Courier_Key)
            cou:Courier = job:Courier
            If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
              !Found one!
            END
            If Today() %7 = 6
                thedate#    = Today() + 2
            Else
                ! Start Change 3813 BE(19/01/04)
                !IF cou:Last_Despatch_Time <> '' AND cou:Courier_Type = 'TOTE'
                IF ((cou:Last_Despatch_Time <> '') AND |
                    ((cou:Courier_Type = 'TOTE') OR (cou:Courier_Type = 'TOTE PARCELINE'))) THEN
                ! End Change 3813 BE(19/01/04)
                  IF CLOCK() > cou:Last_Despatch_Time
                    thedate# = TODAY()+1
                    If thedate# %7 = 6
                       thedate# = thedate# + 2
                    End
                  ELSE
                    thedate#    = Today()
                  END
                ELSE
                  thedate#    = Today()
                END
            End!If Today() %7 = 6            
            job:date_despatched = thedate#
            job:despatch_number = dbt:Batch_Number
            job:despatched  = 'YES'
            access:users.clearkey(use:password_key)
            use:password    = glo:password
            access:users.fetch(use:password_key)
            job:despatch_user   = use:user_code

            Access:COURIER.ClearKey(cou:Courier_Key)
            cou:Courier = job:Courier
            If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                !Start - Allow for "Customer Collection" courier, not having the status filled in - TrkBs: 4982 (DBH: 08-12-2004)
                StatusChanged# = False
                If cou:CustomerCollection = 1
                    If cou:NewStatus <> ''
                        GetStatus(Sub(cou:NewStatus,1,3),1,'JOB')
                        StatusChanged# = True
                    End ! If cou:NewStatus <> ''
                End ! If cou:CustomerCollection = 1

                If StatusChanged# = False
                    If job:Paid = 'YES'            
                        GetStatus(910,1,'JOB') !Despatch Paid
                    Else!If job:Paid = 'YES'
                        GetStatus(905,1,'JOB') !Despatch Unpaid
                    End!If job:Paid = 'YES'
                End ! If StatusChanged# = False
                !End   - Allow for "Customer Collection" courier, not having the status filled in - TrkBs: 4982 (DBH: 08-12-2004)

            End!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign

            If def:RemoveWorkshopDespatch = 1        
                job:Workshop = 'NO'
                job:Location = 'DESPATCHED'
            End!If def:RemoveWorkshopDespatch = 1
                
            access:jobs.update()

            get(audit,0)
            if access:audit.primerecord() = level:benign
                aud:notes         = 'CONSIGNMENT NUMBER: ' & Clip(job:Consignment_number)
                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                aud:type          = 'JOB'
                access:users.clearkey(use:password_key)
                use:password = glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'JOB DESPATCHED VIA ' & Clip(job:courier)
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign

            !Return Location
            access:locinter.clearkey(loi:location_key)
            loi:location    = job:location
            If access:locinter.fetch(loi:location_key) = Level:Benign
                If loi:allocate_spaces  = 'YES'
                    loi:current_spaces  += 1
                    loi:location_available  = 'YES'
                    access:locinter.update()
                End!If loi:allocate_spaces  = 'YES'
            End!If access:locinter.fetch(loi:location_key) = Level:Benign

            tmp:PrintDespatch = 0
!            If ~func:Multiple
            If InvoiceSubAccounts(job:Account_Number)

            End !If InvoiceSubAccounts(job:Account_Number)
                If tra:Use_Sub_Accounts = 'YES'
                    If sub:Print_Despatch_Despatch = 'YES'
                        If sub:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If sub:Despatch_Note_Per_Item = 'YES'
                        If sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If sub:Print_Despatch_Despatch = 'YES'
                    
                Else !If tra:Use_Sub_Accounts = 'YES'
                    If tra:Print_Despatch_Despatch = 'YES'
                        If tra:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If tra:Despatch_Note_Per_Item = 'YES'
                        If tra:Summary_Despatch_Notes = 'YES' And tra:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If tra:Print_Despatch_Despatch = 'YES'
                End !If tra:Use_Sub_Accounts = 'YES'
!            End !If ~func:Multiple

            If tmp:PrintDespatch
                glo:select1  = job:ref_number
                If cou:CustomerCollection = 1
                    glo:Select2 = cou:NoOfDespatchNotes
                Else!If cou:CustomerCollection = 1
                    glo:Select2 = 1
                End!If cou:CustomerCollection = 1
                Despatch_Note
                glo:select1 = ''
                glo:Select2 = ''
            End!If print_despatch# = 1

            If tmp:PrintSummaryDespatch
                glo:select1  = job:Account_Number
                glo:select2  = func:ConsignmentNumber
                glo:select3  = dbt:batch_number
                glo:select4  = job:Courier
                SingleSummaryDespatchNote(job:Ref_Number,func:Type)
                glo:Select1 = ''
                glo:Select2 = ''
                glo:Select3 = ''
                glo:Select4 = ''
            End !If tmp:PrintSummaryDespatch
        Of 'EXC'

            job:exchange_consignment_number    = func:ConsignmentNumber
            job:exchange_despatched    = Today()
            job:exchange_despatch_number    = dbt:batch_number    
            job:despatched    = ''
            access:users.clearkey(use:password_key)
            use:password    = glo:password
            access:users.tryfetch(use:password_key)
            job:exchange_despatched_user    = use:user_code
            If job:workshop    <> 'YES' and job:third_party_site = ''
                GetStatus(116,1,'JOB')
            End!If job:workshop    <> 'YES'
            GetStatus(901,1,'EXC')

            access:jobs.update()    
            get(audit,0)
            if access:audit.primerecord() = level:benign
                aud:notes         = 'CONSIGNMENT NOTE NUMBER: ' & Clip(job:exchange_consignment_number)
                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                aud:type          = 'EXC'
                access:users.clearkey(use:password_key)
                use:password = glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'EXCHANGE UNIT DESPATCHED VIA ' & CLip(job:exchange_courier)
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign

            access:exchange.clearkey(xch:ref_number_key)
            xch:ref_number    = job:exchange_unit_number
            If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                xch:available    = 'DES'
                access:exchange.update()
            
                get(exchhist,0)
                if access:exchhist.primerecord() = level:benign
                    exh:ref_number   = xch:ref_number
                    exh:date          = today()
                    exh:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    exh:user = use:user_code
                    exh:status        = 'UNIT DESPATCHED ON JOB: ' & Clip(job:ref_number)
                    access:exchhist.insert()
                end!if access:exchhist.primerecord() = level:benign
            End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign

            tmp:PrintDespatch = 0
!            If ~func:Multiple
            If InvoiceSubAccounts(job:Account_Number)

            End !If InvoiceSubAccounts(job:Account_Number)

                If tra:Use_Sub_Accounts = 'YES'
                    If sub:Print_Despatch_Despatch = 'YES'
                        If sub:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If sub:Despatch_Note_Per_Item = 'YES'
                        If sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If sub:Print_Despatch_Despatch = 'YES'
                    
                Else !If tra:Use_Sub_Accounts = 'YES'
                    If tra:Print_Despatch_Despatch = 'YES'
                        If tra:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If tra:Despatch_Note_Per_Item = 'YES'
                        If tra:Summary_Despatch_Notes = 'YES' And tra:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If tra:Print_Despatch_Despatch = 'YES'
                End !If tra:Use_Sub_Accounts = 'YES'
!            End !If ~func:Multiple
            If tmp:PrintDespatch
                glo:select1  = job:ref_number
                Despatch_Note
                glo:select1  = ''
            End!If print_despatch# = 1

            If tmp:PrintSummaryDespatch
                glo:select1  = job:Account_Number
                glo:select2  = func:ConsignmentNumber
                glo:select3  = dbt:batch_number
                glo:select4  = job:Exchange_Courier
                SingleSummaryDespatchNote(job:Ref_Number,func:Type)
                glo:Select1 = ''
                glo:Select2 = ''
                glo:Select3 = ''
                glo:Select4 = ''
            End !If tmp:PrintSummaryDespatch

        Of 'LOA'
            job:loan_consignment_number    = func:ConsignmentNumber
            job:loan_despatched    = Today()
            job:loan_despatch_number    = dbt:batch_number
            job:despatched    = ''
            access:users.clearkey(use:password_key)
            use:password    = glo:password
            access:users.tryfetch(use:password_key)
            job:loan_despatched_user    = use:user_code
            If job:workshop <> 'YES' and job:third_party_site    = ''        
                GetStatus(117,1,'JOB')

            End!If job:workshop <> 'YES' and job:third_party_site    = ''
            GetStatus(901,1,'LOA')

            access:jobs.update()    
            get(audit,0)
            if access:audit.primerecord() = level:benign
                aud:notes         = 'CONSIGNMENT NOTE NUMBER: ' & Clip(job:loan_consignment_number)
                aud:ref_number    = job:ref_number
                aud:date          = today()
                aud:time          = clock()
                aud:type          = 'LOA'
                access:users.clearkey(use:password_key)
                use:password = glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'LOAN UNIT DESPATCHED VIA ' & CLip(job:loan_courier)
                access:audit.insert()
            end!�if access:audit.primerecord() = level:benign
            
            access:loan.clearkey(loa:ref_number_key)
            loa:ref_number    = job:loan_unit_number
            If access:loan.tryfetch(loa:ref_number_key)    = Level:Benign
                loa:available    = 'DES'
                access:loan.update()
                get(loanhist,0)
                if access:loanhist.primerecord() = level:benign
                    loh:ref_number    = loa:ref_number
                    loh:date          = today()
                    loh:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    loh:user = use:user_code
                    loh:status        = 'UNIT DESPATCHED ON JOB: ' & CLip(job:ref_number)
                    access:loanhist.insert()
                end!if access:loanhist.primerecord() = level:benign
            
            End!    If access:loan.tryfetch(loa:ref_number_key)    = Level:Benign

            tmp:PrintDespatch = 0
!            If ~func:Multiple
            If InvoiceSubAccounts(job:Account_Number)

            End !If InvoiceSubAccounts(job:Account_Number)

                If tra:Use_Sub_Accounts = 'YES'
                    If sub:Print_Despatch_Despatch = 'YES'
                        If sub:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If sub:Despatch_Note_Per_Item = 'YES'
                        If sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If sub:Print_Despatch_Despatch = 'YES'
                    
                Else !If tra:Use_Sub_Accounts = 'YES'
                    If tra:Print_Despatch_Despatch = 'YES'
                        If tra:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If tra:Despatch_Note_Per_Item = 'YES'
                        If tra:Summary_Despatch_Notes = 'YES' And tra:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If tra:Print_Despatch_Despatch = 'YES'
                End !If tra:Use_Sub_Accounts = 'YES'
!            End !If ~func:Multiple
            If tmp:PrintDespatch
                glo:select1  = job:ref_number
                Despatch_Note
                glo:select1  = ''
            End!If print_despatch# = 1

            If tmp:PrintSummaryDespatch
                glo:select1  = job:Account_Number
                glo:select2  = func:ConsignmentNumber
                glo:select3  = dbt:batch_number
                glo:select4  = job:Loan_Courier
                SingleSummaryDespatchNote(job:Ref_Number,func:Type)
                glo:Select1 = ''
                glo:Select2 = ''
                glo:Select3 = ''
                glo:Select4 = ''
            End !If tmp:PrintSummaryDespatch

    End !Case func:Type


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Despatch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:PrintDespatch',tmp:PrintDespatch,'Despatch',1)
    SolaceViewVars('tmp:PrintSummaryDespatch',tmp:PrintSummaryDespatch,'Despatch',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
