

   MEMBER('sba04app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA04011.INC'),ONCE        !Local module procedure declarations
                     END


Insert_Consignment_Note_Number PROCEDURE (f_label_type,f_number1,f_number2,f_consignment_note_number,f_passednumber,func:ShowConsignNo) !Generated from procedure template - Window

FilesOpened          BYTE
consignment_note_number_temp STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Despatch Routine'),AT(,,220,79),FONT('Tahoma',8,,FONT:regular),CENTER,ALRT(AltT),ALRT(F8Key),GRAY,DOUBLE
                       SHEET,AT(4,4,212,44),USE(?Sheet1),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Insert the Consignment Note Number to Despatch this job.'),AT(8,8,204,16),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:bold),COLOR(COLOR:Silver)
                           PROMPT('Consignment Note'),AT(8,32),USE(?consignment_note_number_temp:Prompt)
                           ENTRY(@s30),AT(84,32,124,10),USE(consignment_note_number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                         END
                       END
                       PANEL,AT(4,52,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Address Label [F8]'),AT(8,56,60,16),USE(?Address_Label),SKIP,LEFT,ICON('Print.gif')
                       BUTTON('&OK'),AT(100,56,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,56,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?consignment_note_number_temp:Prompt{prop:FontColor} = -1
    ?consignment_note_number_temp:Prompt{prop:Color} = 15066597
    If ?consignment_note_number_temp{prop:ReadOnly} = True
        ?consignment_note_number_temp{prop:FontColor} = 65793
        ?consignment_note_number_temp{prop:Color} = 15066597
    Elsif ?consignment_note_number_temp{prop:Req} = True
        ?consignment_note_number_temp{prop:FontColor} = 65793
        ?consignment_note_number_temp{prop:Color} = 8454143
    Else ! If ?consignment_note_number_temp{prop:Req} = True
        ?consignment_note_number_temp{prop:FontColor} = 65793
        ?consignment_note_number_temp{prop:Color} = 16777215
    End ! If ?consignment_note_number_temp{prop:Req} = True
    ?consignment_note_number_temp{prop:Trn} = 0
    ?consignment_note_number_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Insert_Consignment_Note_Number',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Insert_Consignment_Note_Number',1)
    SolaceViewVars('consignment_note_number_temp',consignment_note_number_temp,'Insert_Consignment_Note_Number',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?consignment_note_number_temp:Prompt;  SolaceCtrlName = '?consignment_note_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?consignment_note_number_temp;  SolaceCtrlName = '?consignment_note_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address_Label;  SolaceCtrlName = '?Address_Label';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Insert_Consignment_Note_Number')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Insert_Consignment_Note_Number')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Insert_Consignment_Note_Number',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Address_Label
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Address_Label, Accepted)
      Set(defaults)
      access:defaults.next()
      glo:select1 = f_number1
      glo:select2 = f_number2
      glo:select3 = f_label_type
      If f_label_type = 'SUB' Or f_label_type = 'TRA'
          Case def:label_printer_type
              Of 'TEC B-440 / B-442'
                  Address_Label_Multiple(Consignment_Note_Number_Temp)
              Of 'TEC B-452'
                  Address_Label_Multiple_B452
          End!Case def:themal_printer_type
      Else!If f_label_type = 'SUB' Or f_label_type = 'TRA'
          Case def:label_printer_type
              Of 'TEC B-440 / B-442'
                  Address_Label('DEL',Consignment_Note_Number_Temp)
              Of 'TEC B-452'
                  Address_Label_B452
          End!Case def:themal_printer_type
      End!If f_label_type = 'SUB' Or f_label_type = 'TRA'
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Address_Label, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If consignment_note_number_temp <> ''
          If (Sub(consignment_note_number_temp,1,Len(Clip(f_passednumber))) <> Clip(f_passednumber))
              If f_passednumber <> ''
                  Case MessageEx('WARNING! - Consolidation Error<13,10><13,10>Expected Consignment No: '&Clip(f_passednumber)&'<13,10>Returned Consignment No: '&Sub(consignment_note_number_temp,1,Len(Clip(f_passednumber)))&'<13,10><13,10>A unit has already been despatched to this account today, but LABEL G has returned the wrong Consignment Number.<13,10><13,10>DO NOT USE THE LABEL THAT HAS JUST BEEN PRODUCED!','ServiceBase 2000',|
                                 '','|&OK',1,1,'',,'Arial',8,16777215,700,CHARSET:ANSI,255,'',0,'',msgex:samewidths+ MSGEX:CENTERTEXT,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Case MessageEx('The despatch has been CANCELLED!','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,16777215,700,CHARSET:ANSI,255,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  f_consignment_note_number   = ''
              Else!If f_passednumber <> 0
                  f_consignment_note_number   = consignment_note_number_temp
              End!If f_passednumber <> 0
              Post(event:closewindow)
          Else
              f_consignment_note_number   = consignment_note_number_temp
              Post(event:closewindow)
          End!If consignment_note_number_temp <> f_passednumber
      Else
          Select(?consignment_note_number_temp)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      Case MessageEx('Warning! You have chosen to CANCEL the Despatch Procedure.<13,10><13,10>Only continue if you are NOT going to despatch this unit at this time.','ServiceBase 2000',|
                 'Styles\warn.ico','|&Continue|&Cancel',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
      Of 1 ! &Continue Button
          f_consignment_note_number = ''
          Post(event:closewindow)
      Of 2 ! &Cancel Button
      End!Case MessageEx
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Insert_Consignment_Note_Number')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F8Key
              Post(Event:Accepted,?Address_Label)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ALIAS(AltT,AltO)
      If f_passednumber <> '' And func:ShowConsignNo
          consignment_note_number_temp    = f_passednumber
      End !tmp:PassedConsignNo <> '' And tmp:ShowConsignNo
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

