

   MEMBER('sba04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA04009.INC'),ONCE        !Local module procedure declarations
                     END


DespatchUPS          PROCEDURE  (func:Multiple,func:AccountNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'DespatchUPS')      !Add Procedure to Log
  end


    !Company Name
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Fetch(sub:Account_Number_Key) = Level:Benign
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
            If tra:Use_Sub_Accounts = 'YES'
                gen:Line1   = Stripcomma(Clip(sub:Company_Name))
                If sub:Contact_Name <> ''            
                    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(sub:Contact_Name))                                              
                Else!If sub:Contact_Name <> ''
                    gen:Line1   = Clip(gen:Line1) & ',N/A'
                End!If sub:Contact_Name <> ''
            Else!If tra:Use_Sub_Accounts = 'YES'
                
            End!If tra:Use_Sub_Accounts = 'YES'
        End!If Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
    End!If Access:SUBTRACC.Fetch(sub:Account_Number_Key) = Level:Benign


    gen:Line1   = Stripcomma(Clip(job:Company_Name_Delivery))
    !Contact Name
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname))
    !Address
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Address_Line1_Delivery))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Address_LIne2_Delivery))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Address_LIne3_Delivery))
    !Blank
    gen:Line1   = Clip(gen:Line1) & ','
    !Postcode
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(Job:Postcode_Delivery))
    !Telephone
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Telephone_Delivery))
    !Ref 1
    If multi# = 1
        gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip('DB' & Format(dbt:Batch_Number,@s9)))    
    Else!If multi# = 1
        gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip('J' & Format(job:Ref_Number,@s9) & '/DB' & Format(dbt:Batch_Number,@s9)) & '/' & Clip(job:Order_Number))
    End!If multi# = 1

    !Ref 2
    gen:Line1   = Clip(gen:line1) & ',' 
    !Legs
    gen:Line1   = Clip(gen:Line1) & ','


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchUPS',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
